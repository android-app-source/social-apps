.class public abstract LX/51k;
.super LX/51i;
.source ""


# instance fields
.field public final a:Ljava/nio/ByteBuffer;

.field public final b:I

.field public final c:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 825448
    invoke-direct {p0, p1, p1}, LX/51k;-><init>(II)V

    .line 825449
    return-void
.end method

.method private constructor <init>(II)V
    .locals 2

    .prologue
    .line 825386
    invoke-direct {p0}, LX/51i;-><init>()V

    .line 825387
    rem-int v0, p2, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 825388
    add-int/lit8 v0, p2, 0x7

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, LX/51k;->a:Ljava/nio/ByteBuffer;

    .line 825389
    iput p2, p0, LX/51k;->b:I

    .line 825390
    iput p1, p0, LX/51k;->c:I

    .line 825391
    return-void

    .line 825392
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a([BII)LX/51h;
    .locals 2

    .prologue
    .line 825434
    invoke-static {p1, p2, p3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 825435
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    iget-object p1, p0, LX/51k;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result p1

    if-gt v1, p1, :cond_0

    .line 825436
    iget-object v1, p0, LX/51k;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 825437
    invoke-static {p0}, LX/51k;->c(LX/51k;)V

    .line 825438
    :goto_0
    move-object v0, p0

    .line 825439
    return-object v0

    .line 825440
    :cond_0
    iget v1, p0, LX/51k;->b:I

    iget-object p1, p0, LX/51k;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result p1

    sub-int p1, v1, p1

    .line 825441
    const/4 v1, 0x0

    :goto_1
    if-ge v1, p1, :cond_1

    .line 825442
    iget-object p2, p0, LX/51k;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result p3

    invoke-virtual {p2, p3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 825443
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 825444
    :cond_1
    invoke-static {p0}, LX/51k;->d(LX/51k;)V

    .line 825445
    :goto_2
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    iget p1, p0, LX/51k;->c:I

    if-lt v1, p1, :cond_2

    .line 825446
    invoke-virtual {p0, v0}, LX/51k;->a(Ljava/nio/ByteBuffer;)V

    goto :goto_2

    .line 825447
    :cond_2
    iget-object v1, p0, LX/51k;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    goto :goto_0
.end method

.method public static c(LX/51k;)V
    .locals 2

    .prologue
    .line 825431
    iget-object v0, p0, LX/51k;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    .line 825432
    invoke-static {p0}, LX/51k;->d(LX/51k;)V

    .line 825433
    :cond_0
    return-void
.end method

.method public static d(LX/51k;)V
    .locals 2

    .prologue
    .line 825426
    iget-object v0, p0, LX/51k;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 825427
    :goto_0
    iget-object v0, p0, LX/51k;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    iget v1, p0, LX/51k;->c:I

    if-lt v0, v1, :cond_0

    .line 825428
    iget-object v0, p0, LX/51k;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {p0, v0}, LX/51k;->a(Ljava/nio/ByteBuffer;)V

    goto :goto_0

    .line 825429
    :cond_0
    iget-object v0, p0, LX/51k;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->compact()Ljava/nio/ByteBuffer;

    .line 825430
    return-void
.end method


# virtual methods
.method public final a(C)LX/51h;
    .locals 1

    .prologue
    .line 825423
    iget-object v0, p0, LX/51k;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putChar(C)Ljava/nio/ByteBuffer;

    .line 825424
    invoke-static {p0}, LX/51k;->c(LX/51k;)V

    .line 825425
    return-object p0
.end method

.method public final a(I)LX/51h;
    .locals 1

    .prologue
    .line 825420
    iget-object v0, p0, LX/51k;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 825421
    invoke-static {p0}, LX/51k;->c(LX/51k;)V

    .line 825422
    return-object p0
.end method

.method public final a(J)LX/51h;
    .locals 1

    .prologue
    .line 825417
    iget-object v0, p0, LX/51k;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1, p2}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 825418
    invoke-static {p0}, LX/51k;->c(LX/51k;)V

    .line 825419
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)LX/51h;
    .locals 2

    .prologue
    .line 825413
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 825414
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-virtual {p0, v1}, LX/51k;->a(C)LX/51h;

    .line 825415
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 825416
    :cond_0
    return-object p0
.end method

.method public final a()LX/51o;
    .locals 1

    .prologue
    .line 825408
    invoke-static {p0}, LX/51k;->d(LX/51k;)V

    .line 825409
    iget-object v0, p0, LX/51k;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 825410
    iget-object v0, p0, LX/51k;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    if-lez v0, :cond_0

    .line 825411
    iget-object v0, p0, LX/51k;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {p0, v0}, LX/51k;->b(Ljava/nio/ByteBuffer;)V

    .line 825412
    :cond_0
    invoke-virtual {p0}, LX/51k;->b()LX/51o;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(Ljava/nio/ByteBuffer;)V
.end method

.method public final synthetic b([BII)LX/51g;
    .locals 1

    .prologue
    .line 825407
    invoke-direct {p0, p1, p2, p3}, LX/51k;->a([BII)LX/51h;

    move-result-object v0

    return-object v0
.end method

.method public final b(B)LX/51h;
    .locals 1

    .prologue
    .line 825404
    iget-object v0, p0, LX/51k;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 825405
    invoke-static {p0}, LX/51k;->c(LX/51k;)V

    .line 825406
    return-object p0
.end method

.method public final b([B)LX/51h;
    .locals 2

    .prologue
    .line 825403
    const/4 v0, 0x0

    array-length v1, p1

    invoke-direct {p0, p1, v0, v1}, LX/51k;->a([BII)LX/51h;

    move-result-object v0

    return-object v0
.end method

.method public abstract b()LX/51o;
.end method

.method public b(Ljava/nio/ByteBuffer;)V
    .locals 2

    .prologue
    .line 825395
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 825396
    iget v0, p0, LX/51k;->c:I

    add-int/lit8 v0, v0, 0x7

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 825397
    :goto_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    iget v1, p0, LX/51k;->c:I

    if-ge v0, v1, :cond_0

    .line 825398
    const-wide/16 v0, 0x0

    invoke-virtual {p1, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    goto :goto_0

    .line 825399
    :cond_0
    iget v0, p0, LX/51k;->c:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 825400
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 825401
    invoke-virtual {p0, p1}, LX/51k;->a(Ljava/nio/ByteBuffer;)V

    .line 825402
    return-void
.end method

.method public final synthetic c(B)LX/51g;
    .locals 1

    .prologue
    .line 825394
    invoke-virtual {p0, p1}, LX/51k;->b(B)LX/51h;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c([B)LX/51g;
    .locals 1

    .prologue
    .line 825393
    invoke-virtual {p0, p1}, LX/51k;->b([B)LX/51h;

    move-result-object v0

    return-object v0
.end method
