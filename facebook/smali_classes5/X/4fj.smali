.class public LX/4fj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final a:[LX/0RI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LX/0RI",
            "<+TT;>;"
        }
    .end annotation
.end field

.field private final b:LX/0QA;

.field private final c:LX/0RI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0RI",
            "<+TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0QA;Ljava/util/List;LX/0RI;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QA;",
            "Ljava/util/List",
            "<",
            "LX/0RI",
            "<+TT;>;>;",
            "LX/0RI",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 798693
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 798694
    iput-object p1, p0, LX/4fj;->b:LX/0QA;

    .line 798695
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [LX/0RI;

    invoke-interface {p2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0RI;

    iput-object v0, p0, LX/4fj;->a:[LX/0RI;

    .line 798696
    iput-object p3, p0, LX/4fj;->c:LX/0RI;

    .line 798697
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 798698
    const-string v0, "MultiBinderProvider: %s"

    iget-object v1, p0, LX/4fj;->c:LX/0RI;

    const v2, 0x6bd3aca0

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 798699
    :try_start_0
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/4fj;->b:LX/0QA;

    invoke-virtual {v1}, LX/0QA;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    new-instance v2, LX/4fi;

    invoke-direct {v2, p0}, LX/4fi;-><init>(LX/4fj;)V

    invoke-direct {v0, v1, v2}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 798700
    const v1, -0x21921423

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, 0x23f5bc4e

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
