.class public LX/4UQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 722830
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 722831
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 722832
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 722833
    :goto_0
    return v1

    .line 722834
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 722835
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 722836
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 722837
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 722838
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 722839
    const-string v3, "nodes"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 722840
    invoke-static {p0, p1}, LX/2bc;->b(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 722841
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 722842
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 722843
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 722844
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 722845
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 722846
    if-eqz v0, :cond_0

    .line 722847
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 722848
    invoke-static {p0, v0, p2, p3}, LX/2bc;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 722849
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 722850
    return-void
.end method
