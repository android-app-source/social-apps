.class public LX/4MC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 687128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 687091
    const/4 v2, 0x0

    .line 687092
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v6, :cond_7

    .line 687093
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 687094
    :goto_0
    return v0

    .line 687095
    :cond_0
    const-string v11, "rsvp_time"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 687096
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v7, v1

    .line 687097
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_4

    .line 687098
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 687099
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 687100
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 687101
    const-string v11, "node"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 687102
    invoke-static {p0, p1}, LX/2bM;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 687103
    :cond_2
    const-string v11, "seen_state"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 687104
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v6

    move-object v8, v6

    move v6, v1

    goto :goto_1

    .line 687105
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 687106
    :cond_4
    const/4 v10, 0x3

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 687107
    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 687108
    if-eqz v7, :cond_5

    move-object v0, p1

    .line 687109
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 687110
    :cond_5
    if-eqz v6, :cond_6

    .line 687111
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v8}, LX/186;->a(ILjava/lang/Enum;)V

    .line 687112
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :cond_7
    move v6, v0

    move v7, v0

    move-object v8, v2

    move v9, v0

    move-wide v2, v4

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 687113
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 687114
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 687115
    if-eqz v0, :cond_0

    .line 687116
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687117
    invoke-static {p0, v0, p2, p3}, LX/2bM;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 687118
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 687119
    cmp-long v2, v0, v6

    if-eqz v2, :cond_1

    .line 687120
    const-string v2, "rsvp_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687121
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 687122
    :cond_1
    invoke-virtual {p0, p1, v4, v3}, LX/15i;->a(IIS)S

    move-result v0

    .line 687123
    if-eqz v0, :cond_2

    .line 687124
    const-string v0, "seen_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687125
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 687126
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 687127
    return-void
.end method
