.class public LX/3l3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3l4;


# instance fields
.field private final a:LX/34x;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:LX/0hs;


# direct methods
.method public constructor <init>(LX/34x;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 633542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 633543
    iput-object p1, p0, LX/3l3;->a:LX/34x;

    .line 633544
    return-void
.end method

.method private g()Z
    .locals 1

    .prologue
    .line 633541
    iget-object v0, p0, LX/3l3;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 633540
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 633537
    invoke-direct {p0}, LX/3l3;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 633538
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    .line 633539
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(LX/5L2;)LX/ASZ;
    .locals 1

    .prologue
    .line 633536
    sget-object v0, LX/ASZ;->NONE:LX/ASZ;

    return-object v0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 633535
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 633534
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 633532
    iput-object p1, p0, LX/3l3;->b:Landroid/view/View;

    .line 633533
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 633506
    invoke-direct {p0}, LX/3l3;->g()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 633507
    iget-object v0, p0, LX/3l3;->d:LX/0hs;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3l3;->d:LX/0hs;

    .line 633508
    iget-boolean p1, v0, LX/0ht;->r:Z

    move v0, p1

    .line 633509
    if-eqz v0, :cond_0

    .line 633510
    iget-object v0, p0, LX/3l3;->d:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 633511
    const/4 v0, 0x0

    iput-object v0, p0, LX/3l3;->d:LX/0hs;

    .line 633512
    :cond_0
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 633531
    const-string v0, "4233"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 633530
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->COMPOSER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 633526
    iput-object v0, p0, LX/3l3;->b:Landroid/view/View;

    .line 633527
    iput-object v0, p0, LX/3l3;->c:Landroid/view/View;

    .line 633528
    iput-object v0, p0, LX/3l3;->d:LX/0hs;

    .line 633529
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 633518
    invoke-direct {p0}, LX/3l3;->g()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 633519
    invoke-virtual {p0}, LX/3l3;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 633520
    :goto_0
    return-void

    .line 633521
    :cond_0
    iget-object v0, p0, LX/3l3;->c:Landroid/view/View;

    if-nez v0, :cond_1

    .line 633522
    iget-object v0, p0, LX/3l3;->b:Landroid/view/View;

    const v1, 0x7f0d2c24

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/3l3;->c:Landroid/view/View;

    .line 633523
    :cond_1
    iget-object v0, p0, LX/3l3;->a:LX/34x;

    iget-object v1, p0, LX/3l3;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082f13

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/34x;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 633524
    sget-object v1, LX/DJf;->BLUE:LX/DJf;

    iget-object v2, p0, LX/3l3;->c:Landroid/view/View;

    invoke-static {v1, v2, v0}, LX/DJg;->a(LX/DJf;Landroid/view/View;Ljava/lang/String;)LX/0hs;

    move-result-object v0

    iput-object v0, p0, LX/3l3;->d:LX/0hs;

    .line 633525
    iget-object v0, p0, LX/3l3;->d:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->d()V

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 633513
    iget-object v0, p0, LX/3l3;->d:LX/0hs;

    if-nez v0, :cond_0

    .line 633514
    const/4 v0, 0x0

    .line 633515
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/3l3;->d:LX/0hs;

    .line 633516
    iget-boolean p0, v0, LX/0ht;->r:Z

    move v0, p0

    .line 633517
    goto :goto_0
.end method
