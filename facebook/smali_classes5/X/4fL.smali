.class public final LX/4fL;
.super LX/1eP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1eP",
        "<",
        "LX/1FL;",
        "LX/1FL;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4fM;

.field private final b:LX/1cW;

.field private final c:I

.field private final d:LX/1o9;


# direct methods
.method public constructor <init>(LX/4fM;LX/1cd;LX/1cW;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FL;",
            ">;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 798335
    iput-object p1, p0, LX/4fL;->a:LX/4fM;

    .line 798336
    invoke-direct {p0, p2}, LX/1eP;-><init>(LX/1cd;)V

    .line 798337
    iput-object p3, p0, LX/4fL;->b:LX/1cW;

    .line 798338
    iput p4, p0, LX/4fL;->c:I

    .line 798339
    iget-object v0, p0, LX/4fL;->b:LX/1cW;

    .line 798340
    iget-object p1, v0, LX/1cW;->a:LX/1bf;

    move-object v0, p1

    .line 798341
    iget-object p1, v0, LX/1bf;->h:LX/1o9;

    move-object v0, p1

    .line 798342
    iput-object v0, p0, LX/4fL;->d:LX/1o9;

    .line 798343
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Z)V
    .locals 4

    .prologue
    .line 798315
    check-cast p1, LX/1FL;

    .line 798316
    if-eqz p1, :cond_2

    if-eqz p2, :cond_0

    iget-object v0, p0, LX/4fL;->d:LX/1o9;

    invoke-static {p1, v0}, LX/4fN;->a(LX/1FL;LX/1o9;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 798317
    :cond_0
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 798318
    invoke-virtual {v0, p1, p2}, LX/1cd;->b(Ljava/lang/Object;Z)V

    .line 798319
    :cond_1
    :goto_0
    return-void

    .line 798320
    :cond_2
    if-eqz p2, :cond_1

    .line 798321
    invoke-static {p1}, LX/1FL;->d(LX/1FL;)V

    .line 798322
    iget-object v0, p0, LX/4fL;->a:LX/4fM;

    iget v1, p0, LX/4fL;->c:I

    add-int/lit8 v1, v1, 0x1

    .line 798323
    iget-object v2, p0, LX/1eP;->a:LX/1cd;

    move-object v2, v2

    .line 798324
    iget-object v3, p0, LX/4fL;->b:LX/1cW;

    invoke-static {v0, v1, v2, v3}, LX/4fM;->a$redex0(LX/4fM;ILX/1cd;LX/1cW;)Z

    move-result v0

    .line 798325
    if-nez v0, :cond_1

    .line 798326
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 798327
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/1cd;->b(Ljava/lang/Object;Z)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 798328
    iget-object v0, p0, LX/4fL;->a:LX/4fM;

    iget v1, p0, LX/4fL;->c:I

    add-int/lit8 v1, v1, 0x1

    .line 798329
    iget-object v2, p0, LX/1eP;->a:LX/1cd;

    move-object v2, v2

    .line 798330
    iget-object v3, p0, LX/4fL;->b:LX/1cW;

    invoke-static {v0, v1, v2, v3}, LX/4fM;->a$redex0(LX/4fM;ILX/1cd;LX/1cW;)Z

    move-result v0

    .line 798331
    if-nez v0, :cond_0

    .line 798332
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 798333
    invoke-virtual {v0, p1}, LX/1cd;->b(Ljava/lang/Throwable;)V

    .line 798334
    :cond_0
    return-void
.end method
