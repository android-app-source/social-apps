.class public LX/4ca;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MQ;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/4ca;


# instance fields
.field public final a:LX/14D;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1MQ;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1MQ;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/03V;

.field public e:LX/1MQ;


# direct methods
.method public constructor <init>(LX/14D;LX/0Ot;LX/0Ot;LX/03V;)V
    .locals 2
    .param p2    # LX/0Ot;
        .annotation runtime Lcom/facebook/http/annotations/ApacheExecutor;
        .end annotation
    .end param
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/http/executors/liger/annotations/LigerExecutor;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/14D;",
            "LX/0Ot",
            "<",
            "LX/1MQ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1MQ;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 795195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 795196
    iput-object p1, p0, LX/4ca;->a:LX/14D;

    .line 795197
    iput-object p2, p0, LX/4ca;->b:LX/0Ot;

    .line 795198
    iput-object p3, p0, LX/4ca;->c:LX/0Ot;

    .line 795199
    iput-object p4, p0, LX/4ca;->d:LX/03V;

    .line 795200
    new-instance v0, Lcom/facebook/http/executors/qebased/QeHttpRequestExecutor$InitializeExecutorThread;

    invoke-direct {v0, p0}, Lcom/facebook/http/executors/qebased/QeHttpRequestExecutor$InitializeExecutorThread;-><init>(LX/4ca;)V

    invoke-virtual {v0}, Lcom/facebook/http/executors/qebased/QeHttpRequestExecutor$InitializeExecutorThread;->start()V

    .line 795201
    return-void
.end method

.method private declared-synchronized a()LX/1MQ;
    .locals 2

    .prologue
    .line 795172
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v0, p0, LX/4ca;->e:LX/1MQ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 795173
    const v0, 0x147c4e42

    :try_start_1
    invoke-static {p0, v0}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 795174
    :catch_0
    move-exception v0

    .line 795175
    :try_start_2
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 795176
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 795177
    :cond_0
    :try_start_3
    iget-object v0, p0, LX/4ca;->e:LX/1MQ;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/4ca;
    .locals 7

    .prologue
    .line 795182
    sget-object v0, LX/4ca;->f:LX/4ca;

    if-nez v0, :cond_1

    .line 795183
    const-class v1, LX/4ca;

    monitor-enter v1

    .line 795184
    :try_start_0
    sget-object v0, LX/4ca;->f:LX/4ca;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 795185
    if-eqz v2, :cond_0

    .line 795186
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 795187
    new-instance v5, LX/4ca;

    invoke-static {v0}, LX/14D;->a(LX/0QB;)LX/14D;

    move-result-object v3

    check-cast v3, LX/14D;

    const/16 v4, 0x24f9

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v4, 0xb66

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {v5, v3, v6, p0, v4}, LX/4ca;-><init>(LX/14D;LX/0Ot;LX/0Ot;LX/03V;)V

    .line 795188
    move-object v0, v5

    .line 795189
    sput-object v0, LX/4ca;->f:LX/4ca;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 795190
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 795191
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 795192
    :cond_1
    sget-object v0, LX/4ca;->f:LX/4ca;

    return-object v0

    .line 795193
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 795194
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lorg/apache/http/client/methods/HttpUriRequest;LX/15F;Lorg/apache/http/protocol/HttpContext;LX/1iW;)Lorg/apache/http/HttpResponse;
    .locals 1

    .prologue
    .line 795181
    invoke-direct {p0}, LX/4ca;->a()LX/1MQ;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, LX/1MQ;->a(Lorg/apache/http/client/methods/HttpUriRequest;LX/15F;Lorg/apache/http/protocol/HttpContext;LX/1iW;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 795179
    invoke-direct {p0}, LX/4ca;->a()LX/1MQ;

    move-result-object v0

    invoke-interface {v0}, LX/1MQ;->d()V

    .line 795180
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 795178
    invoke-direct {p0}, LX/4ca;->a()LX/1MQ;

    move-result-object v0

    invoke-interface {v0}, LX/1MQ;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
