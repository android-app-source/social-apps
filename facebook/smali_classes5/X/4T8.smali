.class public LX/4T8;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 716427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 59

    .prologue
    .line 716428
    const/16 v53, 0x0

    .line 716429
    const/16 v52, 0x0

    .line 716430
    const/16 v51, 0x0

    .line 716431
    const/16 v50, 0x0

    .line 716432
    const/16 v49, 0x0

    .line 716433
    const/16 v48, 0x0

    .line 716434
    const/16 v47, 0x0

    .line 716435
    const/16 v46, 0x0

    .line 716436
    const/16 v45, 0x0

    .line 716437
    const/16 v44, 0x0

    .line 716438
    const/16 v43, 0x0

    .line 716439
    const/16 v42, 0x0

    .line 716440
    const/16 v41, 0x0

    .line 716441
    const/16 v40, 0x0

    .line 716442
    const/16 v39, 0x0

    .line 716443
    const/16 v38, 0x0

    .line 716444
    const/16 v37, 0x0

    .line 716445
    const/16 v36, 0x0

    .line 716446
    const/16 v35, 0x0

    .line 716447
    const/16 v34, 0x0

    .line 716448
    const/16 v33, 0x0

    .line 716449
    const/16 v32, 0x0

    .line 716450
    const-wide/16 v30, 0x0

    .line 716451
    const/16 v29, 0x0

    .line 716452
    const/16 v28, 0x0

    .line 716453
    const/16 v27, 0x0

    .line 716454
    const/16 v26, 0x0

    .line 716455
    const/16 v25, 0x0

    .line 716456
    const/16 v24, 0x0

    .line 716457
    const/16 v23, 0x0

    .line 716458
    const/16 v22, 0x0

    .line 716459
    const-wide/16 v20, 0x0

    .line 716460
    const/16 v19, 0x0

    .line 716461
    const/16 v18, 0x0

    .line 716462
    const/16 v17, 0x0

    .line 716463
    const/16 v16, 0x0

    .line 716464
    const/4 v15, 0x0

    .line 716465
    const/4 v14, 0x0

    .line 716466
    const/4 v13, 0x0

    .line 716467
    const/4 v12, 0x0

    .line 716468
    const/4 v11, 0x0

    .line 716469
    const/4 v10, 0x0

    .line 716470
    const/4 v9, 0x0

    .line 716471
    const/4 v8, 0x0

    .line 716472
    const/4 v7, 0x0

    .line 716473
    const/4 v6, 0x0

    .line 716474
    const/4 v5, 0x0

    .line 716475
    const/4 v4, 0x0

    .line 716476
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v54

    sget-object v55, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v54

    move-object/from16 v1, v55

    if-eq v0, v1, :cond_32

    .line 716477
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 716478
    const/4 v4, 0x0

    .line 716479
    :goto_0
    return v4

    .line 716480
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v55, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v55

    if-eq v4, v0, :cond_24

    .line 716481
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 716482
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 716483
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v55

    sget-object v56, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v55

    move-object/from16 v1, v56

    if-eq v0, v1, :cond_0

    if-eqz v4, :cond_0

    .line 716484
    const-string v55, "active_team_with_ball"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_1

    .line 716485
    invoke-static/range {p0 .. p1}, LX/2bc;->a(LX/15w;LX/186;)I

    move-result v4

    move/from16 v54, v4

    goto :goto_1

    .line 716486
    :cond_1
    const-string v55, "away_team"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_2

    .line 716487
    invoke-static/range {p0 .. p1}, LX/2bc;->a(LX/15w;LX/186;)I

    move-result v4

    move/from16 v53, v4

    goto :goto_1

    .line 716488
    :cond_2
    const-string v55, "away_team_fan_count"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_3

    .line 716489
    const/4 v4, 0x1

    .line 716490
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v19

    move/from16 v52, v19

    move/from16 v19, v4

    goto :goto_1

    .line 716491
    :cond_3
    const-string v55, "away_team_market"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_4

    .line 716492
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    move/from16 v51, v4

    goto :goto_1

    .line 716493
    :cond_4
    const-string v55, "away_team_name"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_5

    .line 716494
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    move/from16 v50, v4

    goto/16 :goto_1

    .line 716495
    :cond_5
    const-string v55, "away_team_score"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_6

    .line 716496
    const/4 v4, 0x1

    .line 716497
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v18

    move/from16 v49, v18

    move/from16 v18, v4

    goto/16 :goto_1

    .line 716498
    :cond_6
    const-string v55, "broadcast_network"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_7

    .line 716499
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    move/from16 v48, v4

    goto/16 :goto_1

    .line 716500
    :cond_7
    const-string v55, "clock"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_8

    .line 716501
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    move/from16 v47, v4

    goto/16 :goto_1

    .line 716502
    :cond_8
    const-string v55, "facts"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_9

    .line 716503
    invoke-static/range {p0 .. p1}, LX/4TA;->a(LX/15w;LX/186;)I

    move-result v4

    move/from16 v46, v4

    goto/16 :goto_1

    .line 716504
    :cond_9
    const-string v55, "fan_favorite"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_a

    .line 716505
    invoke-static/range {p0 .. p1}, LX/4TB;->a(LX/15w;LX/186;)I

    move-result v4

    move/from16 v45, v4

    goto/16 :goto_1

    .line 716506
    :cond_a
    const-string v55, "first_team_fan_count"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_b

    .line 716507
    const/4 v4, 0x1

    .line 716508
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v17

    move/from16 v44, v17

    move/from16 v17, v4

    goto/16 :goto_1

    .line 716509
    :cond_b
    const-string v55, "first_team_primary_color"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_c

    .line 716510
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    move/from16 v43, v4

    goto/16 :goto_1

    .line 716511
    :cond_c
    const-string v55, "first_team_score"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_d

    .line 716512
    const/4 v4, 0x1

    .line 716513
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v16

    move/from16 v42, v16

    move/from16 v16, v4

    goto/16 :goto_1

    .line 716514
    :cond_d
    const-string v55, "has_match_started"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_e

    .line 716515
    const/4 v4, 0x1

    .line 716516
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v15

    move/from16 v41, v15

    move v15, v4

    goto/16 :goto_1

    .line 716517
    :cond_e
    const-string v55, "home_team"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_f

    .line 716518
    invoke-static/range {p0 .. p1}, LX/2bc;->a(LX/15w;LX/186;)I

    move-result v4

    move/from16 v40, v4

    goto/16 :goto_1

    .line 716519
    :cond_f
    const-string v55, "home_team_fan_count"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_10

    .line 716520
    const/4 v4, 0x1

    .line 716521
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    move/from16 v39, v14

    move v14, v4

    goto/16 :goto_1

    .line 716522
    :cond_10
    const-string v55, "home_team_market"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_11

    .line 716523
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    move/from16 v38, v4

    goto/16 :goto_1

    .line 716524
    :cond_11
    const-string v55, "home_team_name"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_12

    .line 716525
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    move/from16 v37, v4

    goto/16 :goto_1

    .line 716526
    :cond_12
    const-string v55, "home_team_score"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_13

    .line 716527
    const/4 v4, 0x1

    .line 716528
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    move/from16 v36, v9

    move v9, v4

    goto/16 :goto_1

    .line 716529
    :cond_13
    const-string v55, "id"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_14

    .line 716530
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    move/from16 v35, v4

    goto/16 :goto_1

    .line 716531
    :cond_14
    const-string v55, "match_page"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_15

    .line 716532
    invoke-static/range {p0 .. p1}, LX/2bc;->a(LX/15w;LX/186;)I

    move-result v4

    move/from16 v34, v4

    goto/16 :goto_1

    .line 716533
    :cond_15
    const-string v55, "period"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_16

    .line 716534
    const/4 v4, 0x1

    .line 716535
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v8

    move/from16 v33, v8

    move v8, v4

    goto/16 :goto_1

    .line 716536
    :cond_16
    const-string v55, "scheduled_start_time"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_17

    .line 716537
    const/4 v4, 0x1

    .line 716538
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v5, v4

    goto/16 :goto_1

    .line 716539
    :cond_17
    const-string v55, "second_team_fan_count"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_18

    .line 716540
    const/4 v4, 0x1

    .line 716541
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    move/from16 v32, v13

    move v13, v4

    goto/16 :goto_1

    .line 716542
    :cond_18
    const-string v55, "second_team_primary_color"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_19

    .line 716543
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    move/from16 v31, v4

    goto/16 :goto_1

    .line 716544
    :cond_19
    const-string v55, "second_team_score"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_1a

    .line 716545
    const/4 v4, 0x1

    .line 716546
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    move/from16 v30, v12

    move v12, v4

    goto/16 :goto_1

    .line 716547
    :cond_1a
    const-string v55, "sports_data_cover_photo"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_1b

    .line 716548
    invoke-static/range {p0 .. p1}, LX/2sY;->a(LX/15w;LX/186;)I

    move-result v4

    move/from16 v29, v4

    goto/16 :goto_1

    .line 716549
    :cond_1b
    const-string v55, "sports_data_fallback_photo"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_1c

    .line 716550
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v4

    move/from16 v28, v4

    goto/16 :goto_1

    .line 716551
    :cond_1c
    const-string v55, "sports_subtitle"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_1d

    .line 716552
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    move/from16 v27, v4

    goto/16 :goto_1

    .line 716553
    :cond_1d
    const-string v55, "status"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_1e

    .line 716554
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    move/from16 v26, v4

    goto/16 :goto_1

    .line 716555
    :cond_1e
    const-string v55, "status_text"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_1f

    .line 716556
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    move/from16 v23, v4

    goto/16 :goto_1

    .line 716557
    :cond_1f
    const-string v55, "updated_time"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_20

    .line 716558
    const/4 v4, 0x1

    .line 716559
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v24

    move v11, v4

    goto/16 :goto_1

    .line 716560
    :cond_20
    const-string v55, "url"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_21

    .line 716561
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    move/from16 v22, v4

    goto/16 :goto_1

    .line 716562
    :cond_21
    const-string v55, "viewer_can_vote_fan_favorite"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_22

    .line 716563
    const/4 v4, 0x1

    .line 716564
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    move/from16 v21, v10

    move v10, v4

    goto/16 :goto_1

    .line 716565
    :cond_22
    const-string v55, "winning_team"

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_23

    .line 716566
    invoke-static/range {p0 .. p1}, LX/2bc;->a(LX/15w;LX/186;)I

    move-result v4

    move/from16 v20, v4

    goto/16 :goto_1

    .line 716567
    :cond_23
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 716568
    :cond_24
    const/16 v4, 0x24

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 716569
    const/4 v4, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 716570
    const/4 v4, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 716571
    if-eqz v19, :cond_25

    .line 716572
    const/4 v4, 0x3

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v52

    move/from16 v2, v19

    invoke-virtual {v0, v4, v1, v2}, LX/186;->a(III)V

    .line 716573
    :cond_25
    const/4 v4, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 716574
    const/4 v4, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 716575
    if-eqz v18, :cond_26

    .line 716576
    const/4 v4, 0x6

    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v49

    move/from16 v2, v18

    invoke-virtual {v0, v4, v1, v2}, LX/186;->a(III)V

    .line 716577
    :cond_26
    const/4 v4, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 716578
    const/16 v4, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 716579
    const/16 v4, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 716580
    const/16 v4, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 716581
    if-eqz v17, :cond_27

    .line 716582
    const/16 v4, 0xb

    const/16 v17, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v44

    move/from16 v2, v17

    invoke-virtual {v0, v4, v1, v2}, LX/186;->a(III)V

    .line 716583
    :cond_27
    const/16 v4, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 716584
    if-eqz v16, :cond_28

    .line 716585
    const/16 v4, 0xd

    const/16 v16, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v42

    move/from16 v2, v16

    invoke-virtual {v0, v4, v1, v2}, LX/186;->a(III)V

    .line 716586
    :cond_28
    if-eqz v15, :cond_29

    .line 716587
    const/16 v4, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 716588
    :cond_29
    const/16 v4, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 716589
    if-eqz v14, :cond_2a

    .line 716590
    const/16 v4, 0x10

    const/4 v14, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v4, v1, v14}, LX/186;->a(III)V

    .line 716591
    :cond_2a
    const/16 v4, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 716592
    const/16 v4, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 716593
    if-eqz v9, :cond_2b

    .line 716594
    const/16 v4, 0x13

    const/4 v9, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v4, v1, v9}, LX/186;->a(III)V

    .line 716595
    :cond_2b
    const/16 v4, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 716596
    const/16 v4, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 716597
    if-eqz v8, :cond_2c

    .line 716598
    const/16 v4, 0x16

    const/4 v8, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v4, v1, v8}, LX/186;->a(III)V

    .line 716599
    :cond_2c
    if-eqz v5, :cond_2d

    .line 716600
    const/16 v5, 0x17

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IJJ)V

    .line 716601
    :cond_2d
    if-eqz v13, :cond_2e

    .line 716602
    const/16 v4, 0x18

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v4, v1, v5}, LX/186;->a(III)V

    .line 716603
    :cond_2e
    const/16 v4, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 716604
    if-eqz v12, :cond_2f

    .line 716605
    const/16 v4, 0x1a

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v4, v1, v5}, LX/186;->a(III)V

    .line 716606
    :cond_2f
    const/16 v4, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 716607
    const/16 v4, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 716608
    const/16 v4, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 716609
    const/16 v4, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 716610
    const/16 v4, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 716611
    if-eqz v11, :cond_30

    .line 716612
    const/16 v5, 0x20

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    move-wide/from16 v6, v24

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IJJ)V

    .line 716613
    :cond_30
    const/16 v4, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 716614
    if-eqz v10, :cond_31

    .line 716615
    const/16 v4, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 716616
    :cond_31
    const/16 v4, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 716617
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_32
    move/from16 v54, v53

    move/from16 v53, v52

    move/from16 v52, v51

    move/from16 v51, v50

    move/from16 v50, v49

    move/from16 v49, v48

    move/from16 v48, v47

    move/from16 v47, v46

    move/from16 v46, v45

    move/from16 v45, v44

    move/from16 v44, v43

    move/from16 v43, v42

    move/from16 v42, v41

    move/from16 v41, v40

    move/from16 v40, v39

    move/from16 v39, v38

    move/from16 v38, v37

    move/from16 v37, v36

    move/from16 v36, v35

    move/from16 v35, v34

    move/from16 v34, v33

    move/from16 v33, v32

    move/from16 v32, v29

    move/from16 v29, v26

    move/from16 v26, v23

    move/from16 v23, v22

    move/from16 v22, v19

    move/from16 v19, v16

    move/from16 v16, v13

    move v13, v7

    move-wide/from16 v57, v30

    move/from16 v31, v28

    move/from16 v30, v27

    move/from16 v28, v25

    move/from16 v27, v24

    move-wide/from16 v24, v20

    move/from16 v21, v18

    move/from16 v20, v17

    move/from16 v17, v14

    move/from16 v18, v15

    move v15, v12

    move v14, v11

    move v12, v6

    move v11, v5

    move v5, v8

    move-wide/from16 v6, v57

    move v8, v9

    move v9, v10

    move v10, v4

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 716618
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 716619
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 716620
    if-eqz v0, :cond_0

    .line 716621
    const-string v1, "active_team_with_ball"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716622
    invoke-static {p0, v0, p2, p3}, LX/2bc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 716623
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 716624
    if-eqz v0, :cond_1

    .line 716625
    const-string v1, "away_team"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716626
    invoke-static {p0, v0, p2, p3}, LX/2bc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 716627
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 716628
    if-eqz v0, :cond_2

    .line 716629
    const-string v1, "away_team_fan_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716630
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 716631
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 716632
    if-eqz v0, :cond_3

    .line 716633
    const-string v1, "away_team_market"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716634
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716635
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 716636
    if-eqz v0, :cond_4

    .line 716637
    const-string v1, "away_team_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716638
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716639
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 716640
    if-eqz v0, :cond_5

    .line 716641
    const-string v1, "away_team_score"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716642
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 716643
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 716644
    if-eqz v0, :cond_6

    .line 716645
    const-string v1, "broadcast_network"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716646
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716647
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 716648
    if-eqz v0, :cond_7

    .line 716649
    const-string v1, "clock"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716650
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716651
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 716652
    if-eqz v0, :cond_8

    .line 716653
    const-string v1, "facts"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716654
    invoke-static {p0, v0, p2, p3}, LX/4TA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 716655
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 716656
    if-eqz v0, :cond_9

    .line 716657
    const-string v1, "fan_favorite"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716658
    invoke-static {p0, v0, p2, p3}, LX/4TB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 716659
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 716660
    if-eqz v0, :cond_a

    .line 716661
    const-string v1, "first_team_fan_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716662
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 716663
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 716664
    if-eqz v0, :cond_b

    .line 716665
    const-string v1, "first_team_primary_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716666
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716667
    :cond_b
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 716668
    if-eqz v0, :cond_c

    .line 716669
    const-string v1, "first_team_score"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716670
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 716671
    :cond_c
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 716672
    if-eqz v0, :cond_d

    .line 716673
    const-string v1, "has_match_started"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716674
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 716675
    :cond_d
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 716676
    if-eqz v0, :cond_e

    .line 716677
    const-string v1, "home_team"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716678
    invoke-static {p0, v0, p2, p3}, LX/2bc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 716679
    :cond_e
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 716680
    if-eqz v0, :cond_f

    .line 716681
    const-string v1, "home_team_fan_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716682
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 716683
    :cond_f
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 716684
    if-eqz v0, :cond_10

    .line 716685
    const-string v1, "home_team_market"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716686
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716687
    :cond_10
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 716688
    if-eqz v0, :cond_11

    .line 716689
    const-string v1, "home_team_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716690
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716691
    :cond_11
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 716692
    if-eqz v0, :cond_12

    .line 716693
    const-string v1, "home_team_score"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716694
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 716695
    :cond_12
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 716696
    if-eqz v0, :cond_13

    .line 716697
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716698
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716699
    :cond_13
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 716700
    if-eqz v0, :cond_14

    .line 716701
    const-string v1, "match_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716702
    invoke-static {p0, v0, p2, p3}, LX/2bc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 716703
    :cond_14
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 716704
    if-eqz v0, :cond_15

    .line 716705
    const-string v1, "period"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716706
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 716707
    :cond_15
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 716708
    cmp-long v2, v0, v4

    if-eqz v2, :cond_16

    .line 716709
    const-string v2, "scheduled_start_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716710
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 716711
    :cond_16
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 716712
    if-eqz v0, :cond_17

    .line 716713
    const-string v1, "second_team_fan_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716714
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 716715
    :cond_17
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 716716
    if-eqz v0, :cond_18

    .line 716717
    const-string v1, "second_team_primary_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716718
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716719
    :cond_18
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 716720
    if-eqz v0, :cond_19

    .line 716721
    const-string v1, "second_team_score"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716722
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 716723
    :cond_19
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 716724
    if-eqz v0, :cond_1a

    .line 716725
    const-string v1, "sports_data_cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716726
    invoke-static {p0, v0, p2, p3}, LX/2sY;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 716727
    :cond_1a
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 716728
    if-eqz v0, :cond_1b

    .line 716729
    const-string v1, "sports_data_fallback_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716730
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 716731
    :cond_1b
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 716732
    if-eqz v0, :cond_1c

    .line 716733
    const-string v1, "sports_subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716734
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716735
    :cond_1c
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 716736
    if-eqz v0, :cond_1d

    .line 716737
    const-string v1, "status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716738
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716739
    :cond_1d
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 716740
    if-eqz v0, :cond_1e

    .line 716741
    const-string v1, "status_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716742
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716743
    :cond_1e
    const/16 v0, 0x20

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 716744
    cmp-long v2, v0, v4

    if-eqz v2, :cond_1f

    .line 716745
    const-string v2, "updated_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716746
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 716747
    :cond_1f
    const/16 v0, 0x21

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 716748
    if-eqz v0, :cond_20

    .line 716749
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716750
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716751
    :cond_20
    const/16 v0, 0x22

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 716752
    if-eqz v0, :cond_21

    .line 716753
    const-string v1, "viewer_can_vote_fan_favorite"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716754
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 716755
    :cond_21
    const/16 v0, 0x23

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 716756
    if-eqz v0, :cond_22

    .line 716757
    const-string v1, "winning_team"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716758
    invoke-static {p0, v0, p2, p3}, LX/2bc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 716759
    :cond_22
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 716760
    return-void
.end method
