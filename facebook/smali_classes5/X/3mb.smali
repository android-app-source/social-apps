.class public LX/3mb;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DCx;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinVisitCommunityComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 637014
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/3mb;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinVisitCommunityComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 637011
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 637012
    iput-object p1, p0, LX/3mb;->b:LX/0Ot;

    .line 637013
    return-void
.end method

.method public static a(LX/0QB;)LX/3mb;
    .locals 4

    .prologue
    .line 637000
    const-class v1, LX/3mb;

    monitor-enter v1

    .line 637001
    :try_start_0
    sget-object v0, LX/3mb;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 637002
    sput-object v2, LX/3mb;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 637003
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 637004
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 637005
    new-instance v3, LX/3mb;

    const/16 p0, 0x1ed8

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/3mb;-><init>(LX/0Ot;)V

    .line 637006
    move-object v0, v3

    .line 637007
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 637008
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3mb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 637009
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 637010
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 636968
    check-cast p2, LX/DCy;

    .line 636969
    iget-object v0, p0, LX/3mb;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinVisitCommunityComponentSpec;

    iget v2, p2, LX/DCy;->a:I

    iget-object v3, p2, LX/DCy;->b:Ljava/lang/String;

    iget-object v4, p2, LX/DCy;->c:Ljava/lang/String;

    iget-object v5, p2, LX/DCy;->d:Ljava/lang/String;

    move-object v1, p1

    const/16 p1, 0x8

    const/4 p0, 0x1

    const/4 v9, 0x0

    .line 636970
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, v9}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    if-ne v2, p0, :cond_0

    const v6, 0x7f0203e1

    :goto_0
    invoke-interface {v7, v6}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v6

    const/16 p2, 0x8

    const/4 v8, 0x2

    .line 636971
    if-nez v5, :cond_2

    .line 636972
    const/4 v7, 0x0

    .line 636973
    :goto_1
    move-object v7, v7

    .line 636974
    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    const v8, 0x7f0b0933

    invoke-interface {v6, v8}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v6

    const v8, 0x7f0b0936

    invoke-interface {v6, v8}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v6

    const/4 v8, 0x7

    invoke-interface {v6, v8, p1}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v6

    const/4 v8, 0x4

    invoke-interface {v6, v8, p1}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v6

    const/4 v8, 0x2

    invoke-interface {v6, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v8

    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, v9}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p0}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v6

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-interface {v6, v9}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v6

    const/4 v2, 0x0

    const/4 v0, 0x3

    const/4 p2, 0x1

    .line 636975
    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v9

    const p0, 0x7f081c2b

    new-array p1, p2, [Ljava/lang/Object;

    aput-object v4, p1, v2

    invoke-virtual {v1, p0, p1}, LX/1De;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v9, p0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v9

    invoke-virtual {v9, v2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v9

    const p0, 0x7f0b0050

    invoke-virtual {v9, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v9

    const p0, 0x7f0a0428

    invoke-virtual {v9, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v9

    invoke-virtual {v9, p2}, LX/1ne;->t(I)LX/1ne;

    move-result-object v9

    const/4 p0, 0x2

    invoke-virtual {v9, p0}, LX/1ne;->j(I)LX/1ne;

    move-result-object v9

    sget-object p0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v9, p0}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v9

    invoke-virtual {v9}, LX/1X5;->c()LX/1Di;

    move-result-object v9

    const p0, 0x7f020a8d

    invoke-interface {v9, p0}, LX/1Di;->x(I)LX/1Di;

    move-result-object v9

    invoke-interface {v9, v0, v0}, LX/1Di;->d(II)LX/1Di;

    move-result-object v9

    invoke-interface {v9, p2, v0}, LX/1Di;->h(II)LX/1Di;

    move-result-object v9

    move-object v9, v9

    .line 636976
    invoke-interface {v6, v9}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v9

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v6, 0x0

    :goto_2
    invoke-interface {v9, v6}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v6

    invoke-interface {v8, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-interface {v7, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v0, v6

    .line 636977
    return-object v0

    :cond_0
    const v6, 0x7f020a3c

    goto/16 :goto_0

    .line 636978
    :cond_1
    const v6, -0x6515452d

    const/4 p0, 0x0

    invoke-static {v1, v6, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v6, v6

    .line 636979
    goto :goto_2

    :cond_2
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, p2, v8}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, p2, v8}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    const v8, 0x7f020a3c

    invoke-interface {v7, v8}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v8

    iget-object v7, v0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinVisitCommunityComponentSpec;->b:LX/1nu;

    invoke-virtual {v7, v1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v7

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    invoke-virtual {v7, p2}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v7

    sget-object p2, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinVisitCommunityComponentSpec;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v7, p2}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const p2, 0x7f0b0933

    invoke-interface {v7, p2}, LX/1Di;->i(I)LX/1Di;

    move-result-object p2

    if-nez v2, :cond_3

    const v7, 0x7f0b0935

    :goto_3
    invoke-interface {p2, v7}, LX/1Di;->q(I)LX/1Di;

    move-result-object v7

    invoke-interface {v8, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-static {v1}, LX/3mZ;->d(LX/1De;)LX/1dQ;

    move-result-object v8

    invoke-interface {v7, v8}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v7

    goto/16 :goto_1

    :cond_3
    const v7, 0x7f0b1242

    goto :goto_3
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 636988
    invoke-static {}, LX/1dS;->b()V

    .line 636989
    iget v0, p1, LX/1dQ;->b:I

    .line 636990
    packed-switch v0, :pswitch_data_0

    .line 636991
    :goto_0
    return-object v2

    .line 636992
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 636993
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 636994
    check-cast v1, LX/DCy;

    .line 636995
    iget-object v3, p0, LX/3mb;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinVisitCommunityComponentSpec;

    iget-object p1, v1, LX/DCy;->b:Ljava/lang/String;

    iget-object p2, v1, LX/DCy;->c:Ljava/lang/String;

    .line 636996
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "gysj_visit_community_click"

    invoke-direct {p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 636997
    iget-object v1, v3, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinVisitCommunityComponentSpec;->c:LX/0Zb;

    invoke-interface {v1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 636998
    iget-object p0, v3, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinVisitCommunityComponentSpec;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/3mE;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1, p1, p2}, LX/3mE;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 636999
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x6515452d
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/DCx;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 636980
    new-instance v1, LX/DCy;

    invoke-direct {v1, p0}, LX/DCy;-><init>(LX/3mb;)V

    .line 636981
    sget-object v2, LX/3mb;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/DCx;

    .line 636982
    if-nez v2, :cond_0

    .line 636983
    new-instance v2, LX/DCx;

    invoke-direct {v2}, LX/DCx;-><init>()V

    .line 636984
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/DCx;->a$redex0(LX/DCx;LX/1De;IILX/DCy;)V

    .line 636985
    move-object v1, v2

    .line 636986
    move-object v0, v1

    .line 636987
    return-object v0
.end method
