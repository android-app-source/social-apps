.class public LX/4oS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final a:Landroid/view/View;

.field public final b:LX/4oN;

.field private final c:LX/4oO;

.field public d:LX/4oL;

.field public e:Landroid/graphics/ColorFilter;

.field public f:I

.field public g:I

.field public h:I


# direct methods
.method public constructor <init>(Landroid/view/View;LX/4oN;)V
    .locals 1

    .prologue
    .line 809721
    sget-object v0, LX/4oO;->NONE:LX/4oO;

    invoke-direct {p0, p1, p2, v0}, LX/4oS;-><init>(Landroid/view/View;LX/4oN;LX/4oO;)V

    .line 809722
    return-void
.end method

.method private constructor <init>(Landroid/view/View;LX/4oN;LX/4oO;)V
    .locals 1

    .prologue
    .line 809714
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 809715
    const/16 v0, 0xff

    iput v0, p0, LX/4oS;->g:I

    .line 809716
    const/4 v0, 0x0

    iput v0, p0, LX/4oS;->h:I

    .line 809717
    iput-object p1, p0, LX/4oS;->a:Landroid/view/View;

    .line 809718
    iput-object p2, p0, LX/4oS;->b:LX/4oN;

    .line 809719
    iput-object p3, p0, LX/4oS;->c:LX/4oO;

    .line 809720
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 809723
    new-instance v0, LX/4oP;

    iget-object v1, p0, LX/4oS;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    iget-object v2, p0, LX/4oS;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    iget-object v3, p0, LX/4oS;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    iget-object v4, p0, LX/4oS;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    iget-object v5, p0, LX/4oS;->a:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getPaddingRight()I

    move-result v5

    iget-object v6, p0, LX/4oS;->a:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getPaddingBottom()I

    move-result v6

    iget-object v7, p0, LX/4oS;->c:LX/4oO;

    invoke-direct/range {v0 .. v7}, LX/4oP;-><init>(IIIIIILX/4oO;)V

    .line 809724
    iget-object v1, p0, LX/4oS;->d:LX/4oL;

    if-eqz v1, :cond_0

    .line 809725
    iget-object v1, p0, LX/4oS;->d:LX/4oL;

    invoke-virtual {v1}, LX/4oL;->a()V

    .line 809726
    :cond_0
    new-instance v1, LX/4oL;

    iget-object v2, p0, LX/4oS;->b:LX/4oN;

    invoke-direct {v1, v2, v0}, LX/4oL;-><init>(LX/4oN;LX/4oP;)V

    iput-object v1, p0, LX/4oS;->d:LX/4oL;

    .line 809727
    iget-object v0, p0, LX/4oS;->e:Landroid/graphics/ColorFilter;

    if-eqz v0, :cond_1

    .line 809728
    iget-object v0, p0, LX/4oS;->d:LX/4oL;

    iget-object v1, p0, LX/4oS;->e:Landroid/graphics/ColorFilter;

    invoke-virtual {v0, v1}, LX/4oL;->a(Landroid/graphics/ColorFilter;)V

    .line 809729
    :cond_1
    iget v0, p0, LX/4oS;->f:I

    if-eqz v0, :cond_2

    .line 809730
    iget-object v0, p0, LX/4oS;->d:LX/4oL;

    iget v1, p0, LX/4oS;->f:I

    invoke-virtual {v0, v1}, LX/4oL;->a(I)V

    .line 809731
    :cond_2
    iget v0, p0, LX/4oS;->g:I

    const/16 v1, 0xff

    if-eq v0, v1, :cond_3

    .line 809732
    iget-object v0, p0, LX/4oS;->d:LX/4oL;

    iget v1, p0, LX/4oS;->g:I

    invoke-virtual {v0, v1}, LX/4oL;->b(I)V

    .line 809733
    :cond_3
    iget v0, p0, LX/4oS;->h:I

    if-eqz v0, :cond_4

    .line 809734
    iget-object v0, p0, LX/4oS;->d:LX/4oL;

    iget v1, p0, LX/4oS;->h:I

    invoke-virtual {v0, v1}, LX/4oL;->c(I)V

    .line 809735
    :cond_4
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 809711
    iget-object v0, p0, LX/4oS;->d:LX/4oL;

    if-eqz v0, :cond_0

    .line 809712
    iget-object v0, p0, LX/4oS;->d:LX/4oL;

    invoke-virtual {v0}, LX/4oL;->a()V

    .line 809713
    :cond_0
    return-void
.end method
