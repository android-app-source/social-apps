.class public final LX/43c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/32Z;


# instance fields
.field public final synthetic a:LX/1Hv;

.field private b:Z


# direct methods
.method public constructor <init>(LX/1Hv;)V
    .locals 0

    .prologue
    .line 669168
    iput-object p1, p0, LX/43c;->a:LX/1Hv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/io/File;)V
    .locals 1

    .prologue
    .line 669169
    iget-boolean v0, p0, LX/43c;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/43c;->a:LX/1Hv;

    iget-object v0, v0, LX/1Hv;->e:Ljava/io/File;

    invoke-virtual {p1, v0}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 669170
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/43c;->b:Z

    .line 669171
    :cond_0
    return-void
.end method

.method public final b(Ljava/io/File;)V
    .locals 12

    .prologue
    .line 669172
    iget-boolean v0, p0, LX/43c;->b:Z

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 669173
    iget-object v3, p0, LX/43c;->a:LX/1Hv;

    invoke-static {v3, p1}, LX/1Hv;->b(LX/1Hv;Ljava/io/File;)LX/1gF;

    move-result-object v3

    .line 669174
    if-nez v3, :cond_2

    .line 669175
    :goto_0
    move v0, v1

    .line 669176
    if-nez v0, :cond_1

    .line 669177
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 669178
    :cond_1
    return-void

    .line 669179
    :cond_2
    iget-object v4, v3, LX/1gF;->a:LX/1gG;

    sget-object v5, LX/1gG;->TEMP:LX/1gG;

    if-ne v4, v5, :cond_3

    .line 669180
    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    iget-object v8, p0, LX/43c;->a:LX/1Hv;

    iget-object v8, v8, LX/1Hv;->g:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    sget-wide v10, LX/1Hv;->a:J

    sub-long/2addr v8, v10

    cmp-long v6, v6, v8

    if-lez v6, :cond_5

    const/4 v6, 0x1

    :goto_1
    move v1, v6

    .line 669181
    goto :goto_0

    .line 669182
    :cond_3
    iget-object v3, v3, LX/1gF;->a:LX/1gG;

    sget-object v4, LX/1gG;->CONTENT:LX/1gG;

    if-ne v3, v4, :cond_4

    move v1, v2

    :cond_4
    invoke-static {v1}, LX/03g;->b(Z)V

    move v1, v2

    .line 669183
    goto :goto_0

    :cond_5
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public final c(Ljava/io/File;)V
    .locals 1

    .prologue
    .line 669184
    iget-object v0, p0, LX/43c;->a:LX/1Hv;

    iget-object v0, v0, LX/1Hv;->c:Ljava/io/File;

    invoke-virtual {v0, p1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 669185
    iget-boolean v0, p0, LX/43c;->b:Z

    if-nez v0, :cond_0

    .line 669186
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 669187
    :cond_0
    iget-boolean v0, p0, LX/43c;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/43c;->a:LX/1Hv;

    iget-object v0, v0, LX/1Hv;->e:Ljava/io/File;

    invoke-virtual {p1, v0}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 669188
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/43c;->b:Z

    .line 669189
    :cond_1
    return-void
.end method
