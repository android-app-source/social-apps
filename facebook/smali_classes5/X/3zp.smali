.class public LX/3zp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static a:Ljava/lang/reflect/Field;

.field private static volatile b:LX/3zp;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 662668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 662669
    :try_start_0
    const-string v0, "android.view.View"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "mListenerInfo"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    sput-object v0, LX/3zp;->a:Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1

    .line 662670
    :goto_0
    sget-object v0, LX/3zp;->a:Ljava/lang/reflect/Field;

    if-eqz v0, :cond_0

    .line 662671
    sget-object v0, LX/3zp;->a:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 662672
    :cond_0
    return-void

    .line 662673
    :catch_0
    move-exception v0

    .line 662674
    invoke-static {v0}, LX/3zp;->a(Ljava/lang/Exception;)V

    goto :goto_0

    .line 662675
    :catch_1
    move-exception v0

    .line 662676
    invoke-static {v0}, LX/3zp;->a(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/3zp;
    .locals 3

    .prologue
    .line 662652
    sget-object v0, LX/3zp;->b:LX/3zp;

    if-nez v0, :cond_1

    .line 662653
    const-class v1, LX/3zp;

    monitor-enter v1

    .line 662654
    :try_start_0
    sget-object v0, LX/3zp;->b:LX/3zp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 662655
    if-eqz v2, :cond_0

    .line 662656
    :try_start_1
    new-instance v0, LX/3zp;

    invoke-direct {v0}, LX/3zp;-><init>()V

    .line 662657
    move-object v0, v0

    .line 662658
    sput-object v0, LX/3zp;->b:LX/3zp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 662659
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 662660
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 662661
    :cond_1
    sget-object v0, LX/3zp;->b:LX/3zp;

    return-object v0

    .line 662662
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 662663
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 662664
    const/4 v0, 0x0

    .line 662665
    sput-boolean v0, LX/3zo;->f:Z

    .line 662666
    const-string v0, "ListenerGetter"

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 662667
    return-void
.end method
