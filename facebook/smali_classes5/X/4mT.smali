.class public final LX/4mT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field public final synthetic a:LX/4mU;


# direct methods
.method public constructor <init>(LX/4mU;)V
    .locals 0

    .prologue
    .line 805898
    iput-object p1, p0, LX/4mT;->a:LX/4mU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 805887
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1    # Landroid/animation/Animator;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 805888
    iget-object v0, p0, LX/4mT;->a:LX/4mU;

    .line 805889
    iget v1, v0, LX/4mU;->e:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, LX/4mU;->e:I

    .line 805890
    iget-object v0, p0, LX/4mT;->a:LX/4mU;

    iget v0, v0, LX/4mU;->e:I

    if-nez v0, :cond_0

    .line 805891
    iget-object v0, p0, LX/4mT;->a:LX/4mU;

    const/4 v1, 0x0

    .line 805892
    iput-boolean v1, v0, LX/4mU;->d:Z

    .line 805893
    iget-object v0, p0, LX/4mT;->a:LX/4mU;

    iget-object v0, v0, LX/4mU;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 805894
    iget-object v0, p0, LX/4mT;->a:LX/4mU;

    iget-object v0, v0, LX/4mU;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4mR;

    .line 805895
    if-eqz v0, :cond_0

    .line 805896
    invoke-interface {v0}, LX/4mR;->b()V

    .line 805897
    :cond_0
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 805886
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 1
    .param p1    # Landroid/animation/Animator;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 805878
    iget-object v0, p0, LX/4mT;->a:LX/4mU;

    iget v0, v0, LX/4mU;->e:I

    if-nez v0, :cond_0

    .line 805879
    iget-object v0, p0, LX/4mT;->a:LX/4mU;

    iget-object v0, v0, LX/4mU;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 805880
    iget-object v0, p0, LX/4mT;->a:LX/4mU;

    iget-object v0, v0, LX/4mU;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4mR;

    .line 805881
    if-eqz v0, :cond_0

    .line 805882
    invoke-interface {v0}, LX/4mR;->a()V

    .line 805883
    :cond_0
    iget-object v0, p0, LX/4mT;->a:LX/4mU;

    .line 805884
    iget p0, v0, LX/4mU;->e:I

    add-int/lit8 p0, p0, 0x1

    iput p0, v0, LX/4mU;->e:I

    .line 805885
    return-void
.end method
