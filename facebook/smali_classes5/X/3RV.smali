.class public LX/3RV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3RV;


# instance fields
.field public a:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 580494
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/3RV;
    .locals 4

    .prologue
    .line 580495
    sget-object v0, LX/3RV;->b:LX/3RV;

    if-nez v0, :cond_1

    .line 580496
    const-class v1, LX/3RV;

    monitor-enter v1

    .line 580497
    :try_start_0
    sget-object v0, LX/3RV;->b:LX/3RV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 580498
    if-eqz v2, :cond_0

    .line 580499
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 580500
    new-instance p0, LX/3RV;

    invoke-direct {p0}, LX/3RV;-><init>()V

    .line 580501
    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    .line 580502
    iput-object v3, p0, LX/3RV;->a:LX/0ad;

    .line 580503
    move-object v0, p0

    .line 580504
    sput-object v0, LX/3RV;->b:LX/3RV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 580505
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 580506
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 580507
    :cond_1
    sget-object v0, LX/3RV;->b:LX/3RV;

    return-object v0

    .line 580508
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 580509
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
