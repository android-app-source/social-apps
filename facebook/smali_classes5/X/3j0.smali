.class public final LX/3j0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 631569
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 2

    .prologue
    .line 631558
    if-nez p0, :cond_0

    .line 631559
    const/4 v0, 0x0

    .line 631560
    :goto_0
    return-object v0

    .line 631561
    :cond_0
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    .line 631562
    invoke-interface {p0}, LX/1Fb;->a()I

    move-result v1

    .line 631563
    iput v1, v0, LX/2dc;->c:I

    .line 631564
    invoke-interface {p0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    .line 631565
    iput-object v1, v0, LX/2dc;->h:Ljava/lang/String;

    .line 631566
    invoke-interface {p0}, LX/1Fb;->c()I

    move-result v1

    .line 631567
    iput v1, v0, LX/2dc;->i:I

    .line 631568
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateBundleAttributeFragmentModel;)Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;
    .locals 15

    .prologue
    .line 631184
    if-nez p0, :cond_0

    .line 631185
    const/4 v0, 0x0

    .line 631186
    :goto_0
    return-object v0

    .line 631187
    :cond_0
    new-instance v0, LX/4XN;

    invoke-direct {v0}, LX/4XN;-><init>()V

    .line 631188
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateBundleAttributeFragmentModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    .line 631189
    iput-object v1, v0, LX/4XN;->i:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 631190
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateBundleAttributeFragmentModel;->b()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, LX/3j0;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 631191
    iput-object v1, v0, LX/4XN;->d:Lcom/facebook/graphql/model/GraphQLImage;

    .line 631192
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateBundleAttributeFragmentModel;->c()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateNodeFragmentModel$MutationActionNodeValueModel;

    move-result-object v1

    .line 631193
    if-nez v1, :cond_1

    .line 631194
    const/4 v2, 0x0

    .line 631195
    :goto_1
    move-object v1, v2

    .line 631196
    iput-object v1, v0, LX/4XN;->e:Lcom/facebook/graphql/model/GraphQLNode;

    .line 631197
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateBundleAttributeFragmentModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 631198
    iput-object v1, v0, LX/4XN;->f:Ljava/lang/String;

    .line 631199
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateBundleAttributeFragmentModel;->e()LX/3Ab;

    move-result-object v1

    .line 631200
    if-nez v1, :cond_2

    .line 631201
    const/4 v2, 0x0

    .line 631202
    :goto_2
    move-object v1, v2

    .line 631203
    iput-object v1, v0, LX/4XN;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 631204
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateBundleAttributeFragmentModel;->bq_()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;

    move-result-object v1

    .line 631205
    if-nez v1, :cond_b

    .line 631206
    const/4 v2, 0x0

    .line 631207
    :goto_3
    move-object v1, v2

    .line 631208
    iput-object v1, v0, LX/4XN;->h:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 631209
    invoke-virtual {v0}, LX/4XN;->a()Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;

    move-result-object v0

    goto :goto_0

    .line 631210
    :cond_1
    new-instance v2, LX/4XR;

    invoke-direct {v2}, LX/4XR;-><init>()V

    .line 631211
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateNodeFragmentModel$MutationActionNodeValueModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    .line 631212
    iput-object v3, v2, LX/4XR;->qc:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 631213
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateNodeFragmentModel$MutationActionNodeValueModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 631214
    iput-object v3, v2, LX/4XR;->fO:Ljava/lang/String;

    .line 631215
    invoke-virtual {v2}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    goto :goto_1

    .line 631216
    :cond_2
    new-instance v4, LX/173;

    invoke-direct {v4}, LX/173;-><init>()V

    .line 631217
    invoke-interface {v1}, LX/3Ab;->b()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 631218
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 631219
    const/4 v2, 0x0

    move v3, v2

    :goto_4
    invoke-interface {v1}, LX/3Ab;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_3

    .line 631220
    invoke-interface {v1}, LX/3Ab;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/5Ph;

    .line 631221
    if-nez v2, :cond_5

    .line 631222
    const/4 v6, 0x0

    .line 631223
    :goto_5
    move-object v2, v6

    .line 631224
    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 631225
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_4

    .line 631226
    :cond_3
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 631227
    iput-object v2, v4, LX/173;->e:LX/0Px;

    .line 631228
    :cond_4
    invoke-interface {v1}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v2

    .line 631229
    iput-object v2, v4, LX/173;->f:Ljava/lang/String;

    .line 631230
    invoke-virtual {v4}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    goto :goto_2

    .line 631231
    :cond_5
    new-instance v6, LX/4W6;

    invoke-direct {v6}, LX/4W6;-><init>()V

    .line 631232
    invoke-interface {v2}, LX/5Ph;->a()LX/1yA;

    move-result-object v7

    .line 631233
    if-nez v7, :cond_6

    .line 631234
    const/4 v8, 0x0

    .line 631235
    :goto_6
    move-object v7, v8

    .line 631236
    iput-object v7, v6, LX/4W6;->b:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 631237
    invoke-interface {v2}, LX/5Ph;->b()I

    move-result v7

    .line 631238
    iput v7, v6, LX/4W6;->c:I

    .line 631239
    invoke-interface {v2}, LX/5Ph;->c()I

    move-result v7

    .line 631240
    iput v7, v6, LX/4W6;->d:I

    .line 631241
    invoke-virtual {v6}, LX/4W6;->a()Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v6

    goto :goto_5

    .line 631242
    :cond_6
    new-instance v10, LX/170;

    invoke-direct {v10}, LX/170;-><init>()V

    .line 631243
    invoke-interface {v7}, LX/1yA;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    .line 631244
    iput-object v8, v10, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 631245
    invoke-interface {v7}, LX/1yA;->e()Ljava/lang/String;

    move-result-object v8

    .line 631246
    iput-object v8, v10, LX/170;->o:Ljava/lang/String;

    .line 631247
    invoke-interface {v7}, LX/1yA;->v_()Ljava/lang/String;

    move-result-object v8

    .line 631248
    iput-object v8, v10, LX/170;->A:Ljava/lang/String;

    .line 631249
    invoke-interface {v7}, LX/1yA;->n()LX/1yP;

    move-result-object v8

    .line 631250
    if-nez v8, :cond_9

    .line 631251
    const/4 v9, 0x0

    .line 631252
    :goto_7
    move-object v8, v9

    .line 631253
    iput-object v8, v10, LX/170;->C:Lcom/facebook/graphql/model/GraphQLPage;

    .line 631254
    invoke-interface {v7}, LX/1yA;->o()LX/0Px;

    move-result-object v8

    if-eqz v8, :cond_8

    .line 631255
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 631256
    const/4 v8, 0x0

    move v9, v8

    :goto_8
    invoke-interface {v7}, LX/1yA;->o()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v8

    if-ge v9, v8, :cond_7

    .line 631257
    invoke-interface {v7}, LX/1yA;->o()LX/0Px;

    move-result-object v8

    invoke-virtual {v8, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;

    .line 631258
    if-nez v8, :cond_a

    .line 631259
    const/4 v12, 0x0

    .line 631260
    :goto_9
    move-object v8, v12

    .line 631261
    invoke-virtual {v11, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 631262
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_8

    .line 631263
    :cond_7
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v8

    .line 631264
    iput-object v8, v10, LX/170;->N:LX/0Px;

    .line 631265
    :cond_8
    invoke-interface {v7}, LX/1yA;->w_()Ljava/lang/String;

    move-result-object v8

    .line 631266
    iput-object v8, v10, LX/170;->X:Ljava/lang/String;

    .line 631267
    invoke-interface {v7}, LX/1yA;->j()Ljava/lang/String;

    move-result-object v8

    .line 631268
    iput-object v8, v10, LX/170;->Y:Ljava/lang/String;

    .line 631269
    invoke-virtual {v10}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v8

    goto :goto_6

    .line 631270
    :cond_9
    new-instance v9, LX/4XY;

    invoke-direct {v9}, LX/4XY;-><init>()V

    .line 631271
    invoke-interface {v8}, LX/1yP;->e()Ljava/lang/String;

    move-result-object v11

    .line 631272
    iput-object v11, v9, LX/4XY;->ag:Ljava/lang/String;

    .line 631273
    invoke-virtual {v9}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v9

    goto :goto_7

    .line 631274
    :cond_a
    new-instance v12, LX/4YY;

    invoke-direct {v12}, LX/4YY;-><init>()V

    .line 631275
    invoke-virtual {v8}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;->a()Ljava/lang/String;

    move-result-object v13

    .line 631276
    iput-object v13, v12, LX/4YY;->d:Ljava/lang/String;

    .line 631277
    invoke-virtual {v12}, LX/4YY;->a()Lcom/facebook/graphql/model/GraphQLRedirectionInfo;

    move-result-object v12

    goto :goto_9

    .line 631278
    :cond_b
    new-instance v4, LX/2oI;

    invoke-direct {v4}, LX/2oI;-><init>()V

    .line 631279
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->k()I

    move-result v2

    .line 631280
    iput v2, v4, LX/2oI;->e:I

    .line 631281
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->l()J

    move-result-wide v2

    .line 631282
    iput-wide v2, v4, LX/2oI;->i:J

    .line 631283
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->m()I

    move-result v2

    .line 631284
    iput v2, v4, LX/2oI;->j:I

    .line 631285
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->n()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v2

    .line 631286
    iput-object v2, v4, LX/2oI;->k:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 631287
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->o()Z

    move-result v2

    .line 631288
    iput-boolean v2, v4, LX/2oI;->m:Z

    .line 631289
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->p()Z

    move-result v2

    .line 631290
    iput-boolean v2, v4, LX/2oI;->n:Z

    .line 631291
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->q()Z

    move-result v2

    .line 631292
    iput-boolean v2, v4, LX/2oI;->o:Z

    .line 631293
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->r()Ljava/lang/String;

    move-result-object v2

    .line 631294
    iput-object v2, v4, LX/2oI;->p:Ljava/lang/String;

    .line 631295
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->s()J

    move-result-wide v2

    .line 631296
    iput-wide v2, v4, LX/2oI;->t:J

    .line 631297
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->t()Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;

    move-result-object v2

    const/4 v8, 0x0

    .line 631298
    if-nez v2, :cond_e

    .line 631299
    const/4 v6, 0x0

    .line 631300
    :goto_a
    move-object v2, v6

    .line 631301
    iput-object v2, v4, LX/2oI;->u:Lcom/facebook/graphql/model/GraphQLStory;

    .line 631302
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->u()Z

    move-result v2

    .line 631303
    iput-boolean v2, v4, LX/2oI;->x:Z

    .line 631304
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->v()D

    move-result-wide v2

    .line 631305
    iput-wide v2, v4, LX/2oI;->B:D

    .line 631306
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->w()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v2

    .line 631307
    if-nez v2, :cond_17

    .line 631308
    const/4 v6, 0x0

    .line 631309
    :goto_b
    move-object v2, v6

    .line 631310
    iput-object v2, v4, LX/2oI;->D:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    .line 631311
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->x()Z

    move-result v2

    .line 631312
    iput-boolean v2, v4, LX/2oI;->E:Z

    .line 631313
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->y()Z

    move-result v2

    .line 631314
    iput-boolean v2, v4, LX/2oI;->F:Z

    .line 631315
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->z()I

    move-result v2

    .line 631316
    iput v2, v4, LX/2oI;->G:I

    .line 631317
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->A()I

    move-result v2

    .line 631318
    iput v2, v4, LX/2oI;->H:I

    .line 631319
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->B()I

    move-result v2

    .line 631320
    iput v2, v4, LX/2oI;->M:I

    .line 631321
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->d()Ljava/lang/String;

    move-result-object v2

    .line 631322
    iput-object v2, v4, LX/2oI;->N:Ljava/lang/String;

    .line 631323
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->e()LX/1Fb;

    move-result-object v2

    invoke-static {v2}, LX/3j0;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 631324
    iput-object v2, v4, LX/2oI;->O:Lcom/facebook/graphql/model/GraphQLImage;

    .line 631325
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->aj_()LX/1Fb;

    move-result-object v2

    invoke-static {v2}, LX/3j0;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 631326
    iput-object v2, v4, LX/2oI;->P:Lcom/facebook/graphql/model/GraphQLImage;

    .line 631327
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->ai_()LX/1Fb;

    move-result-object v2

    invoke-static {v2}, LX/3j0;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 631328
    iput-object v2, v4, LX/2oI;->T:Lcom/facebook/graphql/model/GraphQLImage;

    .line 631329
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->j()LX/1Fb;

    move-result-object v2

    invoke-static {v2}, LX/3j0;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 631330
    iput-object v2, v4, LX/2oI;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 631331
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->C()LX/1Fb;

    move-result-object v2

    invoke-static {v2}, LX/3j0;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 631332
    iput-object v2, v4, LX/2oI;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 631333
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->D()I

    move-result v2

    .line 631334
    iput v2, v4, LX/2oI;->aa:I

    .line 631335
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->E()I

    move-result v2

    .line 631336
    iput v2, v4, LX/2oI;->ab:I

    .line 631337
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->F()I

    move-result v2

    .line 631338
    iput v2, v4, LX/2oI;->ac:I

    .line 631339
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->G()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_d

    .line 631340
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 631341
    const/4 v2, 0x0

    move v3, v2

    :goto_c
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->G()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_c

    .line 631342
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->G()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;

    .line 631343
    if-nez v2, :cond_1b

    .line 631344
    const/4 v6, 0x0

    .line 631345
    :goto_d
    move-object v2, v6

    .line 631346
    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 631347
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_c

    .line 631348
    :cond_c
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 631349
    iput-object v2, v4, LX/2oI;->ae:LX/0Px;

    .line 631350
    :cond_d
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->H()Z

    move-result v2

    .line 631351
    iput-boolean v2, v4, LX/2oI;->af:Z

    .line 631352
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->I()Z

    move-result v2

    .line 631353
    iput-boolean v2, v4, LX/2oI;->ag:Z

    .line 631354
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->J()Z

    move-result v2

    .line 631355
    iput-boolean v2, v4, LX/2oI;->ah:Z

    .line 631356
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->K()Z

    move-result v2

    .line 631357
    iput-boolean v2, v4, LX/2oI;->aj:Z

    .line 631358
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->L()Z

    move-result v2

    .line 631359
    iput-boolean v2, v4, LX/2oI;->ak:Z

    .line 631360
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->M()Z

    move-result v2

    .line 631361
    iput-boolean v2, v4, LX/2oI;->al:Z

    .line 631362
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->N()Z

    move-result v2

    .line 631363
    iput-boolean v2, v4, LX/2oI;->am:Z

    .line 631364
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->O()Z

    move-result v2

    .line 631365
    iput-boolean v2, v4, LX/2oI;->an:Z

    .line 631366
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->P()Z

    move-result v2

    .line 631367
    iput-boolean v2, v4, LX/2oI;->ap:Z

    .line 631368
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->Q()Z

    move-result v2

    .line 631369
    iput-boolean v2, v4, LX/2oI;->aq:Z

    .line 631370
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->R()Z

    move-result v2

    .line 631371
    iput-boolean v2, v4, LX/2oI;->ar:Z

    .line 631372
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->S()I

    move-result v2

    .line 631373
    iput v2, v4, LX/2oI;->ay:I

    .line 631374
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->T()I

    move-result v2

    .line 631375
    iput v2, v4, LX/2oI;->az:I

    .line 631376
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->U()D

    move-result-wide v2

    .line 631377
    iput-wide v2, v4, LX/2oI;->aL:D

    .line 631378
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->V()Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel;

    move-result-object v2

    .line 631379
    if-nez v2, :cond_1c

    .line 631380
    const/4 v3, 0x0

    .line 631381
    :goto_e
    move-object v2, v3

    .line 631382
    iput-object v2, v4, LX/2oI;->aN:Lcom/facebook/graphql/model/GraphQLActor;

    .line 631383
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->W()I

    move-result v2

    .line 631384
    iput v2, v4, LX/2oI;->aP:I

    .line 631385
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->X()Ljava/lang/String;

    move-result-object v2

    .line 631386
    iput-object v2, v4, LX/2oI;->aQ:Ljava/lang/String;

    .line 631387
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->Y()I

    move-result v2

    .line 631388
    iput v2, v4, LX/2oI;->aS:I

    .line 631389
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->Z()Ljava/lang/String;

    move-result-object v2

    .line 631390
    iput-object v2, v4, LX/2oI;->aT:Ljava/lang/String;

    .line 631391
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->aa()Ljava/lang/String;

    move-result-object v2

    .line 631392
    iput-object v2, v4, LX/2oI;->bl:Ljava/lang/String;

    .line 631393
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->ab()LX/174;

    move-result-object v2

    invoke-static {v2}, LX/3j0;->a(LX/174;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 631394
    iput-object v2, v4, LX/2oI;->bm:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 631395
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->ac()Z

    move-result v2

    .line 631396
    iput-boolean v2, v4, LX/2oI;->bv:Z

    .line 631397
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->ad()D

    move-result-wide v2

    .line 631398
    iput-wide v2, v4, LX/2oI;->bx:D

    .line 631399
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->ae()D

    move-result-wide v2

    .line 631400
    iput-wide v2, v4, LX/2oI;->by:D

    .line 631401
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->af()Ljava/lang/String;

    move-result-object v2

    .line 631402
    iput-object v2, v4, LX/2oI;->bz:Ljava/lang/String;

    .line 631403
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->ag()Ljava/lang/String;

    move-result-object v2

    .line 631404
    iput-object v2, v4, LX/2oI;->bA:Ljava/lang/String;

    .line 631405
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->ah()I

    move-result v2

    .line 631406
    iput v2, v4, LX/2oI;->bC:I

    .line 631407
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->ai()Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    move-result-object v2

    invoke-static {v2}, LX/3j0;->a(Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;)Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v2

    .line 631408
    iput-object v2, v4, LX/2oI;->bG:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 631409
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->aj()Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    move-result-object v2

    invoke-static {v2}, LX/3j0;->a(Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;)Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v2

    .line 631410
    iput-object v2, v4, LX/2oI;->bH:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 631411
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->ak()Z

    move-result v2

    .line 631412
    iput-boolean v2, v4, LX/2oI;->bI:Z

    .line 631413
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->al()LX/0Px;

    move-result-object v2

    .line 631414
    iput-object v2, v4, LX/2oI;->bP:LX/0Px;

    .line 631415
    invoke-virtual {v1}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateVideoFragmentModel$VideoValueModel;->am()I

    move-result v2

    .line 631416
    iput v2, v4, LX/2oI;->cd:I

    .line 631417
    invoke-virtual {v4}, LX/2oI;->a()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    goto/16 :goto_3

    .line 631418
    :cond_e
    new-instance v9, LX/23u;

    invoke-direct {v9}, LX/23u;-><init>()V

    .line 631419
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->b()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_10

    .line 631420
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    move v7, v8

    .line 631421
    :goto_f
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-ge v7, v6, :cond_f

    .line 631422
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ActorsModel;

    .line 631423
    if-nez v6, :cond_13

    .line 631424
    const/4 v11, 0x0

    .line 631425
    :goto_10
    move-object v6, v11

    .line 631426
    invoke-virtual {v10, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 631427
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_f

    .line 631428
    :cond_f
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 631429
    iput-object v6, v9, LX/23u;->d:LX/0Px;

    .line 631430
    :cond_10
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->c()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_12

    .line 631431
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 631432
    :goto_11
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->c()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-ge v8, v6, :cond_11

    .line 631433
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->c()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$AttachmentsModel;

    .line 631434
    if-nez v6, :cond_14

    .line 631435
    const/4 v10, 0x0

    .line 631436
    :goto_12
    move-object v6, v10

    .line 631437
    invoke-virtual {v7, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 631438
    add-int/lit8 v8, v8, 0x1

    goto :goto_11

    .line 631439
    :cond_11
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 631440
    iput-object v6, v9, LX/23u;->k:LX/0Px;

    .line 631441
    :cond_12
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->d()Ljava/lang/String;

    move-result-object v6

    .line 631442
    iput-object v6, v9, LX/23u;->m:Ljava/lang/String;

    .line 631443
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->e()J

    move-result-wide v6

    .line 631444
    iput-wide v6, v9, LX/23u;->v:J

    .line 631445
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->bm_()Ljava/lang/String;

    move-result-object v6

    .line 631446
    iput-object v6, v9, LX/23u;->N:Ljava/lang/String;

    .line 631447
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->bn_()Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;

    move-result-object v6

    .line 631448
    if-nez v6, :cond_16

    .line 631449
    const/4 v7, 0x0

    .line 631450
    :goto_13
    move-object v6, v7

    .line 631451
    iput-object v6, v9, LX/23u;->at:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 631452
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;->j()Ljava/lang/String;

    move-result-object v6

    .line 631453
    iput-object v6, v9, LX/23u;->aM:Ljava/lang/String;

    .line 631454
    invoke-virtual {v9}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    goto/16 :goto_a

    .line 631455
    :cond_13
    new-instance v11, LX/3dL;

    invoke-direct {v11}, LX/3dL;-><init>()V

    .line 631456
    invoke-virtual {v6}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ActorsModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v12

    .line 631457
    iput-object v12, v11, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 631458
    invoke-virtual {v6}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ActorsModel;->b()Z

    move-result v12

    .line 631459
    iput-boolean v12, v11, LX/3dL;->R:Z

    .line 631460
    invoke-virtual {v6}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ActorsModel;->c()Ljava/lang/String;

    move-result-object v12

    .line 631461
    iput-object v12, v11, LX/3dL;->ag:Ljava/lang/String;

    .line 631462
    invoke-virtual {v11}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v11

    goto :goto_10

    .line 631463
    :cond_14
    new-instance v10, LX/39x;

    invoke-direct {v10}, LX/39x;-><init>()V

    .line 631464
    invoke-virtual {v6}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$AttachmentsModel;->a()Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$AttachmentsModel$MediaModel;

    move-result-object v11

    .line 631465
    if-nez v11, :cond_15

    .line 631466
    const/4 v12, 0x0

    .line 631467
    :goto_14
    move-object v11, v12

    .line 631468
    iput-object v11, v10, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 631469
    invoke-virtual {v10}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v10

    goto :goto_12

    .line 631470
    :cond_15
    new-instance v12, LX/4XB;

    invoke-direct {v12}, LX/4XB;-><init>()V

    .line 631471
    invoke-virtual {v11}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$AttachmentsModel$MediaModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    .line 631472
    iput-object v6, v12, LX/4XB;->bQ:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 631473
    invoke-virtual {v11}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$AttachmentsModel$MediaModel;->c()Ljava/lang/String;

    move-result-object v6

    .line 631474
    iput-object v6, v12, LX/4XB;->T:Ljava/lang/String;

    .line 631475
    invoke-virtual {v12}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v12

    goto :goto_14

    .line 631476
    :cond_16
    new-instance v7, LX/170;

    invoke-direct {v7}, LX/170;-><init>()V

    .line 631477
    invoke-virtual {v6}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    .line 631478
    iput-object v8, v7, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 631479
    invoke-virtual {v6}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;->c()Ljava/lang/String;

    move-result-object v8

    .line 631480
    iput-object v8, v7, LX/170;->o:Ljava/lang/String;

    .line 631481
    invoke-virtual {v7}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v7

    goto :goto_13

    .line 631482
    :cond_17
    new-instance v8, LX/4ZS;

    invoke-direct {v8}, LX/4ZS;-><init>()V

    .line 631483
    invoke-virtual {v2}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;->a()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_19

    .line 631484
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 631485
    const/4 v6, 0x0

    move v7, v6

    :goto_15
    invoke-virtual {v2}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-ge v7, v6, :cond_18

    .line 631486
    invoke-virtual {v2}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel$KeyframesModel;

    .line 631487
    if-nez v6, :cond_1a

    .line 631488
    const/4 v10, 0x0

    .line 631489
    :goto_16
    move-object v6, v10

    .line 631490
    invoke-virtual {v9, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 631491
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_15

    .line 631492
    :cond_18
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 631493
    iput-object v6, v8, LX/4ZS;->b:LX/0Px;

    .line 631494
    :cond_19
    invoke-virtual {v8}, LX/4ZS;->a()Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    move-result-object v6

    goto/16 :goto_b

    .line 631495
    :cond_1a
    new-instance v10, LX/4ZT;

    invoke-direct {v10}, LX/4ZT;-><init>()V

    .line 631496
    invoke-virtual {v6}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel$KeyframesModel;->a()I

    move-result v11

    .line 631497
    iput v11, v10, LX/4ZT;->b:I

    .line 631498
    invoke-virtual {v6}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel$KeyframesModel;->b()J

    move-result-wide v12

    .line 631499
    iput-wide v12, v10, LX/4ZT;->c:J

    .line 631500
    invoke-virtual {v6}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel$KeyframesModel;->c()I

    move-result v11

    .line 631501
    iput v11, v10, LX/4ZT;->d:I

    .line 631502
    invoke-virtual {v10}, LX/4ZT;->a()Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;

    move-result-object v10

    goto :goto_16

    .line 631503
    :cond_1b
    new-instance v6, LX/4X1;

    invoke-direct {v6}, LX/4X1;-><init>()V

    .line 631504
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->a()I

    move-result v7

    .line 631505
    iput v7, v6, LX/4X1;->b:I

    .line 631506
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->b()Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    move-result-object v7

    .line 631507
    iput-object v7, v6, LX/4X1;->c:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    .line 631508
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->c()I

    move-result v7

    .line 631509
    iput v7, v6, LX/4X1;->d:I

    .line 631510
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->d()I

    move-result v7

    .line 631511
    iput v7, v6, LX/4X1;->e:I

    .line 631512
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$InstreamVideoAdBreaksModel;->e()I

    move-result v7

    .line 631513
    iput v7, v6, LX/4X1;->f:I

    .line 631514
    invoke-virtual {v6}, LX/4X1;->a()Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;

    move-result-object v6

    goto/16 :goto_d

    .line 631515
    :cond_1c
    new-instance v3, LX/3dL;

    invoke-direct {v3}, LX/3dL;-><init>()V

    .line 631516
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    .line 631517
    iput-object v5, v3, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 631518
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 631519
    iput-object v5, v3, LX/3dL;->E:Ljava/lang/String;

    .line 631520
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel;->d()Z

    move-result v5

    .line 631521
    iput-boolean v5, v3, LX/3dL;->R:Z

    .line 631522
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel;->e()Ljava/lang/String;

    move-result-object v5

    .line 631523
    iput-object v5, v3, LX/3dL;->ag:Ljava/lang/String;

    .line 631524
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel;->bp_()Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel$SinglePublisherVideoChannelsModel;

    move-result-object v5

    .line 631525
    if-nez v5, :cond_1d

    .line 631526
    const/4 v6, 0x0

    .line 631527
    :goto_17
    move-object v5, v6

    .line 631528
    iput-object v5, v3, LX/3dL;->aA:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    .line 631529
    invoke-virtual {v2}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel;->bo_()LX/174;

    move-result-object v5

    invoke-static {v5}, LX/3j0;->a(LX/174;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    .line 631530
    iput-object v5, v3, LX/3dL;->aS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 631531
    invoke-virtual {v3}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    goto/16 :goto_e

    .line 631532
    :cond_1d
    new-instance v8, LX/4Yk;

    invoke-direct {v8}, LX/4Yk;-><init>()V

    .line 631533
    invoke-virtual {v5}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel$SinglePublisherVideoChannelsModel;->a()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_1f

    .line 631534
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 631535
    const/4 v6, 0x0

    move v7, v6

    :goto_18
    invoke-virtual {v5}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel$SinglePublisherVideoChannelsModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-ge v7, v6, :cond_1e

    .line 631536
    invoke-virtual {v5}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel$SinglePublisherVideoChannelsModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel$SinglePublisherVideoChannelsModel$EdgesModel;

    .line 631537
    if-nez v6, :cond_20

    .line 631538
    const/4 v10, 0x0

    .line 631539
    :goto_19
    move-object v6, v10

    .line 631540
    invoke-virtual {v9, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 631541
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_18

    .line 631542
    :cond_1e
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 631543
    iput-object v6, v8, LX/4Yk;->b:LX/0Px;

    .line 631544
    :cond_1f
    invoke-virtual {v8}, LX/4Yk;->a()Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    move-result-object v6

    goto :goto_17

    .line 631545
    :cond_20
    new-instance v10, LX/4Yl;

    invoke-direct {v10}, LX/4Yl;-><init>()V

    .line 631546
    invoke-virtual {v6}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel$SinglePublisherVideoChannelsModel$EdgesModel;->a()Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel$SinglePublisherVideoChannelsModel$EdgesModel$NodeModel;

    move-result-object v11

    .line 631547
    if-nez v11, :cond_21

    .line 631548
    const/4 v12, 0x0

    .line 631549
    :goto_1a
    move-object v11, v12

    .line 631550
    iput-object v11, v10, LX/4Yl;->b:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    .line 631551
    invoke-virtual {v10}, LX/4Yl;->a()Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsEdge;

    move-result-object v10

    goto :goto_19

    .line 631552
    :cond_21
    new-instance v12, LX/4ZQ;

    invoke-direct {v12}, LX/4ZQ;-><init>()V

    .line 631553
    invoke-virtual {v11}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel$SinglePublisherVideoChannelsModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    .line 631554
    iput-object v6, v12, LX/4ZQ;->t:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 631555
    invoke-virtual {v11}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryVideoAttachmentFragmentModel$OwnerModel$SinglePublisherVideoChannelsModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v6

    .line 631556
    iput-object v6, v12, LX/4ZQ;->b:Ljava/lang/String;

    .line 631557
    invoke-virtual {v12}, LX/4ZQ;->a()Lcom/facebook/graphql/model/GraphQLVideoChannel;

    move-result-object v12

    goto :goto_1a
.end method

.method public static a(Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;)Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .locals 2

    .prologue
    .line 631029
    if-nez p0, :cond_0

    .line 631030
    const/4 v0, 0x0

    .line 631031
    :goto_0
    return-object v0

    .line 631032
    :cond_0
    new-instance v0, LX/4Yz;

    invoke-direct {v0}, LX/4Yz;-><init>()V

    .line 631033
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;->a()I

    move-result v1

    .line 631034
    iput v1, v0, LX/4Yz;->b:I

    .line 631035
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 631036
    iput-object v1, v0, LX/4Yz;->c:Ljava/lang/String;

    .line 631037
    invoke-virtual {p0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;->c()I

    move-result v1

    .line 631038
    iput v1, v0, LX/4Yz;->d:I

    .line 631039
    invoke-virtual {v0}, LX/4Yz;->a()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/174;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 2

    .prologue
    .line 631177
    if-nez p0, :cond_0

    .line 631178
    const/4 v0, 0x0

    .line 631179
    :goto_0
    return-object v0

    .line 631180
    :cond_0
    new-instance v0, LX/173;

    invoke-direct {v0}, LX/173;-><init>()V

    .line 631181
    invoke-interface {p0}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    .line 631182
    iput-object v1, v0, LX/173;->f:Ljava/lang/String;

    .line 631183
    invoke-virtual {v0}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialoguePinnedStoryQueryModel;)Lcom/facebook/graphql/model/GraphQLViewer;
    .locals 12
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGraphQLViewer"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 631040
    if-nez p0, :cond_0

    .line 631041
    const/4 v0, 0x0

    .line 631042
    :goto_0
    return-object v0

    .line 631043
    :cond_0
    new-instance v0, LX/2sK;

    invoke-direct {v0}, LX/2sK;-><init>()V

    .line 631044
    invoke-virtual {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialoguePinnedStoryQueryModel;->a()Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel;

    move-result-object v1

    .line 631045
    if-nez v1, :cond_1

    .line 631046
    const/4 v2, 0x0

    .line 631047
    :goto_1
    move-object v1, v2

    .line 631048
    iput-object v1, v0, LX/2sK;->l:Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 631049
    invoke-virtual {v0}, LX/2sK;->a()Lcom/facebook/graphql/model/GraphQLViewer;

    move-result-object v0

    goto :goto_0

    .line 631050
    :cond_1
    new-instance v4, LX/3j1;

    invoke-direct {v4}, LX/3j1;-><init>()V

    .line 631051
    invoke-virtual {v1}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 631052
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 631053
    const/4 v2, 0x0

    move v3, v2

    :goto_2
    invoke-virtual {v1}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_2

    .line 631054
    invoke-virtual {v1}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;

    .line 631055
    if-nez v2, :cond_4

    .line 631056
    const/4 v6, 0x0

    .line 631057
    :goto_3
    move-object v2, v6

    .line 631058
    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 631059
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 631060
    :cond_2
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 631061
    iput-object v2, v4, LX/3j1;->b:LX/0Px;

    .line 631062
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 631063
    iput-object v2, v4, LX/3j1;->f:Ljava/lang/String;

    .line 631064
    invoke-virtual {v1}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel;->c()J

    move-result-wide v2

    .line 631065
    iput-wide v2, v4, LX/3j1;->g:J

    .line 631066
    invoke-virtual {v1}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel;->d()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    move-result-object v2

    .line 631067
    if-nez v2, :cond_5

    .line 631068
    const/4 v3, 0x0

    .line 631069
    :goto_4
    move-object v2, v3

    .line 631070
    iput-object v2, v4, LX/3j1;->r:Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    .line 631071
    invoke-virtual {v1}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel;->kp_()Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$StoryHeaderModel;

    move-result-object v2

    .line 631072
    if-nez v2, :cond_b

    .line 631073
    const/4 v3, 0x0

    .line 631074
    :goto_5
    move-object v2, v3

    .line 631075
    iput-object v2, v4, LX/3j1;->y:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 631076
    invoke-virtual {v1}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel;->e()Ljava/lang/String;

    move-result-object v2

    .line 631077
    iput-object v2, v4, LX/3j1;->E:Ljava/lang/String;

    .line 631078
    new-instance v2, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    invoke-direct {v2, v4}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;-><init>(LX/3j1;)V

    .line 631079
    move-object v2, v2

    .line 631080
    goto :goto_1

    .line 631081
    :cond_4
    new-instance v6, LX/4Ys;

    invoke-direct {v6}, LX/4Ys;-><init>()V

    .line 631082
    invoke-virtual {v2}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    .line 631083
    iput-object v7, v6, LX/4Ys;->bH:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 631084
    invoke-virtual {v2}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->b()Ljava/lang/String;

    move-result-object v7

    .line 631085
    iput-object v7, v6, LX/4Ys;->i:Ljava/lang/String;

    .line 631086
    invoke-virtual {v2}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->c()Ljava/lang/String;

    move-result-object v7

    .line 631087
    iput-object v7, v6, LX/4Ys;->j:Ljava/lang/String;

    .line 631088
    invoke-virtual {v2}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->d()LX/0Px;

    move-result-object v7

    .line 631089
    iput-object v7, v6, LX/4Ys;->R:LX/0Px;

    .line 631090
    invoke-virtual {v2}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->e()Ljava/lang/String;

    move-result-object v7

    .line 631091
    iput-object v7, v6, LX/4Ys;->S:Ljava/lang/String;

    .line 631092
    invoke-virtual {v2}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->kq_()LX/0Px;

    move-result-object v7

    .line 631093
    iput-object v7, v6, LX/4Ys;->av:LX/0Px;

    .line 631094
    invoke-virtual {v2}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->kr_()Ljava/lang/String;

    move-result-object v7

    .line 631095
    iput-object v7, v6, LX/4Ys;->aw:Ljava/lang/String;

    .line 631096
    invoke-virtual {v2}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->j()Ljava/lang/String;

    move-result-object v7

    .line 631097
    iput-object v7, v6, LX/4Ys;->bz:Ljava/lang/String;

    .line 631098
    invoke-virtual {v2}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->k()Ljava/lang/String;

    move-result-object v7

    .line 631099
    iput-object v7, v6, LX/4Ys;->bD:Ljava/lang/String;

    .line 631100
    invoke-virtual {v6}, LX/4Ys;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v6

    goto :goto_3

    .line 631101
    :cond_5
    new-instance v6, LX/4XP;

    invoke-direct {v6}, LX/4XP;-><init>()V

    .line 631102
    invoke-virtual {v2}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;->a()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 631103
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 631104
    const/4 v3, 0x0

    move v5, v3

    :goto_6
    invoke-virtual {v2}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v5, v3, :cond_6

    .line 631105
    invoke-virtual {v2}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel$NativeTemplateBundlesModel;

    .line 631106
    if-nez v3, :cond_8

    .line 631107
    const/4 v8, 0x0

    .line 631108
    :goto_7
    move-object v3, v8

    .line 631109
    invoke-virtual {v7, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 631110
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_6

    .line 631111
    :cond_6
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 631112
    iput-object v3, v6, LX/4XP;->b:LX/0Px;

    .line 631113
    :cond_7
    invoke-virtual {v2}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 631114
    iput-object v3, v6, LX/4XP;->c:Ljava/lang/String;

    .line 631115
    invoke-virtual {v6}, LX/4XP;->a()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v3

    goto/16 :goto_4

    .line 631116
    :cond_8
    new-instance v10, LX/4XO;

    invoke-direct {v10}, LX/4XO;-><init>()V

    .line 631117
    invoke-virtual {v3}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel$NativeTemplateBundlesModel;->a()LX/0Px;

    move-result-object v8

    if-eqz v8, :cond_a

    .line 631118
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 631119
    const/4 v8, 0x0

    move v9, v8

    :goto_8
    invoke-virtual {v3}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel$NativeTemplateBundlesModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v8

    if-ge v9, v8, :cond_9

    .line 631120
    invoke-virtual {v3}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel$NativeTemplateBundlesModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateBundleAttributeFragmentModel;

    invoke-static {v8}, LX/3j0;->a(Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateBundleAttributeFragmentModel;)Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;

    move-result-object v8

    invoke-virtual {v11, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 631121
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_8

    .line 631122
    :cond_9
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v8

    .line 631123
    iput-object v8, v10, LX/4XO;->b:LX/0Px;

    .line 631124
    :cond_a
    invoke-virtual {v3}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel$NativeTemplateBundlesModel;->b()Ljava/lang/String;

    move-result-object v8

    .line 631125
    iput-object v8, v10, LX/4XO;->c:Ljava/lang/String;

    .line 631126
    invoke-virtual {v10}, LX/4XO;->a()Lcom/facebook/graphql/model/GraphQLNativeTemplateBundle;

    move-result-object v8

    goto :goto_7

    .line 631127
    :cond_b
    new-instance v3, LX/3j2;

    invoke-direct {v3}, LX/3j2;-><init>()V

    .line 631128
    invoke-virtual {v2}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$StoryHeaderModel;->a()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 631129
    if-nez v5, :cond_c

    .line 631130
    const/4 v7, 0x0

    .line 631131
    :goto_9
    move-object v5, v7

    .line 631132
    iput-object v5, v3, LX/3j2;->b:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 631133
    invoke-virtual {v2}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$StoryHeaderModel;->c()LX/1Fb;

    move-result-object v5

    invoke-static {v5}, LX/3j0;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 631134
    iput-object v5, v3, LX/3j2;->c:Lcom/facebook/graphql/model/GraphQLImage;

    .line 631135
    invoke-virtual {v2}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$StoryHeaderModel;->d()Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$StoryHeaderModel$DescriptionModel;

    move-result-object v5

    .line 631136
    if-nez v5, :cond_d

    .line 631137
    const/4 v6, 0x0

    .line 631138
    :goto_a
    move-object v5, v6

    .line 631139
    iput-object v5, v3, LX/3j2;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 631140
    invoke-virtual {v2}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$StoryHeaderModel;->e()LX/1Fb;

    move-result-object v5

    invoke-static {v5}, LX/3j0;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 631141
    iput-object v5, v3, LX/3j2;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 631142
    invoke-virtual {v2}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$StoryHeaderModel;->ks_()LX/0Px;

    move-result-object v5

    .line 631143
    iput-object v5, v3, LX/3j2;->g:LX/0Px;

    .line 631144
    invoke-virtual {v2}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$StoryHeaderModel;->kt_()Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$StoryHeaderModel$SubtitleModel;

    move-result-object v5

    .line 631145
    if-nez v5, :cond_e

    .line 631146
    const/4 v6, 0x0

    .line 631147
    :goto_b
    move-object v5, v6

    .line 631148
    iput-object v5, v3, LX/3j2;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 631149
    invoke-virtual {v2}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$StoryHeaderModel;->b()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 631150
    if-nez v5, :cond_f

    .line 631151
    const/4 v7, 0x0

    .line 631152
    :goto_c
    move-object v5, v7

    .line 631153
    iput-object v5, v3, LX/3j2;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 631154
    new-instance v5, Lcom/facebook/graphql/model/GraphQLStoryHeader;

    invoke-direct {v5, v3}, Lcom/facebook/graphql/model/GraphQLStoryHeader;-><init>(LX/3j2;)V

    .line 631155
    move-object v3, v5

    .line 631156
    goto/16 :goto_5

    .line 631157
    :cond_c
    new-instance v8, LX/4Ys;

    invoke-direct {v8}, LX/4Ys;-><init>()V

    .line 631158
    const/4 v7, 0x0

    const-class v9, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v6, v5, v7, v9}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v7

    check-cast v7, Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 631159
    iput-object v7, v8, LX/4Ys;->bH:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 631160
    const/4 v7, 0x1

    invoke-virtual {v6, v5, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    .line 631161
    iput-object v7, v8, LX/4Ys;->bz:Ljava/lang/String;

    .line 631162
    const/4 v7, 0x2

    invoke-virtual {v6, v5, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    .line 631163
    iput-object v7, v8, LX/4Ys;->bD:Ljava/lang/String;

    .line 631164
    invoke-virtual {v8}, LX/4Ys;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v7

    goto :goto_9

    .line 631165
    :cond_d
    new-instance v6, LX/173;

    invoke-direct {v6}, LX/173;-><init>()V

    .line 631166
    invoke-virtual {v5}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$StoryHeaderModel$DescriptionModel;->a()Ljava/lang/String;

    move-result-object v7

    .line 631167
    iput-object v7, v6, LX/173;->f:Ljava/lang/String;

    .line 631168
    invoke-virtual {v6}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    goto :goto_a

    .line 631169
    :cond_e
    new-instance v6, LX/173;

    invoke-direct {v6}, LX/173;-><init>()V

    .line 631170
    invoke-virtual {v5}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$StoryHeaderModel$SubtitleModel;->a()Ljava/lang/String;

    move-result-object v7

    .line 631171
    iput-object v7, v6, LX/173;->f:Ljava/lang/String;

    .line 631172
    invoke-virtual {v6}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    goto :goto_b

    .line 631173
    :cond_f
    new-instance v7, LX/173;

    invoke-direct {v7}, LX/173;-><init>()V

    .line 631174
    const/4 v8, 0x0

    invoke-virtual {v6, v5, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v8

    .line 631175
    iput-object v8, v7, LX/173;->f:Ljava/lang/String;

    .line 631176
    invoke-virtual {v7}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    goto :goto_c
.end method
