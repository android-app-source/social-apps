.class public LX/4Qe;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 705646
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 705647
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 705648
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 705649
    :goto_0
    return v1

    .line 705650
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 705651
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 705652
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 705653
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 705654
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 705655
    const-string v7, "node"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 705656
    invoke-static {p0, p1}, LX/2bO;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 705657
    :cond_2
    const-string v7, "tracking"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 705658
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 705659
    :cond_3
    const-string v7, "recommendation_key"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 705660
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 705661
    :cond_4
    const-string v7, "social_context"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 705662
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 705663
    :cond_5
    const-string v7, "target_group"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 705664
    invoke-static {p0, p1}, LX/30j;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 705665
    :cond_6
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 705666
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 705667
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 705668
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 705669
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 705670
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 705671
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 705672
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 705673
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 705674
    if-eqz v0, :cond_0

    .line 705675
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705676
    invoke-static {p0, v0, p2, p3}, LX/2bO;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 705677
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 705678
    if-eqz v0, :cond_1

    .line 705679
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705680
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 705681
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 705682
    if-eqz v0, :cond_2

    .line 705683
    const-string v1, "recommendation_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705684
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 705685
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 705686
    if-eqz v0, :cond_3

    .line 705687
    const-string v1, "social_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705688
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 705689
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 705690
    if-eqz v0, :cond_4

    .line 705691
    const-string v1, "target_group"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705692
    invoke-static {p0, v0, p2, p3}, LX/30j;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 705693
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 705694
    return-void
.end method
