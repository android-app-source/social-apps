.class public final LX/4qN;
.super LX/32s;
.source ""


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final _annotated:LX/2At;

.field public final _getter:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>(LX/2Aq;LX/0lJ;LX/4qw;LX/0lQ;LX/2At;)V
    .locals 1

    .prologue
    .line 813378
    invoke-direct {p0, p1, p2, p3, p4}, LX/32s;-><init>(LX/2Aq;LX/0lJ;LX/4qw;LX/0lQ;)V

    .line 813379
    iput-object p5, p0, LX/4qN;->_annotated:LX/2At;

    .line 813380
    iget-object v0, p5, LX/2At;->a:Ljava/lang/reflect/Method;

    move-object v0, v0

    .line 813381
    iput-object v0, p0, LX/4qN;->_getter:Ljava/lang/reflect/Method;

    .line 813382
    return-void
.end method

.method private constructor <init>(LX/4qN;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4qN;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 813374
    invoke-direct {p0, p1, p2}, LX/32s;-><init>(LX/32s;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 813375
    iget-object v0, p1, LX/4qN;->_annotated:LX/2At;

    iput-object v0, p0, LX/4qN;->_annotated:LX/2At;

    .line 813376
    iget-object v0, p1, LX/4qN;->_getter:Ljava/lang/reflect/Method;

    iput-object v0, p0, LX/4qN;->_getter:Ljava/lang/reflect/Method;

    .line 813377
    return-void
.end method

.method private constructor <init>(LX/4qN;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 813370
    invoke-direct {p0, p1, p2}, LX/32s;-><init>(LX/32s;Ljava/lang/String;)V

    .line 813371
    iget-object v0, p1, LX/4qN;->_annotated:LX/2At;

    iput-object v0, p0, LX/4qN;->_annotated:LX/2At;

    .line 813372
    iget-object v0, p1, LX/4qN;->_getter:Ljava/lang/reflect/Method;

    iput-object v0, p0, LX/4qN;->_getter:Ljava/lang/reflect/Method;

    .line 813373
    return-void
.end method

.method private a(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/4qN;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)",
            "LX/4qN;"
        }
    .end annotation

    .prologue
    .line 813369
    new-instance v0, LX/4qN;

    invoke-direct {v0, p0, p1}, LX/4qN;-><init>(LX/4qN;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    return-object v0
.end method

.method private c(Ljava/lang/String;)LX/4qN;
    .locals 1

    .prologue
    .line 813368
    new-instance v0, LX/4qN;

    invoke-direct {v0, p0, p1}, LX/4qN;-><init>(LX/4qN;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;)LX/32s;
    .locals 1

    .prologue
    .line 813367
    invoke-direct {p0, p1}, LX/4qN;->c(Ljava/lang/String;)LX/4qN;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15w;LX/0n3;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 813349
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 813350
    sget-object v1, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v0, v1, :cond_0

    .line 813351
    :goto_0
    return-void

    .line 813352
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/4qN;->_getter:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p3, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 813353
    if-nez v0, :cond_1

    .line 813354
    new-instance v0, LX/28E;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Problem deserializing \'setterless\' property \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 813355
    iget-object v2, p0, LX/32s;->_propName:Ljava/lang/String;

    move-object v2, v2

    .line 813356
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\': get method returned null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v0

    .line 813357
    :catch_0
    move-exception v0

    .line 813358
    invoke-static {v0}, LX/32s;->a(Ljava/lang/Exception;)Ljava/io/IOException;

    goto :goto_0

    .line 813359
    :cond_1
    iget-object v1, p0, LX/32s;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v1, p1, p2, v0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 813366
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Should never call \'set\' on setterless property"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b()LX/2An;
    .locals 1

    .prologue
    .line 813365
    iget-object v0, p0, LX/4qN;->_annotated:LX/2At;

    return-object v0
.end method

.method public final synthetic b(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/32s;
    .locals 1

    .prologue
    .line 813364
    invoke-direct {p0, p1}, LX/4qN;->a(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/4qN;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 813362
    invoke-virtual {p0, p1, p2, p3}, LX/32s;->a(LX/15w;LX/0n3;Ljava/lang/Object;)V

    .line 813363
    return-object p3
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 813360
    invoke-virtual {p0, p1, p2}, LX/32s;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 813361
    const/4 v0, 0x0

    return-object v0
.end method
