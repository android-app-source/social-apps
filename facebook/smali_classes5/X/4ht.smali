.class public abstract LX/4ht;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field private c:I

.field private d:I

.field public e:Landroid/os/Message;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 802185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/os/Message;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Message;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 802171
    if-nez p4, :cond_0

    .line 802172
    const-string v0, "Expected non-null \'%s\' extra, actual value was null."

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-direct {p0, p1, v0, v1}, LX/4ht;->a(Landroid/os/Message;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 802173
    :goto_0
    return-void

    .line 802174
    :cond_0
    const-string v0, "Expected \'%s\' extra to be type \'%s\', actual value was type \'%s\'."

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-virtual {p3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x2

    invoke-virtual {p4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {p0, p1, v0, v1}, LX/4ht;->a(Landroid/os/Message;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private varargs a(Landroid/os/Message;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 802175
    invoke-static {p2, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 802176
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 802177
    const-string v2, "com.facebook.platform.status.ERROR_TYPE"

    const-string v3, "ProtocolError"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 802178
    const-string v2, "com.facebook.platform.status.ERROR_DESCRIPTION"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 802179
    const/4 v0, 0x0

    invoke-virtual {p0}, LX/4ht;->a()I

    move-result v2

    invoke-static {v0, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 802180
    const v2, 0x133060d

    iput v2, v0, Landroid/os/Message;->arg1:I

    .line 802181
    iget v2, p1, Landroid/os/Message;->arg2:I

    iput v2, v0, Landroid/os/Message;->arg2:I

    .line 802182
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 802183
    iput-object v0, p0, LX/4ht;->e:Landroid/os/Message;

    .line 802184
    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method public abstract a(Landroid/os/Message;)Z
.end method

.method public final a(Landroid/os/Message;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 802141
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    .line 802142
    iput-object p2, p0, LX/4ht;->b:Ljava/lang/String;

    .line 802143
    iget v0, p1, Landroid/os/Message;->arg1:I

    iput v0, p0, LX/4ht;->c:I

    .line 802144
    sget-object v0, LX/25y;->a:Ljava/util/List;

    iget v4, p0, LX/4ht;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 802145
    const-string v0, "Unknown protocol version in \'%s\': %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "Message.arg1"

    aput-object v4, v3, v1

    iget v4, p0, LX/4ht;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-direct {p0, p1, v0, v3}, LX/4ht;->a(Landroid/os/Message;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 802146
    :goto_0
    return v0

    .line 802147
    :cond_0
    iget v0, p1, Landroid/os/Message;->arg2:I

    iput v0, p0, LX/4ht;->d:I

    .line 802148
    const-string v0, "com.facebook.platform.extra.APPLICATION_ID"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 802149
    instance-of v4, v0, Ljava/lang/String;

    if-nez v4, :cond_1

    .line 802150
    const-string v2, "com.facebook.platform.extra.APPLICATION_ID"

    const-class v3, Ljava/lang/String;

    invoke-direct {p0, p1, v2, v3, v0}, LX/4ht;->a(Landroid/os/Message;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V

    move v0, v1

    .line 802151
    goto :goto_0

    .line 802152
    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/4ht;->a:Ljava/lang/String;

    .line 802153
    invoke-virtual {p0, p1}, LX/4ht;->a(Landroid/os/Message;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 802154
    goto :goto_0

    .line 802155
    :cond_2
    const-string v0, "com.facebook.platform.protocol.PROTOCOL_VALIDATE"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 802156
    const-string v0, "com.facebook.platform.protocol.PROTOCOL_VALIDATE"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 802157
    instance-of v3, v0, Ljava/lang/Boolean;

    if-nez v3, :cond_3

    .line 802158
    const-string v2, "com.facebook.platform.protocol.PROTOCOL_VALIDATE"

    const-class v3, Ljava/lang/Boolean;

    invoke-direct {p0, p1, v2, v3, v0}, LX/4ht;->a(Landroid/os/Message;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V

    move v0, v1

    .line 802159
    goto :goto_0

    .line 802160
    :cond_3
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 802161
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 802162
    const-string v3, "com.facebook.platform.protocol.PROTOCOL_VALIDATED"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 802163
    const/4 v2, 0x0

    invoke-virtual {p0}, LX/4ht;->a()I

    move-result v3

    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v2

    .line 802164
    const v3, 0x133060d

    iput v3, v2, Landroid/os/Message;->arg1:I

    .line 802165
    iget v3, p1, Landroid/os/Message;->arg2:I

    iput v3, v2, Landroid/os/Message;->arg2:I

    .line 802166
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 802167
    iput-object v2, p0, LX/4ht;->e:Landroid/os/Message;

    .line 802168
    move v0, v1

    .line 802169
    goto :goto_0

    :cond_4
    move v0, v2

    .line 802170
    goto :goto_0
.end method
