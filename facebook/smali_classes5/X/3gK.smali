.class public LX/3gK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/3gL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3gL",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final b:LX/1vb;


# direct methods
.method public constructor <init>(LX/3gL;LX/1vb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 624273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 624274
    iput-object p1, p0, LX/3gK;->a:LX/3gL;

    .line 624275
    iput-object p2, p0, LX/3gK;->b:LX/1vb;

    .line 624276
    return-void
.end method

.method public static a(LX/0QB;)LX/3gK;
    .locals 5

    .prologue
    .line 624277
    const-class v1, LX/3gK;

    monitor-enter v1

    .line 624278
    :try_start_0
    sget-object v0, LX/3gK;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 624279
    sput-object v2, LX/3gK;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 624280
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 624281
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 624282
    new-instance p0, LX/3gK;

    invoke-static {v0}, LX/3gL;->a(LX/0QB;)LX/3gL;

    move-result-object v3

    check-cast v3, LX/3gL;

    invoke-static {v0}, LX/1vb;->a(LX/0QB;)LX/1vb;

    move-result-object v4

    check-cast v4, LX/1vb;

    invoke-direct {p0, v3, v4}, LX/3gK;-><init>(LX/3gL;LX/1vb;)V

    .line 624283
    move-object v0, p0

    .line 624284
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 624285
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3gK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 624286
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 624287
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
