.class public LX/3RT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2zj;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/3RT;


# instance fields
.field public final a:LX/3RU;

.field private final b:LX/297;

.field private final c:Landroid/content/Context;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/7Cc;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Z

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/3RU;LX/297;Landroid/content/Context;LX/0Or;Ljava/lang/Boolean;LX/0Or;)V
    .locals 1
    .param p5    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/nux/IsNeueNuxPending;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3RU;",
            "LX/297;",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/7Cc;",
            ">;",
            "Ljava/lang/Boolean;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 580445
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 580446
    iput-object p1, p0, LX/3RT;->a:LX/3RU;

    .line 580447
    iput-object p2, p0, LX/3RT;->b:LX/297;

    .line 580448
    iput-object p3, p0, LX/3RT;->c:Landroid/content/Context;

    .line 580449
    iput-object p4, p0, LX/3RT;->d:LX/0Or;

    .line 580450
    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/3RT;->e:Z

    .line 580451
    iput-object p6, p0, LX/3RT;->f:LX/0Or;

    .line 580452
    return-void
.end method

.method public static a(LX/0QB;)LX/3RT;
    .locals 10

    .prologue
    .line 580475
    sget-object v0, LX/3RT;->h:LX/3RT;

    if-nez v0, :cond_1

    .line 580476
    const-class v1, LX/3RT;

    monitor-enter v1

    .line 580477
    :try_start_0
    sget-object v0, LX/3RT;->h:LX/3RT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 580478
    if-eqz v2, :cond_0

    .line 580479
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 580480
    new-instance v3, LX/3RT;

    invoke-static {v0}, LX/3RU;->b(LX/0QB;)LX/3RU;

    move-result-object v4

    check-cast v4, LX/3RU;

    invoke-static {v0}, LX/297;->b(LX/0QB;)LX/297;

    move-result-object v5

    check-cast v5, LX/297;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    const/16 v7, 0x3564

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    const/16 v9, 0x1528

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, LX/3RT;-><init>(LX/3RU;LX/297;Landroid/content/Context;LX/0Or;Ljava/lang/Boolean;LX/0Or;)V

    .line 580481
    move-object v0, v3

    .line 580482
    sput-object v0, LX/3RT;->h:LX/3RT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 580483
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 580484
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 580485
    :cond_1
    sget-object v0, LX/3RT;->h:LX/3RT;

    return-object v0

    .line 580486
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 580487
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 580474
    iget-object v0, p0, LX/3RT;->b:LX/297;

    invoke-virtual {v0}, LX/297;->a()Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/NotificationSetting;->b()Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 580471
    iget-object v0, p0, LX/3RT;->a:LX/3RU;

    .line 580472
    iget-object v1, v0, LX/3RU;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0db;->R:LX/0Tn;

    const/4 p0, 0x1

    invoke-interface {v1, v2, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    move v0, v1

    .line 580473
    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 580470
    const/4 v0, 0x1

    return v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 580467
    iget-object v0, p0, LX/3RT;->a:LX/3RU;

    .line 580468
    iget-object v1, v0, LX/3RU;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0db;->M:LX/0Tn;

    const/4 p0, 0x1

    invoke-interface {v1, v2, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    move v0, v1

    .line 580469
    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 580466
    iget-object v0, p0, LX/3RT;->a:LX/3RU;

    invoke-virtual {v0}, LX/3RU;->c()Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 3

    .prologue
    .line 580463
    iget-object v0, p0, LX/3RT;->a:LX/3RU;

    .line 580464
    iget-object v1, v0, LX/3RU;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0db;->N:LX/0Tn;

    const/4 p0, 0x1

    invoke-interface {v1, v2, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    move v0, v1

    .line 580465
    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 580462
    const v0, 0x7f0212e2

    return v0
.end method

.method public final h()J
    .locals 6

    .prologue
    .line 580461
    const-wide/16 v0, 0x0

    iget-object v2, p0, LX/3RT;->b:LX/297;

    invoke-virtual {v2}, LX/297;->a()Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v2

    iget-wide v2, v2, Lcom/facebook/messaging/model/threads/NotificationSetting;->e:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 580460
    iget-object v0, p0, LX/3RT;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 580453
    iget-object v0, p0, LX/3RT;->g:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 580454
    iget-boolean v0, p0, LX/3RT;->e:Z

    if-eqz v0, :cond_1

    .line 580455
    iget-object v0, p0, LX/3RT;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Cc;

    iget-object v1, p0, LX/3RT;->c:Landroid/content/Context;

    const-string v2, "work_out_of_app_message"

    invoke-virtual {v0, v1, v2}, LX/7Cc;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/3RT;->g:Ljava/lang/String;

    .line 580456
    :cond_0
    :goto_0
    iget-object v0, p0, LX/3RT;->a:LX/3RU;

    iget-object v1, p0, LX/3RT;->g:Ljava/lang/String;

    .line 580457
    iget-object v2, v0, LX/3RU;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object p0, LX/0db;->Q:LX/0Tn;

    invoke-interface {v2, p0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v0, v2

    .line 580458
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 580459
    :cond_1
    iget-object v0, p0, LX/3RT;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Cc;

    iget-object v1, p0, LX/3RT;->c:Landroid/content/Context;

    const-string v2, "out_of_app_message"

    invoke-virtual {v0, v1, v2}, LX/7Cc;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/3RT;->g:Ljava/lang/String;

    goto :goto_0
.end method
