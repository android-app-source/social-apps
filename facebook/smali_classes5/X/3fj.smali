.class public LX/3fj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2Oj;

.field public final b:LX/2Rd;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3hQ;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/3Lz;


# direct methods
.method public constructor <init>(LX/2Oj;LX/2Rd;LX/0Or;LX/3Lz;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Oj;",
            "Lcom/facebook/user/names/Normalizer;",
            "LX/0Or",
            "<",
            "LX/3hQ;",
            ">;",
            "LX/3Lz;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 623167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 623168
    iput-object p1, p0, LX/3fj;->a:LX/2Oj;

    .line 623169
    iput-object p2, p0, LX/3fj;->b:LX/2Rd;

    .line 623170
    iput-object p3, p0, LX/3fj;->c:LX/0Or;

    .line 623171
    iput-object p4, p0, LX/3fj;->d:LX/3Lz;

    .line 623172
    return-void
.end method

.method public static b(LX/0QB;)LX/3fj;
    .locals 5

    .prologue
    .line 623173
    new-instance v3, LX/3fj;

    invoke-static {p0}, LX/2Oj;->a(LX/0QB;)LX/2Oj;

    move-result-object v0

    check-cast v0, LX/2Oj;

    invoke-static {p0}, LX/2Rd;->b(LX/0QB;)LX/2Rd;

    move-result-object v1

    check-cast v1, LX/2Rd;

    const/16 v2, 0x12d3

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/3Ly;->a(LX/0QB;)LX/3Lz;

    move-result-object v2

    check-cast v2, LX/3Lz;

    invoke-direct {v3, v0, v1, v4, v2}, LX/3fj;-><init>(LX/2Oj;LX/2Rd;LX/0Or;LX/3Lz;)V

    .line 623174
    return-object v3
.end method

.method public static c(LX/3fj;Lcom/facebook/contacts/graphql/Contact;LX/3hM;)V
    .locals 3

    .prologue
    .line 623175
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->f()Lcom/facebook/user/model/Name;

    move-result-object v1

    invoke-static {v0, v1}, LX/2Oj;->a(Lcom/facebook/user/model/Name;Lcom/facebook/user/model/Name;)Ljava/lang/String;

    move-result-object v0

    .line 623176
    if-eqz v0, :cond_0

    .line 623177
    const-string v1, "sort_name_key"

    iget-object v2, p0, LX/3fj;->b:LX/2Rd;

    invoke-virtual {v2, v0}, LX/2Rd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v1, v0}, LX/3hM;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 623178
    :cond_0
    return-void
.end method

.method public static d(LX/3fj;Lcom/facebook/contacts/graphql/Contact;LX/3hM;)V
    .locals 3

    .prologue
    .line 623179
    iget-object v0, p0, LX/3fj;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3hQ;

    .line 623180
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3hQ;->a(Lcom/facebook/user/model/Name;)V

    .line 623181
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->f()Lcom/facebook/user/model/Name;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3hQ;->a(Lcom/facebook/user/model/Name;)V

    .line 623182
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->z()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3hQ;->a(LX/0Px;)V

    .line 623183
    iget-object v1, v0, LX/3hQ;->f:Ljava/util/Set;

    move-object v0, v1

    .line 623184
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 623185
    const-string v2, "name"

    invoke-virtual {p2, v2, v0}, LX/3hM;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 623186
    :cond_0
    return-void
.end method
