.class public abstract LX/3u1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 647355
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 647356
    return-void
.end method


# virtual methods
.method public a(LX/3uG;)LX/3uV;
    .locals 1

    .prologue
    .line 647357
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract a()Landroid/view/View;
.end method

.method public a(F)V
    .locals 2

    .prologue
    .line 647358
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 647359
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Setting a non-zero elevation is not supported in this action bar configuration."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 647360
    :cond_0
    return-void
.end method

.method public abstract a(I)V
.end method

.method public abstract a(II)V
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 647361
    return-void
.end method

.method public abstract a(Landroid/view/View;)V
.end method

.method public abstract a(Ljava/lang/CharSequence;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract b()I
.end method

.method public abstract b(I)V
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
.end method

.method public abstract b(Ljava/lang/CharSequence;)V
.end method

.method public abstract b(Z)V
.end method

.method public abstract c()V
.end method

.method public abstract c(I)V
.end method

.method public c(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 647354
    return-void
.end method

.method public c(Z)V
    .locals 2

    .prologue
    .line 647351
    if-eqz p1, :cond_0

    .line 647352
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Hide on content scroll is not supported in this action bar configuration."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 647353
    :cond_0
    return-void
.end method

.method public abstract d()V
.end method

.method public d(I)V
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 647350
    return-void
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 647349
    return-void
.end method

.method public e()Landroid/content/Context;
    .locals 1

    .prologue
    .line 647348
    const/4 v0, 0x0

    return-object v0
.end method

.method public e(Z)V
    .locals 0

    .prologue
    .line 647346
    return-void
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 647347
    const/4 v0, 0x0

    return v0
.end method
