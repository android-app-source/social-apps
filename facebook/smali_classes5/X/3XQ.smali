.class public LX/3XQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 593035
    sget-object v0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;->d:LX/1Cz;

    sget-object v1, Lcom/facebook/feedplugins/condensedstory/CondensedStoryInlineLinkshareComponentPartDefinition;->d:LX/1Cz;

    sget-object v2, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;->d:LX/1Cz;

    sget-object v3, Lcom/facebook/feedplugins/condensedstory/CondensedStoryReshareComponentPartDefinition;->d:LX/1Cz;

    sget-object v4, Lcom/facebook/feedplugins/condensedstory/CondensedStoryWithFullHeaderComponentPartDefinition;->d:LX/1Cz;

    invoke-static {v0, v1, v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3XQ;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 593036
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 593037
    return-void
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 3

    .prologue
    .line 593038
    sget-object v0, LX/3XQ;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    sget-object v0, LX/3XQ;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cz;

    .line 593039
    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 593040
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 593041
    :cond_0
    return-void
.end method
