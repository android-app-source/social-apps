.class public LX/4L4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 681819
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 681734
    const/4 v9, 0x0

    .line 681735
    const/4 v8, 0x0

    .line 681736
    const/4 v7, 0x0

    .line 681737
    const/4 v6, 0x0

    .line 681738
    const/4 v5, 0x0

    .line 681739
    const/4 v4, 0x0

    .line 681740
    const/4 v3, 0x0

    .line 681741
    const/4 v2, 0x0

    .line 681742
    const/4 v1, 0x0

    .line 681743
    const/4 v0, 0x0

    .line 681744
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 681745
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 681746
    const/4 v0, 0x0

    .line 681747
    :goto_0
    return v0

    .line 681748
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 681749
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_8

    .line 681750
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 681751
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 681752
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 681753
    const-string v11, "action"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 681754
    invoke-static {p0, p1}, LX/4Ka;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 681755
    :cond_2
    const-string v11, "error_for_logging"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 681756
    invoke-static {p0, p1}, LX/4Lz;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 681757
    :cond_3
    const-string v11, "message_type"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 681758
    const/4 v2, 0x1

    .line 681759
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;

    move-result-object v7

    goto :goto_1

    .line 681760
    :cond_4
    const-string v11, "spec_element"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 681761
    const/4 v1, 0x1

    .line 681762
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    move-result-object v6

    goto :goto_1

    .line 681763
    :cond_5
    const-string v11, "text"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 681764
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 681765
    :cond_6
    const-string v11, "title"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 681766
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 681767
    :cond_7
    const-string v11, "is_sticky"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 681768
    const/4 v0, 0x1

    .line 681769
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    goto :goto_1

    .line 681770
    :cond_8
    const/4 v10, 0x7

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 681771
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 681772
    const/4 v9, 0x1

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 681773
    if-eqz v2, :cond_9

    .line 681774
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v7}, LX/186;->a(ILjava/lang/Enum;)V

    .line 681775
    :cond_9
    if-eqz v1, :cond_a

    .line 681776
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v6}, LX/186;->a(ILjava/lang/Enum;)V

    .line 681777
    :cond_a
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 681778
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 681779
    if-eqz v0, :cond_b

    .line 681780
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 681781
    :cond_b
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 681813
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 681814
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 681815
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 681816
    invoke-static {p0, p1}, LX/4L4;->a(LX/15w;LX/186;)I

    move-result v1

    .line 681817
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 681818
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 681782
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 681783
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 681784
    if-eqz v0, :cond_0

    .line 681785
    const-string v1, "action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681786
    invoke-static {p0, v0, p2, p3}, LX/4Ka;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 681787
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 681788
    if-eqz v0, :cond_1

    .line 681789
    const-string v1, "error_for_logging"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681790
    invoke-static {p0, v0, p2}, LX/4Lz;->a(LX/15i;ILX/0nX;)V

    .line 681791
    :cond_1
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 681792
    if-eqz v0, :cond_2

    .line 681793
    const-string v0, "message_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681794
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 681795
    :cond_2
    invoke-virtual {p0, p1, v4, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 681796
    if-eqz v0, :cond_3

    .line 681797
    const-string v0, "spec_element"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681798
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 681799
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 681800
    if-eqz v0, :cond_4

    .line 681801
    const-string v1, "text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681802
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 681803
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 681804
    if-eqz v0, :cond_5

    .line 681805
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681806
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 681807
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 681808
    if-eqz v0, :cond_6

    .line 681809
    const-string v1, "is_sticky"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681810
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 681811
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 681812
    return-void
.end method
