.class public final LX/47H;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Landroid/os/Bundle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/47G;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 672049
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map;)LX/47H;
    .locals 1
    .param p1    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "LX/47H;"
        }
    .end annotation

    .prologue
    .line 672050
    if-eqz p1, :cond_0

    .line 672051
    invoke-static {p1}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/47H;->c:LX/0P1;

    .line 672052
    :goto_0
    return-object p0

    .line 672053
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/47H;->c:LX/0P1;

    goto :goto_0
.end method

.method public final a()LX/47I;
    .locals 6

    .prologue
    .line 672048
    new-instance v0, LX/47I;

    iget-object v1, p0, LX/47H;->a:Ljava/lang/String;

    iget-object v2, p0, LX/47H;->b:Landroid/os/Bundle;

    iget-object v3, p0, LX/47H;->c:LX/0P1;

    iget-object v4, p0, LX/47H;->d:LX/47G;

    invoke-direct/range {v0 .. v4}, LX/47I;-><init>(Ljava/lang/String;Landroid/os/Bundle;LX/0P1;LX/47G;)V

    return-object v0
.end method
