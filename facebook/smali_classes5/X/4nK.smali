.class public final LX/4nK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/2Vx",
        "<TKEY;TVA",
        "LUE;",
        ">.InMemoryCachedValue<TKEY;TVA",
        "LUE;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2Vx;

.field private b:J


# direct methods
.method public constructor <init>(LX/2Vx;)V
    .locals 4

    .prologue
    .line 806731
    iput-object p1, p0, LX/4nK;->a:LX/2Vx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 806732
    iget-object v0, p1, LX/2Vx;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/32 v2, 0x1d4c0

    div-long/2addr v0, v2

    iput-wide v0, p0, LX/4nK;->b:J

    .line 806733
    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 11

    .prologue
    .line 806734
    check-cast p1, LX/4nL;

    check-cast p2, LX/4nL;

    const-wide/32 v6, 0x1d4c0

    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 806735
    iget-wide v2, p1, LX/4nL;->f:J

    div-long/2addr v2, v6

    .line 806736
    iget-wide v4, p2, LX/4nL;->f:J

    div-long/2addr v4, v6

    .line 806737
    iget-wide v6, p1, LX/4nL;->f:J

    .line 806738
    iget-wide v8, p2, LX/4nL;->f:J

    .line 806739
    cmp-long v10, v2, v4

    if-gez v10, :cond_1

    .line 806740
    :cond_0
    :goto_0
    return v0

    .line 806741
    :cond_1
    cmp-long v4, v2, v4

    if-lez v4, :cond_2

    move v0, v1

    .line 806742
    goto :goto_0

    .line 806743
    :cond_2
    iget-wide v4, p0, LX/4nK;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    .line 806744
    cmp-long v2, v6, v8

    if-ltz v2, :cond_0

    .line 806745
    cmp-long v0, v6, v8

    if-lez v0, :cond_3

    move v0, v1

    .line 806746
    goto :goto_0

    .line 806747
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 806748
    :cond_4
    iget v0, p2, LX/4nL;->d:I

    iget v1, p1, LX/4nL;->d:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method
