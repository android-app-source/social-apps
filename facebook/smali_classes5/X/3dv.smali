.class public final LX/3dv;
.super LX/0re;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0re",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3du;

.field public final synthetic b:LX/0rd;


# direct methods
.method public constructor <init>(LX/0rd;IIILX/0pk;LX/0Ot;LX/0rg;LX/3du;)V
    .locals 7

    .prologue
    .line 618030
    iput-object p1, p0, LX/3dv;->b:LX/0rd;

    iput-object p8, p0, LX/3dv;->a:LX/3du;

    move-object v0, p0

    move v1, p2

    move v2, p3

    move v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, LX/0re;-><init>(IIILX/0pk;LX/0Ot;LX/0rg;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)I"
        }
    .end annotation

    .prologue
    .line 618031
    check-cast p1, Ljava/lang/String;

    check-cast p2, Lcom/facebook/stickers/model/Sticker;

    .line 618032
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p0

    if-nez p2, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int/2addr v1, p0

    move v0, v1

    .line 618033
    return v0

    :cond_0
    const/4 v0, 0x0

    .line 618034
    iget-object v1, p2, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object p1, p2, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    add-int/2addr p1, v1

    iget-object v1, p2, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    if-nez v1, :cond_1

    move v1, v0

    :goto_1
    add-int/2addr p1, v1

    iget-object v1, p2, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    if-nez v1, :cond_2

    move v1, v0

    :goto_2
    add-int/2addr p1, v1

    iget-object v1, p2, Lcom/facebook/stickers/model/Sticker;->e:Landroid/net/Uri;

    if-nez v1, :cond_3

    move v1, v0

    :goto_3
    add-int/2addr p1, v1

    iget-object v1, p2, Lcom/facebook/stickers/model/Sticker;->f:Landroid/net/Uri;

    if-nez v1, :cond_4

    move v1, v0

    :goto_4
    add-int/2addr p1, v1

    iget-object v1, p2, Lcom/facebook/stickers/model/Sticker;->g:Landroid/net/Uri;

    if-nez v1, :cond_5

    move v1, v0

    :goto_5
    add-int/2addr v1, p1

    iget-object p1, p2, Lcom/facebook/stickers/model/Sticker;->h:Landroid/net/Uri;

    if-nez p1, :cond_6

    :goto_6
    add-int/2addr v1, v0

    .line 618035
    const/16 v0, 0x14

    move v0, v0

    .line 618036
    add-int/2addr v1, v0

    move v1, v1

    .line 618037
    goto :goto_0

    :cond_1
    iget-object v1, p2, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_1

    :cond_2
    iget-object v1, p2, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    :cond_3
    iget-object v1, p2, Lcom/facebook/stickers/model/Sticker;->e:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_3

    :cond_4
    iget-object v1, p2, Lcom/facebook/stickers/model/Sticker;->f:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_4

    :cond_5
    iget-object v1, p2, Lcom/facebook/stickers/model/Sticker;->g:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_5

    :cond_6
    iget-object v0, p2, Lcom/facebook/stickers/model/Sticker;->h:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_6
.end method
