.class public LX/3Z9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/3Z9;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 598445
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 598446
    return-void
.end method

.method public static a(LX/0QB;)LX/3Z9;
    .locals 3

    .prologue
    .line 598428
    sget-object v0, LX/3Z9;->a:LX/3Z9;

    if-nez v0, :cond_1

    .line 598429
    const-class v1, LX/3Z9;

    monitor-enter v1

    .line 598430
    :try_start_0
    sget-object v0, LX/3Z9;->a:LX/3Z9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 598431
    if-eqz v2, :cond_0

    .line 598432
    :try_start_1
    new-instance v0, LX/3Z9;

    invoke-direct {v0}, LX/3Z9;-><init>()V

    .line 598433
    move-object v0, v0

    .line 598434
    sput-object v0, LX/3Z9;->a:LX/3Z9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 598435
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 598436
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 598437
    :cond_1
    sget-object v0, LX/3Z9;->a:LX/3Z9;

    return-object v0

    .line 598438
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 598439
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 598440
    sget-object v0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 598441
    sget-object v0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 598442
    sget-object v0, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 598443
    sget-object v0, Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 598444
    return-void
.end method
