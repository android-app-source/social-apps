.class public LX/4c6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/4bV;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/4c6;


# instance fields
.field private final a:LX/4cD;


# direct methods
.method public constructor <init>(LX/4cD;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 794573
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 794574
    iput-object p1, p0, LX/4c6;->a:LX/4cD;

    .line 794575
    return-void
.end method

.method public static a(LX/0QB;)LX/4c6;
    .locals 4

    .prologue
    .line 794576
    sget-object v0, LX/4c6;->b:LX/4c6;

    if-nez v0, :cond_1

    .line 794577
    const-class v1, LX/4c6;

    monitor-enter v1

    .line 794578
    :try_start_0
    sget-object v0, LX/4c6;->b:LX/4c6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 794579
    if-eqz v2, :cond_0

    .line 794580
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 794581
    new-instance p0, LX/4c6;

    invoke-static {v0}, LX/4cD;->a(LX/0QB;)LX/4cD;

    move-result-object v3

    check-cast v3, LX/4cD;

    invoke-direct {p0, v3}, LX/4c6;-><init>(LX/4cD;)V

    .line 794582
    move-object v0, p0

    .line 794583
    sput-object v0, LX/4c6;->b:LX/4c6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 794584
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 794585
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 794586
    :cond_1
    sget-object v0, LX/4c6;->b:LX/4c6;

    return-object v0

    .line 794587
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 794588
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/4bV;LX/4bV;)I
    .locals 13

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x1

    .line 794589
    invoke-virtual {p0}, LX/4bV;->a()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v0

    invoke-virtual {p1}, LX/4bV;->a()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v3

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 794590
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 794591
    iget-object v7, p0, LX/4bV;->c:LX/15D;

    .line 794592
    iget-wide v11, v7, LX/15D;->n:J

    move-wide v7, v11

    .line 794593
    iget-object v9, p1, LX/4bV;->c:LX/15D;

    .line 794594
    iget-wide v11, v9, LX/15D;->n:J

    move-wide v9, v11

    .line 794595
    cmp-long v7, v7, v9

    if-eqz v7, :cond_5

    .line 794596
    iget-object v7, p0, LX/4bV;->c:LX/15D;

    .line 794597
    iget-wide v11, v7, LX/15D;->n:J

    move-wide v7, v11

    .line 794598
    iget-object v9, p1, LX/4bV;->c:LX/15D;

    .line 794599
    iget-wide v11, v9, LX/15D;->n:J

    move-wide v9, v11

    .line 794600
    cmp-long v7, v7, v9

    if-gez v7, :cond_4

    .line 794601
    :cond_0
    :goto_1
    move v0, v5

    .line 794602
    invoke-virtual {p0}, LX/4bV;->a()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v3

    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    if-eq v3, v4, :cond_3

    .line 794603
    if-eqz v0, :cond_1

    move v1, v2

    .line 794604
    :cond_1
    :goto_2
    return v1

    .line 794605
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 794606
    :cond_3
    if-nez v0, :cond_1

    move v1, v2

    goto :goto_2

    :cond_4
    move v5, v6

    .line 794607
    goto :goto_1

    .line 794608
    :cond_5
    iget-object v7, p0, LX/4bV;->c:LX/15D;

    .line 794609
    iget v8, v7, LX/15D;->m:I

    move v7, v8

    .line 794610
    iget-object v8, p1, LX/4bV;->c:LX/15D;

    .line 794611
    iget v9, v8, LX/15D;->m:I

    move v8, v9

    .line 794612
    if-lt v7, v8, :cond_0

    move v5, v6

    goto :goto_1
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 794613
    check-cast p1, LX/4bV;

    check-cast p2, LX/4bV;

    .line 794614
    if-ne p1, p2, :cond_0

    .line 794615
    const/4 v0, 0x0

    .line 794616
    :goto_0
    return v0

    .line 794617
    :cond_0
    iget-object v0, p0, LX/4c6;->a:LX/4cD;

    invoke-virtual {v0, p1}, LX/4cD;->a(LX/4bV;)Z

    move-result v0

    .line 794618
    iget-object v1, p0, LX/4c6;->a:LX/4cD;

    invoke-virtual {v1, p2}, LX/4cD;->a(LX/4bV;)Z

    move-result v1

    .line 794619
    if-eq v0, v1, :cond_2

    .line 794620
    if-eqz v0, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 794621
    :cond_2
    invoke-virtual {p1}, LX/4bV;->a()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v0

    invoke-virtual {p2}, LX/4bV;->a()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v1

    if-eq v0, v1, :cond_3

    .line 794622
    invoke-virtual {p1}, LX/4bV;->a()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/http/interfaces/RequestPriority;->getNumericValue()I

    move-result v0

    invoke-virtual {p2}, LX/4bV;->a()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/http/interfaces/RequestPriority;->getNumericValue()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0

    .line 794623
    :cond_3
    invoke-static {p1, p2}, LX/4c6;->b(LX/4bV;LX/4bV;)I

    move-result v0

    goto :goto_0
.end method
