.class public LX/4P0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 699312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 699313
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_9

    .line 699314
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 699315
    :goto_0
    return v1

    .line 699316
    :cond_0
    const-string v10, "validation_condition"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 699317
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    move-result-object v3

    move-object v5, v3

    move v3, v2

    .line 699318
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_6

    .line 699319
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 699320
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 699321
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 699322
    const-string v10, "custom_error_message"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 699323
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 699324
    :cond_2
    const-string v10, "range"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 699325
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 699326
    :cond_3
    const-string v10, "single_value"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 699327
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 699328
    :cond_4
    const-string v10, "validation_type"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 699329
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    move-result-object v0

    move-object v4, v0

    move v0, v2

    goto :goto_1

    .line 699330
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 699331
    :cond_6
    const/4 v9, 0x5

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 699332
    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 699333
    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 699334
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 699335
    if-eqz v3, :cond_7

    .line 699336
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v5}, LX/186;->a(ILjava/lang/Enum;)V

    .line 699337
    :cond_7
    if-eqz v0, :cond_8

    .line 699338
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 699339
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v3, v1

    move-object v4, v0

    move-object v5, v0

    move v6, v1

    move v7, v1

    move v8, v1

    move v0, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 699340
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 699341
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 699342
    if-eqz v0, :cond_0

    .line 699343
    const-string v1, "custom_error_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699344
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 699345
    :cond_0
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 699346
    if-eqz v0, :cond_1

    .line 699347
    const-string v0, "range"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699348
    invoke-virtual {p0, p1, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 699349
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 699350
    if-eqz v0, :cond_2

    .line 699351
    const-string v1, "single_value"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699352
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 699353
    :cond_2
    invoke-virtual {p0, p1, v4, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 699354
    if-eqz v0, :cond_3

    .line 699355
    const-string v0, "validation_condition"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699356
    const-class v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 699357
    :cond_3
    invoke-virtual {p0, p1, v5, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 699358
    if-eqz v0, :cond_4

    .line 699359
    const-string v0, "validation_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699360
    const-class v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    invoke-virtual {p0, p1, v5, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 699361
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 699362
    return-void
.end method
