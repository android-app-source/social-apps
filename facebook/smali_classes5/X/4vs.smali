.class public final LX/4vs;
.super LX/1ID;
.source ""


# instance fields
.field private final match1:C

.field private final match2:C


# direct methods
.method public constructor <init>(CC)V
    .locals 0

    .prologue
    .line 819720
    invoke-direct {p0}, LX/1ID;-><init>()V

    .line 819721
    iput-char p1, p0, LX/4vs;->match1:C

    .line 819722
    iput-char p2, p0, LX/4vs;->match2:C

    .line 819723
    return-void
.end method


# virtual methods
.method public final matches(C)Z
    .locals 1

    .prologue
    .line 819724
    iget-char v0, p0, LX/4vs;->match1:C

    if-eq p1, v0, :cond_0

    iget-char v0, p0, LX/4vs;->match2:C

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 819725
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CharMatcher.anyOf(\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-char v1, p0, LX/4vs;->match1:C

    invoke-static {v1}, LX/1IA;->showCharacter(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-char v1, p0, LX/4vs;->match2:C

    invoke-static {v1}, LX/1IA;->showCharacter(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
