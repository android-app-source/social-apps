.class public LX/3mf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "LX/1dV;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 637104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 637105
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/3mf;->a:LX/0YU;

    .line 637106
    return-void
.end method

.method private a(III)V
    .locals 5

    .prologue
    .line 637049
    add-int v0, p1, p2

    add-int/lit8 v2, v0, -0x1

    .line 637050
    iget-object v0, p0, LX/3mf;->a:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/3mf;->a()I

    move-result v0

    invoke-virtual {p0}, LX/3mf;->c()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    if-lt v0, p1, :cond_0

    add-int v0, v2, p3

    invoke-virtual {p0}, LX/3mf;->a()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 637051
    :cond_0
    return-void

    :cond_1
    move v1, v2

    .line 637052
    :goto_0
    if-lt v1, p1, :cond_3

    .line 637053
    iget-object v0, p0, LX/3mf;->a:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dV;

    .line 637054
    if-nez v0, :cond_2

    .line 637055
    iget-object v0, p0, LX/3mf;->a:LX/0YU;

    add-int v3, v1, p3

    .line 637056
    invoke-virtual {v0, v3}, LX/0YU;->b(I)V

    .line 637057
    :goto_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 637058
    :cond_2
    iget-object v0, p0, LX/3mf;->a:LX/0YU;

    add-int v3, v1, p3

    iget-object v4, p0, LX/3mf;->a:LX/0YU;

    invoke-virtual {v4, v1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 637059
    iget-object v0, p0, LX/3mf;->a:LX/0YU;

    .line 637060
    invoke-virtual {v0, v1}, LX/0YU;->b(I)V

    .line 637061
    goto :goto_1

    .line 637062
    :cond_3
    add-int v0, p1, p3

    add-int/lit8 v0, v0, -0x1

    :goto_2
    if-le v0, v2, :cond_0

    .line 637063
    iget-object v1, p0, LX/3mf;->a:LX/0YU;

    .line 637064
    invoke-virtual {v1, v0}, LX/0YU;->b(I)V

    .line 637065
    add-int/lit8 v0, v0, -0x1

    goto :goto_2
.end method

.method public static b(LX/3mf;III)V
    .locals 5

    .prologue
    .line 637086
    add-int v0, p1, p2

    add-int/lit8 v2, v0, -0x1

    .line 637087
    iget-object v0, p0, LX/3mf;->a:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/3mf;->a()I

    move-result v0

    invoke-virtual {p0}, LX/3mf;->c()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    sub-int v1, p1, p3

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, LX/3mf;->a()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 637088
    :cond_0
    return-void

    :cond_1
    move v1, p1

    .line 637089
    :goto_0
    if-gt v1, v2, :cond_4

    .line 637090
    iget-object v0, p0, LX/3mf;->a:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dV;

    .line 637091
    if-nez v0, :cond_2

    .line 637092
    iget-object v0, p0, LX/3mf;->a:LX/0YU;

    sub-int v3, v1, p3

    .line 637093
    invoke-virtual {v0, v3}, LX/0YU;->b(I)V

    .line 637094
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 637095
    :cond_2
    sub-int v3, v1, p3

    if-ltz v3, :cond_3

    .line 637096
    iget-object v3, p0, LX/3mf;->a:LX/0YU;

    sub-int v4, v1, p3

    invoke-virtual {v3, v4, v0}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 637097
    :cond_3
    iget-object v0, p0, LX/3mf;->a:LX/0YU;

    .line 637098
    invoke-virtual {v0, v1}, LX/0YU;->b(I)V

    .line 637099
    goto :goto_1

    .line 637100
    :cond_4
    sub-int v0, v2, p3

    add-int/lit8 v0, v0, 0x1

    :goto_2
    if-ge v0, p1, :cond_0

    .line 637101
    iget-object v1, p0, LX/3mf;->a:LX/0YU;

    .line 637102
    invoke-virtual {v1, v0}, LX/0YU;->b(I)V

    .line 637103
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 637085
    iget-object v0, p0, LX/3mf;->a:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/3mf;->a:LX/0YU;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0YU;->e(I)I

    move-result v0

    goto :goto_0
.end method

.method public final a(ILX/1dV;)V
    .locals 1

    .prologue
    .line 637083
    iget-object v0, p0, LX/3mf;->a:LX/0YU;

    invoke-virtual {v0, p1, p2}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 637084
    return-void
.end method

.method public final b(I)V
    .locals 4

    .prologue
    .line 637073
    const/4 v0, 0x1

    .line 637074
    add-int v1, p1, v0

    invoke-virtual {p0}, LX/3mf;->a()I

    move-result v2

    invoke-virtual {p0}, LX/3mf;->c()I

    move-result v3

    add-int/2addr v2, v3

    if-ge v1, v2, :cond_1

    .line 637075
    add-int v1, p1, v0

    invoke-virtual {p0}, LX/3mf;->a()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 637076
    invoke-virtual {p0}, LX/3mf;->a()I

    move-result v2

    invoke-virtual {p0}, LX/3mf;->c()I

    move-result v3

    add-int/2addr v2, v3

    sub-int/2addr v2, v1

    invoke-static {p0, v1, v2, v0}, LX/3mf;->b(LX/3mf;III)V

    .line 637077
    :cond_0
    return-void

    .line 637078
    :cond_1
    invoke-virtual {p0}, LX/3mf;->a()I

    move-result v1

    invoke-virtual {p0}, LX/3mf;->c()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    .line 637079
    :goto_0
    if-gt p1, v1, :cond_0

    .line 637080
    iget-object v2, p0, LX/3mf;->a:LX/0YU;

    .line 637081
    invoke-virtual {v2, p1}, LX/0YU;->b(I)V

    .line 637082
    add-int/lit8 p1, p1, 0x1

    goto :goto_0
.end method

.method public final b(ILX/1dV;)V
    .locals 2

    .prologue
    .line 637070
    invoke-virtual {p0}, LX/3mf;->a()I

    move-result v0

    invoke-virtual {p0}, LX/3mf;->c()I

    move-result v1

    add-int/2addr v0, v1

    sub-int/2addr v0, p1

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, LX/3mf;->a(III)V

    .line 637071
    iget-object v0, p0, LX/3mf;->a:LX/0YU;

    invoke-virtual {v0, p1, p2}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 637072
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 637069
    iget-object v0, p0, LX/3mf;->a:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    return v0
.end method

.method public final c(I)LX/1dV;
    .locals 1

    .prologue
    .line 637068
    iget-object v0, p0, LX/3mf;->a:LX/0YU;

    invoke-virtual {v0, p1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dV;

    return-object v0
.end method

.method public final d(I)V
    .locals 2

    .prologue
    .line 637066
    invoke-virtual {p0}, LX/3mf;->a()I

    move-result v0

    invoke-virtual {p0}, LX/3mf;->c()I

    move-result v1

    invoke-direct {p0, v0, v1, p1}, LX/3mf;->a(III)V

    .line 637067
    return-void
.end method
