.class public LX/3R8;
.super LX/3R9;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile l:LX/3R8;


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/app/KeyguardManager;

.field private final d:LX/15P;

.field private final e:LX/3RA;

.field private final f:LX/0c4;

.field private final g:LX/0pu;

.field private final h:LX/2Mk;

.field private final i:LX/3RE;

.field private final j:LX/1Ml;

.field private final k:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/0SG;LX/0Or;Landroid/app/KeyguardManager;LX/15P;LX/3RA;LX/0c4;LX/0pu;LX/2Mk;LX/3RE;LX/1Ml;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/chatheads/annotations/IsChatHeadsEnabled;
        .end annotation
    .end param
    .param p6    # LX/0c4;
        .annotation runtime Lcom/facebook/messages/ipc/peer/MessageNotificationPeer;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Landroid/app/KeyguardManager;",
            "LX/15P;",
            "LX/3RA;",
            "Lcom/facebook/multiprocess/peer/state/StatefulPeerManager;",
            "LX/0pu;",
            "LX/2Mk;",
            "LX/3RE;",
            "LX/1Ml;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 579004
    invoke-direct {p0}, LX/3R9;-><init>()V

    .line 579005
    iput-object p1, p0, LX/3R8;->a:LX/0SG;

    .line 579006
    iput-object p2, p0, LX/3R8;->b:LX/0Or;

    .line 579007
    iput-object p3, p0, LX/3R8;->c:Landroid/app/KeyguardManager;

    .line 579008
    iput-object p4, p0, LX/3R8;->d:LX/15P;

    .line 579009
    iput-object p5, p0, LX/3R8;->e:LX/3RA;

    .line 579010
    iput-object p6, p0, LX/3R8;->f:LX/0c4;

    .line 579011
    iput-object p7, p0, LX/3R8;->g:LX/0pu;

    .line 579012
    iput-object p8, p0, LX/3R8;->h:LX/2Mk;

    .line 579013
    iput-object p9, p0, LX/3R8;->i:LX/3RE;

    .line 579014
    iput-object p10, p0, LX/3R8;->j:LX/1Ml;

    .line 579015
    iput-object p11, p0, LX/3R8;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 579016
    return-void
.end method

.method public static a(LX/0QB;)LX/3R8;
    .locals 15

    .prologue
    .line 579017
    sget-object v0, LX/3R8;->l:LX/3R8;

    if-nez v0, :cond_1

    .line 579018
    const-class v1, LX/3R8;

    monitor-enter v1

    .line 579019
    :try_start_0
    sget-object v0, LX/3R8;->l:LX/3R8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 579020
    if-eqz v2, :cond_0

    .line 579021
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 579022
    new-instance v3, LX/3R8;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    const/16 v5, 0x1504

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/1sh;->b(LX/0QB;)Landroid/app/KeyguardManager;

    move-result-object v6

    check-cast v6, Landroid/app/KeyguardManager;

    invoke-static {v0}, LX/15P;->a(LX/0QB;)LX/15P;

    move-result-object v7

    check-cast v7, LX/15P;

    invoke-static {v0}, LX/3RA;->a(LX/0QB;)LX/3RA;

    move-result-object v8

    check-cast v8, LX/3RA;

    invoke-static {v0}, LX/0c4;->a(LX/0QB;)LX/0c4;

    move-result-object v9

    check-cast v9, LX/0c4;

    invoke-static {v0}, LX/0pu;->a(LX/0QB;)LX/0pu;

    move-result-object v10

    check-cast v10, LX/0pu;

    invoke-static {v0}, LX/2Mk;->a(LX/0QB;)LX/2Mk;

    move-result-object v11

    check-cast v11, LX/2Mk;

    invoke-static {v0}, LX/3RE;->b(LX/0QB;)LX/3RE;

    move-result-object v12

    check-cast v12, LX/3RE;

    invoke-static {v0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v13

    check-cast v13, LX/1Ml;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v14

    check-cast v14, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct/range {v3 .. v14}, LX/3R8;-><init>(LX/0SG;LX/0Or;Landroid/app/KeyguardManager;LX/15P;LX/3RA;LX/0c4;LX/0pu;LX/2Mk;LX/3RE;LX/1Ml;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 579023
    move-object v0, v3

    .line 579024
    sput-object v0, LX/3R8;->l:LX/3R8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 579025
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 579026
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 579027
    :cond_1
    sget-object v0, LX/3R8;->l:LX/3R8;

    return-object v0

    .line 579028
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 579029
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 579030
    iget-object v0, p0, LX/3R8;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 579031
    :goto_0
    return-void

    .line 579032
    :cond_0
    iget-object v0, p0, LX/3R8;->e:LX/3RA;

    invoke-virtual {v0, p1, p2}, LX/3RA;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/notify/NewMessageNotification;)V
    .locals 3

    .prologue
    .line 579033
    iget-object v1, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->g:LX/Dhq;

    .line 579034
    iget-object v0, p0, LX/3R8;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3R8;->j:LX/1Ml;

    invoke-virtual {v0}, LX/1Ml;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3R8;->i:LX/3RE;

    iget-object v2, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v2}, LX/3RE;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 579035
    :cond_0
    const/4 v0, 0x1

    .line 579036
    iput-boolean v0, v1, LX/Dhq;->a:Z

    .line 579037
    :cond_1
    return-void
.end method

.method public final a(Lcom/facebook/messaging/notify/ReadThreadNotification;)V
    .locals 4

    .prologue
    .line 579038
    iget-object v0, p0, LX/3R8;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 579039
    :cond_0
    return-void

    .line 579040
    :cond_1
    iget-object v0, p1, Lcom/facebook/messaging/notify/ReadThreadNotification;->a:LX/0P1;

    move-object v0, v0

    .line 579041
    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 579042
    iget-object v2, p0, LX/3R8;->e:LX/3RA;

    const-string v3, "read_on_web"

    invoke-virtual {v2, v0, v3}, LX/3RA;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 579043
    iget-object v0, p0, LX/3R8;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 579044
    :goto_0
    return-void

    .line 579045
    :cond_0
    iget-object v0, p0, LX/3R8;->e:LX/3RA;

    .line 579046
    sget-object v1, LX/3RB;->h:Ljava/lang/String;

    invoke-static {v0, v1}, LX/3RA;->b(LX/3RA;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    sget-object p0, LX/3RB;->n:Ljava/lang/String;

    invoke-virtual {v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 579047
    invoke-static {v0, v1}, LX/3RA;->a$redex0(LX/3RA;Landroid/content/Intent;)V

    .line 579048
    goto :goto_0
.end method

.method public final b(Lcom/facebook/messaging/notify/NewMessageNotification;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 579049
    iget-object v0, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->g:LX/Dhq;

    .line 579050
    iget-boolean v2, v0, LX/Dhq;->a:Z

    move v0, v2

    .line 579051
    if-eqz v0, :cond_1

    .line 579052
    :cond_0
    :goto_0
    return-void

    .line 579053
    :cond_1
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v2, p0, LX/3R8;->f:LX/0c4;

    sget-object v3, LX/0cB;->d:Landroid/net/Uri;

    invoke-virtual {v2, v3}, LX/0c4;->a(Landroid/net/Uri;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v2, p0, LX/3R8;->f:LX/0c4;

    sget-object v3, LX/0cB;->f:Landroid/net/Uri;

    invoke-virtual {v2, v3}, LX/0c4;->a(Landroid/net/Uri;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 579054
    :cond_2
    iget-object v0, p0, LX/3R8;->c:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/3R8;->d:LX/15P;

    .line 579055
    iget-object v2, v0, LX/15P;->b:Landroid/app/Activity;

    move-object v0, v2

    .line 579056
    if-nez v0, :cond_0

    .line 579057
    :cond_3
    iget-object v2, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->g:LX/Dhq;

    .line 579058
    iget-boolean v0, v2, LX/Dhq;->f:Z

    move v0, v0

    .line 579059
    if-nez v0, :cond_0

    .line 579060
    iget-object v0, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    invoke-static {v0}, LX/2Mk;->o(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/3R8;->h:LX/2Mk;

    iget-object v3, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v0, v3}, LX/2Mk;->l(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 579061
    :cond_4
    iget-object v0, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 579062
    if-eqz v0, :cond_0

    .line 579063
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v4, p0, LX/3R8;->f:LX/0c4;

    .line 579064
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "peer://msg_notification_chathead/active_threads/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    move-object v0, v5

    .line 579065
    invoke-virtual {v4, v0}, LX/0c4;->a(Landroid/net/Uri;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/3R8;->g:LX/0pu;

    invoke-virtual {v0}, LX/0pu;->a()Z

    move-result v0

    if-nez v0, :cond_6

    :cond_5
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v3, p0, LX/3R8;->f:LX/0c4;

    sget-object v4, LX/0cB;->f:Landroid/net/Uri;

    invoke-virtual {v3, v4}, LX/0c4;->a(Landroid/net/Uri;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_6
    const/4 v0, 0x1

    .line 579066
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/messaging/notify/NewMessageNotification;->b()Z

    move-result v3

    if-nez v3, :cond_7

    if-eqz v0, :cond_0

    .line 579067
    :cond_7
    iget-object v0, p0, LX/3R8;->e:LX/3RA;

    new-instance v3, LX/6c2;

    invoke-direct {v3}, LX/6c2;-><init>()V

    iget-object v4, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 579068
    iput-object v4, v3, LX/6c2;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 579069
    move-object v3, v3

    .line 579070
    new-instance v4, Lcom/facebook/messaging/chatheads/ipc/ChatHeadMessageNotification;

    iget-object v5, v3, LX/6c2;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-boolean v6, v3, LX/6c2;->b:Z

    iget-boolean v7, v3, LX/6c2;->c:Z

    invoke-direct {v4, v5, v6, v7}, Lcom/facebook/messaging/chatheads/ipc/ChatHeadMessageNotification;-><init>(Lcom/facebook/messaging/model/messages/Message;ZZ)V

    move-object v3, v4

    .line 579071
    sget-object v4, LX/3RB;->a:Ljava/lang/String;

    invoke-static {v0, v4}, LX/3RA;->b(LX/3RA;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 579072
    sget-object v5, LX/3RB;->m:Ljava/lang/String;

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 579073
    invoke-static {v0, v4}, LX/3RA;->a$redex0(LX/3RA;Landroid/content/Intent;)V

    .line 579074
    const/4 v0, 0x1

    iput-boolean v0, v2, LX/Dhq;->f:Z

    .line 579075
    iget-object v0, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->f:Lcom/facebook/push/PushProperty;

    iget-object v0, v0, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    sget-object v2, LX/3B4;->SMS_READONLY_MODE:LX/3B4;

    if-ne v0, v2, :cond_0

    .line 579076
    iget-object v0, p0, LX/3R8;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/3Kr;->i:LX/0Tn;

    invoke-interface {v0, v2, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 579077
    iget-object v1, p0, LX/3R8;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/3Kr;->i:LX/0Tn;

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    sget-object v1, LX/3Kr;->h:LX/0Tn;

    iget-object v2, p0, LX/3R8;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto/16 :goto_0

    :cond_8
    move v0, v1

    .line 579078
    goto :goto_1
.end method
