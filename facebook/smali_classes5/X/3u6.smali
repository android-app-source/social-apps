.class public abstract LX/3u6;
.super LX/3u3;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/3uM;

.field public final c:Landroid/view/Window$Callback;

.field public final d:Landroid/view/Window$Callback;

.field public final e:LX/3u2;

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:LX/3u1;

.field private l:Landroid/view/MenuInflater;

.field private m:Ljava/lang/CharSequence;

.field public n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/3uM;LX/3u2;)V
    .locals 3

    .prologue
    .line 647455
    invoke-direct {p0}, LX/3u3;-><init>()V

    .line 647456
    iput-object p1, p0, LX/3u6;->a:Landroid/content/Context;

    .line 647457
    iput-object p2, p0, LX/3u6;->b:LX/3uM;

    .line 647458
    iput-object p3, p0, LX/3u6;->e:LX/3u2;

    .line 647459
    iget-object v0, p0, LX/3u6;->b:LX/3uM;

    invoke-interface {v0}, LX/3uM;->a()Landroid/view/Window$Callback;

    move-result-object v0

    .line 647460
    instance-of v1, v0, LX/3u5;

    if-eqz v1, :cond_0

    .line 647461
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "AppCompat has already installed itself into the Window"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 647462
    :cond_0
    instance-of v1, v0, LX/3uO;

    if-eqz v1, :cond_1

    .line 647463
    check-cast v0, LX/3uO;

    .line 647464
    iget-object v1, v0, LX/3uO;->c:Landroid/view/Window$Callback;

    move-object v1, v1

    .line 647465
    iput-object v1, p0, LX/3u6;->c:Landroid/view/Window$Callback;

    .line 647466
    :goto_0
    new-instance v1, LX/3u5;

    .line 647467
    iget-object v2, v0, LX/3uO;->c:Landroid/view/Window$Callback;

    move-object v2, v2

    .line 647468
    invoke-direct {v1, p0, v2}, LX/3u5;-><init>(LX/3u6;Landroid/view/Window$Callback;)V

    iput-object v1, p0, LX/3u6;->d:Landroid/view/Window$Callback;

    .line 647469
    iget-object v1, p0, LX/3u6;->d:Landroid/view/Window$Callback;

    .line 647470
    iget-object v2, v0, LX/3uO;->a:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 647471
    return-void

    .line 647472
    :cond_1
    iget-object v1, p0, LX/3u6;->b:LX/3uM;

    .line 647473
    new-instance v2, LX/3uO;

    invoke-interface {v1}, LX/3uM;->a()Landroid/view/Window$Callback;

    move-result-object p1

    invoke-direct {v2, v1, p1}, LX/3uO;-><init>(LX/3uM;Landroid/view/Window$Callback;)V

    .line 647474
    invoke-interface {v1, v2}, LX/3uM;->a(Landroid/view/Window$Callback;)V

    .line 647475
    move-object v1, v2

    .line 647476
    iput-object v0, p0, LX/3u6;->c:Landroid/view/Window$Callback;

    move-object v0, v1

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/Window;LX/3u2;)V
    .locals 1

    .prologue
    .line 647477
    new-instance v0, LX/3uP;

    invoke-direct {v0, p2}, LX/3uP;-><init>(Landroid/view/Window;)V

    invoke-direct {p0, p1, v0, p3}, LX/3u6;-><init>(Landroid/content/Context;LX/3uM;LX/3u2;)V

    .line 647478
    return-void
.end method


# virtual methods
.method public final a()LX/3u1;
    .locals 1

    .prologue
    .line 647479
    iget-boolean v0, p0, LX/3u6;->f:Z

    if-eqz v0, :cond_0

    .line 647480
    iget-object v0, p0, LX/3u6;->k:LX/3u1;

    if-nez v0, :cond_0

    .line 647481
    invoke-virtual {p0}, LX/3u6;->f()LX/3u1;

    move-result-object v0

    iput-object v0, p0, LX/3u6;->k:LX/3u1;

    .line 647482
    :cond_0
    iget-object v0, p0, LX/3u6;->k:LX/3u1;

    return-object v0
.end method

.method public abstract a(LX/3uG;)LX/3uV;
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 647483
    iget-object v0, p0, LX/3u6;->a:Landroid/content/Context;

    sget-object v1, LX/03r;->Theme:[I

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 647484
    const/16 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 647485
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 647486
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You need to use a Theme.AppCompat theme (or descendant) with this activity."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 647487
    :cond_0
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 647488
    iput-boolean v3, p0, LX/3u6;->f:Z

    .line 647489
    :cond_1
    const/16 v1, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 647490
    iput-boolean v3, p0, LX/3u6;->g:Z

    .line 647491
    :cond_2
    const/16 v1, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 647492
    iput-boolean v3, p0, LX/3u6;->h:Z

    .line 647493
    :cond_3
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, LX/3u6;->i:Z

    .line 647494
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, LX/3u6;->j:Z

    .line 647495
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 647496
    return-void
.end method

.method public abstract a(Ljava/lang/CharSequence;)V
.end method

.method public abstract a(ILandroid/view/KeyEvent;)Z
.end method

.method public abstract a(Landroid/view/KeyEvent;)Z
.end method

.method public final b()Landroid/view/MenuInflater;
    .locals 2

    .prologue
    .line 647431
    iget-object v0, p0, LX/3u6;->l:Landroid/view/MenuInflater;

    if-nez v0, :cond_0

    .line 647432
    new-instance v0, LX/3ui;

    invoke-virtual {p0}, LX/3u6;->h()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3ui;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/3u6;->l:Landroid/view/MenuInflater;

    .line 647433
    :cond_0
    iget-object v0, p0, LX/3u6;->l:Landroid/view/MenuInflater;

    return-object v0
.end method

.method public abstract c(I)Z
.end method

.method public abstract d(I)Z
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 647445
    iget-object v0, p0, LX/3u6;->b:LX/3uM;

    invoke-interface {v0}, LX/3uM;->a()Landroid/view/Window$Callback;

    move-result-object v0

    .line 647446
    instance-of v1, v0, LX/3uO;

    if-eqz v1, :cond_0

    .line 647447
    check-cast v0, LX/3uO;

    iget-object v1, p0, LX/3u6;->d:Landroid/view/Window$Callback;

    .line 647448
    iget-object v2, v0, LX/3uO;->a:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 647449
    iget-object v2, v0, LX/3uO;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 647450
    iget-object v2, v0, LX/3uO;->b:LX/3uM;

    iget-object v3, v0, LX/3uO;->c:Landroid/view/Window$Callback;

    invoke-interface {v2, v3}, LX/3uM;->a(Landroid/view/Window$Callback;)V

    .line 647451
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3u6;->n:Z

    .line 647452
    return-void

    .line 647453
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 647454
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Something went wrong, expecting AppCompatWindowCallbackWrapper but found "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " instead."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    goto :goto_0
.end method

.method public abstract f()LX/3u1;
.end method

.method public final h()Landroid/content/Context;
    .locals 2

    .prologue
    .line 647438
    const/4 v0, 0x0

    .line 647439
    invoke-virtual {p0}, LX/3u6;->a()LX/3u1;

    move-result-object v1

    .line 647440
    if-eqz v1, :cond_0

    .line 647441
    invoke-virtual {v1}, LX/3u1;->e()Landroid/content/Context;

    move-result-object v0

    .line 647442
    :cond_0
    if-nez v0, :cond_1

    .line 647443
    iget-object v0, p0, LX/3u6;->a:Landroid/content/Context;

    .line 647444
    :cond_1
    return-object v0
.end method

.method public final j()Landroid/view/Window$Callback;
    .locals 1

    .prologue
    .line 647437
    iget-object v0, p0, LX/3u6;->b:LX/3uM;

    invoke-interface {v0}, LX/3uM;->a()Landroid/view/Window$Callback;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 647434
    iget-object v0, p0, LX/3u6;->c:Landroid/view/Window$Callback;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 647435
    iget-object v0, p0, LX/3u6;->c:Landroid/view/Window$Callback;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    .line 647436
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/3u6;->m:Ljava/lang/CharSequence;

    goto :goto_0
.end method
