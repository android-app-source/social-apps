.class public LX/3U6;
.super LX/1Qj;
.source ""

# interfaces
.implements LX/1Pf;
.implements LX/3U7;
.implements LX/2km;
.implements LX/3U8;
.implements LX/2kn;
.implements LX/2ko;
.implements LX/3Tw;
.implements LX/3U9;
.implements LX/2kp;


# instance fields
.field private final n:LX/1SX;

.field private final o:LX/3UC;

.field private final p:LX/3UD;

.field private final q:LX/1PT;

.field private final r:LX/2kt;

.field private final s:LX/2ku;

.field private final t:LX/3Tv;

.field private final u:LX/2ja;

.field private final v:LX/2jY;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1PT;LX/1SX;Ljava/lang/Runnable;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;LX/3Tv;LX/2ja;LX/2jY;LX/1PY;LX/3UA;LX/3UB;LX/2kq;LX/2kr;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1PT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1SX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Runnable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/3Tv;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/2ja;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/2jY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/1PY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 585476
    invoke-direct {p0, p1, p4, p9}, LX/1Qj;-><init>(Landroid/content/Context;Ljava/lang/Runnable;LX/1PY;)V

    .line 585477
    iput-object p6, p0, LX/3U6;->t:LX/3Tv;

    .line 585478
    iput-object p2, p0, LX/3U6;->q:LX/1PT;

    .line 585479
    iput-object p3, p0, LX/3U6;->n:LX/1SX;

    .line 585480
    iget-object v0, p0, LX/3U6;->n:LX/1SX;

    if-eqz v0, :cond_0

    .line 585481
    iget-object v0, p0, LX/3U6;->n:LX/1SX;

    invoke-virtual {v0, p0}, LX/1SX;->a(LX/1Pf;)V

    .line 585482
    :cond_0
    invoke-static {p0}, LX/3UA;->a(LX/3Tw;)LX/3UC;

    move-result-object v0

    iput-object v0, p0, LX/3U6;->o:LX/3UC;

    .line 585483
    new-instance v0, LX/3UD;

    invoke-direct {v0, p0}, LX/3UD;-><init>(LX/3Tw;)V

    .line 585484
    move-object v0, v0

    .line 585485
    iput-object v0, p0, LX/3U6;->p:LX/3UD;

    .line 585486
    invoke-static {p5}, LX/2kq;->a(Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)LX/2kt;

    move-result-object v0

    iput-object v0, p0, LX/3U6;->r:LX/2kt;

    .line 585487
    invoke-static {p1}, LX/2kr;->a(Landroid/content/Context;)LX/2ku;

    move-result-object v0

    iput-object v0, p0, LX/3U6;->s:LX/2ku;

    .line 585488
    iput-object p7, p0, LX/3U6;->u:LX/2ja;

    .line 585489
    iput-object p8, p0, LX/3U6;->v:LX/2jY;

    .line 585490
    return-void
.end method


# virtual methods
.method public a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 585475
    iget-object v0, p0, LX/3U6;->s:LX/2ku;

    invoke-virtual {v0, p1}, LX/2ku;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 1

    .prologue
    .line 585473
    iget-object v0, p0, LX/3U6;->o:LX/3UC;

    invoke-virtual {v0, p1, p2}, LX/3UC;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 585474
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V
    .locals 1
    .param p3    # LX/Cfl;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 585471
    iget-object v0, p0, LX/3U6;->t:LX/3Tv;

    invoke-virtual {v0, p1, p2, p3}, LX/3Tv;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 585472
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/Cfl;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 585461
    iget-object v0, p0, LX/3U6;->t:LX/3Tv;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/3Tv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 585462
    return-void
.end method

.method public final b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/9uc;)V
    .locals 1

    .prologue
    .line 585469
    iget-object v0, p0, LX/3U6;->p:LX/3UD;

    invoke-virtual {v0, p1, p2}, LX/3UD;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/9uc;)V

    .line 585470
    return-void
.end method

.method public final c()LX/1PT;
    .locals 1

    .prologue
    .line 585468
    iget-object v0, p0, LX/3U6;->q:LX/1PT;

    return-object v0
.end method

.method public final e()LX/1SX;
    .locals 1

    .prologue
    .line 585467
    iget-object v0, p0, LX/3U6;->n:LX/1SX;

    return-object v0
.end method

.method public final n()LX/2ja;
    .locals 1

    .prologue
    .line 585466
    iget-object v0, p0, LX/3U6;->u:LX/2ja;

    return-object v0
.end method

.method public final t()LX/2jY;
    .locals 1

    .prologue
    .line 585465
    iget-object v0, p0, LX/3U6;->v:LX/2jY;

    return-object v0
.end method

.method public final u()LX/0o8;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 585464
    iget-object v0, p0, LX/3U6;->t:LX/3Tv;

    invoke-virtual {v0}, LX/3Tv;->u()LX/0o8;

    move-result-object v0

    return-object v0
.end method

.method public final v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 585463
    iget-object v0, p0, LX/3U6;->r:LX/2kt;

    invoke-virtual {v0}, LX/2kt;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v0

    return-object v0
.end method
