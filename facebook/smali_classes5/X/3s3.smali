.class public final LX/3s3;
.super LX/1u9;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 643745
    invoke-direct {p0}, LX/1u9;-><init>()V

    return-void
.end method


# virtual methods
.method public final A(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 643743
    invoke-virtual {p1}, Landroid/view/View;->getTranslationZ()F

    move-result v0

    move v0, v0

    .line 643744
    return v0
.end method

.method public final E(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 643741
    invoke-virtual {p1}, Landroid/view/View;->stopNestedScroll()V

    .line 643742
    return-void
.end method

.method public final G(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 643739
    invoke-virtual {p1}, Landroid/view/View;->getZ()F

    move-result v0

    move v0, v0

    .line 643740
    return v0
.end method

.method public final a(Landroid/view/View;LX/3sc;)LX/3sc;
    .locals 1

    .prologue
    .line 643731
    instance-of v0, p2, LX/3sd;

    if-eqz v0, :cond_0

    move-object v0, p2

    .line 643732
    check-cast v0, LX/3sd;

    .line 643733
    iget-object p0, v0, LX/3sd;->a:Landroid/view/WindowInsets;

    move-object v0, p0

    .line 643734
    invoke-virtual {p1, v0}, Landroid/view/View;->onApplyWindowInsets(Landroid/view/WindowInsets;)Landroid/view/WindowInsets;

    move-result-object p0

    .line 643735
    if-eq p0, v0, :cond_0

    .line 643736
    new-instance p2, LX/3sd;

    invoke-direct {p2, p0}, LX/3sd;-><init>(Landroid/view/WindowInsets;)V

    .line 643737
    :cond_0
    move-object v0, p2

    .line 643738
    return-object v0
.end method

.method public final a(Landroid/view/View;LX/1uR;)V
    .locals 0

    .prologue
    .line 643729
    new-instance p0, LX/3s6;

    invoke-direct {p0, p2}, LX/3s6;-><init>(LX/1uR;)V

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnApplyWindowInsetsListener(Landroid/view/View$OnApplyWindowInsetsListener;)V

    .line 643730
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/content/res/ColorStateList;)V
    .locals 0

    .prologue
    .line 643711
    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 643712
    return-void
.end method

.method public final b(Landroid/view/View;LX/3sc;)LX/3sc;
    .locals 1

    .prologue
    .line 643721
    instance-of v0, p2, LX/3sd;

    if-eqz v0, :cond_0

    move-object v0, p2

    .line 643722
    check-cast v0, LX/3sd;

    .line 643723
    iget-object p0, v0, LX/3sd;->a:Landroid/view/WindowInsets;

    move-object v0, p0

    .line 643724
    invoke-virtual {p1, v0}, Landroid/view/View;->dispatchApplyWindowInsets(Landroid/view/WindowInsets;)Landroid/view/WindowInsets;

    move-result-object p0

    .line 643725
    if-eq p0, v0, :cond_0

    .line 643726
    new-instance p2, LX/3sd;

    invoke-direct {p2, p0}, LX/3sd;-><init>(Landroid/view/WindowInsets;)V

    .line 643727
    :cond_0
    move-object v0, p2

    .line 643728
    return-object v0
.end method

.method public final c(Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 643719
    invoke-virtual {p1, p2}, Landroid/view/View;->setNestedScrollingEnabled(Z)V

    .line 643720
    return-void
.end method

.method public final f(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 643717
    invoke-virtual {p1, p2}, Landroid/view/View;->setElevation(F)V

    .line 643718
    return-void
.end method

.method public final y(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 643715
    invoke-virtual {p1}, Landroid/view/View;->requestApplyInsets()V

    .line 643716
    return-void
.end method

.method public final z(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 643713
    invoke-virtual {p1}, Landroid/view/View;->getElevation()F

    move-result v0

    move v0, v0

    .line 643714
    return v0
.end method
