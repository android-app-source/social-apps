.class public LX/3fA;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0lC;

.field private final c:Lcom/facebook/performancelogger/PerformanceLogger;

.field private final d:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 621495
    const-class v0, LX/3fA;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3fA;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0lC;Lcom/facebook/performancelogger/PerformanceLogger;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 621528
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 621529
    iput-object p1, p0, LX/3fA;->b:LX/0lC;

    .line 621530
    iput-object p2, p0, LX/3fA;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 621531
    iput-object p3, p0, LX/3fA;->d:LX/03V;

    .line 621532
    return-void
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/ipc/pages/PageInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 621523
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/pages/PageInfo;

    .line 621524
    iget-wide v4, v0, Lcom/facebook/ipc/pages/PageInfo;->pageId:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/facebook/ipc/pages/PageInfo;->accessToken:Ljava/lang/String;

    invoke-static {v4}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 621525
    :cond_0
    iget-object v4, p0, LX/3fA;->d:LX/03V;

    sget-object v5, LX/3fA;->a:Ljava/lang/String;

    const-string v6, "invalid page info found, id=%d, accessToken=%s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    iget-wide v8, v0, Lcom/facebook/ipc/pages/PageInfo;->pageId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v2

    const/4 v8, 0x1

    iget-object v0, v0, Lcom/facebook/ipc/pages/PageInfo;->accessToken:Ljava/lang/String;

    aput-object v0, v7, v8

    invoke-static {v6, v7}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 621526
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 621527
    :cond_2
    return-void
.end method

.method public static b(LX/0QB;)LX/3fA;
    .locals 4

    .prologue
    .line 621521
    new-instance v3, LX/3fA;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v0

    check-cast v0, LX/0lC;

    invoke-static {p0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v1

    check-cast v1, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-direct {v3, v0, v1, v2}, LX/3fA;-><init>(LX/0lC;Lcom/facebook/performancelogger/PerformanceLogger;LX/03V;)V

    .line 621522
    return-object v3
.end method


# virtual methods
.method public final a(LX/0lF;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lF;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/ipc/pages/PageInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const v4, 0x130015

    .line 621511
    iget-object v0, p0, LX/3fA;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v1, "DeserializeAllPages"

    invoke-interface {v0, v4, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 621512
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p1}, LX/0lF;->e()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 621513
    invoke-virtual {p1}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 621514
    sget-object v3, LX/461;->g:LX/461;

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0lp;->b(Ljava/lang/String;)LX/15w;

    move-result-object v0

    .line 621515
    iget-object v3, p0, LX/3fA;->b:LX/0lC;

    invoke-virtual {v0, v3}, LX/15w;->a(LX/0lD;)V

    .line 621516
    const-class v3, Lcom/facebook/ipc/pages/PageInfo;

    invoke-virtual {v0, v3}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/pages/PageInfo;

    .line 621517
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 621518
    :cond_0
    iget-object v0, p0, LX/3fA;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v2, "DeserializeAllPages"

    invoke-interface {v0, v4, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 621519
    invoke-direct {p0, v1}, LX/3fA;->a(Ljava/util/ArrayList;)V

    .line 621520
    return-object v1
.end method

.method public final c(LX/0lF;)LX/0lF;
    .locals 9

    .prologue
    const v8, 0x130014

    .line 621496
    iget-object v0, p0, LX/3fA;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v1, "DeserializePagesAttributes"

    invoke-interface {v0, v8, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 621497
    new-instance v1, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/162;-><init>(LX/0mC;)V

    .line 621498
    const-string v0, "data"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 621499
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 621500
    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 621501
    new-instance v3, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 621502
    const-string v4, "page_id"

    const-string v5, "id"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 621503
    const-string v4, "name"

    const-string v5, "name"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 621504
    const-string v4, "access_token"

    const-string v5, "access_token"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 621505
    const-string v4, "perms"

    const-string v5, "perms"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 621506
    const-string v4, "page_url"

    const-string v5, "link"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 621507
    const-string v4, "square_pic_url"

    const-string v5, "picture"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const-string v5, "data"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const-string v5, "url"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 621508
    invoke-virtual {v1, v3}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_0

    .line 621509
    :cond_0
    iget-object v0, p0, LX/3fA;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v2, "DeserializePagesAttributes"

    invoke-interface {v0, v8, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 621510
    return-object v1
.end method
