.class public LX/3eU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3eU;


# instance fields
.field public final a:LX/0Xl;


# direct methods
.method public constructor <init>(LX/0Xl;)V
    .locals 0
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 620242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 620243
    iput-object p1, p0, LX/3eU;->a:LX/0Xl;

    .line 620244
    return-void
.end method

.method public static a(LX/0QB;)LX/3eU;
    .locals 4

    .prologue
    .line 620245
    sget-object v0, LX/3eU;->b:LX/3eU;

    if-nez v0, :cond_1

    .line 620246
    const-class v1, LX/3eU;

    monitor-enter v1

    .line 620247
    :try_start_0
    sget-object v0, LX/3eU;->b:LX/3eU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 620248
    if-eqz v2, :cond_0

    .line 620249
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 620250
    new-instance p0, LX/3eU;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v3

    check-cast v3, LX/0Xl;

    invoke-direct {p0, v3}, LX/3eU;-><init>(LX/0Xl;)V

    .line 620251
    move-object v0, p0

    .line 620252
    sput-object v0, LX/3eU;->b:LX/3eU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 620253
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 620254
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 620255
    :cond_1
    sget-object v0, LX/3eU;->b:LX/3eU;

    return-object v0

    .line 620256
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 620257
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 620258
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.orca.stickers.STICKER_CONFIG_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 620259
    iget-object v1, p0, LX/3eU;->a:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 620260
    return-void
.end method
