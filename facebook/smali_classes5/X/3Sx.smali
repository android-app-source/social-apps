.class public LX/3Sx;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;",
        "Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:LX/1rt;

.field private final c:LX/1rU;


# direct methods
.method public constructor <init>(LX/1rt;LX/0sO;LX/1rU;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 583290
    invoke-direct {p0, p2}, LX/0ro;-><init>(LX/0sO;)V

    .line 583291
    iput-object p1, p0, LX/3Sx;->b:LX/1rt;

    .line 583292
    iput-object p3, p0, LX/3Sx;->c:LX/1rU;

    .line 583293
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 583255
    check-cast p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    const/4 v1, 0x0

    .line 583256
    invoke-virtual {p3}, LX/15w;->a()LX/0lD;

    move-result-object v0

    check-cast v0, LX/0lC;

    .line 583257
    iget-object v2, p0, LX/3Sx;->c:LX/1rU;

    invoke-virtual {v2}, LX/1rU;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 583258
    const-class v2, Lcom/facebook/notifications/model/NewNotificationStories;

    invoke-virtual {v0, p3, v2}, LX/0lD;->a(LX/15w;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/notifications/model/NewNotificationStories;

    .line 583259
    new-instance v0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;

    invoke-virtual {p1}, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->f()J

    move-result-wide v3

    sget-object v5, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct/range {v0 .. v7}, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;-><init>(Lcom/facebook/notifications/model/NotificationStories;Lcom/facebook/notifications/model/NewNotificationStories;JLX/0ta;J)V

    .line 583260
    :goto_0
    return-object v0

    .line 583261
    :cond_0
    const-class v2, Lcom/facebook/notifications/model/NotificationStories;

    invoke-virtual {v0, p3, v2}, LX/0lD;->a(LX/15w;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/notifications/model/NotificationStories;

    .line 583262
    new-instance v2, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;

    invoke-virtual {p1}, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->f()J

    move-result-wide v5

    sget-object v7, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    move-object v4, v1

    invoke-direct/range {v2 .. v9}, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;-><init>(Lcom/facebook/notifications/model/NotificationStories;Lcom/facebook/notifications/model/NewNotificationStories;JLX/0ta;J)V

    move-object v0, v2

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 583263
    const/4 v0, 0x2

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 583264
    check-cast p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    .line 583265
    iget-object v0, p0, LX/3Sx;->b:LX/1rt;

    .line 583266
    if-eqz p1, :cond_1

    .line 583267
    iget-object v1, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->e:Ljava/lang/String;

    move-object v1, v1

    .line 583268
    if-eqz v1, :cond_1

    const/4 v1, 0x1

    .line 583269
    :goto_0
    invoke-virtual {v0, p1, v1}, LX/1rt;->a(Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;Z)LX/0gW;

    move-result-object v2

    .line 583270
    if-eqz p1, :cond_0

    .line 583271
    const-string v3, "first_notification_stories"

    .line 583272
    iget p0, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->b:I

    move p0, p0

    .line 583273
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, v3, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 583274
    if-eqz v1, :cond_0

    .line 583275
    const-string v1, "last_notification_stories"

    .line 583276
    iget v3, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->c:I

    move v3, v3

    .line 583277
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 583278
    :cond_0
    move-object v0, v2

    .line 583279
    return-object v0

    .line 583280
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final i(Ljava/lang/Object;)Lcom/facebook/http/interfaces/RequestPriority;
    .locals 2

    .prologue
    .line 583281
    check-cast p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    .line 583282
    iget-object v0, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->g:Ljava/lang/String;

    move-object v0, v0

    .line 583283
    invoke-static {v0}, LX/2ub;->fromString(Ljava/lang/String;)LX/2ub;

    move-result-object v0

    .line 583284
    sget-object v1, LX/2ub;->PULL_TO_REFRESH:LX/2ub;

    invoke-virtual {v1, v0}, LX/2ub;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 583285
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 583286
    :goto_0
    return-object v0

    .line 583287
    :cond_0
    sget-object v1, LX/2ub;->BACKGROUND:LX/2ub;

    invoke-virtual {v1, v0}, LX/2ub;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 583288
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0

    .line 583289
    :cond_1
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0
.end method
