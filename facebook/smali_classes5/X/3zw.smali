.class public LX/3zw;
.super LX/2D7;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2D7",
        "<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/2D7;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 662790
    invoke-direct {p0}, LX/2D7;-><init>()V

    .line 662791
    return-void
.end method

.method public static declared-synchronized a()LX/2D7;
    .locals 2

    .prologue
    .line 662792
    const-class v1, LX/3zw;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/3zw;->b:LX/2D7;

    if-nez v0, :cond_0

    .line 662793
    new-instance v0, LX/3zw;

    invoke-direct {v0}, LX/3zw;-><init>()V

    sput-object v0, LX/3zw;->b:LX/2D7;

    .line 662794
    :cond_0
    sget-object v0, LX/3zw;->b:LX/2D7;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 662795
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final b(Ljava/lang/Object;)LX/2DZ;
    .locals 4

    .prologue
    .line 662796
    check-cast p1, Ljava/io/File;

    .line 662797
    :try_start_0
    new-instance v0, LX/3zu;

    invoke-direct {v0, p0, p1}, LX/3zu;-><init>(LX/2D7;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 662798
    :catch_0
    move-exception v0

    .line 662799
    new-instance v1, LX/3zv;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected error, failed to create file: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/3zv;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
