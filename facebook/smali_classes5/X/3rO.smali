.class public final LX/3rO;
.super Landroid/view/View$AccessibilityDelegate;
.source ""


# instance fields
.field public final synthetic a:LX/39C;


# direct methods
.method public constructor <init>(LX/39C;)V
    .locals 0

    .prologue
    .line 643012
    iput-object p1, p0, LX/3rO;->a:LX/39C;

    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method public final dispatchPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .prologue
    .line 643011
    iget-object v0, p0, LX/3rO;->a:LX/39C;

    invoke-interface {v0, p1, p2}, LX/39C;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public final onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 643009
    iget-object v0, p0, LX/3rO;->a:LX/39C;

    invoke-interface {v0, p1, p2}, LX/39C;->b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 643010
    return-void
.end method

.method public final onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 643007
    iget-object v0, p0, LX/3rO;->a:LX/39C;

    invoke-interface {v0, p1, p2}, LX/39C;->a(Landroid/view/View;Ljava/lang/Object;)V

    .line 643008
    return-void
.end method

.method public final onPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 643005
    iget-object v0, p0, LX/3rO;->a:LX/39C;

    invoke-interface {v0, p1, p2}, LX/39C;->c(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 643006
    return-void
.end method

.method public final onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .prologue
    .line 643000
    iget-object v0, p0, LX/3rO;->a:LX/39C;

    invoke-interface {v0, p1, p2, p3}, LX/39C;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public final sendAccessibilityEvent(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 643003
    iget-object v0, p0, LX/3rO;->a:LX/39C;

    invoke-interface {v0, p1, p2}, LX/39C;->a(Landroid/view/View;I)V

    .line 643004
    return-void
.end method

.method public final sendAccessibilityEventUnchecked(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 643001
    iget-object v0, p0, LX/3rO;->a:LX/39C;

    invoke-interface {v0, p1, p2}, LX/39C;->d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 643002
    return-void
.end method
