.class public LX/3fy;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final h:Ljava/lang/Object;


# instance fields
.field public final b:LX/3fz;

.field public final c:LX/3fX;

.field public final d:LX/3fq;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Jp;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3fg;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6Nd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 623927
    const-class v0, LX/3fy;

    sput-object v0, LX/3fy;->a:Ljava/lang/Class;

    .line 623928
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/3fy;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/3fz;LX/3fX;LX/3fq;LX/0Or;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3fz;",
            "LX/3fX;",
            "LX/3fq;",
            "LX/0Or",
            "<",
            "LX/2Jp;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3fg;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6Nd;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 623919
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 623920
    iput-object p1, p0, LX/3fy;->b:LX/3fz;

    .line 623921
    iput-object p2, p0, LX/3fy;->c:LX/3fX;

    .line 623922
    iput-object p3, p0, LX/3fy;->d:LX/3fq;

    .line 623923
    iput-object p4, p0, LX/3fy;->e:LX/0Or;

    .line 623924
    iput-object p5, p0, LX/3fy;->f:LX/0Ot;

    .line 623925
    iput-object p6, p0, LX/3fy;->g:LX/0Ot;

    .line 623926
    return-void
.end method

.method public static a(LX/0QB;)LX/3fy;
    .locals 14

    .prologue
    .line 623890
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 623891
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 623892
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 623893
    if-nez v1, :cond_0

    .line 623894
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 623895
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 623896
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 623897
    sget-object v1, LX/3fy;->h:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 623898
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 623899
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 623900
    :cond_1
    if-nez v1, :cond_4

    .line 623901
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 623902
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 623903
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 623904
    new-instance v7, LX/3fy;

    invoke-static {v0}, LX/3fz;->a(LX/0QB;)LX/3fz;

    move-result-object v8

    check-cast v8, LX/3fz;

    invoke-static {v0}, LX/3fX;->a(LX/0QB;)LX/3fX;

    move-result-object v9

    check-cast v9, LX/3fX;

    invoke-static {v0}, LX/3fq;->a(LX/0QB;)LX/3fq;

    move-result-object v10

    check-cast v10, LX/3fq;

    const/16 v11, 0x438

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const/16 v12, 0x416

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x1a18

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct/range {v7 .. v13}, LX/3fy;-><init>(LX/3fz;LX/3fX;LX/3fq;LX/0Or;LX/0Ot;LX/0Ot;)V

    .line 623905
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 623906
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 623907
    if-nez v1, :cond_2

    .line 623908
    sget-object v0, LX/3fy;->h:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3fy;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 623909
    :goto_1
    if-eqz v0, :cond_3

    .line 623910
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 623911
    :goto_3
    check-cast v0, LX/3fy;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 623912
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 623913
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 623914
    :catchall_1
    move-exception v0

    .line 623915
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 623916
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 623917
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 623918
    :cond_2
    :try_start_8
    sget-object v0, LX/3fy;->h:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3fy;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method
