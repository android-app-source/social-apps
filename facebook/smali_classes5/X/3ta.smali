.class public final LX/3ta;
.super LX/3t3;
.source ""


# instance fields
.field public final synthetic a:LX/1cr;


# direct methods
.method public constructor <init>(LX/1cr;)V
    .locals 0

    .prologue
    .line 645536
    iput-object p1, p0, LX/3ta;->a:LX/1cr;

    invoke-direct {p0}, LX/3t3;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)LX/3sp;
    .locals 3

    .prologue
    .line 645537
    iget-object v0, p0, LX/3ta;->a:LX/1cr;

    .line 645538
    packed-switch p1, :pswitch_data_0

    .line 645539
    invoke-static {v0, p1}, LX/1cr;->e(LX/1cr;I)LX/3sp;

    move-result-object v1

    :goto_0
    move-object v0, v1

    .line 645540
    return-object v0

    .line 645541
    :pswitch_0
    iget-object v1, v0, LX/1cr;->h:Landroid/view/View;

    invoke-static {v1}, LX/3sp;->a(Landroid/view/View;)LX/3sp;

    move-result-object v2

    .line 645542
    iget-object v1, v0, LX/1cr;->h:Landroid/view/View;

    invoke-static {v1, v2}, LX/0vv;->a(Landroid/view/View;LX/3sp;)V

    .line 645543
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 645544
    invoke-virtual {v0, v1}, LX/1cr;->a(Ljava/util/List;)V

    .line 645545
    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 645546
    iget-object p1, v0, LX/1cr;->h:Landroid/view/View;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, p1, v1}, LX/3sp;->c(Landroid/view/View;I)V

    goto :goto_1

    .line 645547
    :cond_0
    move-object v1, v2

    .line 645548
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(IILandroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 645549
    iget-object v0, p0, LX/3ta;->a:LX/1cr;

    .line 645550
    packed-switch p1, :pswitch_data_0

    .line 645551
    sparse-switch p2, :sswitch_data_0

    .line 645552
    invoke-virtual {v0, p1, p2}, LX/1cr;->b(II)Z

    move-result p0

    :goto_0
    move p0, p0

    .line 645553
    :goto_1
    move v0, p0

    .line 645554
    return v0

    .line 645555
    :pswitch_0
    iget-object p0, v0, LX/1cr;->h:Landroid/view/View;

    invoke-static {p0, p2, p3}, LX/0vv;->a(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result p0

    move p0, p0

    .line 645556
    goto :goto_1

    .line 645557
    :sswitch_0
    sparse-switch p2, :sswitch_data_1

    .line 645558
    const/4 p0, 0x0

    :goto_2
    move p0, p0

    .line 645559
    goto :goto_0

    .line 645560
    :sswitch_1
    const/4 p0, 0x0

    .line 645561
    iget-object p2, v0, LX/1cr;->g:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result p2

    if-eqz p2, :cond_0

    iget-object p2, v0, LX/1cr;->g:Landroid/view/accessibility/AccessibilityManager;

    invoke-static {p2}, LX/1d8;->b(Landroid/view/accessibility/AccessibilityManager;)Z

    move-result p2

    if-nez p2, :cond_1

    .line 645562
    :cond_0
    :goto_3
    move p0, p0

    .line 645563
    goto :goto_2

    .line 645564
    :sswitch_2
    invoke-static {v0, p1}, LX/1cr;->f(LX/1cr;I)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 645565
    const/high16 p0, -0x80000000

    iput p0, v0, LX/1cr;->j:I

    .line 645566
    iget-object p0, v0, LX/1cr;->h:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 645567
    const/high16 p0, 0x10000

    invoke-virtual {v0, p1, p0}, LX/1cr;->a(II)Z

    .line 645568
    const/4 p0, 0x1

    .line 645569
    :goto_4
    move p0, p0

    .line 645570
    goto :goto_2

    .line 645571
    :cond_1
    invoke-static {v0, p1}, LX/1cr;->f(LX/1cr;I)Z

    move-result p2

    if-nez p2, :cond_0

    .line 645572
    iput p1, v0, LX/1cr;->j:I

    .line 645573
    iget-object p0, v0, LX/1cr;->h:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 645574
    const p0, 0x8000

    invoke-virtual {v0, p1, p0}, LX/1cr;->a(II)Z

    .line 645575
    const/4 p0, 0x1

    goto :goto_3

    :cond_2
    const/4 p0, 0x0

    goto :goto_4

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x40 -> :sswitch_0
        0x80 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x40 -> :sswitch_1
        0x80 -> :sswitch_2
    .end sparse-switch
.end method
