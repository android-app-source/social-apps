.class public LX/4mm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Landroid/net/Uri;

.field public final e:Lcom/facebook/fbservice/service/ServiceException;

.field public final f:Landroid/content/DialogInterface$OnClickListener;

.field public final g:Landroid/content/DialogInterface$OnCancelListener;

.field public final h:Landroid/app/Activity;

.field public final i:Landroid/support/v4/app/DialogFragment;

.field public final j:Z

.field public final k:Z


# direct methods
.method public constructor <init>(LX/4mn;)V
    .locals 1

    .prologue
    .line 806264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 806265
    iget-object v0, p1, LX/4mn;->b:Ljava/lang/String;

    move-object v0, v0

    .line 806266
    iput-object v0, p0, LX/4mm;->a:Ljava/lang/String;

    .line 806267
    iget-object v0, p1, LX/4mn;->c:Ljava/lang/String;

    move-object v0, v0

    .line 806268
    iput-object v0, p0, LX/4mm;->b:Ljava/lang/String;

    .line 806269
    iget-object v0, p1, LX/4mn;->d:Ljava/lang/String;

    move-object v0, v0

    .line 806270
    iput-object v0, p0, LX/4mm;->c:Ljava/lang/String;

    .line 806271
    iget-object v0, p1, LX/4mn;->e:Landroid/net/Uri;

    move-object v0, v0

    .line 806272
    iput-object v0, p0, LX/4mm;->d:Landroid/net/Uri;

    .line 806273
    iget-object v0, p1, LX/4mn;->f:Lcom/facebook/fbservice/service/ServiceException;

    move-object v0, v0

    .line 806274
    iput-object v0, p0, LX/4mm;->e:Lcom/facebook/fbservice/service/ServiceException;

    .line 806275
    iget-object v0, p1, LX/4mn;->g:Landroid/content/DialogInterface$OnClickListener;

    move-object v0, v0

    .line 806276
    iput-object v0, p0, LX/4mm;->f:Landroid/content/DialogInterface$OnClickListener;

    .line 806277
    iget-object v0, p1, LX/4mn;->h:Landroid/content/DialogInterface$OnCancelListener;

    move-object v0, v0

    .line 806278
    iput-object v0, p0, LX/4mm;->g:Landroid/content/DialogInterface$OnCancelListener;

    .line 806279
    iget-object v0, p1, LX/4mn;->i:Landroid/app/Activity;

    move-object v0, v0

    .line 806280
    iput-object v0, p0, LX/4mm;->h:Landroid/app/Activity;

    .line 806281
    iget-object v0, p1, LX/4mn;->j:Landroid/support/v4/app/DialogFragment;

    move-object v0, v0

    .line 806282
    iput-object v0, p0, LX/4mm;->i:Landroid/support/v4/app/DialogFragment;

    .line 806283
    iget-boolean v0, p1, LX/4mn;->k:Z

    move v0, v0

    .line 806284
    iput-boolean v0, p0, LX/4mm;->j:Z

    .line 806285
    iget-boolean v0, p1, LX/4mn;->l:Z

    move v0, v0

    .line 806286
    iput-boolean v0, p0, LX/4mm;->k:Z

    .line 806287
    return-void
.end method

.method public static a(Landroid/content/Context;)LX/4mn;
    .locals 2

    .prologue
    .line 806289
    new-instance v0, LX/4mn;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4mn;-><init>(Landroid/content/res/Resources;)V

    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;)LX/4mn;
    .locals 1

    .prologue
    .line 806288
    new-instance v0, LX/4mn;

    invoke-direct {v0, p0}, LX/4mn;-><init>(Landroid/content/res/Resources;)V

    return-object v0
.end method
