.class public LX/4LM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 682910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 682911
    const/4 v0, 0x0

    .line 682912
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_9

    .line 682913
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 682914
    :goto_0
    return v1

    .line 682915
    :cond_0
    const-string v10, "entity_type"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 682916
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    move-result-object v4

    move-object v7, v4

    move v4, v2

    .line 682917
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_5

    .line 682918
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 682919
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 682920
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 682921
    const-string v10, "entity"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 682922
    invoke-static {p0, p1}, LX/2ap;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 682923
    :cond_2
    const-string v10, "length"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 682924
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v6, v3

    move v3, v2

    goto :goto_1

    .line 682925
    :cond_3
    const-string v10, "offset"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 682926
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 682927
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 682928
    :cond_5
    const/4 v9, 0x4

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 682929
    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 682930
    if-eqz v4, :cond_6

    .line 682931
    invoke-virtual {p1, v2, v7}, LX/186;->a(ILjava/lang/Enum;)V

    .line 682932
    :cond_6
    if-eqz v3, :cond_7

    .line 682933
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v6, v1}, LX/186;->a(III)V

    .line 682934
    :cond_7
    if-eqz v0, :cond_8

    .line 682935
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v5, v1}, LX/186;->a(III)V

    .line 682936
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_9
    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move-object v7, v0

    move v8, v1

    move v0, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 682937
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 682938
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 682939
    if-eqz v0, :cond_0

    .line 682940
    const-string v1, "entity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682941
    invoke-static {p0, v0, p2, p3}, LX/2ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 682942
    :cond_0
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 682943
    if-eqz v0, :cond_1

    .line 682944
    const-string v0, "entity_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682945
    const-class v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 682946
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 682947
    if-eqz v0, :cond_2

    .line 682948
    const-string v1, "length"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682949
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 682950
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 682951
    if-eqz v0, :cond_3

    .line 682952
    const-string v1, "offset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682953
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 682954
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 682955
    return-void
.end method
