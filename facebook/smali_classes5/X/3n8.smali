.class public final LX/3n8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/http/client/ResponseHandler",
        "<",
        "LX/1Bx;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Z

.field private final e:LX/1Bt;


# direct methods
.method private constructor <init>(Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;Ljava/lang/String;Ljava/lang/String;ZLX/1Bt;)V
    .locals 0

    .prologue
    .line 637791
    iput-object p1, p0, LX/3n8;->a:Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 637792
    iput-object p2, p0, LX/3n8;->b:Ljava/lang/String;

    .line 637793
    iput-object p3, p0, LX/3n8;->c:Ljava/lang/String;

    .line 637794
    iput-boolean p4, p0, LX/3n8;->d:Z

    .line 637795
    iput-object p5, p0, LX/3n8;->e:LX/1Bt;

    .line 637796
    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;Ljava/lang/String;Ljava/lang/String;ZLX/1Bt;B)V
    .locals 0

    .prologue
    .line 637797
    invoke-direct/range {p0 .. p5}, LX/3n8;-><init>(Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;Ljava/lang/String;Ljava/lang/String;ZLX/1Bt;)V

    return-void
.end method


# virtual methods
.method public final handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 13
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 637798
    const/4 v1, 0x0

    const/16 v8, 0xc8

    .line 637799
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    .line 637800
    if-nez v0, :cond_0

    .line 637801
    new-instance v0, Lorg/apache/http/client/ClientProtocolException;

    const-string v1, "Invalid HttpResponse"

    invoke-direct {v0, v1}, Lorg/apache/http/client/ClientProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 637802
    :cond_0
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v2

    const/4 v5, 0x0

    .line 637803
    new-instance v3, LX/7i0;

    invoke-direct {v3}, LX/7i0;-><init>()V

    .line 637804
    if-nez v2, :cond_e

    .line 637805
    :cond_1
    :goto_0
    move-object v5, v3

    .line 637806
    iget-object v2, p0, LX/3n8;->a:Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;

    iget-boolean v3, p0, LX/3n8;->d:Z

    iget-object v4, p0, LX/3n8;->e:LX/1Bt;

    iget-object v6, p0, LX/3n8;->c:Ljava/lang/String;

    iget-object v7, v5, LX/7i0;->d:Ljava/util/List;

    invoke-virtual {v2, v3, v4, v6, v7}, LX/1Bp;->a(ZLX/1Bt;Ljava/lang/String;Ljava/util/List;)V

    .line 637807
    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    .line 637808
    invoke-static {v2}, LX/3C8;->a(I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 637809
    iget-object v1, v5, LX/7i0;->e:Ljava/lang/String;

    .line 637810
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 637811
    new-instance v0, LX/1Bx;

    invoke-direct {v0, v1, v2}, LX/1Bx;-><init>(Ljava/lang/String;I)V

    .line 637812
    :cond_2
    :goto_1
    return-object v0

    .line 637813
    :cond_3
    new-instance v0, Lorg/apache/http/client/ClientProtocolException;

    const-string v1, "Redirect without location"

    invoke-direct {v0, v1}, Lorg/apache/http/client/ClientProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 637814
    :cond_4
    if-eq v2, v8, :cond_5

    .line 637815
    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    .line 637816
    new-instance v0, LX/1Bx;

    invoke-direct {v0, v2}, LX/1Bx;-><init>(I)V

    .line 637817
    sget-object v1, LX/1Bu;->ERROR_RESPONSE:LX/1Bu;

    iput-object v1, v0, LX/1Bx;->f:LX/1Bu;

    goto :goto_1

    .line 637818
    :cond_5
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 637819
    if-nez v0, :cond_6

    .line 637820
    new-instance v0, Lorg/apache/http/client/ClientProtocolException;

    const-string v1, "Missing HTTP entity"

    invoke-direct {v0, v1}, Lorg/apache/http/client/ClientProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 637821
    :cond_6
    :try_start_0
    iget-boolean v2, p0, LX/3n8;->d:Z

    if-nez v2, :cond_d

    .line 637822
    iget-object v2, p0, LX/3n8;->e:LX/1Bt;

    if-nez v2, :cond_8

    .line 637823
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "mPrefetchRequest can\'t be null for non-click request"

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 637824
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_7

    .line 637825
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_7
    throw v0

    .line 637826
    :cond_8
    :try_start_1
    iget-object v2, p0, LX/3n8;->e:LX/1Bt;

    iget-boolean v2, v2, LX/1Bt;->c:Z

    if-eqz v2, :cond_9

    iget-object v2, v5, LX/7i0;->a:Ljava/lang/String;

    invoke-static {v2}, LX/3C8;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_9
    iget-object v2, p0, LX/3n8;->e:LX/1Bt;

    iget-boolean v2, v2, LX/1Bt;->c:Z

    if-nez v2, :cond_b

    iget-object v2, v5, LX/7i0;->a:Ljava/lang/String;

    invoke-static {v2}, LX/3C8;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 637827
    :cond_a
    new-instance v0, LX/1Bx;

    const/16 v2, 0xc8

    invoke-direct {v0, v2}, LX/1Bx;-><init>(I)V

    .line 637828
    iput-object v5, v0, LX/1Bx;->c:LX/7i0;

    .line 637829
    sget-object v2, LX/1Bu;->NOT_HTML:LX/1Bu;

    iput-object v2, v0, LX/1Bx;->f:LX/1Bu;

    goto :goto_1

    .line 637830
    :cond_b
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 637831
    :try_start_2
    iget-object v0, p0, LX/3n8;->a:Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;

    iget-object v0, v0, Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;->c:LX/1Bn;

    iget-object v1, p0, LX/3n8;->e:LX/1Bt;

    iget-object v2, p0, LX/3n8;->b:Ljava/lang/String;

    iget-object v3, p0, LX/3n8;->c:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, LX/1Bn;->a(LX/1Bt;Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;LX/7i0;)LX/1By;

    move-result-object v1

    .line 637832
    new-instance v0, LX/1Bx;

    iget-object v2, p0, LX/3n8;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, LX/1Bx;-><init>(LX/1By;Ljava/lang/String;)V

    .line 637833
    iput-object v5, v0, LX/1Bx;->c:LX/7i0;

    .line 637834
    if-nez v1, :cond_c

    .line 637835
    sget-object v1, LX/1Bu;->STORE_ERROR:LX/1Bu;

    iput-object v1, v0, LX/1Bx;->f:LX/1Bu;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 637836
    :cond_c
    if-eqz v4, :cond_2

    .line 637837
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    goto/16 :goto_1

    .line 637838
    :cond_d
    move-object v0, v1

    .line 637839
    goto/16 :goto_1

    .line 637840
    :catchall_1
    move-exception v0

    move-object v1, v4

    goto :goto_2

    .line 637841
    :cond_e
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    .line 637842
    array-length v9, v2

    move v6, v5

    :goto_3
    if-ge v6, v9, :cond_10

    aget-object v10, v2, v6

    .line 637843
    invoke-interface {v10}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v4

    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v4, v11}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v11

    .line 637844
    const/4 v4, -0x1

    invoke-virtual {v11}, Ljava/lang/String;->hashCode()I

    move-result v12

    sparse-switch v12, :sswitch_data_0

    :cond_f
    :goto_4
    packed-switch v4, :pswitch_data_0

    .line 637845
    :goto_5
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_3

    .line 637846
    :sswitch_0
    const-string v12, "content-type"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_f

    move v4, v5

    goto :goto_4

    :sswitch_1
    const-string v12, "cache-control"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_f

    const/4 v4, 0x1

    goto :goto_4

    :sswitch_2
    const-string v12, "set-cookie"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_f

    const/4 v4, 0x2

    goto :goto_4

    :sswitch_3
    const-string v12, "location"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_f

    const/4 v4, 0x3

    goto :goto_4

    .line 637847
    :pswitch_0
    invoke-interface {v10}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v3}, LX/3C8;->b(Ljava/lang/String;LX/7i0;)V

    goto :goto_5

    .line 637848
    :pswitch_1
    invoke-interface {v10}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v3}, LX/3C8;->c(Ljava/lang/String;LX/7i0;)V

    goto :goto_5

    .line 637849
    :pswitch_2
    invoke-interface {v10}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 637850
    :pswitch_3
    invoke-interface {v10}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v3}, LX/3C8;->a(Ljava/lang/String;LX/7i0;)V

    goto :goto_5

    .line 637851
    :cond_10
    invoke-virtual {v7}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 637852
    iput-object v7, v3, LX/7i0;->d:Ljava/util/List;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0xc71a9ee -> :sswitch_1
        0x2ed4600e -> :sswitch_0
        0x49be662f -> :sswitch_2
        0x714f9fb5 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
