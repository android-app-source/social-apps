.class public LX/4No;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 694511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 694512
    const/4 v9, 0x0

    .line 694513
    const/4 v8, 0x0

    .line 694514
    const/4 v7, 0x0

    .line 694515
    const/4 v6, 0x0

    .line 694516
    const/4 v5, 0x0

    .line 694517
    const/4 v4, 0x0

    .line 694518
    const/4 v3, 0x0

    .line 694519
    const/4 v2, 0x0

    .line 694520
    const/4 v1, 0x0

    .line 694521
    const/4 v0, 0x0

    .line 694522
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 694523
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 694524
    const/4 v0, 0x0

    .line 694525
    :goto_0
    return v0

    .line 694526
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 694527
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_8

    .line 694528
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 694529
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 694530
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 694531
    const-string v11, "corrected_query"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 694532
    invoke-static {p0, p1}, LX/4Np;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 694533
    :cond_2
    const-string v11, "count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 694534
    const/4 v2, 0x1

    .line 694535
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v8

    goto :goto_1

    .line 694536
    :cond_3
    const-string v11, "disable_auto_load"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 694537
    const/4 v1, 0x1

    .line 694538
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v7

    goto :goto_1

    .line 694539
    :cond_4
    const-string v11, "page_info"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 694540
    invoke-static {p0, p1}, LX/264;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 694541
    :cond_5
    const-string v11, "session_id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 694542
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 694543
    :cond_6
    const-string v11, "speller_confidence"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 694544
    const/4 v0, 0x1

    .line 694545
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    move-result-object v4

    goto :goto_1

    .line 694546
    :cond_7
    const-string v11, "vertical_to_log"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 694547
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 694548
    :cond_8
    const/4 v10, 0x7

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 694549
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 694550
    if-eqz v2, :cond_9

    .line 694551
    const/4 v2, 0x1

    const/4 v9, 0x0

    invoke-virtual {p1, v2, v8, v9}, LX/186;->a(III)V

    .line 694552
    :cond_9
    if-eqz v1, :cond_a

    .line 694553
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v7}, LX/186;->a(IZ)V

    .line 694554
    :cond_a
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 694555
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 694556
    if-eqz v0, :cond_b

    .line 694557
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 694558
    :cond_b
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 694559
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x0

    .line 694560
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 694561
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 694562
    if-eqz v0, :cond_0

    .line 694563
    const-string v1, "corrected_query"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694564
    invoke-static {p0, v0, p2, p3}, LX/4Np;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 694565
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 694566
    if-eqz v0, :cond_1

    .line 694567
    const-string v1, "count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694568
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 694569
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 694570
    if-eqz v0, :cond_2

    .line 694571
    const-string v1, "disable_auto_load"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694572
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 694573
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 694574
    if-eqz v0, :cond_3

    .line 694575
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694576
    invoke-static {p0, v0, p2}, LX/264;->a(LX/15i;ILX/0nX;)V

    .line 694577
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 694578
    if-eqz v0, :cond_4

    .line 694579
    const-string v1, "session_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694580
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 694581
    :cond_4
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 694582
    if-eqz v0, :cond_5

    .line 694583
    const-string v0, "speller_confidence"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694584
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 694585
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 694586
    if-eqz v0, :cond_6

    .line 694587
    const-string v1, "vertical_to_log"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694588
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 694589
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 694590
    return-void
.end method
