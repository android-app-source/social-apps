.class public LX/4ft;
.super LX/0RV;
.source ""

# interfaces
.implements LX/0Ot;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/0RV",
        "<TT;>;",
        "LX/0Ot",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0RF;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<TT;>;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private d:Z


# direct methods
.method public constructor <init>(LX/0RF;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0RF;",
            "LX/0Or",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 798796
    invoke-direct {p0}, LX/0RV;-><init>()V

    .line 798797
    iput-object p1, p0, LX/4ft;->a:LX/0RF;

    .line 798798
    iput-object p2, p0, LX/4ft;->b:LX/0Or;

    .line 798799
    return-void
.end method


# virtual methods
.method public final declared-synchronized get()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 798800
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/4ft;->d:Z

    if-nez v0, :cond_0

    .line 798801
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v1

    .line 798802
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, LX/0SD;->b(B)B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result v2

    .line 798803
    :try_start_1
    iget-object v0, p0, LX/4ft;->a:LX/0RF;

    invoke-virtual {v0}, LX/0RF;->enterScope()LX/0S7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 798804
    :try_start_2
    iget-object v0, p0, LX/4ft;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/4ft;->c:Ljava/lang/Object;

    .line 798805
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4ft;->d:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 798806
    :try_start_3
    invoke-static {v3}, LX/0RF;->a(LX/0S7;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 798807
    :try_start_4
    iput-byte v2, v1, LX/0SD;->a:B

    .line 798808
    :cond_0
    iget-object v0, p0, LX/4ft;->c:Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    monitor-exit p0

    return-object v0

    .line 798809
    :catchall_0
    move-exception v0

    :try_start_5
    invoke-static {v3}, LX/0RF;->a(LX/0S7;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 798810
    :catchall_1
    move-exception v0

    .line 798811
    :try_start_6
    iput-byte v2, v1, LX/0SD;->a:B

    .line 798812
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 798813
    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0
.end method
