.class public final enum LX/4m4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4m4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4m4;

.field public static final enum COMMENTS:LX/4m4;

.field public static final enum COMPOSER:LX/4m4;

.field public static final enum MESSENGER:LX/4m4;

.field public static final enum MONTAGE:LX/4m4;

.field public static final enum POSTS:LX/4m4;

.field public static final enum SMS:LX/4m4;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 805248
    new-instance v0, LX/4m4;

    const-string v1, "MESSENGER"

    invoke-direct {v0, v1, v3}, LX/4m4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4m4;->MESSENGER:LX/4m4;

    .line 805249
    new-instance v0, LX/4m4;

    const-string v1, "COMMENTS"

    invoke-direct {v0, v1, v4}, LX/4m4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4m4;->COMMENTS:LX/4m4;

    .line 805250
    new-instance v0, LX/4m4;

    const-string v1, "COMPOSER"

    invoke-direct {v0, v1, v5}, LX/4m4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4m4;->COMPOSER:LX/4m4;

    .line 805251
    new-instance v0, LX/4m4;

    const-string v1, "POSTS"

    invoke-direct {v0, v1, v6}, LX/4m4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4m4;->POSTS:LX/4m4;

    .line 805252
    new-instance v0, LX/4m4;

    const-string v1, "SMS"

    invoke-direct {v0, v1, v7}, LX/4m4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4m4;->SMS:LX/4m4;

    .line 805253
    new-instance v0, LX/4m4;

    const-string v1, "MONTAGE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/4m4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4m4;->MONTAGE:LX/4m4;

    .line 805254
    const/4 v0, 0x6

    new-array v0, v0, [LX/4m4;

    sget-object v1, LX/4m4;->MESSENGER:LX/4m4;

    aput-object v1, v0, v3

    sget-object v1, LX/4m4;->COMMENTS:LX/4m4;

    aput-object v1, v0, v4

    sget-object v1, LX/4m4;->COMPOSER:LX/4m4;

    aput-object v1, v0, v5

    sget-object v1, LX/4m4;->POSTS:LX/4m4;

    aput-object v1, v0, v6

    sget-object v1, LX/4m4;->SMS:LX/4m4;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/4m4;->MONTAGE:LX/4m4;

    aput-object v2, v0, v1

    sput-object v0, LX/4m4;->$VALUES:[LX/4m4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 805255
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4m4;
    .locals 1

    .prologue
    .line 805256
    const-class v0, LX/4m4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4m4;

    return-object v0
.end method

.method public static values()[LX/4m4;
    .locals 1

    .prologue
    .line 805257
    sget-object v0, LX/4m4;->$VALUES:[LX/4m4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4m4;

    return-object v0
.end method
