.class public LX/3RQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:[J
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public static final b:[J
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final d:[J

.field private static volatile q:LX/3RQ;


# instance fields
.field public final e:Landroid/content/Context;

.field public final f:Landroid/os/Vibrator;

.field public final g:Landroid/media/AudioManager;

.field public final h:LX/3RS;

.field public final i:LX/3RT;

.field public final j:LX/0Uo;

.field public final k:LX/01T;

.field public final l:LX/3RW;

.field public final m:LX/2Ly;

.field private final n:LX/2Mk;

.field private final o:LX/0pu;

.field public final p:LX/3RZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 580424
    const-class v0, LX/3RQ;

    sput-object v0, LX/3RQ;->c:Ljava/lang/Class;

    .line 580425
    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    sput-object v0, LX/3RQ;->a:[J

    .line 580426
    const/4 v0, 0x4

    new-array v0, v0, [J

    fill-array-data v0, :array_1

    sput-object v0, LX/3RQ;->b:[J

    .line 580427
    const/4 v0, 0x1

    new-array v0, v0, [J

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    aput-wide v2, v0, v1

    sput-object v0, LX/3RQ;->d:[J

    return-void

    nop

    :array_0
    .array-data 8
        0x0
        0x64
    .end array-data

    .line 580428
    :array_1
    .array-data 8
        0x0
        0xc8
        0xc8
        0xc8
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Vibrator;Landroid/media/AudioManager;LX/3RS;LX/3RT;LX/0Uo;LX/01T;LX/3RW;LX/2Mk;LX/2Ly;LX/0pu;LX/3RZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 580410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 580411
    iput-object p1, p0, LX/3RQ;->e:Landroid/content/Context;

    .line 580412
    iput-object p2, p0, LX/3RQ;->f:Landroid/os/Vibrator;

    .line 580413
    iput-object p3, p0, LX/3RQ;->g:Landroid/media/AudioManager;

    .line 580414
    iput-object p4, p0, LX/3RQ;->h:LX/3RS;

    .line 580415
    iput-object p5, p0, LX/3RQ;->i:LX/3RT;

    .line 580416
    iput-object p6, p0, LX/3RQ;->j:LX/0Uo;

    .line 580417
    iput-object p7, p0, LX/3RQ;->k:LX/01T;

    .line 580418
    iput-object p8, p0, LX/3RQ;->l:LX/3RW;

    .line 580419
    iput-object p9, p0, LX/3RQ;->n:LX/2Mk;

    .line 580420
    iput-object p10, p0, LX/3RQ;->m:LX/2Ly;

    .line 580421
    iput-object p11, p0, LX/3RQ;->o:LX/0pu;

    .line 580422
    iput-object p12, p0, LX/3RQ;->p:LX/3RZ;

    .line 580423
    return-void
.end method

.method public static a(LX/0QB;)LX/3RQ;
    .locals 3

    .prologue
    .line 580400
    sget-object v0, LX/3RQ;->q:LX/3RQ;

    if-nez v0, :cond_1

    .line 580401
    const-class v1, LX/3RQ;

    monitor-enter v1

    .line 580402
    :try_start_0
    sget-object v0, LX/3RQ;->q:LX/3RQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 580403
    if-eqz v2, :cond_0

    .line 580404
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/3RQ;->b(LX/0QB;)LX/3RQ;

    move-result-object v0

    sput-object v0, LX/3RQ;->q:LX/3RQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 580405
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 580406
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 580407
    :cond_1
    sget-object v0, LX/3RQ;->q:LX/3RQ;

    return-object v0

    .line 580408
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 580409
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/3RQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/net/Uri;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 580374
    iget-object v0, p0, LX/3RQ;->i:LX/3RT;

    instance-of v0, v0, LX/3RT;

    if-eqz v0, :cond_0

    .line 580375
    iget-object v0, p0, LX/3RQ;->i:LX/3RT;

    .line 580376
    invoke-static {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 580377
    iget-object v1, v0, LX/3RT;->a:LX/3RU;

    .line 580378
    iget-object v2, v1, LX/3RU;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v0, LX/3Kr;->ah:LX/0Tn;

    const/4 p1, 0x2

    invoke-static {p1}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v2, v0, p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 580379
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 580380
    :goto_0
    move-object v0, v1

    .line 580381
    invoke-direct {p0, v0}, LX/3RQ;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 580382
    :goto_1
    return-object v0

    :cond_0
    invoke-static {p0}, LX/3RQ;->d(LX/3RQ;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_1

    .line 580383
    :cond_1
    iget-object v1, v0, LX/3RT;->a:LX/3RU;

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 580384
    iget-object v4, v1, LX/3RU;->b:LX/3RV;

    .line 580385
    iget-object v5, v4, LX/3RV;->a:LX/0ad;

    sget-short v6, LX/DiL;->a:S

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v5

    move v4, v5

    .line 580386
    if-nez v4, :cond_4

    .line 580387
    :cond_2
    :goto_2
    move v1, v2

    .line 580388
    if-eqz v1, :cond_3

    .line 580389
    iget-object v1, v0, LX/3RT;->a:LX/3RU;

    invoke-virtual {v0}, LX/3RT;->j()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 580390
    iget-object v3, v1, LX/3RU;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 580391
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 580392
    sget-object v1, LX/0db;->ab:LX/0Tn;

    invoke-virtual {v1, v4}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v4

    check-cast v4, LX/0Tn;

    const-string v1, "/thread_ringtone"

    invoke-virtual {v4, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v4

    check-cast v4, LX/0Tn;

    move-object v4, v4

    .line 580393
    invoke-interface {v3, v4, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v1, v3

    .line 580394
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 580395
    :cond_3
    invoke-virtual {v0}, LX/3RT;->j()Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 580396
    :cond_4
    iget-object v4, v1, LX/3RU;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/0db;->ac:LX/0Tn;

    invoke-interface {v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 580397
    iget-object v4, v1, LX/3RU;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    sget-object v5, LX/0db;->ac:LX/0Tn;

    invoke-interface {v4, v5, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    move v2, v3

    .line 580398
    goto :goto_2

    .line 580399
    :cond_5
    iget-object v4, v1, LX/3RU;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/0db;->ac:LX/0Tn;

    invoke-interface {v4, v5, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v4

    if-nez v4, :cond_2

    move v2, v3

    goto :goto_2
.end method

.method private a(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 4
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 580347
    if-nez p1, :cond_1

    move-object p1, v0

    .line 580348
    :cond_0
    :goto_0
    return-object p1

    .line 580349
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 580350
    if-eqz v1, :cond_2

    const-string v2, "file"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 580351
    :cond_2
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 580352
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    move-object p1, v0

    goto :goto_0

    .line 580353
    :cond_3
    const/4 v2, 0x0

    .line 580354
    :try_start_0
    iget-object v1, p0, LX/3RQ;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 580355
    const-string v3, "r"

    invoke-virtual {v1, p1, v3}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 580356
    if-nez v1, :cond_5

    .line 580357
    if-eqz v1, :cond_4

    .line 580358
    :try_start_1
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    :cond_4
    :goto_1
    move-object p1, v0

    .line 580359
    goto :goto_0

    .line 580360
    :cond_5
    if-eqz v1, :cond_0

    .line 580361
    :try_start_2
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    goto :goto_0

    .line 580362
    :catch_1
    if-eqz v0, :cond_6

    .line 580363
    :try_start_3
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5

    :cond_6
    :goto_2
    move-object p1, v0

    .line 580364
    goto :goto_0

    .line 580365
    :catch_2
    if-eqz v0, :cond_7

    .line 580366
    :try_start_4
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6

    :cond_7
    :goto_3
    move-object p1, v0

    .line 580367
    goto :goto_0

    .line 580368
    :catch_3
    if-eqz v0, :cond_8

    .line 580369
    :try_start_5
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7

    :cond_8
    :goto_4
    move-object p1, v0

    .line 580370
    goto :goto_0

    .line 580371
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_9

    .line 580372
    :try_start_6
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_8

    .line 580373
    :cond_9
    :goto_5
    throw v1

    :catch_4
    goto :goto_1

    :catch_5
    goto :goto_2

    :catch_6
    goto :goto_3

    :catch_7
    goto :goto_4

    :catch_8
    goto :goto_5
.end method

.method private static b(LX/0QB;)LX/3RQ;
    .locals 13

    .prologue
    .line 580342
    new-instance v0, LX/3RQ;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/3RR;->b(LX/0QB;)Landroid/os/Vibrator;

    move-result-object v2

    check-cast v2, Landroid/os/Vibrator;

    invoke-static {p0}, LX/19T;->b(LX/0QB;)Landroid/media/AudioManager;

    move-result-object v3

    check-cast v3, Landroid/media/AudioManager;

    .line 580343
    new-instance v4, LX/3RS;

    const/16 v5, 0x1510

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-direct {v4, v5}, LX/3RS;-><init>(LX/0Or;)V

    .line 580344
    move-object v4, v4

    .line 580345
    check-cast v4, LX/3RS;

    invoke-static {p0}, LX/3RT;->a(LX/0QB;)LX/3RT;

    move-result-object v5

    check-cast v5, LX/3RT;

    invoke-static {p0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v6

    check-cast v6, LX/0Uo;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v7

    check-cast v7, LX/01T;

    invoke-static {p0}, LX/3RW;->b(LX/0QB;)LX/3RW;

    move-result-object v8

    check-cast v8, LX/3RW;

    invoke-static {p0}, LX/2Mk;->a(LX/0QB;)LX/2Mk;

    move-result-object v9

    check-cast v9, LX/2Mk;

    invoke-static {p0}, LX/2Ly;->a(LX/0QB;)LX/2Ly;

    move-result-object v10

    check-cast v10, LX/2Ly;

    invoke-static {p0}, LX/0pu;->a(LX/0QB;)LX/0pu;

    move-result-object v11

    check-cast v11, LX/0pu;

    invoke-static {p0}, LX/3RZ;->b(LX/0QB;)LX/3RZ;

    move-result-object v12

    check-cast v12, LX/3RZ;

    invoke-direct/range {v0 .. v12}, LX/3RQ;-><init>(Landroid/content/Context;Landroid/os/Vibrator;Landroid/media/AudioManager;LX/3RS;LX/3RT;LX/0Uo;LX/01T;LX/3RW;LX/2Mk;LX/2Ly;LX/0pu;LX/3RZ;)V

    .line 580346
    return-object v0
.end method

.method public static c(LX/3RQ;)Z
    .locals 1

    .prologue
    .line 580297
    iget-object v0, p0, LX/3RQ;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3RQ;->i:LX/3RT;

    invoke-virtual {v0}, LX/3RT;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/3RQ;)Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 580341
    iget-object v0, p0, LX/3RQ;->i:LX/3RT;

    invoke-virtual {v0}, LX/3RT;->j()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, LX/3RQ;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/2HB;LX/Dhq;Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 6
    .param p3    # Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 580339
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, LX/3RQ;->a(LX/2HB;LX/Dhq;Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/net/Uri;)V

    .line 580340
    return-void
.end method

.method public final a(LX/2HB;LX/Dhq;Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/net/Uri;)V
    .locals 7
    .param p3    # Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 580303
    iget-object v0, p0, LX/3RQ;->i:LX/3RT;

    invoke-virtual {v0}, LX/3RT;->d()Z

    move-result v5

    .line 580304
    iget-object v0, p0, LX/3RQ;->i:LX/3RT;

    invoke-virtual {v0}, LX/3RT;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/3RQ;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 580305
    :goto_0
    iget-object v3, p0, LX/3RQ;->i:LX/3RT;

    invoke-virtual {v3}, LX/3RT;->f()Z

    move-result v6

    .line 580306
    if-eqz p3, :cond_5

    iget-boolean v3, p3, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;->a:Z

    if-eqz v3, :cond_5

    move v4, v1

    .line 580307
    :goto_1
    if-eqz p3, :cond_6

    iget-boolean v3, p3, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;->b:Z

    if-eqz v3, :cond_6

    move v3, v1

    .line 580308
    :goto_2
    if-eqz v0, :cond_0

    .line 580309
    iget-boolean v0, p2, LX/Dhq;->c:Z

    move v0, v0

    .line 580310
    if-nez v0, :cond_0

    if-nez v4, :cond_0

    .line 580311
    if-nez p5, :cond_b

    .line 580312
    if-eqz p4, :cond_7

    invoke-static {p0, p4}, LX/3RQ;->a(LX/3RQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/net/Uri;

    move-result-object v0

    .line 580313
    :goto_3
    if-eqz v0, :cond_8

    .line 580314
    invoke-virtual {p1, v0}, LX/2HB;->a(Landroid/net/Uri;)LX/2HB;

    .line 580315
    :goto_4
    invoke-virtual {p2}, LX/Dhq;->c()V

    .line 580316
    :cond_0
    if-eqz v5, :cond_a

    .line 580317
    iget-boolean v0, p2, LX/Dhq;->d:Z

    move v0, v0

    .line 580318
    if-nez v0, :cond_a

    if-nez v3, :cond_a

    .line 580319
    iget-object v0, p0, LX/3RQ;->o:LX/0pu;

    invoke-virtual {v0}, LX/0pu;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 580320
    sget-object v0, LX/3RQ;->a:[J

    invoke-virtual {p1, v0}, LX/2HB;->a([J)LX/2HB;

    .line 580321
    :cond_1
    :goto_5
    invoke-virtual {p2}, LX/Dhq;->e()V

    .line 580322
    :goto_6
    if-eqz v2, :cond_2

    .line 580323
    invoke-virtual {p1, v2}, LX/2HB;->c(I)LX/2HB;

    .line 580324
    :cond_2
    if-eqz v6, :cond_3

    .line 580325
    iget-boolean v0, p2, LX/Dhq;->e:Z

    move v0, v0

    .line 580326
    if-nez v0, :cond_3

    .line 580327
    const v0, -0xff0100

    const/16 v1, 0x12c

    const/16 v2, 0x3e8

    invoke-virtual {p1, v0, v1, v2}, LX/2HB;->a(III)LX/2HB;

    .line 580328
    const/4 v0, 0x1

    iput-boolean v0, p2, LX/Dhq;->e:Z

    .line 580329
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 580330
    goto :goto_0

    :cond_5
    move v4, v2

    .line 580331
    goto :goto_1

    :cond_6
    move v3, v2

    .line 580332
    goto :goto_2

    .line 580333
    :cond_7
    invoke-static {p0}, LX/3RQ;->d(LX/3RQ;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_3

    :cond_8
    move v2, v1

    .line 580334
    goto :goto_4

    .line 580335
    :cond_9
    sget-object v0, LX/3RQ;->b:[J

    invoke-virtual {p1, v0}, LX/2HB;->a([J)LX/2HB;

    .line 580336
    const/4 v0, 0x2

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 580337
    sget-object v0, LX/3RQ;->b:[J

    invoke-static {v0}, Ljava/util/Arrays;->toString([J)Ljava/lang/String;

    goto :goto_5

    .line 580338
    :cond_a
    sget-object v0, LX/3RQ;->d:[J

    invoke-virtual {p1, v0}, LX/2HB;->a([J)LX/2HB;

    goto :goto_6

    :cond_b
    move-object v0, p5

    goto :goto_3
.end method

.method public final a(LX/2HB;)Z
    .locals 1

    .prologue
    .line 580298
    invoke-static {p0}, LX/3RQ;->c(LX/3RQ;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 580299
    const/4 v0, 0x0

    .line 580300
    :goto_0
    return v0

    .line 580301
    :cond_0
    sget-object v0, LX/3RQ;->b:[J

    invoke-virtual {p1, v0}, LX/2HB;->a([J)LX/2HB;

    .line 580302
    const/4 v0, 0x1

    goto :goto_0
.end method
