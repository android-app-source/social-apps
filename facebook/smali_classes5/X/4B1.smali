.class public final enum LX/4B1;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4B1;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4B1;

.field public static final enum COMPOSED:LX/4B1;

.field public static final enum IN_MEMORY_CACHE:LX/4B1;

.field public static final enum LOCAL_DISK_CACHE:LX/4B1;

.field public static final enum LOCAL_UNSPECIFIED_CACHE:LX/4B1;

.field public static final enum SERVER:LX/4B1;

.field public static final enum SMS:LX/4B1;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 677174
    new-instance v0, LX/4B1;

    const-string v1, "IN_MEMORY_CACHE"

    invoke-direct {v0, v1, v3}, LX/4B1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4B1;->IN_MEMORY_CACHE:LX/4B1;

    .line 677175
    new-instance v0, LX/4B1;

    const-string v1, "LOCAL_DISK_CACHE"

    invoke-direct {v0, v1, v4}, LX/4B1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4B1;->LOCAL_DISK_CACHE:LX/4B1;

    .line 677176
    new-instance v0, LX/4B1;

    const-string v1, "LOCAL_UNSPECIFIED_CACHE"

    invoke-direct {v0, v1, v5}, LX/4B1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4B1;->LOCAL_UNSPECIFIED_CACHE:LX/4B1;

    .line 677177
    new-instance v0, LX/4B1;

    const-string v1, "SERVER"

    invoke-direct {v0, v1, v6}, LX/4B1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4B1;->SERVER:LX/4B1;

    .line 677178
    new-instance v0, LX/4B1;

    const-string v1, "SMS"

    invoke-direct {v0, v1, v7}, LX/4B1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4B1;->SMS:LX/4B1;

    .line 677179
    new-instance v0, LX/4B1;

    const-string v1, "COMPOSED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/4B1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4B1;->COMPOSED:LX/4B1;

    .line 677180
    const/4 v0, 0x6

    new-array v0, v0, [LX/4B1;

    sget-object v1, LX/4B1;->IN_MEMORY_CACHE:LX/4B1;

    aput-object v1, v0, v3

    sget-object v1, LX/4B1;->LOCAL_DISK_CACHE:LX/4B1;

    aput-object v1, v0, v4

    sget-object v1, LX/4B1;->LOCAL_UNSPECIFIED_CACHE:LX/4B1;

    aput-object v1, v0, v5

    sget-object v1, LX/4B1;->SERVER:LX/4B1;

    aput-object v1, v0, v6

    sget-object v1, LX/4B1;->SMS:LX/4B1;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/4B1;->COMPOSED:LX/4B1;

    aput-object v2, v0, v1

    sput-object v0, LX/4B1;->$VALUES:[LX/4B1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 677181
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4B1;
    .locals 1

    .prologue
    .line 677182
    const-class v0, LX/4B1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4B1;

    return-object v0
.end method

.method public static values()[LX/4B1;
    .locals 1

    .prologue
    .line 677183
    sget-object v0, LX/4B1;->$VALUES:[LX/4B1;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4B1;

    return-object v0
.end method


# virtual methods
.method public final isLocal()Z
    .locals 1

    .prologue
    .line 677184
    sget-object v0, LX/4B1;->IN_MEMORY_CACHE:LX/4B1;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/4B1;->LOCAL_DISK_CACHE:LX/4B1;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/4B1;->LOCAL_UNSPECIFIED_CACHE:LX/4B1;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
