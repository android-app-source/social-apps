.class public LX/4pe;
.super LX/4pZ;
.source ""


# instance fields
.field public final g:Z

.field public h:C

.field public i:I

.field public j:I

.field public final k:Z


# direct methods
.method public constructor <init>(LX/12A;Ljava/io/InputStream;[BIIZ)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 811585
    invoke-direct/range {p0 .. p5}, LX/4pZ;-><init>(LX/12A;Ljava/io/InputStream;[BII)V

    .line 811586
    iput-char v0, p0, LX/4pe;->h:C

    .line 811587
    iput v0, p0, LX/4pe;->i:I

    .line 811588
    iput v0, p0, LX/4pe;->j:I

    .line 811589
    iput-boolean p6, p0, LX/4pe;->g:Z

    .line 811590
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, LX/4pe;->k:Z

    .line 811591
    return-void
.end method

.method private a(II)V
    .locals 5

    .prologue
    .line 811521
    iget v0, p0, LX/4pe;->j:I

    add-int/2addr v0, p1

    .line 811522
    iget v1, p0, LX/4pe;->i:I

    .line 811523
    new-instance v2, Ljava/io/CharConversionException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected EOF in the middle of a 4-byte UTF-32 char: got "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", needed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", at char #"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", byte #"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/CharConversionException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private a(IILjava/lang/String;)V
    .locals 5

    .prologue
    .line 811592
    iget v0, p0, LX/4pe;->j:I

    iget v1, p0, LX/4pZ;->d:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    .line 811593
    iget v1, p0, LX/4pe;->i:I

    add-int/2addr v1, p2

    .line 811594
    new-instance v2, Ljava/io/CharConversionException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid UTF-32 character 0x"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " at char #"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", byte #"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/CharConversionException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private a(I)Z
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 811555
    iget v2, p0, LX/4pe;->j:I

    iget v3, p0, LX/4pZ;->e:I

    sub-int/2addr v3, p1

    add-int/2addr v2, v3

    iput v2, p0, LX/4pe;->j:I

    .line 811556
    if-lez p1, :cond_5

    .line 811557
    iget v2, p0, LX/4pZ;->d:I

    if-lez v2, :cond_1

    move v2, v0

    .line 811558
    :goto_0
    if-ge v2, p1, :cond_0

    .line 811559
    iget-object v3, p0, LX/4pZ;->c:[B

    iget-object v4, p0, LX/4pZ;->c:[B

    iget v5, p0, LX/4pZ;->d:I

    add-int/2addr v5, v2

    aget-byte v4, v4, v5

    aput-byte v4, v3, v2

    .line 811560
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 811561
    :cond_0
    iput v0, p0, LX/4pe;->d:I

    .line 811562
    :cond_1
    iput p1, p0, LX/4pe;->e:I

    .line 811563
    :goto_1
    iget v0, p0, LX/4pZ;->e:I

    if-ge v0, v6, :cond_b

    .line 811564
    iget-object v0, p0, LX/4pZ;->b:Ljava/io/InputStream;

    if-nez v0, :cond_a

    move v0, v1

    .line 811565
    :goto_2
    if-gtz v0, :cond_4

    .line 811566
    if-gez v0, :cond_3

    .line 811567
    iget-boolean v2, p0, LX/4pe;->k:Z

    if-eqz v2, :cond_2

    .line 811568
    invoke-virtual {p0}, LX/4pZ;->a()V

    .line 811569
    :cond_2
    iget v2, p0, LX/4pZ;->e:I

    invoke-direct {p0, v2, v6}, LX/4pe;->a(II)V

    .line 811570
    :cond_3
    invoke-static {}, LX/4pZ;->b()V

    .line 811571
    :cond_4
    iget v2, p0, LX/4pZ;->e:I

    add-int/2addr v0, v2

    iput v0, p0, LX/4pe;->e:I

    goto :goto_1

    .line 811572
    :cond_5
    iput v0, p0, LX/4pe;->d:I

    .line 811573
    iget-object v2, p0, LX/4pZ;->b:Ljava/io/InputStream;

    if-nez v2, :cond_7

    move v2, v1

    .line 811574
    :goto_3
    if-gtz v2, :cond_9

    .line 811575
    iput v0, p0, LX/4pe;->e:I

    .line 811576
    if-gez v2, :cond_8

    .line 811577
    iget-boolean v1, p0, LX/4pe;->k:Z

    if-eqz v1, :cond_6

    .line 811578
    invoke-virtual {p0}, LX/4pZ;->a()V

    .line 811579
    :cond_6
    :goto_4
    return v0

    .line 811580
    :cond_7
    iget-object v2, p0, LX/4pZ;->b:Ljava/io/InputStream;

    iget-object v3, p0, LX/4pZ;->c:[B

    invoke-virtual {v2, v3}, Ljava/io/InputStream;->read([B)I

    move-result v2

    goto :goto_3

    .line 811581
    :cond_8
    invoke-static {}, LX/4pZ;->b()V

    .line 811582
    :cond_9
    iput v2, p0, LX/4pe;->e:I

    goto :goto_1

    .line 811583
    :cond_a
    iget-object v0, p0, LX/4pZ;->b:Ljava/io/InputStream;

    iget-object v2, p0, LX/4pZ;->c:[B

    iget v3, p0, LX/4pZ;->e:I

    iget-object v4, p0, LX/4pZ;->c:[B

    array-length v4, v4

    iget v5, p0, LX/4pZ;->e:I

    sub-int/2addr v4, v5

    invoke-virtual {v0, v2, v3, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    goto :goto_2

    .line 811584
    :cond_b
    const/4 v0, 0x1

    goto :goto_4
.end method


# virtual methods
.method public final read([CII)I
    .locals 7

    .prologue
    const v6, 0x10ffff

    const/4 v0, -0x1

    .line 811524
    iget-object v1, p0, LX/4pZ;->c:[B

    if-nez v1, :cond_1

    move p3, v0

    .line 811525
    :cond_0
    :goto_0
    return p3

    .line 811526
    :cond_1
    if-lez p3, :cond_0

    .line 811527
    if-ltz p2, :cond_2

    add-int v1, p2, p3

    array-length v2, p1

    if-le v1, v2, :cond_3

    .line 811528
    :cond_2
    invoke-static {p1, p2, p3}, LX/4pZ;->a([CII)V

    .line 811529
    :cond_3
    add-int v3, p3, p2

    .line 811530
    iget-char v1, p0, LX/4pe;->h:C

    if-eqz v1, :cond_6

    .line 811531
    add-int/lit8 v2, p2, 0x1

    iget-char v0, p0, LX/4pe;->h:C

    aput-char v0, p1, p2

    .line 811532
    const/4 v0, 0x0

    iput-char v0, p0, LX/4pe;->h:C

    .line 811533
    :cond_4
    :goto_1
    if-ge v2, v3, :cond_a

    .line 811534
    iget v0, p0, LX/4pZ;->d:I

    .line 811535
    iget-boolean v1, p0, LX/4pe;->g:Z

    if-eqz v1, :cond_7

    .line 811536
    iget-object v1, p0, LX/4pZ;->c:[B

    aget-byte v1, v1, v0

    shl-int/lit8 v1, v1, 0x18

    iget-object v4, p0, LX/4pZ;->c:[B

    add-int/lit8 v5, v0, 0x1

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x10

    or-int/2addr v1, v4

    iget-object v4, p0, LX/4pZ;->c:[B

    add-int/lit8 v5, v0, 0x2

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x8

    or-int/2addr v1, v4

    iget-object v4, p0, LX/4pZ;->c:[B

    add-int/lit8 v0, v0, 0x3

    aget-byte v0, v4, v0

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v1

    .line 811537
    :goto_2
    iget v1, p0, LX/4pZ;->d:I

    add-int/lit8 v1, v1, 0x4

    iput v1, p0, LX/4pe;->d:I

    .line 811538
    const v1, 0xffff

    if-le v0, v1, :cond_8

    .line 811539
    if-le v0, v6, :cond_5

    .line 811540
    sub-int v1, v2, p2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "(above "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v0, v1, v4}, LX/4pe;->a(IILjava/lang/String;)V

    .line 811541
    :cond_5
    const/high16 v1, 0x10000

    sub-int/2addr v0, v1

    .line 811542
    add-int/lit8 v1, v2, 0x1

    const v4, 0xd800

    shr-int/lit8 v5, v0, 0xa

    add-int/2addr v4, v5

    int-to-char v4, v4

    aput-char v4, p1, v2

    .line 811543
    const v2, 0xdc00

    and-int/lit16 v0, v0, 0x3ff

    or-int/2addr v0, v2

    .line 811544
    if-lt v1, v3, :cond_9

    .line 811545
    int-to-char v0, v0

    iput-char v0, p0, LX/4pe;->h:C

    .line 811546
    :goto_3
    sub-int p3, v1, p2

    .line 811547
    iget v0, p0, LX/4pe;->i:I

    add-int/2addr v0, p3

    iput v0, p0, LX/4pe;->i:I

    goto/16 :goto_0

    .line 811548
    :cond_6
    iget v1, p0, LX/4pZ;->e:I

    iget v2, p0, LX/4pZ;->d:I

    sub-int/2addr v1, v2

    .line 811549
    const/4 v2, 0x4

    if-ge v1, v2, :cond_b

    .line 811550
    invoke-direct {p0, v1}, LX/4pe;->a(I)Z

    move-result v1

    if-nez v1, :cond_b

    move p3, v0

    .line 811551
    goto/16 :goto_0

    .line 811552
    :cond_7
    iget-object v1, p0, LX/4pZ;->c:[B

    aget-byte v1, v1, v0

    and-int/lit16 v1, v1, 0xff

    iget-object v4, p0, LX/4pZ;->c:[B

    add-int/lit8 v5, v0, 0x1

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x8

    or-int/2addr v1, v4

    iget-object v4, p0, LX/4pZ;->c:[B

    add-int/lit8 v5, v0, 0x2

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x10

    or-int/2addr v1, v4

    iget-object v4, p0, LX/4pZ;->c:[B

    add-int/lit8 v0, v0, 0x3

    aget-byte v0, v4, v0

    shl-int/lit8 v0, v0, 0x18

    or-int/2addr v0, v1

    goto/16 :goto_2

    :cond_8
    move v1, v2

    .line 811553
    :cond_9
    add-int/lit8 v2, v1, 0x1

    int-to-char v0, v0

    aput-char v0, p1, v1

    .line 811554
    iget v0, p0, LX/4pZ;->d:I

    iget v1, p0, LX/4pZ;->e:I

    if-lt v0, v1, :cond_4

    :cond_a
    move v1, v2

    goto :goto_3

    :cond_b
    move v2, p2

    goto/16 :goto_1
.end method
