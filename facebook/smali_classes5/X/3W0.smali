.class public LX/3W0;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field private final b:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

.field private final c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 588958
    new-instance v0, LX/3W1;

    invoke-direct {v0}, LX/3W1;-><init>()V

    sput-object v0, LX/3W0;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 588952
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 588953
    const v0, 0x7f031526

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 588954
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/3W0;->setOrientation(I)V

    .line 588955
    const v0, 0x7f0d2fbd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iput-object v0, p0, LX/3W0;->b:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 588956
    const v0, 0x7f0d2fbe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iput-object v0, p0, LX/3W0;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 588957
    return-void
.end method

.method private a(LX/2mR;)Lcom/facebook/fbui/widget/text/GlyphWithTextView;
    .locals 2

    .prologue
    .line 588948
    sget-object v0, LX/Ao4;->a:[I

    invoke-virtual {p1}, LX/2mR;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 588949
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid button location"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 588950
    :pswitch_0
    iget-object v0, p0, LX/3W0;->b:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 588951
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, LX/3W0;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/2mR;I)V
    .locals 2

    .prologue
    .line 588959
    invoke-direct {p0, p1}, LX/3W0;->a(LX/2mR;)Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    move-result-object v0

    invoke-virtual {p0}, LX/3W0;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setTextColor(I)V

    .line 588960
    return-void
.end method

.method public final a(LX/2mR;Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 588946
    invoke-direct {p0, p1}, LX/3W0;->a(LX/2mR;)Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 588947
    return-void
.end method

.method public final a(LX/2mR;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 588944
    invoke-direct {p0, p1}, LX/3W0;->a(LX/2mR;)Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 588945
    return-void
.end method
