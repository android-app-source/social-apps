.class public LX/4lQ;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/4lP;

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 804445
    const-class v0, LX/4lQ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/4lQ;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 804441
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 804442
    iput-object p1, p0, LX/4lQ;->d:Landroid/content/Context;

    .line 804443
    new-instance v0, LX/4lP;

    invoke-direct {v0}, LX/4lP;-><init>()V

    iput-object v0, p0, LX/4lQ;->b:LX/4lP;

    .line 804444
    return-void
.end method

.method private static b(LX/4lQ;Ljava/lang/String;)Lcom/facebook/schemaenforcement/AnalyticsEventSchema;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 804424
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 804425
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 804426
    const-string v2, "json/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 804427
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 804428
    const-string v2, ".json"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 804429
    iget-object v2, p0, LX/4lQ;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch LX/2aQ; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 804430
    :try_start_1
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v0

    invoke-virtual {v0}, LX/0lD;->b()LX/0lp;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0lp;->a(Ljava/io/InputStream;)LX/15w;

    move-result-object v0

    .line 804431
    const-class v3, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;

    invoke-virtual {v0, v3}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;
    :try_end_1
    .catch LX/2aQ; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 804432
    invoke-static {v2}, LX/1md;->a(Ljava/io/InputStream;)V

    .line 804433
    :goto_0
    return-object v0

    .line 804434
    :catch_0
    move-object v0, v1

    :goto_1
    const/4 v2, 0x1

    :try_start_2
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 804435
    invoke-static {v0}, LX/1md;->a(Ljava/io/InputStream;)V

    :cond_0
    :goto_2
    move-object v0, v1

    .line 804436
    goto :goto_0

    .line 804437
    :catch_1
    move-object v2, v1

    :goto_3
    const/4 v0, 0x1

    :try_start_3
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v0, v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 804438
    invoke-static {v2}, LX/1md;->a(Ljava/io/InputStream;)V

    goto :goto_2

    :catchall_0
    move-exception v0

    :goto_4
    invoke-static {v1}, LX/1md;->a(Ljava/io/InputStream;)V

    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_4

    :catchall_2
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_4

    .line 804439
    :catch_2
    goto :goto_3

    .line 804440
    :catch_3
    move-object v0, v2

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 804418
    :try_start_0
    iget-object v1, p0, LX/4lQ;->c:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 804419
    iget-object v1, p0, LX/4lQ;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    const-string v2, "json"

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, LX/4lQ;->c:Ljava/util/ArrayList;

    .line 804420
    :cond_0
    iget-object v1, p0, LX/4lQ;->c:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 804421
    :goto_0
    return v0

    .line 804422
    :catch_0
    move-exception v1

    .line 804423
    sget-object v2, LX/4lQ;->a:Ljava/lang/String;

    const-string v3, "Failed to load event schemas from assets"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;)Z
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v9, 0x1

    .line 804342
    :try_start_0
    invoke-static {p0, p1}, LX/4lQ;->b(LX/4lQ;Ljava/lang/String;)Lcom/facebook/schemaenforcement/AnalyticsEventSchema;

    move-result-object v5

    .line 804343
    if-nez v5, :cond_1

    .line 804344
    :cond_0
    :goto_0
    return v9

    .line 804345
    :cond_1
    if-eqz v5, :cond_d

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 804346
    if-eqz v0, :cond_0

    .line 804347
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 804348
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 804349
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 804350
    invoke-virtual {v5}, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->b()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 804351
    invoke-virtual {v5}, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 804352
    :cond_2
    invoke-virtual {v5}, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->c()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 804353
    invoke-virtual {v5}, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->c()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 804354
    :cond_3
    invoke-virtual {v5}, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->d()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 804355
    invoke-virtual {v5}, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->d()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 804356
    :cond_4
    invoke-virtual {v5}, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->e()Ljava/util/List;

    move-result-object v7

    move v1, v6

    .line 804357
    :goto_2
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 804358
    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, LX/4lQ;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 804359
    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v0}, LX/4lQ;->b(LX/4lQ;Ljava/lang/String;)Lcom/facebook/schemaenforcement/AnalyticsEventSchema;

    move-result-object v0

    .line 804360
    if-eqz v0, :cond_7

    .line 804361
    invoke-virtual {v0}, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->b()Ljava/util/Map;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 804362
    invoke-virtual {v0}, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->b()Ljava/util/Map;

    move-result-object v8

    invoke-interface {v2, v8}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 804363
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->c()Ljava/util/Map;

    move-result-object v8

    if-eqz v8, :cond_6

    .line 804364
    invoke-virtual {v0}, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->c()Ljava/util/Map;

    move-result-object v8

    invoke-interface {v3, v8}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 804365
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->d()Ljava/util/Map;

    move-result-object v8

    if-eqz v8, :cond_7

    .line 804366
    invoke-virtual {v0}, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->d()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 804367
    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 804368
    :cond_8
    iget-object v0, p0, LX/4lQ;->b:LX/4lP;

    invoke-virtual {v5}, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->a()Z

    move-result v5

    move-object v1, p2

    .line 804369
    if-eqz v2, :cond_9

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_e

    .line 804370
    :cond_9
    if-eqz v3, :cond_a

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_12

    .line 804371
    :cond_a
    if-eqz v5, :cond_b

    .line 804372
    if-nez v1, :cond_15

    .line 804373
    :cond_b
    iget-object v7, v0, LX/4lP;->a:LX/4lO;

    move-object v0, v7

    .line 804374
    iget-object v1, v0, LX/4lO;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    move v1, v1

    .line 804375
    if-nez v1, :cond_0

    .line 804376
    new-instance v1, Ljava/lang/RuntimeException;

    .line 804377
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 804378
    const-string v2, "Schema validation of event \""

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 804379
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 804380
    const-string v2, "\" had following errors :\n"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 804381
    const/4 v2, 0x0

    move v3, v2

    :goto_3
    iget-object v2, v0, LX/4lO;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_c

    .line 804382
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 804383
    const-string v2, ") "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 804384
    iget-object v2, v0, LX/4lO;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 804385
    const-string v2, "\n"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 804386
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    .line 804387
    :cond_c
    const-string v2, "To view the schema of the event bunnylol \"lw\" and enter event name in the \"Load\" mobileEventSchema section.\n"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 804388
    const-string v2, "Refer our dex https://our.intern.facebook.com/intern/dex/structured-unified-logging/ "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 804389
    const-string v2, " for more details."

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 804390
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v0, v2

    .line 804391
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 804392
    :catch_0
    move-exception v0

    .line 804393
    sget-object v1, LX/4lQ;->a:Ljava/lang/String;

    const-string v2, "Failed to load schema of %s event"

    new-array v3, v9, [Ljava/lang/Object;

    aput-object p1, v3, v6

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_d
    :try_start_1
    const/4 v0, 0x0

    goto/16 :goto_1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 804394
    :cond_e
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_4
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_9

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 804395
    const/4 v8, 0x0

    .line 804396
    if-eqz v1, :cond_11

    .line 804397
    invoke-interface {v1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    move-object v10, v8

    .line 804398
    :goto_5
    if-eqz v10, :cond_f

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_10

    .line 804399
    :cond_f
    iget-object v8, v0, LX/4lP;->a:LX/4lO;

    .line 804400
    new-instance v10, Ljava/lang/StringBuilder;

    const-string p2, "Required field \""

    invoke-direct {v10, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string p2, "\" is missing in the event payload"

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 804401
    iget-object p2, v8, LX/4lO;->a:Ljava/util/ArrayList;

    invoke-virtual {p2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 804402
    goto :goto_4

    .line 804403
    :cond_10
    invoke-interface {v2, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 804404
    invoke-static {v0, v7, v8, v10, v4}, LX/4lP;->a(LX/4lP;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/util/Map;)V

    goto :goto_4

    :cond_11
    move-object v10, v8

    goto :goto_5

    .line 804405
    :cond_12
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_13
    :goto_6
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_a

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 804406
    const/4 v8, 0x0

    .line 804407
    if-eqz v1, :cond_14

    .line 804408
    invoke-interface {v1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    move-object v10, v8

    .line 804409
    :goto_7
    if-eqz v10, :cond_13

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_13

    .line 804410
    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 804411
    invoke-static {v0, v7, v8, v10, v4}, LX/4lP;->a(LX/4lP;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/util/Map;)V

    goto :goto_6

    :cond_14
    move-object v10, v8

    goto :goto_7

    .line 804412
    :cond_15
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_16
    :goto_8
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_b

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 804413
    invoke-interface {v2, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_16

    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_16

    .line 804414
    iget-object v10, v0, LX/4lP;->a:LX/4lO;

    .line 804415
    new-instance p0, Ljava/lang/StringBuilder;

    const-string v1, "Payload has field \""

    invoke-direct {p0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    const-string v1, "\" which is not defined in the schema"

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 804416
    iget-object v1, v10, LX/4lO;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 804417
    goto :goto_8
.end method
