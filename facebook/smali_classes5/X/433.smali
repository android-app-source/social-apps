.class public LX/433;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 668347
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 668348
    return-void
.end method

.method public static a(LX/0Or;LX/0Or;LX/0Or;)LX/43C;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/43E;",
            ">;",
            "LX/0Or",
            "<",
            "LX/43H;",
            ">;",
            "LX/0Or",
            "<",
            "LX/43J;",
            ">;)",
            "LX/43C;"
        }
    .end annotation

    .prologue
    .line 668349
    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/43E;->NATIVE_JT_13:LX/43E;

    if-ne v0, v1, :cond_0

    .line 668350
    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/43C;

    .line 668351
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/43C;

    goto :goto_0
.end method

.method public static a(LX/0Or;LX/03V;)LX/43E;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/bitmaps/NativeImageProcessor;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")",
            "LX/43E;"
        }
    .end annotation

    .prologue
    .line 668352
    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bitmaps/NativeImageProcessor;

    invoke-virtual {v0}, Lcom/facebook/bitmaps/NativeImageProcessor;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 668353
    sget-object v0, LX/43E;->NATIVE_JT_13:LX/43E;

    .line 668354
    :goto_0
    return-object v0

    .line 668355
    :cond_0
    const-string v0, "NativeImageProcessor"

    const-string v1, "Failed to load native library"

    invoke-virtual {p1, v0, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 668356
    sget-object v0, LX/43E;->JAVA_RESIZER:LX/43E;

    goto :goto_0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 668357
    return-void
.end method
