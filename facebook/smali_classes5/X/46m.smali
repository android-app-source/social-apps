.class public LX/46m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0rz;


# direct methods
.method public constructor <init>(LX/0rz;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 671601
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 671602
    iput-object p1, p0, LX/46m;->a:LX/0rz;

    .line 671603
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 3

    .prologue
    .line 671574
    check-cast p1, Ljava/lang/String;

    .line 671575
    const/4 v0, 0x1

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 671576
    if-eqz p1, :cond_0

    .line 671577
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "channel_id"

    invoke-direct {v1, v2, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 671578
    :cond_0
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "encryptChannelRequestMethod"

    .line 671579
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 671580
    move-object v1, v1

    .line 671581
    const-string v2, "POST"

    .line 671582
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 671583
    move-object v1, v1

    .line 671584
    const-string v2, "/me/encryptedchannels"

    .line 671585
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 671586
    move-object v1, v1

    .line 671587
    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v1, v2}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v1

    .line 671588
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 671589
    move-object v0, v1

    .line 671590
    iget-object v1, p0, LX/46m;->a:LX/0rz;

    .line 671591
    iget-object v2, v1, LX/0rz;->b:Ljava/lang/String;

    move-object v1, v2

    .line 671592
    iput-object v1, v0, LX/14O;->e:Ljava/lang/String;

    .line 671593
    move-object v0, v0

    .line 671594
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 671595
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 671596
    move-object v0, v0

    .line 671597
    sget-object v1, LX/14Q;->FALLBACK_REQUIRED:LX/14Q;

    .line 671598
    iput-object v1, v0, LX/14O;->v:LX/14Q;

    .line 671599
    move-object v0, v0

    .line 671600
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 671568
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v8

    .line 671569
    sget-object v0, LX/00c;->a:LX/00c;

    move-object v0, v0

    .line 671570
    const-string v1, "fbid"

    invoke-virtual {v8, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "client_mac_key"

    invoke-virtual {v8, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "client_encryption_key"

    invoke-virtual {v8, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/46m;->a:LX/0rz;

    .line 671571
    iget-object v5, v4, LX/0rz;->b:Ljava/lang/String;

    move-object v4, v5

    .line 671572
    const-string v5, "ttl"

    invoke-virtual {v8, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->d(LX/0lF;)I

    move-result v5

    mul-int/lit16 v5, v5, 0x3e8

    const-string v6, "ttl_after_first_use"

    invoke-virtual {v8, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->d(LX/0lF;)I

    move-result v6

    mul-int/lit16 v6, v6, 0x3e8

    const-string v7, "algorithm_version"

    invoke-virtual {v8, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    invoke-static {v7}, LX/16N;->d(LX/0lF;)I

    move-result v7

    const-string v9, "creation_time"

    invoke-virtual {v8, v9}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v8

    invoke-static {v8}, LX/16N;->c(LX/0lF;)J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    invoke-virtual/range {v0 .. v9}, LX/00c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIJ)V

    .line 671573
    const/4 v0, 0x0

    return-object v0
.end method
