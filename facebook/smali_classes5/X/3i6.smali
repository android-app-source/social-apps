.class public LX/3i6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Landroid/view/View$OnClickListener;

.field private b:[I

.field private c:[Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/view/View$OnClickListener;ILjava/lang/Object;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 628688
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 628689
    iput-object p1, p0, LX/3i6;->a:Landroid/view/View$OnClickListener;

    .line 628690
    new-array v0, v3, [I

    aput p2, v0, v1

    aput p4, v0, v2

    iput-object v0, p0, LX/3i6;->b:[I

    .line 628691
    new-array v0, v3, [Ljava/lang/Object;

    aput-object p3, v0, v1

    aput-object p5, v0, v2

    iput-object v0, p0, LX/3i6;->c:[Ljava/lang/Object;

    .line 628692
    return-void
.end method

.method public constructor <init>(Landroid/view/View$OnClickListener;ILjava/lang/Object;ILjava/lang/Object;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 628693
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 628694
    iput-object p1, p0, LX/3i6;->a:Landroid/view/View$OnClickListener;

    .line 628695
    new-array v0, v4, [I

    aput p2, v0, v1

    aput p4, v0, v2

    aput p6, v0, v3

    iput-object v0, p0, LX/3i6;->b:[I

    .line 628696
    new-array v0, v4, [Ljava/lang/Object;

    aput-object p3, v0, v1

    aput-object p5, v0, v2

    aput-object p7, v0, v3

    iput-object v0, p0, LX/3i6;->c:[Ljava/lang/Object;

    .line 628697
    return-void
.end method

.method public constructor <init>(Landroid/view/View$OnClickListener;ILjava/lang/Object;ILjava/lang/Object;ILjava/lang/Object;ILjava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 628698
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 628699
    iput-object p1, p0, LX/3i6;->a:Landroid/view/View$OnClickListener;

    .line 628700
    new-array v0, v5, [I

    aput p2, v0, v1

    aput p4, v0, v2

    aput p6, v0, v3

    aput p8, v0, v4

    iput-object v0, p0, LX/3i6;->b:[I

    .line 628701
    new-array v0, v5, [Ljava/lang/Object;

    aput-object p3, v0, v1

    aput-object p5, v0, v2

    aput-object p7, v0, v3

    aput-object p9, v0, v4

    iput-object v0, p0, LX/3i6;->c:[Ljava/lang/Object;

    .line 628702
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x1

    const v3, -0x58d5d471

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 628703
    iget-object v1, p0, LX/3i6;->b:[I

    array-length v3, v1

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 628704
    iget-object v4, p0, LX/3i6;->b:[I

    aget v4, v4, v1

    iget-object v5, p0, LX/3i6;->c:[Ljava/lang/Object;

    aget-object v5, v5, v1

    invoke-virtual {p1, v4, v5}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 628705
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 628706
    :cond_0
    iget-object v1, p0, LX/3i6;->a:Landroid/view/View$OnClickListener;

    invoke-interface {v1, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 628707
    iget-object v1, p0, LX/3i6;->b:[I

    array-length v1, v1

    :goto_1
    if-ge v0, v1, :cond_1

    .line 628708
    iget-object v3, p0, LX/3i6;->b:[I

    aget v3, v3, v0

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 628709
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 628710
    :cond_1
    const v0, -0x74ae9ec4

    invoke-static {v0, v2}, LX/02F;->a(II)V

    return-void
.end method
