.class public final enum LX/4Ak;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4Ak;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4Ak;

.field public static final enum COMPLETED:LX/4Ak;

.field public static final enum INIT:LX/4Ak;

.field public static final enum OPERATION_QUEUED:LX/4Ak;

.field public static final enum READY_TO_QUEUE:LX/4Ak;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 676932
    new-instance v0, LX/4Ak;

    const-string v1, "INIT"

    invoke-direct {v0, v1, v2}, LX/4Ak;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4Ak;->INIT:LX/4Ak;

    .line 676933
    new-instance v0, LX/4Ak;

    const-string v1, "READY_TO_QUEUE"

    invoke-direct {v0, v1, v3}, LX/4Ak;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4Ak;->READY_TO_QUEUE:LX/4Ak;

    .line 676934
    new-instance v0, LX/4Ak;

    const-string v1, "OPERATION_QUEUED"

    invoke-direct {v0, v1, v4}, LX/4Ak;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4Ak;->OPERATION_QUEUED:LX/4Ak;

    .line 676935
    new-instance v0, LX/4Ak;

    const-string v1, "COMPLETED"

    invoke-direct {v0, v1, v5}, LX/4Ak;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4Ak;->COMPLETED:LX/4Ak;

    .line 676936
    const/4 v0, 0x4

    new-array v0, v0, [LX/4Ak;

    sget-object v1, LX/4Ak;->INIT:LX/4Ak;

    aput-object v1, v0, v2

    sget-object v1, LX/4Ak;->READY_TO_QUEUE:LX/4Ak;

    aput-object v1, v0, v3

    sget-object v1, LX/4Ak;->OPERATION_QUEUED:LX/4Ak;

    aput-object v1, v0, v4

    sget-object v1, LX/4Ak;->COMPLETED:LX/4Ak;

    aput-object v1, v0, v5

    sput-object v0, LX/4Ak;->$VALUES:[LX/4Ak;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 676937
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4Ak;
    .locals 1

    .prologue
    .line 676938
    const-class v0, LX/4Ak;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4Ak;

    return-object v0
.end method

.method public static values()[LX/4Ak;
    .locals 1

    .prologue
    .line 676939
    sget-object v0, LX/4Ak;->$VALUES:[LX/4Ak;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4Ak;

    return-object v0
.end method
