.class public LX/4Mc;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 688832
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 688833
    const/4 v13, 0x0

    .line 688834
    const/4 v12, 0x0

    .line 688835
    const/4 v11, 0x0

    .line 688836
    const/4 v10, 0x0

    .line 688837
    const/4 v9, 0x0

    .line 688838
    const/4 v8, 0x0

    .line 688839
    const-wide/16 v6, 0x0

    .line 688840
    const/4 v5, 0x0

    .line 688841
    const/4 v4, 0x0

    .line 688842
    const/4 v3, 0x0

    .line 688843
    const/4 v2, 0x0

    .line 688844
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_d

    .line 688845
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 688846
    const/4 v2, 0x0

    .line 688847
    :goto_0
    return v2

    .line 688848
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v15, :cond_9

    .line 688849
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 688850
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 688851
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_0

    if-eqz v7, :cond_0

    .line 688852
    const-string v15, "bump_reason"

    invoke-virtual {v7, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 688853
    const/4 v6, 0x1

    .line 688854
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLBumpReason;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBumpReason;

    move-result-object v7

    move-object v14, v7

    goto :goto_1

    .line 688855
    :cond_1
    const-string v15, "cursor"

    invoke-virtual {v7, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 688856
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    move v13, v7

    goto :goto_1

    .line 688857
    :cond_2
    const-string v15, "deduplication_key"

    invoke-virtual {v7, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 688858
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    move v12, v7

    goto :goto_1

    .line 688859
    :cond_3
    const-string v15, "disallow_first_position"

    invoke-virtual {v7, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 688860
    const/4 v3, 0x1

    .line 688861
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move v11, v7

    goto :goto_1

    .line 688862
    :cond_4
    const-string v15, "features_meta"

    invoke-virtual {v7, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 688863
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    move v10, v7

    goto :goto_1

    .line 688864
    :cond_5
    const-string v15, "node"

    invoke-virtual {v7, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 688865
    invoke-static/range {p0 .. p1}, LX/2ao;->a(LX/15w;LX/186;)I

    move-result v7

    move v9, v7

    goto/16 :goto_1

    .line 688866
    :cond_6
    const-string v15, "ranking_weight"

    invoke-virtual {v7, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 688867
    const/4 v2, 0x1

    .line 688868
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    goto/16 :goto_1

    .line 688869
    :cond_7
    const-string v15, "sort_key"

    invoke-virtual {v7, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 688870
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    move v8, v7

    goto/16 :goto_1

    .line 688871
    :cond_8
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 688872
    :cond_9
    const/16 v7, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 688873
    if-eqz v6, :cond_a

    .line 688874
    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v14}, LX/186;->a(ILjava/lang/Enum;)V

    .line 688875
    :cond_a
    const/4 v6, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v13}, LX/186;->b(II)V

    .line 688876
    const/4 v6, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v12}, LX/186;->b(II)V

    .line 688877
    if-eqz v3, :cond_b

    .line 688878
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->a(IZ)V

    .line 688879
    :cond_b
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 688880
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 688881
    if-eqz v2, :cond_c

    .line 688882
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 688883
    :cond_c
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 688884
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_d
    move-object v14, v13

    move v13, v12

    move v12, v11

    move v11, v10

    move v10, v9

    move v9, v8

    move v8, v5

    move-wide/from16 v17, v6

    move v6, v4

    move-wide/from16 v4, v17

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 688885
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 688886
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(IIS)S

    move-result v0

    .line 688887
    if-eqz v0, :cond_0

    .line 688888
    const-string v0, "bump_reason"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688889
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLBumpReason;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 688890
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 688891
    if-eqz v0, :cond_1

    .line 688892
    const-string v1, "cursor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688893
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 688894
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 688895
    if-eqz v0, :cond_2

    .line 688896
    const-string v1, "deduplication_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688897
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 688898
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 688899
    if-eqz v0, :cond_3

    .line 688900
    const-string v1, "disallow_first_position"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688901
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 688902
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 688903
    if-eqz v0, :cond_4

    .line 688904
    const-string v1, "features_meta"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688905
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 688906
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688907
    if-eqz v0, :cond_5

    .line 688908
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688909
    invoke-static {p0, v0, p2, p3}, LX/2ao;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 688910
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 688911
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_6

    .line 688912
    const-string v2, "ranking_weight"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688913
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 688914
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 688915
    if-eqz v0, :cond_7

    .line 688916
    const-string v1, "sort_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688917
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 688918
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 688919
    return-void
.end method
