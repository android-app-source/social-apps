.class public final LX/3gs;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 624885
    const-class v1, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel;

    const v0, -0x38573e42

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchContactsFullQuery"

    const-string v6, "c286f36f782ad1ebb23296602bac240c"

    const-string v7, "viewer"

    const-string v8, "10155069964676729"

    const-string v9, "10155259087266729"

    .line 624886
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 624887
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 624888
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 624889
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 624890
    sparse-switch v0, :sswitch_data_0

    .line 624891
    :goto_0
    return-object p1

    .line 624892
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 624893
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 624894
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 624895
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 624896
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 624897
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x719ba5ef -> :sswitch_1
        -0x55d248cb -> :sswitch_4
        -0x4e92d738 -> :sswitch_5
        -0x2a875d9d -> :sswitch_3
        0x6234bbb -> :sswitch_2
        0x2956b75c -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 624898
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 624899
    :goto_1
    return v0

    .line 624900
    :pswitch_0
    const-string v2, "5"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 624901
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x35
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
