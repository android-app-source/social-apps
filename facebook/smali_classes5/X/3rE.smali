.class public final LX/3rE;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:LX/0zr;

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;

.field public static final d:LX/3rE;

.field public static final e:LX/3rE;


# instance fields
.field private final f:Z

.field private final g:I

.field public final h:LX/0zr;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 642817
    sget-object v0, LX/0zo;->c:LX/0zr;

    sput-object v0, LX/3rE;->a:LX/0zr;

    .line 642818
    const/16 v0, 0x200e

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3rE;->b:Ljava/lang/String;

    .line 642819
    const/16 v0, 0x200f

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3rE;->c:Ljava/lang/String;

    .line 642820
    new-instance v0, LX/3rE;

    const/4 v1, 0x0

    sget-object v2, LX/3rE;->a:LX/0zr;

    invoke-direct {v0, v1, v3, v2}, LX/3rE;-><init>(ZILX/0zr;)V

    sput-object v0, LX/3rE;->d:LX/3rE;

    .line 642821
    new-instance v0, LX/3rE;

    const/4 v1, 0x1

    sget-object v2, LX/3rE;->a:LX/0zr;

    invoke-direct {v0, v1, v3, v2}, LX/3rE;-><init>(ZILX/0zr;)V

    sput-object v0, LX/3rE;->e:LX/3rE;

    return-void
.end method

.method public constructor <init>(ZILX/0zr;)V
    .locals 0

    .prologue
    .line 642822
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 642823
    iput-boolean p1, p0, LX/3rE;->f:Z

    .line 642824
    iput p2, p0, LX/3rE;->g:I

    .line 642825
    iput-object p3, p0, LX/3rE;->h:LX/0zr;

    .line 642826
    return-void
.end method

.method public static a(Z)LX/3rE;
    .locals 5

    .prologue
    .line 642827
    new-instance v0, LX/3rD;

    invoke-direct {v0, p0}, LX/3rD;-><init>(Z)V

    .line 642828
    iget v1, v0, LX/3rD;->b:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    iget-object v1, v0, LX/3rD;->c:LX/0zr;

    sget-object v2, LX/3rE;->a:LX/0zr;

    if-ne v1, v2, :cond_0

    .line 642829
    iget-boolean v1, v0, LX/3rD;->a:Z

    .line 642830
    if-eqz v1, :cond_1

    sget-object v2, LX/3rE;->e:LX/3rE;

    :goto_0
    move-object v1, v2

    .line 642831
    :goto_1
    move-object v0, v1

    .line 642832
    return-object v0

    :cond_0
    new-instance v1, LX/3rE;

    iget-boolean v2, v0, LX/3rD;->a:Z

    iget v3, v0, LX/3rD;->b:I

    iget-object v4, v0, LX/3rD;->c:LX/0zr;

    invoke-direct {v1, v2, v3, v4}, LX/3rE;-><init>(ZILX/0zr;)V

    goto :goto_1

    :cond_1
    sget-object v2, LX/3rE;->d:LX/3rE;

    goto :goto_0
.end method
