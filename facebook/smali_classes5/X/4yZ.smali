.class public LX/4yZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4yY;
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/Beta;
.end annotation

.annotation build Lcom/google/common/annotations/GwtIncompatible;
    value = "NavigableMap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K::",
        "Ljava/lang/Comparable",
        "<*>;V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/4yY",
        "<TK;TV;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field public static final a:LX/4yZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4yZ",
            "<",
            "Ljava/lang/Comparable",
            "<*>;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final transient b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/50M",
            "<TK;>;>;"
        }
    .end annotation
.end field

.field private final transient c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 821899
    new-instance v0, LX/4yZ;

    .line 821900
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 821901
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 821902
    invoke-direct {v0, v1, v2}, LX/4yZ;-><init>(LX/0Px;LX/0Px;)V

    sput-object v0, LX/4yZ;->a:LX/4yZ;

    return-void
.end method

.method public constructor <init>(LX/0Px;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/50M",
            "<TK;>;>;",
            "LX/0Px",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 821895
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 821896
    iput-object p1, p0, LX/4yZ;->b:LX/0Px;

    .line 821897
    iput-object p2, p0, LX/4yZ;->c:LX/0Px;

    .line 821898
    return-void
.end method

.method private c()LX/0P1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "LX/50M",
            "<TK;>;TV;>;"
        }
    .end annotation

    .prologue
    .line 821890
    iget-object v0, p0, LX/4yZ;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 821891
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 821892
    :goto_0
    return-object v0

    .line 821893
    :cond_0
    new-instance v1, LX/50U;

    iget-object v0, p0, LX/4yZ;->b:LX/0Px;

    sget-object v2, LX/50M;->a:LX/1sm;

    invoke-direct {v1, v0, v2}, LX/50U;-><init>(LX/0Px;Ljava/util/Comparator;)V

    .line 821894
    new-instance v0, LX/146;

    iget-object v2, p0, LX/4yZ;->c:LX/0Px;

    invoke-direct {v0, v1, v2}, LX/146;-><init>(LX/50U;LX/0Px;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Comparable;)Ljava/lang/Object;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 821879
    iget-object v0, p0, LX/4yZ;->b:LX/0Px;

    .line 821880
    sget-object v2, LX/50M;->b:LX/0QK;

    move-object v2, v2

    .line 821881
    invoke-static {p1}, LX/4xM;->b(Ljava/lang/Comparable;)LX/4xM;

    move-result-object v3

    sget-object v4, LX/50g;->ANY_PRESENT:LX/50g;

    sget-object v5, LX/50c;->NEXT_LOWER:LX/50c;

    .line 821882
    sget-object v6, LX/1zb;->a:LX/1zb;

    move-object v9, v6

    .line 821883
    move-object v6, v0

    move-object v7, v2

    move-object v8, v3

    move-object v10, v4

    move-object v11, v5

    .line 821884
    invoke-static {v6, v7}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v8, v9, v10, v11}, LX/50m;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;LX/50g;LX/50c;)I

    move-result v0

    move v6, v0

    .line 821885
    move v2, v6

    .line 821886
    const/4 v0, -0x1

    if-ne v2, v0, :cond_0

    move-object v0, v1

    .line 821887
    :goto_0
    return-object v0

    .line 821888
    :cond_0
    iget-object v0, p0, LX/4yZ;->b:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/50M;

    .line 821889
    invoke-virtual {v0, p1}, LX/50M;->a(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/4yZ;->c:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/50M;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/50M",
            "<TK;>;TV;)V"
        }
    .end annotation

    .prologue
    .line 821903
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic b()Ljava/util/Map;
    .locals 1

    .prologue
    .line 821878
    invoke-direct {p0}, LX/4yZ;->c()LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 821874
    instance-of v0, p1, LX/4yY;

    if-eqz v0, :cond_0

    .line 821875
    check-cast p1, LX/4yY;

    .line 821876
    invoke-direct {p0}, LX/4yZ;->c()LX/0P1;

    move-result-object v0

    invoke-interface {p1}, LX/4yY;->b()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0P1;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 821877
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 821873
    invoke-direct {p0}, LX/4yZ;->c()LX/0P1;

    move-result-object v0

    invoke-virtual {v0}, LX/0P1;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 821872
    invoke-direct {p0}, LX/4yZ;->c()LX/0P1;

    move-result-object v0

    invoke-virtual {v0}, LX/0P1;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 821871
    new-instance v0, LX/4yX;

    invoke-direct {p0}, LX/4yZ;->c()LX/0P1;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4yX;-><init>(LX/0P1;)V

    return-object v0
.end method
