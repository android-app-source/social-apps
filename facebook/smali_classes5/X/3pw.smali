.class public final LX/3pw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3pk;


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/3pb;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:Landroid/app/PendingIntent;

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/Notification;",
            ">;"
        }
    .end annotation
.end field

.field public e:Landroid/graphics/Bitmap;

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 641841
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 641842
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3pw;->a:Ljava/util/ArrayList;

    .line 641843
    const/4 v0, 0x1

    iput v0, p0, LX/3pw;->b:I

    .line 641844
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3pw;->d:Ljava/util/ArrayList;

    .line 641845
    const v0, 0x800005

    iput v0, p0, LX/3pw;->g:I

    .line 641846
    const/4 v0, -0x1

    iput v0, p0, LX/3pw;->h:I

    .line 641847
    const/4 v0, 0x0

    iput v0, p0, LX/3pw;->i:I

    .line 641848
    const/16 v0, 0x50

    iput v0, p0, LX/3pw;->k:I

    .line 641849
    return-void
.end method

.method public static a(LX/3pw;IZ)V
    .locals 2

    .prologue
    .line 641850
    if-eqz p2, :cond_0

    .line 641851
    iget v0, p0, LX/3pw;->b:I

    or-int/2addr v0, p1

    iput v0, p0, LX/3pw;->b:I

    .line 641852
    :goto_0
    return-void

    .line 641853
    :cond_0
    iget v0, p0, LX/3pw;->b:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, LX/3pw;->b:I

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2HB;)LX/2HB;
    .locals 5

    .prologue
    .line 641854
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 641855
    iget-object v0, p0, LX/3pw;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 641856
    const-string v2, "actions"

    sget-object v3, LX/3px;->a:LX/3pn;

    iget-object v0, p0, LX/3pw;->a:Ljava/util/ArrayList;

    iget-object v4, p0, LX/3pw;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [LX/3pb;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3pb;

    invoke-interface {v3, v0}, LX/3pn;->a([LX/3pb;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 641857
    :cond_0
    iget v0, p0, LX/3pw;->b:I

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    .line 641858
    const-string v0, "flags"

    iget v2, p0, LX/3pw;->b:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 641859
    :cond_1
    iget-object v0, p0, LX/3pw;->c:Landroid/app/PendingIntent;

    if-eqz v0, :cond_2

    .line 641860
    const-string v0, "displayIntent"

    iget-object v2, p0, LX/3pw;->c:Landroid/app/PendingIntent;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 641861
    :cond_2
    iget-object v0, p0, LX/3pw;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 641862
    const-string v2, "pages"

    iget-object v0, p0, LX/3pw;->d:Ljava/util/ArrayList;

    iget-object v3, p0, LX/3pw;->d:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Landroid/app/Notification;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 641863
    :cond_3
    iget-object v0, p0, LX/3pw;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    .line 641864
    const-string v0, "background"

    iget-object v2, p0, LX/3pw;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 641865
    :cond_4
    iget v0, p0, LX/3pw;->f:I

    if-eqz v0, :cond_5

    .line 641866
    const-string v0, "contentIcon"

    iget v2, p0, LX/3pw;->f:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 641867
    :cond_5
    iget v0, p0, LX/3pw;->g:I

    const v2, 0x800005

    if-eq v0, v2, :cond_6

    .line 641868
    const-string v0, "contentIconGravity"

    iget v2, p0, LX/3pw;->g:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 641869
    :cond_6
    iget v0, p0, LX/3pw;->h:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_7

    .line 641870
    const-string v0, "contentActionIndex"

    iget v2, p0, LX/3pw;->h:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 641871
    :cond_7
    iget v0, p0, LX/3pw;->i:I

    if-eqz v0, :cond_8

    .line 641872
    const-string v0, "customSizePreset"

    iget v2, p0, LX/3pw;->i:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 641873
    :cond_8
    iget v0, p0, LX/3pw;->j:I

    if-eqz v0, :cond_9

    .line 641874
    const-string v0, "customContentHeight"

    iget v2, p0, LX/3pw;->j:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 641875
    :cond_9
    iget v0, p0, LX/3pw;->k:I

    const/16 v2, 0x50

    if-eq v0, v2, :cond_a

    .line 641876
    const-string v0, "gravity"

    iget v2, p0, LX/3pw;->k:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 641877
    :cond_a
    iget v0, p0, LX/3pw;->l:I

    if-eqz v0, :cond_b

    .line 641878
    const-string v0, "hintScreenTimeout"

    iget v2, p0, LX/3pw;->l:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 641879
    :cond_b
    invoke-virtual {p1}, LX/2HB;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "android.wearable.EXTENSIONS"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 641880
    return-object p1
.end method

.method public final a(LX/3pb;)LX/3pw;
    .locals 1

    .prologue
    .line 641881
    iget-object v0, p0, LX/3pw;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 641882
    return-object p0
.end method

.method public final a(Landroid/app/Notification;)LX/3pw;
    .locals 1

    .prologue
    .line 641883
    iget-object v0, p0, LX/3pw;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 641884
    return-object p0
.end method

.method public final a(Z)LX/3pw;
    .locals 1

    .prologue
    .line 641885
    const/16 v0, 0x8

    invoke-static {p0, v0, p1}, LX/3pw;->a(LX/3pw;IZ)V

    .line 641886
    return-object p0
.end method

.method public final clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 641887
    new-instance v0, LX/3pw;

    invoke-direct {v0}, LX/3pw;-><init>()V

    .line 641888
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, LX/3pw;->a:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, LX/3pw;->a:Ljava/util/ArrayList;

    .line 641889
    iget v1, p0, LX/3pw;->b:I

    iput v1, v0, LX/3pw;->b:I

    .line 641890
    iget-object v1, p0, LX/3pw;->c:Landroid/app/PendingIntent;

    iput-object v1, v0, LX/3pw;->c:Landroid/app/PendingIntent;

    .line 641891
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, LX/3pw;->d:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, LX/3pw;->d:Ljava/util/ArrayList;

    .line 641892
    iget-object v1, p0, LX/3pw;->e:Landroid/graphics/Bitmap;

    iput-object v1, v0, LX/3pw;->e:Landroid/graphics/Bitmap;

    .line 641893
    iget v1, p0, LX/3pw;->f:I

    iput v1, v0, LX/3pw;->f:I

    .line 641894
    iget v1, p0, LX/3pw;->g:I

    iput v1, v0, LX/3pw;->g:I

    .line 641895
    iget v1, p0, LX/3pw;->h:I

    iput v1, v0, LX/3pw;->h:I

    .line 641896
    iget v1, p0, LX/3pw;->i:I

    iput v1, v0, LX/3pw;->i:I

    .line 641897
    iget v1, p0, LX/3pw;->j:I

    iput v1, v0, LX/3pw;->j:I

    .line 641898
    iget v1, p0, LX/3pw;->k:I

    iput v1, v0, LX/3pw;->k:I

    .line 641899
    iget v1, p0, LX/3pw;->l:I

    iput v1, v0, LX/3pw;->l:I

    .line 641900
    return-object v0
.end method
