.class public final LX/43z;
.super Landroid/telephony/PhoneStateListener;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/common/carrier/CarrierMonitor$3;


# direct methods
.method public constructor <init>(Lcom/facebook/common/carrier/CarrierMonitor$3;)V
    .locals 0

    .prologue
    .line 669319
    iput-object p1, p0, LX/43z;->a:Lcom/facebook/common/carrier/CarrierMonitor$3;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V
    .locals 3

    .prologue
    .line 669320
    iget-object v0, p0, LX/43z;->a:Lcom/facebook/common/carrier/CarrierMonitor$3;

    iget-object v0, v0, Lcom/facebook/common/carrier/CarrierMonitor$3;->b:LX/18b;

    .line 669321
    iput-object p1, v0, LX/18b;->n:Landroid/telephony/SignalStrength;

    .line 669322
    iget-object v0, p0, LX/43z;->a:Lcom/facebook/common/carrier/CarrierMonitor$3;

    iget-object v0, v0, Lcom/facebook/common/carrier/CarrierMonitor$3;->b:LX/18b;

    iget-object v0, v0, LX/18b;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/440;

    .line 669323
    iget-boolean v2, v0, LX/440;->b:Z

    move v2, v2

    .line 669324
    if-eqz v2, :cond_0

    .line 669325
    invoke-virtual {v0, p1}, LX/440;->a(Landroid/telephony/SignalStrength;)V

    goto :goto_0

    .line 669326
    :cond_1
    return-void
.end method
