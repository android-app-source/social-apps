.class public final enum LX/3WA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3WA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3WA;

.field public static final enum PREFETCH:LX/3WA;

.field public static final enum REFRESH:LX/3WA;

.field public static final enum UNKNOWN:LX/3WA;

.field public static final enum USER_INITIATED:LX/3WA;


# instance fields
.field private final mFeedbackRequestPriority:Lcom/facebook/http/interfaces/RequestPriority;

.field private final mMetadataRequestPriority:Lcom/facebook/http/interfaces/RequestPriority;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 589154
    new-instance v0, LX/3WA;

    const-string v1, "USER_INITIATED"

    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-direct {v0, v1, v4, v2, v3}, LX/3WA;-><init>(Ljava/lang/String;ILcom/facebook/http/interfaces/RequestPriority;Lcom/facebook/http/interfaces/RequestPriority;)V

    sput-object v0, LX/3WA;->USER_INITIATED:LX/3WA;

    .line 589155
    new-instance v0, LX/3WA;

    const-string v1, "REFRESH"

    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-direct {v0, v1, v5, v2, v3}, LX/3WA;-><init>(Ljava/lang/String;ILcom/facebook/http/interfaces/RequestPriority;Lcom/facebook/http/interfaces/RequestPriority;)V

    sput-object v0, LX/3WA;->REFRESH:LX/3WA;

    .line 589156
    new-instance v0, LX/3WA;

    const-string v1, "PREFETCH"

    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-direct {v0, v1, v6, v2, v3}, LX/3WA;-><init>(Ljava/lang/String;ILcom/facebook/http/interfaces/RequestPriority;Lcom/facebook/http/interfaces/RequestPriority;)V

    sput-object v0, LX/3WA;->PREFETCH:LX/3WA;

    .line 589157
    new-instance v0, LX/3WA;

    const-string v1, "UNKNOWN"

    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-direct {v0, v1, v7, v2, v3}, LX/3WA;-><init>(Ljava/lang/String;ILcom/facebook/http/interfaces/RequestPriority;Lcom/facebook/http/interfaces/RequestPriority;)V

    sput-object v0, LX/3WA;->UNKNOWN:LX/3WA;

    .line 589158
    const/4 v0, 0x4

    new-array v0, v0, [LX/3WA;

    sget-object v1, LX/3WA;->USER_INITIATED:LX/3WA;

    aput-object v1, v0, v4

    sget-object v1, LX/3WA;->REFRESH:LX/3WA;

    aput-object v1, v0, v5

    sget-object v1, LX/3WA;->PREFETCH:LX/3WA;

    aput-object v1, v0, v6

    sget-object v1, LX/3WA;->UNKNOWN:LX/3WA;

    aput-object v1, v0, v7

    sput-object v0, LX/3WA;->$VALUES:[LX/3WA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/facebook/http/interfaces/RequestPriority;Lcom/facebook/http/interfaces/RequestPriority;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/interfaces/RequestPriority;",
            "Lcom/facebook/http/interfaces/RequestPriority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 589159
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 589160
    iput-object p3, p0, LX/3WA;->mFeedbackRequestPriority:Lcom/facebook/http/interfaces/RequestPriority;

    .line 589161
    iput-object p4, p0, LX/3WA;->mMetadataRequestPriority:Lcom/facebook/http/interfaces/RequestPriority;

    .line 589162
    return-void
.end method

.method public static getFetchCauseFromName(Ljava/lang/String;)LX/3WA;
    .locals 5

    .prologue
    .line 589163
    invoke-static {}, LX/3WA;->values()[LX/3WA;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 589164
    invoke-virtual {v0}, LX/3WA;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 589165
    :goto_1
    return-object v0

    .line 589166
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 589167
    :cond_1
    sget-object v0, LX/3WA;->UNKNOWN:LX/3WA;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/3WA;
    .locals 1

    .prologue
    .line 589153
    const-class v0, LX/3WA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3WA;

    return-object v0
.end method

.method public static values()[LX/3WA;
    .locals 1

    .prologue
    .line 589150
    sget-object v0, LX/3WA;->$VALUES:[LX/3WA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3WA;

    return-object v0
.end method


# virtual methods
.method public final getFeedbackRecommendedRequestPriority()Lcom/facebook/http/interfaces/RequestPriority;
    .locals 1

    .prologue
    .line 589152
    iget-object v0, p0, LX/3WA;->mFeedbackRequestPriority:Lcom/facebook/http/interfaces/RequestPriority;

    return-object v0
.end method

.method public final getMetadataRecommendedRequestPriority()Lcom/facebook/http/interfaces/RequestPriority;
    .locals 1

    .prologue
    .line 589151
    iget-object v0, p0, LX/3WA;->mMetadataRequestPriority:Lcom/facebook/http/interfaces/RequestPriority;

    return-object v0
.end method
