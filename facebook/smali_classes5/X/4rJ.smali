.class public final LX/4rJ;
.super LX/0mE;
.source ""


# instance fields
.field public final a:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 815437
    invoke-direct {p0}, LX/0mE;-><init>()V

    iput-object p1, p0, LX/4rJ;->a:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final B()Ljava/lang/String;
    .locals 1

    .prologue
    .line 815470
    iget-object v0, p0, LX/4rJ;->a:Ljava/lang/Object;

    if-nez v0, :cond_0

    const-string v0, "null"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/4rJ;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(D)D
    .locals 1

    .prologue
    .line 815467
    iget-object v0, p0, LX/4rJ;->a:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Number;

    if-eqz v0, :cond_0

    .line 815468
    iget-object v0, p0, LX/4rJ;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide p1

    .line 815469
    :cond_0
    return-wide p1
.end method

.method public final a(J)J
    .locals 1

    .prologue
    .line 815464
    iget-object v0, p0, LX/4rJ;->a:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Number;

    if-eqz v0, :cond_0

    .line 815465
    iget-object v0, p0, LX/4rJ;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide p1

    .line 815466
    :cond_0
    return-wide p1
.end method

.method public final a()LX/15z;
    .locals 1

    .prologue
    .line 815463
    sget-object v0, LX/15z;->VALUE_EMBEDDED_OBJECT:LX/15z;

    return-object v0
.end method

.method public final a(Z)Z
    .locals 1

    .prologue
    .line 815460
    iget-object v0, p0, LX/4rJ;->a:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4rJ;->a:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 815461
    iget-object v0, p0, LX/4rJ;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    .line 815462
    :cond_0
    return p1
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 815457
    iget-object v0, p0, LX/4rJ;->a:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Number;

    if-eqz v0, :cond_0

    .line 815458
    iget-object v0, p0, LX/4rJ;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result p1

    .line 815459
    :cond_0
    return p1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 815448
    if-ne p1, p0, :cond_1

    .line 815449
    :cond_0
    :goto_0
    return v0

    .line 815450
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    .line 815451
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 815452
    goto :goto_0

    .line 815453
    :cond_3
    check-cast p1, LX/4rJ;

    .line 815454
    iget-object v2, p0, LX/4rJ;->a:Ljava/lang/Object;

    if-nez v2, :cond_4

    .line 815455
    iget-object v2, p1, LX/4rJ;->a:Ljava/lang/Object;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 815456
    :cond_4
    iget-object v0, p0, LX/4rJ;->a:Ljava/lang/Object;

    iget-object v1, p1, LX/4rJ;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 815447
    iget-object v0, p0, LX/4rJ;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final k()LX/0nH;
    .locals 1

    .prologue
    .line 815446
    sget-object v0, LX/0nH;->POJO:LX/0nH;

    return-object v0
.end method

.method public final serialize(LX/0nX;LX/0my;)V
    .locals 1

    .prologue
    .line 815442
    iget-object v0, p0, LX/4rJ;->a:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 815443
    invoke-virtual {p2, p1}, LX/0my;->a(LX/0nX;)V

    .line 815444
    :goto_0
    return-void

    .line 815445
    :cond_0
    iget-object v0, p0, LX/4rJ;->a:Ljava/lang/Object;

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final t()[B
    .locals 1

    .prologue
    .line 815439
    iget-object v0, p0, LX/4rJ;->a:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-eqz v0, :cond_0

    .line 815440
    iget-object v0, p0, LX/4rJ;->a:Ljava/lang/Object;

    check-cast v0, [B

    check-cast v0, [B

    .line 815441
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, LX/0mE;->t()[B

    move-result-object v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 815438
    iget-object v0, p0, LX/4rJ;->a:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
