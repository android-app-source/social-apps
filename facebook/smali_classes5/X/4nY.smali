.class public final enum LX/4nY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4nY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4nY;

.field public static final enum COMMERCE_PAGE_TYPE_AGENT:LX/4nY;

.field public static final enum COMMERCE_PAGE_TYPE_BANK:LX/4nY;

.field public static final enum COMMERCE_PAGE_TYPE_BUSINESS:LX/4nY;

.field public static final enum COMMERCE_PAGE_TYPE_MESSENGER_EXTENSION:LX/4nY;

.field public static final enum COMMERCE_PAGE_TYPE_RIDE_SHARE:LX/4nY;

.field public static final enum COMMERCE_PAGE_TYPE_UNKNOWN:LX/4nY;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 806988
    new-instance v0, LX/4nY;

    const-string v1, "COMMERCE_PAGE_TYPE_AGENT"

    invoke-direct {v0, v1, v3}, LX/4nY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4nY;->COMMERCE_PAGE_TYPE_AGENT:LX/4nY;

    .line 806989
    new-instance v0, LX/4nY;

    const-string v1, "COMMERCE_PAGE_TYPE_BANK"

    invoke-direct {v0, v1, v4}, LX/4nY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4nY;->COMMERCE_PAGE_TYPE_BANK:LX/4nY;

    .line 806990
    new-instance v0, LX/4nY;

    const-string v1, "COMMERCE_PAGE_TYPE_BUSINESS"

    invoke-direct {v0, v1, v5}, LX/4nY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4nY;->COMMERCE_PAGE_TYPE_BUSINESS:LX/4nY;

    .line 806991
    new-instance v0, LX/4nY;

    const-string v1, "COMMERCE_PAGE_TYPE_RIDE_SHARE"

    invoke-direct {v0, v1, v6}, LX/4nY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4nY;->COMMERCE_PAGE_TYPE_RIDE_SHARE:LX/4nY;

    .line 806992
    new-instance v0, LX/4nY;

    const-string v1, "COMMERCE_PAGE_TYPE_UNKNOWN"

    invoke-direct {v0, v1, v7}, LX/4nY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4nY;->COMMERCE_PAGE_TYPE_UNKNOWN:LX/4nY;

    .line 806993
    new-instance v0, LX/4nY;

    const-string v1, "COMMERCE_PAGE_TYPE_MESSENGER_EXTENSION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/4nY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4nY;->COMMERCE_PAGE_TYPE_MESSENGER_EXTENSION:LX/4nY;

    .line 806994
    const/4 v0, 0x6

    new-array v0, v0, [LX/4nY;

    sget-object v1, LX/4nY;->COMMERCE_PAGE_TYPE_AGENT:LX/4nY;

    aput-object v1, v0, v3

    sget-object v1, LX/4nY;->COMMERCE_PAGE_TYPE_BANK:LX/4nY;

    aput-object v1, v0, v4

    sget-object v1, LX/4nY;->COMMERCE_PAGE_TYPE_BUSINESS:LX/4nY;

    aput-object v1, v0, v5

    sget-object v1, LX/4nY;->COMMERCE_PAGE_TYPE_RIDE_SHARE:LX/4nY;

    aput-object v1, v0, v6

    sget-object v1, LX/4nY;->COMMERCE_PAGE_TYPE_UNKNOWN:LX/4nY;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/4nY;->COMMERCE_PAGE_TYPE_MESSENGER_EXTENSION:LX/4nY;

    aput-object v2, v0, v1

    sput-object v0, LX/4nY;->$VALUES:[LX/4nY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 806995
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4nY;
    .locals 1

    .prologue
    .line 806996
    const-class v0, LX/4nY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4nY;

    return-object v0
.end method

.method public static values()[LX/4nY;
    .locals 1

    .prologue
    .line 806997
    sget-object v0, LX/4nY;->$VALUES:[LX/4nY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4nY;

    return-object v0
.end method
