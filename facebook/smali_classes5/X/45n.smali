.class public LX/45n;
.super LX/45m;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/45m",
        "<",
        "Lcom/facebook/common/jobscheduler/compat/GcmTaskServiceCompat;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 670554
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LX/45m;-><init>(Landroid/content/Context;I)V

    .line 670555
    iput-object p1, p0, LX/45n;->a:Landroid/content/Context;

    .line 670556
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/common/jobscheduler/compat/GcmTaskServiceCompat;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 670557
    invoke-static {p1}, Lcom/facebook/common/jobscheduler/compat/GcmTaskServiceCompat;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 670558
    iget-object v1, p0, LX/45n;->a:Landroid/content/Context;

    .line 670559
    invoke-static {v1}, LX/2E6;->a(Landroid/content/Context;)LX/2E6;

    move-result-object p0

    invoke-virtual {p0, v0, p2}, LX/2E6;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 670560
    invoke-static {v1, v0, p2}, Lcom/facebook/common/jobscheduler/compat/GcmTaskServiceCompat;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object p0

    .line 670561
    const/4 v0, 0x0

    const/high16 p2, 0x20000000

    invoke-static {v1, v0, p0, p2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p2

    .line 670562
    if-eqz p2, :cond_0

    .line 670563
    const-string v0, "alarm"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 670564
    invoke-virtual {v0, p2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 670565
    :cond_0
    return-void
.end method

.method public final a(LX/45u;Ljava/lang/Class;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/45u;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/common/jobscheduler/compat/GcmTaskServiceCompat;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 670566
    iget-wide v2, p1, LX/45u;->g:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    .line 670567
    new-instance v8, LX/2E9;

    invoke-direct {v8}, LX/2E9;-><init>()V

    .line 670568
    iget-wide v6, p1, LX/45u;->e:J

    const-wide/16 v10, 0x0

    cmp-long v6, v6, v10

    if-lez v6, :cond_3

    iget-wide v6, p1, LX/45u;->e:J

    .line 670569
    :goto_0
    sget-object v9, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v10, p1, LX/45u;->d:J

    invoke-virtual {v9, v10, v11}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v10

    sget-object v9, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v9, v6, v7}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v6

    invoke-virtual {v8, v10, v11, v6, v7}, LX/2E9;->a(JJ)LX/2E9;

    .line 670570
    move-object v2, v8

    .line 670571
    :goto_1
    invoke-virtual {v2, p2}, LX/2EA;->b(Ljava/lang/Class;)LX/2EA;

    .line 670572
    const/4 v4, 0x1

    .line 670573
    iget v3, p1, LX/45u;->a:I

    invoke-static {v3}, Lcom/facebook/common/jobscheduler/compat/GcmTaskServiceCompat;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2EA;->b(Ljava/lang/String;)LX/2EA;

    .line 670574
    invoke-virtual {v2, v4}, LX/2EA;->d(Z)LX/2EA;

    .line 670575
    iget v3, p1, LX/45u;->b:I

    packed-switch v3, :pswitch_data_0

    .line 670576
    :goto_2
    iget-boolean v3, p1, LX/45u;->c:Z

    if-eqz v3, :cond_0

    .line 670577
    iget-boolean v3, p1, LX/45u;->c:Z

    invoke-virtual {v2, v3}, LX/2EA;->e(Z)LX/2EA;

    .line 670578
    :cond_0
    iget-object v3, p1, LX/45u;->h:LX/46S;

    if-eqz v3, :cond_1

    .line 670579
    iget-object v3, p1, LX/45u;->h:LX/46S;

    check-cast v3, LX/46V;

    .line 670580
    iget-object v4, v3, LX/46V;->a:Landroid/os/Bundle;

    move-object v3, v4

    .line 670581
    invoke-virtual {v2, v3}, LX/2EA;->b(Landroid/os/Bundle;)LX/2EA;

    .line 670582
    :cond_1
    invoke-virtual {v2}, LX/2EA;->c()Lcom/google/android/gms/gcm/Task;

    move-result-object v2

    move-object v0, v2

    .line 670583
    iget-object v1, p0, LX/45n;->a:Landroid/content/Context;

    .line 670584
    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/facebook/common/jobscheduler/compat/GcmTaskServiceCompat;->a(Landroid/content/Context;Lcom/google/android/gms/gcm/Task;I)V

    .line 670585
    return-void

    .line 670586
    :cond_2
    new-instance v6, LX/4tw;

    invoke-direct {v6}, LX/4tw;-><init>()V

    .line 670587
    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v8, p1, LX/45u;->g:J

    invoke-virtual {v7, v8, v9}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v8

    iput-wide v8, v6, LX/4tw;->i:J

    .line 670588
    move-object v2, v6

    .line 670589
    goto :goto_1

    .line 670590
    :cond_3
    iget-wide v6, p1, LX/45u;->f:J

    goto :goto_0

    .line 670591
    :pswitch_0
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/2EA;->b(I)LX/2EA;

    goto :goto_2

    .line 670592
    :pswitch_1
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/2EA;->b(I)LX/2EA;

    goto :goto_2

    .line 670593
    :pswitch_2
    invoke-virtual {v2, v4}, LX/2EA;->b(I)LX/2EA;

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
