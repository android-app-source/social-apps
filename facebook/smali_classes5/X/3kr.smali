.class public LX/3kr;
.super LX/3ko;
.source ""


# instance fields
.field private final a:LX/0kb;

.field public final b:LX/17W;

.field public final c:LX/3kp;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public e:Landroid/content/Context;

.field public f:Landroid/view/View;

.field public g:LX/0hs;


# direct methods
.method public constructor <init>(LX/0kb;LX/17W;LX/3kp;LX/0Or;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/photos/upload/quality/PhotosHighDefUploadSettingValue;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0kb;",
            "LX/17W;",
            "LX/3kp;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 633329
    invoke-direct {p0}, LX/3ko;-><init>()V

    .line 633330
    iput-object p1, p0, LX/3kr;->a:LX/0kb;

    .line 633331
    iput-object p2, p0, LX/3kr;->b:LX/17W;

    .line 633332
    iput-object p3, p0, LX/3kr;->c:LX/3kp;

    .line 633333
    iput-object p4, p0, LX/3kr;->d:LX/0Or;

    .line 633334
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 3

    .prologue
    .line 633335
    iget-object v0, p0, LX/3kr;->a:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 633336
    iget-object v0, p0, LX/3kr;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz v1, :cond_0

    iget-object v0, p0, LX/3kr;->c:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3kr;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 633337
    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    .line 633338
    :goto_0
    return-object v0

    .line 633339
    :cond_1
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    .line 633340
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v0

    const/16 v2, 0xd

    if-ne v0, v2, :cond_4

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 633341
    if-eqz v0, :cond_3

    .line 633342
    :cond_2
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0

    .line 633343
    :cond_3
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 633344
    const-string v0, "4169"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 633345
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PHOTO_PICKER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 633346
    iput-object v0, p0, LX/3kr;->e:Landroid/content/Context;

    .line 633347
    iput-object v0, p0, LX/3kr;->f:Landroid/view/View;

    .line 633348
    iput-object v0, p0, LX/3kr;->g:LX/0hs;

    .line 633349
    return-void
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 633350
    iget-object v0, p0, LX/3kr;->f:Landroid/view/View;

    const v1, 0x7f0d00f3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 633351
    new-instance v1, LX/BIA;

    iget-object v2, p0, LX/3kr;->e:Landroid/content/Context;

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, LX/BIA;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, LX/3kr;->g:LX/0hs;

    .line 633352
    iget-object v1, p0, LX/3kr;->g:LX/0hs;

    const/4 v2, -0x1

    .line 633353
    iput v2, v1, LX/0hs;->t:I

    .line 633354
    iget-object v1, p0, LX/3kr;->g:LX/0hs;

    new-instance v2, LX/BHz;

    invoke-direct {v2, p0}, LX/BHz;-><init>(LX/3kr;)V

    invoke-virtual {v1, v2}, LX/0hs;->a(LX/5Od;)V

    .line 633355
    iget-object v1, p0, LX/3kr;->g:LX/0hs;

    new-instance v2, LX/BI0;

    invoke-direct {v2, p0}, LX/BI0;-><init>(LX/3kr;)V

    .line 633356
    iput-object v2, v1, LX/0ht;->H:LX/2dD;

    .line 633357
    iget-object v1, p0, LX/3kr;->g:LX/0hs;

    sget-object v2, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v1, v2}, LX/0ht;->a(LX/3AV;)V

    .line 633358
    iget-object v1, p0, LX/3kr;->g:LX/0hs;

    iget-object v2, p0, LX/3kr;->e:Landroid/content/Context;

    const v3, 0x7f081377

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 633359
    iget-object v1, p0, LX/3kr;->g:LX/0hs;

    iget-object v2, p0, LX/3kr;->e:Landroid/content/Context;

    const v3, 0x7f081378

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 633360
    iget-object v1, p0, LX/3kr;->g:LX/0hs;

    invoke-virtual {v1, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 633361
    return-void
.end method
