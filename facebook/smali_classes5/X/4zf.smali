.class public final LX/4zf;
.super Ljava/util/AbstractCollection;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractCollection",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0cn;


# direct methods
.method public constructor <init>(LX/0cn;)V
    .locals 0

    .prologue
    .line 823012
    iput-object p1, p0, LX/4zf;->a:LX/0cn;

    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 1

    .prologue
    .line 823010
    iget-object v0, p0, LX/4zf;->a:LX/0cn;

    invoke-virtual {v0}, LX/0cn;->clear()V

    .line 823011
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 823004
    iget-object v0, p0, LX/4zf;->a:LX/0cn;

    invoke-virtual {v0, p1}, LX/0cn;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 823009
    iget-object v0, p0, LX/4zf;->a:LX/0cn;

    invoke-virtual {v0}, LX/0cn;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 823008
    new-instance v0, LX/4ze;

    iget-object v1, p0, LX/4zf;->a:LX/0cn;

    invoke-direct {v0, v1}, LX/4ze;-><init>(LX/0cn;)V

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 823007
    iget-object v0, p0, LX/4zf;->a:LX/0cn;

    invoke-virtual {v0}, LX/0cn;->size()I

    move-result v0

    return v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 823006
    invoke-static {p0}, LX/0cn;->b(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">([TE;)[TE;"
        }
    .end annotation

    .prologue
    .line 823005
    invoke-static {p0}, LX/0cn;->b(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
