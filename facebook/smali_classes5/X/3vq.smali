.class public final LX/3vq;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field public final synthetic a:LX/3vu;


# direct methods
.method public constructor <init>(LX/3vu;)V
    .locals 0

    .prologue
    .line 653641
    iput-object p1, p0, LX/3vq;->a:LX/3vu;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 653642
    iget-object v0, p0, LX/3vq;->a:LX/3vu;

    iget-object v0, v0, LX/3vu;->g:Landroid/support/v7/widget/LinearLayoutCompat;

    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutCompat;->getChildCount()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 653643
    iget-object v0, p0, LX/3vq;->a:LX/3vu;

    iget-object v0, v0, LX/3vu;->g:Landroid/support/v7/widget/LinearLayoutCompat;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/LinearLayoutCompat;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/3vs;

    .line 653644
    iget-object p0, v0, LX/3vs;->c:LX/3u0;

    move-object v0, p0

    .line 653645
    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 653646
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 653647
    if-nez p2, :cond_0

    .line 653648
    iget-object v1, p0, LX/3vq;->a:LX/3vu;

    invoke-virtual {p0, p1}, LX/3vq;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3u0;

    const/4 v2, 0x1

    .line 653649
    new-instance p0, LX/3vs;

    invoke-virtual {v1}, LX/3vu;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p0, v1, p1, v0, v2}, LX/3vs;-><init>(LX/3vu;Landroid/content/Context;LX/3u0;Z)V

    .line 653650
    if-eqz v2, :cond_1

    .line 653651
    const/4 p1, 0x0

    invoke-virtual {p0, p1}, LX/3vs;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 653652
    new-instance p1, Landroid/widget/AbsListView$LayoutParams;

    const/4 p2, -0x1

    iget p3, v1, LX/3vu;->j:I

    invoke-direct {p1, p2, p3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1}, LX/3vs;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 653653
    :goto_0
    move-object p2, p0

    .line 653654
    :goto_1
    return-object p2

    :cond_0
    move-object v0, p2

    .line 653655
    check-cast v0, LX/3vs;

    invoke-virtual {p0, p1}, LX/3vq;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3u0;

    .line 653656
    iput-object v1, v0, LX/3vs;->c:LX/3u0;

    .line 653657
    invoke-static {v0}, LX/3vs;->b(LX/3vs;)V

    .line 653658
    goto :goto_1

    .line 653659
    :cond_1
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/3vs;->setFocusable(Z)V

    .line 653660
    iget-object p1, v1, LX/3vu;->f:LX/3vr;

    if-nez p1, :cond_2

    .line 653661
    new-instance p1, LX/3vr;

    invoke-direct {p1, v1}, LX/3vr;-><init>(LX/3vu;)V

    iput-object p1, v1, LX/3vu;->f:LX/3vr;

    .line 653662
    :cond_2
    iget-object p1, v1, LX/3vu;->f:LX/3vr;

    invoke-virtual {p0, p1}, LX/3vs;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
