.class public final enum LX/482;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/482;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/482;

.field public static final enum IMPERIAL:LX/482;

.field public static final enum METRIC:LX/482;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 672642
    new-instance v0, LX/482;

    const-string v1, "METRIC"

    invoke-direct {v0, v1, v2}, LX/482;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/482;->METRIC:LX/482;

    .line 672643
    new-instance v0, LX/482;

    const-string v1, "IMPERIAL"

    invoke-direct {v0, v1, v3}, LX/482;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/482;->IMPERIAL:LX/482;

    .line 672644
    const/4 v0, 0x2

    new-array v0, v0, [LX/482;

    sget-object v1, LX/482;->METRIC:LX/482;

    aput-object v1, v0, v2

    sget-object v1, LX/482;->IMPERIAL:LX/482;

    aput-object v1, v0, v3

    sput-object v0, LX/482;->$VALUES:[LX/482;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 672641
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static from(Ljava/util/Locale;)LX/482;
    .locals 1

    .prologue
    .line 672640
    invoke-virtual {p0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/482;->isImperial(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/482;->IMPERIAL:LX/482;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/482;->METRIC:LX/482;

    goto :goto_0
.end method

.method public static getDefault()LX/482;
    .locals 1

    .prologue
    .line 672639
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, LX/482;->from(Ljava/util/Locale;)LX/482;

    move-result-object v0

    return-object v0
.end method

.method private static isImperial(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 672638
    const-string v0, "US"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "LR"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GB"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "MM"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/482;
    .locals 1

    .prologue
    .line 672637
    const-class v0, LX/482;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/482;

    return-object v0
.end method

.method public static values()[LX/482;
    .locals 1

    .prologue
    .line 672636
    sget-object v0, LX/482;->$VALUES:[LX/482;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/482;

    return-object v0
.end method
