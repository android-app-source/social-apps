.class public LX/3zM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private a:LX/0Zb;

.field public b:LX/3zF;

.field public c:I

.field public d:Landroid/os/Handler;

.field public e:Ljava/lang/Runnable;

.field public f:Z


# direct methods
.method public constructor <init>(LX/0Zb;LX/3zF;)V
    .locals 1

    .prologue
    .line 662395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 662396
    iput-object p1, p0, LX/3zM;->a:LX/0Zb;

    .line 662397
    iput-object p2, p0, LX/3zM;->b:LX/3zF;

    .line 662398
    const/4 v0, 0x0

    iput v0, p0, LX/3zM;->c:I

    .line 662399
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/3zM;->d:Landroid/os/Handler;

    .line 662400
    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 662421
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 662420
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 7

    .prologue
    .line 662401
    iget-boolean v0, p0, LX/3zM;->f:Z

    if-eqz v0, :cond_1

    .line 662402
    :cond_0
    :goto_0
    return-void

    .line 662403
    :cond_1
    if-lt p4, p3, :cond_0

    const/4 v1, 0x0

    .line 662404
    move v0, v1

    move p2, v1

    .line 662405
    :goto_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p3

    if-ge v0, p3, :cond_3

    .line 662406
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result p3

    const/16 p4, 0x20

    if-eq p3, p4, :cond_2

    .line 662407
    add-int/lit8 p2, p2, 0x1

    .line 662408
    :cond_2
    const/4 p3, 0x5

    if-lt p2, p3, :cond_4

    .line 662409
    const/4 v1, 0x1

    .line 662410
    :cond_3
    move v0, v1

    .line 662411
    if-eqz v0, :cond_0

    .line 662412
    iget-object v0, p0, LX/3zM;->b:LX/3zF;

    invoke-virtual {v0}, LX/3zF;->a()Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 662413
    if-eqz v0, :cond_0

    .line 662414
    iget-object v1, p0, LX/3zM;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 662415
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3zM;->f:Z

    .line 662416
    new-instance v2, Lcom/facebook/analytics/SelfCensorshipTextWatcher$1;

    invoke-direct {v2, p0}, Lcom/facebook/analytics/SelfCensorshipTextWatcher$1;-><init>(LX/3zM;)V

    iput-object v2, p0, LX/3zM;->e:Ljava/lang/Runnable;

    .line 662417
    iget-object v2, p0, LX/3zM;->d:Landroid/os/Handler;

    iget-object v3, p0, LX/3zM;->e:Ljava/lang/Runnable;

    const-wide/32 v4, 0x927c0

    const v6, 0x3bf50ddd

    invoke-static {v2, v3, v4, v5, v6}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 662418
    goto :goto_0

    .line 662419
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
