.class public final LX/4AJ;
.super LX/4AB;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "LX/4AB",
        "<TK;>;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public transient c:[LX/4AI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LX/4AI",
            "<TK;>;"
        }
    .end annotation
.end field

.field public transient d:LX/4AI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4AI",
            "<TK;>;"
        }
    .end annotation
.end field

.field public transient e:I

.field public transient f:I

.field private transient g:I

.field private transient h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation
.end field

.field private transient i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/dracula/runtime/jdk/DraculaMap$1$Dracula$Entry$1$Dracula",
            "<TK;>;>;"
        }
    .end annotation
.end field

.field private transient j:LX/39P;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 676002
    invoke-direct {p0}, LX/4AB;-><init>()V

    .line 676003
    const/4 v0, 0x2

    new-array v0, v0, [LX/4AI;

    check-cast v0, [LX/4AI;

    move-object v0, v0

    .line 676004
    iput-object v0, p0, LX/4AJ;->c:[LX/4AI;

    .line 676005
    const/4 v0, -0x1

    iput v0, p0, LX/4AJ;->g:I

    .line 676006
    return-void
.end method

.method private a(LX/15i;II)LX/1vs;
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 676007
    iget-object v0, p0, LX/4AJ;->d:LX/4AI;

    .line 676008
    if-nez v0, :cond_0

    .line 676009
    const/4 v6, 0x0

    .line 676010
    new-instance v5, LX/4AI;

    const/4 v10, 0x0

    move-object v7, p1

    move v8, p2

    move v9, p3

    move-object v11, v6

    invoke-direct/range {v5 .. v11}, LX/4AI;-><init>(Ljava/lang/Object;LX/15i;IIILX/4AI;)V

    iput-object v5, p0, LX/4AJ;->d:LX/4AI;

    .line 676011
    iget v0, p0, LX/4AJ;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4AJ;->e:I

    .line 676012
    iget v0, p0, LX/4AJ;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4AJ;->f:I

    .line 676013
    const/4 v0, 0x0

    invoke-static {v0, v1, v1}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    .line 676014
    :goto_0
    return-object v0

    .line 676015
    :cond_0
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 676016
    :try_start_0
    iget-object v2, v0, LX/4AI;->b:LX/15i;

    .line 676017
    iget v3, v0, LX/4AI;->c:I

    .line 676018
    iget v4, v0, LX/4AI;->d:I

    .line 676019
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 676020
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 676021
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 676022
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 676023
    :try_start_2
    iput-object p1, v0, LX/4AI;->b:LX/15i;

    .line 676024
    iput p2, v0, LX/4AI;->c:I

    .line 676025
    iput p3, v0, LX/4AI;->d:I

    .line 676026
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 676027
    invoke-static {v2, v3, v4}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    goto :goto_0

    .line 676028
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 676029
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 676030
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0
.end method

.method private static a(Ljava/lang/Object;LX/15i;IIILX/4AI;)LX/4AI;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "LX/15i;",
            "III",
            "LX/4AI",
            "<TK;>;)",
            "LX/4AI",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 676031
    new-instance v0, LX/4AI;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/4AI;-><init>(Ljava/lang/Object;LX/15i;IIILX/4AI;)V

    return-object v0
.end method

.method private a(Ljava/lang/Object;LX/15i;IIII)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "LX/15i;",
            "IIII)V"
        }
    .end annotation

    .prologue
    .line 676032
    iget-object v7, p0, LX/4AJ;->c:[LX/4AI;

    new-instance v0, LX/4AI;

    iget-object v1, p0, LX/4AJ;->c:[LX/4AI;

    aget-object v6, v1, p6

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, LX/4AI;-><init>(Ljava/lang/Object;LX/15i;IIILX/4AI;)V

    aput-object v0, v7, p6

    .line 676033
    return-void
.end method

.method private static a(LX/4AJ;I)[LX/4AI;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)[",
            "LX/4AI",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 676116
    new-array v0, p1, [LX/4AI;

    check-cast v0, [LX/4AI;

    check-cast v0, [LX/4AI;

    .line 676117
    iput-object v0, p0, LX/4AJ;->c:[LX/4AI;

    .line 676118
    shr-int/lit8 v1, p1, 0x1

    shr-int/lit8 v2, p1, 0x2

    add-int/2addr v1, v2

    iput v1, p0, LX/4AJ;->g:I

    .line 676119
    return-object v0
.end method

.method public static b(LX/4AJ;Ljava/lang/Object;LX/15i;II)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "LX/15i;",
            "II)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 676034
    if-nez p1, :cond_1

    .line 676035
    iget-object v1, p0, LX/4AJ;->d:LX/4AI;

    .line 676036
    if-nez v1, :cond_0

    .line 676037
    const/4 v4, 0x0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move-object v5, v0

    invoke-static/range {v0 .. v5}, LX/4AJ;->a(Ljava/lang/Object;LX/15i;IIILX/4AI;)LX/4AI;

    move-result-object v0

    iput-object v0, p0, LX/4AJ;->d:LX/4AI;

    .line 676038
    iget v0, p0, LX/4AJ;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4AJ;->e:I

    .line 676039
    :goto_0
    return-void

    .line 676040
    :cond_0
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 676041
    :try_start_0
    iput-object p2, v1, LX/4AI;->b:LX/15i;

    .line 676042
    iput p3, v1, LX/4AI;->c:I

    .line 676043
    iput p4, v1, LX/4AI;->d:I

    .line 676044
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 676045
    :cond_1
    invoke-static {p1}, LX/4AE;->a(Ljava/lang/Object;)I

    move-result v4

    .line 676046
    iget-object v6, p0, LX/4AJ;->c:[LX/4AI;

    .line 676047
    array-length v0, v6

    add-int/lit8 v0, v0, -0x1

    and-int v7, v4, v0

    .line 676048
    aget-object v5, v6, v7

    move-object v0, v5

    .line 676049
    :goto_1
    if-eqz v0, :cond_3

    .line 676050
    iget v1, v0, LX/4AI;->e:I

    if-ne v1, v4, :cond_2

    iget-object v1, v0, LX/4AI;->a:Ljava/lang/Object;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 676051
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 676052
    :try_start_1
    iput-object p2, v0, LX/4AI;->b:LX/15i;

    .line 676053
    iput p3, v0, LX/4AI;->c:I

    .line 676054
    iput p4, v0, LX/4AI;->d:I

    .line 676055
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    .line 676056
    :cond_2
    iget-object v0, v0, LX/4AI;->f:LX/4AI;

    goto :goto_1

    :cond_3
    move-object v0, p1

    move-object v1, p2

    move v2, p3

    move v3, p4

    .line 676057
    invoke-static/range {v0 .. v5}, LX/4AJ;->a(Ljava/lang/Object;LX/15i;IIILX/4AI;)LX/4AI;

    move-result-object v0

    aput-object v0, v6, v7

    .line 676058
    iget v0, p0, LX/4AJ;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4AJ;->e:I

    goto :goto_0
.end method

.method public static c(LX/4AJ;Ljava/lang/Object;LX/15i;II)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 676059
    if-nez p1, :cond_1

    .line 676060
    iget-object v1, p0, LX/4AJ;->d:LX/4AI;

    .line 676061
    if-eqz v1, :cond_0

    .line 676062
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 676063
    :try_start_0
    iget-object v0, v1, LX/4AI;->b:LX/15i;

    .line 676064
    iget v1, v1, LX/4AI;->c:I

    .line 676065
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 676066
    invoke-static {p2, p3, v0, v1}, LX/4A1;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    .line 676067
    :cond_0
    :goto_0
    return v0

    .line 676068
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 676069
    :cond_1
    invoke-static {p1}, LX/4AE;->a(Ljava/lang/Object;)I

    move-result v2

    .line 676070
    iget-object v1, p0, LX/4AJ;->c:[LX/4AI;

    .line 676071
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    and-int/2addr v3, v2

    .line 676072
    aget-object v1, v1, v3

    :goto_1
    if-eqz v1, :cond_0

    .line 676073
    iget v3, v1, LX/4AI;->e:I

    if-ne v3, v2, :cond_2

    iget-object v3, v1, LX/4AI;->a:Ljava/lang/Object;

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 676074
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 676075
    :try_start_2
    iget-object v0, v1, LX/4AI;->b:LX/15i;

    .line 676076
    iget v1, v1, LX/4AI;->c:I

    .line 676077
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 676078
    invoke-static {p2, p3, v0, v1}, LX/4A1;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    goto :goto_0

    .line 676079
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 676080
    :cond_2
    iget-object v1, v1, LX/4AI;->f:LX/4AI;

    goto :goto_1
.end method

.method public static d(LX/4AJ;Ljava/lang/Object;LX/15i;II)Z
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 676081
    if-nez p1, :cond_4

    .line 676082
    iget-object v0, p0, LX/4AJ;->d:LX/4AI;

    .line 676083
    if-nez v0, :cond_1

    move v0, v1

    .line 676084
    :goto_0
    if-eqz v0, :cond_3

    .line 676085
    :cond_0
    :goto_1
    return v2

    .line 676086
    :cond_1
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    .line 676087
    :try_start_0
    iget-object v5, v0, LX/4AI;->b:LX/15i;

    .line 676088
    iget v0, v0, LX/4AI;->c:I

    .line 676089
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 676090
    invoke-static {p2, p3, v5, v0}, LX/4A1;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    .line 676091
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move v0, v2

    .line 676092
    goto :goto_0

    .line 676093
    :cond_3
    iput-object v3, p0, LX/4AJ;->d:LX/4AI;

    .line 676094
    iget v0, p0, LX/4AJ;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4AJ;->f:I

    .line 676095
    iget v0, p0, LX/4AJ;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/4AJ;->e:I

    move v2, v1

    .line 676096
    goto :goto_1

    .line 676097
    :cond_4
    invoke-static {p1}, LX/4AE;->a(Ljava/lang/Object;)I

    move-result v4

    .line 676098
    iget-object v5, p0, LX/4AJ;->c:[LX/4AI;

    .line 676099
    array-length v0, v5

    add-int/lit8 v0, v0, -0x1

    and-int v6, v4, v0

    .line 676100
    aget-object v0, v5, v6

    move-object v9, v3

    move-object v3, v0

    move-object v0, v9

    .line 676101
    :goto_2
    if-eqz v3, :cond_0

    .line 676102
    iget v7, v3, LX/4AI;->e:I

    if-ne v7, v4, :cond_6

    iget-object v7, v3, LX/4AI;->a:Ljava/lang/Object;

    invoke-virtual {p1, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 676103
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    .line 676104
    :try_start_2
    iget-object v7, v3, LX/4AI;->b:LX/15i;

    .line 676105
    iget v8, v3, LX/4AI;->c:I

    .line 676106
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 676107
    invoke-static {p2, p3, v7, v8}, LX/4A1;->a(LX/15i;ILX/15i;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 676108
    if-nez v0, :cond_5

    .line 676109
    iget-object v0, v3, LX/4AI;->f:LX/4AI;

    aput-object v0, v5, v6

    .line 676110
    :goto_3
    iget v0, p0, LX/4AJ;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4AJ;->f:I

    .line 676111
    iget v0, p0, LX/4AJ;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/4AJ;->e:I

    move v2, v1

    .line 676112
    goto :goto_1

    .line 676113
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 676114
    :cond_5
    iget-object v2, v3, LX/4AI;->f:LX/4AI;

    iput-object v2, v0, LX/4AI;->f:LX/4AI;

    goto :goto_3

    .line 676115
    :cond_6
    iget-object v0, v3, LX/4AI;->f:LX/4AI;

    move-object v9, v3

    move-object v3, v0

    move-object v0, v9

    goto :goto_2
.end method

.method private g()[LX/4AI;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()[",
            "LX/4AI",
            "<TK;>;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 675966
    iget-object v4, p0, LX/4AJ;->c:[LX/4AI;

    .line 675967
    array-length v9, v4

    .line 675968
    const/high16 v0, 0x40000000    # 2.0f

    if-ne v9, v0, :cond_0

    move-object v0, v4

    .line 675969
    :goto_0
    return-object v0

    .line 675970
    :cond_0
    mul-int/lit8 v0, v9, 0x2

    .line 675971
    invoke-static {p0, v0}, LX/4AJ;->a(LX/4AJ;I)[LX/4AI;

    move-result-object v5

    .line 675972
    iget v0, p0, LX/4AJ;->e:I

    if-nez v0, :cond_1

    move-object v0, v5

    .line 675973
    goto :goto_0

    .line 675974
    :cond_1
    const/4 v0, 0x0

    move v8, v0

    :goto_1
    if-ge v8, v9, :cond_5

    .line 675975
    aget-object v6, v4, v8

    .line 675976
    if-eqz v6, :cond_4

    .line 675977
    iget v0, v6, LX/4AI;->e:I

    and-int v3, v0, v9

    .line 675978
    or-int v0, v8, v3

    aput-object v6, v5, v0

    .line 675979
    iget-object v0, v6, LX/4AI;->f:LX/4AI;

    move-object v1, v7

    move-object v10, v0

    move-object v0, v6

    move-object v6, v10

    :goto_2
    if-eqz v6, :cond_3

    .line 675980
    iget v2, v6, LX/4AI;->e:I

    and-int/2addr v2, v9

    .line 675981
    if-eq v2, v3, :cond_6

    .line 675982
    if-nez v1, :cond_2

    or-int v1, v8, v2

    aput-object v6, v5, v1

    :goto_3
    move v1, v2

    .line 675983
    :goto_4
    iget-object v2, v6, LX/4AI;->f:LX/4AI;

    move v3, v1

    move-object v1, v0

    move-object v0, v6

    move-object v6, v2

    goto :goto_2

    .line 675984
    :cond_2
    iput-object v6, v1, LX/4AI;->f:LX/4AI;

    goto :goto_3

    .line 675985
    :cond_3
    if-eqz v1, :cond_4

    iput-object v7, v1, LX/4AI;->f:LX/4AI;

    .line 675986
    :cond_4
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_1

    :cond_5
    move-object v0, v5

    .line 675987
    goto :goto_0

    :cond_6
    move-object v0, v1

    move v1, v3

    goto :goto_4
.end method

.method private h()LX/1vs;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 675988
    iget-object v0, p0, LX/4AJ;->d:LX/4AI;

    .line 675989
    if-nez v0, :cond_0

    .line 675990
    invoke-static {v2, v1, v1}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    .line 675991
    :goto_0
    return-object v0

    .line 675992
    :cond_0
    iput-object v2, p0, LX/4AJ;->d:LX/4AI;

    .line 675993
    iget v1, p0, LX/4AJ;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/4AJ;->f:I

    .line 675994
    iget v1, p0, LX/4AJ;->e:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/4AJ;->e:I

    .line 675995
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 675996
    :try_start_0
    iget-object v2, v0, LX/4AI;->b:LX/15i;

    .line 675997
    iget v3, v0, LX/4AI;->c:I

    .line 675998
    iget v0, v0, LX/4AI;->d:I

    .line 675999
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 676000
    invoke-static {v2, v3, v0}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    goto :goto_0

    .line 676001
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/15i;II)LX/1vs;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "LX/15i;",
            "II)",
            "LX/1vs;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 675843
    if-nez p1, :cond_0

    .line 675844
    invoke-direct {p0, p2, p3, p4}, LX/4AJ;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    .line 675845
    iget-object v1, v0, LX/1vs;->a:LX/15i;

    .line 675846
    iget v2, v0, LX/1vs;->b:I

    .line 675847
    iget v0, v0, LX/1vs;->c:I

    .line 675848
    invoke-static {v1, v2, v0}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    .line 675849
    :goto_0
    return-object v0

    .line 675850
    :cond_0
    invoke-static {p1}, LX/4AE;->a(Ljava/lang/Object;)I

    move-result v5

    .line 675851
    iget-object v0, p0, LX/4AJ;->c:[LX/4AI;

    .line 675852
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    and-int v6, v5, v1

    .line 675853
    aget-object v0, v0, v6

    :goto_1
    if-eqz v0, :cond_2

    .line 675854
    iget v1, v0, LX/4AI;->e:I

    if-ne v1, v5, :cond_1

    iget-object v1, v0, LX/4AI;->a:Ljava/lang/Object;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 675855
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 675856
    :try_start_0
    iget-object v2, v0, LX/4AI;->b:LX/15i;

    .line 675857
    iget v3, v0, LX/4AI;->c:I

    .line 675858
    iget v4, v0, LX/4AI;->d:I

    .line 675859
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 675860
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 675861
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 675862
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 675863
    :try_start_2
    iput-object p2, v0, LX/4AI;->b:LX/15i;

    .line 675864
    iput p3, v0, LX/4AI;->c:I

    .line 675865
    iput p4, v0, LX/4AI;->d:I

    .line 675866
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 675867
    invoke-static {v2, v3, v4}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    goto :goto_0

    .line 675868
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 675869
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 675870
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    .line 675871
    :cond_1
    iget-object v0, v0, LX/4AI;->f:LX/4AI;

    goto :goto_1

    .line 675872
    :cond_2
    iget v0, p0, LX/4AJ;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4AJ;->f:I

    .line 675873
    iget v0, p0, LX/4AJ;->e:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LX/4AJ;->e:I

    iget v1, p0, LX/4AJ;->g:I

    if-le v0, v1, :cond_3

    .line 675874
    invoke-direct {p0}, LX/4AJ;->g()[LX/4AI;

    move-result-object v0

    .line 675875
    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    and-int v6, v5, v0

    :cond_3
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    .line 675876
    invoke-direct/range {v0 .. v6}, LX/4AJ;->a(Ljava/lang/Object;LX/15i;IIII)V

    .line 675877
    const/4 v0, 0x0

    invoke-static {v0, v7, v7}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 675878
    iget v0, p0, LX/4AJ;->e:I

    if-eqz v0, :cond_0

    .line 675879
    iget-object v0, p0, LX/4AJ;->c:[LX/4AI;

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 675880
    iput-object v1, p0, LX/4AJ;->d:LX/4AI;

    .line 675881
    iget v0, p0, LX/4AJ;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4AJ;->f:I

    .line 675882
    const/4 v0, 0x0

    iput v0, p0, LX/4AJ;->e:I

    .line 675883
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 675955
    if-nez p1, :cond_2

    .line 675956
    iget-object v2, p0, LX/4AJ;->d:LX/4AI;

    if-eqz v2, :cond_1

    .line 675957
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 675958
    goto :goto_0

    .line 675959
    :cond_2
    invoke-static {p1}, LX/4AE;->a(Ljava/lang/Object;)I

    move-result v3

    .line 675960
    iget-object v2, p0, LX/4AJ;->c:[LX/4AI;

    .line 675961
    array-length v4, v2

    add-int/lit8 v4, v4, -0x1

    and-int/2addr v4, v3

    aget-object v2, v2, v4

    :goto_1
    if-eqz v2, :cond_4

    .line 675962
    iget-object v4, v2, LX/4AI;->a:Ljava/lang/Object;

    .line 675963
    if-eq v4, p1, :cond_0

    iget v5, v2, LX/4AI;->e:I

    if-ne v5, v3, :cond_3

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 675964
    :cond_3
    iget-object v2, v2, LX/4AI;->f:LX/4AI;

    goto :goto_1

    :cond_4
    move v0, v1

    .line 675965
    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/1vs;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 675884
    if-nez p1, :cond_1

    .line 675885
    iget-object v3, p0, LX/4AJ;->d:LX/4AI;

    .line 675886
    if-nez v3, :cond_0

    move-object v2, v1

    move v1, v0

    .line 675887
    :goto_0
    invoke-static {v2, v1, v0}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    .line 675888
    :goto_1
    return-object v0

    .line 675889
    :cond_0
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    .line 675890
    :try_start_0
    iget-object v2, v3, LX/4AI;->b:LX/15i;

    .line 675891
    iget v1, v3, LX/4AI;->c:I

    .line 675892
    iget v0, v3, LX/4AI;->d:I

    .line 675893
    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 675894
    :cond_1
    invoke-static {p1}, LX/4AE;->a(Ljava/lang/Object;)I

    move-result v3

    .line 675895
    iget-object v2, p0, LX/4AJ;->c:[LX/4AI;

    .line 675896
    array-length v4, v2

    add-int/lit8 v4, v4, -0x1

    and-int/2addr v4, v3

    aget-object v2, v2, v4

    :goto_2
    if-eqz v2, :cond_4

    .line 675897
    iget-object v4, v2, LX/4AI;->a:Ljava/lang/Object;

    .line 675898
    if-eq v4, p1, :cond_2

    iget v5, v2, LX/4AI;->e:I

    if-ne v5, v3, :cond_3

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 675899
    :cond_2
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 675900
    :try_start_1
    iget-object v0, v2, LX/4AI;->b:LX/15i;

    .line 675901
    iget v3, v2, LX/4AI;->c:I

    .line 675902
    iget v2, v2, LX/4AI;->d:I

    .line 675903
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 675904
    invoke-static {v0, v3, v2}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    goto :goto_1

    .line 675905
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 675906
    :cond_3
    iget-object v2, v2, LX/4AI;->f:LX/4AI;

    goto :goto_2

    .line 675907
    :cond_4
    invoke-static {v1, v0, v0}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    goto :goto_1
.end method

.method public final b()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/dracula/runtime/jdk/DraculaMap$1$Dracula$Entry$1$Dracula",
            "<TK;>;>;"
        }
    .end annotation

    .prologue
    .line 675908
    iget-object v0, p0, LX/4AJ;->i:Ljava/util/Set;

    .line 675909
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/4AH;

    invoke-direct {v0, p0}, LX/4AH;-><init>(LX/4AJ;)V

    iput-object v0, p0, LX/4AJ;->i:Ljava/util/Set;

    goto :goto_0
.end method

.method public final c(Ljava/lang/Object;)LX/1vs;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x0

    .line 675910
    if-nez p1, :cond_0

    .line 675911
    invoke-direct {p0}, LX/4AJ;->h()LX/1vs;

    move-result-object v0

    .line 675912
    iget-object v1, v0, LX/1vs;->a:LX/15i;

    .line 675913
    iget v2, v0, LX/1vs;->b:I

    .line 675914
    iget v0, v0, LX/1vs;->c:I

    .line 675915
    invoke-static {v1, v2, v0}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    .line 675916
    :goto_0
    return-object v0

    .line 675917
    :cond_0
    invoke-static {p1}, LX/4AE;->a(Ljava/lang/Object;)I

    move-result v3

    .line 675918
    iget-object v4, p0, LX/4AJ;->c:[LX/4AI;

    .line 675919
    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    and-int v5, v3, v0

    .line 675920
    aget-object v2, v4, v5

    move-object v0, v1

    .line 675921
    :goto_1
    if-eqz v2, :cond_3

    .line 675922
    iget v6, v2, LX/4AI;->e:I

    if-ne v6, v3, :cond_2

    iget-object v6, v2, LX/4AI;->a:Ljava/lang/Object;

    invoke-virtual {p1, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 675923
    if-nez v0, :cond_1

    .line 675924
    iget-object v0, v2, LX/4AI;->f:LX/4AI;

    aput-object v0, v4, v5

    .line 675925
    :goto_2
    iget v0, p0, LX/4AJ;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4AJ;->f:I

    .line 675926
    iget v0, p0, LX/4AJ;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/4AJ;->e:I

    .line 675927
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 675928
    :try_start_0
    iget-object v0, v2, LX/4AI;->b:LX/15i;

    .line 675929
    iget v3, v2, LX/4AI;->c:I

    .line 675930
    iget v2, v2, LX/4AI;->d:I

    .line 675931
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 675932
    invoke-static {v0, v3, v2}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    goto :goto_0

    .line 675933
    :cond_1
    iget-object v1, v2, LX/4AI;->f:LX/4AI;

    iput-object v1, v0, LX/4AI;->f:LX/4AI;

    goto :goto_2

    .line 675934
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 675935
    :cond_2
    iget-object v0, v2, LX/4AI;->f:LX/4AI;

    move-object v8, v2

    move-object v2, v0

    move-object v0, v8

    goto :goto_1

    .line 675936
    :cond_3
    invoke-static {v1, v7, v7}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 675937
    iget v0, p0, LX/4AJ;->e:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final clone()Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 675938
    :try_start_0
    invoke-super {p0}, LX/4AB;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4AJ;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 675939
    iget-object v1, p0, LX/4AJ;->c:[LX/4AI;

    array-length v1, v1

    invoke-static {v0, v1}, LX/4AJ;->a(LX/4AJ;I)[LX/4AI;

    .line 675940
    iput-object v2, v0, LX/4AJ;->d:LX/4AI;

    .line 675941
    const/4 v1, 0x0

    iput v1, v0, LX/4AJ;->e:I

    .line 675942
    iput-object v2, v0, LX/4AJ;->h:Ljava/util/Set;

    .line 675943
    iput-object v2, v0, LX/4AJ;->i:Ljava/util/Set;

    .line 675944
    iput-object v2, v0, LX/4AJ;->j:LX/39P;

    .line 675945
    invoke-virtual {p0}, LX/4AB;->b()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4AI;

    .line 675946
    invoke-virtual {v1}, LX/4AI;->b()LX/1vs;

    move-result-object v3

    .line 675947
    iget-object v4, v3, LX/1vs;->a:LX/15i;

    .line 675948
    iget v5, v3, LX/1vs;->b:I

    .line 675949
    iget v3, v3, LX/1vs;->c:I

    .line 675950
    iget-object p0, v1, LX/4AI;->a:Ljava/lang/Object;

    move-object v1, p0

    .line 675951
    invoke-static {v0, v1, v4, v5, v3}, LX/4AJ;->b(LX/4AJ;Ljava/lang/Object;LX/15i;II)V

    goto :goto_0

    .line 675952
    :cond_0
    return-object v0

    .line 675953
    :catch_0
    move-exception v0

    .line 675954
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 675842
    iget v0, p0, LX/4AJ;->e:I

    return v0
.end method
