.class public LX/3vd;
.super Landroid/view/ViewGroup;
.source ""


# instance fields
.field public a:LX/3rR;

.field public final b:LX/3vb;

.field private final c:Landroid/support/v7/internal/widget/ActivityChooserView$Callbacks;

.field private final d:Landroid/support/v7/widget/LinearLayoutCompat;

.field public final e:Landroid/widget/FrameLayout;

.field private final f:Landroid/widget/ImageView;

.field public final g:Landroid/widget/FrameLayout;

.field private final h:I

.field public final i:Landroid/database/DataSetObserver;

.field private final j:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private k:LX/3w1;

.field public l:Landroid/widget/PopupWindow$OnDismissListener;

.field public m:Z

.field public n:I

.field public o:Z

.field private p:I


# direct methods
.method public static a$redex0(LX/3vd;I)V
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 653306
    iget-object v0, p0, LX/3vd;->b:LX/3vb;

    .line 653307
    iget-object v3, v0, LX/3vb;->b:LX/3va;

    move-object v0, v3

    .line 653308
    if-nez v0, :cond_0

    .line 653309
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No data model. Did you call #setDataModel?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 653310
    :cond_0
    invoke-virtual {p0}, LX/3vd;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v3, p0, LX/3vd;->j:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 653311
    iget-object v0, p0, LX/3vd;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 653312
    :goto_0
    iget-object v3, p0, LX/3vd;->b:LX/3vb;

    .line 653313
    iget-object v4, v3, LX/3vb;->b:LX/3va;

    invoke-virtual {v4}, LX/3va;->a()I

    move-result v4

    move v4, v4

    .line 653314
    if-eqz v0, :cond_6

    move v3, v1

    .line 653315
    :goto_1
    const v5, 0x7fffffff

    if-eq p1, v5, :cond_7

    add-int/2addr v3, p1

    if-le v4, v3, :cond_7

    .line 653316
    iget-object v3, p0, LX/3vd;->b:LX/3vb;

    invoke-virtual {v3, v1}, LX/3vb;->a(Z)V

    .line 653317
    iget-object v3, p0, LX/3vd;->b:LX/3vb;

    add-int/lit8 v4, p1, -0x1

    invoke-virtual {v3, v4}, LX/3vb;->a(I)V

    .line 653318
    :goto_2
    invoke-direct {p0}, LX/3vd;->getListPopupWindow()LX/3w1;

    move-result-object v3

    .line 653319
    invoke-virtual {v3}, LX/3w1;->b()Z

    move-result v4

    if-nez v4, :cond_4

    .line 653320
    iget-boolean v4, p0, LX/3vd;->m:Z

    if-nez v4, :cond_1

    if-nez v0, :cond_8

    .line 653321
    :cond_1
    iget-object v2, p0, LX/3vd;->b:LX/3vb;

    invoke-virtual {v2, v1, v0}, LX/3vb;->a(ZZ)V

    .line 653322
    :goto_3
    iget-object v0, p0, LX/3vd;->b:LX/3vb;

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 653323
    iget v7, v0, LX/3vb;->c:I

    .line 653324
    const v4, 0x7fffffff

    iput v4, v0, LX/3vb;->c:I

    .line 653325
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 653326
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    .line 653327
    invoke-virtual {v0}, LX/3vb;->getCount()I

    move-result v10

    move-object v4, v5

    move v6, v2

    .line 653328
    :goto_4
    if-ge v2, v10, :cond_2

    .line 653329
    invoke-virtual {v0, v2, v4, v5}, LX/3vb;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 653330
    invoke-virtual {v4, v8, v9}, Landroid/view/View;->measure(II)V

    .line 653331
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    invoke-static {v6, p1}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 653332
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 653333
    :cond_2
    iput v7, v0, LX/3vb;->c:I

    .line 653334
    move v0, v6

    .line 653335
    iget v2, p0, LX/3vd;->h:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 653336
    invoke-virtual {v3, v0}, LX/3w1;->c(I)V

    .line 653337
    invoke-virtual {v3}, LX/3w1;->c()V

    .line 653338
    iget-object v0, p0, LX/3vd;->a:LX/3rR;

    if-eqz v0, :cond_3

    .line 653339
    iget-object v0, p0, LX/3vd;->a:LX/3rR;

    invoke-virtual {v0, v1}, LX/3rR;->a(Z)V

    .line 653340
    :cond_3
    iget-object v0, v3, LX/3w1;->f:LX/3wy;

    move-object v0, v0

    .line 653341
    invoke-virtual {p0}, LX/3vd;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f08000d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 653342
    :cond_4
    return-void

    :cond_5
    move v0, v2

    .line 653343
    goto/16 :goto_0

    :cond_6
    move v3, v2

    .line 653344
    goto/16 :goto_1

    .line 653345
    :cond_7
    iget-object v3, p0, LX/3vd;->b:LX/3vb;

    invoke-virtual {v3, v2}, LX/3vb;->a(Z)V

    .line 653346
    iget-object v3, p0, LX/3vd;->b:LX/3vb;

    invoke-virtual {v3, p1}, LX/3vb;->a(I)V

    goto :goto_2

    .line 653347
    :cond_8
    iget-object v0, p0, LX/3vd;->b:LX/3vb;

    invoke-virtual {v0, v2, v2}, LX/3vb;->a(ZZ)V

    goto :goto_3
.end method

.method public static c(LX/3vd;)Z
    .locals 1

    .prologue
    .line 653305
    invoke-direct {p0}, LX/3vd;->getListPopupWindow()LX/3w1;

    move-result-object v0

    invoke-virtual {v0}, LX/3w1;->b()Z

    move-result v0

    return v0
.end method

.method private getListPopupWindow()LX/3w1;
    .locals 2

    .prologue
    .line 653295
    iget-object v0, p0, LX/3vd;->k:LX/3w1;

    if-nez v0, :cond_0

    .line 653296
    new-instance v0, LX/3w1;

    invoke-virtual {p0}, LX/3vd;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3w1;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/3vd;->k:LX/3w1;

    .line 653297
    iget-object v0, p0, LX/3vd;->k:LX/3w1;

    iget-object v1, p0, LX/3vd;->b:LX/3vb;

    invoke-virtual {v0, v1}, LX/3w1;->a(Landroid/widget/ListAdapter;)V

    .line 653298
    iget-object v0, p0, LX/3vd;->k:LX/3w1;

    .line 653299
    iput-object p0, v0, LX/3w1;->r:Landroid/view/View;

    .line 653300
    iget-object v0, p0, LX/3vd;->k:LX/3w1;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/3w1;->a(Z)V

    .line 653301
    iget-object v0, p0, LX/3vd;->k:LX/3w1;

    iget-object v1, p0, LX/3vd;->c:Landroid/support/v7/internal/widget/ActivityChooserView$Callbacks;

    .line 653302
    iput-object v1, v0, LX/3w1;->t:Landroid/widget/AdapterView$OnItemClickListener;

    .line 653303
    iget-object v0, p0, LX/3vd;->k:LX/3w1;

    iget-object v1, p0, LX/3vd;->c:Landroid/support/v7/internal/widget/ActivityChooserView$Callbacks;

    invoke-virtual {v0, v1}, LX/3w1;->a(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 653304
    :cond_0
    iget-object v0, p0, LX/3vd;->k:LX/3w1;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 653289
    invoke-static {p0}, LX/3vd;->c(LX/3vd;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 653290
    invoke-direct {p0}, LX/3vd;->getListPopupWindow()LX/3w1;

    move-result-object v0

    invoke-virtual {v0}, LX/3w1;->a()V

    .line 653291
    invoke-virtual {p0}, LX/3vd;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 653292
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 653293
    iget-object v1, p0, LX/3vd;->j:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 653294
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public getDataModel()LX/3va;
    .locals 1

    .prologue
    .line 653286
    iget-object v0, p0, LX/3vd;->b:LX/3vb;

    .line 653287
    iget-object p0, v0, LX/3vb;->b:LX/3va;

    move-object v0, p0

    .line 653288
    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x36ddf05

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 653244
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 653245
    iget-object v1, p0, LX/3vd;->b:LX/3vb;

    .line 653246
    iget-object v2, v1, LX/3vb;->b:LX/3va;

    move-object v1, v2

    .line 653247
    if-eqz v1, :cond_0

    .line 653248
    iget-object v2, p0, LX/3vd;->i:Landroid/database/DataSetObserver;

    invoke-virtual {v1, v2}, LX/3va;->registerObserver(Ljava/lang/Object;)V

    .line 653249
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/3vd;->o:Z

    .line 653250
    const/16 v1, 0x2d

    const v2, -0x1a3b0cbf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x1af683e1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 653274
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 653275
    iget-object v1, p0, LX/3vd;->b:LX/3vb;

    .line 653276
    iget-object v2, v1, LX/3vb;->b:LX/3va;

    move-object v1, v2

    .line 653277
    if-eqz v1, :cond_0

    .line 653278
    iget-object v2, p0, LX/3vd;->i:Landroid/database/DataSetObserver;

    invoke-virtual {v1, v2}, LX/3va;->unregisterObserver(Ljava/lang/Object;)V

    .line 653279
    :cond_0
    invoke-virtual {p0}, LX/3vd;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    .line 653280
    invoke-virtual {v1}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 653281
    iget-object v2, p0, LX/3vd;->j:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 653282
    :cond_1
    invoke-static {p0}, LX/3vd;->c(LX/3vd;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 653283
    invoke-virtual {p0}, LX/3vd;->a()Z

    .line 653284
    :cond_2
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/3vd;->o:Z

    .line 653285
    const/16 v1, 0x2d

    const v2, -0x4898b4ac

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 653270
    iget-object v0, p0, LX/3vd;->d:Landroid/support/v7/widget/LinearLayoutCompat;

    sub-int v1, p4, p2

    sub-int v2, p5, p3

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/support/v7/widget/LinearLayoutCompat;->layout(IIII)V

    .line 653271
    invoke-static {p0}, LX/3vd;->c(LX/3vd;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 653272
    invoke-virtual {p0}, LX/3vd;->a()Z

    .line 653273
    :cond_0
    return-void
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 653348
    iget-object v0, p0, LX/3vd;->d:Landroid/support/v7/widget/LinearLayoutCompat;

    .line 653349
    iget-object v1, p0, LX/3vd;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    .line 653350
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 653351
    :cond_0
    invoke-virtual {p0, v0, p1, p2}, LX/3vd;->measureChild(Landroid/view/View;II)V

    .line 653352
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0, v1, v0}, LX/3vd;->setMeasuredDimension(II)V

    .line 653353
    return-void
.end method

.method public setActivityChooserModel(LX/3va;)V
    .locals 3

    .prologue
    .line 653253
    iget-object v0, p0, LX/3vd;->b:LX/3vb;

    .line 653254
    iget-object v1, v0, LX/3vb;->a:LX/3vd;

    iget-object v1, v1, LX/3vd;->b:LX/3vb;

    .line 653255
    iget-object v2, v1, LX/3vb;->b:LX/3va;

    move-object v1, v2

    .line 653256
    if-eqz v1, :cond_0

    iget-object v2, v0, LX/3vb;->a:LX/3vd;

    invoke-virtual {v2}, LX/3vd;->isShown()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 653257
    iget-object v2, v0, LX/3vb;->a:LX/3vd;

    iget-object v2, v2, LX/3vd;->i:Landroid/database/DataSetObserver;

    invoke-virtual {v1, v2}, LX/3va;->unregisterObserver(Ljava/lang/Object;)V

    .line 653258
    :cond_0
    iput-object p1, v0, LX/3vb;->b:LX/3va;

    .line 653259
    if-eqz p1, :cond_1

    iget-object v1, v0, LX/3vb;->a:LX/3vd;

    invoke-virtual {v1}, LX/3vd;->isShown()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 653260
    iget-object v1, v0, LX/3vb;->a:LX/3vd;

    iget-object v1, v1, LX/3vd;->i:Landroid/database/DataSetObserver;

    invoke-virtual {p1, v1}, LX/3va;->registerObserver(Ljava/lang/Object;)V

    .line 653261
    :cond_1
    const v1, 0x1852e4b5

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 653262
    invoke-static {p0}, LX/3vd;->c(LX/3vd;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 653263
    invoke-virtual {p0}, LX/3vd;->a()Z

    .line 653264
    const/4 v0, 0x0

    .line 653265
    invoke-static {p0}, LX/3vd;->c(LX/3vd;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-boolean v1, p0, LX/3vd;->o:Z

    if-nez v1, :cond_3

    .line 653266
    :cond_2
    :goto_0
    return-void

    .line 653267
    :cond_3
    iput-boolean v0, p0, LX/3vd;->m:Z

    .line 653268
    iget v0, p0, LX/3vd;->n:I

    invoke-static {p0, v0}, LX/3vd;->a$redex0(LX/3vd;I)V

    .line 653269
    goto :goto_0
.end method

.method public setDefaultActionButtonContentDescription(I)V
    .locals 0

    .prologue
    .line 653251
    iput p1, p0, LX/3vd;->p:I

    .line 653252
    return-void
.end method

.method public setExpandActivityOverflowButtonContentDescription(I)V
    .locals 2

    .prologue
    .line 653241
    invoke-virtual {p0}, LX/3vd;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 653242
    iget-object v1, p0, LX/3vd;->f:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 653243
    return-void
.end method

.method public setExpandActivityOverflowButtonDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 653239
    iget-object v0, p0, LX/3vd;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 653240
    return-void
.end method

.method public setInitialActivityCount(I)V
    .locals 0

    .prologue
    .line 653237
    iput p1, p0, LX/3vd;->n:I

    .line 653238
    return-void
.end method

.method public setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V
    .locals 0

    .prologue
    .line 653235
    iput-object p1, p0, LX/3vd;->l:Landroid/widget/PopupWindow$OnDismissListener;

    .line 653236
    return-void
.end method

.method public setProvider(LX/3rR;)V
    .locals 0

    .prologue
    .line 653233
    iput-object p1, p0, LX/3vd;->a:LX/3rR;

    .line 653234
    return-void
.end method
