.class public LX/3t3;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/3sv;


# instance fields
.field public final b:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 644449
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 644450
    new-instance v0, LX/3t2;

    invoke-direct {v0}, LX/3t2;-><init>()V

    sput-object v0, LX/3t3;->a:LX/3sv;

    .line 644451
    :goto_0
    return-void

    .line 644452
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 644453
    new-instance v0, LX/3sz;

    invoke-direct {v0}, LX/3sz;-><init>()V

    sput-object v0, LX/3t3;->a:LX/3sv;

    goto :goto_0

    .line 644454
    :cond_1
    new-instance v0, LX/3sy;

    invoke-direct {v0}, LX/3sy;-><init>()V

    sput-object v0, LX/3t3;->a:LX/3sv;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 644446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 644447
    sget-object v0, LX/3t3;->a:LX/3sv;

    invoke-interface {v0, p0}, LX/3sv;->a(LX/3t3;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/3t3;->b:Ljava/lang/Object;

    .line 644448
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 644455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 644456
    iput-object p1, p0, LX/3t3;->b:Ljava/lang/Object;

    .line 644457
    return-void
.end method


# virtual methods
.method public a(I)LX/3sp;
    .locals 1

    .prologue
    .line 644445
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(IILandroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 644444
    const/4 v0, 0x0

    return v0
.end method
