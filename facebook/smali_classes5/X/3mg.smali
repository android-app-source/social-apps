.class public final LX/3mg;
.super LX/1OM;
.source ""

# interfaces
.implements LX/3mh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/25d;",
        ">;",
        "LX/3mh;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/3mX;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/3mX;)V
    .locals 0

    .prologue
    .line 637123
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 637124
    iput-object p1, p0, LX/3mg;->a:Landroid/content/Context;

    .line 637125
    iput-object p2, p0, LX/3mg;->b:LX/3mX;

    .line 637126
    return-void
.end method


# virtual methods
.method public final C_(I)J
    .locals 4

    .prologue
    .line 637107
    const-wide/16 v2, -0x1

    move-wide v0, v2

    .line 637108
    return-wide v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 637122
    new-instance v0, LX/25d;

    new-instance v1, Lcom/facebook/components/ComponentView;

    iget-object v2, p0, LX/3mg;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/components/ComponentView;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/25d;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 637118
    check-cast p1, LX/25d;

    .line 637119
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/components/ComponentView;

    .line 637120
    iget-object v1, p0, LX/3mg;->b:LX/3mX;

    invoke-virtual {v1, p2}, LX/3mY;->d(I)LX/1dV;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 637121
    return-void
.end method

.method public final a_(II)V
    .locals 0

    .prologue
    .line 637116
    invoke-virtual {p0, p1, p2}, LX/1OM;->c(II)V

    .line 637117
    return-void
.end method

.method public final bG_()V
    .locals 0

    .prologue
    .line 637127
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 637128
    return-void
.end method

.method public final c_(I)V
    .locals 0

    .prologue
    .line 637114
    invoke-virtual {p0, p1}, LX/1OM;->j_(I)V

    .line 637115
    return-void
.end method

.method public final e(I)V
    .locals 0

    .prologue
    .line 637112
    invoke-virtual {p0, p1}, LX/1OM;->i_(I)V

    .line 637113
    return-void
.end method

.method public final f(I)V
    .locals 0

    .prologue
    .line 637110
    invoke-virtual {p0, p1}, LX/1OM;->d(I)V

    .line 637111
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 637109
    iget-object v0, p0, LX/3mg;->b:LX/3mX;

    invoke-virtual {v0}, LX/3mX;->e()I

    move-result v0

    return v0
.end method
