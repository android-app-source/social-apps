.class public LX/3el;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile l:LX/3el;


# instance fields
.field public a:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0kb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:LX/1HI;

.field private final e:Ljava/util/concurrent/ExecutorService;

.field private final f:Ljava/util/concurrent/ExecutorService;

.field private final g:Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;

.field private final h:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Uh;

.field public final j:LX/2MA;

.field private final k:LX/03V;


# direct methods
.method public constructor <init>(LX/1HI;Ljava/util/concurrent/ExecutorService;Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;LX/0Uh;LX/2MA;LX/03V;LX/0Wb;)V
    .locals 4
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 620850
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 620851
    iput-object p1, p0, LX/3el;->d:LX/1HI;

    .line 620852
    iput-object p2, p0, LX/3el;->e:Ljava/util/concurrent/ExecutorService;

    .line 620853
    iput-object p3, p0, LX/3el;->g:Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;

    .line 620854
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    const-wide/16 v2, 0x3

    sget-object v1, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/3el;->h:LX/0QI;

    .line 620855
    iput-object p4, p0, LX/3el;->i:LX/0Uh;

    .line 620856
    iput-object p5, p0, LX/3el;->j:LX/2MA;

    .line 620857
    iput-object p6, p0, LX/3el;->k:LX/03V;

    .line 620858
    invoke-virtual {p7, p2}, LX/0Wb;->a(Ljava/util/concurrent/ExecutorService;)LX/0Wd;

    move-result-object v0

    iput-object v0, p0, LX/3el;->f:Ljava/util/concurrent/ExecutorService;

    .line 620859
    return-void
.end method

.method private a(LX/1bX;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)LX/1bf;
    .locals 3

    .prologue
    .line 620827
    iget-object v0, p0, LX/3el;->j:LX/2MA;

    sget-object v1, LX/8Bk;->HTTP:LX/8Bk;

    invoke-interface {v0, v1}, LX/2MA;->b(LX/8Bk;)Z

    move-result v0

    move v1, v0

    .line 620828
    if-nez v1, :cond_0

    .line 620829
    sget-object v0, LX/1bY;->DISK_CACHE:LX/1bY;

    .line 620830
    iput-object v0, p1, LX/1bX;->b:LX/1bY;

    .line 620831
    :cond_0
    invoke-static {p2}, LX/1H1;->g(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    .line 620832
    iget-object v0, p0, LX/3el;->h:LX/0QI;

    invoke-interface {v0, v2}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 620833
    if-eqz v0, :cond_1

    .line 620834
    invoke-virtual {p1, v0}, LX/1bX;->b(Landroid/net/Uri;)LX/1bX;

    .line 620835
    invoke-virtual {p1}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 620836
    :goto_0
    return-object v0

    .line 620837
    :cond_1
    if-nez v1, :cond_2

    .line 620838
    invoke-virtual {p1}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    goto :goto_0

    .line 620839
    :cond_2
    iget-object v0, p0, LX/3el;->d:LX/1HI;

    invoke-virtual {v0, v2}, LX/1HI;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 620840
    invoke-virtual {p1}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    goto :goto_0

    .line 620841
    :cond_3
    invoke-static {p2}, LX/1H1;->f(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 620842
    iget-object v0, p0, LX/3el;->h:LX/0QI;

    invoke-interface {v0, v2, p2}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 620843
    invoke-virtual {p1}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    goto :goto_0

    .line 620844
    :cond_4
    iget-object v0, p1, LX/1bX;->c:LX/1o9;

    move-object v0, v0

    .line 620845
    invoke-direct {p0, p2, p3, v0}, LX/3el;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;LX/1o9;)Landroid/net/Uri;

    move-result-object v0

    .line 620846
    if-eqz v0, :cond_5

    .line 620847
    iget-object v1, p0, LX/3el;->h:LX/0QI;

    invoke-interface {v1, v2, v0}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 620848
    invoke-virtual {p1, v0}, LX/1bX;->b(Landroid/net/Uri;)LX/1bX;

    .line 620849
    :cond_5
    invoke-virtual {p1}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/3el;
    .locals 11

    .prologue
    .line 620900
    sget-object v0, LX/3el;->l:LX/3el;

    if-nez v0, :cond_1

    .line 620901
    const-class v1, LX/3el;

    monitor-enter v1

    .line 620902
    :try_start_0
    sget-object v0, LX/3el;->l:LX/3el;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 620903
    if-eqz v2, :cond_0

    .line 620904
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 620905
    new-instance v3, LX/3el;

    invoke-static {v0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v4

    check-cast v4, LX/1HI;

    invoke-static {v0}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;->b(LX/0QB;)Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;

    move-result-object v6

    check-cast v6, Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static {v0}, LX/2M4;->a(LX/0QB;)LX/2MA;

    move-result-object v8

    check-cast v8, LX/2MA;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v0}, LX/0Wb;->a(LX/0QB;)LX/0Wb;

    move-result-object v10

    check-cast v10, LX/0Wb;

    invoke-direct/range {v3 .. v10}, LX/3el;-><init>(LX/1HI;Ljava/util/concurrent/ExecutorService;Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;LX/0Uh;LX/2MA;LX/03V;LX/0Wb;)V

    .line 620906
    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v5

    check-cast v5, LX/0kb;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    .line 620907
    iput-object v4, v3, LX/3el;->a:LX/0Sh;

    iput-object v5, v3, LX/3el;->b:LX/0kb;

    iput-object v6, v3, LX/3el;->c:Ljava/util/concurrent/ExecutorService;

    .line 620908
    move-object v0, v3

    .line 620909
    sput-object v0, LX/3el;->l:LX/3el;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 620910
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 620911
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 620912
    :cond_1
    sget-object v0, LX/3el;->l:LX/3el;

    return-object v0

    .line 620913
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 620914
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;LX/1o9;)Landroid/net/Uri;
    .locals 4
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/1o9;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 620890
    invoke-static {p1}, LX/1H1;->i(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 620891
    :goto_0
    return-object p1

    .line 620892
    :cond_0
    invoke-static {p1}, LX/1H1;->h(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 620893
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/CharSequence;

    aput-object v1, v2, v0

    invoke-static {v2}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 620894
    new-instance v0, Ljava/lang/Throwable;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Image CDN Uri has expired and FbId not found within Uri: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    throw v0

    .line 620895
    :cond_1
    iget-object v2, p0, LX/3el;->g:Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;

    if-nez p3, :cond_2

    :goto_1
    invoke-virtual {v2, v1, v0, p2}, Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;->a(Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;)Landroid/net/Uri;

    move-result-object v0

    .line 620896
    if-nez v0, :cond_3

    .line 620897
    new-instance v0, Ljava/lang/Throwable;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CDN Uri expired but could not retrieve new uri to replace expiring cdn uri. FBID = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , original Uri = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    throw v0

    .line 620898
    :cond_2
    iget v0, p3, LX/1o9;->a:I

    iget v3, p3, LX/1o9;->b:I

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_1

    :cond_3
    move-object p1, v0

    .line 620899
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1bX;Lcom/facebook/common/callercontext/CallerContext;)LX/1ca;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bX;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 620915
    iget-object v0, p0, LX/3el;->i:LX/0Uh;

    const/16 v1, 0x16b

    invoke-virtual {v0, v1, v5}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 620916
    iget-object v0, p0, LX/3el;->d:LX/1HI;

    invoke-virtual {p1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, LX/1HI;->c(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    .line 620917
    :goto_0
    return-object v0

    .line 620918
    :cond_0
    iget-object v0, p1, LX/1bX;->a:Landroid/net/Uri;

    move-object v1, v0

    .line 620919
    invoke-static {v1}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    .line 620920
    :try_start_0
    iget-object v0, p0, LX/3el;->d:LX/1HI;

    invoke-direct {p0, p1, v1, p2}, LX/3el;->a(LX/1bX;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)LX/1bf;

    move-result-object v2

    invoke-virtual {v0, v2, p2}, LX/1HI;->c(LX/1bf;Ljava/lang/Object;)LX/1ca;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 620921
    :catch_0
    move-exception v0

    .line 620922
    const-string v2, "ImagePipelineWrapper"

    const-string v3, "Fetching new encoded image failed for original Uri: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 620923
    invoke-static {v0}, LX/49D;->a(Ljava/lang/Throwable;)LX/1ca;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1bf;)Ljava/util/concurrent/ExecutorService;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 620872
    iget-object v0, p0, LX/3el;->i:LX/0Uh;

    const/16 v1, 0x555

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 620873
    iget-object v0, p0, LX/3el;->c:Ljava/util/concurrent/ExecutorService;

    .line 620874
    :goto_0
    return-object v0

    .line 620875
    :cond_0
    iget-object v0, p0, LX/3el;->i:LX/0Uh;

    const/16 v1, 0x16c

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 620876
    iget-object v0, p1, LX/1bf;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 620877
    invoke-static {v0}, LX/1H1;->g(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 620878
    iget-object v1, p0, LX/3el;->d:LX/1HI;

    invoke-virtual {v1, v0}, LX/1HI;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 620879
    iget-object v0, p0, LX/3el;->i:LX/0Uh;

    const/16 v1, 0x556

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3el;->f:Ljava/util/concurrent/ExecutorService;

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/3el;->e:Ljava/util/concurrent/ExecutorService;

    goto :goto_0

    .line 620880
    :cond_2
    iget-object v0, p0, LX/3el;->c:Ljava/util/concurrent/ExecutorService;

    goto :goto_0

    .line 620881
    :cond_3
    iget-object v0, p0, LX/3el;->j:LX/2MA;

    sget-object v1, LX/8Bk;->HTTP:LX/8Bk;

    invoke-interface {v0, v1}, LX/2MA;->b(LX/8Bk;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 620882
    iget-object v0, p0, LX/3el;->i:LX/0Uh;

    const/16 v1, 0x556

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/3el;->f:Ljava/util/concurrent/ExecutorService;

    goto :goto_0

    :cond_4
    iget-object v0, p0, LX/3el;->e:Ljava/util/concurrent/ExecutorService;

    goto :goto_0

    .line 620883
    :cond_5
    iget-object v0, p1, LX/1bf;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 620884
    invoke-static {v0}, LX/1H1;->f(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 620885
    iget-object v0, p0, LX/3el;->i:LX/0Uh;

    const/16 v1, 0x556

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/3el;->f:Ljava/util/concurrent/ExecutorService;

    goto :goto_0

    :cond_6
    iget-object v0, p0, LX/3el;->e:Ljava/util/concurrent/ExecutorService;

    goto :goto_0

    .line 620886
    :cond_7
    iget-object v0, p1, LX/1bf;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 620887
    invoke-static {v0}, LX/1H1;->i(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 620888
    iget-object v0, p0, LX/3el;->i:LX/0Uh;

    const/16 v1, 0x556

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/3el;->f:Ljava/util/concurrent/ExecutorService;

    goto :goto_0

    :cond_8
    iget-object v0, p0, LX/3el;->e:Ljava/util/concurrent/ExecutorService;

    goto :goto_0

    .line 620889
    :cond_9
    iget-object v0, p0, LX/3el;->c:Ljava/util/concurrent/ExecutorService;

    goto :goto_0
.end method

.method public final a(LX/1ca;LX/8CK;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;",
            "LX/8CK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 620869
    new-instance v0, LX/8CJ;

    invoke-direct {v0, p0, p2}, LX/8CJ;-><init>(LX/3el;LX/8CK;)V

    move-object v0, v0

    .line 620870
    iget-object v1, p0, LX/3el;->e:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p1, v0, v1}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 620871
    return-void
.end method

.method public final b(LX/1bX;Lcom/facebook/common/callercontext/CallerContext;)LX/1ca;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bX;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 620860
    iget-object v0, p0, LX/3el;->i:LX/0Uh;

    const/16 v1, 0x16b

    invoke-virtual {v0, v1, v5}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 620861
    iget-object v0, p0, LX/3el;->d:LX/1HI;

    invoke-virtual {p1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    .line 620862
    :goto_0
    return-object v0

    .line 620863
    :cond_0
    iget-object v0, p1, LX/1bX;->a:Landroid/net/Uri;

    move-object v1, v0

    .line 620864
    invoke-static {v1}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    .line 620865
    :try_start_0
    iget-object v0, p0, LX/3el;->d:LX/1HI;

    invoke-direct {p0, p1, v1, p2}, LX/3el;->a(LX/1bX;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)LX/1bf;

    move-result-object v2

    invoke-virtual {v0, v2, p2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 620866
    :catch_0
    move-exception v0

    .line 620867
    const-string v2, "ImagePipelineWrapper"

    const-string v3, "Fetching new decoded image failed for original Uri: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 620868
    invoke-static {v0}, LX/49D;->a(Ljava/lang/Throwable;)LX/1ca;

    move-result-object v0

    goto :goto_0
.end method
