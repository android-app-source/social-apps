.class public LX/4NF;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 691689
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 691690
    const/4 v13, 0x0

    .line 691691
    const/4 v12, 0x0

    .line 691692
    const/4 v11, 0x0

    .line 691693
    const/4 v10, 0x0

    .line 691694
    const/4 v9, 0x0

    .line 691695
    const/4 v8, 0x0

    .line 691696
    const/4 v7, 0x0

    .line 691697
    const/4 v6, 0x0

    .line 691698
    const/4 v5, 0x0

    .line 691699
    const/4 v4, 0x0

    .line 691700
    const/4 v3, 0x0

    .line 691701
    const/4 v2, 0x0

    .line 691702
    const/4 v1, 0x0

    .line 691703
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_1

    .line 691704
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 691705
    const/4 v1, 0x0

    .line 691706
    :goto_0
    return v1

    .line 691707
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 691708
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v14, v15, :cond_a

    .line 691709
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v14

    .line 691710
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 691711
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_1

    if-eqz v14, :cond_1

    .line 691712
    const-string v15, "__type__"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_2

    const-string v15, "__typename"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 691713
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v13

    invoke-virtual {v13}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->a(Ljava/lang/String;)I

    move-result v13

    goto :goto_1

    .line 691714
    :cond_3
    const-string v15, "posted_item_privacy_scope"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 691715
    invoke-static/range {p0 .. p1}, LX/2bo;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 691716
    :cond_4
    const-string v15, "can_viewer_delete"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 691717
    const/4 v5, 0x1

    .line 691718
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto :goto_1

    .line 691719
    :cond_5
    const-string v15, "can_viewer_edit"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 691720
    const/4 v4, 0x1

    .line 691721
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto :goto_1

    .line 691722
    :cond_6
    const-string v15, "can_viewer_post"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 691723
    const/4 v3, 0x1

    .line 691724
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto :goto_1

    .line 691725
    :cond_7
    const-string v15, "can_viewer_report"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 691726
    const/4 v2, 0x1

    .line 691727
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto :goto_1

    .line 691728
    :cond_8
    const-string v15, "is_viewer_following"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 691729
    const/4 v1, 0x1

    .line 691730
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    goto/16 :goto_1

    .line 691731
    :cond_9
    const-string v15, "privacy_scope"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 691732
    invoke-static/range {p0 .. p1}, LX/2bo;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 691733
    :cond_a
    const/16 v14, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 691734
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 691735
    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 691736
    if-eqz v5, :cond_b

    .line 691737
    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v11}, LX/186;->a(IZ)V

    .line 691738
    :cond_b
    if-eqz v4, :cond_c

    .line 691739
    const/4 v4, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->a(IZ)V

    .line 691740
    :cond_c
    if-eqz v3, :cond_d

    .line 691741
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->a(IZ)V

    .line 691742
    :cond_d
    if-eqz v2, :cond_e

    .line 691743
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->a(IZ)V

    .line 691744
    :cond_e
    if-eqz v1, :cond_f

    .line 691745
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->a(IZ)V

    .line 691746
    :cond_f
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 691747
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
