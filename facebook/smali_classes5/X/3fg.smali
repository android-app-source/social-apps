.class public LX/3fg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile m:LX/3fg;


# instance fields
.field private final b:LX/2Iv;

.field private final c:LX/2It;

.field public final d:LX/3Lz;

.field private final e:LX/0Sy;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/3Lo;

.field private final h:LX/3fh;

.field private final i:LX/3fj;

.field private final j:LX/2RC;

.field private final k:LX/2JB;

.field private final l:LX/2RQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 623142
    const-class v0, LX/3fg;

    sput-object v0, LX/3fg;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2Iv;LX/3fh;LX/2It;LX/3Lz;LX/0Sy;LX/0Or;LX/3Lo;LX/3fj;LX/2RC;LX/2JB;LX/2RQ;)V
    .locals 0
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Iv;",
            "LX/3fh;",
            "LX/2It;",
            "LX/3Lz;",
            "Lcom/facebook/common/userinteraction/UserInteractionController;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/3Lo;",
            "LX/3fj;",
            "LX/2RC;",
            "LX/2JB;",
            "LX/2RQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 623129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 623130
    iput-object p1, p0, LX/3fg;->b:LX/2Iv;

    .line 623131
    iput-object p3, p0, LX/3fg;->c:LX/2It;

    .line 623132
    iput-object p4, p0, LX/3fg;->d:LX/3Lz;

    .line 623133
    iput-object p5, p0, LX/3fg;->e:LX/0Sy;

    .line 623134
    iput-object p6, p0, LX/3fg;->f:LX/0Or;

    .line 623135
    iput-object p7, p0, LX/3fg;->g:LX/3Lo;

    .line 623136
    iput-object p2, p0, LX/3fg;->h:LX/3fh;

    .line 623137
    iput-object p8, p0, LX/3fg;->i:LX/3fj;

    .line 623138
    iput-object p9, p0, LX/3fg;->j:LX/2RC;

    .line 623139
    iput-object p10, p0, LX/3fg;->k:LX/2JB;

    .line 623140
    iput-object p11, p0, LX/3fg;->l:LX/2RQ;

    .line 623141
    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/contacts/graphql/Contact;ZLX/0ta;)J
    .locals 10
    .param p4    # LX/0ta;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/16 v2, 0x1

    const-wide/16 v4, 0x0

    .line 623066
    sget-object v0, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne p4, v0, :cond_1

    .line 623067
    const-string v0, "?"

    .line 623068
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "INSERT OR REPLACE INTO contacts (internal_id, contact_id, fbid, first_name, last_name, display_name, small_picture_url, big_picture_url, huge_picture_url, small_picture_size, big_picture_size, huge_picture_size, communication_rank, is_mobile_pushable, is_messenger_user, messenger_install_time_ms, added_time_ms, phonebook_section_key, is_on_viewer_contact_list, type, link_type, is_indexed, data, bday_month, bday_day, is_partial, is_memorialized, messenger_invite_priority, viewer_connection_status, last_fetch_time_ms) VALUES ((select internal_id from contacts where contact_id = ?), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v6

    .line 623069
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 623070
    const/4 v0, 0x2

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 623071
    const/4 v0, 0x3

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v0, v1}, LX/3fg;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    .line 623072
    const/4 v0, 0x4

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v0, v1}, LX/3fg;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    .line 623073
    const/4 v0, 0x5

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/user/model/Name;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v0, v1}, LX/3fg;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    .line 623074
    const/4 v0, 0x6

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v0, v1}, LX/3fg;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    .line 623075
    const/4 v0, 0x7

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v0, v1}, LX/3fg;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    .line 623076
    const/16 v0, 0x8

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v0, v1}, LX/3fg;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    .line 623077
    const/16 v0, 0x9

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->i()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v0, v1}, LX/3fg;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    .line 623078
    const/16 v0, 0xa

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->j()I

    move-result v1

    int-to-long v8, v1

    invoke-virtual {v6, v0, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 623079
    const/16 v0, 0xb

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->k()I

    move-result v1

    int-to-long v8, v1

    invoke-virtual {v6, v0, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 623080
    const/16 v0, 0xc

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->l()I

    move-result v1

    int-to-long v8, v1

    invoke-virtual {v6, v0, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 623081
    const/16 v0, 0xd

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->m()F

    move-result v1

    float-to-double v8, v1

    invoke-virtual {v6, v0, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindDouble(ID)V

    .line 623082
    const/16 v0, 0xe

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->r()LX/03R;

    move-result-object v1

    invoke-virtual {v1}, LX/03R;->getDbValue()I

    move-result v1

    int-to-long v8, v1

    invoke-virtual {v6, v0, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 623083
    const/16 v0, 0xf

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->s()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 623084
    const/16 v0, 0x10

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->t()J

    move-result-wide v8

    invoke-virtual {v6, v0, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 623085
    const/16 v0, 0x11

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->w()J

    move-result-wide v8

    invoke-virtual {v6, v0, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 623086
    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->f()Lcom/facebook/user/model/Name;

    move-result-object v1

    .line 623087
    new-instance v7, LX/3hD;

    invoke-direct {v7}, LX/3hD;-><init>()V

    .line 623088
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v8

    .line 623089
    iput-object v8, v7, LX/3hD;->a:Ljava/lang/String;

    .line 623090
    move-object v8, v7

    .line 623091
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v9

    .line 623092
    iput-object v9, v8, LX/3hD;->b:Ljava/lang/String;

    .line 623093
    move-object v8, v8

    .line 623094
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->c()Ljava/lang/String;

    move-result-object v9

    .line 623095
    iput-object v9, v8, LX/3hD;->c:Ljava/lang/String;

    .line 623096
    if-eqz v1, :cond_0

    .line 623097
    invoke-virtual {v1}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v8

    .line 623098
    iput-object v8, v7, LX/3hD;->d:Ljava/lang/String;

    .line 623099
    move-object v8, v7

    .line 623100
    invoke-virtual {v1}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v9

    .line 623101
    iput-object v9, v8, LX/3hD;->e:Ljava/lang/String;

    .line 623102
    move-object v8, v8

    .line 623103
    invoke-virtual {v1}, Lcom/facebook/user/model/Name;->c()Ljava/lang/String;

    move-result-object v9

    .line 623104
    iput-object v9, v8, LX/3hD;->f:Ljava/lang/String;

    .line 623105
    :cond_0
    invoke-virtual {v7}, LX/3hD;->a()LX/3hE;

    move-result-object v7

    .line 623106
    iget-object v8, p0, LX/3fg;->g:LX/3Lo;

    invoke-virtual {v8, p1, v7}, LX/3Lo;->a(Landroid/database/sqlite/SQLiteDatabase;LX/3hE;)Ljava/lang/String;

    move-result-object v7

    move-object v0, v7

    .line 623107
    const/16 v1, 0x12

    invoke-static {v6, v1, v0}, LX/3fg;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    .line 623108
    const/16 v0, 0x13

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->v()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 623109
    const/16 v0, 0x14

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->A()LX/2RU;

    move-result-object v1

    invoke-virtual {v1}, LX/2RU;->getDbValue()I

    move-result v1

    int-to-long v8, v1

    invoke-virtual {v6, v0, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 623110
    const/16 v1, 0x15

    iget-object v0, p0, LX/3fg;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p2, v0}, LX/3Oq;->getFromContact(Lcom/facebook/contacts/graphql/Contact;Ljava/lang/String;)LX/3Oq;

    move-result-object v0

    invoke-virtual {v0}, LX/3Oq;->getDbValue()I

    move-result v0

    int-to-long v8, v0

    invoke-virtual {v6, v1, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 623111
    const/16 v7, 0x16

    if-eqz p3, :cond_2

    move-wide v0, v2

    :goto_1
    invoke-virtual {v6, v7, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 623112
    const/16 v0, 0x17

    iget-object v1, p0, LX/3fg;->h:LX/3fh;

    invoke-virtual {v1, p2}, LX/3fh;->a(Lcom/facebook/contacts/graphql/Contact;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v0, v1}, LX/3fg;->a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    .line 623113
    const/16 v0, 0x18

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->D()I

    move-result v1

    int-to-long v8, v1

    invoke-virtual {v6, v0, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 623114
    const/16 v0, 0x19

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->C()I

    move-result v1

    int-to-long v8, v1

    invoke-virtual {v6, v0, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 623115
    const/16 v7, 0x1a

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->E()Z

    move-result v0

    if-eqz v0, :cond_3

    move-wide v0, v2

    :goto_2
    invoke-virtual {v6, v7, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 623116
    const/16 v7, 0x1b

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->u()Z

    move-result v0

    if-eqz v0, :cond_4

    move-wide v0, v2

    :goto_3
    invoke-virtual {v6, v7, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 623117
    const/16 v0, 0x1c

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->L()F

    move-result v1

    float-to-double v2, v1

    invoke-virtual {v6, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindDouble(ID)V

    .line 623118
    const/16 v0, 0x1d

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->N()Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 623119
    sget-object v0, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne p4, v0, :cond_5

    .line 623120
    const/16 v0, 0x1e

    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->F()J

    move-result-wide v2

    invoke-virtual {v6, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 623121
    :goto_4
    const v0, -0x3ebf1bcc

    invoke-static {v0}, LX/03h;->a(I)V

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v0

    const v2, 0x28895a6f

    invoke-static {v2}, LX/03h;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 623122
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteStatement;->close()V

    return-wide v0

    .line 623123
    :cond_1
    const-string v0, "(select last_fetch_time_ms from contacts where contact_id = ?)"

    goto/16 :goto_0

    :cond_2
    move-wide v0, v4

    .line 623124
    goto :goto_1

    :cond_3
    move-wide v0, v4

    .line 623125
    goto :goto_2

    :cond_4
    move-wide v0, v4

    .line 623126
    goto :goto_3

    .line 623127
    :cond_5
    const/16 v0, 0x1e

    :try_start_1
    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    .line 623128
    :catchall_0
    move-exception v0

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteStatement;->close()V

    throw v0
.end method

.method public static a(LX/0QB;)LX/3fg;
    .locals 15

    .prologue
    .line 623053
    sget-object v0, LX/3fg;->m:LX/3fg;

    if-nez v0, :cond_1

    .line 623054
    const-class v1, LX/3fg;

    monitor-enter v1

    .line 623055
    :try_start_0
    sget-object v0, LX/3fg;->m:LX/3fg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 623056
    if-eqz v2, :cond_0

    .line 623057
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 623058
    new-instance v3, LX/3fg;

    invoke-static {v0}, LX/2Iv;->a(LX/0QB;)LX/2Iv;

    move-result-object v4

    check-cast v4, LX/2Iv;

    invoke-static {v0}, LX/3fh;->a(LX/0QB;)LX/3fh;

    move-result-object v5

    check-cast v5, LX/3fh;

    invoke-static {v0}, LX/2It;->b(LX/0QB;)LX/2It;

    move-result-object v6

    check-cast v6, LX/2It;

    invoke-static {v0}, LX/3Ly;->a(LX/0QB;)LX/3Lz;

    move-result-object v7

    check-cast v7, LX/3Lz;

    invoke-static {v0}, LX/0Sy;->a(LX/0QB;)LX/0Sy;

    move-result-object v8

    check-cast v8, LX/0Sy;

    const/16 v9, 0x15e7

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v0}, LX/3Lo;->a(LX/0QB;)LX/3Lo;

    move-result-object v10

    check-cast v10, LX/3Lo;

    invoke-static {v0}, LX/3fj;->b(LX/0QB;)LX/3fj;

    move-result-object v11

    check-cast v11, LX/3fj;

    invoke-static {v0}, LX/2RC;->a(LX/0QB;)LX/2RC;

    move-result-object v12

    check-cast v12, LX/2RC;

    invoke-static {v0}, LX/2JB;->a(LX/0QB;)LX/2JB;

    move-result-object v13

    check-cast v13, LX/2JB;

    invoke-static {v0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v14

    check-cast v14, LX/2RQ;

    invoke-direct/range {v3 .. v14}, LX/3fg;-><init>(LX/2Iv;LX/3fh;LX/2It;LX/3Lz;LX/0Sy;LX/0Or;LX/3Lo;LX/3fj;LX/2RC;LX/2JB;LX/2RQ;)V

    .line 623059
    move-object v0, v3

    .line 623060
    sput-object v0, LX/3fg;->m:LX/3fg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 623061
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 623062
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 623063
    :cond_1
    sget-object v0, LX/3fg;->m:LX/3fg;

    return-object v0

    .line 623064
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 623065
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)Landroid/database/sqlite/SQLiteStatement;
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 623049
    if-nez p2, :cond_0

    .line 623050
    invoke-virtual {p0, p1}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 623051
    :goto_0
    return-object p0

    .line 623052
    :cond_0
    invoke-virtual {p0, p1, p2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private static a(LX/3fg;LX/0Px;LX/0Px;)V
    .locals 10
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/graphql/Contact;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 623025
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 623026
    :goto_0
    return-void

    .line 623027
    :cond_0
    const-string v0, "reindexContactsNames (%d contacts)"

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v3, -0x61cc342b

    invoke-static {v0, v1, v3}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 623028
    :try_start_0
    iget-object v0, p0, LX/3fg;->b:LX/2Iv;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 623029
    const v0, -0x6ca9b385

    invoke-static {v4, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 623030
    :try_start_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 623031
    invoke-virtual {p2}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v6

    .line 623032
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v7

    move v3, v2

    :goto_1
    if-ge v3, v7, :cond_1

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;

    .line 623033
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 623034
    new-instance v1, LX/3hM;

    invoke-direct {v1, v8, v9}, LX/3hM;-><init>(J)V

    .line 623035
    iget-object v8, p0, LX/3fg;->i:LX/3fj;

    .line 623036
    invoke-static {v8, v0, v1}, LX/3fj;->c(LX/3fj;Lcom/facebook/contacts/graphql/Contact;LX/3hM;)V

    .line 623037
    invoke-static {v8, v0, v1}, LX/3fj;->d(LX/3fj;Lcom/facebook/contacts/graphql/Contact;LX/3hM;)V

    .line 623038
    invoke-virtual {v1}, LX/3hM;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 623039
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 623040
    :cond_1
    iget-object v0, p0, LX/3fg;->e:LX/0Sy;

    invoke-virtual {v0}, LX/0Sy;->c()V

    .line 623041
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_2
    if-ge v1, v5, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    .line 623042
    const-string v2, "contacts_indexed_data"

    const/4 v6, 0x0

    const v7, 0x754b0187

    invoke-static {v7}, LX/03h;->a(I)V

    invoke-virtual {v4, v2, v6, v0}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, 0x4be661fb    # 3.0196726E7f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 623043
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 623044
    :cond_2
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 623045
    const v0, 0x44c65d08

    :try_start_2
    invoke-static {v4, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 623046
    const v0, -0x2dd8c030

    invoke-static {v0}, LX/02m;->a(I)V

    goto/16 :goto_0

    .line 623047
    :catchall_0
    move-exception v0

    const v1, -0x24f02f09

    :try_start_3
    invoke-static {v4, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 623048
    :catchall_1
    move-exception v0

    const v1, -0x19713b6

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/contacts/graphql/Contact;J)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 622830
    new-instance v1, LX/3hM;

    invoke-direct {v1, p3, p4}, LX/3hM;-><init>(J)V

    .line 622831
    iget-object v2, p0, LX/3fg;->i:LX/3fj;

    .line 622832
    invoke-static {v2, p2, v1}, LX/3fj;->c(LX/3fj;Lcom/facebook/contacts/graphql/Contact;LX/3hM;)V

    .line 622833
    invoke-static {v2, p2, v1}, LX/3fj;->d(LX/3fj;Lcom/facebook/contacts/graphql/Contact;LX/3hM;)V

    .line 622834
    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->o()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_2

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/contacts/graphql/ContactPhone;

    .line 622835
    :try_start_0
    iget-object v7, v2, LX/3fj;->d:LX/3Lz;

    invoke-virtual {v3}, Lcom/facebook/contacts/graphql/ContactPhone;->d()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, LX/3Lz;->parse(Ljava/lang/String;Ljava/lang/String;)LX/4hT;
    :try_end_0
    .catch LX/4hE; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 622836
    const-string v8, "phone_e164"

    iget-object v9, v2, LX/3fj;->d:LX/3Lz;

    sget-object p0, LX/4hG;->E164:LX/4hG;

    invoke-virtual {v9, v7, p0}, LX/3Lz;->format(LX/4hT;LX/4hG;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, LX/3hM;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 622837
    iget-object v8, v2, LX/3fj;->d:LX/3Lz;

    invoke-virtual {v8, v7}, LX/3Lz;->getNationalSignificantNumber(LX/4hT;)Ljava/lang/String;

    move-result-object v8

    .line 622838
    const-string v9, "phone_national"

    invoke-virtual {v1, v9, v8}, LX/3hM;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 622839
    iget-object v9, v2, LX/3fj;->d:LX/3Lz;

    invoke-virtual {v9, v7}, LX/3Lz;->getLengthOfGeographicalAreaCode(LX/4hT;)I

    move-result v7

    .line 622840
    if-lez v7, :cond_0

    .line 622841
    const-string v9, "phone_local"

    invoke-virtual {v8, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v9, v7}, LX/3hM;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 622842
    :cond_0
    invoke-virtual {v3}, Lcom/facebook/contacts/graphql/ContactPhone;->e()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 622843
    const-string v7, "phone_verified"

    invoke-virtual {v3}, Lcom/facebook/contacts/graphql/ContactPhone;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v7, v3}, LX/3hM;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 622844
    :cond_1
    :goto_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 622845
    :cond_2
    const/4 v5, 0x0

    .line 622846
    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->m()F

    move-result v3

    .line 622847
    cmpl-float v4, v3, v5

    if-lez v4, :cond_3

    .line 622848
    const-string v4, "communication_rank"

    invoke-virtual {v1, v4, v3}, LX/3hM;->a(Ljava/lang/String;F)V

    .line 622849
    :cond_3
    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->n()F

    move-result v3

    .line 622850
    cmpl-float v4, v3, v5

    if-lez v4, :cond_4

    .line 622851
    const-string v4, "with_tagging_rank"

    invoke-virtual {v1, v4, v3}, LX/3hM;->a(Ljava/lang/String;F)V

    .line 622852
    :cond_4
    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->J()F

    move-result v3

    .line 622853
    cmpl-float v4, v3, v5

    if-lez v4, :cond_5

    .line 622854
    const-string v4, "phat_rank"

    invoke-virtual {v1, v4, v3}, LX/3hM;->a(Ljava/lang/String;F)V

    .line 622855
    :cond_5
    invoke-virtual {p2}, Lcom/facebook/contacts/graphql/Contact;->K()Ljava/lang/String;

    move-result-object v3

    .line 622856
    if-eqz v3, :cond_6

    .line 622857
    const-string v4, "username"

    iget-object v5, v2, LX/3fj;->b:LX/2Rd;

    invoke-virtual {v5, v3}, LX/2Rd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v4, v3}, LX/3hM;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 622858
    :cond_6
    const v2, -0x29f62e84

    invoke-static {p1, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 622859
    :try_start_1
    const-string v2, "contacts_indexed_data"

    const-string v3, "contact_internal_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 622860
    invoke-virtual {v1}, LX/3hM;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_7

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    .line 622861
    const-string v4, "contacts_indexed_data"

    const/4 v5, 0x0

    const v6, 0x57e02d9a

    invoke-static {v6}, LX/03h;->a(I)V

    invoke-virtual {p1, v4, v5, v0}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, 0x6baf0a8d

    invoke-static {v0}, LX/03h;->a(I)V

    .line 622862
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 622863
    :cond_7
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 622864
    const v0, 0x667668f

    invoke-static {p1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 622865
    return-void

    .line 622866
    :catchall_0
    move-exception v0

    const v1, -0x14b48bf6

    invoke-static {p1, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 622867
    :catch_0
    goto/16 :goto_1
.end method

.method private static b(LX/3fg;)V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 623018
    sget-object v0, LX/2J3;->a:LX/0U1;

    .line 623019
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 623020
    const-string v1, "sort_name_key"

    const-string v2, "name"

    invoke-static {v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v0

    .line 623021
    iget-object v1, p0, LX/3fg;->e:LX/0Sy;

    invoke-virtual {v1}, LX/0Sy;->c()V

    .line 623022
    iget-object v1, p0, LX/3fg;->b:LX/2Iv;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 623023
    const-string v2, "contacts_indexed_data"

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 623024
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/contacts/graphql/Contact;LX/0ta;)J
    .locals 4
    .param p2    # LX/0ta;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 623004
    const-string v0, "insertContactIntoDatabase"

    const v1, -0x3d911c8d

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 623005
    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 623006
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->A()LX/2RU;

    move-result-object v0

    sget-object v1, LX/2RU;->PAGE:LX/2RU;

    if-ne v0, v1, :cond_3

    :cond_1
    const/4 v0, 0x1

    .line 623007
    :goto_0
    iget-object v1, p0, LX/3fg;->b:LX/2Iv;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 623008
    const v2, 0x46a4b4bf

    invoke-static {v1, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 623009
    :try_start_1
    invoke-direct {p0, v1, p1, v0, p2}, LX/3fg;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/contacts/graphql/Contact;ZLX/0ta;)J

    move-result-wide v2

    .line 623010
    if-eqz v0, :cond_2

    .line 623011
    invoke-direct {p0, v1, p1, v2, v3}, LX/3fg;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/contacts/graphql/Contact;J)V

    .line 623012
    :cond_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 623013
    const v0, -0x3fae7cdf

    :try_start_2
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 623014
    const v0, -0x1d0bc981

    invoke-static {v0}, LX/02m;->a(I)V

    return-wide v2

    .line 623015
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 623016
    :catchall_0
    move-exception v0

    const v2, 0x5839655d

    :try_start_3
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 623017
    :catchall_1
    move-exception v0

    const v1, -0x6fb53af9

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a()V
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 622970
    const-string v0, "reindexContactsNames"

    const v2, 0x2996ef9b

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 622971
    :try_start_0
    iget-object v0, p0, LX/3fg;->l:LX/2RQ;

    invoke-virtual {v0}, LX/2RQ;->a()LX/2RR;

    move-result-object v0

    sget-object v2, LX/2RS;->ID:LX/2RS;

    .line 622972
    iput-object v2, v0, LX/2RR;->n:LX/2RS;

    .line 622973
    move-object v0, v0

    .line 622974
    iget-object v2, p0, LX/3fg;->j:LX/2RC;

    sget-object v3, LX/2RV;->CONTACT:LX/2RV;

    invoke-virtual {v2, v0, v3}, LX/2RC;->a(LX/2RR;LX/2RV;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v5

    const/4 v4, 0x0

    .line 622975
    :try_start_1
    const-string v0, "data"

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 622976
    const-string v0, "_id"

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 622977
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 622978
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 622979
    iget-object v0, p0, LX/3fg;->b:LX/2Iv;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    .line 622980
    const v0, -0x4732dae7

    invoke-static {v8, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 622981
    :try_start_2
    invoke-static {p0}, LX/3fg;->b(LX/3fg;)V

    move v0, v1

    .line 622982
    :cond_0
    :goto_0
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 622983
    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 622984
    iget-object v10, p0, LX/3fg;->h:LX/3fh;

    invoke-virtual {v10, v9}, LX/3fh;->a(Ljava/lang/String;)Lcom/facebook/contacts/graphql/Contact;

    move-result-object v9

    invoke-virtual {v3, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 622985
    invoke-interface {v5, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v2, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 622986
    add-int/lit8 v0, v0, 0x1

    .line 622987
    const/16 v9, 0x14

    if-ne v0, v9, :cond_0

    .line 622988
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-static {p0, v0, v2}, LX/3fg;->a(LX/3fg;LX/0Px;LX/0Px;)V

    .line 622989
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 622990
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    move-object v3, v2

    move-object v2, v0

    move v0, v1

    goto :goto_0

    .line 622991
    :cond_1
    if-eqz v0, :cond_2

    .line 622992
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/3fg;->a(LX/3fg;LX/0Px;LX/0Px;)V

    .line 622993
    :cond_2
    iget-object v0, p0, LX/3fg;->k:LX/2JB;

    invoke-virtual {v0}, LX/2JB;->c()V

    .line 622994
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 622995
    const v0, -0x639cef55

    :try_start_3
    invoke-static {v8, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 622996
    if-eqz v5, :cond_3

    :try_start_4
    invoke-interface {v5}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 622997
    :cond_3
    const v0, 0x50dac0ab

    invoke-static {v0}, LX/02m;->a(I)V

    .line 622998
    return-void

    .line 622999
    :catchall_0
    move-exception v0

    const v1, 0x1d909d0f

    :try_start_5
    invoke-static {v8, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 623000
    :catch_0
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 623001
    :catchall_1
    move-exception v1

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    :goto_1
    if-eqz v5, :cond_4

    if-eqz v1, :cond_5

    :try_start_7
    invoke-interface {v5}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :cond_4
    :goto_2
    :try_start_8
    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 623002
    :catchall_2
    move-exception v0

    const v1, -0x10bcd307

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 623003
    :catch_1
    move-exception v2

    :try_start_9
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_5
    invoke-interface {v5}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_2

    :catchall_3
    move-exception v0

    move-object v1, v4

    goto :goto_1
.end method

.method public final a(LX/0Px;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactCoefficientModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 622937
    iget-object v0, p0, LX/3fg;->b:LX/2Iv;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 622938
    const v1, 0x11c75b49

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 622939
    :try_start_0
    sget-object v1, LX/2J3;->a:LX/0U1;

    .line 622940
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 622941
    const-string v2, "communication_rank"

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 622942
    const-string v2, "contacts_indexed_data"

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 622943
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v12

    const/4 v1, 0x0

    move v11, v1

    :goto_0
    if-ge v11, v12, :cond_7

    invoke-virtual {p1, v11}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactCoefficientModel;

    .line 622944
    invoke-virtual {v1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactCoefficientModel;->j()Ljava/lang/String;

    move-result-object v2

    .line 622945
    invoke-virtual {v1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactCoefficientModel;->k()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactCoefficientModel$RepresentedProfileModel;

    move-result-object v1

    .line 622946
    if-nez v1, :cond_2

    const/4 v1, 0x0

    move v10, v1

    .line 622947
    :goto_1
    const/4 v1, 0x0

    cmpl-float v1, v10, v1

    if-eqz v1, :cond_1

    .line 622948
    sget-object v1, LX/2Iz;->b:LX/0U1;

    .line 622949
    iget-object v3, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v3

    .line 622950
    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v5

    .line 622951
    const/4 v1, 0x0

    const-string v2, "contacts"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    sget-object v6, LX/2Iz;->a:LX/0U1;

    .line 622952
    iget-object v7, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v6, v7

    .line 622953
    aput-object v6, v3, v4

    invoke-virtual {v5}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "1"

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    const/4 v2, 0x0

    .line 622954
    if-eqz v3, :cond_0

    :try_start_1
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v1

    if-nez v1, :cond_3

    .line 622955
    :cond_0
    if-eqz v3, :cond_1

    :try_start_2
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 622956
    :cond_1
    :goto_2
    add-int/lit8 v1, v11, 0x1

    move v11, v1

    goto :goto_0

    .line 622957
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactCoefficientModel$RepresentedProfileModel;->j()D
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v4

    double-to-float v1, v4

    move v10, v1

    goto :goto_1

    .line 622958
    :cond_3
    const/4 v1, 0x0

    :try_start_3
    invoke-interface {v3, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-wide v4

    .line 622959
    if-eqz v3, :cond_4

    :try_start_4
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 622960
    :cond_4
    new-instance v1, LX/3hM;

    invoke-direct {v1, v4, v5}, LX/3hM;-><init>(J)V

    .line 622961
    const-string v2, "communication_rank"

    invoke-virtual {v1, v2, v10}, LX/3hM;->a(Ljava/lang/String;F)V

    .line 622962
    const-string v2, "contacts_indexed_data"

    const/4 v3, 0x0

    invoke-virtual {v1}, LX/3hM;->a()LX/0Px;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ContentValues;

    const v4, -0x4fc185d

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v1, 0x2722b23d

    invoke-static {v1}, LX/03h;->a(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 622963
    :catchall_0
    move-exception v1

    const v2, 0x2d1587a0

    invoke-static {v0, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v1

    .line 622964
    :catch_0
    move-exception v1

    :try_start_5
    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 622965
    :catchall_1
    move-exception v2

    move-object v13, v2

    move-object v2, v1

    move-object v1, v13

    :goto_3
    if-eqz v3, :cond_5

    if-eqz v2, :cond_6

    :try_start_6
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_5
    :goto_4
    :try_start_7
    throw v1

    :catch_1
    move-exception v3

    invoke-static {v2, v3}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_6
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto :goto_4

    .line 622966
    :cond_7
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 622967
    const v1, 0x7ad26a75

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 622968
    return-void

    .line 622969
    :catchall_2
    move-exception v1

    goto :goto_3
.end method

.method public final a(LX/0Py;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Py",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 622926
    invoke-virtual {p1}, LX/0Py;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 622927
    :goto_0
    return-void

    .line 622928
    :cond_0
    iget-object v0, p0, LX/3fg;->e:LX/0Sy;

    invoke-virtual {v0}, LX/0Sy;->c()V

    .line 622929
    iget-object v0, p0, LX/3fg;->b:LX/2Iv;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 622930
    const-string v0, "contact_id"

    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v0

    .line 622931
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "contact_internal_id in (select internal_id from contacts where "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 622932
    const v3, 0x50ce1b4b

    invoke-static {v1, v3}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 622933
    :try_start_0
    const-string v3, "contacts_indexed_data"

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 622934
    const-string v2, "contacts"

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 622935
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 622936
    const v0, -0x54085704

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v2, -0x4a15e830

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(LX/0Py;LX/3gm;LX/0ta;)V
    .locals 4
    .param p3    # LX/0ta;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Py",
            "<",
            "Lcom/facebook/contacts/graphql/Contact;",
            ">;",
            "LX/3gm;",
            "LX/0ta;",
            ")V"
        }
    .end annotation

    .prologue
    .line 622910
    invoke-virtual {p1}, LX/0Py;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 622911
    :goto_0
    return-void

    .line 622912
    :cond_0
    const-string v0, "insertContactsIntoDatabase"

    const v1, -0xe59de28

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 622913
    :try_start_0
    iget-object v0, p0, LX/3fg;->e:LX/0Sy;

    invoke-virtual {v0}, LX/0Sy;->c()V

    .line 622914
    iget-object v0, p0, LX/3fg;->b:LX/2Iv;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 622915
    const v0, -0x87fdd85

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 622916
    :try_start_1
    sget-object v0, LX/3gm;->REPLACE_ALL:LX/3gm;

    if-ne p2, v0, :cond_1

    .line 622917
    const-string v0, "contacts"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 622918
    const-string v0, "contacts_indexed_data"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 622919
    :cond_1
    invoke-virtual {p1}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;

    .line 622920
    invoke-virtual {p0, v0, p3}, LX/3fg;->a(Lcom/facebook/contacts/graphql/Contact;LX/0ta;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 622921
    :catchall_0
    move-exception v0

    const v2, -0x383a62a8

    :try_start_2
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 622922
    :catchall_1
    move-exception v0

    const v1, 0x4484591c

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 622923
    :cond_2
    :try_start_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 622924
    const v0, 0x61143972

    :try_start_4
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 622925
    const v0, 0x6045eb4c

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0
.end method

.method public final a(LX/18f;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/18f",
            "<",
            "Ljava/lang/String;",
            "LX/4hT;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 622868
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 622869
    invoke-virtual {p1}, LX/18f;->v()LX/0Py;

    move-result-object v1

    .line 622870
    invoke-virtual {v1}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 622871
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 622872
    const-string v5, "contact_id"

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 622873
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4hT;

    .line 622874
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 622875
    const-string v5, "type"

    const-string v6, "phone_e164"

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 622876
    invoke-virtual {v2, v4}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 622877
    const-string v5, "indexed_data"

    iget-object v6, p0, LX/3fg;->d:LX/3Lz;

    sget-object p1, LX/4hG;->E164:LX/4hG;

    invoke-virtual {v6, v1, p1}, LX/3Lz;->format(LX/4hT;LX/4hG;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 622878
    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 622879
    iget-object v2, p0, LX/3fg;->d:LX/3Lz;

    invoke-virtual {v2, v1}, LX/3Lz;->getNationalSignificantNumber(LX/4hT;)Ljava/lang/String;

    move-result-object v2

    .line 622880
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 622881
    const-string v6, "type"

    const-string p1, "phone_national"

    invoke-virtual {v5, v6, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 622882
    invoke-virtual {v5, v4}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 622883
    const-string v6, "indexed_data"

    invoke-virtual {v5, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 622884
    invoke-virtual {v0, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 622885
    iget-object v5, p0, LX/3fg;->d:LX/3Lz;

    invoke-virtual {v5, v1}, LX/3Lz;->getLengthOfGeographicalAreaCode(LX/4hT;)I

    move-result v5

    .line 622886
    if-lez v5, :cond_0

    .line 622887
    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 622888
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 622889
    const-string v6, "type"

    const-string p1, "phone_local"

    invoke-virtual {v5, v6, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 622890
    invoke-virtual {v5, v4}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 622891
    const-string v6, "indexed_data"

    invoke-virtual {v5, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 622892
    invoke-virtual {v0, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 622893
    :cond_0
    goto/16 :goto_0

    .line 622894
    :cond_1
    iget-object v1, p0, LX/3fg;->e:LX/0Sy;

    invoke-virtual {v1}, LX/0Sy;->c()V

    .line 622895
    iget-object v1, p0, LX/3fg;->b:LX/2Iv;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 622896
    const v1, -0x4aa6f57

    invoke-static {v2, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 622897
    :try_start_0
    const-string v1, "insert into contacts_indexed_data(contact_internal_id, type, indexed_data) values ((select internal_id from contacts where contact_id = ?), ?, ?)"

    invoke-virtual {v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    move-object v3, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 622898
    :try_start_1
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v5, :cond_2

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    .line 622899
    const/4 v6, 0x1

    const-string v7, "contact_id"

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 622900
    const/4 v6, 0x2

    const-string v7, "type"

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 622901
    const/4 v6, 0x3

    const-string v7, "indexed_data"

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v6, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 622902
    const v0, 0xcd3324c

    invoke-static {v0}, LX/03h;->a(I)V

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    const v0, -0x11084ad0

    invoke-static {v0}, LX/03h;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 622903
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 622904
    :cond_2
    :try_start_2
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 622905
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 622906
    const v0, 0x7a3c3070

    invoke-static {v2, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 622907
    return-void

    .line 622908
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 622909
    :catchall_1
    move-exception v0

    const v1, -0x6728f8e

    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method
