.class public LX/3sd;
.super LX/3sc;
.source ""


# instance fields
.field public final a:Landroid/view/WindowInsets;


# direct methods
.method public constructor <init>(Landroid/view/WindowInsets;)V
    .locals 0

    .prologue
    .line 644037
    invoke-direct {p0}, LX/3sc;-><init>()V

    .line 644038
    iput-object p1, p0, LX/3sd;->a:Landroid/view/WindowInsets;

    .line 644039
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 644046
    iget-object v0, p0, LX/3sd;->a:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->getSystemWindowInsetLeft()I

    move-result v0

    return v0
.end method

.method public final a(IIII)LX/3sc;
    .locals 2

    .prologue
    .line 644045
    new-instance v0, LX/3sd;

    iget-object v1, p0, LX/3sd;->a:Landroid/view/WindowInsets;

    invoke-virtual {v1, p1, p2, p3, p4}, Landroid/view/WindowInsets;->replaceSystemWindowInsets(IIII)Landroid/view/WindowInsets;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3sd;-><init>(Landroid/view/WindowInsets;)V

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 644044
    iget-object v0, p0, LX/3sd;->a:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->getSystemWindowInsetTop()I

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 644043
    iget-object v0, p0, LX/3sd;->a:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->getSystemWindowInsetRight()I

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 644042
    iget-object v0, p0, LX/3sd;->a:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->getSystemWindowInsetBottom()I

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 644041
    iget-object v0, p0, LX/3sd;->a:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->isConsumed()Z

    move-result v0

    return v0
.end method

.method public final f()LX/3sc;
    .locals 2

    .prologue
    .line 644040
    new-instance v0, LX/3sd;

    iget-object v1, p0, LX/3sd;->a:Landroid/view/WindowInsets;

    invoke-virtual {v1}, Landroid/view/WindowInsets;->consumeSystemWindowInsets()Landroid/view/WindowInsets;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3sd;-><init>(Landroid/view/WindowInsets;)V

    return-object v0
.end method
