.class public LX/4P5;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 699654
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 36

    .prologue
    .line 699528
    const/16 v32, 0x0

    .line 699529
    const/16 v31, 0x0

    .line 699530
    const/16 v30, 0x0

    .line 699531
    const/16 v29, 0x0

    .line 699532
    const/16 v28, 0x0

    .line 699533
    const/16 v27, 0x0

    .line 699534
    const/16 v26, 0x0

    .line 699535
    const/16 v25, 0x0

    .line 699536
    const/16 v24, 0x0

    .line 699537
    const/16 v23, 0x0

    .line 699538
    const/16 v22, 0x0

    .line 699539
    const/16 v21, 0x0

    .line 699540
    const/16 v20, 0x0

    .line 699541
    const/16 v19, 0x0

    .line 699542
    const/16 v18, 0x0

    .line 699543
    const/16 v17, 0x0

    .line 699544
    const/16 v16, 0x0

    .line 699545
    const/4 v15, 0x0

    .line 699546
    const/4 v14, 0x0

    .line 699547
    const/4 v13, 0x0

    .line 699548
    const/4 v12, 0x0

    .line 699549
    const/4 v11, 0x0

    .line 699550
    const/4 v10, 0x0

    .line 699551
    const/4 v9, 0x0

    .line 699552
    const/4 v8, 0x0

    .line 699553
    const/4 v7, 0x0

    .line 699554
    const/4 v6, 0x0

    .line 699555
    const/4 v5, 0x0

    .line 699556
    const/4 v4, 0x0

    .line 699557
    const/4 v3, 0x0

    .line 699558
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v33

    sget-object v34, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    if-eq v0, v1, :cond_1

    .line 699559
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 699560
    const/4 v3, 0x0

    .line 699561
    :goto_0
    return v3

    .line 699562
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 699563
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v33

    sget-object v34, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    if-eq v0, v1, :cond_1a

    .line 699564
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v33

    .line 699565
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 699566
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v34

    sget-object v35, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    if-eq v0, v1, :cond_1

    if-eqz v33, :cond_1

    .line 699567
    const-string v34, "ad"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    .line 699568
    invoke-static/range {p0 .. p1}, LX/4Kf;->a(LX/15w;LX/186;)I

    move-result v32

    goto :goto_1

    .line 699569
    :cond_2
    const-string v34, "can_watch_and_browse"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_3

    .line 699570
    const/4 v7, 0x1

    .line 699571
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v31

    goto :goto_1

    .line 699572
    :cond_3
    const-string v34, "destination_type"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_4

    .line 699573
    const/4 v6, 0x1

    .line 699574
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    move-result-object v30

    goto :goto_1

    .line 699575
    :cond_4
    const-string v34, "event"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_5

    .line 699576
    invoke-static/range {p0 .. p1}, LX/4M6;->a(LX/15w;LX/186;)I

    move-result v29

    goto :goto_1

    .line 699577
    :cond_5
    const-string v34, "header_color"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_6

    .line 699578
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    goto :goto_1

    .line 699579
    :cond_6
    const-string v34, "link_description"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_7

    .line 699580
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    goto/16 :goto_1

    .line 699581
    :cond_7
    const-string v34, "link_display"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_8

    .line 699582
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v26

    goto/16 :goto_1

    .line 699583
    :cond_8
    const-string v34, "link_icon_image"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_9

    .line 699584
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v25

    goto/16 :goto_1

    .line 699585
    :cond_9
    const-string v34, "link_style"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_a

    .line 699586
    const/4 v5, 0x1

    .line 699587
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    move-result-object v24

    goto/16 :goto_1

    .line 699588
    :cond_a
    const-string v34, "link_target_store_data"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_b

    .line 699589
    invoke-static/range {p0 .. p1}, LX/4P6;->a(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 699590
    :cond_b
    const-string v34, "link_title"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_c

    .line 699591
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    goto/16 :goto_1

    .line 699592
    :cond_c
    const-string v34, "link_type"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_d

    .line 699593
    const/4 v4, 0x1

    .line 699594
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v21

    goto/16 :goto_1

    .line 699595
    :cond_d
    const-string v34, "link_video_endscreen_icon"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_e

    .line 699596
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 699597
    :cond_e
    const-string v34, "logo_uri"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_f

    .line 699598
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    goto/16 :goto_1

    .line 699599
    :cond_f
    const-string v34, "page"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_10

    .line 699600
    invoke-static/range {p0 .. p1}, LX/2bc;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 699601
    :cond_10
    const-string v34, "stateful_title"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_11

    .line 699602
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    goto/16 :goto_1

    .line 699603
    :cond_11
    const-string v34, "title"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_12

    .line 699604
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto/16 :goto_1

    .line 699605
    :cond_12
    const-string v34, "url"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_13

    .line 699606
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 699607
    :cond_13
    const-string v34, "video_annotations"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_14

    .line 699608
    invoke-static/range {p0 .. p1}, LX/4UB;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 699609
    :cond_14
    const-string v34, "messenger_extensions_payment_privacy_policy"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_15

    .line 699610
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto/16 :goto_1

    .line 699611
    :cond_15
    const-string v34, "messenger_extensions_whitelisted_domains"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_16

    .line 699612
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 699613
    :cond_16
    const-string v34, "messenger_extensions_user_profile"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_17

    .line 699614
    invoke-static/range {p0 .. p1}, LX/4PX;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 699615
    :cond_17
    const-string v34, "cannot_watch_and_browse_reason"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_18

    .line 699616
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 699617
    :cond_18
    const-string v34, "offer"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_19

    .line 699618
    invoke-static/range {p0 .. p1}, LX/4Q2;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 699619
    :cond_19
    const-string v34, "is_on_fb_event_ticket_link"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_0

    .line 699620
    const/4 v3, 0x1

    .line 699621
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto/16 :goto_1

    .line 699622
    :cond_1a
    const/16 v33, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 699623
    const/16 v33, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v33

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 699624
    if-eqz v7, :cond_1b

    .line 699625
    const/4 v7, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 699626
    :cond_1b
    if-eqz v6, :cond_1c

    .line 699627
    const/4 v6, 0x2

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v6, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 699628
    :cond_1c
    const/4 v6, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 699629
    const/4 v6, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 699630
    const/4 v6, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 699631
    const/4 v6, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 699632
    const/4 v6, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 699633
    if-eqz v5, :cond_1d

    .line 699634
    const/16 v5, 0x8

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v5, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 699635
    :cond_1d
    const/16 v5, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 699636
    const/16 v5, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 699637
    if-eqz v4, :cond_1e

    .line 699638
    const/16 v4, 0xb

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 699639
    :cond_1e
    const/16 v4, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 699640
    const/16 v4, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 699641
    const/16 v4, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 699642
    const/16 v4, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 699643
    const/16 v4, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 699644
    const/16 v4, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 699645
    const/16 v4, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 699646
    const/16 v4, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 699647
    const/16 v4, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 699648
    const/16 v4, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 699649
    const/16 v4, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 699650
    const/16 v4, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 699651
    if-eqz v3, :cond_1f

    .line 699652
    const/16 v3, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->a(IZ)V

    .line 699653
    :cond_1f
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
