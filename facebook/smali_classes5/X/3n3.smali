.class public final LX/3n3;
.super Landroid/util/LruCache;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache",
        "<",
        "Ljava/lang/String;",
        "LX/1By;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1Bn;


# direct methods
.method public constructor <init>(LX/1Bn;I)V
    .locals 0

    .prologue
    .line 637645
    iput-object p1, p0, LX/3n3;->a:LX/1Bn;

    .line 637646
    invoke-direct {p0, p2}, Landroid/util/LruCache;-><init>(I)V

    .line 637647
    return-void
.end method


# virtual methods
.method public final entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 637648
    check-cast p2, Ljava/lang/String;

    check-cast p3, LX/1By;

    .line 637649
    iget-object v0, p3, LX/1By;->c:Ljava/lang/String;

    move-object v1, v0

    .line 637650
    invoke-virtual {p3}, LX/1By;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 637651
    iget-object v0, p0, LX/3n3;->a:LX/1Bn;

    iget-object v0, v0, LX/1Bn;->q:LX/2n5;

    .line 637652
    iget-object v2, v0, LX/2n5;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1fI;

    .line 637653
    const-string p1, "ATTACHMENT_LINK"

    const/4 v0, 0x0

    invoke-static {v2, v1, p1, v0}, LX/1fI;->b(LX/1fI;Ljava/lang/String;Ljava/lang/String;I)V

    .line 637654
    goto :goto_0

    .line 637655
    :cond_0
    iget-object v0, p0, LX/3n3;->a:LX/1Bn;

    invoke-virtual {v0}, LX/1Bn;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 637656
    iget-object v0, p0, LX/3n3;->a:LX/1Bn;

    iget-object v0, v0, LX/1Bn;->j:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 637657
    iget-object v2, p0, LX/3n3;->a:LX/1Bn;

    iget-object v2, v2, LX/1Bn;->s:LX/2n7;

    invoke-virtual {v2, v1, v0}, LX/2n7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 637658
    :cond_1
    iget-object v0, p0, LX/3n3;->a:LX/1Bn;

    iget-object v0, v0, LX/1Bn;->j:Ljava/util/Map;

    .line 637659
    iget-object v1, p3, LX/1By;->c:Ljava/lang/String;

    move-object v1, v1

    .line 637660
    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 637661
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/3n3;->a:LX/1Bn;

    iget-object v1, v1, LX/1Bn;->v:LX/2nB;

    invoke-virtual {v1, p2}, LX/2nB;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 637662
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_2

    .line 637663
    iget-object v0, p0, LX/3n3;->a:LX/1Bn;

    iget-object v0, v0, LX/1Bn;->e:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/1Bn;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".BrowserPrefetchDiskCache.entryRemoved"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed delete existing cache file for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 637664
    :cond_2
    return-void
.end method

.method public final sizeOf(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 637665
    check-cast p2, LX/1By;

    .line 637666
    iget v0, p2, LX/1By;->b:I

    move v0, v0

    .line 637667
    return v0
.end method
