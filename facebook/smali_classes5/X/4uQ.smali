.class public final LX/4uQ;
.super LX/2wf;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2wf",
        "<",
        "LX/4sT;",
        ">;"
    }
.end annotation


# instance fields
.field private d:I

.field private e:Z


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)LX/2NW;
    .locals 4

    const/4 v0, 0x0

    monitor-enter v0

    :try_start_0
    new-instance v0, Lcom/google/android/gms/common/ConnectionResult;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/ConnectionResult;-><init>(I)V

    const/4 v3, 0x0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-virtual {v3}, LX/01J;->size()I

    move-result v1

    if-ge v2, v1, :cond_0

    invoke-virtual {v3, v2}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4uL;

    invoke-virtual {p0, v1, v0}, LX/4uQ;->a(LX/4uL;Lcom/google/android/gms/common/ConnectionResult;)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {v0}, LX/01J;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    new-instance v0, LX/4sU;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, LX/4sU;-><init>(Lcom/google/android/gms/common/api/Status;LX/026;)V

    :goto_1
    const/4 v1, 0x0

    monitor-exit v1

    return-object v0

    :cond_1
    new-instance v0, LX/4sT;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, LX/4sT;-><init>(Lcom/google/android/gms/common/api/Status;LX/026;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    const/4 v1, 0x0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/4uL;Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4uL",
            "<*>;",
            "Lcom/google/android/gms/common/ConnectionResult;",
            ")V"
        }
    .end annotation

    const/4 v2, 0x1

    const/4 v0, 0x0

    monitor-enter v0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v0, p1, p2}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, p0, LX/4uQ;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/4uQ;->d:I

    invoke-virtual {p2}, Lcom/google/android/gms/common/ConnectionResult;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4uQ;->e:Z

    :cond_0
    iget v0, p0, LX/4uQ;->d:I

    if-nez v0, :cond_1

    iget-boolean v0, p0, LX/4uQ;->e:Z

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0xd

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    move-object v1, v0

    :goto_0
    const/4 v0, 0x0

    invoke-virtual {v0}, LX/01J;->size()I

    move-result v0

    if-ne v0, v2, :cond_3

    new-instance v0, LX/4sU;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/4sU;-><init>(Lcom/google/android/gms/common/api/Status;LX/026;)V

    :goto_1
    invoke-virtual {p0, v0}, LX/2wf;->a(LX/2NW;)V

    :cond_1
    const/4 v0, 0x0

    monitor-exit v0

    return-void

    :cond_2
    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    move-object v1, v0

    goto :goto_0

    :cond_3
    new-instance v0, LX/4sT;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/4sT;-><init>(Lcom/google/android/gms/common/api/Status;LX/026;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    const/4 v1, 0x0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
