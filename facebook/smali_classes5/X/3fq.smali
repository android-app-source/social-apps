.class public LX/3fq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final j:Ljava/lang/Object;


# instance fields
.field public final a:LX/0SG;

.field private final b:LX/3fr;

.field public final c:LX/3fX;

.field private final d:LX/3fs;

.field private final e:LX/11H;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Jp;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3fg;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6Nd;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/2RQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 623644
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/3fq;->j:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/3fr;LX/3fX;LX/3fs;LX/11H;LX/0Or;LX/0Ot;LX/0Ot;LX/2RQ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/3fr;",
            "LX/3fX;",
            "LX/3fs;",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "LX/0Or",
            "<",
            "LX/2Jp;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3fg;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6Nd;",
            ">;",
            "LX/2RQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 623645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 623646
    iput-object p1, p0, LX/3fq;->a:LX/0SG;

    .line 623647
    iput-object p2, p0, LX/3fq;->b:LX/3fr;

    .line 623648
    iput-object p3, p0, LX/3fq;->c:LX/3fX;

    .line 623649
    iput-object p4, p0, LX/3fq;->d:LX/3fs;

    .line 623650
    iput-object p5, p0, LX/3fq;->e:LX/11H;

    .line 623651
    iput-object p6, p0, LX/3fq;->f:LX/0Or;

    .line 623652
    iput-object p7, p0, LX/3fq;->g:LX/0Ot;

    .line 623653
    iput-object p8, p0, LX/3fq;->h:LX/0Ot;

    .line 623654
    iput-object p9, p0, LX/3fq;->i:LX/2RQ;

    .line 623655
    return-void
.end method

.method public static a(LX/0QB;)LX/3fq;
    .locals 7

    .prologue
    .line 623616
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 623617
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 623618
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 623619
    if-nez v1, :cond_0

    .line 623620
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 623621
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 623622
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 623623
    sget-object v1, LX/3fq;->j:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 623624
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 623625
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 623626
    :cond_1
    if-nez v1, :cond_4

    .line 623627
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 623628
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 623629
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/3fq;->b(LX/0QB;)LX/3fq;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 623630
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 623631
    if-nez v1, :cond_2

    .line 623632
    sget-object v0, LX/3fq;->j:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3fq;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 623633
    :goto_1
    if-eqz v0, :cond_3

    .line 623634
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 623635
    :goto_3
    check-cast v0, LX/3fq;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 623636
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 623637
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 623638
    :catchall_1
    move-exception v0

    .line 623639
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 623640
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 623641
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 623642
    :cond_2
    :try_start_8
    sget-object v0, LX/3fq;->j:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3fq;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a(LX/3fq;Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;Ljava/util/HashSet;LX/0Pz;)Lcom/facebook/contacts/server/FetchContactsResult;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;",
            "Ljava/util/HashSet",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;",
            "LX/0Pz",
            "<",
            "Lcom/facebook/contacts/graphql/Contact;",
            ">;)",
            "Lcom/facebook/contacts/server/FetchContactsResult;"
        }
    .end annotation

    .prologue
    .line 623588
    iget-object v0, p1, Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;->b:LX/0rS;

    move-object v3, v0

    .line 623589
    sget-object v1, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    .line 623590
    new-instance v4, Ljava/util/HashMap;

    invoke-virtual {p2}, Ljava/util/HashSet;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 623591
    invoke-virtual {p2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 623592
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 623593
    :cond_0
    iget-object v0, p0, LX/3fq;->b:LX/3fr;

    iget-object v2, p0, LX/3fq;->i:LX/2RQ;

    .line 623594
    invoke-virtual {v2}, LX/2RQ;->a()LX/2RR;

    move-result-object v5

    .line 623595
    iput-object p2, v5, LX/2RR;->d:Ljava/util/Collection;

    .line 623596
    move-object v5, v5

    .line 623597
    move-object v2, v5

    .line 623598
    invoke-virtual {v0, v2}, LX/3fr;->a(LX/2RR;)LX/6N1;

    move-result-object v5

    .line 623599
    const/4 v2, 0x0

    .line 623600
    :goto_1
    :try_start_0
    invoke-interface {v5}, LX/6N1;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 623601
    invoke-interface {v5}, LX/6N1;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;

    .line 623602
    invoke-static {p0, v0}, LX/3fq;->a(LX/3fq;Lcom/facebook/contacts/graphql/Contact;)Z

    move-result v6

    .line 623603
    if-nez v6, :cond_1

    sget-object v7, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    if-eq v3, v7, :cond_8

    .line 623604
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 623605
    invoke-virtual {p3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 623606
    if-eqz v6, :cond_3

    .line 623607
    iget-object v6, p0, LX/3fq;->c:LX/3fX;

    invoke-virtual {v6, v0}, LX/3fX;->a(Lcom/facebook/contacts/graphql/Contact;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_1

    .line 623608
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 623609
    :catchall_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_2
    if-eqz v5, :cond_2

    if-eqz v1, :cond_7

    :try_start_2
    invoke-interface {v5}, LX/6N1;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_2
    :goto_3
    throw v0

    .line 623610
    :cond_3
    :try_start_3
    sget-object v0, LX/0ta;->FROM_CACHE_STALE:LX/0ta;

    :goto_4
    move-object v1, v0

    .line 623611
    goto :goto_1

    .line 623612
    :cond_4
    invoke-virtual {v4}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    if-eq v3, v0, :cond_6

    invoke-direct {p0, p1}, LX/3fq;->c(Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;)Lcom/facebook/contacts/server/FetchContactsResult;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    .line 623613
    :goto_5
    if-eqz v5, :cond_5

    invoke-interface {v5}, LX/6N1;->close()V

    :cond_5
    return-object v0

    .line 623614
    :cond_6
    :try_start_4
    new-instance v0, Lcom/facebook/contacts/server/FetchContactsResult;

    iget-object v3, p0, LX/3fq;->a:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v6

    invoke-virtual {p3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-direct {v0, v1, v6, v7, v3}, Lcom/facebook/contacts/server/FetchContactsResult;-><init>(LX/0ta;JLX/0Px;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_5

    .line 623615
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_7
    invoke-interface {v5}, LX/6N1;->close()V

    goto :goto_3

    :cond_8
    move-object v0, v1

    goto :goto_4
.end method

.method private static a(LX/3fq;Lcom/facebook/contacts/graphql/Contact;)Z
    .locals 4

    .prologue
    .line 623643
    iget-object v0, p0, LX/3fq;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/2Jp;->OMNISTORE_CONTACTS_COLLECTION:LX/2Jp;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/3fq;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->F()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x5265c00

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/3fq;
    .locals 10

    .prologue
    .line 623583
    new-instance v0, LX/3fq;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p0}, LX/3fr;->a(LX/0QB;)LX/3fr;

    move-result-object v2

    check-cast v2, LX/3fr;

    invoke-static {p0}, LX/3fX;->a(LX/0QB;)LX/3fX;

    move-result-object v3

    check-cast v3, LX/3fX;

    .line 623584
    new-instance v8, LX/3fs;

    invoke-static {p0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v4

    check-cast v4, LX/0sO;

    invoke-static {p0}, LX/3fb;->a(LX/0QB;)LX/3fb;

    move-result-object v5

    check-cast v5, LX/3fb;

    invoke-static {p0}, LX/3fc;->b(LX/0QB;)LX/3fc;

    move-result-object v6

    check-cast v6, LX/3fc;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-direct {v8, v4, v5, v6, v7}, LX/3fs;-><init>(LX/0sO;LX/3fb;LX/3fc;LX/0SG;)V

    .line 623585
    move-object v4, v8

    .line 623586
    check-cast v4, LX/3fs;

    invoke-static {p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v5

    check-cast v5, LX/11H;

    const/16 v6, 0x438

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x416

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1a18

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {p0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v9

    check-cast v9, LX/2RQ;

    invoke-direct/range {v0 .. v9}, LX/3fq;-><init>(LX/0SG;LX/3fr;LX/3fX;LX/3fs;LX/11H;LX/0Or;LX/0Ot;LX/0Ot;LX/2RQ;)V

    .line 623587
    return-object v0
.end method

.method private c(Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;)Lcom/facebook/contacts/server/FetchContactsResult;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 623570
    iget-object v0, p0, LX/3fq;->e:LX/11H;

    iget-object v1, p0, LX/3fq;->d:LX/3fs;

    invoke-virtual {v0, v1, p1}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/server/FetchContactsResult;

    .line 623571
    iget-object v1, v0, Lcom/facebook/contacts/server/FetchContactsResult;->a:LX/0Px;

    move-object v5, v1

    .line 623572
    sget-object v2, LX/6Mv;->a:[I

    iget-object v1, p0, LX/3fq;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Jp;

    invoke-virtual {v1}, LX/2Jp;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 623573
    :cond_0
    :goto_0
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v4

    move v2, v3

    :goto_1
    if-ge v2, v4, :cond_1

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/graphql/Contact;

    .line 623574
    iget-object v3, p0, LX/3fq;->c:LX/3fX;

    invoke-virtual {v3, v1}, LX/3fX;->a(Lcom/facebook/contacts/graphql/Contact;)V

    .line 623575
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 623576
    :pswitch_0
    iget-object v1, p0, LX/3fq;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3fg;

    sget-object v2, LX/3gm;->REPLACE:LX/3gm;

    .line 623577
    iget-object v4, v0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v4, v4

    .line 623578
    invoke-virtual {v1, v5, v2, v4}, LX/3fg;->a(LX/0Py;LX/3gm;LX/0ta;)V

    goto :goto_0

    .line 623579
    :pswitch_1
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    :goto_2
    if-ge v4, v6, :cond_0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/graphql/Contact;

    .line 623580
    iget-object v2, p0, LX/3fq;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6Nd;

    invoke-virtual {v2, v1}, LX/6Nd;->a(Lcom/facebook/contacts/graphql/Contact;)V

    .line 623581
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    .line 623582
    :cond_1
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;)Lcom/facebook/contacts/server/FetchContactsResult;
    .locals 10

    .prologue
    .line 623554
    iget-object v0, p1, Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;->a:LX/0Rf;

    move-object v0, v0

    .line 623555
    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 623556
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v2

    sget-object v3, LX/0XG;->FACEBOOK:LX/0XG;

    if-eq v2, v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v2

    sget-object v3, LX/0XG;->FACEBOOK_CONTACT:LX/0XG;

    if-eq v2, v3, :cond_0

    .line 623557
    new-instance v1, LX/6Mw;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported UserKey type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/6Mw;-><init>(Ljava/lang/String;)V

    throw v1

    .line 623558
    :cond_1
    iget-object v0, p1, Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;->b:LX/0rS;

    move-object v0, v0

    .line 623559
    sget-object v1, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-ne v0, v1, :cond_2

    invoke-direct {p0, p1}, LX/3fq;->c(Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;)Lcom/facebook/contacts/server/FetchContactsResult;

    move-result-object v0

    :goto_0
    return-object v0

    .line 623560
    :cond_2
    iget-object v4, p1, Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;->a:LX/0Rf;

    move-object v4, v4

    .line 623561
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5, v4}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 623562
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 623563
    invoke-virtual {v4}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_3
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/user/model/UserKey;

    .line 623564
    iget-object v8, p0, LX/3fq;->c:LX/3fX;

    invoke-virtual {v8, v4}, LX/3fX;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/contacts/graphql/Contact;

    move-result-object v8

    .line 623565
    if-eqz v8, :cond_3

    .line 623566
    invoke-virtual {v6, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 623567
    invoke-virtual {v5, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 623568
    :cond_4
    invoke-virtual {v5}, Ljava/util/HashSet;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_5

    new-instance v4, Lcom/facebook/contacts/server/FetchContactsResult;

    sget-object v5, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    iget-object v7, p0, LX/3fq;->a:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v8

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    invoke-direct {v4, v5, v8, v9, v6}, Lcom/facebook/contacts/server/FetchContactsResult;-><init>(LX/0ta;JLX/0Px;)V

    :goto_2
    move-object v0, v4

    .line 623569
    goto :goto_0

    :cond_5
    invoke-static {p0, p1, v5, v6}, LX/3fq;->a(LX/3fq;Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;Ljava/util/HashSet;LX/0Pz;)Lcom/facebook/contacts/server/FetchContactsResult;

    move-result-object v4

    goto :goto_2
.end method
