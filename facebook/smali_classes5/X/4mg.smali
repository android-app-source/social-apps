.class public LX/4mg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/4mg;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/ui/dialogs/DialogFragmentEventListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/ui/dialogs/DialogFragmentEventListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/ui/dialogs/DialogFragmentEventListener;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 806133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 806134
    iput-object p1, p0, LX/4mg;->a:LX/0Ot;

    .line 806135
    invoke-static {}, LX/0RA;->c()Ljava/util/LinkedHashSet;

    move-result-object v0

    iput-object v0, p0, LX/4mg;->b:Ljava/util/Set;

    .line 806136
    return-void
.end method

.method public static a(LX/0QB;)LX/4mg;
    .locals 5

    .prologue
    .line 806117
    sget-object v0, LX/4mg;->c:LX/4mg;

    if-nez v0, :cond_1

    .line 806118
    const-class v1, LX/4mg;

    monitor-enter v1

    .line 806119
    :try_start_0
    sget-object v0, LX/4mg;->c:LX/4mg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 806120
    if-eqz v2, :cond_0

    .line 806121
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 806122
    new-instance v3, LX/4mg;

    .line 806123
    new-instance v4, LX/4mk;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-direct {v4, p0}, LX/4mk;-><init>(LX/0QB;)V

    move-object v4, v4

    .line 806124
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-static {v4, p0}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v4

    move-object v4, v4

    .line 806125
    invoke-direct {v3, v4}, LX/4mg;-><init>(LX/0Ot;)V

    .line 806126
    move-object v0, v3

    .line 806127
    sput-object v0, LX/4mg;->c:LX/4mg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 806128
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 806129
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 806130
    :cond_1
    sget-object v0, LX/4mg;->c:LX/4mg;

    return-object v0

    .line 806131
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 806132
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 806137
    iget-object v0, p0, LX/4mg;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 806138
    :cond_0
    iget-object v1, p0, LX/4mg;->b:Ljava/util/Set;

    monitor-enter v1

    .line 806139
    :try_start_0
    iget-object v0, p0, LX/4mg;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_1

    .line 806140
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 806099
    iget-object v0, p0, LX/4mg;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3zb;

    .line 806100
    invoke-virtual {v0, p1}, LX/3zb;->a(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 806101
    :cond_0
    iget-object v1, p0, LX/4mg;->b:Ljava/util/Set;

    monitor-enter v1

    .line 806102
    :try_start_0
    iget-object v0, p0, LX/4mg;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3zb;

    .line 806103
    invoke-virtual {v0, p1}, LX/3zb;->a(Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 806104
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(Lcom/facebook/ui/dialogs/FbDialogFragment;)V
    .locals 3

    .prologue
    .line 806111
    iget-object v0, p0, LX/4mg;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3zb;

    .line 806112
    invoke-virtual {v0, p1}, LX/3zb;->a(Lcom/facebook/ui/dialogs/FbDialogFragment;)V

    goto :goto_0

    .line 806113
    :cond_0
    iget-object v1, p0, LX/4mg;->b:Ljava/util/Set;

    monitor-enter v1

    .line 806114
    :try_start_0
    iget-object v0, p0, LX/4mg;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3zb;

    .line 806115
    invoke-virtual {v0, p1}, LX/3zb;->a(Lcom/facebook/ui/dialogs/FbDialogFragment;)V

    goto :goto_1

    .line 806116
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final b(Lcom/facebook/ui/dialogs/FbDialogFragment;)V
    .locals 3

    .prologue
    .line 806105
    iget-object v0, p0, LX/4mg;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3zb;

    .line 806106
    invoke-virtual {v0, p1}, LX/3zb;->b(Lcom/facebook/ui/dialogs/FbDialogFragment;)V

    goto :goto_0

    .line 806107
    :cond_0
    iget-object v1, p0, LX/4mg;->b:Ljava/util/Set;

    monitor-enter v1

    .line 806108
    :try_start_0
    iget-object v0, p0, LX/4mg;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3zb;

    .line 806109
    invoke-virtual {v0, p1}, LX/3zb;->b(Lcom/facebook/ui/dialogs/FbDialogFragment;)V

    goto :goto_1

    .line 806110
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
