.class public abstract LX/4wD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public b:I

.field public c:I

.field public d:LX/0Qx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Qx",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public e:Ljava/util/concurrent/atomic/AtomicReferenceArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReferenceArray",
            "<",
            "LX/0R1",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field public f:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public g:LX/4wa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Qd",
            "<TK;TV;>.WriteThroughEntry;"
        }
    .end annotation
.end field

.field public h:LX/4wa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Qd",
            "<TK;TV;>.WriteThroughEntry;"
        }
    .end annotation
.end field

.field public final synthetic i:LX/0Qd;


# direct methods
.method public constructor <init>(LX/0Qd;)V
    .locals 1

    .prologue
    .line 819960
    iput-object p1, p0, LX/4wD;->i:LX/0Qd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 819961
    iget-object v0, p1, LX/0Qd;->d:[LX/0Qx;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/4wD;->b:I

    .line 819962
    const/4 v0, -0x1

    iput v0, p0, LX/4wD;->c:I

    .line 819963
    invoke-direct {p0}, LX/4wD;->b()V

    .line 819964
    return-void
.end method

.method private a(LX/0R1;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 819948
    :try_start_0
    iget-object v0, p0, LX/4wD;->i:LX/0Qd;

    iget-object v0, v0, LX/0Qd;->q:LX/0QV;

    invoke-virtual {v0}, LX/0QV;->read()J

    move-result-wide v0

    .line 819949
    invoke-interface {p1}, LX/0R1;->getKey()Ljava/lang/Object;

    move-result-object v2

    .line 819950
    iget-object v3, p0, LX/4wD;->i:LX/0Qd;

    const/4 v4, 0x0

    .line 819951
    invoke-interface {p1}, LX/0R1;->getKey()Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_2

    .line 819952
    :cond_0
    :goto_0
    move-object v0, v4

    .line 819953
    if-eqz v0, :cond_1

    .line 819954
    new-instance v1, LX/4wa;

    iget-object v3, p0, LX/4wD;->i:LX/0Qd;

    invoke-direct {v1, v3, v2, v0}, LX/4wa;-><init>(LX/0Qd;Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, LX/4wD;->g:LX/4wa;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 819955
    iget-object v0, p0, LX/4wD;->d:LX/0Qx;

    invoke-virtual {v0}, LX/0Qx;->b()V

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    iget-object v0, p0, LX/4wD;->d:LX/0Qx;

    invoke-virtual {v0}, LX/0Qx;->b()V

    const/4 v0, 0x0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/4wD;->d:LX/0Qx;

    invoke-virtual {v1}, LX/0Qx;->b()V

    throw v0

    .line 819956
    :cond_2
    invoke-interface {p1}, LX/0R1;->getValueReference()LX/0Qf;

    move-result-object v5

    invoke-interface {v5}, LX/0Qf;->get()Ljava/lang/Object;

    move-result-object v5

    .line 819957
    if-eqz v5, :cond_0

    .line 819958
    invoke-virtual {v3, p1, v0, v1}, LX/0Qd;->b(LX/0R1;J)Z

    move-result v6

    if-nez v6, :cond_0

    move-object v4, v5

    .line 819959
    goto :goto_0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 819938
    const/4 v0, 0x0

    iput-object v0, p0, LX/4wD;->g:LX/4wa;

    .line 819939
    invoke-direct {p0}, LX/4wD;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 819940
    :cond_0
    :goto_0
    return-void

    .line 819941
    :cond_1
    invoke-direct {p0}, LX/4wD;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 819942
    :cond_2
    iget v0, p0, LX/4wD;->b:I

    if-ltz v0, :cond_0

    .line 819943
    iget-object v0, p0, LX/4wD;->i:LX/0Qd;

    iget-object v0, v0, LX/0Qd;->d:[LX/0Qx;

    iget v1, p0, LX/4wD;->b:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, LX/4wD;->b:I

    aget-object v0, v0, v1

    iput-object v0, p0, LX/4wD;->d:LX/0Qx;

    .line 819944
    iget-object v0, p0, LX/4wD;->d:LX/0Qx;

    iget v0, v0, LX/0Qx;->count:I

    if-eqz v0, :cond_2

    .line 819945
    iget-object v0, p0, LX/4wD;->d:LX/0Qx;

    iget-object v0, v0, LX/0Qx;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iput-object v0, p0, LX/4wD;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 819946
    iget-object v0, p0, LX/4wD;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/4wD;->c:I

    .line 819947
    invoke-direct {p0}, LX/4wD;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 819931
    iget-object v0, p0, LX/4wD;->f:LX/0R1;

    if-eqz v0, :cond_1

    .line 819932
    iget-object v0, p0, LX/4wD;->f:LX/0R1;

    invoke-interface {v0}, LX/0R1;->getNext()LX/0R1;

    move-result-object v0

    iput-object v0, p0, LX/4wD;->f:LX/0R1;

    :goto_0
    iget-object v0, p0, LX/4wD;->f:LX/0R1;

    if-eqz v0, :cond_1

    .line 819933
    iget-object v0, p0, LX/4wD;->f:LX/0R1;

    invoke-direct {p0, v0}, LX/4wD;->a(LX/0R1;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 819934
    const/4 v0, 0x1

    .line 819935
    :goto_1
    return v0

    .line 819936
    :cond_0
    iget-object v0, p0, LX/4wD;->f:LX/0R1;

    invoke-interface {v0}, LX/0R1;->getNext()LX/0R1;

    move-result-object v0

    iput-object v0, p0, LX/4wD;->f:LX/0R1;

    goto :goto_0

    .line 819937
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private d()Z
    .locals 3

    .prologue
    .line 819926
    :cond_0
    iget v0, p0, LX/4wD;->c:I

    if-ltz v0, :cond_2

    .line 819927
    iget-object v0, p0, LX/4wD;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget v1, p0, LX/4wD;->c:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, LX/4wD;->c:I

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0R1;

    iput-object v0, p0, LX/4wD;->f:LX/0R1;

    if-eqz v0, :cond_0

    .line 819928
    iget-object v0, p0, LX/4wD;->f:LX/0R1;

    invoke-direct {p0, v0}, LX/4wD;->a(LX/0R1;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, LX/4wD;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 819929
    :cond_1
    const/4 v0, 0x1

    .line 819930
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/4wa;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Qd",
            "<TK;TV;>.WriteThroughEntry;"
        }
    .end annotation

    .prologue
    .line 819921
    iget-object v0, p0, LX/4wD;->g:LX/4wa;

    if-nez v0, :cond_0

    .line 819922
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 819923
    :cond_0
    iget-object v0, p0, LX/4wD;->g:LX/4wa;

    iput-object v0, p0, LX/4wD;->h:LX/4wa;

    .line 819924
    invoke-direct {p0}, LX/4wD;->b()V

    .line 819925
    iget-object v0, p0, LX/4wD;->h:LX/4wa;

    return-object v0
.end method

.method public final hasNext()Z
    .locals 1

    .prologue
    .line 819920
    iget-object v0, p0, LX/4wD;->g:LX/4wa;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 819915
    iget-object v0, p0, LX/4wD;->h:LX/4wa;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 819916
    iget-object v0, p0, LX/4wD;->i:LX/0Qd;

    iget-object v1, p0, LX/4wD;->h:LX/4wa;

    invoke-virtual {v1}, LX/4wa;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Qd;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 819917
    const/4 v0, 0x0

    iput-object v0, p0, LX/4wD;->h:LX/4wa;

    .line 819918
    return-void

    .line 819919
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
