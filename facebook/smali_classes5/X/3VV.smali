.class public LX/3VV;
.super Lcom/facebook/events/widget/eventcard/EventsCardView;
.source ""

# interfaces
.implements LX/35q;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field public b:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587849
    new-instance v0, LX/3VW;

    invoke-direct {v0}, LX/3VW;-><init>()V

    sput-object v0, LX/3VV;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 587841
    invoke-direct {p0, p1}, Lcom/facebook/events/widget/eventcard/EventsCardView;-><init>(Landroid/content/Context;)V

    .line 587842
    const-class v0, LX/3VV;

    invoke-static {v0, p0}, LX/3VV;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 587843
    iput-object p1, p0, LX/3VV;->c:Landroid/content/Context;

    .line 587844
    const-string v1, "newsfeed_angora_attachment_view"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {p0, v1, v2}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/Class;)V

    .line 587845
    sget-object v0, LX/1vY;->ATTACHMENT:LX/1vY;

    invoke-static {p0, v0}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 587846
    invoke-virtual {p0}, Lcom/facebook/events/widget/eventcard/EventsCardView;->getTitleView()Landroid/widget/TextView;

    move-result-object v0

    sget-object v1, LX/1vY;->TITLE:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 587847
    invoke-virtual {p0}, Lcom/facebook/events/widget/eventcard/EventsCardView;->getSocialContextTextView()Landroid/widget/TextView;

    move-result-object v0

    sget-object v1, LX/1vY;->SOCIAL_CONTEXT:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 587848
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/3VV;

    invoke-static {p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object p0

    check-cast p0, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    iput-object p0, p1, LX/3VV;->b:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    return-void
.end method


# virtual methods
.method public setLargeImageAspectRatio(F)V
    .locals 0

    .prologue
    .line 587839
    invoke-super {p0, p1}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setCoverPhotoAspectRatio(F)V

    .line 587840
    return-void
.end method

.method public setLargeImageController(LX/1aZ;)V
    .locals 0
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587837
    invoke-super {p0, p1}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setCoverPhotoController(LX/1aZ;)V

    .line 587838
    return-void
.end method
