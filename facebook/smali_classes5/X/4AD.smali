.class public final LX/4AD;
.super LX/4A8;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;
.implements Ljava/util/RandomAccess;


# instance fields
.field public b:I

.field public transient c:LX/3Sd;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 675698
    invoke-direct {p0}, LX/4A8;-><init>()V

    .line 675699
    const/4 v0, 0x0

    invoke-static {v0}, LX/3Sd;->a(I)LX/3Sd;

    move-result-object v0

    iput-object v0, p0, LX/4AD;->c:LX/3Sd;

    .line 675700
    return-void
.end method

.method public constructor <init>(I)V
    .locals 3

    .prologue
    .line 675583
    invoke-direct {p0}, LX/4A8;-><init>()V

    .line 675584
    if-gez p1, :cond_0

    .line 675585
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "capacity < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 675586
    :cond_0
    invoke-static {p1}, LX/3Sd;->a(I)LX/3Sd;

    move-result-object v0

    iput-object v0, p0, LX/4AD;->c:LX/3Sd;

    .line 675587
    return-void
.end method

.method private static a(II)Ljava/lang/IndexOutOfBoundsException;
    .locals 3

    .prologue
    .line 675697
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", size is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static b(I)I
    .locals 1

    .prologue
    .line 675694
    const/4 v0, 0x6

    if-ge p0, v0, :cond_0

    const/16 v0, 0xc

    .line 675695
    :goto_0
    add-int/2addr v0, p0

    return v0

    .line 675696
    :cond_0
    shr-int/lit8 v0, p0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(I)LX/1vs;
    .locals 4

    .prologue
    .line 675684
    iget v0, p0, LX/4AD;->b:I

    if-lt p1, v0, :cond_0

    .line 675685
    iget v0, p0, LX/4AD;->b:I

    invoke-static {p1, v0}, LX/4AD;->a(II)Ljava/lang/IndexOutOfBoundsException;

    .line 675686
    :cond_0
    iget-object v0, p0, LX/4AD;->c:LX/3Sd;

    .line 675687
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 675688
    :try_start_0
    invoke-virtual {v0, p1}, LX/3Sd;->b(I)LX/15i;

    move-result-object v2

    .line 675689
    invoke-virtual {v0, p1}, LX/3Sd;->c(I)I

    move-result v3

    .line 675690
    invoke-virtual {v0, p1}, LX/3Sd;->d(I)I

    move-result v0

    .line 675691
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 675692
    invoke-static {v2, v3, v0}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    return-object v0

    .line 675693
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(ILX/15i;II)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 675665
    iget-object v0, p0, LX/4AD;->c:LX/3Sd;

    .line 675666
    iget v2, p0, LX/4AD;->b:I

    .line 675667
    if-gt p1, v2, :cond_0

    if-gez p1, :cond_1

    .line 675668
    :cond_0
    invoke-static {p1, v2}, LX/4AD;->a(II)Ljava/lang/IndexOutOfBoundsException;

    .line 675669
    :cond_1
    iget v1, v0, LX/3Sd;->b:I

    if-ge v2, v1, :cond_2

    .line 675670
    add-int/lit8 v1, p1, 0x1

    sub-int v3, v2, p1

    invoke-static {v0, p1, v0, v1, v3}, LX/3Sd;->a(LX/3Sd;ILX/3Sd;II)V

    .line 675671
    :goto_0
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 675672
    :try_start_0
    invoke-virtual {v0, p1, p2}, LX/3Sd;->a(ILX/15i;)LX/15i;

    .line 675673
    invoke-virtual {v0, p1, p3}, LX/3Sd;->a(II)I

    .line 675674
    invoke-virtual {v0, p1, p4}, LX/3Sd;->b(II)I

    .line 675675
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 675676
    add-int/lit8 v0, v2, 0x1

    iput v0, p0, LX/4AD;->b:I

    .line 675677
    iget v0, p0, LX/4A8;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4AD;->a:I

    .line 675678
    return-void

    .line 675679
    :cond_2
    invoke-static {v2}, LX/4AD;->b(I)I

    move-result v1

    invoke-static {v1}, LX/3Sd;->a(I)LX/3Sd;

    move-result-object v1

    .line 675680
    invoke-static {v0, v3, v1, v3, p1}, LX/3Sd;->a(LX/3Sd;ILX/3Sd;II)V

    .line 675681
    add-int/lit8 v3, p1, 0x1

    sub-int v4, v2, p1

    invoke-static {v0, p1, v1, v3, v4}, LX/3Sd;->a(LX/3Sd;ILX/3Sd;II)V

    .line 675682
    iput-object v1, p0, LX/4AD;->c:LX/3Sd;

    move-object v0, v1

    goto :goto_0

    .line 675683
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 675664
    iget v0, p0, LX/4AD;->b:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/15i;II)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 675701
    iget-object v1, p0, LX/4AD;->c:LX/3Sd;

    .line 675702
    iget v2, p0, LX/4AD;->b:I

    .line 675703
    iget v0, v1, LX/3Sd;->b:I

    if-ne v2, v0, :cond_1

    .line 675704
    const/4 v0, 0x6

    if-ge v2, v0, :cond_0

    const/16 v0, 0xc

    :goto_0
    add-int/2addr v0, v2

    invoke-static {v0}, LX/3Sd;->a(I)LX/3Sd;

    move-result-object v0

    .line 675705
    invoke-static {v1, v3, v0, v3, v2}, LX/3Sd;->a(LX/3Sd;ILX/3Sd;II)V

    .line 675706
    iput-object v0, p0, LX/4AD;->c:LX/3Sd;

    .line 675707
    :goto_1
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 675708
    :try_start_0
    invoke-virtual {v0, v2, p1}, LX/3Sd;->a(ILX/15i;)LX/15i;

    .line 675709
    invoke-virtual {v0, v2, p2}, LX/3Sd;->a(II)I

    .line 675710
    invoke-virtual {v0, v2, p3}, LX/3Sd;->b(II)I

    .line 675711
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 675712
    add-int/lit8 v0, v2, 0x1

    iput v0, p0, LX/4AD;->b:I

    .line 675713
    iget v0, p0, LX/4A8;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4AD;->a:I

    .line 675714
    const/4 v0, 0x1

    return v0

    .line 675715
    :cond_0
    shr-int/lit8 v0, v2, 0x1

    goto :goto_0

    .line 675716
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public final a(LX/39P;)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 675648
    invoke-interface {p1}, LX/39P;->d()LX/3Sd;

    move-result-object v3

    .line 675649
    iget v4, v3, LX/3Sd;->b:I

    .line 675650
    if-nez v4, :cond_0

    move v0, v2

    .line 675651
    :goto_0
    return v0

    .line 675652
    :cond_0
    iget-object v1, p0, LX/4AD;->c:LX/3Sd;

    .line 675653
    iget v5, p0, LX/4AD;->b:I

    .line 675654
    add-int v6, v5, v4

    .line 675655
    iget v0, v1, LX/3Sd;->b:I

    if-le v6, v0, :cond_1

    .line 675656
    add-int/lit8 v0, v6, -0x1

    invoke-static {v0}, LX/4AD;->b(I)I

    move-result v0

    .line 675657
    invoke-static {v0}, LX/3Sd;->a(I)LX/3Sd;

    move-result-object v0

    .line 675658
    invoke-static {v1, v2, v0, v2, v5}, LX/3Sd;->a(LX/3Sd;ILX/3Sd;II)V

    .line 675659
    iput-object v0, p0, LX/4AD;->c:LX/3Sd;

    .line 675660
    :goto_1
    invoke-static {v3, v2, v0, v5, v4}, LX/3Sd;->a(LX/3Sd;ILX/3Sd;II)V

    .line 675661
    iput v6, p0, LX/4AD;->b:I

    .line 675662
    iget v0, p0, LX/4A8;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4AD;->a:I

    .line 675663
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public final b()LX/2sN;
    .locals 2

    .prologue
    .line 675647
    new-instance v0, LX/4AC;

    invoke-direct {v0, p0}, LX/4AC;-><init>(LX/4AD;)V

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 675646
    iget v0, p0, LX/4AD;->b:I

    return v0
.end method

.method public final clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 675642
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4AD;

    .line 675643
    iget-object v1, p0, LX/4AD;->c:LX/3Sd;

    invoke-virtual {v1}, LX/3Sd;->a()LX/3Sd;

    move-result-object v1

    iput-object v1, v0, LX/4AD;->c:LX/3Sd;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 675644
    return-object v0

    .line 675645
    :catch_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public final d()LX/3Sd;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 675638
    iget v0, p0, LX/4AD;->b:I

    .line 675639
    invoke-static {v0}, LX/3Sd;->a(I)LX/3Sd;

    move-result-object v1

    .line 675640
    iget-object v2, p0, LX/4AD;->c:LX/3Sd;

    invoke-static {v2, v3, v1, v3, v0}, LX/3Sd;->a(LX/3Sd;ILX/3Sd;II)V

    .line 675641
    return-object v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 11

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 675588
    if-ne p1, p0, :cond_1

    .line 675589
    :cond_0
    :goto_0
    return v0

    .line 675590
    :cond_1
    :try_start_0
    check-cast p1, LX/3Sb;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 675591
    iget v3, p0, LX/4AD;->b:I

    .line 675592
    invoke-interface {p1}, LX/3Sb;->c()I

    move-result v2

    if-eq v2, v3, :cond_2

    move v0, v1

    .line 675593
    goto :goto_0

    .line 675594
    :catch_0
    move v0, v1

    goto :goto_0

    .line 675595
    :cond_2
    iget-object v4, p0, LX/4AD;->c:LX/3Sd;

    .line 675596
    instance-of v2, p1, Ljava/util/RandomAccess;

    if-eqz v2, :cond_6

    move v2, v1

    .line 675597
    :goto_1
    if-ge v2, v3, :cond_0

    .line 675598
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    .line 675599
    :try_start_1
    invoke-virtual {v4, v2}, LX/3Sd;->b(I)LX/15i;

    move-result-object v6

    .line 675600
    invoke-virtual {v4, v2}, LX/3Sd;->c(I)I

    move-result v7

    .line 675601
    invoke-virtual {v4, v2}, LX/3Sd;->d(I)I

    .line 675602
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 675603
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    .line 675604
    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 675605
    invoke-interface {p1, v2}, LX/3Sb;->a(I)LX/1vs;

    move-result-object v5

    .line 675606
    iget-object v8, v5, LX/1vs;->a:LX/15i;

    .line 675607
    iget v9, v5, LX/1vs;->b:I

    .line 675608
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    .line 675609
    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 675610
    if-nez v7, :cond_4

    if-eqz v9, :cond_5

    :cond_3
    move v0, v1

    .line 675611
    goto :goto_0

    .line 675612
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 675613
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 675614
    :catchall_2
    move-exception v0

    :try_start_6
    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v0

    .line 675615
    :cond_4
    invoke-static {v6, v7, v8, v9}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 675616
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 675617
    :cond_6
    invoke-interface {p1}, LX/3Sb;->b()LX/2sN;

    move-result-object v5

    move v2, v1

    .line 675618
    :goto_2
    if-ge v2, v3, :cond_0

    .line 675619
    sget-object v6, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v6

    .line 675620
    :try_start_7
    invoke-virtual {v4, v2}, LX/3Sd;->b(I)LX/15i;

    move-result-object v7

    .line 675621
    invoke-virtual {v4, v2}, LX/3Sd;->c(I)I

    move-result v8

    .line 675622
    invoke-virtual {v4, v2}, LX/3Sd;->d(I)I

    .line 675623
    monitor-exit v6
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 675624
    sget-object v6, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v6

    .line 675625
    :try_start_8
    monitor-exit v6
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 675626
    invoke-interface {v5}, LX/2sN;->b()LX/1vs;

    move-result-object v6

    .line 675627
    iget-object v9, v6, LX/1vs;->a:LX/15i;

    .line 675628
    iget v10, v6, LX/1vs;->b:I

    .line 675629
    sget-object v6, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v6

    .line 675630
    :try_start_9
    monitor-exit v6
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    .line 675631
    if-nez v8, :cond_8

    if-eqz v10, :cond_9

    :cond_7
    move v0, v1

    .line 675632
    goto :goto_0

    .line 675633
    :catchall_3
    move-exception v0

    :try_start_a
    monitor-exit v6
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    throw v0

    .line 675634
    :catchall_4
    move-exception v0

    :try_start_b
    monitor-exit v6
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    throw v0

    .line 675635
    :catchall_5
    move-exception v0

    :try_start_c
    monitor-exit v6
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    throw v0

    .line 675636
    :cond_8
    invoke-static {v7, v8, v9, v10}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 675637
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public final hashCode()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 675566
    iget-object v4, p0, LX/4AD;->c:LX/3Sd;

    .line 675567
    const/4 v0, 0x1

    .line 675568
    iget v5, p0, LX/4AD;->b:I

    move v2, v1

    .line 675569
    :goto_0
    if-ge v2, v5, :cond_1

    .line 675570
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    .line 675571
    :try_start_0
    invoke-virtual {v4, v2}, LX/3Sd;->b(I)LX/15i;

    move-result-object v6

    .line 675572
    invoke-virtual {v4, v2}, LX/3Sd;->c(I)I

    move-result v7

    .line 675573
    invoke-virtual {v4, v2}, LX/3Sd;->d(I)I

    .line 675574
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 675575
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    .line 675576
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 675577
    mul-int/lit8 v3, v0, 0x1f

    if-nez v7, :cond_0

    move v0, v1

    :goto_1
    add-int/2addr v3, v0

    .line 675578
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_0

    .line 675579
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 675580
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 675581
    :cond_0
    invoke-virtual {v6}, Ljava/lang/Object;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v0, v7

    goto :goto_1

    .line 675582
    :cond_1
    return v0
.end method
