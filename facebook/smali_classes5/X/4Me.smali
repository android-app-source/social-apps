.class public LX/4Me;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 688977
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 688941
    const/4 v0, 0x0

    .line 688942
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_6

    .line 688943
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 688944
    :goto_0
    return v1

    .line 688945
    :cond_0
    const-string v7, "real_time_activity_type"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 688946
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    move-result-object v0

    move-object v3, v0

    move v0, v2

    .line 688947
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_4

    .line 688948
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 688949
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 688950
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 688951
    const-string v7, "real_time_activity_actors"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 688952
    invoke-static {p0, p1}, LX/4Md;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 688953
    :cond_2
    const-string v7, "real_time_activity_sentence"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 688954
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 688955
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 688956
    :cond_4
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 688957
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 688958
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 688959
    if-eqz v0, :cond_5

    .line 688960
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->a(ILjava/lang/Enum;)V

    .line 688961
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move-object v3, v0

    move v4, v1

    move v5, v1

    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 688962
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 688963
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 688964
    if-eqz v0, :cond_0

    .line 688965
    const-string v1, "real_time_activity_actors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688966
    invoke-static {p0, v0, p2, p3}, LX/4Md;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 688967
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688968
    if-eqz v0, :cond_1

    .line 688969
    const-string v1, "real_time_activity_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688970
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 688971
    :cond_1
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 688972
    if-eqz v0, :cond_2

    .line 688973
    const-string v0, "real_time_activity_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688974
    const-class v0, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 688975
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 688976
    return-void
.end method
