.class public final LX/52A;
.super Ljava/util/AbstractList;
.source ""

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/RandomAccess;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractList",
        "<",
        "Ljava/lang/Long;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/util/RandomAccess;"
    }
.end annotation


# instance fields
.field public final array:[J

.field public final end:I

.field public final start:I


# direct methods
.method public constructor <init>([J)V
    .locals 2

    .prologue
    .line 825849
    const/4 v0, 0x0

    array-length v1, p1

    invoke-direct {p0, p1, v0, v1}, LX/52A;-><init>([JII)V

    .line 825850
    return-void
.end method

.method private constructor <init>([JII)V
    .locals 0

    .prologue
    .line 825844
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 825845
    iput-object p1, p0, LX/52A;->array:[J

    .line 825846
    iput p2, p0, LX/52A;->start:I

    .line 825847
    iput p3, p0, LX/52A;->end:I

    .line 825848
    return-void
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    .line 825843
    instance-of v0, p1, Ljava/lang/Long;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/52A;->array:[J

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget v1, p0, LX/52A;->start:I

    iget v4, p0, LX/52A;->end:I

    invoke-static {v0, v2, v3, v1, v4}, LX/1YA;->c([JJII)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 825789
    if-ne p1, p0, :cond_1

    .line 825790
    :cond_0
    :goto_0
    return v0

    .line 825791
    :cond_1
    instance-of v2, p1, LX/52A;

    if-eqz v2, :cond_4

    .line 825792
    check-cast p1, LX/52A;

    .line 825793
    invoke-virtual {p0}, LX/52A;->size()I

    move-result v3

    .line 825794
    invoke-virtual {p1}, LX/52A;->size()I

    move-result v2

    if-eq v2, v3, :cond_2

    move v0, v1

    .line 825795
    goto :goto_0

    :cond_2
    move v2, v1

    .line 825796
    :goto_1
    if-ge v2, v3, :cond_0

    .line 825797
    iget-object v4, p0, LX/52A;->array:[J

    iget v5, p0, LX/52A;->start:I

    add-int/2addr v5, v2

    aget-wide v4, v4, v5

    iget-object v6, p1, LX/52A;->array:[J

    iget v7, p1, LX/52A;->start:I

    add-int/2addr v7, v2

    aget-wide v6, v6, v7

    cmp-long v4, v4, v6

    if-eqz v4, :cond_3

    move v0, v1

    .line 825798
    goto :goto_0

    .line 825799
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 825800
    :cond_4
    invoke-super {p0, p1}, Ljava/util/AbstractList;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final get(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 825841
    invoke-virtual {p0}, LX/52A;->size()I

    move-result v0

    invoke-static {p1, v0}, LX/0PB;->checkElementIndex(II)I

    .line 825842
    iget-object v0, p0, LX/52A;->array:[J

    iget v1, p0, LX/52A;->start:I

    add-int/2addr v1, p1

    aget-wide v0, v0, v1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    .line 825834
    const/4 v1, 0x1

    .line 825835
    iget v0, p0, LX/52A;->start:I

    :goto_0
    iget v2, p0, LX/52A;->end:I

    if-ge v0, v2, :cond_0

    .line 825836
    mul-int/lit8 v1, v1, 0x1f

    iget-object v2, p0, LX/52A;->array:[J

    aget-wide v2, v2, v0

    .line 825837
    const/16 v4, 0x20

    ushr-long v4, v2, v4

    xor-long/2addr v4, v2

    long-to-int v4, v4

    move v2, v4

    .line 825838
    add-int/2addr v1, v2

    .line 825839
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 825840
    :cond_0
    return v1
.end method

.method public final indexOf(Ljava/lang/Object;)I
    .locals 5

    .prologue
    .line 825829
    instance-of v0, p1, Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 825830
    iget-object v0, p0, LX/52A;->array:[J

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget v1, p0, LX/52A;->start:I

    iget v4, p0, LX/52A;->end:I

    invoke-static {v0, v2, v3, v1, v4}, LX/1YA;->c([JJII)I

    move-result v0

    .line 825831
    if-ltz v0, :cond_0

    .line 825832
    iget v1, p0, LX/52A;->start:I

    sub-int/2addr v0, v1

    .line 825833
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 825828
    const/4 v0, 0x0

    return v0
.end method

.method public final lastIndexOf(Ljava/lang/Object;)I
    .locals 10

    .prologue
    .line 825818
    instance-of v0, p1, Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 825819
    iget-object v0, p0, LX/52A;->array:[J

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget v1, p0, LX/52A;->start:I

    iget v4, p0, LX/52A;->end:I

    .line 825820
    add-int/lit8 v5, v4, -0x1

    :goto_0
    if-lt v5, v1, :cond_2

    .line 825821
    aget-wide v7, v0, v5

    cmp-long v6, v7, v2

    if-nez v6, :cond_1

    .line 825822
    :goto_1
    move v0, v5

    .line 825823
    if-ltz v0, :cond_0

    .line 825824
    iget v1, p0, LX/52A;->start:I

    sub-int/2addr v0, v1

    .line 825825
    :goto_2
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_2

    .line 825826
    :cond_1
    add-int/lit8 v5, v5, -0x1

    goto :goto_0

    .line 825827
    :cond_2
    const/4 v5, -0x1

    goto :goto_1
.end method

.method public final set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 825813
    check-cast p2, Ljava/lang/Long;

    .line 825814
    invoke-virtual {p0}, LX/52A;->size()I

    move-result v0

    invoke-static {p1, v0}, LX/0PB;->checkElementIndex(II)I

    .line 825815
    iget-object v0, p0, LX/52A;->array:[J

    iget v1, p0, LX/52A;->start:I

    add-int/2addr v1, p1

    aget-wide v2, v0, v1

    .line 825816
    iget-object v1, p0, LX/52A;->array:[J

    iget v0, p0, LX/52A;->start:I

    add-int v4, v0, p1

    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v1, v4

    .line 825817
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 2

    .prologue
    .line 825812
    iget v0, p0, LX/52A;->end:I

    iget v1, p0, LX/52A;->start:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final subList(II)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 825807
    invoke-virtual {p0}, LX/52A;->size()I

    move-result v0

    .line 825808
    invoke-static {p1, p2, v0}, LX/0PB;->checkPositionIndexes(III)V

    .line 825809
    if-ne p1, p2, :cond_0

    .line 825810
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 825811
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/52A;

    iget-object v1, p0, LX/52A;->array:[J

    iget v2, p0, LX/52A;->start:I

    add-int/2addr v2, p1

    iget v3, p0, LX/52A;->start:I

    add-int/2addr v3, p2

    invoke-direct {v0, v1, v2, v3}, LX/52A;-><init>([JII)V

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 825801
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, LX/52A;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0xa

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 825802
    const/16 v0, 0x5b

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LX/52A;->array:[J

    iget v3, p0, LX/52A;->start:I

    aget-wide v2, v2, v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 825803
    iget v0, p0, LX/52A;->start:I

    add-int/lit8 v0, v0, 0x1

    :goto_0
    iget v2, p0, LX/52A;->end:I

    if-ge v0, v2, :cond_0

    .line 825804
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LX/52A;->array:[J

    aget-wide v4, v3, v0

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 825805
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 825806
    :cond_0
    const/16 v0, 0x5d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
