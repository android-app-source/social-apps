.class public final LX/3rB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final a:LX/24x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/24x",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/24x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/24x",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 642801
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 642802
    iput-object p1, p0, LX/3rB;->a:LX/24x;

    .line 642803
    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 642800
    iget-object v0, p0, LX/3rB;->a:LX/24x;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/24x;->a(Landroid/os/Parcel;Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)[TT;"
        }
    .end annotation

    .prologue
    .line 642799
    iget-object v0, p0, LX/3rB;->a:LX/24x;

    invoke-interface {v0, p1}, LX/24x;->a(I)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
