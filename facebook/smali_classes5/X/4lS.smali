.class public final LX/4lS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3nY;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/3nV;


# direct methods
.method private constructor <init>(LX/3nV;Ljava/lang/String;JLX/0P1;)V
    .locals 1
    .param p5    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 804456
    iput-object p1, p0, LX/4lS;->d:LX/3nV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 804457
    iput-object p2, p0, LX/4lS;->a:Ljava/lang/String;

    .line 804458
    iput-wide p3, p0, LX/4lS;->b:J

    .line 804459
    iput-object p5, p0, LX/4lS;->c:LX/0P1;

    .line 804460
    return-void
.end method

.method public synthetic constructor <init>(LX/3nV;Ljava/lang/String;JLX/0P1;B)V
    .locals 1

    .prologue
    .line 804461
    invoke-direct/range {p0 .. p5}, LX/4lS;-><init>(LX/3nV;Ljava/lang/String;JLX/0P1;)V

    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 804462
    iget-object v0, p0, LX/4lS;->d:LX/3nV;

    iget-wide v0, v0, LX/3nV;->k:J

    return-wide v0
.end method

.method public final b()LX/0lF;
    .locals 6

    .prologue
    .line 804463
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 804464
    const-string v1, "name"

    iget-object v2, p0, LX/4lS;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 804465
    const-string v1, "type"

    const-string v2, "m"

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 804466
    const-string v1, "relative_start_ms"

    iget-wide v2, p0, LX/4lS;->b:J

    iget-object v4, p0, LX/4lS;->d:LX/3nV;

    iget-wide v4, v4, LX/3nV;->k:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 804467
    iget-object v1, p0, LX/4lS;->c:LX/0P1;

    if-eqz v1, :cond_0

    .line 804468
    const-string v1, "extra"

    iget-object v2, p0, LX/4lS;->c:LX/0P1;

    invoke-static {v2}, LX/16N;->a(Ljava/util/Map;)LX/0m9;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 804469
    :cond_0
    return-object v0
.end method
