.class public final LX/3T3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/auth/credentials/DBLFacebookCredentials;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/10M;


# direct methods
.method public constructor <init>(LX/10M;)V
    .locals 0

    .prologue
    .line 583534
    iput-object p1, p0, LX/3T3;->a:LX/10M;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 8

    .prologue
    .line 583535
    check-cast p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    check-cast p2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    const-wide/16 v6, 0x0

    const/4 v0, 0x1

    const/4 v1, -0x1

    .line 583536
    iget-object v2, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 583537
    :cond_0
    :goto_0
    return v0

    .line 583538
    :cond_1
    iget-object v2, p2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    if-nez v2, :cond_2

    move v0, v1

    .line 583539
    goto :goto_0

    .line 583540
    :cond_2
    iget-object v2, p0, LX/3T3;->a:LX/10M;

    iget-object v3, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-static {v2, v3}, LX/10M;->i(LX/10M;Ljava/lang/String;)J

    move-result-wide v2

    .line 583541
    iget-object v4, p0, LX/3T3;->a:LX/10M;

    iget-object v5, p2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-static {v4, v5}, LX/10M;->i(LX/10M;Ljava/lang/String;)J

    move-result-wide v4

    .line 583542
    sub-long v2, v4, v2

    .line 583543
    cmp-long v4, v2, v6

    if-nez v4, :cond_3

    .line 583544
    iget-object v0, p2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 583545
    :cond_3
    cmp-long v2, v2, v6

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method
