.class public LX/444;
.super LX/0Rr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<",
        "LEFT:Ljava/lang/Object;",
        "RIGHT:",
        "Ljava/lang/Object;",
        "KEY:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Rr",
        "<",
        "LX/443",
        "<T",
        "LEFT;",
        "TRIGHT;>;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<TKEY;>;"
        }
    .end annotation
.end field

.field private final b:LX/4yu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4yu",
            "<T",
            "LEFT;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/4yu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4yu",
            "<TRIGHT;>;"
        }
    .end annotation
.end field

.field private final d:LX/30b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/30b",
            "<T",
            "LEFT;",
            "TKEY;>;"
        }
    .end annotation
.end field

.field private final e:LX/30b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/30b",
            "<TRIGHT;TKEY;>;"
        }
    .end annotation
.end field

.field public f:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TKEY;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TKEY;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Comparator;Ljava/util/Iterator;Ljava/util/Iterator;LX/30b;LX/30b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<TKEY;>;",
            "Ljava/util/Iterator",
            "<T",
            "LEFT;",
            ">;",
            "Ljava/util/Iterator",
            "<TRIGHT;>;",
            "LX/30b",
            "<T",
            "LEFT;",
            "TKEY;>;",
            "LX/30b",
            "<TRIGHT;TKEY;>;)V"
        }
    .end annotation

    .prologue
    .line 669381
    invoke-direct {p0}, LX/0Rr;-><init>()V

    .line 669382
    iput-object p1, p0, LX/444;->a:Ljava/util/Comparator;

    .line 669383
    invoke-static {p2}, LX/0RZ;->i(Ljava/util/Iterator;)LX/4yu;

    move-result-object v0

    iput-object v0, p0, LX/444;->b:LX/4yu;

    .line 669384
    invoke-static {p3}, LX/0RZ;->i(Ljava/util/Iterator;)LX/4yu;

    move-result-object v0

    iput-object v0, p0, LX/444;->c:LX/4yu;

    .line 669385
    iput-object p4, p0, LX/444;->d:LX/30b;

    .line 669386
    iput-object p5, p0, LX/444;->e:LX/30b;

    .line 669387
    return-void
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)LX/443;
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(T",
            "LEFT;",
            "TRIGHT;)",
            "LX/443",
            "<T",
            "LEFT;",
            "TRIGHT;>;"
        }
    .end annotation

    .prologue
    .line 669388
    new-instance v0, LX/443;

    invoke-direct {v0, p0, p1}, LX/443;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static newBuilder()LX/445;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<",
            "LEFT:Ljava/lang/Object;",
            "RIGHT:",
            "Ljava/lang/Object;",
            "KEY:",
            "Ljava/lang/Object;",
            ">()",
            "LX/445",
            "<T",
            "LEFT;",
            "TRIGHT;TKEY;>;"
        }
    .end annotation

    .prologue
    .line 669389
    new-instance v0, LX/445;

    invoke-direct {v0}, LX/445;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 9

    .prologue
    .line 669390
    const/4 v1, 0x0

    .line 669391
    iget-object v0, p0, LX/444;->b:LX/4yu;

    invoke-interface {v0}, LX/4yu;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/444;->c:LX/4yu;

    invoke-interface {v0}, LX/4yu;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 669392
    invoke-virtual {p0}, LX/0Rr;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/443;

    .line 669393
    :goto_0
    return-object v0

    .line 669394
    :cond_0
    iget-object v0, p0, LX/444;->b:LX/4yu;

    invoke-interface {v0}, LX/4yu;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 669395
    iget-object v0, p0, LX/444;->d:LX/30b;

    iget-object v2, p0, LX/444;->b:LX/4yu;

    invoke-interface {v2}, LX/4yu;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, LX/30b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 669396
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 669397
    iget-object v2, p0, LX/444;->f:Ljava/lang/Object;

    if-eqz v2, :cond_1

    .line 669398
    iget-object v2, p0, LX/444;->a:Ljava/util/Comparator;

    iget-object v5, p0, LX/444;->f:Ljava/lang/Object;

    invoke-interface {v2, v0, v5}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_9

    move v2, v3

    :goto_1
    const-string v5, "Left iterator keys must be strictly ascending. (%s %s)"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, LX/444;->f:Ljava/lang/Object;

    aput-object v7, v6, v4

    aput-object v0, v6, v3

    invoke-static {v2, v5, v6}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 669399
    :cond_1
    :goto_2
    iget-object v2, p0, LX/444;->c:LX/4yu;

    invoke-interface {v2}, LX/4yu;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 669400
    iget-object v2, p0, LX/444;->e:LX/30b;

    iget-object v3, p0, LX/444;->c:LX/4yu;

    invoke-interface {v3}, LX/4yu;->a()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, LX/30b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 669401
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 669402
    iget-object v3, p0, LX/444;->g:Ljava/lang/Object;

    if-eqz v3, :cond_2

    .line 669403
    iget-object v3, p0, LX/444;->a:Ljava/util/Comparator;

    iget-object v6, p0, LX/444;->g:Ljava/lang/Object;

    invoke-interface {v3, v2, v6}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    if-lez v3, :cond_a

    move v3, v4

    :goto_3
    const-string v6, "Right iterator keys must be strictly ascending. (%s %s)"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, p0, LX/444;->f:Ljava/lang/Object;

    aput-object v8, v7, v5

    aput-object v2, v7, v4

    invoke-static {v3, v6, v7}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 669404
    :cond_2
    :goto_4
    iget-object v3, p0, LX/444;->b:LX/4yu;

    invoke-interface {v3}, LX/4yu;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, LX/444;->c:LX/4yu;

    invoke-interface {v3}, LX/4yu;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 669405
    iget-object v0, p0, LX/444;->c:LX/4yu;

    invoke-interface {v0}, LX/4yu;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1, v0}, LX/444;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/443;

    move-result-object v0

    goto/16 :goto_0

    .line 669406
    :cond_3
    iget-object v3, p0, LX/444;->b:LX/4yu;

    invoke-interface {v3}, LX/4yu;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, LX/444;->c:LX/4yu;

    invoke-interface {v3}, LX/4yu;->hasNext()Z

    move-result v3

    if-nez v3, :cond_4

    .line 669407
    iget-object v0, p0, LX/444;->b:LX/4yu;

    invoke-interface {v0}, LX/4yu;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, v1}, LX/444;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/443;

    move-result-object v0

    goto/16 :goto_0

    .line 669408
    :cond_4
    iget-object v3, p0, LX/444;->a:Ljava/util/Comparator;

    invoke-interface {v3, v0, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    .line 669409
    if-lez v3, :cond_5

    .line 669410
    iput-object v2, p0, LX/444;->g:Ljava/lang/Object;

    .line 669411
    iget-object v0, p0, LX/444;->c:LX/4yu;

    invoke-interface {v0}, LX/4yu;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1, v0}, LX/444;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/443;

    move-result-object v0

    goto/16 :goto_0

    .line 669412
    :cond_5
    if-gez v3, :cond_6

    .line 669413
    iput-object v0, p0, LX/444;->f:Ljava/lang/Object;

    .line 669414
    iget-object v0, p0, LX/444;->b:LX/4yu;

    invoke-interface {v0}, LX/4yu;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, v1}, LX/444;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/443;

    move-result-object v0

    goto/16 :goto_0

    .line 669415
    :cond_6
    iput-object v2, p0, LX/444;->g:Ljava/lang/Object;

    .line 669416
    iput-object v0, p0, LX/444;->f:Ljava/lang/Object;

    .line 669417
    iget-object v0, p0, LX/444;->b:LX/4yu;

    invoke-interface {v0}, LX/4yu;->next()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LX/444;->c:LX/4yu;

    invoke-interface {v1}, LX/4yu;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, LX/444;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/443;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    move-object v2, v1

    goto :goto_4

    :cond_8
    move-object v0, v1

    goto/16 :goto_2

    :cond_9
    move v2, v4

    .line 669418
    goto/16 :goto_1

    :cond_a
    move v3, v5

    .line 669419
    goto/16 :goto_3
.end method
