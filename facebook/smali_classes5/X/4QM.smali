.class public LX/4QM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 704394
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 704395
    const/4 v12, 0x0

    .line 704396
    const/4 v9, 0x0

    .line 704397
    const-wide/16 v10, 0x0

    .line 704398
    const/4 v8, 0x0

    .line 704399
    const/4 v7, 0x0

    .line 704400
    const/4 v6, 0x0

    .line 704401
    const/4 v5, 0x0

    .line 704402
    const/4 v4, 0x0

    .line 704403
    const/4 v3, 0x0

    .line 704404
    const/4 v2, 0x0

    .line 704405
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_c

    .line 704406
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 704407
    const/4 v2, 0x0

    .line 704408
    :goto_0
    return v2

    .line 704409
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v14, :cond_a

    .line 704410
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 704411
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 704412
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_0

    if-eqz v3, :cond_0

    .line 704413
    const-string v14, "cache_id"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 704414
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v7, v3

    goto :goto_1

    .line 704415
    :cond_1
    const-string v14, "debug_info"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 704416
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v6, v3

    goto :goto_1

    .line 704417
    :cond_2
    const-string v14, "fetchTimeMs"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 704418
    const/4 v2, 0x1

    .line 704419
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto :goto_1

    .line 704420
    :cond_3
    const-string v14, "pageContextualRecommendationsItems"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 704421
    invoke-static/range {p0 .. p1}, LX/4QN;->a(LX/15w;LX/186;)I

    move-result v3

    move v13, v3

    goto :goto_1

    .line 704422
    :cond_4
    const-string v14, "pageContextualRecommendationsTitle"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 704423
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    move v12, v3

    goto :goto_1

    .line 704424
    :cond_5
    const-string v14, "short_term_cache_key"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 704425
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v11, v3

    goto :goto_1

    .line 704426
    :cond_6
    const-string v14, "title"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 704427
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    move v10, v3

    goto/16 :goto_1

    .line 704428
    :cond_7
    const-string v14, "tracking"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 704429
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v9, v3

    goto/16 :goto_1

    .line 704430
    :cond_8
    const-string v14, "impression_info"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 704431
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 704432
    :cond_9
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 704433
    :cond_a
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 704434
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 704435
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 704436
    if-eqz v2, :cond_b

    .line 704437
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 704438
    :cond_b
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 704439
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 704440
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 704441
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 704442
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 704443
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 704444
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_c
    move v13, v8

    move v8, v3

    move/from16 v16, v5

    move/from16 v17, v6

    move v6, v9

    move v9, v4

    move-wide v4, v10

    move/from16 v10, v16

    move/from16 v11, v17

    move/from16 v18, v7

    move v7, v12

    move/from16 v12, v18

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 704445
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 704446
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 704447
    invoke-static {p0, v2}, LX/4QM;->a(LX/15w;LX/186;)I

    move-result v1

    .line 704448
    if-eqz v0, :cond_0

    .line 704449
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 704450
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 704451
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 704452
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 704453
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 704454
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 704455
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 704456
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 704457
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704458
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 704459
    const-string v0, "name"

    const-string v1, "PageContextualRecommendationsFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 704460
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 704461
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 704462
    if-eqz v0, :cond_0

    .line 704463
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704464
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704465
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 704466
    if-eqz v0, :cond_1

    .line 704467
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704468
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704469
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 704470
    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    .line 704471
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704472
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 704473
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 704474
    if-eqz v0, :cond_4

    .line 704475
    const-string v1, "pageContextualRecommendationsItems"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704476
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 704477
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 704478
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/4QN;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 704479
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 704480
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 704481
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 704482
    if-eqz v0, :cond_5

    .line 704483
    const-string v1, "pageContextualRecommendationsTitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704484
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 704485
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 704486
    if-eqz v0, :cond_6

    .line 704487
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704488
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704489
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 704490
    if-eqz v0, :cond_7

    .line 704491
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704492
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 704493
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 704494
    if-eqz v0, :cond_8

    .line 704495
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704496
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704497
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 704498
    if-eqz v0, :cond_9

    .line 704499
    const-string v1, "impression_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704500
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704501
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 704502
    return-void
.end method
