.class public LX/3eK;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/stickers/service/FetchStickerTagsParams;",
        "Lcom/facebook/stickers/service/FetchStickerTagsResult;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3eK;


# instance fields
.field private b:LX/3eL;


# direct methods
.method public constructor <init>(LX/0sO;LX/3eL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 619381
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 619382
    iput-object p2, p0, LX/3eK;->b:LX/3eL;

    .line 619383
    return-void
.end method

.method public static a(LX/0QB;)LX/3eK;
    .locals 5

    .prologue
    .line 619384
    sget-object v0, LX/3eK;->c:LX/3eK;

    if-nez v0, :cond_1

    .line 619385
    const-class v1, LX/3eK;

    monitor-enter v1

    .line 619386
    :try_start_0
    sget-object v0, LX/3eK;->c:LX/3eK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 619387
    if-eqz v2, :cond_0

    .line 619388
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 619389
    new-instance p0, LX/3eK;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v3

    check-cast v3, LX/0sO;

    invoke-static {v0}, LX/3eL;->a(LX/0QB;)LX/3eL;

    move-result-object v4

    check-cast v4, LX/3eL;

    invoke-direct {p0, v3, v4}, LX/3eK;-><init>(LX/0sO;LX/3eL;)V

    .line 619390
    move-object v0, p0

    .line 619391
    sput-object v0, LX/3eK;->c:LX/3eK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 619392
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 619393
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 619394
    :cond_1
    sget-object v0, LX/3eK;->c:LX/3eK;

    return-object v0

    .line 619395
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 619396
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 619397
    const/4 v2, 0x0

    .line 619398
    const-class v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStickerTagsQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStickerTagsQueryModel;

    .line 619399
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 619400
    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStickerTagsQueryModel;->a()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStickerTagsQueryModel$StickerStoreModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStickerTagsQueryModel$StickerStoreModel;->a()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStickerTagsQueryModel$StickerStoreModel$StickerTagsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStickerTagsQueryModel$StickerStoreModel$StickerTagsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStickerTagsQueryModel$StickerStoreModel$StickerTagsModel$NodesModel;

    .line 619401
    invoke-static {}, Lcom/facebook/stickers/model/StickerTag;->newBuilder()LX/4m8;

    move-result-object v6

    .line 619402
    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStickerTagsQueryModel$StickerStoreModel$StickerTagsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v7

    .line 619403
    iput-object v7, v6, LX/4m8;->a:Ljava/lang/String;

    .line 619404
    move-object v7, v6

    .line 619405
    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStickerTagsQueryModel$StickerStoreModel$StickerTagsModel$NodesModel;->m()Ljava/lang/String;

    move-result-object v8

    .line 619406
    iput-object v8, v7, LX/4m8;->b:Ljava/lang/String;

    .line 619407
    move-object v7, v7

    .line 619408
    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStickerTagsQueryModel$StickerStoreModel$StickerTagsModel$NodesModel;->l()Z

    move-result v8

    .line 619409
    iput-boolean v8, v7, LX/4m8;->d:Z

    .line 619410
    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStickerTagsQueryModel$StickerStoreModel$StickerTagsModel$NodesModel;->l()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 619411
    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStickerTagsQueryModel$StickerStoreModel$StickerTagsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v7

    .line 619412
    iput-object v7, v6, LX/4m8;->c:Ljava/lang/String;

    .line 619413
    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStickerTagsQueryModel$StickerStoreModel$StickerTagsModel$NodesModel;->n()I

    move-result v7

    .line 619414
    iput v7, v6, LX/4m8;->e:I

    .line 619415
    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStickerTagsQueryModel$StickerStoreModel$StickerTagsModel$NodesModel;->o()LX/1vs;

    move-result-object v0

    iget-object v7, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v7, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 619416
    iput-object v0, v6, LX/4m8;->f:Ljava/lang/String;

    .line 619417
    :cond_0
    invoke-virtual {v6}, LX/4m8;->a()Lcom/facebook/stickers/model/StickerTag;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 619418
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 619419
    :cond_1
    new-instance v0, Lcom/facebook/stickers/service/FetchStickerTagsResult;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/stickers/service/FetchStickerTagsResult;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 619420
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 4

    .prologue
    .line 619421
    check-cast p1, Lcom/facebook/stickers/service/FetchStickerTagsParams;

    .line 619422
    new-instance v0, LX/8jl;

    invoke-direct {v0}, LX/8jl;-><init>()V

    move-object v0, v0

    .line 619423
    const-string v1, "tag_type"

    .line 619424
    iget-object v2, p1, Lcom/facebook/stickers/service/FetchStickerTagsParams;->b:LX/8m4;

    move-object v2, v2

    .line 619425
    invoke-virtual {v2}, LX/8m4;->getQueryParam()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "thumbnail_size"

    iget-object v2, p0, LX/3eK;->b:LX/3eL;

    .line 619426
    iget-object v3, v2, LX/3eL;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p1, 0x7f0b07ce

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    move v2, v3

    .line 619427
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "thumbnail_scale_factor"

    iget-object v2, p0, LX/3eK;->b:LX/3eL;

    invoke-virtual {v2}, LX/3eL;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "thumbnail_format"

    iget-object v2, p0, LX/3eK;->b:LX/3eL;

    invoke-virtual {v2}, LX/3eL;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    return-object v0
.end method
