.class public LX/3RX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3RY;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/7Cb;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/7Cc;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/3RZ;

.field public final d:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "LX/7Cb;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/3RZ;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/7Cb;",
            ">;",
            "LX/0Or",
            "<",
            "LX/7Cc;",
            ">;",
            "LX/3RZ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 580539
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 580540
    iput-object p1, p0, LX/3RX;->a:LX/0Or;

    .line 580541
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, LX/0QN;->a(J)LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/3RX;->d:LX/0QI;

    .line 580542
    iput-object p2, p0, LX/3RX;->b:LX/0Or;

    .line 580543
    iput-object p3, p0, LX/3RX;->c:LX/3RZ;

    .line 580544
    return-void
.end method

.method public static a(LX/3RX;LX/7Cb;Ljava/lang/Object;)V
    .locals 1
    .param p1    # LX/7Cb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 580545
    iget-object v0, p0, LX/3RX;->d:LX/0QI;

    if-nez p2, :cond_0

    const-string p2, "NULL"

    :cond_0
    invoke-interface {v0, p1, p2}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 580546
    return-void
.end method

.method public static b(LX/3RX;)LX/7Cb;
    .locals 1

    .prologue
    .line 580547
    iget-object v0, p0, LX/3RX;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Cb;

    .line 580548
    iput-object p0, v0, LX/7Cb;->g:LX/3RY;

    .line 580549
    return-object v0
.end method


# virtual methods
.method public final a(IIF)LX/7Cb;
    .locals 3

    .prologue
    .line 580550
    invoke-static {p0}, LX/3RX;->b(LX/3RX;)LX/7Cb;

    move-result-object v0

    .line 580551
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/3RX;->a(LX/3RX;LX/7Cb;Ljava/lang/Object;)V

    .line 580552
    iget-object v1, v0, LX/7Cb;->c:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 580553
    invoke-static {v0, p1, p2, p3}, LX/7Cb;->b(LX/7Cb;IIF)V

    .line 580554
    :goto_0
    return-object v0

    .line 580555
    :cond_0
    :try_start_0
    iget-object v1, v0, LX/7Cb;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/sounds/SoundPlayer$2;

    invoke-direct {v2, v0, p1, p2, p3}, Lcom/facebook/sounds/SoundPlayer$2;-><init>(LX/7Cb;IIF)V

    const p0, 0x1bc9cb8b

    invoke-static {v1, v2, p0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 580556
    :catch_0
    move-exception v1

    .line 580557
    sget-object v2, LX/7Cb;->a:Ljava/lang/Class;

    const-string p0, "Attempt to play sound rejected by executor service"

    invoke-static {v2, p0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;IF)LX/7Cb;
    .locals 1
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 580558
    invoke-static {p0}, LX/3RX;->b(LX/3RX;)LX/7Cb;

    move-result-object v0

    .line 580559
    invoke-static {p0, v0, p1}, LX/3RX;->a(LX/3RX;LX/7Cb;Ljava/lang/Object;)V

    .line 580560
    invoke-virtual {v0, p1, p2, p3}, LX/7Cb;->a(Landroid/net/Uri;IF)V

    .line 580561
    return-object v0
.end method

.method public final a(Ljava/lang/String;F)LX/7Cb;
    .locals 2

    .prologue
    .line 580562
    iget-object v0, p0, LX/3RX;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Cc;

    invoke-virtual {v0, p1}, LX/7Cc;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 580563
    if-nez v0, :cond_0

    .line 580564
    const/4 v0, 0x0

    .line 580565
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/3RX;->c:LX/3RZ;

    invoke-virtual {v1}, LX/3RZ;->a()I

    move-result v1

    invoke-virtual {p0, v0, v1, p2}, LX/3RX;->a(IIF)LX/7Cb;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/7Cb;)V
    .locals 1

    .prologue
    .line 580566
    iget-object v0, p0, LX/3RX;->d:LX/0QI;

    invoke-interface {v0, p1}, LX/0QI;->b(Ljava/lang/Object;)V

    .line 580567
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 580568
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 580569
    const/4 v0, 0x0

    return v0
.end method
