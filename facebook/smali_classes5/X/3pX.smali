.class public final LX/3pX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:I

.field private final b:Ljava/lang/CharSequence;

.field private final c:Landroid/app/PendingIntent;

.field public final d:Landroid/os/Bundle;

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/3qL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 641606
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-direct {p0, p1, p2, p3, v0}, LX/3pX;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;)V

    .line 641607
    return-void
.end method

.method private constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 641608
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 641609
    iput p1, p0, LX/3pX;->a:I

    .line 641610
    invoke-static {p2}, LX/2HB;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, LX/3pX;->b:Ljava/lang/CharSequence;

    .line 641611
    iput-object p3, p0, LX/3pX;->c:Landroid/app/PendingIntent;

    .line 641612
    iput-object p4, p0, LX/3pX;->d:Landroid/os/Bundle;

    .line 641613
    return-void
.end method


# virtual methods
.method public final a(LX/3qL;)LX/3pX;
    .locals 1

    .prologue
    .line 641614
    iget-object v0, p0, LX/3pX;->e:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 641615
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3pX;->e:Ljava/util/ArrayList;

    .line 641616
    :cond_0
    iget-object v0, p0, LX/3pX;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 641617
    return-object p0
.end method

.method public final b()LX/3pb;
    .locals 7

    .prologue
    .line 641618
    iget-object v0, p0, LX/3pX;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3pX;->e:Ljava/util/ArrayList;

    iget-object v1, p0, LX/3pX;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [LX/3qL;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3qL;

    move-object v5, v0

    .line 641619
    :goto_0
    new-instance v0, LX/3pb;

    iget v1, p0, LX/3pX;->a:I

    iget-object v2, p0, LX/3pX;->b:Ljava/lang/CharSequence;

    iget-object v3, p0, LX/3pX;->c:Landroid/app/PendingIntent;

    iget-object v4, p0, LX/3pX;->d:Landroid/os/Bundle;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, LX/3pb;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[LX/3qL;B)V

    return-object v0

    .line 641620
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method
