.class public LX/3i4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/3i4;


# instance fields
.field private final a:LX/6DR;

.field private final b:LX/7iI;

.field private final c:LX/0Uh;

.field private final d:LX/1Bg;

.field public final e:LX/0W3;

.field private final f:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(LX/6DR;LX/7iI;LX/0Uh;LX/1Bg;LX/0W3;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 628444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 628445
    iput-object p1, p0, LX/3i4;->a:LX/6DR;

    .line 628446
    iput-object p2, p0, LX/3i4;->b:LX/7iI;

    .line 628447
    iput-object p3, p0, LX/3i4;->c:LX/0Uh;

    .line 628448
    iput-object p4, p0, LX/3i4;->d:LX/1Bg;

    .line 628449
    iput-object p5, p0, LX/3i4;->e:LX/0W3;

    .line 628450
    iput-object p6, p0, LX/3i4;->f:Lcom/facebook/content/SecureContextHelper;

    .line 628451
    return-void
.end method

.method public static a(LX/0QB;)LX/3i4;
    .locals 10

    .prologue
    .line 628452
    sget-object v0, LX/3i4;->g:LX/3i4;

    if-nez v0, :cond_1

    .line 628453
    const-class v1, LX/3i4;

    monitor-enter v1

    .line 628454
    :try_start_0
    sget-object v0, LX/3i4;->g:LX/3i4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 628455
    if-eqz v2, :cond_0

    .line 628456
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 628457
    new-instance v3, LX/3i4;

    invoke-static {v0}, LX/6DR;->a(LX/0QB;)LX/6DR;

    move-result-object v4

    check-cast v4, LX/6DR;

    .line 628458
    new-instance v6, LX/7iI;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {v6, v5}, LX/7iI;-><init>(LX/03V;)V

    .line 628459
    move-object v5, v6

    .line 628460
    check-cast v5, LX/7iI;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-static {v0}, LX/1Bg;->b(LX/0QB;)LX/1Bg;

    move-result-object v7

    check-cast v7, LX/1Bg;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v8

    check-cast v8, LX/0W3;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-direct/range {v3 .. v9}, LX/3i4;-><init>(LX/6DR;LX/7iI;LX/0Uh;LX/1Bg;LX/0W3;Lcom/facebook/content/SecureContextHelper;)V

    .line 628461
    move-object v0, v3

    .line 628462
    sput-object v0, LX/3i4;->g:LX/3i4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 628463
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 628464
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 628465
    :cond_1
    sget-object v0, LX/3i4;->g:LX/3i4;

    return-object v0

    .line 628466
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 628467
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;
    .locals 1

    .prologue
    .line 628468
    const v0, -0x3659e4dc    # -1360740.5f

    invoke-static {p0, v0}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v0

    .line 628469
    return-object v0
.end method

.method public static b(LX/7i7;)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 628470
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 628471
    const-string v1, "JS_BRIDGE_APP_ID"

    .line 628472
    iget-object v2, p0, LX/7i7;->b:Ljava/lang/String;

    move-object v2, v2

    .line 628473
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 628474
    const-string v1, "JS_BRIDGE_PAGE_ID"

    .line 628475
    iget-object v2, p0, LX/7i7;->d:Ljava/lang/String;

    move-object v2, v2

    .line 628476
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 628477
    const-string v1, "JS_BRIDGE_PAGE_NAME"

    .line 628478
    iget-object v2, p0, LX/7i7;->e:Ljava/lang/String;

    move-object v2, v2

    .line 628479
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 628480
    const-string v1, "JS_BRIDGE_WHITELISTED_DOMAINS"

    new-instance v2, Ljava/util/ArrayList;

    .line 628481
    iget-object v3, p0, LX/7i7;->h:Ljava/util/List;

    move-object v3, v3

    .line 628482
    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 628483
    const-string v1, "JS_BRIDGE_APP_NAME"

    .line 628484
    iget-object v2, p0, LX/7i7;->c:Ljava/lang/String;

    move-object v2, v2

    .line 628485
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 628486
    const-string v1, "JS_BRIDGE_ASID"

    .line 628487
    iget-object v2, p0, LX/7i7;->f:Ljava/lang/String;

    move-object v2, v2

    .line 628488
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 628489
    const-string v1, "JS_BRIDGE_PSID"

    .line 628490
    iget-object v2, p0, LX/7i7;->g:Ljava/lang/String;

    move-object v2, v2

    .line 628491
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 628492
    const-string v1, "JS_BRIDGE_PAGE_POLICY_URL"

    .line 628493
    iget-object v2, p0, LX/7i7;->j:Ljava/lang/String;

    move-object v2, v2

    .line 628494
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 628495
    const-string v1, "JS_BRIDGE_EXTENSION_TYPE"

    sget-object v2, LX/6Cx;->INSTANT_EXPERIENCE:LX/6Cx;

    iget-object v2, v2, LX/6Cx;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 628496
    const-string v1, "JS_BRIDGE_LOG_SOURCE"

    .line 628497
    iget-object v2, p0, LX/7i7;->i:LX/7i6;

    move-object v2, v2

    .line 628498
    iget-object v2, v2, LX/7i6;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 628499
    const-string v1, "JS_BRIDGE_APP_ICON_URL"

    .line 628500
    iget-object v2, p0, LX/7i7;->l:Ljava/lang/String;

    move-object v2, v2

    .line 628501
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 628502
    const-string v1, "JS_BRIDGE_SPLASH_SCREEN_COLOR"

    .line 628503
    iget-object v2, p0, LX/7i7;->m:Ljava/lang/String;

    move-object v2, v2

    .line 628504
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 628505
    const-string v1, "JS_BRIDGE_SHOW_INSTANT_EXPERIENCES_WEBVIEW_CHROME"

    .line 628506
    iget-boolean v2, p0, LX/7i7;->p:Z

    move v2, v2

    .line 628507
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 628508
    const-string v1, "JS_BRIDGE_IS_AUTOFILL_HORIZONTAL_SCROLL_BAR_ENABLED"

    .line 628509
    iget-boolean v2, p0, LX/7i7;->q:Z

    move v2, v2

    .line 628510
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 628511
    const-string v1, "JS_BRIDGE_SHOW_INSTANT_EXPERIENCES_COPY_LINK"

    .line 628512
    iget-boolean v2, p0, LX/7i7;->r:Z

    move v2, v2

    .line 628513
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 628514
    const-string v1, "JS_BRIDGE_SHOW_INSTANT_EXPERIENCES_MANAGE_SETTING"

    .line 628515
    iget-boolean v2, p0, LX/7i7;->s:Z

    move v2, v2

    .line 628516
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 628517
    const-string v1, "JS_BRIDGE_SHOW_INSTANT_EXPERIENCES_AUTOFILL_SETTING"

    .line 628518
    iget-boolean v2, p0, LX/7i7;->t:Z

    move v2, v2

    .line 628519
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 628520
    const-string v1, "JS_BRIDGE_INSTANT_EXPERIENCES_WEBVIEW_CHROME_MANIFEST"

    .line 628521
    iget-object v2, p0, LX/7i7;->u:Ljava/lang/String;

    move-object v2, v2

    .line 628522
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 628523
    const-string v1, "JS_BRIDGE_AD_ID"

    .line 628524
    iget-object v2, p0, LX/7i7;->v:Ljava/lang/String;

    move-object v2, v2

    .line 628525
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 628526
    const-string v1, "JS_BRIDGE_ENABLE_PAYMENT"

    .line 628527
    iget-boolean v2, p0, LX/7i7;->w:Z

    move v2, v2

    .line 628528
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 628529
    const-string v1, "JS_BRIDGE_SHOW_INSTANT_EXPERIENCES_PRODUCT_HISTORY"

    .line 628530
    iget-boolean v2, p0, LX/7i7;->x:Z

    move v2, v2

    .line 628531
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 628532
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/7i7;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 628533
    iget-boolean v0, p2, LX/7i7;->o:Z

    move v0, v0

    .line 628534
    if-eqz v0, :cond_0

    .line 628535
    iget-object v0, p0, LX/3i4;->a:LX/6DR;

    invoke-virtual {v0}, LX/6DR;->a()V

    .line 628536
    :cond_0
    const/4 v10, 0x0

    .line 628537
    new-instance v4, LX/0DW;

    invoke-direct {v4}, LX/0DW;-><init>()V

    const-string v5, "THEME_INSTANT_EXPERIENCE"

    invoke-virtual {v4, v5}, LX/0DW;->d(Ljava/lang/String;)LX/0DW;

    move-result-object v4

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget-object v5, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4, v5}, LX/0DW;->a(Ljava/util/Locale;)LX/0DW;

    move-result-object v4

    invoke-virtual {v4, v10}, LX/0DW;->b(Z)LX/0DW;

    move-result-object v4

    const-string v5, " FBExtensions/0.1"

    invoke-virtual {v4, v5}, LX/0DW;->e(Ljava/lang/String;)LX/0DW;

    move-result-object v4

    .line 628538
    const/4 v5, -0x1

    .line 628539
    iget-object v6, p2, LX/7i7;->m:Ljava/lang/String;

    if-eqz v6, :cond_2

    iget-object v6, p2, LX/7i7;->m:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_2

    .line 628540
    iget-object v5, p2, LX/7i7;->m:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/lang/String;->codePointAt(I)I

    move-result v5

    const/16 v6, 0x23

    if-eq v5, v6, :cond_1

    .line 628541
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "#"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p2, LX/7i7;->m:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p2, LX/7i7;->m:Ljava/lang/String;

    .line 628542
    :cond_1
    iget-object v5, p2, LX/7i7;->m:Ljava/lang/String;

    invoke-static {v5}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v5

    .line 628543
    :cond_2
    move v5, v5

    .line 628544
    iget-object v6, v4, LX/0DW;->a:Landroid/content/Intent;

    const-string v7, "splash_screen_color"

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 628545
    move-object v4, v4

    .line 628546
    iget-object v5, p2, LX/7i7;->l:Ljava/lang/String;

    move-object v5, v5

    .line 628547
    iget-object v6, v4, LX/0DW;->a:Landroid/content/Intent;

    const-string v7, "splash_icon_url"

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 628548
    move-object v4, v4

    .line 628549
    iget-boolean v5, p2, LX/7i7;->p:Z

    move v5, v5

    .line 628550
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 628551
    iget-object v6, v4, LX/0DW;->a:Landroid/content/Intent;

    const-string v7, "show_instant_experiences_webview_chrome"

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 628552
    move-object v4, v4

    .line 628553
    iget-object v5, p2, LX/7i7;->u:Ljava/lang/String;

    move-object v5, v5

    .line 628554
    iget-object v6, v4, LX/0DW;->a:Landroid/content/Intent;

    const-string v7, "instant_experiences_webview_chrome_manifest"

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 628555
    move-object v4, v4

    .line 628556
    iget-boolean v5, p2, LX/7i7;->x:Z

    move v5, v5

    .line 628557
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 628558
    iget-object v6, v4, LX/0DW;->a:Landroid/content/Intent;

    const-string v7, "show_instant_experiences_product_history"

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 628559
    move-object v4, v4

    .line 628560
    const v5, 0x7f020724

    invoke-virtual {v4, v5}, LX/0DW;->f(I)LX/0DW;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iget-object v6, p0, LX/3i4;->e:LX/0W3;

    sget-wide v8, LX/0X5;->fs:J

    const-string v7, "//connect.facebook.com/en_US/platform.Extensions.js"

    invoke-interface {v6, v8, v9, v7}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 628561
    iget-boolean v7, p2, LX/7i7;->q:Z

    move v7, v7

    .line 628562
    iget-object v8, v4, LX/0DW;->a:Landroid/content/Intent;

    const-string v9, "BrowserLiteIntent.EXTRA_ENABLE_AUTOFILL"

    invoke-virtual {v8, v9, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 628563
    iget-object v8, v4, LX/0DW;->a:Landroid/content/Intent;

    const-string v9, "BrowserLiteIntent.AutofillExtras.EXTRA_FB_AUTOFILL_SDK_URL"

    invoke-virtual {v8, v9, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 628564
    iget-object v8, v4, LX/0DW;->a:Landroid/content/Intent;

    const-string v9, "BrowserLiteIntent.AutofillExtras.EXTRA_FB_AUTOFILL_SHOW_HORIZONTAL_SCROLL_BAR"

    invoke-virtual {v8, v9, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 628565
    move-object v4, v4

    .line 628566
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f081d74

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "SAVE_LINK"

    invoke-virtual {v4, v5, v10, v6}, LX/0DW;->b(Ljava/lang/String;ILjava/lang/String;)LX/0DW;

    move-result-object v4

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f081d77

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "SHARE_TIMELINE"

    invoke-virtual {v4, v5, v10, v6}, LX/0DW;->b(Ljava/lang/String;ILjava/lang/String;)LX/0DW;

    move-result-object v4

    const v5, 0x7f0400db

    const v6, 0x7f0400de

    const v7, 0x7f0400eb

    const v8, 0x7f0400ee

    invoke-virtual {v4, v5, v6, v7, v8}, LX/0DW;->a(IIII)LX/0DW;

    move-result-object v4

    .line 628567
    invoke-static {p2}, LX/3i4;->b(LX/7i7;)Landroid/os/Bundle;

    move-result-object v5

    .line 628568
    new-instance v6, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;

    invoke-direct {v6}, Lcom/facebook/browserextensions/ipc/BrowserExtensionsJSBridgeProxy;-><init>()V

    .line 628569
    invoke-virtual {v6, v5}, Lcom/facebook/browser/lite/bridge/BrowserLiteJSBridgeProxy;->a(Landroid/os/Bundle;)V

    .line 628570
    move-object v5, v6

    .line 628571
    invoke-virtual {v4, v5}, LX/0DW;->a(Landroid/os/Parcelable;)LX/0DW;

    move-result-object v4

    .line 628572
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 628573
    const-string v6, "JS_BRIDGE_SESSION_ID"

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 628574
    const-string v6, "JS_BRIDGE_EXTENSION_TYPE"

    sget-object v7, LX/6Cx;->INSTANT_EXPERIENCE:LX/6Cx;

    iget-object v7, v7, LX/6Cx;->value:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 628575
    const-string v6, "JS_BRIDGE_APP_ID"

    .line 628576
    iget-object v7, p2, LX/7i7;->b:Ljava/lang/String;

    move-object v7, v7

    .line 628577
    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 628578
    const-string v6, "JS_BRIDGE_APP_NAME"

    .line 628579
    iget-object v7, p2, LX/7i7;->c:Ljava/lang/String;

    move-object v7, v7

    .line 628580
    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 628581
    const-string v6, "JS_BRIDGE_LOG_SOURCE"

    .line 628582
    iget-object v7, p2, LX/7i7;->i:LX/7i6;

    move-object v7, v7

    .line 628583
    iget-object v7, v7, LX/7i6;->value:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 628584
    const-string v6, "JS_BRIDGE_AD_ID"

    .line 628585
    iget-object v7, p2, LX/7i7;->v:Ljava/lang/String;

    move-object v7, v7

    .line 628586
    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 628587
    move-object v5, v5

    .line 628588
    invoke-virtual {v4, v5}, LX/0DW;->a(Landroid/os/Bundle;)LX/0DW;

    move-result-object v4

    invoke-static {p2}, LX/3i4;->b(LX/7i7;)Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0DW;->a(Landroid/os/Bundle;)LX/0DW;

    move-result-object v4

    .line 628589
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 628590
    const-string v6, "JS_BRIDGE_URL"

    .line 628591
    iget-object v7, p2, LX/7i7;->a:Landroid/net/Uri;

    move-object v7, v7

    .line 628592
    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 628593
    move-object v5, v5

    .line 628594
    invoke-virtual {v4, v5}, LX/0DW;->a(Landroid/os/Bundle;)LX/0DW;

    move-result-object v4

    .line 628595
    iget-boolean v5, p2, LX/7i7;->n:Z

    move v5, v5

    .line 628596
    if-eqz v5, :cond_3

    .line 628597
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f081d6c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget-object v6, LX/6Eo;->ADD_TO_HOME_SCREEN:LX/6Eo;

    invoke-virtual {v6}, LX/6Eo;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v10, v6}, LX/0DW;->b(Ljava/lang/String;ILjava/lang/String;)LX/0DW;

    .line 628598
    :cond_3
    iget-boolean v5, p2, LX/7i7;->r:Z

    move v5, v5

    .line 628599
    if-eqz v5, :cond_4

    .line 628600
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f081d6d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "COPY_LINK"

    invoke-virtual {v4, v5, v10, v6}, LX/0DW;->b(Ljava/lang/String;ILjava/lang/String;)LX/0DW;

    .line 628601
    :cond_4
    iget-boolean v5, p2, LX/7i7;->s:Z

    move v5, v5

    .line 628602
    if-eqz v5, :cond_5

    .line 628603
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f081d6b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget-object v6, LX/6Eo;->MANAGE_SETTINGS:LX/6Eo;

    invoke-virtual {v6}, LX/6Eo;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v10, v6}, LX/0DW;->b(Ljava/lang/String;ILjava/lang/String;)LX/0DW;

    .line 628604
    :cond_5
    move-object v0, v4

    .line 628605
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/browser/lite/BrowserLiteActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 628606
    iget-object v2, p2, LX/7i7;->a:Landroid/net/Uri;

    move-object v2, v2

    .line 628607
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 628608
    iget-object v2, p0, LX/3i4;->d:LX/1Bg;

    invoke-virtual {v2, v0}, LX/1Bg;->a(LX/0DW;)V

    .line 628609
    invoke-virtual {v0}, LX/0DW;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 628610
    const-string v0, "iab_click_source"

    const-string v2, "fbfeed_instant_experience"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 628611
    iget-object v0, p0, LX/3i4;->b:LX/7iI;

    .line 628612
    iget-object v2, p2, LX/7i7;->i:LX/7i6;

    move-object v2, v2

    .line 628613
    iget-object v3, p2, LX/7i7;->k:Lorg/json/JSONObject;

    move-object v3, v3

    .line 628614
    if-eqz v3, :cond_6

    sget-object v4, LX/7i6;->OFFERS:LX/7i6;

    if-eq v2, v4, :cond_7

    .line 628615
    :cond_6
    :goto_0
    move-object v0, v1

    .line 628616
    return-object v0

    .line 628617
    :cond_7
    :try_start_0
    invoke-virtual {v3}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v6

    .line 628618
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 628619
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 628620
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 628621
    const/4 v5, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    :cond_8
    move v4, v5

    :goto_2
    packed-switch v4, :pswitch_data_0

    goto :goto_1

    .line 628622
    :pswitch_0
    const-string v4, "offer_code"

    invoke-virtual {v1, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 628623
    :catch_0
    move-exception v4

    .line 628624
    iget-object v5, v0, LX/7iI;->b:LX/03V;

    sget-object v6, LX/7iI;->a:Ljava/lang/String;

    invoke-virtual {v5, v6, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 628625
    :sswitch_0
    :try_start_1
    const-string v8, "offer_code"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v4, 0x0

    goto :goto_2

    :sswitch_1
    const-string v8, "offer_id"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v4, 0x1

    goto :goto_2

    :sswitch_2
    const-string v8, "share_id"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v4, 0x2

    goto :goto_2

    :sswitch_3
    const-string v8, "title"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v4, 0x3

    goto :goto_2

    :sswitch_4
    const-string v8, "offer_view_id"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v4, 0x4

    goto :goto_2

    :sswitch_5
    const-string v8, "claim_type"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v4, 0x5

    goto :goto_2

    :sswitch_6
    const-string v8, "notif_trigger"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v4, 0x6

    goto :goto_2

    :sswitch_7
    const-string v8, "notif_medium"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v4, 0x7

    goto :goto_2

    :sswitch_8
    const-string v8, "rule"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0x8

    goto :goto_2

    .line 628626
    :pswitch_1
    const-string v4, "offer_id"

    invoke-virtual {v1, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 628627
    :pswitch_2
    const-string v4, "share_id"

    invoke-virtual {v1, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 628628
    :pswitch_3
    const-string v4, "title"

    invoke-virtual {v1, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 628629
    :pswitch_4
    const-string v4, "offer_view_id"

    invoke-virtual {v1, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 628630
    :pswitch_5
    const-string v4, "claim_type"

    invoke-virtual {v1, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 628631
    :pswitch_6
    const-string v4, "notif_trigger"

    invoke-virtual {v1, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 628632
    :pswitch_7
    const-string v4, "notif_medium"

    invoke-virtual {v1, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 628633
    :pswitch_8
    const-string v4, "rule"

    invoke-virtual {v1, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x2dcf1622 -> :sswitch_1
        -0x2c889aa3 -> :sswitch_5
        -0x2c54de85 -> :sswitch_2
        -0x2344e457 -> :sswitch_6
        -0x223361ee -> :sswitch_4
        0x3596fc -> :sswitch_8
        0x6942258 -> :sswitch_3
        0x99b65f0 -> :sswitch_0
        0x23bdc764 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;)V
    .locals 18

    .prologue
    .line 628634
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->H()Ljava/lang/String;

    move-result-object v4

    .line 628635
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->W()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    .line 628636
    if-nez v2, :cond_1

    const/4 v6, 0x0

    .line 628637
    :goto_0
    if-nez v2, :cond_2

    const/4 v2, 0x0

    move-object v10, v2

    .line 628638
    :goto_1
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->O()Ljava/lang/String;

    move-result-object v16

    .line 628639
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->P()Ljava/lang/String;

    move-result-object v7

    .line 628640
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->X()Ljava/lang/String;

    move-result-object v17

    .line 628641
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->N()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v3

    .line 628642
    if-nez v3, :cond_3

    const/4 v5, 0x0

    .line 628643
    :goto_2
    if-nez v3, :cond_4

    const/4 v2, 0x0

    move-object v15, v2

    .line 628644
    :goto_3
    if-nez v3, :cond_5

    const/4 v2, 0x0

    move-object v3, v2

    .line 628645
    :goto_4
    if-nez v3, :cond_6

    const/4 v2, 0x0

    move-object v14, v2

    .line 628646
    :goto_5
    if-nez v3, :cond_7

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v9

    .line 628647
    :goto_6
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->I()Ljava/lang/String;

    move-result-object v8

    .line 628648
    if-nez v3, :cond_8

    const/4 v2, 0x0

    move-object v13, v2

    .line 628649
    :goto_7
    if-nez v3, :cond_9

    const/4 v2, 0x0

    move-object v12, v2

    .line 628650
    :goto_8
    if-nez v3, :cond_a

    const/4 v2, 0x0

    move-object v11, v2

    .line 628651
    :goto_9
    new-instance v2, LX/7i7;

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v8, LX/7i6;->ORGANIC_SHARE:LX/7i6;

    invoke-direct/range {v2 .. v9}, LX/7i7;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/7i6;Ljava/util/List;)V

    invoke-virtual {v2, v10}, LX/7i7;->a(Ljava/lang/String;)LX/7i7;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, LX/7i7;->b(Ljava/lang/String;)LX/7i7;

    move-result-object v2

    invoke-virtual {v2, v15}, LX/7i7;->c(Ljava/lang/String;)LX/7i7;

    move-result-object v2

    invoke-virtual {v2, v13}, LX/7i7;->d(Ljava/lang/String;)LX/7i7;

    move-result-object v2

    invoke-virtual {v2, v14}, LX/7i7;->e(Ljava/lang/String;)LX/7i7;

    move-result-object v3

    if-eqz v12, :cond_b

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;->k()Z

    move-result v2

    if-eqz v2, :cond_b

    const/4 v2, 0x1

    :goto_a
    invoke-virtual {v3, v2}, LX/7i7;->d(Z)LX/7i7;

    move-result-object v3

    if-eqz v12, :cond_c

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;->a()Z

    move-result v2

    if-eqz v2, :cond_c

    const/4 v2, 0x1

    :goto_b
    invoke-virtual {v3, v2}, LX/7i7;->a(Z)LX/7i7;

    move-result-object v3

    if-eqz v12, :cond_d

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;->m()Z

    move-result v2

    if-eqz v2, :cond_d

    const/4 v2, 0x1

    :goto_c
    invoke-virtual {v3, v2}, LX/7i7;->e(Z)LX/7i7;

    move-result-object v3

    if-eqz v12, :cond_e

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;->l()Z

    move-result v2

    if-eqz v2, :cond_e

    const/4 v2, 0x1

    :goto_d
    invoke-virtual {v3, v2}, LX/7i7;->f(Z)LX/7i7;

    move-result-object v3

    if-eqz v12, :cond_f

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;->n()Z

    move-result v2

    if-eqz v2, :cond_f

    const/4 v2, 0x1

    :goto_e
    invoke-virtual {v3, v2}, LX/7i7;->c(Z)LX/7i7;

    move-result-object v3

    if-eqz v12, :cond_10

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;->j()Z

    move-result v2

    if-eqz v2, :cond_10

    const/4 v2, 0x1

    :goto_f
    invoke-virtual {v3, v2}, LX/7i7;->b(Z)LX/7i7;

    move-result-object v3

    if-eqz v12, :cond_11

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;->o()Z

    move-result v2

    if-eqz v2, :cond_11

    const/4 v2, 0x1

    :goto_10
    invoke-virtual {v3, v2}, LX/7i7;->g(Z)LX/7i7;

    move-result-object v3

    if-eqz v12, :cond_12

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;->p()Z

    move-result v2

    if-eqz v2, :cond_12

    const/4 v2, 0x1

    :goto_11
    invoke-virtual {v3, v2}, LX/7i7;->h(Z)LX/7i7;

    move-result-object v2

    invoke-virtual {v2, v11}, LX/7i7;->g(Ljava/lang/String;)LX/7i7;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, LX/7i7;->f(Ljava/lang/String;)LX/7i7;

    move-result-object v2

    .line 628652
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2}, LX/3i4;->a(Landroid/content/Context;LX/7i7;)Landroid/content/Intent;

    move-result-object v2

    .line 628653
    move-object/from16 v0, p0

    iget-object v3, v0, LX/3i4;->f:Lcom/facebook/content/SecureContextHelper;

    move-object/from16 v0, p1

    invoke-interface {v3, v2, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 628654
    move-object/from16 v0, p1

    instance-of v2, v0, Landroid/app/Activity;

    if-eqz v2, :cond_0

    .line 628655
    check-cast p1, Landroid/app/Activity;

    const v2, 0x7f0400db

    const v3, 0x7f0400de

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 628656
    :cond_0
    return-void

    .line 628657
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 628658
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v2

    move-object v10, v2

    goto/16 :goto_1

    .line 628659
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLApplication;->l()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2

    .line 628660
    :cond_4
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLApplication;->n()Ljava/lang/String;

    move-result-object v2

    move-object v15, v2

    goto/16 :goto_3

    .line 628661
    :cond_5
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLApplication;->r()Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;

    move-result-object v2

    move-object v3, v2

    goto/16 :goto_4

    .line 628662
    :cond_6
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->l()Ljava/lang/String;

    move-result-object v2

    move-object v14, v2

    goto/16 :goto_5

    .line 628663
    :cond_7
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->j()LX/0Px;

    move-result-object v9

    goto/16 :goto_6

    .line 628664
    :cond_8
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->m()Ljava/lang/String;

    move-result-object v2

    move-object v13, v2

    goto/16 :goto_7

    .line 628665
    :cond_9
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->k()Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    move-result-object v2

    move-object v12, v2

    goto/16 :goto_8

    .line 628666
    :cond_a
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->n()Ljava/lang/String;

    move-result-object v2

    move-object v11, v2

    goto/16 :goto_9

    .line 628667
    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_a

    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_b

    :cond_d
    const/4 v2, 0x0

    goto/16 :goto_c

    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_d

    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_e

    :cond_10
    const/4 v2, 0x0

    goto/16 :goto_f

    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_10

    :cond_12
    const/4 v2, 0x0

    goto/16 :goto_11
.end method
