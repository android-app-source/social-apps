.class public final LX/4f8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1ex;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1ex",
        "<",
        "LX/1FL;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1BV;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/1cd;

.field public final synthetic d:LX/1cW;

.field public final synthetic e:Z

.field public final synthetic f:LX/1cJ;


# direct methods
.method public constructor <init>(LX/1cJ;LX/1BV;Ljava/lang/String;LX/1cd;LX/1cW;Z)V
    .locals 0

    .prologue
    .line 797958
    iput-object p1, p0, LX/4f8;->f:LX/1cJ;

    iput-object p2, p0, LX/4f8;->a:LX/1BV;

    iput-object p3, p0, LX/4f8;->b:Ljava/lang/String;

    iput-object p4, p0, LX/4f8;->c:LX/1cd;

    iput-object p5, p0, LX/4f8;->d:LX/1cW;

    iput-boolean p6, p0, LX/4f8;->e:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1eg;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 797959
    const/4 v9, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 797960
    invoke-virtual {p1}, LX/1eg;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, LX/1eg;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, LX/1eg;->e()Ljava/lang/Exception;

    move-result-object v0

    instance-of v0, v0, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_7

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 797961
    if-eqz v0, :cond_2

    .line 797962
    iget-object v0, p0, LX/4f8;->a:LX/1BV;

    iget-object v1, p0, LX/4f8;->b:Ljava/lang/String;

    const-string v3, "MediaVariationsFallbackProducer"

    invoke-interface {v0, v1, v3, v9}, LX/1BV;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 797963
    iget-object v0, p0, LX/4f8;->c:LX/1cd;

    invoke-virtual {v0}, LX/1cd;->b()V

    move v1, v2

    .line 797964
    :goto_1
    if-eqz v1, :cond_1

    .line 797965
    iget-object v0, p0, LX/4f8;->f:LX/1cJ;

    iget-object v1, p0, LX/4f8;->c:LX/1cd;

    iget-object v2, p0, LX/4f8;->d:LX/1cW;

    invoke-static {v0, v1, v2}, LX/1cJ;->b(LX/1cJ;LX/1cd;LX/1cW;)V

    .line 797966
    :cond_1
    return-object v9

    .line 797967
    :cond_2
    invoke-virtual {p1}, LX/1eg;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 797968
    iget-object v0, p0, LX/4f8;->a:LX/1BV;

    iget-object v2, p0, LX/4f8;->b:Ljava/lang/String;

    const-string v3, "MediaVariationsFallbackProducer"

    invoke-virtual {p1}, LX/1eg;->e()Ljava/lang/Exception;

    move-result-object v4

    invoke-interface {v0, v2, v3, v4, v9}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    .line 797969
    iget-object v0, p0, LX/4f8;->f:LX/1cJ;

    iget-object v2, p0, LX/4f8;->c:LX/1cd;

    iget-object v3, p0, LX/4f8;->d:LX/1cW;

    invoke-static {v0, v2, v3}, LX/1cJ;->b(LX/1cJ;LX/1cd;LX/1cW;)V

    goto :goto_1

    .line 797970
    :cond_3
    invoke-virtual {p1}, LX/1eg;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FL;

    .line 797971
    if-eqz v0, :cond_6

    .line 797972
    iget-object v3, p0, LX/4f8;->a:LX/1BV;

    iget-object v4, p0, LX/4f8;->b:Ljava/lang/String;

    const-string v5, "MediaVariationsFallbackProducer"

    iget-object v6, p0, LX/4f8;->a:LX/1BV;

    iget-object v7, p0, LX/4f8;->b:Ljava/lang/String;

    iget-boolean v8, p0, LX/4f8;->e:Z

    invoke-static {v6, v7, v1, v8}, LX/1cJ;->a(LX/1BV;Ljava/lang/String;ZZ)Ljava/util/Map;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 797973
    iget-boolean v3, p0, LX/4f8;->e:Z

    if-eqz v3, :cond_4

    .line 797974
    iget-object v3, p0, LX/4f8;->c:LX/1cd;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, LX/1cd;->b(F)V

    .line 797975
    :cond_4
    iget-object v3, p0, LX/4f8;->c:LX/1cd;

    iget-boolean v4, p0, LX/4f8;->e:Z

    invoke-virtual {v3, v0, v4}, LX/1cd;->b(Ljava/lang/Object;Z)V

    .line 797976
    invoke-virtual {v0}, LX/1FL;->close()V

    .line 797977
    iget-boolean v0, p0, LX/4f8;->e:Z

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    move v1, v0

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2

    .line 797978
    :cond_6
    iget-object v0, p0, LX/4f8;->a:LX/1BV;

    iget-object v3, p0, LX/4f8;->b:Ljava/lang/String;

    const-string v4, "MediaVariationsFallbackProducer"

    iget-object v5, p0, LX/4f8;->a:LX/1BV;

    iget-object v6, p0, LX/4f8;->b:Ljava/lang/String;

    invoke-static {v5, v6, v2, v2}, LX/1cJ;->a(LX/1BV;Ljava/lang/String;ZZ)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v0, v3, v4, v2}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_1

    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
