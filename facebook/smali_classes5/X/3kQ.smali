.class public final LX/3kQ;
.super LX/3kR;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/3kQ;


# instance fields
.field private final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/13J;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 632914
    invoke-direct {p0, p1}, LX/3kR;-><init>(LX/13J;)V

    .line 632915
    iput-object p2, p0, LX/3kQ;->c:Landroid/content/Context;

    .line 632916
    return-void
.end method

.method public static a(LX/0QB;)LX/3kQ;
    .locals 5

    .prologue
    .line 632917
    sget-object v0, LX/3kQ;->d:LX/3kQ;

    if-nez v0, :cond_1

    .line 632918
    const-class v1, LX/3kQ;

    monitor-enter v1

    .line 632919
    :try_start_0
    sget-object v0, LX/3kQ;->d:LX/3kQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 632920
    if-eqz v2, :cond_0

    .line 632921
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 632922
    new-instance p0, LX/3kQ;

    const-class v3, LX/13J;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/13J;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-direct {p0, v3, v4}, LX/3kQ;-><init>(LX/13J;Landroid/content/Context;)V

    .line 632923
    move-object v0, p0

    .line 632924
    sput-object v0, LX/3kQ;->d:LX/3kQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 632925
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 632926
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 632927
    :cond_1
    sget-object v0, LX/3kQ;->d:LX/3kQ;

    return-object v0

    .line 632928
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 632929
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 632930
    const-string v0, "1957"

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 632931
    const-string v0, "Thread List Interstitial"

    return-object v0
.end method

.method public final h()V
    .locals 5

    .prologue
    .line 632932
    const/4 v0, 0x0

    .line 632933
    invoke-virtual {p0}, LX/13D;->i()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 632934
    invoke-virtual {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 632935
    iget-object v0, v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;->action:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    sget-object v4, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSAGES_DIODE_TAB_BADGEABLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-virtual {v0, v4}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 632936
    const/4 v0, 0x1

    .line 632937
    :goto_1
    if-eqz v0, :cond_2

    .line 632938
    iget-object v0, p0, LX/3kQ;->c:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "ACTION_BADGEABLE_DIODE_PROMOTION_FETCHED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 632939
    :cond_1
    return-void

    :cond_2
    move v1, v0

    .line 632940
    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method
