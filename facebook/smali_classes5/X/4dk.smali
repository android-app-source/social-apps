.class public LX/4dk;
.super LX/1FZ;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/4eM;


# direct methods
.method public constructor <init>(LX/4eM;)V
    .locals 0

    .prologue
    .line 796417
    invoke-direct {p0}, LX/1FZ;-><init>()V

    .line 796418
    iput-object p1, p0, LX/4dk;->a:LX/4eM;

    .line 796419
    return-void
.end method


# virtual methods
.method public final b(IILandroid/graphics/Bitmap$Config;)LX/1FJ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Landroid/graphics/Bitmap$Config;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 796420
    invoke-static {p1, p2, p3}, LX/1le;->a(IILandroid/graphics/Bitmap$Config;)I

    move-result v0

    .line 796421
    iget-object v1, p0, LX/4dk;->a:LX/4eM;

    invoke-virtual {v1, v0}, LX/1FR;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 796422
    invoke-static {v0, p1, p2, p3}, Lcom/facebook/imagepipeline/nativecode/Bitmaps;->a(Landroid/graphics/Bitmap;IILandroid/graphics/Bitmap$Config;)V

    .line 796423
    iget-object v1, p0, LX/4dk;->a:LX/4eM;

    invoke-static {v0, v1}, LX/1FJ;->a(Ljava/lang/Object;LX/1FN;)LX/1FJ;

    move-result-object v0

    return-object v0
.end method
