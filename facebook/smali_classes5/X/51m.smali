.class public final LX/51m;
.super Ljava/io/OutputStream;
.source ""


# instance fields
.field public final a:LX/51g;


# direct methods
.method public constructor <init>(LX/51g;)V
    .locals 1

    .prologue
    .line 825460
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 825461
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/51g;

    iput-object v0, p0, LX/51m;->a:LX/51g;

    .line 825462
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 825453
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Funnels.asOutputStream("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/51m;->a:LX/51g;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final write(I)V
    .locals 2

    .prologue
    .line 825454
    iget-object v0, p0, LX/51m;->a:LX/51g;

    int-to-byte v1, p1

    invoke-interface {v0, v1}, LX/51g;->c(B)LX/51g;

    .line 825455
    return-void
.end method

.method public final write([B)V
    .locals 1

    .prologue
    .line 825456
    iget-object v0, p0, LX/51m;->a:LX/51g;

    invoke-interface {v0, p1}, LX/51g;->c([B)LX/51g;

    .line 825457
    return-void
.end method

.method public final write([BII)V
    .locals 1

    .prologue
    .line 825458
    iget-object v0, p0, LX/51m;->a:LX/51g;

    invoke-interface {v0, p1, p2, p3}, LX/51g;->b([BII)LX/51g;

    .line 825459
    return-void
.end method
