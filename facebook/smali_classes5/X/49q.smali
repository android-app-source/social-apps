.class public final LX/49q;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    .line 675044
    const-wide/16 v6, 0x0

    .line 675045
    const/4 v5, 0x0

    .line 675046
    const/4 v4, 0x0

    .line 675047
    const/4 v3, 0x0

    .line 675048
    const/4 v2, 0x0

    .line 675049
    const/4 v1, 0x0

    .line 675050
    const/4 v0, 0x0

    .line 675051
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v9, :cond_9

    .line 675052
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 675053
    const/4 v0, 0x0

    .line 675054
    :goto_0
    return v0

    .line 675055
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v4, :cond_5

    .line 675056
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 675057
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 675058
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_0

    if-eqz v0, :cond_0

    .line 675059
    const-string v4, "expiration"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 675060
    const/4 v0, 0x1

    .line 675061
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 675062
    :cond_1
    const-string v4, "free_photos"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 675063
    invoke-static {p0, p1}, LX/49p;->a(LX/15w;LX/186;)I

    move-result v0

    move v10, v0

    goto :goto_1

    .line 675064
    :cond_2
    const-string v4, "remaining_quota"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 675065
    const/4 v0, 0x1

    .line 675066
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v7, v0

    move v9, v4

    goto :goto_1

    .line 675067
    :cond_3
    const-string v4, "total_quota"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 675068
    const/4 v0, 0x1

    .line 675069
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v6, v0

    move v8, v4

    goto :goto_1

    .line 675070
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 675071
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 675072
    if-eqz v1, :cond_6

    .line 675073
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 675074
    :cond_6
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 675075
    if-eqz v7, :cond_7

    .line 675076
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v9, v1}, LX/186;->a(III)V

    .line 675077
    :cond_7
    if-eqz v6, :cond_8

    .line 675078
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v8, v1}, LX/186;->a(III)V

    .line 675079
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :cond_9
    move v8, v3

    move v9, v4

    move v10, v5

    move v11, v1

    move v1, v2

    move-wide v2, v6

    move v7, v11

    move v6, v0

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 675080
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 675081
    invoke-virtual {p0, p1, v3, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 675082
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 675083
    const-string v2, "expiration"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 675084
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 675085
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 675086
    if-eqz v0, :cond_4

    .line 675087
    const-string v1, "free_photos"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 675088
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 675089
    const/4 v6, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v7

    if-ge v6, v7, :cond_3

    .line 675090
    invoke-virtual {p0, v0, v6}, LX/15i;->q(II)I

    move-result v7

    const-wide/16 v10, 0x0

    .line 675091
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 675092
    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8, v10, v11}, LX/15i;->a(IIJ)J

    move-result-wide v8

    .line 675093
    cmp-long v10, v8, v10

    if-eqz v10, :cond_1

    .line 675094
    const-string v10, "expiration"

    invoke-virtual {p2, v10}, LX/0nX;->a(Ljava/lang/String;)V

    .line 675095
    invoke-virtual {p2, v8, v9}, LX/0nX;->a(J)V

    .line 675096
    :cond_1
    const/4 v8, 0x1

    invoke-virtual {p0, v7, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v8

    .line 675097
    if-eqz v8, :cond_2

    .line 675098
    const-string v9, "photo_regex"

    invoke-virtual {p2, v9}, LX/0nX;->a(Ljava/lang/String;)V

    .line 675099
    invoke-virtual {p2, v8}, LX/0nX;->b(Ljava/lang/String;)V

    .line 675100
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 675101
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 675102
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 675103
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 675104
    if-eqz v0, :cond_5

    .line 675105
    const-string v1, "remaining_quota"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 675106
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 675107
    :cond_5
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 675108
    if-eqz v0, :cond_6

    .line 675109
    const-string v1, "total_quota"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 675110
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 675111
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 675112
    return-void
.end method
