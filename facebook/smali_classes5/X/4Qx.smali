.class public LX/4Qx;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 707105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    .line 707106
    const-wide/16 v8, 0x0

    .line 707107
    const-wide/16 v6, 0x0

    .line 707108
    const-wide/16 v4, 0x0

    .line 707109
    const/4 v2, 0x0

    .line 707110
    const/4 v1, 0x0

    .line 707111
    const/4 v0, 0x0

    .line 707112
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v10, :cond_8

    .line 707113
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 707114
    const/4 v0, 0x0

    .line 707115
    :goto_0
    return v0

    .line 707116
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v4, :cond_4

    .line 707117
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 707118
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 707119
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_0

    if-eqz v0, :cond_0

    .line 707120
    const-string v4, "x"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 707121
    const/4 v0, 0x1

    .line 707122
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 707123
    :cond_1
    const-string v4, "y"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 707124
    const/4 v0, 0x1

    .line 707125
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v4

    move v7, v0

    move-wide v10, v4

    goto :goto_1

    .line 707126
    :cond_2
    const-string v4, "z"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 707127
    const/4 v0, 0x1

    .line 707128
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v4

    move v6, v0

    move-wide v8, v4

    goto :goto_1

    .line 707129
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 707130
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 707131
    if-eqz v1, :cond_5

    .line 707132
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 707133
    :cond_5
    if-eqz v7, :cond_6

    .line 707134
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p1

    move-wide v2, v10

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 707135
    :cond_6
    if-eqz v6, :cond_7

    .line 707136
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p1

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 707137
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :cond_8
    move-wide v10, v6

    move v7, v1

    move v6, v0

    move v1, v2

    move-wide v2, v8

    move-wide v8, v4

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 707138
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 707139
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 707140
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_0

    .line 707141
    const-string v2, "x"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707142
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 707143
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 707144
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 707145
    const-string v2, "y"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707146
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 707147
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 707148
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_2

    .line 707149
    const-string v2, "z"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707150
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 707151
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 707152
    return-void
.end method
