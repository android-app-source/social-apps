.class public LX/4Tw;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 720091
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 720092
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 720093
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 720094
    :goto_0
    return v1

    .line 720095
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 720096
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 720097
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 720098
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 720099
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 720100
    const-string v4, "nodes"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 720101
    invoke-static {p0, p1}, LX/2aD;->b(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 720102
    :cond_2
    const-string v4, "page_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 720103
    invoke-static {p0, p1}, LX/264;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 720104
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 720105
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 720106
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 720107
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 720080
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 720081
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 720082
    if-eqz v0, :cond_0

    .line 720083
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720084
    invoke-static {p0, v0, p2, p3}, LX/2aD;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 720085
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 720086
    if-eqz v0, :cond_1

    .line 720087
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720088
    invoke-static {p0, v0, p2}, LX/264;->a(LX/15i;ILX/0nX;)V

    .line 720089
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 720090
    return-void
.end method
