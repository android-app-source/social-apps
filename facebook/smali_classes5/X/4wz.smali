.class public final LX/4wz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TE;",
            "LX/4xL;",
            ">;>;"
        }
    .end annotation
.end field

.field public b:Ljava/util/Map$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map$Entry",
            "<TE;",
            "LX/4xL;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field public d:Z

.field public final synthetic e:LX/1Lz;


# direct methods
.method public constructor <init>(LX/1Lz;)V
    .locals 1

    .prologue
    .line 820660
    iput-object p1, p0, LX/4wz;->e:LX/1Lz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 820661
    iget-object v0, p1, LX/1Lz;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, LX/4wz;->a:Ljava/util/Iterator;

    .line 820662
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 820663
    iget v0, p0, LX/4wz;->c:I

    if-gtz v0, :cond_0

    iget-object v0, p0, LX/4wz;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 820664
    iget v0, p0, LX/4wz;->c:I

    if-nez v0, :cond_0

    .line 820665
    iget-object v0, p0, LX/4wz;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iput-object v0, p0, LX/4wz;->b:Ljava/util/Map$Entry;

    .line 820666
    iget-object v0, p0, LX/4wz;->b:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4xL;

    .line 820667
    iget v1, v0, LX/4xL;->value:I

    move v0, v1

    .line 820668
    iput v0, p0, LX/4wz;->c:I

    .line 820669
    :cond_0
    iget v0, p0, LX/4wz;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/4wz;->c:I

    .line 820670
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4wz;->d:Z

    .line 820671
    iget-object v0, p0, LX/4wz;->b:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 6

    .prologue
    .line 820672
    iget-boolean v0, p0, LX/4wz;->d:Z

    invoke-static {v0}, LX/0P6;->a(Z)V

    .line 820673
    iget-object v0, p0, LX/4wz;->b:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4xL;

    .line 820674
    iget v1, v0, LX/4xL;->value:I

    move v0, v1

    .line 820675
    if-gtz v0, :cond_0

    .line 820676
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 820677
    :cond_0
    iget-object v0, p0, LX/4wz;->b:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4xL;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, LX/4xL;->b(I)I

    move-result v0

    if-nez v0, :cond_1

    .line 820678
    iget-object v0, p0, LX/4wz;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 820679
    :cond_1
    iget-object v0, p0, LX/4wz;->e:LX/1Lz;

    .line 820680
    iget-wide v2, v0, LX/1Lz;->b:J

    const-wide/16 v4, 0x1

    sub-long v4, v2, v4

    iput-wide v4, v0, LX/1Lz;->b:J

    .line 820681
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4wz;->d:Z

    .line 820682
    return-void
.end method
