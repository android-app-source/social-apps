.class public LX/3u8;
.super LX/3u6;
.source ""

# interfaces
.implements LX/3u7;


# instance fields
.field private A:[LX/3uK;

.field private B:LX/3uK;

.field public C:Z

.field public D:I

.field public final E:Ljava/lang/Runnable;

.field private F:Z

.field public G:Z

.field private H:Landroid/graphics/Rect;

.field private I:Landroid/graphics/Rect;

.field public k:LX/3uV;

.field public l:Landroid/support/v7/internal/widget/ActionBarContextView;

.field public m:Landroid/widget/PopupWindow;

.field public n:Ljava/lang/Runnable;

.field private o:Landroid/support/v4/app/Fragment;

.field public p:LX/3vU;

.field private q:LX/3uF;

.field public r:LX/3uL;

.field private s:Z

.field public t:Landroid/view/ViewGroup;

.field private u:Landroid/view/ViewGroup;

.field private v:Landroid/widget/TextView;

.field private w:Landroid/view/View;

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/Window;LX/3u2;)V
    .locals 1

    .prologue
    .line 647741
    invoke-direct {p0, p1, p2, p3}, LX/3u6;-><init>(Landroid/content/Context;Landroid/view/Window;LX/3u2;)V

    .line 647742
    new-instance v0, Landroid/support/v7/app/AppCompatDelegateImplV7$1;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AppCompatDelegateImplV7$1;-><init>(LX/3u8;)V

    iput-object v0, p0, LX/3u8;->E:Ljava/lang/Runnable;

    .line 647743
    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/Fragment;LX/3uM;LX/3u2;)V
    .locals 1

    .prologue
    .line 647744
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, LX/3u6;-><init>(Landroid/content/Context;LX/3uM;LX/3u2;)V

    .line 647745
    new-instance v0, Landroid/support/v7/app/AppCompatDelegateImplV7$1;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AppCompatDelegateImplV7$1;-><init>(LX/3u8;)V

    iput-object v0, p0, LX/3u8;->E:Ljava/lang/Runnable;

    .line 647746
    iput-object p1, p0, LX/3u8;->o:Landroid/support/v4/app/Fragment;

    .line 647747
    return-void
.end method

.method public static a(LX/3u8;LX/3uK;Landroid/view/KeyEvent;)V
    .locals 10

    .prologue
    const/4 v1, -0x1

    const/4 v3, 0x0

    const/4 v9, 0x1

    const/4 v2, -0x2

    .line 647748
    iget-boolean v0, p1, LX/3uK;->o:Z

    if-nez v0, :cond_0

    .line 647749
    iget-boolean v0, p0, LX/3u6;->n:Z

    move v0, v0

    .line 647750
    if-eqz v0, :cond_1

    .line 647751
    :cond_0
    :goto_0
    return-void

    .line 647752
    :cond_1
    iget v0, p1, LX/3uK;->a:I

    if-nez v0, :cond_2

    .line 647753
    iget-object v4, p0, LX/3u6;->a:Landroid/content/Context;

    .line 647754
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 647755
    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    const/4 v5, 0x4

    if-ne v0, v5, :cond_3

    move v0, v9

    .line 647756
    :goto_1
    invoke-virtual {v4}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v5, 0xb

    if-lt v4, v5, :cond_4

    move v4, v9

    .line 647757
    :goto_2
    if-eqz v0, :cond_2

    if-nez v4, :cond_0

    .line 647758
    :cond_2
    invoke-virtual {p0}, LX/3u6;->j()Landroid/view/Window$Callback;

    move-result-object v0

    .line 647759
    if-eqz v0, :cond_5

    iget v4, p1, LX/3uK;->a:I

    iget-object v5, p1, LX/3uK;->j:LX/3v0;

    invoke-interface {v0, v4, v5}, Landroid/view/Window$Callback;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 647760
    invoke-static {p0, p1, v9}, LX/3u8;->a$redex0(LX/3u8;LX/3uK;Z)V

    goto :goto_0

    :cond_3
    move v0, v3

    .line 647761
    goto :goto_1

    :cond_4
    move v4, v3

    .line 647762
    goto :goto_2

    .line 647763
    :cond_5
    iget-object v0, p0, LX/3u6;->a:Landroid/content/Context;

    const-string v4, "window"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/view/WindowManager;

    .line 647764
    if-eqz v8, :cond_0

    .line 647765
    invoke-static {p0, p1, p2}, LX/3u8;->b(LX/3u8;LX/3uK;Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 647766
    iget-object v0, p1, LX/3uK;->g:Landroid/view/ViewGroup;

    if-eqz v0, :cond_6

    iget-boolean v0, p1, LX/3uK;->q:Z

    if-eqz v0, :cond_e

    .line 647767
    :cond_6
    iget-object v0, p1, LX/3uK;->g:Landroid/view/ViewGroup;

    if-nez v0, :cond_d

    .line 647768
    invoke-virtual {p0}, LX/3u6;->h()Landroid/content/Context;

    move-result-object v0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 647769
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 647770
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    .line 647771
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 647772
    const v5, 0x7f010010

    invoke-virtual {v4, v5, v1, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 647773
    iget v5, v1, Landroid/util/TypedValue;->resourceId:I

    if-eqz v5, :cond_7

    .line 647774
    iget v5, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 647775
    :cond_7
    const v5, 0x7f01004f

    invoke-virtual {v4, v5, v1, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 647776
    iget v5, v1, Landroid/util/TypedValue;->resourceId:I

    if-eqz v5, :cond_11

    .line 647777
    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v4, v1, v6}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 647778
    :goto_3
    new-instance v1, LX/3uc;

    invoke-direct {v1, v0, v7}, LX/3uc;-><init>(Landroid/content/Context;I)V

    .line 647779
    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 647780
    iput-object v1, p1, LX/3uK;->l:Landroid/content/Context;

    .line 647781
    sget-object v4, LX/03r;->Theme:[I

    invoke-virtual {v1, v4}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 647782
    const/16 v4, 0x4c

    invoke-virtual {v1, v4, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    iput v4, p1, LX/3uK;->b:I

    .line 647783
    const/16 v4, 0x1

    invoke-virtual {v1, v4, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    iput v4, p1, LX/3uK;->f:I

    .line 647784
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 647785
    new-instance v0, LX/3uI;

    iget-object v1, p1, LX/3uK;->l:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, LX/3uI;-><init>(LX/3u8;Landroid/content/Context;)V

    iput-object v0, p1, LX/3uK;->g:Landroid/view/ViewGroup;

    .line 647786
    const/16 v0, 0x51

    iput v0, p1, LX/3uK;->c:I

    .line 647787
    const/4 v0, 0x1

    move v0, v0

    .line 647788
    if-eqz v0, :cond_0

    iget-object v0, p1, LX/3uK;->g:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 647789
    :cond_8
    :goto_4
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 647790
    iget-object v0, p1, LX/3uK;->i:Landroid/view/View;

    if-eqz v0, :cond_12

    .line 647791
    iget-object v0, p1, LX/3uK;->i:Landroid/view/View;

    iput-object v0, p1, LX/3uK;->h:Landroid/view/View;

    move v0, v1

    .line 647792
    :goto_5
    move v0, v0

    .line 647793
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 647794
    iget-object v4, p1, LX/3uK;->h:Landroid/view/View;

    if-nez v4, :cond_16

    .line 647795
    :cond_9
    :goto_6
    move v0, v0

    .line 647796
    if-eqz v0, :cond_0

    .line 647797
    iget-object v0, p1, LX/3uK;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 647798
    if-nez v0, :cond_10

    .line 647799
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    move-object v1, v0

    .line 647800
    :goto_7
    iget v0, p1, LX/3uK;->b:I

    .line 647801
    iget-object v4, p1, LX/3uK;->g:Landroid/view/ViewGroup;

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 647802
    iget-object v0, p1, LX/3uK;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 647803
    if-eqz v0, :cond_a

    instance-of v4, v0, Landroid/view/ViewGroup;

    if-eqz v4, :cond_a

    .line 647804
    check-cast v0, Landroid/view/ViewGroup;

    iget-object v4, p1, LX/3uK;->h:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 647805
    :cond_a
    iget-object v0, p1, LX/3uK;->g:Landroid/view/ViewGroup;

    iget-object v4, p1, LX/3uK;->h:Landroid/view/View;

    invoke-virtual {v0, v4, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 647806
    iget-object v0, p1, LX/3uK;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_b

    .line 647807
    iget-object v0, p1, LX/3uK;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_b
    move v1, v2

    .line 647808
    :cond_c
    :goto_8
    iput-boolean v3, p1, LX/3uK;->n:Z

    .line 647809
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    iget v3, p1, LX/3uK;->d:I

    iget v4, p1, LX/3uK;->e:I

    const/16 v5, 0x3ea

    const/high16 v6, 0x820000

    const/4 v7, -0x3

    invoke-direct/range {v0 .. v7}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIIIII)V

    .line 647810
    iget v1, p1, LX/3uK;->c:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 647811
    iget v1, p1, LX/3uK;->f:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 647812
    iget-object v1, p1, LX/3uK;->g:Landroid/view/ViewGroup;

    invoke-interface {v8, v1, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 647813
    iput-boolean v9, p1, LX/3uK;->o:Z

    goto/16 :goto_0

    .line 647814
    :cond_d
    iget-boolean v0, p1, LX/3uK;->q:Z

    if-eqz v0, :cond_8

    iget-object v0, p1, LX/3uK;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_8

    .line 647815
    iget-object v0, p1, LX/3uK;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    goto/16 :goto_4

    .line 647816
    :cond_e
    iget-object v0, p1, LX/3uK;->i:Landroid/view/View;

    if-eqz v0, :cond_f

    .line 647817
    iget-object v0, p1, LX/3uK;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 647818
    if-eqz v0, :cond_f

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-eq v0, v1, :cond_c

    :cond_f
    move v1, v2

    goto :goto_8

    :cond_10
    move-object v1, v0

    goto :goto_7

    .line 647819
    :cond_11
    const v1, 0x7f0e00d2

    invoke-virtual {v4, v1, v6}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    goto/16 :goto_3

    .line 647820
    :cond_12
    iget-object v0, p1, LX/3uK;->j:LX/3v0;

    if-nez v0, :cond_13

    move v0, v4

    .line 647821
    goto/16 :goto_5

    .line 647822
    :cond_13
    iget-object v0, p0, LX/3u8;->r:LX/3uL;

    if-nez v0, :cond_14

    .line 647823
    new-instance v0, LX/3uL;

    invoke-direct {v0, p0}, LX/3uL;-><init>(LX/3u8;)V

    iput-object v0, p0, LX/3u8;->r:LX/3uL;

    .line 647824
    :cond_14
    iget-object v0, p0, LX/3u8;->r:LX/3uL;

    invoke-virtual {p1, v0}, LX/3uK;->a(LX/3uE;)LX/3ux;

    move-result-object v0

    .line 647825
    check-cast v0, Landroid/view/View;

    iput-object v0, p1, LX/3uK;->h:Landroid/view/View;

    .line 647826
    iget-object v0, p1, LX/3uK;->h:Landroid/view/View;

    if-eqz v0, :cond_15

    move v0, v1

    goto/16 :goto_5

    :cond_15
    move v0, v4

    goto/16 :goto_5

    .line 647827
    :cond_16
    iget-object v4, p1, LX/3uK;->i:Landroid/view/View;

    if-eqz v4, :cond_17

    move v0, v1

    goto/16 :goto_6

    .line 647828
    :cond_17
    iget-object v4, p1, LX/3uK;->k:LX/3uz;

    invoke-virtual {v4}, LX/3uz;->a()Landroid/widget/ListAdapter;

    move-result-object v4

    invoke-interface {v4}, Landroid/widget/ListAdapter;->getCount()I

    move-result v4

    if-lez v4, :cond_9

    move v0, v1

    goto/16 :goto_6
.end method

.method private a(LX/3uK;ILandroid/view/KeyEvent;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 647829
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 647830
    :cond_0
    :goto_0
    return v0

    .line 647831
    :cond_1
    iget-boolean v1, p1, LX/3uK;->m:Z

    if-nez v1, :cond_2

    invoke-static {p0, p1, p3}, LX/3u8;->b(LX/3u8;LX/3uK;Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p1, LX/3uK;->j:LX/3v0;

    if-eqz v1, :cond_3

    .line 647832
    iget-object v0, p1, LX/3uK;->j:LX/3v0;

    invoke-virtual {v0, p2, p3, p4}, LX/3v0;->performShortcut(ILandroid/view/KeyEvent;I)Z

    move-result v0

    .line 647833
    :cond_3
    if-eqz v0, :cond_0

    .line 647834
    and-int/lit8 v1, p4, 0x1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/3u8;->p:LX/3vU;

    if-nez v1, :cond_0

    .line 647835
    const/4 v1, 0x1

    invoke-static {p0, p1, v1}, LX/3u8;->a$redex0(LX/3u8;LX/3uK;Z)V

    goto :goto_0
.end method

.method public static a$redex0(LX/3u8;Landroid/view/Menu;)LX/3uK;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 647665
    iget-object v3, p0, LX/3u8;->A:[LX/3uK;

    .line 647666
    if-eqz v3, :cond_0

    array-length v0, v3

    :goto_0
    move v2, v1

    .line 647667
    :goto_1
    if-ge v2, v0, :cond_2

    .line 647668
    aget-object v1, v3, v2

    .line 647669
    if-eqz v1, :cond_1

    iget-object v4, v1, LX/3uK;->j:LX/3v0;

    if-ne v4, p1, :cond_1

    move-object v0, v1

    .line 647670
    :goto_2
    return-object v0

    :cond_0
    move v0, v1

    .line 647671
    goto :goto_0

    .line 647672
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 647673
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static a$redex0(LX/3u8;ILX/3uK;Landroid/view/Menu;)V
    .locals 1

    .prologue
    .line 647836
    if-nez p3, :cond_1

    .line 647837
    if-nez p2, :cond_0

    .line 647838
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/3u8;->A:[LX/3uK;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 647839
    iget-object v0, p0, LX/3u8;->A:[LX/3uK;

    aget-object p2, v0, p1

    .line 647840
    :cond_0
    if-eqz p2, :cond_1

    .line 647841
    iget-object p3, p2, LX/3uK;->j:LX/3v0;

    .line 647842
    :cond_1
    if-eqz p2, :cond_3

    iget-boolean v0, p2, LX/3uK;->o:Z

    if-nez v0, :cond_3

    .line 647843
    :cond_2
    :goto_0
    return-void

    .line 647844
    :cond_3
    invoke-virtual {p0}, LX/3u6;->j()Landroid/view/Window$Callback;

    move-result-object v0

    .line 647845
    if-eqz v0, :cond_2

    .line 647846
    invoke-interface {v0, p1, p3}, Landroid/view/Window$Callback;->onPanelClosed(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/3u8;LX/3uK;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 647847
    if-eqz p2, :cond_1

    iget v0, p1, LX/3uK;->a:I

    if-nez v0, :cond_1

    iget-object v0, p0, LX/3u8;->p:LX/3vU;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3u8;->p:LX/3vU;

    invoke-interface {v0}, LX/3vU;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 647848
    iget-object v0, p1, LX/3uK;->j:LX/3v0;

    invoke-static {p0, v0}, LX/3u8;->b(LX/3u8;LX/3v0;)V

    .line 647849
    :cond_0
    :goto_0
    return-void

    .line 647850
    :cond_1
    iget-boolean v1, p1, LX/3uK;->o:Z

    .line 647851
    iget-object v0, p0, LX/3u6;->a:Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 647852
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    iget-object v2, p1, LX/3uK;->g:Landroid/view/ViewGroup;

    if-eqz v2, :cond_2

    .line 647853
    iget-object v2, p1, LX/3uK;->g:Landroid/view/ViewGroup;

    invoke-interface {v0, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 647854
    :cond_2
    iput-boolean v3, p1, LX/3uK;->m:Z

    .line 647855
    iput-boolean v3, p1, LX/3uK;->n:Z

    .line 647856
    iput-boolean v3, p1, LX/3uK;->o:Z

    .line 647857
    if-eqz v1, :cond_3

    if-eqz p2, :cond_3

    .line 647858
    iget v0, p1, LX/3uK;->a:I

    invoke-static {p0, v0, p1, v4}, LX/3u8;->a$redex0(LX/3u8;ILX/3uK;Landroid/view/Menu;)V

    .line 647859
    :cond_3
    iput-object v4, p1, LX/3uK;->h:Landroid/view/View;

    .line 647860
    const/4 v0, 0x1

    iput-boolean v0, p1, LX/3uK;->q:Z

    .line 647861
    iget-object v0, p0, LX/3u8;->B:LX/3uK;

    if-ne v0, p1, :cond_0

    .line 647862
    iput-object v4, p0, LX/3u8;->B:LX/3uK;

    goto :goto_0
.end method

.method public static b(LX/3u8;LX/3v0;)V
    .locals 2

    .prologue
    .line 647863
    iget-boolean v0, p0, LX/3u8;->z:Z

    if-eqz v0, :cond_0

    .line 647864
    :goto_0
    return-void

    .line 647865
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3u8;->z:Z

    .line 647866
    iget-object v0, p0, LX/3u8;->p:LX/3vU;

    invoke-interface {v0}, LX/3vU;->h()V

    .line 647867
    invoke-virtual {p0}, LX/3u6;->j()Landroid/view/Window$Callback;

    move-result-object v0

    .line 647868
    if-eqz v0, :cond_1

    .line 647869
    iget-boolean v1, p0, LX/3u6;->n:Z

    move v1, v1

    .line 647870
    if-nez v1, :cond_1

    .line 647871
    const/16 v1, 0x8

    invoke-interface {v0, v1, p1}, Landroid/view/Window$Callback;->onPanelClosed(ILandroid/view/Menu;)V

    .line 647872
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3u8;->z:Z

    goto :goto_0
.end method

.method public static b(LX/3u8;LX/3uK;Landroid/view/KeyEvent;)Z
    .locals 11

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 647873
    iget-boolean v0, p0, LX/3u6;->n:Z

    move v0, v0

    .line 647874
    if-eqz v0, :cond_1

    .line 647875
    :cond_0
    :goto_0
    return v2

    .line 647876
    :cond_1
    iget-boolean v0, p1, LX/3uK;->m:Z

    if-eqz v0, :cond_2

    move v2, v1

    .line 647877
    goto :goto_0

    .line 647878
    :cond_2
    iget-object v0, p0, LX/3u8;->B:LX/3uK;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/3u8;->B:LX/3uK;

    if-eq v0, p1, :cond_3

    .line 647879
    iget-object v0, p0, LX/3u8;->B:LX/3uK;

    invoke-static {p0, v0, v2}, LX/3u8;->a$redex0(LX/3u8;LX/3uK;Z)V

    .line 647880
    :cond_3
    invoke-virtual {p0}, LX/3u6;->j()Landroid/view/Window$Callback;

    move-result-object v3

    .line 647881
    if-eqz v3, :cond_4

    .line 647882
    iget v0, p1, LX/3uK;->a:I

    invoke-interface {v3, v0}, Landroid/view/Window$Callback;->onCreatePanelView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, LX/3uK;->i:Landroid/view/View;

    .line 647883
    :cond_4
    iget v0, p1, LX/3uK;->a:I

    if-eqz v0, :cond_5

    iget v0, p1, LX/3uK;->a:I

    const/16 v4, 0x8

    if-ne v0, v4, :cond_e

    :cond_5
    move v0, v1

    .line 647884
    :goto_1
    if-eqz v0, :cond_6

    iget-object v4, p0, LX/3u8;->p:LX/3vU;

    if-eqz v4, :cond_6

    .line 647885
    iget-object v4, p0, LX/3u8;->p:LX/3vU;

    invoke-interface {v4}, LX/3vU;->g()V

    .line 647886
    :cond_6
    iget-object v4, p1, LX/3uK;->i:Landroid/view/View;

    if-nez v4, :cond_14

    .line 647887
    iget-object v4, p1, LX/3uK;->j:LX/3v0;

    if-eqz v4, :cond_7

    iget-boolean v4, p1, LX/3uK;->r:Z

    if-eqz v4, :cond_10

    .line 647888
    :cond_7
    iget-object v4, p1, LX/3uK;->j:LX/3v0;

    if-nez v4, :cond_b

    .line 647889
    const/4 v10, 0x1

    .line 647890
    iget-object v5, p0, LX/3u6;->a:Landroid/content/Context;

    .line 647891
    iget v4, p1, LX/3uK;->a:I

    if-eqz v4, :cond_8

    iget v4, p1, LX/3uK;->a:I

    const/16 v6, 0x8

    if-ne v4, v6, :cond_18

    :cond_8
    iget-object v4, p0, LX/3u8;->p:LX/3vU;

    if-eqz v4, :cond_18

    .line 647892
    new-instance v6, Landroid/util/TypedValue;

    invoke-direct {v6}, Landroid/util/TypedValue;-><init>()V

    .line 647893
    invoke-virtual {v5}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v8

    .line 647894
    const v4, 0x7f010013

    invoke-virtual {v8, v4, v6, v10}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 647895
    const/4 v4, 0x0

    .line 647896
    iget v9, v6, Landroid/util/TypedValue;->resourceId:I

    if-eqz v9, :cond_17

    .line 647897
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    .line 647898
    invoke-virtual {v4, v8}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 647899
    iget v9, v6, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v4, v9, v10}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 647900
    const v9, 0x7f010014

    invoke-virtual {v4, v9, v6, v10}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 647901
    :goto_2
    iget v9, v6, Landroid/util/TypedValue;->resourceId:I

    if-eqz v9, :cond_a

    .line 647902
    if-nez v4, :cond_9

    .line 647903
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    .line 647904
    invoke-virtual {v4, v8}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 647905
    :cond_9
    iget v6, v6, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v4, v6, v10}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    :cond_a
    move-object v6, v4

    .line 647906
    if-eqz v6, :cond_18

    .line 647907
    new-instance v4, LX/3uc;

    const/4 v8, 0x0

    invoke-direct {v4, v5, v8}, LX/3uc;-><init>(Landroid/content/Context;I)V

    .line 647908
    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 647909
    :goto_3
    new-instance v5, LX/3v0;

    invoke-direct {v5, v4}, LX/3v0;-><init>(Landroid/content/Context;)V

    .line 647910
    invoke-virtual {v5, p0}, LX/3v0;->a(LX/3u7;)V

    .line 647911
    invoke-virtual {p1, v5}, LX/3uK;->a(LX/3v0;)V

    .line 647912
    move v4, v10

    .line 647913
    if-eqz v4, :cond_0

    iget-object v4, p1, LX/3uK;->j:LX/3v0;

    if-eqz v4, :cond_0

    .line 647914
    :cond_b
    if-eqz v0, :cond_d

    iget-object v4, p0, LX/3u8;->p:LX/3vU;

    if-eqz v4, :cond_d

    .line 647915
    iget-object v4, p0, LX/3u8;->q:LX/3uF;

    if-nez v4, :cond_c

    .line 647916
    new-instance v4, LX/3uF;

    invoke-direct {v4, p0}, LX/3uF;-><init>(LX/3u8;)V

    iput-object v4, p0, LX/3u8;->q:LX/3uF;

    .line 647917
    :cond_c
    iget-object v4, p0, LX/3u8;->p:LX/3vU;

    iget-object v5, p1, LX/3uK;->j:LX/3v0;

    iget-object v6, p0, LX/3u8;->q:LX/3uF;

    invoke-interface {v4, v5, v6}, LX/3vU;->a(Landroid/view/Menu;LX/3uE;)V

    .line 647918
    :cond_d
    iget-object v4, p1, LX/3uK;->j:LX/3v0;

    invoke-virtual {v4}, LX/3v0;->f()V

    .line 647919
    iget v4, p1, LX/3uK;->a:I

    iget-object v5, p1, LX/3uK;->j:LX/3v0;

    invoke-interface {v3, v4, v5}, Landroid/view/Window$Callback;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v4

    if-nez v4, :cond_f

    .line 647920
    invoke-virtual {p1, v7}, LX/3uK;->a(LX/3v0;)V

    .line 647921
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3u8;->p:LX/3vU;

    if-eqz v0, :cond_0

    .line 647922
    iget-object v0, p0, LX/3u8;->p:LX/3vU;

    iget-object v1, p0, LX/3u8;->q:LX/3uF;

    invoke-interface {v0, v7, v1}, LX/3vU;->a(Landroid/view/Menu;LX/3uE;)V

    goto/16 :goto_0

    :cond_e
    move v0, v2

    .line 647923
    goto/16 :goto_1

    .line 647924
    :cond_f
    iput-boolean v2, p1, LX/3uK;->r:Z

    .line 647925
    :cond_10
    iget-object v4, p1, LX/3uK;->j:LX/3v0;

    invoke-virtual {v4}, LX/3v0;->f()V

    .line 647926
    iget-object v4, p1, LX/3uK;->s:Landroid/os/Bundle;

    if-eqz v4, :cond_11

    .line 647927
    iget-object v4, p1, LX/3uK;->j:LX/3v0;

    iget-object v5, p1, LX/3uK;->s:Landroid/os/Bundle;

    invoke-virtual {v4, v5}, LX/3v0;->b(Landroid/os/Bundle;)V

    .line 647928
    iput-object v7, p1, LX/3uK;->s:Landroid/os/Bundle;

    .line 647929
    :cond_11
    iget-object v4, p1, LX/3uK;->i:Landroid/view/View;

    iget-object v5, p1, LX/3uK;->j:LX/3v0;

    invoke-interface {v3, v2, v4, v5}, Landroid/view/Window$Callback;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v3

    if-nez v3, :cond_13

    .line 647930
    if-eqz v0, :cond_12

    iget-object v0, p0, LX/3u8;->p:LX/3vU;

    if-eqz v0, :cond_12

    .line 647931
    iget-object v0, p0, LX/3u8;->p:LX/3vU;

    iget-object v1, p0, LX/3u8;->q:LX/3uF;

    invoke-interface {v0, v7, v1}, LX/3vU;->a(Landroid/view/Menu;LX/3uE;)V

    .line 647932
    :cond_12
    iget-object v0, p1, LX/3uK;->j:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->g()V

    goto/16 :goto_0

    .line 647933
    :cond_13
    if-eqz p2, :cond_15

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDeviceId()I

    move-result v0

    :goto_4
    invoke-static {v0}, Landroid/view/KeyCharacterMap;->load(I)Landroid/view/KeyCharacterMap;

    move-result-object v0

    .line 647934
    invoke-virtual {v0}, Landroid/view/KeyCharacterMap;->getKeyboardType()I

    move-result v0

    if-eq v0, v1, :cond_16

    move v0, v1

    :goto_5
    iput-boolean v0, p1, LX/3uK;->p:Z

    .line 647935
    iget-object v0, p1, LX/3uK;->j:LX/3v0;

    iget-boolean v3, p1, LX/3uK;->p:Z

    invoke-virtual {v0, v3}, LX/3v0;->setQwertyMode(Z)V

    .line 647936
    iget-object v0, p1, LX/3uK;->j:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->g()V

    .line 647937
    :cond_14
    iput-boolean v1, p1, LX/3uK;->m:Z

    .line 647938
    iput-boolean v2, p1, LX/3uK;->n:Z

    .line 647939
    iput-object p1, p0, LX/3u8;->B:LX/3uK;

    move v2, v1

    .line 647940
    goto/16 :goto_0

    .line 647941
    :cond_15
    const/4 v0, -0x1

    goto :goto_4

    :cond_16
    move v0, v2

    .line 647942
    goto :goto_5

    .line 647943
    :cond_17
    const v9, 0x7f010014

    invoke-virtual {v8, v9, v6, v10}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    goto/16 :goto_2

    :cond_18
    move-object v4, v5

    goto/16 :goto_3
.end method

.method public static f(LX/3u8;I)LX/3uK;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 648137
    iget-object v0, p0, LX/3u8;->A:[LX/3uK;

    if-eqz v0, :cond_0

    array-length v1, v0

    if-gt v1, p1, :cond_2

    .line 648138
    :cond_0
    add-int/lit8 v1, p1, 0x1

    new-array v1, v1, [LX/3uK;

    .line 648139
    if-eqz v0, :cond_1

    .line 648140
    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 648141
    :cond_1
    iput-object v1, p0, LX/3u8;->A:[LX/3uK;

    move-object v0, v1

    .line 648142
    :cond_2
    aget-object v1, v0, p1

    .line 648143
    if-nez v1, :cond_3

    .line 648144
    new-instance v1, LX/3uK;

    invoke-direct {v1, p1}, LX/3uK;-><init>(I)V

    aput-object v1, v0, p1

    move-object v0, v1

    .line 648145
    :goto_0
    return-object v0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method private g(I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 647944
    iget v0, p0, LX/3u8;->D:I

    shl-int v1, v2, p1

    or-int/2addr v0, v1

    iput v0, p0, LX/3u8;->D:I

    .line 647945
    iget-boolean v0, p0, LX/3u8;->C:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3u8;->t:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 647946
    iget-object v0, p0, LX/3u8;->t:Landroid/view/ViewGroup;

    iget-object v1, p0, LX/3u8;->E:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/0vv;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 647947
    iput-boolean v2, p0, LX/3u8;->C:Z

    .line 647948
    :cond_0
    return-void
.end method

.method public static h(LX/3u8;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 647949
    invoke-static {p0, p1}, LX/3u8;->f(LX/3u8;I)LX/3uK;

    move-result-object v0

    .line 647950
    iget-object v1, v0, LX/3uK;->j:LX/3v0;

    if-eqz v1, :cond_1

    .line 647951
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 647952
    iget-object v2, v0, LX/3uK;->j:LX/3v0;

    invoke-virtual {v2, v1}, LX/3v0;->a(Landroid/os/Bundle;)V

    .line 647953
    invoke-virtual {v1}, Landroid/os/Bundle;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 647954
    iput-object v1, v0, LX/3uK;->s:Landroid/os/Bundle;

    .line 647955
    :cond_0
    iget-object v1, v0, LX/3uK;->j:LX/3v0;

    invoke-virtual {v1}, LX/3v0;->f()V

    .line 647956
    iget-object v1, v0, LX/3uK;->j:LX/3v0;

    invoke-virtual {v1}, LX/3v0;->clear()V

    .line 647957
    :cond_1
    iput-boolean v4, v0, LX/3uK;->r:Z

    .line 647958
    iput-boolean v4, v0, LX/3uK;->q:Z

    .line 647959
    const/16 v0, 0x8

    if-eq p1, v0, :cond_2

    if-nez p1, :cond_3

    :cond_2
    iget-object v0, p0, LX/3u8;->p:LX/3vU;

    if-eqz v0, :cond_3

    .line 647960
    invoke-static {p0, v3}, LX/3u8;->f(LX/3u8;I)LX/3uK;

    move-result-object v0

    .line 647961
    if-eqz v0, :cond_3

    .line 647962
    iput-boolean v3, v0, LX/3uK;->m:Z

    .line 647963
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/3u8;->b(LX/3u8;LX/3uK;Landroid/view/KeyEvent;)Z

    .line 647964
    :cond_3
    return-void
.end method

.method public static i(LX/3u8;I)I
    .locals 8

    .prologue
    const/4 v6, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 647965
    iget-object v0, p0, LX/3u8;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_c

    .line 647966
    iget-object v0, p0, LX/3u8;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_c

    .line 647967
    iget-object v0, p0, LX/3u8;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 647968
    iget-object v1, p0, LX/3u8;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarContextView;->isShown()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 647969
    iget-object v1, p0, LX/3u8;->H:Landroid/graphics/Rect;

    if-nez v1, :cond_0

    .line 647970
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, LX/3u8;->H:Landroid/graphics/Rect;

    .line 647971
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, LX/3u8;->I:Landroid/graphics/Rect;

    .line 647972
    :cond_0
    iget-object v1, p0, LX/3u8;->H:Landroid/graphics/Rect;

    .line 647973
    iget-object v4, p0, LX/3u8;->I:Landroid/graphics/Rect;

    .line 647974
    invoke-virtual {v1, v2, p1, v2, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 647975
    iget-object v5, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    invoke-static {v5, v1, v4}, LX/3wJ;->a(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 647976
    iget v1, v4, Landroid/graphics/Rect;->top:I

    if-nez v1, :cond_4

    move v1, p1

    .line 647977
    :goto_0
    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-eq v4, v1, :cond_b

    .line 647978
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 647979
    iget-object v1, p0, LX/3u8;->w:Landroid/view/View;

    if-nez v1, :cond_5

    .line 647980
    new-instance v1, Landroid/view/View;

    iget-object v4, p0, LX/3u6;->a:Landroid/content/Context;

    invoke-direct {v1, v4}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/3u8;->w:Landroid/view/View;

    .line 647981
    iget-object v1, p0, LX/3u8;->w:Landroid/view/View;

    iget-object v4, p0, LX/3u6;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0003

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 647982
    iget-object v1, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    iget-object v4, p0, LX/3u8;->w:Landroid/view/View;

    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v5, v6, p1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v4, v6, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    move v1, v3

    .line 647983
    :goto_1
    iget-object v4, p0, LX/3u8;->w:Landroid/view/View;

    if-eqz v4, :cond_7

    .line 647984
    :goto_2
    iget-boolean v4, p0, LX/3u6;->h:Z

    if-nez v4, :cond_1

    if-eqz v3, :cond_1

    move p1, v2

    :cond_1
    move v7, v1

    move v1, v3

    move v3, v7

    .line 647985
    :goto_3
    if-eqz v3, :cond_2

    .line 647986
    iget-object v3, p0, LX/3u8;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v3, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    move v0, v1

    .line 647987
    :goto_4
    iget-object v1, p0, LX/3u8;->w:Landroid/view/View;

    if-eqz v1, :cond_3

    .line 647988
    iget-object v1, p0, LX/3u8;->w:Landroid/view/View;

    if-eqz v0, :cond_9

    :goto_5
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 647989
    :cond_3
    return p1

    :cond_4
    move v1, v2

    .line 647990
    goto :goto_0

    .line 647991
    :cond_5
    iget-object v1, p0, LX/3u8;->w:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 647992
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v4, p1, :cond_6

    .line 647993
    iput p1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 647994
    iget-object v4, p0, LX/3u8;->w:Landroid/view/View;

    invoke-virtual {v4, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_6
    move v1, v3

    goto :goto_1

    :cond_7
    move v3, v2

    .line 647995
    goto :goto_2

    .line 647996
    :cond_8
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-eqz v1, :cond_a

    .line 647997
    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move v1, v2

    goto :goto_3

    .line 647998
    :cond_9
    const/16 v2, 0x8

    goto :goto_5

    :cond_a
    move v3, v2

    move v1, v2

    goto :goto_3

    :cond_b
    move v1, v2

    goto :goto_1

    :cond_c
    move v0, v2

    goto :goto_4
.end method

.method private l()V
    .locals 13

    .prologue
    const v7, 0x1020002

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 647999
    iget-boolean v0, p0, LX/3u8;->s:Z

    if-nez v0, :cond_13

    .line 648000
    iget-object v0, p0, LX/3u6;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 648001
    iget-boolean v1, p0, LX/3u6;->j:Z

    if-nez v1, :cond_5

    .line 648002
    iget-boolean v1, p0, LX/3u6;->i:Z

    if-eqz v1, :cond_1

    .line 648003
    const v1, 0x7f030009

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    .line 648004
    :cond_0
    :goto_0
    iget-object v0, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    if-nez v0, :cond_8

    .line 648005
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "AppCompat does not support the current theme features"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 648006
    :cond_1
    iget-boolean v0, p0, LX/3u6;->f:Z

    if-eqz v0, :cond_0

    .line 648007
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 648008
    iget-object v0, p0, LX/3u6;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v2, 0x7f010013

    invoke-virtual {v0, v2, v1, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 648009
    iget v0, v1, Landroid/util/TypedValue;->resourceId:I

    if-eqz v0, :cond_4

    .line 648010
    new-instance v0, LX/3uc;

    iget-object v2, p0, LX/3u6;->a:Landroid/content/Context;

    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-direct {v0, v2, v1}, LX/3uc;-><init>(Landroid/content/Context;I)V

    .line 648011
    :goto_1
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030013

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    .line 648012
    iget-object v0, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    const v1, 0x7f0d0349

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/3vU;

    iput-object v0, p0, LX/3u8;->p:LX/3vU;

    .line 648013
    iget-object v0, p0, LX/3u8;->p:LX/3vU;

    invoke-virtual {p0}, LX/3u6;->j()Landroid/view/Window$Callback;

    move-result-object v1

    invoke-interface {v0, v1}, LX/3vU;->setWindowCallback(Landroid/view/Window$Callback;)V

    .line 648014
    iget-boolean v0, p0, LX/3u6;->g:Z

    if-eqz v0, :cond_2

    .line 648015
    iget-object v0, p0, LX/3u8;->p:LX/3vU;

    const/16 v1, 0x9

    invoke-interface {v0, v1}, LX/3vU;->a(I)V

    .line 648016
    :cond_2
    iget-boolean v0, p0, LX/3u8;->x:Z

    if-eqz v0, :cond_3

    .line 648017
    iget-object v0, p0, LX/3u8;->p:LX/3vU;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, LX/3vU;->a(I)V

    .line 648018
    :cond_3
    iget-boolean v0, p0, LX/3u8;->y:Z

    if-eqz v0, :cond_0

    .line 648019
    iget-object v0, p0, LX/3u8;->p:LX/3vU;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, LX/3vU;->a(I)V

    goto :goto_0

    .line 648020
    :cond_4
    iget-object v0, p0, LX/3u6;->a:Landroid/content/Context;

    goto :goto_1

    .line 648021
    :cond_5
    iget-boolean v1, p0, LX/3u6;->h:Z

    if-eqz v1, :cond_6

    .line 648022
    const v1, 0x7f030012

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    .line 648023
    :goto_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_7

    .line 648024
    iget-object v0, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    new-instance v1, LX/3uB;

    invoke-direct {v1, p0}, LX/3uB;-><init>(LX/3u8;)V

    invoke-static {v0, v1}, LX/0vv;->a(Landroid/view/View;LX/1uR;)V

    goto/16 :goto_0

    .line 648025
    :cond_6
    const v1, 0x7f030011

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    goto :goto_2

    .line 648026
    :cond_7
    iget-object v0, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    check-cast v0, LX/3vl;

    new-instance v1, LX/3uD;

    invoke-direct {v1, p0}, LX/3uD;-><init>(LX/3u8;)V

    invoke-interface {v0, v1}, LX/3vl;->setOnFitSystemWindowsListener(LX/3uC;)V

    goto/16 :goto_0

    .line 648027
    :cond_8
    iget-object v0, p0, LX/3u8;->p:LX/3vU;

    if-nez v0, :cond_9

    .line 648028
    iget-object v0, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    const v1, 0x7f0d02c4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3u8;->v:Landroid/widget/TextView;

    .line 648029
    :cond_9
    iget-object v0, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    .line 648030
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_b

    .line 648031
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "makeOptionalFitsSystemWindows"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 648032
    invoke-virtual {v1}, Ljava/lang/reflect/Method;->isAccessible()Z

    move-result v2

    if-nez v2, :cond_a

    .line 648033
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 648034
    :cond_a
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 648035
    :cond_b
    :goto_3
    iget-object v0, p0, LX/3u6;->b:LX/3uM;

    invoke-interface {v0, v7}, LX/3uM;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 648036
    iget-object v1, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    const v2, 0x7f0d0003

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 648037
    :goto_4
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-lez v2, :cond_c

    .line 648038
    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 648039
    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 648040
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_4

    .line 648041
    :cond_c
    iget-object v2, p0, LX/3u6;->b:LX/3uM;

    iget-object v3, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    invoke-interface {v2, v3}, LX/3uM;->a(Landroid/view/View;)V

    .line 648042
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setId(I)V

    .line 648043
    invoke-virtual {v1, v7}, Landroid/view/ViewGroup;->setId(I)V

    .line 648044
    instance-of v1, v0, Landroid/widget/FrameLayout;

    if-eqz v1, :cond_d

    .line 648045
    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 648046
    :cond_d
    invoke-virtual {p0}, LX/3u6;->k()Ljava/lang/CharSequence;

    move-result-object v0

    .line 648047
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 648048
    invoke-virtual {p0, v0}, LX/3u8;->a(Ljava/lang/CharSequence;)V

    .line 648049
    :cond_e
    const/4 v12, 0x6

    const/4 v11, 0x5

    const/4 v1, 0x0

    const/4 v4, -0x1

    .line 648050
    iget-object v0, p0, LX/3u6;->a:Landroid/content/Context;

    sget-object v2, LX/03r;->Theme:[I

    invoke-virtual {v0, v2}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v8

    .line 648051
    const/16 v0, 0x6

    invoke-virtual {v8, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 648052
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 648053
    const/16 v2, 0x6

    invoke-virtual {v8, v2, v0}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 648054
    :goto_5
    const/16 v2, 0x8

    invoke-virtual {v8, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 648055
    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    .line 648056
    const/16 v3, 0x8

    invoke-virtual {v8, v3, v2}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 648057
    :goto_6
    const/16 v3, 0x9

    invoke-virtual {v8, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 648058
    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    .line 648059
    const/16 v7, 0x9

    invoke-virtual {v8, v7, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 648060
    :goto_7
    const/16 v7, 0x7

    invoke-virtual {v8, v7}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 648061
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 648062
    const/16 v7, 0x7

    invoke-virtual {v8, v7, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 648063
    :cond_f
    iget-object v7, p0, LX/3u6;->a:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    .line 648064
    iget v7, v9, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v10, v9, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v7, v10, :cond_14

    const/4 v7, 0x1

    .line 648065
    :goto_8
    if-eqz v7, :cond_15

    .line 648066
    :goto_9
    if-eqz v2, :cond_1a

    iget v0, v2, Landroid/util/TypedValue;->type:I

    if-eqz v0, :cond_1a

    .line 648067
    iget v0, v2, Landroid/util/TypedValue;->type:I

    if-ne v0, v11, :cond_16

    .line 648068
    invoke-virtual {v2, v9}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    move v2, v0

    .line 648069
    :goto_a
    if-eqz v7, :cond_17

    .line 648070
    :goto_b
    if-eqz v3, :cond_19

    iget v0, v3, Landroid/util/TypedValue;->type:I

    if-eqz v0, :cond_19

    .line 648071
    iget v0, v3, Landroid/util/TypedValue;->type:I

    if-ne v0, v11, :cond_18

    .line 648072
    invoke-virtual {v3, v9}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 648073
    :goto_c
    if-ne v2, v4, :cond_10

    if-eq v0, v4, :cond_11

    .line 648074
    :cond_10
    iget-object v1, p0, LX/3u6;->b:LX/3uM;

    invoke-interface {v1, v2, v0}, LX/3uM;->a(II)V

    .line 648075
    :cond_11
    invoke-virtual {v8}, Landroid/content/res/TypedArray;->recycle()V

    .line 648076
    iget-object v0, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, LX/3u8;->a(Landroid/view/ViewGroup;)V

    .line 648077
    iput-boolean v6, p0, LX/3u8;->s:Z

    .line 648078
    invoke-static {p0, v5}, LX/3u8;->f(LX/3u8;I)LX/3uK;

    move-result-object v0

    .line 648079
    iget-boolean v1, p0, LX/3u6;->n:Z

    move v1, v1

    .line 648080
    if-nez v1, :cond_13

    if-eqz v0, :cond_12

    iget-object v0, v0, LX/3uK;->j:LX/3v0;

    if-nez v0, :cond_13

    .line 648081
    :cond_12
    const/16 v0, 0x8

    invoke-direct {p0, v0}, LX/3u8;->g(I)V

    .line 648082
    :cond_13
    return-void

    :catch_0
    goto/16 :goto_3

    .line 648083
    :catch_1
    goto/16 :goto_3

    :catch_2
    goto/16 :goto_3

    .line 648084
    :cond_14
    const/4 v7, 0x0

    goto :goto_8

    :cond_15
    move-object v2, v0

    .line 648085
    goto :goto_9

    .line 648086
    :cond_16
    iget v0, v2, Landroid/util/TypedValue;->type:I

    if-ne v0, v12, :cond_1a

    .line 648087
    iget v0, v9, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    iget v10, v9, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v10, v10

    invoke-virtual {v2, v0, v10}, Landroid/util/TypedValue;->getFraction(FF)F

    move-result v0

    float-to-int v0, v0

    move v2, v0

    goto :goto_a

    :cond_17
    move-object v3, v1

    .line 648088
    goto :goto_b

    .line 648089
    :cond_18
    iget v0, v3, Landroid/util/TypedValue;->type:I

    if-ne v0, v12, :cond_19

    .line 648090
    iget v0, v9, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v0, v0

    iget v1, v9, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v1, v1

    invoke-virtual {v3, v0, v1}, Landroid/util/TypedValue;->getFraction(FF)F

    move-result v0

    float-to-int v0, v0

    goto :goto_c

    :cond_19
    move v0, v4

    goto :goto_c

    :cond_1a
    move v2, v4

    goto :goto_a

    :cond_1b
    move-object v3, v1

    goto/16 :goto_7

    :cond_1c
    move-object v2, v1

    goto/16 :goto_6

    :cond_1d
    move-object v0, v1

    goto/16 :goto_5
.end method

.method private o()V
    .locals 2

    .prologue
    .line 648091
    iget-boolean v0, p0, LX/3u8;->s:Z

    if-eqz v0, :cond_0

    .line 648092
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Window feature must be requested before adding content"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 648093
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/3uG;)LX/3uV;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 648094
    iget-object v0, p0, LX/3u8;->k:LX/3uV;

    if-eqz v0, :cond_0

    .line 648095
    iget-object v0, p0, LX/3u8;->k:LX/3uV;

    invoke-virtual {v0}, LX/3uV;->c()V

    .line 648096
    :cond_0
    new-instance v3, LX/3uH;

    invoke-direct {v3, p0, p1}, LX/3uH;-><init>(LX/3u8;LX/3uG;)V

    .line 648097
    invoke-virtual {p0}, LX/3u6;->h()Landroid/content/Context;

    move-result-object v4

    .line 648098
    iget-object v0, p0, LX/3u8;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-nez v0, :cond_1

    .line 648099
    iget-boolean v0, p0, LX/3u6;->i:Z

    if-eqz v0, :cond_4

    .line 648100
    new-instance v0, Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-direct {v0, v4}, Landroid/support/v7/internal/widget/ActionBarContextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/3u8;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    .line 648101
    new-instance v0, Landroid/widget/PopupWindow;

    const v5, 0x7f010026

    invoke-direct {v0, v4, v7, v5}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, LX/3u8;->m:Landroid/widget/PopupWindow;

    .line 648102
    iget-object v0, p0, LX/3u8;->m:Landroid/widget/PopupWindow;

    iget-object v5, p0, LX/3u8;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0, v5}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 648103
    iget-object v0, p0, LX/3u8;->m:Landroid/widget/PopupWindow;

    const/4 v5, -0x1

    invoke-virtual {v0, v5}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 648104
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 648105
    iget-object v5, p0, LX/3u6;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    const v6, 0x7f010015

    invoke-virtual {v5, v6, v0, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 648106
    iget v0, v0, Landroid/util/TypedValue;->data:I

    iget-object v5, p0, LX/3u6;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result v0

    .line 648107
    iget-object v5, p0, LX/3u8;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v5, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->setContentHeight(I)V

    .line 648108
    iget-object v0, p0, LX/3u8;->m:Landroid/widget/PopupWindow;

    const/4 v5, -0x2

    invoke-virtual {v0, v5}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 648109
    new-instance v0, Landroid/support/v7/app/AppCompatDelegateImplV7$4;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AppCompatDelegateImplV7$4;-><init>(LX/3u8;)V

    iput-object v0, p0, LX/3u8;->n:Ljava/lang/Runnable;

    .line 648110
    :cond_1
    :goto_0
    iget-object v0, p0, LX/3u8;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_3

    .line 648111
    iget-object v0, p0, LX/3u8;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->c()V

    .line 648112
    new-instance v5, LX/3ud;

    iget-object v6, p0, LX/3u8;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    iget-object v0, p0, LX/3u8;->m:Landroid/widget/PopupWindow;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    invoke-direct {v5, v4, v6, v3, v0}, LX/3ud;-><init>(Landroid/content/Context;Landroid/support/v7/internal/widget/ActionBarContextView;LX/3uG;Z)V

    .line 648113
    invoke-virtual {v5}, LX/3uV;->b()Landroid/view/Menu;

    move-result-object v0

    invoke-interface {p1, v5, v0}, LX/3uG;->a(LX/3uV;Landroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 648114
    invoke-virtual {v5}, LX/3uV;->d()V

    .line 648115
    iget-object v0, p0, LX/3u8;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0, v5}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(LX/3uV;)V

    .line 648116
    iget-object v0, p0, LX/3u8;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ActionBarContextView;->setVisibility(I)V

    .line 648117
    iput-object v5, p0, LX/3u8;->k:LX/3uV;

    .line 648118
    iget-object v0, p0, LX/3u8;->m:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_2

    .line 648119
    iget-object v0, p0, LX/3u6;->b:LX/3uM;

    invoke-interface {v0}, LX/3uM;->b()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, LX/3u8;->n:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 648120
    :cond_2
    iget-object v0, p0, LX/3u8;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarContextView;->sendAccessibilityEvent(I)V

    .line 648121
    iget-object v0, p0, LX/3u8;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 648122
    iget-object v0, p0, LX/3u8;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, LX/0vv;->z(Landroid/view/View;)V

    .line 648123
    :cond_3
    :goto_2
    iget-object v0, p0, LX/3u8;->k:LX/3uV;

    return-object v0

    .line 648124
    :cond_4
    iget-object v0, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    const v5, 0x7f0d0347

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    .line 648125
    if-eqz v0, :cond_1

    .line 648126
    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 648127
    iput-object v5, v0, Landroid/support/v7/internal/widget/ViewStubCompat;->e:Landroid/view/LayoutInflater;

    .line 648128
    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ViewStubCompat;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContextView;

    iput-object v0, p0, LX/3u8;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    goto :goto_0

    :cond_5
    move v0, v2

    .line 648129
    goto :goto_1

    .line 648130
    :cond_6
    iput-object v7, p0, LX/3u8;->k:LX/3uV;

    goto :goto_2
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 647710
    iget-boolean v0, p0, LX/3u6;->f:Z

    if-nez v0, :cond_0

    .line 647711
    iget-object v0, p0, LX/3u8;->o:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 647712
    :goto_0
    return-object v0

    .line 647713
    :cond_0
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 647714
    iget-object v0, p0, LX/3u6;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v2, 0x7f010013

    invoke-virtual {v0, v2, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 647715
    iget v0, v1, Landroid/util/TypedValue;->resourceId:I

    if-eqz v0, :cond_7

    .line 647716
    new-instance v0, LX/3uc;

    iget-object v2, p0, LX/3u6;->a:Landroid/content/Context;

    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-direct {v0, v2, v1}, LX/3uc;-><init>(Landroid/content/Context;I)V

    .line 647717
    :goto_1
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030013

    invoke-virtual {v0, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    .line 647718
    iget-object v0, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    const v1, 0x7f0d0349

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/3vU;

    iput-object v0, p0, LX/3u8;->p:LX/3vU;

    .line 647719
    iget-object v0, p0, LX/3u8;->p:LX/3vU;

    invoke-virtual {p0}, LX/3u6;->j()Landroid/view/Window$Callback;

    move-result-object v1

    invoke-interface {v0, v1}, LX/3vU;->setWindowCallback(Landroid/view/Window$Callback;)V

    .line 647720
    iget-boolean v0, p0, LX/3u6;->g:Z

    if-eqz v0, :cond_1

    .line 647721
    iget-object v0, p0, LX/3u8;->p:LX/3vU;

    const/16 v1, 0x9

    invoke-interface {v0, v1}, LX/3vU;->a(I)V

    .line 647722
    :cond_1
    iget-boolean v0, p0, LX/3u8;->x:Z

    if-eqz v0, :cond_2

    .line 647723
    iget-object v0, p0, LX/3u8;->p:LX/3vU;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, LX/3vU;->a(I)V

    .line 647724
    :cond_2
    iget-boolean v0, p0, LX/3u8;->y:Z

    if-eqz v0, :cond_3

    .line 647725
    iget-object v0, p0, LX/3u8;->p:LX/3vU;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, LX/3vU;->a(I)V

    .line 647726
    :cond_3
    iget-object v0, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    const v1, 0x7f0d0003

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 647727
    invoke-virtual {p0}, LX/3u6;->k()Ljava/lang/CharSequence;

    move-result-object v1

    .line 647728
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 647729
    invoke-virtual {p0, v1}, LX/3u8;->a(Ljava/lang/CharSequence;)V

    .line 647730
    :cond_4
    iget-object v1, p0, LX/3u8;->o:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, p1, v0, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 647731
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 647732
    iget-object v0, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, LX/3u8;->a(Landroid/view/ViewGroup;)V

    .line 647733
    iput-boolean v4, p0, LX/3u8;->s:Z

    .line 647734
    iput-boolean v3, p0, LX/3u8;->G:Z

    .line 647735
    invoke-static {p0, v3}, LX/3u8;->f(LX/3u8;I)LX/3uK;

    move-result-object v0

    .line 647736
    iget-boolean v1, p0, LX/3u6;->n:Z

    move v1, v1

    .line 647737
    if-nez v1, :cond_6

    if-eqz v0, :cond_5

    iget-object v0, v0, LX/3uK;->j:LX/3v0;

    if-nez v0, :cond_6

    .line 647738
    :cond_5
    const/16 v0, 0x8

    invoke-direct {p0, v0}, LX/3u8;->g(I)V

    .line 647739
    :cond_6
    iget-object v0, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    goto/16 :goto_0

    .line 647740
    :cond_7
    iget-object v0, p0, LX/3u6;->a:Landroid/content/Context;

    goto/16 :goto_1
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 648131
    invoke-direct {p0}, LX/3u8;->l()V

    .line 648132
    iget-object v0, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 648133
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 648134
    iget-object v1, p0, LX/3u6;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 648135
    iget-object v0, p0, LX/3u6;->c:Landroid/view/Window$Callback;

    invoke-interface {v0}, Landroid/view/Window$Callback;->onContentChanged()V

    .line 648136
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 647497
    invoke-super {p0, p1}, LX/3u6;->a(Landroid/os/Bundle;)V

    .line 647498
    iget-object v0, p0, LX/3u6;->b:LX/3uM;

    invoke-interface {v0}, LX/3uM;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/3u8;->t:Landroid/view/ViewGroup;

    .line 647499
    iget-object v0, p0, LX/3u6;->c:Landroid/view/Window$Callback;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 647500
    iget-object v0, p0, LX/3u6;->c:Landroid/view/Window$Callback;

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, LX/3pR;->c(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 647501
    iget-object v0, p0, LX/3u6;->k:LX/3u1;

    move-object v0, v0

    .line 647502
    if-nez v0, :cond_1

    .line 647503
    iput-boolean v1, p0, LX/3u8;->F:Z

    .line 647504
    :cond_0
    :goto_0
    return-void

    .line 647505
    :cond_1
    invoke-virtual {v0, v1}, LX/3u1;->d(Z)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 647506
    invoke-direct {p0}, LX/3u8;->l()V

    .line 647507
    iget-object v0, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 647508
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 647509
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 647510
    iget-object v0, p0, LX/3u6;->c:Landroid/view/Window$Callback;

    invoke-interface {v0}, Landroid/view/Window$Callback;->onContentChanged()V

    .line 647511
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 647512
    invoke-direct {p0}, LX/3u8;->l()V

    .line 647513
    iget-object v0, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 647514
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 647515
    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 647516
    iget-object v0, p0, LX/3u6;->c:Landroid/view/Window$Callback;

    invoke-interface {v0}, Landroid/view/Window$Callback;->onContentChanged()V

    .line 647517
    return-void
.end method

.method public a(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 647518
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 647519
    iget-object v0, p0, LX/3u8;->p:LX/3vU;

    if-eqz v0, :cond_1

    .line 647520
    iget-object v0, p0, LX/3u8;->p:LX/3vU;

    invoke-interface {v0, p1}, LX/3vU;->setWindowTitle(Ljava/lang/CharSequence;)V

    .line 647521
    :cond_0
    :goto_0
    return-void

    .line 647522
    :cond_1
    invoke-virtual {p0}, LX/3u3;->a()LX/3u1;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 647523
    invoke-virtual {p0}, LX/3u3;->a()LX/3u1;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/3u1;->c(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 647524
    :cond_2
    iget-object v0, p0, LX/3u8;->v:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 647525
    iget-object v0, p0, LX/3u8;->v:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 647526
    invoke-virtual {p0}, LX/3u3;->a()LX/3u1;

    move-result-object v2

    .line 647527
    if-eqz v2, :cond_1

    .line 647528
    goto :goto_1

    .line 647529
    :cond_0
    :goto_0
    return v0

    .line 647530
    :cond_1
    :goto_1
    iget-object v2, p0, LX/3u8;->B:LX/3uK;

    if-eqz v2, :cond_2

    .line 647531
    iget-object v2, p0, LX/3u8;->B:LX/3uK;

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    invoke-direct {p0, v2, v3, p2, v0}, LX/3u8;->a(LX/3uK;ILandroid/view/KeyEvent;I)Z

    move-result v2

    .line 647532
    if-eqz v2, :cond_2

    .line 647533
    iget-object v1, p0, LX/3u8;->B:LX/3uK;

    if-eqz v1, :cond_0

    .line 647534
    iget-object v1, p0, LX/3u8;->B:LX/3uK;

    iput-boolean v0, v1, LX/3uK;->n:Z

    goto :goto_0

    .line 647535
    :cond_2
    iget-object v2, p0, LX/3u8;->B:LX/3uK;

    if-nez v2, :cond_3

    .line 647536
    invoke-static {p0, v1}, LX/3u8;->f(LX/3u8;I)LX/3uK;

    move-result-object v2

    .line 647537
    invoke-static {p0, v2, p2}, LX/3u8;->b(LX/3u8;LX/3uK;Landroid/view/KeyEvent;)Z

    .line 647538
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    invoke-direct {p0, v2, v3, p2, v0}, LX/3u8;->a(LX/3uK;ILandroid/view/KeyEvent;I)Z

    move-result v3

    .line 647539
    iput-boolean v1, v2, LX/3uK;->m:Z

    .line 647540
    if-nez v3, :cond_0

    :cond_3
    move v0, v1

    .line 647541
    goto :goto_0
.end method

.method public final a(LX/3v0;Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 647542
    invoke-virtual {p0}, LX/3u6;->j()Landroid/view/Window$Callback;

    move-result-object v0

    .line 647543
    if-eqz v0, :cond_0

    .line 647544
    iget-boolean v1, p0, LX/3u6;->n:Z

    move v1, v1

    .line 647545
    if-nez v1, :cond_0

    .line 647546
    invoke-virtual {p1}, LX/3v0;->q()LX/3v0;

    move-result-object v1

    invoke-static {p0, v1}, LX/3u8;->a$redex0(LX/3u8;Landroid/view/Menu;)LX/3uK;

    move-result-object v1

    .line 647547
    if-eqz v1, :cond_0

    .line 647548
    iget v1, v1, LX/3uK;->a:I

    invoke-interface {v0, v1, p2}, Landroid/view/Window$Callback;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    .line 647549
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/KeyEvent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 647550
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x52

    if-ne v1, v2, :cond_0

    .line 647551
    iget-object v1, p0, LX/3u6;->c:Landroid/view/Window$Callback;

    invoke-interface {v1, p1}, Landroid/view/Window$Callback;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 647552
    :goto_0
    return v0

    .line 647553
    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    .line 647554
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    .line 647555
    if-nez v2, :cond_2

    .line 647556
    :goto_1
    if-eqz v0, :cond_3

    const/4 v0, 0x0

    .line 647557
    packed-switch v1, :pswitch_data_0

    .line 647558
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_1

    .line 647559
    invoke-virtual {p0, v1, p1}, LX/3u8;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 647560
    :cond_1
    :goto_2
    move v0, v0

    .line 647561
    goto :goto_0

    .line 647562
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 647563
    :cond_3
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 647564
    sparse-switch v1, :sswitch_data_0

    :cond_4
    move v0, v2

    .line 647565
    :goto_3
    move v0, v0

    .line 647566
    goto :goto_0

    .line 647567
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v2

    if-nez v2, :cond_5

    .line 647568
    invoke-static {p0, v0}, LX/3u8;->f(LX/3u8;I)LX/3uK;

    move-result-object v2

    .line 647569
    iget-boolean v3, v2, LX/3uK;->o:Z

    if-nez v3, :cond_5

    .line 647570
    invoke-static {p0, v2, p1}, LX/3u8;->b(LX/3u8;LX/3uK;Landroid/view/KeyEvent;)Z

    .line 647571
    :goto_4
    const/4 v0, 0x1

    goto :goto_2

    :cond_5
    goto :goto_4

    .line 647572
    :sswitch_0
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 647573
    iget-object v5, p0, LX/3u8;->k:LX/3uV;

    if-eqz v5, :cond_9

    .line 647574
    :cond_6
    :goto_5
    goto :goto_3

    .line 647575
    :sswitch_1
    invoke-static {p0, v2}, LX/3u8;->f(LX/3u8;I)LX/3uK;

    move-result-object v3

    .line 647576
    if-eqz v3, :cond_7

    iget-boolean v4, v3, LX/3uK;->o:Z

    if-eqz v4, :cond_7

    .line 647577
    invoke-static {p0, v3, v0}, LX/3u8;->a$redex0(LX/3u8;LX/3uK;Z)V

    goto :goto_3

    .line 647578
    :cond_7
    const/4 v3, 0x1

    .line 647579
    iget-object v4, p0, LX/3u8;->k:LX/3uV;

    if-eqz v4, :cond_11

    .line 647580
    iget-object v4, p0, LX/3u8;->k:LX/3uV;

    invoke-virtual {v4}, LX/3uV;->c()V

    .line 647581
    :cond_8
    :goto_6
    move v3, v3

    .line 647582
    if-eqz v3, :cond_4

    goto :goto_3

    .line 647583
    :cond_9
    invoke-static {p0, v2}, LX/3u8;->f(LX/3u8;I)LX/3uK;

    move-result-object v1

    .line 647584
    if-nez v2, :cond_b

    iget-object v5, p0, LX/3u8;->p:LX/3vU;

    if-eqz v5, :cond_b

    iget-object v5, p0, LX/3u8;->p:LX/3vU;

    invoke-interface {v5}, LX/3vU;->b()Z

    move-result v5

    if-eqz v5, :cond_b

    iget-object v5, p0, LX/3u6;->a:Landroid/content/Context;

    invoke-static {v5}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v5

    invoke-static {v5}, LX/0iP;->b(Landroid/view/ViewConfiguration;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 647585
    iget-object v3, p0, LX/3u8;->p:LX/3vU;

    invoke-interface {v3}, LX/3vU;->c()Z

    move-result v3

    if-nez v3, :cond_a

    .line 647586
    iget-boolean v3, p0, LX/3u6;->n:Z

    move v3, v3

    .line 647587
    if-nez v3, :cond_f

    invoke-static {p0, v1, p1}, LX/3u8;->b(LX/3u8;LX/3uK;Landroid/view/KeyEvent;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 647588
    iget-object v3, p0, LX/3u8;->p:LX/3vU;

    invoke-interface {v3}, LX/3vU;->e()Z

    move-result v3

    .line 647589
    :goto_7
    if-eqz v3, :cond_6

    .line 647590
    iget-object v3, p0, LX/3u6;->a:Landroid/content/Context;

    const-string v5, "audio"

    invoke-virtual {v3, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/media/AudioManager;

    .line 647591
    if-eqz v3, :cond_e

    .line 647592
    invoke-virtual {v3, v4}, Landroid/media/AudioManager;->playSoundEffect(I)V

    goto :goto_5

    .line 647593
    :cond_a
    iget-object v3, p0, LX/3u8;->p:LX/3vU;

    invoke-interface {v3}, LX/3vU;->f()Z

    move-result v3

    goto :goto_7

    .line 647594
    :cond_b
    iget-boolean v5, v1, LX/3uK;->o:Z

    if-nez v5, :cond_c

    iget-boolean v5, v1, LX/3uK;->n:Z

    if-eqz v5, :cond_d

    .line 647595
    :cond_c
    iget-boolean v5, v1, LX/3uK;->o:Z

    .line 647596
    invoke-static {p0, v1, v3}, LX/3u8;->a$redex0(LX/3u8;LX/3uK;Z)V

    move v3, v5

    goto :goto_7

    .line 647597
    :cond_d
    iget-boolean v5, v1, LX/3uK;->m:Z

    if-eqz v5, :cond_f

    .line 647598
    iget-boolean v5, v1, LX/3uK;->r:Z

    if-eqz v5, :cond_10

    .line 647599
    iput-boolean v4, v1, LX/3uK;->m:Z

    .line 647600
    invoke-static {p0, v1, p1}, LX/3u8;->b(LX/3u8;LX/3uK;Landroid/view/KeyEvent;)Z

    move-result v5

    .line 647601
    :goto_8
    if-eqz v5, :cond_f

    .line 647602
    invoke-static {p0, v1, p1}, LX/3u8;->a(LX/3u8;LX/3uK;Landroid/view/KeyEvent;)V

    goto :goto_7

    .line 647603
    :cond_e
    const-string v3, "AppCompatDelegate"

    const-string v4, "Couldn\'t get audio manager"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    :cond_f
    move v3, v4

    goto :goto_7

    :cond_10
    move v5, v3

    goto :goto_8

    .line 647604
    :cond_11
    invoke-virtual {p0}, LX/3u3;->a()LX/3u1;

    move-result-object v4

    .line 647605
    if-eqz v4, :cond_12

    invoke-virtual {v4}, LX/3u1;->h()Z

    move-result v4

    if-nez v4, :cond_8

    .line 647606
    :cond_12
    const/4 v3, 0x0

    goto/16 :goto_6

    :pswitch_data_0
    .packed-switch 0x52
        :pswitch_0
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x52 -> :sswitch_0
    .end sparse-switch
.end method

.method public final b(LX/3uG;)LX/3uV;
    .locals 2

    .prologue
    .line 647607
    if-nez p1, :cond_0

    .line 647608
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ActionMode callback can not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 647609
    :cond_0
    iget-object v0, p0, LX/3u8;->k:LX/3uV;

    if-eqz v0, :cond_1

    .line 647610
    iget-object v0, p0, LX/3u8;->k:LX/3uV;

    invoke-virtual {v0}, LX/3uV;->c()V

    .line 647611
    :cond_1
    new-instance v0, LX/3uH;

    invoke-direct {v0, p0, p1}, LX/3uH;-><init>(LX/3u8;LX/3uG;)V

    .line 647612
    invoke-virtual {p0}, LX/3u3;->a()LX/3u1;

    move-result-object v1

    .line 647613
    if-eqz v1, :cond_2

    .line 647614
    invoke-virtual {v1, v0}, LX/3u1;->a(LX/3uG;)LX/3uV;

    move-result-object v1

    iput-object v1, p0, LX/3u8;->k:LX/3uV;

    .line 647615
    :cond_2
    iget-object v1, p0, LX/3u8;->k:LX/3uV;

    if-nez v1, :cond_3

    .line 647616
    invoke-virtual {p0, v0}, LX/3u8;->a(LX/3uG;)LX/3uV;

    move-result-object v0

    iput-object v0, p0, LX/3u8;->k:LX/3uV;

    .line 647617
    :cond_3
    iget-object v0, p0, LX/3u8;->k:LX/3uV;

    return-object v0
.end method

.method public final b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 647618
    invoke-direct {p0}, LX/3u8;->l()V

    .line 647619
    iget-object v0, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 647620
    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 647621
    iget-object v0, p0, LX/3u6;->c:Landroid/view/Window$Callback;

    invoke-interface {v0}, Landroid/view/Window$Callback;->onContentChanged()V

    .line 647622
    return-void
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 647623
    packed-switch p1, :pswitch_data_0

    .line 647624
    :pswitch_0
    iget-object v0, p0, LX/3u6;->b:LX/3uM;

    invoke-interface {v0, p1}, LX/3uM;->b(I)Z

    move-result v0

    :goto_0
    return v0

    .line 647625
    :pswitch_1
    invoke-direct {p0}, LX/3u8;->o()V

    .line 647626
    iput-boolean v0, p0, LX/3u8;->f:Z

    goto :goto_0

    .line 647627
    :pswitch_2
    invoke-direct {p0}, LX/3u8;->o()V

    .line 647628
    iput-boolean v0, p0, LX/3u8;->g:Z

    goto :goto_0

    .line 647629
    :pswitch_3
    invoke-direct {p0}, LX/3u8;->o()V

    .line 647630
    iput-boolean v0, p0, LX/3u8;->h:Z

    goto :goto_0

    .line 647631
    :pswitch_4
    invoke-direct {p0}, LX/3u8;->o()V

    .line 647632
    iput-boolean v0, p0, LX/3u8;->x:Z

    goto :goto_0

    .line 647633
    :pswitch_5
    invoke-direct {p0}, LX/3u8;->o()V

    .line 647634
    iput-boolean v0, p0, LX/3u8;->y:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b_(LX/3v0;)V
    .locals 6

    .prologue
    .line 647635
    const/4 v0, 0x1

    const/16 p1, 0x8

    const/4 v5, 0x0

    .line 647636
    iget-object v1, p0, LX/3u8;->p:LX/3vU;

    if-eqz v1, :cond_5

    iget-object v1, p0, LX/3u8;->p:LX/3vU;

    invoke-interface {v1}, LX/3vU;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LX/3u6;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-static {v1}, LX/0iP;->b(Landroid/view/ViewConfiguration;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/3u8;->p:LX/3vU;

    invoke-interface {v1}, LX/3vU;->d()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 647637
    :cond_0
    invoke-virtual {p0}, LX/3u6;->j()Landroid/view/Window$Callback;

    move-result-object v1

    .line 647638
    iget-object v2, p0, LX/3u8;->p:LX/3vU;

    invoke-interface {v2}, LX/3vU;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    if-nez v0, :cond_4

    .line 647639
    :cond_1
    if-eqz v1, :cond_3

    .line 647640
    iget-boolean v2, p0, LX/3u6;->n:Z

    move v2, v2

    .line 647641
    if-nez v2, :cond_3

    .line 647642
    iget-boolean v2, p0, LX/3u8;->C:Z

    if-eqz v2, :cond_2

    iget v2, p0, LX/3u8;->D:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    .line 647643
    iget-object v2, p0, LX/3u8;->t:Landroid/view/ViewGroup;

    iget-object v3, p0, LX/3u8;->E:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 647644
    iget-object v2, p0, LX/3u8;->E:Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    .line 647645
    :cond_2
    invoke-static {p0, v5}, LX/3u8;->f(LX/3u8;I)LX/3uK;

    move-result-object v2

    .line 647646
    iget-object v3, v2, LX/3uK;->j:LX/3v0;

    if-eqz v3, :cond_3

    iget-boolean v3, v2, LX/3uK;->r:Z

    if-nez v3, :cond_3

    iget-object v3, v2, LX/3uK;->i:Landroid/view/View;

    iget-object v4, v2, LX/3uK;->j:LX/3v0;

    invoke-interface {v1, v5, v3, v4}, Landroid/view/Window$Callback;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 647647
    iget-object v2, v2, LX/3uK;->j:LX/3v0;

    invoke-interface {v1, p1, v2}, Landroid/view/Window$Callback;->onMenuOpened(ILandroid/view/Menu;)Z

    .line 647648
    iget-object v1, p0, LX/3u8;->p:LX/3vU;

    invoke-interface {v1}, LX/3vU;->e()Z

    .line 647649
    :cond_3
    :goto_0
    return-void

    .line 647650
    :cond_4
    iget-object v2, p0, LX/3u8;->p:LX/3vU;

    invoke-interface {v2}, LX/3vU;->f()Z

    .line 647651
    iget-boolean v2, p0, LX/3u6;->n:Z

    move v2, v2

    .line 647652
    if-nez v2, :cond_3

    .line 647653
    invoke-static {p0, v5}, LX/3u8;->f(LX/3u8;I)LX/3uK;

    move-result-object v2

    .line 647654
    iget-object v2, v2, LX/3uK;->j:LX/3v0;

    invoke-interface {v1, p1, v2}, Landroid/view/Window$Callback;->onPanelClosed(ILandroid/view/Menu;)V

    goto :goto_0

    .line 647655
    :cond_5
    invoke-static {p0, v5}, LX/3u8;->f(LX/3u8;I)LX/3uK;

    move-result-object v1

    .line 647656
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/3uK;->q:Z

    .line 647657
    invoke-static {p0, v1, v5}, LX/3u8;->a$redex0(LX/3u8;LX/3uK;Z)V

    .line 647658
    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, LX/3u8;->a(LX/3u8;LX/3uK;Landroid/view/KeyEvent;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 647659
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3u8;->G:Z

    .line 647660
    iget-object v0, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 647661
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3u8;->C:Z

    .line 647662
    iget-object v0, p0, LX/3u8;->t:Landroid/view/ViewGroup;

    iget-object v1, p0, LX/3u8;->E:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 647663
    const/4 v0, 0x0

    iput-object v0, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    .line 647664
    :cond_0
    return-void
.end method

.method public final c(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 647674
    const/16 v1, 0x8

    if-ne p1, v1, :cond_2

    .line 647675
    invoke-virtual {p0}, LX/3u3;->a()LX/3u1;

    move-result-object v1

    .line 647676
    if-eqz v1, :cond_0

    .line 647677
    invoke-virtual {v1, v0}, LX/3u1;->e(Z)V

    .line 647678
    :cond_0
    const/4 v0, 0x1

    .line 647679
    :cond_1
    :goto_0
    return v0

    .line 647680
    :cond_2
    if-nez p1, :cond_1

    .line 647681
    invoke-static {p0, p1}, LX/3u8;->f(LX/3u8;I)LX/3uK;

    move-result-object v1

    .line 647682
    iget-boolean v2, v1, LX/3uK;->o:Z

    if-eqz v2, :cond_1

    .line 647683
    invoke-static {p0, v1, v0}, LX/3u8;->a$redex0(LX/3u8;LX/3uK;Z)V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 647684
    invoke-virtual {p0}, LX/3u3;->a()LX/3u1;

    move-result-object v0

    .line 647685
    if-eqz v0, :cond_0

    .line 647686
    goto :goto_1

    .line 647687
    :goto_0
    return-void

    .line 647688
    :cond_0
    :goto_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/3u8;->g(I)V

    goto :goto_0
.end method

.method public final d(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 647689
    const/16 v1, 0x8

    if-ne p1, v1, :cond_1

    .line 647690
    invoke-virtual {p0}, LX/3u3;->a()LX/3u1;

    move-result-object v1

    .line 647691
    if-eqz v1, :cond_0

    .line 647692
    invoke-virtual {v1, v0}, LX/3u1;->e(Z)V

    .line 647693
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()LX/3u1;
    .locals 3

    .prologue
    .line 647694
    invoke-direct {p0}, LX/3u8;->l()V

    .line 647695
    iget-object v0, p0, LX/3u8;->o:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_1

    .line 647696
    iget-object v0, p0, LX/3u8;->o:Landroid/support/v4/app/Fragment;

    .line 647697
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v1

    .line 647698
    if-nez v0, :cond_0

    .line 647699
    const/4 v0, 0x0

    .line 647700
    :goto_0
    return-object v0

    .line 647701
    :cond_0
    new-instance v0, LX/3uZ;

    iget-object v1, p0, LX/3u8;->o:Landroid/support/v4/app/Fragment;

    .line 647702
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v2

    .line 647703
    invoke-direct {v0, v1}, LX/3uZ;-><init>(Landroid/view/View;)V

    .line 647704
    :goto_1
    iget-boolean v1, p0, LX/3u8;->F:Z

    invoke-virtual {v0, v1}, LX/3u1;->d(Z)V

    goto :goto_0

    .line 647705
    :cond_1
    iget-object v0, p0, LX/3u6;->c:Landroid/view/Window$Callback;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_2

    .line 647706
    new-instance v1, LX/3uZ;

    iget-object v0, p0, LX/3u6;->c:Landroid/view/Window$Callback;

    check-cast v0, Landroid/app/Activity;

    iget-boolean v2, p0, LX/3u6;->g:Z

    invoke-direct {v1, v0, v2}, LX/3uZ;-><init>(Landroid/app/Activity;Z)V

    move-object v0, v1

    goto :goto_1

    .line 647707
    :cond_2
    iget-object v0, p0, LX/3u6;->c:Landroid/view/Window$Callback;

    instance-of v0, v0, Landroid/app/Dialog;

    if-eqz v0, :cond_3

    .line 647708
    new-instance v1, LX/3uZ;

    iget-object v0, p0, LX/3u6;->c:Landroid/view/Window$Callback;

    check-cast v0, Landroid/app/Dialog;

    invoke-direct {v1, v0}, LX/3uZ;-><init>(Landroid/app/Dialog;)V

    move-object v0, v1

    goto :goto_1

    .line 647709
    :cond_3
    new-instance v0, LX/3uZ;

    iget-object v1, p0, LX/3u8;->u:Landroid/view/ViewGroup;

    invoke-direct {v0, v1}, LX/3uZ;-><init>(Landroid/view/View;)V

    goto :goto_1
.end method
