.class public final LX/3TT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1PH;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field public final synthetic b:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;Lcom/facebook/widget/FbSwipeRefreshLayout;)V
    .locals 0

    .prologue
    .line 584287
    iput-object p1, p0, LX/3TT;->b:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iput-object p2, p0, LX/3TT;->a:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 584288
    iget-object v0, p0, LX/3TT;->b:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v0, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->C:LX/16I;

    invoke-virtual {v0}, LX/16I;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 584289
    iget-object v0, p0, LX/3TT;->b:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v0, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->Y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wL;

    iget-object v1, p0, LX/3TT;->a:Lcom/facebook/widget/FbSwipeRefreshLayout;

    iget-object v2, p0, LX/3TT;->b:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0805ec

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 584290
    invoke-static {v0, v1, v2}, LX/0wL;->b(LX/0wL;Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 584291
    iget-object v0, p0, LX/3TT;->b:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    sget-object v1, LX/2ub;->PULL_TO_REFRESH:LX/2ub;

    invoke-static {v0, v1}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->a$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;LX/2ub;)V

    .line 584292
    :goto_0
    return-void

    .line 584293
    :cond_0
    iget-object v0, p0, LX/3TT;->a:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 584294
    iget-object v0, p0, LX/3TT;->b:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-static {v0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->R(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    goto :goto_0
.end method
