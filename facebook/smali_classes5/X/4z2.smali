.class public final LX/4z2;
.super LX/1Lz;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/1Lz",
        "<TE;>;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 822312
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-direct {p0, v0}, LX/1Lz;-><init>(Ljava/util/Map;)V

    .line 822313
    return-void
.end method

.method private constructor <init>(I)V
    .locals 1

    .prologue
    .line 822310
    invoke-static {p1}, LX/0PM;->c(I)Ljava/util/LinkedHashMap;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1Lz;-><init>(Ljava/util/Map;)V

    .line 822311
    return-void
.end method

.method public static a(I)LX/4z2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(I)",
            "LX/4z2",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 822309
    new-instance v0, LX/4z2;

    invoke-direct {v0, p0}, LX/4z2;-><init>(I)V

    return-object v0
.end method

.method public static g()LX/4z2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "LX/4z2",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 822291
    new-instance v0, LX/4z2;

    invoke-direct {v0}, LX/4z2;-><init>()V

    return-object v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    .line 822303
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 822304
    invoke-static {p1}, LX/50X;->a(Ljava/io/ObjectInputStream;)I

    move-result v0

    .line 822305
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 822306
    iput-object v1, p0, LX/1Lz;->a:Ljava/util/Map;

    .line 822307
    invoke-static {p0, p1, v0}, LX/50X;->a(LX/1M1;Ljava/io/ObjectInputStream;I)V

    .line 822308
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 0
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 822300
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 822301
    invoke-static {p0, p1}, LX/50X;->a(LX/1M1;Ljava/io/ObjectOutputStream;)V

    .line 822302
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;II)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 822299
    invoke-super {p0, p1, p2, p3}, LX/1Lz;->a(Ljava/lang/Object;II)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic add(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 822298
    invoke-super {p0, p1}, LX/1Lz;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic addAll(Ljava/util/Collection;)Z
    .locals 1

    .prologue
    .line 822297
    invoke-super {p0, p1}, LX/1Lz;->addAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 822314
    invoke-super {p0, p1}, LX/1Lz;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic d()Ljava/util/Set;
    .locals 1

    .prologue
    .line 822296
    invoke-super {p0}, LX/1Lz;->d()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 822295
    invoke-super {p0, p1}, LX/1Lz;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic hashCode()I
    .locals 1

    .prologue
    .line 822294
    invoke-super {p0}, LX/1Lz;->hashCode()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic isEmpty()Z
    .locals 1

    .prologue
    .line 822293
    invoke-super {p0}, LX/1Lz;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic remove(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 822292
    invoke-super {p0, p1}, LX/1Lz;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic removeAll(Ljava/util/Collection;)Z
    .locals 1

    .prologue
    .line 822290
    invoke-super {p0, p1}, LX/1Lz;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic retainAll(Ljava/util/Collection;)Z
    .locals 1

    .prologue
    .line 822289
    invoke-super {p0, p1}, LX/1Lz;->retainAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 822288
    invoke-super {p0}, LX/1Lz;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
