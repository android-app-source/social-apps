.class public final LX/4nm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/3oW;

.field private final b:Landroid/content/Context;

.field public c:LX/3oV;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/3oW;)V
    .locals 1

    .prologue
    .line 807448
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 807449
    iput-object p1, p0, LX/4nm;->a:LX/3oW;

    .line 807450
    iget-object v0, p1, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    move-object v0, v0

    .line 807451
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/4nm;->b:Landroid/content/Context;

    .line 807452
    return-void
.end method

.method public static a(Landroid/view/View;Ljava/lang/CharSequence;I)LX/4nm;
    .locals 2

    .prologue
    .line 807444
    invoke-static {p0, p1, p2}, LX/3oW;->a(Landroid/view/View;Ljava/lang/CharSequence;I)LX/3oW;

    move-result-object v0

    .line 807445
    invoke-virtual {v0, p1}, LX/3oW;->a(Ljava/lang/CharSequence;)LX/3oW;

    .line 807446
    iput p2, v0, LX/3oW;->e:I

    .line 807447
    new-instance v1, LX/4nm;

    invoke-direct {v1, v0}, LX/4nm;-><init>(LX/3oW;)V

    return-object v1
.end method

.method public static a(Landroid/view/View;Ljava/lang/String;III)LX/4nm;
    .locals 3

    .prologue
    .line 807436
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 807437
    invoke-static {p0, p1, p2}, LX/4nm;->a(Landroid/view/View;Ljava/lang/CharSequence;I)LX/4nm;

    move-result-object v1

    .line 807438
    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, LX/4nm;->a(I)LX/4nm;

    .line 807439
    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 807440
    iget-object v2, v1, LX/4nm;->a:LX/3oW;

    .line 807441
    iget-object p0, v2, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    move-object v2, p0

    .line 807442
    new-instance p0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {p0, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, p0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 807443
    return-object v1
.end method


# virtual methods
.method public final a(I)LX/4nm;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 807453
    iget-object v0, p0, LX/4nm;->a:LX/3oW;

    .line 807454
    iget-object v1, v0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    move-object v0, v1

    .line 807455
    const v1, 0x7f0d0c51

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 807456
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 807457
    return-object p0
.end method

.method public final a(ILandroid/view/View$OnClickListener;)LX/4nm;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 807432
    iget-object v0, p0, LX/4nm;->b:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 807433
    iget-object p1, p0, LX/4nm;->a:LX/3oW;

    invoke-virtual {p1, v0, p2}, LX/3oW;->a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)LX/3oW;

    .line 807434
    move-object v0, p0

    .line 807435
    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 807427
    iget-object v0, p0, LX/4nm;->a:LX/3oW;

    .line 807428
    iget-object v1, v0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    move-object v0, v1

    .line 807429
    new-instance v1, LX/4nl;

    invoke-direct {v1, p0}, LX/4nl;-><init>(LX/4nm;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 807430
    iget-object v0, p0, LX/4nm;->a:LX/3oW;

    invoke-virtual {v0}, LX/3oW;->b()V

    .line 807431
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 807425
    iget-object v0, p0, LX/4nm;->a:LX/3oW;

    invoke-virtual {v0}, LX/3oW;->c()V

    .line 807426
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 807422
    iget-object v0, p0, LX/4nm;->a:LX/3oW;

    .line 807423
    invoke-static {}, LX/3oZ;->a()LX/3oZ;

    move-result-object v1

    iget-object p0, v0, LX/3oW;->g:LX/3oI;

    invoke-virtual {v1, p0}, LX/3oZ;->e(LX/3oI;)Z

    move-result v1

    move v0, v1

    .line 807424
    return v0
.end method
