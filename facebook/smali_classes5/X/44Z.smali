.class public LX/44Z;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/util/concurrent/atomic/AtomicInteger;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile g:LX/44Z;


# instance fields
.field private final c:LX/0TD;

.field private final d:LX/0So;

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "LX/44Y;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final f:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/Object;",
            "Lcom/facebook/common/executors/KeyedExecutor$Task;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this for writes"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 669790
    const-class v0, LX/44Z;

    sput-object v0, LX/44Z;->a:Ljava/lang/Class;

    .line 669791
    new-instance v0, LX/44X;

    invoke-direct {v0}, LX/44X;-><init>()V

    sput-object v0, LX/44Z;->b:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0So;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 669792
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 669793
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/44Z;->e:Ljava/util/Map;

    .line 669794
    new-instance v0, LX/0S8;

    invoke-direct {v0}, LX/0S8;-><init>()V

    invoke-virtual {v0}, LX/0S8;->g()LX/0S8;

    move-result-object v0

    invoke-virtual {v0}, LX/0S8;->l()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LX/44Z;->f:Ljava/util/concurrent/ConcurrentMap;

    .line 669795
    invoke-static {p1}, LX/0TA;->a(Ljava/util/concurrent/ExecutorService;)LX/0TD;

    move-result-object v0

    iput-object v0, p0, LX/44Z;->c:LX/0TD;

    .line 669796
    iput-object p2, p0, LX/44Z;->d:LX/0So;

    .line 669797
    return-void
.end method

.method public static a(LX/0QB;)LX/44Z;
    .locals 5

    .prologue
    .line 669798
    sget-object v0, LX/44Z;->g:LX/44Z;

    if-nez v0, :cond_1

    .line 669799
    const-class v1, LX/44Z;

    monitor-enter v1

    .line 669800
    :try_start_0
    sget-object v0, LX/44Z;->g:LX/44Z;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 669801
    if-eqz v2, :cond_0

    .line 669802
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 669803
    new-instance p0, LX/44Z;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-direct {p0, v3, v4}, LX/44Z;-><init>(Ljava/util/concurrent/ExecutorService;LX/0So;)V

    .line 669804
    move-object v0, p0

    .line 669805
    sput-object v0, LX/44Z;->g:LX/44Z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 669806
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 669807
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 669808
    :cond_1
    sget-object v0, LX/44Z;->g:LX/44Z;

    return-object v0

    .line 669809
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 669810
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static declared-synchronized a(LX/44Z;LX/44Y;)V
    .locals 5

    .prologue
    .line 669811
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, LX/44Y;->c:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 669812
    :goto_0
    monitor-exit p0

    return-void

    .line 669813
    :cond_0
    :try_start_1
    sget-object v0, LX/44Z;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicInteger;

    .line 669814
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 669815
    :try_start_2
    iget-object v1, p1, LX/44Y;->b:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 669816
    iget-object v1, p0, LX/44Z;->e:Ljava/util/Map;

    iget-object v2, p1, LX/44Y;->a:Ljava/lang/Object;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 669817
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 669818
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 669819
    :cond_1
    :try_start_4
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_2

    iget-object v1, p0, LX/44Z;->c:LX/0TD;

    move-object v2, v1

    .line 669820
    :goto_2
    iget-object v1, p1, LX/44Y;->b:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/common/executors/KeyedExecutor$Task;

    .line 669821
    iget-object v3, p0, LX/44Z;->c:LX/0TD;

    invoke-interface {v3, v1}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    iput-object v3, p1, LX/44Y;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 669822
    iget-object v3, p1, LX/44Y;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v4, Lcom/facebook/common/executors/KeyedExecutor$2;

    invoke-direct {v4, p0, v1, p1}, Lcom/facebook/common/executors/KeyedExecutor$2;-><init>(LX/44Z;Lcom/facebook/common/executors/KeyedExecutor$Task;LX/44Y;)V

    invoke-interface {v3, v4, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    .line 669823
    :catchall_1
    move-exception v1

    :try_start_5
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 669824
    :cond_2
    :try_start_6
    invoke-static {}, LX/0TA;->a()LX/0TD;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-object v1

    move-object v2, v1

    goto :goto_2
.end method

.method public static declared-synchronized a$redex0(LX/44Z;Lcom/facebook/common/executors/KeyedExecutor$Task;LX/44Y;)V
    .locals 2

    .prologue
    .line 669825
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p2, LX/44Y;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 669826
    iget-object v0, p0, LX/44Z;->f:Ljava/util/concurrent/ConcurrentMap;

    iget-object v1, p1, Lcom/facebook/common/executors/KeyedExecutor$Task;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 669827
    invoke-static {p0, p2}, LX/44Z;->a(LX/44Z;LX/44Y;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 669828
    monitor-exit p0

    return-void

    .line 669829
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/concurrent/Callable;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/util/concurrent/Callable",
            "<TT;>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 669830
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/44Z;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/44Y;

    .line 669831
    if-nez v0, :cond_1

    .line 669832
    new-instance v0, LX/44Y;

    invoke-direct {v0, p1}, LX/44Y;-><init>(Ljava/lang/Object;)V

    .line 669833
    iget-object v1, p0, LX/44Z;->e:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v7, v0

    .line 669834
    :goto_0
    iget-object v0, p0, LX/44Z;->f:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p2}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 669835
    sget-object v0, LX/44Z;->a:Ljava/lang/Class;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Already contains a callable for key "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 669836
    :cond_0
    new-instance v0, Lcom/facebook/common/executors/KeyedExecutor$Task;

    iget-object v5, p0, LX/44Z;->d:LX/0So;

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/facebook/common/executors/KeyedExecutor$Task;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/concurrent/Callable;Ljava/lang/String;LX/0So;B)V

    .line 669837
    iget-object v1, p0, LX/44Z;->f:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, p2, v0}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 669838
    iget-object v1, v7, LX/44Y;->b:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 669839
    invoke-static {p0, v7}, LX/44Z;->a(LX/44Z;LX/44Y;)V

    .line 669840
    iget-object v0, v0, Lcom/facebook/common/executors/KeyedExecutor$Task;->c:LX/0Va;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 669841
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move-object v7, v0

    goto :goto_0
.end method
