.class public final LX/4FW;
.super LX/0gS;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 679323
    invoke-direct {p0}, LX/0gS;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Double;)LX/4FW;
    .locals 1

    .prologue
    .line 679336
    const-string v0, "latitude"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 679337
    return-object p0
.end method

.method public final a(Ljava/lang/Integer;)LX/4FW;
    .locals 1

    .prologue
    .line 679334
    const-string v0, "pulsar_rssi"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 679335
    return-object p0
.end method

.method public final b(Ljava/lang/Double;)LX/4FW;
    .locals 1

    .prologue
    .line 679332
    const-string v0, "longitude"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 679333
    return-object p0
.end method

.method public final b(Ljava/lang/Integer;)LX/4FW;
    .locals 1

    .prologue
    .line 679330
    const-string v0, "client_location_time"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 679331
    return-object p0
.end method

.method public final c(Ljava/lang/Double;)LX/4FW;
    .locals 1

    .prologue
    .line 679328
    const-string v0, "horizontal_accuracy"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 679329
    return-object p0
.end method

.method public final c(Ljava/lang/Integer;)LX/4FW;
    .locals 1

    .prologue
    .line 679326
    const-string v0, "client_current_time"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 679327
    return-object p0
.end method

.method public final d(Ljava/lang/Double;)LX/4FW;
    .locals 1

    .prologue
    .line 679324
    const-string v0, "vertical_accuracy"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 679325
    return-object p0
.end method
