.class public final LX/45E;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/CharSequence;
.implements Ljava/util/Formattable;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private final b:I

.field private final c:I

.field private final d:I


# direct methods
.method public constructor <init>(Ljava/util/List;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;III)V"
        }
    .end annotation

    .prologue
    .line 670220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 670221
    iput-object p1, p0, LX/45E;->a:Ljava/util/List;

    .line 670222
    iput p2, p0, LX/45E;->b:I

    .line 670223
    iput p3, p0, LX/45E;->c:I

    .line 670224
    iput p4, p0, LX/45E;->d:I

    .line 670225
    return-void
.end method

.method private a(Ljava/lang/StringBuilder;)V
    .locals 3

    .prologue
    .line 670213
    iget v0, p0, LX/45E;->b:I

    move v1, v0

    :goto_0
    iget v0, p0, LX/45E;->c:I

    if-ge v1, v0, :cond_1

    .line 670214
    iget-object v0, p0, LX/45E;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 670215
    instance-of v2, v0, LX/45E;

    if-eqz v2, :cond_0

    .line 670216
    check-cast v0, LX/45E;

    invoke-direct {v0, p1}, LX/45E;->a(Ljava/lang/StringBuilder;)V

    .line 670217
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 670218
    :cond_0
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 670219
    :cond_1
    return-void
.end method


# virtual methods
.method public final charAt(I)C
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.String.charAt"
        }
    .end annotation

    .prologue
    .line 670235
    invoke-virtual {p0}, LX/45E;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    return v0
.end method

.method public final formatTo(Ljava/util/Formatter;III)V
    .locals 2

    .prologue
    .line 670230
    :try_start_0
    invoke-virtual {p1}, Ljava/util/Formatter;->out()Ljava/lang/Appendable;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 670231
    return-void

    .line 670232
    :catch_0
    move-exception v0

    .line 670233
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final length()I
    .locals 1

    .prologue
    .line 670234
    iget v0, p0, LX/45E;->d:I

    return v0
.end method

.method public final subSequence(II)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 670229
    invoke-virtual {p0}, LX/45E;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 670226
    new-instance v0, Ljava/lang/StringBuilder;

    iget v1, p0, LX/45E;->d:I

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 670227
    invoke-direct {p0, v0}, LX/45E;->a(Ljava/lang/StringBuilder;)V

    .line 670228
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
