.class public LX/3lm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final isIncrementalUpdate:Ljava/lang/Boolean;

.field public final updates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6mY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 634981
    new-instance v0, LX/1sv;

    const-string v1, "PresenceUpdateBatch"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/3lm;->b:LX/1sv;

    .line 634982
    new-instance v0, LX/1sw;

    const-string v1, "isIncrementalUpdate"

    invoke-direct {v0, v1, v4, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/3lm;->c:LX/1sw;

    .line 634983
    new-instance v0, LX/1sw;

    const-string v1, "updates"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/3lm;->d:LX/1sw;

    .line 634984
    sput-boolean v3, LX/3lm;->a:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/Boolean;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Ljava/util/List",
            "<",
            "LX/6mY;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 634985
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 634986
    iput-object p1, p0, LX/3lm;->isIncrementalUpdate:Ljava/lang/Boolean;

    .line 634987
    iput-object p2, p0, LX/3lm;->updates:Ljava/util/List;

    .line 634988
    return-void
.end method

.method public static b(LX/1su;)LX/3lm;
    .locals 15

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 634989
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v3, v0

    .line 634990
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v2

    .line 634991
    iget-byte v4, v2, LX/1sw;->b:B

    if-eqz v4, :cond_b

    .line 634992
    iget-short v4, v2, LX/1sw;->c:S

    packed-switch v4, :pswitch_data_0

    .line 634993
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 634994
    :pswitch_0
    iget-byte v4, v2, LX/1sw;->b:B

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 634995
    invoke-virtual {p0}, LX/1su;->j()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    move-object v3, v2

    goto :goto_0

    .line 634996
    :cond_0
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 634997
    :pswitch_1
    iget-byte v4, v2, LX/1sw;->b:B

    const/16 v5, 0xf

    if-ne v4, v5, :cond_a

    .line 634998
    invoke-virtual {p0}, LX/1su;->h()LX/1u3;

    move-result-object v4

    .line 634999
    new-instance v2, Ljava/util/ArrayList;

    iget v0, v4, LX/1u3;->b:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v1

    .line 635000
    :goto_1
    iget v5, v4, LX/1u3;->b:I

    if-gez v5, :cond_8

    invoke-static {}, LX/1su;->t()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 635001
    :cond_1
    const/16 v14, 0xa

    const/4 v11, 0x0

    .line 635002
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v10, v11

    move-object v9, v11

    move-object v8, v11

    move-object v7, v11

    .line 635003
    :goto_2
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v6

    .line 635004
    iget-byte v12, v6, LX/1sw;->b:B

    if-eqz v12, :cond_7

    .line 635005
    iget-short v12, v6, LX/1sw;->c:S

    packed-switch v12, :pswitch_data_1

    .line 635006
    iget-byte v6, v6, LX/1sw;->b:B

    invoke-static {p0, v6}, LX/3ae;->a(LX/1su;B)V

    goto :goto_2

    .line 635007
    :pswitch_2
    iget-byte v12, v6, LX/1sw;->b:B

    if-ne v12, v14, :cond_2

    .line 635008
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    goto :goto_2

    .line 635009
    :cond_2
    iget-byte v6, v6, LX/1sw;->b:B

    invoke-static {p0, v6}, LX/3ae;->a(LX/1su;B)V

    goto :goto_2

    .line 635010
    :pswitch_3
    iget-byte v12, v6, LX/1sw;->b:B

    const/16 v13, 0x8

    if-ne v12, v13, :cond_3

    .line 635011
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    goto :goto_2

    .line 635012
    :cond_3
    iget-byte v6, v6, LX/1sw;->b:B

    invoke-static {p0, v6}, LX/3ae;->a(LX/1su;B)V

    goto :goto_2

    .line 635013
    :pswitch_4
    iget-byte v12, v6, LX/1sw;->b:B

    if-ne v12, v14, :cond_4

    .line 635014
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    goto :goto_2

    .line 635015
    :cond_4
    iget-byte v6, v6, LX/1sw;->b:B

    invoke-static {p0, v6}, LX/3ae;->a(LX/1su;B)V

    goto :goto_2

    .line 635016
    :pswitch_5
    iget-byte v12, v6, LX/1sw;->b:B

    const/4 v13, 0x6

    if-ne v12, v13, :cond_5

    .line 635017
    invoke-virtual {p0}, LX/1su;->l()S

    move-result v6

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v10

    goto :goto_2

    .line 635018
    :cond_5
    iget-byte v6, v6, LX/1sw;->b:B

    invoke-static {p0, v6}, LX/3ae;->a(LX/1su;B)V

    goto :goto_2

    .line 635019
    :pswitch_6
    iget-byte v12, v6, LX/1sw;->b:B

    if-ne v12, v14, :cond_6

    .line 635020
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    goto :goto_2

    .line 635021
    :cond_6
    iget-byte v6, v6, LX/1sw;->b:B

    invoke-static {p0, v6}, LX/3ae;->a(LX/1su;B)V

    goto :goto_2

    .line 635022
    :cond_7
    invoke-virtual {p0}, LX/1su;->e()V

    .line 635023
    new-instance v6, LX/6mY;

    invoke-direct/range {v6 .. v11}, LX/6mY;-><init>(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Short;Ljava/lang/Long;)V

    .line 635024
    invoke-static {v6}, LX/6mY;->a(LX/6mY;)V

    .line 635025
    move-object v5, v6

    .line 635026
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 635027
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 635028
    :cond_8
    iget v5, v4, LX/1u3;->b:I

    if-lt v0, v5, :cond_1

    :cond_9
    move-object v0, v2

    .line 635029
    goto/16 :goto_0

    .line 635030
    :cond_a
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 635031
    :cond_b
    invoke-virtual {p0}, LX/1su;->e()V

    .line 635032
    new-instance v1, LX/3lm;

    invoke-direct {v1, v3, v0}, LX/3lm;-><init>(Ljava/lang/Boolean;Ljava/util/List;)V

    .line 635033
    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 635034
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 635035
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v2, v0

    .line 635036
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 635037
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "PresenceUpdateBatch"

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 635038
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 635039
    const-string v1, "("

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 635040
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 635041
    const/4 v1, 0x1

    .line 635042
    iget-object v5, p0, LX/3lm;->isIncrementalUpdate:Ljava/lang/Boolean;

    if-eqz v5, :cond_0

    .line 635043
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 635044
    const-string v1, "isIncrementalUpdate"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 635045
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 635046
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 635047
    iget-object v1, p0, LX/3lm;->isIncrementalUpdate:Ljava/lang/Boolean;

    if-nez v1, :cond_6

    .line 635048
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 635049
    :goto_3
    const/4 v1, 0x0

    .line 635050
    :cond_0
    iget-object v5, p0, LX/3lm;->updates:Ljava/util/List;

    if-eqz v5, :cond_2

    .line 635051
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 635052
    :cond_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 635053
    const-string v1, "updates"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 635054
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 635055
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 635056
    iget-object v0, p0, LX/3lm;->updates:Ljava/util/List;

    if-nez v0, :cond_7

    .line 635057
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 635058
    :cond_2
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 635059
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 635060
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 635061
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 635062
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 635063
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 635064
    :cond_6
    iget-object v1, p0, LX/3lm;->isIncrementalUpdate:Ljava/lang/Boolean;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 635065
    :cond_7
    iget-object v0, p0, LX/3lm;->updates:Ljava/util/List;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 635066
    invoke-virtual {p1}, LX/1su;->a()V

    .line 635067
    iget-object v0, p0, LX/3lm;->isIncrementalUpdate:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 635068
    iget-object v0, p0, LX/3lm;->isIncrementalUpdate:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 635069
    sget-object v0, LX/3lm;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 635070
    iget-object v0, p0, LX/3lm;->isIncrementalUpdate:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 635071
    :cond_0
    iget-object v0, p0, LX/3lm;->updates:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 635072
    iget-object v0, p0, LX/3lm;->updates:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 635073
    sget-object v0, LX/3lm;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 635074
    new-instance v0, LX/1u3;

    const/16 v1, 0xc

    iget-object v2, p0, LX/3lm;->updates:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 635075
    iget-object v0, p0, LX/3lm;->updates:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6mY;

    .line 635076
    invoke-virtual {v0, p1}, LX/6mY;->a(LX/1su;)V

    goto :goto_0

    .line 635077
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 635078
    invoke-virtual {p1}, LX/1su;->b()V

    .line 635079
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 635080
    if-nez p1, :cond_1

    .line 635081
    :cond_0
    :goto_0
    return v0

    .line 635082
    :cond_1
    instance-of v1, p1, LX/3lm;

    if-eqz v1, :cond_0

    .line 635083
    check-cast p1, LX/3lm;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 635084
    if-nez p1, :cond_3

    .line 635085
    :cond_2
    :goto_1
    move v0, v2

    .line 635086
    goto :goto_0

    .line 635087
    :cond_3
    iget-object v0, p0, LX/3lm;->isIncrementalUpdate:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    move v0, v1

    .line 635088
    :goto_2
    iget-object v3, p1, LX/3lm;->isIncrementalUpdate:Ljava/lang/Boolean;

    if-eqz v3, :cond_9

    move v3, v1

    .line 635089
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 635090
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 635091
    iget-object v0, p0, LX/3lm;->isIncrementalUpdate:Ljava/lang/Boolean;

    iget-object v3, p1, LX/3lm;->isIncrementalUpdate:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 635092
    :cond_5
    iget-object v0, p0, LX/3lm;->updates:Ljava/util/List;

    if-eqz v0, :cond_a

    move v0, v1

    .line 635093
    :goto_4
    iget-object v3, p1, LX/3lm;->updates:Ljava/util/List;

    if-eqz v3, :cond_b

    move v3, v1

    .line 635094
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 635095
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 635096
    iget-object v0, p0, LX/3lm;->updates:Ljava/util/List;

    iget-object v3, p1, LX/3lm;->updates:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 635097
    goto :goto_1

    :cond_8
    move v0, v2

    .line 635098
    goto :goto_2

    :cond_9
    move v3, v2

    .line 635099
    goto :goto_3

    :cond_a
    move v0, v2

    .line 635100
    goto :goto_4

    :cond_b
    move v3, v2

    .line 635101
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 635102
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 635103
    sget-boolean v0, LX/3lm;->a:Z

    .line 635104
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/3lm;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 635105
    return-object v0
.end method
