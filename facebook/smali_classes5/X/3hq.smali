.class public LX/3hq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final c:I

.field public final d:I

.field public final e:Z

.field public final f:Z

.field public final g:Z

.field public final h:Z

.field public final i:F

.field public final j:F

.field public final k:F


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;IIZZZFFF)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;IIZZZFFF)V"
        }
    .end annotation

    .prologue
    .line 628217
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    invoke-direct/range {v0 .. v10}, LX/3hq;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;IIZZZZFFF)V

    .line 628218
    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;IIZZZZFFF)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;IIZZZZFFF)V"
        }
    .end annotation

    .prologue
    .line 628203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 628204
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 628205
    iput-object p1, p0, LX/3hq;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 628206
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    iput-object v0, p0, LX/3hq;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 628207
    iput p2, p0, LX/3hq;->c:I

    .line 628208
    iput p3, p0, LX/3hq;->d:I

    .line 628209
    iput-boolean p4, p0, LX/3hq;->e:Z

    .line 628210
    iput-boolean p5, p0, LX/3hq;->f:Z

    .line 628211
    iput-boolean p6, p0, LX/3hq;->g:Z

    .line 628212
    iput p8, p0, LX/3hq;->i:F

    .line 628213
    iput p9, p0, LX/3hq;->j:F

    .line 628214
    iput p10, p0, LX/3hq;->k:F

    .line 628215
    iput-boolean p7, p0, LX/3hq;->h:Z

    .line 628216
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 628219
    iget-object v0, p0, LX/3hq;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 628202
    iget v0, p0, LX/3hq;->d:I

    return v0
.end method
