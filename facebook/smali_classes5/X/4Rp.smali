.class public LX/4Rp;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 710700
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 710701
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 710702
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 710703
    :goto_0
    return v1

    .line 710704
    :cond_0
    const-string v11, "unread_count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 710705
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    .line 710706
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_8

    .line 710707
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 710708
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 710709
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 710710
    const-string v11, "bucket_id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 710711
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 710712
    :cond_2
    const-string v11, "fbglyph_image"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 710713
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 710714
    :cond_3
    const-string v11, "image"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 710715
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 710716
    :cond_4
    const-string v11, "subtitle"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 710717
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 710718
    :cond_5
    const-string v11, "title"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 710719
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 710720
    :cond_6
    const-string v11, "uri"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 710721
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 710722
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 710723
    :cond_8
    const/4 v10, 0x7

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 710724
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 710725
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 710726
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 710727
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 710728
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 710729
    if-eqz v0, :cond_9

    .line 710730
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4, v1}, LX/186;->a(III)V

    .line 710731
    :cond_9
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 710732
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 710733
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 710734
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 710735
    if-eqz v0, :cond_0

    .line 710736
    const-string v1, "bucket_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710737
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 710738
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 710739
    if-eqz v0, :cond_1

    .line 710740
    const-string v1, "fbglyph_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710741
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 710742
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 710743
    if-eqz v0, :cond_2

    .line 710744
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710745
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 710746
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 710747
    if-eqz v0, :cond_3

    .line 710748
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710749
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 710750
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 710751
    if-eqz v0, :cond_4

    .line 710752
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710753
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 710754
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 710755
    if-eqz v0, :cond_5

    .line 710756
    const-string v1, "unread_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710757
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 710758
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 710759
    if-eqz v0, :cond_6

    .line 710760
    const-string v1, "uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710761
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 710762
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 710763
    return-void
.end method
