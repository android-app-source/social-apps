.class public LX/3gg;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Ljava/lang/Void;",
        "Lcom/facebook/user/model/PicSquare;",
        ">;"
    }
.end annotation


# instance fields
.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3MX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0sO;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 624678
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 624679
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 624680
    const-class v0, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel;

    .line 624681
    invoke-virtual {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel;->a()Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 624682
    invoke-virtual {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel;->a()Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;

    move-result-object v0

    .line 624683
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 624684
    invoke-virtual {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 624685
    new-instance p0, Lcom/facebook/user/model/PicSquareUrlWithSize;

    invoke-virtual {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->c()I

    move-result p1

    invoke-virtual {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    invoke-virtual {v1, p0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 624686
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 624687
    new-instance p0, Lcom/facebook/user/model/PicSquareUrlWithSize;

    invoke-virtual {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->c()I

    move-result p1

    invoke-virtual {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    invoke-virtual {v1, p0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 624688
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p0

    if-eqz p0, :cond_2

    .line 624689
    new-instance p0, Lcom/facebook/user/model/PicSquareUrlWithSize;

    invoke-virtual {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->c()I

    move-result p1

    invoke-virtual {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    invoke-virtual {v1, p0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 624690
    :cond_2
    new-instance p0, Lcom/facebook/user/model/PicSquare;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/facebook/user/model/PicSquare;-><init>(LX/0Px;)V

    move-object v0, p0

    .line 624691
    :goto_0
    return-object v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 624692
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 4

    .prologue
    .line 624693
    iget-object v0, p0, LX/3gg;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3MX;

    .line 624694
    new-instance v1, LX/3k3;

    invoke-direct {v1}, LX/3k3;-><init>()V

    move-object v1, v1

    .line 624695
    const-string v2, "square_profile_pic_size_small"

    sget-object v3, LX/3MY;->SMALL:LX/3MY;

    invoke-virtual {v0, v3}, LX/3MX;->a(LX/3MY;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "square_profile_pic_size_big"

    sget-object v3, LX/3MY;->BIG:LX/3MY;

    invoke-virtual {v0, v3}, LX/3MX;->a(LX/3MY;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "square_profile_pic_size_huge"

    sget-object v3, LX/3MY;->HUGE:LX/3MY;

    invoke-virtual {v0, v3}, LX/3MX;->a(LX/3MY;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    return-object v0
.end method
