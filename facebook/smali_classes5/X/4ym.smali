.class public final LX/4ym;
.super LX/0wv;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0wv",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/Iterable;

.field public final synthetic b:I


# direct methods
.method public constructor <init>(Ljava/lang/Iterable;I)V
    .locals 0

    .prologue
    .line 822022
    iput-object p1, p0, LX/4ym;->a:Ljava/lang/Iterable;

    iput p2, p0, LX/4ym;->b:I

    invoke-direct {p0}, LX/0wv;-><init>()V

    return-void
.end method


# virtual methods
.method public final iterator()Ljava/util/Iterator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 822023
    iget-object v0, p0, LX/4ym;->a:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iget v1, p0, LX/4ym;->b:I

    .line 822024
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 822025
    if-ltz v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    const-string p0, "limit is negative"

    invoke-static {v2, p0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 822026
    new-instance v2, LX/4yr;

    invoke-direct {v2, v1, v0}, LX/4yr;-><init>(ILjava/util/Iterator;)V

    move-object v0, v2

    .line 822027
    return-object v0

    .line 822028
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
