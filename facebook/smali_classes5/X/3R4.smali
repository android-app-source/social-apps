.class public final LX/3R4;
.super LX/0aT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aT",
        "<",
        "LX/2PA;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3R4;


# instance fields
.field private final a:LX/26j;


# direct methods
.method public constructor <init>(LX/0Ot;LX/26j;LX/01T;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2PA;",
            ">;",
            "LX/26j;",
            "LX/01T;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 578936
    sget-object v0, LX/01T;->MESSENGER:LX/01T;

    if-ne p3, v0, :cond_0

    .line 578937
    :goto_0
    move-object v0, p1

    .line 578938
    invoke-direct {p0, v0}, LX/0aT;-><init>(LX/0Ot;)V

    .line 578939
    iput-object p2, p0, LX/3R4;->a:LX/26j;

    .line 578940
    return-void

    :cond_0
    const/4 v0, 0x0

    .line 578941
    new-instance p3, LX/3R6;

    invoke-direct {p3, v0}, LX/3R6;-><init>(Ljava/lang/Object;)V

    move-object p1, p3

    .line 578942
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/3R4;
    .locals 6

    .prologue
    .line 578943
    sget-object v0, LX/3R4;->b:LX/3R4;

    if-nez v0, :cond_1

    .line 578944
    const-class v1, LX/3R4;

    monitor-enter v1

    .line 578945
    :try_start_0
    sget-object v0, LX/3R4;->b:LX/3R4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 578946
    if-eqz v2, :cond_0

    .line 578947
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 578948
    new-instance v5, LX/3R4;

    const/16 v3, 0xdd3

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/26j;->b(LX/0QB;)LX/26j;

    move-result-object v3

    check-cast v3, LX/26j;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v4

    check-cast v4, LX/01T;

    invoke-direct {v5, p0, v3, v4}, LX/3R4;-><init>(LX/0Ot;LX/26j;LX/01T;)V

    .line 578949
    move-object v0, v5

    .line 578950
    sput-object v0, LX/3R4;->b:LX/3R4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 578951
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 578952
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 578953
    :cond_1
    sget-object v0, LX/3R4;->b:LX/3R4;

    return-object v0

    .line 578954
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 578955
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 578956
    check-cast p3, LX/2PA;

    .line 578957
    if-eqz p3, :cond_0

    iget-object v0, p0, LX/3R4;->a:LX/26j;

    invoke-virtual {v0}, LX/26j;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 578958
    invoke-static {p3}, LX/2PA;->m(LX/2PA;)V

    .line 578959
    :cond_0
    return-void
.end method
