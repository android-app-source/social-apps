.class public final LX/4xZ;
.super LX/2QI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2QI",
        "<TK;",
        "Ljava/util/Collection",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4xd;


# direct methods
.method public constructor <init>(LX/4xd;)V
    .locals 0

    .prologue
    .line 821113
    iput-object p1, p0, LX/4xZ;->a:LX/4xd;

    invoke-direct {p0}, LX/2QI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 821112
    iget-object v0, p0, LX/4xZ;->a:LX/4xd;

    return-object v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;>;"
        }
    .end annotation

    .prologue
    .line 821108
    new-instance v0, LX/4xY;

    invoke-direct {v0, p0}, LX/4xY;-><init>(LX/4xZ;)V

    return-object v0
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 821111
    iget-object v0, p0, LX/4xZ;->a:LX/4xd;

    iget-object v0, v0, LX/4xd;->a:LX/4xj;

    invoke-static {p1}, LX/0Rj;->in(Ljava/util/Collection;)LX/0Rl;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4xj;->a(LX/0Rl;)Z

    move-result v0

    return v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 821110
    iget-object v0, p0, LX/4xZ;->a:LX/4xd;

    iget-object v0, v0, LX/4xd;->a:LX/4xj;

    invoke-static {p1}, LX/0Rj;->in(Ljava/util/Collection;)LX/0Rl;

    move-result-object v1

    invoke-static {v1}, LX/0Rj;->not(LX/0Rl;)LX/0Rl;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4xj;->a(LX/0Rl;)Z

    move-result v0

    return v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 821109
    invoke-virtual {p0}, LX/4xZ;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0RZ;->b(Ljava/util/Iterator;)I

    move-result v0

    return v0
.end method
