.class public LX/3f9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;
.implements LX/0rp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;",
        ">;",
        "LX/0rp",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/3fA;

.field public b:LX/3fB;


# direct methods
.method public constructor <init>(LX/3fA;LX/3fB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 621476
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 621477
    iput-object p1, p0, LX/3f9;->a:LX/3fA;

    .line 621478
    iput-object p2, p0, LX/3f9;->b:LX/3fB;

    .line 621479
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 621480
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 621481
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "fields"

    const-string v2, "picture.height(74).type(square),id,access_token,perms,name,link"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 621482
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "limit"

    const-string v2, "50"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 621483
    new-instance v0, LX/14N;

    const-string v1, "fetch_pages"

    const-string v2, "GET"

    const-string v3, "me/accounts"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 621484
    iget-object v0, p0, LX/3f9;->b:LX/3fB;

    const-string v1, "FetchAllPagesGraphApiMethod"

    .line 621485
    sget-object p0, LX/8EG;->d:LX/0Px;

    invoke-static {v0, p0, v1}, LX/3fB;->b(LX/3fB;LX/0Px;Ljava/lang/String;)LX/3fB;

    .line 621486
    sget-object p0, LX/8EG;->e:LX/0Px;

    invoke-static {v0, p0, v1}, LX/3fB;->b(LX/3fB;LX/0Px;Ljava/lang/String;)LX/3fB;

    .line 621487
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 621488
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 621489
    iget-object v0, p0, LX/3f9;->a:LX/3fA;

    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3fA;->c(LX/0lF;)LX/0lF;

    move-result-object v3

    .line 621490
    new-instance v0, Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;

    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v2, p0, LX/3f9;->a:LX/3fA;

    invoke-virtual {v2, v3}, LX/3fA;->a(LX/0lF;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v3}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;-><init>(LX/0ta;Ljava/util/ArrayList;Ljava/lang/String;J)V

    return-object v0
.end method

.method public final a_(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 621491
    iget-object v0, p0, LX/3f9;->b:LX/3fB;

    const-string v1, "FetchAllPagesGraphApiMethod"

    invoke-virtual {v0, v1}, LX/3fB;->a(Ljava/lang/String;)LX/3fB;

    .line 621492
    return-void
.end method

.method public final c(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 621493
    iget-object v0, p0, LX/3f9;->b:LX/3fB;

    const-string v1, "FetchAllPagesGraphApiMethod"

    invoke-virtual {v0, v1}, LX/3fB;->b(Ljava/lang/String;)LX/3fB;

    .line 621494
    return-void
.end method
