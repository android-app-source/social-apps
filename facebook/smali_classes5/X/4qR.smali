.class public final LX/4qR;
.super LX/320;
.source ""


# static fields
.field public static final a:LX/4qR;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 813737
    new-instance v0, LX/4qR;

    invoke-direct {v0}, LX/4qR;-><init>()V

    sput-object v0, LX/4qR;->a:LX/4qR;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 813746
    invoke-direct {p0}, LX/320;-><init>()V

    return-void
.end method

.method private static final a(Ljava/lang/Object;)J
    .locals 2

    .prologue
    .line 813747
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    check-cast p0, Ljava/lang/Number;

    invoke-virtual {p0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;LX/0lJ;I)LX/4q3;
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 813744
    new-instance v0, LX/4q3;

    const/4 v9, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move v7, p2

    move-object v8, v3

    invoke-direct/range {v0 .. v9}, LX/4q3;-><init>(Ljava/lang/String;LX/0lJ;LX/2Vb;LX/4qw;LX/0lQ;LX/2Vd;ILjava/lang/Object;Z)V

    return-object v0
.end method

.method private static final b(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 813745
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    check-cast p0, Ljava/lang/Number;

    invoke-virtual {p0}, Ljava/lang/Number;->intValue()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 813743
    new-instance v0, LX/28G;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    const/4 v2, 0x1

    aget-object v2, p1, v2

    invoke-static {v2}, LX/4qR;->a(Ljava/lang/Object;)J

    move-result-wide v2

    const/4 v4, 0x2

    aget-object v4, p1, v4

    invoke-static {v4}, LX/4qR;->a(Ljava/lang/Object;)J

    move-result-wide v4

    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-static {v6}, LX/4qR;->b(Ljava/lang/Object;)I

    move-result v6

    const/4 v7, 0x4

    aget-object v7, p1, v7

    invoke-static {v7}, LX/4qR;->b(Ljava/lang/Object;)I

    move-result v7

    invoke-direct/range {v0 .. v7}, LX/28G;-><init>(Ljava/lang/Object;JJII)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 813742
    const-class v0, LX/28G;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0mu;)[LX/32s;
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 813739
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-virtual {p1, v0}, LX/0m4;->b(Ljava/lang/Class;)LX/0lJ;

    move-result-object v1

    .line 813740
    sget-object v0, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-virtual {p1, v0}, LX/0m4;->b(Ljava/lang/Class;)LX/0lJ;

    move-result-object v2

    .line 813741
    const/4 v0, 0x5

    new-array v0, v0, [LX/4q3;

    const-string v3, "sourceRef"

    const-class v4, Ljava/lang/Object;

    invoke-virtual {p1, v4}, LX/0m4;->b(Ljava/lang/Class;)LX/0lJ;

    move-result-object v4

    invoke-static {v3, v4, v5}, LX/4qR;->a(Ljava/lang/String;LX/0lJ;I)LX/4q3;

    move-result-object v3

    aput-object v3, v0, v5

    const-string v3, "byteOffset"

    invoke-static {v3, v2, v6}, LX/4qR;->a(Ljava/lang/String;LX/0lJ;I)LX/4q3;

    move-result-object v3

    aput-object v3, v0, v6

    const-string v3, "charOffset"

    invoke-static {v3, v2, v7}, LX/4qR;->a(Ljava/lang/String;LX/0lJ;I)LX/4q3;

    move-result-object v2

    aput-object v2, v0, v7

    const-string v2, "lineNr"

    invoke-static {v2, v1, v8}, LX/4qR;->a(Ljava/lang/String;LX/0lJ;I)LX/4q3;

    move-result-object v2

    aput-object v2, v0, v8

    const-string v2, "columnNr"

    invoke-static {v2, v1, v9}, LX/4qR;->a(Ljava/lang/String;LX/0lJ;I)LX/4q3;

    move-result-object v1

    aput-object v1, v0, v9

    check-cast v0, [LX/32s;

    return-object v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 813738
    const/4 v0, 0x1

    return v0
.end method
