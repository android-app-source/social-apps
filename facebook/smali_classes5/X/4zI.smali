.class public final LX/4zI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/ListIterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/ListIterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public a:Z

.field public final synthetic b:Ljava/util/ListIterator;

.field public final synthetic c:LX/4zG;


# direct methods
.method public constructor <init>(LX/4zG;Ljava/util/ListIterator;)V
    .locals 0

    .prologue
    .line 822645
    iput-object p1, p0, LX/4zI;->c:LX/4zG;

    iput-object p2, p0, LX/4zI;->b:Ljava/util/ListIterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 822641
    iget-object v0, p0, LX/4zI;->b:Ljava/util/ListIterator;

    invoke-interface {v0, p1}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    .line 822642
    iget-object v0, p0, LX/4zI;->b:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    .line 822643
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4zI;->a:Z

    .line 822644
    return-void
.end method

.method public final hasNext()Z
    .locals 1

    .prologue
    .line 822640
    iget-object v0, p0, LX/4zI;->b:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    return v0
.end method

.method public final hasPrevious()Z
    .locals 1

    .prologue
    .line 822639
    iget-object v0, p0, LX/4zI;->b:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 822646
    invoke-virtual {p0}, LX/4zI;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 822647
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 822648
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4zI;->a:Z

    .line 822649
    iget-object v0, p0, LX/4zI;->b:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final nextIndex()I
    .locals 2

    .prologue
    .line 822638
    iget-object v0, p0, LX/4zI;->c:LX/4zG;

    iget-object v1, p0, LX/4zI;->b:Ljava/util/ListIterator;

    invoke-interface {v1}, Ljava/util/ListIterator;->nextIndex()I

    move-result v1

    invoke-static {v0, v1}, LX/4zG;->b(LX/4zG;I)I

    move-result v0

    return v0
.end method

.method public final previous()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 822634
    invoke-virtual {p0}, LX/4zI;->hasPrevious()Z

    move-result v0

    if-nez v0, :cond_0

    .line 822635
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 822636
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4zI;->a:Z

    .line 822637
    iget-object v0, p0, LX/4zI;->b:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final previousIndex()I
    .locals 1

    .prologue
    .line 822633
    invoke-virtual {p0}, LX/4zI;->nextIndex()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 822629
    iget-boolean v0, p0, LX/4zI;->a:Z

    invoke-static {v0}, LX/0P6;->a(Z)V

    .line 822630
    iget-object v0, p0, LX/4zI;->b:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->remove()V

    .line 822631
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4zI;->a:Z

    .line 822632
    return-void
.end method

.method public final set(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 822626
    iget-boolean v0, p0, LX/4zI;->a:Z

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 822627
    iget-object v0, p0, LX/4zI;->b:Ljava/util/ListIterator;

    invoke-interface {v0, p1}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    .line 822628
    return-void
.end method
