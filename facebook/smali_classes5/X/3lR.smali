.class public final LX/3lR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 633911
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 633912
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 633913
    :goto_0
    return v1

    .line 633914
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 633915
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 633916
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 633917
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 633918
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 633919
    const-string v3, "composer_privacy_guardrail_info"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 633920
    invoke-static {p0, p1}, LX/3lS;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 633921
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 633922
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 633923
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 633924
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 633925
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 633926
    if-eqz v0, :cond_4

    .line 633927
    const-string v1, "composer_privacy_guardrail_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 633928
    const-wide/16 v4, 0x0

    .line 633929
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 633930
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 633931
    if-eqz v2, :cond_0

    .line 633932
    const-string v3, "current_privacy_option"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 633933
    invoke-static {p0, v2, p2, p3}, LX/4iA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 633934
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 633935
    if-eqz v2, :cond_1

    .line 633936
    const-string v3, "eligible_for_guardrail"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 633937
    invoke-virtual {p2, v2}, LX/0nX;->a(Z)V

    .line 633938
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {p0, v0, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 633939
    cmp-long v4, v2, v4

    if-eqz v4, :cond_2

    .line 633940
    const-string v4, "suggested_option_timestamp"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 633941
    invoke-virtual {p2, v2, v3}, LX/0nX;->a(J)V

    .line 633942
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {p0, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 633943
    if-eqz v2, :cond_3

    .line 633944
    const-string v3, "suggested_privacy_option"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 633945
    invoke-static {p0, v2, p2, p3}, LX/4iA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 633946
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 633947
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 633948
    return-void
.end method
