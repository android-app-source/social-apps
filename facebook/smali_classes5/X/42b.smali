.class public LX/42b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0Or",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:[B


# instance fields
.field private final c:LX/0S2;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 667514
    const-class v0, LX/42b;

    sput-object v0, LX/42b;->a:Ljava/lang/Class;

    .line 667515
    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, LX/42b;->b:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x1t
        0x8t
    .end array-data
.end method

.method public constructor <init>(LX/0S2;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0S2;",
            "LX/0Or",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 667516
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 667517
    iput-object p1, p0, LX/42b;->c:LX/0S2;

    .line 667518
    iput-object p2, p0, LX/42b;->d:LX/0Or;

    .line 667519
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x4

    .line 667520
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 667521
    sget-object v0, LX/42b;->b:[B

    invoke-virtual {v2, v1, v0}, LX/0SD;->a(B[B)V

    .line 667522
    iget-object v0, p0, LX/42b;->c:LX/0S2;

    invoke-virtual {v0}, LX/0S2;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 667523
    if-nez v0, :cond_0

    .line 667524
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 667525
    :cond_0
    iget-object v1, p0, LX/42b;->c:LX/0S2;

    invoke-virtual {v1, v0}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 667526
    :try_start_0
    iget-object v0, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v0

    .line 667527
    invoke-interface {v4, p0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 667528
    sget-object v1, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v0, v1, :cond_1

    .line 667529
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 667530
    :cond_1
    if-nez v0, :cond_2

    .line 667531
    const/4 v0, 0x4

    :try_start_1
    invoke-virtual {v2, v0}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 667532
    :try_start_2
    iget-object v0, p0, LX/42b;->c:LX/0S2;

    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 667533
    :try_start_3
    iget-object v0, p0, LX/42b;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 667534
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 667535
    if-nez v1, :cond_3

    .line 667536
    sget-object v0, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, p0, v0}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v0

    .line 667537
    :goto_1
    if-eqz v0, :cond_4

    .line 667538
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 667539
    :cond_2
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 667540
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 667541
    :catchall_1
    move-exception v0

    .line 667542
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 667543
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 667544
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 667545
    :cond_3
    :try_start_8
    invoke-interface {v4, p0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result-object v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method
