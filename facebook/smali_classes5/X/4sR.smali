.class public final LX/4sR;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public a:I

.field public b:I

.field public c:[Lcom/google/android/gms/common/api/Scope;

.field public d:Landroid/view/View;

.field private e:Landroid/view/View$OnClickListener;


# direct methods
.method public static a(Landroid/content/Context;II[Lcom/google/android/gms/common/api/Scope;)Landroid/widget/Button;
    .locals 7

    new-instance v0, LX/4t1;

    invoke-direct {v0, p0}, LX/4t1;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    move v2, v2

    const/high16 v6, 0x42400000    # 48.0f

    const/high16 v5, 0x3f000000    # 0.5f

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, LX/4t1;->setTypeface(Landroid/graphics/Typeface;)V

    const/high16 v3, 0x41600000    # 14.0f

    invoke-virtual {v0, v3}, LX/4t1;->setTextSize(F)V

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float v4, v3, v6

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-virtual {v0, v4}, LX/4t1;->setMinHeight(I)V

    mul-float/2addr v3, v6

    add-float/2addr v3, v5

    float-to-int v3, v3

    invoke-virtual {v0, v3}, LX/4t1;->setMinWidth(I)V

    if-eqz v2, :cond_4

    const v3, 0x7f0202cc

    const v4, 0x7f0202d1

    const v5, 0x7f0202cc

    invoke-static {p2, v3, v4, v5}, LX/4t1;->a(IIII)I

    move-result v3

    const v4, 0x7f0202d6

    const v5, 0x7f0202db

    const v6, 0x7f0202d6

    invoke-static {p2, v4, v5, v6}, LX/4t1;->a(IIII)I

    move-result v4

    invoke-static {p1, v3, v4}, LX/4t1;->a(III)I

    move-result v3

    :goto_1
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/4t1;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-static {v0, v1, p1, p2, v2}, LX/4t1;->b(LX/4t1;Landroid/content/res/Resources;IIZ)V

    return-object v0

    :cond_1
    array-length v5, p3

    move v4, v2

    :goto_2
    if-ge v4, v5, :cond_0

    aget-object v6, p3, v4

    iget-object p0, v6, Lcom/google/android/gms/common/api/Scope;->b:Ljava/lang/String;

    move-object v6, p0

    const-string p0, "/plus."

    invoke-virtual {v6, p0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_2

    const-string p0, "https://www.googleapis.com/auth/plus.me"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_2

    move v2, v3

    goto :goto_0

    :cond_2
    const-string p0, "https://www.googleapis.com/auth/games"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    move v2, v3

    goto :goto_0

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_4
    const v3, 0x7f0202b6

    const v4, 0x7f0202bb

    const v5, 0x7f0202bb

    invoke-static {p2, v3, v4, v5}, LX/4t1;->a(IIII)I

    move-result v3

    const v4, 0x7f0202c0

    const v5, 0x7f0202c5

    const v6, 0x7f0202c5

    invoke-static {p2, v4, v5, v6}, LX/4t1;->a(IIII)I

    move-result v4

    invoke-static {p1, v3, v4}, LX/4t1;->a(III)I

    move-result v3

    goto :goto_1
.end method

.method private a(II[Lcom/google/android/gms/common/api/Scope;)V
    .locals 2

    iput p1, p0, LX/4sR;->a:I

    iput p2, p0, LX/4sR;->b:I

    iput-object p3, p0, LX/4sR;->c:[Lcom/google/android/gms/common/api/Scope;

    invoke-virtual {p0}, LX/4sR;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/4sR;->d:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/4sR;->d:Landroid/view/View;

    invoke-virtual {p0, v1}, LX/4sR;->removeView(Landroid/view/View;)V

    :cond_0
    :try_start_0
    iget v1, p0, LX/4sR;->a:I

    iget p1, p0, LX/4sR;->b:I

    iget-object p2, p0, LX/4sR;->c:[Lcom/google/android/gms/common/api/Scope;

    sget-object p3, LX/4t0;->a:LX/4t0;

    invoke-static {p3, v0, v1, p1, p2}, LX/4t0;->b(LX/4t0;Landroid/content/Context;II[Lcom/google/android/gms/common/api/Scope;)Landroid/view/View;

    move-result-object p3

    move-object v1, p3

    iput-object v1, p0, LX/4sR;->d:Landroid/view/View;
    :try_end_0
    .catch LX/4tj; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, LX/4sR;->d:Landroid/view/View;

    invoke-virtual {p0, v1}, LX/4sR;->addView(Landroid/view/View;)V

    iget-object v1, p0, LX/4sR;->d:Landroid/view/View;

    invoke-virtual {p0}, LX/4sR;->isEnabled()Z

    move-result p1

    invoke-virtual {v1, p1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, LX/4sR;->d:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :catch_0
    const-string v1, "SignInButton"

    const-string p1, "Sign in button not found, using placeholder instead"

    invoke-static {v1, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, LX/4sR;->a:I

    iget p1, p0, LX/4sR;->b:I

    iget-object p2, p0, LX/4sR;->c:[Lcom/google/android/gms/common/api/Scope;

    invoke-static {v0, v1, p1, p2}, LX/4sR;->a(Landroid/content/Context;II[Lcom/google/android/gms/common/api/Scope;)Landroid/widget/Button;

    move-result-object v1

    iput-object v1, p0, LX/4sR;->d:Landroid/view/View;

    goto :goto_0
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x603f5df5

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    iget-object v1, p0, LX/4sR;->e:Landroid/view/View$OnClickListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/4sR;->d:Landroid/view/View;

    if-ne p1, v1, :cond_0

    iget-object v1, p0, LX/4sR;->e:Landroid/view/View$OnClickListener;

    invoke-interface {v1, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_0
    const v1, -0x5e867ff4

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final setColorScheme(I)V
    .locals 2

    iget v0, p0, LX/4sR;->a:I

    iget-object v1, p0, LX/4sR;->c:[Lcom/google/android/gms/common/api/Scope;

    invoke-direct {p0, v0, p1, v1}, LX/4sR;->a(II[Lcom/google/android/gms/common/api/Scope;)V

    return-void
.end method

.method public final setEnabled(Z)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    iget-object v0, p0, LX/4sR;->d:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method public final setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iput-object p1, p0, LX/4sR;->e:Landroid/view/View$OnClickListener;

    iget-object v0, p0, LX/4sR;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4sR;->d:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public final setScopes([Lcom/google/android/gms/common/api/Scope;)V
    .locals 2

    iget v0, p0, LX/4sR;->a:I

    iget v1, p0, LX/4sR;->b:I

    invoke-direct {p0, v0, v1, p1}, LX/4sR;->a(II[Lcom/google/android/gms/common/api/Scope;)V

    return-void
.end method

.method public final setSize(I)V
    .locals 2

    iget v0, p0, LX/4sR;->b:I

    iget-object v1, p0, LX/4sR;->c:[Lcom/google/android/gms/common/api/Scope;

    invoke-direct {p0, p1, v0, v1}, LX/4sR;->a(II[Lcom/google/android/gms/common/api/Scope;)V

    return-void
.end method
