.class public LX/46A;
.super LX/0WC;
.source ""


# static fields
.field private static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 670863
    sget-object v1, LX/01Q;->f:[Ljava/lang/String;

    move-object v2, v1

    .line 670864
    new-instance v3, LX/0cA;

    invoke-direct {v3}, LX/0cA;-><init>()V

    .line 670865
    array-length v4, v2

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v5, v2, v1

    .line 670866
    const-string v6, "en"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 670867
    invoke-virtual {v3, v5}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 670868
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 670869
    :cond_0
    const/4 v0, 0x1

    goto :goto_1

    .line 670870
    :cond_1
    invoke-virtual {v3}, LX/0cA;->b()LX/0Rf;

    move-result-object v1

    sput-object v1, LX/46A;->b:LX/0Rf;

    .line 670871
    if-eqz v0, :cond_2

    .line 670872
    const-string v0, "en"

    invoke-virtual {v3, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 670873
    :cond_2
    invoke-virtual {v3}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    sput-object v0, LX/46A;->a:LX/0Rf;

    .line 670874
    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 670883
    invoke-direct {p0}, LX/0WC;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 670882
    sget-object v0, LX/46A;->a:LX/0Rf;

    return-object v0
.end method

.method public final a(LX/0ea;)LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ea;",
            ")",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 670875
    sget-object v0, LX/469;->a:[I

    invoke-virtual {p1}, LX/0ea;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 670876
    :cond_0
    :goto_0
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 670877
    :goto_1
    return-object v0

    .line 670878
    :pswitch_0
    goto :goto_0

    .line 670879
    :pswitch_1
    const/16 v0, 0x1

    move v0, v0

    .line 670880
    if-eqz v0, :cond_0

    .line 670881
    sget-object v0, LX/46A;->b:LX/0Rf;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
