.class public LX/4AN;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static b:LX/4AO;

.field private static volatile c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 676136
    const-class v0, LX/4AN;

    sput-object v0, LX/4AN;->a:Ljava/lang/Class;

    .line 676137
    const/4 v0, 0x0

    sput-boolean v0, LX/4AN;->c:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 676135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()LX/1bk;
    .locals 1

    .prologue
    .line 676122
    sget-object v0, LX/4AN;->b:LX/4AO;

    invoke-virtual {v0}, LX/4AO;->b()LX/1bk;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;LX/1H6;LX/4AM;)V
    .locals 2
    .param p1    # LX/1H6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/4AM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 676124
    sget-boolean v0, LX/4AN;->c:Z

    if-eqz v0, :cond_0

    .line 676125
    sget-object v0, LX/4AN;->a:Ljava/lang/Class;

    const-string v1, "Fresco has already been initialized! `Fresco.initialize(...)` should only be called 1 single time to avoid memory leaks!"

    invoke-static {v0, v1}, LX/03J;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 676126
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 676127
    if-nez p1, :cond_1

    .line 676128
    invoke-static {v0}, LX/1H6;->a(Landroid/content/Context;)LX/1H8;

    move-result-object v1

    invoke-virtual {v1}, LX/1H8;->c()LX/1H6;

    move-result-object v1

    invoke-static {v1}, LX/1FE;->a(LX/1H6;)V

    .line 676129
    :goto_1
    new-instance v1, LX/4AO;

    invoke-direct {v1, v0, p2}, LX/4AO;-><init>(Landroid/content/Context;LX/4AM;)V

    .line 676130
    sput-object v1, LX/4AN;->b:LX/4AO;

    .line 676131
    sput-object v1, Lcom/facebook/drawee/view/SimpleDraweeView;->a:LX/1Gd;

    .line 676132
    return-void

    .line 676133
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, LX/4AN;->c:Z

    goto :goto_0

    .line 676134
    :cond_1
    invoke-static {p1}, LX/1FE;->a(LX/1H6;)V

    goto :goto_1
.end method

.method public static b()LX/1HI;
    .locals 1

    .prologue
    .line 676123
    invoke-static {}, LX/1FE;->a()LX/1FE;

    move-result-object v0

    invoke-virtual {v0}, LX/1FE;->h()LX/1HI;

    move-result-object v0

    return-object v0
.end method
