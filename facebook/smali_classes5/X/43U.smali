.class public final LX/43U;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/1MS;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/1MS;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method private constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 669079
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 669080
    iput-object p1, p0, LX/43U;->a:LX/0QB;

    .line 669081
    return-void
.end method

.method public static a(LX/0QB;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "Ljava/util/Set",
            "<",
            "LX/1MS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 669082
    new-instance v0, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    new-instance v2, LX/43U;

    invoke-direct {v2, p0}, LX/43U;-><init>(LX/0QB;)V

    invoke-direct {v0, v1, v2}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 669083
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/43U;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 669084
    packed-switch p2, :pswitch_data_0

    .line 669085
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 669086
    :pswitch_0
    invoke-static {p1}, LX/3Rl;->a(LX/0QB;)LX/3Rl;

    move-result-object v0

    .line 669087
    :goto_0
    return-object v0

    .line 669088
    :pswitch_1
    invoke-static {p1}, LX/7m4;->a(LX/0QB;)LX/7m4;

    move-result-object v0

    goto :goto_0

    .line 669089
    :pswitch_2
    invoke-static {p1}, LX/6N6;->a(LX/0QB;)LX/6N6;

    move-result-object v0

    goto :goto_0

    .line 669090
    :pswitch_3
    invoke-static {p1}, LX/1MU;->a(LX/0QB;)LX/1MU;

    move-result-object v0

    goto :goto_0

    .line 669091
    :pswitch_4
    invoke-static {p1}, LX/1MR;->a(LX/0QB;)LX/1MR;

    move-result-object v0

    goto :goto_0

    .line 669092
    :pswitch_5
    invoke-static {p1}, LX/Jeo;->a(LX/0QB;)LX/Jeo;

    move-result-object v0

    goto :goto_0

    .line 669093
    :pswitch_6
    invoke-static {p1}, LX/Jep;->a(LX/0QB;)LX/Jep;

    move-result-object v0

    goto :goto_0

    .line 669094
    :pswitch_7
    invoke-static {p1}, LX/Jer;->a(LX/0QB;)LX/Jer;

    move-result-object v0

    goto :goto_0

    .line 669095
    :pswitch_8
    invoke-static {p1}, LX/Jm0;->a(LX/0QB;)LX/Jm0;

    move-result-object v0

    goto :goto_0

    .line 669096
    :pswitch_9
    invoke-static {p1}, LX/3QS;->a(LX/0QB;)LX/3QS;

    move-result-object v0

    goto :goto_0

    .line 669097
    :pswitch_a
    invoke-static {p1}, LX/DqF;->a(LX/0QB;)LX/DqF;

    move-result-object v0

    goto :goto_0

    .line 669098
    :pswitch_b
    invoke-static {p1}, LX/6n7;->getInstance__com_facebook_omnistore_module_OmnistoreExtraFileProvider__INJECTED_BY_TemplateInjector(LX/0QB;)LX/6n7;

    move-result-object v0

    goto :goto_0

    .line 669099
    :pswitch_c
    invoke-static {p1}, LX/2Pt;->getInstance__com_facebook_omnistore_module_OmnistoreInitTimeBugReportInfo__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2Pt;

    move-result-object v0

    goto :goto_0

    .line 669100
    :pswitch_d
    new-instance p2, LX/Ckz;

    invoke-static {p1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v1

    check-cast v1, LX/0lB;

    invoke-static {p1}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v2

    check-cast v2, LX/0So;

    invoke-static {p1}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object p0

    check-cast p0, LX/0W3;

    invoke-direct {p2, v0, v1, v2, p0}, LX/Ckz;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lB;LX/0So;LX/0W3;)V

    .line 669101
    move-object v0, p2

    .line 669102
    goto :goto_0

    .line 669103
    :pswitch_e
    invoke-static {p1}, LX/2S9;->a(LX/0QB;)LX/2S9;

    move-result-object v0

    goto :goto_0

    .line 669104
    :pswitch_f
    invoke-static {p1}, LX/2Sd;->a(LX/0QB;)LX/2Sd;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 669105
    const/16 v0, 0x10

    return v0
.end method
