.class public LX/3uA;
.super LX/3u8;
.source ""

# interfaces
.implements LX/3u9;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private o:Landroid/support/v4/app/Fragment;

.field private p:Landroid/support/v7/internal/widget/NativeActionModeAwareLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/Window;LX/3u2;)V
    .locals 0

    .prologue
    .line 648146
    invoke-direct {p0, p1, p2, p3}, LX/3u8;-><init>(Landroid/content/Context;Landroid/view/Window;LX/3u2;)V

    .line 648147
    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/Fragment;LX/3uM;LX/3u2;)V
    .locals 0

    .prologue
    .line 648160
    invoke-direct {p0, p1, p2, p3}, LX/3u8;-><init>(Landroid/support/v4/app/Fragment;LX/3uM;LX/3u2;)V

    .line 648161
    iput-object p1, p0, LX/3uA;->o:Landroid/support/v4/app/Fragment;

    .line 648162
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 3

    .prologue
    .line 648155
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 648156
    new-instance v1, LX/3ue;

    invoke-direct {v1, v0, p2}, LX/3ue;-><init>(Landroid/content/Context;Landroid/view/ActionMode$Callback;)V

    invoke-virtual {p0, v1}, LX/3u8;->b(LX/3uG;)LX/3uV;

    move-result-object v1

    .line 648157
    if-eqz v1, :cond_0

    .line 648158
    new-instance v0, LX/3uf;

    iget-object v2, p0, LX/3u6;->a:Landroid/content/Context;

    invoke-direct {v0, v2, v1}, LX/3uf;-><init>(Landroid/content/Context;LX/3uV;)V

    .line 648159
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 648148
    iget-object v0, p0, LX/3uA;->o:Landroid/support/v4/app/Fragment;

    if-nez v0, :cond_1

    const v0, 0x1020002

    .line 648149
    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/NativeActionModeAwareLayout;

    iput-object v0, p0, LX/3uA;->p:Landroid/support/v7/internal/widget/NativeActionModeAwareLayout;

    .line 648150
    iget-object v0, p0, LX/3uA;->p:Landroid/support/v7/internal/widget/NativeActionModeAwareLayout;

    if-eqz v0, :cond_0

    .line 648151
    iget-object v0, p0, LX/3uA;->p:Landroid/support/v7/internal/widget/NativeActionModeAwareLayout;

    .line 648152
    iput-object p0, v0, Landroid/support/v7/internal/widget/NativeActionModeAwareLayout;->a:LX/3u9;

    .line 648153
    :cond_0
    return-void

    .line 648154
    :cond_1
    const v0, 0x7f0d0003

    goto :goto_0
.end method
