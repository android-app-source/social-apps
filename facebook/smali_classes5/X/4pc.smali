.class public final LX/4pc;
.super Ljava/io/InputStream;
.source ""


# instance fields
.field public final a:LX/12A;

.field public final b:Ljava/io/InputStream;

.field public c:[B

.field public d:I

.field public final e:I


# direct methods
.method public constructor <init>(LX/12A;Ljava/io/InputStream;[BII)V
    .locals 0

    .prologue
    .line 811513
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 811514
    iput-object p1, p0, LX/4pc;->a:LX/12A;

    .line 811515
    iput-object p2, p0, LX/4pc;->b:Ljava/io/InputStream;

    .line 811516
    iput-object p3, p0, LX/4pc;->c:[B

    .line 811517
    iput p4, p0, LX/4pc;->d:I

    .line 811518
    iput p5, p0, LX/4pc;->e:I

    .line 811519
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 811507
    iget-object v0, p0, LX/4pc;->c:[B

    .line 811508
    if-eqz v0, :cond_0

    .line 811509
    const/4 v1, 0x0

    iput-object v1, p0, LX/4pc;->c:[B

    .line 811510
    iget-object v1, p0, LX/4pc;->a:LX/12A;

    if-eqz v1, :cond_0

    .line 811511
    iget-object v1, p0, LX/4pc;->a:LX/12A;

    invoke-virtual {v1, v0}, LX/12A;->a([B)V

    .line 811512
    :cond_0
    return-void
.end method


# virtual methods
.method public final available()I
    .locals 2

    .prologue
    .line 811504
    iget-object v0, p0, LX/4pc;->c:[B

    if-eqz v0, :cond_0

    .line 811505
    iget v0, p0, LX/4pc;->e:I

    iget v1, p0, LX/4pc;->d:I

    sub-int/2addr v0, v1

    .line 811506
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/4pc;->b:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    goto :goto_0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 811501
    invoke-direct {p0}, LX/4pc;->a()V

    .line 811502
    iget-object v0, p0, LX/4pc;->b:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 811503
    return-void
.end method

.method public final mark(I)V
    .locals 1

    .prologue
    .line 811498
    iget-object v0, p0, LX/4pc;->c:[B

    if-nez v0, :cond_0

    .line 811499
    iget-object v0, p0, LX/4pc;->b:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->mark(I)V

    .line 811500
    :cond_0
    return-void
.end method

.method public final markSupported()Z
    .locals 1

    .prologue
    .line 811497
    iget-object v0, p0, LX/4pc;->c:[B

    if-nez v0, :cond_0

    iget-object v0, p0, LX/4pc;->b:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->markSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final read()I
    .locals 3

    .prologue
    .line 811469
    iget-object v0, p0, LX/4pc;->c:[B

    if-eqz v0, :cond_1

    .line 811470
    iget-object v0, p0, LX/4pc;->c:[B

    iget v1, p0, LX/4pc;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4pc;->d:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    .line 811471
    iget v1, p0, LX/4pc;->d:I

    iget v2, p0, LX/4pc;->e:I

    if-lt v1, v2, :cond_0

    .line 811472
    invoke-direct {p0}, LX/4pc;->a()V

    .line 811473
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/4pc;->b:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    goto :goto_0
.end method

.method public final read([B)I
    .locals 2

    .prologue
    .line 811496
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, LX/4pc;->read([BII)I

    move-result v0

    return v0
.end method

.method public final read([BII)I
    .locals 2

    .prologue
    .line 811488
    iget-object v0, p0, LX/4pc;->c:[B

    if-eqz v0, :cond_2

    .line 811489
    iget v0, p0, LX/4pc;->e:I

    iget v1, p0, LX/4pc;->d:I

    sub-int/2addr v0, v1

    .line 811490
    if-le p3, v0, :cond_0

    move p3, v0

    .line 811491
    :cond_0
    iget-object v0, p0, LX/4pc;->c:[B

    iget v1, p0, LX/4pc;->d:I

    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 811492
    iget v0, p0, LX/4pc;->d:I

    add-int/2addr v0, p3

    iput v0, p0, LX/4pc;->d:I

    .line 811493
    iget v0, p0, LX/4pc;->d:I

    iget v1, p0, LX/4pc;->e:I

    if-lt v0, v1, :cond_1

    .line 811494
    invoke-direct {p0}, LX/4pc;->a()V

    .line 811495
    :cond_1
    :goto_0
    return p3

    :cond_2
    iget-object v0, p0, LX/4pc;->b:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result p3

    goto :goto_0
.end method

.method public final reset()V
    .locals 1

    .prologue
    .line 811485
    iget-object v0, p0, LX/4pc;->c:[B

    if-nez v0, :cond_0

    .line 811486
    iget-object v0, p0, LX/4pc;->b:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V

    .line 811487
    :cond_0
    return-void
.end method

.method public final skip(J)J
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    .line 811474
    iget-object v0, p0, LX/4pc;->c:[B

    if-eqz v0, :cond_2

    .line 811475
    iget v0, p0, LX/4pc;->e:I

    iget v1, p0, LX/4pc;->d:I

    sub-int v4, v0, v1

    .line 811476
    int-to-long v0, v4

    cmp-long v0, v0, p1

    if-lez v0, :cond_0

    .line 811477
    iget v0, p0, LX/4pc;->d:I

    long-to-int v1, p1

    add-int/2addr v0, v1

    iput v0, p0, LX/4pc;->d:I

    .line 811478
    :goto_0
    return-wide p1

    .line 811479
    :cond_0
    invoke-direct {p0}, LX/4pc;->a()V

    .line 811480
    int-to-long v0, v4

    add-long/2addr v0, v2

    .line 811481
    int-to-long v4, v4

    sub-long/2addr p1, v4

    .line 811482
    :goto_1
    cmp-long v2, p1, v2

    if-lez v2, :cond_1

    .line 811483
    iget-object v2, p0, LX/4pc;->b:Ljava/io/InputStream;

    invoke-virtual {v2, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v2

    add-long/2addr v0, v2

    :cond_1
    move-wide p1, v0

    .line 811484
    goto :goto_0

    :cond_2
    move-wide v0, v2

    goto :goto_1
.end method
