.class public LX/4mQ;
.super Landroid/animation/ValueAnimator;
.source ""


# instance fields
.field public final a:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

.field public final b:Landroid/animation/ObjectAnimator;

.field public c:F

.field public d:F

.field public e:F

.field public f:F

.field public g:F

.field private h:Ljava/lang/reflect/Method;

.field public i:Ljava/lang/Class;

.field private j:Z

.field public k:Ljava/lang/Number;

.field public l:Ljava/lang/Number;

.field public m:F

.field public n:J

.field public o:I

.field public p:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Landroid/animation/ValueAnimator$AnimatorUpdateListener;",
            ">;"
        }
    .end annotation
.end field

.field public q:Z


# direct methods
.method public constructor <init>(Landroid/animation/ObjectAnimator;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 805777
    invoke-direct {p0}, Landroid/animation/ValueAnimator;-><init>()V

    .line 805778
    new-instance v0, LX/4mN;

    invoke-direct {v0, p0}, LX/4mN;-><init>(LX/4mQ;)V

    iput-object v0, p0, LX/4mQ;->a:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 805779
    const v0, 0x3dcccccd    # 0.1f

    iput v0, p0, LX/4mQ;->c:F

    .line 805780
    const v0, 0x3f333333    # 0.7f

    iput v0, p0, LX/4mQ;->d:F

    .line 805781
    const v0, 0x3c23d70a    # 0.01f

    iput v0, p0, LX/4mQ;->e:F

    .line 805782
    const v0, 0x3f19999a    # 0.6f

    iput v0, p0, LX/4mQ;->f:F

    .line 805783
    const/4 v0, 0x0

    iput v0, p0, LX/4mQ;->g:F

    .line 805784
    iput-boolean v2, p0, LX/4mQ;->j:Z

    .line 805785
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 805786
    iput-object v0, p0, LX/4mQ;->p:LX/0Px;

    .line 805787
    iput-object p1, p0, LX/4mQ;->b:Landroid/animation/ObjectAnimator;

    .line 805788
    iget-object v0, p0, LX/4mQ;->b:Landroid/animation/ObjectAnimator;

    new-instance v1, LX/4mP;

    invoke-direct {v1, p0}, LX/4mP;-><init>(LX/4mQ;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 805789
    iget-object v0, p0, LX/4mQ;->b:Landroid/animation/ObjectAnimator;

    new-instance v1, LX/4mO;

    invoke-direct {v1, p0}, LX/4mO;-><init>(LX/4mQ;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 805790
    return-void
.end method

.method private a(Ljava/lang/Float;)LX/4mQ;
    .locals 4

    .prologue
    .line 805791
    const-class v0, Ljava/lang/Float;

    iput-object v0, p0, LX/4mQ;->i:Ljava/lang/Class;

    .line 805792
    iput-object p1, p0, LX/4mQ;->l:Ljava/lang/Number;

    .line 805793
    invoke-virtual {p0}, LX/4mQ;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 805794
    invoke-static {p0}, LX/4mQ;->h(LX/4mQ;)Ljava/lang/Number;

    move-result-object v0

    iput-object v0, p0, LX/4mQ;->k:Ljava/lang/Number;

    .line 805795
    iget-object v1, p0, LX/4mQ;->b:Landroid/animation/ObjectAnimator;

    const/4 v0, 0x2

    new-array v2, v0, [F

    const/4 v3, 0x0

    iget-object v0, p0, LX/4mQ;->k:Ljava/lang/Number;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v2, v3

    const/4 v3, 0x1

    iget-object v0, p0, LX/4mQ;->l:Ljava/lang/Number;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v2, v3

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 805796
    iget-object v0, p0, LX/4mQ;->b:Landroid/animation/ObjectAnimator;

    const-wide/32 v2, 0x7fffffff

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 805797
    :cond_0
    return-object p0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/String;F)LX/4mQ;
    .locals 2

    .prologue
    .line 805798
    invoke-static {p0, p1}, LX/4mQ;->a(Ljava/lang/Object;Ljava/lang/String;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 805799
    new-instance v1, LX/4mQ;

    invoke-direct {v1, v0}, LX/4mQ;-><init>(Landroid/animation/ObjectAnimator;)V

    .line 805800
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-direct {v1, v0}, LX/4mQ;->a(Ljava/lang/Float;)LX/4mQ;

    .line 805801
    return-object v1
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/String;FF)LX/4mQ;
    .locals 2

    .prologue
    .line 805802
    invoke-static {p0, p1}, LX/4mQ;->a(Ljava/lang/Object;Ljava/lang/String;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 805803
    new-instance v1, LX/4mQ;

    invoke-direct {v1, v0}, LX/4mQ;-><init>(Landroid/animation/ObjectAnimator;)V

    .line 805804
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v1, LX/4mQ;->k:Ljava/lang/Number;

    .line 805805
    const/4 v0, 0x1

    iput-boolean v0, v1, LX/4mQ;->j:Z

    .line 805806
    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-direct {v1, v0}, LX/4mQ;->a(Ljava/lang/Float;)LX/4mQ;

    .line 805807
    return-object v1
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/String;)Landroid/animation/ObjectAnimator;
    .locals 1

    .prologue
    .line 805808
    new-instance v0, Landroid/animation/ObjectAnimator;

    invoke-direct {v0}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 805809
    invoke-virtual {v0, p0}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 805810
    invoke-virtual {v0, p1}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 805811
    return-object v0
.end method

.method private b(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/reflect/Method;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 805812
    iget-object v0, p0, LX/4mQ;->h:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 805813
    invoke-virtual {p2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    .line 805814
    const/4 v1, 0x1

    invoke-virtual {p2, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 805815
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "get"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 805816
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 805817
    const/4 v2, 0x0

    :try_start_0
    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v1, v0, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, LX/4mQ;->h:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 805818
    :cond_0
    iget-object v0, p0, LX/4mQ;->h:Ljava/lang/reflect/Method;

    return-object v0

    .line 805819
    :catch_0
    move-exception v0

    .line 805820
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    .line 805821
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No getter found for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 805822
    iget-object v0, p0, LX/4mQ;->b:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, LX/4mQ;->a:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->removeUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 805823
    return-void
.end method

.method private d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 805824
    iget-object v0, p0, LX/4mQ;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->getTarget()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 805825
    iget-object v0, p0, LX/4mQ;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->getPropertyName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static h(LX/4mQ;)Ljava/lang/Number;
    .locals 4

    .prologue
    .line 805826
    invoke-direct {p0}, LX/4mQ;->d()Ljava/lang/Object;

    move-result-object v0

    .line 805827
    invoke-direct {p0}, LX/4mQ;->e()Ljava/lang/String;

    move-result-object v1

    .line 805828
    invoke-direct {p0, v0, v1}, LX/4mQ;->b(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 805829
    const/4 v2, 0x0

    :try_start_0
    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 805830
    :catch_0
    move-exception v0

    .line 805831
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 805832
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error occurred invoking "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, LX/4mQ;->d()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private i()LX/4mQ;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 805833
    iget-object v0, p0, LX/4mQ;->i:Ljava/lang/Class;

    const-class v1, Ljava/lang/Float;

    if-ne v0, v1, :cond_0

    .line 805834
    invoke-direct {p0}, LX/4mQ;->d()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0}, LX/4mQ;->e()Ljava/lang/String;

    move-result-object v1

    .line 805835
    iget-object v3, p0, LX/4mQ;->l:Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->floatValue()F

    move-result v3

    move v3, v3

    .line 805836
    invoke-static {v0, v1, v3}, LX/4mQ;->a(Ljava/lang/Object;Ljava/lang/String;F)LX/4mQ;

    move-result-object v0

    move-object v1, v0

    .line 805837
    :goto_0
    invoke-virtual {v1}, LX/4mQ;->getListeners()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 805838
    invoke-virtual {p0}, LX/4mQ;->getListeners()Ljava/util/ArrayList;

    move-result-object v4

    .line 805839
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v2

    .line 805840
    :goto_1
    if-ge v3, v5, :cond_2

    .line 805841
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v0}, LX/4mQ;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 805842
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 805843
    :cond_0
    invoke-direct {p0}, LX/4mQ;->d()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0}, LX/4mQ;->e()Ljava/lang/String;

    move-result-object v1

    .line 805844
    iget-object v3, p0, LX/4mQ;->l:Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v3

    move v3, v3

    .line 805845
    invoke-static {v0, v1}, LX/4mQ;->a(Ljava/lang/Object;Ljava/lang/String;)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 805846
    new-instance v7, LX/4mQ;

    invoke-direct {v7, v6}, LX/4mQ;-><init>(Landroid/animation/ObjectAnimator;)V

    .line 805847
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 805848
    const-class v8, Ljava/lang/Integer;

    iput-object v8, v7, LX/4mQ;->i:Ljava/lang/Class;

    .line 805849
    iput-object v6, v7, LX/4mQ;->l:Ljava/lang/Number;

    .line 805850
    invoke-virtual {v7}, LX/4mQ;->isStarted()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 805851
    invoke-static {v7}, LX/4mQ;->h(LX/4mQ;)Ljava/lang/Number;

    move-result-object v8

    iput-object v8, v7, LX/4mQ;->k:Ljava/lang/Number;

    .line 805852
    iget-object v9, v7, LX/4mQ;->b:Landroid/animation/ObjectAnimator;

    const/4 v8, 0x2

    new-array v10, v8, [I

    const/4 v11, 0x0

    iget-object v8, v7, LX/4mQ;->k:Ljava/lang/Number;

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    aput v8, v10, v11

    const/4 v11, 0x1

    iget-object v8, v7, LX/4mQ;->l:Ljava/lang/Number;

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    aput v8, v10, v11

    invoke-virtual {v9, v10}, Landroid/animation/ObjectAnimator;->setIntValues([I)V

    .line 805853
    iget-object v8, v7, LX/4mQ;->b:Landroid/animation/ObjectAnimator;

    const-wide/32 v10, 0x7fffffff

    invoke-virtual {v8, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 805854
    :cond_1
    move-object v0, v7

    .line 805855
    move-object v1, v0

    goto :goto_0

    .line 805856
    :cond_2
    iget-object v0, p0, LX/4mQ;->p:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    .line 805857
    :goto_2
    if-ge v2, v3, :cond_3

    .line 805858
    iget-object v0, p0, LX/4mQ;->p:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v1, v0}, LX/4mQ;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 805859
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 805860
    :cond_3
    return-object v1
.end method


# virtual methods
.method public final addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V
    .locals 2

    .prologue
    .line 805861
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 805862
    iget-object v0, p0, LX/4mQ;->p:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 805863
    :goto_0
    return-void

    .line 805864
    :cond_0
    iget-object v0, p0, LX/4mQ;->p:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 805865
    iget-object v0, p0, LX/4mQ;->b:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, LX/4mQ;->a:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 805866
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 805867
    iget-object v1, p0, LX/4mQ;->p:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 805868
    invoke-virtual {v0, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 805869
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/4mQ;->p:LX/0Px;

    goto :goto_0
.end method

.method public final cancel()V
    .locals 2

    .prologue
    .line 805870
    iget-object v0, p0, LX/4mQ;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 805871
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/4mQ;->n:J

    .line 805872
    const/4 v0, 0x0

    iput v0, p0, LX/4mQ;->o:I

    .line 805873
    return-void
.end method

.method public final synthetic clone()Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 805737
    invoke-direct {p0}, LX/4mQ;->i()LX/4mQ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Landroid/animation/ValueAnimator;
    .locals 1

    .prologue
    .line 805776
    invoke-direct {p0}, LX/4mQ;->i()LX/4mQ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 805874
    invoke-direct {p0}, LX/4mQ;->i()LX/4mQ;

    move-result-object v0

    return-object v0
.end method

.method public final end()V
    .locals 2

    .prologue
    .line 805738
    iget-object v0, p0, LX/4mQ;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->end()V

    .line 805739
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/4mQ;->n:J

    .line 805740
    const/4 v0, 0x0

    iput v0, p0, LX/4mQ;->o:I

    .line 805741
    return-void
.end method

.method public final getDuration()J
    .locals 2

    .prologue
    .line 805742
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getDuration is not supported for SpringAnimator"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getStartDelay()J
    .locals 2

    .prologue
    .line 805743
    iget-object v0, p0, LX/4mQ;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->getStartDelay()J

    move-result-wide v0

    return-wide v0
.end method

.method public final isRunning()Z
    .locals 1

    .prologue
    .line 805744
    iget-object v0, p0, LX/4mQ;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    return v0
.end method

.method public final isStarted()Z
    .locals 1

    .prologue
    .line 805745
    iget-object v0, p0, LX/4mQ;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isStarted()Z

    move-result v0

    return v0
.end method

.method public final removeAllUpdateListeners()V
    .locals 1

    .prologue
    .line 805746
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 805747
    iput-object v0, p0, LX/4mQ;->p:LX/0Px;

    .line 805748
    invoke-direct {p0}, LX/4mQ;->c()V

    .line 805749
    return-void
.end method

.method public final removeUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V
    .locals 1

    .prologue
    .line 805750
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 805751
    iget-object v0, p0, LX/4mQ;->p:LX/0Px;

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    .line 805752
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 805753
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/4mQ;->p:LX/0Px;

    .line 805754
    iget-object v0, p0, LX/4mQ;->p:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 805755
    invoke-direct {p0}, LX/4mQ;->c()V

    .line 805756
    :cond_0
    return-void
.end method

.method public final bridge synthetic setDuration(J)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 805757
    invoke-virtual {p0, p1, p2}, LX/4mQ;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v0

    return-object v0
.end method

.method public final setDuration(J)Landroid/animation/ValueAnimator;
    .locals 2

    .prologue
    .line 805758
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "setDuration is not supported for SpringAnimator"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setEvaluator(Landroid/animation/TypeEvaluator;)V
    .locals 1

    .prologue
    .line 805759
    iget-object v0, p0, LX/4mQ;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p1}, Landroid/animation/ObjectAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 805760
    return-void
.end method

.method public final setInterpolator(Landroid/animation/TimeInterpolator;)V
    .locals 2

    .prologue
    .line 805761
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "setInterpolator is not supported for SpringAnimator"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setStartDelay(J)V
    .locals 1

    .prologue
    .line 805762
    iget-object v0, p0, LX/4mQ;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p1, p2}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 805763
    return-void
.end method

.method public final setTarget(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 805764
    iget-object v0, p0, LX/4mQ;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p1}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 805765
    return-void
.end method

.method public final start()V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 805766
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/4mQ;->n:J

    .line 805767
    iput v3, p0, LX/4mQ;->o:I

    .line 805768
    iget-boolean v0, p0, LX/4mQ;->j:Z

    if-nez v0, :cond_0

    .line 805769
    invoke-static {p0}, LX/4mQ;->h(LX/4mQ;)Ljava/lang/Number;

    move-result-object v0

    iput-object v0, p0, LX/4mQ;->k:Ljava/lang/Number;

    .line 805770
    :cond_0
    iget-object v0, p0, LX/4mQ;->k:Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    iput v0, p0, LX/4mQ;->m:F

    .line 805771
    iget-object v0, p0, LX/4mQ;->i:Ljava/lang/Class;

    const-class v1, Ljava/lang/Integer;

    if-ne v0, v1, :cond_1

    .line 805772
    iget-object v1, p0, LX/4mQ;->b:Landroid/animation/ObjectAnimator;

    new-array v2, v2, [I

    iget-object v0, p0, LX/4mQ;->k:Ljava/lang/Number;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v2, v3

    iget-object v0, p0, LX/4mQ;->l:Ljava/lang/Number;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v2, v4

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setIntValues([I)V

    .line 805773
    :goto_0
    iget-object v0, p0, LX/4mQ;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 805774
    return-void

    .line 805775
    :cond_1
    iget-object v1, p0, LX/4mQ;->b:Landroid/animation/ObjectAnimator;

    new-array v2, v2, [F

    iget-object v0, p0, LX/4mQ;->k:Ljava/lang/Number;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v2, v3

    iget-object v0, p0, LX/4mQ;->l:Ljava/lang/Number;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v2, v4

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    goto :goto_0
.end method
