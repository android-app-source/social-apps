.class public LX/3qO;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 642194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a([LX/3qK;)[Landroid/os/Bundle;
    .locals 6

    .prologue
    .line 642195
    if-nez p0, :cond_0

    .line 642196
    const/4 v0, 0x0

    .line 642197
    :goto_0
    return-object v0

    .line 642198
    :cond_0
    array-length v0, p0

    new-array v1, v0, [Landroid/os/Bundle;

    .line 642199
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 642200
    aget-object v2, p0, v0

    .line 642201
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 642202
    const-string v4, "resultKey"

    invoke-virtual {v2}, LX/3qK;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 642203
    const-string v4, "label"

    invoke-virtual {v2}, LX/3qK;->b()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 642204
    const-string v4, "choices"

    invoke-virtual {v2}, LX/3qK;->c()[Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putCharSequenceArray(Ljava/lang/String;[Ljava/lang/CharSequence;)V

    .line 642205
    const-string v4, "allowFreeFormInput"

    invoke-virtual {v2}, LX/3qK;->d()Z

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 642206
    const-string v4, "extras"

    invoke-virtual {v2}, LX/3qK;->e()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 642207
    move-object v2, v3

    .line 642208
    aput-object v2, v1, v0

    .line 642209
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 642210
    goto :goto_0
.end method
