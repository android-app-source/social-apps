.class public LX/48o;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:[F

.field public b:[F

.field public c:Lcom/facebook/csslayout/YogaDirection;

.field public d:F

.field public e:I

.field public f:Lcom/facebook/csslayout/YogaDirection;

.field public g:I

.field public h:[LX/48n;

.field public i:[F

.field public j:LX/48n;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 673204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 673205
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, LX/48o;->a:[F

    .line 673206
    new-array v0, v1, [F

    iput-object v0, p0, LX/48o;->b:[F

    .line 673207
    sget-object v0, Lcom/facebook/csslayout/YogaDirection;->LTR:Lcom/facebook/csslayout/YogaDirection;

    iput-object v0, p0, LX/48o;->c:Lcom/facebook/csslayout/YogaDirection;

    .line 673208
    const/16 v0, 0x10

    new-array v0, v0, [LX/48n;

    iput-object v0, p0, LX/48o;->h:[LX/48n;

    .line 673209
    new-array v0, v1, [F

    iput-object v0, p0, LX/48o;->i:[F

    .line 673210
    new-instance v0, LX/48n;

    invoke-direct {v0}, LX/48n;-><init>()V

    iput-object v0, p0, LX/48o;->j:LX/48n;

    .line 673211
    invoke-virtual {p0}, LX/48o;->a()V

    .line 673212
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/high16 v2, 0x7fc00000    # NaNf

    .line 673213
    iget-object v0, p0, LX/48o;->a:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 673214
    iget-object v0, p0, LX/48o;->b:[F

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([FF)V

    .line 673215
    sget-object v0, Lcom/facebook/csslayout/YogaDirection;->LTR:Lcom/facebook/csslayout/YogaDirection;

    iput-object v0, p0, LX/48o;->c:Lcom/facebook/csslayout/YogaDirection;

    .line 673216
    iput v2, p0, LX/48o;->d:F

    .line 673217
    iput v3, p0, LX/48o;->e:I

    .line 673218
    iput-object v4, p0, LX/48o;->f:Lcom/facebook/csslayout/YogaDirection;

    .line 673219
    iput v3, p0, LX/48o;->g:I

    .line 673220
    iget-object v0, p0, LX/48o;->i:[F

    aput v2, v0, v3

    .line 673221
    iget-object v0, p0, LX/48o;->i:[F

    const/4 v1, 0x1

    aput v2, v0, v1

    .line 673222
    iget-object v0, p0, LX/48o;->j:LX/48n;

    iput-object v4, v0, LX/48n;->c:Lcom/facebook/csslayout/YogaMeasureMode;

    .line 673223
    iget-object v0, p0, LX/48o;->j:LX/48n;

    iput-object v4, v0, LX/48n;->d:Lcom/facebook/csslayout/YogaMeasureMode;

    .line 673224
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 673225
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "layout: {left: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/48o;->a:[F

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", top: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/48o;->a:[F

    aget v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", width: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/48o;->b:[F

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", height: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/48o;->b:[F

    aget v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", direction: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/48o;->c:Lcom/facebook/csslayout/YogaDirection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
