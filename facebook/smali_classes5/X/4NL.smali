.class public LX/4NL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 692148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 22

    .prologue
    .line 692073
    const/16 v18, 0x0

    .line 692074
    const/16 v17, 0x0

    .line 692075
    const/16 v16, 0x0

    .line 692076
    const/4 v15, 0x0

    .line 692077
    const/4 v14, 0x0

    .line 692078
    const/4 v13, 0x0

    .line 692079
    const/4 v12, 0x0

    .line 692080
    const/4 v11, 0x0

    .line 692081
    const/4 v10, 0x0

    .line 692082
    const/4 v9, 0x0

    .line 692083
    const/4 v8, 0x0

    .line 692084
    const/4 v7, 0x0

    .line 692085
    const/4 v6, 0x0

    .line 692086
    const/4 v5, 0x0

    .line 692087
    const/4 v4, 0x0

    .line 692088
    const/4 v3, 0x0

    .line 692089
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    .line 692090
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 692091
    const/4 v3, 0x0

    .line 692092
    :goto_0
    return v3

    .line 692093
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 692094
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_11

    .line 692095
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v19

    .line 692096
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 692097
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_1

    if-eqz v19, :cond_1

    .line 692098
    const-string v20, "campaign_owner"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 692099
    invoke-static/range {p0 .. p1}, LX/2aw;->a(LX/15w;LX/186;)I

    move-result v18

    goto :goto_1

    .line 692100
    :cond_2
    const-string v20, "featured_video_attachments"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 692101
    invoke-static/range {p0 .. p1}, LX/2as;->b(LX/15w;LX/186;)I

    move-result v17

    goto :goto_1

    .line 692102
    :cond_3
    const-string v20, "feed_promotion_call_to_action"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 692103
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v16

    goto :goto_1

    .line 692104
    :cond_4
    const-string v20, "feed_promotion_favicon"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 692105
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v15

    goto :goto_1

    .line 692106
    :cond_5
    const-string v20, "feed_promotion_icon_image"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 692107
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v14

    goto :goto_1

    .line 692108
    :cond_6
    const-string v20, "feed_promotion_privacy_notice"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 692109
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 692110
    :cond_7
    const-string v20, "feed_promotion_title"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 692111
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 692112
    :cond_8
    const-string v20, "id"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 692113
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 692114
    :cond_9
    const-string v20, "media_set"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 692115
    invoke-static/range {p0 .. p1}, LX/4PN;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 692116
    :cond_a
    const-string v20, "photo_attachments"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 692117
    invoke-static/range {p0 .. p1}, LX/2as;->b(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 692118
    :cond_b
    const-string v20, "relationship_context"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_c

    .line 692119
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 692120
    :cond_c
    const-string v20, "share_preview_icon_image"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_d

    .line 692121
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 692122
    :cond_d
    const-string v20, "share_preview_story_placeholder"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_e

    .line 692123
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 692124
    :cond_e
    const-string v20, "share_preview_title"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_f

    .line 692125
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 692126
    :cond_f
    const-string v20, "url"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_10

    .line 692127
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 692128
    :cond_10
    const-string v20, "video_campaign"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 692129
    invoke-static/range {p0 .. p1}, LX/4Nk;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 692130
    :cond_11
    const/16 v19, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 692131
    const/16 v19, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 692132
    const/16 v18, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 692133
    const/16 v17, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 692134
    const/16 v16, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 692135
    const/4 v15, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 692136
    const/4 v14, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 692137
    const/4 v13, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 692138
    const/16 v12, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 692139
    const/16 v11, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 692140
    const/16 v10, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 692141
    const/16 v9, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 692142
    const/16 v8, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 692143
    const/16 v7, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 692144
    const/16 v6, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 692145
    const/16 v5, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 692146
    const/16 v4, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 692147
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 692006
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 692007
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692008
    if-eqz v0, :cond_0

    .line 692009
    const-string v1, "campaign_owner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692010
    invoke-static {p0, v0, p2, p3}, LX/2aw;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 692011
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692012
    if-eqz v0, :cond_1

    .line 692013
    const-string v1, "featured_video_attachments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692014
    invoke-static {p0, v0, p2, p3}, LX/2as;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 692015
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692016
    if-eqz v0, :cond_2

    .line 692017
    const-string v1, "feed_promotion_call_to_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692018
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 692019
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692020
    if-eqz v0, :cond_3

    .line 692021
    const-string v1, "feed_promotion_favicon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692022
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 692023
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692024
    if-eqz v0, :cond_4

    .line 692025
    const-string v1, "feed_promotion_icon_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692026
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 692027
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692028
    if-eqz v0, :cond_5

    .line 692029
    const-string v1, "feed_promotion_privacy_notice"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692030
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 692031
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692032
    if-eqz v0, :cond_6

    .line 692033
    const-string v1, "feed_promotion_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692034
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 692035
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 692036
    if-eqz v0, :cond_7

    .line 692037
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692038
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 692039
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692040
    if-eqz v0, :cond_8

    .line 692041
    const-string v1, "media_set"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692042
    invoke-static {p0, v0, p2, p3}, LX/4PN;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 692043
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692044
    if-eqz v0, :cond_9

    .line 692045
    const-string v1, "photo_attachments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692046
    invoke-static {p0, v0, p2, p3}, LX/2as;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 692047
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692048
    if-eqz v0, :cond_a

    .line 692049
    const-string v1, "relationship_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692050
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 692051
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692052
    if-eqz v0, :cond_b

    .line 692053
    const-string v1, "share_preview_icon_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692054
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 692055
    :cond_b
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692056
    if-eqz v0, :cond_c

    .line 692057
    const-string v1, "share_preview_story_placeholder"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692058
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 692059
    :cond_c
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692060
    if-eqz v0, :cond_d

    .line 692061
    const-string v1, "share_preview_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692062
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 692063
    :cond_d
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 692064
    if-eqz v0, :cond_e

    .line 692065
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692066
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 692067
    :cond_e
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692068
    if-eqz v0, :cond_f

    .line 692069
    const-string v1, "video_campaign"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692070
    invoke-static {p0, v0, p2, p3}, LX/4Nk;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 692071
    :cond_f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 692072
    return-void
.end method
