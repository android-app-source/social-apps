.class public LX/4mf;
.super Landroid/app/Dialog;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/ui/dialogs/FbDialogFragment;

.field private b:Ljava/lang/reflect/Field;

.field private c:Ljava/lang/reflect/Field;

.field private d:Ljava/lang/reflect/Field;

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/facebook/ui/dialogs/FbDialogFragment;Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 806063
    iput-object p1, p0, LX/4mf;->a:Lcom/facebook/ui/dialogs/FbDialogFragment;

    .line 806064
    invoke-direct {p0, p2, p3}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 806065
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4mf;->e:Z

    .line 806066
    iget-object v0, p0, LX/4mf;->a:Lcom/facebook/ui/dialogs/FbDialogFragment;

    iget-object v0, v0, Lcom/facebook/ui/dialogs/FbDialogFragment;->k:LX/0ad;

    sget-short v1, LX/3ie;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 806067
    if-eqz v0, :cond_0

    .line 806068
    const-class v0, Landroid/app/Dialog;

    .line 806069
    :try_start_0
    const-string v1, "mCancelMessage"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    iput-object v1, p0, LX/4mf;->b:Ljava/lang/reflect/Field;

    .line 806070
    const-string v1, "mDismissMessage"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    iput-object v1, p0, LX/4mf;->c:Ljava/lang/reflect/Field;

    .line 806071
    const-string v1, "mShowMessage"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    iput-object v0, p0, LX/4mf;->d:Ljava/lang/reflect/Field;

    .line 806072
    iget-object v0, p0, LX/4mf;->b:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 806073
    iget-object v0, p0, LX/4mf;->c:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 806074
    iget-object v0, p0, LX/4mf;->d:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 806075
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4mf;->e:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 806076
    :cond_0
    :goto_0
    return-void

    .line 806077
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 806078
    iget-object v0, p1, Lcom/facebook/ui/dialogs/FbDialogFragment;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "FbDialog"

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public dismiss()V
    .locals 3

    .prologue
    .line 806079
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 806080
    iget-boolean v0, p0, LX/4mf;->e:Z

    if-eqz v0, :cond_2

    .line 806081
    :try_start_0
    iget-object v0, p0, LX/4mf;->b:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Message;

    .line 806082
    if-eqz v0, :cond_0

    .line 806083
    invoke-virtual {v0}, Landroid/os/Message;->recycle()V

    .line 806084
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/4mf;->setCancelMessage(Landroid/os/Message;)V

    .line 806085
    :cond_0
    iget-object v0, p0, LX/4mf;->c:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Message;

    .line 806086
    if-eqz v0, :cond_1

    .line 806087
    invoke-virtual {v0}, Landroid/os/Message;->recycle()V

    .line 806088
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/4mf;->setDismissMessage(Landroid/os/Message;)V

    .line 806089
    :cond_1
    iget-object v0, p0, LX/4mf;->d:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Message;

    .line 806090
    if-eqz v0, :cond_2

    .line 806091
    invoke-virtual {v0}, Landroid/os/Message;->recycle()V

    .line 806092
    iget-object v0, p0, LX/4mf;->d:Ljava/lang/reflect/Field;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 806093
    :cond_2
    :goto_0
    return-void

    .line 806094
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 806095
    :goto_1
    iget-object v0, p0, LX/4mf;->a:Lcom/facebook/ui/dialogs/FbDialogFragment;

    iget-object v0, v0, Lcom/facebook/ui/dialogs/FbDialogFragment;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "FbDialog"

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 806096
    :catch_1
    move-exception v0

    move-object v1, v0

    goto :goto_1
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 806097
    iget-object v0, p0, LX/4mf;->a:Lcom/facebook/ui/dialogs/FbDialogFragment;

    iget-object v0, v0, Lcom/facebook/ui/dialogs/FbDialogFragment;->j:LX/4mg;

    invoke-virtual {v0, p1}, LX/4mg;->a(Landroid/view/MotionEvent;)V

    .line 806098
    invoke-super {p0, p1}, Landroid/app/Dialog;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
