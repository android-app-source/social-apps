.class public final LX/3gy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;

.field public final synthetic b:[I

.field public final synthetic c:Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;[I)V
    .locals 0

    .prologue
    .line 624949
    iput-object p1, p0, LX/3gy;->c:Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;

    iput-object p2, p0, LX/3gy;->a:Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;

    iput-object p3, p0, LX/3gy;->b:[I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 624950
    sget-object v0, LX/1zn;->a:Ljava/lang/Class;

    const-string v1, "Failed to fetch reaction assets from the server - "

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 624951
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 14

    .prologue
    .line 624952
    check-cast p1, Ljava/lang/Boolean;

    .line 624953
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 624954
    iget-object v0, p0, LX/3gy;->c:Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;

    iget-object v1, p0, LX/3gy;->a:Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;

    iget-object v2, p0, LX/3gy;->b:[I

    .line 624955
    iget-object v3, v0, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->a:LX/1zn;

    iget-object v3, v3, LX/1zn;->e:LX/1zi;

    invoke-interface {v3}, LX/1zi;->a()[I

    move-result-object v3

    .line 624956
    iget-object v4, v0, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->a:LX/1zn;

    iget-object v4, v4, LX/1zn;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    .line 624957
    invoke-virtual {v1}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->l()I

    move-result v5

    invoke-static {v5}, LX/1zs;->a(I)LX/0Tn;

    move-result-object v5

    invoke-virtual {v1}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->n()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 624958
    invoke-virtual {v1}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->l()I

    move-result v5

    invoke-static {v5}, LX/1zs;->b(I)LX/0Tn;

    move-result-object v5

    invoke-virtual {v1}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->k()Z

    move-result v6

    invoke-interface {v4, v5, v6}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    .line 624959
    invoke-virtual {v1}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->l()I

    move-result v5

    invoke-static {v5}, LX/1zs;->c(I)LX/0Tn;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "#"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v4, v5, v6}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 624960
    sget-object v6, LX/1zn;->b:[LX/1zo;

    array-length v7, v6

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v7, :cond_0

    aget-object v8, v6, v5

    .line 624961
    invoke-virtual {v1}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->l()I

    move-result v9

    invoke-virtual {v8}, LX/1zo;->name()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, LX/1zs;->a(ILjava/lang/String;)LX/0Tn;

    move-result-object v9

    iget-object v10, v0, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->a:LX/1zn;

    iget-object v10, v10, LX/1zn;->f:LX/1zk;

    invoke-virtual {v10, v1, v8}, LX/1zk;->c(Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;LX/1zo;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v4, v9, v10}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 624962
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 624963
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->l()I

    move-result v5

    .line 624964
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 624965
    array-length v8, v2

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v8, :cond_5

    aget v9, v2, v6

    .line 624966
    if-eq v9, v5, :cond_2

    const/4 v10, 0x0

    .line 624967
    array-length v12, v3

    move v11, v10

    :goto_2
    if-ge v11, v12, :cond_1

    aget v13, v3, v11

    .line 624968
    if-ne v13, v9, :cond_7

    .line 624969
    const/4 v10, 0x1

    .line 624970
    :cond_1
    move v10, v10

    .line 624971
    if-eqz v10, :cond_4

    .line 624972
    :cond_2
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v10

    if-eqz v10, :cond_3

    .line 624973
    const-string v10, ","

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 624974
    :cond_3
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 624975
    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 624976
    :cond_5
    sget-object v6, LX/1zs;->b:LX/0Tn;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v6, v7}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 624977
    invoke-interface {v4}, LX/0hN;->commit()V

    .line 624978
    iget-object v3, v0, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->a:LX/1zn;

    iget-object v3, v3, LX/1zn;->f:LX/1zk;

    sget-object v4, LX/1zo;->VECTOR:LX/1zo;

    invoke-virtual {v3, v1, v4}, LX/1zk;->c(Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;LX/1zo;)Ljava/lang/String;

    move-result-object v3

    .line 624979
    iget-object v4, v0, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->a:LX/1zn;

    iget-object v4, v4, LX/1zn;->m:LX/1zl;

    invoke-virtual {v1}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->l()I

    move-result v5

    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x1

    invoke-virtual {v4, v5, v6, v3}, LX/1zl;->a(ILjava/io/File;Z)V

    .line 624980
    :cond_6
    iget-object v0, p0, LX/3gy;->c:Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->a:LX/1zn;

    iget-object v0, v0, LX/1zn;->j:LX/1zq;

    iget-object v1, p0, LX/3gy;->a:Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;

    invoke-virtual {v1}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->l()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/1zq;->a(IZ)V

    .line 624981
    return-void

    .line 624982
    :cond_7
    add-int/lit8 v11, v11, 0x1

    goto :goto_2
.end method
