.class public LX/4fM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1cF",
        "<",
        "LX/1FL;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:[LX/4ey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LX/4ey",
            "<",
            "LX/1FL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>([LX/4ey;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "LX/4ey",
            "<",
            "LX/1FL;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 798344
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 798345
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4ey;

    iput-object v0, p0, LX/4fM;->a:[LX/4ey;

    .line 798346
    const/4 v0, 0x0

    iget-object v1, p0, LX/4fM;->a:[LX/4ey;

    array-length v1, v1

    invoke-static {v0, v1}, LX/03g;->a(II)I

    .line 798347
    return-void
.end method

.method public static a$redex0(LX/4fM;ILX/1cd;LX/1cW;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FL;",
            ">;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 798348
    iget-object v0, p3, LX/1cW;->a:LX/1bf;

    move-object v0, v0

    .line 798349
    iget-object v1, v0, LX/1bf;->h:LX/1o9;

    move-object v0, v1

    .line 798350
    :goto_0
    iget-object v1, p0, LX/4fM;->a:[LX/4ey;

    array-length v1, v1

    if-ge p1, v1, :cond_2

    .line 798351
    iget-object v1, p0, LX/4fM;->a:[LX/4ey;

    aget-object v1, v1, p1

    invoke-interface {v1, v0}, LX/4ey;->a(LX/1o9;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 798352
    :goto_1
    move v0, p1

    .line 798353
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 798354
    const/4 v0, 0x0

    .line 798355
    :goto_2
    return v0

    .line 798356
    :cond_0
    iget-object v1, p0, LX/4fM;->a:[LX/4ey;

    aget-object v1, v1, v0

    new-instance v2, LX/4fL;

    invoke-direct {v2, p0, p2, p3, v0}, LX/4fL;-><init>(LX/4fM;LX/1cd;LX/1cW;I)V

    invoke-interface {v1, v2, p3}, LX/1cF;->a(LX/1cd;LX/1cW;)V

    .line 798357
    const/4 v0, 0x1

    goto :goto_2

    .line 798358
    :cond_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 798359
    :cond_2
    const/4 p1, -0x1

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/1cd;LX/1cW;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FL;",
            ">;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 798360
    iget-object v0, p2, LX/1cW;->a:LX/1bf;

    move-object v0, v0

    .line 798361
    iget-object v3, v0, LX/1bf;->h:LX/1o9;

    move-object v0, v3

    .line 798362
    if-nez v0, :cond_1

    .line 798363
    invoke-virtual {p1, v2, v1}, LX/1cd;->b(Ljava/lang/Object;Z)V

    .line 798364
    :cond_0
    :goto_0
    return-void

    .line 798365
    :cond_1
    const/4 v0, 0x0

    invoke-static {p0, v0, p1, p2}, LX/4fM;->a$redex0(LX/4fM;ILX/1cd;LX/1cW;)Z

    move-result v0

    .line 798366
    if-nez v0, :cond_0

    .line 798367
    invoke-virtual {p1, v2, v1}, LX/1cd;->b(Ljava/lang/Object;Z)V

    goto :goto_0
.end method
