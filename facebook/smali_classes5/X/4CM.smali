.class public LX/4CM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1c8;


# instance fields
.field public final a:LX/1G9;

.field public final b:Ljava/util/concurrent/ScheduledExecutorService;

.field public final c:LX/0So;

.field public final d:LX/1FZ;

.field private final e:LX/1Fg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Fg",
            "<",
            "LX/1bh;",
            "LX/1ln;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/1Gd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Gd",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1G9;Ljava/util/concurrent/ScheduledExecutorService;LX/0So;LX/1FZ;LX/1Fg;LX/1Gd;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1G9;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/0So;",
            "LX/1FZ;",
            "LX/1Fg",
            "<",
            "LX/1bh;",
            "LX/1ln;",
            ">;",
            "LX/1Gd",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 678729
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 678730
    iput-object p1, p0, LX/4CM;->a:LX/1G9;

    .line 678731
    iput-object p2, p0, LX/4CM;->b:Ljava/util/concurrent/ScheduledExecutorService;

    .line 678732
    iput-object p3, p0, LX/4CM;->c:LX/0So;

    .line 678733
    iput-object p4, p0, LX/4CM;->d:LX/1FZ;

    .line 678734
    iput-object p5, p0, LX/4CM;->e:LX/1Fg;

    .line 678735
    iput-object p6, p0, LX/4CM;->f:LX/1Gd;

    .line 678736
    return-void
.end method

.method public static d(LX/4CM;LX/4dO;)LX/4de;
    .locals 3

    .prologue
    .line 678737
    new-instance v0, LX/4de;

    new-instance v1, LX/4CL;

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-direct {v1, v2}, LX/4CL;-><init>(I)V

    iget-object v2, p0, LX/4CM;->e:LX/1Fg;

    invoke-direct {v0, v1, v2}, LX/4de;-><init>(LX/1bh;LX/1Fg;)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/1ln;)Z
    .locals 1

    .prologue
    .line 678728
    instance-of v0, p1, LX/4e7;

    return v0
.end method

.method public final b(LX/1ln;)Landroid/graphics/drawable/Drawable;
    .locals 7

    .prologue
    .line 678715
    new-instance v0, LX/4CI;

    check-cast p1, LX/4e7;

    invoke-virtual {p1}, LX/4e7;->a()LX/4dO;

    move-result-object v1

    .line 678716
    const/4 v5, 0x0

    .line 678717
    iget-object v2, v1, LX/4dO;->a:LX/1GB;

    move-object v2, v2

    .line 678718
    new-instance v3, Landroid/graphics/Rect;

    invoke-interface {v2}, LX/1GB;->a()I

    move-result v4

    invoke-interface {v2}, LX/1GB;->b()I

    move-result v2

    invoke-direct {v3, v5, v5, v4, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 678719
    iget-object v2, p0, LX/4CM;->a:LX/1G9;

    invoke-interface {v2, v1, v3}, LX/1G9;->a(LX/4dO;Landroid/graphics/Rect;)LX/4dG;

    move-result-object v2

    move-object v2, v2

    .line 678720
    iget-object v3, p0, LX/4CM;->f:LX/1Gd;

    invoke-interface {v3}, LX/1Gd;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 678721
    new-instance v3, LX/4CD;

    invoke-direct {v3}, LX/4CD;-><init>()V

    :goto_0
    move-object v3, v3

    .line 678722
    new-instance v4, LX/4C9;

    iget-object v5, p0, LX/4CM;->d:LX/1FZ;

    new-instance v6, LX/4CE;

    invoke-direct {v6, v2}, LX/4CE;-><init>(LX/4dG;)V

    new-instance p1, LX/4CH;

    invoke-direct {p1, v3, v2}, LX/4CH;-><init>(LX/4CA;LX/4dG;)V

    invoke-direct {v4, v5, v3, v6, p1}, LX/4C9;-><init>(LX/1FZ;LX/4CA;LX/4C4;LX/4CH;)V

    .line 678723
    iget-object v2, p0, LX/4CM;->c:LX/0So;

    iget-object v3, p0, LX/4CM;->b:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v4, v2, v3}, LX/4C8;->a(LX/4C5;LX/0So;Ljava/util/concurrent/ScheduledExecutorService;)LX/4C6;

    move-result-object v2

    move-object v1, v2

    .line 678724
    invoke-direct {v0, v1}, LX/4CI;-><init>(LX/4C5;)V

    return-object v0

    .line 678725
    :pswitch_0
    new-instance v3, LX/4CB;

    invoke-static {p0, v1}, LX/4CM;->d(LX/4CM;LX/4dO;)LX/4de;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct {v3, v4, v5}, LX/4CB;-><init>(LX/4de;Z)V

    goto :goto_0

    .line 678726
    :pswitch_1
    new-instance v3, LX/4CB;

    invoke-static {p0, v1}, LX/4CM;->d(LX/4CM;LX/4dO;)LX/4de;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, LX/4CB;-><init>(LX/4de;Z)V

    goto :goto_0

    .line 678727
    :pswitch_2
    new-instance v3, LX/4CC;

    invoke-direct {v3}, LX/4CC;-><init>()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
