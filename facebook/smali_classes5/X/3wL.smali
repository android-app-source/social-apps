.class public LX/3wL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3wK;


# instance fields
.field public final a:LX/3wK;

.field public b:I

.field public c:I

.field public d:I

.field public e:Ljava/lang/Object;


# direct methods
.method public constructor <init>(LX/3wK;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 655005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 655006
    const/4 v0, 0x0

    iput v0, p0, LX/3wL;->b:I

    .line 655007
    iput v1, p0, LX/3wL;->c:I

    .line 655008
    iput v1, p0, LX/3wL;->d:I

    .line 655009
    const/4 v0, 0x0

    iput-object v0, p0, LX/3wL;->e:Ljava/lang/Object;

    .line 655010
    iput-object p1, p0, LX/3wL;->a:LX/3wK;

    .line 655011
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 654997
    iget v0, p0, LX/3wL;->b:I

    if-nez v0, :cond_0

    .line 654998
    :goto_0
    return-void

    .line 654999
    :cond_0
    iget v0, p0, LX/3wL;->b:I

    packed-switch v0, :pswitch_data_0

    .line 655000
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/3wL;->e:Ljava/lang/Object;

    .line 655001
    const/4 v0, 0x0

    iput v0, p0, LX/3wL;->b:I

    goto :goto_0

    .line 655002
    :pswitch_0
    iget-object v0, p0, LX/3wL;->a:LX/3wK;

    iget v1, p0, LX/3wL;->c:I

    iget v2, p0, LX/3wL;->d:I

    invoke-interface {v0, v1, v2}, LX/3wK;->a(II)V

    goto :goto_1

    .line 655003
    :pswitch_1
    iget-object v0, p0, LX/3wL;->a:LX/3wK;

    iget v1, p0, LX/3wL;->c:I

    iget v2, p0, LX/3wL;->d:I

    invoke-interface {v0, v1, v2}, LX/3wK;->b(II)V

    goto :goto_1

    .line 655004
    :pswitch_2
    iget-object v0, p0, LX/3wL;->a:LX/3wK;

    iget v1, p0, LX/3wL;->c:I

    iget v2, p0, LX/3wL;->d:I

    iget-object v3, p0, LX/3wL;->e:Ljava/lang/Object;

    invoke-interface {v0, v1, v2, v3}, LX/3wK;->a(IILjava/lang/Object;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(II)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 654989
    iget v0, p0, LX/3wL;->b:I

    if-ne v0, v2, :cond_0

    iget v0, p0, LX/3wL;->c:I

    if-lt p1, v0, :cond_0

    iget v0, p0, LX/3wL;->c:I

    iget v1, p0, LX/3wL;->d:I

    add-int/2addr v0, v1

    if-gt p1, v0, :cond_0

    .line 654990
    iget v0, p0, LX/3wL;->d:I

    add-int/2addr v0, p2

    iput v0, p0, LX/3wL;->d:I

    .line 654991
    iget v0, p0, LX/3wL;->c:I

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, LX/3wL;->c:I

    .line 654992
    :goto_0
    return-void

    .line 654993
    :cond_0
    invoke-virtual {p0}, LX/3wL;->a()V

    .line 654994
    iput p1, p0, LX/3wL;->c:I

    .line 654995
    iput p2, p0, LX/3wL;->d:I

    .line 654996
    iput v2, p0, LX/3wL;->b:I

    goto :goto_0
.end method

.method public final a(IILjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 655012
    iget v0, p0, LX/3wL;->b:I

    if-ne v0, v2, :cond_0

    iget v0, p0, LX/3wL;->c:I

    iget v1, p0, LX/3wL;->d:I

    add-int/2addr v0, v1

    if-gt p1, v0, :cond_0

    add-int v0, p1, p2

    iget v1, p0, LX/3wL;->c:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, LX/3wL;->e:Ljava/lang/Object;

    if-ne v0, p3, :cond_0

    .line 655013
    iget v0, p0, LX/3wL;->c:I

    iget v1, p0, LX/3wL;->d:I

    add-int/2addr v0, v1

    .line 655014
    iget v1, p0, LX/3wL;->c:I

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, LX/3wL;->c:I

    .line 655015
    add-int v1, p1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v1, p0, LX/3wL;->c:I

    sub-int/2addr v0, v1

    iput v0, p0, LX/3wL;->d:I

    .line 655016
    :goto_0
    return-void

    .line 655017
    :cond_0
    invoke-virtual {p0}, LX/3wL;->a()V

    .line 655018
    iput p1, p0, LX/3wL;->c:I

    .line 655019
    iput p2, p0, LX/3wL;->d:I

    .line 655020
    iput-object p3, p0, LX/3wL;->e:Ljava/lang/Object;

    .line 655021
    iput v2, p0, LX/3wL;->b:I

    goto :goto_0
.end method

.method public final b(II)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 654981
    iget v0, p0, LX/3wL;->b:I

    if-ne v0, v2, :cond_0

    iget v0, p0, LX/3wL;->c:I

    if-lt v0, p1, :cond_0

    iget v0, p0, LX/3wL;->c:I

    add-int v1, p1, p2

    if-gt v0, v1, :cond_0

    .line 654982
    iget v0, p0, LX/3wL;->d:I

    add-int/2addr v0, p2

    iput v0, p0, LX/3wL;->d:I

    .line 654983
    iput p1, p0, LX/3wL;->c:I

    .line 654984
    :goto_0
    return-void

    .line 654985
    :cond_0
    invoke-virtual {p0}, LX/3wL;->a()V

    .line 654986
    iput p1, p0, LX/3wL;->c:I

    .line 654987
    iput p2, p0, LX/3wL;->d:I

    .line 654988
    iput v2, p0, LX/3wL;->b:I

    goto :goto_0
.end method

.method public final c(II)V
    .locals 1

    .prologue
    .line 654978
    invoke-virtual {p0}, LX/3wL;->a()V

    .line 654979
    iget-object v0, p0, LX/3wL;->a:LX/3wK;

    invoke-interface {v0, p1, p2}, LX/3wK;->c(II)V

    .line 654980
    return-void
.end method
