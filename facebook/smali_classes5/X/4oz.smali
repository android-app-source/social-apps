.class public LX/4oz;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final c:LX/0Tn;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;

.field public static final m:LX/0Tn;

.field public static final n:LX/0Tn;

.field public static final o:LX/0Tn;

.field public static final p:LX/0Tn;

.field private static final q:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 810526
    sget-object v0, LX/0Tm;->e:LX/0Tn;

    const-string v1, "work/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 810527
    sput-object v0, LX/4oz;->a:LX/0Tn;

    const-string v1, "community_name"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/4oz;->b:LX/0Tn;

    .line 810528
    sget-object v0, LX/4oz;->a:LX/0Tn;

    const-string v1, "community_logo_url"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/4oz;->c:LX/0Tn;

    .line 810529
    sget-object v0, LX/0Tm;->d:LX/0Tn;

    const-string v1, "work_community/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 810530
    sput-object v0, LX/4oz;->q:LX/0Tn;

    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/4oz;->d:LX/0Tn;

    .line 810531
    sget-object v0, LX/4oz;->q:LX/0Tn;

    const-string v1, "name"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/4oz;->e:LX/0Tn;

    .line 810532
    sget-object v0, LX/4oz;->q:LX/0Tn;

    const-string v1, "subdomain"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/4oz;->f:LX/0Tn;

    .line 810533
    sget-object v0, LX/4oz;->q:LX/0Tn;

    const-string v1, "logo_url"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/4oz;->g:LX/0Tn;

    .line 810534
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "work_reauth/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 810535
    sput-object v0, LX/4oz;->h:LX/0Tn;

    const-string v1, "code_verifier"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/4oz;->i:LX/0Tn;

    .line 810536
    sget-object v0, LX/4oz;->h:LX/0Tn;

    const-string v1, "last_reauth_ts"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/4oz;->j:LX/0Tn;

    .line 810537
    sget-object v0, LX/4oz;->h:LX/0Tn;

    const-string v1, "reauth_activity_verifier"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/4oz;->k:LX/0Tn;

    .line 810538
    sget-object v0, LX/4oz;->h:LX/0Tn;

    const-string v1, "is_reauth_neeeded"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/4oz;->l:LX/0Tn;

    .line 810539
    sget-object v0, LX/4oz;->a:LX/0Tn;

    const-string v1, "saved_accounts/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/4oz;->m:LX/0Tn;

    .line 810540
    sget-object v0, LX/4oz;->a:LX/0Tn;

    const-string v1, "pending_auth_email"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/4oz;->n:LX/0Tn;

    .line 810541
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "work_auth/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 810542
    sput-object v0, LX/4oz;->o:LX/0Tn;

    const-string v1, "code_verifier"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/4oz;->p:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 810543
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
