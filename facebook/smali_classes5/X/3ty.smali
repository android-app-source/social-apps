.class public LX/3ty;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final v:Landroid/view/animation/Interpolator;


# instance fields
.field public a:I

.field public b:I

.field private c:I

.field public d:[F

.field public e:[F

.field public f:[F

.field public g:[F

.field public h:[I

.field public i:[I

.field public j:[I

.field public k:I

.field private l:Landroid/view/VelocityTracker;

.field private m:F

.field public n:F

.field public o:I

.field public p:I

.field private q:LX/1Og;

.field public final r:LX/3nx;

.field public s:Landroid/view/View;

.field private t:Z

.field public final u:Landroid/view/ViewGroup;

.field private final w:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 647171
    new-instance v0, LX/3tx;

    invoke-direct {v0}, LX/3tx;-><init>()V

    sput-object v0, LX/3ty;->v:Landroid/view/animation/Interpolator;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;LX/3nx;)V
    .locals 3

    .prologue
    .line 647172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 647173
    const/4 v0, -0x1

    iput v0, p0, LX/3ty;->c:I

    .line 647174
    new-instance v0, Landroid/support/v4/widget/ViewDragHelper$2;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/ViewDragHelper$2;-><init>(LX/3ty;)V

    iput-object v0, p0, LX/3ty;->w:Ljava/lang/Runnable;

    .line 647175
    if-nez p2, :cond_0

    .line 647176
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Parent view may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 647177
    :cond_0
    if-nez p3, :cond_1

    .line 647178
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Callback may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 647179
    :cond_1
    iput-object p2, p0, LX/3ty;->u:Landroid/view/ViewGroup;

    .line 647180
    iput-object p3, p0, LX/3ty;->r:LX/3nx;

    .line 647181
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 647182
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 647183
    const/high16 v2, 0x41a00000    # 20.0f

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, LX/3ty;->o:I

    .line 647184
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, LX/3ty;->b:I

    .line 647185
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, LX/3ty;->m:F

    .line 647186
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LX/3ty;->n:F

    .line 647187
    sget-object v0, LX/3ty;->v:Landroid/view/animation/Interpolator;

    invoke-static {p1, v0}, LX/1Og;->a(Landroid/content/Context;Landroid/view/animation/Interpolator;)LX/1Og;

    move-result-object v0

    iput-object v0, p0, LX/3ty;->q:LX/1Og;

    .line 647188
    return-void
.end method

.method private static a(FFF)F
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 647189
    invoke-static {p0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 647190
    cmpg-float v2, v1, p1

    if-gez v2, :cond_1

    move p2, v0

    .line 647191
    :cond_0
    :goto_0
    return p2

    .line 647192
    :cond_1
    cmpl-float v1, v1, p2

    if-lez v1, :cond_2

    cmpl-float v0, p0, v0

    if-gtz v0, :cond_0

    neg-float p2, p2

    goto :goto_0

    :cond_2
    move p2, p0

    .line 647193
    goto :goto_0
.end method

.method private static a(LX/3ty;III)I
    .locals 8

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 647194
    if-nez p1, :cond_0

    .line 647195
    const/4 v0, 0x0

    .line 647196
    :goto_0
    return v0

    .line 647197
    :cond_0
    iget-object v0, p0, LX/3ty;->u:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    .line 647198
    div-int/lit8 v1, v0, 0x2

    .line 647199
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v2

    int-to-float v2, v2

    int-to-float v0, v0

    div-float v0, v2, v0

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 647200
    int-to-float v2, v1

    int-to-float v1, v1

    .line 647201
    const/high16 v4, 0x3f000000    # 0.5f

    sub-float v4, v0, v4

    .line 647202
    float-to-double v4, v4

    const-wide v6, 0x3fde28c7460698c7L    # 0.4712389167638204

    mul-double/2addr v4, v6

    double-to-float v4, v4

    .line 647203
    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v4, v4

    move v0, v4

    .line 647204
    mul-float/2addr v0, v1

    add-float/2addr v0, v2

    .line 647205
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 647206
    if-lez v1, :cond_1

    .line 647207
    const/high16 v2, 0x447a0000    # 1000.0f

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    .line 647208
    :goto_1
    const/16 v1, 0x258

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    .line 647209
    :cond_1
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    int-to-float v1, p3

    div-float/2addr v0, v1

    .line 647210
    add-float/2addr v0, v3

    const/high16 v1, 0x43800000    # 256.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_1
.end method

.method private a(Landroid/view/View;IIII)I
    .locals 8

    .prologue
    .line 647211
    iget v0, p0, LX/3ty;->n:F

    float-to-int v0, v0

    iget v1, p0, LX/3ty;->m:F

    float-to-int v1, v1

    invoke-static {p4, v0, v1}, LX/3ty;->b(III)I

    move-result v2

    .line 647212
    iget v0, p0, LX/3ty;->n:F

    float-to-int v0, v0

    iget v1, p0, LX/3ty;->m:F

    float-to-int v1, v1

    invoke-static {p5, v0, v1}, LX/3ty;->b(III)I

    move-result v3

    .line 647213
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 647214
    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v4

    .line 647215
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 647216
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v5

    .line 647217
    add-int v6, v1, v5

    .line 647218
    add-int v7, v0, v4

    .line 647219
    if-eqz v2, :cond_0

    int-to-float v0, v1

    int-to-float v1, v6

    div-float/2addr v0, v1

    move v1, v0

    .line 647220
    :goto_0
    if-eqz v3, :cond_1

    int-to-float v0, v5

    int-to-float v4, v6

    div-float/2addr v0, v4

    .line 647221
    :goto_1
    iget-object v4, p0, LX/3ty;->r:LX/3nx;

    invoke-virtual {v4, p1}, LX/3nx;->c(Landroid/view/View;)I

    move-result v4

    invoke-static {p0, p2, v2, v4}, LX/3ty;->a(LX/3ty;III)I

    move-result v2

    .line 647222
    const/4 v4, 0x0

    move v4, v4

    .line 647223
    invoke-static {p0, p3, v3, v4}, LX/3ty;->a(LX/3ty;III)I

    move-result v3

    .line 647224
    int-to-float v2, v2

    mul-float/2addr v1, v2

    int-to-float v2, v3

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0

    .line 647225
    :cond_0
    int-to-float v0, v0

    int-to-float v1, v7

    div-float/2addr v0, v1

    move v1, v0

    goto :goto_0

    .line 647226
    :cond_1
    int-to-float v0, v4

    int-to-float v4, v7

    div-float/2addr v0, v4

    goto :goto_1
.end method

.method public static a(Landroid/view/ViewGroup;FLX/3nx;)LX/3ty;
    .locals 3

    .prologue
    .line 647227
    invoke-static {p0, p2}, LX/3ty;->a(Landroid/view/ViewGroup;LX/3nx;)LX/3ty;

    move-result-object v0

    .line 647228
    iget v1, v0, LX/3ty;->b:I

    int-to-float v1, v1

    const/high16 v2, 0x3f800000    # 1.0f

    div-float/2addr v2, p1

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, LX/3ty;->b:I

    .line 647229
    return-object v0
.end method

.method public static a(Landroid/view/ViewGroup;LX/3nx;)LX/3ty;
    .locals 2

    .prologue
    .line 647230
    new-instance v0, LX/3ty;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1}, LX/3ty;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;LX/3nx;)V

    return-object v0
.end method

.method private a(FF)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 647231
    iput-boolean v3, p0, LX/3ty;->t:Z

    .line 647232
    iget-object v0, p0, LX/3ty;->r:LX/3nx;

    iget-object v1, p0, LX/3ty;->s:Landroid/view/View;

    invoke-virtual {v0, v1, p1, p2}, LX/3nx;->a(Landroid/view/View;FF)V

    .line 647233
    iput-boolean v2, p0, LX/3ty;->t:Z

    .line 647234
    iget v0, p0, LX/3ty;->a:I

    if-ne v0, v3, :cond_0

    .line 647235
    invoke-virtual {p0, v2}, LX/3ty;->b(I)V

    .line 647236
    :cond_0
    return-void
.end method

.method private a(FFI)V
    .locals 10

    .prologue
    .line 647237
    const/4 v9, 0x0

    .line 647238
    iget-object v0, p0, LX/3ty;->d:[F

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3ty;->d:[F

    array-length v0, v0

    if-gt v0, p3, :cond_2

    .line 647239
    :cond_0
    add-int/lit8 v0, p3, 0x1

    new-array v0, v0, [F

    .line 647240
    add-int/lit8 v1, p3, 0x1

    new-array v1, v1, [F

    .line 647241
    add-int/lit8 v2, p3, 0x1

    new-array v2, v2, [F

    .line 647242
    add-int/lit8 v3, p3, 0x1

    new-array v3, v3, [F

    .line 647243
    add-int/lit8 v4, p3, 0x1

    new-array v4, v4, [I

    .line 647244
    add-int/lit8 v5, p3, 0x1

    new-array v5, v5, [I

    .line 647245
    add-int/lit8 v6, p3, 0x1

    new-array v6, v6, [I

    .line 647246
    iget-object v7, p0, LX/3ty;->d:[F

    if-eqz v7, :cond_1

    .line 647247
    iget-object v7, p0, LX/3ty;->d:[F

    iget-object v8, p0, LX/3ty;->d:[F

    array-length v8, v8

    invoke-static {v7, v9, v0, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 647248
    iget-object v7, p0, LX/3ty;->e:[F

    iget-object v8, p0, LX/3ty;->e:[F

    array-length v8, v8

    invoke-static {v7, v9, v1, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 647249
    iget-object v7, p0, LX/3ty;->f:[F

    iget-object v8, p0, LX/3ty;->f:[F

    array-length v8, v8

    invoke-static {v7, v9, v2, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 647250
    iget-object v7, p0, LX/3ty;->g:[F

    iget-object v8, p0, LX/3ty;->g:[F

    array-length v8, v8

    invoke-static {v7, v9, v3, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 647251
    iget-object v7, p0, LX/3ty;->h:[I

    iget-object v8, p0, LX/3ty;->h:[I

    array-length v8, v8

    invoke-static {v7, v9, v4, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 647252
    iget-object v7, p0, LX/3ty;->i:[I

    iget-object v8, p0, LX/3ty;->i:[I

    array-length v8, v8

    invoke-static {v7, v9, v5, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 647253
    iget-object v7, p0, LX/3ty;->j:[I

    iget-object v8, p0, LX/3ty;->j:[I

    array-length v8, v8

    invoke-static {v7, v9, v6, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 647254
    :cond_1
    iput-object v0, p0, LX/3ty;->d:[F

    .line 647255
    iput-object v1, p0, LX/3ty;->e:[F

    .line 647256
    iput-object v2, p0, LX/3ty;->f:[F

    .line 647257
    iput-object v3, p0, LX/3ty;->g:[F

    .line 647258
    iput-object v4, p0, LX/3ty;->h:[I

    .line 647259
    iput-object v5, p0, LX/3ty;->i:[I

    .line 647260
    iput-object v6, p0, LX/3ty;->j:[I

    .line 647261
    :cond_2
    iget-object v0, p0, LX/3ty;->d:[F

    iget-object v1, p0, LX/3ty;->f:[F

    aput p1, v1, p3

    aput p1, v0, p3

    .line 647262
    iget-object v0, p0, LX/3ty;->e:[F

    iget-object v1, p0, LX/3ty;->g:[F

    aput p2, v1, p3

    aput p2, v0, p3

    .line 647263
    iget-object v0, p0, LX/3ty;->h:[I

    float-to-int v1, p1

    float-to-int v2, p2

    .line 647264
    const/4 v3, 0x0

    .line 647265
    iget-object v4, p0, LX/3ty;->u:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getLeft()I

    move-result v4

    iget v5, p0, LX/3ty;->o:I

    add-int/2addr v4, v5

    if-ge v1, v4, :cond_3

    const/4 v3, 0x1

    .line 647266
    :cond_3
    iget-object v4, p0, LX/3ty;->u:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getTop()I

    move-result v4

    iget v5, p0, LX/3ty;->o:I

    add-int/2addr v4, v5

    if-ge v2, v4, :cond_4

    or-int/lit8 v3, v3, 0x4

    .line 647267
    :cond_4
    iget-object v4, p0, LX/3ty;->u:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getRight()I

    move-result v4

    iget v5, p0, LX/3ty;->o:I

    sub-int/2addr v4, v5

    if-le v1, v4, :cond_5

    or-int/lit8 v3, v3, 0x2

    .line 647268
    :cond_5
    iget-object v4, p0, LX/3ty;->u:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getBottom()I

    move-result v4

    iget v5, p0, LX/3ty;->o:I

    sub-int/2addr v4, v5

    if-le v2, v4, :cond_6

    or-int/lit8 v3, v3, 0x8

    .line 647269
    :cond_6
    move v1, v3

    .line 647270
    aput v1, v0, p3

    .line 647271
    iget v0, p0, LX/3ty;->k:I

    const/4 v1, 0x1

    shl-int/2addr v1, p3

    or-int/2addr v0, v1

    iput v0, p0, LX/3ty;->k:I

    .line 647272
    return-void
.end method

.method private a(FFII)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 647273
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 647274
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 647275
    iget-object v3, p0, LX/3ty;->h:[I

    aget v3, v3, p3

    and-int/2addr v3, p4

    if-ne v3, p4, :cond_0

    iget v3, p0, LX/3ty;->p:I

    and-int/2addr v3, p4

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/3ty;->j:[I

    aget v3, v3, p3

    and-int/2addr v3, p4

    if-eq v3, p4, :cond_0

    iget-object v3, p0, LX/3ty;->i:[I

    aget v3, v3, p3

    and-int/2addr v3, p4

    if-eq v3, p4, :cond_0

    iget v3, p0, LX/3ty;->b:I

    int-to-float v3, v3

    cmpg-float v3, v1, v3

    if-gtz v3, :cond_1

    iget v3, p0, LX/3ty;->b:I

    int-to-float v3, v3

    cmpg-float v3, v2, v3

    if-gtz v3, :cond_1

    .line 647276
    :cond_0
    :goto_0
    return v0

    .line 647277
    :cond_1
    const/high16 v3, 0x3f000000    # 0.5f

    mul-float/2addr v2, v3

    cmpg-float v2, v1, v2

    if-gez v2, :cond_2

    iget-object v2, p0, LX/3ty;->r:LX/3nx;

    invoke-virtual {v2}, LX/3nx;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 647278
    iget-object v1, p0, LX/3ty;->j:[I

    aget v2, v1, p3

    or-int/2addr v2, p4

    aput v2, v1, p3

    goto :goto_0

    .line 647279
    :cond_2
    iget-object v2, p0, LX/3ty;->i:[I

    aget v2, v2, p3

    and-int/2addr v2, p4

    if-nez v2, :cond_0

    iget v2, p0, LX/3ty;->b:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(IIII)Z
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 647280
    iget-object v1, p0, LX/3ty;->s:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v7

    .line 647281
    iget-object v1, p0, LX/3ty;->s:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v6

    .line 647282
    sub-int v2, p1, v7

    .line 647283
    sub-int v3, p2, v6

    .line 647284
    if-nez v2, :cond_0

    if-nez v3, :cond_0

    .line 647285
    iget-object v1, p0, LX/3ty;->q:LX/1Og;

    invoke-virtual {v1}, LX/1Og;->h()V

    .line 647286
    invoke-virtual {p0, v0}, LX/3ty;->b(I)V

    .line 647287
    :goto_0
    return v0

    .line 647288
    :cond_0
    iget-object v1, p0, LX/3ty;->s:Landroid/view/View;

    move-object v0, p0

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LX/3ty;->a(Landroid/view/View;IIII)I

    move-result v9

    .line 647289
    iget-object v4, p0, LX/3ty;->q:LX/1Og;

    move v5, v7

    move v7, v2

    move v8, v3

    invoke-virtual/range {v4 .. v9}, LX/1Og;->a(IIIII)V

    .line 647290
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/3ty;->b(I)V

    .line 647291
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Landroid/view/View;FF)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 647292
    if-nez p1, :cond_1

    .line 647293
    :cond_0
    :goto_0
    return v2

    .line 647294
    :cond_1
    iget-object v0, p0, LX/3ty;->r:LX/3nx;

    invoke-virtual {v0, p1}, LX/3nx;->c(Landroid/view/View;)I

    move-result v0

    if-lez v0, :cond_2

    move v0, v1

    .line 647295
    :goto_1
    const/4 v3, 0x0

    move v3, v3

    .line 647296
    if-lez v3, :cond_3

    move v3, v1

    .line 647297
    :goto_2
    if-eqz v0, :cond_4

    if-eqz v3, :cond_4

    .line 647298
    mul-float v0, p2, p2

    mul-float v3, p3, p3

    add-float/2addr v0, v3

    iget v3, p0, LX/3ty;->b:I

    iget v4, p0, LX/3ty;->b:I

    mul-int/2addr v3, v4

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 647299
    goto :goto_1

    :cond_3
    move v3, v2

    .line 647300
    goto :goto_2

    .line 647301
    :cond_4
    if-eqz v0, :cond_5

    .line 647302
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v3, p0, LX/3ty;->b:I

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    move v2, v1

    goto :goto_0

    .line 647303
    :cond_5
    if-eqz v3, :cond_0

    .line 647304
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v3, p0, LX/3ty;->b:I

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    move v2, v1

    goto :goto_0
.end method

.method private static b(III)I
    .locals 1

    .prologue
    .line 647305
    invoke-static {p0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 647306
    if-ge v0, p1, :cond_1

    const/4 p2, 0x0

    .line 647307
    :cond_0
    :goto_0
    return p2

    .line 647308
    :cond_1
    if-le v0, p2, :cond_2

    if-gtz p0, :cond_0

    neg-int p2, p2

    goto :goto_0

    :cond_2
    move p2, p0

    .line 647309
    goto :goto_0
.end method

.method private b(FFI)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 647310
    const/4 v1, 0x0

    .line 647311
    invoke-direct {p0, p1, p2, p3, v0}, LX/3ty;->a(FFII)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 647312
    :goto_0
    const/4 v1, 0x4

    invoke-direct {p0, p2, p1, p3, v1}, LX/3ty;->a(FFII)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 647313
    or-int/lit8 v0, v0, 0x4

    .line 647314
    :cond_0
    const/4 v1, 0x2

    invoke-direct {p0, p1, p2, p3, v1}, LX/3ty;->a(FFII)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 647315
    or-int/lit8 v0, v0, 0x2

    .line 647316
    :cond_1
    const/16 v1, 0x8

    invoke-direct {p0, p2, p1, p3, v1}, LX/3ty;->a(FFII)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 647317
    or-int/lit8 v0, v0, 0x8

    .line 647318
    :cond_2
    if-eqz v0, :cond_3

    .line 647319
    iget-object v1, p0, LX/3ty;->i:[I

    aget v2, v1, p3

    or-int/2addr v2, v0

    aput v2, v1, p3

    .line 647320
    iget-object v1, p0, LX/3ty;->r:LX/3nx;

    invoke-virtual {v1, v0, p3}, LX/3nx;->a(II)V

    .line 647321
    :cond_3
    return-void

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method private b(Landroid/view/View;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 647322
    iget-object v1, p0, LX/3ty;->s:Landroid/view/View;

    if-ne p1, v1, :cond_0

    iget v1, p0, LX/3ty;->c:I

    if-ne v1, p2, :cond_0

    .line 647323
    :goto_0
    return v0

    .line 647324
    :cond_0
    if-eqz p1, :cond_1

    iget-object v1, p0, LX/3ty;->r:LX/3nx;

    invoke-virtual {v1, p1}, LX/3nx;->a(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 647325
    iput p2, p0, LX/3ty;->c:I

    .line 647326
    invoke-virtual {p0, p1, p2}, LX/3ty;->a(Landroid/view/View;I)V

    goto :goto_0

    .line 647327
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/view/View;II)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 647169
    if-nez p0, :cond_1

    .line 647170
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    if-lt p1, v1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v1

    if-ge p1, v1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    if-lt p2, v1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v1

    if-ge p2, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private c(Landroid/view/MotionEvent;)V
    .locals 6

    .prologue
    .line 646924
    invoke-static {p1}, LX/2xd;->c(Landroid/view/MotionEvent;)I

    move-result v1

    .line 646925
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 646926
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v2

    .line 646927
    invoke-direct {p0, v2}, LX/3ty;->g(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 646928
    invoke-static {p1, v0}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v3

    .line 646929
    invoke-static {p1, v0}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v4

    .line 646930
    iget-object v5, p0, LX/3ty;->f:[F

    aput v3, v5, v2

    .line 646931
    iget-object v3, p0, LX/3ty;->g:[F

    aput v4, v3, v2

    .line 646932
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 646933
    :cond_1
    return-void
.end method

.method private d(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 646914
    iget-object v0, p0, LX/3ty;->d:[F

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, LX/3ty;->f(LX/3ty;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 646915
    :cond_0
    :goto_0
    return-void

    .line 646916
    :cond_1
    iget-object v0, p0, LX/3ty;->d:[F

    aput v1, v0, p1

    .line 646917
    iget-object v0, p0, LX/3ty;->e:[F

    aput v1, v0, p1

    .line 646918
    iget-object v0, p0, LX/3ty;->f:[F

    aput v1, v0, p1

    .line 646919
    iget-object v0, p0, LX/3ty;->g:[F

    aput v1, v0, p1

    .line 646920
    iget-object v0, p0, LX/3ty;->h:[I

    aput v2, v0, p1

    .line 646921
    iget-object v0, p0, LX/3ty;->i:[I

    aput v2, v0, p1

    .line 646922
    iget-object v0, p0, LX/3ty;->j:[I

    aput v2, v0, p1

    .line 646923
    iget v0, p0, LX/3ty;->k:I

    const/4 v1, 0x1

    shl-int/2addr v1, p1

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, LX/3ty;->k:I

    goto :goto_0
.end method

.method public static f(LX/3ty;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 646934
    iget v1, p0, LX/3ty;->k:I

    shl-int v2, v0, p1

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g(I)Z
    .locals 3

    .prologue
    .line 646935
    invoke-static {p0, p1}, LX/3ty;->f(LX/3ty;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 646936
    const-string v0, "ViewDragHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring pointerId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " because ACTION_DOWN was not received for this pointer before ACTION_MOVE. It likely happened because "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ViewDragHelper did not receive all the events in the event stream."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 646937
    const/4 v0, 0x0

    .line 646938
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private h()V
    .locals 4

    .prologue
    .line 646939
    iget-object v0, p0, LX/3ty;->l:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    iget v2, p0, LX/3ty;->m:F

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 646940
    iget-object v0, p0, LX/3ty;->l:Landroid/view/VelocityTracker;

    iget v1, p0, LX/3ty;->c:I

    invoke-static {v0, v1}, LX/2uG;->a(Landroid/view/VelocityTracker;I)F

    move-result v0

    iget v1, p0, LX/3ty;->n:F

    iget v2, p0, LX/3ty;->m:F

    invoke-static {v0, v1, v2}, LX/3ty;->a(FFF)F

    move-result v0

    .line 646941
    iget-object v1, p0, LX/3ty;->l:Landroid/view/VelocityTracker;

    iget v2, p0, LX/3ty;->c:I

    invoke-static {v1, v2}, LX/2uG;->b(Landroid/view/VelocityTracker;I)F

    move-result v1

    iget v2, p0, LX/3ty;->n:F

    iget v3, p0, LX/3ty;->m:F

    invoke-static {v1, v2, v3}, LX/3ty;->a(FFF)F

    move-result v1

    .line 646942
    invoke-direct {p0, v0, v1}, LX/3ty;->a(FF)V

    .line 646943
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 646944
    iget v0, p0, LX/3ty;->a:I

    return v0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 646945
    iput p1, p0, LX/3ty;->p:I

    .line 646946
    return-void
.end method

.method public final a(Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 646947
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, LX/3ty;->u:Landroid/view/ViewGroup;

    if-eq v0, v1, :cond_0

    .line 646948
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "captureChildView: parameter must be a descendant of the ViewDragHelper\'s tracked parent view ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/3ty;->u:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 646949
    :cond_0
    iput-object p1, p0, LX/3ty;->s:Landroid/view/View;

    .line 646950
    iput p2, p0, LX/3ty;->c:I

    .line 646951
    iget-object v0, p0, LX/3ty;->r:LX/3nx;

    invoke-virtual {v0, p1}, LX/3nx;->b(Landroid/view/View;)V

    .line 646952
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/3ty;->b(I)V

    .line 646953
    return-void
.end method

.method public final a(II)Z
    .locals 3

    .prologue
    .line 646954
    iget-boolean v0, p0, LX/3ty;->t:Z

    if-nez v0, :cond_0

    .line 646955
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot settleCapturedViewAt outside of a call to Callback#onViewReleased"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 646956
    :cond_0
    iget-object v0, p0, LX/3ty;->l:Landroid/view/VelocityTracker;

    iget v1, p0, LX/3ty;->c:I

    invoke-static {v0, v1}, LX/2uG;->a(Landroid/view/VelocityTracker;I)F

    move-result v0

    float-to-int v0, v0

    iget-object v1, p0, LX/3ty;->l:Landroid/view/VelocityTracker;

    iget v2, p0, LX/3ty;->c:I

    invoke-static {v1, v2}, LX/2uG;->b(Landroid/view/VelocityTracker;I)F

    move-result v1

    float-to-int v1, v1

    invoke-direct {p0, p1, p2, v0, v1}, LX/3ty;->a(IIII)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 13

    .prologue
    .line 646957
    invoke-static {p1}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v0

    .line 646958
    invoke-static {p1}, LX/2xd;->b(Landroid/view/MotionEvent;)I

    move-result v1

    .line 646959
    if-nez v0, :cond_0

    .line 646960
    invoke-virtual {p0}, LX/3ty;->e()V

    .line 646961
    :cond_0
    iget-object v2, p0, LX/3ty;->l:Landroid/view/VelocityTracker;

    if-nez v2, :cond_1

    .line 646962
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v2

    iput-object v2, p0, LX/3ty;->l:Landroid/view/VelocityTracker;

    .line 646963
    :cond_1
    iget-object v2, p0, LX/3ty;->l:Landroid/view/VelocityTracker;

    invoke-virtual {v2, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 646964
    packed-switch v0, :pswitch_data_0

    .line 646965
    :cond_2
    :goto_0
    :pswitch_0
    iget v0, p0, LX/3ty;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 646966
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 646967
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 646968
    const/4 v2, 0x0

    invoke-static {p1, v2}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v2

    .line 646969
    invoke-direct {p0, v0, v1, v2}, LX/3ty;->a(FFI)V

    .line 646970
    float-to-int v0, v0

    float-to-int v1, v1

    invoke-virtual {p0, v0, v1}, LX/3ty;->b(II)Landroid/view/View;

    move-result-object v0

    .line 646971
    iget-object v1, p0, LX/3ty;->s:Landroid/view/View;

    if-ne v0, v1, :cond_3

    iget v1, p0, LX/3ty;->a:I

    const/4 v3, 0x2

    if-ne v1, v3, :cond_3

    .line 646972
    invoke-direct {p0, v0, v2}, LX/3ty;->b(Landroid/view/View;I)Z

    .line 646973
    :cond_3
    iget-object v0, p0, LX/3ty;->h:[I

    aget v0, v0, v2

    .line 646974
    iget v1, p0, LX/3ty;->p:I

    and-int/2addr v0, v1

    if-eqz v0, :cond_2

    .line 646975
    iget-object v0, p0, LX/3ty;->r:LX/3nx;

    invoke-virtual {v0}, LX/3nx;->b()V

    goto :goto_0

    .line 646976
    :pswitch_2
    invoke-static {p1, v1}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 646977
    invoke-static {p1, v1}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v2

    .line 646978
    invoke-static {p1, v1}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v1

    .line 646979
    invoke-direct {p0, v2, v1, v0}, LX/3ty;->a(FFI)V

    .line 646980
    iget v3, p0, LX/3ty;->a:I

    if-nez v3, :cond_4

    .line 646981
    iget-object v1, p0, LX/3ty;->h:[I

    aget v0, v1, v0

    .line 646982
    iget v1, p0, LX/3ty;->p:I

    and-int/2addr v0, v1

    if-eqz v0, :cond_2

    .line 646983
    iget-object v0, p0, LX/3ty;->r:LX/3nx;

    invoke-virtual {v0}, LX/3nx;->b()V

    goto :goto_0

    .line 646984
    :cond_4
    iget v3, p0, LX/3ty;->a:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 646985
    float-to-int v2, v2

    float-to-int v1, v1

    invoke-virtual {p0, v2, v1}, LX/3ty;->b(II)Landroid/view/View;

    move-result-object v1

    .line 646986
    iget-object v2, p0, LX/3ty;->s:Landroid/view/View;

    if-ne v1, v2, :cond_2

    .line 646987
    invoke-direct {p0, v1, v0}, LX/3ty;->b(Landroid/view/View;I)Z

    goto :goto_0

    .line 646988
    :pswitch_3
    invoke-static {p1}, LX/2xd;->c(Landroid/view/MotionEvent;)I

    move-result v2

    .line 646989
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v2, :cond_9

    .line 646990
    invoke-static {p1, v1}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v3

    .line 646991
    invoke-direct {p0, v3}, LX/3ty;->g(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 646992
    invoke-static {p1, v1}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v0

    .line 646993
    invoke-static {p1, v1}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v4

    .line 646994
    iget-object v5, p0, LX/3ty;->d:[F

    aget v5, v5, v3

    sub-float v5, v0, v5

    .line 646995
    iget-object v6, p0, LX/3ty;->e:[F

    aget v6, v6, v3

    sub-float v6, v4, v6

    .line 646996
    float-to-int v0, v0

    float-to-int v4, v4

    invoke-virtual {p0, v0, v4}, LX/3ty;->b(II)Landroid/view/View;

    move-result-object v4

    .line 646997
    if-eqz v4, :cond_8

    invoke-direct {p0, v4, v5, v6}, LX/3ty;->a(Landroid/view/View;FF)Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    .line 646998
    :goto_3
    if-eqz v0, :cond_6

    .line 646999
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v7

    .line 647000
    float-to-int v8, v5

    add-int/2addr v8, v7

    .line 647001
    iget-object v9, p0, LX/3ty;->r:LX/3nx;

    invoke-virtual {v9, v4, v8}, LX/3nx;->b(Landroid/view/View;I)I

    move-result v8

    .line 647002
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v9

    .line 647003
    float-to-int v10, v6

    add-int/2addr v10, v9

    .line 647004
    iget-object v11, p0, LX/3ty;->r:LX/3nx;

    invoke-virtual {v11, v4, v10}, LX/3nx;->c(Landroid/view/View;I)I

    move-result v10

    .line 647005
    iget-object v11, p0, LX/3ty;->r:LX/3nx;

    invoke-virtual {v11, v4}, LX/3nx;->c(Landroid/view/View;)I

    move-result v11

    .line 647006
    const/4 v12, 0x0

    move v12, v12

    .line 647007
    if-eqz v11, :cond_5

    if-lez v11, :cond_6

    if-ne v8, v7, :cond_6

    :cond_5
    if-eqz v12, :cond_9

    if-lez v12, :cond_6

    if-eq v10, v9, :cond_9

    .line 647008
    :cond_6
    invoke-direct {p0, v5, v6, v3}, LX/3ty;->b(FFI)V

    .line 647009
    iget v5, p0, LX/3ty;->a:I

    const/4 v6, 0x1

    if-eq v5, v6, :cond_9

    .line 647010
    if-eqz v0, :cond_7

    invoke-direct {p0, v4, v3}, LX/3ty;->b(Landroid/view/View;I)Z

    move-result v0

    if-nez v0, :cond_9

    .line 647011
    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 647012
    :cond_8
    const/4 v0, 0x0

    goto :goto_3

    .line 647013
    :cond_9
    invoke-direct {p0, p1}, LX/3ty;->c(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 647014
    :pswitch_4
    invoke-static {p1, v1}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 647015
    invoke-direct {p0, v0}, LX/3ty;->d(I)V

    goto/16 :goto_0

    .line 647016
    :pswitch_5
    invoke-virtual {p0}, LX/3ty;->e()V

    goto/16 :goto_0

    .line 647017
    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Landroid/view/View;II)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 647018
    iput-object p1, p0, LX/3ty;->s:Landroid/view/View;

    .line 647019
    const/4 v0, -0x1

    iput v0, p0, LX/3ty;->c:I

    .line 647020
    invoke-direct {p0, p2, p3, v1, v1}, LX/3ty;->a(IIII)Z

    move-result v0

    .line 647021
    if-nez v0, :cond_0

    iget v1, p0, LX/3ty;->a:I

    if-nez v1, :cond_0

    iget-object v1, p0, LX/3ty;->s:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 647022
    const/4 v1, 0x0

    iput-object v1, p0, LX/3ty;->s:Landroid/view/View;

    .line 647023
    :cond_0
    return v0
.end method

.method public final a(Z)Z
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x0

    .line 647024
    iget v1, p0, LX/3ty;->a:I

    if-ne v1, v7, :cond_5

    .line 647025
    iget-object v1, p0, LX/3ty;->q:LX/1Og;

    invoke-virtual {v1}, LX/1Og;->g()Z

    move-result v1

    .line 647026
    iget-object v2, p0, LX/3ty;->q:LX/1Og;

    invoke-virtual {v2}, LX/1Og;->b()I

    move-result v2

    .line 647027
    iget-object v3, p0, LX/3ty;->q:LX/1Og;

    invoke-virtual {v3}, LX/1Og;->c()I

    move-result v3

    .line 647028
    iget-object v4, p0, LX/3ty;->s:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    sub-int v4, v2, v4

    .line 647029
    iget-object v5, p0, LX/3ty;->s:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v5

    sub-int v5, v3, v5

    .line 647030
    if-eqz v4, :cond_0

    .line 647031
    iget-object v6, p0, LX/3ty;->s:Landroid/view/View;

    invoke-virtual {v6, v4}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 647032
    :cond_0
    if-eqz v5, :cond_1

    .line 647033
    iget-object v6, p0, LX/3ty;->s:Landroid/view/View;

    invoke-virtual {v6, v5}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 647034
    :cond_1
    if-nez v4, :cond_2

    if-eqz v5, :cond_3

    .line 647035
    :cond_2
    iget-object v4, p0, LX/3ty;->r:LX/3nx;

    iget-object v5, p0, LX/3ty;->s:Landroid/view/View;

    invoke-virtual {v4, v5, v2}, LX/3nx;->a(Landroid/view/View;I)V

    .line 647036
    :cond_3
    if-eqz v1, :cond_4

    iget-object v4, p0, LX/3ty;->q:LX/1Og;

    invoke-virtual {v4}, LX/1Og;->d()I

    move-result v4

    if-ne v2, v4, :cond_4

    iget-object v2, p0, LX/3ty;->q:LX/1Og;

    invoke-virtual {v2}, LX/1Og;->e()I

    move-result v2

    if-ne v3, v2, :cond_4

    .line 647037
    iget-object v1, p0, LX/3ty;->q:LX/1Og;

    invoke-virtual {v1}, LX/1Og;->h()V

    move v1, v0

    .line 647038
    :cond_4
    if-nez v1, :cond_5

    .line 647039
    if-eqz p1, :cond_7

    .line 647040
    iget-object v1, p0, LX/3ty;->u:Landroid/view/ViewGroup;

    iget-object v2, p0, LX/3ty;->w:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    .line 647041
    :cond_5
    :goto_0
    iget v1, p0, LX/3ty;->a:I

    if-ne v1, v7, :cond_6

    const/4 v0, 0x1

    :cond_6
    return v0

    .line 647042
    :cond_7
    invoke-virtual {p0, v0}, LX/3ty;->b(I)V

    goto :goto_0
.end method

.method public final b(II)Landroid/view/View;
    .locals 3

    .prologue
    .line 647043
    iget-object v0, p0, LX/3ty;->u:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 647044
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 647045
    iget-object v0, p0, LX/3ty;->u:Landroid/view/ViewGroup;

    .line 647046
    move v2, v1

    .line 647047
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 647048
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    if-lt p1, v2, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v2

    if-ge p1, v2, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    if-lt p2, v2, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v2

    if-ge p2, v2, :cond_0

    .line 647049
    :goto_1
    return-object v0

    .line 647050
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 647051
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 647052
    iget-object v0, p0, LX/3ty;->u:Landroid/view/ViewGroup;

    iget-object v1, p0, LX/3ty;->w:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 647053
    iget v0, p0, LX/3ty;->a:I

    if-eq v0, p1, :cond_0

    .line 647054
    iput p1, p0, LX/3ty;->a:I

    .line 647055
    iget-object v0, p0, LX/3ty;->r:LX/3nx;

    invoke-virtual {v0, p1}, LX/3nx;->a(I)V

    .line 647056
    iget v0, p0, LX/3ty;->a:I

    if-nez v0, :cond_0

    .line 647057
    const/4 v0, 0x0

    iput-object v0, p0, LX/3ty;->s:Landroid/view/View;

    .line 647058
    :cond_0
    return-void
.end method

.method public final b(Landroid/view/MotionEvent;)V
    .locals 9

    .prologue
    const/4 v1, -0x1

    const/4 v5, 0x0

    const/4 v0, 0x0

    const/4 v8, 0x1

    .line 647059
    invoke-static {p1}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v2

    .line 647060
    invoke-static {p1}, LX/2xd;->b(Landroid/view/MotionEvent;)I

    move-result v3

    .line 647061
    if-nez v2, :cond_0

    .line 647062
    invoke-virtual {p0}, LX/3ty;->e()V

    .line 647063
    :cond_0
    iget-object v4, p0, LX/3ty;->l:Landroid/view/VelocityTracker;

    if-nez v4, :cond_1

    .line 647064
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v4

    iput-object v4, p0, LX/3ty;->l:Landroid/view/VelocityTracker;

    .line 647065
    :cond_1
    iget-object v4, p0, LX/3ty;->l:Landroid/view/VelocityTracker;

    invoke-virtual {v4, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 647066
    packed-switch v2, :pswitch_data_0

    .line 647067
    :cond_2
    :goto_0
    :pswitch_0
    return-void

    .line 647068
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 647069
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 647070
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 647071
    float-to-int v3, v1

    float-to-int v4, v2

    invoke-virtual {p0, v3, v4}, LX/3ty;->b(II)Landroid/view/View;

    move-result-object v3

    .line 647072
    invoke-direct {p0, v1, v2, v0}, LX/3ty;->a(FFI)V

    .line 647073
    invoke-direct {p0, v3, v0}, LX/3ty;->b(Landroid/view/View;I)Z

    .line 647074
    iget-object v1, p0, LX/3ty;->h:[I

    aget v0, v1, v0

    .line 647075
    iget v1, p0, LX/3ty;->p:I

    and-int/2addr v0, v1

    if-eqz v0, :cond_2

    .line 647076
    iget-object v0, p0, LX/3ty;->r:LX/3nx;

    invoke-virtual {v0}, LX/3nx;->b()V

    goto :goto_0

    .line 647077
    :pswitch_2
    invoke-static {p1, v3}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 647078
    invoke-static {p1, v3}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v1

    .line 647079
    invoke-static {p1, v3}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v2

    .line 647080
    invoke-direct {p0, v1, v2, v0}, LX/3ty;->a(FFI)V

    .line 647081
    iget v3, p0, LX/3ty;->a:I

    if-nez v3, :cond_3

    .line 647082
    float-to-int v1, v1

    float-to-int v2, v2

    invoke-virtual {p0, v1, v2}, LX/3ty;->b(II)Landroid/view/View;

    move-result-object v1

    .line 647083
    invoke-direct {p0, v1, v0}, LX/3ty;->b(Landroid/view/View;I)Z

    .line 647084
    iget-object v1, p0, LX/3ty;->h:[I

    aget v0, v1, v0

    .line 647085
    iget v1, p0, LX/3ty;->p:I

    and-int/2addr v0, v1

    if-eqz v0, :cond_2

    .line 647086
    iget-object v0, p0, LX/3ty;->r:LX/3nx;

    invoke-virtual {v0}, LX/3nx;->b()V

    goto :goto_0

    .line 647087
    :cond_3
    float-to-int v1, v1

    float-to-int v2, v2

    .line 647088
    iget-object v3, p0, LX/3ty;->s:Landroid/view/View;

    invoke-static {v3, v1, v2}, LX/3ty;->b(Landroid/view/View;II)Z

    move-result v3

    move v1, v3

    .line 647089
    if-eqz v1, :cond_2

    .line 647090
    iget-object v1, p0, LX/3ty;->s:Landroid/view/View;

    invoke-direct {p0, v1, v0}, LX/3ty;->b(Landroid/view/View;I)Z

    goto :goto_0

    .line 647091
    :pswitch_3
    iget v1, p0, LX/3ty;->a:I

    if-ne v1, v8, :cond_8

    .line 647092
    iget v0, p0, LX/3ty;->c:I

    invoke-direct {p0, v0}, LX/3ty;->g(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 647093
    iget v0, p0, LX/3ty;->c:I

    invoke-static {p1, v0}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 647094
    invoke-static {p1, v0}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v1

    .line 647095
    invoke-static {p1, v0}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    .line 647096
    iget-object v2, p0, LX/3ty;->f:[F

    iget v3, p0, LX/3ty;->c:I

    aget v2, v2, v3

    sub-float/2addr v1, v2

    float-to-int v1, v1

    .line 647097
    iget-object v2, p0, LX/3ty;->g:[F

    iget v3, p0, LX/3ty;->c:I

    aget v2, v2, v3

    sub-float/2addr v0, v2

    float-to-int v0, v0

    .line 647098
    iget-object v2, p0, LX/3ty;->s:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    add-int/2addr v2, v1

    iget-object v3, p0, LX/3ty;->s:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    add-int/2addr v3, v0

    .line 647099
    iget-object v4, p0, LX/3ty;->s:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    .line 647100
    iget-object v5, p0, LX/3ty;->s:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v5

    .line 647101
    if-eqz v1, :cond_4

    .line 647102
    iget-object v6, p0, LX/3ty;->r:LX/3nx;

    iget-object v7, p0, LX/3ty;->s:Landroid/view/View;

    invoke-virtual {v6, v7, v2}, LX/3nx;->b(Landroid/view/View;I)I

    move-result v2

    .line 647103
    iget-object v6, p0, LX/3ty;->s:Landroid/view/View;

    sub-int v4, v2, v4

    invoke-virtual {v6, v4}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 647104
    :cond_4
    if-eqz v0, :cond_5

    .line 647105
    iget-object v4, p0, LX/3ty;->r:LX/3nx;

    iget-object v6, p0, LX/3ty;->s:Landroid/view/View;

    invoke-virtual {v4, v6, v3}, LX/3nx;->c(Landroid/view/View;I)I

    move-result v4

    .line 647106
    iget-object v6, p0, LX/3ty;->s:Landroid/view/View;

    sub-int/2addr v4, v5

    invoke-virtual {v6, v4}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 647107
    :cond_5
    if-nez v1, :cond_6

    if-eqz v0, :cond_7

    .line 647108
    :cond_6
    iget-object v4, p0, LX/3ty;->r:LX/3nx;

    iget-object v5, p0, LX/3ty;->s:Landroid/view/View;

    invoke-virtual {v4, v5, v2}, LX/3nx;->a(Landroid/view/View;I)V

    .line 647109
    :cond_7
    invoke-direct {p0, p1}, LX/3ty;->c(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 647110
    :cond_8
    invoke-static {p1}, LX/2xd;->c(Landroid/view/MotionEvent;)I

    move-result v1

    .line 647111
    :goto_1
    if-ge v0, v1, :cond_a

    .line 647112
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v2

    .line 647113
    invoke-direct {p0, v2}, LX/3ty;->g(I)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 647114
    invoke-static {p1, v0}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v3

    .line 647115
    invoke-static {p1, v0}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v4

    .line 647116
    iget-object v5, p0, LX/3ty;->d:[F

    aget v5, v5, v2

    sub-float v5, v3, v5

    .line 647117
    iget-object v6, p0, LX/3ty;->e:[F

    aget v6, v6, v2

    sub-float v6, v4, v6

    .line 647118
    invoke-direct {p0, v5, v6, v2}, LX/3ty;->b(FFI)V

    .line 647119
    iget v7, p0, LX/3ty;->a:I

    if-eq v7, v8, :cond_a

    .line 647120
    float-to-int v3, v3

    float-to-int v4, v4

    invoke-virtual {p0, v3, v4}, LX/3ty;->b(II)Landroid/view/View;

    move-result-object v3

    .line 647121
    invoke-direct {p0, v3, v5, v6}, LX/3ty;->a(Landroid/view/View;FF)Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-direct {p0, v3, v2}, LX/3ty;->b(Landroid/view/View;I)Z

    move-result v2

    if-nez v2, :cond_a

    .line 647122
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 647123
    :cond_a
    invoke-direct {p0, p1}, LX/3ty;->c(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 647124
    :pswitch_4
    invoke-static {p1, v3}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v2

    .line 647125
    iget v3, p0, LX/3ty;->a:I

    if-ne v3, v8, :cond_b

    iget v3, p0, LX/3ty;->c:I

    if-ne v2, v3, :cond_b

    .line 647126
    invoke-static {p1}, LX/2xd;->c(Landroid/view/MotionEvent;)I

    move-result v3

    .line 647127
    :goto_2
    if-ge v0, v3, :cond_f

    .line 647128
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v4

    .line 647129
    iget v5, p0, LX/3ty;->c:I

    if-eq v4, v5, :cond_c

    .line 647130
    invoke-static {p1, v0}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v5

    .line 647131
    invoke-static {p1, v0}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v6

    .line 647132
    float-to-int v5, v5

    float-to-int v6, v6

    invoke-virtual {p0, v5, v6}, LX/3ty;->b(II)Landroid/view/View;

    move-result-object v5

    iget-object v6, p0, LX/3ty;->s:Landroid/view/View;

    if-ne v5, v6, :cond_c

    iget-object v5, p0, LX/3ty;->s:Landroid/view/View;

    invoke-direct {p0, v5, v4}, LX/3ty;->b(Landroid/view/View;I)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 647133
    iget v0, p0, LX/3ty;->c:I

    .line 647134
    :goto_3
    if-ne v0, v1, :cond_b

    .line 647135
    invoke-direct {p0}, LX/3ty;->h()V

    .line 647136
    :cond_b
    invoke-direct {p0, v2}, LX/3ty;->d(I)V

    goto/16 :goto_0

    .line 647137
    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 647138
    :pswitch_5
    iget v0, p0, LX/3ty;->a:I

    if-ne v0, v8, :cond_d

    .line 647139
    invoke-direct {p0}, LX/3ty;->h()V

    .line 647140
    :cond_d
    invoke-virtual {p0}, LX/3ty;->e()V

    goto/16 :goto_0

    .line 647141
    :pswitch_6
    iget v0, p0, LX/3ty;->a:I

    if-ne v0, v8, :cond_e

    .line 647142
    invoke-direct {p0, v5, v5}, LX/3ty;->a(FF)V

    .line 647143
    :cond_e
    invoke-virtual {p0}, LX/3ty;->e()V

    goto/16 :goto_0

    :cond_f
    move v0, v1

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 647144
    const/4 v0, -0x1

    iput v0, p0, LX/3ty;->c:I

    .line 647145
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 647146
    iget-object v0, p0, LX/3ty;->d:[F

    if-nez v0, :cond_1

    .line 647147
    :goto_0
    iget-object v0, p0, LX/3ty;->l:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 647148
    iget-object v0, p0, LX/3ty;->l:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 647149
    const/4 v0, 0x0

    iput-object v0, p0, LX/3ty;->l:Landroid/view/VelocityTracker;

    .line 647150
    :cond_0
    return-void

    .line 647151
    :cond_1
    iget-object v0, p0, LX/3ty;->d:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 647152
    iget-object v0, p0, LX/3ty;->e:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 647153
    iget-object v0, p0, LX/3ty;->f:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 647154
    iget-object v0, p0, LX/3ty;->g:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 647155
    iget-object v0, p0, LX/3ty;->h:[I

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    .line 647156
    iget-object v0, p0, LX/3ty;->i:[I

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    .line 647157
    iget-object v0, p0, LX/3ty;->j:[I

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    .line 647158
    iput v2, p0, LX/3ty;->k:I

    goto :goto_0
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 647159
    invoke-virtual {p0}, LX/3ty;->e()V

    .line 647160
    iget v0, p0, LX/3ty;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 647161
    iget-object v0, p0, LX/3ty;->q:LX/1Og;

    invoke-virtual {v0}, LX/1Og;->b()I

    .line 647162
    iget-object v0, p0, LX/3ty;->q:LX/1Og;

    invoke-virtual {v0}, LX/1Og;->c()I

    .line 647163
    iget-object v0, p0, LX/3ty;->q:LX/1Og;

    invoke-virtual {v0}, LX/1Og;->h()V

    .line 647164
    iget-object v0, p0, LX/3ty;->q:LX/1Og;

    invoke-virtual {v0}, LX/1Og;->b()I

    move-result v0

    .line 647165
    iget-object v1, p0, LX/3ty;->q:LX/1Og;

    invoke-virtual {v1}, LX/1Og;->c()I

    .line 647166
    iget-object v1, p0, LX/3ty;->r:LX/3nx;

    iget-object v2, p0, LX/3ty;->s:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, LX/3nx;->a(Landroid/view/View;I)V

    .line 647167
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/3ty;->b(I)V

    .line 647168
    return-void
.end method
