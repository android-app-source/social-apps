.class public final enum LX/3UN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3UN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3UN;

.field public static final enum HEADER:LX/3UN;

.field public static final enum SEE_ALL_FOOTER:LX/3UN;

.field public static final enum SEE_MORE_FOOTER:LX/3UN;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 585827
    new-instance v0, LX/3UN;

    const-string v1, "HEADER"

    invoke-direct {v0, v1, v2}, LX/3UN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3UN;->HEADER:LX/3UN;

    .line 585828
    new-instance v0, LX/3UN;

    const-string v1, "SEE_ALL_FOOTER"

    invoke-direct {v0, v1, v3}, LX/3UN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3UN;->SEE_ALL_FOOTER:LX/3UN;

    .line 585829
    new-instance v0, LX/3UN;

    const-string v1, "SEE_MORE_FOOTER"

    invoke-direct {v0, v1, v4}, LX/3UN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3UN;->SEE_MORE_FOOTER:LX/3UN;

    .line 585830
    const/4 v0, 0x3

    new-array v0, v0, [LX/3UN;

    sget-object v1, LX/3UN;->HEADER:LX/3UN;

    aput-object v1, v0, v2

    sget-object v1, LX/3UN;->SEE_ALL_FOOTER:LX/3UN;

    aput-object v1, v0, v3

    sget-object v1, LX/3UN;->SEE_MORE_FOOTER:LX/3UN;

    aput-object v1, v0, v4

    sput-object v0, LX/3UN;->$VALUES:[LX/3UN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 585831
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3UN;
    .locals 1

    .prologue
    .line 585832
    const-class v0, LX/3UN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3UN;

    return-object v0
.end method

.method public static values()[LX/3UN;
    .locals 1

    .prologue
    .line 585833
    sget-object v0, LX/3UN;->$VALUES:[LX/3UN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3UN;

    return-object v0
.end method
