.class public abstract LX/49c;
.super Landroid/content/BroadcastReceiver;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 674565
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()LX/282;
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 674566
    const/4 v0, 0x1

    return v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x26

    const v1, -0x2f1c7b6d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 674567
    invoke-virtual {p0}, LX/49c;->a()LX/282;

    move-result-object v1

    .line 674568
    const-string v2, "com.facebook.GET_PHONE_ID"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, LX/49c;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez v1, :cond_1

    .line 674569
    :cond_0
    const/16 v1, 0x27

    const v2, 0x2364a4cf

    invoke-static {p2, v4, v1, v2, v0}, LX/02F;->a(Landroid/content/Intent;IIII)V

    .line 674570
    :goto_0
    return-void

    .line 674571
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/49c;->getResultExtras(Z)Landroid/os/Bundle;

    move-result-object v2

    .line 674572
    const/4 v4, 0x0

    .line 674573
    const-string v3, "auth"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/app/PendingIntent;

    .line 674574
    if-nez v3, :cond_3

    move v3, v4

    .line 674575
    :goto_1
    move v2, v3

    .line 674576
    if-eqz v2, :cond_2

    .line 674577
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 674578
    const-string v3, "timestamp"

    iget-wide v4, v1, LX/282;->b:J

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 674579
    const/4 v3, -0x1

    iget-object v1, v1, LX/282;->a:Ljava/lang/String;

    invoke-virtual {p0, v3, v1, v2}, LX/49c;->setResult(ILjava/lang/String;Landroid/os/Bundle;)V

    .line 674580
    :cond_2
    const v1, 0x75554ef7

    invoke-static {p2, v1, v0}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0

    .line 674581
    :cond_3
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x11

    if-lt v5, v6, :cond_4

    .line 674582
    invoke-virtual {v3}, Landroid/app/PendingIntent;->getCreatorPackage()Ljava/lang/String;

    move-result-object v3

    .line 674583
    :goto_2
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/16 v6, 0x40

    invoke-virtual {v5, v3, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 674584
    invoke-static {v3}, LX/283;->a(Landroid/content/pm/PackageInfo;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    goto :goto_1

    .line 674585
    :cond_4
    invoke-virtual {v3}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 674586
    :catch_0
    move v3, v4

    goto :goto_1
.end method
