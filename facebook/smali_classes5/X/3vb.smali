.class public final LX/3vb;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field public final synthetic a:LX/3vd;

.field public b:LX/3va;

.field public c:I

.field public d:Z

.field private e:Z

.field private f:Z


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 652587
    iget v0, p0, LX/3vb;->c:I

    if-eq v0, p1, :cond_0

    .line 652588
    iput p1, p0, LX/3vb;->c:I

    .line 652589
    const v0, -0x56356685

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 652590
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 652575
    iget-boolean v0, p0, LX/3vb;->f:Z

    if-eq v0, p1, :cond_0

    .line 652576
    iput-boolean p1, p0, LX/3vb;->f:Z

    .line 652577
    const v0, 0x2f4a78b8

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 652578
    :cond_0
    return-void
.end method

.method public final a(ZZ)V
    .locals 1

    .prologue
    .line 652570
    iget-boolean v0, p0, LX/3vb;->d:Z

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, LX/3vb;->e:Z

    if-eq v0, p2, :cond_1

    .line 652571
    :cond_0
    iput-boolean p1, p0, LX/3vb;->d:Z

    .line 652572
    iput-boolean p2, p0, LX/3vb;->e:Z

    .line 652573
    const v0, 0x43a818f9

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 652574
    :cond_1
    return-void
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 652563
    iget-object v0, p0, LX/3vb;->b:LX/3va;

    invoke-virtual {v0}, LX/3va;->a()I

    move-result v0

    .line 652564
    iget-boolean v1, p0, LX/3vb;->d:Z

    if-nez v1, :cond_0

    iget-object v1, p0, LX/3vb;->b:LX/3va;

    invoke-virtual {v1}, LX/3va;->b()Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 652565
    add-int/lit8 v0, v0, -0x1

    .line 652566
    :cond_0
    iget v1, p0, LX/3vb;->c:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 652567
    iget-boolean v1, p0, LX/3vb;->f:Z

    if-eqz v1, :cond_1

    .line 652568
    add-int/lit8 v0, v0, 0x1

    .line 652569
    :cond_1
    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 652579
    invoke-virtual {p0, p1}, LX/3vb;->getItemViewType(I)I

    move-result v0

    .line 652580
    packed-switch v0, :pswitch_data_0

    .line 652581
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 652582
    :pswitch_0
    const/4 v0, 0x0

    .line 652583
    :goto_0
    return-object v0

    .line 652584
    :pswitch_1
    iget-boolean v0, p0, LX/3vb;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3vb;->b:LX/3va;

    invoke-virtual {v0}, LX/3va;->b()Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 652585
    add-int/lit8 p1, p1, 0x1

    .line 652586
    :cond_0
    iget-object v0, p0, LX/3vb;->b:LX/3va;

    invoke-virtual {v0, p1}, LX/3va;->a(I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 652562
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 652559
    iget-boolean v0, p0, LX/3vb;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/3vb;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    .line 652560
    const/4 v0, 0x1

    .line 652561
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 652539
    invoke-virtual {p0, p1}, LX/3vb;->getItemViewType(I)I

    move-result v0

    .line 652540
    packed-switch v0, :pswitch_data_0

    .line 652541
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 652542
    :pswitch_0
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v0

    if-eq v0, v5, :cond_1

    .line 652543
    :cond_0
    iget-object v0, p0, LX/3vb;->a:LX/3vd;

    invoke-virtual {v0}, LX/3vd;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030008

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 652544
    invoke-virtual {p2, v5}, Landroid/view/View;->setId(I)V

    .line 652545
    const v0, 0x7f0d02c4

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 652546
    iget-object v1, p0, LX/3vb;->a:LX/3vd;

    invoke-virtual {v1}, LX/3vd;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f08000e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 652547
    :cond_1
    :goto_0
    return-object p2

    .line 652548
    :pswitch_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0d0342

    if-eq v0, v1, :cond_3

    .line 652549
    :cond_2
    iget-object v0, p0, LX/3vb;->a:LX/3vd;

    invoke-virtual {v0}, LX/3vd;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030008

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 652550
    :cond_3
    iget-object v0, p0, LX/3vb;->a:LX/3vd;

    invoke-virtual {v0}, LX/3vd;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 652551
    const v0, 0x7f0d0343

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 652552
    invoke-virtual {p0, p1}, LX/3vb;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    .line 652553
    invoke-virtual {v1, v2}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 652554
    const v0, 0x7f0d02c4

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 652555
    invoke-virtual {v1, v2}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 652556
    iget-boolean v0, p0, LX/3vb;->d:Z

    if-eqz v0, :cond_4

    if-nez p1, :cond_4

    iget-boolean v0, p0, LX/3vb;->e:Z

    if-eqz v0, :cond_4

    .line 652557
    invoke-static {p2, v5}, LX/0vv;->b(Landroid/view/View;Z)V

    goto :goto_0

    .line 652558
    :cond_4
    invoke-static {p2, v4}, LX/0vv;->b(Landroid/view/View;Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 652538
    const/4 v0, 0x3

    return v0
.end method
