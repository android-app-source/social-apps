.class public final enum LX/4cd;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4cd;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4cd;

.field public static final enum CHECKED_CONNECTION_ONION:LX/4cd;

.field public static final enum CHECKED_CONNECTION_TOR:LX/4cd;

.field public static final enum CHECKING_CONNECTION:LX/4cd;

.field public static final enum CONNECTION_CHECK_ERROR:LX/4cd;

.field public static final enum CONNECTION_CHECK_FAILED:LX/4cd;

.field public static final enum PROXY_ERROR:LX/4cd;


# instance fields
.field public resId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 795259
    new-instance v0, LX/4cd;

    const-string v1, "CHECKING_CONNECTION"

    const v2, 0x7f08007f

    invoke-direct {v0, v1, v4, v2}, LX/4cd;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/4cd;->CHECKING_CONNECTION:LX/4cd;

    .line 795260
    new-instance v0, LX/4cd;

    const-string v1, "CHECKED_CONNECTION_TOR"

    const v2, 0x7f080080

    invoke-direct {v0, v1, v5, v2}, LX/4cd;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/4cd;->CHECKED_CONNECTION_TOR:LX/4cd;

    .line 795261
    new-instance v0, LX/4cd;

    const-string v1, "CHECKED_CONNECTION_ONION"

    const v2, 0x7f080081

    invoke-direct {v0, v1, v6, v2}, LX/4cd;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/4cd;->CHECKED_CONNECTION_ONION:LX/4cd;

    .line 795262
    new-instance v0, LX/4cd;

    const-string v1, "CONNECTION_CHECK_ERROR"

    const v2, 0x7f080083

    invoke-direct {v0, v1, v7, v2}, LX/4cd;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/4cd;->CONNECTION_CHECK_ERROR:LX/4cd;

    .line 795263
    new-instance v0, LX/4cd;

    const-string v1, "CONNECTION_CHECK_FAILED"

    const v2, 0x7f080083

    invoke-direct {v0, v1, v8, v2}, LX/4cd;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/4cd;->CONNECTION_CHECK_FAILED:LX/4cd;

    .line 795264
    new-instance v0, LX/4cd;

    const-string v1, "PROXY_ERROR"

    const/4 v2, 0x5

    const v3, 0x7f080083

    invoke-direct {v0, v1, v2, v3}, LX/4cd;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/4cd;->PROXY_ERROR:LX/4cd;

    .line 795265
    const/4 v0, 0x6

    new-array v0, v0, [LX/4cd;

    sget-object v1, LX/4cd;->CHECKING_CONNECTION:LX/4cd;

    aput-object v1, v0, v4

    sget-object v1, LX/4cd;->CHECKED_CONNECTION_TOR:LX/4cd;

    aput-object v1, v0, v5

    sget-object v1, LX/4cd;->CHECKED_CONNECTION_ONION:LX/4cd;

    aput-object v1, v0, v6

    sget-object v1, LX/4cd;->CONNECTION_CHECK_ERROR:LX/4cd;

    aput-object v1, v0, v7

    sget-object v1, LX/4cd;->CONNECTION_CHECK_FAILED:LX/4cd;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/4cd;->PROXY_ERROR:LX/4cd;

    aput-object v2, v0, v1

    sput-object v0, LX/4cd;->$VALUES:[LX/4cd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 795256
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 795257
    iput p3, p0, LX/4cd;->resId:I

    .line 795258
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4cd;
    .locals 1

    .prologue
    .line 795255
    const-class v0, LX/4cd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4cd;

    return-object v0
.end method

.method public static values()[LX/4cd;
    .locals 1

    .prologue
    .line 795254
    sget-object v0, LX/4cd;->$VALUES:[LX/4cd;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4cd;

    return-object v0
.end method


# virtual methods
.method public final hasEncounteredError()Z
    .locals 1

    .prologue
    .line 795253
    sget-object v0, LX/4cd;->CONNECTION_CHECK_ERROR:LX/4cd;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/4cd;->CONNECTION_CHECK_FAILED:LX/4cd;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/4cd;->PROXY_ERROR:LX/4cd;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isCheckedConnection()Z
    .locals 1

    .prologue
    .line 795252
    sget-object v0, LX/4cd;->CHECKED_CONNECTION_TOR:LX/4cd;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/4cd;->CHECKED_CONNECTION_ONION:LX/4cd;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isComplete()Z
    .locals 1

    .prologue
    .line 795251
    sget-object v0, LX/4cd;->CHECKING_CONNECTION:LX/4cd;

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
