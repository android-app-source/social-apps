.class public final enum LX/41B;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/41B;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/41B;

.field public static final enum DEVICE_BASED_LOGIN_TYPE:LX/41B;


# instance fields
.field private final mServerValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 666031
    new-instance v0, LX/41B;

    const-string v1, "DEVICE_BASED_LOGIN_TYPE"

    const-string v2, "device_based_login"

    invoke-direct {v0, v1, v3, v2}, LX/41B;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/41B;->DEVICE_BASED_LOGIN_TYPE:LX/41B;

    .line 666032
    const/4 v0, 0x1

    new-array v0, v0, [LX/41B;

    sget-object v1, LX/41B;->DEVICE_BASED_LOGIN_TYPE:LX/41B;

    aput-object v1, v0, v3

    sput-object v0, LX/41B;->$VALUES:[LX/41B;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 666033
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 666034
    iput-object p3, p0, LX/41B;->mServerValue:Ljava/lang/String;

    .line 666035
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/41B;
    .locals 1

    .prologue
    .line 666036
    const-class v0, LX/41B;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/41B;

    return-object v0
.end method

.method public static values()[LX/41B;
    .locals 1

    .prologue
    .line 666037
    sget-object v0, LX/41B;->$VALUES:[LX/41B;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/41B;

    return-object v0
.end method


# virtual methods
.method public final getServerValue()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 666038
    iget-object v0, p0, LX/41B;->mServerValue:Ljava/lang/String;

    return-object v0
.end method
