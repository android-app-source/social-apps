.class public LX/3ae;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 605870
    const v0, 0x7fffffff

    sput v0, LX/3ae;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 605871
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1su;B)V
    .locals 1

    .prologue
    .line 605872
    sget v0, LX/3ae;->a:I

    invoke-static {p0, p1, v0}, LX/3ae;->a(LX/1su;BI)V

    .line 605873
    return-void
.end method

.method private static a(LX/1su;BI)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 605874
    if-gtz p2, :cond_0

    .line 605875
    new-instance v0, LX/7H0;

    const-string v1, "Maximum skip depth exceeded"

    invoke-direct {v0, v1}, LX/7H0;-><init>(Ljava/lang/String;)V

    throw v0

    .line 605876
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 605877
    :cond_1
    :goto_0
    :pswitch_0
    return-void

    .line 605878
    :pswitch_1
    invoke-virtual {p0}, LX/1su;->j()Z

    goto :goto_0

    .line 605879
    :pswitch_2
    invoke-virtual {p0}, LX/1su;->k()B

    goto :goto_0

    .line 605880
    :pswitch_3
    invoke-virtual {p0}, LX/1su;->l()S

    goto :goto_0

    .line 605881
    :pswitch_4
    invoke-virtual {p0}, LX/1su;->m()I

    goto :goto_0

    .line 605882
    :pswitch_5
    invoke-virtual {p0}, LX/1su;->n()J

    goto :goto_0

    .line 605883
    :pswitch_6
    invoke-virtual {p0}, LX/1su;->o()D

    goto :goto_0

    .line 605884
    :pswitch_7
    invoke-virtual {p0}, LX/1su;->q()[B

    goto :goto_0

    .line 605885
    :pswitch_8
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    invoke-virtual {p0}, LX/1su;->d()LX/1sv;

    .line 605886
    :goto_1
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 605887
    iget-byte v1, v0, LX/1sw;->b:B

    if-eqz v1, :cond_2

    .line 605888
    iget-byte v0, v0, LX/1sw;->b:B

    add-int/lit8 v1, p2, -0x1

    invoke-static {p0, v0, v1}, LX/3ae;->a(LX/1su;BI)V

    goto :goto_1

    .line 605889
    :cond_2
    invoke-virtual {p0}, LX/1su;->e()V

    goto :goto_0

    .line 605890
    :pswitch_9
    invoke-virtual {p0}, LX/1su;->g()LX/7H3;

    move-result-object v1

    .line 605891
    :goto_2
    iget v2, v1, LX/7H3;->c:I

    if-gez v2, :cond_3

    invoke-static {}, LX/1su;->s()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 605892
    :goto_3
    iget-byte v2, v1, LX/7H3;->a:B

    add-int/lit8 v3, p2, -0x1

    invoke-static {p0, v2, v3}, LX/3ae;->a(LX/1su;BI)V

    .line 605893
    iget-byte v2, v1, LX/7H3;->b:B

    add-int/lit8 v3, p2, -0x1

    invoke-static {p0, v2, v3}, LX/3ae;->a(LX/1su;BI)V

    .line 605894
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 605895
    :cond_3
    iget v2, v1, LX/7H3;->c:I

    if-ge v0, v2, :cond_1

    goto :goto_3

    .line 605896
    :pswitch_a
    invoke-virtual {p0}, LX/1su;->i()LX/7H5;

    move-result-object v1

    .line 605897
    :goto_4
    iget v2, v1, LX/7H5;->b:I

    if-gez v2, :cond_4

    invoke-static {}, LX/1su;->u()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 605898
    :goto_5
    iget-byte v2, v1, LX/7H5;->a:B

    add-int/lit8 v3, p2, -0x1

    invoke-static {p0, v2, v3}, LX/3ae;->a(LX/1su;BI)V

    .line 605899
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 605900
    :cond_4
    iget v2, v1, LX/7H5;->b:I

    if-ge v0, v2, :cond_1

    goto :goto_5

    .line 605901
    :pswitch_b
    invoke-virtual {p0}, LX/1su;->h()LX/1u3;

    move-result-object v1

    .line 605902
    :goto_6
    iget v2, v1, LX/1u3;->b:I

    if-gez v2, :cond_5

    invoke-static {}, LX/1su;->t()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 605903
    :goto_7
    iget-byte v2, v1, LX/1u3;->a:B

    add-int/lit8 v3, p2, -0x1

    invoke-static {p0, v2, v3}, LX/3ae;->a(LX/1su;BI)V

    .line 605904
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 605905
    :cond_5
    iget v2, v1, LX/1u3;->b:I

    if-ge v0, v2, :cond_1

    goto :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_6
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method
