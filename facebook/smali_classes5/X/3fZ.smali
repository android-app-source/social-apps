.class public LX/3fZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/contacts/server/UpdateFavoriteContactsParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 622573
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 622574
    return-void
.end method

.method public static a(LX/0QB;)LX/3fZ;
    .locals 1

    .prologue
    .line 622575
    new-instance v0, LX/3fZ;

    invoke-direct {v0}, LX/3fZ;-><init>()V

    .line 622576
    move-object v0, v0

    .line 622577
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 622578
    check-cast p1, Lcom/facebook/contacts/server/UpdateFavoriteContactsParams;

    const/4 v0, 0x0

    .line 622579
    new-instance v5, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 622580
    iget-object v1, p1, Lcom/facebook/contacts/server/UpdateFavoriteContactsParams;->a:LX/0Px;

    move-object v4, v1

    .line 622581
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v6, :cond_0

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 622582
    add-int/lit8 v3, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 622583
    iget-object p0, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, p0

    .line 622584
    invoke-virtual {v5, v2, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 622585
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v2, v3

    goto :goto_0

    .line 622586
    :cond_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 622587
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 622588
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "favorites"

    invoke-virtual {v5}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 622589
    new-instance v0, LX/14N;

    const-string v1, "updateMessagingFavorites"

    const-string v2, "POST"

    const-string v3, "/me/messagingfavorites"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 622590
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 622591
    const/4 v0, 0x0

    return-object v0
.end method
