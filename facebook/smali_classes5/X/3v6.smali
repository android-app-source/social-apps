.class public final LX/3v6;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements LX/3v5;


# instance fields
.field public final a:Landroid/view/CollapsibleActionView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 650675
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    move-object v0, p1

    .line 650676
    check-cast v0, Landroid/view/CollapsibleActionView;

    iput-object v0, p0, LX/3v6;->a:Landroid/view/CollapsibleActionView;

    .line 650677
    invoke-virtual {p0, p1}, LX/3v6;->addView(Landroid/view/View;)V

    .line 650678
    return-void
.end method


# virtual methods
.method public final onActionViewCollapsed()V
    .locals 1

    .prologue
    .line 650679
    iget-object v0, p0, LX/3v6;->a:Landroid/view/CollapsibleActionView;

    invoke-interface {v0}, Landroid/view/CollapsibleActionView;->onActionViewCollapsed()V

    .line 650680
    return-void
.end method

.method public final onActionViewExpanded()V
    .locals 1

    .prologue
    .line 650681
    iget-object v0, p0, LX/3v6;->a:Landroid/view/CollapsibleActionView;

    invoke-interface {v0}, Landroid/view/CollapsibleActionView;->onActionViewExpanded()V

    .line 650682
    return-void
.end method
