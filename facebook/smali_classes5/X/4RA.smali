.class public LX/4RA;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 707930
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 707931
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 707932
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 707933
    :goto_0
    return v1

    .line 707934
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 707935
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 707936
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 707937
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 707938
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 707939
    const-string v6, "employer_context"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 707940
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 707941
    :cond_2
    const-string v6, "social_context"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 707942
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 707943
    :cond_3
    const-string v6, "tracking"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 707944
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 707945
    :cond_4
    const-string v6, "user"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 707946
    invoke-static {p0, p1}, LX/2bO;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 707947
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 707948
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 707949
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 707950
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 707951
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 707952
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 707953
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 707954
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 707955
    if-eqz v0, :cond_0

    .line 707956
    const-string v1, "employer_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707957
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 707958
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 707959
    if-eqz v0, :cond_1

    .line 707960
    const-string v1, "social_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707961
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 707962
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 707963
    if-eqz v0, :cond_2

    .line 707964
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707965
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 707966
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 707967
    if-eqz v0, :cond_3

    .line 707968
    const-string v1, "user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707969
    invoke-static {p0, v0, p2, p3}, LX/2bO;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 707970
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 707971
    return-void
.end method
