.class public final LX/3x1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:LX/3w1;


# direct methods
.method public constructor <init>(LX/3w1;)V
    .locals 0

    .prologue
    .line 656684
    iput-object p1, p0, LX/3x1;->a:LX/3w1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    .line 656685
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 656686
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    .line 656687
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    .line 656688
    if-nez v0, :cond_1

    iget-object v3, p0, LX/3x1;->a:LX/3w1;

    iget-object v3, v3, LX/3w1;->d:Landroid/widget/PopupWindow;

    if-eqz v3, :cond_1

    iget-object v3, p0, LX/3x1;->a:LX/3w1;

    iget-object v3, v3, LX/3w1;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_1

    if-ltz v1, :cond_1

    iget-object v3, p0, LX/3x1;->a:LX/3w1;

    iget-object v3, v3, LX/3w1;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v3

    if-ge v1, v3, :cond_1

    if-ltz v2, :cond_1

    iget-object v1, p0, LX/3x1;->a:LX/3w1;

    iget-object v1, v1, LX/3w1;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->getHeight()I

    move-result v1

    if-ge v2, v1, :cond_1

    .line 656689
    iget-object v0, p0, LX/3x1;->a:LX/3w1;

    iget-object v0, v0, LX/3w1;->A:Landroid/os/Handler;

    iget-object v1, p0, LX/3x1;->a:LX/3w1;

    iget-object v1, v1, LX/3w1;->v:Landroid/support/v7/widget/ListPopupWindow$ResizePopupRunnable;

    const-wide/16 v2, 0xfa

    const v4, 0x239fe0bc

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 656690
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 656691
    :cond_1
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 656692
    iget-object v0, p0, LX/3x1;->a:LX/3w1;

    iget-object v0, v0, LX/3w1;->A:Landroid/os/Handler;

    iget-object v1, p0, LX/3x1;->a:LX/3w1;

    iget-object v1, v1, LX/3w1;->v:Landroid/support/v7/widget/ListPopupWindow$ResizePopupRunnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    goto :goto_0
.end method
