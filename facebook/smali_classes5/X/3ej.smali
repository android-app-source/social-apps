.class public LX/3ej;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/43N;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 620742
    const-class v0, LX/3ej;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3ej;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/03V;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/43N;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 620743
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 620744
    iput-object p1, p0, LX/3ej;->b:LX/0Ot;

    .line 620745
    iput-object p2, p0, LX/3ej;->c:LX/03V;

    .line 620746
    return-void
.end method

.method private static a(LX/3ej;)LX/43N;
    .locals 4

    .prologue
    .line 620747
    iget-object v0, p0, LX/3ej;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/43N;

    .line 620748
    iget-boolean v1, v0, LX/43N;->b:Z

    move v1, v1

    .line 620749
    if-nez v1, :cond_1

    .line 620750
    new-instance v0, LX/5zf;

    const-string v1, "NativeImageLibrary not loaded for webp transcoding!"

    invoke-direct {v0, p0, v1}, LX/5zf;-><init>(LX/3ej;Ljava/lang/String;)V

    .line 620751
    iget-object v1, p0, LX/3ej;->c:LX/03V;

    if-eqz v1, :cond_0

    .line 620752
    iget-object v1, p0, LX/3ej;->c:LX/03V;

    sget-object v2, LX/3ej;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 620753
    :cond_0
    throw v0

    .line 620754
    :cond_1
    return-object v0
.end method

.method private static a(LX/3ej;LX/1lW;)Z
    .locals 1

    .prologue
    .line 620755
    invoke-static {p1}, LX/1ld;->b(LX/1lW;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 620756
    iget-object v0, p0, LX/3ej;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    const/4 v0, 0x0

    .line 620757
    sget-object p0, LX/1ld;->e:LX/1lW;

    if-ne p1, p0, :cond_2

    .line 620758
    :cond_0
    :goto_0
    move v0, v0

    .line 620759
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 620760
    :cond_2
    sget-object p0, LX/1ld;->f:LX/1lW;

    if-eq p1, p0, :cond_3

    sget-object p0, LX/1ld;->g:LX/1lW;

    if-eq p1, p0, :cond_3

    sget-object p0, LX/1ld;->h:LX/1lW;

    if-ne p1, p0, :cond_0

    .line 620761
    :cond_3
    sget-boolean p0, LX/1cG;->c:Z

    if-nez p0, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(LX/3ej;Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 4

    .prologue
    .line 620762
    invoke-static {p0}, LX/3ej;->a(LX/3ej;)LX/43N;

    move-result-object v0

    .line 620763
    const/16 v1, 0x50

    :try_start_0
    invoke-virtual {v0, p1, p2, v1}, LX/43N;->a(Ljava/io/InputStream;Ljava/io/OutputStream;I)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 620764
    return-void

    .line 620765
    :catch_0
    move-exception v0

    .line 620766
    new-instance v1, LX/5zf;

    const-string v2, "NativeImageLibraries reports loaded but fails!"

    invoke-direct {v1, p0, v2}, LX/5zf;-><init>(LX/3ej;Ljava/lang/String;)V

    .line 620767
    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 620768
    iget-object v0, p0, LX/3ej;->c:LX/03V;

    sget-object v2, LX/3ej;->a:Ljava/lang/String;

    const-string v3, "Transcode without library"

    invoke-virtual {v0, v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 620769
    throw v1
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 620770
    new-instance v0, Ljava/io/BufferedInputStream;

    const/16 v1, 0x2000

    invoke-direct {v0, p1, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 620771
    invoke-static {v0}, LX/1la;->a(Ljava/io/InputStream;)LX/1lW;

    move-result-object v1

    .line 620772
    invoke-static {p0, v1}, LX/3ej;->a(LX/3ej;LX/1lW;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 620773
    new-instance v1, LX/526;

    invoke-direct {v1, v0}, LX/526;-><init>(Ljava/io/InputStream;)V

    .line 620774
    new-instance v0, Ljava/io/BufferedOutputStream;

    invoke-direct {v0, p2}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 620775
    :try_start_0
    invoke-static {p0, v1, v0}, LX/3ej;->b(LX/3ej;Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 620776
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->flush()V

    .line 620777
    :goto_0
    return-void

    .line 620778
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->flush()V

    throw v1

    .line 620779
    :cond_0
    invoke-static {v0, p2}, LX/0hW;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    goto :goto_0
.end method
