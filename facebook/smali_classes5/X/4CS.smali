.class public LX/4CS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0UW;)V
    .locals 5

    .prologue
    .line 678846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 678847
    invoke-interface {p1}, LX/0UW;->b()Ljava/util/ArrayList;

    move-result-object v0

    .line 678848
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 678849
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 678850
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 678851
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v3, v4, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 678852
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 678853
    :cond_0
    move-object v0, v3

    .line 678854
    iput-object v0, p0, LX/4CS;->a:Ljava/util/Map;

    .line 678855
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 678856
    iget-object v0, p0, LX/4CS;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method
