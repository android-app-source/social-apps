.class public LX/4M5;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 685972
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 685973
    const/4 v7, 0x0

    .line 685974
    const/4 v6, 0x0

    .line 685975
    const-wide/16 v4, 0x0

    .line 685976
    const/4 v3, 0x0

    .line 685977
    const/4 v2, 0x0

    .line 685978
    const/4 v1, 0x0

    .line 685979
    const/4 v0, 0x0

    .line 685980
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v9, :cond_9

    .line 685981
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 685982
    const/4 v0, 0x0

    .line 685983
    :goto_0
    return v0

    .line 685984
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v5, :cond_5

    .line 685985
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 685986
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 685987
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v10, :cond_0

    if-eqz v0, :cond_0

    .line 685988
    const-string v5, "can_user_edit_rsvp_status_of_guest"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 685989
    const/4 v0, 0x1

    .line 685990
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    move v9, v4

    move v4, v0

    goto :goto_1

    .line 685991
    :cond_1
    const-string v5, "node"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 685992
    invoke-static {p0, p1}, LX/2bM;->a(LX/15w;LX/186;)I

    move-result v0

    move v8, v0

    goto :goto_1

    .line 685993
    :cond_2
    const-string v5, "rsvp_time"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 685994
    const/4 v0, 0x1

    .line 685995
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 685996
    :cond_3
    const-string v5, "seen_state"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 685997
    const/4 v0, 0x1

    .line 685998
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v5

    move v6, v0

    move-object v7, v5

    goto :goto_1

    .line 685999
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 686000
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 686001
    if-eqz v4, :cond_6

    .line 686002
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v9}, LX/186;->a(IZ)V

    .line 686003
    :cond_6
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 686004
    if-eqz v1, :cond_7

    .line 686005
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 686006
    :cond_7
    if-eqz v6, :cond_8

    .line 686007
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->a(ILjava/lang/Enum;)V

    .line 686008
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :cond_9
    move v8, v6

    move v9, v7

    move-object v7, v3

    move v6, v0

    move-wide v11, v4

    move v4, v2

    move-wide v2, v11

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 686009
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 686010
    invoke-virtual {p0, p1, v3}, LX/15i;->b(II)Z

    move-result v0

    .line 686011
    if-eqz v0, :cond_0

    .line 686012
    const-string v1, "can_user_edit_rsvp_status_of_guest"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 686013
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 686014
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 686015
    if-eqz v0, :cond_1

    .line 686016
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 686017
    invoke-static {p0, v0, p2, p3}, LX/2bM;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 686018
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 686019
    cmp-long v2, v0, v6

    if-eqz v2, :cond_2

    .line 686020
    const-string v2, "rsvp_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 686021
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 686022
    :cond_2
    invoke-virtual {p0, p1, v4, v3}, LX/15i;->a(IIS)S

    move-result v0

    .line 686023
    if-eqz v0, :cond_3

    .line 686024
    const-string v0, "seen_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 686025
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 686026
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 686027
    return-void
.end method
