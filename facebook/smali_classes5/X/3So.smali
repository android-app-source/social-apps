.class public abstract LX/3So;
.super LX/3Sg;
.source ""


# instance fields
.field private final a:I

.field private b:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 582976
    invoke-direct {p0}, LX/3Sg;-><init>()V

    .line 582977
    invoke-static {p2, p1}, LX/0PB;->checkPositionIndex(II)I

    .line 582978
    iput p1, p0, LX/3So;->a:I

    .line 582979
    iput p2, p0, LX/3So;->b:I

    .line 582980
    return-void
.end method


# virtual methods
.method public abstract a(I)LX/1vs;
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 582981
    iget v0, p0, LX/3So;->b:I

    iget v1, p0, LX/3So;->a:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/1vs;
    .locals 3

    .prologue
    .line 582982
    invoke-virtual {p0}, LX/3So;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 582983
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 582984
    :cond_0
    iget v0, p0, LX/3So;->b:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LX/3So;->b:I

    invoke-virtual {p0, v0}, LX/3So;->a(I)LX/1vs;

    move-result-object v0

    .line 582985
    iget-object v1, v0, LX/1vs;->a:LX/15i;

    .line 582986
    iget v2, v0, LX/1vs;->b:I

    .line 582987
    iget v0, v0, LX/1vs;->c:I

    .line 582988
    invoke-static {v1, v2, v0}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    return-object v0
.end method
