.class public LX/3Z3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 598019
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 598020
    return-void
.end method

.method public static a(LX/0QB;)LX/3Z3;
    .locals 3

    .prologue
    .line 598026
    const-class v1, LX/3Z3;

    monitor-enter v1

    .line 598027
    :try_start_0
    sget-object v0, LX/3Z3;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 598028
    sput-object v2, LX/3Z3;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 598029
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598030
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 598031
    new-instance v0, LX/3Z3;

    invoke-direct {v0}, LX/3Z3;-><init>()V

    .line 598032
    move-object v0, v0

    .line 598033
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 598034
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3Z3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 598035
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 598036
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 6

    .prologue
    .line 598021
    sget-object v0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->a:LX/1Cz;

    sget-object v1, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->a:LX/1Cz;

    sget-object v2, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;->a:LX/1Cz;

    sget-object v3, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->d:LX/1Cz;

    sget-object v4, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->a:LX/1Cz;

    sget-object v5, Lcom/facebook/feedplugins/video/VideoAttachmentViewCountComponentPartDefinition;->d:LX/1Cz;

    invoke-static/range {v0 .. v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 598022
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cz;

    .line 598023
    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 598024
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 598025
    :cond_0
    return-void
.end method
