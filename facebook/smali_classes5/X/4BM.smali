.class public LX/4BM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 677687
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/FutureOperationResult;
    .locals 3

    .prologue
    .line 677688
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 677689
    iget-object v2, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v2, v2

    .line 677690
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 677691
    invoke-virtual {p0, p1}, LX/4BM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/FutureOperationResult;

    move-result-object v0

    return-object v0
.end method
