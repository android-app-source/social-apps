.class public LX/3rt;
.super Landroid/view/ViewGroup;
.source ""

# interfaces
.implements LX/18z;


# static fields
.field private static final n:[I

.field private static final o:[I

.field private static final q:LX/3rw;


# instance fields
.field public a:Landroid/support/v4/view/ViewPager;

.field public b:Landroid/widget/TextView;

.field public c:Landroid/widget/TextView;

.field public d:Landroid/widget/TextView;

.field public e:I

.field private f:I

.field public g:F

.field public h:I

.field public i:I

.field private j:Z

.field private k:Z

.field private final l:LX/3rv;

.field private m:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/0gG;",
            ">;"
        }
    .end annotation
.end field

.field public p:I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 643363
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/3rt;->n:[I

    .line 643364
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x101038c

    aput v2, v0, v1

    sput-object v0, LX/3rt;->o:[I

    .line 643365
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 643366
    new-instance v0, LX/3ry;

    invoke-direct {v0}, LX/3ry;-><init>()V

    sput-object v0, LX/3rt;->q:LX/3rw;

    .line 643367
    :goto_0
    return-void

    .line 643368
    :cond_0
    new-instance v0, LX/3rx;

    invoke-direct {v0}, LX/3rx;-><init>()V

    sput-object v0, LX/3rt;->q:LX/3rw;

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x1010034
        0x1010095
        0x1010098
        0x10100af
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 643569
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3rt;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 643570
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x0

    .line 643527
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 643528
    const/4 v1, -0x1

    iput v1, p0, LX/3rt;->f:I

    .line 643529
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, LX/3rt;->g:F

    .line 643530
    new-instance v1, LX/3rv;

    invoke-direct {v1, p0}, LX/3rv;-><init>(LX/3rt;)V

    iput-object v1, p0, LX/3rt;->l:LX/3rv;

    .line 643531
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/3rt;->b:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, LX/3rt;->addView(Landroid/view/View;)V

    .line 643532
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/3rt;->c:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, LX/3rt;->addView(Landroid/view/View;)V

    .line 643533
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/3rt;->d:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, LX/3rt;->addView(Landroid/view/View;)V

    .line 643534
    sget-object v1, LX/3rt;->n:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 643535
    invoke-virtual {v1, v0, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 643536
    if-eqz v2, :cond_0

    .line 643537
    iget-object v3, p0, LX/3rt;->b:Landroid/widget/TextView;

    invoke-virtual {v3, p1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 643538
    iget-object v3, p0, LX/3rt;->c:Landroid/widget/TextView;

    invoke-virtual {v3, p1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 643539
    iget-object v3, p0, LX/3rt;->d:Landroid/widget/TextView;

    invoke-virtual {v3, p1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 643540
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {v1, v3, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    .line 643541
    if-eqz v3, :cond_1

    .line 643542
    int-to-float v3, v3

    invoke-virtual {p0, v0, v3}, LX/3rt;->a(IF)V

    .line 643543
    :cond_1
    invoke-virtual {v1, v4}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 643544
    invoke-virtual {v1, v4, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    .line 643545
    iget-object v4, p0, LX/3rt;->b:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 643546
    iget-object v4, p0, LX/3rt;->c:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 643547
    iget-object v4, p0, LX/3rt;->d:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 643548
    :cond_2
    const/4 v3, 0x3

    const/16 v4, 0x50

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    iput v3, p0, LX/3rt;->i:I

    .line 643549
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 643550
    iget-object v1, p0, LX/3rt;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v1

    iput v1, p0, LX/3rt;->e:I

    .line 643551
    const v1, 0x3f19999a    # 0.6f

    invoke-virtual {p0, v1}, LX/3rt;->setNonPrimaryAlpha(F)V

    .line 643552
    iget-object v1, p0, LX/3rt;->b:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 643553
    iget-object v1, p0, LX/3rt;->c:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 643554
    iget-object v1, p0, LX/3rt;->d:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 643555
    if-eqz v2, :cond_3

    .line 643556
    sget-object v1, LX/3rt;->o:[I

    invoke-virtual {p1, v2, v1}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 643557
    invoke-virtual {v1, v0, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 643558
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 643559
    :cond_3
    if-eqz v0, :cond_4

    .line 643560
    iget-object v0, p0, LX/3rt;->b:Landroid/widget/TextView;

    invoke-static {v0}, LX/3rt;->setSingleLineAllCaps(Landroid/widget/TextView;)V

    .line 643561
    iget-object v0, p0, LX/3rt;->c:Landroid/widget/TextView;

    invoke-static {v0}, LX/3rt;->setSingleLineAllCaps(Landroid/widget/TextView;)V

    .line 643562
    iget-object v0, p0, LX/3rt;->d:Landroid/widget/TextView;

    invoke-static {v0}, LX/3rt;->setSingleLineAllCaps(Landroid/widget/TextView;)V

    .line 643563
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 643564
    const/high16 v1, 0x41800000    # 16.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, LX/3rt;->h:I

    .line 643565
    return-void

    .line 643566
    :cond_4
    iget-object v0, p0, LX/3rt;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 643567
    iget-object v0, p0, LX/3rt;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 643568
    iget-object v0, p0, LX/3rt;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    goto :goto_0
.end method

.method private static setSingleLineAllCaps(Landroid/widget/TextView;)V
    .locals 1

    .prologue
    .line 643525
    sget-object v0, LX/3rt;->q:LX/3rw;

    invoke-interface {v0, p0}, LX/3rw;->a(Landroid/widget/TextView;)V

    .line 643526
    return-void
.end method


# virtual methods
.method public final a(IF)V
    .locals 1

    .prologue
    .line 643521
    iget-object v0, p0, LX/3rt;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1, p2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 643522
    iget-object v0, p0, LX/3rt;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1, p2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 643523
    iget-object v0, p0, LX/3rt;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1, p2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 643524
    return-void
.end method

.method public a(IFZ)V
    .locals 18

    .prologue
    .line 643464
    move-object/from16 v0, p0

    iget v2, v0, LX/3rt;->f:I

    move/from16 v0, p1

    if-eq v0, v2, :cond_2

    .line 643465
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3rt;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v2

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v2}, LX/3rt;->a(ILX/0gG;)V

    .line 643466
    :cond_0
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/3rt;->k:Z

    .line 643467
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3rt;->b:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    .line 643468
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3rt;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    .line 643469
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3rt;->d:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v6

    .line 643470
    div-int/lit8 v4, v3, 0x2

    .line 643471
    invoke-virtual/range {p0 .. p0}, LX/3rt;->getWidth()I

    move-result v7

    .line 643472
    invoke-virtual/range {p0 .. p0}, LX/3rt;->getHeight()I

    move-result v8

    .line 643473
    invoke-virtual/range {p0 .. p0}, LX/3rt;->getPaddingLeft()I

    move-result v9

    .line 643474
    invoke-virtual/range {p0 .. p0}, LX/3rt;->getPaddingRight()I

    move-result v10

    .line 643475
    invoke-virtual/range {p0 .. p0}, LX/3rt;->getPaddingTop()I

    move-result v11

    .line 643476
    invoke-virtual/range {p0 .. p0}, LX/3rt;->getPaddingBottom()I

    move-result v12

    .line 643477
    add-int v2, v9, v4

    .line 643478
    add-int v13, v10, v4

    .line 643479
    sub-int v2, v7, v2

    sub-int v14, v2, v13

    .line 643480
    const/high16 v2, 0x3f000000    # 0.5f

    add-float v2, v2, p2

    .line 643481
    const/high16 v15, 0x3f800000    # 1.0f

    cmpl-float v15, v2, v15

    if-lez v15, :cond_1

    .line 643482
    const/high16 v15, 0x3f800000    # 1.0f

    sub-float/2addr v2, v15

    .line 643483
    :cond_1
    sub-int v13, v7, v13

    int-to-float v14, v14

    mul-float/2addr v2, v14

    float-to-int v2, v2

    sub-int v2, v13, v2

    .line 643484
    sub-int v13, v2, v4

    .line 643485
    add-int v14, v13, v3

    .line 643486
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3rt;->b:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getBaseline()I

    move-result v2

    .line 643487
    move-object/from16 v0, p0

    iget-object v3, v0, LX/3rt;->c:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getBaseline()I

    move-result v3

    .line 643488
    move-object/from16 v0, p0

    iget-object v4, v0, LX/3rt;->d:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getBaseline()I

    move-result v4

    .line 643489
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v15

    invoke-static {v15, v4}, Ljava/lang/Math;->max(II)I

    move-result v15

    .line 643490
    sub-int v2, v15, v2

    .line 643491
    sub-int v3, v15, v3

    .line 643492
    sub-int/2addr v15, v4

    .line 643493
    move-object/from16 v0, p0

    iget-object v4, v0, LX/3rt;->b:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v2

    .line 643494
    move-object/from16 v0, p0

    iget-object v0, v0, LX/3rt;->c:Landroid/widget/TextView;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v16

    add-int v16, v16, v3

    .line 643495
    move-object/from16 v0, p0

    iget-object v0, v0, LX/3rt;->d:Landroid/widget/TextView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v17

    add-int v17, v17, v15

    .line 643496
    move/from16 v0, v16

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    move/from16 v0, v17

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 643497
    move-object/from16 v0, p0

    iget v0, v0, LX/3rt;->i:I

    move/from16 v16, v0

    and-int/lit8 v16, v16, 0x70

    .line 643498
    sparse-switch v16, :sswitch_data_0

    .line 643499
    add-int v4, v11, v2

    .line 643500
    add-int/2addr v3, v11

    .line 643501
    add-int v2, v11, v15

    .line 643502
    :goto_0
    move-object/from16 v0, p0

    iget-object v8, v0, LX/3rt;->c:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/3rt;->c:Landroid/widget/TextView;

    invoke-virtual {v11}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v11

    add-int/2addr v11, v3

    invoke-virtual {v8, v13, v3, v14, v11}, Landroid/widget/TextView;->layout(IIII)V

    .line 643503
    move-object/from16 v0, p0

    iget v3, v0, LX/3rt;->h:I

    sub-int v3, v13, v3

    sub-int/2addr v3, v5

    invoke-static {v9, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 643504
    move-object/from16 v0, p0

    iget-object v8, v0, LX/3rt;->b:Landroid/widget/TextView;

    add-int/2addr v5, v3

    move-object/from16 v0, p0

    iget-object v9, v0, LX/3rt;->b:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v4

    invoke-virtual {v8, v3, v4, v5, v9}, Landroid/widget/TextView;->layout(IIII)V

    .line 643505
    sub-int v3, v7, v10

    sub-int/2addr v3, v6

    move-object/from16 v0, p0

    iget v4, v0, LX/3rt;->h:I

    add-int/2addr v4, v14

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 643506
    move-object/from16 v0, p0

    iget-object v4, v0, LX/3rt;->d:Landroid/widget/TextView;

    add-int v5, v3, v6

    move-object/from16 v0, p0

    iget-object v6, v0, LX/3rt;->d:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v2

    invoke-virtual {v4, v3, v2, v5, v6}, Landroid/widget/TextView;->layout(IIII)V

    .line 643507
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, LX/3rt;->g:F

    .line 643508
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/3rt;->k:Z

    .line 643509
    :goto_1
    return-void

    .line 643510
    :cond_2
    if-nez p3, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, LX/3rt;->g:F

    cmpl-float v2, p2, v2

    if-nez v2, :cond_0

    goto :goto_1

    .line 643511
    :sswitch_0
    sub-int/2addr v8, v11

    sub-int/2addr v8, v12

    .line 643512
    sub-int v4, v8, v4

    div-int/lit8 v8, v4, 0x2

    .line 643513
    add-int v4, v8, v2

    .line 643514
    add-int/2addr v3, v8

    .line 643515
    add-int v2, v8, v15

    .line 643516
    goto :goto_0

    .line 643517
    :sswitch_1
    sub-int/2addr v8, v12

    sub-int/2addr v8, v4

    .line 643518
    add-int v4, v8, v2

    .line 643519
    add-int/2addr v3, v8

    .line 643520
    add-int v2, v8, v15

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x50 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(ILX/0gG;)V
    .locals 6

    .prologue
    const/high16 v5, -0x80000000

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 643441
    if-eqz p2, :cond_2

    invoke-virtual {p2}, LX/0gG;->b()I

    move-result v0

    .line 643442
    :goto_0
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/3rt;->j:Z

    .line 643443
    if-lez p1, :cond_4

    if-eqz p2, :cond_4

    .line 643444
    add-int/lit8 v2, p1, -0x1

    invoke-virtual {p2, v2}, LX/0gG;->G_(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 643445
    :goto_1
    iget-object v4, p0, LX/3rt;->b:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 643446
    iget-object v4, p0, LX/3rt;->c:Landroid/widget/TextView;

    if-eqz p2, :cond_3

    if-ge p1, v0, :cond_3

    invoke-virtual {p2, p1}, LX/0gG;->G_(I)Ljava/lang/CharSequence;

    move-result-object v2

    :goto_2
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 643447
    add-int/lit8 v2, p1, 0x1

    if-ge v2, v0, :cond_0

    if-eqz p2, :cond_0

    .line 643448
    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p2, v0}, LX/0gG;->G_(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 643449
    :cond_0
    iget-object v0, p0, LX/3rt;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 643450
    invoke-virtual {p0}, LX/3rt;->getWidth()I

    move-result v0

    invoke-virtual {p0}, LX/3rt;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, LX/3rt;->getPaddingRight()I

    move-result v2

    sub-int/2addr v0, v2

    .line 643451
    invoke-virtual {p0}, LX/3rt;->getHeight()I

    move-result v2

    invoke-virtual {p0}, LX/3rt;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, LX/3rt;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    .line 643452
    int-to-float v0, v0

    const v3, 0x3f4ccccd    # 0.8f

    mul-float/2addr v0, v3

    float-to-int v0, v0

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 643453
    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 643454
    iget-object v3, p0, LX/3rt;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v0, v2}, Landroid/widget/TextView;->measure(II)V

    .line 643455
    iget-object v3, p0, LX/3rt;->c:Landroid/widget/TextView;

    invoke-virtual {v3, v0, v2}, Landroid/widget/TextView;->measure(II)V

    .line 643456
    iget-object v3, p0, LX/3rt;->d:Landroid/widget/TextView;

    invoke-virtual {v3, v0, v2}, Landroid/widget/TextView;->measure(II)V

    .line 643457
    iput p1, p0, LX/3rt;->f:I

    .line 643458
    iget-boolean v0, p0, LX/3rt;->k:Z

    if-nez v0, :cond_1

    .line 643459
    iget v0, p0, LX/3rt;->g:F

    invoke-virtual {p0, p1, v0, v1}, LX/3rt;->a(IFZ)V

    .line 643460
    :cond_1
    iput-boolean v1, p0, LX/3rt;->j:Z

    .line 643461
    return-void

    :cond_2
    move v0, v1

    .line 643462
    goto :goto_0

    :cond_3
    move-object v2, v3

    .line 643463
    goto :goto_2

    :cond_4
    move-object v2, v3

    goto :goto_1
.end method

.method public final a(LX/0gG;LX/0gG;)V
    .locals 1

    .prologue
    .line 643429
    if-eqz p1, :cond_0

    .line 643430
    iget-object v0, p0, LX/3rt;->l:LX/3rv;

    invoke-virtual {p1, v0}, LX/0gG;->b(Landroid/database/DataSetObserver;)V

    .line 643431
    const/4 v0, 0x0

    iput-object v0, p0, LX/3rt;->m:Ljava/lang/ref/WeakReference;

    .line 643432
    :cond_0
    if-eqz p2, :cond_1

    .line 643433
    iget-object v0, p0, LX/3rt;->l:LX/3rv;

    invoke-virtual {p2, v0}, LX/0gG;->a(Landroid/database/DataSetObserver;)V

    .line 643434
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/3rt;->m:Ljava/lang/ref/WeakReference;

    .line 643435
    :cond_1
    iget-object v0, p0, LX/3rt;->a:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_2

    .line 643436
    const/4 v0, -0x1

    iput v0, p0, LX/3rt;->f:I

    .line 643437
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, LX/3rt;->g:F

    .line 643438
    iget-object v0, p0, LX/3rt;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    invoke-virtual {p0, v0, p2}, LX/3rt;->a(ILX/0gG;)V

    .line 643439
    invoke-virtual {p0}, LX/3rt;->requestLayout()V

    .line 643440
    :cond_2
    return-void
.end method

.method public getMinHeight()I
    .locals 2

    .prologue
    .line 643424
    const/4 v0, 0x0

    .line 643425
    invoke-virtual {p0}, LX/3rt;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 643426
    if-eqz v1, :cond_0

    .line 643427
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 643428
    :cond_0
    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2c

    const v1, 0x67e389eb

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 643411
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 643412
    invoke-virtual {p0}, LX/3rt;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 643413
    instance-of v2, v0, Landroid/support/v4/view/ViewPager;

    if-nez v2, :cond_0

    .line 643414
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "PagerTitleStrip must be a direct child of a ViewPager."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x2d

    const v3, 0x720003ff

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v0

    .line 643415
    :cond_0
    check-cast v0, Landroid/support/v4/view/ViewPager;

    .line 643416
    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v2

    .line 643417
    iget-object v3, p0, LX/3rt;->l:LX/3rv;

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->a(LX/0hc;)LX/0hc;

    .line 643418
    iget-object v3, p0, LX/3rt;->l:LX/3rv;

    .line 643419
    iput-object v3, v0, Landroid/support/v4/view/ViewPager;->ag:LX/3ru;

    .line 643420
    iput-object v0, p0, LX/3rt;->a:Landroid/support/v4/view/ViewPager;

    .line 643421
    iget-object v0, p0, LX/3rt;->m:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3rt;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gG;

    :goto_0
    invoke-virtual {p0, v0, v2}, LX/3rt;->a(LX/0gG;LX/0gG;)V

    .line 643422
    const v0, 0xf05e7d2

    invoke-static {v0, v1}, LX/02F;->g(II)V

    return-void

    .line 643423
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2c

    const v1, -0x741db510

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 643403
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 643404
    iget-object v1, p0, LX/3rt;->a:Landroid/support/v4/view/ViewPager;

    if-eqz v1, :cond_0

    .line 643405
    iget-object v1, p0, LX/3rt;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v1

    invoke-virtual {p0, v1, v2}, LX/3rt;->a(LX/0gG;LX/0gG;)V

    .line 643406
    iget-object v1, p0, LX/3rt;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->a(LX/0hc;)LX/0hc;

    .line 643407
    iget-object v1, p0, LX/3rt;->a:Landroid/support/v4/view/ViewPager;

    .line 643408
    iput-object v2, v1, Landroid/support/v4/view/ViewPager;->ag:LX/3ru;

    .line 643409
    iput-object v2, p0, LX/3rt;->a:Landroid/support/v4/view/ViewPager;

    .line 643410
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x3cf09815

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 643399
    iget-object v1, p0, LX/3rt;->a:Landroid/support/v4/view/ViewPager;

    if-eqz v1, :cond_1

    .line 643400
    iget v1, p0, LX/3rt;->g:F

    cmpl-float v1, v1, v0

    if-ltz v1, :cond_0

    iget v0, p0, LX/3rt;->g:F

    .line 643401
    :cond_0
    iget v1, p0, LX/3rt;->f:I

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, v2}, LX/3rt;->a(IFZ)V

    .line 643402
    :cond_1
    return-void
.end method

.method public final onMeasure(II)V
    .locals 10

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v8, -0x80000000

    .line 643380
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 643381
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 643382
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 643383
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 643384
    if-eq v0, v9, :cond_0

    .line 643385
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must measure with an exact width"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 643386
    :cond_0
    invoke-virtual {p0}, LX/3rt;->getMinHeight()I

    move-result v0

    .line 643387
    invoke-virtual {p0}, LX/3rt;->getPaddingTop()I

    move-result v4

    invoke-virtual {p0}, LX/3rt;->getPaddingBottom()I

    move-result v5

    add-int/2addr v4, v5

    .line 643388
    sub-int v5, v3, v4

    .line 643389
    int-to-float v6, v2

    const v7, 0x3f4ccccd    # 0.8f

    mul-float/2addr v6, v7

    float-to-int v6, v6

    invoke-static {v6, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 643390
    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 643391
    iget-object v7, p0, LX/3rt;->b:Landroid/widget/TextView;

    invoke-virtual {v7, v6, v5}, Landroid/widget/TextView;->measure(II)V

    .line 643392
    iget-object v7, p0, LX/3rt;->c:Landroid/widget/TextView;

    invoke-virtual {v7, v6, v5}, Landroid/widget/TextView;->measure(II)V

    .line 643393
    iget-object v7, p0, LX/3rt;->d:Landroid/widget/TextView;

    invoke-virtual {v7, v6, v5}, Landroid/widget/TextView;->measure(II)V

    .line 643394
    if-ne v1, v9, :cond_1

    .line 643395
    invoke-virtual {p0, v2, v3}, LX/3rt;->setMeasuredDimension(II)V

    .line 643396
    :goto_0
    return-void

    .line 643397
    :cond_1
    iget-object v1, p0, LX/3rt;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    .line 643398
    add-int/2addr v1, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v2, v0}, LX/3rt;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public final requestLayout()V
    .locals 1

    .prologue
    .line 643377
    iget-boolean v0, p0, LX/3rt;->j:Z

    if-nez v0, :cond_0

    .line 643378
    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 643379
    :cond_0
    return-void
.end method

.method public setNonPrimaryAlpha(F)V
    .locals 3

    .prologue
    .line 643372
    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr v0, p1

    float-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    iput v0, p0, LX/3rt;->p:I

    .line 643373
    iget v0, p0, LX/3rt;->p:I

    shl-int/lit8 v0, v0, 0x18

    iget v1, p0, LX/3rt;->e:I

    const v2, 0xffffff

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    .line 643374
    iget-object v1, p0, LX/3rt;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 643375
    iget-object v1, p0, LX/3rt;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 643376
    return-void
.end method

.method public setTextSpacing(I)V
    .locals 0

    .prologue
    .line 643369
    iput p1, p0, LX/3rt;->h:I

    .line 643370
    invoke-virtual {p0}, LX/3rt;->requestLayout()V

    .line 643371
    return-void
.end method
