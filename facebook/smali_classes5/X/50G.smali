.class public final LX/50G;
.super LX/1sm;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/1sm",
        "<TT;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field public final ordering:LX/1sm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1sm",
            "<-TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1sm;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1sm",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 823575
    invoke-direct {p0}, LX/1sm;-><init>()V

    .line 823576
    iput-object p1, p0, LX/50G;->ordering:LX/1sm;

    .line 823577
    return-void
.end method


# virtual methods
.method public final a()LX/1sm;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:TT;>()",
            "LX/1sm",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 823555
    iget-object v0, p0, LX/50G;->ordering:LX/1sm;

    invoke-virtual {v0}, LX/1sm;->a()LX/1sm;

    move-result-object v0

    invoke-virtual {v0}, LX/1sm;->c()LX/1sm;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/1sm;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:TT;>()",
            "LX/1sm",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 823574
    return-object p0
.end method

.method public final c()LX/1sm;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:TT;>()",
            "LX/1sm",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 823573
    iget-object v0, p0, LX/50G;->ordering:LX/1sm;

    invoke-virtual {v0}, LX/1sm;->c()LX/1sm;

    move-result-object v0

    return-object v0
.end method

.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)I"
        }
    .end annotation

    .prologue
    .line 823565
    if-ne p1, p2, :cond_0

    .line 823566
    const/4 v0, 0x0

    .line 823567
    :goto_0
    return v0

    .line 823568
    :cond_0
    if-nez p1, :cond_1

    .line 823569
    const/4 v0, -0x1

    goto :goto_0

    .line 823570
    :cond_1
    if-nez p2, :cond_2

    .line 823571
    const/4 v0, 0x1

    goto :goto_0

    .line 823572
    :cond_2
    iget-object v0, p0, LX/50G;->ordering:LX/1sm;

    invoke-virtual {v0, p1, p2}, LX/1sm;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 823558
    if-ne p1, p0, :cond_0

    .line 823559
    const/4 v0, 0x1

    .line 823560
    :goto_0
    return v0

    .line 823561
    :cond_0
    instance-of v0, p1, LX/50G;

    if-eqz v0, :cond_1

    .line 823562
    check-cast p1, LX/50G;

    .line 823563
    iget-object v0, p0, LX/50G;->ordering:LX/1sm;

    iget-object v1, p1, LX/50G;->ordering:LX/1sm;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 823564
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 823557
    iget-object v0, p0, LX/50G;->ordering:LX/1sm;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    const v1, 0x39153a74

    xor-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 823556
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/50G;->ordering:LX/1sm;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".nullsFirst()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
