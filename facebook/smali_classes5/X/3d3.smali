.class public LX/3d3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3d4;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3d4",
        "<",
        "LX/16i;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/18L;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 615968
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 615969
    new-instance v0, LX/18L;

    invoke-direct {v0}, LX/18L;-><init>()V

    iput-object v0, p0, LX/3d3;->b:LX/18L;

    .line 615970
    iput-object p1, p0, LX/3d3;->a:Ljava/util/Map;

    .line 615971
    return-void
.end method


# virtual methods
.method public final a(LX/16i;)LX/16i;
    .locals 6
    .param p1    # LX/16i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615972
    if-nez p1, :cond_1

    .line 615973
    const/4 p1, 0x0

    .line 615974
    :cond_0
    :goto_0
    return-object p1

    .line 615975
    :cond_1
    instance-of v0, p1, LX/16f;

    if-eqz v0, :cond_0

    .line 615976
    invoke-interface {p1}, LX/16i;->a()Ljava/lang/String;

    move-result-object v0

    .line 615977
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 615978
    iget-object v1, p0, LX/3d3;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 615979
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    move-object v1, p1

    .line 615980
    check-cast v1, LX/16f;

    iget-object v2, p0, LX/3d3;->b:LX/18L;

    .line 615981
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 615982
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 615983
    invoke-interface {v1, v4, v2}, LX/16f;->a(Ljava/lang/String;LX/18L;)V

    .line 615984
    iget-object v4, v2, LX/18L;->a:Ljava/lang/Object;

    .line 615985
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    .line 615986
    invoke-static {v4, v3}, LX/2lm;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 615987
    const/4 v3, 0x1

    .line 615988
    :goto_1
    move v1, v3

    .line 615989
    if-eqz v1, :cond_0

    .line 615990
    invoke-interface {p1}, LX/0jT;->t_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/16f;

    .line 615991
    iget-object v2, p0, LX/3d3;->b:LX/18L;

    .line 615992
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 615993
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 615994
    invoke-interface {v1, v4, v2}, LX/16f;->a(Ljava/lang/String;LX/18L;)V

    .line 615995
    iget-object p0, v2, LX/18L;->a:Ljava/lang/Object;

    .line 615996
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    .line 615997
    invoke-static {p0, v3}, LX/2lm;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 615998
    const/4 p0, 0x1

    invoke-interface {v1, v4, v3, p0}, LX/16f;->a(Ljava/lang/String;Ljava/lang/Object;Z)V

    goto :goto_2

    .line 615999
    :cond_4
    check-cast v1, LX/16i;

    move-object p1, v1

    goto/16 :goto_0

    :cond_5
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 616000
    check-cast p1, LX/16i;

    invoke-virtual {p0, p1}, LX/3d3;->a(LX/16i;)LX/16i;

    move-result-object v0

    return-object v0
.end method
