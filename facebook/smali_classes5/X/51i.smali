.class public abstract LX/51i;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/51h;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 825351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;)LX/51h;
    .locals 3

    .prologue
    .line 825354
    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    .line 825355
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2}, LX/51i;->a(C)LX/51h;

    .line 825356
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 825357
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)LX/51h;
    .locals 1

    .prologue
    .line 825353
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    invoke-virtual {p0, v0}, LX/51i;->b([B)LX/51h;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)LX/51h;
    .locals 1

    .prologue
    .line 825352
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, LX/51i;->b(B)LX/51h;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
