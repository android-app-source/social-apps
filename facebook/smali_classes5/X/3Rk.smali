.class public LX/3Rk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/0SG;

.field public final c:Ljava/util/Locale;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0SG;Ljava/util/Locale;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 581479
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 581480
    iput-object p1, p0, LX/3Rk;->a:Landroid/content/Context;

    .line 581481
    iput-object p2, p0, LX/3Rk;->b:LX/0SG;

    .line 581482
    iput-object p3, p0, LX/3Rk;->c:Ljava/util/Locale;

    .line 581483
    return-void
.end method

.method public static a(LX/0QB;)LX/3Rk;
    .locals 6

    .prologue
    .line 581484
    const-class v1, LX/3Rk;

    monitor-enter v1

    .line 581485
    :try_start_0
    sget-object v0, LX/3Rk;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 581486
    sput-object v2, LX/3Rk;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 581487
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 581488
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 581489
    new-instance p0, LX/3Rk;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0eD;->b(LX/0QB;)Ljava/util/Locale;

    move-result-object v5

    check-cast v5, Ljava/util/Locale;

    invoke-direct {p0, v3, v4, v5}, LX/3Rk;-><init>(Landroid/content/Context;LX/0SG;Ljava/util/Locale;)V

    .line 581490
    move-object v0, p0

    .line 581491
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 581492
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3Rk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 581493
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 581494
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/util/Date;
    .locals 15

    .prologue
    const/4 v0, 0x0

    .line 581495
    iget-object v1, p0, LX/3Rk;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "next_alarm_formatted"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 581496
    if-nez v1, :cond_0

    .line 581497
    :goto_0
    return-object v0

    .line 581498
    :cond_0
    const/16 v10, 0xc

    const/16 v9, 0xb

    const/4 v8, 0x7

    const/4 v7, 0x0

    .line 581499
    :try_start_0
    new-instance v3, Ljava/text/SimpleDateFormat;

    .line 581500
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x12

    if-lt v4, v5, :cond_2

    .line 581501
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    const-string v5, "Ehma"

    invoke-static {v4, v5}, Landroid/text/format/DateFormat;->getBestDateTimePattern(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 581502
    :goto_1
    move-object v4, v4

    .line 581503
    iget-object v5, p0, LX/3Rk;->c:Ljava/util/Locale;

    invoke-direct {v3, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v3, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    move-result-object v3

    .line 581504
    :goto_2
    iget-object v4, p0, LX/3Rk;->c:Ljava/util/Locale;

    invoke-static {v4}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v4

    .line 581505
    invoke-virtual {v4, v3}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 581506
    iget-object v11, p0, LX/3Rk;->c:Ljava/util/Locale;

    invoke-static {v11}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v11

    .line 581507
    iget-object v12, p0, LX/3Rk;->b:LX/0SG;

    invoke-interface {v12}, LX/0SG;->a()J

    move-result-wide v13

    invoke-virtual {v11, v13, v14}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 581508
    move-object v5, v11

    .line 581509
    invoke-virtual {v5}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Calendar;

    .line 581510
    invoke-virtual {v4, v8}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-virtual {v3, v8, v6}, Ljava/util/Calendar;->set(II)V

    .line 581511
    invoke-virtual {v4, v9}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-virtual {v3, v9, v6}, Ljava/util/Calendar;->set(II)V

    .line 581512
    invoke-virtual {v4, v10}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v3, v10, v4}, Ljava/util/Calendar;->set(II)V

    .line 581513
    const/16 v4, 0xd

    invoke-virtual {v3, v4, v7}, Ljava/util/Calendar;->set(II)V

    .line 581514
    const/16 v4, 0xe

    invoke-virtual {v3, v4, v7}, Ljava/util/Calendar;->set(II)V

    .line 581515
    invoke-virtual {v3, v5}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 581516
    const/4 v4, 0x3

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->add(II)V

    .line 581517
    :cond_1
    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    move-object v0, v3
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_0

    .line 581518
    goto :goto_0

    .line 581519
    :catch_0
    goto :goto_0

    .line 581520
    :catch_1
    :try_start_2
    new-instance v3, Ljava/text/SimpleDateFormat;

    .line 581521
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x12

    if-lt v4, v5, :cond_3

    .line 581522
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    const-string v5, "EHm"

    invoke-static {v4, v5}, Landroid/text/format/DateFormat;->getBestDateTimePattern(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 581523
    :goto_3
    move-object v4, v4

    .line 581524
    iget-object v5, p0, LX/3Rk;->c:Ljava/util/Locale;

    invoke-direct {v3, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v3, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    goto :goto_2
    :try_end_2
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_2
    :try_start_3
    const-string v4, "E h:mm aa"

    goto :goto_1
    :try_end_3
    .catch Ljava/text/ParseException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/text/ParseException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_3
    const-string v4, "E k:mm"

    goto :goto_3
.end method
