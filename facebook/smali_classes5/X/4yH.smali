.class public final LX/4yH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/GwtIncompatible;
    value = "serialization"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field public final map:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<TK;*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0P1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<TK;*>;)V"
        }
    .end annotation

    .prologue
    .line 821677
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 821678
    iput-object p1, p0, LX/4yH;->map:LX/0P1;

    .line 821679
    return-void
.end method


# virtual methods
.method public readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 821676
    iget-object v0, p0, LX/4yH;->map:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    return-object v0
.end method
