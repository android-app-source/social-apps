.class public LX/468;
.super LX/467;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/467",
        "<",
        "Lcom/facebook/common/locale/Country;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 670858
    invoke-direct {p0}, LX/467;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Locale;)Lcom/facebook/common/locale/LocaleMember;
    .locals 1

    .prologue
    .line 670859
    new-instance v0, Lcom/facebook/common/locale/Country;

    invoke-direct {v0, p1}, Lcom/facebook/common/locale/Country;-><init>(Ljava/util/Locale;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/util/Locale;
    .locals 2

    .prologue
    .line 670860
    new-instance v0, Ljava/util/Locale;

    const-string v1, ""

    invoke-direct {v0, v1, p1}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 670861
    invoke-static {}, Ljava/util/Locale;->getISOCountries()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
