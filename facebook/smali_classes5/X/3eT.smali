.class public LX/3eT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/stickers/model/StickerPack;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/3eT;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 620220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 620221
    return-void
.end method

.method public static a(LX/0QB;)LX/3eT;
    .locals 3

    .prologue
    .line 620222
    sget-object v0, LX/3eT;->a:LX/3eT;

    if-nez v0, :cond_1

    .line 620223
    const-class v1, LX/3eT;

    monitor-enter v1

    .line 620224
    :try_start_0
    sget-object v0, LX/3eT;->a:LX/3eT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 620225
    if-eqz v2, :cond_0

    .line 620226
    :try_start_1
    new-instance v0, LX/3eT;

    invoke-direct {v0}, LX/3eT;-><init>()V

    .line 620227
    move-object v0, v0

    .line 620228
    sput-object v0, LX/3eT;->a:LX/3eT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 620229
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 620230
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 620231
    :cond_1
    sget-object v0, LX/3eT;->a:LX/3eT;

    return-object v0

    .line 620232
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 620233
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 620234
    check-cast p1, Lcom/facebook/stickers/model/StickerPack;

    .line 620235
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 620236
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "pack_id"

    .line 620237
    iget-object v2, p1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v2, v2

    .line 620238
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 620239
    new-instance v0, LX/14N;

    const-string v1, "addStickerPack"

    const-string v2, "POST"

    const-string v3, "me/stickerpacks"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 620240
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 620241
    const/4 v0, 0x0

    return-object v0
.end method
