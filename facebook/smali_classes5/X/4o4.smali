.class public final enum LX/4o4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4o4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4o4;

.field public static final enum HORIZONTAL:LX/4o4;

.field public static final enum VERTICAL:LX/4o4;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 808525
    new-instance v0, LX/4o4;

    const-string v1, "HORIZONTAL"

    invoke-direct {v0, v1, v2}, LX/4o4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4o4;->HORIZONTAL:LX/4o4;

    .line 808526
    new-instance v0, LX/4o4;

    const-string v1, "VERTICAL"

    invoke-direct {v0, v1, v3}, LX/4o4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4o4;->VERTICAL:LX/4o4;

    .line 808527
    const/4 v0, 0x2

    new-array v0, v0, [LX/4o4;

    sget-object v1, LX/4o4;->HORIZONTAL:LX/4o4;

    aput-object v1, v0, v2

    sget-object v1, LX/4o4;->VERTICAL:LX/4o4;

    aput-object v1, v0, v3

    sput-object v0, LX/4o4;->$VALUES:[LX/4o4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 808528
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4o4;
    .locals 1

    .prologue
    .line 808529
    const-class v0, LX/4o4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4o4;

    return-object v0
.end method

.method public static values()[LX/4o4;
    .locals 1

    .prologue
    .line 808530
    sget-object v0, LX/4o4;->$VALUES:[LX/4o4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4o4;

    return-object v0
.end method
