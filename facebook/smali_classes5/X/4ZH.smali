.class public final LX/4ZH;
.super LX/0ur;
.source ""


# instance fields
.field public b:I

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 788041
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 788042
    instance-of v0, p0, LX/4ZH;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 788043
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/4ZH;
    .locals 2

    .prologue
    .line 788033
    new-instance v0, LX/4ZH;

    invoke-direct {v0}, LX/4ZH;-><init>()V

    .line 788034
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 788035
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->a()I

    move-result v1

    iput v1, v0, LX/4ZH;->b:I

    .line 788036
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->j()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4ZH;->c:LX/0Px;

    .line 788037
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    iput-object v1, v0, LX/4ZH;->d:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 788038
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->b()I

    move-result v1

    iput v1, v0, LX/4ZH;->e:I

    .line 788039
    invoke-static {v0, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 788040
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;
    .locals 2

    .prologue
    .line 788031
    new-instance v0, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;-><init>(LX/4ZH;)V

    .line 788032
    return-object v0
.end method
