.class public final LX/51u;
.super LX/51j;
.source ""


# instance fields
.field private final a:Ljava/security/MessageDigest;

.field private final b:I

.field private c:Z


# direct methods
.method public constructor <init>(Ljava/security/MessageDigest;I)V
    .locals 0

    .prologue
    .line 825532
    invoke-direct {p0}, LX/51j;-><init>()V

    .line 825533
    iput-object p1, p0, LX/51u;->a:Ljava/security/MessageDigest;

    .line 825534
    iput p2, p0, LX/51u;->b:I

    .line 825535
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 825517
    iget-boolean v0, p0, LX/51u;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cannot re-use a Hasher after calling hash() on it"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 825518
    return-void

    .line 825519
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/51o;
    .locals 2

    .prologue
    .line 825529
    invoke-direct {p0}, LX/51u;->b()V

    .line 825530
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/51u;->c:Z

    .line 825531
    iget v0, p0, LX/51u;->b:I

    iget-object v1, p0, LX/51u;->a:Ljava/security/MessageDigest;

    invoke-virtual {v1}, Ljava/security/MessageDigest;->getDigestLength()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/51u;->a:Ljava/security/MessageDigest;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    invoke-static {v0}, LX/51o;->a([B)LX/51o;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/51u;->a:Ljava/security/MessageDigest;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    iget v1, p0, LX/51u;->b:I

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    invoke-static {v0}, LX/51o;->a([B)LX/51o;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(B)V
    .locals 1

    .prologue
    .line 825526
    invoke-direct {p0}, LX/51u;->b()V

    .line 825527
    iget-object v0, p0, LX/51u;->a:Ljava/security/MessageDigest;

    invoke-virtual {v0, p1}, Ljava/security/MessageDigest;->update(B)V

    .line 825528
    return-void
.end method

.method public final a([B)V
    .locals 1

    .prologue
    .line 825523
    invoke-direct {p0}, LX/51u;->b()V

    .line 825524
    iget-object v0, p0, LX/51u;->a:Ljava/security/MessageDigest;

    invoke-virtual {v0, p1}, Ljava/security/MessageDigest;->update([B)V

    .line 825525
    return-void
.end method

.method public final a([BII)V
    .locals 1

    .prologue
    .line 825520
    invoke-direct {p0}, LX/51u;->b()V

    .line 825521
    iget-object v0, p0, LX/51u;->a:Ljava/security/MessageDigest;

    invoke-virtual {v0, p1, p2, p3}, Ljava/security/MessageDigest;->update([BII)V

    .line 825522
    return-void
.end method
