.class public LX/3yQ;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Ljava/lang/String;


# instance fields
.field public final clientOverrideGroupIndex:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "client_override_group_index"
    .end annotation
.end field

.field public final groupNames:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "group_names"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final isInExperiment:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_in_experiment"
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field

.field public final quicker:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "quicker"
    .end annotation
.end field

.field public final serverAssignedGroupIndex:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "server_assigned_group_index"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 661009
    const-string v0, "unassigned"

    sput-object v0, LX/3yQ;->a:Ljava/lang/String;

    .line 661010
    const-string v0, "unavailable"

    sput-object v0, LX/3yQ;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZLX/0Px;IIZ)V
    .locals 0
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "AndroidTrapUsingDefaultJsonDeserializer"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;IIZ)V"
        }
    .end annotation

    .prologue
    .line 660992
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 660993
    iput-object p1, p0, LX/3yQ;->name:Ljava/lang/String;

    .line 660994
    iput-boolean p2, p0, LX/3yQ;->isInExperiment:Z

    .line 660995
    iput-object p3, p0, LX/3yQ;->groupNames:LX/0Px;

    .line 660996
    iput p4, p0, LX/3yQ;->serverAssignedGroupIndex:I

    .line 660997
    iput p5, p0, LX/3yQ;->clientOverrideGroupIndex:I

    .line 660998
    iput-boolean p6, p0, LX/3yQ;->quicker:Z

    .line 660999
    return-void
.end method


# virtual methods
.method public final c()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 661001
    iget-boolean v0, p0, LX/3yQ;->isInExperiment:Z

    if-nez v0, :cond_0

    .line 661002
    sget-object v0, LX/3yQ;->a:Ljava/lang/String;

    .line 661003
    :goto_0
    return-object v0

    .line 661004
    :cond_0
    iget v0, p0, LX/3yQ;->clientOverrideGroupIndex:I

    if-eq v0, v1, :cond_1

    .line 661005
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, LX/3yQ;->groupNames:LX/0Px;

    iget v2, p0, LX/3yQ;->clientOverrideGroupIndex:I

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (client override)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 661006
    :cond_1
    iget v0, p0, LX/3yQ;->serverAssignedGroupIndex:I

    if-eq v0, v1, :cond_2

    .line 661007
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, LX/3yQ;->groupNames:LX/0Px;

    iget v2, p0, LX/3yQ;->serverAssignedGroupIndex:I

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (server group)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 661008
    :cond_2
    sget-object v0, LX/3yQ;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 661000
    iget v0, p0, LX/3yQ;->clientOverrideGroupIndex:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
