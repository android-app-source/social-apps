.class public LX/3d6;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 616006
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bundleForException(Ljava/lang/Throwable;)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 616007
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 616008
    const-string v1, "originalExceptionMessage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 616009
    const-string v1, "originalExceptionStack"

    invoke-static {p0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 616010
    instance-of v1, p0, LX/2Op;

    if-eqz v1, :cond_0

    .line 616011
    check-cast p0, LX/2Op;

    .line 616012
    const-string v1, "result"

    invoke-interface {p0}, LX/2Op;->getExtraData()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 616013
    :cond_0
    return-object v0
.end method

.method public static forException(Ljava/lang/Throwable;)LX/1nY;
    .locals 3

    .prologue
    .line 616014
    invoke-static {p0}, LX/3d7;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 616015
    sget-object v0, LX/1nY;->HTTP_400_AUTHENTICATION:LX/1nY;

    .line 616016
    :goto_0
    return-object v0

    .line 616017
    :cond_0
    invoke-static {p0}, LX/3d7;->f(Ljava/lang/Throwable;)Lorg/apache/http/client/HttpResponseException;

    move-result-object v0

    .line 616018
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v1

    const/16 v2, 0x190

    if-lt v1, v2, :cond_9

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v0

    const/16 v1, 0x1f4

    if-ge v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 616019
    if-eqz v0, :cond_1

    .line 616020
    sget-object v0, LX/1nY;->HTTP_400_OTHER:LX/1nY;

    goto :goto_0

    .line 616021
    :cond_1
    invoke-static {p0}, LX/3d7;->f(Ljava/lang/Throwable;)Lorg/apache/http/client/HttpResponseException;

    move-result-object v0

    .line 616022
    if-eqz v0, :cond_a

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v1

    const/16 v2, 0x1f4

    if-lt v1, v2, :cond_a

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v0

    const/16 v1, 0x258

    if-ge v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 616023
    if-eqz v0, :cond_2

    .line 616024
    sget-object v0, LX/1nY;->HTTP_500_CLASS:LX/1nY;

    goto :goto_0

    .line 616025
    :cond_2
    instance-of v0, p0, LX/2Oo;

    if-eqz v0, :cond_3

    .line 616026
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    goto :goto_0

    .line 616027
    :cond_3
    const-class v0, Ljava/io/IOException;

    invoke-static {p0, v0}, LX/3d7;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Z

    move-result v0

    move v0, v0

    .line 616028
    if-eqz v0, :cond_5

    .line 616029
    if-eqz p0, :cond_4

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Could not validate certificate"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 616030
    sget-object v0, LX/1nY;->DATE_ERROR:LX/1nY;

    goto :goto_0

    .line 616031
    :cond_4
    sget-object v0, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    goto :goto_0

    .line 616032
    :cond_5
    const-class v0, Ljava/lang/OutOfMemoryError;

    invoke-static {p0, v0}, LX/3d7;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Z

    move-result v0

    move v0, v0

    .line 616033
    if-eqz v0, :cond_6

    .line 616034
    sget-object v0, LX/1nY;->OUT_OF_MEMORY:LX/1nY;

    goto :goto_0

    .line 616035
    :cond_6
    instance-of v0, p0, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_7

    instance-of v0, p0, Ljava/lang/InterruptedException;

    if-eqz v0, :cond_8

    .line 616036
    :cond_7
    sget-object v0, LX/1nY;->CANCELLED:LX/1nY;

    goto :goto_0

    .line 616037
    :cond_8
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    goto :goto_0

    :cond_9
    const/4 v0, 0x0

    goto :goto_1

    :cond_a
    const/4 v0, 0x0

    goto :goto_2
.end method
