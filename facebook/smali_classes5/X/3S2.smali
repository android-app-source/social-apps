.class public LX/3S2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/3S2;


# instance fields
.field private final a:LX/0Zb;

.field public final b:LX/29E;

.field private final c:LX/2bC;


# direct methods
.method public constructor <init>(LX/0Zb;LX/29E;LX/2bC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 581733
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 581734
    iput-object p1, p0, LX/3S2;->a:LX/0Zb;

    .line 581735
    iput-object p2, p0, LX/3S2;->b:LX/29E;

    .line 581736
    iput-object p3, p0, LX/3S2;->c:LX/2bC;

    .line 581737
    return-void
.end method

.method public static a(LX/0QB;)LX/3S2;
    .locals 6

    .prologue
    .line 581720
    sget-object v0, LX/3S2;->d:LX/3S2;

    if-nez v0, :cond_1

    .line 581721
    const-class v1, LX/3S2;

    monitor-enter v1

    .line 581722
    :try_start_0
    sget-object v0, LX/3S2;->d:LX/3S2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 581723
    if-eqz v2, :cond_0

    .line 581724
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 581725
    new-instance p0, LX/3S2;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/29E;->b(LX/0QB;)LX/29E;

    move-result-object v4

    check-cast v4, LX/29E;

    invoke-static {v0}, LX/2bC;->a(LX/0QB;)LX/2bC;

    move-result-object v5

    check-cast v5, LX/2bC;

    invoke-direct {p0, v3, v4, v5}, LX/3S2;-><init>(LX/0Zb;LX/29E;LX/2bC;)V

    .line 581726
    move-object v0, p0

    .line 581727
    sput-object v0, LX/3S2;->d:LX/3S2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 581728
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 581729
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 581730
    :cond_1
    sget-object v0, LX/3S2;->d:LX/3S2;

    return-object v0

    .line 581731
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 581732
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/3S2;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 581752
    const-string v0, "notification"

    .line 581753
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 581754
    iget-object v0, p0, LX/3S2;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 581755
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 581756
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "messaging_notification_add_to_tray"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 581757
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-static {p2, v2}, LX/29E;->a(Ljava/util/Map;[Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    .line 581758
    const-string v2, "notif_type"

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 581759
    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 581760
    invoke-static {p0, v0}, LX/3S2;->a(LX/3S2;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 581761
    iget-object v0, p0, LX/3S2;->c:LX/2bC;

    invoke-virtual {v0, v3, p1, v3, v3}, LX/2bC;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 581762
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/util/Map;)V
    .locals 11
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 581738
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "messaging_notification_click_from_tray"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 581739
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-static {p2, v2}, LX/29E;->a(Ljava/util/Map;[Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    .line 581740
    const-string v2, "notif_type"

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 581741
    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 581742
    invoke-static {p0, v0}, LX/3S2;->a(LX/3S2;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 581743
    if-eqz p2, :cond_2

    const-string v0, "push_type"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 581744
    iget-object v3, p0, LX/3S2;->c:LX/2bC;

    const-string v0, "push_source"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "push_id"

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "push_type"

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v8, 0x0

    .line 581745
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "source"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-static {v4}, LX/29E;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v7

    .line 581746
    if-eqz v1, :cond_0

    .line 581747
    const-string v4, "push_id"

    invoke-interface {v7, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 581748
    :cond_0
    if-eqz v2, :cond_1

    .line 581749
    const-string v4, "type"

    invoke-interface {v7, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 581750
    :cond_1
    const-string v5, "messaging_push_notif"

    const-string v6, "click_from_tray"

    move-object v4, v3

    move-object v9, v8

    move-object v10, v8

    invoke-virtual/range {v4 .. v10}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 581751
    :cond_2
    return-void
.end method
