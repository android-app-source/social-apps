.class public LX/4c5;
.super Lorg/apache/http/entity/HttpEntityWrapper;
.source ""


# instance fields
.field public final a:LX/4c3;

.field private b:LX/4cF;


# direct methods
.method public constructor <init>(LX/4c3;Lorg/apache/http/HttpEntity;)V
    .locals 0

    .prologue
    .line 794570
    invoke-direct {p0, p2}, Lorg/apache/http/entity/HttpEntityWrapper;-><init>(Lorg/apache/http/HttpEntity;)V

    .line 794571
    iput-object p1, p0, LX/4c5;->a:LX/4c3;

    .line 794572
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 794551
    invoke-virtual {p0}, LX/4c5;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 794552
    :try_start_0
    sget-object v0, LX/0hW;->b:Ljava/io/OutputStream;

    move-object v0, v0

    .line 794553
    invoke-static {v1, v0}, LX/0hW;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 794554
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 794555
    return-void

    .line 794556
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
.end method


# virtual methods
.method public final consumeContent()V
    .locals 1

    .prologue
    .line 794565
    :try_start_0
    iget-object v0, p0, LX/4c5;->a:LX/4c3;

    invoke-virtual {v0}, LX/4c3;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 794566
    invoke-direct {p0}, LX/4c5;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 794567
    :cond_0
    invoke-super {p0}, Lorg/apache/http/entity/HttpEntityWrapper;->consumeContent()V

    .line 794568
    return-void

    .line 794569
    :catchall_0
    move-exception v0

    invoke-super {p0}, Lorg/apache/http/entity/HttpEntityWrapper;->consumeContent()V

    throw v0
.end method

.method public final getContent()Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 794558
    iget-object v0, p0, LX/4c5;->b:LX/4cF;

    if-nez v0, :cond_0

    .line 794559
    invoke-super {p0}, Lorg/apache/http/entity/HttpEntityWrapper;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 794560
    iget-object v1, p0, LX/4c5;->a:LX/4c3;

    invoke-virtual {v1, v0}, LX/4c3;->a(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v0

    .line 794561
    new-instance v1, LX/4cF;

    new-instance v2, LX/4c1;

    new-instance v3, LX/4c4;

    invoke-direct {v3, p0}, LX/4c4;-><init>(LX/4c5;)V

    invoke-direct {v2, v0, v3}, LX/4c1;-><init>(Ljava/io/InputStream;LX/4c4;)V

    iget-object v0, p0, LX/4c5;->a:LX/4c3;

    .line 794562
    iget-object v3, v0, LX/4c3;->c:LX/1iW;

    move-object v0, v3

    .line 794563
    iget-object v0, v0, LX/1iW;->bytesReadByApp:LX/1iX;

    invoke-direct {v1, v2, v0}, LX/4cF;-><init>(Ljava/io/InputStream;LX/1iX;)V

    iput-object v1, p0, LX/4c5;->b:LX/4cF;

    .line 794564
    :cond_0
    iget-object v0, p0, LX/4c5;->b:LX/4cF;

    return-object v0
.end method

.method public final isRepeatable()Z
    .locals 1

    .prologue
    .line 794557
    const/4 v0, 0x0

    return v0
.end method
