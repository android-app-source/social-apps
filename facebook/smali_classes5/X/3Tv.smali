.class public LX/3Tv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2km;
.implements LX/3Tw;


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/0o8;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;

.field private final c:LX/3Tx;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0o8;LX/3Tx;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0o8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 584897
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 584898
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/3Tv;->a:Ljava/lang/ref/WeakReference;

    .line 584899
    iput-object p1, p0, LX/3Tv;->b:Landroid/content/Context;

    .line 584900
    iput-object p3, p0, LX/3Tv;->c:LX/3Tx;

    .line 584901
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V
    .locals 3
    .param p3    # LX/Cfl;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 584902
    if-nez p3, :cond_1

    .line 584903
    :cond_0
    :goto_0
    return-void

    .line 584904
    :cond_1
    iget-object v0, p0, LX/3Tv;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0o8;

    .line 584905
    if-eqz v0, :cond_0

    .line 584906
    invoke-interface {v0}, LX/0o8;->m()LX/2ja;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 584907
    invoke-interface {v0}, LX/0o8;->m()LX/2ja;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 584908
    :cond_2
    iget-object v1, p0, LX/3Tv;->c:LX/3Tx;

    iget-object v2, p0, LX/3Tv;->b:Landroid/content/Context;

    invoke-virtual {v1, p1, p3, v0, v2}, LX/3Tx;->a(Ljava/lang/String;LX/Cfl;LX/0o8;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/Cfl;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 584909
    if-nez p4, :cond_1

    .line 584910
    :cond_0
    :goto_0
    return-void

    .line 584911
    :cond_1
    iget-object v0, p0, LX/3Tv;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0o8;

    .line 584912
    if-eqz v0, :cond_0

    .line 584913
    invoke-interface {v0}, LX/0o8;->m()LX/2ja;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 584914
    invoke-interface {v0}, LX/0o8;->m()LX/2ja;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3, p4}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 584915
    :cond_2
    iget-object v1, p0, LX/3Tv;->c:LX/3Tx;

    iget-object v2, p0, LX/3Tv;->b:Landroid/content/Context;

    invoke-virtual {v1, p1, p4, v0, v2}, LX/3Tx;->a(Ljava/lang/String;LX/Cfl;LX/0o8;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final u()LX/0o8;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 584916
    iget-object v0, p0, LX/3Tv;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0o8;

    return-object v0
.end method
