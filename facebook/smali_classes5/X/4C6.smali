.class public LX/4C6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4C5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/4C5;",
        ">",
        "Ljava/lang/Object;",
        "LX/4C5;"
    }
.end annotation


# instance fields
.field private a:LX/4C5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:I
    .annotation build Landroid/support/annotation/IntRange;
    .end annotation
.end field

.field private c:Landroid/graphics/Rect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/4C5;)V
    .locals 1
    .param p1    # LX/4C5;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 678403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 678404
    const/4 v0, -0x1

    iput v0, p0, LX/4C6;->b:I

    .line 678405
    iput-object p1, p0, LX/4C6;->a:LX/4C5;

    .line 678406
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 678400
    iget-object v0, p0, LX/4C6;->a:LX/4C5;

    if-nez v0, :cond_0

    .line 678401
    const/4 v0, -0x1

    .line 678402
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/4C6;->a:LX/4C5;

    invoke-interface {v0}, LX/4C5;->a()I

    move-result v0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/IntRange;
        .end annotation
    .end param

    .prologue
    .line 678397
    iget-object v0, p0, LX/4C6;->a:LX/4C5;

    if-eqz v0, :cond_0

    .line 678398
    iget-object v0, p0, LX/4C6;->a:LX/4C5;

    invoke-interface {v0, p1}, LX/4C5;->a(I)V

    .line 678399
    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 678394
    iget-object v0, p0, LX/4C6;->a:LX/4C5;

    if-eqz v0, :cond_0

    .line 678395
    iget-object v0, p0, LX/4C6;->a:LX/4C5;

    invoke-interface {v0, p1}, LX/4C5;->a(Landroid/graphics/ColorFilter;)V

    .line 678396
    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 1
    .param p1    # Landroid/graphics/Rect;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 678390
    iget-object v0, p0, LX/4C6;->a:LX/4C5;

    if-eqz v0, :cond_0

    .line 678391
    iget-object v0, p0, LX/4C6;->a:LX/4C5;

    invoke-interface {v0, p1}, LX/4C5;->a(Landroid/graphics/Rect;)V

    .line 678392
    :cond_0
    iput-object p1, p0, LX/4C6;->c:Landroid/graphics/Rect;

    .line 678393
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Canvas;I)Z
    .locals 1

    .prologue
    .line 678407
    iget-object v0, p0, LX/4C6;->a:LX/4C5;

    if-eqz v0, :cond_0

    .line 678408
    iget-object v0, p0, LX/4C6;->a:LX/4C5;

    invoke-interface {v0, p1, p2, p3}, LX/4C5;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Canvas;I)Z

    move-result v0

    .line 678409
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 678387
    iget-object v0, p0, LX/4C6;->a:LX/4C5;

    if-nez v0, :cond_0

    .line 678388
    const/4 v0, -0x1

    .line 678389
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/4C6;->a:LX/4C5;

    invoke-interface {v0}, LX/4C5;->b()I

    move-result v0

    goto :goto_0
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 678386
    iget-object v0, p0, LX/4C6;->a:LX/4C5;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/4C6;->a:LX/4C5;

    invoke-interface {v0, p1}, LX/4C4;->b(I)I

    move-result v0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 678383
    iget-object v0, p0, LX/4C6;->a:LX/4C5;

    if-eqz v0, :cond_0

    .line 678384
    iget-object v0, p0, LX/4C6;->a:LX/4C5;

    invoke-interface {v0}, LX/4C5;->c()V

    .line 678385
    :cond_0
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 678382
    iget-object v0, p0, LX/4C6;->a:LX/4C5;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/4C6;->a:LX/4C5;

    invoke-interface {v0}, LX/4C4;->d()I

    move-result v0

    goto :goto_0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 678381
    iget-object v0, p0, LX/4C6;->a:LX/4C5;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/4C6;->a:LX/4C5;

    invoke-interface {v0}, LX/4C4;->e()I

    move-result v0

    goto :goto_0
.end method
