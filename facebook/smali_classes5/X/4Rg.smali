.class public LX/4Rg;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 709638
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 709639
    const/4 v0, 0x0

    .line 709640
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_7

    .line 709641
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 709642
    :goto_0
    return v1

    .line 709643
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_4

    .line 709644
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 709645
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 709646
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_0

    if-eqz v7, :cond_0

    .line 709647
    const-string v8, "has_reply_permission"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 709648
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v6, v3

    move v3, v2

    goto :goto_1

    .line 709649
    :cond_1
    const-string v8, "page"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 709650
    invoke-static {p0, p1}, LX/2bc;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 709651
    :cond_2
    const-string v8, "status"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 709652
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    move-result-object v0

    move-object v4, v0

    move v0, v2

    goto :goto_1

    .line 709653
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 709654
    :cond_4
    const/4 v7, 0x3

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 709655
    if-eqz v3, :cond_5

    .line 709656
    invoke-virtual {p1, v1, v6}, LX/186;->a(IZ)V

    .line 709657
    :cond_5
    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 709658
    if-eqz v0, :cond_6

    .line 709659
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 709660
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v3, v1

    move-object v4, v0

    move v5, v1

    move v6, v1

    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 709661
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 709662
    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v0

    .line 709663
    if-eqz v0, :cond_0

    .line 709664
    const-string v1, "has_reply_permission"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709665
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 709666
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 709667
    if-eqz v0, :cond_1

    .line 709668
    const-string v1, "page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709669
    invoke-static {p0, v0, p2, p3}, LX/2bc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 709670
    :cond_1
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 709671
    if-eqz v0, :cond_2

    .line 709672
    const-string v0, "status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709673
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 709674
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 709675
    return-void
.end method
