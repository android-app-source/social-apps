.class public LX/3zs;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3zs;


# instance fields
.field private final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 662712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 662713
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/3zs;->a:Ljava/util/HashMap;

    .line 662714
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/3zs;->b:Ljava/util/WeakHashMap;

    .line 662715
    return-void
.end method

.method public static a(LX/0QB;)LX/3zs;
    .locals 3

    .prologue
    .line 662716
    sget-object v0, LX/3zs;->c:LX/3zs;

    if-nez v0, :cond_1

    .line 662717
    const-class v1, LX/3zs;

    monitor-enter v1

    .line 662718
    :try_start_0
    sget-object v0, LX/3zs;->c:LX/3zs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 662719
    if-eqz v2, :cond_0

    .line 662720
    :try_start_1
    new-instance v0, LX/3zs;

    invoke-direct {v0}, LX/3zs;-><init>()V

    .line 662721
    move-object v0, v0

    .line 662722
    sput-object v0, LX/3zs;->c:LX/3zs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 662723
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 662724
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 662725
    :cond_1
    sget-object v0, LX/3zs;->c:LX/3zs;

    return-object v0

    .line 662726
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 662727
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
