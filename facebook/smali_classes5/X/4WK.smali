.class public final LX/4WK;
.super LX/0ur;
.source ""


# instance fields
.field public b:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

.field public e:I

.field public f:I

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

.field public k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLRelevantReactorsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 751235
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 751236
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    iput-object v0, p0, LX/4WK;->d:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    .line 751237
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    iput-object v0, p0, LX/4WK;->h:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    .line 751238
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    iput-object v0, p0, LX/4WK;->j:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    .line 751239
    instance-of v0, p0, LX/4WK;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 751240
    return-void
.end method
