.class public final enum LX/52h;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/52h;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/52h;

.field public static final enum COERCED:LX/52h;

.field public static final enum EXACT:LX/52h;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 826223
    new-instance v0, LX/52h;

    const-string v1, "EXACT"

    invoke-direct {v0, v1, v2}, LX/52h;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/52h;->EXACT:LX/52h;

    new-instance v0, LX/52h;

    const-string v1, "COERCED"

    invoke-direct {v0, v1, v3}, LX/52h;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/52h;->COERCED:LX/52h;

    const/4 v0, 0x2

    new-array v0, v0, [LX/52h;

    sget-object v1, LX/52h;->EXACT:LX/52h;

    aput-object v1, v0, v2

    sget-object v1, LX/52h;->COERCED:LX/52h;

    aput-object v1, v0, v3

    sput-object v0, LX/52h;->$VALUES:[LX/52h;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 826224
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/52h;
    .locals 1

    .prologue
    .line 826225
    const-class v0, LX/52h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/52h;

    return-object v0
.end method

.method public static values()[LX/52h;
    .locals 1

    .prologue
    .line 826226
    sget-object v0, LX/52h;->$VALUES:[LX/52h;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/52h;

    return-object v0
.end method
