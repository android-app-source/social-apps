.class public final LX/3TY;
.super LX/1Cd;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V
    .locals 0

    .prologue
    .line 584328
    iput-object p1, p0, LX/3TY;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-direct {p0}, LX/1Cd;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;Ljava/lang/Object;II)V
    .locals 7
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 584329
    add-int v0, p3, p4

    invoke-interface {p1}, LX/0g8;->t()I

    move-result v1

    sub-int/2addr v0, v1

    .line 584330
    instance-of v1, p2, Lcom/facebook/friends/model/FriendRequest;

    if-eqz v1, :cond_3

    .line 584331
    iget-object v1, p0, LX/3TY;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v1, v1, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 584332
    invoke-virtual {v1, v0}, LX/3Tf;->d(I)[I

    move-result-object v4

    .line 584333
    iget-object v5, v1, LX/3Te;->h:Ljava/util/List;

    aget v6, v4, v2

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    iget-object v6, v1, LX/3Te;->f:LX/3UE;

    if-ne v5, v6, :cond_0

    move v2, v3

    :cond_0
    const-string v5, "The given position is not within the Friend Requests section"

    invoke-static {v2, v5}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 584334
    aget v2, v4, v3

    move v0, v2

    .line 584335
    iget-object v1, p0, LX/3TY;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v2, p0, LX/3TY;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget v2, v2, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aG:I

    add-int/lit8 v0, v0, 0x1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 584336
    iput v0, v1, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aG:I

    .line 584337
    check-cast p2, Lcom/facebook/friends/model/FriendRequest;

    .line 584338
    iget-boolean v0, p2, Lcom/facebook/friends/model/FriendRequest;->h:Z

    move v0, v0

    .line 584339
    if-nez v0, :cond_1

    .line 584340
    iget-object v0, p0, LX/3TY;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-static {v0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->H(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    .line 584341
    iget-object v0, p0, LX/3TY;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-static {v0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->x(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    .line 584342
    :cond_1
    iget-object v0, p0, LX/3TY;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v0, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->u:LX/2hW;

    invoke-virtual {v0}, LX/2hW;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 584343
    iget-boolean v0, p2, Lcom/facebook/friends/model/FriendRequest;->k:Z

    move v0, v0

    .line 584344
    if-nez v0, :cond_2

    .line 584345
    iget-object v0, p0, LX/3TY;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v0, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->u:LX/2hW;

    invoke-virtual {p2}, Lcom/facebook/friends/model/FriendRequest;->a()J

    move-result-wide v2

    sget-object v1, LX/2hA;->MOBILE_JEWEL:LX/2hA;

    iget-object v4, p0, LX/3TY;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v4, v4, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aF:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v1, v4}, LX/2hW;->a(JLX/2hA;Ljava/lang/String;)V

    .line 584346
    invoke-virtual {p2}, Lcom/facebook/friends/model/FriendRequest;->m()V

    .line 584347
    :cond_2
    :goto_0
    return-void

    .line 584348
    :cond_3
    instance-of v1, p2, Lcom/facebook/friends/model/PersonYouMayKnow;

    if-eqz v1, :cond_2

    .line 584349
    iget-object v1, p0, LX/3TY;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v1, v1, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 584350
    invoke-virtual {v1, v0}, LX/3Tf;->d(I)[I

    move-result-object v4

    .line 584351
    iget-object v5, v1, LX/3Te;->h:Ljava/util/List;

    aget v6, v4, v2

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    iget-object v6, v1, LX/3Te;->g:LX/3UK;

    if-ne v5, v6, :cond_4

    move v2, v3

    :cond_4
    const-string v5, "The given position is not within the People You May Know section"

    invoke-static {v2, v5}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 584352
    aget v2, v4, v3

    move v0, v2

    .line 584353
    iget-object v1, p0, LX/3TY;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v2, p0, LX/3TY;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget v2, v2, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aH:I

    add-int/lit8 v0, v0, 0x1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 584354
    iput v0, v1, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aH:I

    .line 584355
    check-cast p2, Lcom/facebook/friends/model/PersonYouMayKnow;

    .line 584356
    iget-boolean v0, p2, Lcom/facebook/friends/model/PersonYouMayKnow;->i:Z

    move v0, v0

    .line 584357
    if-nez v0, :cond_2

    .line 584358
    iget-object v0, p0, LX/3TY;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v1, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->Q:LX/2hd;

    iget-object v0, p0, LX/3TY;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v2, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aF:Ljava/lang/String;

    .line 584359
    iget-object v0, p2, Lcom/facebook/friends/model/PersonYouMayKnow;->j:Ljava/lang/String;

    move-object v3, v0

    .line 584360
    invoke-virtual {p2}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v4

    sget-object v6, LX/2hC;->JEWEL:LX/2hC;

    invoke-virtual/range {v1 .. v6}, LX/2hd;->a(Ljava/lang/String;Ljava/lang/String;JLX/2hC;)V

    .line 584361
    invoke-virtual {p2}, Lcom/facebook/friends/model/PersonYouMayKnow;->k()V

    goto :goto_0
.end method
