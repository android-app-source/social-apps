.class public LX/4CN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/4C4;

.field private b:J


# direct methods
.method public constructor <init>(LX/4C4;)V
    .locals 2

    .prologue
    .line 678739
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 678740
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/4CN;->b:J

    .line 678741
    iput-object p1, p0, LX/4CN;->a:LX/4C4;

    .line 678742
    return-void
.end method

.method private static b(LX/4CN;)Z
    .locals 1

    .prologue
    .line 678738
    iget-object v0, p0, LX/4CN;->a:LX/4C4;

    invoke-interface {v0}, LX/4C4;->e()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(J)I
    .locals 12

    .prologue
    .line 678756
    invoke-static {p0}, LX/4CN;->b(LX/4CN;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 678757
    invoke-virtual {p0}, LX/4CN;->a()J

    move-result-wide v0

    div-long v0, p1, v0

    .line 678758
    iget-object v2, p0, LX/4CN;->a:LX/4C4;

    invoke-interface {v2}, LX/4C4;->e()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 678759
    const/4 v0, -0x1

    .line 678760
    :goto_0
    return v0

    .line 678761
    :cond_0
    invoke-virtual {p0}, LX/4CN;->a()J

    move-result-wide v0

    rem-long v0, p1, v0

    .line 678762
    const/4 v7, 0x0

    .line 678763
    const-wide/16 v5, 0x0

    .line 678764
    :cond_1
    iget-object v8, p0, LX/4CN;->a:LX/4C4;

    invoke-interface {v8, v7}, LX/4C4;->b(I)I

    move-result v8

    int-to-long v9, v8

    add-long/2addr v5, v9

    .line 678765
    add-int/lit8 v7, v7, 0x1

    .line 678766
    cmp-long v8, v0, v5

    if-gez v8, :cond_1

    .line 678767
    add-int/lit8 v5, v7, -0x1

    move v0, v5

    .line 678768
    goto :goto_0
.end method

.method public final a()J
    .locals 6

    .prologue
    .line 678769
    iget-wide v0, p0, LX/4CN;->b:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 678770
    iget-wide v0, p0, LX/4CN;->b:J

    .line 678771
    :goto_0
    return-wide v0

    .line 678772
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/4CN;->b:J

    .line 678773
    iget-object v0, p0, LX/4CN;->a:LX/4C4;

    invoke-interface {v0}, LX/4C4;->d()I

    move-result v1

    .line 678774
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_1

    .line 678775
    iget-wide v2, p0, LX/4CN;->b:J

    iget-object v4, p0, LX/4CN;->a:LX/4C4;

    invoke-interface {v4, v0}, LX/4C4;->b(I)I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/4CN;->b:J

    .line 678776
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 678777
    :cond_1
    iget-wide v0, p0, LX/4CN;->b:J

    goto :goto_0
.end method

.method public final b(J)J
    .locals 11

    .prologue
    const-wide/16 v2, 0x0

    const-wide/16 v0, -0x1

    .line 678743
    invoke-virtual {p0}, LX/4CN;->a()J

    move-result-wide v4

    .line 678744
    cmp-long v6, v4, v2

    if-nez v6, :cond_1

    .line 678745
    :cond_0
    :goto_0
    return-wide v0

    .line 678746
    :cond_1
    invoke-static {p0}, LX/4CN;->b(LX/4CN;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 678747
    invoke-virtual {p0}, LX/4CN;->a()J

    move-result-wide v6

    div-long v6, p1, v6

    .line 678748
    iget-object v8, p0, LX/4CN;->a:LX/4C4;

    invoke-interface {v8}, LX/4C4;->e()I

    move-result v8

    int-to-long v8, v8

    cmp-long v6, v6, v8

    if-gez v6, :cond_0

    .line 678749
    :cond_2
    rem-long v4, p1, v4

    .line 678750
    iget-object v0, p0, LX/4CN;->a:LX/4C4;

    invoke-interface {v0}, LX/4C4;->d()I

    move-result v1

    .line 678751
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_3

    cmp-long v6, v2, v4

    if-gtz v6, :cond_3

    .line 678752
    iget-object v6, p0, LX/4CN;->a:LX/4C4;

    invoke-interface {v6, v0}, LX/4C4;->b(I)I

    move-result v6

    int-to-long v6, v6

    add-long/2addr v2, v6

    .line 678753
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 678754
    :cond_3
    sub-long v0, v2, v4

    .line 678755
    add-long/2addr v0, p1

    goto :goto_0
.end method
