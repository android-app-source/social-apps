.class public LX/4Tp;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 719652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 33

    .prologue
    .line 719653
    const/16 v27, 0x0

    .line 719654
    const/16 v26, 0x0

    .line 719655
    const-wide/16 v24, 0x0

    .line 719656
    const/16 v23, 0x0

    .line 719657
    const/16 v22, 0x0

    .line 719658
    const/16 v21, 0x0

    .line 719659
    const/16 v20, 0x0

    .line 719660
    const/16 v19, 0x0

    .line 719661
    const/16 v18, 0x0

    .line 719662
    const/16 v17, 0x0

    .line 719663
    const/16 v16, 0x0

    .line 719664
    const/4 v15, 0x0

    .line 719665
    const/4 v14, 0x0

    .line 719666
    const/4 v13, 0x0

    .line 719667
    const/4 v12, 0x0

    .line 719668
    const/4 v11, 0x0

    .line 719669
    const/4 v10, 0x0

    .line 719670
    const/4 v9, 0x0

    .line 719671
    const/4 v8, 0x0

    .line 719672
    const/4 v7, 0x0

    .line 719673
    const/4 v6, 0x0

    .line 719674
    const/4 v5, 0x0

    .line 719675
    const/4 v4, 0x0

    .line 719676
    const/4 v3, 0x0

    .line 719677
    const/4 v2, 0x0

    .line 719678
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v28

    sget-object v29, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    if-eq v0, v1, :cond_1b

    .line 719679
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 719680
    const/4 v2, 0x0

    .line 719681
    :goto_0
    return v2

    .line 719682
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v7, :cond_17

    .line 719683
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 719684
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 719685
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v30, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v30

    if-eq v7, v0, :cond_0

    if-eqz v2, :cond_0

    .line 719686
    const-string v7, "attribution_text"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 719687
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v29, v2

    goto :goto_1

    .line 719688
    :cond_1
    const-string v7, "collection_item_type"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 719689
    const/4 v2, 0x1

    .line 719690
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;

    move-result-object v6

    move-object/from16 v28, v6

    move v6, v2

    goto :goto_1

    .line 719691
    :cond_2
    const-string v7, "creation_time"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 719692
    const/4 v2, 0x1

    .line 719693
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 719694
    :cond_3
    const-string v7, "feedback"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 719695
    invoke-static/range {p0 .. p1}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v27, v2

    goto :goto_1

    .line 719696
    :cond_4
    const-string v7, "global_share"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 719697
    invoke-static/range {p0 .. p1}, LX/4MV;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v26, v2

    goto :goto_1

    .line 719698
    :cond_5
    const-string v7, "icon_image"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 719699
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v25, v2

    goto :goto_1

    .line 719700
    :cond_6
    const-string v7, "id"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 719701
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 719702
    :cond_7
    const-string v7, "image"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 719703
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 719704
    :cond_8
    const-string v7, "listImage"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 719705
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 719706
    :cond_9
    const-string v7, "locally_updated_containing_collection_id"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 719707
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 719708
    :cond_a
    const-string v7, "node"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 719709
    invoke-static/range {p0 .. p1}, LX/2bR;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 719710
    :cond_b
    const-string v7, "permalink_node"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 719711
    invoke-static/range {p0 .. p1}, LX/2bR;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 719712
    :cond_c
    const-string v7, "profileImageLarge"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 719713
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 719714
    :cond_d
    const-string v7, "profileImageSmall"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 719715
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 719716
    :cond_e
    const-string v7, "rating"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 719717
    invoke-static/range {p0 .. p1}, LX/2sB;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 719718
    :cond_f
    const-string v7, "source_object"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 719719
    invoke-static/range {p0 .. p1}, LX/2ao;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 719720
    :cond_10
    const-string v7, "subtitle_text"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_11

    .line 719721
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 719722
    :cond_11
    const-string v7, "tableImage"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_12

    .line 719723
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 719724
    :cond_12
    const-string v7, "title"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_13

    .line 719725
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 719726
    :cond_13
    const-string v7, "titleForSummary"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_14

    .line 719727
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 719728
    :cond_14
    const-string v7, "url"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_15

    .line 719729
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 719730
    :cond_15
    const-string v7, "viewed_state"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 719731
    const/4 v2, 0x1

    .line 719732
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    move-result-object v7

    move v8, v2

    move-object v9, v7

    goto/16 :goto_1

    .line 719733
    :cond_16
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 719734
    :cond_17
    const/16 v2, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 719735
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 719736
    if-eqz v6, :cond_18

    .line 719737
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 719738
    :cond_18
    if-eqz v3, :cond_19

    .line 719739
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 719740
    :cond_19
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 719741
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 719742
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 719743
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 719744
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 719745
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 719746
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 719747
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 719748
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 719749
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 719750
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 719751
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 719752
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 719753
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 719754
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 719755
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 719756
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 719757
    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 719758
    if-eqz v8, :cond_1a

    .line 719759
    const/16 v2, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->a(ILjava/lang/Enum;)V

    .line 719760
    :cond_1a
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_1b
    move-object/from16 v28, v26

    move/from16 v29, v27

    move/from16 v26, v22

    move/from16 v27, v23

    move/from16 v22, v18

    move/from16 v23, v19

    move/from16 v18, v14

    move/from16 v19, v15

    move v15, v11

    move v14, v10

    move v11, v7

    move v10, v6

    move v6, v4

    move/from16 v31, v16

    move/from16 v16, v12

    move v12, v8

    move v8, v2

    move/from16 v32, v17

    move/from16 v17, v13

    move v13, v9

    move-object v9, v5

    move-wide/from16 v4, v24

    move/from16 v24, v20

    move/from16 v25, v21

    move/from16 v21, v32

    move/from16 v20, v31

    goto/16 :goto_1
.end method
