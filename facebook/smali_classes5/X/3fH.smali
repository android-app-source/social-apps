.class public final LX/3fH;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 621736
    const/4 v9, 0x0

    .line 621737
    const/4 v8, 0x0

    .line 621738
    const/4 v7, 0x0

    .line 621739
    const/4 v6, 0x0

    .line 621740
    const/4 v5, 0x0

    .line 621741
    const/4 v4, 0x0

    .line 621742
    const/4 v3, 0x0

    .line 621743
    const/4 v2, 0x0

    .line 621744
    const/4 v1, 0x0

    .line 621745
    const/4 v0, 0x0

    .line 621746
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 621747
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 621748
    const/4 v0, 0x0

    .line 621749
    :goto_0
    return v0

    .line 621750
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 621751
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_9

    .line 621752
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 621753
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 621754
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 621755
    const-string v11, "animation"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 621756
    invoke-static {p0, p1}, LX/3fJ;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 621757
    :cond_2
    const-string v11, "color"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 621758
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 621759
    :cond_3
    const-string v11, "is_deprecated"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 621760
    const/4 v1, 0x1

    .line 621761
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v7

    goto :goto_1

    .line 621762
    :cond_4
    const-string v11, "key"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 621763
    const/4 v0, 0x1

    .line 621764
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v6

    goto :goto_1

    .line 621765
    :cond_5
    const-string v11, "largeFaceImage"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 621766
    invoke-static {p0, p1}, LX/3fI;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 621767
    :cond_6
    const-string v11, "localized_name"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 621768
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 621769
    :cond_7
    const-string v11, "smallFaceImage"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 621770
    invoke-static {p0, p1}, LX/3fI;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 621771
    :cond_8
    const-string v11, "tabIconImage"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 621772
    invoke-static {p0, p1}, LX/3fI;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 621773
    :cond_9
    const/16 v10, 0x8

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 621774
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 621775
    const/4 v9, 0x1

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 621776
    if-eqz v1, :cond_a

    .line 621777
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v7}, LX/186;->a(IZ)V

    .line 621778
    :cond_a
    if-eqz v0, :cond_b

    .line 621779
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v6, v1}, LX/186;->a(III)V

    .line 621780
    :cond_b
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 621781
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 621782
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 621783
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 621784
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 621785
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 621786
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 621787
    if-eqz v0, :cond_2

    .line 621788
    const-string v1, "animation"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 621789
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 621790
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 621791
    if-eqz v1, :cond_0

    .line 621792
    const-string p3, "data"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 621793
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 621794
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 621795
    if-eqz v1, :cond_1

    .line 621796
    const-string p3, "name"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 621797
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 621798
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 621799
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 621800
    if-eqz v0, :cond_3

    .line 621801
    const-string v1, "color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 621802
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 621803
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 621804
    if-eqz v0, :cond_4

    .line 621805
    const-string v1, "is_deprecated"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 621806
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 621807
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 621808
    if-eqz v0, :cond_5

    .line 621809
    const-string v1, "key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 621810
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 621811
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 621812
    if-eqz v0, :cond_6

    .line 621813
    const-string v1, "largeFaceImage"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 621814
    invoke-static {p0, v0, p2}, LX/3fI;->a(LX/15i;ILX/0nX;)V

    .line 621815
    :cond_6
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 621816
    if-eqz v0, :cond_7

    .line 621817
    const-string v1, "localized_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 621818
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 621819
    :cond_7
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 621820
    if-eqz v0, :cond_8

    .line 621821
    const-string v1, "smallFaceImage"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 621822
    invoke-static {p0, v0, p2}, LX/3fI;->a(LX/15i;ILX/0nX;)V

    .line 621823
    :cond_8
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 621824
    if-eqz v0, :cond_9

    .line 621825
    const-string v1, "tabIconImage"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 621826
    invoke-static {p0, v0, p2}, LX/3fI;->a(LX/15i;ILX/0nX;)V

    .line 621827
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 621828
    return-void
.end method
