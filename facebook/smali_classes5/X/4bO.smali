.class public LX/4bO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1hk;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile g:LX/4bO;


# instance fields
.field public final b:LX/1hl;

.field private final c:LX/0UR;

.field private final d:LX/0ad;

.field private volatile e:LX/0TU;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/15D",
            "<*>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 793689
    const-class v0, LX/4bO;

    sput-object v0, LX/4bO;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1hl;LX/0UR;LX/0ad;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 793683
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 793684
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/4bO;->f:Ljava/util/Set;

    .line 793685
    iput-object p1, p0, LX/4bO;->b:LX/1hl;

    .line 793686
    iput-object p2, p0, LX/4bO;->c:LX/0UR;

    .line 793687
    iput-object p3, p0, LX/4bO;->d:LX/0ad;

    .line 793688
    return-void
.end method

.method public static a(LX/0QB;)LX/4bO;
    .locals 6

    .prologue
    .line 793670
    sget-object v0, LX/4bO;->g:LX/4bO;

    if-nez v0, :cond_1

    .line 793671
    const-class v1, LX/4bO;

    monitor-enter v1

    .line 793672
    :try_start_0
    sget-object v0, LX/4bO;->g:LX/4bO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 793673
    if-eqz v2, :cond_0

    .line 793674
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 793675
    new-instance p0, LX/4bO;

    invoke-static {v0}, LX/1hl;->a(LX/0QB;)LX/1hl;

    move-result-object v3

    check-cast v3, LX/1hl;

    invoke-static {v0}, LX/0UR;->a(LX/0QB;)LX/0UR;

    move-result-object v4

    check-cast v4, LX/0UR;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, LX/4bO;-><init>(LX/1hl;LX/0UR;LX/0ad;)V

    .line 793676
    move-object v0, p0

    .line 793677
    sput-object v0, LX/4bO;->g:LX/4bO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 793678
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 793679
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 793680
    :cond_1
    sget-object v0, LX/4bO;->g:LX/4bO;

    return-object v0

    .line 793681
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 793682
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private f()LX/0TU;
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InvalidAccessToGuardedField"
        }
    .end annotation

    .prologue
    .line 793662
    iget-object v0, p0, LX/4bO;->e:LX/0TU;

    if-eqz v0, :cond_0

    .line 793663
    iget-object v0, p0, LX/4bO;->e:LX/0TU;

    .line 793664
    :goto_0
    return-object v0

    .line 793665
    :cond_0
    monitor-enter p0

    .line 793666
    :try_start_0
    iget-object v0, p0, LX/4bO;->e:LX/0TU;

    if-eqz v0, :cond_1

    .line 793667
    iget-object v0, p0, LX/4bO;->e:LX/0TU;

    monitor-exit p0

    goto :goto_0

    .line 793668
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 793669
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/4bO;->c:LX/0UR;

    sget-object v1, LX/4bO;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xa

    const v3, 0x7fffffff

    const/16 v4, 0xa

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0UR;->a(Ljava/lang/String;III)LX/0TU;

    move-result-object v0

    iput-object v0, p0, LX/4bO;->e:LX/0TU;

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/15D;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/15D",
            "<TT;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 793661
    invoke-direct {p0}, LX/4bO;->f()LX/0TU;

    move-result-object v0

    new-instance v1, LX/4bN;

    invoke-direct {v1, p0, p1}, LX/4bN;-><init>(LX/4bO;LX/15D;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 793652
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 793653
    monitor-enter p0

    .line 793654
    :try_start_0
    iget-object v1, p0, LX/4bO;->f:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 793655
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 793656
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15D;

    .line 793657
    iget-object p0, v0, LX/15D;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    move-object v0, p0

    .line 793658
    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    goto :goto_0

    .line 793659
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 793660
    :cond_0
    return-void
.end method

.method public final a(LX/15D;Lcom/facebook/http/interfaces/RequestPriority;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15D",
            "<*>;",
            "Lcom/facebook/http/interfaces/RequestPriority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 793690
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 793651
    return-void
.end method

.method public final declared-synchronized b()LX/2Ee;
    .locals 3

    .prologue
    .line 793650
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/2Ee;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, LX/4bO;->f:Ljava/util/Set;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v1, v2}, LX/2Ee;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(LX/15D;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15D",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 793649
    const/4 v0, 0x0

    return v0
.end method

.method public final declared-synchronized c()Ljava/lang/String;
    .locals 5

    .prologue
    .line 793648
    monitor-enter p0

    :try_start_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "PassthroughRequestEngine Inflight requests: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/4bO;->f:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 793647
    sget-object v0, LX/4bO;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 793646
    iget-object v0, p0, LX/4bO;->b:LX/1hl;

    invoke-virtual {v0}, LX/1hl;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
