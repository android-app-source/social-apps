.class public LX/4oj;
.super LX/4oi;
.source ""


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 810349
    invoke-direct {p0, p1}, LX/4oi;-><init>(Landroid/content/Context;)V

    .line 810350
    const v0, 0x7f03145a

    invoke-virtual {p0, v0}, LX/4oj;->setWidgetLayoutResource(I)V

    .line 810351
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 810352
    invoke-virtual {p0}, LX/4oj;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f0103ac

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, v0, Landroid/util/TypedValue;->type:I

    const/16 v2, 0x12

    if-ne v1, v2, :cond_0

    .line 810353
    invoke-virtual {v0}, Landroid/util/TypedValue;->coerceToString()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/4oj;->a:Z

    .line 810354
    :goto_0
    return-void

    .line 810355
    :cond_0
    iput-boolean v3, p0, LX/4oj;->a:Z

    goto :goto_0
.end method

.method public static synthetic a(LX/4oj;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 810348
    invoke-virtual {p0, p1}, LX/4oj;->callChangeListener(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final onBindView(Landroid/view/View;)V
    .locals 8

    .prologue
    .line 810324
    invoke-super {p0, p1}, LX/4oi;->onBindView(Landroid/view/View;)V

    .line 810325
    const v0, 0x1020001

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 810326
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 810327
    const/4 v1, 0x0

    .line 810328
    iget-boolean v2, p0, LX/4oj;->a:Z

    if-eqz v2, :cond_0

    .line 810329
    const v1, 0x1020010

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 810330
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 810331
    :cond_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 810332
    invoke-virtual {p0}, LX/4oj;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 810333
    const v2, 0x7f0805eb

    .line 810334
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, LX/4oj;->getTitle()Ljava/lang/CharSequence;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object v1, v4, v7

    invoke-virtual {v3, v2, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 810335
    :goto_1
    move-object v1, v1

    .line 810336
    invoke-virtual {p1, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 810337
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/facebook/widget/SwitchCompat;

    if-eqz v1, :cond_1

    .line 810338
    check-cast v0, Lcom/facebook/widget/SwitchCompat;

    .line 810339
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setClickable(Z)V

    .line 810340
    invoke-virtual {p0}, LX/4oj;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 810341
    new-instance v1, LX/4ov;

    invoke-direct {v1, p0}, LX/4ov;-><init>(LX/4oj;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 810342
    :cond_1
    return-void

    .line 810343
    :cond_2
    const v2, 0x7f0805ea

    goto :goto_0

    .line 810344
    :cond_3
    invoke-virtual {p0}, LX/4oj;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 810345
    const v1, 0x7f0805e9

    .line 810346
    :goto_2
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {p0}, LX/4oj;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 810347
    :cond_4
    const v1, 0x7f0805e8

    goto :goto_2
.end method

.method public final setSummary(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 810321
    iget-boolean v0, p0, LX/4oj;->a:Z

    if-eqz v0, :cond_0

    .line 810322
    invoke-super {p0, p1}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 810323
    :cond_0
    return-void
.end method

.method public final setSummaryOff(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 810318
    iget-boolean v0, p0, LX/4oj;->a:Z

    if-eqz v0, :cond_0

    .line 810319
    invoke-super {p0, p1}, LX/4oi;->setSummaryOff(Ljava/lang/CharSequence;)V

    .line 810320
    :cond_0
    return-void
.end method

.method public final setSummaryOn(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 810315
    iget-boolean v0, p0, LX/4oj;->a:Z

    if-eqz v0, :cond_0

    .line 810316
    invoke-super {p0, p1}, LX/4oi;->setSummaryOn(Ljava/lang/CharSequence;)V

    .line 810317
    :cond_0
    return-void
.end method
