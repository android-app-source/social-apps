.class public final LX/3TZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0rl",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:LX/0v6;


# direct methods
.method public constructor <init>(LX/0v6;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 584362
    iput-object p1, p0, LX/3TZ;->b:LX/0v6;

    iput-object p2, p0, LX/3TZ;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 584363
    iget-object v0, p0, LX/3TZ;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0SQ;->cancel(Z)Z

    .line 584364
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 584365
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 584366
    iget-object v0, p0, LX/3TZ;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0}, LX/0SQ;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 584367
    iget-object v0, p0, LX/3TZ;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, 0x3a84ef5f

    invoke-static {v0, p1, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 584368
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 584369
    iget-object v0, p0, LX/3TZ;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0}, LX/0SQ;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 584370
    iget-object v0, p0, LX/3TZ;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 584371
    :cond_0
    return-void
.end method
