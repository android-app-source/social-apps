.class public final LX/4wP;
.super LX/4wN;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/4wN",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public volatile a:J

.field public b:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public c:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public volatile d:J

.field public e:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public f:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;ILX/0R1;)V
    .locals 4
    .param p3    # LX/0R1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    const-wide v2, 0x7fffffffffffffffL

    .line 820120
    invoke-direct {p0, p1, p2, p3}, LX/4wN;-><init>(Ljava/lang/Object;ILX/0R1;)V

    .line 820121
    iput-wide v2, p0, LX/4wP;->a:J

    .line 820122
    sget-object v0, LX/0SZ;->INSTANCE:LX/0SZ;

    move-object v0, v0

    .line 820123
    iput-object v0, p0, LX/4wP;->b:LX/0R1;

    .line 820124
    sget-object v0, LX/0SZ;->INSTANCE:LX/0SZ;

    move-object v0, v0

    .line 820125
    iput-object v0, p0, LX/4wP;->c:LX/0R1;

    .line 820126
    iput-wide v2, p0, LX/4wP;->d:J

    .line 820127
    sget-object v0, LX/0SZ;->INSTANCE:LX/0SZ;

    move-object v0, v0

    .line 820128
    iput-object v0, p0, LX/4wP;->e:LX/0R1;

    .line 820129
    sget-object v0, LX/0SZ;->INSTANCE:LX/0SZ;

    move-object v0, v0

    .line 820130
    iput-object v0, p0, LX/4wP;->f:LX/0R1;

    .line 820131
    return-void
.end method


# virtual methods
.method public final getAccessTime()J
    .locals 2

    .prologue
    .line 820119
    iget-wide v0, p0, LX/4wP;->a:J

    return-wide v0
.end method

.method public final getNextInAccessQueue()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 820118
    iget-object v0, p0, LX/4wP;->b:LX/0R1;

    return-object v0
.end method

.method public final getNextInWriteQueue()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 820117
    iget-object v0, p0, LX/4wP;->e:LX/0R1;

    return-object v0
.end method

.method public final getPreviousInAccessQueue()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 820116
    iget-object v0, p0, LX/4wP;->c:LX/0R1;

    return-object v0
.end method

.method public final getPreviousInWriteQueue()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 820115
    iget-object v0, p0, LX/4wP;->f:LX/0R1;

    return-object v0
.end method

.method public final getWriteTime()J
    .locals 2

    .prologue
    .line 820114
    iget-wide v0, p0, LX/4wP;->d:J

    return-wide v0
.end method

.method public final setAccessTime(J)V
    .locals 1

    .prologue
    .line 820112
    iput-wide p1, p0, LX/4wP;->a:J

    .line 820113
    return-void
.end method

.method public final setNextInAccessQueue(LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 820102
    iput-object p1, p0, LX/4wP;->b:LX/0R1;

    .line 820103
    return-void
.end method

.method public final setNextInWriteQueue(LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 820104
    iput-object p1, p0, LX/4wP;->e:LX/0R1;

    .line 820105
    return-void
.end method

.method public final setPreviousInAccessQueue(LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 820106
    iput-object p1, p0, LX/4wP;->c:LX/0R1;

    .line 820107
    return-void
.end method

.method public final setPreviousInWriteQueue(LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 820108
    iput-object p1, p0, LX/4wP;->f:LX/0R1;

    .line 820109
    return-void
.end method

.method public final setWriteTime(J)V
    .locals 1

    .prologue
    .line 820110
    iput-wide p1, p0, LX/4wP;->d:J

    .line 820111
    return-void
.end method
