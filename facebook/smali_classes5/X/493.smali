.class public LX/493;
.super LX/0ux;
.source ""


# instance fields
.field private a:Ljava/lang/String;

.field private b:LX/490;

.field private c:LX/0ux;


# direct methods
.method public constructor <init>(LX/492;)V
    .locals 1

    .prologue
    .line 674127
    invoke-direct {p0}, LX/0ux;-><init>()V

    .line 674128
    iget-object v0, p1, LX/492;->a:Ljava/lang/String;

    iput-object v0, p0, LX/493;->a:Ljava/lang/String;

    .line 674129
    iget-object v0, p1, LX/492;->b:LX/490;

    iput-object v0, p0, LX/493;->b:LX/490;

    .line 674130
    iget-object v0, p1, LX/492;->c:LX/0ux;

    iput-object v0, p0, LX/493;->c:LX/0ux;

    .line 674131
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 674132
    const-string v0, "UPDATE %1$s SET %2$s WHERE %3$s"

    iget-object v1, p0, LX/493;->a:Ljava/lang/String;

    iget-object v2, p0, LX/493;->b:LX/490;

    invoke-virtual {v2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/493;->c:LX/0ux;

    invoke-virtual {v3}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 674133
    invoke-virtual {p0}, LX/493;->c()Ljava/lang/Iterable;

    move-result-object v0

    .line 674134
    const-class v1, Ljava/lang/String;

    invoke-static {v0, v1}, LX/0Ph;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 674135
    iget-object v0, p0, LX/493;->b:LX/490;

    invoke-virtual {v0}, LX/490;->c()Ljava/lang/Iterable;

    move-result-object v0

    iget-object v1, p0, LX/493;->c:LX/0ux;

    invoke-virtual {v1}, LX/0ux;->c()Ljava/lang/Iterable;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method
