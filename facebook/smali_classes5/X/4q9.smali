.class public final LX/4q9;
.super LX/32s;
.source ""


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final _creator:Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/reflect/Constructor",
            "<*>;"
        }
    .end annotation
.end field

.field public final _delegate:LX/32s;


# direct methods
.method public constructor <init>(LX/32s;Ljava/lang/reflect/Constructor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/32s;",
            "Ljava/lang/reflect/Constructor",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 813060
    invoke-direct {p0, p1}, LX/32s;-><init>(LX/32s;)V

    .line 813061
    iput-object p1, p0, LX/4q9;->_delegate:LX/32s;

    .line 813062
    iput-object p2, p0, LX/4q9;->_creator:Ljava/lang/reflect/Constructor;

    .line 813063
    return-void
.end method

.method private constructor <init>(LX/4q9;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4q9;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 813068
    invoke-direct {p0, p1, p2}, LX/32s;-><init>(LX/32s;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 813069
    iget-object v0, p1, LX/4q9;->_delegate:LX/32s;

    invoke-virtual {v0, p2}, LX/32s;->b(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/32s;

    move-result-object v0

    iput-object v0, p0, LX/4q9;->_delegate:LX/32s;

    .line 813070
    iget-object v0, p1, LX/4q9;->_creator:Ljava/lang/reflect/Constructor;

    iput-object v0, p0, LX/4q9;->_creator:Ljava/lang/reflect/Constructor;

    .line 813071
    return-void
.end method

.method private constructor <init>(LX/4q9;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 813064
    invoke-direct {p0, p1, p2}, LX/32s;-><init>(LX/32s;Ljava/lang/String;)V

    .line 813065
    iget-object v0, p1, LX/4q9;->_delegate:LX/32s;

    invoke-virtual {v0, p2}, LX/32s;->a(Ljava/lang/String;)LX/32s;

    move-result-object v0

    iput-object v0, p0, LX/4q9;->_delegate:LX/32s;

    .line 813066
    iget-object v0, p1, LX/4q9;->_creator:Ljava/lang/reflect/Constructor;

    iput-object v0, p0, LX/4q9;->_creator:Ljava/lang/reflect/Constructor;

    .line 813067
    return-void
.end method

.method private a(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/4q9;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)",
            "LX/4q9;"
        }
    .end annotation

    .prologue
    .line 813051
    new-instance v0, LX/4q9;

    invoke-direct {v0, p0, p1}, LX/4q9;-><init>(LX/4q9;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    return-object v0
.end method

.method private c(Ljava/lang/String;)LX/4q9;
    .locals 1

    .prologue
    .line 813059
    new-instance v0, LX/4q9;

    invoke-direct {v0, p0, p1}, LX/4q9;-><init>(LX/4q9;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;)LX/32s;
    .locals 1

    .prologue
    .line 813058
    invoke-direct {p0, p1}, LX/4q9;->c(Ljava/lang/String;)LX/4q9;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15w;LX/0n3;Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 813072
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    .line 813073
    sget-object v2, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v1, v2, :cond_1

    .line 813074
    iget-object v1, p0, LX/32s;->_nullProvider:LX/4qC;

    if-nez v1, :cond_0

    .line 813075
    :goto_0
    invoke-virtual {p0, p3, v0}, LX/32s;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 813076
    return-void

    .line 813077
    :cond_0
    iget-object v0, p0, LX/32s;->_nullProvider:LX/4qC;

    invoke-virtual {v0, p2}, LX/4qC;->a(LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 813078
    :cond_1
    iget-object v1, p0, LX/32s;->_valueTypeDeserializer:LX/4qw;

    if-eqz v1, :cond_2

    .line 813079
    iget-object v0, p0, LX/32s;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    iget-object v1, p0, LX/32s;->_valueTypeDeserializer:LX/4qw;

    invoke-virtual {v0, p1, p2, v1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserializeWithType(LX/15w;LX/0n3;LX/4qw;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 813080
    :cond_2
    :try_start_0
    iget-object v1, p0, LX/4q9;->_creator:Ljava/lang/reflect/Constructor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p3, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 813081
    :goto_1
    iget-object v1, p0, LX/32s;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v1, p1, p2, v0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 813082
    :catch_0
    move-exception v1

    .line 813083
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to instantiate class "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/4q9;->_creator:Ljava/lang/reflect/Constructor;

    invoke-virtual {v3}, Ljava/lang/reflect/Constructor;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", problem: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/1Xw;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 813056
    iget-object v0, p0, LX/4q9;->_delegate:LX/32s;

    invoke-virtual {v0, p1, p2}, LX/32s;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 813057
    return-void
.end method

.method public final b()LX/2An;
    .locals 1

    .prologue
    .line 813055
    iget-object v0, p0, LX/4q9;->_delegate:LX/32s;

    invoke-virtual {v0}, LX/32s;->b()LX/2An;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/32s;
    .locals 1

    .prologue
    .line 813054
    invoke-direct {p0, p1}, LX/4q9;->a(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/4q9;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 813053
    invoke-virtual {p0, p1, p2}, LX/32s;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, p3, v0}, LX/32s;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 813052
    iget-object v0, p0, LX/4q9;->_delegate:LX/32s;

    invoke-virtual {v0, p1, p2}, LX/32s;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
