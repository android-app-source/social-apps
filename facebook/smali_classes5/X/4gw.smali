.class public LX/4gw;
.super LX/3G3;
.source ""


# instance fields
.field public final h:Ljava/lang/String;

.field public final i:Landroid/os/Bundle;

.field public j:LX/3G4;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;JJII)V
    .locals 9
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 800295
    move-object v0, p0

    move/from16 v1, p9

    move-wide v2, p5

    move-object v4, p1

    move-object v5, p2

    move/from16 v6, p10

    move-wide/from16 v7, p7

    invoke-direct/range {v0 .. v8}, LX/3G3;-><init>(IJLjava/lang/String;Ljava/lang/String;IJ)V

    .line 800296
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 800297
    iput-object p3, p0, LX/4gw;->h:Ljava/lang/String;

    .line 800298
    iput-object p4, p0, LX/4gw;->i:Landroid/os/Bundle;

    .line 800299
    return-void

    .line 800300
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;JJIIB)V
    .locals 1

    .prologue
    .line 800301
    invoke-direct/range {p0 .. p10}, LX/4gw;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;JJII)V

    return-void
.end method


# virtual methods
.method public final a(LX/3G4;)LX/4gw;
    .locals 0

    .prologue
    .line 800302
    iput-object p1, p0, LX/4gw;->j:LX/3G4;

    .line 800303
    return-object p0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 800304
    iget-object v0, p0, LX/4gw;->h:Ljava/lang/String;

    return-object v0
.end method
