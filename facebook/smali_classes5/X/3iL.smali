.class public LX/3iL;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/1CW;

.field private final c:LX/1CX;

.field public final d:LX/03V;

.field public final e:LX/0kL;

.field private final f:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 629198
    const-class v0, LX/3iL;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3iL;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1CW;LX/1CX;LX/03V;LX/0kL;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 629199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 629200
    iput-object p2, p0, LX/3iL;->c:LX/1CX;

    .line 629201
    iput-object p1, p0, LX/3iL;->b:LX/1CW;

    .line 629202
    iput-object p3, p0, LX/3iL;->d:LX/03V;

    .line 629203
    iput-object p4, p0, LX/3iL;->e:LX/0kL;

    .line 629204
    iput-object p5, p0, LX/3iL;->f:Landroid/content/res/Resources;

    .line 629205
    return-void
.end method

.method public static a(LX/0QB;)LX/3iL;
    .locals 1

    .prologue
    .line 629206
    invoke-static {p0}, LX/3iL;->b(LX/0QB;)LX/3iL;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3iL;
    .locals 6

    .prologue
    .line 629207
    new-instance v0, LX/3iL;

    invoke-static {p0}, LX/1CW;->b(LX/0QB;)LX/1CW;

    move-result-object v1

    check-cast v1, LX/1CW;

    invoke-static {p0}, LX/1CX;->a(LX/0QB;)LX/1CX;

    move-result-object v2

    check-cast v2, LX/1CX;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v4

    check-cast v4, LX/0kL;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-direct/range {v0 .. v5}, LX/3iL;-><init>(LX/1CW;LX/1CX;LX/03V;LX/0kL;Landroid/content/res/Resources;)V

    .line 629208
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 629209
    iget-object v0, p0, LX/3iL;->b:LX/1CW;

    invoke-virtual {v0, p1, v1, v1}, LX/1CW;->a(Lcom/facebook/fbservice/service/ServiceException;ZZ)Ljava/lang/String;

    move-result-object v0

    .line 629210
    invoke-static {p1}, LX/1CW;->b(Lcom/facebook/fbservice/service/ServiceException;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 629211
    iget-object v1, p0, LX/3iL;->c:LX/1CX;

    iget-object v2, p0, LX/3iL;->f:Landroid/content/res/Resources;

    invoke-static {v2}, LX/4mm;->a(Landroid/content/res/Resources;)LX/4mn;

    move-result-object v2

    sget-object v3, LX/8iJ;->SENTRY_LIKE_BLOCK:LX/8iJ;

    invoke-virtual {v3}, LX/8iJ;->getTitleId()I

    move-result v3

    invoke-virtual {v2, v3}, LX/4mn;->a(I)LX/4mn;

    move-result-object v2

    .line 629212
    iput-object v0, v2, LX/4mn;->c:Ljava/lang/String;

    .line 629213
    move-object v0, v2

    .line 629214
    const v2, 0x7f080054

    invoke-virtual {v0, v2}, LX/4mn;->c(I)LX/4mn;

    move-result-object v0

    sget-object v2, LX/8iK;->a:Landroid/net/Uri;

    .line 629215
    iput-object v2, v0, LX/4mn;->e:Landroid/net/Uri;

    .line 629216
    move-object v0, v0

    .line 629217
    invoke-virtual {v0}, LX/4mn;->l()LX/4mm;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1CX;->a(LX/4mm;)LX/2EJ;

    .line 629218
    :goto_0
    iget-object v0, p0, LX/3iL;->d:LX/03V;

    sget-object v1, LX/3iL;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 629219
    return-void

    .line 629220
    :cond_0
    iget-object v1, p0, LX/3iL;->e:LX/0kL;

    new-instance v2, LX/27k;

    invoke-direct {v2, v0}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 629221
    invoke-static {p1}, Lcom/facebook/fbservice/service/ServiceException;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/ServiceException;

    move-result-object v0

    .line 629222
    invoke-virtual {p0, v0}, LX/3iL;->a(Lcom/facebook/fbservice/service/ServiceException;)V

    .line 629223
    return-void
.end method
