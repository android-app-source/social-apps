.class public abstract LX/4wp;
.super LX/4wo;
.source ""

# interfaces
.implements LX/0Ri;
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/4wo",
        "<TK;TV;>;",
        "LX/0Ri",
        "<TK;TV;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field public transient a:LX/4wp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4wp",
            "<TV;TK;>;"
        }
    .end annotation
.end field

.field public transient b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private transient c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation
.end field

.field private transient d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TV;>;"
        }
    .end annotation
.end field

.field private transient e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;LX/4wp;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TK;TV;>;",
            "LX/4wp",
            "<TV;TK;>;)V"
        }
    .end annotation

    .prologue
    .line 820519
    invoke-direct {p0}, LX/4wo;-><init>()V

    .line 820520
    iput-object p1, p0, LX/4wp;->b:Ljava/util/Map;

    .line 820521
    iput-object p2, p0, LX/4wp;->a:LX/4wp;

    .line 820522
    return-void
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;Z)TV;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 820508
    invoke-virtual {p0, p1}, LX/4wp;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 820509
    invoke-virtual {p0, p2}, LX/4wp;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 820510
    invoke-virtual {p0, p1}, LX/4wo;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    .line 820511
    if-eqz v3, :cond_0

    invoke-virtual {p0, p1}, LX/4wo;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {p2, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 820512
    :goto_0
    return-object p2

    .line 820513
    :cond_0
    if-eqz p3, :cond_1

    .line 820514
    invoke-virtual {p0}, LX/4wp;->a_()LX/0Ri;

    move-result-object v0

    invoke-interface {v0, p2}, LX/0Ri;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 820515
    :goto_1
    iget-object v0, p0, LX/4wp;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 820516
    invoke-static {p0, p1, v3, v0, p2}, LX/4wp;->a$redex0(LX/4wp;Ljava/lang/Object;ZLjava/lang/Object;Ljava/lang/Object;)V

    move-object p2, v0

    .line 820517
    goto :goto_0

    .line 820518
    :cond_1
    invoke-virtual {p0, p2}, LX/4wp;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    const-string v4, "value already present: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-static {v0, v4, v1}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method public static a$redex0(LX/4wp;Ljava/lang/Object;ZLjava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;ZTV;TV;)V"
        }
    .end annotation

    .prologue
    .line 820504
    if-eqz p2, :cond_0

    .line 820505
    invoke-static {p0, p3}, LX/4wp;->d(LX/4wp;Ljava/lang/Object;)V

    .line 820506
    :cond_0
    iget-object v0, p0, LX/4wp;->a:LX/4wp;

    iget-object v0, v0, LX/4wp;->b:Ljava/util/Map;

    invoke-interface {v0, p4, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 820507
    return-void
.end method

.method public static c(LX/4wp;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 820501
    iget-object v0, p0, LX/4wp;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 820502
    invoke-static {p0, v0}, LX/4wp;->d(LX/4wp;Ljava/lang/Object;)V

    .line 820503
    return-object v0
.end method

.method public static d(LX/4wp;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 820499
    iget-object v0, p0, LX/4wp;->a:LX/4wp;

    iget-object v0, v0, LX/4wp;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 820500
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TK;"
        }
    .end annotation

    .prologue
    .line 820498
    return-object p1
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 820497
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, LX/4wp;->a(Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 820496
    iget-object v0, p0, LX/4wp;->b:Ljava/util/Map;

    return-object v0
.end method

.method public final a(Ljava/util/Map;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TK;TV;>;",
            "Ljava/util/Map",
            "<TV;TK;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 820485
    iget-object v0, p0, LX/4wp;->b:Ljava/util/Map;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 820486
    iget-object v0, p0, LX/4wp;->a:LX/4wp;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 820487
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 820488
    invoke-interface {p2}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 820489
    if-eq p1, p2, :cond_2

    :goto_2
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 820490
    iput-object p1, p0, LX/4wp;->b:Ljava/util/Map;

    .line 820491
    new-instance v0, LX/4wq;

    invoke-direct {v0, p2, p0}, LX/4wq;-><init>(Ljava/util/Map;LX/4wp;)V

    iput-object v0, p0, LX/4wp;->a:LX/4wp;

    .line 820492
    return-void

    :cond_0
    move v0, v2

    .line 820493
    goto :goto_0

    :cond_1
    move v0, v2

    .line 820494
    goto :goto_1

    :cond_2
    move v1, v2

    .line 820495
    goto :goto_2
.end method

.method public a_()LX/0Ri;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Ri",
            "<TV;TK;>;"
        }
    .end annotation

    .prologue
    .line 820484
    iget-object v0, p0, LX/4wp;->a:LX/4wp;

    return-object v0
.end method

.method public b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)TV;"
        }
    .end annotation

    .prologue
    .line 820523
    return-object p1
.end method

.method public b_()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 820467
    iget-object v0, p0, LX/4wp;->d:Ljava/util/Set;

    .line 820468
    if-nez v0, :cond_0

    new-instance v0, LX/4ws;

    invoke-direct {v0, p0}, LX/4ws;-><init>(LX/4wp;)V

    iput-object v0, p0, LX/4wp;->d:Ljava/util/Set;

    :cond_0
    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 820470
    iget-object v0, p0, LX/4wp;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 820471
    iget-object v0, p0, LX/4wp;->a:LX/4wp;

    iget-object v0, v0, LX/4wp;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 820472
    return-void
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 820469
    iget-object v0, p0, LX/4wp;->a:LX/4wp;

    invoke-virtual {v0, p1}, LX/4wo;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 820473
    invoke-virtual {p0}, LX/4wp;->a()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 820474
    iget-object v0, p0, LX/4wp;->e:Ljava/util/Set;

    .line 820475
    if-nez v0, :cond_0

    new-instance v0, LX/4wn;

    invoke-direct {v0, p0}, LX/4wn;-><init>(LX/4wp;)V

    iput-object v0, p0, LX/4wp;->e:Ljava/util/Set;

    :cond_0
    return-object v0
.end method

.method public keySet()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 820476
    iget-object v0, p0, LX/4wp;->c:Ljava/util/Set;

    .line 820477
    if-nez v0, :cond_0

    new-instance v0, LX/4wr;

    invoke-direct {v0, p0}, LX/4wr;-><init>(LX/4wp;)V

    iput-object v0, p0, LX/4wp;->c:Ljava/util/Set;

    :cond_0
    return-object v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 820478
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/4wp;->a(Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<+TK;+TV;>;)V"
        }
    .end annotation

    .prologue
    .line 820479
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 820480
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, LX/4wp;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 820481
    :cond_0
    return-void
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 820482
    invoke-virtual {p0, p1}, LX/4wo;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, LX/4wp;->c(LX/4wp;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 820483
    invoke-virtual {p0}, LX/4wp;->b_()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
