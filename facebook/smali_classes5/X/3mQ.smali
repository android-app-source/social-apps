.class public LX/3mQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 636358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;)Lcom/facebook/graphql/model/GraphQLGroup;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 636359
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;

    if-eqz v0, :cond_0

    .line 636360
    check-cast p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    .line 636361
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;)Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 636362
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;

    if-eqz v0, :cond_0

    .line 636363
    check-cast p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;

    move-result-object v0

    .line 636364
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
