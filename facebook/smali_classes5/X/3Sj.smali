.class public abstract LX/3Sj;
.super LX/3Sk;
.source ""


# instance fields
.field public a:LX/3Sd;

.field public b:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 582932
    invoke-direct {p0}, LX/3Sk;-><init>()V

    .line 582933
    const-string v0, "initialCapacity"

    invoke-static {p1, v0}, LX/3Sl;->a(ILjava/lang/String;)I

    .line 582934
    invoke-static {p1}, LX/3Sd;->a(I)LX/3Sd;

    move-result-object v0

    iput-object v0, p0, LX/3Sj;->a:LX/3Sd;

    .line 582935
    const/4 v0, 0x0

    iput v0, p0, LX/3Sj;->b:I

    .line 582936
    return-void
.end method


# virtual methods
.method public a(LX/15i;II)LX/3Sj;
    .locals 3

    .prologue
    .line 582938
    invoke-static {p1, p2, p3}, LX/3Sm;->a(LX/15i;II)LX/1vs;

    .line 582939
    iget v0, p0, LX/3Sj;->b:I

    add-int/lit8 v0, v0, 0x1

    .line 582940
    iget-object v1, p0, LX/3Sj;->a:LX/3Sd;

    iget v1, v1, LX/3Sd;->b:I

    if-ge v1, v0, :cond_0

    .line 582941
    iget-object v1, p0, LX/3Sj;->a:LX/3Sd;

    iget-object v2, p0, LX/3Sj;->a:LX/3Sd;

    iget v2, v2, LX/3Sd;->b:I

    invoke-static {v2, v0}, LX/4A2;->a(II)I

    move-result v2

    invoke-static {v1, v2}, LX/3Sc;->a(LX/3Sd;I)LX/3Sd;

    move-result-object v1

    iput-object v1, p0, LX/3Sj;->a:LX/3Sd;

    .line 582942
    :cond_0
    iget-object v0, p0, LX/3Sj;->a:LX/3Sd;

    .line 582943
    iget v1, p0, LX/3Sj;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/3Sj;->b:I

    .line 582944
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 582945
    :try_start_0
    invoke-virtual {v0, v1, p1}, LX/3Sd;->a(ILX/15i;)LX/15i;

    .line 582946
    invoke-virtual {v0, v1, p2}, LX/3Sd;->a(II)I

    .line 582947
    invoke-virtual {v0, v1, p3}, LX/3Sd;->b(II)I

    .line 582948
    monitor-exit v2

    .line 582949
    return-object p0

    .line 582950
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public synthetic b(LX/15i;II)LX/3Sk;
    .locals 1

    .prologue
    .line 582937
    invoke-virtual {p0, p1, p2, p3}, LX/3Sj;->a(LX/15i;II)LX/3Sj;

    move-result-object v0

    return-object v0
.end method
