.class public LX/4d4;
.super Ljava/io/FilterOutputStream;
.source ""


# instance fields
.field private a:J

.field private final b:J


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;J)V
    .locals 2

    .prologue
    .line 795414
    invoke-direct {p0, p1}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 795415
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Limit must be non-negative"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 795416
    iput-wide p2, p0, LX/4d4;->b:J

    .line 795417
    return-void

    .line 795418
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final write(I)V
    .locals 4

    .prologue
    .line 795419
    iget-wide v0, p0, LX/4d4;->a:J

    iget-wide v2, p0, LX/4d4;->b:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 795420
    new-instance v0, LX/4d3;

    iget-wide v2, p0, LX/4d4;->b:J

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, LX/4d3;-><init>(JI)V

    throw v0

    .line 795421
    :cond_0
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 795422
    return-void
.end method

.method public final write([BII)V
    .locals 6

    .prologue
    .line 795423
    iget-wide v0, p0, LX/4d4;->b:J

    iget-wide v2, p0, LX/4d4;->a:J

    sub-long/2addr v0, v2

    .line 795424
    int-to-long v2, p3

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    .line 795425
    if-lez v0, :cond_0

    .line 795426
    iget-object v1, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v1, p1, p2, v0}, Ljava/io/OutputStream;->write([BII)V

    .line 795427
    iget-wide v2, p0, LX/4d4;->a:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/4d4;->a:J

    .line 795428
    :cond_0
    if-ge v0, p3, :cond_1

    .line 795429
    sub-int v0, p3, v0

    .line 795430
    new-instance v1, LX/4d3;

    iget-wide v2, p0, LX/4d4;->b:J

    invoke-direct {v1, v2, v3, v0}, LX/4d3;-><init>(JI)V

    throw v1

    .line 795431
    :cond_1
    return-void
.end method
