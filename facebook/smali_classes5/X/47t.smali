.class public LX/47t;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 672535
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static varargs a(IILandroid/text/SpannableString;[Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 672541
    array-length v1, p3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p3, v0

    .line 672542
    const/16 v3, 0x21

    invoke-virtual {p2, v2, p0, p1, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 672543
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 672544
    :cond_0
    return-void
.end method

.method public static varargs a(Landroid/text/SpannableStringBuilder;[Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 672536
    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 672537
    array-length v3, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, p1, v2

    .line 672538
    const/16 v5, 0x21

    invoke-virtual {p0, v4, v0, v1, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 672539
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 672540
    :cond_0
    return-void
.end method
