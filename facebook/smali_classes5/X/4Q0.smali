.class public LX/4Q0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 702436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 702388
    const/4 v9, 0x0

    .line 702389
    const/4 v8, 0x0

    .line 702390
    const/4 v7, 0x0

    .line 702391
    const/4 v6, 0x0

    .line 702392
    const/4 v5, 0x0

    .line 702393
    const/4 v4, 0x0

    .line 702394
    const/4 v3, 0x0

    .line 702395
    const/4 v2, 0x0

    .line 702396
    const/4 v1, 0x0

    .line 702397
    const/4 v0, 0x0

    .line 702398
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 702399
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 702400
    const/4 v0, 0x0

    .line 702401
    :goto_0
    return v0

    .line 702402
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 702403
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_8

    .line 702404
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 702405
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 702406
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 702407
    const-string v11, "cache_id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 702408
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 702409
    :cond_2
    const-string v11, "cursor"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 702410
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 702411
    :cond_3
    const-string v11, "importance_score"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 702412
    const/4 v2, 0x1

    .line 702413
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v7

    goto :goto_1

    .line 702414
    :cond_4
    const-string v11, "local_is_rich_notif_collapsed"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 702415
    const/4 v1, 0x1

    .line 702416
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v6

    goto :goto_1

    .line 702417
    :cond_5
    const-string v11, "local_num_impressions"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 702418
    const/4 v0, 0x1

    .line 702419
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    goto :goto_1

    .line 702420
    :cond_6
    const-string v11, "node"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 702421
    invoke-static {p0, p1}, LX/2aD;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 702422
    :cond_7
    const-string v11, "eligible_buckets"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 702423
    const-class v3, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    invoke-static {p0, p1, v3}, LX/2gu;->a(LX/15w;LX/186;Ljava/lang/Class;)I

    move-result v3

    goto :goto_1

    .line 702424
    :cond_8
    const/16 v10, 0xe

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 702425
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 702426
    const/4 v9, 0x1

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 702427
    if-eqz v2, :cond_9

    .line 702428
    const/4 v2, 0x5

    const/4 v8, 0x0

    invoke-virtual {p1, v2, v7, v8}, LX/186;->a(III)V

    .line 702429
    :cond_9
    if-eqz v1, :cond_a

    .line 702430
    const/4 v1, 0x7

    invoke-virtual {p1, v1, v6}, LX/186;->a(IZ)V

    .line 702431
    :cond_a
    if-eqz v0, :cond_b

    .line 702432
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v5, v1}, LX/186;->a(III)V

    .line 702433
    :cond_b
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 702434
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 702435
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/16 v3, 0xd

    const/4 v2, 0x0

    .line 702357
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 702358
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 702359
    if-eqz v0, :cond_0

    .line 702360
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702361
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 702362
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 702363
    if-eqz v0, :cond_1

    .line 702364
    const-string v1, "cursor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702365
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 702366
    :cond_1
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 702367
    if-eqz v0, :cond_2

    .line 702368
    const-string v1, "importance_score"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702369
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 702370
    :cond_2
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 702371
    if-eqz v0, :cond_3

    .line 702372
    const-string v1, "local_is_rich_notif_collapsed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702373
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 702374
    :cond_3
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 702375
    if-eqz v0, :cond_4

    .line 702376
    const-string v1, "local_num_impressions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702377
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 702378
    :cond_4
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 702379
    if-eqz v0, :cond_5

    .line 702380
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702381
    invoke-static {p0, v0, p2, p3}, LX/2aD;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 702382
    :cond_5
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 702383
    if-eqz v0, :cond_6

    .line 702384
    const-string v0, "eligible_buckets"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702385
    const-class v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->b(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->b(Ljava/util/Iterator;LX/0nX;)V

    .line 702386
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 702387
    return-void
.end method
