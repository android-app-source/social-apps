.class public final LX/3mu;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/3mt;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1X1;

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 637392
    invoke-static {}, LX/3mt;->q()LX/3mt;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 637393
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 637394
    const-string v0, "PageWrapper"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 637395
    if-ne p0, p1, :cond_1

    .line 637396
    :cond_0
    :goto_0
    return v0

    .line 637397
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 637398
    goto :goto_0

    .line 637399
    :cond_3
    check-cast p1, LX/3mu;

    .line 637400
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 637401
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 637402
    if-eq v2, v3, :cond_0

    .line 637403
    iget-object v2, p0, LX/3mu;->a:LX/1X1;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/3mu;->a:LX/1X1;

    iget-object v3, p1, LX/3mu;->a:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 637404
    goto :goto_0

    .line 637405
    :cond_5
    iget-object v2, p1, LX/3mu;->a:LX/1X1;

    if-nez v2, :cond_4

    .line 637406
    :cond_6
    iget v2, p0, LX/3mu;->b:I

    iget v3, p1, LX/3mu;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 637407
    goto :goto_0

    .line 637408
    :cond_7
    iget v2, p0, LX/3mu;->c:I

    iget v3, p1, LX/3mu;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 637409
    goto :goto_0
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 637410
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/3mu;

    .line 637411
    iget-object v1, v0, LX/3mu;->a:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/3mu;->a:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/3mu;->a:LX/1X1;

    .line 637412
    return-object v0

    .line 637413
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
