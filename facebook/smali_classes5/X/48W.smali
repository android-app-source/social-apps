.class public final LX/48W;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/48W;


# instance fields
.field public final b:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 672962
    new-instance v0, LX/48W;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/48W;-><init>(I)V

    sput-object v0, LX/48W;->a:LX/48W;

    return-void
.end method

.method private constructor <init>(I)V
    .locals 0

    .prologue
    .line 672959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 672960
    iput p1, p0, LX/48W;->b:I

    .line 672961
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 672958
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AppStrictMode.AppPolicy{mask="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, LX/48W;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
