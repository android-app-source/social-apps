.class public LX/4qG;
.super LX/4pV;
.source ""


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 813275
    invoke-direct {p0, p1}, LX/4pV;-><init>(Ljava/lang/Class;)V

    .line 813276
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/4pR;
    .locals 3

    .prologue
    .line 813277
    new-instance v0, LX/4pR;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    iget-object v2, p0, LX/4pT;->_scope:Ljava/lang/Class;

    invoke-direct {v0, v1, v2, p1}, LX/4pR;-><init>(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Object;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Class;)LX/4pS;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/4pS",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 813278
    iget-object v0, p0, LX/4pT;->_scope:Ljava/lang/Class;

    if-ne p1, v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance p0, LX/4qG;

    invoke-direct {p0, p1}, LX/4qG;-><init>(Ljava/lang/Class;)V

    goto :goto_0
.end method

.method public final b()LX/4pS;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4pS",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 813279
    return-object p0
.end method

.method public final b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 813280
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
