.class public LX/3tt;
.super Landroid/view/View;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 646876
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3tt;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 646877
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 646891
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3tt;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 646892
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 646887
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 646888
    invoke-virtual {p0}, LX/3tt;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 646889
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, LX/3tt;->setVisibility(I)V

    .line 646890
    :cond_0
    return-void
.end method

.method private static a(II)I
    .locals 2

    .prologue
    .line 646881
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 646882
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 646883
    sparse-switch v1, :sswitch_data_0

    .line 646884
    :goto_0
    :sswitch_0
    return p0

    .line 646885
    :sswitch_1
    invoke-static {p0, v0}, Ljava/lang/Math;->min(II)I

    move-result p0

    goto :goto_0

    :sswitch_2
    move p0, v0

    .line 646886
    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 646880
    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 646878
    invoke-virtual {p0}, LX/3tt;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-static {v0, p1}, LX/3tt;->a(II)I

    move-result v0

    invoke-virtual {p0}, LX/3tt;->getSuggestedMinimumHeight()I

    move-result v1

    invoke-static {v1, p2}, LX/3tt;->a(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/3tt;->setMeasuredDimension(II)V

    .line 646879
    return-void
.end method
