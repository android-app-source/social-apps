.class public abstract LX/3p4;
.super LX/0k3;
.source ""

# interfaces
.implements Landroid/view/LayoutInflater$Factory;


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/0jz;

.field private final c:Landroid/view/MenuInflater;

.field private final d:Landroid/view/LayoutInflater;

.field private final e:Landroid/os/Handler;

.field private f:LX/0gc;

.field private g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/0k4;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 641187
    invoke-direct {p0}, LX/0k3;-><init>()V

    .line 641188
    iput-object p1, p0, LX/3p4;->a:Landroid/content/Context;

    .line 641189
    new-instance v0, LX/0jz;

    invoke-direct {v0}, LX/0jz;-><init>()V

    iput-object v0, p0, LX/3p4;->b:LX/0jz;

    .line 641190
    iget-object v0, p0, LX/3p4;->b:LX/0jz;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, p0, v1}, LX/0jz;->a(LX/0k3;LX/0k1;Landroid/support/v4/app/Fragment;)V

    .line 641191
    new-instance v0, Landroid/view/MenuInflater;

    invoke-virtual {p0}, LX/0k3;->h()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/3p4;->c:Landroid/view/MenuInflater;

    .line 641192
    invoke-virtual {p0}, LX/0k3;->h()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {p0}, LX/0k3;->h()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/3p4;->d:Landroid/view/LayoutInflater;

    .line 641193
    iget-object v0, p0, LX/3p4;->d:Landroid/view/LayoutInflater;

    invoke-virtual {v0}, Landroid/view/LayoutInflater;->getFactory()Landroid/view/LayoutInflater$Factory;

    move-result-object v0

    if-nez v0, :cond_0

    .line 641194
    iget-object v0, p0, LX/3p4;->d:Landroid/view/LayoutInflater;

    invoke-virtual {v0, p0}, Landroid/view/LayoutInflater;->setFactory(Landroid/view/LayoutInflater$Factory;)V

    .line 641195
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, LX/0k3;->h()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/3p4;->e:Landroid/os/Handler;

    .line 641196
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;ZZ)LX/0k4;
    .locals 2

    .prologue
    .line 641157
    iget-object v0, p0, LX/3p4;->g:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 641158
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/3p4;->g:Ljava/util/HashMap;

    .line 641159
    :cond_0
    iget-object v0, p0, LX/3p4;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0k4;

    .line 641160
    if-nez v0, :cond_2

    .line 641161
    if-eqz p3, :cond_1

    .line 641162
    new-instance v0, LX/0k4;

    invoke-direct {v0, p1, p0, p2}, LX/0k4;-><init>(Ljava/lang/String;LX/0k3;Z)V

    .line 641163
    iget-object v1, p0, LX/3p4;->g:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 641164
    :cond_1
    :goto_0
    return-object v0

    .line 641165
    :cond_2
    iput-object p0, v0, LX/0k4;->e:LX/0k3;

    .line 641166
    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 641185
    iget-object v0, p0, LX/3p4;->b:LX/0jz;

    invoke-virtual {v0}, LX/0jz;->n()V

    .line 641186
    return-void
.end method

.method public final a(LX/0gc;)V
    .locals 0

    .prologue
    .line 641183
    iput-object p1, p0, LX/3p4;->f:LX/0gc;

    .line 641184
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 641178
    if-eqz p1, :cond_0

    .line 641179
    const-string v0, "android:support:fragments"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 641180
    iget-object v1, p0, LX/3p4;->b:LX/0jz;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/0jz;->a(Landroid/os/Parcelable;Ljava/util/ArrayList;)V

    .line 641181
    :cond_0
    iget-object v0, p0, LX/3p4;->b:LX/0jz;

    invoke-virtual {v0}, LX/0jz;->m()V

    .line 641182
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 641177
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 641175
    invoke-virtual {p0}, LX/0k3;->h()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 641176
    return-void
.end method

.method public final a(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 0

    .prologue
    .line 641174
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 641173
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 641172
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 641170
    iget-object v0, p0, LX/3p4;->b:LX/0jz;

    invoke-virtual {v0}, LX/0jz;->p()V

    .line 641171
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 641168
    iget-object v0, p0, LX/3p4;->b:LX/0jz;

    invoke-virtual {v0}, LX/0jz;->q()V

    .line 641169
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 641197
    iget-object v0, p0, LX/3p4;->b:LX/0jz;

    invoke-virtual {v0}, LX/0jz;->u()V

    .line 641198
    return-void
.end method

.method public final h()Landroid/content/Context;
    .locals 1

    .prologue
    .line 641102
    iget-object v0, p0, LX/3p4;->a:Landroid/content/Context;

    return-object v0
.end method

.method public final i()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 641103
    iget-object v0, p0, LX/3p4;->e:Landroid/os/Handler;

    return-object v0
.end method

.method public j()Landroid/view/Window;
    .locals 1

    .prologue
    .line 641104
    const/4 v0, 0x0

    return-object v0
.end method

.method public k()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 641105
    invoke-virtual {p0}, LX/0k3;->h()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 641106
    iget-object v0, p0, LX/3p4;->b:LX/0jz;

    invoke-virtual {v0}, LX/0jz;->x()V

    .line 641107
    return-void
.end method

.method public final m()Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 641108
    iget-object v0, p0, LX/3p4;->d:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 641109
    const/4 v0, 0x0

    return v0
.end method

.method public final o()LX/0jz;
    .locals 1

    .prologue
    .line 641110
    iget-object v0, p0, LX/3p4;->b:LX/0jz;

    return-object v0
.end method

.method public final onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v9, 0x1

    const/4 v3, -0x1

    .line 641111
    const-string v0, "fragment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v4

    .line 641112
    :goto_0
    return-object v0

    .line 641113
    :cond_0
    const-string v0, "class"

    invoke-interface {p3, v4, v0}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 641114
    sget-object v1, LX/3p3;->a:[I

    invoke-virtual {p2, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v5

    .line 641115
    if-nez v0, :cond_e

    .line 641116
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 641117
    :goto_1
    invoke-virtual {v5, v9, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 641118
    const/4 v0, 0x2

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 641119
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    .line 641120
    if-ne v2, v3, :cond_1

    if-nez v6, :cond_1

    .line 641121
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": Must specify unique android:id, android:tag, or have a parent with an id for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 641122
    :cond_1
    iget-object v0, p0, LX/3p4;->f:LX/0gc;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/3p4;->f:LX/0gc;

    check-cast v0, LX/0jz;

    move-object v5, v0

    .line 641123
    :goto_2
    if-eq v2, v3, :cond_6

    invoke-virtual {v5, v2}, LX/0jz;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 641124
    :goto_3
    if-nez v0, :cond_2

    if-eqz v6, :cond_2

    .line 641125
    invoke-virtual {v5, v6}, LX/0jz;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 641126
    :cond_2
    sget-boolean v7, LX/0jz;->a:Z

    if-eqz v7, :cond_3

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "onCreateView: id=0x"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " fname="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " existing="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 641127
    :cond_3
    if-nez v0, :cond_8

    .line 641128
    invoke-virtual {p0}, LX/0k3;->h()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    .line 641129
    iput-boolean v9, v4, Landroid/support/v4/app/Fragment;->mFromLayout:Z

    .line 641130
    if-eqz v2, :cond_7

    move v0, v2

    :goto_4
    iput v0, v4, Landroid/support/v4/app/Fragment;->mFragmentId:I

    .line 641131
    iput v3, v4, Landroid/support/v4/app/Fragment;->mContainerId:I

    .line 641132
    iput-object v6, v4, Landroid/support/v4/app/Fragment;->mTag:Ljava/lang/String;

    .line 641133
    iput-boolean v9, v4, Landroid/support/v4/app/Fragment;->mInLayout:Z

    .line 641134
    iput-object v5, v4, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    .line 641135
    const-string v0, "com.google.android.gms.maps.SupportMapFragment"

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 641136
    new-instance v0, Landroid/support/v4/app/FakeActivityForMapFragment;

    invoke-virtual {p0}, LX/0k3;->h()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v4, v3}, Landroid/support/v4/app/FakeActivityForMapFragment;-><init>(Landroid/support/v4/app/Fragment;Landroid/content/Context;)V

    iput-object v0, v4, Landroid/support/v4/app/Fragment;->mFakeActivityForMapFragment:Landroid/support/v4/app/FakeActivityForMapFragment;

    .line 641137
    :cond_4
    iget-object v0, v4, Landroid/support/v4/app/Fragment;->mFakeActivityForMapFragment:Landroid/support/v4/app/FakeActivityForMapFragment;

    iget-object v3, v4, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    invoke-virtual {v4, v0, p3, v3}, Landroid/support/v4/app/Fragment;->onInflate(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    .line 641138
    invoke-virtual {v5, v4, v9}, LX/0jz;->a(Landroid/support/v4/app/Fragment;Z)V

    move-object v0, v4

    .line 641139
    :goto_5
    iget-object v3, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    if-nez v3, :cond_b

    .line 641140
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not create a view."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 641141
    :cond_5
    iget-object v0, p0, LX/3p4;->b:LX/0jz;

    move-object v5, v0

    goto/16 :goto_2

    :cond_6
    move-object v0, v4

    .line 641142
    goto/16 :goto_3

    :cond_7
    move v0, v3

    .line 641143
    goto :goto_4

    .line 641144
    :cond_8
    iget-boolean v7, v0, Landroid/support/v4/app/Fragment;->mInLayout:Z

    if-eqz v7, :cond_9

    .line 641145
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": Duplicate id 0x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", tag "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", or parent id 0x"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with another fragment for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 641146
    :cond_9
    iput-boolean v9, v0, Landroid/support/v4/app/Fragment;->mInLayout:Z

    .line 641147
    iget-boolean v3, v0, Landroid/support/v4/app/Fragment;->mRetaining:Z

    if-nez v3, :cond_a

    .line 641148
    iget-object v3, v0, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    invoke-virtual {v0, v4, p3, v3}, Landroid/support/v4/app/Fragment;->onInflate(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    .line 641149
    :cond_a
    invoke-virtual {v5, v0}, LX/0jz;->c(Landroid/support/v4/app/Fragment;)V

    goto/16 :goto_5

    .line 641150
    :cond_b
    if-eqz v2, :cond_c

    .line 641151
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    .line 641152
    :cond_c
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_d

    .line 641153
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 641154
    :cond_d
    iget-object v0, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    goto/16 :goto_0

    :cond_e
    move-object v1, v0

    goto/16 :goto_1
.end method

.method public final p()LX/0gc;
    .locals 1

    .prologue
    .line 641155
    iget-object v0, p0, LX/3p4;->b:LX/0jz;

    return-object v0
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 641156
    const/4 v0, 0x0

    return v0
.end method

.method public final r()LX/0gc;
    .locals 1

    .prologue
    .line 641167
    iget-object v0, p0, LX/3p4;->f:LX/0gc;

    return-object v0
.end method
