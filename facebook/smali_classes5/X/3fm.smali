.class public LX/3fm;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/contacts/server/FetchAllContactsParams;",
        "Lcom/facebook/contacts/server/FetchAllContactsResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final c:LX/3fn;

.field private final d:LX/3fb;

.field private final e:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 623434
    const-class v0, LX/3fm;

    sput-object v0, LX/3fm;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/3fn;LX/3fb;LX/0sO;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 623435
    invoke-direct {p0, p3}, LX/0ro;-><init>(LX/0sO;)V

    .line 623436
    iput-object p1, p0, LX/3fm;->c:LX/3fn;

    .line 623437
    iput-object p2, p0, LX/3fm;->d:LX/3fb;

    .line 623438
    iput-object p4, p0, LX/3fm;->e:LX/0SG;

    .line 623439
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 623440
    check-cast p1, Lcom/facebook/contacts/server/FetchAllContactsParams;

    .line 623441
    const/4 v8, 0x0

    .line 623442
    iget-object v0, p1, Lcom/facebook/contacts/server/FetchAllContactsParams;->b:Ljava/lang/String;

    move-object v0, v0

    .line 623443
    if-eqz v0, :cond_0

    .line 623444
    const-class v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullWithAfterQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullWithAfterQueryModel;

    .line 623445
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullWithAfterQueryModel;->a()LX/3h7;

    move-result-object v0

    .line 623446
    :goto_0
    invoke-interface {v0}, LX/3h8;->a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactsSyncFullModel$PageInfoModel;

    move-result-object v1

    .line 623447
    invoke-virtual {v1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactsSyncFullModel$PageInfoModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 623448
    invoke-virtual {v1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactsSyncFullModel$PageInfoModel;->b()Z

    move-result v4

    .line 623449
    invoke-virtual {v1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactsSyncFullModel$PageInfoModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 623450
    invoke-interface {v0}, LX/3h7;->b()LX/0Px;

    move-result-object v2

    .line 623451
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 623452
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 623453
    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;

    .line 623454
    iget-object v7, p0, LX/3fm;->d:LX/3fb;

    invoke-virtual {v7, v0}, LX/3fb;->a(Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;)LX/3hB;

    move-result-object v0

    .line 623455
    invoke-virtual {v0}, LX/3hB;->O()Lcom/facebook/contacts/graphql/Contact;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 623456
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 623457
    :cond_0
    const-class v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel;

    .line 623458
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel;->a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel$MessengerContactsModel;

    move-result-object v1

    .line 623459
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel;->a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel$MessengerContactsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel$MessengerContactsModel;->c()Ljava/lang/String;

    move-result-object v8

    move-object v0, v1

    goto :goto_0

    .line 623460
    :cond_1
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 623461
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Got result: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 623462
    new-instance v0, Lcom/facebook/contacts/server/FetchAllContactsResult;

    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v6, p0, LX/3fm;->e:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    invoke-direct/range {v0 .. v8}, Lcom/facebook/contacts/server/FetchAllContactsResult;-><init>(LX/0ta;LX/0Px;Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;)V

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 623463
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 4

    .prologue
    .line 623464
    check-cast p1, Lcom/facebook/contacts/server/FetchAllContactsParams;

    .line 623465
    iget-object v0, p0, LX/3fm;->c:LX/3fn;

    .line 623466
    iget v1, p1, Lcom/facebook/contacts/server/FetchAllContactsParams;->a:I

    move v1, v1

    .line 623467
    iget-object v2, p1, Lcom/facebook/contacts/server/FetchAllContactsParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 623468
    sget-object v3, LX/3gq;->FULL:LX/3gq;

    invoke-virtual {v0, v1, v2, v3}, LX/3fn;->a(ILjava/lang/String;LX/3gq;)LX/0gW;

    move-result-object v0

    return-object v0
.end method
