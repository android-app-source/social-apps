.class public LX/3mx;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3n6;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/3mx",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/3n6;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 637547
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 637548
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/3mx;->b:LX/0Zi;

    .line 637549
    iput-object p1, p0, LX/3mx;->a:LX/0Ot;

    .line 637550
    return-void
.end method

.method public static a(LX/0QB;)LX/3mx;
    .locals 4

    .prologue
    .line 637536
    const-class v1, LX/3mx;

    monitor-enter v1

    .line 637537
    :try_start_0
    sget-object v0, LX/3mx;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 637538
    sput-object v2, LX/3mx;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 637539
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 637540
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 637541
    new-instance v3, LX/3mx;

    const/16 p0, 0x89d

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/3mx;-><init>(LX/0Ot;)V

    .line 637542
    move-object v0, v3

    .line 637543
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 637544
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3mx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 637545
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 637546
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 637504
    const v0, -0x1aac3fe5

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 637514
    check-cast p2, LX/3n4;

    .line 637515
    iget-object v0, p0, LX/3mx;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/3n4;->a:LX/3mR;

    iget-object v1, p2, LX/3n4;->b:LX/3n2;

    const/4 p2, 0x3

    const/4 p0, 0x0

    const/4 v6, 0x2

    .line 637516
    iget-object v2, v0, LX/3mR;->b:LX/254;

    .line 637517
    const/4 v3, 0x0

    .line 637518
    invoke-static {v2}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProfile;->P()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v4

    .line 637519
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq v4, v5, :cond_0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v4, v5, :cond_1

    .line 637520
    :cond_0
    const/4 v3, 0x1

    .line 637521
    :cond_1
    iget-boolean v4, v1, LX/3n2;->a:Z

    move v4, v4

    .line 637522
    if-eqz v4, :cond_5

    .line 637523
    if-eqz v3, :cond_4

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 637524
    :goto_0
    move-object v2, v3

    .line 637525
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq v2, v3, :cond_2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v2, v3, :cond_3

    .line 637526
    :cond_2
    const v3, 0x7f020a52

    .line 637527
    const v2, 0x7f081b98

    .line 637528
    :goto_1
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v4, v5}, LX/1o5;->a(Landroid/widget/ImageView$ScaleType;)LX/1o5;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/1o5;->h(I)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, 0x7f0b0937

    invoke-interface {v3, v4}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b0936

    invoke-interface {v3, v4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v6}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Di;->y(I)LX/1Di;

    move-result-object v3

    .line 637529
    const v4, -0x1aac3fe5

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 637530
    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v2}, LX/1Di;->A(I)LX/1Di;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, p0, v3}, LX/1Di;->h(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2, v6, v6}, LX/1Di;->h(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2, p2, p2}, LX/1Di;->d(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 637531
    return-object v0

    .line 637532
    :cond_3
    const v3, 0x7f020ad5

    .line 637533
    const v2, 0x7f081b99

    goto :goto_1

    .line 637534
    :cond_4
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    goto :goto_0

    .line 637535
    :cond_5
    if-eqz v3, :cond_6

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    goto :goto_0

    :cond_6
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_JOIN:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 637505
    invoke-static {}, LX/1dS;->b()V

    .line 637506
    iget v0, p1, LX/1dQ;->b:I

    .line 637507
    packed-switch v0, :pswitch_data_0

    .line 637508
    :goto_0
    return-object v1

    .line 637509
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 637510
    check-cast v0, LX/3n4;

    .line 637511
    iget-object v2, p0, LX/3mx;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3n6;

    iget-object v3, v0, LX/3n4;->a:LX/3mR;

    iget-object v4, v0, LX/3n4;->b:LX/3n2;

    iget-object p1, v0, LX/3n4;->c:LX/1Po;

    .line 637512
    iget-object p2, v2, LX/3n6;->a:LX/3mE;

    iget-object p0, v3, LX/3mR;->a:Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;

    iget-object v0, v3, LX/3mR;->b:LX/254;

    invoke-virtual {p2, p1, p0, v0, v4}, LX/3mE;->a(LX/1Po;Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;LX/254;LX/3n2;)V

    .line 637513
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1aac3fe5
        :pswitch_0
    .end packed-switch
.end method
