.class public abstract LX/3tG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static final r:I


# instance fields
.field public final a:LX/3tF;

.field public final b:Landroid/view/animation/Interpolator;

.field public final c:Landroid/view/View;

.field public d:Ljava/lang/Runnable;

.field public e:[F

.field public f:[F

.field public g:I

.field public h:I

.field public i:[F

.field public j:[F

.field public k:[F

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Z

.field private p:Z

.field private q:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 644663
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    sput v0, LX/3tG;->r:I

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 9

    .prologue
    const v6, 0x7f7fffff    # Float.MAX_VALUE

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f000000    # 0.5f

    const v3, 0x3e4ccccd    # 0.2f

    const/4 v1, 0x2

    .line 644623
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 644624
    new-instance v0, LX/3tF;

    invoke-direct {v0}, LX/3tF;-><init>()V

    iput-object v0, p0, LX/3tG;->a:LX/3tF;

    .line 644625
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    iput-object v0, p0, LX/3tG;->b:Landroid/view/animation/Interpolator;

    .line 644626
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, LX/3tG;->e:[F

    .line 644627
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, LX/3tG;->f:[F

    .line 644628
    new-array v0, v1, [F

    fill-array-data v0, :array_2

    iput-object v0, p0, LX/3tG;->i:[F

    .line 644629
    new-array v0, v1, [F

    fill-array-data v0, :array_3

    iput-object v0, p0, LX/3tG;->j:[F

    .line 644630
    new-array v0, v1, [F

    fill-array-data v0, :array_4

    iput-object v0, p0, LX/3tG;->k:[F

    .line 644631
    iput-object p1, p0, LX/3tG;->c:Landroid/view/View;

    .line 644632
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 644633
    const v1, 0x44c4e000    # 1575.0f

    iget v2, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    add-float/2addr v1, v4

    float-to-int v1, v1

    .line 644634
    const v2, 0x439d8000    # 315.0f

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v2

    add-float/2addr v0, v4

    float-to-int v0, v0

    .line 644635
    int-to-float v2, v1

    int-to-float v1, v1

    const/high16 p1, 0x447a0000    # 1000.0f

    .line 644636
    iget-object v4, p0, LX/3tG;->k:[F

    const/4 v7, 0x0

    div-float v8, v2, p1

    aput v8, v4, v7

    .line 644637
    iget-object v4, p0, LX/3tG;->k:[F

    const/4 v7, 0x1

    div-float v8, v1, p1

    aput v8, v4, v7

    .line 644638
    int-to-float v1, v0

    int-to-float v0, v0

    const/high16 v8, 0x447a0000    # 1000.0f

    .line 644639
    iget-object v2, p0, LX/3tG;->j:[F

    const/4 v4, 0x0

    div-float v7, v1, v8

    aput v7, v2, v4

    .line 644640
    iget-object v2, p0, LX/3tG;->j:[F

    const/4 v4, 0x1

    div-float v7, v0, v8

    aput v7, v2, v4

    .line 644641
    const/4 v0, 0x1

    .line 644642
    iput v0, p0, LX/3tG;->g:I

    .line 644643
    iget-object v0, p0, LX/3tG;->f:[F

    const/4 v1, 0x0

    aput v6, v0, v1

    .line 644644
    iget-object v0, p0, LX/3tG;->f:[F

    const/4 v1, 0x1

    aput v6, v0, v1

    .line 644645
    iget-object v0, p0, LX/3tG;->e:[F

    const/4 v1, 0x0

    aput v3, v0, v1

    .line 644646
    iget-object v0, p0, LX/3tG;->e:[F

    const/4 v1, 0x1

    aput v3, v0, v1

    .line 644647
    const/high16 v3, 0x447a0000    # 1000.0f

    .line 644648
    iget-object v0, p0, LX/3tG;->i:[F

    const/4 v1, 0x0

    div-float v2, v5, v3

    aput v2, v0, v1

    .line 644649
    iget-object v0, p0, LX/3tG;->i:[F

    const/4 v1, 0x1

    div-float v2, v5, v3

    aput v2, v0, v1

    .line 644650
    sget v0, LX/3tG;->r:I

    .line 644651
    iput v0, p0, LX/3tG;->h:I

    .line 644652
    const/16 v0, 0x1f4

    .line 644653
    iget-object v1, p0, LX/3tG;->a:LX/3tF;

    .line 644654
    iput v0, v1, LX/3tF;->a:I

    .line 644655
    const/16 v0, 0x1f4

    .line 644656
    iget-object v1, p0, LX/3tG;->a:LX/3tF;

    .line 644657
    iput v0, v1, LX/3tF;->b:I

    .line 644658
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 644659
    :array_1
    .array-data 4
        0x7f7fffff    # Float.MAX_VALUE
        0x7f7fffff    # Float.MAX_VALUE
    .end array-data

    .line 644660
    :array_2
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 644661
    :array_3
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 644662
    :array_4
    .array-data 4
        0x7f7fffff    # Float.MAX_VALUE
        0x7f7fffff    # Float.MAX_VALUE
    .end array-data
.end method

.method private a(IFFF)F
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 644602
    iget-object v1, p0, LX/3tG;->e:[F

    aget v1, v1, p1

    .line 644603
    iget-object v2, p0, LX/3tG;->f:[F

    aget v2, v2, p1

    .line 644604
    const/4 v3, 0x0

    .line 644605
    mul-float v4, v1, p3

    invoke-static {v4, v3, v2}, LX/3tG;->b(FFF)F

    move-result v4

    .line 644606
    invoke-static {p0, p2, v4}, LX/3tG;->f(LX/3tG;FF)F

    move-result v5

    .line 644607
    sub-float v6, p3, p2

    invoke-static {p0, v6, v4}, LX/3tG;->f(LX/3tG;FF)F

    move-result v4

    .line 644608
    sub-float/2addr v4, v5

    .line 644609
    cmpg-float v5, v4, v3

    if-gez v5, :cond_3

    .line 644610
    iget-object v3, p0, LX/3tG;->b:Landroid/view/animation/Interpolator;

    neg-float v4, v4

    invoke-interface {v3, v4}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v3

    neg-float v3, v3

    .line 644611
    :goto_0
    const/high16 v4, -0x40800000    # -1.0f

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {v3, v4, v5}, LX/3tG;->b(FFF)F

    move-result v3

    :cond_0
    move v1, v3

    .line 644612
    cmpl-float v2, v1, v0

    if-nez v2, :cond_1

    .line 644613
    :goto_1
    return v0

    .line 644614
    :cond_1
    iget-object v2, p0, LX/3tG;->i:[F

    aget v2, v2, p1

    .line 644615
    iget-object v3, p0, LX/3tG;->j:[F

    aget v3, v3, p1

    .line 644616
    iget-object v4, p0, LX/3tG;->k:[F

    aget v4, v4, p1

    .line 644617
    mul-float/2addr v2, p4

    .line 644618
    cmpl-float v0, v1, v0

    if-lez v0, :cond_2

    .line 644619
    mul-float v0, v1, v2

    invoke-static {v0, v3, v4}, LX/3tG;->b(FFF)F

    move-result v0

    goto :goto_1

    .line 644620
    :cond_2
    neg-float v0, v1

    mul-float/2addr v0, v2

    invoke-static {v0, v3, v4}, LX/3tG;->b(FFF)F

    move-result v0

    neg-float v0, v0

    goto :goto_1

    .line 644621
    :cond_3
    cmpl-float v5, v4, v3

    if-lez v5, :cond_0

    .line 644622
    iget-object v3, p0, LX/3tG;->b:Landroid/view/animation/Interpolator;

    invoke-interface {v3, v4}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v3

    goto :goto_0
.end method

.method public static b(FFF)F
    .locals 1

    .prologue
    .line 644540
    cmpl-float v0, p0, p2

    if-lez v0, :cond_0

    .line 644541
    :goto_0
    return p2

    .line 644542
    :cond_0
    cmpg-float v0, p0, p1

    if-gez v0, :cond_1

    move p2, p1

    .line 644543
    goto :goto_0

    :cond_1
    move p2, p0

    .line 644544
    goto :goto_0
.end method

.method public static b$redex0(LX/3tG;)Z
    .locals 4

    .prologue
    .line 644598
    iget-object v0, p0, LX/3tG;->a:LX/3tF;

    .line 644599
    iget v1, v0, LX/3tF;->d:F

    iget v2, v0, LX/3tF;->d:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float/2addr v1, v2

    float-to-int v1, v1

    move v1, v1

    .line 644600
    iget v2, v0, LX/3tF;->c:F

    iget v3, v0, LX/3tF;->c:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    div-float/2addr v2, v3

    float-to-int v2, v2

    move v0, v2

    .line 644601
    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, LX/3tG;->b(I)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    if-eqz v0, :cond_2

    invoke-virtual {p0}, LX/3tG;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 6

    .prologue
    .line 644583
    iget-boolean v0, p0, LX/3tG;->m:Z

    if-eqz v0, :cond_0

    .line 644584
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3tG;->o:Z

    .line 644585
    :goto_0
    return-void

    .line 644586
    :cond_0
    iget-object v0, p0, LX/3tG;->a:LX/3tF;

    .line 644587
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v1

    .line 644588
    iget-wide v3, v0, LX/3tF;->e:J

    sub-long v3, v1, v3

    long-to-int v3, v3

    const/4 v4, 0x0

    iget v5, v0, LX/3tF;->b:I

    .line 644589
    if-le v3, v5, :cond_1

    .line 644590
    :goto_1
    move v3, v5

    .line 644591
    iput v3, v0, LX/3tF;->k:I

    .line 644592
    invoke-static {v0, v1, v2}, LX/3tF;->a(LX/3tF;J)F

    move-result v3

    iput v3, v0, LX/3tF;->j:F

    .line 644593
    iput-wide v1, v0, LX/3tF;->i:J

    .line 644594
    goto :goto_0

    .line 644595
    :cond_1
    if-ge v3, v4, :cond_2

    move v5, v4

    .line 644596
    goto :goto_1

    :cond_2
    move v5, v3

    .line 644597
    goto :goto_1
.end method

.method public static f(LX/3tG;FF)F
    .locals 4

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 644573
    cmpl-float v2, p2, v0

    if-nez v2, :cond_1

    .line 644574
    :cond_0
    :goto_0
    return v0

    .line 644575
    :cond_1
    iget v2, p0, LX/3tG;->g:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 644576
    :pswitch_0
    cmpg-float v2, p1, p2

    if-gez v2, :cond_0

    .line 644577
    cmpl-float v2, p1, v0

    if-ltz v2, :cond_2

    .line 644578
    div-float v0, p1, p2

    sub-float v0, v1, v0

    goto :goto_0

    .line 644579
    :cond_2
    iget-boolean v2, p0, LX/3tG;->o:Z

    if-eqz v2, :cond_0

    iget v2, p0, LX/3tG;->g:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    move v0, v1

    .line 644580
    goto :goto_0

    .line 644581
    :pswitch_1
    cmpg-float v1, p1, v0

    if-gez v1, :cond_0

    .line 644582
    neg-float v0, p2

    div-float v0, p1, v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Z)LX/3tG;
    .locals 1

    .prologue
    .line 644569
    iget-boolean v0, p0, LX/3tG;->p:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 644570
    invoke-direct {p0}, LX/3tG;->d()V

    .line 644571
    :cond_0
    iput-boolean p1, p0, LX/3tG;->p:Z

    .line 644572
    return-object p0
.end method

.method public abstract a(I)V
.end method

.method public abstract a()Z
.end method

.method public abstract b(I)Z
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 644545
    iget-boolean v2, p0, LX/3tG;->p:Z

    if-nez v2, :cond_1

    .line 644546
    :cond_0
    :goto_0
    return v0

    .line 644547
    :cond_1
    invoke-static {p2}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v2

    .line 644548
    packed-switch v2, :pswitch_data_0

    .line 644549
    :cond_2
    :goto_1
    iget-boolean v2, p0, LX/3tG;->q:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, LX/3tG;->o:Z

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 644550
    :pswitch_0
    iput-boolean v1, p0, LX/3tG;->n:Z

    .line 644551
    iput-boolean v0, p0, LX/3tG;->l:Z

    .line 644552
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, LX/3tG;->c:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    invoke-direct {p0, v0, v2, v3, v4}, LX/3tG;->a(IFFF)F

    move-result v2

    .line 644553
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, LX/3tG;->c:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-direct {p0, v1, v3, v4, v5}, LX/3tG;->a(IFFF)F

    move-result v3

    .line 644554
    iget-object v4, p0, LX/3tG;->a:LX/3tF;

    .line 644555
    iput v2, v4, LX/3tF;->c:F

    .line 644556
    iput v3, v4, LX/3tF;->d:F

    .line 644557
    iget-boolean v2, p0, LX/3tG;->o:Z

    if-nez v2, :cond_2

    invoke-static {p0}, LX/3tG;->b$redex0(LX/3tG;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 644558
    const/4 v10, 0x1

    .line 644559
    iget-object v6, p0, LX/3tG;->d:Ljava/lang/Runnable;

    if-nez v6, :cond_3

    .line 644560
    new-instance v6, Landroid/support/v4/widget/AutoScrollHelper$ScrollAnimationRunnable;

    invoke-direct {v6, p0}, Landroid/support/v4/widget/AutoScrollHelper$ScrollAnimationRunnable;-><init>(LX/3tG;)V

    iput-object v6, p0, LX/3tG;->d:Ljava/lang/Runnable;

    .line 644561
    :cond_3
    iput-boolean v10, p0, LX/3tG;->o:Z

    .line 644562
    iput-boolean v10, p0, LX/3tG;->m:Z

    .line 644563
    iget-boolean v6, p0, LX/3tG;->l:Z

    if-nez v6, :cond_4

    iget v6, p0, LX/3tG;->h:I

    if-lez v6, :cond_4

    .line 644564
    iget-object v6, p0, LX/3tG;->c:Landroid/view/View;

    iget-object v7, p0, LX/3tG;->d:Ljava/lang/Runnable;

    iget v8, p0, LX/3tG;->h:I

    int-to-long v8, v8

    invoke-static {v6, v7, v8, v9}, LX/0vv;->a(Landroid/view/View;Ljava/lang/Runnable;J)V

    .line 644565
    :goto_2
    iput-boolean v10, p0, LX/3tG;->l:Z

    .line 644566
    goto :goto_1

    .line 644567
    :pswitch_2
    invoke-direct {p0}, LX/3tG;->d()V

    goto :goto_1

    .line 644568
    :cond_4
    iget-object v6, p0, LX/3tG;->d:Ljava/lang/Runnable;

    invoke-interface {v6}, Ljava/lang/Runnable;->run()V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
