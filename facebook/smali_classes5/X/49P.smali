.class public final enum LX/49P;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/49P;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/49P;

.field public static final enum COMMENT:LX/49P;

.field public static final enum SPAWN:LX/49P;

.field public static final enum START:LX/49P;

.field public static final enum START_ASYNC:LX/49P;

.field public static final enum STOP:LX/49P;

.field public static final enum STOP_ASYNC:LX/49P;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 674453
    new-instance v0, LX/49P;

    const-string v1, "START"

    invoke-direct {v0, v1, v3}, LX/49P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/49P;->START:LX/49P;

    .line 674454
    new-instance v0, LX/49P;

    const-string v1, "STOP"

    invoke-direct {v0, v1, v4}, LX/49P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/49P;->STOP:LX/49P;

    .line 674455
    new-instance v0, LX/49P;

    const-string v1, "START_ASYNC"

    invoke-direct {v0, v1, v5}, LX/49P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/49P;->START_ASYNC:LX/49P;

    .line 674456
    new-instance v0, LX/49P;

    const-string v1, "STOP_ASYNC"

    invoke-direct {v0, v1, v6}, LX/49P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/49P;->STOP_ASYNC:LX/49P;

    .line 674457
    new-instance v0, LX/49P;

    const-string v1, "COMMENT"

    invoke-direct {v0, v1, v7}, LX/49P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/49P;->COMMENT:LX/49P;

    .line 674458
    new-instance v0, LX/49P;

    const-string v1, "SPAWN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/49P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/49P;->SPAWN:LX/49P;

    .line 674459
    const/4 v0, 0x6

    new-array v0, v0, [LX/49P;

    sget-object v1, LX/49P;->START:LX/49P;

    aput-object v1, v0, v3

    sget-object v1, LX/49P;->STOP:LX/49P;

    aput-object v1, v0, v4

    sget-object v1, LX/49P;->START_ASYNC:LX/49P;

    aput-object v1, v0, v5

    sget-object v1, LX/49P;->STOP_ASYNC:LX/49P;

    aput-object v1, v0, v6

    sget-object v1, LX/49P;->COMMENT:LX/49P;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/49P;->SPAWN:LX/49P;

    aput-object v2, v0, v1

    sput-object v0, LX/49P;->$VALUES:[LX/49P;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 674451
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/49P;
    .locals 1

    .prologue
    .line 674452
    const-class v0, LX/49P;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/49P;

    return-object v0
.end method

.method public static values()[LX/49P;
    .locals 1

    .prologue
    .line 674450
    sget-object v0, LX/49P;->$VALUES:[LX/49P;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/49P;

    return-object v0
.end method


# virtual methods
.method public final isStartLikeEvent()Z
    .locals 1

    .prologue
    .line 674449
    sget-object v0, LX/49P;->START:LX/49P;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/49P;->START_ASYNC:LX/49P;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isStopLikeEvent()Z
    .locals 1

    .prologue
    .line 674448
    sget-object v0, LX/49P;->STOP:LX/49P;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/49P;->STOP_ASYNC:LX/49P;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
