.class public abstract LX/4of;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:J

.field private b:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 810268
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 810269
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/4of;->b:J

    .line 810270
    const-wide/16 v0, 0x1f4

    iput-wide v0, p0, LX/4of;->a:J

    .line 810271
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v0, 0x1

    const v1, -0x5f777ba8

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 810272
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 810273
    iget-wide v4, p0, LX/4of;->b:J

    sub-long v4, v2, v4

    .line 810274
    iget-wide v6, p0, LX/4of;->a:J

    cmp-long v1, v4, v6

    if-lez v1, :cond_0

    .line 810275
    invoke-virtual {p0}, LX/4of;->a()V

    .line 810276
    :cond_0
    iput-wide v2, p0, LX/4of;->b:J

    .line 810277
    const v1, -0x6b43047a

    invoke-static {v8, v8, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
