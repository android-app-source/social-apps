.class public final LX/3cC;
.super LX/1Cz;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 613302
    invoke-direct {p0}, LX/1Cz;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/view/View;
    .locals 5

    .prologue
    .line 613303
    new-instance v0, Lcom/facebook/maps/FbStaticMapView;

    invoke-direct {v0, p1}, Lcom/facebook/maps/FbStaticMapView;-><init>(Landroid/content/Context;)V

    .line 613304
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1d29

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/facebook/maps/FbStaticMapView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 613305
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020e90

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3BP;->setCenteredMapPinDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 613306
    return-object v0
.end method
