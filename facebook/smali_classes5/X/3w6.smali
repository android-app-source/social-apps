.class public LX/3w6;
.super Landroid/widget/AutoCompleteTextView;
.source ""

# interfaces
.implements LX/3s1;


# static fields
.field private static final a:[I


# instance fields
.field private b:LX/3wA;

.field private c:LX/3w8;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 654403
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/3w6;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x10100d4
        0x1010176
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 654404
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3w6;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 654405
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 654364
    const v0, 0x101006b

    invoke-direct {p0, p1, p2, v0}, LX/3w6;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 654365
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 654388
    instance-of v0, p1, LX/3w7;

    if-nez v0, :cond_0

    .line 654389
    new-instance v0, LX/3w7;

    invoke-direct {v0, p1}, LX/3w7;-><init>(Landroid/content/Context;)V

    move-object p1, v0

    .line 654390
    :cond_0
    move-object v0, p1

    .line 654391
    invoke-direct {p0, v0, p2, p3}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 654392
    sget-boolean v0, LX/3wA;->a:Z

    if-eqz v0, :cond_3

    .line 654393
    invoke-virtual {p0}, LX/3w6;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/3w6;->a:[I

    invoke-static {v0, p2, v1, p3, v3}, LX/3wC;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)LX/3wC;

    move-result-object v0

    .line 654394
    invoke-virtual {v0}, LX/3wC;->c()LX/3wA;

    move-result-object v1

    iput-object v1, p0, LX/3w6;->b:LX/3wA;

    .line 654395
    invoke-virtual {v0, v3}, LX/3wC;->d(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 654396
    iget-object v1, p0, LX/3w6;->b:LX/3wA;

    const/4 v2, -0x1

    invoke-virtual {v0, v3, v2}, LX/3wC;->f(II)I

    move-result v2

    .line 654397
    sget-object v3, LX/3wA;->h:[I

    invoke-static {v3, v2}, LX/3wA;->a([II)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {v1, v2}, LX/3wA;->d(LX/3wA;I)Landroid/content/res/ColorStateList;

    move-result-object v3

    :goto_0
    move-object v1, v3

    .line 654398
    invoke-virtual {p0, v1}, LX/3w6;->setSupportBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 654399
    :cond_1
    invoke-virtual {v0, v4}, LX/3wC;->d(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 654400
    invoke-virtual {v0, v4}, LX/3wC;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/3w6;->setDropDownBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 654401
    :cond_2
    invoke-virtual {v0}, LX/3wC;->b()V

    .line 654402
    :cond_3
    return-void

    :cond_4
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private a()V
    .locals 5

    .prologue
    .line 654380
    invoke-virtual {p0}, LX/3w6;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3w6;->c:LX/3w8;

    if-eqz v0, :cond_0

    .line 654381
    iget-object v0, p0, LX/3w6;->c:LX/3w8;

    .line 654382
    invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 654383
    iget-object v1, v0, LX/3w8;->a:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_2

    .line 654384
    iget-object v1, v0, LX/3w8;->a:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Landroid/view/View;->getDrawableState()[I

    move-result-object v3

    iget-object v4, v0, LX/3w8;->a:Landroid/content/res/ColorStateList;

    invoke-virtual {v4}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v3

    iget-object v1, v0, LX/3w8;->b:Landroid/graphics/PorterDuff$Mode;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/3w8;->b:Landroid/graphics/PorterDuff$Mode;

    :goto_0
    invoke-static {v2, v3, v1}, LX/3wA;->a(Landroid/graphics/drawable/Drawable;ILandroid/graphics/PorterDuff$Mode;)V

    .line 654385
    :cond_0
    :goto_1
    return-void

    .line 654386
    :cond_1
    sget-object v1, LX/3wA;->b:Landroid/graphics/PorterDuff$Mode;

    goto :goto_0

    .line 654387
    :cond_2
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    goto :goto_1
.end method


# virtual methods
.method public final drawableStateChanged()V
    .locals 0

    .prologue
    .line 654406
    invoke-super {p0}, Landroid/widget/AutoCompleteTextView;->drawableStateChanged()V

    .line 654407
    invoke-direct {p0}, LX/3w6;->a()V

    .line 654408
    return-void
.end method

.method public getSupportBackgroundTintList()Landroid/content/res/ColorStateList;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 654379
    iget-object v0, p0, LX/3w6;->c:LX/3w8;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3w6;->c:LX/3w8;

    iget-object v0, v0, LX/3w8;->a:Landroid/content/res/ColorStateList;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSupportBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 654378
    iget-object v0, p0, LX/3w6;->c:LX/3w8;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3w6;->c:LX/3w8;

    iget-object v0, v0, LX/3w8;->b:Landroid/graphics/PorterDuff$Mode;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDropDownBackgroundResource(I)V
    .locals 1

    .prologue
    .line 654376
    iget-object v0, p0, LX/3w6;->b:LX/3wA;

    invoke-virtual {v0, p1}, LX/3wA;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3w6;->setDropDownBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 654377
    return-void
.end method

.method public setSupportBackgroundTintList(Landroid/content/res/ColorStateList;)V
    .locals 1
    .param p1    # Landroid/content/res/ColorStateList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 654371
    iget-object v0, p0, LX/3w6;->c:LX/3w8;

    if-nez v0, :cond_0

    .line 654372
    new-instance v0, LX/3w8;

    invoke-direct {v0}, LX/3w8;-><init>()V

    iput-object v0, p0, LX/3w6;->c:LX/3w8;

    .line 654373
    :cond_0
    iget-object v0, p0, LX/3w6;->c:LX/3w8;

    iput-object p1, v0, LX/3w8;->a:Landroid/content/res/ColorStateList;

    .line 654374
    invoke-direct {p0}, LX/3w6;->a()V

    .line 654375
    return-void
.end method

.method public setSupportBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1
    .param p1    # Landroid/graphics/PorterDuff$Mode;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 654366
    iget-object v0, p0, LX/3w6;->c:LX/3w8;

    if-nez v0, :cond_0

    .line 654367
    new-instance v0, LX/3w8;

    invoke-direct {v0}, LX/3w8;-><init>()V

    iput-object v0, p0, LX/3w6;->c:LX/3w8;

    .line 654368
    :cond_0
    iget-object v0, p0, LX/3w6;->c:LX/3w8;

    iput-object p1, v0, LX/3w8;->b:Landroid/graphics/PorterDuff$Mode;

    .line 654369
    invoke-direct {p0}, LX/3w6;->a()V

    .line 654370
    return-void
.end method
