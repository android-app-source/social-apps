.class public LX/4bi;
.super Lorg/apache/http/conn/ssl/SSLSocketFactory;
.source ""


# static fields
.field private static final a:[Ljava/lang/String;

.field public static final b:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

.field private static d:Ljavax/net/ssl/TrustManager;


# instance fields
.field private c:Ljavax/net/ssl/SSLContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 794051
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "facebook.com"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "beta.facebook.com"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "latest.facebook.com"

    aput-object v2, v0, v1

    sput-object v0, LX/4bi;->a:[Ljava/lang/String;

    .line 794052
    new-instance v0, LX/4bh;

    sget-object v1, LX/4bi;->a:[Ljava/lang/String;

    sget-object v2, Lorg/apache/http/conn/ssl/SSLSocketFactory;->BROWSER_COMPATIBLE_HOSTNAME_VERIFIER:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    invoke-direct {v0, v1, v2}, LX/4bh;-><init>([Ljava/lang/String;Lorg/apache/http/conn/ssl/X509HostnameVerifier;)V

    sput-object v0, LX/4bi;->b:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    .line 794053
    new-instance v0, LX/4bg;

    invoke-direct {v0}, LX/4bg;-><init>()V

    sput-object v0, LX/4bi;->d:Ljavax/net/ssl/TrustManager;

    return-void
.end method

.method public constructor <init>(Ljava/security/KeyStore;)V
    .locals 5

    .prologue
    .line 794054
    invoke-direct {p0, p1}, Lorg/apache/http/conn/ssl/SSLSocketFactory;-><init>(Ljava/security/KeyStore;)V

    .line 794055
    const-string v0, "TLS"

    invoke-static {v0}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v0

    iput-object v0, p0, LX/4bi;->c:Ljavax/net/ssl/SSLContext;

    .line 794056
    iget-object v0, p0, LX/4bi;->c:Ljavax/net/ssl/SSLContext;

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljavax/net/ssl/TrustManager;

    const/4 v3, 0x0

    sget-object v4, LX/4bi;->d:Ljavax/net/ssl/TrustManager;

    aput-object v4, v2, v3

    new-instance v3, Ljava/security/SecureRandom;

    invoke-direct {v3}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v0, v1, v2, v3}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 794057
    return-void
.end method


# virtual methods
.method public final createSocket()Ljava/net/Socket;
    .locals 1

    .prologue
    .line 794058
    iget-object v0, p0, LX/4bi;->c:Ljavax/net/ssl/SSLContext;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocketFactory;->createSocket()Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method public final createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;
    .locals 2

    .prologue
    .line 794059
    iget-object v0, p0, LX/4bi;->c:Ljavax/net/ssl/SSLContext;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    .line 794060
    invoke-virtual {p0}, LX/4bi;->getHostnameVerifier()Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    move-result-object v1

    invoke-interface {v1, p2, v0}, Lorg/apache/http/conn/ssl/X509HostnameVerifier;->verify(Ljava/lang/String;Ljavax/net/ssl/SSLSocket;)V

    .line 794061
    return-object v0
.end method
