.class public LX/3iF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ph;",
        ":",
        "LX/1Pq;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/3iK;

.field public final c:LX/3iH;

.field public final d:LX/3Af;

.field public final e:Lcom/facebook/user/model/User;

.field public final f:LX/0SI;

.field public final g:LX/0pf;

.field public final h:LX/1Ck;

.field public final i:LX/03V;

.field public final j:Landroid/content/Context;

.field public k:Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;

.field public l:LX/4At;

.field public m:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/189;

.field public o:LX/1Ps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 628986
    const-class v0, LX/3iF;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3iF;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/3iG;LX/0Or;LX/0Or;LX/3iH;LX/0SI;LX/0pf;LX/189;Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;LX/1Ck;LX/03V;Landroid/content/Context;)V
    .locals 1
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3iG;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/3iH;",
            "LX/0SI;",
            "LX/0pf;",
            "LX/189;",
            "Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;",
            "LX/1Ck;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 629013
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 629014
    sget-object v0, Lcom/facebook/feedback/ui/FeedbackControllerParams;->a:Lcom/facebook/feedback/ui/FeedbackControllerParams;

    invoke-virtual {p1, v0}, LX/3iG;->a(Lcom/facebook/feedback/ui/FeedbackControllerParams;)LX/3iK;

    move-result-object v0

    iput-object v0, p0, LX/3iF;->b:LX/3iK;

    .line 629015
    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Af;

    iput-object v0, p0, LX/3iF;->d:LX/3Af;

    .line 629016
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    iput-object v0, p0, LX/3iF;->e:Lcom/facebook/user/model/User;

    .line 629017
    iput-object p4, p0, LX/3iF;->c:LX/3iH;

    .line 629018
    iput-object p5, p0, LX/3iF;->f:LX/0SI;

    .line 629019
    iput-object p6, p0, LX/3iF;->g:LX/0pf;

    .line 629020
    iput-object p7, p0, LX/3iF;->n:LX/189;

    .line 629021
    iput-object p8, p0, LX/3iF;->k:Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;

    .line 629022
    iput-object p9, p0, LX/3iF;->h:LX/1Ck;

    .line 629023
    iput-object p10, p0, LX/3iF;->i:LX/03V;

    .line 629024
    iput-object p11, p0, LX/3iF;->j:Landroid/content/Context;

    .line 629025
    return-void
.end method

.method public static a$redex0(LX/3iF;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 4

    .prologue
    .line 628991
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 628992
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-static {v0}, LX/6X4;->a(Lcom/facebook/graphql/model/GraphQLPage;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 628993
    :goto_0
    iget-object v1, p0, LX/3iF;->g:LX/0pf;

    invoke-virtual {v1, p1}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v1

    .line 628994
    iput-object v0, v1, LX/1g0;->p:Lcom/facebook/graphql/model/GraphQLActor;

    .line 628995
    iput-object p2, v1, LX/1g0;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 628996
    iget-object v0, p0, LX/3iF;->g:LX/0pf;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    .line 628997
    iget-object v2, v0, LX/0pf;->b:Ljava/util/Map;

    invoke-static {p1}, LX/0pf;->b(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 628998
    return-void

    .line 628999
    :cond_0
    new-instance v0, LX/3dL;

    invoke-direct {v0}, LX/3dL;-><init>()V

    iget-object v1, p0, LX/3iF;->e:Lcom/facebook/user/model/User;

    .line 629000
    iget-object v2, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v2

    .line 629001
    iput-object v1, v0, LX/3dL;->E:Ljava/lang/String;

    .line 629002
    move-object v0, v0

    .line 629003
    const-string v1, "%s %s"

    iget-object v2, p0, LX/3iF;->e:Lcom/facebook/user/model/User;

    invoke-virtual {v2}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/3iF;->e:Lcom/facebook/user/model/User;

    invoke-virtual {v3}, Lcom/facebook/user/model/User;->h()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 629004
    iput-object v1, v0, LX/3dL;->ag:Ljava/lang/String;

    .line 629005
    move-object v0, v0

    .line 629006
    new-instance v1, LX/2dc;

    invoke-direct {v1}, LX/2dc;-><init>()V

    iget-object v2, p0, LX/3iF;->e:Lcom/facebook/user/model/User;

    invoke-virtual {v2}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v2

    .line 629007
    iput-object v2, v1, LX/2dc;->h:Ljava/lang/String;

    .line 629008
    move-object v1, v1

    .line 629009
    invoke-virtual {v1}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 629010
    iput-object v1, v0, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 629011
    move-object v0, v0

    .line 629012
    invoke-virtual {v0}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    goto :goto_0
.end method

.method public static a$redex0(LX/3iF;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 628987
    iget-object v0, p0, LX/3iF;->j:Landroid/content/Context;

    const v1, 0x7f08003a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 628988
    iget-object v0, p0, LX/3iF;->i:LX/03V;

    sget-object v1, LX/3iF;->a:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 628989
    invoke-static {p0}, LX/3iF;->e$redex0(LX/3iF;)V

    .line 628990
    return-void
.end method

.method public static b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 629026
    if-eqz p0, :cond_0

    .line 629027
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 629028
    if-eqz v0, :cond_0

    .line 629029
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 629030
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 629031
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 629032
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b$redex0(LX/3iF;)V
    .locals 2

    .prologue
    .line 628954
    iget-object v0, p0, LX/3iF;->d:LX/3Af;

    iget-object v1, p0, LX/3iF;->k:Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;

    invoke-virtual {v0, v1}, LX/3Af;->a(LX/1OM;)V

    .line 628955
    iget-object v0, p0, LX/3iF;->d:LX/3Af;

    invoke-virtual {v0}, LX/3Af;->show()V

    .line 628956
    iget-object v0, p0, LX/3iF;->l:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 628957
    return-void
.end method

.method public static d$redex0(LX/3iF;)V
    .locals 3

    .prologue
    .line 628958
    iget-object v1, p0, LX/3iF;->g:LX/0pf;

    iget-object v0, p0, LX/3iF;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 628959
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 628960
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v1, v0}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v0

    .line 628961
    if-eqz v0, :cond_1

    .line 628962
    iget-object v1, v0, LX/1g0;->p:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v1, v1

    .line 628963
    if-eqz v1, :cond_1

    .line 628964
    iget-object v1, v0, LX/1g0;->p:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v1, v1

    .line 628965
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 628966
    iget-object v1, p0, LX/3iF;->k:Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;

    .line 628967
    iget-object v2, v0, LX/1g0;->p:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v0, v2

    .line 628968
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;->a(Ljava/lang/String;)V

    .line 628969
    :cond_0
    :goto_0
    return-void

    .line 628970
    :cond_1
    iget-object v0, p0, LX/3iF;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/3iF;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 628971
    iget-object v0, p0, LX/3iF;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 628972
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 628973
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 628974
    iget-object v1, p0, LX/3iF;->f:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/3iF;->f:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 628975
    iget-boolean v2, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v1, v2

    .line 628976
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    if-nez v1, :cond_3

    :cond_2
    iget-object v0, p0, LX/3iF;->e:Lcom/facebook/user/model/User;

    .line 628977
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 628978
    :goto_1
    iget-object v1, p0, LX/3iF;->k:Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;

    invoke-virtual {v1, v0}, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 628979
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static e$redex0(LX/3iF;)V
    .locals 2

    .prologue
    .line 628980
    iget-object v0, p0, LX/3iF;->k:Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;

    if-eqz v0, :cond_0

    .line 628981
    iget-object v0, p0, LX/3iF;->k:Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;

    const/4 v1, 0x0

    .line 628982
    iput-object v1, v0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;->c:LX/CDo;

    .line 628983
    :cond_0
    iget-object v0, p0, LX/3iF;->l:LX/4At;

    if-eqz v0, :cond_1

    .line 628984
    iget-object v0, p0, LX/3iF;->l:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 628985
    :cond_1
    return-void
.end method
