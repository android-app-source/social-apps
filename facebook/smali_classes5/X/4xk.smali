.class public final LX/4xk;
.super LX/4xj;
.source ""

# interfaces
.implements LX/4xi;
.implements LX/0vX;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/4xj",
        "<TK;TV;>;",
        "Lcom/google/common/collect/FilteredSetMultimap",
        "<TK;TV;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0vX;LX/0Rl;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0vX",
            "<TK;TV;>;",
            "LX/0Rl",
            "<-",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 821287
    invoke-direct {p0, p1, p2}, LX/4xj;-><init>(LX/0Xu;LX/0Rl;)V

    .line 821288
    return-void
.end method


# virtual methods
.method public final synthetic a()LX/0Xu;
    .locals 1

    .prologue
    .line 821286
    invoke-virtual {p0}, LX/4xk;->d()LX/0vX;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/util/Set",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 821285
    invoke-super {p0, p1}, LX/4xj;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/Set",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 821278
    invoke-super {p0, p1}, LX/4xj;->d(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 821284
    invoke-virtual {p0, p1}, LX/4xk;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/0vX;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0vX",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 821283
    iget-object v0, p0, LX/4xj;->a:LX/0Xu;

    check-cast v0, LX/0vX;

    return-object v0
.end method

.method public final synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 821282
    invoke-virtual {p0, p1}, LX/4xk;->b(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 821281
    invoke-virtual {p0}, LX/4xk;->t()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final o()Ljava/util/Collection;
    .locals 2

    .prologue
    .line 821280
    invoke-virtual {p0}, LX/4xk;->d()LX/0vX;

    move-result-object v0

    invoke-interface {v0}, LX/0vX;->t()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0}, LX/4xj;->c()LX/0Rl;

    move-result-object v1

    invoke-static {v0, v1}, LX/0RA;->a(Ljava/util/Set;LX/0Rl;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final t()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 821279
    invoke-super {p0}, LX/4xj;->k()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method
