.class public final LX/4qF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/320;

.field public final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/32s;",
            ">;"
        }
    .end annotation
.end field

.field public final c:I

.field public final d:[Ljava/lang/Object;

.field public final e:[LX/32s;


# direct methods
.method private constructor <init>(LX/320;[LX/32s;[Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 813222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 813223
    iput-object p1, p0, LX/4qF;->a:LX/320;

    .line 813224
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/4qF;->b:Ljava/util/HashMap;

    .line 813225
    const/4 v1, 0x0

    .line 813226
    array-length v2, p2

    .line 813227
    iput v2, p0, LX/4qF;->c:I

    .line 813228
    const/4 v0, 0x0

    move v6, v0

    move-object v0, v1

    move v1, v6

    :goto_0
    if-ge v1, v2, :cond_2

    .line 813229
    aget-object v3, p2, v1

    .line 813230
    iget-object v4, p0, LX/4qF;->b:Ljava/util/HashMap;

    .line 813231
    iget-object v5, v3, LX/32s;->_propName:Ljava/lang/String;

    move-object v5, v5

    .line 813232
    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 813233
    invoke-virtual {v3}, LX/32s;->d()Ljava/lang/Object;

    move-result-object v4

    .line 813234
    if-eqz v4, :cond_1

    .line 813235
    if-nez v0, :cond_0

    .line 813236
    new-array v0, v2, [LX/32s;

    .line 813237
    :cond_0
    aput-object v3, v0, v1

    .line 813238
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 813239
    :cond_2
    iput-object p3, p0, LX/4qF;->d:[Ljava/lang/Object;

    .line 813240
    iput-object v0, p0, LX/4qF;->e:[LX/32s;

    .line 813241
    return-void
.end method

.method public static a(LX/0n3;LX/320;[LX/32s;)LX/4qF;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 813251
    array-length v5, p2

    .line 813252
    new-array v6, v5, [LX/32s;

    .line 813253
    const/4 v0, 0x0

    move v4, v0

    move-object v0, v3

    :goto_0
    if-ge v4, v5, :cond_4

    .line 813254
    aget-object v1, p2, v4

    .line 813255
    invoke-virtual {v1}, LX/32s;->j()Z

    move-result v2

    if-nez v2, :cond_0

    .line 813256
    invoke-virtual {v1}, LX/32s;->a()LX/0lJ;

    move-result-object v2

    invoke-virtual {p0, v2, v1}, LX/0n3;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/32s;->b(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/32s;

    move-result-object v1

    .line 813257
    :cond_0
    aput-object v1, v6, v4

    .line 813258
    invoke-virtual {v1}, LX/32s;->l()Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v2

    .line 813259
    if-nez v2, :cond_3

    move-object v2, v3

    .line 813260
    :goto_1
    if-nez v2, :cond_5

    invoke-virtual {v1}, LX/32s;->a()LX/0lJ;

    move-result-object v7

    invoke-virtual {v7}, LX/0lJ;->j()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 813261
    invoke-virtual {v1}, LX/32s;->a()LX/0lJ;

    move-result-object v1

    .line 813262
    iget-object v2, v1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v1, v2

    .line 813263
    invoke-static {v1}, LX/1Xw;->f(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    .line 813264
    :goto_2
    if-eqz v1, :cond_2

    .line 813265
    if-nez v0, :cond_1

    .line 813266
    new-array v0, v5, [Ljava/lang/Object;

    .line 813267
    :cond_1
    aput-object v1, v0, v4

    .line 813268
    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    .line 813269
    :cond_3
    invoke-virtual {v2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->getNullValue()Ljava/lang/Object;

    move-result-object v2

    goto :goto_1

    .line 813270
    :cond_4
    new-instance v1, LX/4qF;

    invoke-direct {v1, p1, v6, v0}, LX/4qF;-><init>(LX/320;[LX/32s;[Ljava/lang/Object;)V

    return-object v1

    :cond_5
    move-object v1, v2

    goto :goto_2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/32s;
    .locals 1

    .prologue
    .line 813250
    iget-object v0, p0, LX/4qF;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/32s;

    return-object v0
.end method

.method public final a(LX/15w;LX/0n3;LX/4qD;)LX/4qL;
    .locals 2

    .prologue
    .line 813271
    new-instance v0, LX/4qL;

    iget v1, p0, LX/4qF;->c:I

    invoke-direct {v0, p1, p2, v1, p3}, LX/4qL;-><init>(LX/15w;LX/0n3;ILX/4qD;)V

    .line 813272
    iget-object v1, p0, LX/4qF;->e:[LX/32s;

    if-eqz v1, :cond_0

    .line 813273
    iget-object v1, p0, LX/4qF;->e:[LX/32s;

    invoke-virtual {v0, v1}, LX/4qL;->a([LX/32s;)V

    .line 813274
    :cond_0
    return-object v0
.end method

.method public final a(LX/0n3;LX/4qL;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 813243
    iget-object v0, p0, LX/4qF;->a:LX/320;

    iget-object v1, p0, LX/4qF;->d:[Ljava/lang/Object;

    invoke-virtual {p2, v1}, LX/4qL;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/320;->a([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 813244
    invoke-virtual {p2, p1, v0}, LX/4qL;->a(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 813245
    iget-object v0, p2, LX/4qL;->f:LX/4qH;

    move-object v0, v0

    .line 813246
    :goto_0
    if-eqz v0, :cond_0

    .line 813247
    invoke-virtual {v0, v1}, LX/4qH;->a(Ljava/lang/Object;)V

    .line 813248
    iget-object v0, v0, LX/4qH;->a:LX/4qH;

    goto :goto_0

    .line 813249
    :cond_0
    return-object v1
.end method

.method public final a()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "LX/32s;",
            ">;"
        }
    .end annotation

    .prologue
    .line 813242
    iget-object v0, p0, LX/4qF;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
