.class public LX/3m9;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3mC;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/3m9",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/3mC;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 635414
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 635415
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/3m9;->b:LX/0Zi;

    .line 635416
    iput-object p1, p0, LX/3m9;->a:LX/0Ot;

    .line 635417
    return-void
.end method

.method public static a(LX/0QB;)LX/3m9;
    .locals 4

    .prologue
    .line 635418
    const-class v1, LX/3m9;

    monitor-enter v1

    .line 635419
    :try_start_0
    sget-object v0, LX/3m9;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 635420
    sput-object v2, LX/3m9;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 635421
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 635422
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 635423
    new-instance v3, LX/3m9;

    const/16 p0, 0x89f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/3m9;-><init>(LX/0Ot;)V

    .line 635424
    move-object v0, v3

    .line 635425
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 635426
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3m9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 635427
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 635428
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 635429
    const v0, 0x4ae82fa6    # 7608275.0f

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 635430
    check-cast p2, LX/3mA;

    .line 635431
    iget-object v0, p0, LX/3m9;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3mC;

    iget-object v1, p2, LX/3mA;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/3mA;->b:LX/1Po;

    .line 635432
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 p2, 0x1

    .line 635433
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 635434
    check-cast v4, Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;

    invoke-interface {v4}, Lcom/facebook/graphql/model/ItemListFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    .line 635435
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const p0, 0x7f0a015d

    invoke-virtual {v4, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    const p0, 0x7f0b0050

    invoke-virtual {v4, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/1ne;->t(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 p0, 0x6

    const p2, 0x7f0b0917

    invoke-interface {v4, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    const/4 p0, 0x7

    const p2, 0x7f0b08fe

    invoke-interface {v4, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v4, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v4

    move-object v4, v4

    .line 635436
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/3mC;->a:LX/3mD;

    const/4 p0, 0x0

    .line 635437
    new-instance p2, LX/3mI;

    invoke-direct {p2, v4}, LX/3mI;-><init>(LX/3mD;)V

    .line 635438
    iget-object v0, v4, LX/3mD;->b:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3mJ;

    .line 635439
    if-nez v0, :cond_0

    .line 635440
    new-instance v0, LX/3mJ;

    invoke-direct {v0, v4}, LX/3mJ;-><init>(LX/3mD;)V

    .line 635441
    :cond_0
    invoke-static {v0, p1, p0, p0, p2}, LX/3mJ;->a$redex0(LX/3mJ;LX/1De;IILX/3mI;)V

    .line 635442
    move-object p2, v0

    .line 635443
    move-object p0, p2

    .line 635444
    move-object v4, p0

    .line 635445
    iget-object p0, v4, LX/3mJ;->a:LX/3mI;

    iput-object v2, p0, LX/3mI;->a:LX/1Po;

    .line 635446
    iget-object p0, v4, LX/3mJ;->e:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 635447
    move-object v4, v4

    .line 635448
    iget-object p0, v4, LX/3mJ;->a:LX/3mI;

    iput-object v1, p0, LX/3mI;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 635449
    iget-object p0, v4, LX/3mJ;->e:Ljava/util/BitSet;

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 635450
    move-object v4, v4

    .line 635451
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/3mm;->c(LX/1De;)LX/3mp;

    move-result-object p0

    .line 635452
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 635453
    check-cast v3, Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;

    invoke-static {v3}, LX/3mQ;->a(Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;)Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v3

    if-eqz v3, :cond_1

    const v3, 0x7f08107b

    :goto_0
    invoke-virtual {p0, v3}, LX/3mp;->h(I)LX/3mp;

    move-result-object v3

    .line 635454
    const p0, 0x4ae82fa6    # 7608275.0f

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 635455
    invoke-virtual {v3, p0}, LX/3mp;->a(LX/1dQ;)LX/3mp;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 635456
    return-object v0

    :cond_1
    const v3, 0x7f08107a

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 635457
    invoke-static {}, LX/1dS;->b()V

    .line 635458
    iget v0, p1, LX/1dQ;->b:I

    .line 635459
    packed-switch v0, :pswitch_data_0

    .line 635460
    :goto_0
    return-object v2

    .line 635461
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 635462
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 635463
    check-cast v1, LX/3mA;

    .line 635464
    iget-object v3, p0, LX/3m9;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3mC;

    iget-object v4, v1, LX/3mA;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v5, v1, LX/3mA;->b:LX/1Po;

    .line 635465
    iget-object p1, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p1

    .line 635466
    check-cast p1, Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;

    invoke-static {p1}, LX/3mQ;->a(Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;)Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object p1

    .line 635467
    if-eqz p1, :cond_0

    .line 635468
    iget-object p2, v3, LX/3mC;->b:LX/3mE;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroup;->v()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p0, v1, p1}, LX/3mE;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 635469
    :goto_1
    goto :goto_0

    .line 635470
    :cond_0
    iget-object p1, v3, LX/3mC;->b:LX/3mE;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    .line 635471
    invoke-interface {v5}, LX/1Po;->c()LX/1PT;

    move-result-object p0

    .line 635472
    if-eqz p0, :cond_2

    invoke-interface {p0}, LX/1PT;->a()LX/1Qt;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, LX/1PT;->a()LX/1Qt;

    move-result-object p0

    invoke-virtual {p0}, LX/1Qt;->name()Ljava/lang/String;

    move-result-object p0

    .line 635473
    :goto_2
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "gysj_see_all_click"

    invoke-direct {v1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "native_newsfeed"

    .line 635474
    iput-object v3, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 635475
    move-object v1, v1

    .line 635476
    if-eqz p0, :cond_1

    .line 635477
    const-string v3, "feed_name"

    invoke-virtual {v1, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 635478
    :cond_1
    move-object p0, v1

    .line 635479
    iget-object v1, p1, LX/3mE;->e:LX/0Zb;

    invoke-interface {v1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 635480
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object p0, p1, LX/3mE;->h:LX/0Or;

    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/ComponentName;

    invoke-virtual {v1, p0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object p0

    .line 635481
    const-string v1, "target_fragment"

    sget-object v3, LX/0cQ;->GROUPS_DISCOVER_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 635482
    const-string v1, "extra_navigation_source"

    const-string v3, "gysj"

    invoke-virtual {p0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 635483
    iget-object v1, p1, LX/3mE;->i:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, p0, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 635484
    goto :goto_1

    .line 635485
    :cond_2
    const/4 p0, 0x0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x4ae82fa6
        :pswitch_0
    .end packed-switch
.end method
