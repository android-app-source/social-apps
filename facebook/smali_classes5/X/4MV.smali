.class public LX/4MV;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 688352
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 38

    .prologue
    .line 688353
    const/16 v33, 0x0

    .line 688354
    const/16 v32, 0x0

    .line 688355
    const/16 v31, 0x0

    .line 688356
    const/16 v30, 0x0

    .line 688357
    const-wide/16 v28, 0x0

    .line 688358
    const/16 v27, 0x0

    .line 688359
    const/16 v26, 0x0

    .line 688360
    const/16 v25, 0x0

    .line 688361
    const/16 v24, 0x0

    .line 688362
    const/16 v23, 0x0

    .line 688363
    const/16 v22, 0x0

    .line 688364
    const/16 v21, 0x0

    .line 688365
    const/16 v20, 0x0

    .line 688366
    const/16 v19, 0x0

    .line 688367
    const/16 v18, 0x0

    .line 688368
    const/16 v17, 0x0

    .line 688369
    const/16 v16, 0x0

    .line 688370
    const/4 v15, 0x0

    .line 688371
    const/4 v14, 0x0

    .line 688372
    const/4 v13, 0x0

    .line 688373
    const/4 v12, 0x0

    .line 688374
    const/4 v11, 0x0

    .line 688375
    const/4 v10, 0x0

    .line 688376
    const/4 v9, 0x0

    .line 688377
    const/4 v8, 0x0

    .line 688378
    const/4 v7, 0x0

    .line 688379
    const/4 v6, 0x0

    .line 688380
    const/4 v5, 0x0

    .line 688381
    const/4 v4, 0x0

    .line 688382
    const/4 v3, 0x0

    .line 688383
    const/4 v2, 0x0

    .line 688384
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v34

    sget-object v35, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    if-eq v0, v1, :cond_21

    .line 688385
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 688386
    const/4 v2, 0x0

    .line 688387
    :goto_0
    return v2

    .line 688388
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_1e

    .line 688389
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 688390
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 688391
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v36, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v36

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 688392
    const-string v6, "all_share_stories"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 688393
    invoke-static/range {p0 .. p1}, LX/4Kq;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v35, v2

    goto :goto_1

    .line 688394
    :cond_1
    const-string v6, "android_urls"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 688395
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v34, v2

    goto :goto_1

    .line 688396
    :cond_2
    const-string v6, "application"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 688397
    invoke-static/range {p0 .. p1}, LX/2uk;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v33, v2

    goto :goto_1

    .line 688398
    :cond_3
    const-string v6, "canonical"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 688399
    invoke-static/range {p0 .. p1}, LX/4MV;->a(LX/15w;LX/186;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 688400
    :cond_4
    const-string v6, "creation_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 688401
    const/4 v2, 0x1

    .line 688402
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 688403
    :cond_5
    const-string v6, "emotional_analysis"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 688404
    invoke-static/range {p0 .. p1}, LX/4Lr;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v32, v2

    goto :goto_1

    .line 688405
    :cond_6
    const-string v6, "external_url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 688406
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v31, v2

    goto/16 :goto_1

    .line 688407
    :cond_7
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 688408
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v30, v2

    goto/16 :goto_1

    .line 688409
    :cond_8
    const-string v6, "instant_article"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 688410
    invoke-static/range {p0 .. p1}, LX/4Og;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v29, v2

    goto/16 :goto_1

    .line 688411
    :cond_9
    const-string v6, "link_media"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 688412
    invoke-static/range {p0 .. p1}, LX/2at;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v28, v2

    goto/16 :goto_1

    .line 688413
    :cond_a
    const-string v6, "messenger_content_subscription_option"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 688414
    invoke-static/range {p0 .. p1}, LX/4PW;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v27, v2

    goto/16 :goto_1

    .line 688415
    :cond_b
    const-string v6, "name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 688416
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v26, v2

    goto/16 :goto_1

    .line 688417
    :cond_c
    const-string v6, "name_search_tokens"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 688418
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v25, v2

    goto/16 :goto_1

    .line 688419
    :cond_d
    const-string v6, "open_graph_node"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 688420
    invoke-static/range {p0 .. p1}, LX/2bR;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 688421
    :cond_e
    const-string v6, "phrases_analysis"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 688422
    invoke-static/range {p0 .. p1}, LX/4RH;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 688423
    :cond_f
    const-string v6, "quote"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 688424
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 688425
    :cond_10
    const-string v6, "quotes_analysis"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 688426
    invoke-static/range {p0 .. p1}, LX/4SA;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 688427
    :cond_11
    const-string v6, "source"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 688428
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 688429
    :cond_12
    const-string v6, "summary"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 688430
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 688431
    :cond_13
    const-string v6, "title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 688432
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 688433
    :cond_14
    const-string v6, "titleForSummary"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 688434
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 688435
    :cond_15
    const-string v6, "url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_16

    .line 688436
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 688437
    :cond_16
    const-string v6, "user_url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 688438
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 688439
    :cond_17
    const-string v6, "video_share"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_18

    .line 688440
    invoke-static/range {p0 .. p1}, LX/4UJ;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 688441
    :cond_18
    const-string v6, "external_url_owning_profile"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_19

    .line 688442
    invoke-static/range {p0 .. p1}, LX/2aw;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 688443
    :cond_19
    const-string v6, "viewer_saved_state"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1a

    .line 688444
    const/4 v2, 0x1

    .line 688445
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLSavedState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v6

    move v8, v2

    move-object v12, v6

    goto/16 :goto_1

    .line 688446
    :cond_1a
    const-string v6, "relatedArticleInstantArticle"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1b

    .line 688447
    invoke-static/range {p0 .. p1}, LX/4Og;->a(LX/15w;LX/186;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 688448
    :cond_1b
    const-string v6, "article_author_name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 688449
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 688450
    :cond_1c
    const-string v6, "icon"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 688451
    invoke-static/range {p0 .. p1}, LX/2bm;->a(LX/15w;LX/186;)I

    move-result v2

    move v9, v2

    goto/16 :goto_1

    .line 688452
    :cond_1d
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 688453
    :cond_1e
    const/16 v2, 0x22

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 688454
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 688455
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 688456
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 688457
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 688458
    if-eqz v3, :cond_1f

    .line 688459
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 688460
    :cond_1f
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 688461
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 688462
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 688463
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 688464
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 688465
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 688466
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 688467
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 688468
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 688469
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 688470
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 688471
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 688472
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 688473
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 688474
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 688475
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 688476
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 688477
    const/16 v2, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 688478
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 688479
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 688480
    if-eqz v8, :cond_20

    .line 688481
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->a(ILjava/lang/Enum;)V

    .line 688482
    :cond_20
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 688483
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 688484
    const/16 v2, 0x20

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 688485
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_21
    move/from16 v34, v32

    move/from16 v35, v33

    move/from16 v32, v27

    move/from16 v33, v31

    move/from16 v27, v22

    move/from16 v31, v26

    move/from16 v22, v17

    move/from16 v26, v21

    move/from16 v17, v12

    move/from16 v21, v16

    move/from16 v16, v11

    move-object v12, v7

    move/from16 v7, v30

    move v11, v6

    move/from16 v30, v25

    move/from16 v25, v20

    move/from16 v20, v15

    move v15, v10

    move v10, v5

    move/from16 v37, v14

    move v14, v9

    move v9, v4

    move-wide/from16 v4, v28

    move/from16 v29, v24

    move/from16 v28, v23

    move/from16 v23, v18

    move/from16 v24, v19

    move/from16 v19, v37

    move/from16 v18, v13

    move v13, v8

    move v8, v2

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/16 v4, 0x1d

    const/16 v3, 0xd

    const/4 v2, 0x2

    .line 688486
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 688487
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688488
    if-eqz v0, :cond_0

    .line 688489
    const-string v1, "all_share_stories"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688490
    invoke-static {p0, v0, p2}, LX/4Kq;->a(LX/15i;ILX/0nX;)V

    .line 688491
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 688492
    if-eqz v0, :cond_1

    .line 688493
    const-string v0, "android_urls"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688494
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 688495
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688496
    if-eqz v0, :cond_2

    .line 688497
    const-string v1, "application"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688498
    invoke-static {p0, v0, p2, p3}, LX/2uk;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 688499
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688500
    if-eqz v0, :cond_3

    .line 688501
    const-string v1, "canonical"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688502
    invoke-static {p0, v0, p2, p3}, LX/4MV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 688503
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 688504
    cmp-long v2, v0, v6

    if-eqz v2, :cond_4

    .line 688505
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688506
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 688507
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688508
    if-eqz v0, :cond_5

    .line 688509
    const-string v1, "emotional_analysis"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688510
    invoke-static {p0, v0, p2, p3}, LX/4Lr;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 688511
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 688512
    if-eqz v0, :cond_6

    .line 688513
    const-string v1, "external_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688514
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 688515
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 688516
    if-eqz v0, :cond_7

    .line 688517
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688518
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 688519
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688520
    if-eqz v0, :cond_8

    .line 688521
    const-string v1, "instant_article"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688522
    invoke-static {p0, v0, p2, p3}, LX/4Og;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 688523
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688524
    if-eqz v0, :cond_9

    .line 688525
    const-string v1, "link_media"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688526
    invoke-static {p0, v0, p2, p3}, LX/2at;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 688527
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688528
    if-eqz v0, :cond_a

    .line 688529
    const-string v1, "messenger_content_subscription_option"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688530
    invoke-static {p0, v0, p2, p3}, LX/4PW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 688531
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 688532
    if-eqz v0, :cond_b

    .line 688533
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688534
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 688535
    :cond_b
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 688536
    if-eqz v0, :cond_c

    .line 688537
    const-string v0, "name_search_tokens"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688538
    invoke-virtual {p0, p1, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 688539
    :cond_c
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688540
    if-eqz v0, :cond_d

    .line 688541
    const-string v1, "open_graph_node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688542
    invoke-static {p0, v0, p2, p3}, LX/2bR;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 688543
    :cond_d
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688544
    if-eqz v0, :cond_e

    .line 688545
    const-string v1, "phrases_analysis"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688546
    invoke-static {p0, v0, p2, p3}, LX/4RH;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 688547
    :cond_e
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688548
    if-eqz v0, :cond_f

    .line 688549
    const-string v1, "quote"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688550
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 688551
    :cond_f
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688552
    if-eqz v0, :cond_10

    .line 688553
    const-string v1, "quotes_analysis"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688554
    invoke-static {p0, v0, p2, p3}, LX/4SA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 688555
    :cond_10
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688556
    if-eqz v0, :cond_11

    .line 688557
    const-string v1, "source"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688558
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 688559
    :cond_11
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688560
    if-eqz v0, :cond_12

    .line 688561
    const-string v1, "summary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688562
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 688563
    :cond_12
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688564
    if-eqz v0, :cond_13

    .line 688565
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688566
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 688567
    :cond_13
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688568
    if-eqz v0, :cond_14

    .line 688569
    const-string v1, "titleForSummary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688570
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 688571
    :cond_14
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 688572
    if-eqz v0, :cond_15

    .line 688573
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688574
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 688575
    :cond_15
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 688576
    if-eqz v0, :cond_16

    .line 688577
    const-string v1, "user_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688578
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 688579
    :cond_16
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688580
    if-eqz v0, :cond_17

    .line 688581
    const-string v1, "video_share"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688582
    invoke-static {p0, v0, p2}, LX/4UJ;->a(LX/15i;ILX/0nX;)V

    .line 688583
    :cond_17
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688584
    if-eqz v0, :cond_18

    .line 688585
    const-string v1, "external_url_owning_profile"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688586
    invoke-static {p0, v0, p2, p3}, LX/2aw;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 688587
    :cond_18
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IIS)S

    move-result v0

    .line 688588
    if-eqz v0, :cond_19

    .line 688589
    const-string v0, "viewer_saved_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688590
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLSavedState;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 688591
    :cond_19
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688592
    if-eqz v0, :cond_1a

    .line 688593
    const-string v1, "relatedArticleInstantArticle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688594
    invoke-static {p0, v0, p2, p3}, LX/4Og;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 688595
    :cond_1a
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 688596
    if-eqz v0, :cond_1b

    .line 688597
    const-string v1, "article_author_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688598
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 688599
    :cond_1b
    const/16 v0, 0x20

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688600
    if-eqz v0, :cond_1c

    .line 688601
    const-string v1, "icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688602
    invoke-static {p0, v0, p2}, LX/2bm;->a(LX/15i;ILX/0nX;)V

    .line 688603
    :cond_1c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 688604
    return-void
.end method
