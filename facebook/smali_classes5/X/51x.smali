.class public final LX/51x;
.super LX/51k;
.source ""


# instance fields
.field public a:J

.field public b:J

.field private c:I


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 825654
    const/16 v0, 0x10

    invoke-direct {p0, v0}, LX/51k;-><init>(I)V

    .line 825655
    int-to-long v0, p1

    iput-wide v0, p0, LX/51x;->a:J

    .line 825656
    int-to-long v0, p1

    iput-wide v0, p0, LX/51x;->b:J

    .line 825657
    const/4 v0, 0x0

    iput v0, p0, LX/51x;->c:I

    .line 825658
    return-void
.end method

.method private static b(J)J
    .locals 6

    .prologue
    const/16 v4, 0x21

    .line 825648
    ushr-long v0, p0, v4

    xor-long/2addr v0, p0

    .line 825649
    const-wide v2, -0xae502812aa7333L

    mul-long/2addr v0, v2

    .line 825650
    ushr-long v2, v0, v4

    xor-long/2addr v0, v2

    .line 825651
    const-wide v2, -0x3b314601e57a13adL    # -2.902039044684214E23

    mul-long/2addr v0, v2

    .line 825652
    ushr-long v2, v0, v4

    xor-long/2addr v0, v2

    .line 825653
    return-wide v0
.end method

.method public static c(J)J
    .locals 4

    .prologue
    .line 825644
    const-wide v0, -0x783c846eeebdac2bL

    mul-long/2addr v0, p0

    .line 825645
    const/16 v2, 0x1f

    invoke-static {v0, v1, v2}, Ljava/lang/Long;->rotateLeft(JI)J

    move-result-wide v0

    .line 825646
    const-wide v2, 0x4cf5ad432745937fL    # 5.573325460219186E62

    mul-long/2addr v0, v2

    .line 825647
    return-wide v0
.end method

.method public static d(J)J
    .locals 4

    .prologue
    .line 825640
    const-wide v0, 0x4cf5ad432745937fL    # 5.573325460219186E62

    mul-long/2addr v0, p0

    .line 825641
    const/16 v2, 0x21

    invoke-static {v0, v1, v2}, Ljava/lang/Long;->rotateLeft(JI)J

    move-result-wide v0

    .line 825642
    const-wide v2, -0x783c846eeebdac2bL

    mul-long/2addr v0, v2

    .line 825643
    return-wide v0
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 11

    .prologue
    .line 825627
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v0

    .line 825628
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v2

    .line 825629
    const-wide/16 v8, 0x5

    .line 825630
    iget-wide v4, p0, LX/51x;->a:J

    invoke-static {v0, v1}, LX/51x;->c(J)J

    move-result-wide v6

    xor-long/2addr v4, v6

    iput-wide v4, p0, LX/51x;->a:J

    .line 825631
    iget-wide v4, p0, LX/51x;->a:J

    const/16 v6, 0x1b

    invoke-static {v4, v5, v6}, Ljava/lang/Long;->rotateLeft(JI)J

    move-result-wide v4

    iput-wide v4, p0, LX/51x;->a:J

    .line 825632
    iget-wide v4, p0, LX/51x;->a:J

    iget-wide v6, p0, LX/51x;->b:J

    add-long/2addr v4, v6

    iput-wide v4, p0, LX/51x;->a:J

    .line 825633
    iget-wide v4, p0, LX/51x;->a:J

    mul-long/2addr v4, v8

    const-wide/32 v6, 0x52dce729

    add-long/2addr v4, v6

    iput-wide v4, p0, LX/51x;->a:J

    .line 825634
    iget-wide v4, p0, LX/51x;->b:J

    invoke-static {v2, v3}, LX/51x;->d(J)J

    move-result-wide v6

    xor-long/2addr v4, v6

    iput-wide v4, p0, LX/51x;->b:J

    .line 825635
    iget-wide v4, p0, LX/51x;->b:J

    const/16 v6, 0x1f

    invoke-static {v4, v5, v6}, Ljava/lang/Long;->rotateLeft(JI)J

    move-result-wide v4

    iput-wide v4, p0, LX/51x;->b:J

    .line 825636
    iget-wide v4, p0, LX/51x;->b:J

    iget-wide v6, p0, LX/51x;->a:J

    add-long/2addr v4, v6

    iput-wide v4, p0, LX/51x;->b:J

    .line 825637
    iget-wide v4, p0, LX/51x;->b:J

    mul-long/2addr v4, v8

    const-wide/32 v6, 0x38495ab5

    add-long/2addr v4, v6

    iput-wide v4, p0, LX/51x;->b:J

    .line 825638
    iget v0, p0, LX/51x;->c:I

    add-int/lit8 v0, v0, 0x10

    iput v0, p0, LX/51x;->c:I

    .line 825639
    return-void
.end method

.method public final b()LX/51o;
    .locals 4

    .prologue
    .line 825568
    iget-wide v0, p0, LX/51x;->a:J

    iget v2, p0, LX/51x;->c:I

    int-to-long v2, v2

    xor-long/2addr v0, v2

    iput-wide v0, p0, LX/51x;->a:J

    .line 825569
    iget-wide v0, p0, LX/51x;->b:J

    iget v2, p0, LX/51x;->c:I

    int-to-long v2, v2

    xor-long/2addr v0, v2

    iput-wide v0, p0, LX/51x;->b:J

    .line 825570
    iget-wide v0, p0, LX/51x;->a:J

    iget-wide v2, p0, LX/51x;->b:J

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/51x;->a:J

    .line 825571
    iget-wide v0, p0, LX/51x;->b:J

    iget-wide v2, p0, LX/51x;->a:J

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/51x;->b:J

    .line 825572
    iget-wide v0, p0, LX/51x;->a:J

    invoke-static {v0, v1}, LX/51x;->b(J)J

    move-result-wide v0

    iput-wide v0, p0, LX/51x;->a:J

    .line 825573
    iget-wide v0, p0, LX/51x;->b:J

    invoke-static {v0, v1}, LX/51x;->b(J)J

    move-result-wide v0

    iput-wide v0, p0, LX/51x;->b:J

    .line 825574
    iget-wide v0, p0, LX/51x;->a:J

    iget-wide v2, p0, LX/51x;->b:J

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/51x;->a:J

    .line 825575
    iget-wide v0, p0, LX/51x;->b:J

    iget-wide v2, p0, LX/51x;->a:J

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/51x;->b:J

    .line 825576
    const/16 v0, 0x10

    new-array v0, v0, [B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iget-wide v2, p0, LX/51x;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v0

    iget-wide v2, p0, LX/51x;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-static {v0}, LX/51o;->a([B)LX/51o;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/nio/ByteBuffer;)V
    .locals 13

    .prologue
    const/16 v10, 0x20

    const/16 v9, 0x18

    const/16 v8, 0x10

    const/16 v7, 0x8

    const-wide/16 v2, 0x0

    .line 825577
    iget v0, p0, LX/51x;->c:I

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, LX/51x;->c:I

    .line 825578
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 825579
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Should never get here."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 825580
    :pswitch_0
    const/16 v0, 0xe

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    .line 825581
    and-int/lit16 v1, v0, 0xff

    move v0, v1

    .line 825582
    int-to-long v0, v0

    const/16 v4, 0x30

    shl-long/2addr v0, v4

    xor-long/2addr v0, v2

    .line 825583
    :goto_0
    const/16 v4, 0xd

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    .line 825584
    and-int/lit16 v5, v4, 0xff

    move v4, v5

    .line 825585
    int-to-long v4, v4

    const/16 v6, 0x28

    shl-long/2addr v4, v6

    xor-long/2addr v0, v4

    .line 825586
    :goto_1
    const/16 v4, 0xc

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    .line 825587
    and-int/lit16 v5, v4, 0xff

    move v4, v5

    .line 825588
    int-to-long v4, v4

    shl-long/2addr v4, v10

    xor-long/2addr v0, v4

    .line 825589
    :goto_2
    const/16 v4, 0xb

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    .line 825590
    and-int/lit16 v5, v4, 0xff

    move v4, v5

    .line 825591
    int-to-long v4, v4

    shl-long/2addr v4, v9

    xor-long/2addr v0, v4

    .line 825592
    :goto_3
    const/16 v4, 0xa

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    .line 825593
    and-int/lit16 v5, v4, 0xff

    move v4, v5

    .line 825594
    int-to-long v4, v4

    shl-long/2addr v4, v8

    xor-long/2addr v0, v4

    .line 825595
    :goto_4
    const/16 v4, 0x9

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    .line 825596
    and-int/lit16 v5, v4, 0xff

    move v4, v5

    .line 825597
    int-to-long v4, v4

    shl-long/2addr v4, v7

    xor-long/2addr v0, v4

    .line 825598
    :goto_5
    invoke-virtual {p1, v7}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    .line 825599
    and-int/lit16 v5, v4, 0xff

    move v4, v5

    .line 825600
    int-to-long v4, v4

    xor-long/2addr v0, v4

    .line 825601
    :goto_6
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v4

    xor-long/2addr v2, v4

    .line 825602
    :goto_7
    iget-wide v4, p0, LX/51x;->a:J

    invoke-static {v2, v3}, LX/51x;->c(J)J

    move-result-wide v2

    xor-long/2addr v2, v4

    iput-wide v2, p0, LX/51x;->a:J

    .line 825603
    iget-wide v2, p0, LX/51x;->b:J

    invoke-static {v0, v1}, LX/51x;->d(J)J

    move-result-wide v0

    xor-long/2addr v0, v2

    iput-wide v0, p0, LX/51x;->b:J

    .line 825604
    return-void

    .line 825605
    :pswitch_1
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    .line 825606
    and-int/lit16 v1, v0, 0xff

    move v0, v1

    .line 825607
    int-to-long v0, v0

    const/16 v4, 0x30

    shl-long/2addr v0, v4

    xor-long/2addr v0, v2

    .line 825608
    :goto_8
    const/4 v4, 0x5

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    .line 825609
    and-int/lit16 v5, v4, 0xff

    move v4, v5

    .line 825610
    int-to-long v4, v4

    const/16 v6, 0x28

    shl-long/2addr v4, v6

    xor-long/2addr v0, v4

    .line 825611
    :goto_9
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    .line 825612
    and-int/lit16 v5, v4, 0xff

    move v4, v5

    .line 825613
    int-to-long v4, v4

    shl-long/2addr v4, v10

    xor-long/2addr v0, v4

    .line 825614
    :goto_a
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    .line 825615
    and-int/lit16 v5, v4, 0xff

    move v4, v5

    .line 825616
    int-to-long v4, v4

    shl-long/2addr v4, v9

    xor-long/2addr v0, v4

    .line 825617
    :goto_b
    const/4 v4, 0x2

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    .line 825618
    and-int/lit16 v5, v4, 0xff

    move v4, v5

    .line 825619
    int-to-long v4, v4

    shl-long/2addr v4, v8

    xor-long/2addr v0, v4

    .line 825620
    :goto_c
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    .line 825621
    and-int/lit16 v5, v4, 0xff

    move v4, v5

    .line 825622
    int-to-long v4, v4

    shl-long/2addr v4, v7

    xor-long/2addr v0, v4

    .line 825623
    :goto_d
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    .line 825624
    and-int/lit16 v5, v4, 0xff

    move v4, v5

    .line 825625
    int-to-long v4, v4

    xor-long/2addr v0, v4

    move-wide v11, v2

    move-wide v2, v0

    move-wide v0, v11

    .line 825626
    goto :goto_7

    :pswitch_2
    move-wide v0, v2

    goto/16 :goto_0

    :pswitch_3
    move-wide v0, v2

    goto/16 :goto_1

    :pswitch_4
    move-wide v0, v2

    goto/16 :goto_2

    :pswitch_5
    move-wide v0, v2

    goto/16 :goto_3

    :pswitch_6
    move-wide v0, v2

    goto/16 :goto_4

    :pswitch_7
    move-wide v0, v2

    goto/16 :goto_5

    :pswitch_8
    move-wide v0, v2

    goto :goto_6

    :pswitch_9
    move-wide v0, v2

    goto :goto_8

    :pswitch_a
    move-wide v0, v2

    goto :goto_9

    :pswitch_b
    move-wide v0, v2

    goto :goto_a

    :pswitch_c
    move-wide v0, v2

    goto :goto_b

    :pswitch_d
    move-wide v0, v2

    goto :goto_c

    :pswitch_e
    move-wide v0, v2

    goto :goto_d

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
