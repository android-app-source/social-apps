.class public final LX/4lH;
.super Landroid/view/inputmethod/BaseInputConnection;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/resources/ui/DigitEditText;


# direct methods
.method public constructor <init>(Lcom/facebook/resources/ui/DigitEditText;Lcom/facebook/resources/ui/DigitEditText;Z)V
    .locals 0

    .prologue
    .line 803894
    iput-object p1, p0, LX/4lH;->a:Lcom/facebook/resources/ui/DigitEditText;

    .line 803895
    invoke-direct {p0, p2, p3}, Landroid/view/inputmethod/BaseInputConnection;-><init>(Landroid/view/View;Z)V

    .line 803896
    return-void
.end method


# virtual methods
.method public final deleteSurroundingText(II)Z
    .locals 1

    .prologue
    .line 803897
    iget-object v0, p0, LX/4lH;->a:Lcom/facebook/resources/ui/DigitEditText;

    iget-object v0, v0, Lcom/facebook/resources/ui/DigitEditText;->b:LX/4lG;

    if-eqz v0, :cond_0

    .line 803898
    iget-object v0, p0, LX/4lH;->a:Lcom/facebook/resources/ui/DigitEditText;

    iget-object v0, v0, Lcom/facebook/resources/ui/DigitEditText;->b:LX/4lG;

    invoke-interface {v0}, LX/4lG;->a()V

    .line 803899
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->deleteSurroundingText(II)Z

    move-result v0

    return v0
.end method

.method public final sendKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 803900
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x43

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 803901
    iget-object v0, p0, LX/4lH;->a:Lcom/facebook/resources/ui/DigitEditText;

    iget-object v0, v0, Lcom/facebook/resources/ui/DigitEditText;->b:LX/4lG;

    if-eqz v0, :cond_0

    .line 803902
    iget-object v0, p0, LX/4lH;->a:Lcom/facebook/resources/ui/DigitEditText;

    iget-object v0, v0, Lcom/facebook/resources/ui/DigitEditText;->b:LX/4lG;

    invoke-interface {v0}, LX/4lG;->a()V

    .line 803903
    :cond_0
    invoke-super {p0, p1}, Landroid/view/inputmethod/BaseInputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method
