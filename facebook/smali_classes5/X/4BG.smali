.class public final LX/4BG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qB;


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method public constructor <init>(Landroid/os/IBinder;)V
    .locals 0

    .prologue
    .line 677615
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 677616
    iput-object p1, p0, LX/4BG;->mRemote:Landroid/os/IBinder;

    .line 677617
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 677618
    iget-object v0, p0, LX/4BG;->mRemote:Landroid/os/IBinder;

    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 677619
    const-string v0, "com.facebook.fbservice.service.ICompletionHandler"

    return-object v0
.end method

.method public onOperationCompleted(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 5

    .prologue
    .line 677620
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 677621
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 677622
    :try_start_0
    const-string v0, "com.facebook.fbservice.service.ICompletionHandler"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 677623
    if-eqz p1, :cond_0

    .line 677624
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 677625
    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Lcom/facebook/fbservice/service/OperationResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 677626
    :goto_0
    iget-object v0, p0, LX/4BG;->mRemote:Landroid/os/IBinder;

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 677627
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 677628
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 677629
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 677630
    return-void

    .line 677631
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 677632
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 677633
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public onOperationProgress(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 5

    .prologue
    .line 677634
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 677635
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 677636
    :try_start_0
    const-string v0, "com.facebook.fbservice.service.ICompletionHandler"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 677637
    if-eqz p1, :cond_0

    .line 677638
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 677639
    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Lcom/facebook/fbservice/service/OperationResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 677640
    :goto_0
    iget-object v0, p0, LX/4BG;->mRemote:Landroid/os/IBinder;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 677641
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 677642
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 677643
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 677644
    return-void

    .line 677645
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 677646
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 677647
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method
