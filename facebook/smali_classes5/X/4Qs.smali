.class public LX/4Qs;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 706601
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 706602
    const/4 v15, 0x0

    .line 706603
    const/4 v14, 0x0

    .line 706604
    const/4 v13, 0x0

    .line 706605
    const/4 v12, 0x0

    .line 706606
    const/4 v11, 0x0

    .line 706607
    const/4 v10, 0x0

    .line 706608
    const/4 v9, 0x0

    .line 706609
    const/4 v8, 0x0

    .line 706610
    const/4 v7, 0x0

    .line 706611
    const/4 v6, 0x0

    .line 706612
    const/4 v5, 0x0

    .line 706613
    const/4 v4, 0x0

    .line 706614
    const/4 v3, 0x0

    .line 706615
    const/4 v2, 0x0

    .line 706616
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_1

    .line 706617
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 706618
    const/4 v2, 0x0

    .line 706619
    :goto_0
    return v2

    .line 706620
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 706621
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_c

    .line 706622
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v16

    .line 706623
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 706624
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    if-eqz v16, :cond_1

    .line 706625
    const-string v17, "attribution_text"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 706626
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v15

    goto :goto_1

    .line 706627
    :cond_2
    const-string v17, "attribution_thumbnail"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 706628
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v14

    goto :goto_1

    .line 706629
    :cond_3
    const-string v17, "emitters"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 706630
    invoke-static/range {p0 .. p1}, LX/4Qy;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 706631
    :cond_4
    const-string v17, "has_location_constraints"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 706632
    const/4 v4, 0x1

    .line 706633
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto :goto_1

    .line 706634
    :cond_5
    const-string v17, "id"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 706635
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 706636
    :cond_6
    const-string v17, "instructions"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 706637
    invoke-static/range {p0 .. p1}, LX/4PI;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 706638
    :cond_7
    const-string v17, "is_logging_disabled"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 706639
    const/4 v3, 0x1

    .line 706640
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto/16 :goto_1

    .line 706641
    :cond_8
    const-string v17, "name"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 706642
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 706643
    :cond_9
    const-string v17, "url"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 706644
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 706645
    :cond_a
    const-string v17, "optical_flow_disabled"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 706646
    const/4 v2, 0x1

    .line 706647
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    goto/16 :goto_1

    .line 706648
    :cond_b
    const-string v17, "thumbnail_image"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 706649
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 706650
    :cond_c
    const/16 v16, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 706651
    const/16 v16, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 706652
    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 706653
    const/4 v14, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 706654
    if-eqz v4, :cond_d

    .line 706655
    const/4 v4, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->a(IZ)V

    .line 706656
    :cond_d
    const/4 v4, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 706657
    const/4 v4, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 706658
    if-eqz v3, :cond_e

    .line 706659
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->a(IZ)V

    .line 706660
    :cond_e
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 706661
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 706662
    if-eqz v2, :cond_f

    .line 706663
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->a(IZ)V

    .line 706664
    :cond_f
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 706665
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 706666
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 706667
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 706668
    if-eqz v0, :cond_0

    .line 706669
    const-string v1, "attribution_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706670
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 706671
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 706672
    if-eqz v0, :cond_1

    .line 706673
    const-string v1, "attribution_thumbnail"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706674
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 706675
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 706676
    if-eqz v0, :cond_2

    .line 706677
    const-string v1, "emitters"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706678
    invoke-static {p0, v0, p2, p3}, LX/4Qy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 706679
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 706680
    if-eqz v0, :cond_3

    .line 706681
    const-string v1, "has_location_constraints"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706682
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 706683
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 706684
    if-eqz v0, :cond_4

    .line 706685
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706686
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 706687
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 706688
    if-eqz v0, :cond_5

    .line 706689
    const-string v1, "instructions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706690
    invoke-static {p0, v0, p2}, LX/4PI;->a(LX/15i;ILX/0nX;)V

    .line 706691
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 706692
    if-eqz v0, :cond_6

    .line 706693
    const-string v1, "is_logging_disabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706694
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 706695
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 706696
    if-eqz v0, :cond_7

    .line 706697
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706698
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 706699
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 706700
    if-eqz v0, :cond_8

    .line 706701
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706702
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 706703
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 706704
    if-eqz v0, :cond_9

    .line 706705
    const-string v1, "optical_flow_disabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706706
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 706707
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 706708
    if-eqz v0, :cond_a

    .line 706709
    const-string v1, "thumbnail_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706710
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 706711
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 706712
    return-void
.end method
