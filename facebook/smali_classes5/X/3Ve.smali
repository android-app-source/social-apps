.class public LX/3Ve;
.super LX/3Va;
.source ""


# static fields
.field public static final j:LX/1Cz;


# instance fields
.field public k:LX/C01;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 588012
    new-instance v0, LX/3Vf;

    invoke-direct {v0}, LX/3Vf;-><init>()V

    sput-object v0, LX/3Ve;->j:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 588008
    const v0, 0x7f030fed

    invoke-direct {p0, p1, v0}, LX/3Va;-><init>(Landroid/content/Context;I)V

    .line 588009
    const-class v0, LX/3Ve;

    invoke-static {v0, p0}, LX/3Ve;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 588010
    invoke-virtual {p0}, LX/3Ve;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00c7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const v0, 0x7f0d052c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/EllipsizingTextView;

    const v1, 0x7f0d052d

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/EllipsizingTextView;

    invoke-static {v3, v0, v1}, LX/C01;->a(ILcom/facebook/resources/ui/EllipsizingTextView;Lcom/facebook/resources/ui/EllipsizingTextView;)V

    .line 588011
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/3Ve;

    invoke-static {p0}, LX/C01;->a(LX/0QB;)LX/C01;

    move-result-object p0

    check-cast p0, LX/C01;

    iput-object p0, p1, LX/3Ve;->k:LX/C01;

    return-void
.end method
