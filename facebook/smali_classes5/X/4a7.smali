.class public final LX/4a7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Z

.field public c:Z

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 790540
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 790541
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 790542
    iget-object v1, p0, LX/4a7;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 790543
    iget-object v3, p0, LX/4a7;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 790544
    const/4 v5, 0x4

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 790545
    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 790546
    iget-boolean v1, p0, LX/4a7;->b:Z

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 790547
    const/4 v1, 0x2

    iget-boolean v5, p0, LX/4a7;->c:Z

    invoke-virtual {v0, v1, v5}, LX/186;->a(IZ)V

    .line 790548
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 790549
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 790550
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 790551
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 790552
    invoke-virtual {v1, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 790553
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 790554
    new-instance v1, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;-><init>(LX/15i;)V

    .line 790555
    return-object v1
.end method
