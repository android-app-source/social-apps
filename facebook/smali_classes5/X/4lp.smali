.class public LX/4lp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4lm;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/4lp;


# instance fields
.field private final a:LX/4lu;


# direct methods
.method public constructor <init>(LX/4lu;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 804810
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 804811
    iput-object p1, p0, LX/4lp;->a:LX/4lu;

    .line 804812
    return-void
.end method

.method public static a(LX/0QB;)LX/4lp;
    .locals 4

    .prologue
    .line 804813
    sget-object v0, LX/4lp;->b:LX/4lp;

    if-nez v0, :cond_1

    .line 804814
    const-class v1, LX/4lp;

    monitor-enter v1

    .line 804815
    :try_start_0
    sget-object v0, LX/4lp;->b:LX/4lp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 804816
    if-eqz v2, :cond_0

    .line 804817
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 804818
    new-instance p0, LX/4lp;

    invoke-static {v0}, LX/4lu;->a(LX/0QB;)LX/4lu;

    move-result-object v3

    check-cast v3, LX/4lu;

    invoke-direct {p0, v3}, LX/4lp;-><init>(LX/4lu;)V

    .line 804819
    move-object v0, p0

    .line 804820
    sput-object v0, LX/4lp;->b:LX/4lp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 804821
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 804822
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 804823
    :cond_1
    sget-object v0, LX/4lp;->b:LX/4lp;

    return-object v0

    .line 804824
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 804825
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 804826
    sget-boolean v0, LX/4lu;->c:Z

    move v0, v0

    .line 804827
    return v0
.end method
