.class public final LX/3dX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/3dW;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 617175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 617176
    check-cast p1, LX/3dW;

    check-cast p2, LX/3dW;

    .line 617177
    iget v0, p1, LX/3dW;->c:I

    iget v1, p1, LX/3dW;->d:I

    add-int/2addr v0, v1

    iget v1, p2, LX/3dW;->c:I

    if-ge v0, v1, :cond_0

    .line 617178
    const/4 v0, -0x1

    .line 617179
    :goto_0
    return v0

    .line 617180
    :cond_0
    iget v0, p1, LX/3dW;->c:I

    iget v1, p2, LX/3dW;->c:I

    iget v2, p2, LX/3dW;->d:I

    add-int/2addr v1, v2

    if-le v0, v1, :cond_1

    .line 617181
    const/4 v0, 0x1

    goto :goto_0

    .line 617182
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
