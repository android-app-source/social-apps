.class public LX/4Ls;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 685454
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 685455
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_6

    .line 685456
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 685457
    :goto_0
    return v1

    .line 685458
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_4

    .line 685459
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 685460
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 685461
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_0

    if-eqz v6, :cond_0

    .line 685462
    const-string v7, "count"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 685463
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 685464
    :cond_1
    const-string v7, "icon_image"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 685465
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 685466
    :cond_2
    const-string v7, "mood_page"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 685467
    invoke-static {p0, p1}, LX/2bc;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 685468
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 685469
    :cond_4
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 685470
    if-eqz v0, :cond_5

    .line 685471
    invoke-virtual {p1, v1, v5, v1}, LX/186;->a(III)V

    .line 685472
    :cond_5
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 685473
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 685474
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 685475
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 685476
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 685477
    if-eqz v0, :cond_0

    .line 685478
    const-string v1, "count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685479
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 685480
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 685481
    if-eqz v0, :cond_1

    .line 685482
    const-string v1, "icon_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685483
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 685484
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 685485
    if-eqz v0, :cond_2

    .line 685486
    const-string v1, "mood_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685487
    invoke-static {p0, v0, p2, p3}, LX/2bc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 685488
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 685489
    return-void
.end method
