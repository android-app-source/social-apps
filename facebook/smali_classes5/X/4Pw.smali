.class public LX/4Pw;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 702265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 34

    .prologue
    .line 702144
    const/16 v30, 0x0

    .line 702145
    const/16 v29, 0x0

    .line 702146
    const/16 v28, 0x0

    .line 702147
    const/16 v27, 0x0

    .line 702148
    const/16 v26, 0x0

    .line 702149
    const/16 v25, 0x0

    .line 702150
    const/16 v24, 0x0

    .line 702151
    const/16 v23, 0x0

    .line 702152
    const/16 v22, 0x0

    .line 702153
    const/16 v21, 0x0

    .line 702154
    const/16 v20, 0x0

    .line 702155
    const/16 v19, 0x0

    .line 702156
    const/16 v18, 0x0

    .line 702157
    const/16 v17, 0x0

    .line 702158
    const/16 v16, 0x0

    .line 702159
    const/4 v15, 0x0

    .line 702160
    const/4 v14, 0x0

    .line 702161
    const/4 v13, 0x0

    .line 702162
    const/4 v12, 0x0

    .line 702163
    const/4 v11, 0x0

    .line 702164
    const/4 v10, 0x0

    .line 702165
    const/4 v9, 0x0

    .line 702166
    const/4 v8, 0x0

    .line 702167
    const/4 v7, 0x0

    .line 702168
    const/4 v6, 0x0

    .line 702169
    const/4 v5, 0x0

    .line 702170
    const/4 v4, 0x0

    .line 702171
    const/4 v3, 0x0

    .line 702172
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v31

    sget-object v32, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    if-eq v0, v1, :cond_1

    .line 702173
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 702174
    const/4 v3, 0x0

    .line 702175
    :goto_0
    return v3

    .line 702176
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 702177
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v31

    sget-object v32, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    if-eq v0, v1, :cond_1b

    .line 702178
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v31

    .line 702179
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 702180
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v32

    sget-object v33, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    if-eq v0, v1, :cond_1

    if-eqz v31, :cond_1

    .line 702181
    const-string v32, "blurredCoverPhoto"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_2

    .line 702182
    invoke-static/range {p0 .. p1}, LX/2sX;->a(LX/15w;LX/186;)I

    move-result v30

    goto :goto_1

    .line 702183
    :cond_2
    const-string v32, "cover_photo"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_3

    .line 702184
    invoke-static/range {p0 .. p1}, LX/2sX;->a(LX/15w;LX/186;)I

    move-result v29

    goto :goto_1

    .line 702185
    :cond_3
    const-string v32, "feedAwesomizerProfilePicture"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_4

    .line 702186
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v28

    goto :goto_1

    .line 702187
    :cond_4
    const-string v32, "feedback"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_5

    .line 702188
    invoke-static/range {p0 .. p1}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v27

    goto :goto_1

    .line 702189
    :cond_5
    const-string v32, "from"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_6

    .line 702190
    invoke-static/range {p0 .. p1}, LX/2bM;->a(LX/15w;LX/186;)I

    move-result v26

    goto :goto_1

    .line 702191
    :cond_6
    const-string v32, "id"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_7

    .line 702192
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    goto :goto_1

    .line 702193
    :cond_7
    const-string v32, "imageHighOrig"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_8

    .line 702194
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 702195
    :cond_8
    const-string v32, "name"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_9

    .line 702196
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    goto/16 :goto_1

    .line 702197
    :cond_9
    const-string v32, "plain_body"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_a

    .line 702198
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    goto/16 :goto_1

    .line 702199
    :cond_a
    const-string v32, "privacy_scope"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_b

    .line 702200
    invoke-static/range {p0 .. p1}, LX/2bo;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 702201
    :cond_b
    const-string v32, "profileImageLarge"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_c

    .line 702202
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 702203
    :cond_c
    const-string v32, "profileImageSmall"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_d

    .line 702204
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 702205
    :cond_d
    const-string v32, "profilePicture50"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_e

    .line 702206
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 702207
    :cond_e
    const-string v32, "profilePictureHighRes"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_f

    .line 702208
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 702209
    :cond_f
    const-string v32, "profilePictureLarge"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_10

    .line 702210
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 702211
    :cond_10
    const-string v32, "profile_photo"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_11

    .line 702212
    invoke-static/range {p0 .. p1}, LX/2sY;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 702213
    :cond_11
    const-string v32, "profile_picture"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_12

    .line 702214
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 702215
    :cond_12
    const-string v32, "profile_picture_is_silhouette"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_13

    .line 702216
    const/4 v4, 0x1

    .line 702217
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    goto/16 :goto_1

    .line 702218
    :cond_13
    const-string v32, "published_document"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_14

    .line 702219
    invoke-static/range {p0 .. p1}, LX/4LL;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 702220
    :cond_14
    const-string v32, "streaming_profile_picture"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_15

    .line 702221
    invoke-static/range {p0 .. p1}, LX/4TN;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 702222
    :cond_15
    const-string v32, "subject"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_16

    .line 702223
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 702224
    :cond_16
    const-string v32, "taggable_object_profile_picture"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_17

    .line 702225
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 702226
    :cond_17
    const-string v32, "url"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_18

    .line 702227
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 702228
    :cond_18
    const-string v32, "viewer_saved_state"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_19

    .line 702229
    const/4 v3, 0x1

    .line 702230
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLSavedState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v7

    goto/16 :goto_1

    .line 702231
    :cond_19
    const-string v32, "profilePicture180"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_1a

    .line 702232
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 702233
    :cond_1a
    const-string v32, "publisher_profile_image"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_0

    .line 702234
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 702235
    :cond_1b
    const/16 v31, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 702236
    const/16 v31, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v31

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 702237
    const/16 v30, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v30

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 702238
    const/16 v29, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v29

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 702239
    const/16 v28, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v28

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 702240
    const/16 v27, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 702241
    const/16 v26, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v26

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 702242
    const/16 v25, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 702243
    const/16 v24, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 702244
    const/16 v23, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 702245
    const/16 v22, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 702246
    const/16 v21, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 702247
    const/16 v20, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 702248
    const/16 v19, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 702249
    const/16 v18, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 702250
    const/16 v17, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 702251
    const/16 v16, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 702252
    const/16 v15, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 702253
    if-eqz v4, :cond_1c

    .line 702254
    const/16 v4, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->a(IZ)V

    .line 702255
    :cond_1c
    const/16 v4, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 702256
    const/16 v4, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 702257
    const/16 v4, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 702258
    const/16 v4, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 702259
    const/16 v4, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8}, LX/186;->b(II)V

    .line 702260
    if-eqz v3, :cond_1d

    .line 702261
    const/16 v3, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->a(ILjava/lang/Enum;)V

    .line 702262
    :cond_1d
    const/16 v3, 0x1c

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 702263
    const/16 v3, 0x1d

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 702264
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
