.class public abstract LX/3zF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:J

.field public d:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 662320
    const-string v0, "unknown"

    const-string v1, ""

    const-wide/16 v2, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, LX/3zF;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 662321
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3

    .prologue
    .line 662322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 662323
    iput-object p1, p0, LX/3zF;->a:Ljava/lang/String;

    .line 662324
    if-eqz p2, :cond_1

    .line 662325
    const/16 v0, 0x5f

    invoke-static {v0}, LX/2Cb;->on(C)LX/2Cb;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/2Cb;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    .line 662326
    invoke-static {v0}, LX/0Ph;->b(Ljava/lang/Iterable;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    :goto_0
    iput-object p2, p0, LX/3zF;->b:Ljava/lang/String;

    .line 662327
    :goto_1
    iput-wide p3, p0, LX/3zF;->c:J

    .line 662328
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3zF;->d:J

    .line 662329
    return-void

    .line 662330
    :cond_0
    invoke-static {v0}, LX/0Ph;->g(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object p2, v0

    goto :goto_0

    .line 662331
    :cond_1
    const-string v0, "-1"

    iput-object v0, p0, LX/3zF;->b:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public abstract a()Lcom/facebook/analytics/logger/HoneyClientEvent;
.end method
