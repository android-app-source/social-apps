.class public final LX/4yc;
.super LX/0Rf;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Rf",
        "<",
        "Ljava/util/Map$Entry",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field private final transient a:LX/2Q6;
    .annotation build Lcom/google/j2objc/annotations/Weak;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2Q6",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2Q6;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Q6",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 821920
    invoke-direct {p0}, LX/0Rf;-><init>()V

    .line 821921
    iput-object p1, p0, LX/4yc;->a:LX/2Q6;

    .line 821922
    return-void
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 821916
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-eqz v0, :cond_0

    .line 821917
    check-cast p1, Ljava/util/Map$Entry;

    .line 821918
    iget-object v0, p0, LX/4yc;->a:LX/2Q6;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0Xt;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 821919
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isPartialView()Z
    .locals 1

    .prologue
    .line 821912
    const/4 v0, 0x0

    return v0
.end method

.method public final iterator()LX/0Rc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 821915
    iget-object v0, p0, LX/4yc;->a:LX/2Q6;

    invoke-virtual {v0}, LX/18f;->w()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 821914
    invoke-virtual {p0}, LX/4yc;->iterator()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 821913
    iget-object v0, p0, LX/4yc;->a:LX/2Q6;

    invoke-virtual {v0}, LX/18f;->f()I

    move-result v0

    return v0
.end method
