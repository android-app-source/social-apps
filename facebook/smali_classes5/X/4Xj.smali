.class public final LX/4Xj;
.super LX/0ur;
.source ""


# instance fields
.field public b:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:J

.field public f:I

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 772235
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 772236
    const/4 v0, 0x0

    iput-object v0, p0, LX/4Xj;->n:LX/0x2;

    .line 772237
    instance-of v0, p0, LX/4Xj;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 772238
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;)LX/4Xj;
    .locals 4

    .prologue
    .line 772216
    new-instance v1, LX/4Xj;

    invoke-direct {v1}, LX/4Xj;-><init>()V

    .line 772217
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 772218
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;

    move-result-object v0

    iput-object v0, v1, LX/4Xj;->b:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;

    .line 772219
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xj;->c:Ljava/lang/String;

    .line 772220
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->E_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xj;->d:Ljava/lang/String;

    .line 772221
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->F_()J

    move-result-wide v2

    iput-wide v2, v1, LX/4Xj;->e:J

    .line 772222
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->o()I

    move-result v0

    iput v0, v1, LX/4Xj;->f:I

    .line 772223
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->r()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xj;->g:Ljava/lang/String;

    .line 772224
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->w()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/4Xj;->h:LX/0Px;

    .line 772225
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->s()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xj;->i:Ljava/lang/String;

    .line 772226
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/4Xj;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 772227
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/4Xj;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 772228
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xj;->l:Ljava/lang/String;

    .line 772229
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->v()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xj;->m:Ljava/lang/String;

    .line 772230
    invoke-static {v1, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 772231
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->L_()LX/0x2;

    move-result-object v0

    invoke-virtual {v0}, LX/0x2;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x2;

    iput-object v0, v1, LX/4Xj;->n:LX/0x2;

    .line 772232
    return-object v1
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;
    .locals 2

    .prologue
    .line 772233
    new-instance v0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;-><init>(LX/4Xj;)V

    .line 772234
    return-object v0
.end method
