.class public final LX/3uI;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field public final synthetic a:LX/3u8;


# direct methods
.method public constructor <init>(LX/3u8;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 648213
    iput-object p1, p0, LX/3uI;->a:LX/3u8;

    .line 648214
    invoke-direct {p0, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 648215
    return-void
.end method


# virtual methods
.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 648216
    iget-object v0, p0, LX/3uI;->a:LX/3u8;

    invoke-virtual {v0, p1}, LX/3u8;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 648217
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 648218
    if-nez v0, :cond_1

    .line 648219
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 648220
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 648221
    const/4 v2, -0x5

    .line 648222
    if-lt v0, v2, :cond_0

    if-lt v1, v2, :cond_0

    invoke-virtual {p0}, LX/3uI;->getWidth()I

    move-result v2

    add-int/lit8 v2, v2, 0x5

    if-gt v0, v2, :cond_0

    invoke-virtual {p0}, LX/3uI;->getHeight()I

    move-result v2

    add-int/lit8 v2, v2, 0x5

    if-le v1, v2, :cond_2

    :cond_0
    const/4 v2, 0x1

    :goto_0
    move v0, v2

    .line 648223
    if-eqz v0, :cond_1

    .line 648224
    iget-object v0, p0, LX/3uI;->a:LX/3u8;

    const/4 v1, 0x0

    .line 648225
    invoke-static {v0, v1}, LX/3u8;->f(LX/3u8;I)LX/3uK;

    move-result-object v2

    const/4 p0, 0x1

    invoke-static {v0, v2, p0}, LX/3u8;->a$redex0(LX/3u8;LX/3uK;Z)V

    .line 648226
    const/4 v0, 0x1

    .line 648227
    :goto_1
    return v0

    :cond_1
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final setBackgroundResource(I)V
    .locals 1

    .prologue
    .line 648228
    invoke-virtual {p0}, LX/3uI;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/3wA;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3uI;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 648229
    return-void
.end method
