.class public final LX/3uH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3uG;


# instance fields
.field public final synthetic a:LX/3u8;

.field private b:LX/3uG;


# direct methods
.method public constructor <init>(LX/3u8;LX/3uG;)V
    .locals 0

    .prologue
    .line 648210
    iput-object p1, p0, LX/3uH;->a:LX/3u8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 648211
    iput-object p2, p0, LX/3uH;->b:LX/3uG;

    .line 648212
    return-void
.end method


# virtual methods
.method public final a(LX/3uV;)V
    .locals 2

    .prologue
    .line 648198
    iget-object v0, p0, LX/3uH;->b:LX/3uG;

    invoke-interface {v0, p1}, LX/3uG;->a(LX/3uV;)V

    .line 648199
    iget-object v0, p0, LX/3uH;->a:LX/3u8;

    iget-object v0, v0, LX/3u8;->m:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_2

    .line 648200
    iget-object v0, p0, LX/3uH;->a:LX/3u8;

    iget-object v0, v0, LX/3u6;->b:LX/3uM;

    invoke-interface {v0}, LX/3uM;->b()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, LX/3uH;->a:LX/3u8;

    iget-object v1, v1, LX/3u8;->n:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 648201
    iget-object v0, p0, LX/3uH;->a:LX/3u8;

    iget-object v0, v0, LX/3u8;->m:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 648202
    :cond_0
    :goto_0
    iget-object v0, p0, LX/3uH;->a:LX/3u8;

    iget-object v0, v0, LX/3u8;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_1

    .line 648203
    iget-object v0, p0, LX/3uH;->a:LX/3u8;

    iget-object v0, v0, LX/3u8;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->removeAllViews()V

    .line 648204
    :cond_1
    iget-object v0, p0, LX/3uH;->a:LX/3u8;

    const/4 v1, 0x0

    iput-object v1, v0, LX/3u8;->k:LX/3uV;

    .line 648205
    return-void

    .line 648206
    :cond_2
    iget-object v0, p0, LX/3uH;->a:LX/3u8;

    iget-object v0, v0, LX/3u8;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_0

    .line 648207
    iget-object v0, p0, LX/3uH;->a:LX/3u8;

    iget-object v0, v0, LX/3u8;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarContextView;->setVisibility(I)V

    .line 648208
    iget-object v0, p0, LX/3uH;->a:LX/3u8;

    iget-object v0, v0, LX/3u8;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 648209
    iget-object v0, p0, LX/3uH;->a:LX/3u8;

    iget-object v0, v0, LX/3u8;->l:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, LX/0vv;->z(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(LX/3uV;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 648197
    iget-object v0, p0, LX/3uH;->b:LX/3uG;

    invoke-interface {v0, p1, p2}, LX/3uG;->a(LX/3uV;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final a(LX/3uV;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 648195
    iget-object v0, p0, LX/3uH;->b:LX/3uG;

    invoke-interface {v0, p1, p2}, LX/3uG;->a(LX/3uV;Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/3uV;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 648196
    iget-object v0, p0, LX/3uH;->b:LX/3uG;

    invoke-interface {v0, p1, p2}, LX/3uG;->b(LX/3uV;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method
