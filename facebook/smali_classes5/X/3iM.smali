.class public LX/3iM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3iN;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3iM;


# instance fields
.field public a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3iO;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 629224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 629225
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/3iM;->a:Ljava/util/Map;

    .line 629226
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/3iM;->b:Ljava/util/Map;

    .line 629227
    invoke-virtual {p1, p0}, LX/3iO;->a(LX/3iN;)V

    .line 629228
    return-void
.end method

.method public static a(LX/0QB;)LX/3iM;
    .locals 4

    .prologue
    .line 629238
    sget-object v0, LX/3iM;->c:LX/3iM;

    if-nez v0, :cond_1

    .line 629239
    const-class v1, LX/3iM;

    monitor-enter v1

    .line 629240
    :try_start_0
    sget-object v0, LX/3iM;->c:LX/3iM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 629241
    if-eqz v2, :cond_0

    .line 629242
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 629243
    new-instance p0, LX/3iM;

    invoke-static {v0}, LX/3iO;->a(LX/0QB;)LX/3iO;

    move-result-object v3

    check-cast v3, LX/3iO;

    invoke-direct {p0, v3}, LX/3iM;-><init>(LX/3iO;)V

    .line 629244
    move-object v0, p0

    .line 629245
    sput-object v0, LX/3iM;->c:LX/3iM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 629246
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 629247
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 629248
    :cond_1
    sget-object v0, LX/3iM;->c:LX/3iM;

    return-object v0

    .line 629249
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 629250
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 1

    .prologue
    .line 629234
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3iM;->b(Ljava/lang/String;)V

    .line 629235
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V
    .locals 1

    .prologue
    .line 629236
    iget-object v0, p0, LX/3iM;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 629237
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 629231
    iget-object v0, p0, LX/3iM;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 629232
    iget-object v0, p0, LX/3iM;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 629233
    return-void
.end method

.method public final c(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;
    .locals 1

    .prologue
    .line 629229
    iget-object v0, p0, LX/3iM;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    .line 629230
    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    :cond_0
    return-object v0
.end method
