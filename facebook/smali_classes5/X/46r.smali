.class public final enum LX/46r;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/46r;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/46r;

.field public static final enum BOTTOM:LX/46r;

.field public static final enum CENTER:LX/46r;

.field public static final enum LEFT:LX/46r;

.field public static final enum RIGHT:LX/46r;

.field public static final enum TOP:LX/46r;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 671626
    new-instance v0, LX/46r;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v2}, LX/46r;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/46r;->TOP:LX/46r;

    .line 671627
    new-instance v0, LX/46r;

    const-string v1, "BOTTOM"

    invoke-direct {v0, v1, v3}, LX/46r;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/46r;->BOTTOM:LX/46r;

    .line 671628
    new-instance v0, LX/46r;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v4}, LX/46r;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/46r;->LEFT:LX/46r;

    .line 671629
    new-instance v0, LX/46r;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v5}, LX/46r;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/46r;->RIGHT:LX/46r;

    .line 671630
    new-instance v0, LX/46r;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v6}, LX/46r;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/46r;->CENTER:LX/46r;

    .line 671631
    const/4 v0, 0x5

    new-array v0, v0, [LX/46r;

    sget-object v1, LX/46r;->TOP:LX/46r;

    aput-object v1, v0, v2

    sget-object v1, LX/46r;->BOTTOM:LX/46r;

    aput-object v1, v0, v3

    sget-object v1, LX/46r;->LEFT:LX/46r;

    aput-object v1, v0, v4

    sget-object v1, LX/46r;->RIGHT:LX/46r;

    aput-object v1, v0, v5

    sget-object v1, LX/46r;->CENTER:LX/46r;

    aput-object v1, v0, v6

    sput-object v0, LX/46r;->$VALUES:[LX/46r;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 671632
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/46r;
    .locals 1

    .prologue
    .line 671633
    const-class v0, LX/46r;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/46r;

    return-object v0
.end method

.method public static values()[LX/46r;
    .locals 1

    .prologue
    .line 671634
    sget-object v0, LX/46r;->$VALUES:[LX/46r;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/46r;

    return-object v0
.end method
