.class public LX/4f7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1cF",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 0

    .prologue
    .line 797948
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 797949
    iput-object p1, p0, LX/4f7;->a:Ljava/util/concurrent/Executor;

    .line 797950
    return-void
.end method


# virtual methods
.method public final a(LX/1cd;LX/1cW;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 797951
    iget-object v0, p2, LX/1cW;->c:LX/1BV;

    move-object v3, v0

    .line 797952
    iget-object v0, p2, LX/1cW;->b:Ljava/lang/String;

    move-object v5, v0

    .line 797953
    iget-object v0, p2, LX/1cW;->a:LX/1bf;

    move-object v6, v0

    .line 797954
    new-instance v0, Lcom/facebook/imagepipeline/producers/LocalVideoThumbnailProducer$1;

    const-string v4, "VideoThumbnailProducer"

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/facebook/imagepipeline/producers/LocalVideoThumbnailProducer$1;-><init>(LX/4f7;LX/1cd;LX/1BV;Ljava/lang/String;Ljava/lang/String;LX/1bf;)V

    .line 797955
    new-instance v1, LX/4f6;

    invoke-direct {v1, p0, v0}, LX/4f6;-><init>(LX/4f7;Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;)V

    invoke-virtual {p2, v1}, LX/1cW;->a(LX/1cg;)V

    .line 797956
    iget-object v1, p0, LX/4f7;->a:Ljava/util/concurrent/Executor;

    const v2, 0x5e92c38a

    invoke-static {v1, v0, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 797957
    return-void
.end method
