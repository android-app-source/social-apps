.class public LX/4on;
.super LX/4om;
.source ""


# instance fields
.field public final a:LX/0dN;

.field public b:Ljava/lang/CharSequence;

.field public c:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 810387
    invoke-direct {p0, p1}, LX/4om;-><init>(Landroid/content/Context;)V

    .line 810388
    new-instance v0, LX/4ol;

    invoke-direct {v0, p0}, LX/4ol;-><init>(LX/4on;)V

    iput-object v0, p0, LX/4on;->a:LX/0dN;

    .line 810389
    invoke-virtual {p0}, LX/4on;->getSummary()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, LX/4on;->b:Ljava/lang/CharSequence;

    .line 810390
    invoke-virtual {p0}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/4on;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v0, p0, LX/4on;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 810391
    return-void
.end method

.method public static b(LX/4on;)V
    .locals 2

    .prologue
    .line 810392
    invoke-virtual {p0}, LX/4on;->getText()Ljava/lang/String;

    move-result-object v0

    .line 810393
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 810394
    iget-object v0, p0, LX/4on;->b:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, LX/4on;->setSummary(Ljava/lang/CharSequence;)V

    .line 810395
    :goto_0
    return-void

    .line 810396
    :cond_0
    invoke-virtual {p0, v0}, LX/4on;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final onBindView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 810397
    invoke-static {p0}, LX/4on;->b(LX/4on;)V

    .line 810398
    invoke-super {p0, p1}, LX/4om;->onBindView(Landroid/view/View;)V

    .line 810399
    return-void
.end method
