.class public final LX/4im;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Y4;
.implements Lcom/facebook/quicklog/QuickPerformanceLoggerEventListener;


# instance fields
.field public final synthetic a:LX/0Y7;


# direct methods
.method public constructor <init>(LX/0Y7;)V
    .locals 0

    .prologue
    .line 803543
    iput-object p1, p0, LX/4im;->a:LX/0Y7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Pn;)V
    .locals 2

    .prologue
    .line 803541
    const-class v0, LX/0Y7;

    const-string v1, "Should never get called"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 803542
    return-void
.end method

.method public final b(LX/0Pn;)V
    .locals 2

    .prologue
    .line 803539
    const-class v0, LX/0Y7;

    const-string v1, "Should never get called"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 803540
    return-void
.end method

.method public final c(LX/0Pn;)V
    .locals 2

    .prologue
    .line 803532
    const-class v0, LX/0Y7;

    const-string v1, "Should never get called"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 803533
    return-void
.end method

.method public final d(LX/0Pn;)V
    .locals 2

    .prologue
    .line 803537
    const-class v0, LX/0Y7;

    const-string v1, "Should never get called"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 803538
    return-void
.end method

.method public final e(LX/0Pn;)V
    .locals 0

    .prologue
    .line 803536
    return-void
.end method

.method public final onPerformanceLoggingEvent(Lcom/facebook/quicklog/PerformanceLoggingEvent;)V
    .locals 2

    .prologue
    .line 803534
    const-class v0, LX/0Y7;

    const-string v1, "Should never get called"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 803535
    return-void
.end method
