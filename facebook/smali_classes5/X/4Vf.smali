.class public final LX/4Vf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/1MF;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:J

.field public final synthetic e:J

.field public final synthetic f:I

.field public final synthetic g:I

.field public final synthetic h:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic i:LX/4Vg;

.field public final synthetic j:LX/1dw;


# direct methods
.method public constructor <init>(LX/1dw;ZLX/1MF;Ljava/lang/String;JJIILcom/google/common/util/concurrent/SettableFuture;LX/4Vg;)V
    .locals 1

    .prologue
    .line 742757
    iput-object p1, p0, LX/4Vf;->j:LX/1dw;

    iput-boolean p2, p0, LX/4Vf;->a:Z

    iput-object p3, p0, LX/4Vf;->b:LX/1MF;

    iput-object p4, p0, LX/4Vf;->c:Ljava/lang/String;

    iput-wide p5, p0, LX/4Vf;->d:J

    iput-wide p7, p0, LX/4Vf;->e:J

    iput p9, p0, LX/4Vf;->f:I

    iput p10, p0, LX/4Vf;->g:I

    iput-object p11, p0, LX/4Vf;->h:Lcom/google/common/util/concurrent/SettableFuture;

    iput-object p12, p0, LX/4Vf;->i:LX/4Vg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 742758
    iget-object v0, p0, LX/4Vf;->j:LX/1dw;

    iget-object v0, v0, LX/1dw;->b:LX/1dx;

    invoke-virtual {v0, p1}, LX/1dx;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 742759
    new-instance v0, LX/4gv;

    invoke-direct {v0}, LX/4gv;-><init>()V

    iget-object v1, p0, LX/4Vf;->b:LX/1MF;

    invoke-virtual {v0, v1}, LX/4gv;->a(LX/1MF;)LX/4gv;

    move-result-object v0

    iget-object v1, p0, LX/4Vf;->c:Ljava/lang/String;

    .line 742760
    iput-object v1, v0, LX/3G2;->a:Ljava/lang/String;

    .line 742761
    move-object v0, v0

    .line 742762
    iget-wide v2, p0, LX/4Vf;->d:J

    .line 742763
    iput-wide v2, v0, LX/3G2;->c:J

    .line 742764
    move-object v0, v0

    .line 742765
    iget-wide v2, p0, LX/4Vf;->e:J

    .line 742766
    iput-wide v2, v0, LX/3G2;->d:J

    .line 742767
    move-object v0, v0

    .line 742768
    iget v1, p0, LX/4Vf;->f:I

    add-int/lit8 v1, v1, 0x1

    .line 742769
    iput v1, v0, LX/3G2;->e:I

    .line 742770
    move-object v0, v0

    .line 742771
    iget v1, p0, LX/4Vf;->g:I

    .line 742772
    iput v1, v0, LX/3G2;->f:I

    .line 742773
    move-object v0, v0

    .line 742774
    invoke-virtual {v0}, LX/3G2;->a()LX/3G3;

    move-result-object v1

    .line 742775
    iget-object v0, p0, LX/4Vf;->j:LX/1dw;

    iget-object v0, v0, LX/1dw;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Kz;

    .line 742776
    iget-object v2, v0, LX/2Kz;->o:LX/2L7;

    iget-object v3, v1, LX/3G3;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/2L7;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 742777
    iget-object v2, v0, LX/2Kz;->o:LX/2L7;

    iget-object v3, v1, LX/3G3;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/2L7;->remove(Ljava/lang/Object;)Z

    .line 742778
    :cond_0
    :goto_0
    iget-object v0, p0, LX/4Vf;->i:LX/4Vg;

    sget-object v1, LX/4Vg;->THROW_CUSTOM_EXCEPTION:LX/4Vg;

    if-ne v0, v1, :cond_2

    .line 742779
    iget-object v0, p0, LX/4Vf;->h:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v1, LX/4gu;

    iget-object v2, p0, LX/4Vf;->b:LX/1MF;

    invoke-direct {v1, v2, p1}, LX/4gu;-><init>(LX/1MF;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 742780
    :cond_1
    :goto_1
    return-void

    .line 742781
    :cond_2
    iget-object v0, p0, LX/4Vf;->i:LX/4Vg;

    sget-object v1, LX/4Vg;->FAKE_SUCCESS:LX/4Vg;

    if-ne v0, v1, :cond_1

    .line 742782
    iget-object v0, p0, LX/4Vf;->h:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    const v2, -0x6070bcd3

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    goto :goto_1

    .line 742783
    :cond_3
    iget-object v0, p0, LX/4Vf;->h:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_1

    .line 742784
    :cond_4
    iget-object v2, v0, LX/2Kz;->l:LX/2L0;

    invoke-virtual {v2, v1}, LX/2L0;->a(LX/3G3;)V

    .line 742785
    iget-object v2, v0, LX/2Kz;->d:LX/1dx;

    invoke-virtual {v2, v1}, LX/1dx;->a(LX/3G3;)V

    .line 742786
    iget-object v2, v0, LX/2Kz;->j:LX/0sT;

    invoke-virtual {v2}, LX/0sT;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/2Kz;->i:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 742787
    invoke-static {v0}, LX/2Kz;->c(LX/2Kz;)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 742788
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 742789
    iget-boolean v0, p0, LX/4Vf;->a:Z

    if-nez v0, :cond_0

    .line 742790
    new-instance v0, LX/4gv;

    invoke-direct {v0}, LX/4gv;-><init>()V

    iget-object v1, p0, LX/4Vf;->b:LX/1MF;

    invoke-virtual {v0, v1}, LX/4gv;->a(LX/1MF;)LX/4gv;

    move-result-object v0

    iget-object v1, p0, LX/4Vf;->c:Ljava/lang/String;

    .line 742791
    iput-object v1, v0, LX/3G2;->a:Ljava/lang/String;

    .line 742792
    move-object v0, v0

    .line 742793
    iget-wide v2, p0, LX/4Vf;->d:J

    .line 742794
    iput-wide v2, v0, LX/3G2;->c:J

    .line 742795
    move-object v0, v0

    .line 742796
    iget-wide v2, p0, LX/4Vf;->e:J

    .line 742797
    iput-wide v2, v0, LX/3G2;->d:J

    .line 742798
    move-object v0, v0

    .line 742799
    iget v1, p0, LX/4Vf;->f:I

    .line 742800
    iput v1, v0, LX/3G2;->e:I

    .line 742801
    move-object v0, v0

    .line 742802
    iget v1, p0, LX/4Vf;->g:I

    .line 742803
    iput v1, v0, LX/3G2;->f:I

    .line 742804
    move-object v0, v0

    .line 742805
    invoke-virtual {v0}, LX/3G2;->a()LX/3G3;

    move-result-object v0

    .line 742806
    iget-object v1, p0, LX/4Vf;->j:LX/1dw;

    iget-object v1, v1, LX/1dw;->b:LX/1dx;

    invoke-virtual {v1, v0}, LX/1dx;->c(LX/3G3;)V

    .line 742807
    :cond_0
    iget-object v0, p0, LX/4Vf;->h:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, 0x30c59a7a

    invoke-static {v0, p1, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 742808
    return-void
.end method
