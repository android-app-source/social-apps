.class public final LX/4BT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Landroid/widget/ListView;

.field public final synthetic b:LX/4BW;

.field public final synthetic c:LX/31a;


# direct methods
.method public constructor <init>(LX/31a;Landroid/widget/ListView;LX/4BW;)V
    .locals 0

    .prologue
    .line 677745
    iput-object p1, p0, LX/4BT;->c:LX/31a;

    iput-object p2, p0, LX/4BT;->a:Landroid/widget/ListView;

    iput-object p3, p0, LX/4BT;->b:LX/4BW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    .line 677741
    iget-object v0, p0, LX/4BT;->c:LX/31a;

    iget-object v0, v0, LX/31a;->E:[Z

    if-eqz v0, :cond_0

    .line 677742
    iget-object v0, p0, LX/4BT;->c:LX/31a;

    iget-object v0, v0, LX/31a;->E:[Z

    iget-object v1, p0, LX/4BT;->a:Landroid/widget/ListView;

    invoke-virtual {v1, p3}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v1

    aput-boolean v1, v0, p3

    .line 677743
    :cond_0
    iget-object v0, p0, LX/4BT;->c:LX/31a;

    iget-object v0, v0, LX/31a;->I:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

    iget-object v1, p0, LX/4BT;->b:LX/4BW;

    iget-object v1, v1, LX/4BW;->b:Landroid/content/DialogInterface;

    iget-object v2, p0, LX/4BT;->a:Landroid/widget/ListView;

    invoke-virtual {v2, p3}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v2

    invoke-interface {v0, v1, p3, v2}, Landroid/content/DialogInterface$OnMultiChoiceClickListener;->onClick(Landroid/content/DialogInterface;IZ)V

    .line 677744
    return-void
.end method
