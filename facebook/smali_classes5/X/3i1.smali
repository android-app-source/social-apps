.class public LX/3i1;
.super LX/2y5;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "LX/2y5",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static k:LX/0Xm;


# instance fields
.field public final b:LX/1nA;

.field private final c:LX/1Nt;

.field private final d:LX/2yS;

.field public final e:LX/3i2;

.field public final f:LX/3i3;

.field public final g:LX/0wM;

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/2yT;

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3i4;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 628327
    const-class v0, LX/3i1;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3i1;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1nA;LX/2yS;LX/3i2;LX/3i3;LX/0wM;LX/0Ot;LX/2yT;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nA;",
            "LX/2yS;",
            "LX/3i2;",
            "LX/3i3;",
            "LX/0wM;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;",
            "LX/2yT;",
            "LX/0Ot",
            "<",
            "LX/3i4;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 628316
    invoke-direct {p0}, LX/2y5;-><init>()V

    .line 628317
    iput-object p1, p0, LX/3i1;->b:LX/1nA;

    .line 628318
    iput-object p2, p0, LX/3i1;->d:LX/2yS;

    .line 628319
    iput-object p3, p0, LX/3i1;->e:LX/3i2;

    .line 628320
    iput-object p4, p0, LX/3i1;->f:LX/3i3;

    .line 628321
    iput-object p5, p0, LX/3i1;->g:LX/0wM;

    .line 628322
    iput-object p6, p0, LX/3i1;->h:LX/0Ot;

    .line 628323
    iput-object p7, p0, LX/3i1;->i:LX/2yT;

    .line 628324
    new-instance v0, Lcom/facebook/attachments/angora/actionbutton/LinkOpenActionButton$LinkOpenActionButtonPartDefinition;

    invoke-direct {v0, p0}, Lcom/facebook/attachments/angora/actionbutton/LinkOpenActionButton$LinkOpenActionButtonPartDefinition;-><init>(LX/3i1;)V

    iput-object v0, p0, LX/3i1;->c:LX/1Nt;

    .line 628325
    iput-object p8, p0, LX/3i1;->j:LX/0Ot;

    .line 628326
    return-void
.end method

.method public static a(LX/0QB;)LX/3i1;
    .locals 12

    .prologue
    .line 628341
    const-class v1, LX/3i1;

    monitor-enter v1

    .line 628342
    :try_start_0
    sget-object v0, LX/3i1;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 628343
    sput-object v2, LX/3i1;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 628344
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 628345
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 628346
    new-instance v3, LX/3i1;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v4

    check-cast v4, LX/1nA;

    invoke-static {v0}, LX/2yS;->a(LX/0QB;)LX/2yS;

    move-result-object v5

    check-cast v5, LX/2yS;

    const-class v6, LX/3i2;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/3i2;

    const-class v7, LX/3i3;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/3i3;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v8

    check-cast v8, LX/0wM;

    const/16 v9, 0xac0

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const-class v10, LX/2yT;

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/2yT;

    const/16 v11, 0x1ff

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-direct/range {v3 .. v11}, LX/3i1;-><init>(LX/1nA;LX/2yS;LX/3i2;LX/3i3;LX/0wM;LX/0Ot;LX/2yT;LX/0Ot;)V

    .line 628347
    move-object v0, v3

    .line 628348
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 628349
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3i1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 628350
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 628351
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z
    .locals 2

    .prologue
    .line 628352
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ae()Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->BUTTON_WITH_TEXT_ONLY:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ae()Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->VIDEO_DR_STYLE:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Nt;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ":",
            "LX/35p;",
            ">()",
            "LX/1Nt",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;*TE;TV;>;"
        }
    .end annotation

    .prologue
    .line 628340
    iget-object v0, p0, LX/3i1;->c:LX/1Nt;

    return-object v0
.end method

.method public final a(LX/1De;LX/1PW;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/AE0;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;Z)",
            "LX/AE0;"
        }
    .end annotation

    .prologue
    .line 628328
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 628329
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const v1, -0x1e53800c

    invoke-static {v0, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 628330
    invoke-static {v1}, LX/3i1;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v0

    .line 628331
    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v0

    .line 628332
    :goto_0
    iget-object v2, p0, LX/3i1;->i:LX/2yT;

    const/4 v3, 0x2

    iget-object v4, p0, LX/3i1;->d:LX/2yS;

    invoke-virtual {v2, p4, v3, v4}, LX/2yT;->a(ZILX/2yS;)LX/AE0;

    move-result-object v2

    .line 628333
    iput-object v0, v2, LX/AE0;->f:Ljava/lang/CharSequence;

    .line 628334
    move-object v0, v2

    .line 628335
    iget-object v2, p0, LX/3i1;->b:LX/1nA;

    invoke-virtual {v2, p3, v1}, LX/1nA;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Landroid/view/View$OnClickListener;

    move-result-object v1

    .line 628336
    iput-object v1, v0, LX/AE0;->o:Landroid/view/View$OnClickListener;

    .line 628337
    move-object v0, v0

    .line 628338
    return-object v0

    .line 628339
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
