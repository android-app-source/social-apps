.class public final LX/4BA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:LX/1qD;

.field public final synthetic val$operationHolder:LX/1qF;


# direct methods
.method public constructor <init>(LX/1qD;LX/1qF;)V
    .locals 0

    .prologue
    .line 677460
    iput-object p1, p0, LX/4BA;->this$0:LX/1qD;

    iput-object p2, p0, LX/4BA;->val$operationHolder:LX/1qF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 677461
    iget-object v0, p0, LX/4BA;->this$0:LX/1qD;

    iget-object v0, v0, LX/1qD;->mHandlerExecutorService:LX/0Te;

    invoke-interface {v0}, LX/0Te;->b()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 677462
    iget-object v0, p0, LX/4BA;->this$0:LX/1qD;

    iget-object v1, p0, LX/4BA;->val$operationHolder:LX/1qF;

    invoke-static {p1}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v2

    invoke-static {p1}, LX/3d6;->bundleForException(Ljava/lang/Throwable;)Landroid/os/Bundle;

    move-result-object v3

    invoke-static {v2, v3, p1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Landroid/os/Bundle;Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/1qD;->operationDone(LX/1qD;LX/1qF;Lcom/facebook/fbservice/service/OperationResult;)V

    .line 677463
    iget-object v0, p0, LX/4BA;->this$0:LX/1qD;

    const/4 v1, 0x0

    .line 677464
    iput-object v1, v0, LX/1qD;->mCurrentDeferredOperationHolder:LX/1qF;

    .line 677465
    iget-object v0, p0, LX/4BA;->this$0:LX/1qD;

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, LX/1qD;->processQueueOnHandler(LX/1qD;J)V

    .line 677466
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 677467
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 677468
    iget-object v0, p0, LX/4BA;->this$0:LX/1qD;

    iget-object v0, v0, LX/1qD;->mHandlerExecutorService:LX/0Te;

    invoke-interface {v0}, LX/0Te;->b()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 677469
    iget-object v0, p0, LX/4BA;->this$0:LX/1qD;

    iget-object v1, p0, LX/4BA;->val$operationHolder:LX/1qF;

    invoke-static {v0, v1, p1}, LX/1qD;->operationDone(LX/1qD;LX/1qF;Lcom/facebook/fbservice/service/OperationResult;)V

    .line 677470
    iget-object v0, p0, LX/4BA;->this$0:LX/1qD;

    const/4 v1, 0x0

    .line 677471
    iput-object v1, v0, LX/1qD;->mCurrentDeferredOperationHolder:LX/1qF;

    .line 677472
    iget-object v0, p0, LX/4BA;->this$0:LX/1qD;

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, LX/1qD;->processQueueOnHandler(LX/1qD;J)V

    .line 677473
    return-void
.end method
