.class public LX/4gS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/4gS;


# instance fields
.field public final a:LX/0if;

.field public final b:LX/0ih;


# direct methods
.method public constructor <init>(LX/0if;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 799622
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799623
    iput-object p1, p0, LX/4gS;->a:LX/0if;

    .line 799624
    sget-object v0, LX/0ig;->as:LX/0ih;

    iput-object v0, p0, LX/4gS;->b:LX/0ih;

    .line 799625
    return-void
.end method

.method public static a(LX/0QB;)LX/4gS;
    .locals 4

    .prologue
    .line 799626
    sget-object v0, LX/4gS;->c:LX/4gS;

    if-nez v0, :cond_1

    .line 799627
    const-class v1, LX/4gS;

    monitor-enter v1

    .line 799628
    :try_start_0
    sget-object v0, LX/4gS;->c:LX/4gS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 799629
    if-eqz v2, :cond_0

    .line 799630
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 799631
    new-instance p0, LX/4gS;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-direct {p0, v3}, LX/4gS;-><init>(LX/0if;)V

    .line 799632
    move-object v0, p0

    .line 799633
    sput-object v0, LX/4gS;->c:LX/4gS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 799634
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 799635
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 799636
    :cond_1
    sget-object v0, LX/4gS;->c:LX/4gS;

    return-object v0

    .line 799637
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 799638
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static e(LX/4gS;)V
    .locals 2

    .prologue
    .line 799639
    iget-object v0, p0, LX/4gS;->a:LX/0if;

    sget-object v1, LX/0ig;->as:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 799640
    return-void
.end method
