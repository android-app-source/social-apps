.class public LX/4Ko;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 680708
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 40

    .prologue
    .line 680808
    const/16 v34, 0x0

    .line 680809
    const/16 v33, 0x0

    .line 680810
    const/16 v32, 0x0

    .line 680811
    const/16 v31, 0x0

    .line 680812
    const/16 v30, 0x0

    .line 680813
    const/16 v29, 0x0

    .line 680814
    const/16 v28, 0x0

    .line 680815
    const/16 v25, 0x0

    .line 680816
    const-wide/16 v26, 0x0

    .line 680817
    const/16 v24, 0x0

    .line 680818
    const/16 v23, 0x0

    .line 680819
    const/16 v22, 0x0

    .line 680820
    const/16 v21, 0x0

    .line 680821
    const/16 v20, 0x0

    .line 680822
    const/16 v19, 0x0

    .line 680823
    const/16 v18, 0x0

    .line 680824
    const-wide/16 v16, 0x0

    .line 680825
    const/4 v15, 0x0

    .line 680826
    const/4 v14, 0x0

    .line 680827
    const/4 v13, 0x0

    .line 680828
    const/4 v12, 0x0

    .line 680829
    const/4 v11, 0x0

    .line 680830
    const/4 v10, 0x0

    .line 680831
    const/4 v9, 0x0

    .line 680832
    const/4 v8, 0x0

    .line 680833
    const/4 v7, 0x0

    .line 680834
    const/4 v6, 0x0

    .line 680835
    const/4 v5, 0x0

    .line 680836
    const/4 v4, 0x0

    .line 680837
    const/4 v3, 0x0

    .line 680838
    const/4 v2, 0x0

    .line 680839
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v35

    sget-object v36, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    if-eq v0, v1, :cond_21

    .line 680840
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 680841
    const/4 v2, 0x0

    .line 680842
    :goto_0
    return v2

    .line 680843
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v36, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v36

    if-eq v2, v0, :cond_19

    .line 680844
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 680845
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 680846
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v36

    sget-object v37, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 680847
    const-string v36, "album_cover_photo"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_1

    .line 680848
    invoke-static/range {p0 .. p1}, LX/2sY;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v35, v2

    goto :goto_1

    .line 680849
    :cond_1
    const-string v36, "album_type"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_2

    .line 680850
    const/4 v2, 0x1

    .line 680851
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v11

    move-object/from16 v34, v11

    move v11, v2

    goto :goto_1

    .line 680852
    :cond_2
    const-string v36, "allow_contributors"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_3

    .line 680853
    const/4 v2, 0x1

    .line 680854
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    move/from16 v33, v10

    move v10, v2

    goto :goto_1

    .line 680855
    :cond_3
    const-string v36, "application"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_4

    .line 680856
    invoke-static/range {p0 .. p1}, LX/2uk;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v32, v2

    goto :goto_1

    .line 680857
    :cond_4
    const-string v36, "can_edit_caption"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_5

    .line 680858
    const/4 v2, 0x1

    .line 680859
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    move/from16 v31, v9

    move v9, v2

    goto :goto_1

    .line 680860
    :cond_5
    const-string v36, "can_upload"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_6

    .line 680861
    const/4 v2, 0x1

    .line 680862
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v30, v7

    move v7, v2

    goto/16 :goto_1

    .line 680863
    :cond_6
    const-string v36, "can_viewer_delete"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_7

    .line 680864
    const/4 v2, 0x1

    .line 680865
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v29, v6

    move v6, v2

    goto/16 :goto_1

    .line 680866
    :cond_7
    const-string v36, "contributors"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_8

    .line 680867
    invoke-static/range {p0 .. p1}, LX/2bM;->b(LX/15w;LX/186;)I

    move-result v2

    move/from16 v28, v2

    goto/16 :goto_1

    .line 680868
    :cond_8
    const-string v36, "created_time"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_9

    .line 680869
    const/4 v2, 0x1

    .line 680870
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 680871
    :cond_9
    const-string v36, "explicit_place"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_a

    .line 680872
    invoke-static/range {p0 .. p1}, LX/2um;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v27, v2

    goto/16 :goto_1

    .line 680873
    :cond_a
    const-string v36, "feedback"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_b

    .line 680874
    invoke-static/range {p0 .. p1}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v26, v2

    goto/16 :goto_1

    .line 680875
    :cond_b
    const-string v36, "id"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_c

    .line 680876
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v25, v2

    goto/16 :goto_1

    .line 680877
    :cond_c
    const-string v36, "media"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_d

    .line 680878
    invoke-static/range {p0 .. p1}, LX/4PO;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 680879
    :cond_d
    const-string v36, "media_list"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_e

    .line 680880
    invoke-static/range {p0 .. p1}, LX/4PO;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 680881
    :cond_e
    const-string v36, "media_owner_object"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_f

    .line 680882
    invoke-static/range {p0 .. p1}, LX/2aw;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 680883
    :cond_f
    const-string v36, "message"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_10

    .line 680884
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 680885
    :cond_10
    const-string v36, "modified_time"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_11

    .line 680886
    const/4 v2, 0x1

    .line 680887
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v20

    move v8, v2

    goto/16 :goto_1

    .line 680888
    :cond_11
    const-string v36, "name"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_12

    .line 680889
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 680890
    :cond_12
    const-string v36, "owner"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_13

    .line 680891
    invoke-static/range {p0 .. p1}, LX/2bM;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 680892
    :cond_13
    const-string v36, "photo_items"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_14

    .line 680893
    invoke-static/range {p0 .. p1}, LX/4PO;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 680894
    :cond_14
    const-string v36, "privacy_scope"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_15

    .line 680895
    invoke-static/range {p0 .. p1}, LX/2bo;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 680896
    :cond_15
    const-string v36, "title"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_16

    .line 680897
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 680898
    :cond_16
    const-string v36, "titleForSummary"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_17

    .line 680899
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 680900
    :cond_17
    const-string v36, "url"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 680901
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 680902
    :cond_18
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 680903
    :cond_19
    const/16 v2, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 680904
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 680905
    if-eqz v11, :cond_1a

    .line 680906
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 680907
    :cond_1a
    if-eqz v10, :cond_1b

    .line 680908
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 680909
    :cond_1b
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 680910
    if-eqz v9, :cond_1c

    .line 680911
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 680912
    :cond_1c
    if-eqz v7, :cond_1d

    .line 680913
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 680914
    :cond_1d
    if-eqz v6, :cond_1e

    .line 680915
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 680916
    :cond_1e
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 680917
    if-eqz v3, :cond_1f

    .line 680918
    const/16 v3, 0x9

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 680919
    :cond_1f
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 680920
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 680921
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 680922
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 680923
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 680924
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 680925
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 680926
    if-eqz v8, :cond_20

    .line 680927
    const/16 v3, 0x11

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v20

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 680928
    :cond_20
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 680929
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 680930
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 680931
    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 680932
    const/16 v2, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 680933
    const/16 v2, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 680934
    const/16 v2, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 680935
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_21
    move/from16 v35, v34

    move-object/from16 v34, v33

    move/from16 v33, v32

    move/from16 v32, v31

    move/from16 v31, v30

    move/from16 v30, v29

    move/from16 v29, v28

    move/from16 v28, v25

    move/from16 v25, v22

    move/from16 v22, v19

    move/from16 v19, v18

    move/from16 v18, v15

    move v15, v12

    move v12, v9

    move v9, v6

    move v6, v4

    move/from16 v38, v20

    move/from16 v39, v21

    move-wide/from16 v20, v16

    move/from16 v16, v13

    move/from16 v17, v14

    move v13, v10

    move v14, v11

    move v11, v8

    move v10, v7

    move v7, v5

    move v8, v2

    move-wide/from16 v4, v26

    move/from16 v27, v24

    move/from16 v26, v23

    move/from16 v23, v38

    move/from16 v24, v39

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const-wide/16 v4, 0x0

    .line 680709
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 680710
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 680711
    if-eqz v0, :cond_0

    .line 680712
    const-string v1, "album_cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680713
    invoke-static {p0, v0, p2, p3}, LX/2sY;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 680714
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->a(IIS)S

    move-result v0

    .line 680715
    if-eqz v0, :cond_1

    .line 680716
    const-string v0, "album_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680717
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 680718
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 680719
    if-eqz v0, :cond_2

    .line 680720
    const-string v1, "allow_contributors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680721
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 680722
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 680723
    if-eqz v0, :cond_3

    .line 680724
    const-string v1, "application"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680725
    invoke-static {p0, v0, p2, p3}, LX/2uk;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 680726
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 680727
    if-eqz v0, :cond_4

    .line 680728
    const-string v1, "can_edit_caption"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680729
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 680730
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 680731
    if-eqz v0, :cond_5

    .line 680732
    const-string v1, "can_upload"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680733
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 680734
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 680735
    if-eqz v0, :cond_6

    .line 680736
    const-string v1, "can_viewer_delete"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680737
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 680738
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 680739
    if-eqz v0, :cond_7

    .line 680740
    const-string v1, "contributors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680741
    invoke-static {p0, v0, p2, p3}, LX/2bM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 680742
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 680743
    cmp-long v2, v0, v4

    if-eqz v2, :cond_8

    .line 680744
    const-string v2, "created_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680745
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 680746
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 680747
    if-eqz v0, :cond_9

    .line 680748
    const-string v1, "explicit_place"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680749
    invoke-static {p0, v0, p2, p3}, LX/2um;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 680750
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 680751
    if-eqz v0, :cond_a

    .line 680752
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680753
    invoke-static {p0, v0, p2, p3}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 680754
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 680755
    if-eqz v0, :cond_b

    .line 680756
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680757
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 680758
    :cond_b
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 680759
    if-eqz v0, :cond_c

    .line 680760
    const-string v1, "media"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680761
    invoke-static {p0, v0, p2, p3}, LX/4PO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 680762
    :cond_c
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 680763
    if-eqz v0, :cond_d

    .line 680764
    const-string v1, "media_list"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680765
    invoke-static {p0, v0, p2, p3}, LX/4PO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 680766
    :cond_d
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 680767
    if-eqz v0, :cond_e

    .line 680768
    const-string v1, "media_owner_object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680769
    invoke-static {p0, v0, p2, p3}, LX/2aw;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 680770
    :cond_e
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 680771
    if-eqz v0, :cond_f

    .line 680772
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680773
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 680774
    :cond_f
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 680775
    cmp-long v2, v0, v4

    if-eqz v2, :cond_10

    .line 680776
    const-string v2, "modified_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680777
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 680778
    :cond_10
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 680779
    if-eqz v0, :cond_11

    .line 680780
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680781
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 680782
    :cond_11
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 680783
    if-eqz v0, :cond_12

    .line 680784
    const-string v1, "owner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680785
    invoke-static {p0, v0, p2, p3}, LX/2bM;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 680786
    :cond_12
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 680787
    if-eqz v0, :cond_13

    .line 680788
    const-string v1, "photo_items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680789
    invoke-static {p0, v0, p2, p3}, LX/4PO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 680790
    :cond_13
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 680791
    if-eqz v0, :cond_14

    .line 680792
    const-string v1, "privacy_scope"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680793
    invoke-static {p0, v0, p2, p3}, LX/2bo;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 680794
    :cond_14
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 680795
    if-eqz v0, :cond_15

    .line 680796
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680797
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 680798
    :cond_15
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 680799
    if-eqz v0, :cond_16

    .line 680800
    const-string v1, "titleForSummary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680801
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 680802
    :cond_16
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 680803
    if-eqz v0, :cond_17

    .line 680804
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680805
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 680806
    :cond_17
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 680807
    return-void
.end method
