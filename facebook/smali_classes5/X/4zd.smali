.class public final LX/4zd;
.super LX/4za;
.source ""

# interfaces
.implements LX/0qF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/4za",
        "<TK;TV;>;",
        "LX/0qF",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public volatile e:J

.field public f:LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public g:LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public h:LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public i:LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;ILX/0qF;)V
    .locals 2
    .param p3    # LX/0qF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822990
    invoke-direct {p0, p1, p2, p3}, LX/4za;-><init>(Ljava/lang/Object;ILX/0qF;)V

    .line 822991
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LX/4zd;->e:J

    .line 822992
    sget-object v0, LX/4zX;->INSTANCE:LX/4zX;

    move-object v0, v0

    .line 822993
    iput-object v0, p0, LX/4zd;->f:LX/0qF;

    .line 822994
    sget-object v0, LX/4zX;->INSTANCE:LX/4zX;

    move-object v0, v0

    .line 822995
    iput-object v0, p0, LX/4zd;->g:LX/0qF;

    .line 822996
    sget-object v0, LX/4zX;->INSTANCE:LX/4zX;

    move-object v0, v0

    .line 822997
    iput-object v0, p0, LX/4zd;->h:LX/0qF;

    .line 822998
    sget-object v0, LX/4zX;->INSTANCE:LX/4zX;

    move-object v0, v0

    .line 822999
    iput-object v0, p0, LX/4zd;->i:LX/0qF;

    .line 823000
    return-void
.end method


# virtual methods
.method public final getExpirationTime()J
    .locals 2

    .prologue
    .line 822989
    iget-wide v0, p0, LX/4zd;->e:J

    return-wide v0
.end method

.method public final getNextEvictable()LX/0qF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 822980
    iget-object v0, p0, LX/4zd;->h:LX/0qF;

    return-object v0
.end method

.method public final getNextExpirable()LX/0qF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 822988
    iget-object v0, p0, LX/4zd;->f:LX/0qF;

    return-object v0
.end method

.method public final getPreviousEvictable()LX/0qF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 822987
    iget-object v0, p0, LX/4zd;->i:LX/0qF;

    return-object v0
.end method

.method public final getPreviousExpirable()LX/0qF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 823001
    iget-object v0, p0, LX/4zd;->g:LX/0qF;

    return-object v0
.end method

.method public final setExpirationTime(J)V
    .locals 1

    .prologue
    .line 822985
    iput-wide p1, p0, LX/4zd;->e:J

    .line 822986
    return-void
.end method

.method public final setNextEvictable(LX/0qF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822983
    iput-object p1, p0, LX/4zd;->h:LX/0qF;

    .line 822984
    return-void
.end method

.method public final setNextExpirable(LX/0qF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822981
    iput-object p1, p0, LX/4zd;->f:LX/0qF;

    .line 822982
    return-void
.end method

.method public final setPreviousEvictable(LX/0qF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822978
    iput-object p1, p0, LX/4zd;->i:LX/0qF;

    .line 822979
    return-void
.end method

.method public final setPreviousExpirable(LX/0qF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822976
    iput-object p1, p0, LX/4zd;->g:LX/0qF;

    .line 822977
    return-void
.end method
