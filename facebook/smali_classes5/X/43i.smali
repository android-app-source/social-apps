.class public final LX/43i;
.super LX/0T1;
.source ""


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/0T2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0T2;)V
    .locals 1

    .prologue
    .line 669253
    invoke-direct {p0}, LX/0T1;-><init>()V

    .line 669254
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/43i;->a:Ljava/lang/ref/WeakReference;

    .line 669255
    return-void
.end method

.method private g(Landroid/app/Activity;)LX/0T2;
    .locals 2

    .prologue
    .line 669256
    iget-object v0, p0, LX/43i;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T2;

    .line 669257
    if-nez v0, :cond_0

    .line 669258
    instance-of v1, p1, LX/0ex;

    invoke-static {v1}, LX/03g;->a(Z)V

    .line 669259
    check-cast p1, LX/0ex;

    .line 669260
    invoke-interface {p1, p0}, LX/0ex;->b(LX/0T2;)V

    .line 669261
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 669245
    invoke-direct {p0, p1}, LX/43i;->g(Landroid/app/Activity;)LX/0T2;

    move-result-object v0

    .line 669246
    if-eqz v0, :cond_0

    .line 669247
    invoke-interface {v0, p1}, LX/0T2;->a(Landroid/app/Activity;)V

    .line 669248
    :cond_0
    return-void
.end method

.method public final b(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 669249
    invoke-direct {p0, p1}, LX/43i;->g(Landroid/app/Activity;)LX/0T2;

    move-result-object v0

    .line 669250
    if-eqz v0, :cond_0

    .line 669251
    invoke-interface {v0, p1}, LX/0T2;->b(Landroid/app/Activity;)V

    .line 669252
    :cond_0
    return-void
.end method

.method public final c(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 669229
    invoke-direct {p0, p1}, LX/43i;->g(Landroid/app/Activity;)LX/0T2;

    move-result-object v0

    .line 669230
    if-eqz v0, :cond_0

    .line 669231
    invoke-interface {v0, p1}, LX/0T2;->c(Landroid/app/Activity;)V

    .line 669232
    :cond_0
    return-void
.end method

.method public final d(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 669241
    invoke-direct {p0, p1}, LX/43i;->g(Landroid/app/Activity;)LX/0T2;

    move-result-object v0

    .line 669242
    if-eqz v0, :cond_0

    .line 669243
    invoke-interface {v0, p1}, LX/0T2;->d(Landroid/app/Activity;)V

    .line 669244
    :cond_0
    return-void
.end method

.method public final e(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 669233
    invoke-direct {p0, p1}, LX/43i;->g(Landroid/app/Activity;)LX/0T2;

    move-result-object v0

    .line 669234
    if-eqz v0, :cond_0

    .line 669235
    invoke-interface {v0, p1}, LX/0T2;->e(Landroid/app/Activity;)V

    .line 669236
    :cond_0
    return-void
.end method

.method public final f(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 669237
    invoke-direct {p0, p1}, LX/43i;->g(Landroid/app/Activity;)LX/0T2;

    move-result-object v0

    .line 669238
    if-eqz v0, :cond_0

    .line 669239
    invoke-interface {v0, p1}, LX/0T2;->f(Landroid/app/Activity;)V

    .line 669240
    :cond_0
    return-void
.end method
