.class public LX/3fl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final p:Ljava/lang/Object;


# instance fields
.field private final b:LX/0SG;

.field private final c:LX/3fX;

.field public final d:LX/2It;

.field private final e:LX/3fg;

.field private final f:LX/3fm;

.field private final g:LX/3fo;

.field private final h:LX/0Xl;

.field private final i:LX/11H;

.field public final j:LX/0W9;

.field public final k:LX/2JB;

.field private final l:LX/03V;

.field private final m:LX/3fp;

.field private final n:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Jp;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 623384
    const-class v0, LX/3fl;

    sput-object v0, LX/3fl;->a:Ljava/lang/Class;

    .line 623385
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/3fl;->p:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/3fX;LX/2It;LX/3fg;LX/3fm;LX/3fo;LX/0Xl;LX/11H;LX/0W9;LX/2JB;LX/03V;LX/3fp;LX/0Or;LX/0Uh;)V
    .locals 0
    .param p7    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/3fX;",
            "LX/2It;",
            "LX/3fg;",
            "LX/3fm;",
            "LX/3fo;",
            "LX/0Xl;",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "LX/0W9;",
            "LX/2JB;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/3fp;",
            "LX/0Or",
            "<",
            "LX/2Jp;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 623368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 623369
    iput-object p1, p0, LX/3fl;->b:LX/0SG;

    .line 623370
    iput-object p2, p0, LX/3fl;->c:LX/3fX;

    .line 623371
    iput-object p3, p0, LX/3fl;->d:LX/2It;

    .line 623372
    iput-object p4, p0, LX/3fl;->e:LX/3fg;

    .line 623373
    iput-object p5, p0, LX/3fl;->f:LX/3fm;

    .line 623374
    iput-object p6, p0, LX/3fl;->g:LX/3fo;

    .line 623375
    iput-object p7, p0, LX/3fl;->h:LX/0Xl;

    .line 623376
    iput-object p8, p0, LX/3fl;->i:LX/11H;

    .line 623377
    iput-object p9, p0, LX/3fl;->j:LX/0W9;

    .line 623378
    iput-object p10, p0, LX/3fl;->k:LX/2JB;

    .line 623379
    iput-object p11, p0, LX/3fl;->l:LX/03V;

    .line 623380
    iput-object p12, p0, LX/3fl;->m:LX/3fp;

    .line 623381
    iput-object p13, p0, LX/3fl;->n:LX/0Or;

    .line 623382
    iput-object p14, p0, LX/3fl;->o:LX/0Uh;

    .line 623383
    return-void
.end method

.method public static a(LX/0QB;)LX/3fl;
    .locals 7

    .prologue
    .line 623341
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 623342
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 623343
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 623344
    if-nez v1, :cond_0

    .line 623345
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 623346
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 623347
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 623348
    sget-object v1, LX/3fl;->p:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 623349
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 623350
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 623351
    :cond_1
    if-nez v1, :cond_4

    .line 623352
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 623353
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 623354
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/3fl;->b(LX/0QB;)LX/3fl;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 623355
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 623356
    if-nez v1, :cond_2

    .line 623357
    sget-object v0, LX/3fl;->p:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3fl;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 623358
    :goto_1
    if-eqz v0, :cond_3

    .line 623359
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 623360
    :goto_3
    check-cast v0, LX/3fl;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 623361
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 623362
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 623363
    :catchall_1
    move-exception v0

    .line 623364
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 623365
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 623366
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 623367
    :cond_2
    :try_start_8
    sget-object v0, LX/3fl;->p:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3fl;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static a(LX/3fl;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;
    .locals 9

    .prologue
    const/16 v6, 0x32

    .line 623311
    const/4 v0, 0x0

    move v1, v0

    .line 623312
    :cond_0
    const-string v0, "syncContactsDelta (%d contacts)"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, -0x1786f31c

    invoke-static {v0, v2, v3}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 623313
    :try_start_0
    new-instance v0, Lcom/facebook/contacts/server/FetchDeltaContactsParams;

    const/16 v2, 0x32

    invoke-direct {v0, v2, p1}, Lcom/facebook/contacts/server/FetchDeltaContactsParams;-><init>(ILjava/lang/String;)V

    .line 623314
    new-instance v2, LX/14U;

    invoke-direct {v2}, LX/14U;-><init>()V

    .line 623315
    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    .line 623316
    iput-object v3, v2, LX/14U;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 623317
    iget-object v3, p0, LX/3fl;->i:LX/11H;

    iget-object v4, p0, LX/3fl;->g:LX/3fo;

    invoke-virtual {v3, v4, v0, v2, p2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/server/FetchDeltaContactsResult;

    .line 623318
    iget-object v2, v0, Lcom/facebook/contacts/server/FetchDeltaContactsResult;->a:LX/0Px;

    move-object v2, v2

    .line 623319
    iget-object v3, p0, LX/3fl;->e:LX/3fg;

    sget-object v4, LX/3gm;->INSERT:LX/3gm;

    sget-object v5, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-virtual {v3, v2, v4, v5}, LX/3fg;->a(LX/0Py;LX/3gm;LX/0ta;)V

    .line 623320
    invoke-virtual {v2}, LX/0Py;->size()I

    move-result v2

    add-int/2addr v1, v2

    .line 623321
    iget-object v2, v0, Lcom/facebook/contacts/server/FetchDeltaContactsResult;->b:LX/0Px;

    move-object v2, v2

    .line 623322
    iget-object v3, p0, LX/3fl;->e:LX/3fg;

    invoke-virtual {v3, v2}, LX/3fg;->a(LX/0Py;)V

    .line 623323
    iget-object v3, p0, LX/3fl;->c:LX/3fX;

    .line 623324
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 623325
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 623326
    new-instance v8, Lcom/facebook/user/model/UserKey;

    sget-object p1, LX/0XG;->FACEBOOK_CONTACT:LX/0XG;

    invoke-direct {v8, p1, v4}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    invoke-virtual {v5, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 623327
    :cond_1
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v4, v4

    .line 623328
    invoke-virtual {v3, v4}, LX/3fX;->a(Ljava/lang/Iterable;)V

    .line 623329
    invoke-virtual {v2}, LX/0Py;->size()I

    move-result v2

    add-int/2addr v1, v2

    .line 623330
    iget-object v2, v0, Lcom/facebook/contacts/server/FetchDeltaContactsResult;->c:Ljava/lang/String;

    move-object p1, v2

    .line 623331
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.facebook.contacts.ACTION_CONTACT_SYNC_PROGRESS"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 623332
    iget-object v3, p0, LX/3fl;->h:LX/0Xl;

    invoke-interface {v3, v2}, LX/0Xl;->a(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 623333
    const v2, 0x3a7cf154

    invoke-static {v2}, LX/02m;->a(I)V

    .line 623334
    iget-boolean v2, v0, Lcom/facebook/contacts/server/FetchDeltaContactsResult;->d:Z

    move v0, v2

    .line 623335
    if-nez v0, :cond_0

    .line 623336
    if-lez v1, :cond_2

    .line 623337
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.contacts.CONTACTS_SYNC_DONE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 623338
    iget-object v1, p0, LX/3fl;->h:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 623339
    :cond_2
    return-object p1

    .line 623340
    :catchall_0
    move-exception v0

    const v1, -0x5f3089e8

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static b(LX/0QB;)LX/3fl;
    .locals 15

    .prologue
    .line 623300
    new-instance v0, LX/3fl;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p0}, LX/3fX;->a(LX/0QB;)LX/3fX;

    move-result-object v2

    check-cast v2, LX/3fX;

    invoke-static {p0}, LX/2It;->b(LX/0QB;)LX/2It;

    move-result-object v3

    check-cast v3, LX/2It;

    invoke-static {p0}, LX/3fg;->a(LX/0QB;)LX/3fg;

    move-result-object v4

    check-cast v4, LX/3fg;

    .line 623301
    new-instance v9, LX/3fm;

    invoke-static {p0}, LX/3fn;->b(LX/0QB;)LX/3fn;

    move-result-object v5

    check-cast v5, LX/3fn;

    invoke-static {p0}, LX/3fb;->a(LX/0QB;)LX/3fb;

    move-result-object v6

    check-cast v6, LX/3fb;

    invoke-static {p0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v7

    check-cast v7, LX/0sO;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-direct {v9, v5, v6, v7, v8}, LX/3fm;-><init>(LX/3fn;LX/3fb;LX/0sO;LX/0SG;)V

    .line 623302
    move-object v5, v9

    .line 623303
    check-cast v5, LX/3fm;

    .line 623304
    new-instance v9, LX/3fo;

    invoke-static {p0}, LX/3fn;->b(LX/0QB;)LX/3fn;

    move-result-object v6

    check-cast v6, LX/3fn;

    invoke-static {p0}, LX/3fb;->a(LX/0QB;)LX/3fb;

    move-result-object v7

    check-cast v7, LX/3fb;

    invoke-static {p0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v8

    check-cast v8, LX/0sO;

    invoke-direct {v9, v6, v7, v8}, LX/3fo;-><init>(LX/3fn;LX/3fb;LX/0sO;)V

    .line 623305
    move-object v6, v9

    .line 623306
    check-cast v6, LX/3fo;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v7

    check-cast v7, LX/0Xl;

    invoke-static {p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v8

    check-cast v8, LX/11H;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v9

    check-cast v9, LX/0W9;

    invoke-static {p0}, LX/2JB;->a(LX/0QB;)LX/2JB;

    move-result-object v10

    check-cast v10, LX/2JB;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    .line 623307
    new-instance v13, LX/3fp;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v12

    check-cast v12, LX/0tX;

    invoke-direct {v13, v12}, LX/3fp;-><init>(LX/0tX;)V

    .line 623308
    move-object v12, v13

    .line 623309
    check-cast v12, LX/3fp;

    const/16 v13, 0x438

    invoke-static {p0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v14

    check-cast v14, LX/0Uh;

    invoke-direct/range {v0 .. v14}, LX/3fl;-><init>(LX/0SG;LX/3fX;LX/2It;LX/3fg;LX/3fm;LX/3fo;LX/0Xl;LX/11H;LX/0W9;LX/2JB;LX/03V;LX/3fp;LX/0Or;LX/0Uh;)V

    .line 623310
    return-object v0
.end method

.method private static b(LX/3fl;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/contacts/server/FetchAllContactsResult;
    .locals 13

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 623386
    sget-object v2, LX/3gm;->REPLACE_ALL:LX/3gm;

    move-object v4, v2

    move v5, v0

    move-object v3, v1

    move v2, v0

    .line 623387
    :goto_0
    if-nez v5, :cond_2

    const/16 v0, 0x14

    .line 623388
    :goto_1
    const-string v6, "syncContactsFull (%d contacts)"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const v8, 0x4d1cc80a

    invoke-static {v6, v7, v8}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 623389
    :try_start_0
    new-instance v9, Lcom/facebook/contacts/server/FetchAllContactsParams;

    const-wide/16 v11, -0x1

    invoke-direct {v9, v0, v3, v11, v12}, Lcom/facebook/contacts/server/FetchAllContactsParams;-><init>(ILjava/lang/String;J)V

    move-object v0, v9

    .line 623390
    new-instance v3, LX/14U;

    invoke-direct {v3}, LX/14U;-><init>()V

    .line 623391
    sget-object v6, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    .line 623392
    iput-object v6, v3, LX/14U;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 623393
    iget-object v6, p0, LX/3fl;->i:LX/11H;

    iget-object v7, p0, LX/3fl;->f:LX/3fm;

    invoke-virtual {v6, v7, v0, v3, p1}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/server/FetchAllContactsResult;

    .line 623394
    add-int/lit8 v3, v2, 0x1

    if-nez v2, :cond_0

    .line 623395
    iget-object v1, v0, Lcom/facebook/contacts/server/FetchAllContactsResult;->e:Ljava/lang/String;

    move-object v1, v1

    .line 623396
    :cond_0
    iget-object v2, v0, Lcom/facebook/contacts/server/FetchAllContactsResult;->a:LX/0Px;

    move-object v2, v2

    .line 623397
    iget-object v6, p0, LX/3fl;->e:LX/3fg;

    sget-object v7, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-virtual {v6, v2, v4, v7}, LX/3fg;->a(LX/0Py;LX/3gm;LX/0ta;)V

    .line 623398
    iget-object v4, v0, Lcom/facebook/contacts/server/FetchAllContactsResult;->b:Ljava/lang/String;

    move-object v6, v4

    .line 623399
    invoke-virtual {v2}, LX/0Py;->size()I

    .line 623400
    invoke-virtual {v2}, LX/0Py;->size()I

    move-result v2

    add-int v4, v5, v2

    .line 623401
    new-instance v2, Landroid/content/Intent;

    const-string v5, "com.facebook.contacts.ACTION_CONTACT_SYNC_PROGRESS"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 623402
    iget-object v5, p0, LX/3fl;->h:LX/0Xl;

    invoke-interface {v5, v2}, LX/0Xl;->a(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 623403
    const v2, -0x175efb2

    invoke-static {v2}, LX/02m;->a(I)V

    .line 623404
    sget-object v2, LX/3gm;->INSERT:LX/3gm;

    .line 623405
    iget-boolean v5, v0, Lcom/facebook/contacts/server/FetchAllContactsResult;->c:Z

    move v5, v5

    .line 623406
    if-nez v5, :cond_3

    .line 623407
    iget-object v2, p0, LX/3fl;->k:LX/2JB;

    invoke-virtual {v2}, LX/2JB;->c()V

    .line 623408
    iget-object v2, p0, LX/3fl;->c:LX/3fX;

    invoke-virtual {v2}, LX/3fX;->a()V

    .line 623409
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.facebook.contacts.CONTACTS_SYNC_DONE"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 623410
    iget-object v4, p0, LX/3fl;->h:LX/0Xl;

    invoke-interface {v4, v2}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 623411
    const/4 v2, 0x1

    if-le v3, v2, :cond_1

    .line 623412
    new-instance v2, LX/6O8;

    invoke-direct {v2}, LX/6O8;-><init>()V

    .line 623413
    iget-object v9, v0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v9, v9

    .line 623414
    iput-object v9, v2, LX/6O8;->a:LX/0ta;

    .line 623415
    iget-wide v11, v0, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v9, v11

    .line 623416
    iput-wide v9, v2, LX/6O8;->b:J

    .line 623417
    iget-object v9, v0, Lcom/facebook/contacts/server/FetchAllContactsResult;->a:LX/0Px;

    move-object v9, v9

    .line 623418
    iput-object v9, v2, LX/6O8;->c:LX/0Px;

    .line 623419
    iget-object v9, v0, Lcom/facebook/contacts/server/FetchAllContactsResult;->b:Ljava/lang/String;

    move-object v9, v9

    .line 623420
    iput-object v9, v2, LX/6O8;->d:Ljava/lang/String;

    .line 623421
    iget-boolean v9, v0, Lcom/facebook/contacts/server/FetchAllContactsResult;->c:Z

    move v9, v9

    .line 623422
    iput-boolean v9, v2, LX/6O8;->e:Z

    .line 623423
    iget-object v9, v0, Lcom/facebook/contacts/server/FetchAllContactsResult;->d:Ljava/lang/String;

    move-object v9, v9

    .line 623424
    iput-object v9, v2, LX/6O8;->f:Ljava/lang/String;

    .line 623425
    iget-object v9, v0, Lcom/facebook/contacts/server/FetchAllContactsResult;->e:Ljava/lang/String;

    move-object v9, v9

    .line 623426
    iput-object v9, v2, LX/6O8;->g:Ljava/lang/String;

    .line 623427
    move-object v0, v2

    .line 623428
    iput-object v1, v0, LX/6O8;->g:Ljava/lang/String;

    .line 623429
    move-object v0, v0

    .line 623430
    new-instance v1, Lcom/facebook/contacts/server/FetchAllContactsResult;

    invoke-direct {v1, v0}, Lcom/facebook/contacts/server/FetchAllContactsResult;-><init>(LX/6O8;)V

    move-object v0, v1

    .line 623431
    :cond_1
    return-object v0

    .line 623432
    :cond_2
    const/16 v0, 0x32

    goto/16 :goto_1

    .line 623433
    :catchall_0
    move-exception v0

    const v1, 0x29050219

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_3
    move v5, v4

    move-object v4, v2

    move v2, v3

    move-object v3, v6

    goto/16 :goto_0
.end method

.method private static b(LX/3fl;)V
    .locals 2

    .prologue
    .line 623297
    iget-object v0, p0, LX/3fl;->n:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/2Jp;->CONTACTS_DATABASE:LX/2Jp;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Trying to download contacts with legacy contacts disabled"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 623298
    return-void

    .line 623299
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(LX/3fl;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactCoefficientModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 623283
    const-string v0, "syncContactsCoefficients"

    const v1, 0x4420ffe

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 623284
    :try_start_0
    iget-object v0, p0, LX/3fl;->m:LX/3fp;

    invoke-virtual {v0}, LX/3fp;->a()LX/0Px;

    move-result-object v0

    .line 623285
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 623286
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "GQLContactsCoefficientQueryHelper returned an empty list"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 623287
    :catchall_0
    move-exception v0

    const v1, -0x6be55550

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 623288
    :cond_0
    :try_start_1
    const-string v1, "syncContactsCoefficients/UpdateDB"

    const v2, 0x1c96f4

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 623289
    :try_start_2
    iget-object v1, p0, LX/3fl;->e:LX/3fg;

    invoke-virtual {v1, v0}, LX/3fg;->a(LX/0Px;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 623290
    const v1, 0x44daa17f

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    .line 623291
    iget-object v1, p0, LX/3fl;->c:LX/3fX;

    invoke-virtual {v1}, LX/3fX;->a()V

    .line 623292
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.contacts.ACTION_COEFFICIENTS_UPDATED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 623293
    iget-object v2, p0, LX/3fl;->h:LX/0Xl;

    invoke-interface {v2, v1}, LX/0Xl;->a(Landroid/content/Intent;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 623294
    const v1, -0x66a2708e

    invoke-static {v1}, LX/02m;->a(I)V

    .line 623295
    return-object v0

    .line 623296
    :catchall_1
    move-exception v0

    const v1, 0x783a6c2c

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static c(LX/3fl;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 623269
    iget-object v1, p0, LX/3fl;->k:LX/2JB;

    invoke-virtual {v1}, LX/2JB;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 623270
    const/4 v1, 0x0

    .line 623271
    :goto_0
    move-object v1, v1

    .line 623272
    if-nez v1, :cond_0

    .line 623273
    :goto_1
    return-object v0

    .line 623274
    :cond_0
    :try_start_0
    invoke-static {p0, v1, p1}, LX/3fl;->a(LX/3fl;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;
    :try_end_0
    .catch LX/2Oo; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1

    .line 623275
    :catch_0
    move-exception v2

    .line 623276
    invoke-virtual {v2}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v3

    .line 623277
    invoke-virtual {v3}, Lcom/facebook/http/protocol/ApiErrorResult;->h()LX/2Aa;

    move-result-object v4

    sget-object v5, LX/2Aa;->GRAPHQL_KERROR_DOMAIN:LX/2Aa;

    if-ne v4, v5, :cond_1

    invoke-virtual {v3}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v4

    const v5, 0x19f871

    if-ne v4, v5, :cond_1

    .line 623278
    sget-object v3, LX/3fl;->a:Ljava/lang/Class;

    const-string v4, "Delta sync cursor %s no longer valid, falling back to full sync."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-static {v3, v2, v4, v5}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 623279
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v3

    const v4, 0x198f03

    if-ne v3, v4, :cond_2

    .line 623280
    iget-object v3, p0, LX/3fl;->l:LX/03V;

    const-string v4, "ContactsWebFetcher"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Invalid cursor: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 623281
    :cond_2
    throw v2

    .line 623282
    :cond_3
    iget-object v1, p0, LX/3fl;->d:LX/2It;

    sget-object v2, LX/3Fg;->d:LX/3OK;

    invoke-virtual {v1, v2}, LX/2Iu;->a(LX/0To;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactCoefficientModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 623266
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/3fl;->b(LX/3fl;)V

    .line 623267
    invoke-static {p0}, LX/3fl;->c(LX/3fl;)LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 623268
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 6

    .prologue
    .line 623249
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/3fl;->b(LX/3fl;)V

    .line 623250
    iget-object v0, p0, LX/3fl;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 623251
    const/4 v1, 0x0

    .line 623252
    invoke-static {p0, p1}, LX/3fl;->c(LX/3fl;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;

    move-result-object v0

    .line 623253
    if-nez v0, :cond_0

    .line 623254
    invoke-static {p0, p1}, LX/3fl;->b(LX/3fl;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/contacts/server/FetchAllContactsResult;

    move-result-object v0

    .line 623255
    iget-object v1, v0, Lcom/facebook/contacts/server/FetchAllContactsResult;->d:Ljava/lang/String;

    move-object v0, v1

    .line 623256
    const/4 v1, 0x1

    .line 623257
    :cond_0
    if-eqz v0, :cond_2

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 623258
    iget-object v4, p0, LX/3fl;->d:LX/2It;

    sget-object v5, LX/3Fg;->a:LX/3OK;

    invoke-virtual {v4, v5, v2, v3}, LX/2Iu;->b(LX/0To;J)V

    .line 623259
    if-eqz v1, :cond_1

    .line 623260
    iget-object v4, p0, LX/3fl;->d:LX/2It;

    sget-object v5, LX/3Fg;->b:LX/3OK;

    invoke-virtual {v4, v5, v2, v3}, LX/2Iu;->b(LX/0To;J)V

    .line 623261
    :cond_1
    iget-object v4, p0, LX/3fl;->d:LX/2It;

    sget-object v5, LX/3Fg;->c:LX/3OK;

    iget-object p1, p0, LX/3fl;->j:LX/0W9;

    invoke-virtual {p1}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, v5, p1}, LX/2Iu;->b(LX/0To;Ljava/lang/String;)V

    .line 623262
    iget-object v4, p0, LX/3fl;->d:LX/2It;

    sget-object v5, LX/3Fg;->d:LX/3OK;

    invoke-virtual {v4, v5, v0}, LX/2Iu;->b(LX/0To;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 623263
    monitor-exit p0

    return-void

    .line 623264
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 623265
    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method
