.class public LX/4sB;
.super LX/12G;
.source ""


# static fields
.field public static final v:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/lang/ref/SoftReference",
            "<",
            "LX/4s6",
            "<",
            "LX/4sA;",
            ">;>;>;"
        }
    .end annotation
.end field


# instance fields
.field public final g:LX/12A;

.field public final h:Ljava/io/OutputStream;

.field public i:I

.field public final j:LX/4s6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4s6",
            "<",
            "LX/4sA;",
            ">;"
        }
    .end annotation
.end field

.field public k:[B

.field public l:I

.field public final m:I

.field public n:[C

.field public final o:I

.field public p:I

.field public q:[LX/4sA;

.field public r:I

.field public s:[LX/4sA;

.field public t:I

.field public u:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 817929
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, LX/4sB;->v:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>(LX/12A;IILX/0lD;Ljava/io/OutputStream;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x40

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 817901
    invoke-direct {p0, p2, p4}, LX/12G;-><init>(ILX/0lD;)V

    .line 817902
    iput v2, p0, LX/4sB;->l:I

    .line 817903
    iput p3, p0, LX/4sB;->i:I

    .line 817904
    iput-object p1, p0, LX/4sB;->g:LX/12A;

    .line 817905
    invoke-static {}, LX/4sB;->m()LX/4s6;

    move-result-object v0

    iput-object v0, p0, LX/4sB;->j:LX/4s6;

    .line 817906
    iput-object p5, p0, LX/4sB;->h:Ljava/io/OutputStream;

    .line 817907
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4sB;->u:Z

    .line 817908
    invoke-virtual {p1}, LX/12A;->f()[B

    move-result-object v0

    iput-object v0, p0, LX/4sB;->k:[B

    .line 817909
    iget-object v0, p0, LX/4sB;->k:[B

    array-length v0, v0

    iput v0, p0, LX/4sB;->m:I

    .line 817910
    invoke-virtual {p1}, LX/12A;->h()[C

    move-result-object v0

    iput-object v0, p0, LX/4sB;->n:[C

    .line 817911
    iget-object v0, p0, LX/4sB;->n:[C

    array-length v0, v0

    iput v0, p0, LX/4sB;->o:I

    .line 817912
    iget v0, p0, LX/4sB;->m:I

    const/16 v1, 0x302

    if-ge v0, v1, :cond_0

    .line 817913
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Internal encoding buffer length ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/4sB;->m:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") too short, must be at least 770"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 817914
    :cond_0
    sget-object v0, LX/4s9;->CHECK_SHARED_NAMES:LX/4s9;

    invoke-virtual {v0}, LX/4s9;->getMask()I

    move-result v0

    and-int/2addr v0, p3

    if-nez v0, :cond_1

    .line 817915
    iput-object v5, p0, LX/4sB;->q:[LX/4sA;

    .line 817916
    iput v3, p0, LX/4sB;->r:I

    .line 817917
    :goto_0
    sget-object v0, LX/4s9;->CHECK_SHARED_STRING_VALUES:LX/4s9;

    invoke-virtual {v0}, LX/4s9;->getMask()I

    move-result v0

    and-int/2addr v0, p3

    if-nez v0, :cond_3

    .line 817918
    iput-object v5, p0, LX/4sB;->s:[LX/4sA;

    .line 817919
    iput v3, p0, LX/4sB;->t:I

    .line 817920
    :goto_1
    return-void

    .line 817921
    :cond_1
    iget-object v0, p0, LX/4sB;->j:LX/4s6;

    invoke-virtual {v0}, LX/4s6;->a()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4sA;

    iput-object v0, p0, LX/4sB;->q:[LX/4sA;

    .line 817922
    iget-object v0, p0, LX/4sB;->q:[LX/4sA;

    if-nez v0, :cond_2

    .line 817923
    new-array v0, v4, [LX/4sA;

    iput-object v0, p0, LX/4sB;->q:[LX/4sA;

    .line 817924
    :cond_2
    iput v2, p0, LX/4sB;->r:I

    goto :goto_0

    .line 817925
    :cond_3
    iget-object v0, p0, LX/4sB;->j:LX/4s6;

    invoke-virtual {v0}, LX/4s6;->b()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4sA;

    iput-object v0, p0, LX/4sB;->s:[LX/4sA;

    .line 817926
    iget-object v0, p0, LX/4sB;->s:[LX/4sA;

    if-nez v0, :cond_4

    .line 817927
    new-array v0, v4, [LX/4sA;

    iput-object v0, p0, LX/4sB;->s:[LX/4sA;

    .line 817928
    :cond_4
    iput v2, p0, LX/4sB;->t:I

    goto :goto_1
.end method

.method private static a(II)I
    .locals 3

    .prologue
    const v2, 0xdc00

    .line 817898
    if-lt p1, v2, :cond_0

    const v0, 0xdfff

    if-le p1, v0, :cond_1

    .line 817899
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Broken surrogate pair: first char 0x"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", second 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; illegal combination"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 817900
    :cond_1
    const/high16 v0, 0x10000

    const v1, 0xd800

    sub-int v1, p0, v1

    shl-int/lit8 v1, v1, 0xa

    add-int/2addr v0, v1

    sub-int v1, p1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method private final a([CIII)I
    .locals 6

    .prologue
    .line 817869
    iget-object v2, p0, LX/4sB;->k:[B

    .line 817870
    :goto_0
    if-ge p2, p3, :cond_7

    .line 817871
    add-int/lit8 v1, p2, 0x1

    aget-char v3, p1, p2

    .line 817872
    const/16 v0, 0x7f

    if-gt v3, v0, :cond_0

    .line 817873
    add-int/lit8 v0, p4, 0x1

    int-to-byte v3, v3

    aput-byte v3, v2, p4

    move p4, v0

    move p2, v1

    .line 817874
    goto :goto_0

    .line 817875
    :cond_0
    const/16 v0, 0x800

    if-ge v3, v0, :cond_1

    .line 817876
    add-int/lit8 v0, p4, 0x1

    shr-int/lit8 v4, v3, 0x6

    or-int/lit16 v4, v4, 0xc0

    int-to-byte v4, v4

    aput-byte v4, v2, p4

    .line 817877
    add-int/lit8 p4, v0, 0x1

    and-int/lit8 v3, v3, 0x3f

    or-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    aput-byte v3, v2, v0

    move p2, v1

    .line 817878
    goto :goto_0

    .line 817879
    :cond_1
    const v0, 0xd800

    if-lt v3, v0, :cond_2

    const v0, 0xdfff

    if-le v3, v0, :cond_3

    .line 817880
    :cond_2
    add-int/lit8 v0, p4, 0x1

    shr-int/lit8 v4, v3, 0xc

    or-int/lit16 v4, v4, 0xe0

    int-to-byte v4, v4

    aput-byte v4, v2, p4

    .line 817881
    add-int/lit8 v4, v0, 0x1

    shr-int/lit8 v5, v3, 0x6

    and-int/lit8 v5, v5, 0x3f

    or-int/lit16 v5, v5, 0x80

    int-to-byte v5, v5

    aput-byte v5, v2, v0

    .line 817882
    add-int/lit8 p4, v4, 0x1

    and-int/lit8 v0, v3, 0x3f

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    aput-byte v0, v2, v4

    move p2, v1

    .line 817883
    goto :goto_0

    .line 817884
    :cond_3
    const v0, 0xdbff

    if-le v3, v0, :cond_4

    .line 817885
    invoke-static {v3}, LX/4sB;->e(I)V

    .line 817886
    :cond_4
    if-lt v1, p3, :cond_5

    .line 817887
    invoke-static {v3}, LX/4sB;->e(I)V

    .line 817888
    :cond_5
    add-int/lit8 p2, v1, 0x1

    aget-char v0, p1, v1

    invoke-static {v3, v0}, LX/4sB;->a(II)I

    move-result v0

    .line 817889
    const v1, 0x10ffff

    if-le v0, v1, :cond_6

    .line 817890
    invoke-static {v0}, LX/4sB;->e(I)V

    .line 817891
    :cond_6
    add-int/lit8 v1, p4, 0x1

    shr-int/lit8 v3, v0, 0x12

    or-int/lit16 v3, v3, 0xf0

    int-to-byte v3, v3

    aput-byte v3, v2, p4

    .line 817892
    add-int/lit8 v3, v1, 0x1

    shr-int/lit8 v4, v0, 0xc

    and-int/lit8 v4, v4, 0x3f

    or-int/lit16 v4, v4, 0x80

    int-to-byte v4, v4

    aput-byte v4, v2, v1

    .line 817893
    add-int/lit8 v1, v3, 0x1

    shr-int/lit8 v4, v0, 0x6

    and-int/lit8 v4, v4, 0x3f

    or-int/lit16 v4, v4, 0x80

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 817894
    add-int/lit8 p4, v1, 0x1

    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    aput-byte v0, v2, v1

    goto/16 :goto_0

    .line 817895
    :cond_7
    iget v0, p0, LX/4sB;->l:I

    sub-int v0, p4, v0

    .line 817896
    iput p4, p0, LX/4sB;->l:I

    .line 817897
    return v0
.end method

.method private final a(B)V
    .locals 3

    .prologue
    .line 817865
    iget v0, p0, LX/4sB;->l:I

    iget v1, p0, LX/4sB;->m:I

    if-lt v0, v1, :cond_0

    .line 817866
    invoke-direct {p0}, LX/4sB;->n()V

    .line 817867
    :cond_0
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    aput-byte p1, v0, v1

    .line 817868
    return-void
.end method

.method private final a(BB)V
    .locals 3

    .prologue
    .line 817860
    iget v0, p0, LX/4sB;->l:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, LX/4sB;->m:I

    if-lt v0, v1, :cond_0

    .line 817861
    invoke-direct {p0}, LX/4sB;->n()V

    .line 817862
    :cond_0
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    aput-byte p1, v0, v1

    .line 817863
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    aput-byte p2, v0, v1

    .line 817864
    return-void
.end method

.method private final a(BBB)V
    .locals 3

    .prologue
    .line 817854
    iget v0, p0, LX/4sB;->l:I

    add-int/lit8 v0, v0, 0x2

    iget v1, p0, LX/4sB;->m:I

    if-lt v0, v1, :cond_0

    .line 817855
    invoke-direct {p0}, LX/4sB;->n()V

    .line 817856
    :cond_0
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    aput-byte p1, v0, v1

    .line 817857
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    aput-byte p2, v0, v1

    .line 817858
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    aput-byte p3, v0, v1

    .line 817859
    return-void
.end method

.method private final a(BBBB)V
    .locals 3

    .prologue
    .line 817742
    iget v0, p0, LX/4sB;->l:I

    add-int/lit8 v0, v0, 0x3

    iget v1, p0, LX/4sB;->m:I

    if-lt v0, v1, :cond_0

    .line 817743
    invoke-direct {p0}, LX/4sB;->n()V

    .line 817744
    :cond_0
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    aput-byte p1, v0, v1

    .line 817745
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    aput-byte p2, v0, v1

    .line 817746
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    aput-byte p3, v0, v1

    .line 817747
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    aput-byte p4, v0, v1

    .line 817748
    return-void
.end method

.method private final a(BBBBB)V
    .locals 3

    .prologue
    .line 817832
    iget v0, p0, LX/4sB;->l:I

    add-int/lit8 v0, v0, 0x4

    iget v1, p0, LX/4sB;->m:I

    if-lt v0, v1, :cond_0

    .line 817833
    invoke-direct {p0}, LX/4sB;->n()V

    .line 817834
    :cond_0
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    aput-byte p1, v0, v1

    .line 817835
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    aput-byte p2, v0, v1

    .line 817836
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    aput-byte p3, v0, v1

    .line 817837
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    aput-byte p4, v0, v1

    .line 817838
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    aput-byte p5, v0, v1

    .line 817839
    return-void
.end method

.method private final a(BBBBBB)V
    .locals 3

    .prologue
    .line 817823
    iget v0, p0, LX/4sB;->l:I

    add-int/lit8 v0, v0, 0x5

    iget v1, p0, LX/4sB;->m:I

    if-lt v0, v1, :cond_0

    .line 817824
    invoke-direct {p0}, LX/4sB;->n()V

    .line 817825
    :cond_0
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    aput-byte p1, v0, v1

    .line 817826
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    aput-byte p2, v0, v1

    .line 817827
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    aput-byte p3, v0, v1

    .line 817828
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    aput-byte p4, v0, v1

    .line 817829
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    aput-byte p5, v0, v1

    .line 817830
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    aput-byte p6, v0, v1

    .line 817831
    return-void
.end method

.method private a(LX/0lc;[B)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 817797
    array-length v0, p2

    .line 817798
    const/16 v1, 0x38

    if-gt v0, v1, :cond_2

    .line 817799
    iget v1, p0, LX/4sB;->l:I

    add-int/2addr v1, v0

    iget v2, p0, LX/4sB;->m:I

    if-lt v1, v2, :cond_0

    .line 817800
    invoke-direct {p0}, LX/4sB;->n()V

    .line 817801
    :cond_0
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sB;->l:I

    add-int/lit16 v3, v0, 0xbe

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 817802
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    invoke-static {p2, v4, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 817803
    iget v1, p0, LX/4sB;->l:I

    add-int/2addr v0, v1

    iput v0, p0, LX/4sB;->l:I

    .line 817804
    iget v0, p0, LX/4sB;->r:I

    if-ltz v0, :cond_1

    .line 817805
    invoke-interface {p1}, LX/0lc;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/4sB;->m(Ljava/lang/String;)V

    .line 817806
    :cond_1
    :goto_0
    return-void

    .line 817807
    :cond_2
    iget v1, p0, LX/4sB;->l:I

    iget v2, p0, LX/4sB;->m:I

    if-lt v1, v2, :cond_3

    .line 817808
    invoke-direct {p0}, LX/4sB;->n()V

    .line 817809
    :cond_3
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sB;->l:I

    const/16 v3, 0x34

    aput-byte v3, v1, v2

    .line 817810
    iget v1, p0, LX/4sB;->l:I

    add-int/2addr v1, v0

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, LX/4sB;->m:I

    if-ge v1, v2, :cond_4

    .line 817811
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    invoke-static {p2, v4, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 817812
    iget v1, p0, LX/4sB;->l:I

    add-int/2addr v0, v1

    iput v0, p0, LX/4sB;->l:I

    .line 817813
    :goto_1
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    const/4 v2, -0x4

    aput-byte v2, v0, v1

    .line 817814
    iget v0, p0, LX/4sB;->r:I

    if-ltz v0, :cond_1

    .line 817815
    invoke-interface {p1}, LX/0lc;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/4sB;->m(Ljava/lang/String;)V

    goto :goto_0

    .line 817816
    :cond_4
    invoke-direct {p0}, LX/4sB;->n()V

    .line 817817
    const/16 v1, 0x302

    if-ge v0, v1, :cond_5

    .line 817818
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    invoke-static {p2, v4, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 817819
    iget v1, p0, LX/4sB;->l:I

    add-int/2addr v0, v1

    iput v0, p0, LX/4sB;->l:I

    goto :goto_1

    .line 817820
    :cond_5
    iget v1, p0, LX/4sB;->l:I

    if-lez v1, :cond_6

    .line 817821
    invoke-direct {p0}, LX/4sB;->n()V

    .line 817822
    :cond_6
    iget-object v1, p0, LX/4sB;->h:Ljava/io/OutputStream;

    invoke-virtual {v1, p2, v4, v0}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_1
.end method

.method private final a([BII)V
    .locals 2

    .prologue
    .line 817791
    if-nez p3, :cond_0

    .line 817792
    :goto_0
    return-void

    .line 817793
    :cond_0
    iget v0, p0, LX/4sB;->l:I

    add-int/2addr v0, p3

    iget v1, p0, LX/4sB;->m:I

    if-lt v0, v1, :cond_1

    .line 817794
    invoke-direct {p0, p1, p2, p3}, LX/4sB;->b([BII)V

    goto :goto_0

    .line 817795
    :cond_1
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 817796
    iget v0, p0, LX/4sB;->l:I

    add-int/2addr v0, p3

    iput v0, p0, LX/4sB;->l:I

    goto :goto_0
.end method

.method private a(LX/4s9;)Z
    .locals 2

    .prologue
    .line 817790
    iget v0, p0, LX/4sB;->i:I

    invoke-virtual {p1}, LX/4s9;->getMask()I

    move-result v1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final b(Ljava/lang/String;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 817774
    const/16 v0, 0x34

    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    .line 817775
    iget v0, p0, LX/4sB;->o:I

    if-le p2, v0, :cond_2

    .line 817776
    invoke-direct {p0, p1}, LX/4sB;->k(Ljava/lang/String;)V

    .line 817777
    :goto_0
    iget v0, p0, LX/4sB;->r:I

    if-ltz v0, :cond_0

    .line 817778
    invoke-direct {p0, p1}, LX/4sB;->m(Ljava/lang/String;)V

    .line 817779
    :cond_0
    iget v0, p0, LX/4sB;->l:I

    iget v1, p0, LX/4sB;->m:I

    if-lt v0, v1, :cond_1

    .line 817780
    invoke-direct {p0}, LX/4sB;->n()V

    .line 817781
    :cond_1
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    const/4 v2, -0x4

    aput-byte v2, v0, v1

    .line 817782
    return-void

    .line 817783
    :cond_2
    iget-object v0, p0, LX/4sB;->n:[C

    invoke-virtual {p1, v2, p2, v0, v2}, Ljava/lang/String;->getChars(II[CI)V

    .line 817784
    add-int v0, p2, p2

    add-int/2addr v0, p2

    .line 817785
    iget-object v1, p0, LX/4sB;->k:[B

    array-length v1, v1

    if-gt v0, v1, :cond_4

    .line 817786
    iget v1, p0, LX/4sB;->l:I

    add-int/2addr v0, v1

    iget v1, p0, LX/4sB;->m:I

    if-lt v0, v1, :cond_3

    .line 817787
    invoke-direct {p0}, LX/4sB;->n()V

    .line 817788
    :cond_3
    iget-object v0, p0, LX/4sB;->n:[C

    invoke-direct {p0, v0, v2, p2}, LX/4sB;->c([CII)I

    goto :goto_0

    .line 817789
    :cond_4
    iget-object v0, p0, LX/4sB;->n:[C

    invoke-direct {p0, v0, v2, p2}, LX/4sB;->d([CII)V

    goto :goto_0
.end method

.method private final b([B)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 817758
    array-length v0, p1

    .line 817759
    iget v1, p0, LX/4sB;->l:I

    iget v2, p0, LX/4sB;->m:I

    if-lt v1, v2, :cond_0

    .line 817760
    invoke-direct {p0}, LX/4sB;->n()V

    .line 817761
    :cond_0
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sB;->l:I

    const/16 v3, 0x34

    aput-byte v3, v1, v2

    .line 817762
    iget v1, p0, LX/4sB;->l:I

    add-int/2addr v1, v0

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, LX/4sB;->m:I

    if-ge v1, v2, :cond_1

    .line 817763
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    invoke-static {p1, v4, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 817764
    iget v1, p0, LX/4sB;->l:I

    add-int/2addr v0, v1

    iput v0, p0, LX/4sB;->l:I

    .line 817765
    :goto_0
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    const/4 v2, -0x4

    aput-byte v2, v0, v1

    .line 817766
    return-void

    .line 817767
    :cond_1
    invoke-direct {p0}, LX/4sB;->n()V

    .line 817768
    const/16 v1, 0x302

    if-ge v0, v1, :cond_2

    .line 817769
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    invoke-static {p1, v4, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 817770
    iget v1, p0, LX/4sB;->l:I

    add-int/2addr v0, v1

    iput v0, p0, LX/4sB;->l:I

    goto :goto_0

    .line 817771
    :cond_2
    iget v1, p0, LX/4sB;->l:I

    if-lez v1, :cond_3

    .line 817772
    invoke-direct {p0}, LX/4sB;->n()V

    .line 817773
    :cond_3
    iget-object v1, p0, LX/4sB;->h:Ljava/io/OutputStream;

    invoke-virtual {v1, p1, v4, v0}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0
.end method

.method private final b([BII)V
    .locals 3

    .prologue
    .line 817749
    iget v0, p0, LX/4sB;->l:I

    iget v1, p0, LX/4sB;->m:I

    if-lt v0, v1, :cond_0

    .line 817750
    invoke-direct {p0}, LX/4sB;->n()V

    .line 817751
    :cond_0
    :goto_0
    iget v0, p0, LX/4sB;->m:I

    iget v1, p0, LX/4sB;->l:I

    sub-int/2addr v0, v1

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 817752
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 817753
    iget v1, p0, LX/4sB;->l:I

    add-int/2addr v1, v0

    iput v1, p0, LX/4sB;->l:I

    .line 817754
    sub-int/2addr p3, v0

    if-eqz p3, :cond_1

    .line 817755
    add-int/2addr p2, v0

    .line 817756
    invoke-direct {p0}, LX/4sB;->n()V

    goto :goto_0

    .line 817757
    :cond_1
    return-void
.end method

.method private final c([CII)I
    .locals 4

    .prologue
    .line 818212
    iget v0, p0, LX/4sB;->l:I

    .line 818213
    iget-object v2, p0, LX/4sB;->k:[B

    .line 818214
    :goto_0
    aget-char v3, p1, p2

    .line 818215
    const/16 v1, 0x7f

    if-le v3, v1, :cond_0

    .line 818216
    invoke-direct {p0, p1, p2, p3, v0}, LX/4sB;->a([CIII)I

    move-result v0

    .line 818217
    :goto_1
    return v0

    .line 818218
    :cond_0
    add-int/lit8 v1, v0, 0x1

    int-to-byte v3, v3

    aput-byte v3, v2, v0

    .line 818219
    add-int/lit8 p2, p2, 0x1

    if-lt p2, p3, :cond_1

    .line 818220
    iget v0, p0, LX/4sB;->l:I

    sub-int v0, v1, v0

    .line 818221
    iput v1, p0, LX/4sB;->l:I

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private final c(I)V
    .locals 3

    .prologue
    .line 817935
    iget v0, p0, LX/4sB;->r:I

    if-lt p1, v0, :cond_0

    .line 817936
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Internal error: trying to write shared name with index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; but have only seen "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/4sB;->r:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " so far!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 817937
    :cond_0
    const/16 v0, 0x40

    if-ge p1, v0, :cond_1

    .line 817938
    add-int/lit8 v0, p1, 0x40

    int-to-byte v0, v0

    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    .line 817939
    :goto_0
    return-void

    .line 817940
    :cond_1
    shr-int/lit8 v0, p1, 0x8

    add-int/lit8 v0, v0, 0x30

    int-to-byte v0, v0

    int-to-byte v1, p1

    invoke-direct {p0, v0, v1}, LX/4sB;->a(BB)V

    goto :goto_0
.end method

.method private final c(Ljava/lang/String;I)V
    .locals 5

    .prologue
    const/4 v4, -0x4

    const/16 v3, -0x1c

    const/4 v2, 0x0

    .line 818193
    iget v0, p0, LX/4sB;->o:I

    if-le p2, v0, :cond_0

    .line 818194
    invoke-direct {p0, v3}, LX/4sB;->a(B)V

    .line 818195
    invoke-direct {p0, p1}, LX/4sB;->k(Ljava/lang/String;)V

    .line 818196
    invoke-direct {p0, v4}, LX/4sB;->a(B)V

    .line 818197
    :goto_0
    return-void

    .line 818198
    :cond_0
    iget-object v0, p0, LX/4sB;->n:[C

    invoke-virtual {p1, v2, p2, v0, v2}, Ljava/lang/String;->getChars(II[CI)V

    .line 818199
    add-int v0, p2, p2

    add-int/2addr v0, p2

    add-int/lit8 v0, v0, 0x2

    .line 818200
    iget-object v1, p0, LX/4sB;->k:[B

    array-length v1, v1

    if-le v0, v1, :cond_1

    .line 818201
    invoke-direct {p0, v3}, LX/4sB;->a(B)V

    .line 818202
    iget-object v0, p0, LX/4sB;->n:[C

    invoke-direct {p0, v0, v2, p2}, LX/4sB;->d([CII)V

    .line 818203
    invoke-direct {p0, v4}, LX/4sB;->a(B)V

    goto :goto_0

    .line 818204
    :cond_1
    iget v1, p0, LX/4sB;->l:I

    add-int/2addr v0, v1

    iget v1, p0, LX/4sB;->m:I

    if-lt v0, v1, :cond_2

    .line 818205
    invoke-direct {p0}, LX/4sB;->n()V

    .line 818206
    :cond_2
    iget v0, p0, LX/4sB;->l:I

    .line 818207
    const/16 v1, -0x20

    invoke-direct {p0, v1}, LX/4sB;->a(B)V

    .line 818208
    iget-object v1, p0, LX/4sB;->n:[C

    invoke-direct {p0, v1, v2, p2}, LX/4sB;->c([CII)I

    move-result v1

    .line 818209
    if-le v1, p2, :cond_3

    .line 818210
    iget-object v1, p0, LX/4sB;->k:[B

    aput-byte v3, v1, v0

    .line 818211
    :cond_3
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    aput-byte v4, v0, v1

    goto :goto_0
.end method

.method private c([BII)V
    .locals 5

    .prologue
    .line 818145
    invoke-direct {p0, p3}, LX/4sB;->g(I)V

    .line 818146
    :goto_0
    const/4 v0, 0x7

    if-lt p3, v0, :cond_1

    .line 818147
    iget v0, p0, LX/4sB;->l:I

    add-int/lit8 v0, v0, 0x8

    iget v1, p0, LX/4sB;->m:I

    if-lt v0, v1, :cond_0

    .line 818148
    invoke-direct {p0}, LX/4sB;->n()V

    .line 818149
    :cond_0
    add-int/lit8 v0, p2, 0x1

    aget-byte v1, p1, p2

    .line 818150
    iget-object v2, p0, LX/4sB;->k:[B

    iget v3, p0, LX/4sB;->l:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/4sB;->l:I

    shr-int/lit8 v4, v1, 0x1

    and-int/lit8 v4, v4, 0x7f

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 818151
    shl-int/lit8 v1, v1, 0x8

    add-int/lit8 v2, v0, 0x1

    aget-byte v0, p1, v0

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v1

    .line 818152
    iget-object v1, p0, LX/4sB;->k:[B

    iget v3, p0, LX/4sB;->l:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/4sB;->l:I

    shr-int/lit8 v4, v0, 0x2

    and-int/lit8 v4, v4, 0x7f

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    .line 818153
    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v1, v2, 0x1

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v0, v2

    .line 818154
    iget-object v2, p0, LX/4sB;->k:[B

    iget v3, p0, LX/4sB;->l:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/4sB;->l:I

    shr-int/lit8 v4, v0, 0x3

    and-int/lit8 v4, v4, 0x7f

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 818155
    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v2, v1, 0x1

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    .line 818156
    iget-object v1, p0, LX/4sB;->k:[B

    iget v3, p0, LX/4sB;->l:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/4sB;->l:I

    shr-int/lit8 v4, v0, 0x4

    and-int/lit8 v4, v4, 0x7f

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    .line 818157
    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v1, v2, 0x1

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v0, v2

    .line 818158
    iget-object v2, p0, LX/4sB;->k:[B

    iget v3, p0, LX/4sB;->l:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/4sB;->l:I

    shr-int/lit8 v4, v0, 0x5

    and-int/lit8 v4, v4, 0x7f

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 818159
    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v2, v1, 0x1

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    .line 818160
    iget-object v1, p0, LX/4sB;->k:[B

    iget v3, p0, LX/4sB;->l:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/4sB;->l:I

    shr-int/lit8 v4, v0, 0x6

    and-int/lit8 v4, v4, 0x7f

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    .line 818161
    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 p2, v2, 0x1

    aget-byte v1, p1, v2

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    .line 818162
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sB;->l:I

    shr-int/lit8 v3, v0, 0x7

    and-int/lit8 v3, v3, 0x7f

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 818163
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sB;->l:I

    and-int/lit8 v0, v0, 0x7f

    int-to-byte v0, v0

    aput-byte v0, v1, v2

    .line 818164
    add-int/lit8 p3, p3, -0x7

    .line 818165
    goto/16 :goto_0

    .line 818166
    :cond_1
    if-lez p3, :cond_3

    .line 818167
    iget v0, p0, LX/4sB;->l:I

    add-int/lit8 v0, v0, 0x7

    iget v1, p0, LX/4sB;->m:I

    if-lt v0, v1, :cond_2

    .line 818168
    invoke-direct {p0}, LX/4sB;->n()V

    .line 818169
    :cond_2
    add-int/lit8 v0, p2, 0x1

    aget-byte v1, p1, p2

    .line 818170
    iget-object v2, p0, LX/4sB;->k:[B

    iget v3, p0, LX/4sB;->l:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/4sB;->l:I

    shr-int/lit8 v4, v1, 0x1

    and-int/lit8 v4, v4, 0x7f

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 818171
    const/4 v2, 0x1

    if-le p3, v2, :cond_8

    .line 818172
    and-int/lit8 v1, v1, 0x1

    shl-int/lit8 v1, v1, 0x8

    add-int/lit8 v2, v0, 0x1

    aget-byte v0, p1, v0

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v1

    .line 818173
    iget-object v1, p0, LX/4sB;->k:[B

    iget v3, p0, LX/4sB;->l:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/4sB;->l:I

    shr-int/lit8 v4, v0, 0x2

    and-int/lit8 v4, v4, 0x7f

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    .line 818174
    const/4 v1, 0x2

    if-le p3, v1, :cond_7

    .line 818175
    and-int/lit8 v0, v0, 0x3

    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v1, v2, 0x1

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v0, v2

    .line 818176
    iget-object v2, p0, LX/4sB;->k:[B

    iget v3, p0, LX/4sB;->l:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/4sB;->l:I

    shr-int/lit8 v4, v0, 0x3

    and-int/lit8 v4, v4, 0x7f

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 818177
    const/4 v2, 0x3

    if-le p3, v2, :cond_6

    .line 818178
    and-int/lit8 v0, v0, 0x7

    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v2, v1, 0x1

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    .line 818179
    iget-object v1, p0, LX/4sB;->k:[B

    iget v3, p0, LX/4sB;->l:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/4sB;->l:I

    shr-int/lit8 v4, v0, 0x4

    and-int/lit8 v4, v4, 0x7f

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    .line 818180
    const/4 v1, 0x4

    if-le p3, v1, :cond_5

    .line 818181
    and-int/lit8 v0, v0, 0xf

    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v1, v2, 0x1

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v0, v2

    .line 818182
    iget-object v2, p0, LX/4sB;->k:[B

    iget v3, p0, LX/4sB;->l:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/4sB;->l:I

    shr-int/lit8 v4, v0, 0x5

    and-int/lit8 v4, v4, 0x7f

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 818183
    const/4 v2, 0x5

    if-le p3, v2, :cond_4

    .line 818184
    and-int/lit8 v0, v0, 0x1f

    shl-int/lit8 v0, v0, 0x8

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    .line 818185
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sB;->l:I

    shr-int/lit8 v3, v0, 0x6

    and-int/lit8 v3, v3, 0x7f

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 818186
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sB;->l:I

    and-int/lit8 v0, v0, 0x3f

    int-to-byte v0, v0

    aput-byte v0, v1, v2

    .line 818187
    :cond_3
    :goto_1
    return-void

    .line 818188
    :cond_4
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sB;->l:I

    and-int/lit8 v0, v0, 0x1f

    int-to-byte v0, v0

    aput-byte v0, v1, v2

    goto :goto_1

    .line 818189
    :cond_5
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sB;->l:I

    and-int/lit8 v0, v0, 0xf

    int-to-byte v0, v0

    aput-byte v0, v1, v2

    goto :goto_1

    .line 818190
    :cond_6
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sB;->l:I

    and-int/lit8 v0, v0, 0x7

    int-to-byte v0, v0

    aput-byte v0, v1, v2

    goto :goto_1

    .line 818191
    :cond_7
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sB;->l:I

    and-int/lit8 v0, v0, 0x3

    int-to-byte v0, v0

    aput-byte v0, v1, v2

    goto :goto_1

    .line 818192
    :cond_8
    iget-object v0, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sB;->l:I

    and-int/lit8 v1, v1, 0x1

    int-to-byte v1, v1

    aput-byte v1, v0, v2

    goto :goto_1
.end method

.method private final d(I)V
    .locals 3

    .prologue
    .line 818139
    iget v0, p0, LX/4sB;->t:I

    if-lt p1, v0, :cond_0

    .line 818140
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Internal error: trying to write shared String value with index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; but have only seen "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/4sB;->t:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " so far!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 818141
    :cond_0
    const/16 v0, 0x1f

    if-ge p1, v0, :cond_1

    .line 818142
    add-int/lit8 v0, p1, 0x1

    int-to-byte v0, v0

    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    .line 818143
    :goto_0
    return-void

    .line 818144
    :cond_1
    shr-int/lit8 v0, p1, 0x8

    add-int/lit16 v0, v0, 0xec

    int-to-byte v0, v0

    int-to-byte v1, p1

    invoke-direct {p0, v0, v1}, LX/4sB;->a(BB)V

    goto :goto_0
.end method

.method private d([CII)V
    .locals 8

    .prologue
    const/16 v7, 0x7f

    .line 818104
    iget v0, p0, LX/4sB;->m:I

    add-int/lit8 v3, v0, -0x4

    move v1, p2

    .line 818105
    :goto_0
    if-ge v1, p3, :cond_9

    .line 818106
    iget v0, p0, LX/4sB;->l:I

    if-lt v0, v3, :cond_0

    .line 818107
    invoke-direct {p0}, LX/4sB;->n()V

    .line 818108
    :cond_0
    add-int/lit8 v2, v1, 0x1

    aget-char v0, p1, v1

    .line 818109
    if-gt v0, v7, :cond_1

    .line 818110
    iget-object v1, p0, LX/4sB;->k:[B

    iget v4, p0, LX/4sB;->l:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, LX/4sB;->l:I

    int-to-byte v0, v0

    aput-byte v0, v1, v4

    .line 818111
    sub-int v1, p3, v2

    .line 818112
    iget v0, p0, LX/4sB;->l:I

    sub-int v0, v3, v0

    .line 818113
    if-le v1, v0, :cond_b

    .line 818114
    :goto_1
    add-int v4, v0, v2

    .line 818115
    :goto_2
    if-ge v2, v4, :cond_a

    .line 818116
    add-int/lit8 v1, v2, 0x1

    aget-char v0, p1, v2

    .line 818117
    if-gt v0, v7, :cond_2

    .line 818118
    iget-object v2, p0, LX/4sB;->k:[B

    iget v5, p0, LX/4sB;->l:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, LX/4sB;->l:I

    int-to-byte v0, v0

    aput-byte v0, v2, v5

    move v2, v1

    goto :goto_2

    :cond_1
    move v1, v2

    .line 818119
    :cond_2
    const/16 v2, 0x800

    if-ge v0, v2, :cond_3

    .line 818120
    iget-object v2, p0, LX/4sB;->k:[B

    iget v4, p0, LX/4sB;->l:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, LX/4sB;->l:I

    shr-int/lit8 v5, v0, 0x6

    or-int/lit16 v5, v5, 0xc0

    int-to-byte v5, v5

    aput-byte v5, v2, v4

    .line 818121
    iget-object v2, p0, LX/4sB;->k:[B

    iget v4, p0, LX/4sB;->l:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, LX/4sB;->l:I

    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    aput-byte v0, v2, v4

    goto :goto_0

    .line 818122
    :cond_3
    const v2, 0xd800

    if-lt v0, v2, :cond_4

    const v2, 0xdfff

    if-le v0, v2, :cond_5

    .line 818123
    :cond_4
    iget-object v2, p0, LX/4sB;->k:[B

    iget v4, p0, LX/4sB;->l:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, LX/4sB;->l:I

    shr-int/lit8 v5, v0, 0xc

    or-int/lit16 v5, v5, 0xe0

    int-to-byte v5, v5

    aput-byte v5, v2, v4

    .line 818124
    iget-object v2, p0, LX/4sB;->k:[B

    iget v4, p0, LX/4sB;->l:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, LX/4sB;->l:I

    shr-int/lit8 v5, v0, 0x6

    and-int/lit8 v5, v5, 0x3f

    or-int/lit16 v5, v5, 0x80

    int-to-byte v5, v5

    aput-byte v5, v2, v4

    .line 818125
    iget-object v2, p0, LX/4sB;->k:[B

    iget v4, p0, LX/4sB;->l:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, LX/4sB;->l:I

    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    aput-byte v0, v2, v4

    goto/16 :goto_0

    .line 818126
    :cond_5
    const v2, 0xdbff

    if-le v0, v2, :cond_6

    .line 818127
    invoke-static {v0}, LX/4sB;->e(I)V

    .line 818128
    :cond_6
    if-lt v1, p3, :cond_7

    .line 818129
    invoke-static {v0}, LX/4sB;->e(I)V

    .line 818130
    :cond_7
    add-int/lit8 p2, v1, 0x1

    aget-char v1, p1, v1

    invoke-static {v0, v1}, LX/4sB;->a(II)I

    move-result v0

    .line 818131
    const v1, 0x10ffff

    if-le v0, v1, :cond_8

    .line 818132
    invoke-static {v0}, LX/4sB;->e(I)V

    .line 818133
    :cond_8
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v4, v2, 0x1

    iput v4, p0, LX/4sB;->l:I

    shr-int/lit8 v4, v0, 0x12

    or-int/lit16 v4, v4, 0xf0

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    .line 818134
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v4, v2, 0x1

    iput v4, p0, LX/4sB;->l:I

    shr-int/lit8 v4, v0, 0xc

    and-int/lit8 v4, v4, 0x3f

    or-int/lit16 v4, v4, 0x80

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    .line 818135
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v4, v2, 0x1

    iput v4, p0, LX/4sB;->l:I

    shr-int/lit8 v4, v0, 0x6

    and-int/lit8 v4, v4, 0x3f

    or-int/lit16 v4, v4, 0x80

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    .line 818136
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v4, v2, 0x1

    iput v4, p0, LX/4sB;->l:I

    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    aput-byte v0, v1, v2

    move v1, p2

    .line 818137
    goto/16 :goto_0

    .line 818138
    :cond_9
    return-void

    :cond_a
    move v1, v2

    goto/16 :goto_0

    :cond_b
    move v0, v1

    goto/16 :goto_1
.end method

.method private static e(I)V
    .locals 3

    .prologue
    .line 818097
    const v0, 0x10ffff

    if-le p0, v0, :cond_0

    .line 818098
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal character point (0x"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") to output; max is 0x10FFFF as per RFC 4627"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 818099
    :cond_0
    const v0, 0xd800

    if-lt p0, v0, :cond_2

    .line 818100
    const v0, 0xdbff

    if-gt p0, v0, :cond_1

    .line 818101
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unmatched first part of surrogate pair (0x"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 818102
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unmatched second part of surrogate pair (0x"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 818103
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal character point (0x"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") to output"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private e(LX/0lc;)V
    .locals 5

    .prologue
    .line 818076
    invoke-interface {p1}, LX/0lc;->b()I

    move-result v0

    .line 818077
    if-nez v0, :cond_1

    .line 818078
    const/16 v0, 0x20

    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    .line 818079
    :cond_0
    :goto_0
    return-void

    .line 818080
    :cond_1
    iget v1, p0, LX/4sB;->r:I

    if-ltz v1, :cond_2

    .line 818081
    invoke-interface {p1}, LX/0lc;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, LX/4sB;->l(Ljava/lang/String;)I

    move-result v1

    .line 818082
    if-ltz v1, :cond_2

    .line 818083
    invoke-direct {p0, v1}, LX/4sB;->c(I)V

    goto :goto_0

    .line 818084
    :cond_2
    invoke-interface {p1}, LX/0lc;->d()[B

    move-result-object v1

    .line 818085
    array-length v2, v1

    .line 818086
    if-eq v2, v0, :cond_3

    .line 818087
    invoke-direct {p0, p1, v1}, LX/4sB;->a(LX/0lc;[B)V

    goto :goto_0

    .line 818088
    :cond_3
    const/16 v0, 0x40

    if-gt v2, v0, :cond_5

    .line 818089
    iget v0, p0, LX/4sB;->l:I

    add-int/2addr v0, v2

    iget v3, p0, LX/4sB;->m:I

    if-lt v0, v3, :cond_4

    .line 818090
    invoke-direct {p0}, LX/4sB;->n()V

    .line 818091
    :cond_4
    iget-object v0, p0, LX/4sB;->k:[B

    iget v3, p0, LX/4sB;->l:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/4sB;->l:I

    add-int/lit8 v4, v2, 0x7f

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    .line 818092
    const/4 v0, 0x0

    iget-object v3, p0, LX/4sB;->k:[B

    iget v4, p0, LX/4sB;->l:I

    invoke-static {v1, v0, v3, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 818093
    iget v0, p0, LX/4sB;->l:I

    add-int/2addr v0, v2

    iput v0, p0, LX/4sB;->l:I

    .line 818094
    :goto_1
    iget v0, p0, LX/4sB;->r:I

    if-ltz v0, :cond_0

    .line 818095
    invoke-interface {p1}, LX/0lc;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/4sB;->m(Ljava/lang/String;)V

    goto :goto_0

    .line 818096
    :cond_5
    invoke-direct {p0, v1}, LX/4sB;->b([B)V

    goto :goto_1
.end method

.method private final f(I)V
    .locals 2

    .prologue
    .line 818073
    iget v0, p0, LX/4sB;->l:I

    add-int/2addr v0, p1

    iget v1, p0, LX/4sB;->m:I

    if-lt v0, v1, :cond_0

    .line 818074
    invoke-direct {p0}, LX/4sB;->n()V

    .line 818075
    :cond_0
    return-void
.end method

.method private g(I)V
    .locals 8

    .prologue
    const/16 v4, 0x7f

    .line 818046
    const/4 v0, 0x5

    invoke-direct {p0, v0}, LX/4sB;->f(I)V

    .line 818047
    and-int/lit8 v0, p1, 0x3f

    add-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    .line 818048
    shr-int/lit8 v1, p1, 0x6

    .line 818049
    if-gt v1, v4, :cond_1

    .line 818050
    if-lez v1, :cond_0

    .line 818051
    iget-object v2, p0, LX/4sB;->k:[B

    iget v3, p0, LX/4sB;->l:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/4sB;->l:I

    int-to-byte v1, v1

    aput-byte v1, v2, v3

    .line 818052
    :cond_0
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sB;->l:I

    aput-byte v0, v1, v2

    .line 818053
    :goto_0
    return-void

    .line 818054
    :cond_1
    and-int/lit8 v2, v1, 0x7f

    int-to-byte v2, v2

    .line 818055
    shr-int/lit8 v1, v1, 0x7

    .line 818056
    if-gt v1, v4, :cond_2

    .line 818057
    iget-object v3, p0, LX/4sB;->k:[B

    iget v4, p0, LX/4sB;->l:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, LX/4sB;->l:I

    int-to-byte v1, v1

    aput-byte v1, v3, v4

    .line 818058
    iget-object v1, p0, LX/4sB;->k:[B

    iget v3, p0, LX/4sB;->l:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/4sB;->l:I

    aput-byte v2, v1, v3

    .line 818059
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sB;->l:I

    aput-byte v0, v1, v2

    goto :goto_0

    .line 818060
    :cond_2
    and-int/lit8 v3, v1, 0x7f

    int-to-byte v3, v3

    .line 818061
    shr-int/lit8 v1, v1, 0x7

    .line 818062
    if-gt v1, v4, :cond_3

    .line 818063
    iget-object v4, p0, LX/4sB;->k:[B

    iget v5, p0, LX/4sB;->l:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, LX/4sB;->l:I

    int-to-byte v1, v1

    aput-byte v1, v4, v5

    .line 818064
    iget-object v1, p0, LX/4sB;->k:[B

    iget v4, p0, LX/4sB;->l:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, LX/4sB;->l:I

    aput-byte v3, v1, v4

    .line 818065
    iget-object v1, p0, LX/4sB;->k:[B

    iget v3, p0, LX/4sB;->l:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/4sB;->l:I

    aput-byte v2, v1, v3

    .line 818066
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sB;->l:I

    aput-byte v0, v1, v2

    goto :goto_0

    .line 818067
    :cond_3
    and-int/lit8 v4, v1, 0x7f

    int-to-byte v4, v4

    .line 818068
    iget-object v5, p0, LX/4sB;->k:[B

    iget v6, p0, LX/4sB;->l:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, LX/4sB;->l:I

    shr-int/lit8 v1, v1, 0x7

    int-to-byte v1, v1

    aput-byte v1, v5, v6

    .line 818069
    iget-object v1, p0, LX/4sB;->k:[B

    iget v5, p0, LX/4sB;->l:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, LX/4sB;->l:I

    aput-byte v4, v1, v5

    .line 818070
    iget-object v1, p0, LX/4sB;->k:[B

    iget v4, p0, LX/4sB;->l:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, LX/4sB;->l:I

    aput-byte v3, v1, v4

    .line 818071
    iget-object v1, p0, LX/4sB;->k:[B

    iget v3, p0, LX/4sB;->l:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/4sB;->l:I

    aput-byte v2, v1, v3

    .line 818072
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sB;->l:I

    aput-byte v0, v1, v2

    goto/16 :goto_0
.end method

.method private h(I)V
    .locals 1

    .prologue
    .line 818044
    invoke-static {p1}, LX/4sG;->a(I)I

    move-result v0

    invoke-direct {p0, v0}, LX/4sB;->g(I)V

    .line 818045
    return-void
.end method

.method private final j(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/16 v6, 0x38

    const/16 v0, 0x34

    const/4 v5, -0x4

    const/4 v4, 0x0

    .line 818018
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 818019
    if-nez v1, :cond_1

    .line 818020
    const/16 v0, 0x20

    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    .line 818021
    :cond_0
    :goto_0
    return-void

    .line 818022
    :cond_1
    iget v2, p0, LX/4sB;->r:I

    if-ltz v2, :cond_2

    .line 818023
    invoke-direct {p0, p1}, LX/4sB;->l(Ljava/lang/String;)I

    move-result v2

    .line 818024
    if-ltz v2, :cond_2

    .line 818025
    invoke-direct {p0, v2}, LX/4sB;->c(I)V

    goto :goto_0

    .line 818026
    :cond_2
    if-le v1, v6, :cond_3

    .line 818027
    invoke-direct {p0, p1, v1}, LX/4sB;->b(Ljava/lang/String;I)V

    goto :goto_0

    .line 818028
    :cond_3
    iget v2, p0, LX/4sB;->l:I

    add-int/lit16 v2, v2, 0xc4

    iget v3, p0, LX/4sB;->m:I

    if-lt v2, v3, :cond_4

    .line 818029
    invoke-direct {p0}, LX/4sB;->n()V

    .line 818030
    :cond_4
    iget-object v2, p0, LX/4sB;->n:[C

    invoke-virtual {p1, v4, v1, v2, v4}, Ljava/lang/String;->getChars(II[CI)V

    .line 818031
    iget v2, p0, LX/4sB;->l:I

    .line 818032
    iget v3, p0, LX/4sB;->l:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/4sB;->l:I

    .line 818033
    iget-object v3, p0, LX/4sB;->n:[C

    invoke-direct {p0, v3, v4, v1}, LX/4sB;->c([CII)I

    move-result v3

    .line 818034
    if-ne v3, v1, :cond_6

    .line 818035
    const/16 v1, 0x40

    if-gt v3, v1, :cond_5

    .line 818036
    add-int/lit8 v0, v3, 0x7f

    int-to-byte v0, v0

    .line 818037
    :goto_1
    iget-object v1, p0, LX/4sB;->k:[B

    aput-byte v0, v1, v2

    .line 818038
    iget v0, p0, LX/4sB;->r:I

    if-ltz v0, :cond_0

    .line 818039
    invoke-direct {p0, p1}, LX/4sB;->m(Ljava/lang/String;)V

    goto :goto_0

    .line 818040
    :cond_5
    iget-object v1, p0, LX/4sB;->k:[B

    iget v3, p0, LX/4sB;->l:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/4sB;->l:I

    aput-byte v5, v1, v3

    goto :goto_1

    .line 818041
    :cond_6
    if-gt v3, v6, :cond_7

    .line 818042
    add-int/lit16 v0, v3, 0xbe

    int-to-byte v0, v0

    goto :goto_1

    .line 818043
    :cond_7
    iget-object v1, p0, LX/4sB;->k:[B

    iget v3, p0, LX/4sB;->l:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/4sB;->l:I

    aput-byte v5, v1, v3

    goto :goto_1
.end method

.method private k(Ljava/lang/String;)V
    .locals 9

    .prologue
    const/16 v8, 0x7f

    .line 817981
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    .line 817982
    const/4 v1, 0x0

    .line 817983
    iget v0, p0, LX/4sB;->m:I

    add-int/lit8 v4, v0, -0x4

    .line 817984
    :goto_0
    if-ge v1, v3, :cond_9

    .line 817985
    iget v0, p0, LX/4sB;->l:I

    if-lt v0, v4, :cond_0

    .line 817986
    invoke-direct {p0}, LX/4sB;->n()V

    .line 817987
    :cond_0
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 817988
    if-gt v0, v8, :cond_1

    .line 817989
    iget-object v1, p0, LX/4sB;->k:[B

    iget v5, p0, LX/4sB;->l:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, LX/4sB;->l:I

    int-to-byte v0, v0

    aput-byte v0, v1, v5

    .line 817990
    sub-int v1, v3, v2

    .line 817991
    iget v0, p0, LX/4sB;->l:I

    sub-int v0, v4, v0

    .line 817992
    if-le v1, v0, :cond_b

    .line 817993
    :goto_1
    add-int v5, v0, v2

    .line 817994
    :goto_2
    if-ge v2, v5, :cond_a

    .line 817995
    add-int/lit8 v1, v2, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 817996
    if-gt v0, v8, :cond_2

    .line 817997
    iget-object v2, p0, LX/4sB;->k:[B

    iget v6, p0, LX/4sB;->l:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, LX/4sB;->l:I

    int-to-byte v0, v0

    aput-byte v0, v2, v6

    move v2, v1

    goto :goto_2

    :cond_1
    move v1, v2

    .line 817998
    :cond_2
    const/16 v2, 0x800

    if-ge v0, v2, :cond_3

    .line 817999
    iget-object v2, p0, LX/4sB;->k:[B

    iget v5, p0, LX/4sB;->l:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, LX/4sB;->l:I

    shr-int/lit8 v6, v0, 0x6

    or-int/lit16 v6, v6, 0xc0

    int-to-byte v6, v6

    aput-byte v6, v2, v5

    .line 818000
    iget-object v2, p0, LX/4sB;->k:[B

    iget v5, p0, LX/4sB;->l:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, LX/4sB;->l:I

    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    aput-byte v0, v2, v5

    goto :goto_0

    .line 818001
    :cond_3
    const v2, 0xd800

    if-lt v0, v2, :cond_4

    const v2, 0xdfff

    if-le v0, v2, :cond_5

    .line 818002
    :cond_4
    iget-object v2, p0, LX/4sB;->k:[B

    iget v5, p0, LX/4sB;->l:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, LX/4sB;->l:I

    shr-int/lit8 v6, v0, 0xc

    or-int/lit16 v6, v6, 0xe0

    int-to-byte v6, v6

    aput-byte v6, v2, v5

    .line 818003
    iget-object v2, p0, LX/4sB;->k:[B

    iget v5, p0, LX/4sB;->l:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, LX/4sB;->l:I

    shr-int/lit8 v6, v0, 0x6

    and-int/lit8 v6, v6, 0x3f

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, v2, v5

    .line 818004
    iget-object v2, p0, LX/4sB;->k:[B

    iget v5, p0, LX/4sB;->l:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, LX/4sB;->l:I

    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    aput-byte v0, v2, v5

    goto/16 :goto_0

    .line 818005
    :cond_5
    const v2, 0xdbff

    if-le v0, v2, :cond_6

    .line 818006
    invoke-static {v0}, LX/4sB;->e(I)V

    .line 818007
    :cond_6
    if-lt v1, v3, :cond_7

    .line 818008
    invoke-static {v0}, LX/4sB;->e(I)V

    .line 818009
    :cond_7
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v0, v1}, LX/4sB;->a(II)I

    move-result v0

    .line 818010
    const v1, 0x10ffff

    if-le v0, v1, :cond_8

    .line 818011
    invoke-static {v0}, LX/4sB;->e(I)V

    .line 818012
    :cond_8
    iget-object v1, p0, LX/4sB;->k:[B

    iget v5, p0, LX/4sB;->l:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, LX/4sB;->l:I

    shr-int/lit8 v6, v0, 0x12

    or-int/lit16 v6, v6, 0xf0

    int-to-byte v6, v6

    aput-byte v6, v1, v5

    .line 818013
    iget-object v1, p0, LX/4sB;->k:[B

    iget v5, p0, LX/4sB;->l:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, LX/4sB;->l:I

    shr-int/lit8 v6, v0, 0xc

    and-int/lit8 v6, v6, 0x3f

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, v1, v5

    .line 818014
    iget-object v1, p0, LX/4sB;->k:[B

    iget v5, p0, LX/4sB;->l:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, LX/4sB;->l:I

    shr-int/lit8 v6, v0, 0x6

    and-int/lit8 v6, v6, 0x3f

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, v1, v5

    .line 818015
    iget-object v1, p0, LX/4sB;->k:[B

    iget v5, p0, LX/4sB;->l:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, LX/4sB;->l:I

    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    aput-byte v0, v1, v5

    move v1, v2

    .line 818016
    goto/16 :goto_0

    .line 818017
    :cond_9
    return-void

    :cond_a
    move v1, v2

    goto/16 :goto_0

    :cond_b
    move v0, v1

    goto/16 :goto_1
.end method

.method private final l(Ljava/lang/String;)I
    .locals 5

    .prologue
    const/4 v2, -0x1

    .line 817966
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    .line 817967
    iget-object v0, p0, LX/4sB;->q:[LX/4sA;

    iget-object v1, p0, LX/4sB;->q:[LX/4sA;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    and-int/2addr v1, v3

    aget-object v1, v0, v1

    .line 817968
    if-nez v1, :cond_0

    move v0, v2

    .line 817969
    :goto_0
    return v0

    .line 817970
    :cond_0
    iget-object v0, v1, LX/4sA;->a:Ljava/lang/String;

    if-ne v0, p1, :cond_1

    .line 817971
    iget v0, v1, LX/4sA;->b:I

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 817972
    :cond_2
    iget-object v0, v0, LX/4sA;->c:LX/4sA;

    if-eqz v0, :cond_3

    .line 817973
    iget-object v4, v0, LX/4sA;->a:Ljava/lang/String;

    if-ne v4, p1, :cond_2

    .line 817974
    iget v0, v0, LX/4sA;->b:I

    goto :goto_0

    .line 817975
    :cond_3
    iget-object v0, v1, LX/4sA;->a:Ljava/lang/String;

    .line 817976
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v4

    if-ne v4, v3, :cond_4

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 817977
    iget v0, v1, LX/4sA;->b:I

    goto :goto_0

    .line 817978
    :cond_4
    iget-object v1, v1, LX/4sA;->c:LX/4sA;

    .line 817979
    if-nez v1, :cond_3

    move v0, v2

    .line 817980
    goto :goto_0
.end method

.method private static m()LX/4s6;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4s6",
            "<",
            "LX/4sA;",
            ">;"
        }
    .end annotation

    .prologue
    .line 817959
    sget-object v0, LX/4sB;->v:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    .line 817960
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 817961
    :goto_0
    if-nez v0, :cond_0

    .line 817962
    new-instance v0, LX/4s6;

    invoke-direct {v0}, LX/4s6;-><init>()V

    .line 817963
    sget-object v1, LX/4sB;->v:Ljava/lang/ThreadLocal;

    new-instance v2, Ljava/lang/ref/SoftReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 817964
    :cond_0
    return-object v0

    .line 817965
    :cond_1
    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4s6;

    goto :goto_0
.end method

.method private final m(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/16 v4, 0x400

    const/4 v0, 0x0

    .line 817941
    iget v1, p0, LX/4sB;->r:I

    iget-object v2, p0, LX/4sB;->q:[LX/4sA;

    array-length v2, v2

    if-ne v1, v2, :cond_0

    .line 817942
    iget v1, p0, LX/4sB;->r:I

    if-ne v1, v4, :cond_1

    .line 817943
    iget-object v1, p0, LX/4sB;->q:[LX/4sA;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 817944
    iput v0, p0, LX/4sB;->r:I

    .line 817945
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget-object v1, p0, LX/4sB;->q:[LX/4sA;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    .line 817946
    iget-object v1, p0, LX/4sB;->q:[LX/4sA;

    new-instance v2, LX/4sA;

    iget v3, p0, LX/4sB;->r:I

    iget-object v4, p0, LX/4sB;->q:[LX/4sA;

    aget-object v4, v4, v0

    invoke-direct {v2, p1, v3, v4}, LX/4sA;-><init>(Ljava/lang/String;ILX/4sA;)V

    aput-object v2, v1, v0

    .line 817947
    iget v0, p0, LX/4sB;->r:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4sB;->r:I

    .line 817948
    return-void

    .line 817949
    :cond_1
    iget-object v3, p0, LX/4sB;->q:[LX/4sA;

    .line 817950
    new-array v1, v4, [LX/4sA;

    iput-object v1, p0, LX/4sB;->q:[LX/4sA;

    .line 817951
    array-length v4, v3

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    .line 817952
    :goto_1
    if-eqz v0, :cond_2

    .line 817953
    iget-object v1, v0, LX/4sA;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    and-int/lit16 v5, v1, 0x3ff

    .line 817954
    iget-object v1, v0, LX/4sA;->c:LX/4sA;

    .line 817955
    iget-object v6, p0, LX/4sB;->q:[LX/4sA;

    aget-object v6, v6, v5

    iput-object v6, v0, LX/4sA;->c:LX/4sA;

    .line 817956
    iget-object v6, p0, LX/4sB;->q:[LX/4sA;

    aput-object v0, v6, v5

    move-object v0, v1

    .line 817957
    goto :goto_1

    .line 817958
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method private final n(Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 817840
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    .line 817841
    iget-object v0, p0, LX/4sB;->s:[LX/4sA;

    iget-object v1, p0, LX/4sB;->s:[LX/4sA;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    and-int/2addr v1, v2

    aget-object v1, v0, v1

    .line 817842
    if-eqz v1, :cond_4

    move-object v0, v1

    .line 817843
    :cond_0
    iget-object v3, v0, LX/4sA;->a:Ljava/lang/String;

    if-ne v3, p1, :cond_1

    .line 817844
    iget v0, v0, LX/4sA;->b:I

    .line 817845
    :goto_0
    return v0

    .line 817846
    :cond_1
    iget-object v0, v0, LX/4sA;->c:LX/4sA;

    .line 817847
    if-nez v0, :cond_0

    .line 817848
    :cond_2
    iget-object v0, v1, LX/4sA;->a:Ljava/lang/String;

    .line 817849
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v3

    if-ne v3, v2, :cond_3

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 817850
    iget v0, v1, LX/4sA;->b:I

    goto :goto_0

    .line 817851
    :cond_3
    iget-object v1, v1, LX/4sA;->c:LX/4sA;

    .line 817852
    if-nez v1, :cond_2

    .line 817853
    :cond_4
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private n()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 817930
    iget v0, p0, LX/4sB;->l:I

    if-lez v0, :cond_0

    .line 817931
    iget v0, p0, LX/4sB;->p:I

    iget v1, p0, LX/4sB;->l:I

    add-int/2addr v0, v1

    iput v0, p0, LX/4sB;->p:I

    .line 817932
    iget-object v0, p0, LX/4sB;->h:Ljava/io/OutputStream;

    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 817933
    iput v3, p0, LX/4sB;->l:I

    .line 817934
    :cond_0
    return-void
.end method

.method private static o()Ljava/lang/UnsupportedOperationException;
    .locals 1

    .prologue
    .line 817625
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    return-object v0
.end method

.method private final o(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/16 v4, 0x400

    const/4 v0, 0x0

    .line 817607
    iget v1, p0, LX/4sB;->t:I

    iget-object v2, p0, LX/4sB;->s:[LX/4sA;

    array-length v2, v2

    if-ne v1, v2, :cond_0

    .line 817608
    iget v1, p0, LX/4sB;->t:I

    if-ne v1, v4, :cond_1

    .line 817609
    iget-object v1, p0, LX/4sB;->s:[LX/4sA;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 817610
    iput v0, p0, LX/4sB;->t:I

    .line 817611
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget-object v1, p0, LX/4sB;->s:[LX/4sA;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    .line 817612
    iget-object v1, p0, LX/4sB;->s:[LX/4sA;

    new-instance v2, LX/4sA;

    iget v3, p0, LX/4sB;->t:I

    iget-object v4, p0, LX/4sB;->s:[LX/4sA;

    aget-object v4, v4, v0

    invoke-direct {v2, p1, v3, v4}, LX/4sA;-><init>(Ljava/lang/String;ILX/4sA;)V

    aput-object v2, v1, v0

    .line 817613
    iget v0, p0, LX/4sB;->t:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4sB;->t:I

    .line 817614
    return-void

    .line 817615
    :cond_1
    iget-object v3, p0, LX/4sB;->s:[LX/4sA;

    .line 817616
    new-array v1, v4, [LX/4sA;

    iput-object v1, p0, LX/4sB;->s:[LX/4sA;

    .line 817617
    array-length v4, v3

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    .line 817618
    :goto_1
    if-eqz v0, :cond_2

    .line 817619
    iget-object v1, v0, LX/4sA;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    and-int/lit16 v5, v1, 0x3ff

    .line 817620
    iget-object v1, v0, LX/4sA;->c:LX/4sA;

    .line 817621
    iget-object v6, p0, LX/4sB;->s:[LX/4sA;

    aget-object v6, v6, v5

    iput-object v6, v0, LX/4sA;->c:LX/4sA;

    .line 817622
    iget-object v6, p0, LX/4sB;->s:[LX/4sA;

    aput-object v0, v6, v5

    move-object v0, v1

    .line 817623
    goto :goto_1

    .line 817624
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0lZ;)LX/0nX;
    .locals 0

    .prologue
    .line 817606
    return-object p0
.end method

.method public final a(C)V
    .locals 1

    .prologue
    .line 817605
    invoke-static {}, LX/4sB;->o()Ljava/lang/UnsupportedOperationException;

    move-result-object v0

    throw v0
.end method

.method public final a(D)V
    .locals 7

    .prologue
    .line 817578
    const/16 v0, 0xb

    invoke-direct {p0, v0}, LX/4sB;->f(I)V

    .line 817579
    const-string v0, "write number"

    invoke-virtual {p0, v0}, LX/4sB;->h(Ljava/lang/String;)V

    .line 817580
    invoke-static {p1, p2}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    .line 817581
    iget-object v2, p0, LX/4sB;->k:[B

    iget v3, p0, LX/4sB;->l:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/4sB;->l:I

    const/16 v4, 0x29

    aput-byte v4, v2, v3

    .line 817582
    const/16 v2, 0x23

    ushr-long v2, v0, v2

    long-to-int v2, v2

    .line 817583
    iget-object v3, p0, LX/4sB;->k:[B

    iget v4, p0, LX/4sB;->l:I

    add-int/lit8 v4, v4, 0x4

    and-int/lit8 v5, v2, 0x7f

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 817584
    shr-int/lit8 v2, v2, 0x7

    .line 817585
    iget-object v3, p0, LX/4sB;->k:[B

    iget v4, p0, LX/4sB;->l:I

    add-int/lit8 v4, v4, 0x3

    and-int/lit8 v5, v2, 0x7f

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 817586
    shr-int/lit8 v2, v2, 0x7

    .line 817587
    iget-object v3, p0, LX/4sB;->k:[B

    iget v4, p0, LX/4sB;->l:I

    add-int/lit8 v4, v4, 0x2

    and-int/lit8 v5, v2, 0x7f

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 817588
    shr-int/lit8 v2, v2, 0x7

    .line 817589
    iget-object v3, p0, LX/4sB;->k:[B

    iget v4, p0, LX/4sB;->l:I

    add-int/lit8 v4, v4, 0x1

    and-int/lit8 v5, v2, 0x7f

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 817590
    shr-int/lit8 v2, v2, 0x7

    .line 817591
    iget-object v3, p0, LX/4sB;->k:[B

    iget v4, p0, LX/4sB;->l:I

    int-to-byte v2, v2

    aput-byte v2, v3, v4

    .line 817592
    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v2, v2, 0x5

    iput v2, p0, LX/4sB;->l:I

    .line 817593
    const/16 v2, 0x1c

    shr-long v2, v0, v2

    long-to-int v2, v2

    .line 817594
    iget-object v3, p0, LX/4sB;->k:[B

    iget v4, p0, LX/4sB;->l:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, LX/4sB;->l:I

    and-int/lit8 v2, v2, 0x7f

    int-to-byte v2, v2

    aput-byte v2, v3, v4

    .line 817595
    long-to-int v0, v0

    .line 817596
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v2, v2, 0x3

    and-int/lit8 v3, v0, 0x7f

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 817597
    shr-int/lit8 v0, v0, 0x7

    .line 817598
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v2, v2, 0x2

    and-int/lit8 v3, v0, 0x7f

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 817599
    shr-int/lit8 v0, v0, 0x7

    .line 817600
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v2, v2, 0x1

    and-int/lit8 v3, v0, 0x7f

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 817601
    shr-int/lit8 v0, v0, 0x7

    .line 817602
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    and-int/lit8 v0, v0, 0x7f

    int-to-byte v0, v0

    aput-byte v0, v1, v2

    .line 817603
    iget v0, p0, LX/4sB;->l:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/4sB;->l:I

    .line 817604
    return-void
.end method

.method public final a(F)V
    .locals 4

    .prologue
    .line 817563
    const/4 v0, 0x6

    invoke-direct {p0, v0}, LX/4sB;->f(I)V

    .line 817564
    const-string v0, "write number"

    invoke-virtual {p0, v0}, LX/4sB;->h(Ljava/lang/String;)V

    .line 817565
    invoke-static {p1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    .line 817566
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sB;->l:I

    const/16 v3, 0x28

    aput-byte v3, v1, v2

    .line 817567
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v2, v2, 0x4

    and-int/lit8 v3, v0, 0x7f

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 817568
    shr-int/lit8 v0, v0, 0x7

    .line 817569
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v2, v2, 0x3

    and-int/lit8 v3, v0, 0x7f

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 817570
    shr-int/lit8 v0, v0, 0x7

    .line 817571
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v2, v2, 0x2

    and-int/lit8 v3, v0, 0x7f

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 817572
    shr-int/lit8 v0, v0, 0x7

    .line 817573
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v2, v2, 0x1

    and-int/lit8 v3, v0, 0x7f

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 817574
    shr-int/lit8 v0, v0, 0x7

    .line 817575
    iget-object v1, p0, LX/4sB;->k:[B

    iget v2, p0, LX/4sB;->l:I

    and-int/lit8 v0, v0, 0x7f

    int-to-byte v0, v0

    aput-byte v0, v1, v2

    .line 817576
    iget v0, p0, LX/4sB;->l:I

    add-int/lit8 v0, v0, 0x5

    iput v0, p0, LX/4sB;->l:I

    .line 817577
    return-void
.end method

.method public final a(J)V
    .locals 13

    .prologue
    .line 817526
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    const-wide/32 v0, -0x80000000

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    .line 817527
    long-to-int v0, p1

    invoke-virtual {p0, v0}, LX/0nX;->b(I)V

    .line 817528
    :goto_0
    return-void

    .line 817529
    :cond_0
    const-string v0, "write number"

    invoke-virtual {p0, v0}, LX/4sB;->h(Ljava/lang/String;)V

    .line 817530
    invoke-static {p1, p2}, LX/4sG;->a(J)J

    move-result-wide v0

    .line 817531
    long-to-int v2, v0

    .line 817532
    and-int/lit8 v3, v2, 0x3f

    add-int/lit16 v3, v3, 0x80

    int-to-byte v6, v3

    .line 817533
    shr-int/lit8 v3, v2, 0x6

    and-int/lit8 v3, v3, 0x7f

    int-to-byte v5, v3

    .line 817534
    shr-int/lit8 v3, v2, 0xd

    and-int/lit8 v3, v3, 0x7f

    int-to-byte v4, v3

    .line 817535
    shr-int/lit8 v2, v2, 0x14

    and-int/lit8 v2, v2, 0x7f

    int-to-byte v3, v2

    .line 817536
    const/16 v2, 0x1b

    ushr-long/2addr v0, v2

    .line 817537
    long-to-int v2, v0

    and-int/lit8 v2, v2, 0x7f

    int-to-byte v2, v2

    .line 817538
    const/4 v7, 0x7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    .line 817539
    if-nez v0, :cond_1

    .line 817540
    const/16 v1, 0x25

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, LX/4sB;->a(BBBBBB)V

    goto :goto_0

    .line 817541
    :cond_1
    const/16 v1, 0x7f

    if-gt v0, v1, :cond_2

    .line 817542
    const/16 v1, 0x25

    int-to-byte v0, v0

    invoke-direct {p0, v1, v0}, LX/4sB;->a(BB)V

    move-object v1, p0

    .line 817543
    invoke-direct/range {v1 .. v6}, LX/4sB;->a(BBBBB)V

    goto :goto_0

    .line 817544
    :cond_2
    and-int/lit8 v1, v0, 0x7f

    int-to-byte v1, v1

    .line 817545
    shr-int/lit8 v0, v0, 0x7

    .line 817546
    const/16 v7, 0x7f

    if-gt v0, v7, :cond_3

    .line 817547
    const/16 v7, 0x25

    int-to-byte v0, v0

    invoke-direct {p0, v7, v0}, LX/4sB;->a(BB)V

    move-object v0, p0

    .line 817548
    invoke-direct/range {v0 .. v6}, LX/4sB;->a(BBBBBB)V

    goto :goto_0

    .line 817549
    :cond_3
    and-int/lit8 v7, v0, 0x7f

    int-to-byte v12, v7

    .line 817550
    shr-int/lit8 v0, v0, 0x7

    .line 817551
    const/16 v7, 0x7f

    if-gt v0, v7, :cond_4

    .line 817552
    const/16 v7, 0x25

    int-to-byte v0, v0

    invoke-direct {p0, v7, v0, v12}, LX/4sB;->a(BBB)V

    move-object v0, p0

    .line 817553
    invoke-direct/range {v0 .. v6}, LX/4sB;->a(BBBBBB)V

    goto :goto_0

    .line 817554
    :cond_4
    and-int/lit8 v7, v0, 0x7f

    int-to-byte v11, v7

    .line 817555
    shr-int/lit8 v0, v0, 0x7

    .line 817556
    const/16 v7, 0x7f

    if-gt v0, v7, :cond_5

    .line 817557
    const/16 v7, 0x25

    int-to-byte v0, v0

    invoke-direct {p0, v7, v0, v11, v12}, LX/4sB;->a(BBBB)V

    move-object v0, p0

    .line 817558
    invoke-direct/range {v0 .. v6}, LX/4sB;->a(BBBBBB)V

    goto :goto_0

    .line 817559
    :cond_5
    and-int/lit8 v7, v0, 0x7f

    int-to-byte v10, v7

    .line 817560
    shr-int/lit8 v0, v0, 0x7

    .line 817561
    const/16 v8, 0x25

    int-to-byte v9, v0

    move-object v7, p0

    invoke-direct/range {v7 .. v12}, LX/4sB;->a(BBBBB)V

    move-object v0, p0

    .line 817562
    invoke-direct/range {v0 .. v6}, LX/4sB;->a(BBBBBB)V

    goto/16 :goto_0
.end method

.method public final a(LX/0ln;[BII)V
    .locals 1

    .prologue
    .line 817516
    if-nez p2, :cond_0

    .line 817517
    invoke-virtual {p0}, LX/0nX;->h()V

    .line 817518
    :goto_0
    return-void

    .line 817519
    :cond_0
    const-string v0, "write Binary value"

    invoke-virtual {p0, v0}, LX/4sB;->h(Ljava/lang/String;)V

    .line 817520
    sget-object v0, LX/4s9;->ENCODE_BINARY_AS_7BIT:LX/4s9;

    invoke-direct {p0, v0}, LX/4sB;->a(LX/4s9;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 817521
    const/16 v0, -0x18

    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    .line 817522
    invoke-direct {p0, p2, p3, p4}, LX/4sB;->c([BII)V

    goto :goto_0

    .line 817523
    :cond_1
    const/4 v0, -0x3

    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    .line 817524
    invoke-direct {p0, p4}, LX/4sB;->g(I)V

    .line 817525
    invoke-direct {p0, p2, p3, p4}, LX/4sB;->a([BII)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 817512
    iget-object v0, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v0, p1}, LX/12U;->a(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 817513
    const-string v0, "Can not write a field name, expecting a value"

    invoke-static {v0}, LX/12G;->i(Ljava/lang/String;)V

    .line 817514
    :cond_0
    invoke-direct {p0, p1}, LX/4sB;->j(Ljava/lang/String;)V

    .line 817515
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 817507
    iget-object v0, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v0, p1}, LX/12U;->a(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 817508
    const-string v0, "Can not write a field name, expecting a value"

    invoke-static {v0}, LX/12G;->i(Ljava/lang/String;)V

    .line 817509
    :cond_0
    invoke-direct {p0, p1}, LX/4sB;->j(Ljava/lang/String;)V

    .line 817510
    invoke-virtual {p0, p2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 817511
    return-void
.end method

.method public final a(Ljava/math/BigDecimal;)V
    .locals 3

    .prologue
    .line 817497
    if-nez p1, :cond_0

    .line 817498
    invoke-virtual {p0}, LX/0nX;->h()V

    .line 817499
    :goto_0
    return-void

    .line 817500
    :cond_0
    const-string v0, "write number"

    invoke-virtual {p0, v0}, LX/4sB;->h(Ljava/lang/String;)V

    .line 817501
    const/16 v0, 0x2a

    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    .line 817502
    invoke-virtual {p1}, Ljava/math/BigDecimal;->scale()I

    move-result v0

    .line 817503
    invoke-direct {p0, v0}, LX/4sB;->h(I)V

    .line 817504
    invoke-virtual {p1}, Ljava/math/BigDecimal;->unscaledValue()Ljava/math/BigInteger;

    move-result-object v0

    .line 817505
    invoke-virtual {v0}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v0

    .line 817506
    const/4 v1, 0x0

    array-length v2, v0

    invoke-direct {p0, v0, v1, v2}, LX/4sB;->c([BII)V

    goto :goto_0
.end method

.method public final a(Ljava/math/BigInteger;)V
    .locals 3

    .prologue
    .line 817490
    if-nez p1, :cond_0

    .line 817491
    invoke-virtual {p0}, LX/0nX;->h()V

    .line 817492
    :goto_0
    return-void

    .line 817493
    :cond_0
    const-string v0, "write number"

    invoke-virtual {p0, v0}, LX/4sB;->h(Ljava/lang/String;)V

    .line 817494
    const/16 v0, 0x26

    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    .line 817495
    invoke-virtual {p1}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v0

    .line 817496
    const/4 v1, 0x0

    array-length v2, v0

    invoke-direct {p0, v0, v1, v2}, LX/4sB;->c([BII)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 817485
    const-string v0, "write boolean value"

    invoke-virtual {p0, v0}, LX/4sB;->h(Ljava/lang/String;)V

    .line 817486
    if-eqz p1, :cond_0

    .line 817487
    const/16 v0, 0x23

    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    .line 817488
    :goto_0
    return-void

    .line 817489
    :cond_0
    const/16 v0, 0x22

    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    goto :goto_0
.end method

.method public final a([CII)V
    .locals 6

    .prologue
    const/16 v3, 0x40

    const/4 v5, -0x4

    const/16 v0, -0x1c

    .line 817454
    const/16 v1, 0x41

    if-gt p3, v1, :cond_0

    iget v1, p0, LX/4sB;->t:I

    if-ltz v1, :cond_0

    if-lez p3, :cond_0

    .line 817455
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {p0, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 817456
    :goto_0
    return-void

    .line 817457
    :cond_0
    const-string v1, "write String value"

    invoke-virtual {p0, v1}, LX/4sB;->h(Ljava/lang/String;)V

    .line 817458
    if-nez p3, :cond_1

    .line 817459
    const/16 v0, 0x20

    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    goto :goto_0

    .line 817460
    :cond_1
    if-gt p3, v3, :cond_5

    .line 817461
    iget v1, p0, LX/4sB;->l:I

    add-int/lit16 v1, v1, 0xc4

    iget v2, p0, LX/4sB;->m:I

    if-lt v1, v2, :cond_2

    .line 817462
    invoke-direct {p0}, LX/4sB;->n()V

    .line 817463
    :cond_2
    iget v1, p0, LX/4sB;->l:I

    .line 817464
    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/4sB;->l:I

    .line 817465
    add-int v2, p2, p3

    invoke-direct {p0, p1, p2, v2}, LX/4sB;->c([CII)I

    move-result v2

    .line 817466
    if-gt v2, v3, :cond_4

    .line 817467
    if-ne v2, p3, :cond_3

    .line 817468
    add-int/lit8 v0, v2, 0x3f

    int-to-byte v0, v0

    .line 817469
    :goto_1
    iget-object v2, p0, LX/4sB;->k:[B

    aput-byte v0, v2, v1

    goto :goto_0

    .line 817470
    :cond_3
    add-int/lit8 v0, v2, 0x7e

    int-to-byte v0, v0

    goto :goto_1

    .line 817471
    :cond_4
    iget-object v2, p0, LX/4sB;->k:[B

    iget v3, p0, LX/4sB;->l:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/4sB;->l:I

    aput-byte v5, v2, v3

    goto :goto_1

    .line 817472
    :cond_5
    add-int v1, p3, p3

    add-int/2addr v1, p3

    add-int/lit8 v1, v1, 0x2

    .line 817473
    iget-object v2, p0, LX/4sB;->k:[B

    array-length v2, v2

    if-gt v1, v2, :cond_8

    .line 817474
    iget v2, p0, LX/4sB;->l:I

    add-int/2addr v1, v2

    iget v2, p0, LX/4sB;->m:I

    if-lt v1, v2, :cond_6

    .line 817475
    invoke-direct {p0}, LX/4sB;->n()V

    .line 817476
    :cond_6
    iget v1, p0, LX/4sB;->l:I

    .line 817477
    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    .line 817478
    add-int v0, p2, p3

    invoke-direct {p0, p1, p2, v0}, LX/4sB;->c([CII)I

    move-result v0

    .line 817479
    if-ne v0, p3, :cond_7

    .line 817480
    iget-object v0, p0, LX/4sB;->k:[B

    const/16 v2, -0x20

    aput-byte v2, v0, v1

    .line 817481
    :cond_7
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    aput-byte v5, v0, v1

    goto :goto_0

    .line 817482
    :cond_8
    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    .line 817483
    add-int v0, p2, p3

    invoke-direct {p0, p1, p2, v0}, LX/4sB;->d([CII)V

    .line 817484
    invoke-direct {p0, v5}, LX/4sB;->a(B)V

    goto/16 :goto_0
.end method

.method public final b(I)V
    .locals 13

    .prologue
    const/16 v6, 0x7f

    const/16 v1, 0x24

    .line 817433
    const-string v0, "write number"

    invoke-virtual {p0, v0}, LX/4sB;->h(Ljava/lang/String;)V

    .line 817434
    invoke-static {p1}, LX/4sG;->a(I)I

    move-result v0

    .line 817435
    const/16 v2, 0x3f

    if-gt v0, v2, :cond_1

    if-ltz v0, :cond_1

    .line 817436
    const/16 v2, 0x1f

    if-gt v0, v2, :cond_0

    .line 817437
    add-int/lit16 v0, v0, 0xc0

    int-to-byte v0, v0

    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    .line 817438
    :goto_0
    return-void

    .line 817439
    :cond_0
    add-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    invoke-direct {p0, v1, v0}, LX/4sB;->a(BB)V

    goto :goto_0

    .line 817440
    :cond_1
    and-int/lit8 v2, v0, 0x3f

    add-int/lit16 v2, v2, 0x80

    int-to-byte v5, v2

    .line 817441
    ushr-int/lit8 v0, v0, 0x6

    .line 817442
    if-gt v0, v6, :cond_2

    .line 817443
    int-to-byte v0, v0

    invoke-direct {p0, v1, v0, v5}, LX/4sB;->a(BBB)V

    goto :goto_0

    .line 817444
    :cond_2
    and-int/lit8 v2, v0, 0x7f

    int-to-byte v4, v2

    .line 817445
    shr-int/lit8 v0, v0, 0x7

    .line 817446
    if-gt v0, v6, :cond_3

    .line 817447
    int-to-byte v0, v0

    invoke-direct {p0, v1, v0, v4, v5}, LX/4sB;->a(BBBB)V

    goto :goto_0

    .line 817448
    :cond_3
    and-int/lit8 v2, v0, 0x7f

    int-to-byte v3, v2

    .line 817449
    shr-int/lit8 v0, v0, 0x7

    .line 817450
    if-gt v0, v6, :cond_4

    .line 817451
    int-to-byte v2, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, LX/4sB;->a(BBBBB)V

    goto :goto_0

    .line 817452
    :cond_4
    and-int/lit8 v2, v0, 0x7f

    int-to-byte v9, v2

    .line 817453
    shr-int/lit8 v0, v0, 0x7

    int-to-byte v8, v0

    move-object v6, p0

    move v7, v1

    move v10, v3

    move v11, v4

    move v12, v5

    invoke-direct/range {v6 .. v12}, LX/4sB;->a(BBBBBB)V

    goto :goto_0
.end method

.method public final b(LX/0lc;)V
    .locals 2

    .prologue
    .line 817429
    iget-object v0, p0, LX/12G;->e:LX/12U;

    invoke-interface {p1}, LX/0lc;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/12U;->a(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 817430
    const-string v0, "Can not write a field name, expecting a value"

    invoke-static {v0}, LX/12G;->i(Ljava/lang/String;)V

    .line 817431
    :cond_0
    invoke-direct {p0, p1}, LX/4sB;->e(LX/0lc;)V

    .line 817432
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 817401
    if-nez p1, :cond_0

    .line 817402
    invoke-virtual {p0}, LX/0nX;->h()V

    .line 817403
    :goto_0
    return-void

    .line 817404
    :cond_0
    const-string v0, "write String value"

    invoke-virtual {p0, v0}, LX/4sB;->h(Ljava/lang/String;)V

    .line 817405
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 817406
    if-nez v0, :cond_1

    .line 817407
    const/16 v0, 0x20

    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    goto :goto_0

    .line 817408
    :cond_1
    const/16 v1, 0x41

    if-le v0, v1, :cond_2

    .line 817409
    invoke-direct {p0, p1, v0}, LX/4sB;->c(Ljava/lang/String;I)V

    goto :goto_0

    .line 817410
    :cond_2
    iget v1, p0, LX/4sB;->t:I

    if-ltz v1, :cond_3

    .line 817411
    invoke-direct {p0, p1}, LX/4sB;->n(Ljava/lang/String;)I

    move-result v1

    .line 817412
    if-ltz v1, :cond_3

    .line 817413
    invoke-direct {p0, v1}, LX/4sB;->d(I)V

    goto :goto_0

    .line 817414
    :cond_3
    iget v1, p0, LX/4sB;->l:I

    add-int/lit16 v1, v1, 0xc4

    iget v2, p0, LX/4sB;->m:I

    if-lt v1, v2, :cond_4

    .line 817415
    invoke-direct {p0}, LX/4sB;->n()V

    .line 817416
    :cond_4
    iget-object v1, p0, LX/4sB;->n:[C

    invoke-virtual {p1, v3, v0, v1, v3}, Ljava/lang/String;->getChars(II[CI)V

    .line 817417
    iget v1, p0, LX/4sB;->l:I

    .line 817418
    iget v2, p0, LX/4sB;->l:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/4sB;->l:I

    .line 817419
    iget-object v2, p0, LX/4sB;->n:[C

    invoke-direct {p0, v2, v3, v0}, LX/4sB;->c([CII)I

    move-result v2

    .line 817420
    const/16 v3, 0x40

    if-gt v2, v3, :cond_7

    .line 817421
    iget v3, p0, LX/4sB;->t:I

    if-ltz v3, :cond_5

    .line 817422
    invoke-direct {p0, p1}, LX/4sB;->o(Ljava/lang/String;)V

    .line 817423
    :cond_5
    if-ne v2, v0, :cond_6

    .line 817424
    iget-object v0, p0, LX/4sB;->k:[B

    add-int/lit8 v2, v2, 0x3f

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    goto :goto_0

    .line 817425
    :cond_6
    iget-object v0, p0, LX/4sB;->k:[B

    add-int/lit8 v2, v2, 0x7e

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    goto :goto_0

    .line 817426
    :cond_7
    iget-object v3, p0, LX/4sB;->k:[B

    if-ne v2, v0, :cond_8

    const/16 v0, -0x20

    :goto_1
    aput-byte v0, v3, v1

    .line 817427
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sB;->l:I

    const/4 v2, -0x4

    aput-byte v2, v0, v1

    goto :goto_0

    .line 817428
    :cond_8
    const/16 v0, -0x1c

    goto :goto_1
.end method

.method public final b([CII)V
    .locals 1

    .prologue
    .line 817682
    invoke-static {}, LX/4sB;->o()Ljava/lang/UnsupportedOperationException;

    move-result-object v0

    throw v0
.end method

.method public final c()LX/0nX;
    .locals 0

    .prologue
    .line 817741
    return-object p0
.end method

.method public final c(LX/0lc;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 817714
    const-string v0, "write String value"

    invoke-virtual {p0, v0}, LX/4sB;->h(Ljava/lang/String;)V

    .line 817715
    invoke-interface {p1}, LX/0lc;->a()Ljava/lang/String;

    move-result-object v0

    .line 817716
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    .line 817717
    if-nez v1, :cond_1

    .line 817718
    const/16 v0, 0x20

    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    .line 817719
    :cond_0
    :goto_0
    return-void

    .line 817720
    :cond_1
    const/16 v2, 0x41

    if-gt v1, v2, :cond_2

    iget v2, p0, LX/4sB;->t:I

    if-ltz v2, :cond_2

    .line 817721
    invoke-direct {p0, v0}, LX/4sB;->n(Ljava/lang/String;)I

    move-result v0

    .line 817722
    if-ltz v0, :cond_2

    .line 817723
    invoke-direct {p0, v0}, LX/4sB;->d(I)V

    goto :goto_0

    .line 817724
    :cond_2
    invoke-interface {p1}, LX/0lc;->d()[B

    move-result-object v2

    .line 817725
    array-length v3, v2

    .line 817726
    const/16 v0, 0x40

    if-gt v3, v0, :cond_5

    .line 817727
    iget v0, p0, LX/4sB;->l:I

    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x1

    iget v4, p0, LX/4sB;->m:I

    if-lt v0, v4, :cond_3

    .line 817728
    invoke-direct {p0}, LX/4sB;->n()V

    .line 817729
    :cond_3
    if-ne v3, v1, :cond_4

    add-int/lit8 v0, v3, 0x3f

    .line 817730
    :goto_1
    iget-object v1, p0, LX/4sB;->k:[B

    iget v4, p0, LX/4sB;->l:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, LX/4sB;->l:I

    int-to-byte v0, v0

    aput-byte v0, v1, v4

    .line 817731
    iget-object v0, p0, LX/4sB;->k:[B

    iget v1, p0, LX/4sB;->l:I

    invoke-static {v2, v6, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 817732
    iget v0, p0, LX/4sB;->l:I

    add-int/2addr v0, v3

    iput v0, p0, LX/4sB;->l:I

    .line 817733
    iget v0, p0, LX/4sB;->t:I

    if-ltz v0, :cond_0

    .line 817734
    invoke-interface {p1}, LX/0lc;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/4sB;->o(Ljava/lang/String;)V

    goto :goto_0

    .line 817735
    :cond_4
    add-int/lit8 v0, v3, 0x7e

    goto :goto_1

    .line 817736
    :cond_5
    if-ne v3, v1, :cond_6

    const/16 v0, -0x20

    .line 817737
    :goto_2
    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    .line 817738
    array-length v0, v2

    invoke-direct {p0, v2, v6, v0}, LX/4sB;->a([BII)V

    .line 817739
    const/4 v0, -0x4

    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    goto :goto_0

    .line 817740
    :cond_6
    const/16 v0, -0x1c

    goto :goto_2
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 817713
    invoke-static {}, LX/4sB;->o()Ljava/lang/UnsupportedOperationException;

    move-result-object v0

    throw v0
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 817695
    iget-object v0, p0, LX/4sB;->k:[B

    if-eqz v0, :cond_1

    sget-object v0, LX/0ls;->AUTO_CLOSE_JSON_CONTENT:LX/0ls;

    invoke-virtual {p0, v0}, LX/12G;->a(LX/0ls;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 817696
    :goto_0
    iget-object v0, p0, LX/12G;->e:LX/12U;

    move-object v0, v0

    .line 817697
    invoke-virtual {v0}, LX/12V;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 817698
    invoke-virtual {p0}, LX/0nX;->e()V

    goto :goto_0

    .line 817699
    :cond_0
    invoke-virtual {v0}, LX/12V;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 817700
    invoke-virtual {p0}, LX/0nX;->g()V

    goto :goto_0

    .line 817701
    :cond_1
    iget-boolean v0, p0, LX/12G;->f:Z

    .line 817702
    invoke-super {p0}, LX/12G;->close()V

    .line 817703
    if-nez v0, :cond_2

    sget-object v0, LX/4s9;->WRITE_END_MARKER:LX/4s9;

    invoke-direct {p0, v0}, LX/4sB;->a(LX/4s9;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 817704
    const/4 v0, -0x1

    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    .line 817705
    :cond_2
    invoke-direct {p0}, LX/4sB;->n()V

    .line 817706
    iget-object v0, p0, LX/4sB;->g:LX/12A;

    .line 817707
    iget-boolean v1, v0, LX/12A;->c:Z

    move v0, v1

    .line 817708
    if-nez v0, :cond_3

    sget-object v0, LX/0ls;->AUTO_CLOSE_TARGET:LX/0ls;

    invoke-virtual {p0, v0}, LX/12G;->a(LX/0ls;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 817709
    :cond_3
    iget-object v0, p0, LX/4sB;->h:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 817710
    :goto_1
    invoke-virtual {p0}, LX/4sB;->j()V

    .line 817711
    return-void

    .line 817712
    :cond_4
    iget-object v0, p0, LX/4sB;->h:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    goto :goto_1
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 817691
    const-string v0, "start an array"

    invoke-virtual {p0, v0}, LX/4sB;->h(Ljava/lang/String;)V

    .line 817692
    iget-object v0, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v0}, LX/12U;->j()LX/12U;

    move-result-object v0

    iput-object v0, p0, LX/4sB;->e:LX/12U;

    .line 817693
    const/4 v0, -0x8

    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    .line 817694
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 817690
    invoke-static {}, LX/4sB;->o()Ljava/lang/UnsupportedOperationException;

    move-result-object v0

    throw v0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 817683
    iget-object v0, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v0}, LX/12V;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 817684
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Current context not an ARRAY but "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v1}, LX/12V;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/12G;->i(Ljava/lang/String;)V

    .line 817685
    :cond_0
    const/4 v0, -0x7

    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    .line 817686
    iget-object v0, p0, LX/12G;->e:LX/12U;

    .line 817687
    iget-object v1, v0, LX/12U;->c:LX/12U;

    move-object v0, v1

    .line 817688
    iput-object v0, p0, LX/4sB;->e:LX/12U;

    .line 817689
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 817626
    invoke-static {}, LX/4sB;->o()Ljava/lang/UnsupportedOperationException;

    move-result-object v0

    throw v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 817678
    const-string v0, "start an object"

    invoke-virtual {p0, v0}, LX/4sB;->h(Ljava/lang/String;)V

    .line 817679
    iget-object v0, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v0}, LX/12U;->k()LX/12U;

    move-result-object v0

    iput-object v0, p0, LX/4sB;->e:LX/12U;

    .line 817680
    const/4 v0, -0x6

    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    .line 817681
    return-void
.end method

.method public final flush()V
    .locals 1

    .prologue
    .line 817674
    invoke-direct {p0}, LX/4sB;->n()V

    .line 817675
    sget-object v0, LX/0ls;->FLUSH_PASSED_TO_STREAM:LX/0ls;

    invoke-virtual {p0, v0}, LX/12G;->a(LX/0ls;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 817676
    iget-object v0, p0, LX/4sB;->h:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 817677
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 817667
    iget-object v0, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v0}, LX/12V;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 817668
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Current context not an object but "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v1}, LX/12V;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/12G;->i(Ljava/lang/String;)V

    .line 817669
    :cond_0
    iget-object v0, p0, LX/12G;->e:LX/12U;

    .line 817670
    iget-object v1, v0, LX/12U;->c:LX/12U;

    move-object v0, v1

    .line 817671
    iput-object v0, p0, LX/4sB;->e:LX/12U;

    .line 817672
    const/4 v0, -0x5

    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    .line 817673
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 817664
    const-string v0, "write null value"

    invoke-virtual {p0, v0}, LX/4sB;->h(Ljava/lang/String;)V

    .line 817665
    const/16 v0, 0x21

    invoke-direct {p0, v0}, LX/4sB;->a(B)V

    .line 817666
    return-void
.end method

.method public final h(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 817660
    iget-object v0, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v0}, LX/12U;->m()I

    move-result v0

    .line 817661
    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 817662
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Can not "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", expecting field name"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/12G;->i(Ljava/lang/String;)V

    .line 817663
    :cond_0
    return-void
.end method

.method public final j()V
    .locals 4

    .prologue
    const/16 v3, 0x40

    const/4 v2, 0x0

    .line 817637
    iget-object v0, p0, LX/4sB;->k:[B

    .line 817638
    if-eqz v0, :cond_0

    iget-boolean v1, p0, LX/4sB;->u:Z

    if-eqz v1, :cond_0

    .line 817639
    iput-object v2, p0, LX/4sB;->k:[B

    .line 817640
    iget-object v1, p0, LX/4sB;->g:LX/12A;

    invoke-virtual {v1, v0}, LX/12A;->b([B)V

    .line 817641
    :cond_0
    iget-object v0, p0, LX/4sB;->n:[C

    .line 817642
    if-eqz v0, :cond_1

    .line 817643
    iput-object v2, p0, LX/4sB;->n:[C

    .line 817644
    iget-object v1, p0, LX/4sB;->g:LX/12A;

    invoke-virtual {v1, v0}, LX/12A;->b([C)V

    .line 817645
    :cond_1
    iget-object v0, p0, LX/4sB;->q:[LX/4sA;

    .line 817646
    if-eqz v0, :cond_3

    array-length v1, v0

    if-ne v1, v3, :cond_3

    .line 817647
    iput-object v2, p0, LX/4sB;->q:[LX/4sA;

    .line 817648
    iget v1, p0, LX/4sB;->r:I

    if-lez v1, :cond_2

    .line 817649
    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 817650
    :cond_2
    iget-object v1, p0, LX/4sB;->j:LX/4s6;

    .line 817651
    iput-object v0, v1, LX/4s6;->a:[Ljava/lang/Object;

    .line 817652
    :cond_3
    iget-object v0, p0, LX/4sB;->s:[LX/4sA;

    .line 817653
    if-eqz v0, :cond_5

    array-length v1, v0

    if-ne v1, v3, :cond_5

    .line 817654
    iput-object v2, p0, LX/4sB;->s:[LX/4sA;

    .line 817655
    iget v1, p0, LX/4sB;->t:I

    if-lez v1, :cond_4

    .line 817656
    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 817657
    :cond_4
    iget-object v1, p0, LX/4sB;->j:LX/4s6;

    .line 817658
    iput-object v0, v1, LX/4s6;->b:[Ljava/lang/Object;

    .line 817659
    :cond_5
    return-void
.end method

.method public final l()V
    .locals 4

    .prologue
    .line 817628
    const/4 v0, 0x0

    .line 817629
    iget v1, p0, LX/4sB;->i:I

    sget-object v2, LX/4s9;->CHECK_SHARED_NAMES:LX/4s9;

    invoke-virtual {v2}, LX/4s9;->getMask()I

    move-result v2

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    .line 817630
    const/4 v0, 0x1

    .line 817631
    :cond_0
    iget v1, p0, LX/4sB;->i:I

    sget-object v2, LX/4s9;->CHECK_SHARED_STRING_VALUES:LX/4s9;

    invoke-virtual {v2}, LX/4s9;->getMask()I

    move-result v2

    and-int/2addr v1, v2

    if-eqz v1, :cond_1

    .line 817632
    or-int/lit8 v0, v0, 0x2

    .line 817633
    :cond_1
    iget v1, p0, LX/4sB;->i:I

    sget-object v2, LX/4s9;->ENCODE_BINARY_AS_7BIT:LX/4s9;

    invoke-virtual {v2}, LX/4s9;->getMask()I

    move-result v2

    and-int/2addr v1, v2

    if-nez v1, :cond_2

    .line 817634
    or-int/lit8 v0, v0, 0x4

    .line 817635
    :cond_2
    const/16 v1, 0x3a

    const/16 v2, 0x29

    const/16 v3, 0xa

    int-to-byte v0, v0

    invoke-direct {p0, v1, v2, v3, v0}, LX/4sB;->a(BBBB)V

    .line 817636
    return-void
.end method

.method public final version()LX/0ne;
    .locals 1

    .prologue
    .line 817627
    sget-object v0, LX/4s5;->VERSION:LX/0ne;

    return-object v0
.end method
