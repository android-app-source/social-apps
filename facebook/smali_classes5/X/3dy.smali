.class public final LX/3dy;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;

.field public static final k:LX/0U1;

.field public static final l:LX/0U1;

.field public static final m:LX/0U1;

.field public static final n:LX/0U1;

.field public static final o:LX/0U1;

.field public static final p:LX/0U1;

.field public static final q:LX/0U1;

.field public static final r:LX/0U1;

.field public static final s:LX/0U1;

.field public static final t:LX/0U1;

.field public static final u:LX/0U1;

.field public static final v:LX/0U1;

.field public static final w:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 618945
    new-instance v0, LX/0U1;

    const-string v1, "id"

    const-string v2, "TEXT PRIMARY KEY"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dy;->a:LX/0U1;

    .line 618946
    new-instance v0, LX/0U1;

    const-string v1, "name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dy;->b:LX/0U1;

    .line 618947
    new-instance v0, LX/0U1;

    const-string v1, "artist"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dy;->c:LX/0U1;

    .line 618948
    new-instance v0, LX/0U1;

    const-string v1, "description"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dy;->d:LX/0U1;

    .line 618949
    new-instance v0, LX/0U1;

    const-string v1, "thumbnail"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dy;->e:LX/0U1;

    .line 618950
    new-instance v0, LX/0U1;

    const-string v1, "thumbnail_disk_uri"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dy;->f:LX/0U1;

    .line 618951
    new-instance v0, LX/0U1;

    const-string v1, "preview_uri"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dy;->g:LX/0U1;

    .line 618952
    new-instance v0, LX/0U1;

    const-string v1, "tab_icon_uri"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dy;->h:LX/0U1;

    .line 618953
    new-instance v0, LX/0U1;

    const-string v1, "price"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dy;->i:LX/0U1;

    .line 618954
    new-instance v0, LX/0U1;

    const-string v1, "updated_time"

    const-string v2, "INT64"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dy;->j:LX/0U1;

    .line 618955
    new-instance v0, LX/0U1;

    const-string v1, "hasAssets"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dy;->k:LX/0U1;

    .line 618956
    new-instance v0, LX/0U1;

    const-string v1, "is_auto_downloadable"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dy;->l:LX/0U1;

    .line 618957
    new-instance v0, LX/0U1;

    const-string v1, "is_comments_capable"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dy;->m:LX/0U1;

    .line 618958
    new-instance v0, LX/0U1;

    const-string v1, "is_composer_capable"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dy;->n:LX/0U1;

    .line 618959
    new-instance v0, LX/0U1;

    const-string v1, "is_messenger_capable"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dy;->o:LX/0U1;

    .line 618960
    new-instance v0, LX/0U1;

    const-string v1, "is_featured"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dy;->p:LX/0U1;

    .line 618961
    new-instance v0, LX/0U1;

    const-string v1, "is_promoted"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dy;->q:LX/0U1;

    .line 618962
    new-instance v0, LX/0U1;

    const-string v1, "in_sticker_tray"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dy;->r:LX/0U1;

    .line 618963
    new-instance v0, LX/0U1;

    const-string v1, "copyrights"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dy;->s:LX/0U1;

    .line 618964
    new-instance v0, LX/0U1;

    const-string v1, "sticker_id_list"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dy;->t:LX/0U1;

    .line 618965
    new-instance v0, LX/0U1;

    const-string v1, "is_sms_capable"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dy;->u:LX/0U1;

    .line 618966
    new-instance v0, LX/0U1;

    const-string v1, "is_posts_capable"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dy;->v:LX/0U1;

    .line 618967
    new-instance v0, LX/0U1;

    const-string v1, "is_montage_capable"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dy;->w:LX/0U1;

    return-void
.end method
