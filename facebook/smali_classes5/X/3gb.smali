.class public final LX/3gb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ZE;


# instance fields
.field public final synthetic a:LX/3gZ;


# direct methods
.method public constructor <init>(LX/3gZ;)V
    .locals 0

    .prologue
    .line 624610
    iput-object p1, p0, LX/3gb;->a:LX/3gZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/2Vj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 624605
    iget-object v0, p0, LX/3gb;->a:LX/3gZ;

    iget-object v0, v0, LX/3gZ;->a:LX/3ga;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "fetchViewerAudienceInfo"

    .line 624606
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 624607
    move-object v0, v0

    .line 624608
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    .line 624609
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 624611
    const-string v0, "fetchViewerAudienceInfo"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$FetchAudienceInfoModel;

    .line 624612
    if-nez v0, :cond_0

    .line 624613
    :goto_0
    return-void

    .line 624614
    :cond_0
    iget-object v1, p0, LX/3gb;->a:LX/3gZ;

    iget-object v1, v1, LX/3gZ;->b:LX/339;

    invoke-virtual {v0}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$FetchAudienceInfoModel;->a()Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/339;->b(Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;)V

    goto :goto_0
.end method
