.class public LX/4lY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/4lY;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0wT;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 804536
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 804537
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/4lY;->a:Ljava/util/Map;

    .line 804538
    return-void
.end method

.method public static a(LX/0QB;)LX/4lY;
    .locals 3

    .prologue
    .line 804539
    sget-object v0, LX/4lY;->b:LX/4lY;

    if-nez v0, :cond_1

    .line 804540
    const-class v1, LX/4lY;

    monitor-enter v1

    .line 804541
    :try_start_0
    sget-object v0, LX/4lY;->b:LX/4lY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 804542
    if-eqz v2, :cond_0

    .line 804543
    :try_start_1
    new-instance v0, LX/4lY;

    invoke-direct {v0}, LX/4lY;-><init>()V

    .line 804544
    move-object v0, v0

    .line 804545
    sput-object v0, LX/4lY;->b:LX/4lY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 804546
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 804547
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 804548
    :cond_1
    sget-object v0, LX/4lY;->b:LX/4lY;

    return-object v0

    .line 804549
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 804550
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
