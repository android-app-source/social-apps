.class public LX/4LD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 682409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 682360
    const/4 v7, 0x0

    .line 682361
    const/4 v6, 0x0

    .line 682362
    const/4 v3, 0x0

    .line 682363
    const-wide/16 v4, 0x0

    .line 682364
    const/4 v2, 0x0

    .line 682365
    const/4 v1, 0x0

    .line 682366
    const/4 v0, 0x0

    .line 682367
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v9, :cond_9

    .line 682368
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 682369
    const/4 v0, 0x0

    .line 682370
    :goto_0
    return v0

    .line 682371
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v9, :cond_7

    .line 682372
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 682373
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 682374
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_0

    if-eqz v1, :cond_0

    .line 682375
    const-string v9, "bump_story_deduplication_key"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 682376
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v8, v1

    goto :goto_1

    .line 682377
    :cond_1
    const-string v9, "cache_id"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 682378
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v5, v1

    goto :goto_1

    .line 682379
    :cond_2
    const-string v9, "debug_info"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 682380
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v4, v1

    goto :goto_1

    .line 682381
    :cond_3
    const-string v9, "fetchTimeMs"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 682382
    const/4 v0, 0x1

    .line 682383
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto :goto_1

    .line 682384
    :cond_4
    const-string v9, "short_term_cache_key"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 682385
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v7, v1

    goto :goto_1

    .line 682386
    :cond_5
    const-string v9, "tracking"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 682387
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v6, v1

    goto :goto_1

    .line 682388
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 682389
    :cond_7
    const/4 v1, 0x6

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 682390
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 682391
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 682392
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 682393
    if-eqz v0, :cond_8

    .line 682394
    const/4 v1, 0x3

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 682395
    :cond_8
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 682396
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 682397
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_9
    move v8, v7

    move v7, v2

    move-wide v11, v4

    move v4, v3

    move v5, v6

    move-wide v2, v11

    move v6, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 682398
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 682399
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 682400
    invoke-static {p0, v2}, LX/4LD;->a(LX/15w;LX/186;)I

    move-result v1

    .line 682401
    if-eqz v0, :cond_0

    .line 682402
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 682403
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 682404
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 682405
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 682406
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 682407
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 682408
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 682329
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 682330
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682331
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 682332
    const-string v0, "name"

    const-string v1, "ClientBumpingPlaceHolderFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 682333
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 682334
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 682335
    if-eqz v0, :cond_0

    .line 682336
    const-string v1, "bump_story_deduplication_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682337
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 682338
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 682339
    if-eqz v0, :cond_1

    .line 682340
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682341
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 682342
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 682343
    if-eqz v0, :cond_2

    .line 682344
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682345
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 682346
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 682347
    cmp-long v2, v0, v2

    if-eqz v2, :cond_3

    .line 682348
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682349
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 682350
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 682351
    if-eqz v0, :cond_4

    .line 682352
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682353
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 682354
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 682355
    if-eqz v0, :cond_5

    .line 682356
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682357
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 682358
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 682359
    return-void
.end method
