.class public LX/4nw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/4nv;

.field public b:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/4nv;)V
    .locals 0

    .prologue
    .line 807996
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 807997
    iput-object p1, p0, LX/4nw;->a:LX/4nv;

    .line 807998
    return-void
.end method

.method public static a(LX/4nv;)LX/4nw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "LX/4nv;",
            ")",
            "LX/4nw",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 807999
    new-instance v0, LX/4nw;

    invoke-direct {v0, p0}, LX/4nw;-><init>(LX/4nv;)V

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 808000
    iget-object v0, p0, LX/4nw;->b:Landroid/view/View;

    if-nez v0, :cond_0

    .line 808001
    iget-object v0, p0, LX/4nw;->a:LX/4nv;

    invoke-virtual {v0}, LX/4nv;->a()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/4nw;->b:Landroid/view/View;

    .line 808002
    :cond_0
    iget-object v0, p0, LX/4nw;->b:Landroid/view/View;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 808003
    iget-object v0, p0, LX/4nw;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 808004
    invoke-virtual {p0}, LX/4nw;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/4nw;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 808005
    if-eqz v0, :cond_0

    .line 808006
    iget-object v0, p0, LX/4nw;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 808007
    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 808008
    invoke-virtual {p0}, LX/4nw;->a()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 808009
    return-void
.end method
