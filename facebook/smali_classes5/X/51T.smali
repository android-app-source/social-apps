.class public final LX/51T;
.super LX/306;
.source ""

# interfaces
.implements Ljava/util/Set;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/306",
        "<",
        "LX/50M",
        "<TC;>;>;",
        "Ljava/util/Set",
        "<",
        "LX/50M",
        "<TC;>;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "LX/50M",
            "<TC;>;>;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/51U;


# direct methods
.method public constructor <init>(LX/51U;Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LX/50M",
            "<TC;>;>;)V"
        }
    .end annotation

    .prologue
    .line 825106
    iput-object p1, p0, LX/51T;->b:LX/51U;

    invoke-direct {p0}, LX/306;-><init>()V

    .line 825107
    iput-object p2, p0, LX/51T;->a:Ljava/util/Collection;

    .line 825108
    return-void
.end method


# virtual methods
.method public final b()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "LX/50M",
            "<TC;>;>;"
        }
    .end annotation

    .prologue
    .line 825109
    iget-object v0, p0, LX/51T;->a:Ljava/util/Collection;

    return-object v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 825110
    invoke-virtual {p0}, LX/51T;->b()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 825111
    invoke-static {p0, p1}, LX/0RA;->a(Ljava/util/Set;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 825112
    invoke-static {p0}, LX/0RA;->a(Ljava/util/Set;)I

    move-result v0

    return v0
.end method
