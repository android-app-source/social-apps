.class public LX/4o3;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field private a:I

.field private b:I

.field private c:Landroid/graphics/drawable/ShapeDrawable;

.field private d:Landroid/graphics/drawable/ShapeDrawable;

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private f:I

.field private g:I

.field private h:Landroid/content/Context;

.field private i:I

.field private j:I


# direct methods
.method private b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 808510
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/4o3;->e:Ljava/util/ArrayList;

    .line 808511
    new-instance v0, Landroid/graphics/drawable/ShapeDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/ShapeDrawable;-><init>()V

    iput-object v0, p0, LX/4o3;->c:Landroid/graphics/drawable/ShapeDrawable;

    .line 808512
    new-instance v0, Landroid/graphics/drawable/ShapeDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/ShapeDrawable;-><init>()V

    iput-object v0, p0, LX/4o3;->d:Landroid/graphics/drawable/ShapeDrawable;

    .line 808513
    iget-object v0, p0, LX/4o3;->c:Landroid/graphics/drawable/ShapeDrawable;

    iget v1, p0, LX/4o3;->a:I

    iget v2, p0, LX/4o3;->a:I

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    .line 808514
    iget-object v0, p0, LX/4o3;->d:Landroid/graphics/drawable/ShapeDrawable;

    iget v1, p0, LX/4o3;->a:I

    iget v2, p0, LX/4o3;->a:I

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    .line 808515
    new-instance v0, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v0}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    .line 808516
    iget v1, p0, LX/4o3;->a:I

    int-to-float v1, v1

    iget v2, p0, LX/4o3;->a:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/shapes/Shape;->resize(FF)V

    .line 808517
    new-instance v1, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v1}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    .line 808518
    iget v2, p0, LX/4o3;->a:I

    int-to-float v2, v2

    iget v3, p0, LX/4o3;->a:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/shapes/Shape;->resize(FF)V

    .line 808519
    iget-object v2, p0, LX/4o3;->c:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v2

    iget v3, p0, LX/4o3;->i:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 808520
    iget-object v2, p0, LX/4o3;->d:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v2

    iget v3, p0, LX/4o3;->j:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 808521
    iget-object v2, p0, LX/4o3;->c:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/ShapeDrawable;->setShape(Landroid/graphics/drawable/shapes/Shape;)V

    .line 808522
    iget-object v0, p0, LX/4o3;->d:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/ShapeDrawable;->setShape(Landroid/graphics/drawable/shapes/Shape;)V

    .line 808523
    iget v0, p0, LX/4o3;->a:I

    int-to-float v0, v0

    invoke-virtual {p0}, LX/4o3;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, LX/4o3;->a:I

    .line 808524
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 808479
    iput v0, p0, LX/4o3;->f:I

    .line 808480
    iput v0, p0, LX/4o3;->g:I

    .line 808481
    iget-object v0, p0, LX/4o3;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 808482
    invoke-virtual {p0}, LX/4o3;->removeAllViewsInLayout()V

    .line 808483
    return-void
.end method

.method public getIndicatorDiameter()I
    .locals 1

    .prologue
    .line 808509
    iget v0, p0, LX/4o3;->a:I

    return v0
.end method

.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x7b72d3b9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 808507
    invoke-direct {p0}, LX/4o3;->b()V

    .line 808508
    const/16 v1, 0x2d

    const v2, -0x29a022ea

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setCurrentPage(I)V
    .locals 2

    .prologue
    .line 808502
    iget v0, p0, LX/4o3;->f:I

    if-ge p1, v0, :cond_0

    .line 808503
    iget-object v0, p0, LX/4o3;->e:Ljava/util/ArrayList;

    iget v1, p0, LX/4o3;->g:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, LX/4o3;->d:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 808504
    iget-object v0, p0, LX/4o3;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, LX/4o3;->c:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 808505
    iput p1, p0, LX/4o3;->g:I

    .line 808506
    :cond_0
    return-void
.end method

.method public setIndicatorDiameter(I)V
    .locals 5

    .prologue
    .line 808497
    iput p1, p0, LX/4o3;->a:I

    .line 808498
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget v0, p0, LX/4o3;->f:I

    if-ge v1, v0, :cond_0

    .line 808499
    iget-object v0, p0, LX/4o3;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    iget v3, p0, LX/4o3;->a:I

    iget v4, p0, LX/4o3;->a:I

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 808500
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 808501
    :cond_0
    return-void
.end method

.method public setPageCount(I)V
    .locals 7

    .prologue
    .line 808486
    iput p1, p0, LX/4o3;->f:I

    .line 808487
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    .line 808488
    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, LX/4o3;->h:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 808489
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    iget v3, p0, LX/4o3;->a:I

    iget v4, p0, LX/4o3;->a:I

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 808490
    iget v3, p0, LX/4o3;->a:I

    div-int/lit8 v3, v3, 0x2

    iget v4, p0, LX/4o3;->b:I

    mul-int/2addr v3, v4

    iget v4, p0, LX/4o3;->a:I

    iget v5, p0, LX/4o3;->a:I

    div-int/lit8 v5, v5, 0x2

    iget v6, p0, LX/4o3;->b:I

    mul-int/2addr v5, v6

    iget v6, p0, LX/4o3;->a:I

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 808491
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 808492
    iget-object v2, p0, LX/4o3;->d:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 808493
    iget-object v2, p0, LX/4o3;->e:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 808494
    invoke-virtual {p0, v1}, LX/4o3;->addView(Landroid/view/View;)V

    .line 808495
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 808496
    :cond_0
    return-void
.end method

.method public setSpacingCoefficient(I)V
    .locals 0

    .prologue
    .line 808484
    iput p1, p0, LX/4o3;->b:I

    .line 808485
    return-void
.end method
