.class public LX/4de;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/1bh;

.field public final b:LX/1Fg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Fg",
            "<",
            "LX/1bh;",
            "LX/1ln;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/4dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/imagepipeline/cache/CountingMemoryCache$EntryStateObserver",
            "<",
            "LX/1bh;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet",
            "<",
            "LX/1bh;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1bh;LX/1Fg;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bh;",
            "LX/1Fg",
            "<",
            "LX/1bh;",
            "LX/1ln;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 796284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 796285
    iput-object p1, p0, LX/4de;->a:LX/1bh;

    .line 796286
    iput-object p2, p0, LX/4de;->b:LX/1Fg;

    .line 796287
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, LX/4de;->d:Ljava/util/LinkedHashSet;

    .line 796288
    new-instance v0, LX/4dc;

    invoke-direct {v0, p0}, LX/4dc;-><init>(LX/4de;)V

    iput-object v0, p0, LX/4de;->c:LX/4dc;

    .line 796289
    return-void
.end method

.method public static declared-synchronized b(LX/4de;)LX/1bh;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 796290
    monitor-enter p0

    const/4 v0, 0x0

    .line 796291
    :try_start_0
    iget-object v1, p0, LX/4de;->d:Ljava/util/LinkedHashSet;

    invoke-virtual {v1}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 796292
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 796293
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1bh;

    .line 796294
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 796295
    :cond_0
    monitor-exit p0

    return-object v0

    .line 796296
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static b(LX/4de;I)LX/4dd;
    .locals 2

    .prologue
    .line 796297
    new-instance v0, LX/4dd;

    iget-object v1, p0, LX/4de;->a:LX/1bh;

    invoke-direct {v0, v1, p1}, LX/4dd;-><init>(LX/1bh;I)V

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/1bh;Z)V
    .locals 1

    .prologue
    .line 796298
    monitor-enter p0

    if-eqz p2, :cond_0

    .line 796299
    :try_start_0
    iget-object v0, p0, LX/4de;->d:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 796300
    :goto_0
    monitor-exit p0

    return-void

    .line 796301
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/4de;->d:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashSet;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 796302
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
