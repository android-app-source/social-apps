.class public LX/3Z5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TA;
.implements LX/1TG;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 598240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 598241
    return-void
.end method

.method public static a(LX/0QB;)LX/3Z5;
    .locals 3

    .prologue
    .line 598228
    const-class v1, LX/3Z5;

    monitor-enter v1

    .line 598229
    :try_start_0
    sget-object v0, LX/3Z5;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 598230
    sput-object v2, LX/3Z5;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 598231
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598232
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 598233
    new-instance v0, LX/3Z5;

    invoke-direct {v0}, LX/3Z5;-><init>()V

    .line 598234
    move-object v0, v0

    .line 598235
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 598236
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3Z5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 598237
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 598238
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ja;)V
    .locals 0

    .prologue
    .line 598239
    return-void
.end method

.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 598226
    sget-object v0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 598227
    return-void
.end method

.method public final a(LX/1T8;)V
    .locals 0

    .prologue
    .line 598225
    return-void
.end method
