.class public LX/3kV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0i1;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3kV;


# instance fields
.field private final a:LX/3kW;


# direct methods
.method public constructor <init>(LX/3kW;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 633014
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 633015
    iput-object p1, p0, LX/3kV;->a:LX/3kW;

    .line 633016
    return-void
.end method

.method public static a(LX/0QB;)LX/3kV;
    .locals 4

    .prologue
    .line 633017
    sget-object v0, LX/3kV;->b:LX/3kV;

    if-nez v0, :cond_1

    .line 633018
    const-class v1, LX/3kV;

    monitor-enter v1

    .line 633019
    :try_start_0
    sget-object v0, LX/3kV;->b:LX/3kV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 633020
    if-eqz v2, :cond_0

    .line 633021
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 633022
    new-instance p0, LX/3kV;

    invoke-static {v0}, LX/3kW;->a(LX/0QB;)LX/3kW;

    move-result-object v3

    check-cast v3, LX/3kW;

    invoke-direct {p0, v3}, LX/3kV;-><init>(LX/3kW;)V

    .line 633023
    move-object v0, p0

    .line 633024
    sput-object v0, LX/3kV;->b:LX/3kV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 633025
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 633026
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 633027
    :cond_1
    sget-object v0, LX/3kV;->b:LX/3kV;

    return-object v0

    .line 633028
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 633029
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 633030
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 633031
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    return-object v0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 633032
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0
    .param p1    # Landroid/os/Parcelable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 633033
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 633034
    const-string v0, "2551"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 633035
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PERMALINK_STORY_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
