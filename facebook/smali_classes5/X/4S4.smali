.class public LX/4S4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 711477
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 711478
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_7

    .line 711479
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 711480
    :goto_0
    return v0

    .line 711481
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 711482
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_6

    .line 711483
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 711484
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 711485
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 711486
    const-string v5, "creatives"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 711487
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 711488
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_2

    .line 711489
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_2

    .line 711490
    invoke-static {p0, p1}, LX/4S3;->b(LX/15w;LX/186;)I

    move-result v4

    .line 711491
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 711492
    :cond_2
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 711493
    goto :goto_1

    .line 711494
    :cond_3
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 711495
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 711496
    :cond_4
    const-string v5, "template"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 711497
    invoke-static {p0, p1}, LX/4S9;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 711498
    :cond_5
    const-string v5, "unit_title"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 711499
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 711500
    :cond_6
    const/4 v4, 0x5

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 711501
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 711502
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 711503
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 711504
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 711505
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_7
    move v1, v0

    move v2, v0

    move v3, v0

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 711506
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 711507
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 711508
    if-eqz v0, :cond_1

    .line 711509
    const-string v1, "creatives"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711510
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 711511
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 711512
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/4S3;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 711513
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 711514
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 711515
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 711516
    if-eqz v0, :cond_2

    .line 711517
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711518
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 711519
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 711520
    if-eqz v0, :cond_3

    .line 711521
    const-string v1, "template"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711522
    invoke-static {p0, v0, p2, p3}, LX/4S9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 711523
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 711524
    if-eqz v0, :cond_4

    .line 711525
    const-string v1, "unit_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711526
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 711527
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 711528
    return-void
.end method
