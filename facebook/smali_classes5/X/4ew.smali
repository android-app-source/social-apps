.class public LX/4ew;
.super LX/4ep;
.source ""


# instance fields
.field private final a:Landroid/content/res/AssetManager;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;LX/1Fj;Landroid/content/res/AssetManager;Z)V
    .locals 0

    .prologue
    .line 797729
    invoke-direct {p0, p1, p2, p4}, LX/4ep;-><init>(Ljava/util/concurrent/Executor;LX/1Fj;Z)V

    .line 797730
    iput-object p3, p0, LX/4ew;->a:Landroid/content/res/AssetManager;

    .line 797731
    return-void
.end method

.method private b(LX/1bf;)I
    .locals 5

    .prologue
    .line 797715
    const/4 v0, 0x0

    .line 797716
    :try_start_0
    iget-object v1, p0, LX/4ew;->a:Landroid/content/res/AssetManager;

    invoke-static {p1}, LX/4ew;->c(LX/1bf;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 797717
    :try_start_1
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->getLength()J
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v2

    long-to-int v0, v2

    .line 797718
    if-eqz v1, :cond_0

    .line 797719
    :try_start_2
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 797720
    :cond_0
    :goto_0
    return v0

    .line 797721
    :catch_0
    :goto_1
    if-eqz v0, :cond_1

    .line 797722
    :try_start_3
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 797723
    :cond_1
    :goto_2
    const/4 v0, -0x1

    goto :goto_0

    .line 797724
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    .line 797725
    :goto_3
    if-eqz v1, :cond_2

    .line 797726
    :try_start_4
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 797727
    :cond_2
    :goto_4
    throw v0

    :catch_1
    goto :goto_0

    :catch_2
    goto :goto_2

    :catch_3
    goto :goto_4

    .line 797728
    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_4
    move-object v0, v1

    goto :goto_1
.end method

.method private static c(LX/1bf;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 797732
    iget-object v0, p0, LX/1bf;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 797733
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1bf;)LX/1FL;
    .locals 3

    .prologue
    .line 797714
    iget-object v0, p0, LX/4ew;->a:Landroid/content/res/AssetManager;

    invoke-static {p1}, LX/4ew;->c(LX/1bf;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {p0, p1}, LX/4ew;->b(LX/1bf;)I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/4ep;->b(Ljava/io/InputStream;I)LX/1FL;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 797713
    const-string v0, "LocalAssetFetchProducer"

    return-object v0
.end method
