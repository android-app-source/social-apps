.class public LX/4My;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 689874
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 689875
    const/4 v9, 0x0

    .line 689876
    const/4 v8, 0x0

    .line 689877
    const/4 v7, 0x0

    .line 689878
    const/4 v6, 0x0

    .line 689879
    const/4 v5, 0x0

    .line 689880
    const/4 v4, 0x0

    .line 689881
    const/4 v3, 0x0

    .line 689882
    const/4 v2, 0x0

    .line 689883
    const/4 v1, 0x0

    .line 689884
    const/4 v0, 0x0

    .line 689885
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 689886
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 689887
    const/4 v0, 0x0

    .line 689888
    :goto_0
    return v0

    .line 689889
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 689890
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_9

    .line 689891
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 689892
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 689893
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 689894
    const-string v11, "birthdayPerson"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 689895
    invoke-static {p0, p1}, LX/2bO;->b(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 689896
    :cond_2
    const-string v11, "count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 689897
    const/4 v2, 0x1

    .line 689898
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v8

    goto :goto_1

    .line 689899
    :cond_3
    const-string v11, "edges"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 689900
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 689901
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_ARRAY:LX/15z;

    if-ne v10, v11, :cond_4

    .line 689902
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_ARRAY:LX/15z;

    if-eq v10, v11, :cond_4

    .line 689903
    invoke-static {p0, p1}, LX/4Mz;->b(LX/15w;LX/186;)I

    move-result v10

    .line 689904
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 689905
    :cond_4
    invoke-static {v7, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v7

    move v7, v7

    .line 689906
    goto :goto_1

    .line 689907
    :cond_5
    const-string v11, "friends_who_used_contact_importer_count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 689908
    const/4 v1, 0x1

    .line 689909
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v6

    goto :goto_1

    .line 689910
    :cond_6
    const-string v11, "friends_who_used_contact_importer_on_messenger_count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 689911
    const/4 v0, 0x1

    .line 689912
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    goto :goto_1

    .line 689913
    :cond_7
    const-string v11, "nodes"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 689914
    invoke-static {p0, p1}, LX/2bO;->b(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 689915
    :cond_8
    const-string v11, "page_info"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 689916
    invoke-static {p0, p1}, LX/264;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 689917
    :cond_9
    const/4 v10, 0x7

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 689918
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 689919
    if-eqz v2, :cond_a

    .line 689920
    const/4 v2, 0x1

    const/4 v9, 0x0

    invoke-virtual {p1, v2, v8, v9}, LX/186;->a(III)V

    .line 689921
    :cond_a
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 689922
    if-eqz v1, :cond_b

    .line 689923
    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v6, v2}, LX/186;->a(III)V

    .line 689924
    :cond_b
    if-eqz v0, :cond_c

    .line 689925
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v5, v1}, LX/186;->a(III)V

    .line 689926
    :cond_c
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 689927
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 689928
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 689929
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 689930
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 689931
    if-eqz v0, :cond_0

    .line 689932
    const-string v1, "birthdayPerson"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689933
    invoke-static {p0, v0, p2, p3}, LX/2bO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 689934
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 689935
    if-eqz v0, :cond_1

    .line 689936
    const-string v1, "count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689937
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 689938
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 689939
    if-eqz v0, :cond_3

    .line 689940
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689941
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 689942
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 689943
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2, p3}, LX/4Mz;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 689944
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 689945
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 689946
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 689947
    if-eqz v0, :cond_4

    .line 689948
    const-string v1, "friends_who_used_contact_importer_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689949
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 689950
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 689951
    if-eqz v0, :cond_5

    .line 689952
    const-string v1, "friends_who_used_contact_importer_on_messenger_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689953
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 689954
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 689955
    if-eqz v0, :cond_6

    .line 689956
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689957
    invoke-static {p0, v0, p2, p3}, LX/2bO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 689958
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 689959
    if-eqz v0, :cond_7

    .line 689960
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689961
    invoke-static {p0, v0, p2}, LX/264;->a(LX/15i;ILX/0nX;)V

    .line 689962
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 689963
    return-void
.end method
