.class public LX/3SO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ug;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/0dz;

.field private final c:LX/0W9;

.field private final d:LX/0W3;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0dz;LX/0W9;LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 582198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 582199
    iput-object p1, p0, LX/3SO;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 582200
    iput-object p2, p0, LX/3SO;->b:LX/0dz;

    .line 582201
    iput-object p3, p0, LX/3SO;->c:LX/0W9;

    .line 582202
    iput-object p4, p0, LX/3SO;->d:LX/0W3;

    .line 582203
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 582204
    const/16 v0, 0xf8

    return v0
.end method

.method public final a(I)V
    .locals 5

    .prologue
    .line 582205
    iget-object v0, p0, LX/3SO;->d:LX/0W3;

    invoke-virtual {v0}, LX/0W3;->a()LX/0W4;

    move-result-object v0

    sget-wide v2, LX/0X5;->hZ:J

    const-string v1, "{}"

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 582206
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 582207
    const-string v0, "locale"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 582208
    invoke-static {v0}, LX/0sI;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 582209
    const-string v3, "old_locale"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 582210
    invoke-static {v3}, LX/0sI;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 582211
    const-string v4, "bypass_old_locale"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 582212
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 582213
    iget-object v0, p0, LX/3SO;->b:LX/0dz;

    invoke-virtual {v0}, LX/0dz;->c()LX/0Py;

    move-result-object v0

    .line 582214
    invoke-static {v2}, LX/0e9;->a(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/0Py;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "en"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 582215
    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    iget-object v0, p0, LX/3SO;->c:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->b()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 582216
    :cond_0
    iget-object v0, p0, LX/3SO;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/288;->c:LX/0Tn;

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 582217
    :cond_1
    :goto_1
    return-void

    :catch_0
    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
