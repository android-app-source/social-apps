.class public final enum LX/3lx;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3lx;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3lx;

.field public static final enum AUTOPLAY:LX/3lx;

.field public static final enum DATA_CHANGED:LX/3lx;

.field public static final enum GARBAGE_COLLECTION:LX/3lx;

.field public static final enum GET_VIEW:LX/3lx;

.field public static final enum GET_VIEW_AFTER_DATA_CHANGED:LX/3lx;

.field public static final enum HSCROLL_RENDER:LX/3lx;

.field public static final enum ON_LAYOUT:LX/3lx;

.field public static final enum VIDEO_BIND:LX/3lx;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 635278
    new-instance v0, LX/3lx;

    const-string v1, "GARBAGE_COLLECTION"

    invoke-direct {v0, v1, v3}, LX/3lx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3lx;->GARBAGE_COLLECTION:LX/3lx;

    .line 635279
    new-instance v0, LX/3lx;

    const-string v1, "DATA_CHANGED"

    invoke-direct {v0, v1, v4}, LX/3lx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3lx;->DATA_CHANGED:LX/3lx;

    .line 635280
    new-instance v0, LX/3lx;

    const-string v1, "VIDEO_BIND"

    invoke-direct {v0, v1, v5}, LX/3lx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3lx;->VIDEO_BIND:LX/3lx;

    .line 635281
    new-instance v0, LX/3lx;

    const-string v1, "ON_LAYOUT"

    invoke-direct {v0, v1, v6}, LX/3lx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3lx;->ON_LAYOUT:LX/3lx;

    .line 635282
    new-instance v0, LX/3lx;

    const-string v1, "GET_VIEW_AFTER_DATA_CHANGED"

    invoke-direct {v0, v1, v7}, LX/3lx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3lx;->GET_VIEW_AFTER_DATA_CHANGED:LX/3lx;

    .line 635283
    new-instance v0, LX/3lx;

    const-string v1, "HSCROLL_RENDER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/3lx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3lx;->HSCROLL_RENDER:LX/3lx;

    .line 635284
    new-instance v0, LX/3lx;

    const-string v1, "GET_VIEW"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/3lx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3lx;->GET_VIEW:LX/3lx;

    .line 635285
    new-instance v0, LX/3lx;

    const-string v1, "AUTOPLAY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/3lx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3lx;->AUTOPLAY:LX/3lx;

    .line 635286
    const/16 v0, 0x8

    new-array v0, v0, [LX/3lx;

    sget-object v1, LX/3lx;->GARBAGE_COLLECTION:LX/3lx;

    aput-object v1, v0, v3

    sget-object v1, LX/3lx;->DATA_CHANGED:LX/3lx;

    aput-object v1, v0, v4

    sget-object v1, LX/3lx;->VIDEO_BIND:LX/3lx;

    aput-object v1, v0, v5

    sget-object v1, LX/3lx;->ON_LAYOUT:LX/3lx;

    aput-object v1, v0, v6

    sget-object v1, LX/3lx;->GET_VIEW_AFTER_DATA_CHANGED:LX/3lx;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/3lx;->HSCROLL_RENDER:LX/3lx;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/3lx;->GET_VIEW:LX/3lx;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/3lx;->AUTOPLAY:LX/3lx;

    aput-object v2, v0, v1

    sput-object v0, LX/3lx;->$VALUES:[LX/3lx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 635287
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3lx;
    .locals 1

    .prologue
    .line 635288
    const-class v0, LX/3lx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3lx;

    return-object v0
.end method

.method public static values()[LX/3lx;
    .locals 1

    .prologue
    .line 635289
    sget-object v0, LX/3lx;->$VALUES:[LX/3lx;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3lx;

    return-object v0
.end method
