.class public final LX/3vm;
.super LX/3uQ;
.source ""


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 653464
    invoke-direct {p0, p1}, LX/3uQ;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 653465
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3vm;->a:Z

    .line 653466
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 653467
    iget-boolean v0, p0, LX/3vm;->a:Z

    if-eqz v0, :cond_0

    .line 653468
    invoke-super {p0, p1}, LX/3uQ;->draw(Landroid/graphics/Canvas;)V

    .line 653469
    :cond_0
    return-void
.end method

.method public final setHotspot(FF)V
    .locals 1

    .prologue
    .line 653470
    iget-boolean v0, p0, LX/3vm;->a:Z

    if-eqz v0, :cond_0

    .line 653471
    invoke-super {p0, p1, p2}, LX/3uQ;->setHotspot(FF)V

    .line 653472
    :cond_0
    return-void
.end method

.method public final setHotspotBounds(IIII)V
    .locals 1

    .prologue
    .line 653473
    iget-boolean v0, p0, LX/3vm;->a:Z

    if-eqz v0, :cond_0

    .line 653474
    invoke-super {p0, p1, p2, p3, p4}, LX/3uQ;->setHotspotBounds(IIII)V

    .line 653475
    :cond_0
    return-void
.end method

.method public final setState([I)Z
    .locals 1

    .prologue
    .line 653476
    iget-boolean v0, p0, LX/3vm;->a:Z

    if-eqz v0, :cond_0

    .line 653477
    invoke-super {p0, p1}, LX/3uQ;->setState([I)Z

    move-result v0

    .line 653478
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setVisible(ZZ)Z
    .locals 1

    .prologue
    .line 653479
    iget-boolean v0, p0, LX/3vm;->a:Z

    if-eqz v0, :cond_0

    .line 653480
    invoke-super {p0, p1, p2}, LX/3uQ;->setVisible(ZZ)Z

    move-result v0

    .line 653481
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
