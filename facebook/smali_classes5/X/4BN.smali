.class public final LX/4BN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/4BW;


# direct methods
.method public constructor <init>(LX/4BW;)V
    .locals 0

    .prologue
    .line 677692
    iput-object p1, p0, LX/4BN;->a:LX/4BW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x2

    const v1, 0x7536c293

    invoke-static {v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 677693
    iget-object v0, p0, LX/4BN;->a:LX/4BW;

    iget-object v0, v0, LX/4BW;->m:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, LX/4BN;->a:LX/4BW;

    iget-object v0, v0, LX/4BW;->o:Landroid/os/Message;

    if-eqz v0, :cond_2

    .line 677694
    iget-object v0, p0, LX/4BN;->a:LX/4BW;

    iget-object v0, v0, LX/4BW;->o:Landroid/os/Message;

    invoke-static {v0}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v0

    .line 677695
    :goto_0
    if-eqz v0, :cond_0

    .line 677696
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 677697
    :cond_0
    iget-object v0, p0, LX/4BN;->a:LX/4BW;

    iget-boolean v0, v0, LX/4BW;->L:Z

    if-eqz v0, :cond_1

    .line 677698
    iget-object v0, p0, LX/4BN;->a:LX/4BW;

    iget-object v0, v0, LX/4BW;->M:Landroid/os/Handler;

    iget-object v2, p0, LX/4BN;->a:LX/4BW;

    iget-object v2, v2, LX/4BW;->b:Landroid/content/DialogInterface;

    invoke-virtual {v0, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 677699
    :cond_1
    const v0, -0x7e2f81b6

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 677700
    :cond_2
    iget-object v0, p0, LX/4BN;->a:LX/4BW;

    iget-object v0, v0, LX/4BW;->p:Landroid/widget/Button;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, LX/4BN;->a:LX/4BW;

    iget-object v0, v0, LX/4BW;->r:Landroid/os/Message;

    if-eqz v0, :cond_3

    .line 677701
    iget-object v0, p0, LX/4BN;->a:LX/4BW;

    iget-object v0, v0, LX/4BW;->r:Landroid/os/Message;

    invoke-static {v0}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v0

    goto :goto_0

    .line 677702
    :cond_3
    iget-object v0, p0, LX/4BN;->a:LX/4BW;

    iget-object v0, v0, LX/4BW;->s:Landroid/widget/Button;

    if-ne p1, v0, :cond_4

    iget-object v0, p0, LX/4BN;->a:LX/4BW;

    iget-object v0, v0, LX/4BW;->u:Landroid/os/Message;

    if-eqz v0, :cond_4

    .line 677703
    iget-object v0, p0, LX/4BN;->a:LX/4BW;

    iget-object v0, v0, LX/4BW;->u:Landroid/os/Message;

    invoke-static {v0}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v0

    goto :goto_0

    .line 677704
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method
