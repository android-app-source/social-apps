.class public final LX/4yz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public a:LX/4yx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4yx",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public b:LX/4yy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4yy",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public c:I

.field public final synthetic d:LX/4z0;


# direct methods
.method public constructor <init>(LX/4z0;)V
    .locals 1

    .prologue
    .line 822115
    iput-object p1, p0, LX/4yz;->d:LX/4z0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 822116
    iget-object v0, p0, LX/4yz;->d:LX/4z0;

    iget-object v0, v0, LX/4z0;->f:LX/4yx;

    iput-object v0, p0, LX/4yz;->a:LX/4yx;

    .line 822117
    iget-object v0, p0, LX/4yz;->d:LX/4z0;

    iget v0, v0, LX/4z0;->e:I

    iput v0, p0, LX/4yz;->c:I

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 822118
    iget-object v0, p0, LX/4yz;->d:LX/4z0;

    iget v0, v0, LX/4z0;->e:I

    iget v1, p0, LX/4yz;->c:I

    if-eq v0, v1, :cond_0

    .line 822119
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 822120
    :cond_0
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 2

    .prologue
    .line 822121
    invoke-direct {p0}, LX/4yz;->a()V

    .line 822122
    iget-object v0, p0, LX/4yz;->a:LX/4yx;

    iget-object v1, p0, LX/4yz;->d:LX/4z0;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 822123
    invoke-virtual {p0}, LX/4yz;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 822124
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 822125
    :cond_0
    iget-object v0, p0, LX/4yz;->a:LX/4yx;

    check-cast v0, LX/4yy;

    .line 822126
    invoke-virtual {v0}, LX/0P4;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 822127
    iput-object v0, p0, LX/4yz;->b:LX/4yy;

    .line 822128
    invoke-virtual {v0}, LX/4yy;->b()LX/4yx;

    move-result-object v0

    iput-object v0, p0, LX/4yz;->a:LX/4yx;

    .line 822129
    return-object v1
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 822130
    invoke-direct {p0}, LX/4yz;->a()V

    .line 822131
    iget-object v0, p0, LX/4yz;->b:LX/4yy;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0P6;->a(Z)V

    .line 822132
    iget-object v0, p0, LX/4yz;->d:LX/4z0;

    iget-object v1, p0, LX/4yz;->b:LX/4yy;

    invoke-virtual {v1}, LX/0P4;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4z0;->remove(Ljava/lang/Object;)Z

    .line 822133
    iget-object v0, p0, LX/4yz;->d:LX/4z0;

    iget v0, v0, LX/4z0;->e:I

    iput v0, p0, LX/4yz;->c:I

    .line 822134
    const/4 v0, 0x0

    iput-object v0, p0, LX/4yz;->b:LX/4yy;

    .line 822135
    return-void

    .line 822136
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
