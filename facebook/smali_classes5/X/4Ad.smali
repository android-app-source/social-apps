.class public LX/4Ad;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/ContentResolver;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 676723
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 676724
    iput-object p1, p0, LX/4Ad;->a:Landroid/content/ContentResolver;

    .line 676725
    iput-object p2, p0, LX/4Ad;->b:Ljava/util/List;

    .line 676726
    return-void
.end method

.method public static a(LX/4Ad;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 676727
    const-string v0, "SSO"

    const-string v1, "Using content provider URI for %s"

    invoke-static {v0, v1, p1}, LX/52l;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 676728
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "content://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".provider.UserValuesProvider/user_values"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 676729
    iget-object v0, p0, LX/4Ad;->a:Landroid/content/ContentResolver;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "name"

    aput-object v3, v2, v6

    const-string v3, "value"

    aput-object v3, v2, v5

    const-string v3, "name=\'active_session_info\'"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 676730
    if-eqz v1, :cond_1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 676731
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 676732
    const-string v0, "SSO"

    const-string v2, "%s session data obtained"

    invoke-static {v0, v2, p1}, LX/52l;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 676733
    :goto_0
    if-eqz v1, :cond_0

    .line 676734
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 676735
    :cond_0
    return-object v4

    .line 676736
    :cond_1
    :try_start_1
    const-string v0, "SSO"

    const-string v2, "%s content provider has no session entry."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v3, v5

    invoke-static {v0, v2, v3}, LX/52l;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 676737
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 676738
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/ApplicationInfo;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 676739
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/16 v2, 0x40

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 676740
    :cond_0
    :goto_0
    return-object v0

    .line 676741
    :catch_0
    move-exception v1

    .line 676742
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    instance-of v2, v2, Landroid/os/DeadObjectException;

    if-nez v2, :cond_0

    .line 676743
    throw v1

    .line 676744
    :catch_1
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)LX/3dh;
    .locals 10

    .prologue
    .line 676745
    iget-object v0, p0, LX/4Ad;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 676746
    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 676747
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 676748
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-static {p1, v7}, LX/4Ad;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/ApplicationInfo;

    move-result-object v7

    .line 676749
    invoke-static {p1, v0}, LX/4Ad;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/ApplicationInfo;

    move-result-object v8

    .line 676750
    if-nez v7, :cond_8

    .line 676751
    const-string v4, "SSO"

    const-string v7, "No appinfo found for the current application."

    .line 676752
    sget v8, LX/52l;->b:I

    const/4 v9, 0x6

    if-gt v8, v9, :cond_1

    .line 676753
    sget-object v8, LX/52l;->a:LX/52m;

    if-nez v8, :cond_1

    .line 676754
    invoke-static {v4, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 676755
    :cond_1
    :goto_0
    move v3, v3

    .line 676756
    if-nez v3, :cond_3

    .line 676757
    const-string v3, "SSO"

    const-string v4, "%s has no matching signatures"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-static {v3, v4, v5}, LX/52l;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 676758
    :goto_1
    move-object v0, v2

    .line 676759
    if-eqz v0, :cond_0

    .line 676760
    :goto_2
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 676761
    :cond_3
    :try_start_0
    invoke-static {p0, v0}, LX/4Ad;->a(LX/4Ad;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 676762
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 676763
    const-string v3, "SSO"

    const-string v4, "sso session information from %s is empty!"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v3, v4, v5}, LX/52l;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 676764
    :catch_0
    move-exception v3

    .line 676765
    const-string v4, "SSO"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Exception occurred while resolving sso session from "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 676766
    sget v6, LX/52l;->b:I

    const/4 v7, 0x6

    if-gt v6, v7, :cond_4

    .line 676767
    sget-object v6, LX/52l;->a:LX/52m;

    if-nez v6, :cond_4

    .line 676768
    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 676769
    :cond_4
    goto :goto_1

    .line 676770
    :cond_5
    :try_start_1
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 676771
    const-string v3, "profile"

    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 676772
    const-string v5, "uid"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    const-string v5, "name"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    const-string v5, "access_token"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 676773
    :cond_6
    const-string v3, "SSO"

    const-string v4, "%s session information is malformed"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v3, v4, v5}, LX/52l;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 676774
    :cond_7
    const-string v5, "uid"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 676775
    const-string v6, "name"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 676776
    const-string v3, "access_token"

    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v7

    .line 676777
    :try_start_2
    const-string v3, "username"

    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v3

    move-object v4, v3

    .line 676778
    :goto_3
    :try_start_3
    new-instance v3, LX/3dh;

    invoke-direct {v3, v5, v6, v7, v4}, LX/3dh;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    move-object v2, v3

    goto/16 :goto_1

    .line 676779
    :catch_1
    move-object v4, v5

    goto :goto_3

    .line 676780
    :cond_8
    if-nez v8, :cond_9

    .line 676781
    const-string v7, "SSO"

    const-string v8, "No appinfo found for %s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v3

    invoke-static {v7, v8, v4}, LX/52l;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 676782
    :cond_9
    iget v7, v7, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 676783
    iget v8, v8, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 676784
    if-eq v7, v8, :cond_a

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    invoke-virtual {v9, v7, v8}, Landroid/content/pm/PackageManager;->checkSignatures(II)I

    move-result v7

    if-nez v7, :cond_1

    :cond_a
    move v3, v4

    goto/16 :goto_0
.end method
