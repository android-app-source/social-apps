.class public final LX/4YL;
.super LX/0ur;
.source ""


# instance fields
.field public b:Z

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLIcon;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Z

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 776415
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 776416
    instance-of v0, p0, LX/4YL;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 776417
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 2

    .prologue
    .line 776418
    new-instance v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;-><init>(LX/4YL;)V

    .line 776419
    return-object v0
.end method
