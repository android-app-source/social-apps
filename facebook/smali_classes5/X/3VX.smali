.class public LX/3VX;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/35q;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field public b:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final d:Lcom/facebook/resources/ui/FbButton;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587943
    new-instance v0, LX/3VY;

    invoke-direct {v0}, LX/3VY;-><init>()V

    sput-object v0, LX/3VX;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 587944
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 587945
    const v0, 0x7f030523

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 587946
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/3VX;->setOrientation(I)V

    .line 587947
    const-class v0, LX/3VX;

    invoke-static {v0, p0}, LX/3VX;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 587948
    const v0, 0x7f0d0e94

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/3VX;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 587949
    const v0, 0x7f0d0e98

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/3VX;->d:Lcom/facebook/resources/ui/FbButton;

    .line 587950
    invoke-virtual {p0}, LX/3VX;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 587951
    new-instance v1, LX/1Uo;

    invoke-direct {v1, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    const v2, 0x7f0a00e3

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 587952
    iput-object v0, v1, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 587953
    move-object v0, v1

    .line 587954
    sget-object v1, LX/1Up;->h:LX/1Up;

    invoke-virtual {v0, v1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 587955
    iget-object v1, p0, LX/3VX;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 587956
    const-string v1, "newsfeed_angora_attachment_view"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {p0, v1, v2}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/Class;)V

    .line 587957
    sget-object v0, LX/1vY;->ATTACHMENT:LX/1vY;

    invoke-static {p0, v0}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 587958
    const v0, 0x7f0d0e95

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    sget-object v1, LX/1vY;->TITLE:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 587959
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/3VX;

    invoke-static {p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object p0

    check-cast p0, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    iput-object p0, p1, LX/3VX;->b:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    return-void
.end method


# virtual methods
.method public setLargeImageAspectRatio(F)V
    .locals 1

    .prologue
    .line 587960
    iget-object v0, p0, LX/3VX;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 587961
    return-void
.end method

.method public setLargeImageController(LX/1aZ;)V
    .locals 2
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587962
    iget-object v1, p0, LX/3VX;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 587963
    iget-object v0, p0, LX/3VX;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 587964
    return-void

    .line 587965
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
