.class public final LX/4Vc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/reactivesocket/GatewayCallback;


# instance fields
.field public final synthetic a:LX/0zO;

.field public final synthetic b:LX/0TF;

.field public final synthetic c:LX/1Mz;


# direct methods
.method public constructor <init>(LX/1Mz;LX/0zO;LX/0TF;)V
    .locals 0

    .prologue
    .line 742647
    iput-object p1, p0, LX/4Vc;->c:LX/1Mz;

    iput-object p2, p0, LX/4Vc;->a:LX/0zO;

    iput-object p3, p0, LX/4Vc;->b:LX/0TF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 742648
    iget-object v0, p0, LX/4Vc;->b:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 742649
    return-void
.end method

.method public final onSuccess(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 742650
    iget-object v0, p0, LX/4Vc;->c:LX/1Mz;

    iget-object v1, p0, LX/4Vc;->a:LX/0zO;

    .line 742651
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 742652
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 742653
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 742654
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 742655
    iget-object v2, v0, LX/1Mz;->e:LX/0TD;

    new-instance v3, LX/4Vb;

    invoke-direct {v3, v0, p1, v1, p2}, LX/4Vb;-><init>(LX/1Mz;Ljava/lang/String;LX/0zO;Ljava/lang/String;)V

    invoke-interface {v2, v3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v2, v2

    .line 742656
    new-instance v3, LX/4Va;

    invoke-direct {v3, v0, v1}, LX/4Va;-><init>(LX/1Mz;LX/0zO;)V

    invoke-static {v2, v3}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 742657
    iget-object v1, p0, LX/4Vc;->b:LX/0TF;

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 742658
    return-void
.end method
