.class public LX/3e3;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/3e3;


# direct methods
.method public constructor <init>(LX/3e4;)V
    .locals 10
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 619017
    const-string v8, "stickers"

    const/16 v9, 0x25

    new-instance v0, LX/3e6;

    invoke-direct {v0}, LX/3e6;-><init>()V

    new-instance v1, LX/3e7;

    invoke-direct {v1}, LX/3e7;-><init>()V

    new-instance v2, LX/3e9;

    invoke-direct {v2}, LX/3e9;-><init>()V

    new-instance v3, LX/3eA;

    invoke-direct {v3}, LX/3eA;-><init>()V

    new-instance v4, LX/3eC;

    invoke-direct {v4}, LX/3eC;-><init>()V

    new-instance v5, LX/3eD;

    invoke-direct {v5}, LX/3eD;-><init>()V

    new-instance v6, LX/3eF;

    invoke-direct {v6}, LX/3eF;-><init>()V

    new-instance v7, LX/3eH;

    invoke-direct {v7, p1}, LX/3eH;-><init>(LX/3e4;)V

    invoke-static/range {v0 .. v7}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-direct {p0, v8, v9, v0}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 619018
    return-void
.end method

.method public static a(LX/0QB;)LX/3e3;
    .locals 4

    .prologue
    .line 619019
    sget-object v0, LX/3e3;->a:LX/3e3;

    if-nez v0, :cond_1

    .line 619020
    const-class v1, LX/3e3;

    monitor-enter v1

    .line 619021
    :try_start_0
    sget-object v0, LX/3e3;->a:LX/3e3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 619022
    if-eqz v2, :cond_0

    .line 619023
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 619024
    new-instance p0, LX/3e3;

    invoke-static {v0}, LX/3e4;->a(LX/0QB;)LX/3e4;

    move-result-object v3

    check-cast v3, LX/3e4;

    invoke-direct {p0, v3}, LX/3e3;-><init>(LX/3e4;)V

    .line 619025
    move-object v0, p0

    .line 619026
    sput-object v0, LX/3e3;->a:LX/3e3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 619027
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 619028
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 619029
    :cond_1
    sget-object v0, LX/3e3;->a:LX/3e3;

    return-object v0

    .line 619030
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 619031
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b(Landroid/database/sqlite/SQLiteDatabase;II)I
    .locals 2

    .prologue
    .line 619032
    add-int/lit8 v0, p2, 0x1

    .line 619033
    const/16 v1, 0x24

    if-ne p2, v1, :cond_0

    .line 619034
    const-string v1, "sticker_packs"

    sget-object p0, LX/3dy;->w:LX/0U1;

    sget-object p2, LX/03R;->UNSET:LX/03R;

    invoke-virtual {p2}, LX/03R;->getDbValue()I

    move-result p2

    invoke-static {v1, p0, p2}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;I)Ljava/lang/String;

    move-result-object v1

    const p0, -0x2553bebc

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x786b640a

    invoke-static {v1}, LX/03h;->a(I)V

    .line 619035
    const-string v1, "stickers"

    sget-object p0, LX/3dz;->k:LX/0U1;

    sget-object p2, LX/03R;->UNSET:LX/03R;

    invoke-virtual {p2}, LX/03R;->getDbValue()I

    move-result p2

    invoke-static {v1, p0, p2}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;I)Ljava/lang/String;

    move-result-object v1

    const p0, -0x7a170c0e

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x4dccdff0    # 4.29653504E8f

    invoke-static {v1}, LX/03h;->a(I)V

    .line 619036
    move p3, v0

    .line 619037
    :goto_0
    return p3

    .line 619038
    :cond_0
    const/16 v1, 0x23

    if-ne p2, v1, :cond_1

    .line 619039
    invoke-static {p1}, LX/3e3;->m(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v0

    goto :goto_0

    .line 619040
    :cond_1
    const/16 v1, 0x22

    if-ne p2, v1, :cond_2

    .line 619041
    invoke-static {p1}, LX/3e3;->l(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v0

    goto :goto_0

    .line 619042
    :cond_2
    const/16 v1, 0x21

    if-ne p2, v1, :cond_3

    .line 619043
    const-string v1, "stickers"

    sget-object p0, LX/3dz;->f:LX/0U1;

    sget-object p2, LX/03R;->UNSET:LX/03R;

    invoke-virtual {p2}, LX/03R;->getDbValue()I

    move-result p2

    invoke-static {v1, p0, p2}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;I)Ljava/lang/String;

    move-result-object v1

    const p0, 0x35d807a

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x4490fc00

    invoke-static {v1}, LX/03h;->a(I)V

    .line 619044
    const-string v1, "stickers"

    sget-object p0, LX/3dz;->g:LX/0U1;

    sget-object p2, LX/03R;->UNSET:LX/03R;

    invoke-virtual {p2}, LX/03R;->getDbValue()I

    move-result p2

    invoke-static {v1, p0, p2}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;I)Ljava/lang/String;

    move-result-object v1

    const p0, -0x128214fc

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x5a525b12

    invoke-static {v1}, LX/03h;->a(I)V

    .line 619045
    const-string v1, "stickers"

    sget-object p0, LX/3dz;->h:LX/0U1;

    sget-object p2, LX/03R;->UNSET:LX/03R;

    invoke-virtual {p2}, LX/03R;->getDbValue()I

    move-result p2

    invoke-static {v1, p0, p2}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;I)Ljava/lang/String;

    move-result-object v1

    const p0, -0x3bc390a9

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x4c2bb521

    invoke-static {v1}, LX/03h;->a(I)V

    .line 619046
    const-string v1, "stickers"

    sget-object p0, LX/3dz;->i:LX/0U1;

    sget-object p2, LX/03R;->UNSET:LX/03R;

    invoke-virtual {p2}, LX/03R;->getDbValue()I

    move-result p2

    invoke-static {v1, p0, p2}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;I)Ljava/lang/String;

    move-result-object v1

    const p0, 0x488d6ce5

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x2bb63b33

    invoke-static {v1}, LX/03h;->a(I)V

    .line 619047
    const-string v1, "stickers"

    sget-object p0, LX/3dz;->j:LX/0U1;

    sget-object p2, LX/03R;->UNSET:LX/03R;

    invoke-virtual {p2}, LX/03R;->getDbValue()I

    move-result p2

    invoke-static {v1, p0, p2}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;I)Ljava/lang/String;

    move-result-object v1

    const p0, -0x445b1a70

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x5524990b

    invoke-static {v1}, LX/03h;->a(I)V

    .line 619048
    move p3, v0

    goto/16 :goto_0

    .line 619049
    :cond_3
    const/16 v1, 0x20

    if-ne p2, v1, :cond_4

    .line 619050
    const/4 p2, 0x0

    .line 619051
    const-string v1, "sticker_packs"

    sget-object p0, LX/3dy;->v:LX/0U1;

    invoke-static {v1, p0}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object v1

    const p0, -0x2131f69c

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x48ceec9e

    invoke-static {v1}, LX/03h;->a(I)V

    .line 619052
    const-string v1, "sticker_packs"

    invoke-virtual {p1, v1, p2, p2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 619053
    const-string v1, "pack_types"

    invoke-virtual {p1, v1, p2, p2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 619054
    move p3, v0

    goto/16 :goto_0

    .line 619055
    :cond_4
    const/16 v1, 0x1f

    if-ne p2, v1, :cond_5

    .line 619056
    const/4 p2, 0x0

    .line 619057
    const-string v1, "sticker_packs"

    sget-object p0, LX/3dy;->u:LX/0U1;

    invoke-static {v1, p0}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object v1

    const p0, -0x1c5b8df7

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x71d31c20

    invoke-static {v1}, LX/03h;->a(I)V

    .line 619058
    const-string v1, "sticker_packs"

    invoke-virtual {p1, v1, p2, p2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 619059
    const-string v1, "pack_types"

    invoke-virtual {p1, v1, p2, p2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 619060
    move p3, v0

    goto/16 :goto_0

    .line 619061
    :cond_5
    const/16 v1, 0x1e

    if-ne p2, v1, :cond_6

    .line 619062
    const-string v1, "sticker_packs"

    sget-object p0, LX/3dy;->f:LX/0U1;

    invoke-static {v1, p0}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object v1

    const p0, -0x5de494b7

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x509e3190

    invoke-static {v1}, LX/03h;->a(I)V

    .line 619063
    move p3, v0

    goto/16 :goto_0

    .line 619064
    :cond_6
    const/16 v1, 0x1d

    if-ne p2, v1, :cond_7

    .line 619065
    const-string v1, "closed_download_preview_sticker_packs"

    sget-object p0, LX/3eF;->a:LX/0Px;

    sget-object p2, LX/3eF;->b:LX/0sv;

    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p2

    invoke-static {v1, p0, p2}, LX/0Tz;->a(Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/String;

    move-result-object v1

    const p0, -0x5955e92b

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x30afff21

    invoke-static {v1}, LX/03h;->a(I)V

    .line 619066
    move p3, v0

    goto/16 :goto_0

    .line 619067
    :cond_7
    const/16 v1, 0x1c

    if-ne p2, v1, :cond_8

    .line 619068
    invoke-static {p1}, LX/3e3;->f(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v0

    goto/16 :goto_0

    .line 619069
    :cond_8
    const/16 v1, 0x1b

    if-ne p2, v1, :cond_9

    .line 619070
    invoke-static {p1}, LX/3e3;->e(Landroid/database/sqlite/SQLiteDatabase;)V

    move p3, v0

    goto/16 :goto_0

    .line 619071
    :cond_9
    const/16 v1, 0x18

    if-ne p2, v1, :cond_a

    .line 619072
    const-string v1, "sticker_tags"

    sget-object p0, LX/3eD;->a:LX/0Px;

    sget-object p2, LX/3eD;->b:LX/0sv;

    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p2

    invoke-static {v1, p0, p2}, LX/0Tz;->a(Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/String;

    move-result-object v1

    const p0, -0x7d8ff20

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x5df91296

    invoke-static {v1}, LX/03h;->a(I)V

    .line 619073
    move p3, v0

    goto/16 :goto_0

    .line 619074
    :cond_a
    invoke-static {p1}, LX/3e3;->o(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 619075
    invoke-virtual {p0, p1}, LX/0Tv;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    goto/16 :goto_0
.end method

.method private static e(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 619076
    const v0, -0x322b1796    # -4.4650016E8f

    invoke-static {p0, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 619077
    :try_start_0
    sget-object v0, LX/3e8;->b:LX/0U1;

    .line 619078
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 619079
    const-string v1, "227877430692340"

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 619080
    const-string v1, "pack_types"

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 619081
    sget-object v0, LX/3dy;->a:LX/0U1;

    .line 619082
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 619083
    const-string v1, "227877430692340"

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 619084
    const-string v1, "sticker_packs"

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 619085
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 619086
    const/4 v0, 0x1

    .line 619087
    const v1, -0x535e542e

    invoke-static {p0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 619088
    :goto_0
    if-nez v0, :cond_0

    .line 619089
    invoke-static {p0}, LX/3e3;->o(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 619090
    :cond_0
    return-void

    .line 619091
    :catch_0
    const/4 v0, 0x0

    .line 619092
    const v1, 0x5f61c6af

    invoke-static {p0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x4857b49a

    invoke-static {p0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method private static f(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 13

    .prologue
    const/4 v1, 0x1

    const/4 v10, 0x0

    const/4 v4, 0x0

    .line 619093
    new-array v3, v1, [Ljava/lang/String;

    sget-object v0, LX/3e8;->a:LX/0U1;

    .line 619094
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 619095
    aput-object v0, v3, v10

    .line 619096
    const-string v2, "pack_types"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LX/3e8;->a:LX/0U1;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " DESC"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object v0, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    move-object v9, v4

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 619097
    sget-object v1, LX/3e8;->a:LX/0U1;

    invoke-virtual {v1, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v1

    .line 619098
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 619099
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 619100
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 619101
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 619102
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 619103
    sget-object v1, LX/3e8;->a:LX/0U1;

    .line 619104
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 619105
    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 619106
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v12

    .line 619107
    const-string v3, "pack_types"

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/3e8;->c:LX/0U1;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " DESC"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object v2, p0

    move-object v7, v4

    move-object v8, v4

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 619108
    sget-object v2, LX/3e8;->b:LX/0U1;

    invoke-virtual {v2, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 619109
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 619110
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 619111
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 619112
    const v1, 0x6cf14716

    invoke-static {p0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    move v2, v10

    .line 619113
    :goto_3
    :try_start_0
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_2

    .line 619114
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v3

    .line 619115
    sget-object v1, LX/3e8;->a:LX/0U1;

    .line 619116
    iget-object v5, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v5

    .line 619117
    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 619118
    sget-object v1, LX/3e8;->b:LX/0U1;

    .line 619119
    iget-object v5, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v5

    .line 619120
    invoke-interface {v12, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v5, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 619121
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 619122
    sget-object v5, LX/3e8;->c:LX/0U1;

    .line 619123
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 619124
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 619125
    const-string v5, "pack_types"

    invoke-virtual {v3}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v5, v1, v6, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 619126
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 619127
    :cond_2
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 619128
    const v0, -0x222388ed

    invoke-static {p0, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    const v1, 0x1b605f17

    invoke-static {p0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 619129
    :cond_3
    const-string v0, "Database"

    const-string v1, "Upgraded"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 619130
    return-void
.end method

.method private static l(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 17

    .prologue
    .line 619131
    const-string v14, "sticker_packs"

    sget-object v1, LX/3dy;->a:LX/0U1;

    sget-object v2, LX/3dy;->b:LX/0U1;

    sget-object v3, LX/3dy;->c:LX/0U1;

    sget-object v4, LX/3dy;->d:LX/0U1;

    sget-object v5, LX/3dy;->e:LX/0U1;

    sget-object v6, LX/3dy;->f:LX/0U1;

    sget-object v7, LX/3dy;->g:LX/0U1;

    sget-object v8, LX/3dy;->h:LX/0U1;

    sget-object v9, LX/3dy;->i:LX/0U1;

    sget-object v10, LX/3dy;->k:LX/0U1;

    sget-object v11, LX/3dy;->l:LX/0U1;

    sget-object v12, LX/3dy;->m:LX/0U1;

    const/16 v13, 0xa

    new-array v13, v13, [LX/0U1;

    const/4 v15, 0x0

    sget-object v16, LX/3dy;->n:LX/0U1;

    aput-object v16, v13, v15

    const/4 v15, 0x1

    sget-object v16, LX/3dy;->o:LX/0U1;

    aput-object v16, v13, v15

    const/4 v15, 0x2

    sget-object v16, LX/3dy;->p:LX/0U1;

    aput-object v16, v13, v15

    const/4 v15, 0x3

    sget-object v16, LX/3dy;->q:LX/0U1;

    aput-object v16, v13, v15

    const/4 v15, 0x4

    sget-object v16, LX/3dy;->r:LX/0U1;

    aput-object v16, v13, v15

    const/4 v15, 0x5

    sget-object v16, LX/3dy;->s:LX/0U1;

    aput-object v16, v13, v15

    const/4 v15, 0x6

    sget-object v16, LX/3dy;->t:LX/0U1;

    aput-object v16, v13, v15

    const/4 v15, 0x7

    sget-object v16, LX/3dy;->j:LX/0U1;

    aput-object v16, v13, v15

    const/16 v15, 0x8

    sget-object v16, LX/3dy;->u:LX/0U1;

    aput-object v16, v13, v15

    const/16 v15, 0x9

    sget-object v16, LX/3dy;->v:LX/0U1;

    aput-object v16, v13, v15

    invoke-static/range {v1 .. v13}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v14, v1, v2}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0Px;LX/0Px;)V

    .line 619132
    return-void
.end method

.method private static m(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6

    .prologue
    .line 619133
    const v0, 0x24663c2f

    invoke-static {p0, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 619134
    :try_start_0
    sget-object v0, LX/3dy;->m:LX/0U1;

    sget-object v1, LX/3dy;->n:LX/0U1;

    sget-object v2, LX/3dy;->o:LX/0U1;

    sget-object v3, LX/3dy;->u:LX/0U1;

    sget-object v4, LX/3dy;->v:LX/0U1;

    invoke-static {v0, v1, v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0U1;

    .line 619135
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "UPDATE sticker_packs SET "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 619136
    iget-object v5, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v5

    .line 619137
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = 2 WHERE "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 619138
    iget-object v5, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v5

    .line 619139
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " = 0;"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 619140
    const v4, -0x76bff0bf

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x321c5e02    # 9.10177E-9f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 619141
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 619142
    :cond_0
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 619143
    const v0, 0x7ab46e23

    invoke-static {p0, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 619144
    return-void

    .line 619145
    :catchall_0
    move-exception v0

    const v1, 0x53b5700b

    invoke-static {p0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method private static o(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 619146
    const-string v0, "stickers_db_properties"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x3f3ab45f

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x228b0705

    invoke-static {v0}, LX/03h;->a(I)V

    .line 619147
    const-string v0, "pack_types"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x4cea3d5b

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x44671b19

    invoke-static {v0}, LX/03h;->a(I)V

    .line 619148
    const-string v0, "sticker_packs"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x69d1c41b

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x69bc482d

    invoke-static {v0}, LX/03h;->a(I)V

    .line 619149
    const-string v0, "recent_stickers"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x710339b

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x7cbb321d

    invoke-static {v0}, LX/03h;->a(I)V

    .line 619150
    const-string v0, "stickers"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x7e17b8df

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x2905b6eb

    invoke-static {v0}, LX/03h;->a(I)V

    .line 619151
    const-string v0, "sticker_tags"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x545bbab4

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x45f516cf

    invoke-static {v0}, LX/03h;->a(I)V

    .line 619152
    const-string v0, "sticker_asserts"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x54d30aac

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x700914f1

    invoke-static {v0}, LX/03h;->a(I)V

    .line 619153
    const-string v0, "closed_download_preview_sticker_packs"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x3a2364dc

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x50d4c3a5

    invoke-static {v0}, LX/03h;->a(I)V

    .line 619154
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2

    .prologue
    .line 619155
    :goto_0
    if-ge p2, p3, :cond_0

    .line 619156
    invoke-direct {p0, p1, p2, p3}, LX/3e3;->b(Landroid/database/sqlite/SQLiteDatabase;II)I

    move-result p2

    goto :goto_0

    .line 619157
    :cond_0
    const-string v0, "pack_lists"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x4853a40

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x72c5f123

    invoke-static {v0}, LX/03h;->a(I)V

    .line 619158
    return-void
.end method
