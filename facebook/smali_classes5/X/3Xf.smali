.class public LX/3Xf;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field public b:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 593667
    new-instance v0, LX/3Xg;

    invoke-direct {v0}, LX/3Xg;-><init>()V

    sput-object v0, LX/3Xf;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 593649
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 593650
    const-class v0, LX/3Xf;

    invoke-static {v0, p0}, LX/3Xf;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 593651
    const v0, 0x7f0307d5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 593652
    const v0, 0x7f0d14b7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3Xf;->f:Landroid/widget/TextView;

    .line 593653
    const v0, 0x7f0d14b8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3Xf;->g:Landroid/widget/TextView;

    .line 593654
    const v0, 0x7f0d14b9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3Xf;->h:Landroid/widget/TextView;

    .line 593655
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b004e

    invoke-static {v0, v1}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v0

    .line 593656
    iput v0, p0, LX/3Xf;->d:I

    .line 593657
    iput v0, p0, LX/3Xf;->e:I

    .line 593658
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0052

    invoke-static {v0, v1}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v0

    iput v0, p0, LX/3Xf;->c:I

    .line 593659
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/3Xf;->setOrientation(I)V

    .line 593660
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/3Xf;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object p0

    check-cast p0, LX/0wM;

    iput-object p0, p1, LX/3Xf;->b:LX/0wM;

    return-void
.end method


# virtual methods
.method public setPrivacyIcon(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 593668
    iget-object v0, p0, LX/3Xf;->h:Landroid/widget/TextView;

    iget-object v1, p0, LX/3Xf;->b:LX/0wM;

    const v2, -0x6e685d

    invoke-virtual {v1, p1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 593669
    return-void
.end method

.method public setPrivacyNotice(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 593664
    iget-object v0, p0, LX/3Xf;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 593665
    iget-object v0, p0, LX/3Xf;->h:Landroid/widget/TextView;

    iget v1, p0, LX/3Xf;->e:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 593666
    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 593661
    iget-object v0, p0, LX/3Xf;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 593662
    iget-object v0, p0, LX/3Xf;->g:Landroid/widget/TextView;

    iget v1, p0, LX/3Xf;->d:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 593663
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 593646
    iget-object v0, p0, LX/3Xf;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 593647
    iget-object v0, p0, LX/3Xf;->f:Landroid/widget/TextView;

    iget v1, p0, LX/3Xf;->c:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 593648
    return-void
.end method
