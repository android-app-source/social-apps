.class public LX/3nV;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final b:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/3nY;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field private final g:I

.field private final h:Z

.field private final i:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final j:J

.field public final k:J

.field public l:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private m:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private volatile o:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/3nX;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3nY;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private q:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private r:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private volatile s:Z

.field private volatile t:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 638885
    new-instance v0, LX/3nW;

    invoke-direct {v0}, LX/3nW;-><init>()V

    sput-object v0, LX/3nV;->b:Ljava/util/Comparator;

    .line 638886
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, LX/3nV;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZLX/0P1;Ljava/lang/Boolean;)V
    .locals 6
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JJZ",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 638870
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 638871
    iput-object p1, p0, LX/3nV;->d:Ljava/lang/String;

    .line 638872
    iput-object p2, p0, LX/3nV;->e:Ljava/lang/String;

    .line 638873
    invoke-static {p2, p3}, LX/3nV;->d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/3nV;->f:Ljava/lang/String;

    .line 638874
    invoke-static {}, LX/3nV;->g()I

    move-result v0

    iput v0, p0, LX/3nV;->g:I

    .line 638875
    iput-wide p4, p0, LX/3nV;->k:J

    .line 638876
    iput-wide p6, p0, LX/3nV;->j:J

    .line 638877
    iput-boolean p8, p0, LX/3nV;->h:Z

    .line 638878
    iput-object p9, p0, LX/3nV;->i:LX/0P1;

    .line 638879
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/3nV;->p:Ljava/util/List;

    .line 638880
    invoke-static/range {p10 .. p10}, LX/3nV;->b(Ljava/lang/Boolean;)Z

    move-result v0

    .line 638881
    iput-boolean v0, p0, LX/3nV;->s:Z

    .line 638882
    iput-boolean v0, p0, LX/3nV;->t:Z

    .line 638883
    const-wide/16 v0, 0x2

    iget-object v2, p0, LX/3nV;->f:Ljava/lang/String;

    iget v3, p0, LX/3nV;->g:I

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, p4, p5}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, LX/018;->a(JLjava/lang/String;IJ)V

    .line 638884
    return-void
.end method

.method private static declared-synchronized a(LX/3nV;Ljava/lang/Boolean;)Z
    .locals 4

    .prologue
    .line 638859
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/3nV;->b(Ljava/lang/Boolean;)Z

    move-result v1

    .line 638860
    iget-boolean v0, p0, LX/3nV;->s:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    move v0, v1

    .line 638861
    :goto_0
    monitor-exit p0

    return v0

    .line 638862
    :cond_0
    :try_start_1
    iput-boolean v1, p0, LX/3nV;->s:Z

    .line 638863
    if-eqz v1, :cond_1

    .line 638864
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3nV;->t:Z

    .line 638865
    :cond_1
    if-eqz v1, :cond_2

    .line 638866
    invoke-static {p0}, LX/3nV;->e(LX/3nV;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3nX;

    .line 638867
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v0, v3}, LX/3nX;->a$redex0(LX/3nX;Ljava/lang/Boolean;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 638868
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move v0, v1

    .line 638869
    goto :goto_0
.end method

.method private static b(Ljava/lang/Boolean;)Z
    .locals 1

    .prologue
    .line 638854
    if-eqz p0, :cond_0

    .line 638855
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 638856
    :goto_0
    return v0

    .line 638857
    :cond_0
    sget-object v0, LX/00w;->b:LX/00w;

    move-object v0, v0

    .line 638858
    invoke-virtual {v0}, LX/00w;->e()Z

    move-result v0

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 638852
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 638853
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private static d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 638850
    if-nez p1, :cond_0

    .line 638851
    :goto_0
    return-object p0

    :cond_0
    const-string v0, "%s(%s)"

    invoke-static {v0, p0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private static e(LX/3nV;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/3nX;",
            ">;"
        }
    .end annotation

    .prologue
    .line 638841
    iget-object v0, p0, LX/3nV;->o:Ljava/util/Map;

    .line 638842
    if-nez v0, :cond_1

    .line 638843
    monitor-enter p0

    .line 638844
    :try_start_0
    iget-object v0, p0, LX/3nV;->o:Ljava/util/Map;

    .line 638845
    if-nez v0, :cond_0

    .line 638846
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/3nV;->o:Ljava/util/Map;

    .line 638847
    :cond_0
    monitor-exit p0

    .line 638848
    :cond_1
    return-object v0

    .line 638849
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static declared-synchronized f(LX/3nV;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 638837
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3nV;->r:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 638838
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/3nV;->r:Ljava/util/Map;

    .line 638839
    :cond_0
    iget-object v0, p0, LX/3nV;->r:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 638840
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static g()I
    .locals 1

    .prologue
    .line 638836
    sget-object v0, LX/3nV;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final varargs declared-synchronized a(JLX/0P1;Ljava/lang/Boolean;[Ljava/util/List;)J
    .locals 7
    .param p3    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            "[",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 638887
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, LX/3nV;->l:J

    .line 638888
    iput-object p3, p0, LX/3nV;->m:LX/0P1;

    .line 638889
    array-length v0, p5

    if-lez v0, :cond_1

    .line 638890
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3nV;->n:Ljava/util/List;

    .line 638891
    array-length v1, p5

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p5, v0

    .line 638892
    if-eqz v2, :cond_0

    .line 638893
    iget-object v3, p0, LX/3nV;->n:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 638894
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 638895
    :cond_1
    invoke-static {p0, p4}, LX/3nV;->a(LX/3nV;Ljava/lang/Boolean;)Z

    .line 638896
    const-wide/16 v0, 0x2

    iget-object v2, p0, LX/3nV;->f:Ljava/lang/String;

    iget v3, p0, LX/3nV;->g:I

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, LX/018;->b(JLjava/lang/String;IJ)V

    .line 638897
    iget-wide v0, p0, LX/3nV;->l:J

    iget-wide v2, p0, LX/3nV;->k:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-long/2addr v0, v2

    monitor-exit p0

    return-wide v0

    .line 638898
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;JLX/0P1;Ljava/lang/Boolean;Z)J
    .locals 7
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            "Z)J"
        }
    .end annotation

    .prologue
    .line 638826
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p6}, LX/3nV;->a(LX/3nV;Ljava/lang/Boolean;)Z

    .line 638827
    invoke-static {p1, p2}, LX/3nV;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 638828
    invoke-static {p0}, LX/3nV;->e(LX/3nV;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3nX;

    .line 638829
    const-wide/16 v2, 0x0

    .line 638830
    if-eqz v1, :cond_0

    move-wide v2, p3

    move-object v4, p6

    move-object v5, p5

    move v6, p7

    .line 638831
    invoke-static/range {v1 .. v6}, LX/3nX;->a(LX/3nX;JLjava/lang/Boolean;LX/0P1;Z)J

    move-result-wide v2

    .line 638832
    invoke-static {p0}, LX/3nV;->e(LX/3nV;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 638833
    iget-object v0, p0, LX/3nV;->p:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    move-wide v0, v2

    .line 638834
    monitor-exit p0

    return-wide v0

    .line 638835
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 638817
    monitor-enter p0

    const-wide/16 v0, 0x2

    :try_start_0
    iget-object v2, p0, LX/3nV;->f:Ljava/lang/String;

    iget v3, p0, LX/3nV;->g:I

    invoke-static {v0, v1, v2, v3}, LX/018;->f(JLjava/lang/String;I)V

    .line 638818
    invoke-static {p0}, LX/3nV;->e(LX/3nV;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3nX;

    .line 638819
    invoke-virtual {v0}, LX/3nX;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 638820
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 638821
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/3nV;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3nY;

    .line 638822
    instance-of v2, v0, LX/3nX;

    if-eqz v2, :cond_1

    .line 638823
    check-cast v0, LX/3nX;

    .line 638824
    invoke-virtual {v0}, LX/3nX;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 638825
    :cond_2
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(JLjava/lang/String;)V
    .locals 5

    .prologue
    .line 638807
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3nV;->a:Ljava/util/List;

    if-nez v0, :cond_0

    .line 638808
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/3nV;->a:Ljava/util/List;

    .line 638809
    :cond_0
    iget-object v0, p0, LX/3nV;->a:Ljava/util/List;

    new-instance v1, Landroid/util/Pair;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v1, v2, p3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 638810
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 638811
    if-nez v0, :cond_1

    .line 638812
    sget-boolean v0, LX/007;->l:Z

    move v0, v0

    .line 638813
    if-eqz v0, :cond_2

    .line 638814
    :cond_1
    iget-object v0, p0, LX/3nV;->e:Ljava/lang/String;

    const-string v1, "Stopped Marker %s (%s); Monotonic Timestamp (ms): %d; Elapsed: %d ms"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "error"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 638815
    :cond_2
    monitor-exit p0

    return-void

    .line 638816
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;JLX/0P1;)V
    .locals 8
    .param p4    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 638804
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3nV;->p:Ljava/util/List;

    new-instance v1, LX/4lS;

    const/4 v7, 0x0

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v7}, LX/4lS;-><init>(LX/3nV;Ljava/lang/String;JLX/0P1;B)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 638805
    monitor-exit p0

    return-void

    .line 638806
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 638796
    monitor-enter p0

    :try_start_0
    invoke-static {p1, p2}, LX/3nV;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 638797
    invoke-static {p0}, LX/3nV;->e(LX/3nV;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3nX;

    .line 638798
    if-eqz v0, :cond_0

    .line 638799
    invoke-virtual {v0}, LX/3nX;->c()V

    .line 638800
    invoke-static {p0}, LX/3nV;->e(LX/3nV;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 638801
    :goto_0
    monitor-exit p0

    return-void

    .line 638802
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Called cancel on marker "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") but it was not started yet."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p3, p4, v0}, LX/3nV;->a(JLjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 638803
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;JLX/0P1;Ljava/lang/Boolean;)V
    .locals 13
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 638786
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p6

    invoke-static {p0, v0}, LX/3nV;->a(LX/3nV;Ljava/lang/Boolean;)Z

    .line 638787
    invoke-static {p1, p2}, LX/3nV;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 638788
    invoke-static {p0}, LX/3nV;->e(LX/3nV;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3nX;

    .line 638789
    if-eqz v2, :cond_0

    .line 638790
    invoke-virtual {v2}, LX/3nX;->c()V

    .line 638791
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") was restarted."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-wide/from16 v0, p3

    invoke-virtual {p0, v0, v1, v2}, LX/3nV;->a(JLjava/lang/String;)V

    .line 638792
    :cond_0
    new-instance v2, LX/3nX;

    const/4 v10, 0x0

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-wide/from16 v6, p3

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    invoke-direct/range {v2 .. v10}, LX/3nX;-><init>(LX/3nV;Ljava/lang/String;Ljava/lang/String;JLX/0P1;Ljava/lang/Boolean;B)V

    .line 638793
    invoke-static {p0}, LX/3nV;->e(LX/3nV;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, v11, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 638794
    monitor-exit p0

    return-void

    .line 638795
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 638782
    monitor-enter p0

    :try_start_0
    invoke-static {p1, p2}, LX/3nV;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 638783
    invoke-static {p0}, LX/3nV;->e(LX/3nV;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3nX;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 638784
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 638785
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Lcom/facebook/sequencelogger/HoneySequenceLoggerEvent;
    .locals 18

    .prologue
    .line 638736
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/3nV;->l:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 638737
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Tried to serialize a SequenceData before it was finished."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 638738
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 638739
    :cond_0
    const/4 v13, 0x0

    .line 638740
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3nV;->p:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 638741
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3nV;->p:Ljava/util/List;

    sget-object v3, LX/3nV;->b:Ljava/util/Comparator;

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 638742
    new-instance v13, LX/162;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v13, v2}, LX/162;-><init>(LX/0mC;)V

    .line 638743
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3nV;->p:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3nY;

    .line 638744
    invoke-interface {v2}, LX/3nY;->b()LX/0lF;

    move-result-object v2

    invoke-virtual {v13, v2}, LX/162;->a(LX/0lF;)LX/162;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 638745
    :catch_0
    move-exception v2

    .line 638746
    :try_start_2
    const-string v3, "SequenceData"

    const-string v4, "Exception while serializing the sequence %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, LX/3nV;->e:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v2, v4, v5}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 638747
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 638748
    :cond_1
    const/4 v7, 0x0

    .line 638749
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3nV;->i:LX/0P1;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, LX/3nV;->i:LX/0P1;

    invoke-virtual {v2}, LX/0P1;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 638750
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3nV;->i:LX/0P1;

    invoke-static {v2}, LX/16N;->a(Ljava/util/Map;)LX/0m9;

    move-result-object v7

    .line 638751
    :cond_2
    const/4 v8, 0x0

    .line 638752
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3nV;->m:LX/0P1;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, LX/3nV;->m:LX/0P1;

    invoke-virtual {v2}, LX/0P1;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 638753
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3nV;->m:LX/0P1;

    invoke-static {v2}, LX/16N;->a(Ljava/util/Map;)LX/0m9;

    move-result-object v8

    .line 638754
    :cond_3
    const/4 v9, 0x0

    .line 638755
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3nV;->n:Ljava/util/List;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, LX/3nV;->n:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 638756
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3nV;->n:Ljava/util/List;

    invoke-static {v2}, LX/16N;->a(Ljava/util/List;)LX/0m9;

    move-result-object v9

    .line 638757
    :cond_4
    const/4 v10, 0x0

    .line 638758
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3nV;->a:Ljava/util/List;

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, LX/3nV;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 638759
    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-virtual {v2}, LX/0mC;->b()LX/162;

    move-result-object v10

    .line 638760
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3nV;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    .line 638761
    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-virtual {v3}, LX/0mC;->c()LX/0m9;

    move-result-object v5

    .line 638762
    const-string v6, "relative_start_ms"

    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/3nV;->k:J

    move-wide/from16 v16, v0

    sub-long v14, v14, v16

    invoke-virtual {v5, v6, v14, v15}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 638763
    const-string v3, "error"

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v5, v3, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 638764
    invoke-virtual {v10, v5}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_1

    .line 638765
    :cond_5
    const/4 v11, 0x0

    .line 638766
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3nV;->q:Ljava/util/Map;

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, LX/3nV;->q:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 638767
    new-instance v11, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v11, v2}, LX/0m9;-><init>(LX/0mC;)V

    .line 638768
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3nV;->q:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 638769
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v11, v3, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0m9;

    goto :goto_2

    .line 638770
    :cond_6
    const/4 v12, 0x0

    .line 638771
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3nV;->r:Ljava/util/Map;

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, LX/3nV;->r:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 638772
    new-instance v12, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v12, v2}, LX/0m9;-><init>(LX/0mC;)V

    .line 638773
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3nV;->r:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 638774
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v12, v3, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_3

    .line 638775
    :cond_7
    new-instance v2, Lcom/facebook/sequencelogger/HoneySequenceLoggerEvent;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/3nV;->d:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, LX/3nV;->e:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v14, v0, LX/3nV;->l:J

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/3nV;->k:J

    move-wide/from16 v16, v0

    sub-long v5, v14, v16

    move-object/from16 v0, p0

    iget-wide v14, v0, LX/3nV;->k:J

    invoke-virtual/range {p0 .. p0}, LX/3nV;->c()Ljava/lang/Boolean;

    move-result-object v16

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/3nV;->h:Z

    move/from16 v17, v0

    invoke-direct/range {v2 .. v17}, Lcom/facebook/sequencelogger/HoneySequenceLoggerEvent;-><init>(Ljava/lang/String;Ljava/lang/String;JLX/0lF;LX/0lF;LX/0lF;LX/162;LX/0m9;LX/0m9;LX/0lF;JLjava/lang/Boolean;Z)V

    .line 638776
    move-object/from16 v0, p0

    iget-wide v4, v0, LX/3nV;->j:J

    invoke-virtual {v2, v4, v5}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a(J)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 638777
    monitor-exit p0

    return-object v2
.end method

.method public final declared-synchronized b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 638779
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/3nV;->f(LX/3nV;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 638780
    monitor-exit p0

    return-void

    .line 638781
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 638778
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/3nV;->t:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
