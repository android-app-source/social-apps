.class public final enum LX/45f;
.super LX/45c;
.source ""


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 670476
    invoke-direct {p0, p1, p2}, LX/45c;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public final handle(LX/45g;I)V
    .locals 3

    .prologue
    .line 670466
    invoke-virtual {p1, p2}, LX/45g;->b(I)V

    .line 670467
    invoke-virtual {p1}, LX/45g;->a()Ljava/lang/String;

    move-result-object v0

    .line 670468
    iget v1, p1, LX/45g;->c:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p1, LX/45g;->c:I

    move v1, v1

    .line 670469
    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-eq p2, v1, :cond_2

    .line 670470
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne p2, v0, :cond_1

    .line 670471
    invoke-virtual {p0, p1}, LX/45f;->onEnterState(LX/45g;)V

    .line 670472
    :cond_0
    :goto_0
    return-void

    .line 670473
    :cond_1
    sget-object v0, LX/45c;->AFTER_FIRST_CHARACTER:LX/45c;

    invoke-virtual {p1, v0}, LX/45g;->a(LX/45c;)V

    goto :goto_0

    .line 670474
    :cond_2
    iget v1, p1, LX/45g;->c:I

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v1, v0, :cond_0

    .line 670475
    sget-object v0, LX/45c;->AFTER_CARRIAGE_RETURN:LX/45c;

    invoke-virtual {p1, v0}, LX/45g;->a(LX/45c;)V

    goto :goto_0
.end method

.method public final onEnterState(LX/45g;)V
    .locals 2

    .prologue
    .line 670461
    const/4 v0, 0x1

    .line 670462
    iput v0, p1, LX/45g;->c:I

    .line 670463
    iget v0, p1, LX/45g;->c:I

    invoke-virtual {p1}, LX/45g;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 670464
    sget-object v0, LX/45c;->AFTER_CARRIAGE_RETURN:LX/45c;

    invoke-virtual {p1, v0}, LX/45g;->a(LX/45c;)V

    .line 670465
    :cond_0
    return-void
.end method
