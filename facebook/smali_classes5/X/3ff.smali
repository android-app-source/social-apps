.class public LX/3ff;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Ljava/lang/Integer;",
        "Lcom/facebook/contacts/server/FetchContactsResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final c:LX/3fb;

.field private final d:LX/3fc;

.field private final e:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 622821
    const-class v0, LX/3ff;

    sput-object v0, LX/3ff;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/3fb;LX/3fc;LX/0SG;LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 622803
    invoke-direct {p0, p4}, LX/0ro;-><init>(LX/0sO;)V

    .line 622804
    iput-object p1, p0, LX/3ff;->c:LX/3fb;

    .line 622805
    iput-object p2, p0, LX/3ff;->d:LX/3fc;

    .line 622806
    iput-object p3, p0, LX/3ff;->e:LX/0SG;

    .line 622807
    return-void
.end method

.method public static a(LX/0QB;)LX/3ff;
    .locals 5

    .prologue
    .line 622818
    new-instance v4, LX/3ff;

    invoke-static {p0}, LX/3fb;->a(LX/0QB;)LX/3fb;

    move-result-object v0

    check-cast v0, LX/3fb;

    invoke-static {p0}, LX/3fc;->b(LX/0QB;)LX/3fc;

    move-result-object v1

    check-cast v1, LX/3fc;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-static {p0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v3

    check-cast v3, LX/0sO;

    invoke-direct {v4, v0, v1, v2, v3}, LX/3ff;-><init>(LX/3fb;LX/3fc;LX/0SG;LX/0sO;)V

    .line 622819
    move-object v0, v4

    .line 622820
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 622822
    const-class v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel;

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel;->a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel$MessengerContactsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchTopContactsByCFPHatCoefficientQueryModel$MessengerContactsModel;->a()LX/0Px;

    move-result-object v2

    .line 622823
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 622824
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;

    .line 622825
    iget-object v5, p0, LX/3ff;->c:LX/3fb;

    invoke-virtual {v5, v0}, LX/3fb;->a(Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;)LX/3hB;

    move-result-object v0

    .line 622826
    invoke-virtual {v0}, LX/3hB;->O()Lcom/facebook/contacts/graphql/Contact;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 622827
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 622828
    :cond_0
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 622829
    new-instance v1, Lcom/facebook/contacts/server/FetchContactsResult;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v3, p0, LX/3ff;->e:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-direct {v1, v2, v4, v5, v0}, Lcom/facebook/contacts/server/FetchContactsResult;-><init>(LX/0ta;JLX/0Px;)V

    return-object v1
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 622817
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 2

    .prologue
    .line 622808
    check-cast p1, Ljava/lang/Integer;

    .line 622809
    new-instance v0, LX/6MI;

    invoke-direct {v0}, LX/6MI;-><init>()V

    move-object v0, v0

    .line 622810
    const-string v1, "limit"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/6MI;

    .line 622811
    iget-object v1, p0, LX/3ff;->d:LX/3fc;

    invoke-virtual {v1, v0}, LX/3fc;->a(LX/0gW;)V

    .line 622812
    iget-object v1, p0, LX/3ff;->d:LX/3fc;

    invoke-virtual {v1, v0}, LX/3fc;->c(LX/0gW;)V

    .line 622813
    const/4 v1, 0x1

    .line 622814
    iput-boolean v1, v0, LX/0gW;->l:Z

    .line 622815
    move-object v0, v0

    .line 622816
    return-object v0
.end method
