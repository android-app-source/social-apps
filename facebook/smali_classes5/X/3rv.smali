.class public final LX/3rv;
.super Landroid/database/DataSetObserver;
.source ""

# interfaces
.implements LX/3ru;
.implements LX/0hc;


# instance fields
.field public final synthetic a:LX/3rt;

.field private b:I


# direct methods
.method public constructor <init>(LX/3rt;)V
    .locals 0

    .prologue
    .line 643677
    iput-object p1, p0, LX/3rv;->a:LX/3rt;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 643678
    iget v1, p0, LX/3rv;->b:I

    if-nez v1, :cond_1

    .line 643679
    iget-object v1, p0, LX/3rv;->a:LX/3rt;

    iget-object v2, p0, LX/3rv;->a:LX/3rt;

    iget-object v2, v2, LX/3rt;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    iget-object v3, p0, LX/3rv;->a:LX/3rt;

    iget-object v3, v3, LX/3rt;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/3rt;->a(ILX/0gG;)V

    .line 643680
    iget-object v1, p0, LX/3rv;->a:LX/3rt;

    iget v1, v1, LX/3rt;->g:F

    cmpl-float v1, v1, v0

    if-ltz v1, :cond_0

    iget-object v0, p0, LX/3rv;->a:LX/3rt;

    iget v0, v0, LX/3rt;->g:F

    .line 643681
    :cond_0
    iget-object v1, p0, LX/3rv;->a:LX/3rt;

    iget-object v2, p0, LX/3rv;->a:LX/3rt;

    iget-object v2, v2, LX/3rt;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, LX/3rt;->a(IFZ)V

    .line 643682
    :cond_1
    return-void
.end method

.method public final a(IFI)V
    .locals 2

    .prologue
    .line 643683
    const/high16 v0, 0x3f000000    # 0.5f

    cmpl-float v0, p2, v0

    if-lez v0, :cond_0

    .line 643684
    add-int/lit8 p1, p1, 0x1

    .line 643685
    :cond_0
    iget-object v0, p0, LX/3rv;->a:LX/3rt;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, LX/3rt;->a(IFZ)V

    .line 643686
    return-void
.end method

.method public final a(LX/0gG;LX/0gG;)V
    .locals 1

    .prologue
    .line 643687
    iget-object v0, p0, LX/3rv;->a:LX/3rt;

    invoke-virtual {v0, p1, p2}, LX/3rt;->a(LX/0gG;LX/0gG;)V

    .line 643688
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 643689
    iput p1, p0, LX/3rv;->b:I

    .line 643690
    return-void
.end method

.method public final onChanged()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 643691
    iget-object v1, p0, LX/3rv;->a:LX/3rt;

    iget-object v2, p0, LX/3rv;->a:LX/3rt;

    iget-object v2, v2, LX/3rt;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    iget-object v3, p0, LX/3rv;->a:LX/3rt;

    iget-object v3, v3, LX/3rt;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/3rt;->a(ILX/0gG;)V

    .line 643692
    iget-object v1, p0, LX/3rv;->a:LX/3rt;

    iget v1, v1, LX/3rt;->g:F

    cmpl-float v1, v1, v0

    if-ltz v1, :cond_0

    iget-object v0, p0, LX/3rv;->a:LX/3rt;

    iget v0, v0, LX/3rt;->g:F

    .line 643693
    :cond_0
    iget-object v1, p0, LX/3rv;->a:LX/3rt;

    iget-object v2, p0, LX/3rv;->a:LX/3rt;

    iget-object v2, v2, LX/3rt;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, LX/3rt;->a(IFZ)V

    .line 643694
    return-void
.end method
