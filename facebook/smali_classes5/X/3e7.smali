.class public final LX/3e7;
.super LX/0Tz;
.source ""


# static fields
.field public static final a:LX/0sv;

.field public static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0sv;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 619262
    sget-object v0, LX/3e8;->a:LX/0U1;

    sget-object v1, LX/3e8;->b:LX/0U1;

    sget-object v2, LX/3e8;->c:LX/0U1;

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3e7;->c:LX/0Px;

    .line 619263
    new-instance v0, LX/0su;

    sget-object v1, LX/3e8;->b:LX/0U1;

    sget-object v2, LX/3e8;->a:LX/0U1;

    invoke-static {v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0su;-><init>(LX/0Px;)V

    .line 619264
    sput-object v0, LX/3e7;->a:LX/0sv;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3e7;->b:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 619265
    const-string v0, "pack_types"

    sget-object v1, LX/3e7;->c:LX/0Px;

    sget-object v2, LX/3e7;->b:LX/0Px;

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;)V

    .line 619266
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 619267
    invoke-super {p0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 619268
    const-string v0, "pack_types"

    const-string v1, "pack_types_type_index"

    sget-object v2, LX/3e8;->a:LX/0U1;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x52d9fd8c

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0xd529225

    invoke-static {v0}, LX/03h;->a(I)V

    .line 619269
    return-void
.end method

.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 619270
    return-void
.end method
