.class public LX/4RD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 708167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 708099
    const/4 v8, 0x0

    .line 708100
    const/4 v7, 0x0

    .line 708101
    const/4 v6, 0x0

    .line 708102
    const-wide/16 v4, 0x0

    .line 708103
    const/4 v3, 0x0

    .line 708104
    const/4 v2, 0x0

    .line 708105
    const/4 v1, 0x0

    .line 708106
    const/4 v0, 0x0

    .line 708107
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v10, :cond_a

    .line 708108
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 708109
    const/4 v0, 0x0

    .line 708110
    :goto_0
    return v0

    .line 708111
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v10, :cond_7

    .line 708112
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 708113
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 708114
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v4, :cond_0

    .line 708115
    const-string v10, "can_viewer_remove_tag"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 708116
    const/4 v1, 0x1

    .line 708117
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    move v9, v4

    goto :goto_1

    .line 708118
    :cond_1
    const-string v10, "location"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 708119
    invoke-static {p0, p1}, LX/2ay;->a(LX/15w;LX/186;)I

    move-result v4

    move v8, v4

    goto :goto_1

    .line 708120
    :cond_2
    const-string v10, "tagger"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 708121
    invoke-static {p0, p1}, LX/2bM;->a(LX/15w;LX/186;)I

    move-result v4

    move v5, v4

    goto :goto_1

    .line 708122
    :cond_3
    const-string v10, "time"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 708123
    const/4 v0, 0x1

    .line 708124
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto :goto_1

    .line 708125
    :cond_4
    const-string v10, "subtitle"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 708126
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    move v7, v4

    goto :goto_1

    .line 708127
    :cond_5
    const-string v10, "external_url"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 708128
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    move v6, v4

    goto :goto_1

    .line 708129
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 708130
    :cond_7
    const/4 v4, 0x6

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 708131
    if-eqz v1, :cond_8

    .line 708132
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v9}, LX/186;->a(IZ)V

    .line 708133
    :cond_8
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 708134
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 708135
    if-eqz v0, :cond_9

    .line 708136
    const/4 v1, 0x3

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 708137
    :cond_9
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 708138
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 708139
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_a
    move v9, v8

    move v8, v7

    move v7, v3

    move v12, v6

    move v6, v2

    move-wide v2, v4

    move v5, v12

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 708140
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 708141
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 708142
    if-eqz v0, :cond_0

    .line 708143
    const-string v1, "can_viewer_remove_tag"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708144
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 708145
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 708146
    if-eqz v0, :cond_1

    .line 708147
    const-string v1, "location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708148
    invoke-static {p0, v0, p2}, LX/2ay;->a(LX/15i;ILX/0nX;)V

    .line 708149
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 708150
    if-eqz v0, :cond_2

    .line 708151
    const-string v1, "tagger"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708152
    invoke-static {p0, v0, p2, p3}, LX/2bM;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 708153
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 708154
    cmp-long v2, v0, v2

    if-eqz v2, :cond_3

    .line 708155
    const-string v2, "time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708156
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 708157
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 708158
    if-eqz v0, :cond_4

    .line 708159
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708160
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 708161
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 708162
    if-eqz v0, :cond_5

    .line 708163
    const-string v1, "external_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708164
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 708165
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 708166
    return-void
.end method
