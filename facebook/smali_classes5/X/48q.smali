.class public LX/48q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1mn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1mn",
        "<",
        "LX/48q;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/48r;

.field public final b:LX/48o;

.field public final c:LX/48s;

.field public d:I

.field public e:LX/48q;

.field private f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/48q;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:LX/48q;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/csslayout/YogaMeasureFunction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:LX/48p;

.field private j:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 673299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 673300
    new-instance v0, LX/48r;

    invoke-direct {v0}, LX/48r;-><init>()V

    iput-object v0, p0, LX/48q;->a:LX/48r;

    .line 673301
    new-instance v0, LX/48o;

    invoke-direct {v0}, LX/48o;-><init>()V

    iput-object v0, p0, LX/48q;->b:LX/48o;

    .line 673302
    new-instance v0, LX/48s;

    invoke-direct {v0}, LX/48s;-><init>()V

    iput-object v0, p0, LX/48q;->c:LX/48s;

    .line 673303
    const/4 v0, 0x0

    iput v0, p0, LX/48q;->d:I

    .line 673304
    const/4 v0, 0x0

    iput-object v0, p0, LX/48q;->h:Lcom/facebook/csslayout/YogaMeasureFunction;

    .line 673305
    sget-object v0, LX/48p;->DIRTY:LX/48p;

    iput-object v0, p0, LX/48q;->i:LX/48p;

    return-void
.end method

.method private a(Ljava/lang/StringBuilder;I)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 673306
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move v1, v0

    .line 673307
    :goto_0
    if-ge v1, p2, :cond_0

    .line 673308
    const-string v3, "__"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 673309
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 673310
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 673311
    iget-object v1, p0, LX/48q;->b:LX/48o;

    invoke-virtual {v1}, LX/48o;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 673312
    invoke-virtual {p0}, LX/48q;->b()I

    move-result v1

    if-nez v1, :cond_1

    .line 673313
    :goto_1
    return-void

    .line 673314
    :cond_1
    const-string v1, ", children: [\n"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 673315
    :goto_2
    invoke-virtual {p0}, LX/48q;->b()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 673316
    invoke-virtual {p0, v0}, LX/48q;->a(I)LX/48q;

    move-result-object v1

    add-int/lit8 v3, p2, 0x1

    invoke-direct {v1, p1, v3}, LX/48q;->a(Ljava/lang/StringBuilder;I)V

    .line 673317
    const-string v1, "\n"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 673318
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 673319
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private t()V
    .locals 2

    .prologue
    .line 673320
    iget-object v0, p0, LX/48q;->i:LX/48p;

    sget-object v1, LX/48p;->DIRTY:LX/48p;

    if-ne v0, v1, :cond_1

    .line 673321
    :cond_0
    :goto_0
    return-void

    .line 673322
    :cond_1
    iget-object v0, p0, LX/48q;->i:LX/48p;

    sget-object v1, LX/48p;->HAS_NEW_LAYOUT:LX/48p;

    if-ne v0, v1, :cond_2

    .line 673323
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Previous layout was ignored! markLayoutSeen() never called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 673324
    :cond_2
    sget-object v0, LX/48p;->DIRTY:LX/48p;

    iput-object v0, p0, LX/48q;->i:LX/48p;

    .line 673325
    iget-object v0, p0, LX/48q;->b:LX/48o;

    const/high16 v1, 0x7fc00000    # NaNf

    iput v1, v0, LX/48o;->d:F

    .line 673326
    iget-object v0, p0, LX/48q;->g:LX/48q;

    if-eqz v0, :cond_0

    .line 673327
    iget-object v0, p0, LX/48q;->g:LX/48q;

    invoke-direct {v0}, LX/48q;->t()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/csslayout/YogaEdge;)F
    .locals 2

    .prologue
    .line 673328
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->n:LX/1mz;

    invoke-virtual {p1}, Lcom/facebook/csslayout/YogaEdge;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, LX/1mz;->a(I)F

    move-result v0

    return v0
.end method

.method public final a(FLcom/facebook/csslayout/YogaMeasureMode;FLcom/facebook/csslayout/YogaMeasureMode;)J
    .locals 6

    .prologue
    .line 673329
    invoke-virtual {p0}, LX/48q;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 673330
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Measure function isn\'t defined!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 673331
    :cond_0
    iget-object v0, p0, LX/48q;->h:Lcom/facebook/csslayout/YogaMeasureFunction;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/csslayout/YogaMeasureFunction;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/facebook/csslayout/YogaMeasureFunction;->measure(LX/1mn;FLcom/facebook/csslayout/YogaMeasureMode;FLcom/facebook/csslayout/YogaMeasureMode;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(I)LX/48q;
    .locals 1

    .prologue
    .line 673332
    iget-object v0, p0, LX/48q;->f:Ljava/util/ArrayList;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 673333
    iget-object v0, p0, LX/48q;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48q;

    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 673334
    iget-object v0, p0, LX/48q;->g:LX/48q;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/48q;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/48q;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 673335
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You should not free an attached CSSNodeDEPRECATED"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 673336
    :cond_1
    iget-object v0, p0, LX/48q;->a:LX/48r;

    invoke-virtual {v0}, LX/48r;->a()V

    .line 673337
    iget-object v0, p0, LX/48q;->b:LX/48o;

    invoke-virtual {v0}, LX/48o;->a()V

    .line 673338
    const/4 v0, 0x0

    iput v0, p0, LX/48q;->d:I

    .line 673339
    sget-object v0, LX/48p;->DIRTY:LX/48p;

    iput-object v0, p0, LX/48q;->i:LX/48p;

    .line 673340
    const/4 v0, 0x0

    iput-object v0, p0, LX/48q;->h:Lcom/facebook/csslayout/YogaMeasureFunction;

    .line 673341
    return-void
.end method

.method public final a(F)V
    .locals 3

    .prologue
    const/high16 v2, 0x7fc00000    # NaNf

    const/4 v1, 0x0

    .line 673342
    invoke-static {p1}, LX/1mo;->a(F)Z

    move-result v0

    if-nez v0, :cond_0

    cmpl-float v0, p1, v1

    if-nez v0, :cond_1

    .line 673343
    :cond_0
    invoke-virtual {p0, v1}, LX/48q;->b(F)V

    .line 673344
    invoke-virtual {p0, v1}, LX/48q;->c(F)V

    .line 673345
    invoke-virtual {p0, v2}, LX/48q;->d(F)V

    .line 673346
    :goto_0
    return-void

    .line 673347
    :cond_1
    cmpl-float v0, p1, v1

    if-lez v0, :cond_2

    .line 673348
    invoke-virtual {p0, p1}, LX/48q;->b(F)V

    .line 673349
    invoke-virtual {p0, v1}, LX/48q;->c(F)V

    .line 673350
    invoke-virtual {p0, v1}, LX/48q;->d(F)V

    goto :goto_0

    .line 673351
    :cond_2
    invoke-virtual {p0, v1}, LX/48q;->b(F)V

    .line 673352
    neg-float v0, p1

    invoke-virtual {p0, v0}, LX/48q;->c(F)V

    .line 673353
    invoke-virtual {p0, v2}, LX/48q;->d(F)V

    goto :goto_0
.end method

.method public final a(LX/1mn;I)V
    .locals 2

    .prologue
    .line 673354
    check-cast p1, LX/48q;

    .line 673355
    iget-object v0, p1, LX/48q;->g:LX/48q;

    if-eqz v0, :cond_0

    .line 673356
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Child already has a parent, it must be removed first."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 673357
    :cond_0
    iget-object v0, p0, LX/48q;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 673358
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/48q;->f:Ljava/util/ArrayList;

    .line 673359
    :cond_1
    iget-object v0, p0, LX/48q;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 673360
    iput-object p0, p1, LX/48q;->g:LX/48q;

    .line 673361
    invoke-direct {p0}, LX/48q;->t()V

    .line 673362
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaAlign;)V
    .locals 1

    .prologue
    .line 673363
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->e:Lcom/facebook/csslayout/YogaAlign;

    if-eq v0, p1, :cond_0

    .line 673364
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iput-object p1, v0, LX/48r;->e:Lcom/facebook/csslayout/YogaAlign;

    .line 673365
    invoke-direct {p0}, LX/48q;->t()V

    .line 673366
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaDirection;)V
    .locals 1

    .prologue
    .line 673367
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->a:Lcom/facebook/csslayout/YogaDirection;

    if-eq v0, p1, :cond_0

    .line 673368
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iput-object p1, v0, LX/48r;->a:Lcom/facebook/csslayout/YogaDirection;

    .line 673369
    invoke-direct {p0}, LX/48q;->t()V

    .line 673370
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaEdge;F)V
    .locals 2

    .prologue
    .line 673371
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->m:LX/1mz;

    invoke-virtual {p1}, Lcom/facebook/csslayout/YogaEdge;->intValue()I

    move-result v1

    invoke-virtual {v0, v1, p2}, LX/1mz;->a(IF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 673372
    invoke-direct {p0}, LX/48q;->t()V

    .line 673373
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaFlexDirection;)V
    .locals 1

    .prologue
    .line 673374
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->b:Lcom/facebook/csslayout/YogaFlexDirection;

    if-eq v0, p1, :cond_0

    .line 673375
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iput-object p1, v0, LX/48r;->b:Lcom/facebook/csslayout/YogaFlexDirection;

    .line 673376
    invoke-direct {p0}, LX/48q;->t()V

    .line 673377
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaJustify;)V
    .locals 1

    .prologue
    .line 673414
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->c:Lcom/facebook/csslayout/YogaJustify;

    if-eq v0, p1, :cond_0

    .line 673415
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iput-object p1, v0, LX/48r;->c:Lcom/facebook/csslayout/YogaJustify;

    .line 673416
    invoke-direct {p0}, LX/48q;->t()V

    .line 673417
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaMeasureFunction;)V
    .locals 1

    .prologue
    .line 673378
    iget-object v0, p0, LX/48q;->h:Lcom/facebook/csslayout/YogaMeasureFunction;

    if-eq v0, p1, :cond_0

    .line 673379
    iput-object p1, p0, LX/48q;->h:Lcom/facebook/csslayout/YogaMeasureFunction;

    .line 673380
    invoke-direct {p0}, LX/48q;->t()V

    .line 673381
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaOverflow;)V
    .locals 1

    .prologue
    .line 673382
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->i:Lcom/facebook/csslayout/YogaOverflow;

    if-eq v0, p1, :cond_0

    .line 673383
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iput-object p1, v0, LX/48r;->i:Lcom/facebook/csslayout/YogaOverflow;

    .line 673384
    invoke-direct {p0}, LX/48q;->t()V

    .line 673385
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaPositionType;)V
    .locals 1

    .prologue
    .line 673386
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->g:Lcom/facebook/csslayout/YogaPositionType;

    if-eq v0, p1, :cond_0

    .line 673387
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iput-object p1, v0, LX/48r;->g:Lcom/facebook/csslayout/YogaPositionType;

    .line 673388
    invoke-direct {p0}, LX/48q;->t()V

    .line 673389
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaWrap;)V
    .locals 1

    .prologue
    .line 673237
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->h:Lcom/facebook/csslayout/YogaWrap;

    if-eq v0, p1, :cond_0

    .line 673238
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iput-object p1, v0, LX/48r;->h:Lcom/facebook/csslayout/YogaWrap;

    .line 673239
    invoke-direct {p0}, LX/48q;->t()V

    .line 673240
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 673394
    iput-object p1, p0, LX/48q;->j:Ljava/lang/Object;

    .line 673395
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 673396
    iget-object v0, p0, LX/48q;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/48q;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final b(I)LX/1mn;
    .locals 2

    .prologue
    .line 673397
    iget-object v0, p0, LX/48q;->f:Ljava/util/ArrayList;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 673398
    iget-object v0, p0, LX/48q;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48q;

    .line 673399
    const/4 v1, 0x0

    iput-object v1, v0, LX/48q;->g:LX/48q;

    .line 673400
    invoke-direct {p0}, LX/48q;->t()V

    .line 673401
    return-object v0
.end method

.method public final b(F)V
    .locals 1

    .prologue
    .line 673402
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget v0, v0, LX/48r;->j:F

    invoke-static {v0, p1}, LX/3Fp;->a(FF)Z

    move-result v0

    if-nez v0, :cond_0

    .line 673403
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iput p1, v0, LX/48r;->j:F

    .line 673404
    invoke-direct {p0}, LX/48q;->t()V

    .line 673405
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/csslayout/YogaAlign;)V
    .locals 1

    .prologue
    .line 673406
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->f:Lcom/facebook/csslayout/YogaAlign;

    if-eq v0, p1, :cond_0

    .line 673407
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iput-object p1, v0, LX/48r;->f:Lcom/facebook/csslayout/YogaAlign;

    .line 673408
    invoke-direct {p0}, LX/48q;->t()V

    .line 673409
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/csslayout/YogaEdge;F)V
    .locals 2

    .prologue
    .line 673410
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->n:LX/1mz;

    invoke-virtual {p1}, Lcom/facebook/csslayout/YogaEdge;->intValue()I

    move-result v1

    invoke-virtual {v0, v1, p2}, LX/1mz;->a(IF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 673411
    invoke-direct {p0}, LX/48q;->t()V

    .line 673412
    :cond_0
    return-void
.end method

.method public final synthetic c(I)LX/1mn;
    .locals 1

    .prologue
    .line 673413
    invoke-virtual {p0, p1}, LX/48q;->a(I)LX/48q;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 3

    .prologue
    const/high16 v2, 0x7fc00000    # NaNf

    .line 673297
    new-instance v0, LX/1Dq;

    invoke-direct {v0}, LX/1Dq;-><init>()V

    const/4 v1, 0x0

    invoke-static {v0, p0, v2, v2, v1}, LX/48t;->a(LX/1Dq;LX/48q;FFLcom/facebook/csslayout/YogaDirection;)V

    .line 673298
    return-void
.end method

.method public final c(F)V
    .locals 1

    .prologue
    .line 673390
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget v0, v0, LX/48r;->k:F

    invoke-static {v0, p1}, LX/3Fp;->a(FF)Z

    move-result v0

    if-nez v0, :cond_0

    .line 673391
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iput p1, v0, LX/48r;->k:F

    .line 673392
    invoke-direct {p0}, LX/48q;->t()V

    .line 673393
    :cond_0
    return-void
.end method

.method public final c(Lcom/facebook/csslayout/YogaAlign;)V
    .locals 1

    .prologue
    .line 673233
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->d:Lcom/facebook/csslayout/YogaAlign;

    if-eq v0, p1, :cond_0

    .line 673234
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iput-object p1, v0, LX/48r;->d:Lcom/facebook/csslayout/YogaAlign;

    .line 673235
    invoke-direct {p0}, LX/48q;->t()V

    .line 673236
    :cond_0
    return-void
.end method

.method public final c(Lcom/facebook/csslayout/YogaEdge;F)V
    .locals 2

    .prologue
    .line 673241
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->o:LX/1mz;

    invoke-virtual {p1}, Lcom/facebook/csslayout/YogaEdge;->intValue()I

    move-result v1

    invoke-virtual {v0, v1, p2}, LX/1mz;->a(IF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 673242
    invoke-direct {p0}, LX/48q;->t()V

    .line 673243
    :cond_0
    return-void
.end method

.method public final d(F)V
    .locals 1

    .prologue
    .line 673244
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget v0, v0, LX/48r;->l:F

    invoke-static {v0, p1}, LX/3Fp;->a(FF)Z

    move-result v0

    if-nez v0, :cond_0

    .line 673245
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iput p1, v0, LX/48r;->l:F

    .line 673246
    invoke-direct {p0}, LX/48q;->t()V

    .line 673247
    :cond_0
    return-void
.end method

.method public final d(Lcom/facebook/csslayout/YogaEdge;F)V
    .locals 2

    .prologue
    .line 673248
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->p:LX/1mz;

    invoke-virtual {p1}, Lcom/facebook/csslayout/YogaEdge;->intValue()I

    move-result v1

    invoke-virtual {v0, v1, p2}, LX/1mz;->a(IF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 673249
    invoke-direct {p0}, LX/48q;->t()V

    .line 673250
    :cond_0
    return-void
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 673251
    iget-object v0, p0, LX/48q;->i:LX/48p;

    sget-object v1, LX/48p;->HAS_NEW_LAYOUT:LX/48p;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(F)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 673252
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->q:[F

    aget v0, v0, v1

    invoke-static {v0, p1}, LX/3Fp;->a(FF)Z

    move-result v0

    if-nez v0, :cond_0

    .line 673253
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->q:[F

    aput p1, v0, v1

    .line 673254
    invoke-direct {p0}, LX/48q;->t()V

    .line 673255
    :cond_0
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 673256
    iget-object v0, p0, LX/48q;->h:Lcom/facebook/csslayout/YogaMeasureFunction;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(F)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 673257
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->q:[F

    aget v0, v0, v1

    invoke-static {v0, p1}, LX/3Fp;->a(FF)Z

    move-result v0

    if-nez v0, :cond_0

    .line 673258
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->q:[F

    aput p1, v0, v1

    .line 673259
    invoke-direct {p0}, LX/48q;->t()V

    .line 673260
    :cond_0
    return-void
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 673261
    iget-object v0, p0, LX/48q;->i:LX/48p;

    sget-object v1, LX/48p;->DIRTY:LX/48p;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 673262
    invoke-virtual {p0}, LX/48q;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 673263
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Expected node to have a new layout to be seen!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 673264
    :cond_0
    sget-object v0, LX/48p;->UP_TO_DATE:LX/48p;

    iput-object v0, p0, LX/48q;->i:LX/48p;

    .line 673265
    return-void
.end method

.method public final g(F)V
    .locals 1

    .prologue
    .line 673266
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget v0, v0, LX/48r;->r:F

    invoke-static {v0, p1}, LX/3Fp;->a(FF)Z

    move-result v0

    if-nez v0, :cond_0

    .line 673267
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iput p1, v0, LX/48r;->r:F

    .line 673268
    invoke-direct {p0}, LX/48q;->t()V

    .line 673269
    :cond_0
    return-void
.end method

.method public final h()Lcom/facebook/csslayout/YogaDirection;
    .locals 1

    .prologue
    .line 673270
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->a:Lcom/facebook/csslayout/YogaDirection;

    return-object v0
.end method

.method public final h(F)V
    .locals 1

    .prologue
    .line 673271
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget v0, v0, LX/48r;->s:F

    invoke-static {v0, p1}, LX/3Fp;->a(FF)Z

    move-result v0

    if-nez v0, :cond_0

    .line 673272
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iput p1, v0, LX/48r;->s:F

    .line 673273
    invoke-direct {p0}, LX/48q;->t()V

    .line 673274
    :cond_0
    return-void
.end method

.method public final i()F
    .locals 2

    .prologue
    .line 673275
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->q:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public final i(F)V
    .locals 1

    .prologue
    .line 673276
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget v0, v0, LX/48r;->t:F

    invoke-static {v0, p1}, LX/3Fp;->a(FF)Z

    move-result v0

    if-nez v0, :cond_0

    .line 673277
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iput p1, v0, LX/48r;->t:F

    .line 673278
    invoke-direct {p0}, LX/48q;->t()V

    .line 673279
    :cond_0
    return-void
.end method

.method public final j()F
    .locals 2

    .prologue
    .line 673280
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->q:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    return v0
.end method

.method public final j(F)V
    .locals 1

    .prologue
    .line 673281
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget v0, v0, LX/48r;->u:F

    invoke-static {v0, p1}, LX/3Fp;->a(FF)Z

    move-result v0

    if-nez v0, :cond_0

    .line 673282
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iput p1, v0, LX/48r;->u:F

    .line 673283
    invoke-direct {p0}, LX/48q;->t()V

    .line 673284
    :cond_0
    return-void
.end method

.method public final k()F
    .locals 2

    .prologue
    .line 673285
    iget-object v0, p0, LX/48q;->b:LX/48o;

    iget-object v0, v0, LX/48o;->a:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public final l()F
    .locals 2

    .prologue
    .line 673286
    iget-object v0, p0, LX/48q;->b:LX/48o;

    iget-object v0, v0, LX/48o;->a:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    return v0
.end method

.method public final m()F
    .locals 2

    .prologue
    .line 673287
    iget-object v0, p0, LX/48q;->b:LX/48o;

    iget-object v0, v0, LX/48o;->b:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public final n()F
    .locals 2

    .prologue
    .line 673288
    iget-object v0, p0, LX/48q;->b:LX/48o;

    iget-object v0, v0, LX/48o;->b:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    return v0
.end method

.method public final o()Lcom/facebook/csslayout/YogaDirection;
    .locals 1

    .prologue
    .line 673289
    iget-object v0, p0, LX/48q;->b:LX/48o;

    iget-object v0, v0, LX/48o;->c:Lcom/facebook/csslayout/YogaDirection;

    return-object v0
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 673290
    sget-object v0, LX/48p;->HAS_NEW_LAYOUT:LX/48p;

    iput-object v0, p0, LX/48q;->i:LX/48p;

    .line 673291
    return-void
.end method

.method public final q()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 673292
    iget-object v0, p0, LX/48q;->j:Ljava/lang/Object;

    return-object v0
.end method

.method public final r()LX/1mn;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 673293
    iget-object v0, p0, LX/48q;->g:LX/48q;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 673294
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 673295
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/48q;->a(Ljava/lang/StringBuilder;I)V

    .line 673296
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
