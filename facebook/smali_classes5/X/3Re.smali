.class public LX/3Re;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile E:LX/3Re;

.field private static final d:Ljava/lang/String;


# instance fields
.field private final A:LX/3Rj;

.field private final B:LX/3RE;

.field private final C:LX/0Uh;

.field private final D:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "LX/Jul;",
            ">;"
        }
    .end annotation
.end field

.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2OS;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/CJK;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/lang/String;

.field public final f:Landroid/content/Context;

.field private final g:Landroid/content/res/Resources;

.field private final h:LX/2zj;

.field private final i:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final j:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

.field private final k:LX/3Rf;

.field private final l:LX/34G;

.field private final m:LX/3CQ;

.field private final n:LX/3Rh;

.field private final o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final p:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final q:Landroid/app/KeyguardManager;

.field public final r:Landroid/os/PowerManager;

.field private final s:LX/15P;

.field private final t:LX/03V;

.field public final u:LX/3Ri;

.field private final v:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field private final w:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JtE;",
            ">;"
        }
    .end annotation
.end field

.field private final x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Js0;",
            ">;"
        }
    .end annotation
.end field

.field private final y:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6lN;",
            ">;"
        }
    .end annotation
.end field

.field private final z:Ljava/util/Random;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 581241
    new-instance v0, Ljava/lang/String;

    const v1, 0x1f44d

    invoke-static {v1}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    sput-object v0, LX/3Re;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/2zj;Landroid/content/Context;Landroid/content/res/Resources;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;LX/3Rf;LX/34G;LX/3CQ;LX/3Rh;LX/0Or;LX/0Or;Landroid/app/KeyguardManager;Landroid/os/PowerManager;LX/15P;LX/03V;LX/3Ri;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/util/Random;LX/3Rj;LX/3RE;LX/0Uh;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/common/android/PackageName;
        .end annotation
    .end param
    .param p11    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/chatheads/annotations/IsChatHeadsEnabled;
        .end annotation
    .end param
    .param p12    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/chatheads/annotations/IsChatHeadsPermitted;
        .end annotation
    .end param
    .param p18    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserKey;
        .end annotation
    .end param
    .param p22    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/2zj;",
            "Landroid/content/Context;",
            "Landroid/content/res/Resources;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;",
            "LX/3Rf;",
            "LX/34G;",
            "LX/3CQ;",
            "LX/3Rh;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Landroid/app/KeyguardManager;",
            "Landroid/os/PowerManager;",
            "LX/15P;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/3Ri;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JtE;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Js0;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6lN;",
            ">;",
            "Ljava/util/Random;",
            "LX/3Rj;",
            "LX/3RE;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 581242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 581243
    iput-object p1, p0, LX/3Re;->e:Ljava/lang/String;

    .line 581244
    iput-object p6, p0, LX/3Re;->j:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    .line 581245
    iput-object p7, p0, LX/3Re;->k:LX/3Rf;

    .line 581246
    iput-object p3, p0, LX/3Re;->f:Landroid/content/Context;

    .line 581247
    iput-object p4, p0, LX/3Re;->g:Landroid/content/res/Resources;

    .line 581248
    iput-object p2, p0, LX/3Re;->h:LX/2zj;

    .line 581249
    iput-object p5, p0, LX/3Re;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 581250
    iput-object p8, p0, LX/3Re;->l:LX/34G;

    .line 581251
    iput-object p9, p0, LX/3Re;->m:LX/3CQ;

    .line 581252
    iput-object p10, p0, LX/3Re;->n:LX/3Rh;

    .line 581253
    iput-object p11, p0, LX/3Re;->o:LX/0Or;

    .line 581254
    iput-object p12, p0, LX/3Re;->p:LX/0Or;

    .line 581255
    iput-object p13, p0, LX/3Re;->q:Landroid/app/KeyguardManager;

    .line 581256
    move-object/from16 v0, p14

    iput-object v0, p0, LX/3Re;->r:Landroid/os/PowerManager;

    .line 581257
    move-object/from16 v0, p15

    iput-object v0, p0, LX/3Re;->s:LX/15P;

    .line 581258
    move-object/from16 v0, p16

    iput-object v0, p0, LX/3Re;->t:LX/03V;

    .line 581259
    move-object/from16 v0, p17

    iput-object v0, p0, LX/3Re;->u:LX/3Ri;

    .line 581260
    move-object/from16 v0, p18

    iput-object v0, p0, LX/3Re;->v:LX/0Or;

    .line 581261
    move-object/from16 v0, p19

    iput-object v0, p0, LX/3Re;->w:LX/0Ot;

    .line 581262
    move-object/from16 v0, p20

    iput-object v0, p0, LX/3Re;->x:LX/0Ot;

    .line 581263
    move-object/from16 v0, p21

    iput-object v0, p0, LX/3Re;->y:LX/0Ot;

    .line 581264
    move-object/from16 v0, p22

    iput-object v0, p0, LX/3Re;->z:Ljava/util/Random;

    .line 581265
    move-object/from16 v0, p23

    iput-object v0, p0, LX/3Re;->A:LX/3Rj;

    .line 581266
    move-object/from16 v0, p24

    iput-object v0, p0, LX/3Re;->B:LX/3RE;

    .line 581267
    move-object/from16 v0, p25

    iput-object v0, p0, LX/3Re;->C:LX/0Uh;

    .line 581268
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, LX/3Re;->D:Ljava/util/Map;

    .line 581269
    return-void
.end method

.method private static a(LX/3Re;Lcom/facebook/messaging/model/messages/Message;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/messages/Message;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/attachments/ImageAttachmentData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 581270
    iget-object v0, p0, LX/3Re;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2OS;

    .line 581271
    invoke-virtual {v0, p1}, LX/2OS;->e(Lcom/facebook/messaging/model/messages/Message;)LX/0Px;

    move-result-object v2

    .line 581272
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 581273
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/attachments/ImageAttachmentData;

    .line 581274
    iget-object v5, v0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->e:Ljava/lang/String;

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    const/4 v5, 0x1

    :goto_1
    move v5, v5

    .line 581275
    if-eqz v5, :cond_0

    .line 581276
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 581277
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 581278
    :cond_1
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0

    :cond_2
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private static a(Lcom/facebook/messaging/model/messages/MessagesCollection;LX/Jul;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/messages/MessagesCollection;",
            "LX/Jul;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .prologue
    .line 581279
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 581280
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v3

    .line 581281
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v4, v0

    .line 581282
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 581283
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 581284
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 581285
    :cond_0
    iget-object v0, p1, LX/Jul;->a:Ljava/util/LinkedList;

    move-object v0, v0

    .line 581286
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 581287
    iget-object v4, v0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 581288
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 581289
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 581290
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v0, v0

    .line 581291
    invoke-virtual {v2, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 581292
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/messaging/model/threadkey/ThreadKey;ILX/Jul;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "I",
            "LX/Jul;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .prologue
    .line 581293
    iget-object v0, p0, LX/3Re;->j:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    add-int/lit8 v1, p2, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v0

    .line 581294
    if-nez v0, :cond_0

    .line 581295
    iget-object v0, p3, LX/Jul;->a:Ljava/util/LinkedList;

    move-object v0, v0

    .line 581296
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, LX/0Px;->reverse()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-static {v0, p3}, LX/3Re;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;LX/Jul;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/3Re;
    .locals 3

    .prologue
    .line 581297
    sget-object v0, LX/3Re;->E:LX/3Re;

    if-nez v0, :cond_1

    .line 581298
    const-class v1, LX/3Re;

    monitor-enter v1

    .line 581299
    :try_start_0
    sget-object v0, LX/3Re;->E:LX/3Re;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 581300
    if-eqz v2, :cond_0

    .line 581301
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/3Re;->b(LX/0QB;)LX/3Re;

    move-result-object v0

    sput-object v0, LX/3Re;->E:LX/3Re;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 581302
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 581303
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 581304
    :cond_1
    sget-object v0, LX/3Re;->E:LX/3Re;

    return-object v0

    .line 581305
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 581306
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;LX/Jul;Lcom/facebook/messaging/model/threads/ThreadCustomization;I)LX/3pj;
    .locals 5

    .prologue
    .line 581307
    new-instance v2, LX/3ph;

    invoke-direct {v2, p2}, LX/3ph;-><init>(Ljava/lang/String;)V

    .line 581308
    invoke-static {p0, p1, p3, p5}, LX/3Re;->a(LX/3Re;Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/Jul;I)Ljava/util/List;

    move-result-object v3

    .line 581309
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 581310
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 581311
    const/4 v4, 0x0

    invoke-static {p0, v0, p4, v4}, LX/3Re;->a(LX/3Re;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/3ph;->a(Ljava/lang/String;)LX/3ph;

    .line 581312
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 581313
    :cond_0
    invoke-static {p0, v2, p3, p1}, LX/3Re;->a(LX/3Re;LX/3ph;LX/Jul;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 581314
    invoke-virtual {v2}, LX/3ph;->a()LX/3pj;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;ILX/Jul;Lcom/facebook/messaging/model/threads/ThreadCustomization;)LX/3pm;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 581315
    new-instance v1, LX/3pm;

    invoke-direct {v1}, LX/3pm;-><init>()V

    .line 581316
    invoke-virtual {v1, p1}, LX/3pm;->a(Ljava/lang/CharSequence;)LX/3pm;

    .line 581317
    iget-object v0, p3, LX/Jul;->a:Ljava/util/LinkedList;

    move-object v0, v0

    .line 581318
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 581319
    invoke-static {p0, v0, p4, v6, v6}, LX/3Re;->a(LX/3Re;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;ZZ)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/3pm;->c(Ljava/lang/CharSequence;)LX/3pm;

    goto :goto_0

    .line 581320
    :cond_0
    const/4 v0, 0x7

    if-le p2, v0, :cond_1

    .line 581321
    iget-object v0, p0, LX/3Re;->g:Landroid/content/res/Resources;

    const v2, 0x7f0f0020

    add-int/lit8 v3, p2, -0x7

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    add-int/lit8 v5, p2, -0x7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 581322
    invoke-static {v0}, LX/2HB;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, v1, LX/3pm;->f:Ljava/lang/CharSequence;

    .line 581323
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/3pm;->g:Z

    .line 581324
    :cond_1
    return-object v1
.end method

.method private static a(LX/2uW;)LX/Jsz;
    .locals 2

    .prologue
    .line 581335
    sget-object v0, LX/Juo;->a:[I

    invoke-virtual {p0}, LX/2uW;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 581336
    sget-object v0, LX/Jsz;->REGULAR:LX/Jsz;

    :goto_0
    return-object v0

    .line 581337
    :pswitch_0
    sget-object v0, LX/Jsz;->ADD_MEMBERS:LX/Jsz;

    goto :goto_0

    .line 581338
    :pswitch_1
    sget-object v0, LX/Jsz;->REMOVE_MEMBERS:LX/Jsz;

    goto :goto_0

    .line 581339
    :pswitch_2
    sget-object v0, LX/Jsz;->SET_NAME:LX/Jsz;

    goto :goto_0

    .line 581340
    :pswitch_3
    sget-object v0, LX/Jsz;->SET_IMAGE:LX/Jsz;

    goto :goto_0

    .line 581341
    :pswitch_4
    sget-object v0, LX/Jsz;->ADMIN:LX/Jsz;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(LX/3Re;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;ZZ)Ljava/lang/CharSequence;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 581325
    invoke-static {p0, p1, p2, p4}, LX/3Re;->a(LX/3Re;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;Z)Ljava/lang/String;

    move-result-object v0

    .line 581326
    if-nez p3, :cond_1

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v1, v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v2, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v1, v2, :cond_1

    .line 581327
    :cond_0
    :goto_0
    return-object v0

    .line 581328
    :cond_1
    iget-object v1, p0, LX/3Re;->j:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    invoke-virtual {v1, p1}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->b(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v1

    .line 581329
    if-eqz v1, :cond_0

    .line 581330
    iget-object v2, p0, LX/3Re;->k:LX/3Rf;

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2, v1, v3}, LX/3Rf;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/lang/String;

    move-result-object v2

    .line 581331
    invoke-static {v2, v0}, LX/3Re;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 581332
    new-instance v1, Landroid/text/SpannableString;

    iget-object v3, p0, LX/3Re;->g:Landroid/content/res/Resources;

    const v4, 0x7f08021b

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    aput-object v0, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 581333
    new-instance v0, Landroid/text/style/StyleSpan;

    invoke-direct {v0, v7}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-interface {v1, v0, v6, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    move-object v0, v1

    .line 581334
    goto :goto_0
.end method

.method private static a(LX/3Re;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 581383
    if-eqz p3, :cond_1

    iget-boolean v0, p1, Lcom/facebook/messaging/model/messages/Message;->S:Z

    if-nez v0, :cond_1

    .line 581384
    iget-object v0, p0, LX/3Re;->l:LX/34G;

    invoke-virtual {v0, p1, p2}, LX/34G;->c(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v0

    .line 581385
    :goto_0
    sget-object v1, LX/1zU;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 581386
    sget-object v0, LX/3Re;->d:Ljava/lang/String;

    .line 581387
    :cond_0
    return-object v0

    .line 581388
    :cond_1
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3Re;->l:LX/34G;

    invoke-virtual {v0, p1, p2}, LX/34G;->b(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method private static a(LX/3Re;Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/Jul;I)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "LX/Jul;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .prologue
    .line 581373
    invoke-direct {p0, p1, p3, p2}, LX/3Re;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;ILX/Jul;)LX/0Px;

    move-result-object v3

    .line 581374
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, p3}, Ljava/util/ArrayList;-><init>(I)V

    .line 581375
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    move v1, p3

    :goto_0
    if-ltz v2, :cond_0

    .line 581376
    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 581377
    iget-object v5, p0, LX/3Re;->j:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    invoke-virtual {v5, v0}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->b(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v5

    .line 581378
    iget-object v5, v5, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-static {p0, v5}, LX/3Re;->a(LX/3Re;Lcom/facebook/user/model/UserKey;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 581379
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 581380
    add-int/lit8 v0, v1, -0x1

    if-eqz v0, :cond_0

    .line 581381
    :goto_1
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 581382
    :cond_0
    return-object v4

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private static a(LX/3Re;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3Re;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2OS;",
            ">;",
            "LX/0Or",
            "<",
            "LX/CJK;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 581389
    iput-object p1, p0, LX/3Re;->a:LX/0Or;

    iput-object p2, p0, LX/3Re;->b:LX/0Or;

    iput-object p3, p0, LX/3Re;->c:LX/0Or;

    return-void
.end method

.method private static a(LX/3Re;LX/3ph;LX/Jul;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 4

    .prologue
    .line 581354
    iget-wide v2, p2, LX/Jul;->b:J

    move-wide v0, v2

    .line 581355
    iput-wide v0, p1, LX/3ph;->f:J

    .line 581356
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/3Re;->f:Landroid/content/Context;

    const-class v2, Lcom/facebook/messengercar/CarNotificationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 581357
    const-string v1, "read_thread"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 581358
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 581359
    const-string v1, "thread_key"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 581360
    iget-object v1, p0, LX/3Re;->f:Landroid/content/Context;

    invoke-virtual {p3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->hashCode()I

    move-result v2

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, LX/0nt;->c(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 581361
    iput-object v0, p1, LX/3ph;->d:Landroid/app/PendingIntent;

    .line 581362
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/3Re;->f:Landroid/content/Context;

    const-class v2, Lcom/facebook/messengercar/CarNotificationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 581363
    const-string v1, "reply"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 581364
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 581365
    const-string v1, "thread_key"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 581366
    iget-object v1, p0, LX/3Re;->f:Landroid/content/Context;

    invoke-virtual {p3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->hashCode()I

    move-result v2

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, LX/0nt;->c(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, LX/3qF;

    const-string v2, "voice_reply"

    invoke-direct {v1, v2}, LX/3qF;-><init>(Ljava/lang/String;)V

    const-string v2, "Mute"

    .line 581367
    iput-object v2, v1, LX/3qF;->b:Ljava/lang/CharSequence;

    .line 581368
    move-object v1, v1

    .line 581369
    invoke-virtual {v1}, LX/3qF;->a()LX/3qL;

    move-result-object v1

    .line 581370
    iput-object v1, p1, LX/3ph;->c:LX/3qL;

    .line 581371
    iput-object v0, p1, LX/3ph;->e:Landroid/app/PendingIntent;

    .line 581372
    return-void
.end method

.method private static a(LX/Jsx;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Jsx;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/attachments/ImageAttachmentData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 581349
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/attachments/ImageAttachmentData;

    .line 581350
    new-instance v2, Lcom/facebook/messengerwear/shared/Message$Attachment;

    iget-object v0, v0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->e:Ljava/lang/String;

    sget-object v3, LX/Jsw;->PHOTO:LX/Jsw;

    invoke-direct {v2, v0, v3}, Lcom/facebook/messengerwear/shared/Message$Attachment;-><init>(Ljava/lang/String;LX/Jsw;)V

    .line 581351
    iput-object v2, p0, LX/Jsx;->h:Lcom/facebook/messengerwear/shared/Message$Attachment;

    .line 581352
    goto :goto_0

    .line 581353
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/messaging/model/messages/Message;LX/Jul;Lcom/facebook/messaging/model/threads/ThreadCustomization;ILjava/lang/CharSequence;LX/2HB;LX/3pw;)V
    .locals 9

    .prologue
    .line 581342
    new-instance v0, LX/Jun;

    move-object v1, p0

    move-object v2, p6

    move-object v3, p5

    move-object/from16 v4, p7

    move-object v5, p1

    move-object v6, p3

    move v7, p4

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, LX/Jun;-><init>(LX/3Re;LX/2HB;Ljava/lang/CharSequence;LX/3pw;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;ILX/Jul;)V

    .line 581343
    iget-object v1, p0, LX/3Re;->j:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    invoke-virtual {v1, p1}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/messages/Message;)LX/1ca;

    move-result-object v1

    .line 581344
    if-eqz v1, :cond_0

    .line 581345
    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 581346
    :goto_0
    return-void

    .line 581347
    :cond_0
    new-instance v0, LX/3pe;

    invoke-direct {v0}, LX/3pe;-><init>()V

    invoke-virtual {v0, p5}, LX/3pe;->b(Ljava/lang/CharSequence;)LX/3pe;

    move-result-object v0

    invoke-virtual {p6, v0}, LX/2HB;->a(LX/3pc;)LX/2HB;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move v3, p4

    move-object v4, p2

    move-object/from16 v5, p7

    .line 581348
    invoke-static/range {v0 .. v5}, LX/3Re;->a$redex0(LX/3Re;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;ILX/Jul;LX/3pw;)V

    goto :goto_0
.end method

.method private static a(LX/3Re;Lcom/facebook/user/model/UserKey;)Z
    .locals 1

    .prologue
    .line 581238
    iget-object v0, p0, LX/3Re;->v:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 581239
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/facebook/user/model/UserKey;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 581240
    if-eqz p0, :cond_0

    invoke-virtual {p1, p0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/3Re;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;ILX/Jul;LX/3pw;)V
    .locals 7

    .prologue
    .line 580974
    iget-object v0, p0, LX/3Re;->j:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    add-int/lit8 v2, p3, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v1

    .line 580975
    if-nez v1, :cond_5

    .line 580976
    iget-object v0, p4, LX/Jul;->a:Ljava/util/LinkedList;

    move-object v0, v0

    .line 580977
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 580978
    :goto_0
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 580979
    if-eqz v1, :cond_0

    .line 580980
    iget-boolean v2, v1, Lcom/facebook/messaging/model/messages/MessagesCollection;->c:Z

    move v1, v2

    .line 580981
    if-eqz v1, :cond_6

    :cond_0
    const/4 v1, 0x1

    .line 580982
    :goto_1
    const/4 p4, 0x1

    const/4 v4, 0x0

    .line 580983
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 580984
    if-nez v1, :cond_1

    .line 580985
    const-string v2, "..."

    invoke-virtual {v5, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 580986
    :cond_1
    invoke-virtual {v0}, LX/0Px;->reverse()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result p1

    move v3, v4

    :goto_2
    if-ge v3, p1, :cond_3

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/messages/Message;

    .line 580987
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p3

    if-lez p3, :cond_2

    .line 580988
    const-string p3, "\n\n"

    invoke-virtual {v5, p3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 580989
    :cond_2
    invoke-static {p0, v2, p2, p4, v4}, LX/3Re;->a(LX/3Re;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;ZZ)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 580990
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 580991
    :cond_3
    new-instance v2, LX/2HB;

    iget-object v3, p0, LX/3Re;->f:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/2HB;-><init>(Landroid/content/Context;)V

    new-instance v3, LX/3pe;

    invoke-direct {v3}, LX/3pe;-><init>()V

    invoke-virtual {v3, v5}, LX/3pe;->b(Ljava/lang/CharSequence;)LX/3pe;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2HB;->a(LX/3pc;)LX/2HB;

    move-result-object v2

    new-instance v3, LX/3pw;

    invoke-direct {v3}, LX/3pw;-><init>()V

    invoke-virtual {v3, p4}, LX/3pw;->a(Z)LX/3pw;

    move-result-object v3

    .line 580992
    invoke-interface {v3, v2}, LX/3pk;->a(LX/2HB;)LX/2HB;

    .line 580993
    move-object v2, v2

    .line 580994
    invoke-virtual {v2}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v2

    move-object v0, v2

    .line 580995
    invoke-virtual {p5, v0}, LX/3pw;->a(Landroid/app/Notification;)LX/3pw;

    .line 580996
    :cond_4
    return-void

    .line 580997
    :cond_5
    invoke-static {v1, p4}, LX/3Re;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;LX/Jul;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 580998
    :cond_6
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static b(LX/3Re;Lcom/facebook/messaging/model/threadkey/ThreadKey;)I
    .locals 3

    .prologue
    .line 580999
    invoke-static {p1}, LX/0db;->g(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/0Tn;

    move-result-object v0

    .line 581000
    iget-object v1, p0, LX/3Re;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    return v0
.end method

.method private static b(LX/0QB;)LX/3Re;
    .locals 28

    .prologue
    .line 581001
    new-instance v2, LX/3Re;

    invoke-static/range {p0 .. p0}, LX/0dF;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static/range {p0 .. p0}, LX/2Gv;->a(LX/0QB;)LX/2Gv;

    move-result-object v4

    check-cast v4, LX/2zj;

    const-class v5, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(LX/0QB;)Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    move-result-object v8

    check-cast v8, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    invoke-static/range {p0 .. p0}, LX/3Rf;->a(LX/0QB;)LX/3Rf;

    move-result-object v9

    check-cast v9, LX/3Rf;

    invoke-static/range {p0 .. p0}, LX/34G;->a(LX/0QB;)LX/34G;

    move-result-object v10

    check-cast v10, LX/34G;

    invoke-static/range {p0 .. p0}, LX/3CQ;->a(LX/0QB;)LX/3CQ;

    move-result-object v11

    check-cast v11, LX/3CQ;

    invoke-static/range {p0 .. p0}, LX/3Rh;->a(LX/0QB;)LX/3Rh;

    move-result-object v12

    check-cast v12, LX/3Rh;

    const/16 v13, 0x1504

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    const/16 v14, 0x1508

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    invoke-static/range {p0 .. p0}, LX/1sh;->a(LX/0QB;)Landroid/app/KeyguardManager;

    move-result-object v15

    check-cast v15, Landroid/app/KeyguardManager;

    invoke-static/range {p0 .. p0}, LX/0kZ;->a(LX/0QB;)Landroid/os/PowerManager;

    move-result-object v16

    check-cast v16, Landroid/os/PowerManager;

    invoke-static/range {p0 .. p0}, LX/15P;->a(LX/0QB;)LX/15P;

    move-result-object v17

    check-cast v17, LX/15P;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v18

    check-cast v18, LX/03V;

    invoke-static/range {p0 .. p0}, LX/3Ri;->a(LX/0QB;)LX/3Ri;

    move-result-object v19

    check-cast v19, LX/3Ri;

    const/16 v20, 0x12ce

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v20

    const/16 v21, 0x2a67

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v21

    const/16 v22, 0x2a0a

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v22

    const/16 v23, 0x2a42

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v23

    invoke-static/range {p0 .. p0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v24

    check-cast v24, Ljava/util/Random;

    invoke-static/range {p0 .. p0}, LX/3Rj;->a(LX/0QB;)LX/3Rj;

    move-result-object v25

    check-cast v25, LX/3Rj;

    invoke-static/range {p0 .. p0}, LX/3RE;->a(LX/0QB;)LX/3RE;

    move-result-object v26

    check-cast v26, LX/3RE;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v27

    check-cast v27, LX/0Uh;

    invoke-direct/range {v2 .. v27}, LX/3Re;-><init>(Ljava/lang/String;LX/2zj;Landroid/content/Context;Landroid/content/res/Resources;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;LX/3Rf;LX/34G;LX/3CQ;LX/3Rh;LX/0Or;LX/0Or;Landroid/app/KeyguardManager;Landroid/os/PowerManager;LX/15P;LX/03V;LX/3Ri;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/util/Random;LX/3Rj;LX/3RE;LX/0Uh;)V

    .line 581002
    const/16 v3, 0x455

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0xcd6

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x265c

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, LX/3Re;->a(LX/3Re;LX/0Or;LX/0Or;LX/0Or;)V

    .line 581003
    return-object v2
.end method

.method private b(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;LX/Jul;Lcom/facebook/messaging/model/threads/ThreadCustomization;I)LX/3pj;
    .locals 12

    .prologue
    .line 581004
    new-instance v6, LX/3ph;

    invoke-direct {v6, p2}, LX/3ph;-><init>(Ljava/lang/String;)V

    .line 581005
    move/from16 v0, p5

    invoke-static {p0, p1, p3, v0}, LX/3Re;->a(LX/3Re;Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/Jul;I)Ljava/util/List;

    move-result-object v7

    .line 581006
    const/4 v4, 0x0

    .line 581007
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v5, v1

    :goto_0
    if-ltz v5, :cond_1

    .line 581008
    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/messages/Message;

    .line 581009
    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-static {p0, v1, v0, v2}, LX/3Re;->a(LX/3Re;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;Z)Ljava/lang/String;

    move-result-object v2

    .line 581010
    iget-object v3, p0, LX/3Re;->j:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    invoke-virtual {v3, v1}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->b(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v3

    .line 581011
    iget-object v8, p0, LX/3Re;->k:LX/3Rf;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v8, v3, v1}, LX/3Rf;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/lang/String;

    move-result-object v1

    .line 581012
    invoke-static {v1, v2}, LX/3Re;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 581013
    iget-object v3, v3, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    .line 581014
    invoke-virtual {v3, v4}, Lcom/facebook/user/model/UserKey;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 581015
    iget-object v4, p0, LX/3Re;->g:Landroid/content/res/Resources;

    const v8, 0x7f08021c

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v1, v9, v10

    const/4 v1, 0x1

    aput-object v2, v9, v1

    invoke-virtual {v4, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v3

    .line 581016
    :goto_1
    invoke-virtual {v6, v1}, LX/3ph;->a(Ljava/lang/String;)LX/3ph;

    .line 581017
    add-int/lit8 v1, v5, -0x1

    move v5, v1

    move-object v4, v2

    goto :goto_0

    .line 581018
    :cond_0
    const/4 v1, 0x0

    move-object v11, v2

    move-object v2, v1

    move-object v1, v11

    goto :goto_1

    .line 581019
    :cond_1
    invoke-static {p0, v6, p3, p1}, LX/3Re;->a(LX/3Re;LX/3ph;LX/Jul;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 581020
    invoke-virtual {v6}, LX/3ph;->a()LX/3pj;

    move-result-object v1

    return-object v1

    :cond_2
    move-object v1, v2

    move-object v2, v4

    goto :goto_1
.end method

.method private static b(LX/3Re;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 581021
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/3Re;->f:Landroid/content/Context;

    const-class v2, Lcom/facebook/messengerwear/support/MessageNotificationDeleteHandlerService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 581022
    const-string v1, "thread_key"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 581023
    return-object v0
.end method

.method private b(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 581235
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-static {p0, v0}, LX/3Re;->a(LX/3Re;Lcom/facebook/user/model/UserKey;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 581236
    :cond_0
    const/4 v0, 0x0

    .line 581237
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/3Re;->k:LX/3Rf;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1, v2}, LX/3Rf;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b()Z
    .locals 4

    .prologue
    .line 581024
    iget-object v0, p0, LX/3Re;->n:LX/3Rh;

    .line 581025
    iget-object v1, v0, LX/3Rh;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0db;->aB:LX/0Tn;

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 581026
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3Re;->u:LX/3Ri;

    invoke-virtual {v0}, LX/3Ri;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private b(Lcom/facebook/messaging/notify/NewMessageNotification;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 581027
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v0, v3, :cond_1

    iget-object v0, p0, LX/3Re;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3Re;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3Re;->s:LX/15P;

    .line 581028
    iget-object v3, v0, LX/15P;->b:Landroid/app/Activity;

    move-object v0, v3

    .line 581029
    if-nez v0, :cond_1

    .line 581030
    iget-object v0, p0, LX/3Re;->q:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3Re;->r:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-nez v0, :cond_6

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 581031
    if-nez v0, :cond_1

    move v0, v2

    .line 581032
    :goto_1
    if-nez v0, :cond_2

    move v0, v1

    .line 581033
    :goto_2
    return v0

    :cond_1
    move v0, v1

    .line 581034
    goto :goto_1

    .line 581035
    :cond_2
    iget-boolean v0, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->h:Z

    move v0, v0

    .line 581036
    if-nez v0, :cond_3

    iget-object v0, p0, LX/3Re;->C:LX/0Uh;

    const/16 v3, 0x1e5

    invoke-virtual {v0, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/3Re;->B:LX/3RE;

    iget-object v3, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v3}, LX/3RE;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    move v0, v2

    .line 581037
    :goto_3
    if-nez v0, :cond_5

    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v1

    .line 581038
    goto :goto_3

    :cond_5
    move v0, v1

    .line 581039
    goto :goto_2

    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 12
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 581040
    iget-object v0, p0, LX/3Re;->D:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 581041
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 581042
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 581043
    invoke-static {p0, v0}, LX/3Re;->b(LX/3Re;Lcom/facebook/messaging/model/threadkey/ThreadKey;)I

    move-result v0

    if-nez v0, :cond_0

    .line 581044
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 581045
    :cond_1
    iget-object v0, p0, LX/3Re;->D:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 581046
    :cond_2
    iget-object v0, p0, LX/3Re;->D:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v1, 0x5

    if-lt v0, v1, :cond_4

    .line 581047
    iget-object v0, p0, LX/3Re;->D:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v2, v4

    move-object v3, v4

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 581048
    iget-object v1, p0, LX/3Re;->D:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jul;

    .line 581049
    if-eqz v3, :cond_3

    .line 581050
    iget-wide v10, v3, LX/Jul;->b:J

    move-wide v6, v10

    .line 581051
    iget-wide v10, v1, LX/Jul;->b:J

    move-wide v8, v10

    .line 581052
    cmp-long v6, v6, v8

    if-lez v6, :cond_5

    :cond_3
    :goto_2
    move-object v2, v0

    move-object v3, v1

    .line 581053
    goto :goto_1

    .line 581054
    :cond_4
    return-void

    :cond_5
    move-object v0, v2

    move-object v1, v3

    goto :goto_2
.end method

.method private static c(LX/3Re;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    .locals 1

    .prologue
    .line 581055
    invoke-static {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 581056
    const/4 v0, 0x0

    .line 581057
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/3Re;->h:LX/2zj;

    invoke-interface {v0}, LX/2zj;->b()Z

    move-result v0

    goto :goto_0
.end method

.method private d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/Jul;
    .locals 2
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 581058
    iget-object v0, p0, LX/3Re;->D:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jul;

    .line 581059
    if-nez v0, :cond_0

    .line 581060
    invoke-direct {p0}, LX/3Re;->c()V

    .line 581061
    new-instance v0, LX/Jul;

    invoke-direct {v0}, LX/Jul;-><init>()V

    .line 581062
    iget-object v1, p0, LX/3Re;->D:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 581063
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/notify/NewMessageNotification;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 581064
    iget-object v0, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 581065
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p0, v1}, LX/3Re;->c(LX/3Re;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/3Re;->h:LX/2zj;

    invoke-interface {v1}, LX/2zj;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 581066
    iget-object v0, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->a:Ljava/lang/String;

    .line 581067
    :goto_0
    iget-object v1, p0, LX/3Re;->m:LX/3CQ;

    .line 581068
    iget-object v2, v1, LX/3CQ;->a:Landroid/content/Context;

    const/high16 v3, 0x437a0000    # 250.0f

    invoke-static {v2, v3}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    .line 581069
    if-nez v0, :cond_1

    .line 581070
    :goto_1
    move-object v2, v0

    .line 581071
    move-object v0, v2

    .line 581072
    return-object v0

    .line 581073
    :cond_0
    iget-object v1, p0, LX/3Re;->g:Landroid/content/res/Resources;

    const v2, 0x7f0804a8

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->c:Ljava/lang/String;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v3, v1, LX/3CQ;->b:Landroid/text/TextPaint;

    int-to-float v4, v2

    sget-object p0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v0, v3, v4, p0}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(LX/2HB;Lcom/facebook/messaging/notify/NewMessageNotification;LX/3pw;LX/3pl;Landroid/graphics/Bitmap;)V
    .locals 15
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 581074
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 581075
    iget-object v2, p0, LX/3Re;->j:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    iget-object v4, v3, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2, v4}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v10

    .line 581076
    iget-object v2, p0, LX/3Re;->j:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    invoke-virtual {v2, v3, v10}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadSummary;)Ljava/lang/String;

    move-result-object v14

    .line 581077
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p0, v2}, LX/3Re;->b(LX/3Re;Lcom/facebook/messaging/model/threadkey/ThreadKey;)I

    move-result v6

    .line 581078
    monitor-enter p0

    .line 581079
    :try_start_0
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-direct {p0, v2}, LX/3Re;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/Jul;

    move-result-object v4

    .line 581080
    invoke-virtual {v4, v3}, LX/Jul;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 581081
    :goto_0
    invoke-virtual {v4}, LX/Jul;->b()I

    move-result v2

    if-le v2, v6, :cond_0

    .line 581082
    invoke-virtual {v4}, LX/Jul;->c()V

    goto :goto_0

    .line 581083
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 581084
    :cond_0
    if-eqz v10, :cond_6

    :try_start_1
    iget-object v5, v10, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 581085
    :goto_1
    iget-object v2, v3, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p0, v2}, LX/3Re;->c(LX/3Re;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 581086
    iget-object v2, p0, LX/3Re;->g:Landroid/content/res/Resources;

    const v7, 0x7f0f001f

    invoke-virtual {v4}, LX/Jul;->b()I

    move-result v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {v4}, LX/Jul;->b()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v9, v11

    invoke-virtual {v2, v7, v8, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    move-object v13, v7

    .line 581087
    :goto_2
    iget-object v2, p0, LX/3Re;->C:LX/0Uh;

    const/16 v7, 0x102

    const/4 v8, 0x0

    invoke-virtual {v2, v7, v8}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 581088
    const/4 v2, 0x0

    .line 581089
    if-eqz v10, :cond_1

    iget-object v7, v10, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v7

    const/4 v8, 0x3

    if-ge v7, v8, :cond_c

    .line 581090
    :cond_1
    move-object/from16 v0, p2

    iget-object v8, v0, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v7, p0

    move-object v9, v14

    move-object v10, v4

    move-object v11, v5

    move v12, v6

    invoke-direct/range {v7 .. v12}, LX/3Re;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;LX/Jul;Lcom/facebook/messaging/model/threads/ThreadCustomization;I)LX/3pj;

    move-result-object v2

    .line 581091
    :cond_2
    :goto_3
    if-eqz v2, :cond_3

    .line 581092
    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, LX/3pl;->a(LX/3pj;)LX/3pl;

    .line 581093
    iget v2, v5, Lcom/facebook/messaging/model/threads/ThreadCustomization;->e:I

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, LX/3pl;->a(I)LX/3pl;

    .line 581094
    invoke-virtual/range {p4 .. p5}, LX/3pl;->a(Landroid/graphics/Bitmap;)LX/3pl;

    .line 581095
    :cond_3
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 581096
    const/4 v2, 0x1

    if-le v6, v2, :cond_4

    .line 581097
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/2HB;->b(I)LX/2HB;

    .line 581098
    :cond_4
    if-eqz p5, :cond_5

    .line 581099
    invoke-virtual/range {p5 .. p5}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 581100
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v4, "Attempt to set a recycled bitmap as a notification icon"

    invoke-direct {v2, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 581101
    iget-object v4, p0, LX/3Re;->t:LX/03V;

    const-string v5, "MessagingNotificationWithRecycledBitmap"

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 581102
    :cond_5
    :goto_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    .line 581103
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    .line 581104
    iget-wide v2, v3, Lcom/facebook/messaging/model/messages/Message;->c:J

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/2HB;->a(J)LX/2HB;

    .line 581105
    const-string v2, "msg"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/2HB;->a(Ljava/lang/String;)LX/2HB;

    .line 581106
    iget-object v2, p0, LX/3Re;->C:LX/0Uh;

    const/16 v3, 0x12c

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_e

    const/4 v2, 0x0

    .line 581107
    :goto_5
    move-object/from16 v0, p2

    invoke-direct {p0, v0}, LX/3Re;->b(Lcom/facebook/messaging/notify/NewMessageNotification;)Z

    move-result v3

    if-eqz v3, :cond_f

    :goto_6
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/2HB;->d(I)LX/2HB;

    .line 581108
    return-void

    .line 581109
    :cond_6
    :try_start_2
    sget-object v5, Lcom/facebook/messaging/model/threads/ThreadCustomization;->a:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    goto/16 :goto_1

    .line 581110
    :cond_7
    iget-object v2, p0, LX/3Re;->h:LX/2zj;

    invoke-interface {v2}, LX/2zj;->c()Z

    move-result v2

    if-nez v2, :cond_9

    const/4 v2, 0x1

    .line 581111
    :goto_7
    const/4 v7, 0x0

    invoke-static {p0, v3, v5, v7, v2}, LX/3Re;->a(LX/3Re;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;ZZ)Ljava/lang/CharSequence;

    move-result-object v7

    .line 581112
    if-nez v2, :cond_8

    invoke-direct {p0}, LX/3Re;->b()Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_8
    move-object v2, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p3

    .line 581113
    invoke-direct/range {v2 .. v9}, LX/3Re;->a(Lcom/facebook/messaging/model/messages/Message;LX/Jul;Lcom/facebook/messaging/model/threads/ThreadCustomization;ILjava/lang/CharSequence;LX/2HB;LX/3pw;)V

    move-object v13, v7

    goto/16 :goto_2

    .line 581114
    :cond_9
    const/4 v2, 0x0

    goto :goto_7

    .line 581115
    :cond_a
    const/4 v2, 0x1

    if-le v6, v2, :cond_b

    .line 581116
    invoke-direct {p0, v14, v6, v4, v5}, LX/3Re;->a(Ljava/lang/String;ILX/Jul;Lcom/facebook/messaging/model/threads/ThreadCustomization;)LX/3pm;

    move-result-object v2

    .line 581117
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/2HB;->a(LX/3pc;)LX/2HB;

    move-object v13, v7

    .line 581118
    goto/16 :goto_2

    .line 581119
    :cond_b
    new-instance v2, LX/3pe;

    invoke-direct {v2}, LX/3pe;-><init>()V

    invoke-virtual {v2, v7}, LX/3pe;->b(Ljava/lang/CharSequence;)LX/3pe;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/2HB;->a(LX/3pc;)LX/2HB;

    move-object v13, v7

    goto/16 :goto_2

    .line 581120
    :cond_c
    iget-object v7, p0, LX/3Re;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v8, LX/0db;->O:LX/0Tn;

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 581121
    move-object/from16 v0, p2

    iget-object v8, v0, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v7, p0

    move-object v9, v14

    move-object v10, v4

    move-object v11, v5

    move v12, v6

    invoke-direct/range {v7 .. v12}, LX/3Re;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;LX/Jul;Lcom/facebook/messaging/model/threads/ThreadCustomization;I)LX/3pj;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto/16 :goto_3

    .line 581122
    :cond_d
    move-object/from16 v0, p1

    move-object/from16 v1, p5

    invoke-virtual {v0, v1}, LX/2HB;->a(Landroid/graphics/Bitmap;)LX/2HB;

    goto/16 :goto_4

    .line 581123
    :cond_e
    const/4 v2, -0x1

    goto :goto_5

    .line 581124
    :cond_f
    const/4 v2, 0x1

    goto :goto_6
.end method

.method public final a(Lcom/facebook/messaging/notify/NewMessageNotification;LX/2HB;LX/JuS;)V
    .locals 9

    .prologue
    const/4 v0, 0x1

    .line 581125
    new-instance v5, LX/3pw;

    invoke-direct {v5}, LX/3pw;-><init>()V

    .line 581126
    new-instance v6, LX/3pl;

    invoke-direct {v6}, LX/3pl;-><init>()V

    .line 581127
    iget-object v1, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v1, v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v2, LX/5e9;->SMS:LX/5e9;

    if-ne v1, v2, :cond_1

    move v2, v0

    .line 581128
    :goto_0
    if-eqz v2, :cond_2

    .line 581129
    const v1, 0x7f02130a

    invoke-virtual {p2, v1}, LX/2HB;->a(I)LX/2HB;

    move-result-object v1

    iget-object v3, p0, LX/3Re;->f:Landroid/content/Context;

    const v4, 0x7f0a01a6

    invoke-static {v3, v4}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v3

    .line 581130
    iput v3, v1, LX/2HB;->y:I

    .line 581131
    :goto_1
    if-nez v2, :cond_0

    iget-object v1, p0, LX/3Re;->u:LX/3Ri;

    invoke-virtual {v1}, LX/3Ri;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 581132
    iput-boolean v0, p2, LX/2HB;->v:Z

    .line 581133
    iget-object v0, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-eqz v0, :cond_0

    .line 581134
    iget-object v0, p0, LX/3Re;->f:Landroid/content/Context;

    iget-object v1, p0, LX/3Re;->z:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextInt()I

    move-result v1

    iget-object v3, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, LX/3Re;->b(LX/3Re;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v0, v1, v3, v4}, LX/0nt;->c(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 581135
    invoke-virtual {p2, v0}, LX/2HB;->b(Landroid/app/PendingIntent;)LX/2HB;

    .line 581136
    :cond_0
    iget-object v8, p0, LX/3Re;->j:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    new-instance v0, LX/Jum;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, LX/Jum;-><init>(LX/3Re;ZLcom/facebook/messaging/notify/NewMessageNotification;LX/2HB;LX/3pw;LX/3pl;LX/JuS;)V

    invoke-virtual {v8, p1, v0}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/notify/NewMessageNotification;LX/FJf;)V

    .line 581137
    return-void

    .line 581138
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 581139
    :cond_2
    iget-object v1, p0, LX/3Re;->h:LX/2zj;

    invoke-interface {v1}, LX/2zj;->g()I

    move-result v1

    invoke-virtual {p2, v1}, LX/2HB;->a(I)LX/2HB;

    move-result-object v1

    iget-object v3, p0, LX/3Re;->f:Landroid/content/Context;

    const v4, 0x7f0a019a

    invoke-static {v3, v4}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v3

    .line 581140
    iput v3, v1, LX/2HB;->y:I

    .line 581141
    goto :goto_1
.end method

.method public final a(Lcom/facebook/messaging/notify/NewMessageNotification;Landroid/graphics/Bitmap;)V
    .locals 18

    .prologue
    .line 581142
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 581143
    iget-object v2, v3, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-nez v2, :cond_0

    .line 581144
    :goto_0
    return-void

    .line 581145
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Re;->j:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    iget-object v4, v3, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2, v4}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v2

    .line 581146
    move-object/from16 v0, p0

    iget-object v4, v0, LX/3Re;->j:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    invoke-virtual {v4, v3, v2}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadSummary;)Ljava/lang/String;

    move-result-object v4

    .line 581147
    iget-object v5, v3, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/3Re;->b(LX/3Re;Lcom/facebook/messaging/model/threadkey/ThreadKey;)I

    move-result v5

    .line 581148
    new-instance v10, LX/Jt4;

    invoke-direct {v10}, LX/Jt4;-><init>()V

    .line 581149
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 581150
    monitor-enter p0

    .line 581151
    :try_start_0
    new-instance v6, LX/Jsx;

    sget-object v7, LX/Jsz;->REGULAR:LX/Jsz;

    invoke-direct {v6, v7}, LX/Jsx;-><init>(LX/Jsz;)V

    .line 581152
    iget-object v7, v3, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/3Re;->c(LX/3Re;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 581153
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Re;->g:Landroid/content/res/Resources;

    const v7, 0x7f0f001f

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v8, v9

    invoke-virtual {v2, v7, v5, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, LX/Jsx;->a(Ljava/lang/String;)LX/Jsx;

    .line 581154
    :goto_1
    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/3Re;->a(LX/3Re;Lcom/facebook/messaging/model/messages/Message;)LX/0Px;

    move-result-object v2

    invoke-static {v6, v2}, LX/3Re;->a(LX/Jsx;Ljava/util/List;)V

    .line 581155
    iget-wide v8, v3, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-virtual {v6, v8, v9}, LX/Jsx;->a(J)LX/Jsx;

    move-result-object v2

    iget-object v7, v3, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v7, v7, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/3Re;->a(LX/3Re;Lcom/facebook/user/model/UserKey;)Z

    move-result v7

    invoke-virtual {v2, v7}, LX/Jsx;->a(Z)LX/Jsx;

    .line 581156
    iget-object v2, v3, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, LX/Jt4;->a(Ljava/lang/String;)LX/Jt4;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/messaging/notify/NewMessageNotification;->a()I

    move-result v7

    invoke-virtual {v2, v7}, LX/Jt4;->a(I)LX/Jt4;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/Jt4;->b(Ljava/lang/String;)LX/Jt4;

    move-result-object v2

    invoke-virtual {v6}, LX/Jsx;->a()Lcom/facebook/messengerwear/shared/Message;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/Jt4;->a(Lcom/facebook/messengerwear/shared/Message;)LX/Jt4;

    .line 581157
    iget-object v2, v3, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/3Re;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 581158
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Re;->A:LX/3Rj;

    iget-object v4, v3, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2, v4}, LX/3Rj;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    move-result-object v2

    .line 581159
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 581160
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Jui;

    .line 581161
    iget-object v2, v2, LX/Jui;->b:Ljava/lang/String;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 581162
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 581163
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v7, v0, LX/3Re;->h:LX/2zj;

    invoke-interface {v7}, LX/2zj;->c()Z

    move-result v7

    if-nez v7, :cond_3

    .line 581164
    if-eqz v2, :cond_2

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 581165
    :goto_3
    move-object/from16 v0, p0

    iget-object v7, v0, LX/3Re;->l:LX/34G;

    invoke-virtual {v7, v3, v2}, LX/34G;->c(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, LX/Jsx;->a(Ljava/lang/String;)LX/Jsx;

    goto/16 :goto_1

    .line 581166
    :cond_2
    sget-object v2, Lcom/facebook/messaging/model/threads/ThreadCustomization;->a:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    goto :goto_3

    .line 581167
    :cond_3
    iget-object v2, v3, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    invoke-virtual {v6, v2}, LX/Jsx;->a(Ljava/lang/String;)LX/Jsx;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, LX/3Re;->b(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, LX/Jsx;->b(Ljava/lang/String;)LX/Jsx;

    move-result-object v2

    iget-object v7, v3, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/Jsx;->c(Ljava/lang/String;)LX/Jsx;

    goto/16 :goto_1

    .line 581168
    :cond_4
    invoke-virtual {v10, v4}, LX/Jt4;->a(Ljava/util/List;)LX/Jt4;

    .line 581169
    :cond_5
    iget-object v2, v3, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, LX/3Re;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/Jul;

    move-result-object v2

    .line 581170
    iget-object v3, v3, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v2}, LX/3Re;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;ILX/Jul;)LX/0Px;

    move-result-object v12

    .line 581171
    const/4 v3, 0x1

    .line 581172
    const/4 v2, 0x0

    move v9, v2

    move v6, v3

    :goto_4
    invoke-virtual {v12}, LX/0Px;->size()I

    move-result v2

    if-ge v9, v2, :cond_10

    .line 581173
    if-eqz v6, :cond_9

    const/4 v2, 0x0

    move-object v5, v2

    .line 581174
    :goto_5
    invoke-virtual {v12, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/messages/Message;

    .line 581175
    add-int/lit8 v3, v9, 0x1

    invoke-virtual {v12}, LX/0Px;->size()I

    move-result v4

    if-ge v3, v4, :cond_a

    add-int/lit8 v3, v9, 0x1

    invoke-virtual {v12, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/model/messages/Message;

    move-object v4, v3

    .line 581176
    :goto_6
    move-object/from16 v0, p0

    iget-object v3, v0, LX/3Re;->x:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Js0;

    invoke-static {v2, v6, v5}, LX/Js0;->a(Lcom/facebook/messaging/model/messages/Message;ZLcom/facebook/messaging/model/messages/Message;)Z

    move-result v3

    .line 581177
    if-eqz v3, :cond_6

    .line 581178
    new-instance v3, LX/Jsx;

    sget-object v7, LX/Jsz;->TIME_DIVIDER:LX/Jsz;

    invoke-direct {v3, v7}, LX/Jsx;-><init>(LX/Jsz;)V

    iget-wide v14, v2, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-virtual {v3, v14, v15}, LX/Jsx;->a(J)LX/Jsx;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v3, v0, LX/3Re;->y:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/6lN;

    invoke-static {v2}, LX/2Mk;->b(Lcom/facebook/messaging/model/messages/Message;)J

    move-result-wide v14

    invoke-virtual {v3, v14, v15}, LX/6lN;->b(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, LX/Jsx;->a(Ljava/lang/String;)LX/Jsx;

    move-result-object v3

    invoke-virtual {v3}, LX/Jsx;->a()Lcom/facebook/messengerwear/shared/Message;

    move-result-object v3

    invoke-virtual {v10, v3}, LX/Jt4;->b(Lcom/facebook/messengerwear/shared/Message;)LX/Jt4;

    .line 581179
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, LX/3Re;->x:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Js0;

    invoke-virtual {v3, v2, v6, v5, v4}, LX/Js0;->a(Lcom/facebook/messaging/model/messages/Message;ZLcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/Message;)LX/Jrz;

    move-result-object v5

    .line 581180
    iget-object v3, v2, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    invoke-static {v3}, LX/3Re;->a(LX/2uW;)LX/Jsz;

    move-result-object v4

    .line 581181
    const/4 v3, 0x0

    .line 581182
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, LX/3Re;->b(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v6

    .line 581183
    sget-object v7, LX/Jsz;->REGULAR:LX/Jsz;

    if-ne v4, v7, :cond_14

    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_14

    iget-boolean v7, v5, LX/Jrz;->groupWithOlderRow:Z

    if-nez v7, :cond_14

    .line 581184
    new-instance v3, LX/Jsx;

    sget-object v7, LX/Jsz;->SENDER_NAME:LX/Jsz;

    invoke-direct {v3, v7}, LX/Jsx;-><init>(LX/Jsz;)V

    iget-wide v14, v2, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-virtual {v3, v14, v15}, LX/Jsx;->a(J)LX/Jsx;

    move-result-object v3

    invoke-virtual {v3, v6}, LX/Jsx;->a(Ljava/lang/String;)LX/Jsx;

    move-result-object v3

    invoke-virtual {v3}, LX/Jsx;->a()Lcom/facebook/messengerwear/shared/Message;

    move-result-object v3

    invoke-virtual {v10, v3}, LX/Jt4;->b(Lcom/facebook/messengerwear/shared/Message;)LX/Jt4;

    .line 581185
    const/4 v3, 0x1

    move v8, v3

    .line 581186
    :goto_7
    iget-boolean v3, v5, LX/Jrz;->groupWithNewerRow:Z

    iget-boolean v6, v5, LX/Jrz;->groupWithOlderRow:Z

    invoke-static {v3, v6, v8}, LX/Jsy;->forGrouping(ZZZ)LX/Jsy;

    move-result-object v6

    .line 581187
    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3Re;->a(LX/3Re;Lcom/facebook/messaging/model/messages/Message;)LX/0Px;

    move-result-object v13

    .line 581188
    invoke-interface {v11, v13}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 581189
    iget-object v3, v2, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    .line 581190
    sget-object v7, LX/Jsz;->REGULAR:LX/Jsz;

    if-ne v4, v7, :cond_13

    iget-object v7, v2, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    if-nez v7, :cond_13

    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_13

    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_13

    .line 581191
    move-object/from16 v0, p0

    iget-object v3, v0, LX/3Re;->l:LX/34G;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, LX/34G;->b(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v3

    .line 581192
    sget-object v4, LX/Jsz;->SNIPPET:LX/Jsz;

    move-object v7, v4

    .line 581193
    :goto_8
    sget-object v4, LX/Jsz;->REGULAR:LX/Jsz;

    if-ne v7, v4, :cond_e

    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_e

    .line 581194
    const/4 v4, 0x0

    .line 581195
    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_7

    .line 581196
    new-instance v4, LX/Jsx;

    invoke-direct {v4, v7}, LX/Jsx;-><init>(LX/Jsz;)V

    iget-wide v14, v2, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-virtual {v4, v14, v15}, LX/Jsx;->a(J)LX/Jsx;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/Jsx;->a(Ljava/lang/String;)LX/Jsx;

    move-result-object v3

    iget-object v4, v2, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v4, v4, Lcom/facebook/messaging/model/messages/ParticipantInfo;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/Jsx;->b(Ljava/lang/String;)LX/Jsx;

    move-result-object v3

    iget-object v4, v2, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v4, v4, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/3Re;->a(LX/3Re;Lcom/facebook/user/model/UserKey;)Z

    move-result v4

    invoke-virtual {v3, v4}, LX/Jsx;->a(Z)LX/Jsx;

    move-result-object v3

    const/4 v4, 0x1

    iget-boolean v5, v5, LX/Jrz;->groupWithOlderRow:Z

    invoke-static {v4, v5, v8}, LX/Jsy;->forGrouping(ZZZ)LX/Jsy;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/Jsx;->a(LX/Jsy;)LX/Jsx;

    move-result-object v3

    .line 581197
    invoke-virtual {v3}, LX/Jsx;->a()Lcom/facebook/messengerwear/shared/Message;

    move-result-object v3

    invoke-virtual {v10, v3}, LX/Jt4;->b(Lcom/facebook/messengerwear/shared/Message;)LX/Jt4;

    .line 581198
    const/4 v4, 0x1

    .line 581199
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, LX/3Re;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/CJK;

    .line 581200
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    move v5, v4

    :goto_9
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_f

    .line 581201
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/attachments/ImageAttachmentData;

    .line 581202
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-nez v14, :cond_b

    if-nez v5, :cond_b

    move-object v5, v6

    .line 581203
    :goto_a
    new-instance v14, LX/Jsx;

    invoke-direct {v14, v7}, LX/Jsx;-><init>(LX/Jsz;)V

    iget-wide v0, v2, Lcom/facebook/messaging/model/messages/Message;->c:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, LX/Jsx;->a(J)LX/Jsx;

    move-result-object v14

    iget-object v15, v2, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v15, v15, Lcom/facebook/messaging/model/messages/ParticipantInfo;->c:Ljava/lang/String;

    invoke-virtual {v14, v15}, LX/Jsx;->b(Ljava/lang/String;)LX/Jsx;

    move-result-object v14

    iget-object v15, v2, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v15, v15, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/3Re;->a(LX/3Re;Lcom/facebook/user/model/UserKey;)Z

    move-result v15

    invoke-virtual {v14, v15}, LX/Jsx;->a(Z)LX/Jsx;

    move-result-object v14

    iget-object v15, v2, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-virtual {v14, v15}, LX/Jsx;->c(Ljava/lang/String;)LX/Jsx;

    move-result-object v14

    invoke-virtual {v14, v5}, LX/Jsx;->a(LX/Jsy;)LX/Jsx;

    move-result-object v5

    new-instance v14, Lcom/facebook/messengerwear/shared/Message$Attachment;

    iget-object v4, v4, Lcom/facebook/messaging/attachments/ImageAttachmentData;->e:Ljava/lang/String;

    sget-object v15, LX/Jsw;->PHOTO:LX/Jsw;

    invoke-direct {v14, v4, v15}, Lcom/facebook/messengerwear/shared/Message$Attachment;-><init>(Ljava/lang/String;LX/Jsw;)V

    invoke-virtual {v5, v14}, LX/Jsx;->a(Lcom/facebook/messengerwear/shared/Message$Attachment;)LX/Jsx;

    move-result-object v4

    .line 581204
    invoke-virtual {v3, v2}, LX/CJK;->a(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 581205
    iget-object v5, v2, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    iget-object v5, v5, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/Jsx;->d(Ljava/lang/String;)LX/Jsx;

    .line 581206
    :cond_8
    invoke-virtual {v4}, LX/Jsx;->a()Lcom/facebook/messengerwear/shared/Message;

    move-result-object v4

    invoke-virtual {v10, v4}, LX/Jt4;->b(Lcom/facebook/messengerwear/shared/Message;)LX/Jt4;

    .line 581207
    const/4 v4, 0x1

    move v5, v4

    .line 581208
    goto :goto_9

    .line 581209
    :cond_9
    add-int/lit8 v2, v9, -0x1

    invoke-virtual {v12, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/messages/Message;

    move-object v5, v2

    goto/16 :goto_5

    .line 581210
    :cond_a
    const/4 v3, 0x0

    move-object v4, v3

    goto/16 :goto_6

    .line 581211
    :cond_b
    if-nez v5, :cond_c

    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_c

    .line 581212
    const/4 v5, 0x1

    iget-boolean v14, v6, LX/Jsy;->groupWithOlderRow:Z

    invoke-static {v5, v14, v8}, LX/Jsy;->forGrouping(ZZZ)LX/Jsy;

    move-result-object v5

    goto :goto_a

    .line 581213
    :cond_c
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_d

    .line 581214
    iget-boolean v5, v6, LX/Jsy;->groupWithNewerRow:Z

    invoke-static {v5}, LX/Jsy;->forBottomImageAttachment(Z)LX/Jsy;

    move-result-object v5

    goto :goto_a

    .line 581215
    :cond_d
    invoke-static {}, LX/Jsy;->forMiddleImageAttachment()LX/Jsy;

    move-result-object v5

    goto/16 :goto_a

    .line 581216
    :cond_e
    new-instance v4, LX/Jsx;

    invoke-direct {v4, v7}, LX/Jsx;-><init>(LX/Jsz;)V

    iget-wide v14, v2, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-virtual {v4, v14, v15}, LX/Jsx;->a(J)LX/Jsx;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/Jsx;->a(Ljava/lang/String;)LX/Jsx;

    move-result-object v3

    iget-object v4, v2, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v4, v4, Lcom/facebook/messaging/model/messages/ParticipantInfo;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/Jsx;->b(Ljava/lang/String;)LX/Jsx;

    move-result-object v3

    iget-object v4, v2, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v4, v4, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/3Re;->a(LX/3Re;Lcom/facebook/user/model/UserKey;)Z

    move-result v4

    invoke-virtual {v3, v4}, LX/Jsx;->a(Z)LX/Jsx;

    move-result-object v3

    iget-object v2, v2, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-virtual {v3, v2}, LX/Jsx;->c(Ljava/lang/String;)LX/Jsx;

    move-result-object v2

    invoke-virtual {v2, v6}, LX/Jsx;->a(LX/Jsy;)LX/Jsx;

    move-result-object v2

    .line 581217
    invoke-virtual {v2}, LX/Jsx;->a()Lcom/facebook/messengerwear/shared/Message;

    move-result-object v2

    invoke-virtual {v10, v2}, LX/Jt4;->b(Lcom/facebook/messengerwear/shared/Message;)LX/Jt4;

    .line 581218
    :cond_f
    const/4 v3, 0x0

    .line 581219
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    move v6, v3

    goto/16 :goto_4

    .line 581220
    :cond_10
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 581221
    const/4 v2, 0x0

    .line 581222
    if-eqz p2, :cond_11

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-eqz v3, :cond_11

    .line 581223
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Attempt to set a recycled bitmap as a notification icon"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 581224
    move-object/from16 v0, p0

    iget-object v4, v0, LX/3Re;->t:LX/03V;

    const-string v5, "MessagingNotificationWithRecycledBitmap"

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v3, v2

    .line 581225
    :goto_b
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Re;->w:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/JtE;

    invoke-virtual {v10}, LX/Jt4;->a()Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;

    move-result-object v4

    invoke-static {v11}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v5

    invoke-virtual {v2, v4, v3, v5}, LX/JtE;->a(Lcom/facebook/messengerwear/shared/MessengerWearThreadNotification;[BLX/0Px;)V

    goto/16 :goto_0

    .line 581226
    :cond_11
    if-eqz p2, :cond_12

    .line 581227
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 581228
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 581229
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    move-object v3, v2

    goto :goto_b

    :cond_12
    move-object v3, v2

    goto :goto_b

    :cond_13
    move-object v7, v4

    goto/16 :goto_8

    :cond_14
    move v8, v3

    goto/16 :goto_7
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 581230
    iget-object v0, p0, LX/3Re;->u:LX/3Ri;

    invoke-virtual {v0}, LX/3Ri;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 581231
    :goto_0
    return-void

    .line 581232
    :cond_0
    iget-object v0, p0, LX/3Re;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    .line 581233
    invoke-static {p0, p1}, LX/3Re;->b(LX/3Re;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, LX/3Re;->f:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->c(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    .locals 2

    .prologue
    .line 581234
    invoke-static {p0, p1}, LX/3Re;->b(LX/3Re;Lcom/facebook/messaging/model/threadkey/ThreadKey;)I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
