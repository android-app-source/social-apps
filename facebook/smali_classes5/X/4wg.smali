.class public abstract LX/4wg;
.super Ljava/lang/Number;
.source ""


# static fields
.field public static final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<[I>;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/Random;

.field public static final c:I

.field private static final g:Lsun/misc/Unsafe;

.field private static final h:J

.field private static final i:J


# instance fields
.field public volatile transient d:[LX/4wk;

.field public volatile transient e:J

.field public volatile transient f:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 820266
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, LX/4wg;->a:Ljava/lang/ThreadLocal;

    .line 820267
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, LX/4wg;->b:Ljava/util/Random;

    .line 820268
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    sput v0, LX/4wg;->c:I

    .line 820269
    :try_start_0
    invoke-static {}, LX/4wg;->c()Lsun/misc/Unsafe;

    move-result-object v0

    sput-object v0, LX/4wg;->g:Lsun/misc/Unsafe;

    .line 820270
    const-class v0, LX/4wg;

    .line 820271
    sget-object v1, LX/4wg;->g:Lsun/misc/Unsafe;

    const-string v2, "base"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    invoke-virtual {v1, v2}, Lsun/misc/Unsafe;->objectFieldOffset(Ljava/lang/reflect/Field;)J

    move-result-wide v2

    sput-wide v2, LX/4wg;->h:J

    .line 820272
    sget-object v1, LX/4wg;->g:Lsun/misc/Unsafe;

    const-string v2, "busy"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-virtual {v1, v0}, Lsun/misc/Unsafe;->objectFieldOffset(Ljava/lang/reflect/Field;)J

    move-result-wide v0

    sput-wide v0, LX/4wg;->i:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 820273
    return-void

    .line 820274
    :catch_0
    move-exception v0

    .line 820275
    new-instance v1, Ljava/lang/Error;

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 820276
    invoke-direct {p0}, Ljava/lang/Number;-><init>()V

    .line 820277
    return-void
.end method

.method private static a(LX/4wg;)Z
    .locals 6

    .prologue
    .line 820278
    sget-object v0, LX/4wg;->g:Lsun/misc/Unsafe;

    sget-wide v2, LX/4wg;->i:J

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lsun/misc/Unsafe;->compareAndSwapInt(Ljava/lang/Object;JII)Z

    move-result v0

    return v0
.end method

.method public static c()Lsun/misc/Unsafe;
    .locals 3

    .prologue
    .line 820279
    :try_start_0
    invoke-static {}, Lsun/misc/Unsafe;->getUnsafe()Lsun/misc/Unsafe;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 820280
    :goto_0
    return-object v0

    :catch_0
    :try_start_1
    new-instance v0, LX/4wj;

    invoke-direct {v0}, LX/4wj;-><init>()V

    invoke-static {v0}, Ljava/security/AccessController;->doPrivileged(Ljava/security/PrivilegedExceptionAction;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsun/misc/Unsafe;
    :try_end_1
    .catch Ljava/security/PrivilegedActionException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 820281
    :catch_1
    move-exception v0

    .line 820282
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Could not initialize intrinsics"

    invoke-virtual {v0}, Ljava/security/PrivilegedActionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public abstract a(JJ)J
.end method

.method public final a(J[IZ)V
    .locals 11

    .prologue
    .line 820283
    if-nez p3, :cond_4

    .line 820284
    sget-object v0, LX/4wg;->a:Ljava/lang/ThreadLocal;

    const/4 v1, 0x1

    new-array p3, v1, [I

    invoke-virtual {v0, p3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 820285
    sget-object v0, LX/4wg;->b:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    .line 820286
    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    aput v0, p3, v1

    .line 820287
    :goto_0
    const/4 v1, 0x0

    .line 820288
    :cond_1
    :goto_1
    iget-object v3, p0, LX/4wg;->d:[LX/4wk;

    if-eqz v3, :cond_e

    array-length v2, v3

    if-lez v2, :cond_e

    .line 820289
    add-int/lit8 v4, v2, -0x1

    and-int/2addr v4, v0

    aget-object v4, v3, v4

    if-nez v4, :cond_7

    .line 820290
    iget v2, p0, LX/4wg;->f:I

    if-nez v2, :cond_5

    .line 820291
    new-instance v3, LX/4wk;

    invoke-direct {v3, p1, p2}, LX/4wk;-><init>(J)V

    .line 820292
    iget v2, p0, LX/4wg;->f:I

    if-nez v2, :cond_5

    invoke-static {p0}, LX/4wg;->a(LX/4wg;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 820293
    const/4 v2, 0x0

    .line 820294
    :try_start_0
    iget-object v4, p0, LX/4wg;->d:[LX/4wk;

    if-eqz v4, :cond_2

    array-length v5, v4

    if-lez v5, :cond_2

    add-int/lit8 v5, v5, -0x1

    and-int/2addr v5, v0

    aget-object v6, v4, v5

    if-nez v6, :cond_2

    .line 820295
    aput-object v3, v4, v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 820296
    const/4 v2, 0x1

    .line 820297
    :cond_2
    const/4 v3, 0x0

    iput v3, p0, LX/4wg;->f:I

    .line 820298
    if-eqz v2, :cond_1

    .line 820299
    :cond_3
    return-void

    .line 820300
    :cond_4
    const/4 v0, 0x0

    aget v0, p3, v0

    goto :goto_0

    .line 820301
    :catchall_0
    move-exception v0

    const/4 v1, 0x0

    iput v1, p0, LX/4wg;->f:I

    throw v0

    .line 820302
    :cond_5
    const/4 v1, 0x0

    .line 820303
    :cond_6
    :goto_2
    shl-int/lit8 v2, v0, 0xd

    xor-int/2addr v0, v2

    .line 820304
    ushr-int/lit8 v2, v0, 0x11

    xor-int/2addr v0, v2

    .line 820305
    shl-int/lit8 v2, v0, 0x5

    xor-int/2addr v0, v2

    .line 820306
    const/4 v2, 0x0

    aput v0, p3, v2

    goto :goto_1

    .line 820307
    :cond_7
    if-nez p4, :cond_8

    .line 820308
    const/4 p4, 0x1

    goto :goto_2

    .line 820309
    :cond_8
    iget-wide v6, v4, LX/4wk;->a:J

    invoke-virtual {p0, v6, v7, p1, p2}, LX/4wg;->a(JJ)J

    move-result-wide v8

    invoke-virtual {v4, v6, v7, v8, v9}, LX/4wk;->a(JJ)Z

    move-result v4

    if-nez v4, :cond_3

    .line 820310
    sget v4, LX/4wg;->c:I

    if-ge v2, v4, :cond_9

    iget-object v4, p0, LX/4wg;->d:[LX/4wk;

    if-eq v4, v3, :cond_a

    .line 820311
    :cond_9
    const/4 v1, 0x0

    goto :goto_2

    .line 820312
    :cond_a
    if-nez v1, :cond_b

    .line 820313
    const/4 v1, 0x1

    goto :goto_2

    .line 820314
    :cond_b
    iget v4, p0, LX/4wg;->f:I

    if-nez v4, :cond_6

    invoke-static {p0}, LX/4wg;->a(LX/4wg;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 820315
    :try_start_1
    iget-object v1, p0, LX/4wg;->d:[LX/4wk;

    if-ne v1, v3, :cond_d

    .line 820316
    shl-int/lit8 v1, v2, 0x1

    new-array v4, v1, [LX/4wk;

    .line 820317
    const/4 v1, 0x0

    :goto_3
    if-ge v1, v2, :cond_c

    .line 820318
    aget-object v5, v3, v1

    aput-object v5, v4, v1

    .line 820319
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 820320
    :cond_c
    iput-object v4, p0, LX/4wg;->d:[LX/4wk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 820321
    :cond_d
    const/4 v1, 0x0

    iput v1, p0, LX/4wg;->f:I

    .line 820322
    const/4 v1, 0x0

    .line 820323
    goto/16 :goto_1

    .line 820324
    :catchall_1
    move-exception v0

    const/4 v1, 0x0

    iput v1, p0, LX/4wg;->f:I

    throw v0

    .line 820325
    :cond_e
    iget v2, p0, LX/4wg;->f:I

    if-nez v2, :cond_10

    iget-object v2, p0, LX/4wg;->d:[LX/4wk;

    if-ne v2, v3, :cond_10

    invoke-static {p0}, LX/4wg;->a(LX/4wg;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 820326
    const/4 v2, 0x0

    .line 820327
    :try_start_2
    iget-object v4, p0, LX/4wg;->d:[LX/4wk;

    if-ne v4, v3, :cond_f

    .line 820328
    const/4 v2, 0x2

    new-array v2, v2, [LX/4wk;

    .line 820329
    and-int/lit8 v3, v0, 0x1

    new-instance v4, LX/4wk;

    invoke-direct {v4, p1, p2}, LX/4wk;-><init>(J)V

    aput-object v4, v2, v3

    .line 820330
    iput-object v2, p0, LX/4wg;->d:[LX/4wk;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 820331
    const/4 v2, 0x1

    .line 820332
    :cond_f
    const/4 v3, 0x0

    iput v3, p0, LX/4wg;->f:I

    .line 820333
    if-nez v2, :cond_3

    goto/16 :goto_1

    .line 820334
    :catchall_2
    move-exception v0

    const/4 v1, 0x0

    iput v1, p0, LX/4wg;->f:I

    throw v0

    .line 820335
    :cond_10
    iget-wide v2, p0, LX/4wg;->e:J

    invoke-virtual {p0, v2, v3, p1, p2}, LX/4wg;->a(JJ)J

    move-result-wide v4

    invoke-virtual {p0, v2, v3, v4, v5}, LX/4wg;->b(JJ)Z

    move-result v2

    if-nez v2, :cond_3

    goto/16 :goto_1
.end method

.method public final b(JJ)Z
    .locals 9

    .prologue
    .line 820336
    sget-object v0, LX/4wg;->g:Lsun/misc/Unsafe;

    sget-wide v2, LX/4wg;->h:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-virtual/range {v0 .. v7}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v0

    return v0
.end method
