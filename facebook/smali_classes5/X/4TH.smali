.class public LX/4TH;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 717600
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 717632
    const/4 v0, 0x0

    .line 717633
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_c

    .line 717634
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 717635
    :goto_0
    return v1

    .line 717636
    :cond_0
    const-string v12, "header_type"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 717637
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    move-result-object v3

    move-object v6, v3

    move v3, v2

    .line 717638
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_9

    .line 717639
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 717640
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 717641
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 717642
    const-string v12, "__type__"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    const-string v12, "__typename"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 717643
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->a(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 717644
    :cond_3
    const-string v12, "accent_images"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 717645
    invoke-static {p0, p1}, LX/2ax;->b(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 717646
    :cond_4
    const-string v12, "background_color"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 717647
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 717648
    :cond_5
    const-string v12, "background_image_style"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 717649
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 717650
    :cond_6
    const-string v12, "icon_image"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 717651
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 717652
    :cond_7
    const-string v12, "is_hoisted"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 717653
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 717654
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 717655
    :cond_9
    const/4 v11, 0x7

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 717656
    invoke-virtual {p1, v1, v10}, LX/186;->b(II)V

    .line 717657
    invoke-virtual {p1, v2, v9}, LX/186;->b(II)V

    .line 717658
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 717659
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 717660
    if-eqz v3, :cond_a

    .line 717661
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v6}, LX/186;->a(ILjava/lang/Enum;)V

    .line 717662
    :cond_a
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 717663
    if-eqz v0, :cond_b

    .line 717664
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->a(IZ)V

    .line 717665
    :cond_b
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_c
    move v3, v1

    move v4, v1

    move v5, v1

    move-object v6, v0

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    move v0, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 717601
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 717602
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 717603
    if-eqz v0, :cond_0

    .line 717604
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717605
    invoke-static {p0, p1, v2, p2}, LX/2bt;->a(LX/15i;IILX/0nX;)V

    .line 717606
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 717607
    if-eqz v0, :cond_1

    .line 717608
    const-string v1, "accent_images"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717609
    invoke-static {p0, v0, p2, p3}, LX/2ax;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 717610
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 717611
    if-eqz v0, :cond_2

    .line 717612
    const-string v1, "background_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717613
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 717614
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 717615
    if-eqz v0, :cond_3

    .line 717616
    const-string v1, "background_image_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717617
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 717618
    :cond_3
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 717619
    if-eqz v0, :cond_4

    .line 717620
    const-string v0, "header_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717621
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 717622
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 717623
    if-eqz v0, :cond_5

    .line 717624
    const-string v1, "icon_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717625
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 717626
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 717627
    if-eqz v0, :cond_6

    .line 717628
    const-string v1, "is_hoisted"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717629
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 717630
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 717631
    return-void
.end method
