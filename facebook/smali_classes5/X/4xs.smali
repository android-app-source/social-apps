.class public final LX/4xs;
.super LX/0P5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0P5",
        "<TV;TK;>;"
    }
.end annotation


# instance fields
.field public a:LX/1Ek;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ek",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/4xt;


# direct methods
.method public constructor <init>(LX/4xt;LX/1Ek;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ek",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 821469
    iput-object p1, p0, LX/4xs;->b:LX/4xt;

    invoke-direct {p0}, LX/0P5;-><init>()V

    .line 821470
    iput-object p2, p0, LX/4xs;->a:LX/1Ek;

    .line 821471
    return-void
.end method


# virtual methods
.method public final getKey()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 821472
    iget-object v0, p0, LX/4xs;->a:LX/1Ek;

    iget-object v0, v0, LX/0P4;->value:Ljava/lang/Object;

    return-object v0
.end method

.method public final getValue()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 821473
    iget-object v0, p0, LX/4xs;->a:LX/1Ek;

    iget-object v0, v0, LX/0P4;->key:Ljava/lang/Object;

    return-object v0
.end method

.method public final setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TK;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 821474
    iget-object v0, p0, LX/4xs;->a:LX/1Ek;

    iget-object v3, v0, LX/0P4;->key:Ljava/lang/Object;

    .line 821475
    invoke-static {p1}, LX/0PC;->a(Ljava/lang/Object;)I

    move-result v4

    .line 821476
    iget-object v0, p0, LX/4xs;->a:LX/1Ek;

    iget v0, v0, LX/1Ek;->keyHash:I

    if-ne v4, v0, :cond_0

    invoke-static {p1, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 821477
    :goto_0
    return-object p1

    .line 821478
    :cond_0
    iget-object v0, p0, LX/4xs;->b:LX/4xt;

    iget-object v0, v0, LX/4xt;->a:LX/4xu;

    iget-object v0, v0, LX/4xu;->a:LX/2cM;

    iget-object v0, v0, LX/2cM;->this$0:LX/1Ei;

    invoke-static {v0, p1, v4}, LX/1Ei;->a$redex0(LX/1Ei;Ljava/lang/Object;I)LX/1Ek;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v5, "value already present: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v5, v1}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 821479
    iget-object v0, p0, LX/4xs;->b:LX/4xt;

    iget-object v0, v0, LX/4xt;->a:LX/4xu;

    iget-object v0, v0, LX/4xu;->a:LX/2cM;

    iget-object v0, v0, LX/2cM;->this$0:LX/1Ei;

    iget-object v1, p0, LX/4xs;->a:LX/1Ek;

    invoke-static {v0, v1}, LX/1Ei;->a$redex0(LX/1Ei;LX/1Ek;)V

    .line 821480
    new-instance v0, LX/1Ek;

    iget-object v1, p0, LX/4xs;->a:LX/1Ek;

    iget-object v1, v1, LX/0P4;->value:Ljava/lang/Object;

    iget-object v2, p0, LX/4xs;->a:LX/1Ek;

    iget v2, v2, LX/1Ek;->valueHash:I

    invoke-direct {v0, p1, v4, v1, v2}, LX/1Ek;-><init>(Ljava/lang/Object;ILjava/lang/Object;I)V

    .line 821481
    iput-object v0, p0, LX/4xs;->a:LX/1Ek;

    .line 821482
    iget-object v1, p0, LX/4xs;->b:LX/4xt;

    iget-object v1, v1, LX/4xt;->a:LX/4xu;

    iget-object v1, v1, LX/4xu;->a:LX/2cM;

    iget-object v1, v1, LX/2cM;->this$0:LX/1Ei;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, LX/1Ei;->a$redex0(LX/1Ei;LX/1Ek;LX/1Ek;)V

    .line 821483
    iget-object v0, p0, LX/4xs;->b:LX/4xt;

    iget-object v1, p0, LX/4xs;->b:LX/4xt;

    iget-object v1, v1, LX/4xt;->a:LX/4xu;

    iget-object v1, v1, LX/4xu;->a:LX/2cM;

    iget-object v1, v1, LX/2cM;->this$0:LX/1Ei;

    iget v1, v1, LX/1Ei;->g:I

    iput v1, v0, LX/4xt;->d:I

    move-object p1, v3

    .line 821484
    goto :goto_0

    :cond_1
    move v0, v2

    .line 821485
    goto :goto_1
.end method
