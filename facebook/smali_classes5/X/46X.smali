.class public LX/46X;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<TT;>;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<TT;>;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 671286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 671287
    iput-object v0, p0, LX/46X;->a:Ljava/lang/ref/SoftReference;

    .line 671288
    iput-object v0, p0, LX/46X;->b:Ljava/lang/ref/SoftReference;

    .line 671289
    iput-object v0, p0, LX/46X;->c:Ljava/lang/ref/SoftReference;

    .line 671290
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 671276
    iget-object v0, p0, LX/46X;->a:Ljava/lang/ref/SoftReference;

    if-eqz v0, :cond_0

    .line 671277
    iget-object v0, p0, LX/46X;->a:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->clear()V

    .line 671278
    iput-object v1, p0, LX/46X;->a:Ljava/lang/ref/SoftReference;

    .line 671279
    :cond_0
    iget-object v0, p0, LX/46X;->b:Ljava/lang/ref/SoftReference;

    if-eqz v0, :cond_1

    .line 671280
    iget-object v0, p0, LX/46X;->b:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->clear()V

    .line 671281
    iput-object v1, p0, LX/46X;->b:Ljava/lang/ref/SoftReference;

    .line 671282
    :cond_1
    iget-object v0, p0, LX/46X;->c:Ljava/lang/ref/SoftReference;

    if-eqz v0, :cond_2

    .line 671283
    iget-object v0, p0, LX/46X;->c:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->clear()V

    .line 671284
    iput-object v1, p0, LX/46X;->c:Ljava/lang/ref/SoftReference;

    .line 671285
    :cond_2
    return-void
.end method
