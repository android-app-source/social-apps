.class public abstract LX/451;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ve;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0Ve",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private volatile a:Z

.field private b:Z

.field private c:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/44x;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 670118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 670119
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/451;->b:Z

    .line 670120
    return-void
.end method

.method private a(LX/44x;)V
    .locals 1

    .prologue
    .line 670114
    iget-object v0, p0, LX/451;->c:Ljava/util/Queue;

    if-nez v0, :cond_0

    .line 670115
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, LX/451;->c:Ljava/util/Queue;

    .line 670116
    :cond_0
    iget-object v0, p0, LX/451;->c:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 670117
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 670103
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/451;->b:Z

    .line 670104
    iget-object v0, p0, LX/451;->c:Ljava/util/Queue;

    if-nez v0, :cond_0

    .line 670105
    :goto_0
    return-void

    .line 670106
    :cond_0
    iget-object v1, p0, LX/451;->c:Ljava/util/Queue;

    monitor-enter v1

    .line 670107
    :try_start_0
    iget-object v0, p0, LX/451;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/44x;

    .line 670108
    iget-object v3, v0, LX/44w;->a:Ljava/lang/Object;

    sget-object v4, LX/44y;->SUCCESS:LX/44y;

    if-ne v3, v4, :cond_2

    .line 670109
    iget-object v0, v0, LX/44w;->b:Ljava/lang/Object;

    invoke-virtual {p0, v0}, LX/451;->onSuccess(Ljava/lang/Object;)V

    goto :goto_1

    .line 670110
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 670111
    :cond_2
    :try_start_1
    iget-object v3, v0, LX/44w;->a:Ljava/lang/Object;

    sget-object v4, LX/44y;->FAILURE:LX/44y;

    if-ne v3, v4, :cond_1

    .line 670112
    iget-object v0, v0, LX/44w;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Throwable;

    invoke-virtual {p0, v0}, LX/451;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 670113
    :cond_3
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public abstract a(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Throwable;)V
.end method

.method public a(Ljava/util/concurrent/CancellationException;)V
    .locals 0

    .prologue
    .line 670102
    return-void
.end method

.method public final dispose()V
    .locals 1

    .prologue
    .line 670098
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/451;->a:Z

    .line 670099
    iget-object v0, p0, LX/451;->c:Ljava/util/Queue;

    if-eqz v0, :cond_0

    .line 670100
    iget-object v0, p0, LX/451;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 670101
    :cond_0
    return-void
.end method

.method public final isDisposed()Z
    .locals 1

    .prologue
    .line 670097
    iget-boolean v0, p0, LX/451;->a:Z

    return v0
.end method

.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 670090
    iget-boolean v0, p0, LX/451;->a:Z

    if-eqz v0, :cond_0

    .line 670091
    :goto_0
    return-void

    .line 670092
    :cond_0
    iget-boolean v0, p0, LX/451;->b:Z

    if-nez v0, :cond_1

    .line 670093
    new-instance v0, LX/44z;

    invoke-direct {v0, p0, p1}, LX/44z;-><init>(LX/451;Ljava/lang/Throwable;)V

    invoke-direct {p0, v0}, LX/451;->a(LX/44x;)V

    goto :goto_0

    .line 670094
    :cond_1
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_2

    .line 670095
    check-cast p1, Ljava/util/concurrent/CancellationException;

    invoke-virtual {p0, p1}, LX/451;->a(Ljava/util/concurrent/CancellationException;)V

    goto :goto_0

    .line 670096
    :cond_2
    invoke-virtual {p0, p1}, LX/451;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 670085
    iget-boolean v0, p0, LX/451;->a:Z

    if-eqz v0, :cond_0

    .line 670086
    :goto_0
    return-void

    .line 670087
    :cond_0
    iget-boolean v0, p0, LX/451;->b:Z

    if-nez v0, :cond_1

    .line 670088
    new-instance v0, LX/450;

    invoke-direct {v0, p0, p1}, LX/450;-><init>(LX/451;Ljava/lang/Object;)V

    invoke-direct {p0, v0}, LX/451;->a(LX/44x;)V

    goto :goto_0

    .line 670089
    :cond_1
    invoke-virtual {p0, p1}, LX/451;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method
