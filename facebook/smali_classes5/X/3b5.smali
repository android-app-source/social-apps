.class public LX/3b5;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:Z

.field public b:Lcom/facebook/widget/text/BetterTextView;

.field public c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public d:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 609085
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 609086
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    .line 609087
    const v0, 0x7f030bff

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 609088
    const v0, 0x7f0d02a7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/3b5;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 609089
    const v0, 0x7f0d1dbd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/3b5;->d:Landroid/widget/ImageView;

    .line 609090
    const v0, 0x7f0d0575

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/3b5;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 609091
    return-void
.end method
