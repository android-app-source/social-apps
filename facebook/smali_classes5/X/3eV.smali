.class public LX/3eV;
.super LX/2Iu;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2Iu",
        "<",
        "LX/8je;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3eV;


# direct methods
.method public constructor <init>(LX/3e2;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 620261
    const-string v0, "stickers_db_properties"

    invoke-direct {p0, p1, v0}, LX/2Iu;-><init>(LX/0QR;Ljava/lang/String;)V

    .line 620262
    return-void
.end method

.method public static a(LX/0QB;)LX/3eV;
    .locals 4

    .prologue
    .line 620263
    sget-object v0, LX/3eV;->c:LX/3eV;

    if-nez v0, :cond_1

    .line 620264
    const-class v1, LX/3eV;

    monitor-enter v1

    .line 620265
    :try_start_0
    sget-object v0, LX/3eV;->c:LX/3eV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 620266
    if-eqz v2, :cond_0

    .line 620267
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 620268
    new-instance p0, LX/3eV;

    invoke-static {v0}, LX/3e2;->a(LX/0QB;)LX/3e2;

    move-result-object v3

    check-cast v3, LX/3e2;

    invoke-direct {p0, v3}, LX/3eV;-><init>(LX/3e2;)V

    .line 620269
    move-object v0, p0

    .line 620270
    sput-object v0, LX/3eV;->c:LX/3eV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 620271
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 620272
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 620273
    :cond_1
    sget-object v0, LX/3eV;->c:LX/3eV;

    return-object v0

    .line 620274
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 620275
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
