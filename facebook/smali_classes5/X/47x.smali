.class public LX/47x;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private b:Landroid/text/SpannableStringBuilder;

.field private c:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/47w;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 672604
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-direct {p0, v0, p1}, LX/47x;-><init>(Landroid/text/SpannableStringBuilder;Landroid/content/res/Resources;)V

    .line 672605
    return-void
.end method

.method private constructor <init>(Landroid/text/SpannableStringBuilder;Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 672598
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 672599
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, LX/47x;->b:Landroid/text/SpannableStringBuilder;

    .line 672600
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/47x;->c:Ljava/util/LinkedList;

    .line 672601
    iput-object p1, p0, LX/47x;->b:Landroid/text/SpannableStringBuilder;

    .line 672602
    iput-object p2, p0, LX/47x;->a:Landroid/content/res/Resources;

    .line 672603
    return-void
.end method


# virtual methods
.method public final a()LX/47x;
    .locals 5

    .prologue
    .line 672593
    iget-object v0, p0, LX/47x;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 672594
    iget-object v0, p0, LX/47x;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/47w;

    .line 672595
    iget-object v1, p0, LX/47x;->b:Landroid/text/SpannableStringBuilder;

    iget-object v2, v0, LX/47w;->b:Ljava/lang/Object;

    iget v3, v0, LX/47w;->a:I

    iget-object v4, p0, LX/47x;->b:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    iget v0, v0, LX/47w;->c:I

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 672596
    return-object p0

    .line 672597
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)LX/47x;
    .locals 2

    .prologue
    .line 672591
    iget-object v0, p0, LX/47x;->b:Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, LX/47x;->a:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 672592
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)LX/47x;
    .locals 1

    .prologue
    .line 672573
    iget-object v0, p0, LX/47x;->b:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 672574
    return-object p0
.end method

.method public final a(Ljava/lang/Object;I)LX/47x;
    .locals 3

    .prologue
    .line 672589
    iget-object v0, p0, LX/47x;->c:Ljava/util/LinkedList;

    new-instance v1, LX/47w;

    iget-object v2, p0, LX/47x;->b:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-direct {v1, v2, p1, p2}, LX/47w;-><init>(ILjava/lang/Object;I)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 672590
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/CharSequence;)LX/47x;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 672588
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    aput-object v1, v0, v2

    invoke-virtual {p0, p1, p2, v2, v0}, LX/47x;->a(Ljava/lang/String;Ljava/lang/CharSequence;I[Ljava/lang/Object;)LX/47x;

    move-result-object v0

    return-object v0
.end method

.method public final varargs a(Ljava/lang/String;Ljava/lang/CharSequence;I[Ljava/lang/Object;)LX/47x;
    .locals 6

    .prologue
    .line 672577
    iget-object v0, p0, LX/47x;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 672578
    invoke-static {p1}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 672579
    iget-object v1, p0, LX/47x;->b:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 672580
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 672581
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->start()I

    move-result v1

    .line 672582
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->end()I

    move-result v0

    .line 672583
    iget-object v2, p0, LX/47x;->b:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v2, v1, v0, p2}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 672584
    array-length v2, p4

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p4, v0

    .line 672585
    iget-object v4, p0, LX/47x;->b:Landroid/text/SpannableStringBuilder;

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {v4, v3, v1, v5, p3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 672586
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 672587
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;
    .locals 2

    .prologue
    .line 672576
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p3, v0, v1

    invoke-virtual {p0, p1, p2, p4, v0}, LX/47x;->a(Ljava/lang/String;Ljava/lang/CharSequence;I[Ljava/lang/Object;)LX/47x;

    move-result-object v0

    return-object v0
.end method

.method public final b()Landroid/text/SpannableString;
    .locals 2

    .prologue
    .line 672575
    new-instance v0, Landroid/text/SpannableString;

    iget-object v1, p0, LX/47x;->b:Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    return-object v0
.end method
