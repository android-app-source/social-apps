.class public abstract LX/51o;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/Beta;
.end annotation


# static fields
.field private static final a:[C


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 825485
    const-string v0, "0123456789abcdef"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, LX/51o;->a:[C

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 825484
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a([B)LX/51o;
    .locals 1

    .prologue
    .line 825483
    new-instance v0, LX/51p;

    invoke-direct {v0, p0}, LX/51p;-><init>([B)V

    return-object v0
.end method


# virtual methods
.method public abstract a()I
    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation
.end method

.method public abstract a(LX/51o;)Z
.end method

.method public abstract b()I
    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation
.end method

.method public abstract c()J
    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation
.end method

.method public abstract d()[B
    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation
.end method

.method public e()[B
    .locals 1

    .prologue
    .line 825482
    invoke-virtual {p0}, LX/51o;->d()[B

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 825478
    instance-of v1, p1, LX/51o;

    if-eqz v1, :cond_0

    .line 825479
    check-cast p1, LX/51o;

    .line 825480
    invoke-virtual {p0}, LX/51o;->a()I

    move-result v1

    invoke-virtual {p1}, LX/51o;->a()I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0, p1}, LX/51o;->a(LX/51o;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 825481
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    .line 825470
    invoke-virtual {p0}, LX/51o;->a()I

    move-result v0

    const/16 v1, 0x20

    if-lt v0, v1, :cond_1

    .line 825471
    invoke-virtual {p0}, LX/51o;->b()I

    move-result v1

    .line 825472
    :cond_0
    return v1

    .line 825473
    :cond_1
    invoke-virtual {p0}, LX/51o;->e()[B

    move-result-object v2

    .line 825474
    const/4 v0, 0x0

    aget-byte v0, v2, v0

    and-int/lit16 v1, v0, 0xff

    .line 825475
    const/4 v0, 0x1

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 825476
    aget-byte v3, v2, v0

    and-int/lit16 v3, v3, 0xff

    mul-int/lit8 v4, v0, 0x8

    shl-int/2addr v3, v4

    or-int/2addr v1, v3

    .line 825477
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 825464
    invoke-virtual {p0}, LX/51o;->e()[B

    move-result-object v1

    .line 825465
    new-instance v2, Ljava/lang/StringBuilder;

    array-length v0, v1

    mul-int/lit8 v0, v0, 0x2

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 825466
    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-byte v4, v1, v0

    .line 825467
    sget-object v5, LX/51o;->a:[C

    shr-int/lit8 v6, v4, 0x4

    and-int/lit8 v6, v6, 0xf

    aget-char v5, v5, v6

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, LX/51o;->a:[C

    and-int/lit8 v4, v4, 0xf

    aget-char v4, v6, v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 825468
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 825469
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
