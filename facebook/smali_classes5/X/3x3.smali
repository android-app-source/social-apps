.class public LX/3x3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3u7;
.implements LX/3uE;


# instance fields
.field private a:Landroid/content/Context;

.field public b:LX/3v0;

.field private c:Landroid/view/View;

.field private d:LX/3vD;

.field public e:LX/3x2;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 656698
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3x3;-><init>(Landroid/content/Context;Landroid/view/View;I)V

    .line 656699
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/view/View;I)V
    .locals 6

    .prologue
    .line 656723
    const v4, 0x7f01003b

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, LX/3x3;-><init>(Landroid/content/Context;Landroid/view/View;III)V

    .line 656724
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/view/View;III)V
    .locals 7

    .prologue
    .line 656712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 656713
    iput-object p1, p0, LX/3x3;->a:Landroid/content/Context;

    .line 656714
    new-instance v0, LX/3v0;

    invoke-direct {v0, p1}, LX/3v0;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/3x3;->b:LX/3v0;

    .line 656715
    iget-object v0, p0, LX/3x3;->b:LX/3v0;

    invoke-virtual {v0, p0}, LX/3v0;->a(LX/3u7;)V

    .line 656716
    iput-object p2, p0, LX/3x3;->c:Landroid/view/View;

    .line 656717
    new-instance v0, LX/3vD;

    iget-object v2, p0, LX/3x3;->b:LX/3v0;

    const/4 v4, 0x0

    move-object v1, p1

    move-object v3, p2

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, LX/3vD;-><init>(Landroid/content/Context;LX/3v0;Landroid/view/View;ZII)V

    iput-object v0, p0, LX/3x3;->d:LX/3vD;

    .line 656718
    iget-object v0, p0, LX/3x3;->d:LX/3vD;

    .line 656719
    iput p3, v0, LX/3vD;->r:I

    .line 656720
    iget-object v0, p0, LX/3x3;->d:LX/3vD;

    .line 656721
    iput-object p0, v0, LX/3vD;->n:LX/3uE;

    .line 656722
    return-void
.end method


# virtual methods
.method public final a(LX/3v0;Z)V
    .locals 0

    .prologue
    .line 656711
    return-void
.end method

.method public final a(LX/3v0;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 656708
    iget-object v0, p0, LX/3x3;->e:LX/3x2;

    if-eqz v0, :cond_0

    .line 656709
    iget-object v0, p0, LX/3x3;->e:LX/3x2;

    invoke-interface {v0, p2}, LX/3x2;->a(Landroid/view/MenuItem;)Z

    move-result v0

    .line 656710
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a_(LX/3v0;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 656704
    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 656705
    :cond_0
    :goto_0
    return v0

    .line 656706
    :cond_1
    invoke-virtual {p1}, LX/3v0;->hasVisibleItems()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 656707
    new-instance v1, LX/3vD;

    iget-object v2, p0, LX/3x3;->a:Landroid/content/Context;

    iget-object v3, p0, LX/3x3;->c:Landroid/view/View;

    invoke-direct {v1, v2, p1, v3}, LX/3vD;-><init>(Landroid/content/Context;LX/3v0;Landroid/view/View;)V

    invoke-virtual {v1}, LX/3vD;->a()V

    goto :goto_0
.end method

.method public final b()Landroid/view/MenuInflater;
    .locals 2

    .prologue
    .line 656703
    new-instance v0, LX/3ui;

    iget-object v1, p0, LX/3x3;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/3ui;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final b_(LX/3v0;)V
    .locals 0

    .prologue
    .line 656702
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 656700
    iget-object v0, p0, LX/3x3;->d:LX/3vD;

    invoke-virtual {v0}, LX/3vD;->a()V

    .line 656701
    return-void
.end method
