.class public final enum LX/3gq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3gq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3gq;

.field public static final enum DELTA:LX/3gq;

.field public static final enum FULL:LX/3gq;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 624876
    new-instance v0, LX/3gq;

    const-string v1, "FULL"

    invoke-direct {v0, v1, v2}, LX/3gq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3gq;->FULL:LX/3gq;

    .line 624877
    new-instance v0, LX/3gq;

    const-string v1, "DELTA"

    invoke-direct {v0, v1, v3}, LX/3gq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3gq;->DELTA:LX/3gq;

    .line 624878
    const/4 v0, 0x2

    new-array v0, v0, [LX/3gq;

    sget-object v1, LX/3gq;->FULL:LX/3gq;

    aput-object v1, v0, v2

    sget-object v1, LX/3gq;->DELTA:LX/3gq;

    aput-object v1, v0, v3

    sput-object v0, LX/3gq;->$VALUES:[LX/3gq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 624881
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3gq;
    .locals 1

    .prologue
    .line 624880
    const-class v0, LX/3gq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3gq;

    return-object v0
.end method

.method public static values()[LX/3gq;
    .locals 1

    .prologue
    .line 624879
    sget-object v0, LX/3gq;->$VALUES:[LX/3gq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3gq;

    return-object v0
.end method
