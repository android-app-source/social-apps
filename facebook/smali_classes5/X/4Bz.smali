.class public LX/4Bz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 678318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15i;Ljava/lang/Class;LX/16a;)Ljava/util/List;
    .locals 2
    .param p1    # Ljava/lang/Class;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/16a;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(",
            "LX/15i;",
            "Ljava/lang/Class",
            "<TT;>;",
            "LX/16a;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 678302
    if-eqz p1, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    if-nez p1, :cond_2

    if-eqz p2, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 678303
    invoke-virtual {p0}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 678304
    if-eqz p2, :cond_3

    .line 678305
    invoke-virtual {p0, v0, v1, p2}, LX/15i;->b(IILX/16a;)Ljava/util/Iterator;

    move-result-object v0

    .line 678306
    :goto_1
    if-nez v0, :cond_4

    .line 678307
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    .line 678308
    :goto_2
    return-object v0

    :cond_2
    move v0, v1

    .line 678309
    goto :goto_0

    .line 678310
    :cond_3
    invoke-virtual {p0, v0, v1, p1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    goto :goto_1

    .line 678311
    :cond_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 678312
    invoke-static {v1, v0}, LX/0RZ;->a(Ljava/util/Collection;Ljava/util/Iterator;)Z

    .line 678313
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_2
.end method

.method public static a(Ljava/nio/ByteBuffer;LX/16a;Ljava/lang/String;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)Ljava/util/List;
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/nio/ByteBuffer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/nio/ByteBuffer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/15j;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(",
            "Ljava/nio/ByteBuffer;",
            "LX/16a;",
            "Ljava/lang/String;",
            "Ljava/nio/ByteBuffer;",
            "Ljava/nio/ByteBuffer;",
            "Z",
            "Lcom/facebook/flatbuffers/MutableFlatBuffer$FlatBufferCorruptionHandler;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 678314
    new-instance v0, LX/15i;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    move v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 678315
    if-eqz p2, :cond_0

    .line 678316
    invoke-virtual {v0, p2}, LX/15i;->a(Ljava/lang/String;)V

    .line 678317
    :cond_0
    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, LX/4Bz;->a(LX/15i;Ljava/lang/Class;LX/16a;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/nio/ByteBuffer;Ljava/lang/Class;Ljava/lang/String;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)Ljava/util/List;
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/nio/ByteBuffer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/nio/ByteBuffer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/15j;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(",
            "Ljava/nio/ByteBuffer;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            "Ljava/nio/ByteBuffer;",
            "Ljava/nio/ByteBuffer;",
            "Z",
            "Lcom/facebook/flatbuffers/MutableFlatBuffer$FlatBufferCorruptionHandler;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 678298
    new-instance v0, LX/15i;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    move v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 678299
    if-eqz p2, :cond_0

    .line 678300
    invoke-virtual {v0, p2}, LX/15i;->a(Ljava/lang/String;)V

    .line 678301
    :cond_0
    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, LX/4Bz;->a(LX/15i;Ljava/lang/Class;LX/16a;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/List;LX/16a;)[B
    .locals 4
    .param p1    # LX/16a;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "LX/16a;",
            ")[B"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 678287
    new-instance v1, LX/186;

    const/16 v0, 0x800

    invoke-direct {v1, v0}, LX/186;-><init>(I)V

    .line 678288
    if-nez p1, :cond_0

    invoke-virtual {v1, p0, v3}, LX/186;->a(Ljava/util/List;Z)I

    move-result v0

    .line 678289
    :goto_0
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/186;->c(I)V

    .line 678290
    invoke-virtual {v1, v3, v0}, LX/186;->b(II)V

    .line 678291
    invoke-virtual {v1}, LX/186;->d()I

    move-result v0

    .line 678292
    if-gez v0, :cond_1

    .line 678293
    const/4 v0, 0x0

    .line 678294
    :goto_1
    return-object v0

    .line 678295
    :cond_0
    invoke-virtual {v1, p0, p1, v3}, LX/186;->a(Ljava/util/List;LX/16a;Z)I

    move-result v0

    goto :goto_0

    .line 678296
    :cond_1
    invoke-virtual {v1, v0}, LX/186;->d(I)V

    .line 678297
    invoke-virtual {v1}, LX/186;->e()[B

    move-result-object v0

    goto :goto_1
.end method
