.class public LX/3eP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/stickers/model/StickerPack;",
        ">;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/3eP;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 619982
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 619983
    return-void
.end method

.method public static a(LX/0QB;)LX/3eP;
    .locals 3

    .prologue
    .line 619984
    sget-object v0, LX/3eP;->a:LX/3eP;

    if-nez v0, :cond_1

    .line 619985
    const-class v1, LX/3eP;

    monitor-enter v1

    .line 619986
    :try_start_0
    sget-object v0, LX/3eP;->a:LX/3eP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 619987
    if-eqz v2, :cond_0

    .line 619988
    :try_start_1
    new-instance v0, LX/3eP;

    invoke-direct {v0}, LX/3eP;-><init>()V

    .line 619989
    move-object v0, v0

    .line 619990
    sput-object v0, LX/3eP;->a:LX/3eP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 619991
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 619992
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 619993
    :cond_1
    sget-object v0, LX/3eP;->a:LX/3eP;

    return-object v0

    .line 619994
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 619995
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 619996
    check-cast p1, Ljava/util/List;

    .line 619997
    new-instance v2, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/162;-><init>(LX/0mC;)V

    .line 619998
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 619999
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 620000
    iget-object v3, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v0, v3

    .line 620001
    invoke-virtual {v2, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 620002
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 620003
    :cond_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 620004
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "pack_ids"

    invoke-virtual {v2}, LX/162;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 620005
    new-instance v0, LX/14N;

    const-string v1, "setDownloadedStickerPacks"

    const-string v2, "POST"

    const-string v3, "me/sticker_tray_packs"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 620006
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 620007
    const/4 v0, 0x0

    return-object v0
.end method
