.class public final LX/4n5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final a:LX/4n4;


# instance fields
.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:LX/4n4;

.field public final f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 806484
    sget-object v0, LX/4n4;->MemoryUsagePowerOfTwo:LX/4n4;

    sput-object v0, LX/4n5;->a:LX/4n4;

    return-void
.end method

.method public constructor <init>(LX/4n6;)V
    .locals 1

    .prologue
    .line 806503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 806504
    iget-object v0, p1, LX/4n6;->d:LX/4n4;

    move-object v0, v0

    .line 806505
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 806506
    iget v0, p1, LX/4n6;->a:I

    move v0, v0

    .line 806507
    iput v0, p0, LX/4n5;->b:I

    .line 806508
    iget v0, p1, LX/4n6;->b:I

    move v0, v0

    .line 806509
    iput v0, p0, LX/4n5;->c:I

    .line 806510
    iget v0, p1, LX/4n6;->c:I

    move v0, v0

    .line 806511
    iput v0, p0, LX/4n5;->d:I

    .line 806512
    iget-object v0, p1, LX/4n6;->d:LX/4n4;

    move-object v0, v0

    .line 806513
    iput-object v0, p0, LX/4n5;->e:LX/4n4;

    .line 806514
    iget-boolean v0, p1, LX/4n6;->e:Z

    move v0, v0

    .line 806515
    iput-boolean v0, p0, LX/4n5;->f:Z

    .line 806516
    return-void
.end method

.method public static newBuilder()LX/4n6;
    .locals 1

    .prologue
    .line 806502
    new-instance v0, LX/4n6;

    invoke-direct {v0}, LX/4n6;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 806493
    if-ne p0, p1, :cond_1

    .line 806494
    :cond_0
    :goto_0
    return v0

    .line 806495
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 806496
    :cond_3
    check-cast p1, LX/4n5;

    .line 806497
    iget v2, p0, LX/4n5;->d:I

    iget v3, p1, LX/4n5;->d:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    .line 806498
    :cond_4
    iget v2, p0, LX/4n5;->c:I

    iget v3, p1, LX/4n5;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    .line 806499
    :cond_5
    iget-object v2, p0, LX/4n5;->e:LX/4n4;

    iget-object v3, p1, LX/4n5;->e:LX/4n4;

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    .line 806500
    :cond_6
    iget-boolean v2, p0, LX/4n5;->f:Z

    iget-boolean v3, p1, LX/4n5;->f:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    .line 806501
    :cond_7
    iget v2, p0, LX/4n5;->b:I

    iget v3, p1, LX/4n5;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 806486
    iget v0, p0, LX/4n5;->c:I

    .line 806487
    mul-int/lit8 v0, v0, 0x35

    iget v1, p0, LX/4n5;->b:I

    add-int/2addr v0, v1

    .line 806488
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/4n5;->d:I

    add-int/2addr v0, v1

    .line 806489
    mul-int/lit8 v0, v0, 0x11

    iget-object v1, p0, LX/4n5;->e:LX/4n4;

    invoke-virtual {v1}, LX/4n4;->ordinal()I

    move-result v1

    add-int/2addr v0, v1

    .line 806490
    mul-int/lit8 v1, v0, 0xd

    iget-boolean v0, p0, LX/4n5;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    .line 806491
    return v0

    .line 806492
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 806485
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "w"

    iget v2, p0, LX/4n5;->c:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "h"

    iget v2, p0, LX/4n5;->d:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "d"

    iget-object v2, p0, LX/4n5;->e:LX/4n4;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "o"

    iget-boolean v2, p0, LX/4n5;->f:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "c"

    iget v2, p0, LX/4n5;->b:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
