.class public final LX/4Z1;
.super LX/0ur;
.source ""


# instance fields
.field public b:I

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 785580
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 785581
    instance-of v0, p0, LX/4Z1;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 785582
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;)LX/4Z1;
    .locals 2

    .prologue
    .line 785583
    new-instance v0, LX/4Z1;

    invoke-direct {v0}, LX/4Z1;-><init>()V

    .line 785584
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 785585
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->a()I

    move-result v1

    iput v1, v0, LX/4Z1;->b:I

    .line 785586
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Z1;->c:LX/0Px;

    .line 785587
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    iput-object v1, v0, LX/4Z1;->d:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 785588
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->l()I

    move-result v1

    iput v1, v0, LX/4Z1;->e:I

    .line 785589
    invoke-static {v0, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 785590
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;
    .locals 2

    .prologue
    .line 785591
    new-instance v0, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;-><init>(LX/4Z1;)V

    .line 785592
    return-object v0
.end method
