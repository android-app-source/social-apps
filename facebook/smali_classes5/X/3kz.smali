.class public LX/3kz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0i1;


# static fields
.field private static final a:LX/0Tn;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/3kp;

.field public final d:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field public e:Ljava/lang/Runnable;

.field public f:LX/0hs;

.field public g:Z

.field public h:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 633433
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "feedback/VIDEO_COMMENT_NUX"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3kz;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/3kp;Landroid/os/Handler;)V
    .locals 2
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 633427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 633428
    iput-object p1, p0, LX/3kz;->b:Landroid/content/Context;

    .line 633429
    iput-object p2, p0, LX/3kz;->c:LX/3kp;

    .line 633430
    iget-object v0, p0, LX/3kz;->c:LX/3kp;

    sget-object v1, LX/3kz;->a:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 633431
    iput-object p3, p0, LX/3kz;->d:Landroid/os/Handler;

    .line 633432
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 633426
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 633434
    iget-boolean v0, p0, LX/3kz;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3kz;->c:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 633435
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    .line 633436
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 633425
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 633424
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 633423
    const-string v0, "4181"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 633422
    sget-object v0, LX/2tj;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
