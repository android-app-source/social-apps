.class public LX/3v4;
.super LX/3rR;
.source ""


# instance fields
.field public final a:Landroid/view/ActionProvider;

.field public final synthetic b:LX/3v9;


# direct methods
.method public constructor <init>(LX/3v9;Landroid/content/Context;Landroid/view/ActionProvider;)V
    .locals 0

    .prologue
    .line 650669
    iput-object p1, p0, LX/3v4;->b:LX/3v9;

    .line 650670
    invoke-direct {p0, p2}, LX/3rR;-><init>(Landroid/content/Context;)V

    .line 650671
    iput-object p3, p0, LX/3v4;->a:Landroid/view/ActionProvider;

    .line 650672
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 650668
    iget-object v0, p0, LX/3v4;->a:Landroid/view/ActionProvider;

    invoke-virtual {v0}, Landroid/view/ActionProvider;->onCreateActionView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/SubMenu;)V
    .locals 2

    .prologue
    .line 650673
    iget-object v0, p0, LX/3v4;->a:Landroid/view/ActionProvider;

    iget-object v1, p0, LX/3v4;->b:LX/3v9;

    invoke-virtual {v1, p1}, LX/3uv;->a(Landroid/view/SubMenu;)Landroid/view/SubMenu;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ActionProvider;->onPrepareSubMenu(Landroid/view/SubMenu;)V

    .line 650674
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 650666
    iget-object v0, p0, LX/3v4;->a:Landroid/view/ActionProvider;

    invoke-virtual {v0}, Landroid/view/ActionProvider;->onPerformDefaultAction()Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 650667
    iget-object v0, p0, LX/3v4;->a:Landroid/view/ActionProvider;

    invoke-virtual {v0}, Landroid/view/ActionProvider;->hasSubMenu()Z

    move-result v0

    return v0
.end method
