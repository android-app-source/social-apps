.class public final LX/4yd;
.super LX/12p;
.source ""

# interfaces
.implements LX/0dY;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/12p",
        "<TE;>;",
        "LX/0dY",
        "<TE;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0dW;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0dW",
            "<TE;>;",
            "LX/0Px",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 821923
    invoke-direct {p0, p1, p2}, LX/12p;-><init>(LX/0Py;LX/0Px;)V

    .line 821924
    return-void
.end method

.method private b()LX/0dW;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0dW",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 821925
    invoke-super {p0}, LX/12p;->a()LX/0Py;

    move-result-object v0

    check-cast v0, LX/0dW;

    return-object v0
.end method


# virtual methods
.method public final synthetic a()LX/0Py;
    .locals 1

    .prologue
    .line 821926
    invoke-direct {p0}, LX/4yd;->b()LX/0dW;

    move-result-object v0

    return-object v0
.end method

.method public final comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-TE;>;"
        }
    .end annotation

    .prologue
    .line 821927
    invoke-direct {p0}, LX/4yd;->b()LX/0dW;

    move-result-object v0

    invoke-virtual {v0}, LX/0dW;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 821928
    invoke-virtual {p0, p1}, LX/4yd;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final indexOf(Ljava/lang/Object;)I
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "ImmutableSortedSet.indexOf"
    .end annotation

    .prologue
    .line 821929
    invoke-direct {p0}, LX/4yd;->b()LX/0dW;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0dW;->a(Ljava/lang/Object;)I

    move-result v0

    .line 821930
    if-ltz v0, :cond_0

    invoke-virtual {p0, v0}, LX/12p;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final lastIndexOf(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "ImmutableSortedSet.indexOf"
    .end annotation

    .prologue
    .line 821931
    invoke-virtual {p0, p1}, LX/4yd;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final subListUnchecked(II)LX/0Px;
    .locals 3
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "super.subListUnchecked does not exist; inherited subList is valid if slow"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 821932
    invoke-super {p0, p1, p2}, LX/12p;->subListUnchecked(II)LX/0Px;

    move-result-object v0

    .line 821933
    new-instance v1, LX/50U;

    invoke-virtual {p0}, LX/4yd;->comparator()Ljava/util/Comparator;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/50U;-><init>(LX/0Px;Ljava/util/Comparator;)V

    invoke-virtual {v1}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    return-object v0
.end method
