.class public final LX/4bN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/15D;

.field public final synthetic b:LX/4bO;


# direct methods
.method public constructor <init>(LX/4bO;LX/15D;)V
    .locals 0

    .prologue
    .line 793633
    iput-object p1, p0, LX/4bN;->b:LX/4bO;

    iput-object p2, p0, LX/4bN;->a:LX/15D;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 793634
    iget-object v1, p0, LX/4bN;->b:LX/4bO;

    monitor-enter v1

    .line 793635
    :try_start_0
    iget-object v0, p0, LX/4bN;->b:LX/4bO;

    iget-object v0, v0, LX/4bO;->f:Ljava/util/Set;

    iget-object v2, p0, LX/4bN;->a:LX/15D;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 793636
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 793637
    :try_start_1
    iget-object v0, p0, LX/4bN;->b:LX/4bO;

    iget-object v0, v0, LX/4bO;->b:LX/1hl;

    iget-object v1, p0, LX/4bN;->a:LX/15D;

    invoke-virtual {v0, v1}, LX/1hl;->a(LX/15D;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v0

    .line 793638
    iget-object v1, p0, LX/4bN;->b:LX/4bO;

    monitor-enter v1

    .line 793639
    :try_start_2
    iget-object v2, p0, LX/4bN;->b:LX/4bO;

    iget-object v2, v2, LX/4bO;->f:Ljava/util/Set;

    iget-object v3, p0, LX/4bN;->a:LX/15D;

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 793640
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-object v0

    .line 793641
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 793642
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 793643
    :catchall_2
    move-exception v0

    iget-object v1, p0, LX/4bN;->b:LX/4bO;

    monitor-enter v1

    .line 793644
    :try_start_5
    iget-object v2, p0, LX/4bN;->b:LX/4bO;

    iget-object v2, v2, LX/4bO;->f:Ljava/util/Set;

    iget-object v3, p0, LX/4bN;->a:LX/15D;

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 793645
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    throw v0

    :catchall_3
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    throw v0
.end method
