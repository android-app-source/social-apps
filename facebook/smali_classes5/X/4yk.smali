.class public final LX/4yk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public a:Z

.field public final synthetic b:Ljava/util/Iterator;

.field public final synthetic c:LX/4yl;


# direct methods
.method public constructor <init>(LX/4yl;Ljava/util/Iterator;)V
    .locals 1

    .prologue
    .line 822016
    iput-object p1, p0, LX/4yk;->c:LX/4yl;

    iput-object p2, p0, LX/4yk;->b:Ljava/util/Iterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 822017
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4yk;->a:Z

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 822015
    iget-object v0, p0, LX/4yk;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 822008
    iget-object v0, p0, LX/4yk;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 822009
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/4yk;->a:Z

    .line 822010
    return-object v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 822011
    iget-boolean v0, p0, LX/4yk;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0P6;->a(Z)V

    .line 822012
    iget-object v0, p0, LX/4yk;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 822013
    return-void

    .line 822014
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
