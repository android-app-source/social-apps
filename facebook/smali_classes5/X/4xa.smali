.class public final LX/4xa;
.super LX/1r7;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1r7",
        "<TK;",
        "Ljava/util/Collection",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4xd;


# direct methods
.method public constructor <init>(LX/4xd;)V
    .locals 0

    .prologue
    .line 821114
    iput-object p1, p0, LX/4xa;->a:LX/4xd;

    .line 821115
    invoke-direct {p0, p1}, LX/1r7;-><init>(Ljava/util/Map;)V

    .line 821116
    return-void
.end method


# virtual methods
.method public final remove(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 821117
    iget-object v0, p0, LX/4xa;->a:LX/4xd;

    invoke-virtual {v0, p1}, LX/4xd;->a(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 821118
    iget-object v0, p0, LX/4xa;->a:LX/4xd;

    iget-object v0, v0, LX/4xd;->a:LX/4xj;

    invoke-static {p1}, LX/0Rj;->in(Ljava/util/Collection;)LX/0Rl;

    move-result-object v1

    invoke-static {v1}, LX/0PM;->a(LX/0Rl;)LX/0Rl;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4xj;->a(LX/0Rl;)Z

    move-result v0

    return v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 821119
    iget-object v0, p0, LX/4xa;->a:LX/4xd;

    iget-object v0, v0, LX/4xd;->a:LX/4xj;

    invoke-static {p1}, LX/0Rj;->in(Ljava/util/Collection;)LX/0Rl;

    move-result-object v1

    invoke-static {v1}, LX/0Rj;->not(LX/0Rl;)LX/0Rl;

    move-result-object v1

    invoke-static {v1}, LX/0PM;->a(LX/0Rl;)LX/0Rl;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4xj;->a(LX/0Rl;)Z

    move-result v0

    return v0
.end method
