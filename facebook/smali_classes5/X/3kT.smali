.class public abstract LX/3kT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0i1;


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/0lC;

.field private final c:LX/03V;

.field private final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public e:LX/8D7;


# direct methods
.method public constructor <init>(LX/0SG;LX/0lC;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0

    .prologue
    .line 632975
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 632976
    iput-object p1, p0, LX/3kT;->a:LX/0SG;

    .line 632977
    iput-object p2, p0, LX/3kT;->b:LX/0lC;

    .line 632978
    iput-object p3, p0, LX/3kT;->c:LX/03V;

    .line 632979
    iput-object p4, p0, LX/3kT;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 632980
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 632981
    const-wide/32 v0, 0x927c0

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 3

    .prologue
    .line 632982
    iget-object v0, p0, LX/3kT;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 632983
    iget-object v1, p0, LX/3kT;->e:LX/8D7;

    if-nez v1, :cond_0

    .line 632984
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, LX/8D7;->forControllerClass(Ljava/lang/Class;)LX/8D7;

    move-result-object v1

    iput-object v1, p0, LX/3kT;->e:LX/8D7;

    .line 632985
    :cond_0
    iget-object v1, p0, LX/3kT;->e:LX/8D7;

    move-object v1, v1

    .line 632986
    iget-object v1, v1, LX/8D7;->prefKey:LX/0Tn;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 632987
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 632988
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    .line 632989
    :goto_0
    return-object v0

    .line 632990
    :cond_1
    :try_start_0
    iget-object v1, p0, LX/3kT;->b:LX/0lC;

    const-class v2, Lcom/facebook/nux/NuxHistory;

    invoke-virtual {v1, v0, v2}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nux/NuxHistory;

    iget-object v1, p0, LX/3kT;->a:LX/0SG;

    invoke-virtual {v0, v1}, Lcom/facebook/nux/NuxHistory;->b(LX/0SG;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0

    :cond_2
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 632991
    :catch_0
    move-exception v0

    .line 632992
    iget-object v1, p0, LX/3kT;->c:LX/03V;

    const-string v2, "nux_history_decode_fail"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 632993
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 632994
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0
    .param p1    # Landroid/os/Parcelable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 632995
    return-void
.end method

.method public c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 632996
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_STORY_LOADED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
