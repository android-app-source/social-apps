.class public LX/3e5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile c:LX/3e5;


# instance fields
.field public final b:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 619257
    const-class v0, LX/3e5;

    sput-object v0, LX/3e5;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 619254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 619255
    iput-object p1, p0, LX/3e5;->b:Landroid/content/Context;

    .line 619256
    return-void
.end method

.method public static a(LX/0QB;)LX/3e5;
    .locals 4

    .prologue
    .line 619241
    sget-object v0, LX/3e5;->c:LX/3e5;

    if-nez v0, :cond_1

    .line 619242
    const-class v1, LX/3e5;

    monitor-enter v1

    .line 619243
    :try_start_0
    sget-object v0, LX/3e5;->c:LX/3e5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 619244
    if-eqz v2, :cond_0

    .line 619245
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 619246
    new-instance p0, LX/3e5;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/3e5;-><init>(Landroid/content/Context;)V

    .line 619247
    move-object v0, p0

    .line 619248
    sput-object v0, LX/3e5;->c:LX/3e5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 619249
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 619250
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 619251
    :cond_1
    sget-object v0, LX/3e5;->c:LX/3e5;

    return-object v0

    .line 619252
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 619253
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/io/File;)V
    .locals 4

    .prologue
    .line 619234
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 619235
    :cond_0
    return-void

    .line 619236
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_3

    .line 619237
    invoke-virtual {p0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_0

    .line 619238
    :cond_2
    :goto_0
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "can\'t create "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 619239
    :cond_3
    sget-object v0, LX/3e5;->a:Ljava/lang/Class;

    const-string v1, "%s is not a directory. deleting file and creating directory."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 619240
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Ljava/io/File;->mkdirs()Z

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;LX/3e1;Z)Ljava/io/File;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 619208
    invoke-virtual {p3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    .line 619209
    const-string v1, ""

    .line 619210
    if-eqz v2, :cond_3

    .line 619211
    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    .line 619212
    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    .line 619213
    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 619214
    :goto_0
    if-eqz p5, :cond_1

    .line 619215
    const/4 v1, 0x0

    .line 619216
    iget-object v3, p0, LX/3e5;->b:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    if-nez v3, :cond_5

    .line 619217
    sget-object v3, LX/3e5;->a:Ljava/lang/Class;

    const-string v4, "not external file dir"

    invoke-static {v3, v4}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 619218
    :goto_1
    move-object v1, v1

    .line 619219
    if-nez v1, :cond_4

    .line 619220
    const/4 v1, 0x0

    .line 619221
    :goto_2
    move-object v1, v1

    .line 619222
    :goto_3
    if-nez v1, :cond_2

    .line 619223
    :cond_0
    :goto_4
    return-object v0

    .line 619224
    :cond_1
    iget-object v1, p0, LX/3e5;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 619225
    if-eqz v3, :cond_0

    .line 619226
    new-instance v1, Ljava/io/File;

    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/io/File;

    const-string v6, "stickers"

    invoke-direct {v5, v3, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v4, v5, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v1, v4, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_3

    .line 619227
    :cond_2
    new-instance v0, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p4}, LX/3e1;->getDbName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_4

    :cond_3
    move-object v2, v1

    goto :goto_0

    .line 619228
    :cond_4
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 619229
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v3, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 619230
    invoke-static {v1}, LX/3e5;->a(Ljava/io/File;)V

    goto :goto_2

    .line 619231
    :cond_5
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, LX/3e5;->b:Landroid/content/Context;

    invoke-virtual {v4, v1}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    const-string v4, "stickers"

    invoke-direct {v3, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 619232
    invoke-static {v3}, LX/3e5;->a(Ljava/io/File;)V

    move-object v1, v3

    .line 619233
    goto :goto_1
.end method
