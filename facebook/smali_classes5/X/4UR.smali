.class public LX/4UR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 722851
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 722852
    const/4 v0, 0x0

    .line 722853
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_7

    .line 722854
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 722855
    :goto_0
    return v1

    .line 722856
    :cond_0
    const-string v8, "conditions_code"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 722857
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    move-result-object v0

    move-object v3, v0

    move v0, v2

    .line 722858
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_5

    .line 722859
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 722860
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 722861
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 722862
    const-string v8, "description"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 722863
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 722864
    :cond_2
    const-string v8, "image"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 722865
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 722866
    :cond_3
    const-string v8, "temperature"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 722867
    invoke-static {p0, p1}, LX/4Rx;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 722868
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 722869
    :cond_5
    const/4 v7, 0x4

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 722870
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 722871
    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 722872
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 722873
    if-eqz v0, :cond_6

    .line 722874
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->a(ILjava/lang/Enum;)V

    .line 722875
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move-object v3, v0

    move v4, v1

    move v5, v1

    move v6, v1

    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 722876
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 722877
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 722878
    if-eqz v0, :cond_0

    .line 722879
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 722880
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 722881
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 722882
    if-eqz v0, :cond_1

    .line 722883
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 722884
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 722885
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 722886
    if-eqz v0, :cond_2

    .line 722887
    const-string v1, "temperature"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 722888
    invoke-static {p0, v0, p2}, LX/4Rx;->a(LX/15i;ILX/0nX;)V

    .line 722889
    :cond_2
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 722890
    if-eqz v0, :cond_3

    .line 722891
    const-string v0, "conditions_code"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 722892
    const-class v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 722893
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 722894
    return-void
.end method
