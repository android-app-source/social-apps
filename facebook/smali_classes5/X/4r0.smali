.class public abstract LX/4r0;
.super LX/4qw;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x3dd3c47a2bb4a93L


# instance fields
.field public final _baseType:LX/0lJ;

.field public final _defaultImpl:LX/0lJ;

.field public _defaultImplDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final _deserializers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field public final _idResolver:LX/4qx;

.field public final _property:LX/2Ay;

.field public final _typeIdVisible:Z

.field public final _typePropertyName:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0lJ;LX/4qx;Ljava/lang/String;ZLjava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "LX/4qx;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 814885
    invoke-direct {p0}, LX/4qw;-><init>()V

    .line 814886
    iput-object p1, p0, LX/4r0;->_baseType:LX/0lJ;

    .line 814887
    iput-object p2, p0, LX/4r0;->_idResolver:LX/4qx;

    .line 814888
    iput-object p3, p0, LX/4r0;->_typePropertyName:Ljava/lang/String;

    .line 814889
    iput-boolean p4, p0, LX/4r0;->_typeIdVisible:Z

    .line 814890
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/4r0;->_deserializers:Ljava/util/HashMap;

    .line 814891
    if-nez p5, :cond_0

    .line 814892
    iput-object v1, p0, LX/4r0;->_defaultImpl:LX/0lJ;

    .line 814893
    :goto_0
    iput-object v1, p0, LX/4r0;->_property:LX/2Ay;

    .line 814894
    return-void

    .line 814895
    :cond_0
    invoke-virtual {p1, p5}, LX/0lJ;->b(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    iput-object v0, p0, LX/4r0;->_defaultImpl:LX/0lJ;

    goto :goto_0
.end method

.method public constructor <init>(LX/4r0;LX/2Ay;)V
    .locals 1

    .prologue
    .line 814896
    invoke-direct {p0}, LX/4qw;-><init>()V

    .line 814897
    iget-object v0, p1, LX/4r0;->_baseType:LX/0lJ;

    iput-object v0, p0, LX/4r0;->_baseType:LX/0lJ;

    .line 814898
    iget-object v0, p1, LX/4r0;->_idResolver:LX/4qx;

    iput-object v0, p0, LX/4r0;->_idResolver:LX/4qx;

    .line 814899
    iget-object v0, p1, LX/4r0;->_typePropertyName:Ljava/lang/String;

    iput-object v0, p0, LX/4r0;->_typePropertyName:Ljava/lang/String;

    .line 814900
    iget-boolean v0, p1, LX/4r0;->_typeIdVisible:Z

    iput-boolean v0, p0, LX/4r0;->_typeIdVisible:Z

    .line 814901
    iget-object v0, p1, LX/4r0;->_deserializers:Ljava/util/HashMap;

    iput-object v0, p0, LX/4r0;->_deserializers:Ljava/util/HashMap;

    .line 814902
    iget-object v0, p1, LX/4r0;->_defaultImpl:LX/0lJ;

    iput-object v0, p0, LX/4r0;->_defaultImpl:LX/0lJ;

    .line 814903
    iget-object v0, p1, LX/4r0;->_defaultImplDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    iput-object v0, p0, LX/4r0;->_defaultImplDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 814904
    iput-object p2, p0, LX/4r0;->_property:LX/2Ay;

    .line 814905
    return-void
.end method


# virtual methods
.method public final a(LX/0n3;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0n3;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 814906
    iget-object v0, p0, LX/4r0;->_defaultImpl:LX/0lJ;

    if-nez v0, :cond_1

    .line 814907
    sget-object v0, LX/0mv;->FAIL_ON_INVALID_SUBTYPE:LX/0mv;

    invoke-virtual {p1, v0}, LX/0n3;->a(LX/0mv;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 814908
    sget-object v0, Lcom/fasterxml/jackson/databind/deser/std/NullifyingDeserializer;->a:Lcom/fasterxml/jackson/databind/deser/std/NullifyingDeserializer;

    .line 814909
    :goto_0
    return-object v0

    .line 814910
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 814911
    :cond_1
    iget-object v0, p0, LX/4r0;->_defaultImpl:LX/0lJ;

    .line 814912
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 814913
    const-class v1, LX/1Xp;

    if-ne v0, v1, :cond_2

    .line 814914
    sget-object v0, Lcom/fasterxml/jackson/databind/deser/std/NullifyingDeserializer;->a:Lcom/fasterxml/jackson/databind/deser/std/NullifyingDeserializer;

    goto :goto_0

    .line 814915
    :cond_2
    iget-object v1, p0, LX/4r0;->_defaultImpl:LX/0lJ;

    monitor-enter v1

    .line 814916
    :try_start_0
    iget-object v0, p0, LX/4r0;->_defaultImplDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-nez v0, :cond_3

    .line 814917
    iget-object v0, p0, LX/4r0;->_defaultImpl:LX/0lJ;

    iget-object v2, p0, LX/4r0;->_property:LX/2Ay;

    invoke-virtual {p1, v0, v2}, LX/0n3;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    iput-object v0, p0, LX/4r0;->_defaultImplDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 814918
    :cond_3
    iget-object v0, p0, LX/4r0;->_defaultImplDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    monitor-exit v1

    goto :goto_0

    .line 814919
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/0n3;Ljava/lang/String;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0n3;",
            "Ljava/lang/String;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 814920
    iget-object v1, p0, LX/4r0;->_deserializers:Ljava/util/HashMap;

    monitor-enter v1

    .line 814921
    :try_start_0
    iget-object v0, p0, LX/4r0;->_deserializers:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 814922
    if-nez v0, :cond_1

    .line 814923
    iget-object v0, p0, LX/4r0;->_idResolver:LX/4qx;

    invoke-interface {v0, p2}, LX/4qx;->a(Ljava/lang/String;)LX/0lJ;

    move-result-object v0

    .line 814924
    if-nez v0, :cond_2

    .line 814925
    iget-object v0, p0, LX/4r0;->_defaultImpl:LX/0lJ;

    if-nez v0, :cond_0

    .line 814926
    iget-object v0, p0, LX/4r0;->_baseType:LX/0lJ;

    invoke-virtual {p1, v0, p2}, LX/0n3;->a(LX/0lJ;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 814927
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 814928
    :cond_0
    :try_start_1
    invoke-virtual {p0, p1}, LX/4r0;->a(LX/0n3;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 814929
    :goto_0
    iget-object v2, p0, LX/4r0;->_deserializers:Ljava/util/HashMap;

    invoke-virtual {v2, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 814930
    :cond_1
    monitor-exit v1

    .line 814931
    return-object v0

    .line 814932
    :cond_2
    iget-object v2, p0, LX/4r0;->_baseType:LX/0lJ;

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/4r0;->_baseType:LX/0lJ;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_3

    .line 814933
    iget-object v2, p0, LX/4r0;->_baseType:LX/0lJ;

    .line 814934
    iget-object v3, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v3

    .line 814935
    invoke-virtual {v2, v0}, LX/0lJ;->a(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    .line 814936
    :cond_3
    iget-object v2, p0, LX/4r0;->_property:LX/2Ay;

    invoke-virtual {p1, v0, v2}, LX/0n3;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 814937
    iget-object v0, p0, LX/4r0;->_typePropertyName:Ljava/lang/String;

    return-object v0
.end method

.method public final c()LX/4qx;
    .locals 1

    .prologue
    .line 814938
    iget-object v0, p0, LX/4r0;->_idResolver:LX/4qx;

    return-object v0
.end method

.method public final d()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 814939
    iget-object v0, p0, LX/4r0;->_defaultImpl:LX/0lJ;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/4r0;->_defaultImpl:LX/0lJ;

    .line 814940
    iget-object p0, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, p0

    .line 814941
    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 814942
    iget-object v0, p0, LX/4r0;->_baseType:LX/0lJ;

    .line 814943
    iget-object p0, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, p0

    .line 814944
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 814945
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 814946
    const/16 v1, 0x5b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 814947
    const-string v1, "; base-type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/4r0;->_baseType:LX/0lJ;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 814948
    const-string v1, "; id-resolver: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/4r0;->_idResolver:LX/4qx;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 814949
    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 814950
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
