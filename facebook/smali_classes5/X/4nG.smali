.class public LX/4nG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/4nF;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/view/View;

.field private final c:I

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 806655
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/4nG;-><init>(Landroid/view/View;Z)V

    .line 806656
    return-void
.end method

.method public constructor <init>(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 806657
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 806658
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/4nG;->a:Ljava/util/List;

    .line 806659
    iput-object p1, p0, LX/4nG;->b:Landroid/view/View;

    .line 806660
    iput-boolean p2, p0, LX/4nG;->d:Z

    .line 806661
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x42c80000    # 100.0f

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, LX/4nG;->c:I

    .line 806662
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 806663
    return-void
.end method


# virtual methods
.method public final a(LX/4nF;)V
    .locals 1

    .prologue
    .line 806664
    iget-object v0, p0, LX/4nG;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 806665
    return-void
.end method

.method public final b(LX/4nF;)V
    .locals 1

    .prologue
    .line 806666
    iget-object v0, p0, LX/4nG;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 806667
    return-void
.end method

.method public final onGlobalLayout()V
    .locals 3

    .prologue
    .line 806668
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 806669
    iget-object v1, p0, LX/4nG;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 806670
    iget-object v1, p0, LX/4nG;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v1, v0

    .line 806671
    iget-boolean v1, p0, LX/4nG;->d:Z

    if-nez v1, :cond_2

    iget v1, p0, LX/4nG;->c:I

    if-le v0, v1, :cond_2

    .line 806672
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/4nG;->d:Z

    .line 806673
    iget-object v1, p0, LX/4nG;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4nF;

    .line 806674
    if-eqz v1, :cond_0

    .line 806675
    invoke-interface {v1, v0}, LX/4nF;->e_(I)V

    goto :goto_0

    .line 806676
    :cond_1
    :goto_1
    return-void

    .line 806677
    :cond_2
    iget-boolean v1, p0, LX/4nG;->d:Z

    if-eqz v1, :cond_5

    iget v1, p0, LX/4nG;->c:I

    if-le v0, v1, :cond_5

    .line 806678
    iget-object v1, p0, LX/4nG;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4nF;

    .line 806679
    if-eqz v1, :cond_3

    .line 806680
    invoke-interface {v1, v0}, LX/4nF;->b(I)V

    goto :goto_2

    .line 806681
    :cond_4
    goto :goto_1

    .line 806682
    :cond_5
    iget-boolean v1, p0, LX/4nG;->d:Z

    if-eqz v1, :cond_1

    iget v1, p0, LX/4nG;->c:I

    if-ge v0, v1, :cond_1

    .line 806683
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4nG;->d:Z

    .line 806684
    iget-object v0, p0, LX/4nG;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4nF;

    .line 806685
    if-eqz v0, :cond_6

    .line 806686
    invoke-interface {v0}, LX/4nF;->a()V

    goto :goto_3

    .line 806687
    :cond_7
    goto :goto_1
.end method
