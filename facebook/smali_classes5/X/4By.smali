.class public LX/4By;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 678285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 678286
    return-void
.end method

.method public static a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 678284
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/4By;->b(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(",
            "Landroid/os/Parcel;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 678280
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 678281
    if-eqz v0, :cond_0

    invoke-static {p1}, LX/4By;->a(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 678282
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Class "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not final"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 678283
    :cond_0
    invoke-static {p0, p1}, LX/4By;->b(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Class;[BZ[BI)Lcom/facebook/flatbuffers/Flattenable;
    .locals 8
    .param p3    # [B
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;[BZ[BI)TT;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 678269
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    move-object v7, v0

    .line 678270
    instance-of v1, v7, LX/1vt;

    if-eqz v1, :cond_0

    .line 678271
    move-object v0, v7

    check-cast v0, LX/1vt;

    move-object v1, v0

    iput p4, v1, LX/1vt;->c:I

    .line 678272
    :cond_0
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 678273
    new-instance v1, LX/15i;

    const/4 v3, 0x0

    if-eqz p3, :cond_1

    invoke-static {p3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    :cond_1
    const/4 v6, 0x0

    move v5, p2

    invoke-direct/range {v1 .. v6}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 678274
    const-string v3, "FlatBufferModelHelper"

    invoke-virtual {v1, v3}, LX/15i;->a(Ljava/lang/String;)V

    .line 678275
    invoke-static {v2}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v2

    invoke-interface {v7, v1, v2}, Lcom/facebook/flatbuffers/Flattenable;->a(LX/15i;I)V
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 678276
    return-object v7

    .line 678277
    :catch_0
    move-exception v1

    .line 678278
    :goto_0
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Can\'t init flattenable object"

    invoke-direct {v2, v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 678279
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public static a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 678265
    if-nez p0, :cond_0

    move-object v0, v1

    .line 678266
    :goto_0
    return-object v0

    .line 678267
    :cond_0
    invoke-virtual {p0, p1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/helpers/FlatBufferModelHelper$LazyHolder;

    .line 678268
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/flatbuffers/helpers/FlatBufferModelHelper$LazyHolder;->a()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 678261
    if-nez p0, :cond_0

    move-object v0, v1

    .line 678262
    :goto_0
    return-object v0

    .line 678263
    :cond_0
    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/helpers/FlatBufferModelHelper$LazyHolder;

    .line 678264
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/flatbuffers/helpers/FlatBufferModelHelper$LazyHolder;->a()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method private static a(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/flatbuffers/helpers/FlatBufferModelHelper$LazyHolder;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 678255
    if-nez p0, :cond_0

    .line 678256
    :goto_0
    return-object v1

    .line 678257
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 678258
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 678259
    if-nez v4, :cond_1

    move-object v0, v1

    :goto_2
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    new-instance v0, Lcom/facebook/flatbuffers/helpers/FlatBufferModelHelper$LazyHolder;

    invoke-direct {v0, v4}, Lcom/facebook/flatbuffers/helpers/FlatBufferModelHelper$LazyHolder;-><init>(Ljava/lang/Object;)V

    goto :goto_2

    :cond_2
    move-object v1, v2

    .line 678260
    goto :goto_0
.end method

.method private static a(Ljava/util/ArrayList;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/flatbuffers/helpers/FlatBufferModelHelper$LazyHolder;",
            ">;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 678247
    if-nez p0, :cond_0

    move-object v0, v1

    .line 678248
    :goto_0
    return-object v0

    .line 678249
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 678250
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/helpers/FlatBufferModelHelper$LazyHolder;

    .line 678251
    if-eqz v0, :cond_1

    .line 678252
    invoke-virtual {v0}, Lcom/facebook/flatbuffers/helpers/FlatBufferModelHelper$LazyHolder;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 678253
    :cond_1
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 678254
    goto :goto_0
.end method

.method public static a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 678243
    if-eqz p0, :cond_0

    .line 678244
    if-nez p2, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 678245
    :cond_0
    return-void

    .line 678246
    :cond_1
    new-instance v0, Lcom/facebook/flatbuffers/helpers/FlatBufferModelHelper$LazyHolder;

    invoke-direct {v0, p2}, Lcom/facebook/flatbuffers/helpers/FlatBufferModelHelper$LazyHolder;-><init>(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 678240
    if-eqz p0, :cond_0

    .line 678241
    invoke-static {p2}, LX/4By;->a(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 678242
    :cond_0
    return-void
.end method

.method public static a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 678236
    if-eqz p0, :cond_0

    .line 678237
    if-nez p2, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 678238
    :cond_0
    return-void

    .line 678239
    :cond_1
    new-instance v0, Lcom/facebook/flatbuffers/helpers/FlatBufferModelHelper$LazyHolder;

    invoke-direct {v0, p2}, Lcom/facebook/flatbuffers/helpers/FlatBufferModelHelper$LazyHolder;-><init>(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 678233
    if-eqz p0, :cond_0

    .line 678234
    invoke-static {p2}, LX/4By;->a(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 678235
    :cond_0
    return-void
.end method

.method public static a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V
    .locals 1
    .param p1    # Lcom/facebook/flatbuffers/Flattenable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 678231
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;Z)V

    .line 678232
    return-void
.end method

.method private static a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;Z)V
    .locals 4
    .param p1    # Lcom/facebook/flatbuffers/Flattenable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 678131
    if-nez p1, :cond_0

    const/4 v0, 0x0

    move-object v2, v0

    .line 678132
    :goto_0
    if-nez v2, :cond_1

    .line 678133
    invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 678134
    :goto_1
    return-void

    .line 678135
    :cond_0
    invoke-static {p1}, LX/186;->b(Lcom/facebook/flatbuffers/Flattenable;)[B

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    .line 678136
    :cond_1
    array-length v0, v2

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 678137
    if-nez p2, :cond_2

    .line 678138
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 678139
    :cond_2
    invoke-static {p1}, LX/4By;->a(Lcom/facebook/flatbuffers/Flattenable;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 678140
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 678141
    invoke-static {p1}, LX/4By;->b(Lcom/facebook/flatbuffers/Flattenable;)[B

    move-result-object v0

    .line 678142
    if-nez v0, :cond_4

    .line 678143
    invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 678144
    :goto_3
    instance-of v0, p1, LX/1vt;

    if-eqz v0, :cond_5

    .line 678145
    check-cast p1, LX/1vt;

    iget v0, p1, LX/1vt;->c:I

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1

    :cond_3
    move v0, v1

    .line 678146
    goto :goto_2

    .line 678147
    :cond_4
    array-length v2, v0

    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 678148
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    goto :goto_3

    .line 678149
    :cond_5
    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1
.end method

.method public static a(Landroid/os/Parcel;Ljava/util/List;)V
    .locals 2

    .prologue
    .line 678150
    if-nez p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 678151
    if-eqz p1, :cond_1

    .line 678152
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 678153
    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p0, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    goto :goto_1

    .line 678154
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    .line 678155
    :cond_1
    return-void
.end method

.method public static a(Landroid/os/Parcel;Ljava/util/Map;)V
    .locals 3

    .prologue
    .line 678156
    if-nez p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 678157
    if-eqz p1, :cond_1

    .line 678158
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    .line 678159
    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 678160
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p0, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    goto :goto_1

    .line 678161
    :cond_0
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    goto :goto_0

    .line 678162
    :cond_1
    return-void
.end method

.method private static a(Lcom/facebook/flatbuffers/Flattenable;)Z
    .locals 1

    .prologue
    .line 678163
    instance-of v0, p0, Lcom/facebook/flatbuffers/MutableFlattenable;

    if-eqz v0, :cond_0

    .line 678164
    check-cast p0, Lcom/facebook/flatbuffers/MutableFlattenable;

    invoke-interface {p0}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v0

    .line 678165
    if-eqz v0, :cond_0

    .line 678166
    iget-boolean p0, v0, LX/15i;->g:Z

    move v0, p0

    .line 678167
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 678168
    sget-boolean v2, LX/007;->i:Z

    move v2, v2

    .line 678169
    if-nez v2, :cond_0

    .line 678170
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 678171
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Class;->getModifiers()I

    move-result v2

    invoke-static {v2}, Ljava/lang/reflect/Modifier;->isFinal(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 678172
    :cond_1
    :goto_0
    return v0

    .line 678173
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Class;->isInterface()Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    .line 678174
    goto :goto_0

    .line 678175
    :cond_3
    invoke-virtual {p0}, Ljava/lang/Class;->getDeclaredConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 678176
    invoke-virtual {v5}, Ljava/lang/reflect/Constructor;->getModifiers()I

    move-result v5

    invoke-static {v5}, Ljava/lang/reflect/Modifier;->isPrivate(I)Z

    move-result v5

    if-nez v5, :cond_4

    move v0, v1

    .line 678177
    goto :goto_0

    .line 678178
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private static b(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;
    .locals 5
    .param p1    # Ljava/lang/Class;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(",
            "Landroid/os/Parcel;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v4, -0x1

    .line 678179
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 678180
    if-ne v2, v4, :cond_0

    .line 678181
    :goto_0
    return-object v1

    .line 678182
    :cond_0
    if-nez p1, :cond_1

    .line 678183
    :try_start_0
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 678184
    :cond_1
    invoke-virtual {p0}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 678185
    :goto_1
    new-array v2, v2, [B

    .line 678186
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->readByteArray([B)V

    .line 678187
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 678188
    if-eq v3, v4, :cond_2

    .line 678189
    new-array v1, v3, [B

    .line 678190
    invoke-virtual {p0, v1}, Landroid/os/Parcel;->readByteArray([B)V

    .line 678191
    :cond_2
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 678192
    invoke-static {p1, v2, v0, v1, v3}, LX/4By;->a(Ljava/lang/Class;[BZ[BI)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    goto :goto_0

    .line 678193
    :catch_0
    move-exception v0

    .line 678194
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Can\'t init flattenable object"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 678195
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static b(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 678196
    if-nez p0, :cond_0

    .line 678197
    const/4 v0, 0x0

    .line 678198
    :goto_0
    return-object v0

    .line 678199
    :cond_0
    invoke-virtual {p0, p1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 678200
    invoke-static {v0}, LX/4By;->a(Ljava/util/ArrayList;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 678201
    if-nez p0, :cond_0

    .line 678202
    const/4 v0, 0x0

    .line 678203
    :goto_0
    return-object v0

    .line 678204
    :cond_0
    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 678205
    invoke-static {v0}, LX/4By;->a(Ljava/util/ArrayList;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/os/Parcel;)Ljava/util/List;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 678206
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 678207
    const/4 v0, -0x1

    if-ne v2, v0, :cond_1

    .line 678208
    const/4 v0, 0x0

    .line 678209
    :cond_0
    return-object v0

    .line 678210
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 678211
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 678212
    invoke-static {p0}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 678213
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static b(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V
    .locals 1
    .param p1    # Lcom/facebook/flatbuffers/Flattenable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 678214
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;Z)V

    .line 678215
    return-void
.end method

.method private static b(Lcom/facebook/flatbuffers/Flattenable;)[B
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 678216
    instance-of v0, p0, Lcom/facebook/flatbuffers/MutableFlattenable;

    if-eqz v0, :cond_0

    .line 678217
    check-cast p0, Lcom/facebook/flatbuffers/MutableFlattenable;

    invoke-interface {p0}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v0

    .line 678218
    if-eqz v0, :cond_0

    .line 678219
    invoke-virtual {v0}, LX/15i;->e()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 678220
    if-eqz v0, :cond_0

    .line 678221
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    .line 678222
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Landroid/os/Parcel;)Ljava/util/Map;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 678223
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 678224
    const/4 v0, -0x1

    if-ne v2, v0, :cond_1

    .line 678225
    const/4 v0, 0x0

    .line 678226
    :cond_0
    return-object v0

    .line 678227
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 678228
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 678229
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 678230
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
