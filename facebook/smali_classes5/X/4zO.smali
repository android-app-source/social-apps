.class public abstract LX/4zO;
.super LX/4xn;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/4xn",
        "<TK;TV;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field public transient a:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final concurrencyLevel:I

.field public final expireAfterAccessNanos:J

.field public final expireAfterWriteNanos:J

.field public final keyEquivalence:LX/0Qj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Qj",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final keyStrength:LX/0ci;

.field public final maximumSize:I

.field public final removalListener:LX/0d2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0d2",
            "<-TK;-TV;>;"
        }
    .end annotation
.end field

.field public final valueEquivalence:LX/0Qj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Qj",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final valueStrength:LX/0ci;


# direct methods
.method public constructor <init>(LX/0ci;LX/0ci;LX/0Qj;LX/0Qj;JJIILX/0d2;Ljava/util/concurrent/ConcurrentMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ci;",
            "LX/0ci;",
            "LX/0Qj",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "LX/0Qj",
            "<",
            "Ljava/lang/Object;",
            ">;JJII",
            "LX/0d2",
            "<-TK;-TV;>;",
            "Ljava/util/concurrent/ConcurrentMap",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822741
    invoke-direct {p0}, LX/4xn;-><init>()V

    .line 822742
    iput-object p1, p0, LX/4zO;->keyStrength:LX/0ci;

    .line 822743
    iput-object p2, p0, LX/4zO;->valueStrength:LX/0ci;

    .line 822744
    iput-object p3, p0, LX/4zO;->keyEquivalence:LX/0Qj;

    .line 822745
    iput-object p4, p0, LX/4zO;->valueEquivalence:LX/0Qj;

    .line 822746
    iput-wide p5, p0, LX/4zO;->expireAfterWriteNanos:J

    .line 822747
    iput-wide p7, p0, LX/4zO;->expireAfterAccessNanos:J

    .line 822748
    iput p9, p0, LX/4zO;->maximumSize:I

    .line 822749
    iput p10, p0, LX/4zO;->concurrencyLevel:I

    .line 822750
    iput-object p11, p0, LX/4zO;->removalListener:LX/0d2;

    .line 822751
    iput-object p12, p0, LX/4zO;->a:Ljava/util/concurrent/ConcurrentMap;

    .line 822752
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/ObjectInputStream;)LX/0S8;
    .locals 9

    .prologue
    const-wide/16 v4, 0x0

    .line 822701
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    .line 822702
    new-instance v1, LX/0S8;

    invoke-direct {v1}, LX/0S8;-><init>()V

    invoke-virtual {v1, v0}, LX/0S8;->a(I)LX/0S8;

    move-result-object v0

    iget-object v1, p0, LX/4zO;->keyStrength:LX/0ci;

    invoke-virtual {v0, v1}, LX/0S8;->a(LX/0ci;)LX/0S8;

    move-result-object v0

    iget-object v1, p0, LX/4zO;->valueStrength:LX/0ci;

    invoke-virtual {v0, v1}, LX/0S8;->b(LX/0ci;)LX/0S8;

    move-result-object v0

    iget-object v1, p0, LX/4zO;->keyEquivalence:LX/0Qj;

    const/4 v6, 0x0

    const/4 v3, 0x1

    .line 822703
    iget-object v2, v0, LX/0S8;->k:LX/0Qj;

    if-nez v2, :cond_6

    move v2, v3

    :goto_0
    const-string v7, "key equivalence was already set to %s"

    new-array v8, v3, [Ljava/lang/Object;

    iget-object p1, v0, LX/0S8;->k:LX/0Qj;

    aput-object p1, v8, v6

    invoke-static {v2, v7, v8}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 822704
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Qj;

    iput-object v2, v0, LX/0S8;->k:LX/0Qj;

    .line 822705
    iput-boolean v3, v0, LX/0S8;->b:Z

    .line 822706
    move-object v0, v0

    .line 822707
    iget v1, p0, LX/4zO;->concurrencyLevel:I

    invoke-virtual {v0, v1}, LX/0S8;->c(I)LX/0S8;

    move-result-object v0

    .line 822708
    iget-object v1, p0, LX/4zO;->removalListener:LX/0d2;

    const/4 v3, 0x1

    .line 822709
    iget-object v2, v0, LX/0S9;->a:LX/0d2;

    if-nez v2, :cond_7

    move v2, v3

    :goto_1
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 822710
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0d2;

    iput-object v2, v0, LX/0S9;->a:LX/0d2;

    .line 822711
    iput-boolean v3, v0, LX/0S8;->b:Z

    .line 822712
    iget-wide v2, p0, LX/4zO;->expireAfterWriteNanos:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 822713
    iget-wide v2, p0, LX/4zO;->expireAfterWriteNanos:J

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    .line 822714
    invoke-static {v0, v2, v3, v1}, LX/0S8;->c(LX/0S8;JLjava/util/concurrent/TimeUnit;)V

    .line 822715
    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v6

    iput-wide v6, v0, LX/0S8;->h:J

    .line 822716
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-nez v6, :cond_0

    iget-object v6, v0, LX/0S8;->j:LX/18j;

    if-nez v6, :cond_0

    .line 822717
    sget-object v6, LX/18j;->EXPIRED:LX/18j;

    iput-object v6, v0, LX/0S8;->j:LX/18j;

    .line 822718
    :cond_0
    const/4 v6, 0x1

    iput-boolean v6, v0, LX/0S8;->b:Z

    .line 822719
    :cond_1
    iget-wide v2, p0, LX/4zO;->expireAfterAccessNanos:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_3

    .line 822720
    iget-wide v2, p0, LX/4zO;->expireAfterAccessNanos:J

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    .line 822721
    invoke-static {v0, v2, v3, v1}, LX/0S8;->c(LX/0S8;JLjava/util/concurrent/TimeUnit;)V

    .line 822722
    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v6

    iput-wide v6, v0, LX/0S8;->i:J

    .line 822723
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-nez v6, :cond_2

    iget-object v6, v0, LX/0S8;->j:LX/18j;

    if-nez v6, :cond_2

    .line 822724
    sget-object v6, LX/18j;->EXPIRED:LX/18j;

    iput-object v6, v0, LX/0S8;->j:LX/18j;

    .line 822725
    :cond_2
    const/4 v6, 0x1

    iput-boolean v6, v0, LX/0S8;->b:Z

    .line 822726
    :cond_3
    iget v1, p0, LX/4zO;->maximumSize:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_5

    .line 822727
    iget v1, p0, LX/4zO;->maximumSize:I

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 822728
    iget v2, v0, LX/0S8;->e:I

    const/4 v5, -0x1

    if-ne v2, v5, :cond_8

    move v2, v3

    :goto_2
    const-string v5, "maximum size was already set to %s"

    new-array v6, v3, [Ljava/lang/Object;

    iget v7, v0, LX/0S8;->e:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-static {v2, v5, v6}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 822729
    if-ltz v1, :cond_4

    move v4, v3

    :cond_4
    const-string v2, "maximum size must not be negative"

    invoke-static {v4, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 822730
    iput v1, v0, LX/0S8;->e:I

    .line 822731
    iput-boolean v3, v0, LX/0S8;->b:Z

    .line 822732
    iget v2, v0, LX/0S8;->e:I

    if-nez v2, :cond_5

    .line 822733
    sget-object v2, LX/18j;->SIZE:LX/18j;

    iput-object v2, v0, LX/0S8;->j:LX/18j;

    .line 822734
    :cond_5
    return-object v0

    :cond_6
    move v2, v6

    .line 822735
    goto/16 :goto_0

    .line 822736
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_8
    move v2, v4

    .line 822737
    goto :goto_2
.end method

.method public final synthetic a()Ljava/util/Map;
    .locals 1

    .prologue
    .line 822740
    invoke-virtual {p0}, LX/4zO;->b()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/concurrent/ConcurrentMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/ConcurrentMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 822739
    iget-object v0, p0, LX/4zO;->a:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 822738
    invoke-virtual {p0}, LX/4zO;->b()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    return-object v0
.end method
