.class public final LX/3kG;
.super LX/3JO;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3JO",
        "<",
        "LX/3kI;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 632785
    invoke-direct {p0}, LX/3JO;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/util/JsonReader;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 632786
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 632787
    new-instance v1, LX/3kH;

    invoke-direct {v1}, LX/3kH;-><init>()V

    .line 632788
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 632789
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v2

    .line 632790
    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result p0

    sparse-switch p0, :sswitch_data_0

    :cond_0
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 632791
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_0

    .line 632792
    :sswitch_0
    const-string p0, "start_frame"

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string p0, "data"

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    .line 632793
    :pswitch_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v0

    iput v0, v1, LX/3kH;->a:I

    goto :goto_0

    .line 632794
    :pswitch_1
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, v1, LX/3kH;->b:I

    goto :goto_0

    .line 632795
    :cond_1
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 632796
    new-instance v0, LX/3kI;

    iget v2, v1, LX/3kH;->a:I

    iget p0, v1, LX/3kH;->b:I

    invoke-direct {v0, v2, p0}, LX/3kI;-><init>(II)V

    move-object v0, v0

    .line 632797
    move-object v0, v0

    .line 632798
    return-object v0

    :sswitch_data_0
    .sparse-switch
        -0x5b8680b0 -> :sswitch_0
        0x2eefaa -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
