.class public LX/3mC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/3mD;

.field public final b:LX/3mE;


# direct methods
.method public constructor <init>(LX/3mD;LX/3mE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 635538
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 635539
    iput-object p1, p0, LX/3mC;->a:LX/3mD;

    .line 635540
    iput-object p2, p0, LX/3mC;->b:LX/3mE;

    .line 635541
    return-void
.end method

.method public static a(LX/0QB;)LX/3mC;
    .locals 5

    .prologue
    .line 635527
    const-class v1, LX/3mC;

    monitor-enter v1

    .line 635528
    :try_start_0
    sget-object v0, LX/3mC;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 635529
    sput-object v2, LX/3mC;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 635530
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 635531
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 635532
    new-instance p0, LX/3mC;

    invoke-static {v0}, LX/3mD;->a(LX/0QB;)LX/3mD;

    move-result-object v3

    check-cast v3, LX/3mD;

    invoke-static {v0}, LX/3mE;->a(LX/0QB;)LX/3mE;

    move-result-object v4

    check-cast v4, LX/3mE;

    invoke-direct {p0, v3, v4}, LX/3mC;-><init>(LX/3mD;LX/3mE;)V

    .line 635533
    move-object v0, p0

    .line 635534
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 635535
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3mC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 635536
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 635537
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
