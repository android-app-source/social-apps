.class public LX/4Vd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/4Vd;


# instance fields
.field public final a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 742682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 742683
    iput-object p1, p0, LX/4Vd;->a:LX/0Uh;

    .line 742684
    return-void
.end method

.method public static a(LX/0QB;)LX/4Vd;
    .locals 4

    .prologue
    .line 742685
    sget-object v0, LX/4Vd;->b:LX/4Vd;

    if-nez v0, :cond_1

    .line 742686
    const-class v1, LX/4Vd;

    monitor-enter v1

    .line 742687
    :try_start_0
    sget-object v0, LX/4Vd;->b:LX/4Vd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 742688
    if-eqz v2, :cond_0

    .line 742689
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 742690
    new-instance p0, LX/4Vd;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/4Vd;-><init>(LX/0Uh;)V

    .line 742691
    move-object v0, p0

    .line 742692
    sput-object v0, LX/4Vd;->b:LX/4Vd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 742693
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 742694
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 742695
    :cond_1
    sget-object v0, LX/4Vd;->b:LX/4Vd;

    return-object v0

    .line 742696
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 742697
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
