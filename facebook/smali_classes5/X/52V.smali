.class public final LX/52V;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0RK;


# instance fields
.field public final a:Ljava/lang/annotation/Annotation;


# direct methods
.method public constructor <init>(Ljava/lang/annotation/Annotation;)V
    .locals 1

    .prologue
    .line 826103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 826104
    const-string v0, "annotation"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/annotation/Annotation;

    iput-object v0, p0, LX/52V;->a:Ljava/lang/annotation/Annotation;

    .line 826105
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 826110
    instance-of v0, p1, LX/52V;

    if-nez v0, :cond_0

    .line 826111
    const/4 v0, 0x0

    .line 826112
    :goto_0
    return v0

    .line 826113
    :cond_0
    check-cast p1, LX/52V;

    .line 826114
    iget-object v0, p0, LX/52V;->a:Ljava/lang/annotation/Annotation;

    iget-object v1, p1, LX/52V;->a:Ljava/lang/annotation/Annotation;

    invoke-interface {v0, v1}, Ljava/lang/annotation/Annotation;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final getAnnotation()Ljava/lang/annotation/Annotation;
    .locals 1

    .prologue
    .line 826109
    iget-object v0, p0, LX/52V;->a:Ljava/lang/annotation/Annotation;

    return-object v0
.end method

.method public final getAnnotationType()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 826108
    iget-object v0, p0, LX/52V;->a:Ljava/lang/annotation/Annotation;

    invoke-interface {v0}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 826107
    iget-object v0, p0, LX/52V;->a:Ljava/lang/annotation/Annotation;

    invoke-interface {v0}, Ljava/lang/annotation/Annotation;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 826106
    iget-object v0, p0, LX/52V;->a:Ljava/lang/annotation/Annotation;

    invoke-interface {v0}, Ljava/lang/annotation/Annotation;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
