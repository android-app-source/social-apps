.class public final LX/3io;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/3if;


# direct methods
.method public constructor <init>(LX/3if;)V
    .locals 0

    .prologue
    .line 630618
    iput-object p1, p0, LX/3io;->a:LX/3if;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 4

    .prologue
    .line 630619
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    sub-double/2addr v0, v2

    iget-object v2, p0, LX/3io;->a:LX/3if;

    invoke-virtual {v2}, LX/3if;->getBottom()I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    .line 630620
    iget-object v1, p0, LX/3io;->a:LX/3if;

    iget-object v1, v1, LX/3if;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    .line 630621
    iget-object v1, p0, LX/3io;->a:LX/3if;

    iget-object v1, v1, LX/3if;->g:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 630622
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 4

    .prologue
    .line 630623
    iget-object v0, p0, LX/3io;->a:LX/3if;

    const/4 v1, 0x0

    .line 630624
    iput-boolean v1, v0, LX/3if;->n:Z

    .line 630625
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3io;->a:LX/3if;

    iget-object v0, v0, LX/3if;->p:LX/3iq;

    if-eqz v0, :cond_0

    .line 630626
    iget-object v0, p0, LX/3io;->a:LX/3if;

    iget-object v0, v0, LX/3if;->p:LX/3iq;

    invoke-interface {v0}, LX/3iq;->a()V

    .line 630627
    :cond_0
    return-void
.end method
