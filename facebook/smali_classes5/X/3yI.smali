.class public LX/3yI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ad;
.implements LX/0ae;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 660881
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 660882
    return-void
.end method


# virtual methods
.method public final a(FF)F
    .locals 0

    .prologue
    .line 660883
    return p2
.end method

.method public final a(LX/0c0;LX/0c1;FF)F
    .locals 0

    .prologue
    .line 660884
    return p4
.end method

.method public final a(II)I
    .locals 0

    .prologue
    .line 660875
    return p2
.end method

.method public final a(LX/0c0;II)I
    .locals 0

    .prologue
    .line 660885
    return p3
.end method

.method public final a(LX/0c0;LX/0c1;II)I
    .locals 0

    .prologue
    .line 660886
    return p4
.end method

.method public final a(JJ)J
    .locals 1

    .prologue
    .line 660887
    return-wide p3
.end method

.method public final a(LX/0c0;JJ)J
    .locals 0

    .prologue
    .line 660888
    return-wide p4
.end method

.method public final a(LX/0c0;LX/0c1;JJ)J
    .locals 1

    .prologue
    .line 660889
    return-wide p5
.end method

.method public final a(DLjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ">(D",
            "Ljava/lang/Class",
            "<TT;>;TT;)TT;"
        }
    .end annotation

    .prologue
    .line 660891
    return-object p4
.end method

.method public final a(CILandroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 660890
    invoke-virtual {p3, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(CLjava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 660897
    return-object p2
.end method

.method public final a(LX/0c0;CILandroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 660896
    invoke-virtual {p4, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0c0;CLjava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 660895
    return-object p3
.end method

.method public final a(LX/0c0;LX/0c1;CLjava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 660898
    return-object p4
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 660894
    return-void
.end method

.method public final a(LX/0c0;C)V
    .locals 0

    .prologue
    .line 660893
    return-void
.end method

.method public final a(LX/0c0;I)V
    .locals 0

    .prologue
    .line 660892
    return-void
.end method

.method public final a(LX/0c0;S)V
    .locals 0

    .prologue
    .line 660879
    return-void
.end method

.method public final a(Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;)V
    .locals 0

    .prologue
    .line 660880
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 660860
    return-void
.end method

.method public final a(Ljava/util/Map;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/qe/api/manager/SyncedExperimentData;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 660861
    return-void
.end method

.method public final a(LX/0c0;LX/0c1;SZ)Z
    .locals 0

    .prologue
    .line 660862
    return p4
.end method

.method public final a(LX/0c0;SZ)Z
    .locals 0

    .prologue
    .line 660863
    return p3
.end method

.method public final a(LX/0oc;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 660864
    const/4 v0, 0x0

    return v0
.end method

.method public final a(SZ)Z
    .locals 0

    .prologue
    .line 660865
    return p2
.end method

.method public final b(LX/0oc;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 660866
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 660867
    return-void
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 660868
    const/4 v0, 0x0

    return v0
.end method

.method public final c()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 660869
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final c(LX/0oc;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 660870
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 660871
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 660872
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final d(LX/0oc;Ljava/lang/String;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0oc;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 660873
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 660874
    return-void
.end method

.method public final e()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 660876
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 660877
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 660878
    const/4 v0, 0x0

    return v0
.end method
