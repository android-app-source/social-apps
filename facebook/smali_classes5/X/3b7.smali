.class public LX/3b7;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/1wK;


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field public b:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/20J;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final e:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/20X;",
            "Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Landroid/view/ViewGroup;

.field private final g:LX/EOv;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 609288
    new-instance v0, LX/3b9;

    invoke-direct {v0}, LX/3b9;-><init>()V

    sput-object v0, LX/3b7;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 609286
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3b7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 609287
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 609272
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 609273
    const-class v0, LX/3b7;

    invoke-static {v0, p0}, LX/3b7;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 609274
    const v0, 0x7f0310a6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 609275
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/3b7;->setOrientation(I)V

    .line 609276
    iget-object v0, p0, LX/3b7;->d:LX/0Uh;

    const/16 v1, 0x3ce

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/3b7;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0102a4

    const v2, -0xb1a99b

    invoke-static {v0, v1, v2}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v0

    move v5, v0

    .line 609277
    :goto_0
    sget-object v0, LX/20X;->VISIT_LINK:LX/20X;

    const v1, 0x7f0208f8

    invoke-direct {p0, v1, v5}, LX/3b7;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const v2, 0x7f082283

    const v3, 0x7f0d27a8

    sget v4, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->a:I

    invoke-direct {p0, v1, v2, v3, v4}, LX/3b7;->a(Landroid/graphics/drawable/Drawable;III)Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    move-result-object v1

    sget-object v2, LX/20X;->SHARE:LX/20X;

    const v3, 0x7f0219c9

    invoke-direct {p0, v3, v5}, LX/3b7;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const v4, 0x7f082286

    const v6, 0x7f0d27a9

    sget v7, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->a:I

    invoke-direct {p0, v3, v4, v6, v7}, LX/3b7;->a(Landroid/graphics/drawable/Drawable;III)Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    move-result-object v3

    sget-object v4, LX/20X;->SAVE:LX/20X;

    const v6, 0x7f020781

    invoke-direct {p0, v6, v5}, LX/3b7;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    const v6, 0x7f082284

    const v7, 0x7f0d27aa

    sget v8, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->a:I

    invoke-direct {p0, v5, v6, v7, v8}, LX/3b7;->a(Landroid/graphics/drawable/Drawable;III)Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/3b7;->e:LX/0P1;

    .line 609278
    const v0, 0x7f0d27a7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/3b7;->f:Landroid/view/ViewGroup;

    .line 609279
    new-instance v0, LX/EOv;

    invoke-direct {v0}, LX/EOv;-><init>()V

    iput-object v0, p0, LX/3b7;->g:LX/EOv;

    .line 609280
    iget-object v0, p0, LX/3b7;->e:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 609281
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/20X;

    .line 609282
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 609283
    new-instance v3, LX/EOu;

    iget-object v4, p0, LX/3b7;->g:LX/EOv;

    invoke-direct {v3, v1, v4}, LX/EOu;-><init>(LX/20X;LX/20Z;)V

    invoke-virtual {v0, v3}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 609284
    :cond_0
    invoke-virtual {p0}, LX/3b7;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0102a3

    const v2, -0x6e685d

    invoke-static {v0, v1, v2}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v0

    move v5, v0

    goto/16 :goto_0

    .line 609285
    :cond_1
    return-void
.end method

.method private a(II)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 609213
    iget-object v0, p0, LX/3b7;->b:LX/0wM;

    invoke-virtual {v0, p1, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/graphics/drawable/Drawable;III)Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;
    .locals 2

    .prologue
    .line 609266
    invoke-virtual {p0, p3}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 609267
    invoke-virtual {v0, p2}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setText(I)V

    .line 609268
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setSoundEffectsEnabled(Z)V

    .line 609269
    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 609270
    invoke-virtual {v0, p4}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setWarmupBackgroundResId(I)V

    .line 609271
    return-object v0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/3b7;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    invoke-static {p0}, LX/20J;->b(LX/0QB;)LX/20J;

    move-result-object v2

    check-cast v2, LX/20J;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p0

    check-cast p0, LX/0Uh;

    iput-object v1, p1, LX/3b7;->b:LX/0wM;

    iput-object v2, p1, LX/3b7;->c:LX/20J;

    iput-object p0, p1, LX/3b7;->d:LX/0Uh;

    return-void
.end method


# virtual methods
.method public final a(LX/20X;)Landroid/view/View;
    .locals 1

    .prologue
    .line 609265
    iget-object v0, p0, LX/3b7;->e:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 609262
    iget-object v0, p0, LX/3b7;->e:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 609263
    invoke-virtual {v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->a()V

    goto :goto_0

    .line 609264
    :cond_0
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 609259
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 609260
    iget-object v0, p0, LX/3b7;->c:LX/20J;

    invoke-virtual {v0, p0, p1}, LX/20J;->a(Landroid/view/View;Landroid/graphics/Canvas;)V

    .line 609261
    return-void
.end method

.method public setBottomDividerStyle(LX/1Wl;)V
    .locals 1

    .prologue
    .line 609256
    iget-object v0, p0, LX/3b7;->c:LX/20J;

    .line 609257
    iput-object p1, v0, LX/20J;->e:LX/1Wl;

    .line 609258
    return-void
.end method

.method public setButtonContainerBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 609254
    iget-object v0, p0, LX/3b7;->f:Landroid/view/ViewGroup;

    invoke-static {v0, p1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 609255
    return-void
.end method

.method public setButtonContainerHeight(I)V
    .locals 2

    .prologue
    .line 609250
    iget-object v0, p0, LX/3b7;->f:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 609251
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 609252
    iget-object v1, p0, LX/3b7;->f:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 609253
    return-void
.end method

.method public setButtonWeights([F)V
    .locals 4

    .prologue
    .line 609240
    iget-object v0, p0, LX/3b7;->e:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v2

    .line 609241
    const/4 v0, 0x0

    move v1, v0

    .line 609242
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 609243
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 609244
    invoke-virtual {v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 609245
    if-eqz v0, :cond_0

    .line 609246
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    aget v3, p1, v1

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 609247
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 609248
    goto :goto_0

    .line 609249
    :cond_1
    return-void
.end method

.method public setButtons(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/20X;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 609235
    iget-object v0, p0, LX/3b7;->e:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20X;

    .line 609236
    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    .line 609237
    :goto_1
    iget-object v3, p0, LX/3b7;->e:LX/0P1;

    invoke-virtual {v3, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    goto :goto_0

    .line 609238
    :cond_0
    const/16 v1, 0x8

    goto :goto_1

    .line 609239
    :cond_1
    return-void
.end method

.method public setDownstateType(LX/1Wk;)V
    .locals 2

    .prologue
    .line 609232
    iget-object v0, p0, LX/3b7;->e:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 609233
    invoke-virtual {v0, p1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setDownstateType(LX/1Wk;)V

    goto :goto_0

    .line 609234
    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2

    .prologue
    .line 609229
    iget-object v0, p0, LX/3b7;->e:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 609230
    invoke-virtual {v0, p1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setEnabled(Z)V

    goto :goto_0

    .line 609231
    :cond_0
    return-void
.end method

.method public setHasCachedComments(Z)V
    .locals 0

    .prologue
    .line 609228
    return-void
.end method

.method public setIsLiked(Z)V
    .locals 0

    .prologue
    .line 609227
    return-void
.end method

.method public setOnButtonClickedListener(LX/20Z;)V
    .locals 1

    .prologue
    .line 609224
    iget-object v0, p0, LX/3b7;->g:LX/EOv;

    .line 609225
    iput-object p1, v0, LX/EOv;->a:LX/20Z;

    .line 609226
    return-void
.end method

.method public setShowIcons(Z)V
    .locals 2

    .prologue
    .line 609220
    iget-object v0, p0, LX/3b7;->e:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v1

    .line 609221
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609222
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a(Z)V

    goto :goto_0

    .line 609223
    :cond_0
    return-void
.end method

.method public setSprings(Ljava/util/EnumMap;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumMap",
            "<",
            "LX/20X;",
            "LX/215;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 609217
    invoke-virtual {p1}, Ljava/util/EnumMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20X;

    .line 609218
    iget-object v1, p0, LX/3b7;->e:LX/0P1;

    invoke-virtual {v1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {p1, v0}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/215;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setSpring(LX/215;)V

    goto :goto_0

    .line 609219
    :cond_0
    return-void
.end method

.method public setTopDividerStyle(LX/1Wl;)V
    .locals 1

    .prologue
    .line 609214
    iget-object v0, p0, LX/3b7;->c:LX/20J;

    .line 609215
    iput-object p1, v0, LX/20J;->d:LX/1Wl;

    .line 609216
    return-void
.end method
