.class public LX/4Kg;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 680599
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 28

    .prologue
    .line 680600
    const/16 v23, 0x0

    .line 680601
    const/16 v22, 0x0

    .line 680602
    const/16 v21, 0x0

    .line 680603
    const/16 v20, 0x0

    .line 680604
    const/16 v17, 0x0

    .line 680605
    const-wide/16 v18, 0x0

    .line 680606
    const/16 v16, 0x0

    .line 680607
    const-wide/16 v14, 0x0

    .line 680608
    const/4 v11, 0x0

    .line 680609
    const-wide/16 v12, 0x0

    .line 680610
    const/4 v10, 0x0

    .line 680611
    const/4 v9, 0x0

    .line 680612
    const/4 v8, 0x0

    .line 680613
    const/4 v7, 0x0

    .line 680614
    const/4 v6, 0x0

    .line 680615
    const/4 v5, 0x0

    .line 680616
    const/4 v4, 0x0

    .line 680617
    const/4 v3, 0x0

    .line 680618
    const/4 v2, 0x0

    .line 680619
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_15

    .line 680620
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 680621
    const/4 v2, 0x0

    .line 680622
    :goto_0
    return v2

    .line 680623
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_e

    .line 680624
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 680625
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 680626
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 680627
    const-string v6, "address"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 680628
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v26, v2

    goto :goto_1

    .line 680629
    :cond_1
    const-string v6, "country_code"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 680630
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v25, v2

    goto :goto_1

    .line 680631
    :cond_2
    const-string v6, "display_name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 680632
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v24, v2

    goto :goto_1

    .line 680633
    :cond_3
    const-string v6, "distance_unit"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 680634
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v23, v2

    goto :goto_1

    .line 680635
    :cond_4
    const-string v6, "key"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 680636
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 680637
    :cond_5
    const-string v6, "latitude"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 680638
    const/4 v2, 0x1

    .line 680639
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 680640
    :cond_6
    const-string v6, "location_type"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 680641
    const/4 v2, 0x1

    .line 680642
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    move-result-object v6

    move v12, v2

    move-object/from16 v19, v6

    goto/16 :goto_1

    .line 680643
    :cond_7
    const-string v6, "longitude"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 680644
    const/4 v2, 0x1

    .line 680645
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v11, v2

    move-wide/from16 v20, v6

    goto/16 :goto_1

    .line 680646
    :cond_8
    const-string v6, "name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 680647
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 680648
    :cond_9
    const-string v6, "radius"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 680649
    const/4 v2, 0x1

    .line 680650
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v10, v2

    move-wide/from16 v16, v6

    goto/16 :goto_1

    .line 680651
    :cond_a
    const-string v6, "region_key"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 680652
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 680653
    :cond_b
    const-string v6, "supports_city"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 680654
    const/4 v2, 0x1

    .line 680655
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v9, v2

    move v14, v6

    goto/16 :goto_1

    .line 680656
    :cond_c
    const-string v6, "supports_region"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 680657
    const/4 v2, 0x1

    .line 680658
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v8, v2

    move v13, v6

    goto/16 :goto_1

    .line 680659
    :cond_d
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 680660
    :cond_e
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 680661
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 680662
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 680663
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 680664
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 680665
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 680666
    if-eqz v3, :cond_f

    .line 680667
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 680668
    :cond_f
    if-eqz v12, :cond_10

    .line 680669
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 680670
    :cond_10
    if-eqz v11, :cond_11

    .line 680671
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v20

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 680672
    :cond_11
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 680673
    if-eqz v10, :cond_12

    .line 680674
    const/16 v3, 0x9

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 680675
    :cond_12
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 680676
    if-eqz v9, :cond_13

    .line 680677
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->a(IZ)V

    .line 680678
    :cond_13
    if-eqz v8, :cond_14

    .line 680679
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->a(IZ)V

    .line 680680
    :cond_14
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_15
    move/from16 v24, v21

    move/from16 v25, v22

    move/from16 v26, v23

    move/from16 v22, v17

    move/from16 v23, v20

    move-wide/from16 v20, v14

    move v14, v9

    move v15, v10

    move v9, v3

    move v10, v4

    move v3, v7

    move/from16 v27, v11

    move v11, v5

    move-wide/from16 v4, v18

    move-object/from16 v19, v16

    move/from16 v18, v27

    move-wide/from16 v16, v12

    move v13, v8

    move v12, v6

    move v8, v2

    goto/16 :goto_1
.end method
