.class public LX/3dt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final h:Ljava/lang/Object;


# instance fields
.field private b:LX/0re;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0re",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/3do;",
            "Ljava/util/LinkedHashSet",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/StickerTag;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 618017
    const-class v0, LX/3dt;

    sput-object v0, LX/3dt;->a:Ljava/lang/Class;

    .line 618018
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/3dt;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0rd;LX/3LO;)V
    .locals 12
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 618005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 618006
    const/16 v0, 0x7800

    const v1, 0x19000

    invoke-virtual {p2, v0, v1}, LX/3LO;->a(II)I

    move-result v0

    const-string v1, "stickers"

    new-instance v2, LX/3du;

    invoke-direct {v2, p0}, LX/3du;-><init>(LX/3dt;)V

    .line 618007
    const v7, 0x7fffffff

    sget-object v11, LX/0rg;->SIZE:LX/0rg;

    move-object v6, p1

    move v8, v0

    move-object v9, v1

    move-object v10, v2

    invoke-static/range {v6 .. v11}, LX/0rd;->a(LX/0rd;IILjava/lang/String;LX/3du;LX/0rg;)LX/0re;

    move-result-object v6

    move-object v4, v6

    .line 618008
    iget-object v5, p1, LX/0rd;->c:LX/0rb;

    invoke-interface {v5, v4}, LX/0rb;->a(LX/0rf;)V

    .line 618009
    move-object v0, v4

    .line 618010
    iput-object v0, p0, LX/3dt;->b:LX/0re;

    .line 618011
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/3dt;->c:Ljava/util/Map;

    .line 618012
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/3dt;->d:Ljava/util/Map;

    .line 618013
    iput-object v3, p0, LX/3dt;->g:LX/0Px;

    .line 618014
    iput-object v3, p0, LX/3dt;->e:LX/0Px;

    .line 618015
    iput-object v3, p0, LX/3dt;->f:LX/0Px;

    .line 618016
    return-void
.end method

.method public static a(LX/0QB;)LX/3dt;
    .locals 8

    .prologue
    .line 617976
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 617977
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 617978
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 617979
    if-nez v1, :cond_0

    .line 617980
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 617981
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 617982
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 617983
    sget-object v1, LX/3dt;->h:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 617984
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 617985
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 617986
    :cond_1
    if-nez v1, :cond_4

    .line 617987
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 617988
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 617989
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 617990
    new-instance p0, LX/3dt;

    invoke-static {v0}, LX/0rZ;->a(LX/0QB;)LX/0rd;

    move-result-object v1

    check-cast v1, LX/0rd;

    invoke-static {v0}, LX/3LO;->a(LX/0QB;)LX/3LO;

    move-result-object v7

    check-cast v7, LX/3LO;

    invoke-direct {p0, v1, v7}, LX/3dt;-><init>(LX/0rd;LX/3LO;)V

    .line 617991
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 617992
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 617993
    if-nez v1, :cond_2

    .line 617994
    sget-object v0, LX/3dt;->h:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3dt;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 617995
    :goto_1
    if-eqz v0, :cond_3

    .line 617996
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 617997
    :goto_3
    check-cast v0, LX/3dt;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 617998
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 617999
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 618000
    :catchall_1
    move-exception v0

    .line 618001
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 618002
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 618003
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 618004
    :cond_2
    :try_start_8
    sget-object v0, LX/3dt;->h:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3dt;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final declared-synchronized a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;"
        }
    .end annotation

    .prologue
    .line 617975
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3dt;->e:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/util/Collection;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;"
        }
    .end annotation

    .prologue
    .line 617969
    monitor-enter p0

    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 617970
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 617971
    iget-object v3, p0, LX/3dt;->b:LX/0re;

    invoke-virtual {v3, v0}, LX/0re;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 617972
    iget-object v3, p0, LX/3dt;->b:LX/0re;

    invoke-virtual {v3, v0}, LX/0re;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 617973
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 617974
    :cond_1
    :try_start_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public final declared-synchronized a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/StickerTag;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 617966
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/3dt;->g:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 617967
    monitor-exit p0

    return-void

    .line 617968
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/3do;Lcom/facebook/stickers/model/StickerPack;)V
    .locals 3

    .prologue
    .line 617952
    iget-object v0, p0, LX/3dt;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedHashSet;

    .line 617953
    if-nez v0, :cond_0

    .line 617954
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Tried to add a StickerPack before it was set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 617955
    :cond_0
    iget-object v1, p0, LX/3dt;->c:Ljava/util/Map;

    .line 617956
    iget-object v2, p2, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v2, v2

    .line 617957
    invoke-interface {v1, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 617958
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 617959
    iget-object v2, p2, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v2, v2

    .line 617960
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 617961
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 617962
    invoke-static {}, LX/0RA;->c()Ljava/util/LinkedHashSet;

    move-result-object v0

    .line 617963
    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    .line 617964
    iget-object v1, p0, LX/3dt;->d:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 617965
    return-void
.end method

.method public final declared-synchronized a(LX/3do;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3do;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 617942
    monitor-enter p0

    :try_start_0
    invoke-static {}, LX/0RA;->c()Ljava/util/LinkedHashSet;

    move-result-object v1

    .line 617943
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 617944
    iget-object v3, p0, LX/3dt;->c:Ljava/util/Map;

    .line 617945
    iget-object v4, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v4, v4

    .line 617946
    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 617947
    iget-object v3, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v0, v3

    .line 617948
    invoke-virtual {v1, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 617949
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 617950
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/3dt;->d:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 617951
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Lcom/facebook/stickers/model/StickerPack;)V
    .locals 2

    .prologue
    .line 617934
    monitor-enter p0

    .line 617935
    :try_start_0
    iget-object v0, p1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v0, v0

    .line 617936
    invoke-virtual {p0, v0}, LX/3dt;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 617937
    iget-object v0, p0, LX/3dt;->c:Ljava/util/Map;

    .line 617938
    iget-object v1, p1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v1, v1

    .line 617939
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 617940
    :cond_0
    monitor-exit p0

    return-void

    .line 617941
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 617931
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/3dt;->e:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 617932
    monitor-exit p0

    return-void

    .line 617933
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/3do;)Z
    .locals 1

    .prologue
    .line 617930
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3dt;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 617929
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3dt;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/3do;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3do;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;"
        }
    .end annotation

    .prologue
    .line 618019
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3dt;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedHashSet;

    .line 618020
    if-nez v0, :cond_0

    .line 618021
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 618022
    :goto_0
    move-object v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618023
    monitor-exit p0

    return-object v0

    .line 618024
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 618025
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 618026
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 618027
    iget-object p1, p0, LX/3dt;->c:Ljava/util/Map;

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 618028
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto :goto_0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)Lcom/facebook/stickers/model/StickerPack;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 617896
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3dt;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 617897
    iget-object v0, p0, LX/3dt;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 617898
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 617899
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 617900
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 617901
    iget-object v2, p0, LX/3dt;->b:LX/0re;

    iget-object v3, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, LX/0re;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 617902
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 617903
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized b(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 617904
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/3dt;->f:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 617905
    monitor-exit p0

    return-void

    .line 617906
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Z
    .locals 1

    .prologue
    .line 617907
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3dt;->e:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Ljava/lang/String;)LX/03R;
    .locals 2

    .prologue
    .line 617908
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    invoke-virtual {p0, v0}, LX/3dt;->a(LX/3do;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 617909
    iget-object v0, p0, LX/3dt;->d:Ljava/util/Map;

    sget-object v1, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/03R;->YES:LX/03R;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 617910
    :goto_0
    monitor-exit p0

    return-object v0

    .line 617911
    :cond_0
    :try_start_1
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0

    .line 617912
    :cond_1
    sget-object v0, LX/03R;->UNSET:LX/03R;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 617913
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;"
        }
    .end annotation

    .prologue
    .line 617914
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3dt;->f:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final clearUserData()V
    .locals 0

    .prologue
    .line 617927
    invoke-virtual {p0}, LX/3dt;->h()V

    .line 617928
    return-void
.end method

.method public final declared-synchronized d(Ljava/lang/String;)Lcom/facebook/stickers/model/Sticker;
    .locals 1

    .prologue
    .line 617915
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3dt;->b:LX/0re;

    invoke-virtual {v0, p1}, LX/0re;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Z
    .locals 1

    .prologue
    .line 617916
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3dt;->f:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()V
    .locals 1

    .prologue
    .line 617893
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/3dt;->f:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 617894
    monitor-exit p0

    return-void

    .line 617895
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()Z
    .locals 1

    .prologue
    .line 617917
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3dt;->g:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/StickerTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 617918
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3dt;->g:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized h()V
    .locals 1

    .prologue
    .line 617919
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3dt;->b:LX/0re;

    invoke-virtual {v0}, LX/0re;->a()V

    .line 617920
    iget-object v0, p0, LX/3dt;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 617921
    iget-object v0, p0, LX/3dt;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 617922
    const/4 v0, 0x0

    iput-object v0, p0, LX/3dt;->g:LX/0Px;

    .line 617923
    const/4 v0, 0x0

    iput-object v0, p0, LX/3dt;->f:LX/0Px;

    .line 617924
    const/4 v0, 0x0

    iput-object v0, p0, LX/3dt;->e:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 617925
    monitor-exit p0

    return-void

    .line 617926
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
