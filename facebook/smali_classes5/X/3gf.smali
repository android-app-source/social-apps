.class public LX/3gf;
.super LX/3gB;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/3gf;


# instance fields
.field public final a:LX/0WJ;

.field public final b:LX/3gg;

.field private final c:LX/3gh;


# direct methods
.method public constructor <init>(LX/0WJ;LX/3gg;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 624653
    invoke-direct {p0}, LX/3gB;-><init>()V

    .line 624654
    new-instance v0, LX/3gh;

    invoke-direct {v0, p0}, LX/3gh;-><init>(LX/3gf;)V

    iput-object v0, p0, LX/3gf;->c:LX/3gh;

    .line 624655
    iput-object p1, p0, LX/3gf;->a:LX/0WJ;

    .line 624656
    iput-object p2, p0, LX/3gf;->b:LX/3gg;

    .line 624657
    return-void
.end method

.method public static a(LX/0QB;)LX/3gf;
    .locals 6

    .prologue
    .line 624658
    sget-object v0, LX/3gf;->d:LX/3gf;

    if-nez v0, :cond_1

    .line 624659
    const-class v1, LX/3gf;

    monitor-enter v1

    .line 624660
    :try_start_0
    sget-object v0, LX/3gf;->d:LX/3gf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 624661
    if-eqz v2, :cond_0

    .line 624662
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 624663
    new-instance v5, LX/3gf;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v3

    check-cast v3, LX/0WJ;

    .line 624664
    new-instance p0, LX/3gg;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v4

    check-cast v4, LX/0sO;

    invoke-direct {p0, v4}, LX/3gg;-><init>(LX/0sO;)V

    .line 624665
    const/16 v4, 0x435

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    .line 624666
    iput-object v4, p0, LX/3gg;->b:LX/0Or;

    .line 624667
    move-object v4, p0

    .line 624668
    check-cast v4, LX/3gg;

    invoke-direct {v5, v3, v4}, LX/3gf;-><init>(LX/0WJ;LX/3gg;)V

    .line 624669
    move-object v0, v5

    .line 624670
    sput-object v0, LX/3gf;->d:LX/3gf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 624671
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 624672
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 624673
    :cond_1
    sget-object v0, LX/3gf;->d:LX/3gf;

    return-object v0

    .line 624674
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 624675
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()LX/2ZE;
    .locals 1

    .prologue
    .line 624676
    iget-object v0, p0, LX/3gf;->c:LX/3gh;

    return-object v0
.end method

.method public final dr_()J
    .locals 2

    .prologue
    .line 624677
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method
