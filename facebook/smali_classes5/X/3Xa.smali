.class public LX/3Xa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 593514
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 593515
    return-void
.end method

.method public static a(LX/0QB;)LX/3Xa;
    .locals 3

    .prologue
    .line 593516
    const-class v1, LX/3Xa;

    monitor-enter v1

    .line 593517
    :try_start_0
    sget-object v0, LX/3Xa;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 593518
    sput-object v2, LX/3Xa;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 593519
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593520
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 593521
    new-instance v0, LX/3Xa;

    invoke-direct {v0}, LX/3Xa;-><init>()V

    .line 593522
    move-object v0, v0

    .line 593523
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 593524
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3Xa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 593525
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 593526
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 593512
    sget-object v0, Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 593513
    return-void
.end method
