.class public LX/4lh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile m:LX/4lh;


# instance fields
.field private final b:I

.field private final c:I

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/4lm;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/4lu;

.field private final f:LX/4lv;

.field private final g:LX/4lw;

.field private final h:LX/4lk;

.field private final i:LX/4ly;

.field private final j:LX/03V;

.field public final k:Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;

.field private final l:Lorg/apache/http/conn/ssl/X509HostnameVerifier;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 804629
    const-class v0, LX/4lh;

    sput-object v0, LX/4lh;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/Set;LX/4lu;LX/4lv;LX/4lw;LX/4lk;LX/4ly;LX/03V;Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;Lorg/apache/http/conn/ssl/X509HostnameVerifier;)V
    .locals 1
    .param p1    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/common/android/AndroidSdkVersion;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/ssl/SSLSessionTimeoutSeconds;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/util/Set",
            "<",
            "LX/4lm;",
            ">;",
            "LX/4lu;",
            "LX/4lv;",
            "LX/4lw;",
            "LX/4lk;",
            "LX/4ly;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;",
            "Lorg/apache/http/conn/ssl/X509HostnameVerifier;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 804630
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 804631
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/4lh;->c:I

    .line 804632
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/4lh;->b:I

    .line 804633
    iput-object p3, p0, LX/4lh;->d:Ljava/util/Set;

    .line 804634
    iput-object p4, p0, LX/4lh;->e:LX/4lu;

    .line 804635
    iput-object p5, p0, LX/4lh;->f:LX/4lv;

    .line 804636
    iput-object p6, p0, LX/4lh;->g:LX/4lw;

    .line 804637
    iput-object p7, p0, LX/4lh;->h:LX/4lk;

    .line 804638
    iput-object p8, p0, LX/4lh;->i:LX/4ly;

    .line 804639
    iput-object p9, p0, LX/4lh;->j:LX/03V;

    .line 804640
    iput-object p10, p0, LX/4lh;->k:Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;

    .line 804641
    iput-object p11, p0, LX/4lh;->l:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    .line 804642
    return-void
.end method

.method public static a(LX/0QB;)LX/4lh;
    .locals 15

    .prologue
    .line 804643
    sget-object v0, LX/4lh;->m:LX/4lh;

    if-nez v0, :cond_1

    .line 804644
    const-class v1, LX/4lh;

    monitor-enter v1

    .line 804645
    :try_start_0
    sget-object v0, LX/4lh;->m:LX/4lh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 804646
    if-eqz v2, :cond_0

    .line 804647
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 804648
    new-instance v3, LX/4lh;

    invoke-static {v0}, LX/43s;->a(LX/0QB;)Ljava/lang/Integer;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 804649
    invoke-static {}, LX/4lg;->a()Ljava/lang/Integer;

    move-result-object v5

    move-object v5, v5

    .line 804650
    check-cast v5, Ljava/lang/Integer;

    .line 804651
    new-instance v6, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v7

    new-instance v8, LX/4ls;

    invoke-direct {v8, v0}, LX/4ls;-><init>(LX/0QB;)V

    invoke-direct {v6, v7, v8}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v6, v6

    .line 804652
    invoke-static {v0}, LX/4lu;->a(LX/0QB;)LX/4lu;

    move-result-object v7

    check-cast v7, LX/4lu;

    invoke-static {v0}, LX/4lv;->a(LX/0QB;)LX/4lv;

    move-result-object v8

    check-cast v8, LX/4lv;

    invoke-static {v0}, LX/4lw;->a(LX/0QB;)LX/4lw;

    move-result-object v9

    check-cast v9, LX/4lw;

    invoke-static {v0}, LX/4lk;->a(LX/0QB;)LX/4lk;

    move-result-object v10

    check-cast v10, LX/4lk;

    invoke-static {v0}, LX/4ly;->a(LX/0QB;)LX/4ly;

    move-result-object v11

    check-cast v11, LX/4ly;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v12

    check-cast v12, LX/03V;

    invoke-static {v0}, Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;->b(LX/0QB;)Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;

    move-result-object v13

    check-cast v13, Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;

    invoke-static {v0}, LX/4li;->a(LX/0QB;)Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    move-result-object v14

    check-cast v14, Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    invoke-direct/range {v3 .. v14}, LX/4lh;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/Set;LX/4lu;LX/4lv;LX/4lw;LX/4lk;LX/4ly;LX/03V;Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;Lorg/apache/http/conn/ssl/X509HostnameVerifier;)V

    .line 804653
    move-object v0, v3

    .line 804654
    sput-object v0, LX/4lh;->m:LX/4lh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 804655
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 804656
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 804657
    :cond_1
    sget-object v0, LX/4lh;->m:LX/4lh;

    return-object v0

    .line 804658
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 804659
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b(Lorg/apache/http/conn/scheme/SocketFactory;)Lorg/apache/http/conn/scheme/SocketFactory;
    .locals 2
    .param p1    # Lorg/apache/http/conn/scheme/SocketFactory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 804660
    instance-of v0, p1, Lorg/apache/http/conn/ssl/SSLSocketFactory;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 804661
    check-cast v0, Lorg/apache/http/conn/ssl/SSLSocketFactory;

    iget-object v1, p0, LX/4lh;->l:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    invoke-virtual {v0, v1}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->setHostnameVerifier(Lorg/apache/http/conn/ssl/X509HostnameVerifier;)V

    .line 804662
    :cond_0
    return-object p1
.end method


# virtual methods
.method public final a(Lorg/apache/http/conn/scheme/SocketFactory;)Lorg/apache/http/conn/scheme/SocketFactory;
    .locals 9
    .param p1    # Lorg/apache/http/conn/scheme/SocketFactory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 804663
    iget v0, p0, LX/4lh;->c:I

    const/16 v1, 0x8

    if-gt v0, v1, :cond_0

    .line 804664
    invoke-direct {p0, p1}, LX/4lh;->b(Lorg/apache/http/conn/scheme/SocketFactory;)Lorg/apache/http/conn/scheme/SocketFactory;

    move-result-object v0

    .line 804665
    :goto_0
    return-object v0

    .line 804666
    :cond_0
    :try_start_0
    iget v0, p0, LX/4lh;->c:I

    const/16 v1, 0x10

    if-gt v0, v1, :cond_2

    .line 804667
    iget-object v0, p0, LX/4lh;->d:Ljava/util/Set;

    .line 804668
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4lm;

    .line 804669
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "trying check "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 804670
    invoke-interface {v1}, LX/4lm;->a()Z

    move-result v3

    if-nez v3, :cond_1

    .line 804671
    sget-object v2, LX/4lh;->a:Ljava/lang/Class;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "check fail "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 804672
    const/4 v1, 0x0

    .line 804673
    :goto_1
    move v0, v1

    .line 804674
    if-eqz v0, :cond_2

    .line 804675
    new-instance v0, LX/4lj;

    .line 804676
    iget-object v1, p0, LX/4lh;->k:Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;

    invoke-virtual {v1}, Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;->a()Z

    move-result v1

    if-eqz v1, :cond_5
    :try_end_0
    .catch LX/4ll; {:try_start_0 .. :try_end_0} :catch_0

    .line 804677
    :try_start_1
    const-string v1, "TLS"

    invoke-static {v1}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v1

    .line 804678
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 804679
    iget-object v2, p0, LX/4lh;->k:Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;

    invoke-virtual {v2, v1}, Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;->a(Ljavax/net/ssl/SSLContext;)V

    .line 804680
    invoke-virtual {v1}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catch LX/4ll; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    move-result-object v1

    .line 804681
    :goto_2
    move-object v1, v1

    .line 804682
    iget-object v2, p0, LX/4lh;->i:LX/4ly;

    iget-object v3, p0, LX/4lh;->e:LX/4lu;

    iget-object v4, p0, LX/4lh;->f:LX/4lv;

    iget-object v5, p0, LX/4lh;->g:LX/4lw;

    iget-object v6, p0, LX/4lh;->h:LX/4lk;

    iget v7, p0, LX/4lh;->b:I

    iget-object v8, p0, LX/4lh;->j:LX/03V;

    invoke-direct/range {v0 .. v8}, LX/4lj;-><init>(Ljavax/net/ssl/SSLSocketFactory;LX/4ly;LX/4lu;LX/4lv;LX/4lw;LX/4lk;ILX/03V;)V
    :try_end_2
    .catch LX/4ll; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 804683
    :catch_0
    sget-object v0, LX/4lh;->a:Ljava/lang/Class;

    const-string v1, "exception occured while trying to create the socket factory"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 804684
    :cond_2
    iget-object v0, p0, LX/4lh;->k:Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;

    invoke-virtual {v0}, Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 804685
    iget-object v0, p0, LX/4lh;->j:LX/03V;

    const-string v1, "heartbeat_not_applied"

    const-string v2, "Did not apply heartbleed fix"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 804686
    :cond_3
    invoke-direct {p0, p1}, LX/4lh;->b(Lorg/apache/http/conn/scheme/SocketFactory;)Lorg/apache/http/conn/scheme/SocketFactory;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    :try_start_3
    const/4 v1, 0x1

    goto :goto_1
    :try_end_3
    .catch LX/4ll; {:try_start_3 .. :try_end_3} :catch_0

    :catch_1
    :cond_5
    invoke-static {}, Ljavax/net/ssl/HttpsURLConnection;->getDefaultSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    goto :goto_2
.end method
