.class public LX/4Oc;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 697467
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 697440
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_9

    .line 697441
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 697442
    :goto_0
    return v1

    .line 697443
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_6

    .line 697444
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 697445
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 697446
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v9, :cond_0

    .line 697447
    const-string v10, "field_type"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 697448
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    move-result-object v3

    move-object v8, v3

    move v3, v2

    goto :goto_1

    .line 697449
    :cond_1
    const-string v10, "name"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 697450
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 697451
    :cond_2
    const-string v10, "status"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 697452
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    move-result-object v0

    move-object v6, v0

    move v0, v2

    goto :goto_1

    .line 697453
    :cond_3
    const-string v10, "url"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 697454
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 697455
    :cond_4
    const-string v10, "cache_id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 697456
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 697457
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 697458
    :cond_6
    const/4 v9, 0x5

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 697459
    if-eqz v3, :cond_7

    .line 697460
    invoke-virtual {p1, v1, v8}, LX/186;->a(ILjava/lang/Enum;)V

    .line 697461
    :cond_7
    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 697462
    if-eqz v0, :cond_8

    .line 697463
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->a(ILjava/lang/Enum;)V

    .line 697464
    :cond_8
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 697465
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 697466
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v3, v1

    move v4, v1

    move v5, v1

    move-object v6, v0

    move v7, v1

    move-object v8, v0

    move v0, v1

    goto/16 :goto_1
.end method
