.class public LX/46n;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/view/WindowManager;

.field private b:Z

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/view/WindowManager;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/common/ui/util/IsInRotate270Group;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/common/ui/util/IsInRotate180Group;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/common/ui/util/IsInRotate90Group;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/WindowManager;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 671604
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 671605
    iput-object p1, p0, LX/46n;->a:Landroid/view/WindowManager;

    .line 671606
    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/46n;->d:Z

    .line 671607
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/46n;->c:Z

    .line 671608
    invoke-interface {p4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/46n;->b:Z

    .line 671609
    return-void
.end method

.method public static b(LX/0QB;)LX/46n;
    .locals 5

    .prologue
    .line 671610
    new-instance v1, LX/46n;

    invoke-static {p0}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    const/16 v2, 0x146e

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x146d

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x146f

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-direct {v1, v0, v2, v3, v4}, LX/46n;-><init>(Landroid/view/WindowManager;LX/0Or;LX/0Or;LX/0Or;)V

    .line 671611
    return-object v1
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 671612
    iget-boolean v0, p0, LX/46n;->b:Z

    if-eqz v0, :cond_0

    .line 671613
    iget-object v0, p0, LX/46n;->a:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    add-int/lit8 v0, v0, 0x5

    rem-int/lit8 v0, v0, 0x4

    .line 671614
    :goto_0
    return v0

    .line 671615
    :cond_0
    iget-boolean v0, p0, LX/46n;->c:Z

    if-eqz v0, :cond_1

    .line 671616
    iget-object v0, p0, LX/46n;->a:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    add-int/lit8 v0, v0, 0x6

    rem-int/lit8 v0, v0, 0x4

    goto :goto_0

    .line 671617
    :cond_1
    iget-boolean v0, p0, LX/46n;->d:Z

    if-eqz v0, :cond_2

    .line 671618
    iget-object v0, p0, LX/46n;->a:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    add-int/lit8 v0, v0, 0x7

    rem-int/lit8 v0, v0, 0x4

    goto :goto_0

    .line 671619
    :cond_2
    iget-object v0, p0, LX/46n;->a:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    goto :goto_0
.end method
