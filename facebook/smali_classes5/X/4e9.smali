.class public final LX/4e9;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 796946
    new-instance v0, LX/0U1;

    const-string v1, "resource_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/4e9;->a:LX/0U1;

    .line 796947
    new-instance v0, LX/0U1;

    const-string v1, "size_bytes"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/4e9;->b:LX/0U1;

    .line 796948
    new-instance v0, LX/0U1;

    const-string v1, "write_date"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/4e9;->c:LX/0U1;

    .line 796949
    new-instance v0, LX/0U1;

    const-string v1, "latest_hit_date"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/4e9;->d:LX/0U1;

    .line 796950
    new-instance v0, LX/0U1;

    const-string v1, "hit_count"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/4e9;->e:LX/0U1;

    .line 796951
    new-instance v0, LX/0U1;

    const-string v1, "original_analytics_tag"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/4e9;->f:LX/0U1;

    .line 796952
    new-instance v0, LX/0U1;

    const-string v1, "original_calling_class"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/4e9;->g:LX/0U1;

    .line 796953
    new-instance v0, LX/0U1;

    const-string v1, "original_feature_tag"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/4e9;->h:LX/0U1;

    return-void
.end method
