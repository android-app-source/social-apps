.class public LX/4v2;
.super LX/3KV;
.source ""

# interfaces
.implements LX/27U;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R::",
        "LX/2NW;",
        ">",
        "LX/3KV",
        "<TR;>;",
        "LX/27U",
        "<TR;>;"
    }
.end annotation


# instance fields
.field public a:LX/4sS;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4sS",
            "<-TR;+",
            "LX/2NW;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/4v2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4v2",
            "<+",
            "LX/2NW;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/3KH;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3KH",
            "<-TR;>;"
        }
    .end annotation
.end field

.field public d:LX/2wg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2wg",
            "<TR;>;"
        }
    .end annotation
.end field

.field public final e:Ljava/lang/Object;

.field public f:Lcom/google/android/gms/common/api/Status;

.field public final g:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/2wX;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/4v1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4v1;"
        }
    .end annotation
.end field

.field public i:Z


# direct methods
.method public static a$redex0(LX/4v2;Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    iget-object v1, p0, LX/4v2;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, LX/4v2;->f:Lcom/google/android/gms/common/api/Status;

    iget-object v0, p0, LX/4v2;->f:Lcom/google/android/gms/common/api/Status;

    invoke-static {p0, v0}, LX/4v2;->b(LX/4v2;Lcom/google/android/gms/common/api/Status;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static b(LX/2NW;)V
    .locals 6

    instance-of v1, p0, LX/2xc;

    if-eqz v1, :cond_0

    :try_start_0
    move-object v0, p0

    check-cast v0, LX/2xc;

    move-object v1, v0

    invoke-interface {v1}, LX/2xc;->nc_()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    const-string v2, "TransformedResultImpl"

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x12

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Unable to release "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static b(LX/4v2;Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    iget-object v1, p0, LX/4v2;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/4v2;->a:LX/4sS;

    if-eqz v0, :cond_1

    move-object v0, p1

    const-string v2, "onFailure must not return null"

    invoke-static {v0, v2}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, LX/4v2;->b:LX/4v2;

    invoke-static {v2, v0}, LX/4v2;->a$redex0(LX/4v2;Lcom/google/android/gms/common/api/Status;)V

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    invoke-direct {p0}, LX/4v2;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private c()Z
    .locals 2

    iget-object v0, p0, LX/4v2;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2wX;

    iget-object v1, p0, LX/4v2;->c:LX/3KH;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2NW;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    iget-object v1, p0, LX/4v2;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-interface {p1}, LX/2NW;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/4v2;->a:LX/4sS;

    if-eqz v0, :cond_1

    sget-object v0, LX/4ux;->a:Ljava/util/concurrent/ExecutorService;

    move-object v0, v0

    new-instance v2, Lcom/google/android/gms/internal/zzqx$1;

    invoke-direct {v2, p0, p1}, Lcom/google/android/gms/internal/zzqx$1;-><init>(LX/4v2;LX/2NW;)V

    const v3, -0x549dbc66

    invoke-static {v0, v2, v3}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    invoke-direct {p0}, LX/4v2;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_2
    invoke-interface {p1}, LX/2NW;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-static {p0, v0}, LX/4v2;->a$redex0(LX/4v2;Lcom/google/android/gms/common/api/Status;)V

    invoke-static {p1}, LX/4v2;->b(LX/2NW;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/2wg;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wg",
            "<*>;)V"
        }
    .end annotation

    iget-object v1, p0, LX/4v2;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, LX/4v2;->d:LX/2wg;

    iget-object v0, p0, LX/4v2;->a:LX/4sS;

    if-nez v0, :cond_1

    iget-object v0, p0, LX/4v2;->c:LX/3KH;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    iget-object v0, p0, LX/4v2;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2wX;

    iget-boolean p1, p0, LX/4v2;->i:Z

    if-nez p1, :cond_2

    iget-object p1, p0, LX/4v2;->a:LX/4sS;

    if-eqz p1, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0, p0}, LX/2wX;->a(LX/4v2;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4v2;->i:Z

    :cond_2
    iget-object v0, p0, LX/4v2;->f:Lcom/google/android/gms/common/api/Status;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/4v2;->f:Lcom/google/android/gms/common/api/Status;

    invoke-static {p0, v0}, LX/4v2;->b(LX/4v2;Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, LX/4v2;->d:LX/2wg;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4v2;->d:LX/2wg;

    invoke-virtual {v0, p0}, LX/2wg;->a(LX/27U;)V

    goto :goto_0
.end method
