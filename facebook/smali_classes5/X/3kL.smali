.class public final enum LX/3kL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3kL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3kL;

.field public static final enum END:LX/3kL;

.field public static final enum START:LX/3kL;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 632817
    new-instance v0, LX/3kL;

    const-string v1, "START"

    invoke-direct {v0, v1, v2}, LX/3kL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3kL;->START:LX/3kL;

    .line 632818
    new-instance v0, LX/3kL;

    const-string v1, "END"

    invoke-direct {v0, v1, v3}, LX/3kL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3kL;->END:LX/3kL;

    .line 632819
    const/4 v0, 0x2

    new-array v0, v0, [LX/3kL;

    sget-object v1, LX/3kL;->START:LX/3kL;

    aput-object v1, v0, v2

    sget-object v1, LX/3kL;->END:LX/3kL;

    aput-object v1, v0, v3

    sput-object v0, LX/3kL;->$VALUES:[LX/3kL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 632820
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3kL;
    .locals 1

    .prologue
    .line 632816
    const-class v0, LX/3kL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3kL;

    return-object v0
.end method

.method public static values()[LX/3kL;
    .locals 1

    .prologue
    .line 632815
    sget-object v0, LX/3kL;->$VALUES:[LX/3kL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3kL;

    return-object v0
.end method
