.class public abstract LX/4hh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/platform/common/action/PlatformAppCall;

.field public b:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 801780
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 801781
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 801776
    invoke-static {p1, p2, p3}, LX/4hf;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)LX/4hf;

    move-result-object v0

    .line 801777
    iget-object p1, v0, LX/4hf;->a:Landroid/os/Bundle;

    move-object v0, p1

    .line 801778
    iput-object v0, p0, LX/4hh;->b:Landroid/os/Bundle;

    .line 801779
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 801772
    invoke-static {p1, p2, p3}, LX/4hf;->b(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)LX/4hf;

    move-result-object v0

    .line 801773
    iget-object p1, v0, LX/4hf;->a:Landroid/os/Bundle;

    move-object v0, p1

    .line 801774
    iput-object v0, p0, LX/4hh;->b:Landroid/os/Bundle;

    .line 801775
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/platform/common/action/PlatformAppCall;
    .locals 1

    .prologue
    .line 801771
    iget-object v0, p0, LX/4hh;->a:Lcom/facebook/platform/common/action/PlatformAppCall;

    return-object v0
.end method

.method public abstract a(Landroid/content/Intent;)Z
.end method

.method public final a(Landroid/content/Intent;Ljava/lang/String;ZLX/4hg;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/lang/String;",
            "Z",
            "LX/4hg",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    .line 801714
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3, p4}, LX/4hh;->a(Landroid/os/Bundle;Ljava/lang/String;ZLX/4hg;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/content/Intent;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TExtra:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/content/Intent;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/Class",
            "<TTExtra;>;",
            "LX/4hg",
            "<",
            "Ljava/util/ArrayList",
            "<TTExtra;>;>;)Z"
        }
    .end annotation

    .prologue
    .line 801782
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/4hh;->a(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    return v0
.end method

.method public abstract a(Landroid/os/Bundle;)Z
.end method

.method public final a(Landroid/os/Bundle;Ljava/lang/String;ZLX/4hg;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            "Z",
            "LX/4hg",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    .line 801770
    const-class v4, Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, LX/4hh;->a(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TExtra:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/Class",
            "<TTExtra;>;",
            "LX/4hg",
            "<",
            "Ljava/util/ArrayList",
            "<TTExtra;>;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 801751
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 801752
    if-nez v1, :cond_0

    .line 801753
    invoke-virtual {p2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 801754
    :cond_0
    if-nez v1, :cond_2

    .line 801755
    if-eqz p3, :cond_1

    .line 801756
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p5, v0}, LX/4hg;->a(Ljava/lang/Object;)V

    .line 801757
    :goto_0
    return p3

    .line 801758
    :cond_1
    const-class v0, Ljava/util/ArrayList;

    invoke-direct {p0, p2, v0, v1}, LX/4hh;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 801759
    :cond_2
    instance-of v0, v1, Ljava/util/ArrayList;

    if-nez v0, :cond_3

    .line 801760
    const-class v0, Ljava/util/ArrayList;

    invoke-direct {p0, p2, v0, v1}, LX/4hh;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V

    move p3, v2

    .line 801761
    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 801762
    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_5

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    .line 801763
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {p4, v6}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 801764
    invoke-direct {p0, p2, p4, v5}, LX/4hh;->b(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V

    move p3, v2

    .line 801765
    goto :goto_0

    .line 801766
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 801767
    :cond_5
    check-cast v1, Ljava/util/ArrayList;

    .line 801768
    invoke-interface {p5, v1}, LX/4hg;->a(Ljava/lang/Object;)V

    .line 801769
    const/4 p3, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/facebook/platform/common/action/PlatformAppCall;Landroid/content/Intent;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 801729
    iput-object p1, p0, LX/4hh;->a:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 801730
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 801731
    iget-boolean v3, p1, Lcom/facebook/platform/common/action/PlatformAppCall;->c:Z

    move v3, v3

    .line 801732
    if-eqz v3, :cond_0

    .line 801733
    const-string v3, "com.facebook.platform.protocol.METHOD_ARGS"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    .line 801734
    invoke-virtual {p0, v3}, LX/4hh;->a(Landroid/os/Bundle;)Z

    move-result v3

    if-nez v3, :cond_1

    move v0, v1

    .line 801735
    :goto_0
    return v0

    .line 801736
    :cond_0
    invoke-virtual {p0, p2}, LX/4hh;->a(Landroid/content/Intent;)Z

    move-result v3

    if-nez v3, :cond_1

    move v0, v1

    .line 801737
    goto :goto_0

    .line 801738
    :cond_1
    const-string v3, "com.facebook.platform.protocol.PROTOCOL_VALIDATE"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 801739
    const-string v3, "com.facebook.platform.protocol.PROTOCOL_VALIDATE"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 801740
    instance-of v3, v0, Ljava/lang/Boolean;

    if-nez v3, :cond_2

    .line 801741
    const-string v2, "com.facebook.platform.protocol.PROTOCOL_VALIDATE"

    const-class v3, Ljava/lang/Boolean;

    invoke-direct {p0, v2, v3, v0}, LX/4hh;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V

    move v0, v1

    .line 801742
    goto :goto_0

    .line 801743
    :cond_2
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 801744
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 801745
    const-string v3, "com.facebook.platform.protocol.PROTOCOL_VERSION"

    const v4, 0x133060d

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 801746
    const-string v3, "com.facebook.platform.protocol.PROTOCOL_VALIDATED"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 801747
    iput-object v0, p0, LX/4hh;->b:Landroid/os/Bundle;

    .line 801748
    move v0, v1

    .line 801749
    goto :goto_0

    :cond_3
    move v0, v2

    .line 801750
    goto :goto_0
.end method

.method public final b(Landroid/content/Intent;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TExtra:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/content/Intent;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/Class",
            "<TTExtra;>;",
            "LX/4hg",
            "<TTExtra;>;)Z"
        }
    .end annotation

    .prologue
    .line 801728
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/4hh;->b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/Class;LX/4hg;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TExtra:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/Class",
            "<TTExtra;>;",
            "LX/4hg",
            "<TTExtra;>;)Z"
        }
    .end annotation

    .prologue
    .line 801715
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 801716
    if-nez v0, :cond_0

    .line 801717
    sget-object v0, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {p2, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 801718
    :cond_0
    if-nez v0, :cond_2

    .line 801719
    if-eqz p3, :cond_1

    .line 801720
    const/4 v0, 0x0

    invoke-interface {p5, v0}, LX/4hg;->a(Ljava/lang/Object;)V

    .line 801721
    :goto_0
    return p3

    .line 801722
    :cond_1
    invoke-direct {p0, p2, p4, v0}, LX/4hh;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 801723
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p4, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 801724
    invoke-direct {p0, p2, p4, v0}, LX/4hh;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V

    .line 801725
    const/4 p3, 0x0

    goto :goto_0

    .line 801726
    :cond_3
    invoke-interface {p5, v0}, LX/4hg;->a(Ljava/lang/Object;)V

    .line 801727
    const/4 p3, 0x1

    goto :goto_0
.end method
