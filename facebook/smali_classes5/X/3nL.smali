.class public LX/3nL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26t;


# instance fields
.field public final a:LX/0Ym;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0pG;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Sh;


# direct methods
.method public constructor <init>(LX/0Ym;LX/0Or;LX/0Sh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ym;",
            "LX/0Or",
            "<",
            "LX/0pG;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 638175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 638176
    iput-object p1, p0, LX/3nL;->a:LX/0Ym;

    .line 638177
    iput-object p2, p0, LX/3nL;->b:LX/0Or;

    .line 638178
    iput-object p3, p0, LX/3nL;->c:LX/0Sh;

    .line 638179
    return-void
.end method


# virtual methods
.method public final handleOperation(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 638180
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 638181
    const-string v1, "feed_clear_cache"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 638182
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 638183
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 638184
    const-string v2, "clearCacheResetFeedLoader"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 638185
    if-eqz v1, :cond_0

    .line 638186
    iget-object v1, p0, LX/3nL;->c:LX/0Sh;

    new-instance v2, Lcom/facebook/feed/data/FeedDataLoaderHandler$1;

    invoke-direct {v2, p0}, Lcom/facebook/feed/data/FeedDataLoaderHandler$1;-><init>(LX/3nL;)V

    invoke-virtual {v1, v2}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 638187
    :cond_0
    move-object v0, v0

    .line 638188
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method
