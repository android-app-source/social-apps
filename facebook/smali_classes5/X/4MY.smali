.class public LX/4MY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 688685
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 688686
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 688687
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 688688
    :goto_0
    return v1

    .line 688689
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 688690
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 688691
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 688692
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 688693
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 688694
    const-string v4, "cursor"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 688695
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 688696
    :cond_2
    const-string v4, "node"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 688697
    invoke-static {p0, p1}, LX/2aw;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 688698
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 688699
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 688700
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 688701
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 688702
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 688703
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 688704
    if-eqz v0, :cond_0

    .line 688705
    const-string v1, "cursor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688706
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 688707
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688708
    if-eqz v0, :cond_1

    .line 688709
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688710
    invoke-static {p0, v0, p2, p3}, LX/2aw;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 688711
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 688712
    return-void
.end method
