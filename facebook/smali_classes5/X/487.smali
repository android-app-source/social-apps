.class public LX/487;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Ha;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/compactdisk/DiskCache;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/compactdisk/DiskCache;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 672696
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 672697
    iput-object p1, p0, LX/487;->a:LX/0Or;

    .line 672698
    return-void
.end method

.method private a(Ljava/lang/String;)LX/1gI;
    .locals 2

    .prologue
    .line 672728
    iget-object v0, p0, LX/487;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v0, p1}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->fetchPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 672729
    if-eqz v0, :cond_0

    .line 672730
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, LX/1gH;->a(Ljava/io/File;)LX/1gH;

    move-result-object v0

    .line 672731
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final U_()V
    .locals 0

    .prologue
    .line 672727
    return-void
.end method

.method public final a(J)J
    .locals 2

    .prologue
    .line 672726
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final a(LX/1bh;)LX/1gI;
    .locals 2

    .prologue
    .line 672721
    invoke-static {p1}, LX/1gD;->a(LX/1bh;)Ljava/util/List;

    move-result-object v0

    .line 672722
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 672723
    invoke-direct {p0, v0}, LX/487;->a(Ljava/lang/String;)LX/1gI;

    move-result-object v0

    .line 672724
    if-eqz v0, :cond_0

    .line 672725
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1bh;LX/2uu;)LX/1gI;
    .locals 3

    .prologue
    .line 672718
    invoke-static {p1}, LX/1gD;->b(LX/1bh;)Ljava/lang/String;

    move-result-object v1

    .line 672719
    iget-object v0, p0, LX/487;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/compactdisk/DiskCache;

    new-instance v2, LX/486;

    invoke-direct {v2, p0, p2}, LX/486;-><init>(LX/487;LX/2uu;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->storeToPath(Ljava/lang/String;Lcom/facebook/compactdisk/WriteCallback;)V

    .line 672720
    invoke-direct {p0, v1}, LX/487;->a(Ljava/lang/String;)LX/1gI;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 672732
    iget-object v0, p0, LX/487;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v0}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->clear()V

    .line 672733
    return-void
.end method

.method public final b(LX/1bh;)Z
    .locals 3

    .prologue
    .line 672712
    invoke-static {p1}, LX/1gD;->a(LX/1bh;)Ljava/util/List;

    move-result-object v0

    .line 672713
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 672714
    iget-object v1, p0, LX/487;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v1, v0}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->hasKeyDeep(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 672715
    iget-object v1, p0, LX/487;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v1, v0}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->updateLastAccessDate(Ljava/lang/String;)V

    .line 672716
    const/4 v0, 0x1

    .line 672717
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 672711
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final c(LX/1bh;)Z
    .locals 3

    .prologue
    .line 672706
    invoke-static {p1}, LX/1gD;->a(LX/1bh;)Ljava/util/List;

    move-result-object v0

    .line 672707
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 672708
    iget-object v1, p0, LX/487;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v1, v0}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->hasKeyMem(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 672709
    const/4 v0, 0x1

    .line 672710
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 672704
    iget-object v0, p0, LX/487;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v0}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->clear()V

    .line 672705
    return-void
.end method

.method public final d(LX/1bh;)Z
    .locals 3

    .prologue
    .line 672699
    invoke-static {p1}, LX/1gD;->a(LX/1bh;)Ljava/util/List;

    move-result-object v0

    .line 672700
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 672701
    iget-object v1, p0, LX/487;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v1, v0}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->hasKeyDeep(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 672702
    const/4 v0, 0x1

    .line 672703
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
