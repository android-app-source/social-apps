.class public abstract LX/4A8;
.super LX/39O;
.source ""

# interfaces
.implements LX/3Sb;


# instance fields
.field public transient a:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 675326
    invoke-direct {p0}, LX/39O;-><init>()V

    .line 675327
    return-void
.end method


# virtual methods
.method public abstract a(I)LX/1vs;
.end method

.method public a(ILX/15i;II)V
    .locals 1

    .prologue
    .line 675328
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(LX/15i;II)Z
    .locals 1

    .prologue
    .line 675329
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v0

    invoke-virtual {p0, v0, p1, p2, p3}, LX/4A8;->a(ILX/15i;II)V

    .line 675330
    const/4 v0, 0x1

    return v0
.end method

.method public b()LX/2sN;
    .locals 1

    .prologue
    .line 675331
    new-instance v0, LX/4AA;

    invoke-direct {v0, p0}, LX/4AA;-><init>(LX/4A8;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 675332
    if-ne p0, p1, :cond_1

    .line 675333
    :cond_0
    :goto_0
    return v0

    .line 675334
    :cond_1
    :try_start_0
    check-cast p1, LX/3Sb;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 675335
    invoke-interface {p1}, LX/3Sb;->c()I

    move-result v2

    invoke-virtual {p0}, LX/39O;->c()I

    move-result v3

    if-eq v2, v3, :cond_2

    move v0, v1

    .line 675336
    goto :goto_0

    .line 675337
    :catch_0
    move v0, v1

    goto :goto_0

    .line 675338
    :cond_2
    invoke-virtual {p0}, LX/39O;->b()LX/2sN;

    move-result-object v2

    .line 675339
    invoke-interface {p1}, LX/3Sb;->b()LX/2sN;

    move-result-object v3

    .line 675340
    :cond_3
    invoke-interface {v2}, LX/2sN;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 675341
    invoke-interface {v2}, LX/2sN;->b()LX/1vs;

    move-result-object v4

    .line 675342
    iget-object v5, v4, LX/1vs;->a:LX/15i;

    .line 675343
    iget v6, v4, LX/1vs;->b:I

    .line 675344
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    .line 675345
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 675346
    invoke-interface {v3}, LX/2sN;->b()LX/1vs;

    move-result-object v4

    .line 675347
    iget-object v7, v4, LX/1vs;->a:LX/15i;

    .line 675348
    iget v8, v4, LX/1vs;->b:I

    .line 675349
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    .line 675350
    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 675351
    if-nez v6, :cond_4

    if-eqz v8, :cond_3

    :goto_1
    move v0, v1

    .line 675352
    goto :goto_0

    .line 675353
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 675354
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 675355
    :cond_4
    invoke-static {v5, v6, v7, v8}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v4

    if-nez v4, :cond_3

    goto :goto_1
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 675356
    const/4 v0, 0x1

    .line 675357
    invoke-virtual {p0}, LX/39O;->b()LX/2sN;

    move-result-object v1

    .line 675358
    :goto_0
    invoke-interface {v1}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 675359
    invoke-interface {v1}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    .line 675360
    iget-object v3, v2, LX/1vs;->a:LX/15i;

    .line 675361
    iget v4, v2, LX/1vs;->b:I

    .line 675362
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 675363
    :try_start_0
    monitor-exit v2

    .line 675364
    mul-int/lit8 v2, v0, 0x1f

    if-nez v4, :cond_0

    const/4 v0, 0x0

    :goto_1
    add-int/2addr v0, v2

    .line 675365
    goto :goto_0

    .line 675366
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 675367
    :cond_0
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v0, v4

    goto :goto_1

    .line 675368
    :cond_1
    return v0
.end method
