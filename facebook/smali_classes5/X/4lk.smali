.class public LX/4lk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/4lk;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 804723
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 804724
    return-void
.end method

.method public static a(LX/0QB;)LX/4lk;
    .locals 3

    .prologue
    .line 804725
    sget-object v0, LX/4lk;->a:LX/4lk;

    if-nez v0, :cond_1

    .line 804726
    const-class v1, LX/4lk;

    monitor-enter v1

    .line 804727
    :try_start_0
    sget-object v0, LX/4lk;->a:LX/4lk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 804728
    if-eqz v2, :cond_0

    .line 804729
    :try_start_1
    new-instance v0, LX/4lk;

    invoke-direct {v0}, LX/4lk;-><init>()V

    .line 804730
    move-object v0, v0

    .line 804731
    sput-object v0, LX/4lk;->a:LX/4lk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 804732
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 804733
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 804734
    :cond_1
    sget-object v0, LX/4lk;->a:LX/4lk;

    return-object v0

    .line 804735
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 804736
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/net/Socket;Ljava/lang/String;IZLorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;LX/03V;)Lcom/facebook/ssl/openssl/TicketEnabledOpenSSLSocketImplWrapper;
    .locals 7

    .prologue
    .line 804737
    new-instance v0, Lcom/facebook/ssl/openssl/TicketEnabledOpenSSLSocketImplWrapper;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/ssl/openssl/TicketEnabledOpenSSLSocketImplWrapper;-><init>(Ljava/net/Socket;Ljava/lang/String;IZLorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;LX/03V;)V

    return-object v0
.end method
