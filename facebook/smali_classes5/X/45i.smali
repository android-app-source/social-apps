.class public LX/45i;
.super LX/45a;
.source ""


# instance fields
.field private final a:LX/3DY;

.field private b:J


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;LX/3DY;)V
    .locals 2

    .prologue
    .line 670521
    invoke-direct {p0, p1}, LX/45a;-><init>(Ljava/io/OutputStream;)V

    .line 670522
    iput-object p2, p0, LX/45i;->a:LX/3DY;

    .line 670523
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/45i;->b:J

    .line 670524
    return-void
.end method

.method private a(J)V
    .locals 5

    .prologue
    .line 670518
    iget-wide v0, p0, LX/45i;->b:J

    add-long/2addr v0, p1

    iput-wide v0, p0, LX/45i;->b:J

    .line 670519
    iget-object v0, p0, LX/45i;->a:LX/3DY;

    iget-wide v2, p0, LX/45i;->b:J

    invoke-interface {v0, p1, p2, v2, v3}, LX/3DY;->a(JJ)V

    .line 670520
    return-void
.end method


# virtual methods
.method public final write(I)V
    .locals 2

    .prologue
    .line 670515
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 670516
    const-wide/16 v0, 0x1

    invoke-direct {p0, v0, v1}, LX/45i;->a(J)V

    .line 670517
    return-void
.end method

.method public final write([BII)V
    .locals 2

    .prologue
    .line 670512
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 670513
    int-to-long v0, p3

    invoke-direct {p0, v0, v1}, LX/45i;->a(J)V

    .line 670514
    return-void
.end method
