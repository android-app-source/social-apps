.class public LX/4Tr;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 719797
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 719798
    const/4 v15, 0x0

    .line 719799
    const/4 v14, 0x0

    .line 719800
    const/4 v13, 0x0

    .line 719801
    const/4 v12, 0x0

    .line 719802
    const/4 v11, 0x0

    .line 719803
    const/4 v10, 0x0

    .line 719804
    const/4 v9, 0x0

    .line 719805
    const/4 v8, 0x0

    .line 719806
    const/4 v7, 0x0

    .line 719807
    const/4 v6, 0x0

    .line 719808
    const/4 v5, 0x0

    .line 719809
    const/4 v4, 0x0

    .line 719810
    const/4 v3, 0x0

    .line 719811
    const/4 v2, 0x0

    .line 719812
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_1

    .line 719813
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 719814
    const/4 v2, 0x0

    .line 719815
    :goto_0
    return v2

    .line 719816
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 719817
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_e

    .line 719818
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v16

    .line 719819
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 719820
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    if-eqz v16, :cond_1

    .line 719821
    const-string v17, "collections"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 719822
    invoke-static/range {p0 .. p1}, LX/4Tq;->a(LX/15w;LX/186;)I

    move-result v15

    goto :goto_1

    .line 719823
    :cond_2
    const-string v17, "collectionsNoItems"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 719824
    invoke-static/range {p0 .. p1}, LX/4Tq;->a(LX/15w;LX/186;)I

    move-result v14

    goto :goto_1

    .line 719825
    :cond_3
    const-string v17, "collectionsPeek"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 719826
    invoke-static/range {p0 .. p1}, LX/4Tq;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 719827
    :cond_4
    const-string v17, "icon_image"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 719828
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 719829
    :cond_5
    const-string v17, "id"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 719830
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 719831
    :cond_6
    const-string v17, "name"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 719832
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 719833
    :cond_7
    const-string v17, "section_type"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 719834
    const/4 v2, 0x1

    .line 719835
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v9

    goto/16 :goto_1

    .line 719836
    :cond_8
    const-string v17, "standalone_url"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 719837
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 719838
    :cond_9
    const-string v17, "subtitle"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 719839
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 719840
    :cond_a
    const-string v17, "title"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 719841
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 719842
    :cond_b
    const-string v17, "titleForSummary"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_c

    .line 719843
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 719844
    :cond_c
    const-string v17, "tracking"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_d

    .line 719845
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 719846
    :cond_d
    const-string v17, "url"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 719847
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 719848
    :cond_e
    const/16 v16, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 719849
    const/16 v16, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 719850
    const/4 v15, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 719851
    const/4 v14, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 719852
    const/4 v13, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 719853
    const/4 v12, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 719854
    const/4 v11, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 719855
    if-eqz v2, :cond_f

    .line 719856
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->a(ILjava/lang/Enum;)V

    .line 719857
    :cond_f
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 719858
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 719859
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 719860
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 719861
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 719862
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 719863
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x7

    .line 719864
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 719865
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 719866
    if-eqz v0, :cond_0

    .line 719867
    const-string v1, "collections"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 719868
    invoke-static {p0, v0, p2, p3}, LX/4Tq;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 719869
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 719870
    if-eqz v0, :cond_1

    .line 719871
    const-string v1, "collectionsNoItems"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 719872
    invoke-static {p0, v0, p2, p3}, LX/4Tq;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 719873
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 719874
    if-eqz v0, :cond_2

    .line 719875
    const-string v1, "collectionsPeek"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 719876
    invoke-static {p0, v0, p2, p3}, LX/4Tq;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 719877
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 719878
    if-eqz v0, :cond_3

    .line 719879
    const-string v1, "icon_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 719880
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 719881
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 719882
    if-eqz v0, :cond_4

    .line 719883
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 719884
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 719885
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 719886
    if-eqz v0, :cond_5

    .line 719887
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 719888
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 719889
    :cond_5
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->a(IIS)S

    move-result v0

    .line 719890
    if-eqz v0, :cond_6

    .line 719891
    const-string v0, "section_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 719892
    const-class v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 719893
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 719894
    if-eqz v0, :cond_7

    .line 719895
    const-string v1, "standalone_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 719896
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 719897
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 719898
    if-eqz v0, :cond_8

    .line 719899
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 719900
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 719901
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 719902
    if-eqz v0, :cond_9

    .line 719903
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 719904
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 719905
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 719906
    if-eqz v0, :cond_a

    .line 719907
    const-string v1, "titleForSummary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 719908
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 719909
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 719910
    if-eqz v0, :cond_b

    .line 719911
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 719912
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 719913
    :cond_b
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 719914
    if-eqz v0, :cond_c

    .line 719915
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 719916
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 719917
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 719918
    return-void
.end method
