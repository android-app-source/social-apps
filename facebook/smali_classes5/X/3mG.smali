.class public LX/3mG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1CX;

.field public final b:LX/0kL;

.field public final c:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/1CX;LX/0kL;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 635776
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 635777
    iput-object p1, p0, LX/3mG;->a:LX/1CX;

    .line 635778
    iput-object p2, p0, LX/3mG;->b:LX/0kL;

    .line 635779
    iput-object p3, p0, LX/3mG;->c:Landroid/content/res/Resources;

    .line 635780
    return-void
.end method

.method public static b(LX/0QB;)LX/3mG;
    .locals 4

    .prologue
    .line 635781
    new-instance v3, LX/3mG;

    invoke-static {p0}, LX/1CX;->a(LX/0QB;)LX/1CX;

    move-result-object v0

    check-cast v0, LX/1CX;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v1

    check-cast v1, LX/0kL;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-direct {v3, v0, v1, v2}, LX/3mG;-><init>(LX/1CX;LX/0kL;Landroid/content/res/Resources;)V

    .line 635782
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 635783
    const v0, 0x7f080039

    .line 635784
    const/4 v1, 0x0

    .line 635785
    instance-of v2, p1, LX/4Ua;

    if-nez v2, :cond_2

    .line 635786
    :cond_0
    :goto_0
    move-object v1, v1

    .line 635787
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 635788
    iget-object v2, p0, LX/3mG;->a:LX/1CX;

    iget-object v3, p0, LX/3mG;->c:Landroid/content/res/Resources;

    invoke-static {v3}, LX/4mm;->a(Landroid/content/res/Resources;)LX/4mn;

    move-result-object v3

    const/4 v4, 0x0

    .line 635789
    iput-object v4, v3, LX/4mn;->b:Ljava/lang/String;

    .line 635790
    move-object v3, v3

    .line 635791
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 635792
    iput-object v1, v3, LX/4mn;->c:Ljava/lang/String;

    .line 635793
    move-object v1, v3

    .line 635794
    invoke-virtual {v1}, LX/4mn;->l()LX/4mm;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1CX;->a(LX/4mm;)LX/2EJ;

    .line 635795
    :goto_1
    return-void

    .line 635796
    :cond_1
    iget-object v1, p0, LX/3mG;->b:LX/0kL;

    new-instance v2, LX/27k;

    invoke-direct {v2, v0}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_1

    .line 635797
    :cond_2
    check-cast p1, LX/4Ua;

    .line 635798
    iget-object v2, p1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v2, v2

    .line 635799
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
