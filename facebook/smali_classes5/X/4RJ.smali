.class public LX/4RJ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 708518
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 708530
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 708531
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 708532
    :goto_0
    return v1

    .line 708533
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 708534
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 708535
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 708536
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 708537
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 708538
    const-string v3, "edges"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 708539
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 708540
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_2

    .line 708541
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_2

    .line 708542
    invoke-static {p0, p1}, LX/4RK;->b(LX/15w;LX/186;)I

    move-result v2

    .line 708543
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 708544
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 708545
    goto :goto_1

    .line 708546
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 708547
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 708548
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 708519
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 708520
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 708521
    if-eqz v0, :cond_1

    .line 708522
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708523
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 708524
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    if-ge v1, p1, :cond_0

    .line 708525
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result p1

    invoke-static {p0, p1, p2, p3}, LX/4RK;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 708526
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 708527
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 708528
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 708529
    return-void
.end method
