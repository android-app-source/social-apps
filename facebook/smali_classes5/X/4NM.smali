.class public LX/4NM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 692280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 692149
    const/16 v16, 0x0

    .line 692150
    const/4 v15, 0x0

    .line 692151
    const/4 v14, 0x0

    .line 692152
    const-wide/16 v12, 0x0

    .line 692153
    const/4 v11, 0x0

    .line 692154
    const/4 v10, 0x0

    .line 692155
    const/4 v9, 0x0

    .line 692156
    const/4 v8, 0x0

    .line 692157
    const/4 v7, 0x0

    .line 692158
    const/4 v6, 0x0

    .line 692159
    const/4 v5, 0x0

    .line 692160
    const/4 v4, 0x0

    .line 692161
    const/4 v3, 0x0

    .line 692162
    const/4 v2, 0x0

    .line 692163
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_10

    .line 692164
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 692165
    const/4 v2, 0x0

    .line 692166
    :goto_0
    return v2

    .line 692167
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_d

    .line 692168
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 692169
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 692170
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v19, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v19

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 692171
    const-string v6, "anniversary_campaign"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 692172
    invoke-static/range {p0 .. p1}, LX/4NL;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 692173
    :cond_1
    const-string v6, "cache_id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 692174
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto :goto_1

    .line 692175
    :cond_2
    const-string v6, "debug_info"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 692176
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 692177
    :cond_3
    const-string v6, "fetchTimeMs"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 692178
    const/4 v2, 0x1

    .line 692179
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 692180
    :cond_4
    const-string v6, "hideable_token"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 692181
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v16, v2

    goto :goto_1

    .line 692182
    :cond_5
    const-string v6, "local_last_negative_feedback_action_type"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 692183
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 692184
    :cond_6
    const-string v6, "local_story_visibility"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 692185
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 692186
    :cond_7
    const-string v6, "local_story_visible_height"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 692187
    const/4 v2, 0x1

    .line 692188
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move v13, v6

    goto/16 :goto_1

    .line 692189
    :cond_8
    const-string v6, "negative_feedback_actions"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 692190
    invoke-static/range {p0 .. p1}, LX/2bY;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 692191
    :cond_9
    const-string v6, "render_style"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 692192
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 692193
    :cond_a
    const-string v6, "short_term_cache_key"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 692194
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 692195
    :cond_b
    const-string v6, "tracking"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 692196
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v9, v2

    goto/16 :goto_1

    .line 692197
    :cond_c
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 692198
    :cond_d
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 692199
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 692200
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 692201
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 692202
    if-eqz v3, :cond_e

    .line 692203
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 692204
    :cond_e
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 692205
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 692206
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 692207
    if-eqz v8, :cond_f

    .line 692208
    const/4 v2, 0x7

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13, v3}, LX/186;->a(III)V

    .line 692209
    :cond_f
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 692210
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 692211
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 692212
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 692213
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_10
    move/from16 v17, v15

    move/from16 v18, v16

    move v15, v10

    move/from16 v16, v11

    move v10, v5

    move v11, v6

    move/from16 v20, v9

    move v9, v4

    move-wide v4, v12

    move v13, v8

    move v12, v7

    move v7, v14

    move v8, v2

    move/from16 v14, v20

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 692214
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 692215
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 692216
    invoke-static {p0, v2}, LX/4NM;->a(LX/15w;LX/186;)I

    move-result v1

    .line 692217
    if-eqz v0, :cond_0

    .line 692218
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 692219
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 692220
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 692221
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 692222
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 692223
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 692224
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 692225
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 692226
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692227
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 692228
    const-string v0, "name"

    const-string v1, "GoodwillAnniversaryCampaignFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 692229
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 692230
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 692231
    if-eqz v0, :cond_0

    .line 692232
    const-string v1, "anniversary_campaign"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692233
    invoke-static {p0, v0, p2, p3}, LX/4NL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 692234
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 692235
    if-eqz v0, :cond_1

    .line 692236
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692237
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 692238
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 692239
    if-eqz v0, :cond_2

    .line 692240
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692241
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 692242
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 692243
    cmp-long v2, v0, v4

    if-eqz v2, :cond_3

    .line 692244
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692245
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 692246
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 692247
    if-eqz v0, :cond_4

    .line 692248
    const-string v1, "hideable_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692249
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 692250
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 692251
    if-eqz v0, :cond_5

    .line 692252
    const-string v1, "local_last_negative_feedback_action_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692253
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 692254
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 692255
    if-eqz v0, :cond_6

    .line 692256
    const-string v1, "local_story_visibility"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692257
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 692258
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 692259
    if-eqz v0, :cond_7

    .line 692260
    const-string v1, "local_story_visible_height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692261
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 692262
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692263
    if-eqz v0, :cond_8

    .line 692264
    const-string v1, "negative_feedback_actions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692265
    invoke-static {p0, v0, p2, p3}, LX/2bY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 692266
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 692267
    if-eqz v0, :cond_9

    .line 692268
    const-string v1, "render_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692269
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 692270
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 692271
    if-eqz v0, :cond_a

    .line 692272
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692273
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 692274
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 692275
    if-eqz v0, :cond_b

    .line 692276
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692277
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 692278
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 692279
    return-void
.end method
