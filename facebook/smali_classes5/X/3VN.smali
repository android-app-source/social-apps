.class public LX/3VN;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field private final b:Lcom/facebook/photos/warning/ObjectionableContentWarningView;

.field private final c:Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;

.field private final d:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587776
    new-instance v0, LX/3VO;

    invoke-direct {v0}, LX/3VO;-><init>()V

    sput-object v0, LX/3VN;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 587802
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3VN;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 587803
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587794
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3VN;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 587795
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587796
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 587797
    const v0, 0x7f030c4b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 587798
    const v0, 0x7f0d1e06

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;

    iput-object v0, p0, LX/3VN;->b:Lcom/facebook/photos/warning/ObjectionableContentWarningView;

    .line 587799
    const v0, 0x7f0d1e07

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;

    iput-object v0, p0, LX/3VN;->c:Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;

    .line 587800
    const v0, 0x7f0d1e08

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/3VN;->d:Landroid/view/View;

    .line 587801
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 587788
    iget-object v0, p0, LX/3VN;->c:Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 587789
    iget-object v0, p0, LX/3VN;->c:Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;->setVisibility(I)V

    .line 587790
    iget-object v0, p0, LX/3VN;->b:Lcom/facebook/photos/warning/ObjectionableContentWarningView;

    invoke-virtual {v0}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->a()V

    .line 587791
    iget-object v0, p0, LX/3VN;->d:Landroid/view/View;

    invoke-static {v0, v2}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 587792
    iget-object v0, p0, LX/3VN;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 587793
    return-void
.end method

.method public final a(LX/BJ4;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 587778
    if-nez p1, :cond_0

    .line 587779
    :goto_0
    return-void

    .line 587780
    :cond_0
    iget-boolean v0, p1, LX/BJ4;->b:Z

    if-eqz v0, :cond_1

    .line 587781
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/3VN;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a00f7

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 587782
    iget-object v1, p0, LX/3VN;->d:Landroid/view/View;

    invoke-static {v1, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 587783
    iget-object v0, p0, LX/3VN;->c:Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;

    invoke-virtual {v0, v4}, Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;->setVisibility(I)V

    .line 587784
    iget-object v0, p0, LX/3VN;->d:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 587785
    :cond_1
    iget-object v0, p0, LX/3VN;->c:Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;

    iget-object v1, p1, LX/BJ4;->a:LX/1aZ;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 587786
    iget-object v0, p0, LX/3VN;->c:Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;

    invoke-virtual {v0, v3}, Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;->setVisibility(I)V

    .line 587787
    iget-object v0, p0, LX/3VN;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public getPhotoAttachmentView()Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;
    .locals 1

    .prologue
    .line 587777
    iget-object v0, p0, LX/3VN;->c:Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;

    return-object v0
.end method
