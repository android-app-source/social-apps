.class public LX/406;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2DH;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/2DH",
        "<",
        "Ljava/io/ByteArrayOutputStream;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field private static final a:J


# instance fields
.field private final b:Landroid/content/Context;

.field public final c:LX/0pC;

.field public final d:Lcom/facebook/flexiblesampling/SamplingPolicyConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:Lcom/facebook/analytics2/logger/HandlerThreadFactory;

.field public final f:LX/2Y2;

.field private g:LX/405;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/io/ByteArrayOutputStream;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Lcom/facebook/analytics2/logger/Uploader;

.field private j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 662927
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3c

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, LX/406;->a:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Zh;LX/0pC;)V
    .locals 4

    .prologue
    .line 662894
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 662895
    iput-object p1, p0, LX/406;->b:Landroid/content/Context;

    .line 662896
    iput-object p3, p0, LX/406;->c:LX/0pC;

    .line 662897
    :try_start_0
    iget-object v0, p0, LX/406;->b:Landroid/content/Context;

    invoke-static {v0}, LX/0pF;->a(Landroid/content/Context;)LX/0pF;

    move-result-object v0

    iget-object v1, p0, LX/406;->c:LX/0pC;

    iget-object v1, v1, LX/0pC;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0pF;->c(Ljava/lang/String;)Lcom/facebook/analytics2/logger/Uploader;

    move-result-object v0

    iput-object v0, p0, LX/406;->i:Lcom/facebook/analytics2/logger/Uploader;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    .line 662898
    iget-object v0, p0, LX/406;->c:LX/0pC;

    iget-object v0, v0, LX/0pC;->b:Ljava/lang/Class;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LX/406;->d:Lcom/facebook/flexiblesampling/SamplingPolicyConfig;

    .line 662899
    iget-object v0, p0, LX/406;->b:Landroid/content/Context;

    invoke-static {v0}, LX/0pF;->a(Landroid/content/Context;)LX/0pF;

    move-result-object v0

    iget-object v1, p0, LX/406;->c:LX/0pC;

    iget-object v1, v1, LX/0pC;->c:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0pF;->b(Ljava/lang/String;)Lcom/facebook/analytics2/logger/HandlerThreadFactory;

    move-result-object v0

    iput-object v0, p0, LX/406;->e:Lcom/facebook/analytics2/logger/HandlerThreadFactory;

    .line 662900
    new-instance v0, LX/2Y2;

    iget-object v1, p0, LX/406;->b:Landroid/content/Context;

    iget-object v2, p0, LX/406;->c:LX/0pC;

    iget-object v2, v2, LX/0pC;->e:Ljava/lang/String;

    iget-object v3, p0, LX/406;->d:Lcom/facebook/flexiblesampling/SamplingPolicyConfig;

    invoke-direct {v0, v1, p2, v2, v3}, LX/2Y2;-><init>(Landroid/content/Context;LX/0Zh;Ljava/lang/String;Lcom/facebook/flexiblesampling/SamplingPolicyConfig;)V

    iput-object v0, p0, LX/406;->f:LX/2Y2;

    .line 662901
    return-void

    .line 662902
    :catch_0
    move-exception v0

    .line 662903
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to create instance of "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/406;->c:LX/0pC;

    iget-object v3, v3, LX/0pC;->a:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 662904
    :catch_1
    move-exception v0

    .line 662905
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to create instance of "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/406;->c:LX/0pC;

    iget-object v3, v3, LX/0pC;->a:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 662906
    :catch_2
    move-exception v0

    .line 662907
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to create instance of "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/406;->c:LX/0pC;

    iget-object v3, v3, LX/0pC;->a:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 662908
    :catch_3
    move-exception v0

    .line 662909
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to create instance of "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/406;->c:LX/0pC;

    iget-object v3, v3, LX/0pC;->a:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 662910
    :cond_0
    iget-object v0, p0, LX/406;->b:Landroid/content/Context;

    invoke-static {v0}, LX/0pF;->a(Landroid/content/Context;)LX/0pF;

    move-result-object v0

    iget-object v1, p0, LX/406;->c:LX/0pC;

    iget-object v1, v1, LX/0pC;->b:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0pF;->a(Ljava/lang/String;)Lcom/facebook/flexiblesampling/SamplingPolicyConfig;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 662928
    iget-object v0, p0, LX/406;->h:Ljava/io/ByteArrayOutputStream;

    if-nez v0, :cond_0

    .line 662929
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "mByteArrayOutputStream is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 662930
    :cond_0
    return-void
.end method

.method private d()LX/405;
    .locals 3

    .prologue
    .line 662911
    iget-object v0, p0, LX/406;->g:LX/405;

    if-nez v0, :cond_0

    .line 662912
    iget-object v0, p0, LX/406;->c:LX/0pC;

    iget-object v0, v0, LX/0pC;->d:LX/0pD;

    sget-object v1, LX/0pD;->HIGH:LX/0pD;

    if-ne v0, v1, :cond_1

    .line 662913
    const/4 v1, 0x0

    .line 662914
    const-string v0, "Analytics-HighPri-InMemory-Scheduler"

    .line 662915
    :goto_0
    iget-object v2, p0, LX/406;->e:Lcom/facebook/analytics2/logger/HandlerThreadFactory;

    invoke-interface {v2, v0, v1}, Lcom/facebook/analytics2/logger/HandlerThreadFactory;->a(Ljava/lang/String;I)Landroid/os/HandlerThread;

    move-result-object v0

    .line 662916
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 662917
    new-instance v1, LX/405;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, p0, v0}, LX/405;-><init>(LX/406;Landroid/os/Looper;)V

    iput-object v1, p0, LX/406;->g:LX/405;

    .line 662918
    :cond_0
    iget-object v0, p0, LX/406;->g:LX/405;

    return-object v0

    .line 662919
    :cond_1
    const/16 v1, 0xa

    .line 662920
    const-string v0, "Analytics-NormalPri-InMemory-Scheduler"

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 662921
    invoke-direct {p0}, LX/406;->c()V

    .line 662922
    iget-boolean v0, p0, LX/406;->j:Z

    if-nez v0, :cond_0

    .line 662923
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/406;->j:Z

    .line 662924
    invoke-direct {p0}, LX/406;->d()LX/405;

    move-result-object v0

    iget-object v1, p0, LX/406;->h:Ljava/io/ByteArrayOutputStream;

    sget-wide v2, LX/406;->a:J

    .line 662925
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v1}, LX/405;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v0, v4, v2, v3}, LX/405;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 662926
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 662886
    check-cast p1, Ljava/io/ByteArrayOutputStream;

    .line 662887
    iget-object v0, p0, LX/406;->h:Ljava/io/ByteArrayOutputStream;

    if-eq v0, p1, :cond_0

    .line 662888
    iput-object p1, p0, LX/406;->h:Ljava/io/ByteArrayOutputStream;

    .line 662889
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/406;->j:Z

    .line 662890
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 662883
    iget-object v0, p0, LX/406;->h:Ljava/io/ByteArrayOutputStream;

    if-eqz v0, :cond_0

    .line 662884
    invoke-direct {p0}, LX/406;->d()LX/405;

    move-result-object v0

    iget-object v1, p0, LX/406;->h:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, v1}, LX/405;->a(Ljava/io/ByteArrayOutputStream;)V

    .line 662885
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 662891
    invoke-direct {p0}, LX/406;->c()V

    .line 662892
    invoke-direct {p0}, LX/406;->d()LX/405;

    move-result-object v0

    iget-object v1, p0, LX/406;->h:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, v1}, LX/405;->a(Ljava/io/ByteArrayOutputStream;)V

    .line 662893
    return-void
.end method
