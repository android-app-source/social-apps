.class public final LX/4Xn;
.super LX/0ur;
.source ""


# instance fields
.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:J

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:I

.field public j:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 772393
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 772394
    const/4 v0, 0x0

    iput-object v0, p0, LX/4Xn;->p:LX/0x2;

    .line 772395
    instance-of v0, p0, LX/4Xn;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 772396
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;)LX/4Xn;
    .locals 4

    .prologue
    .line 772397
    new-instance v1, LX/4Xn;

    invoke-direct {v1}, LX/4Xn;-><init>()V

    .line 772398
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 772399
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xn;->b:Ljava/lang/String;

    .line 772400
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->E_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xn;->c:Ljava/lang/String;

    .line 772401
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->F_()J

    move-result-wide v2

    iput-wide v2, v1, LX/4Xn;->d:J

    .line 772402
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->r()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xn;->e:Ljava/lang/String;

    .line 772403
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->s()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xn;->f:Ljava/lang/String;

    .line 772404
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xn;->g:Ljava/lang/String;

    .line 772405
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->w()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xn;->h:Ljava/lang/String;

    .line 772406
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->x()I

    move-result v0

    iput v0, v1, LX/4Xn;->i:I

    .line 772407
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;

    move-result-object v0

    iput-object v0, v1, LX/4Xn;->j:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;

    .line 772408
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->z()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xn;->k:Ljava/lang/String;

    .line 772409
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->A()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/4Xn;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 772410
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/4Xn;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 772411
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xn;->n:Ljava/lang/String;

    .line 772412
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xn;->o:Ljava/lang/String;

    .line 772413
    invoke-static {v1, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 772414
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->L_()LX/0x2;

    move-result-object v0

    invoke-virtual {v0}, LX/0x2;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x2;

    iput-object v0, v1, LX/4Xn;->p:LX/0x2;

    .line 772415
    return-object v1
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;
    .locals 2

    .prologue
    .line 772416
    new-instance v0, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;-><init>(LX/4Xn;)V

    .line 772417
    return-object v0
.end method
