.class public LX/40A;
.super LX/2Xb;
.source ""


# instance fields
.field private final f:Ljava/io/ByteArrayOutputStream;


# direct methods
.method public constructor <init>(Ljava/io/ByteArrayOutputStream;LX/2Y2;)V
    .locals 0

    .prologue
    .line 662984
    invoke-direct {p0, p2, p1}, LX/2Xb;-><init>(LX/2Y2;Ljava/lang/Object;)V

    .line 662985
    iput-object p1, p0, LX/40A;->f:Ljava/io/ByteArrayOutputStream;

    .line 662986
    return-void
.end method


# virtual methods
.method public final b(Ljava/io/Writer;)V
    .locals 1

    .prologue
    .line 662978
    iget-object v0, p0, LX/40A;->f:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 662979
    return-void
.end method

.method public final g()LX/2DZ;
    .locals 2

    .prologue
    .line 662983
    invoke-static {}, LX/2D8;->a()LX/2D7;

    move-result-object v0

    iget-object v1, p0, LX/40A;->f:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, v1}, LX/2D7;->a(Ljava/lang/Object;)LX/2DZ;

    move-result-object v0

    return-object v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 662987
    iget-object v0, p0, LX/40A;->f:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v0

    return v0
.end method

.method public final i()V
    .locals 0

    .prologue
    .line 662982
    return-void
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 662980
    iget-object v0, p0, LX/40A;->f:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 662981
    return-void
.end method
