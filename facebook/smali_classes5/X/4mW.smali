.class public LX/4mW;
.super Landroid/view/animation/Animation;
.source ""


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;II)V
    .locals 0

    .prologue
    .line 805990
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 805991
    iput-object p1, p0, LX/4mW;->c:Landroid/view/View;

    .line 805992
    iput p2, p0, LX/4mW;->a:I

    .line 805993
    iput p3, p0, LX/4mW;->b:I

    .line 805994
    return-void
.end method


# virtual methods
.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3

    .prologue
    .line 805995
    iget v0, p0, LX/4mW;->a:I

    int-to-float v0, v0

    iget v1, p0, LX/4mW;->b:I

    iget v2, p0, LX/4mW;->a:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 805996
    iget-object v1, p0, LX/4mW;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 805997
    iget-object v0, p0, LX/4mW;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 805998
    return-void
.end method

.method public final willChangeBounds()Z
    .locals 1

    .prologue
    .line 805999
    const/4 v0, 0x1

    return v0
.end method
