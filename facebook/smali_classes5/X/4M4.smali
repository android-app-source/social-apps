.class public LX/4M4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 685934
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 685935
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_c

    .line 685936
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 685937
    :goto_0
    return v1

    .line 685938
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_8

    .line 685939
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 685940
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 685941
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v11, :cond_0

    .line 685942
    const-string v12, "count"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 685943
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v10, v4

    move v4, v2

    goto :goto_1

    .line 685944
    :cond_1
    const-string v12, "edges"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 685945
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 685946
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, v12, :cond_2

    .line 685947
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, v12, :cond_2

    .line 685948
    invoke-static {p0, p1}, LX/4M5;->b(LX/15w;LX/186;)I

    move-result v11

    .line 685949
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 685950
    :cond_2
    invoke-static {v9, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v9

    move v9, v9

    .line 685951
    goto :goto_1

    .line 685952
    :cond_3
    const-string v12, "page_info"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 685953
    invoke-static {p0, p1}, LX/264;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 685954
    :cond_4
    const-string v12, "viewer_friend_count"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 685955
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v7, v3

    move v3, v2

    goto :goto_1

    .line 685956
    :cond_5
    const-string v12, "viewer_non_friend_count"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 685957
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v6, v0

    move v0, v2

    goto/16 :goto_1

    .line 685958
    :cond_6
    const-string v12, "nodes"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 685959
    invoke-static {p0, p1}, LX/2bM;->b(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 685960
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 685961
    :cond_8
    const/4 v11, 0x6

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 685962
    if-eqz v4, :cond_9

    .line 685963
    invoke-virtual {p1, v1, v10, v1}, LX/186;->a(III)V

    .line 685964
    :cond_9
    invoke-virtual {p1, v2, v9}, LX/186;->b(II)V

    .line 685965
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 685966
    if-eqz v3, :cond_a

    .line 685967
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v7, v1}, LX/186;->a(III)V

    .line 685968
    :cond_a
    if-eqz v0, :cond_b

    .line 685969
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6, v1}, LX/186;->a(III)V

    .line 685970
    :cond_b
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 685971
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_c
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 685903
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 685904
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 685905
    if-eqz v0, :cond_0

    .line 685906
    const-string v1, "count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685907
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 685908
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 685909
    if-eqz v0, :cond_2

    .line 685910
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685911
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 685912
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 685913
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2, p3}, LX/4M5;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 685914
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 685915
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 685916
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 685917
    if-eqz v0, :cond_3

    .line 685918
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685919
    invoke-static {p0, v0, p2}, LX/264;->a(LX/15i;ILX/0nX;)V

    .line 685920
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 685921
    if-eqz v0, :cond_4

    .line 685922
    const-string v1, "viewer_friend_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685923
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 685924
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 685925
    if-eqz v0, :cond_5

    .line 685926
    const-string v1, "viewer_non_friend_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685927
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 685928
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 685929
    if-eqz v0, :cond_6

    .line 685930
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685931
    invoke-static {p0, v0, p2, p3}, LX/2bM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 685932
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 685933
    return-void
.end method
