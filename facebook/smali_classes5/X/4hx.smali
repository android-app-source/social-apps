.class public final LX/4hx;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/platform/common/service/PlatformService;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/common/service/PlatformService;)V
    .locals 0

    .prologue
    .line 802233
    iput-object p1, p0, LX/4hx;->a:Lcom/facebook/platform/common/service/PlatformService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 802212
    iget-object v0, p0, LX/4hx;->a:Lcom/facebook/platform/common/service/PlatformService;

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-static {v0, v1}, Lcom/facebook/platform/common/service/PlatformService;->a(Lcom/facebook/platform/common/service/PlatformService;I)LX/4hq;

    move-result-object v0

    .line 802213
    if-nez v0, :cond_0

    .line 802214
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 802215
    :goto_0
    return-void

    .line 802216
    :cond_0
    invoke-interface {v0}, LX/4hq;->b()LX/4ht;

    move-result-object v1

    .line 802217
    iget-object v2, p0, LX/4hx;->b:Ljava/lang/String;

    invoke-virtual {v1, p1, v2}, LX/4ht;->a(Landroid/os/Message;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 802218
    iget-object v0, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    if-eqz v0, :cond_1

    .line 802219
    :try_start_0
    iget-object v0, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 802220
    iget-object v2, v1, LX/4ht;->e:Landroid/os/Message;

    move-object v1, v2

    .line 802221
    invoke-virtual {v0, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 802222
    :catch_0
    move-exception v0

    .line 802223
    sget-object v1, Lcom/facebook/platform/common/service/PlatformService;->a:Ljava/lang/Class;

    const-string v2, "Unable to send platform service reply"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 802224
    :cond_1
    sget-object v0, Lcom/facebook/platform/common/service/PlatformService;->a:Ljava/lang/Class;

    const-string v1, "Error parsing platform service message"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 802225
    :cond_2
    invoke-interface {v0, p1, v1}, LX/4hq;->a(Landroid/os/Message;LX/4ht;)V

    goto :goto_0
.end method

.method public final sendMessageAtTime(Landroid/os/Message;J)Z
    .locals 4

    .prologue
    .line 802226
    iget-object v0, p0, LX/4hx;->a:Lcom/facebook/platform/common/service/PlatformService;

    iget-object v0, v0, Lcom/facebook/platform/common/service/PlatformService;->d:LX/3N2;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    .line 802227
    iget-object v2, v0, LX/3N2;->b:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v2

    .line 802228
    array-length v3, v2

    if-lez v3, :cond_0

    .line 802229
    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, LX/3N2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 802230
    :goto_0
    move-object v0, v2

    .line 802231
    iput-object v0, p0, LX/4hx;->b:Ljava/lang/String;

    .line 802232
    invoke-super {p0, p1, p2, p3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    move-result v0

    return v0

    :cond_0
    const-string v2, ""

    goto :goto_0
.end method
