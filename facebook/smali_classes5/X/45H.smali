.class public final LX/45H;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 670259
    const-string v0, "zh"

    const-string v1, "ja"

    const-string v2, "ko"

    const-string v3, "vi"

    const-string v4, "hu"

    const-string v5, "ro"

    new-array v6, v7, [Ljava/lang/String;

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/45H;->a:LX/0Rf;

    .line 670260
    const-string v0, "zh"

    const-string v1, "ja"

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/45H;->b:LX/0Rf;

    .line 670261
    const-string v0, "zh"

    const-string v1, "ja"

    const-string v2, "ko"

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/45H;->c:LX/0Rf;

    .line 670262
    new-instance v0, LX/0cA;

    invoke-direct {v0}, LX/0cA;-><init>()V

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "zh"

    aput-object v2, v1, v7

    const/4 v2, 0x1

    const-string v3, "ja"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "ko"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LX/0cA;->b([Ljava/lang/Object;)LX/0cA;

    move-result-object v0

    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    sput-object v0, LX/45H;->d:LX/0Rf;

    .line 670263
    const-string v0, "zh"

    const-string v1, "ja"

    const-string v2, "ko"

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/45H;->e:LX/0Rf;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 670264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
