.class public LX/3p6;
.super LX/1ED;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 641205
    invoke-direct {p0}, LX/1ED;-><init>()V

    .line 641206
    return-void
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 641201
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 641202
    invoke-virtual {p0}, Landroid/app/Activity;->finishAffinity()V

    .line 641203
    :goto_0
    return-void

    .line 641204
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method
