.class public final LX/3wA;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Z

.field public static final b:Landroid/graphics/PorterDuff$Mode;

.field private static final c:Ljava/lang/String;

.field private static final d:LX/3w9;

.field public static final e:[I

.field public static final f:[I

.field public static final g:[I

.field public static final h:[I

.field public static final i:[I


# instance fields
.field public final j:Landroid/content/Context;

.field public final k:Landroid/content/res/Resources;

.field private final l:Landroid/util/TypedValue;

.field private final m:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/content/res/ColorStateList;",
            ">;"
        }
    .end annotation
.end field

.field public n:Landroid/content/res/ColorStateList;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 654455
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-ge v0, v3, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, LX/3wA;->a:Z

    .line 654456
    const-class v0, LX/3wA;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3wA;->c:Ljava/lang/String;

    .line 654457
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    sput-object v0, LX/3wA;->b:Landroid/graphics/PorterDuff$Mode;

    .line 654458
    new-instance v0, LX/3w9;

    const/4 v3, 0x6

    invoke-direct {v0, v3}, LX/3w9;-><init>(I)V

    sput-object v0, LX/3wA;->d:LX/3w9;

    .line 654459
    const/16 v0, 0xf

    new-array v0, v0, [I

    const v3, 0x7f020014

    aput v3, v0, v2

    const v3, 0x7f020017

    aput v3, v0, v1

    const v3, 0x7f02001e

    aput v3, v0, v5

    const v3, 0x7f020016

    aput v3, v0, v6

    const v3, 0x7f020015

    aput v3, v0, v7

    const/4 v3, 0x5

    const v4, 0x7f02001d

    aput v4, v0, v3

    const/4 v3, 0x6

    const v4, 0x7f020018

    aput v4, v0, v3

    const/4 v3, 0x7

    const v4, 0x7f020019

    aput v4, v0, v3

    const/16 v3, 0x8

    const v4, 0x7f02001c

    aput v4, v0, v3

    const/16 v3, 0x9

    const v4, 0x7f02001b

    aput v4, v0, v3

    const/16 v3, 0xa

    const v4, 0x7f02001a

    aput v4, v0, v3

    const/16 v3, 0xb

    const v4, 0x7f02001f

    aput v4, v0, v3

    const/16 v3, 0xc

    const v4, 0x7f02003b

    aput v4, v0, v3

    const/16 v3, 0xd

    const v4, 0x7f020039

    aput v4, v0, v3

    const/16 v3, 0xe

    const v4, 0x7f020002

    aput v4, v0, v3

    sput-object v0, LX/3wA;->e:[I

    .line 654460
    new-array v0, v7, [I

    const v3, 0x7f020038

    aput v3, v0, v2

    const v3, 0x7f02003a

    aput v3, v0, v1

    const v3, 0x7f020012

    aput v3, v0, v5

    const v3, 0x7f020037

    aput v3, v0, v6

    sput-object v0, LX/3wA;->f:[I

    .line 654461
    new-array v0, v6, [I

    const v3, 0x7f02002e

    aput v3, v0, v2

    const v3, 0x7f020010

    aput v3, v0, v1

    const v3, 0x7f02002d

    aput v3, v0, v5

    sput-object v0, LX/3wA;->g:[I

    .line 654462
    const/16 v0, 0xc

    new-array v0, v0, [I

    const v3, 0x7f020013

    aput v3, v0, v2

    const v3, 0x7f020034

    aput v3, v0, v1

    const v3, 0x7f02003c

    aput v3, v0, v5

    const v3, 0x7f020030

    aput v3, v0, v6

    const v3, 0x7f020004

    aput v3, v0, v7

    const/4 v3, 0x5

    const v4, 0x7f020009

    aput v4, v0, v3

    const/4 v3, 0x6

    const v4, 0x7f020031

    aput v4, v0, v3

    const/4 v3, 0x7

    const v4, 0x7f02002f

    aput v4, v0, v3

    const/16 v3, 0x8

    const v4, 0x7f020033

    aput v4, v0, v3

    const/16 v3, 0x9

    const v4, 0x7f020032

    aput v4, v0, v3

    const/16 v3, 0xa

    const v4, 0x7f020008

    aput v4, v0, v3

    const/16 v3, 0xb

    const v4, 0x7f020003

    aput v4, v0, v3

    sput-object v0, LX/3wA;->h:[I

    .line 654463
    new-array v0, v1, [I

    const v1, 0x7f020011

    aput v1, v0, v2

    sput-object v0, LX/3wA;->i:[I

    return-void

    :cond_0
    move v0, v2

    .line 654464
    goto/16 :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 654601
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 654602
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/3wA;->m:Landroid/util/SparseArray;

    .line 654603
    iput-object p1, p0, LX/3wA;->j:Landroid/content/Context;

    .line 654604
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iput-object v0, p0, LX/3wA;->l:Landroid/util/TypedValue;

    .line 654605
    new-instance v0, LX/3wB;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/3wB;-><init>(Landroid/content/res/Resources;LX/3wA;)V

    iput-object v0, p0, LX/3wA;->k:Landroid/content/res/Resources;

    .line 654606
    return-void
.end method

.method public static a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 654615
    sget-object v0, LX/3wA;->g:[I

    invoke-static {v0, p1}, LX/3wA;->a([II)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/3wA;->e:[I

    invoke-static {v0, p1}, LX/3wA;->a([II)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/3wA;->f:[I

    invoke-static {v0, p1}, LX/3wA;->a([II)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/3wA;->h:[I

    invoke-static {v0, p1}, LX/3wA;->a([II)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/3wA;->i:[I

    invoke-static {v0, p1}, LX/3wA;->a([II)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 654616
    if-eqz v0, :cond_2

    .line 654617
    instance-of v0, p0, LX/3w7;

    if-eqz v0, :cond_1

    check-cast p0, LX/3w7;

    .line 654618
    iget-object v0, p0, LX/3w7;->a:LX/3wA;

    move-object v0, v0

    .line 654619
    :goto_1
    invoke-virtual {v0, p1}, LX/3wA;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 654620
    :goto_2
    return-object v0

    .line 654621
    :cond_1
    new-instance v0, LX/3wA;

    invoke-direct {v0, p0}, LX/3wA;-><init>(Landroid/content/Context;)V

    goto :goto_1

    .line 654622
    :cond_2
    invoke-static {p0, p1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/graphics/drawable/Drawable;ILandroid/graphics/PorterDuff$Mode;)V
    .locals 3

    .prologue
    .line 654607
    sget-object v0, LX/3wA;->d:LX/3w9;

    .line 654608
    invoke-static {p1, p2}, LX/3w9;->b(ILandroid/graphics/PorterDuff$Mode;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PorterDuffColorFilter;

    move-object v0, v1

    .line 654609
    if-nez v0, :cond_0

    .line 654610
    new-instance v0, Landroid/graphics/PorterDuffColorFilter;

    invoke-direct {v0, p1, p2}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    .line 654611
    sget-object v1, LX/3wA;->d:LX/3w9;

    .line 654612
    invoke-static {p1, p2}, LX/3w9;->b(ILandroid/graphics/PorterDuff$Mode;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PorterDuffColorFilter;

    .line 654613
    :cond_0
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 654614
    return-void
.end method

.method public static a([II)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 654519
    array-length v2, p0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget v3, p0, v1

    .line 654520
    if-ne v3, p1, :cond_1

    .line 654521
    const/4 v0, 0x1

    .line 654522
    :cond_0
    return v0

    .line 654523
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static d(LX/3wA;I)Landroid/content/res/ColorStateList;
    .locals 11

    .prologue
    .line 654524
    iget-object v0, p0, LX/3wA;->m:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/ColorStateList;

    .line 654525
    if-nez v0, :cond_0

    .line 654526
    const v0, 0x7f020013

    if-ne p1, v0, :cond_1

    .line 654527
    invoke-direct {p0}, LX/3wA;->e()Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 654528
    :goto_0
    iget-object v1, p0, LX/3wA;->m:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 654529
    :cond_0
    return-object v0

    .line 654530
    :cond_1
    const v0, 0x7f020033

    if-ne p1, v0, :cond_2

    .line 654531
    const/4 v1, 0x3

    const/4 v8, 0x2

    const v7, 0x3e99999a    # 0.3f

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 654532
    new-array v0, v1, [[I

    .line 654533
    new-array v1, v1, [I

    .line 654534
    new-array v2, v6, [I

    const v3, -0x101009e

    aput v3, v2, v5

    aput-object v2, v0, v5

    .line 654535
    iget-object v2, p0, LX/3wA;->j:Landroid/content/Context;

    const v3, 0x1010030

    const v4, 0x3dcccccd    # 0.1f

    invoke-static {v2, v3, v4}, LX/3w5;->a(Landroid/content/Context;IF)I

    move-result v2

    aput v2, v1, v5

    .line 654536
    new-array v2, v6, [I

    const v3, 0x10100a0

    aput v3, v2, v5

    aput-object v2, v0, v6

    .line 654537
    iget-object v2, p0, LX/3wA;->j:Landroid/content/Context;

    const v3, 0x7f010055

    invoke-static {v2, v3, v7}, LX/3w5;->a(Landroid/content/Context;IF)I

    move-result v2

    aput v2, v1, v6

    .line 654538
    new-array v2, v5, [I

    aput-object v2, v0, v8

    .line 654539
    iget-object v2, p0, LX/3wA;->j:Landroid/content/Context;

    const v3, 0x1010030

    invoke-static {v2, v3, v7}, LX/3w5;->a(Landroid/content/Context;IF)I

    move-result v2

    aput v2, v1, v8

    .line 654540
    new-instance v2, Landroid/content/res/ColorStateList;

    invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    move-object v0, v2

    .line 654541
    goto :goto_0

    .line 654542
    :cond_2
    const v0, 0x7f020032

    if-ne p1, v0, :cond_3

    .line 654543
    const/4 v1, 0x3

    const v4, -0x101009e

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 654544
    new-array v0, v1, [[I

    .line 654545
    new-array v1, v1, [I

    .line 654546
    iget-object v2, p0, LX/3wA;->j:Landroid/content/Context;

    const v3, 0x7f010058

    invoke-static {v2, v3}, LX/3w5;->b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 654547
    if-eqz v2, :cond_9

    invoke-virtual {v2}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 654548
    new-array v3, v6, [I

    aput v4, v3, v5

    aput-object v3, v0, v5

    .line 654549
    aget-object v3, v0, v5

    invoke-virtual {v2, v3, v5}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v3

    aput v3, v1, v5

    .line 654550
    new-array v3, v6, [I

    const v4, 0x10100a0

    aput v4, v3, v5

    aput-object v3, v0, v6

    .line 654551
    iget-object v3, p0, LX/3wA;->j:Landroid/content/Context;

    const v4, 0x7f010055

    invoke-static {v3, v4}, LX/3w5;->a(Landroid/content/Context;I)I

    move-result v3

    aput v3, v1, v6

    .line 654552
    new-array v3, v5, [I

    aput-object v3, v0, v7

    .line 654553
    invoke-virtual {v2}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v2

    aput v2, v1, v7

    .line 654554
    :goto_1
    new-instance v2, Landroid/content/res/ColorStateList;

    invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    move-object v0, v2

    .line 654555
    goto/16 :goto_0

    .line 654556
    :cond_3
    const v0, 0x7f020008

    if-eq p1, v0, :cond_4

    const v0, 0x7f020003

    if-ne p1, v0, :cond_5

    .line 654557
    :cond_4
    const/4 v1, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 654558
    new-array v0, v1, [[I

    .line 654559
    new-array v1, v1, [I

    .line 654560
    new-array v2, v5, [I

    const v3, -0x101009e

    aput v3, v2, v4

    aput-object v2, v0, v4

    .line 654561
    iget-object v2, p0, LX/3wA;->j:Landroid/content/Context;

    const v3, 0x7f010057

    invoke-static {v2, v3}, LX/3w5;->c(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v4

    .line 654562
    new-array v2, v5, [I

    const v3, 0x10100a7

    aput v3, v2, v4

    aput-object v2, v0, v5

    .line 654563
    iget-object v2, p0, LX/3wA;->j:Landroid/content/Context;

    const v3, 0x7f010056

    invoke-static {v2, v3}, LX/3w5;->a(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v5

    .line 654564
    new-array v2, v5, [I

    const v3, 0x101009c

    aput v3, v2, v4

    aput-object v2, v0, v6

    .line 654565
    iget-object v2, p0, LX/3wA;->j:Landroid/content/Context;

    const v3, 0x7f010056

    invoke-static {v2, v3}, LX/3w5;->a(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v6

    .line 654566
    new-array v2, v4, [I

    aput-object v2, v0, v7

    .line 654567
    iget-object v2, p0, LX/3wA;->j:Landroid/content/Context;

    const v3, 0x7f010057

    invoke-static {v2, v3}, LX/3w5;->a(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v7

    .line 654568
    new-instance v2, Landroid/content/res/ColorStateList;

    invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    move-object v0, v2

    .line 654569
    goto/16 :goto_0

    .line 654570
    :cond_5
    const v0, 0x7f020030

    if-eq p1, v0, :cond_6

    const v0, 0x7f020031

    if-ne p1, v0, :cond_7

    .line 654571
    :cond_6
    invoke-direct {p0}, LX/3wA;->g()Landroid/content/res/ColorStateList;

    move-result-object v0

    goto/16 :goto_0

    .line 654572
    :cond_7
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 654573
    iget-object v0, p0, LX/3wA;->n:Landroid/content/res/ColorStateList;

    if-nez v0, :cond_8

    .line 654574
    iget-object v0, p0, LX/3wA;->j:Landroid/content/Context;

    const v1, 0x7f010054

    invoke-static {v0, v1}, LX/3w5;->a(Landroid/content/Context;I)I

    move-result v0

    .line 654575
    iget-object v1, p0, LX/3wA;->j:Landroid/content/Context;

    const v2, 0x7f010055

    invoke-static {v1, v2}, LX/3w5;->a(Landroid/content/Context;I)I

    move-result v1

    .line 654576
    const/4 v2, 0x7

    new-array v2, v2, [[I

    .line 654577
    const/4 v3, 0x7

    new-array v3, v3, [I

    .line 654578
    new-array v4, v6, [I

    const v5, -0x101009e

    aput v5, v4, v7

    aput-object v4, v2, v7

    .line 654579
    iget-object v4, p0, LX/3wA;->j:Landroid/content/Context;

    const v5, 0x7f010054

    invoke-static {v4, v5}, LX/3w5;->c(Landroid/content/Context;I)I

    move-result v4

    aput v4, v3, v7

    .line 654580
    new-array v4, v6, [I

    const v5, 0x101009c

    aput v5, v4, v7

    aput-object v4, v2, v6

    .line 654581
    aput v1, v3, v6

    .line 654582
    new-array v4, v6, [I

    const v5, 0x10102fe

    aput v5, v4, v7

    aput-object v4, v2, v8

    .line 654583
    aput v1, v3, v8

    .line 654584
    new-array v4, v6, [I

    const v5, 0x10100a7

    aput v5, v4, v7

    aput-object v4, v2, v9

    .line 654585
    aput v1, v3, v9

    .line 654586
    new-array v4, v6, [I

    const v5, 0x10100a0

    aput v5, v4, v7

    aput-object v4, v2, v10

    .line 654587
    aput v1, v3, v10

    .line 654588
    const/4 v4, 0x5

    new-array v5, v6, [I

    const v6, 0x10100a1

    aput v6, v5, v7

    aput-object v5, v2, v4

    .line 654589
    const/4 v4, 0x5

    aput v1, v3, v4

    .line 654590
    const/4 v1, 0x6

    new-array v4, v7, [I

    aput-object v4, v2, v1

    .line 654591
    const/4 v1, 0x6

    aput v0, v3, v1

    .line 654592
    new-instance v0, Landroid/content/res/ColorStateList;

    invoke-direct {v0, v2, v3}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    iput-object v0, p0, LX/3wA;->n:Landroid/content/res/ColorStateList;

    .line 654593
    :cond_8
    iget-object v0, p0, LX/3wA;->n:Landroid/content/res/ColorStateList;

    move-object v0, v0

    .line 654594
    goto/16 :goto_0

    .line 654595
    :cond_9
    new-array v2, v6, [I

    aput v4, v2, v5

    aput-object v2, v0, v5

    .line 654596
    iget-object v2, p0, LX/3wA;->j:Landroid/content/Context;

    const v3, 0x7f010058

    invoke-static {v2, v3}, LX/3w5;->c(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v5

    .line 654597
    new-array v2, v6, [I

    const v3, 0x10100a0

    aput v3, v2, v5

    aput-object v2, v0, v6

    .line 654598
    iget-object v2, p0, LX/3wA;->j:Landroid/content/Context;

    const v3, 0x7f010055

    invoke-static {v2, v3}, LX/3w5;->a(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v6

    .line 654599
    new-array v2, v5, [I

    aput-object v2, v0, v7

    .line 654600
    iget-object v2, p0, LX/3wA;->j:Landroid/content/Context;

    const v3, 0x7f010058

    invoke-static {v2, v3}, LX/3w5;->a(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v7

    goto/16 :goto_1
.end method

.method private e()Landroid/content/res/ColorStateList;
    .locals 7

    .prologue
    const/4 v1, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 654510
    new-array v0, v1, [[I

    .line 654511
    new-array v1, v1, [I

    .line 654512
    new-array v2, v5, [I

    const v3, -0x101009e

    aput v3, v2, v4

    aput-object v2, v0, v4

    .line 654513
    iget-object v2, p0, LX/3wA;->j:Landroid/content/Context;

    const v3, 0x7f010054

    invoke-static {v2, v3}, LX/3w5;->c(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v4

    .line 654514
    new-array v2, v6, [I

    fill-array-data v2, :array_0

    aput-object v2, v0, v5

    .line 654515
    iget-object v2, p0, LX/3wA;->j:Landroid/content/Context;

    const v3, 0x7f010054

    invoke-static {v2, v3}, LX/3w5;->a(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v5

    .line 654516
    new-array v2, v4, [I

    aput-object v2, v0, v6

    .line 654517
    iget-object v2, p0, LX/3wA;->j:Landroid/content/Context;

    const v3, 0x7f010055

    invoke-static {v2, v3}, LX/3w5;->a(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v6

    .line 654518
    new-instance v2, Landroid/content/res/ColorStateList;

    invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    return-object v2

    nop

    :array_0
    .array-data 4
        -0x10100a7
        -0x101009c
    .end array-data
.end method

.method private g()Landroid/content/res/ColorStateList;
    .locals 7

    .prologue
    const/4 v1, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 654501
    new-array v0, v1, [[I

    .line 654502
    new-array v1, v1, [I

    .line 654503
    new-array v2, v5, [I

    const v3, -0x101009e

    aput v3, v2, v4

    aput-object v2, v0, v4

    .line 654504
    iget-object v2, p0, LX/3wA;->j:Landroid/content/Context;

    const v3, 0x7f010054

    invoke-static {v2, v3}, LX/3w5;->c(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v4

    .line 654505
    new-array v2, v6, [I

    fill-array-data v2, :array_0

    aput-object v2, v0, v5

    .line 654506
    iget-object v2, p0, LX/3wA;->j:Landroid/content/Context;

    const v3, 0x7f010054

    invoke-static {v2, v3}, LX/3w5;->a(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v5

    .line 654507
    new-array v2, v4, [I

    aput-object v2, v0, v6

    .line 654508
    iget-object v2, p0, LX/3wA;->j:Landroid/content/Context;

    const v3, 0x7f010055

    invoke-static {v2, v3}, LX/3w5;->a(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v6

    .line 654509
    new-instance v2, Landroid/content/res/ColorStateList;

    invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    return-object v2

    nop

    :array_0
    .array-data 4
        -0x10100a7
        -0x101009c
    .end array-data
.end method


# virtual methods
.method public final a(I)Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    .line 654485
    iget-object v0, p0, LX/3wA;->j:Landroid/content/Context;

    invoke-static {v0, p1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 654486
    if-eqz v0, :cond_1

    .line 654487
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 654488
    sget-object v1, LX/3wA;->h:[I

    invoke-static {v1, p1}, LX/3wA;->a([II)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 654489
    invoke-static {p0, p1}, LX/3wA;->d(LX/3wA;I)Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 654490
    sget-object v1, LX/3wA;->b:Landroid/graphics/PorterDuff$Mode;

    .line 654491
    const v3, 0x7f020032

    if-ne p1, v3, :cond_0

    .line 654492
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    .line 654493
    :cond_0
    if-eqz v2, :cond_1

    .line 654494
    invoke-static {v0}, LX/1d5;->c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 654495
    invoke-static {v0, v2}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 654496
    invoke-static {v0, v1}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V

    .line 654497
    :cond_1
    :goto_0
    return-object v0

    .line 654498
    :cond_2
    sget-object v1, LX/3wA;->i:[I

    invoke-static {v1, p1}, LX/3wA;->a([II)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 654499
    iget-object v0, p0, LX/3wA;->k:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 654500
    :cond_3
    invoke-virtual {p0, p1, v0}, LX/3wA;->a(ILandroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final a(ILandroid/graphics/drawable/Drawable;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v3, 0x1

    .line 654465
    const/4 v4, 0x0

    .line 654466
    sget-object v1, LX/3wA;->e:[I

    invoke-static {v1, p1}, LX/3wA;->a([II)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 654467
    const v0, 0x7f010054

    move v1, v2

    move v5, v0

    move-object v0, v4

    move v4, v3

    move v3, v5

    .line 654468
    :goto_0
    if-eqz v4, :cond_1

    .line 654469
    if-nez v0, :cond_0

    .line 654470
    sget-object v0, LX/3wA;->b:Landroid/graphics/PorterDuff$Mode;

    .line 654471
    :cond_0
    iget-object v4, p0, LX/3wA;->j:Landroid/content/Context;

    invoke-static {v4, v3}, LX/3w5;->a(Landroid/content/Context;I)I

    move-result v3

    .line 654472
    invoke-static {p2, v3, v0}, LX/3wA;->a(Landroid/graphics/drawable/Drawable;ILandroid/graphics/PorterDuff$Mode;)V

    .line 654473
    if-eq v1, v2, :cond_1

    .line 654474
    invoke-virtual {p2, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 654475
    :cond_1
    return-void

    .line 654476
    :cond_2
    sget-object v1, LX/3wA;->f:[I

    invoke-static {v1, p1}, LX/3wA;->a([II)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 654477
    const v0, 0x7f010055

    move v1, v2

    move v5, v0

    move-object v0, v4

    move v4, v3

    move v3, v5

    .line 654478
    goto :goto_0

    .line 654479
    :cond_3
    sget-object v1, LX/3wA;->g:[I

    invoke-static {v1, p1}, LX/3wA;->a([II)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 654480
    const v0, 0x1010031

    .line 654481
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    move v4, v3

    move v3, v0

    move-object v0, v1

    move v1, v2

    goto :goto_0

    .line 654482
    :cond_4
    const v1, 0x7f020022

    if-ne p1, v1, :cond_5

    .line 654483
    const v1, 0x1010030

    .line 654484
    const v0, 0x42233333    # 40.8f

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    move v5, v0

    move-object v0, v4

    move v4, v3

    move v3, v1

    move v1, v5

    goto :goto_0

    :cond_5
    move v1, v2

    move v3, v0

    move v5, v0

    move-object v0, v4

    move v4, v5

    goto :goto_0
.end method
