.class public LX/45x;
.super LX/45m;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/45m",
        "<",
        "Lcom/facebook/common/jobscheduler/compat/JobServiceCompat;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/app/job/JobScheduler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 670720
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/45m;-><init>(Landroid/content/Context;I)V

    .line 670721
    iput-object p1, p0, LX/45x;->a:Landroid/content/Context;

    .line 670722
    const-string v0, "jobscheduler"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    iput-object v0, p0, LX/45x;->b:Landroid/app/job/JobScheduler;

    .line 670723
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/common/jobscheduler/compat/JobServiceCompat;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 670698
    iget-object v0, p0, LX/45x;->b:Landroid/app/job/JobScheduler;

    .line 670699
    invoke-virtual {v0, p1}, Landroid/app/job/JobScheduler;->cancel(I)V

    .line 670700
    return-void
.end method

.method public final a(LX/45u;Ljava/lang/Class;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/45u;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/common/jobscheduler/compat/JobServiceCompat;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 670701
    iget-object v0, p0, LX/45x;->b:Landroid/app/job/JobScheduler;

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, LX/45x;->a:Landroid/content/Context;

    invoke-direct {v1, v2, p2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-wide/16 v7, -0x1

    .line 670702
    new-instance v4, Landroid/app/job/JobInfo$Builder;

    iget v3, p1, LX/45u;->a:I

    invoke-direct {v4, v3, v1}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    .line 670703
    iget-wide v5, p1, LX/45u;->d:J

    cmp-long v3, v5, v7

    if-lez v3, :cond_0

    .line 670704
    iget-wide v5, p1, LX/45u;->d:J

    invoke-virtual {v4, v5, v6}, Landroid/app/job/JobInfo$Builder;->setMinimumLatency(J)Landroid/app/job/JobInfo$Builder;

    .line 670705
    :cond_0
    iget-wide v5, p1, LX/45u;->f:J

    cmp-long v3, v5, v7

    if-lez v3, :cond_1

    .line 670706
    iget-wide v5, p1, LX/45u;->f:J

    invoke-virtual {v4, v5, v6}, Landroid/app/job/JobInfo$Builder;->setOverrideDeadline(J)Landroid/app/job/JobInfo$Builder;

    .line 670707
    :cond_1
    iget v3, p1, LX/45u;->b:I

    packed-switch v3, :pswitch_data_0

    .line 670708
    :goto_0
    iget-boolean v3, p1, LX/45u;->c:Z

    if-eqz v3, :cond_2

    .line 670709
    iget-boolean v3, p1, LX/45u;->c:Z

    invoke-virtual {v4, v3}, Landroid/app/job/JobInfo$Builder;->setRequiresCharging(Z)Landroid/app/job/JobInfo$Builder;

    .line 670710
    :cond_2
    iget-object v3, p1, LX/45u;->h:LX/46S;

    if-eqz v3, :cond_3

    .line 670711
    iget-object v3, p1, LX/45u;->h:LX/46S;

    check-cast v3, LX/46T;

    .line 670712
    iget-object v5, v3, LX/46T;->a:Landroid/os/PersistableBundle;

    move-object v3, v5

    .line 670713
    invoke-virtual {v4, v3}, Landroid/app/job/JobInfo$Builder;->setExtras(Landroid/os/PersistableBundle;)Landroid/app/job/JobInfo$Builder;

    .line 670714
    :cond_3
    invoke-virtual {v4}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v3

    move-object v1, v3

    .line 670715
    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    .line 670716
    return-void

    .line 670717
    :pswitch_0
    const/4 v3, 0x0

    invoke-virtual {v4, v3}, Landroid/app/job/JobInfo$Builder;->setRequiredNetworkType(I)Landroid/app/job/JobInfo$Builder;

    goto :goto_0

    .line 670718
    :pswitch_1
    const/4 v3, 0x1

    invoke-virtual {v4, v3}, Landroid/app/job/JobInfo$Builder;->setRequiredNetworkType(I)Landroid/app/job/JobInfo$Builder;

    goto :goto_0

    .line 670719
    :pswitch_2
    const/4 v3, 0x2

    invoke-virtual {v4, v3}, Landroid/app/job/JobInfo$Builder;->setRequiredNetworkType(I)Landroid/app/job/JobInfo$Builder;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
