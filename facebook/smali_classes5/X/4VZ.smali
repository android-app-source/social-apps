.class public LX/4VZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/0zO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zO",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:LX/1Mz;

.field private final c:Ljava/util/concurrent/Executor;

.field private final d:LX/1N7;

.field public final e:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private final f:Ljava/lang/String;

.field private g:J

.field public h:Lcom/facebook/reactivesocket/Subscription;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private i:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private j:LX/4VU;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public k:LX/4VY;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0zO;Ljava/lang/String;LX/1Mz;Ljava/util/concurrent/Executor;LX/1N7;LX/0TF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zO",
            "<TT;>;",
            "Ljava/lang/String;",
            "LX/1Mz;",
            "Ljava/util/concurrent/Executor;",
            "LX/1N7;",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 742593
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 742594
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/4VZ;->g:J

    .line 742595
    const/4 v0, 0x0

    iput v0, p0, LX/4VZ;->i:I

    .line 742596
    sget-object v0, LX/4VY;->NOT_STARTED:LX/4VY;

    iput-object v0, p0, LX/4VZ;->k:LX/4VY;

    .line 742597
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zO;

    iput-object v0, p0, LX/4VZ;->a:LX/0zO;

    .line 742598
    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/4VZ;->f:Ljava/lang/String;

    .line 742599
    invoke-static {p3}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Mz;

    iput-object v0, p0, LX/4VZ;->b:LX/1Mz;

    .line 742600
    invoke-static {p4}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, LX/4VZ;->c:Ljava/util/concurrent/Executor;

    .line 742601
    iput-object p5, p0, LX/4VZ;->d:LX/1N7;

    .line 742602
    invoke-static {p6}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0TF;

    iput-object v0, p0, LX/4VZ;->e:LX/0TF;

    .line 742603
    return-void
.end method

.method private static a(LX/4VZ;Ljava/lang/String;)LX/0oG;
    .locals 4

    .prologue
    .line 742586
    iget-object v0, p0, LX/4VZ;->b:LX/1Mz;

    .line 742587
    iget-object v1, v0, LX/1Mz;->j:LX/0Zb;

    const/4 v2, 0x0

    invoke-interface {v1, p1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    move-object v0, v1

    .line 742588
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 742589
    const-string v1, "graphql_live_queries"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "live_query_config_id"

    iget-object v3, p0, LX/4VZ;->f:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 742590
    iget-object v1, p0, LX/4VZ;->l:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 742591
    const-string v1, "subscription_id"

    iget-object v2, p0, LX/4VZ;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 742592
    :cond_0
    return-object v0
.end method

.method public static declared-synchronized e(LX/4VZ;)V
    .locals 2

    .prologue
    .line 742580
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4VZ;->k:LX/4VY;

    sget-object v1, LX/4VY;->STARTED:LX/4VY;

    if-ne v0, v1, :cond_0

    iget v0, p0, LX/4VZ;->i:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    iget-object v0, p0, LX/4VZ;->h:Lcom/facebook/reactivesocket/Subscription;

    if-eqz v0, :cond_0

    .line 742581
    iget v0, p0, LX/4VZ;->i:I

    rsub-int/lit8 v0, v0, 0x2

    .line 742582
    iget-object v1, p0, LX/4VZ;->h:Lcom/facebook/reactivesocket/Subscription;

    invoke-interface {v1, v0}, Lcom/facebook/reactivesocket/Subscription;->request(I)V

    .line 742583
    iget v1, p0, LX/4VZ;->i:I

    add-int/2addr v0, v1

    iput v0, p0, LX/4VZ;->i:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 742584
    :cond_0
    monitor-exit p0

    return-void

    .line 742585
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 6

    .prologue
    .line 742562
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4VZ;->b:LX/1Mz;

    const/4 v1, 0x1

    .line 742563
    iget-object v2, v0, LX/1Mz;->h:LX/0Uh;

    const/16 v3, 0x3f6

    invoke-virtual {v2, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-nez v2, :cond_1

    :goto_0
    move v0, v1

    .line 742564
    if-nez v0, :cond_0

    .line 742565
    iget-object v0, p0, LX/4VZ;->a:LX/0zO;

    invoke-virtual {v0}, LX/0zO;->a()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 742566
    :goto_1
    :pswitch_0
    monitor-exit p0

    return-void

    .line 742567
    :cond_0
    :try_start_1
    sget-object v0, LX/4VX;->a:[I

    iget-object v1, p0, LX/4VZ;->k:LX/4VY;

    invoke-virtual {v1}, LX/4VY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 742568
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not start in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/4VZ;->k:LX/4VY;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " state"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 742569
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 742570
    :pswitch_1
    :try_start_2
    iget-object v0, p0, LX/4VZ;->a:LX/0zO;

    invoke-virtual {v0}, LX/0zO;->a()Ljava/lang/String;

    .line 742571
    iget-object v0, p0, LX/4VZ;->b:LX/1Mz;

    const v1, 0x310030    # 4.500007E-39f

    invoke-virtual {v0, v1}, LX/1Mz;->a(I)V

    .line 742572
    sget-object v0, LX/4VY;->STARTED:LX/4VY;

    iput-object v0, p0, LX/4VZ;->k:LX/4VY;

    .line 742573
    :goto_2
    new-instance v2, Lcom/facebook/graphql/executor/live/QueryMetadata;

    iget-object v0, p0, LX/4VZ;->f:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {v2, v0, v1}, Lcom/facebook/graphql/executor/live/QueryMetadata;-><init>(Ljava/lang/String;Z)V

    .line 742574
    const/4 v0, 0x3

    iput v0, p0, LX/4VZ;->i:I

    .line 742575
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, LX/4VZ;->g:J

    .line 742576
    iget-object v0, p0, LX/4VZ;->b:LX/1Mz;

    iget-object v1, p0, LX/4VZ;->a:LX/0zO;

    iget v3, p0, LX/4VZ;->i:I

    new-instance v4, LX/4VV;

    invoke-direct {v4, p0}, LX/4VV;-><init>(LX/4VZ;)V

    new-instance v5, LX/4VW;

    invoke-direct {v5, p0}, LX/4VW;-><init>(LX/4VZ;)V

    invoke-virtual/range {v0 .. v5}, LX/1Mz;->a(LX/0zO;Lcom/facebook/graphql/executor/live/QueryMetadata;ILX/0TF;LX/4VW;)V

    goto :goto_1

    .line 742577
    :pswitch_2
    iget-object v0, p0, LX/4VZ;->a:LX/0zO;

    invoke-virtual {v0}, LX/0zO;->a()Ljava/lang/String;

    .line 742578
    iget-object v0, p0, LX/4VZ;->b:LX/1Mz;

    const v1, 0x31002f    # 4.500005E-39f

    invoke-virtual {v0, v1}, LX/1Mz;->a(I)V

    .line 742579
    sget-object v0, LX/4VY;->STARTED:LX/4VY;

    iput-object v0, p0, LX/4VZ;->k:LX/4VY;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final declared-synchronized a(LX/4Ve;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4Ve",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const-wide/16 v2, -0x1

    .line 742604
    monitor-enter p0

    .line 742605
    :try_start_0
    iget-object v0, p1, LX/4Ve;->b:Lcom/facebook/graphql/executor/live/ResponseMetadata;

    move-object v0, v0

    .line 742606
    if-eqz v0, :cond_0

    .line 742607
    iget-object v0, p1, LX/4Ve;->b:Lcom/facebook/graphql/executor/live/ResponseMetadata;

    move-object v0, v0

    .line 742608
    iget-object v0, v0, Lcom/facebook/graphql/executor/live/ResponseMetadata;->subscriptionUuid:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 742609
    iget-object v0, p1, LX/4Ve;->b:Lcom/facebook/graphql/executor/live/ResponseMetadata;

    move-object v0, v0

    .line 742610
    iget-object v0, v0, Lcom/facebook/graphql/executor/live/ResponseMetadata;->subscriptionUuid:Ljava/lang/String;

    iput-object v0, p0, LX/4VZ;->l:Ljava/lang/String;

    .line 742611
    :cond_0
    iget-object v0, p0, LX/4VZ;->j:LX/4VU;

    if-eqz v0, :cond_1

    .line 742612
    const/4 v0, 0x0

    iput-object v0, p0, LX/4VZ;->j:LX/4VU;

    .line 742613
    :cond_1
    iget v0, p0, LX/4VZ;->i:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/4VZ;->i:I

    .line 742614
    iget-object v0, p0, LX/4VZ;->b:LX/1Mz;

    const v1, 0x310032    # 4.50001E-39f

    invoke-virtual {v0, v1}, LX/1Mz;->a(I)V

    .line 742615
    iget-wide v0, p0, LX/4VZ;->g:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    .line 742616
    const-string v0, "graphql_live_queries_receive"

    invoke-static {p0, v0}, LX/4VZ;->a(LX/4VZ;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    .line 742617
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 742618
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, LX/4VZ;->g:J

    sub-long/2addr v2, v4

    .line 742619
    const-string v1, "initial_response_latency_sec"

    long-to-float v2, v2

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;D)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 742620
    :cond_2
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/4VZ;->g:J

    .line 742621
    :cond_3
    iget-object v0, p0, LX/4VZ;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/graphql/executor/live/GraphQLLiveQuery$3;

    invoke-direct {v1, p0, p1}, Lcom/facebook/graphql/executor/live/GraphQLLiveQuery$3;-><init>(LX/4VZ;LX/4Ve;)V

    const v2, -0x7672f857

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 742622
    monitor-exit p0

    return-void

    .line 742623
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/Throwable;)V
    .locals 10
    .annotation build Landroid/support/annotation/RequiresApi;
    .end annotation

    .prologue
    const/4 v4, 0x5

    .line 742537
    monitor-enter p0

    :try_start_0
    instance-of v0, p1, LX/4l8;

    if-nez v0, :cond_0

    .line 742538
    const-string v0, "graphql_live_queries_error"

    invoke-static {p0, v0}, LX/4VZ;->a(LX/4VZ;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    .line 742539
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 742540
    const-string v1, "error_message"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 742541
    :cond_0
    iget-object v0, p0, LX/4VZ;->j:LX/4VU;

    if-nez v0, :cond_1

    .line 742542
    iget-object v0, p0, LX/4VZ;->d:LX/1N7;

    const-wide/16 v2, 0x1388

    const/4 v1, 0x5

    .line 742543
    new-instance v6, LX/4VU;

    invoke-static {v0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v5

    check-cast v5, Ljava/util/Random;

    invoke-direct {v6, v5, v2, v3, v1}, LX/4VU;-><init>(Ljava/util/Random;JI)V

    .line 742544
    move-object v0, v6

    .line 742545
    iput-object v0, p0, LX/4VZ;->j:LX/4VU;

    .line 742546
    :cond_1
    iget-object v0, p0, LX/4VZ;->j:LX/4VU;

    .line 742547
    iget v1, v0, LX/4VU;->d:I

    move v0, v1

    .line 742548
    if-ge v0, v4, :cond_3

    .line 742549
    sget-object v0, LX/4VY;->FAILED_WILL_RETRY:LX/4VY;

    iput-object v0, p0, LX/4VZ;->k:LX/4VY;

    .line 742550
    iget-object v0, p0, LX/4VZ;->j:LX/4VU;

    const/4 v9, 0x1

    .line 742551
    iget v5, v0, LX/4VU;->d:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v0, LX/4VU;->d:I

    .line 742552
    iget v6, v0, LX/4VU;->c:I

    if-le v5, v6, :cond_2

    .line 742553
    iget v5, v0, LX/4VU;->c:I

    .line 742554
    :cond_2
    iget-wide v7, v0, LX/4VU;->b:J

    iget-object v6, v0, LX/4VU;->a:Ljava/util/Random;

    shl-int v5, v9, v5

    invoke-virtual {v6, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    invoke-static {v9, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    int-to-long v5, v5

    mul-long/2addr v5, v7

    move-wide v0, v5

    .line 742555
    iget-object v2, p0, LX/4VZ;->b:LX/1Mz;

    .line 742556
    iget-object v3, v2, LX/1Mz;->f:LX/0Tf;

    new-instance v4, Lcom/facebook/graphql/executor/live/GraphQLLiveQueryExecutor$1;

    invoke-direct {v4, v2, p0}, Lcom/facebook/graphql/executor/live/GraphQLLiveQueryExecutor$1;-><init>(LX/1Mz;LX/4VZ;)V

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v4, v0, v1, v5}, LX/0Tf;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 742557
    :goto_0
    monitor-exit p0

    return-void

    .line 742558
    :cond_3
    :try_start_1
    iget-object v0, p0, LX/4VZ;->b:LX/1Mz;

    const v1, 0x310033    # 4.500011E-39f

    invoke-virtual {v0, v1}, LX/1Mz;->a(I)V

    .line 742559
    sget-object v0, LX/4VY;->FAILED_PERMANENTLY:LX/4VY;

    iput-object v0, p0, LX/4VZ;->k:LX/4VY;

    .line 742560
    iget-object v0, p0, LX/4VZ;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/graphql/executor/live/GraphQLLiveQuery$4;

    invoke-direct {v1, p0, p1}, Lcom/facebook/graphql/executor/live/GraphQLLiveQuery$4;-><init>(LX/4VZ;Ljava/lang/Throwable;)V

    const v2, -0x46c2ca4a

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 742561
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 3

    .prologue
    .line 742529
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/4VX;->a:[I

    iget-object v1, p0, LX/4VZ;->k:LX/4VY;

    invoke-virtual {v1}, LX/4VY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 742530
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t resume in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/4VZ;->k:LX/4VY;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " state"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 742531
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 742532
    :pswitch_0
    :try_start_1
    sget-object v0, LX/4VY;->STARTED:LX/4VY;

    iput-object v0, p0, LX/4VZ;->k:LX/4VY;

    .line 742533
    invoke-static {p0}, LX/4VZ;->e(LX/4VZ;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 742534
    :goto_0
    :pswitch_1
    monitor-exit p0

    return-void

    .line 742535
    :pswitch_2
    :try_start_2
    sget-object v0, LX/4VY;->FAILED_WILL_RETRY:LX/4VY;

    iput-object v0, p0, LX/4VZ;->k:LX/4VY;

    .line 742536
    invoke-virtual {p0}, LX/4VZ;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final declared-synchronized c()V
    .locals 3

    .prologue
    .line 742523
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/4VX;->a:[I

    iget-object v1, p0, LX/4VZ;->k:LX/4VY;

    invoke-virtual {v1}, LX/4VY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 742524
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t pause in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/4VZ;->k:LX/4VY;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " state"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 742525
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 742526
    :pswitch_1
    :try_start_1
    sget-object v0, LX/4VY;->PAUSED:LX/4VY;

    iput-object v0, p0, LX/4VZ;->k:LX/4VY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 742527
    :goto_0
    :pswitch_2
    monitor-exit p0

    return-void

    .line 742528
    :pswitch_3
    :try_start_2
    sget-object v0, LX/4VY;->FAILED:LX/4VY;

    iput-object v0, p0, LX/4VZ;->k:LX/4VY;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final declared-synchronized d()V
    .locals 2

    .prologue
    .line 742516
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/4VY;->CANCELLED:LX/4VY;

    iput-object v0, p0, LX/4VZ;->k:LX/4VY;

    .line 742517
    iget-object v0, p0, LX/4VZ;->h:Lcom/facebook/reactivesocket/Subscription;

    if-eqz v0, :cond_0

    .line 742518
    iget-object v0, p0, LX/4VZ;->b:LX/1Mz;

    const v1, 0x310031    # 4.500008E-39f

    invoke-virtual {v0, v1}, LX/1Mz;->a(I)V

    .line 742519
    iget-object v0, p0, LX/4VZ;->h:Lcom/facebook/reactivesocket/Subscription;

    invoke-interface {v0}, Lcom/facebook/reactivesocket/Subscription;->cancel()V

    .line 742520
    const/4 v0, 0x0

    iput-object v0, p0, LX/4VZ;->h:Lcom/facebook/reactivesocket/Subscription;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 742521
    :cond_0
    monitor-exit p0

    return-void

    .line 742522
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
