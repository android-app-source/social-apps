.class public LX/47E;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/47E;


# instance fields
.field public a:Landroid/content/ContentResolver;

.field public b:Landroid/content/Context;

.field public c:LX/0en;

.field public d:LX/03V;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;Landroid/content/Context;LX/0en;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 671995
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 671996
    iput-object p1, p0, LX/47E;->a:Landroid/content/ContentResolver;

    .line 671997
    iput-object p2, p0, LX/47E;->b:Landroid/content/Context;

    .line 671998
    iput-object p3, p0, LX/47E;->c:LX/0en;

    .line 671999
    iput-object p4, p0, LX/47E;->d:LX/03V;

    .line 672000
    return-void
.end method

.method public static a(LX/0QB;)LX/47E;
    .locals 7

    .prologue
    .line 672001
    sget-object v0, LX/47E;->e:LX/47E;

    if-nez v0, :cond_1

    .line 672002
    const-class v1, LX/47E;

    monitor-enter v1

    .line 672003
    :try_start_0
    sget-object v0, LX/47E;->e:LX/47E;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 672004
    if-eqz v2, :cond_0

    .line 672005
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 672006
    new-instance p0, LX/47E;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v3

    check-cast v3, Landroid/content/ContentResolver;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0en;->a(LX/0QB;)LX/0en;

    move-result-object v5

    check-cast v5, LX/0en;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-direct {p0, v3, v4, v5, v6}, LX/47E;-><init>(Landroid/content/ContentResolver;Landroid/content/Context;LX/0en;LX/03V;)V

    .line 672007
    move-object v0, p0

    .line 672008
    sput-object v0, LX/47E;->e:LX/47E;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 672009
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 672010
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 672011
    :cond_1
    sget-object v0, LX/47E;->e:LX/47E;

    return-object v0

    .line 672012
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 672013
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 672014
    if-eqz p0, :cond_0

    const-string v0, "content"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    const-string v1, "media"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
