.class public LX/3jL;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/3jN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 632201
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "FB:ACTION:ANNOUNCE"

    new-instance v2, LX/3jM;

    invoke-direct {v2}, LX/3jM;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "FB:ACTION:ASYNC"

    new-instance v2, LX/3jO;

    invoke-direct {v2}, LX/3jO;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "FB:ACTION:ASYNC_MULTI"

    new-instance v2, LX/3jO;

    invoke-direct {v2}, LX/3jO;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "FB:ACTION:MUTATION_GROUP_REQUEST_TO_JOIN"

    new-instance v2, LX/3jP;

    invoke-direct {v2}, LX/3jP;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "FB:ACTION:MUTATION_GROUP_LEAVE"

    new-instance v2, LX/3jQ;

    invoke-direct {v2}, LX/3jQ;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "FB:ACTION:LOG_EVENT"

    new-instance v2, LX/3jR;

    invoke-direct {v2}, LX/3jR;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "FB:ACTION:OPEN_SERP"

    new-instance v2, LX/3jS;

    invoke-direct {v2}, LX/3jS;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "FB:ACTION:URL"

    new-instance v2, LX/3jT;

    invoke-direct {v2}, LX/3jT;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/3jL;->a:LX/0P1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 632202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
