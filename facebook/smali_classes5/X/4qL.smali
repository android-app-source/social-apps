.class public final LX/4qL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/15w;

.field public final b:LX/0n3;

.field public final c:[Ljava/lang/Object;

.field public final d:LX/4qD;

.field private e:I

.field public f:LX/4qH;

.field private g:Ljava/lang/Object;


# direct methods
.method public constructor <init>(LX/15w;LX/0n3;ILX/4qD;)V
    .locals 1

    .prologue
    .line 813333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 813334
    iput-object p1, p0, LX/4qL;->a:LX/15w;

    .line 813335
    iput-object p2, p0, LX/4qL;->b:LX/0n3;

    .line 813336
    iput p3, p0, LX/4qL;->e:I

    .line 813337
    iput-object p4, p0, LX/4qL;->d:LX/4qD;

    .line 813338
    new-array v0, p3, [Ljava/lang/Object;

    iput-object v0, p0, LX/4qL;->c:[Ljava/lang/Object;

    .line 813339
    return-void
.end method


# virtual methods
.method public final a(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 813325
    iget-object v0, p0, LX/4qL;->d:LX/4qD;

    if-eqz v0, :cond_0

    .line 813326
    iget-object v0, p0, LX/4qL;->g:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 813327
    iget-object v0, p0, LX/4qL;->g:Ljava/lang/Object;

    iget-object v1, p0, LX/4qL;->d:LX/4qD;

    iget-object v1, v1, LX/4qD;->generator:LX/4pS;

    invoke-virtual {p1, v0, v1}, LX/0n3;->a(Ljava/lang/Object;LX/4pS;)LX/4qM;

    move-result-object v0

    .line 813328
    invoke-virtual {v0, p2}, LX/4qM;->a(Ljava/lang/Object;)V

    .line 813329
    iget-object v0, p0, LX/4qL;->d:LX/4qD;

    iget-object v0, v0, LX/4qD;->idProperty:LX/32s;

    .line 813330
    if-eqz v0, :cond_0

    .line 813331
    iget-object v1, p0, LX/4qL;->g:Ljava/lang/Object;

    invoke-virtual {v0, p2, v1}, LX/32s;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    .line 813332
    :cond_0
    return-object p2
.end method

.method public final a(LX/32s;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 813323
    new-instance v0, LX/4qK;

    iget-object v1, p0, LX/4qL;->f:LX/4qH;

    invoke-direct {v0, v1, p2, p1}, LX/4qK;-><init>(LX/4qH;Ljava/lang/Object;LX/32s;)V

    iput-object v0, p0, LX/4qL;->f:LX/4qH;

    .line 813324
    return-void
.end method

.method public final a(LX/4q5;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 813321
    new-instance v0, LX/4qI;

    iget-object v1, p0, LX/4qL;->f:LX/4qH;

    invoke-direct {v0, v1, p3, p1, p2}, LX/4qI;-><init>(LX/4qH;Ljava/lang/Object;LX/4q5;Ljava/lang/String;)V

    iput-object v0, p0, LX/4qL;->f:LX/4qH;

    .line 813322
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 813340
    new-instance v0, LX/4qJ;

    iget-object v1, p0, LX/4qL;->f:LX/4qH;

    invoke-direct {v0, v1, p2, p1}, LX/4qJ;-><init>(LX/4qH;Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v0, p0, LX/4qL;->f:LX/4qH;

    .line 813341
    return-void
.end method

.method public final a([LX/32s;)V
    .locals 7

    .prologue
    .line 813315
    const/4 v0, 0x0

    array-length v1, p1

    :goto_0
    if-ge v0, v1, :cond_1

    .line 813316
    aget-object v2, p1, v0

    .line 813317
    if-eqz v2, :cond_0

    .line 813318
    iget-object v3, p0, LX/4qL;->c:[Ljava/lang/Object;

    iget-object v4, p0, LX/4qL;->b:LX/0n3;

    invoke-virtual {v2}, LX/32s;->d()Ljava/lang/Object;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v2, v6}, LX/0n3;->a(Ljava/lang/Object;LX/2Ay;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v3, v0

    .line 813319
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 813320
    :cond_1
    return-void
.end method

.method public final a(ILjava/lang/Object;)Z
    .locals 1

    .prologue
    .line 813313
    iget-object v0, p0, LX/4qL;->c:[Ljava/lang/Object;

    aput-object p2, v0, p1

    .line 813314
    iget v0, p0, LX/4qL;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/4qL;->e:I

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 813309
    iget-object v0, p0, LX/4qL;->d:LX/4qD;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4qL;->d:LX/4qD;

    iget-object v0, v0, LX/4qD;->propertyName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 813310
    iget-object v0, p0, LX/4qL;->d:LX/4qD;

    iget-object v0, v0, LX/4qD;->deserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    iget-object v1, p0, LX/4qL;->a:LX/15w;

    iget-object v2, p0, LX/4qL;->b:LX/0n3;

    invoke-virtual {v0, v1, v2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/4qL;->g:Ljava/lang/Object;

    .line 813311
    const/4 v0, 0x1

    .line 813312
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 4

    .prologue
    .line 813301
    if-eqz p1, :cond_1

    .line 813302
    const/4 v0, 0x0

    iget-object v1, p0, LX/4qL;->c:[Ljava/lang/Object;

    array-length v1, v1

    :goto_0
    if-ge v0, v1, :cond_1

    .line 813303
    iget-object v2, p0, LX/4qL;->c:[Ljava/lang/Object;

    aget-object v2, v2, v0

    if-nez v2, :cond_0

    .line 813304
    aget-object v2, p1, v0

    .line 813305
    if-eqz v2, :cond_0

    .line 813306
    iget-object v3, p0, LX/4qL;->c:[Ljava/lang/Object;

    aput-object v2, v3, v0

    .line 813307
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 813308
    :cond_1
    iget-object v0, p0, LX/4qL;->c:[Ljava/lang/Object;

    return-object v0
.end method
