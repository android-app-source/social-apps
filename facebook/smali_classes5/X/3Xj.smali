.class public LX/3Xj;
.super LX/24d;
.source ""


# static fields
.field public static final b:LX/1Cz;


# instance fields
.field public c:Landroid/widget/TextView;

.field public d:Landroid/widget/LinearLayout;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 593835
    new-instance v0, LX/3Xk;

    invoke-direct {v0}, LX/3Xk;-><init>()V

    sput-object v0, LX/3Xj;->b:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 593836
    invoke-direct {p0, p1}, LX/24d;-><init>(Landroid/content/Context;)V

    .line 593837
    const/16 p1, 0x10

    const/4 v7, -0x2

    const/4 v6, 0x0

    .line 593838
    invoke-virtual {p0}, LX/24d;->getPhotoAttachmentView()Landroid/view/View;

    move-result-object v0

    .line 593839
    invoke-virtual {v0, v6}, Landroid/view/View;->setBackgroundResource(I)V

    .line 593840
    invoke-virtual {v0, v6, v6, v6, v6}, Landroid/view/View;->setPadding(IIII)V

    .line 593841
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, LX/3Xj;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/3Xj;->d:Landroid/widget/LinearLayout;

    .line 593842
    iget-object v0, p0, LX/3Xj;->d:Landroid/widget/LinearLayout;

    const v1, 0x7f021932

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 593843
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, LX/3Xj;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 593844
    invoke-virtual {p0}, LX/3Xj;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1cf3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, LX/3Xj;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b1cf8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p0}, LX/3Xj;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1cf8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v6, v3}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 593845
    const v1, 0x7f02192c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 593846
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 593847
    iput p1, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 593848
    iget-object v2, p0, LX/3Xj;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 593849
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, LX/3Xj;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/3Xj;->c:Landroid/widget/TextView;

    .line 593850
    iget-object v0, p0, LX/3Xj;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, LX/3Xj;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b004e

    invoke-static {v1, v2}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 593851
    iget-object v0, p0, LX/3Xj;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, LX/3Xj;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1cf3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, LX/3Xj;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b1cf3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p0}, LX/3Xj;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1cf9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {p0}, LX/3Xj;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b1cf3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 593852
    iget-object v0, p0, LX/3Xj;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, LX/3Xj;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 593853
    iget-object v0, p0, LX/3Xj;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setGravity(I)V

    .line 593854
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 593855
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 593856
    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 593857
    iget-object v1, p0, LX/3Xj;->d:Landroid/widget/LinearLayout;

    iget-object v2, p0, LX/3Xj;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 593858
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 593859
    const/4 v1, 0x6

    const v2, 0x7f0d24e0

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 593860
    const/4 v1, 0x7

    const v2, 0x7f0d24e0

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 593861
    invoke-virtual {p0}, LX/3Xj;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1cf4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, LX/3Xj;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b1cf4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v6, v1, v2, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 593862
    iget-object v1, p0, LX/3Xj;->d:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v1, v0}, LX/3Xj;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 593863
    return-void
.end method
