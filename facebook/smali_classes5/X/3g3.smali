.class public final LX/3g3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/2ZK;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/2ZK;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method private constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 624042
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 624043
    iput-object p1, p0, LX/3g3;->a:LX/0QB;

    .line 624044
    return-void
.end method

.method public static a(LX/0QB;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "Ljava/util/Set",
            "<",
            "LX/2ZK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 624041
    new-instance v0, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    new-instance v2, LX/3g3;

    invoke-direct {v2, p0}, LX/3g3;-><init>(LX/0QB;)V

    invoke-direct {v0, v1, v2}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 624040
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/3g3;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 624008
    packed-switch p2, :pswitch_data_0

    .line 624009
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 624010
    :pswitch_0
    new-instance v0, LX/3g5;

    const/16 v1, 0x5c

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3g5;-><init>(LX/0Ot;)V

    .line 624011
    move-object v0, v0

    .line 624012
    :goto_0
    return-object v0

    .line 624013
    :pswitch_1
    new-instance p2, LX/3g7;

    invoke-static {p1}, LX/2CB;->a(LX/0QB;)LX/2CB;

    move-result-object v0

    check-cast v0, LX/2CB;

    invoke-static {p1}, LX/29r;->a(LX/0QB;)LX/29r;

    move-result-object v1

    check-cast v1, LX/29r;

    invoke-static {p1}, LX/3g8;->b(LX/0QB;)LX/3g8;

    move-result-object p0

    check-cast p0, LX/3g8;

    invoke-direct {p2, v0, v1, p0}, LX/3g7;-><init>(LX/2CB;LX/29r;LX/3g8;)V

    .line 624014
    move-object v0, p2

    .line 624015
    goto :goto_0

    .line 624016
    :pswitch_2
    invoke-static {p1}, LX/3gA;->a(LX/0QB;)LX/3gA;

    move-result-object v0

    goto :goto_0

    .line 624017
    :pswitch_3
    new-instance p2, LX/3gG;

    invoke-static {p1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1}, LX/2UQ;->b(LX/0QB;)LX/2UQ;

    move-result-object v1

    check-cast v1, LX/2UQ;

    .line 624018
    new-instance v2, LX/3gH;

    invoke-direct {v2}, LX/3gH;-><init>()V

    .line 624019
    move-object v2, v2

    .line 624020
    move-object v2, v2

    .line 624021
    check-cast v2, LX/3gH;

    invoke-static {p1}, LX/30I;->b(LX/0QB;)LX/30I;

    move-result-object p0

    check-cast p0, LX/30I;

    invoke-direct {p2, v0, v1, v2, p0}, LX/3gG;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2UQ;LX/3gH;LX/30I;)V

    .line 624022
    move-object v0, p2

    .line 624023
    goto :goto_0

    .line 624024
    :pswitch_4
    invoke-static {p1}, LX/2ZJ;->a(LX/0QB;)LX/2ZJ;

    move-result-object v0

    goto :goto_0

    .line 624025
    :pswitch_5
    invoke-static {p1}, LX/2ZN;->b(LX/0QB;)LX/2ZN;

    move-result-object v0

    goto :goto_0

    .line 624026
    :pswitch_6
    invoke-static {p1}, LX/2ZP;->b(LX/0QB;)LX/2ZP;

    move-result-object v0

    goto :goto_0

    .line 624027
    :pswitch_7
    invoke-static {p1}, LX/3gT;->b(LX/0QB;)LX/3gT;

    move-result-object v0

    goto :goto_0

    .line 624028
    :pswitch_8
    invoke-static {p1}, LX/3gU;->a(LX/0QB;)LX/3gU;

    move-result-object v0

    goto :goto_0

    .line 624029
    :pswitch_9
    new-instance v0, LX/3gX;

    const/16 v1, 0xf68

    invoke-static {p1, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0xf66

    invoke-static {p1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/3gX;-><init>(LX/0Or;LX/0Ot;)V

    .line 624030
    move-object v0, v0

    .line 624031
    goto :goto_0

    .line 624032
    :pswitch_a
    new-instance v2, LX/3gZ;

    invoke-static {p1}, LX/3ga;->b(LX/0QB;)LX/3ga;

    move-result-object v0

    check-cast v0, LX/3ga;

    invoke-static {p1}, LX/339;->a(LX/0QB;)LX/339;

    move-result-object v1

    check-cast v1, LX/339;

    invoke-direct {v2, v0, v1}, LX/3gZ;-><init>(LX/3ga;LX/339;)V

    .line 624033
    move-object v0, v2

    .line 624034
    goto :goto_0

    .line 624035
    :pswitch_b
    invoke-static {p1}, LX/3gc;->a(LX/0QB;)LX/3gc;

    move-result-object v0

    goto/16 :goto_0

    .line 624036
    :pswitch_c
    invoke-static {p1}, LX/2Zo;->b(LX/0QB;)LX/2Zo;

    move-result-object v0

    goto/16 :goto_0

    .line 624037
    :pswitch_d
    invoke-static {p1}, LX/3gf;->a(LX/0QB;)LX/3gf;

    move-result-object v0

    goto/16 :goto_0

    .line 624038
    :pswitch_e
    invoke-static {p1}, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->a(LX/0QB;)Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 624039
    const/16 v0, 0xf

    return v0
.end method
