.class public abstract LX/3dH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Bq;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final b:Ljava/lang/Object;


# instance fields
.field private final c:LX/0QJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QJ",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 616507
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/3dH;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 616508
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 616509
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->g()LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->i()LX/0QN;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/0QN;->b(I)LX/0QN;

    move-result-object v0

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, LX/0QN;->a(J)LX/0QN;

    move-result-object v0

    new-instance v1, LX/3dI;

    invoke-direct {v1, p0}, LX/3dI;-><init>(LX/3dH;)V

    invoke-virtual {v0, v1}, LX/0QN;->a(LX/0QM;)LX/0QJ;

    move-result-object v0

    iput-object v0, p0, LX/3dH;->c:LX/0QJ;

    .line 616510
    return-void
.end method


# virtual methods
.method public abstract a(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)TT;"
        }
    .end annotation
.end method

.method public final a(Ljava/lang/Object;Z)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;Z)TT;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 616511
    if-nez p1, :cond_1

    .line 616512
    :cond_0
    :goto_0
    return-object v0

    .line 616513
    :cond_1
    :try_start_0
    iget-object v1, p0, LX/3dH;->c:LX/0QJ;

    invoke-interface {v1, p1}, LX/0QJ;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 616514
    sget-object v2, LX/3dH;->b:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    if-eq v1, v2, :cond_0

    move-object v0, v1

    .line 616515
    goto :goto_0

    .line 616516
    :catch_0
    move-exception v0

    .line 616517
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method
