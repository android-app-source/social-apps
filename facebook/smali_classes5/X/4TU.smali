.class public LX/4TU;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 718490
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 718491
    const/4 v15, 0x0

    .line 718492
    const/4 v14, 0x0

    .line 718493
    const/4 v13, 0x0

    .line 718494
    const/4 v12, 0x0

    .line 718495
    const/4 v11, 0x0

    .line 718496
    const/4 v10, 0x0

    .line 718497
    const/4 v9, 0x0

    .line 718498
    const/4 v8, 0x0

    .line 718499
    const/4 v7, 0x0

    .line 718500
    const/4 v6, 0x0

    .line 718501
    const/4 v5, 0x0

    .line 718502
    const/4 v4, 0x0

    .line 718503
    const/4 v3, 0x0

    .line 718504
    const/4 v2, 0x0

    .line 718505
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_1

    .line 718506
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 718507
    const/4 v2, 0x0

    .line 718508
    :goto_0
    return v2

    .line 718509
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 718510
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_f

    .line 718511
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v16

    .line 718512
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 718513
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    if-eqz v16, :cond_1

    .line 718514
    const-string v17, "checkin_location"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 718515
    invoke-static/range {p0 .. p1}, LX/2um;->a(LX/15w;LX/186;)I

    move-result v15

    goto :goto_1

    .line 718516
    :cond_2
    const-string v17, "minutiae_action"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 718517
    invoke-static/range {p0 .. p1}, LX/2uh;->a(LX/15w;LX/186;)I

    move-result v14

    goto :goto_1

    .line 718518
    :cond_3
    const-string v17, "profile_picture_overlay"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 718519
    invoke-static/range {p0 .. p1}, LX/4Ob;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 718520
    :cond_4
    const-string v17, "suggested_cover_photos"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 718521
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 718522
    :cond_5
    const-string v17, "thumbnail_image"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 718523
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 718524
    :cond_6
    const-string v17, "tagging_action"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 718525
    invoke-static/range {p0 .. p1}, LX/2bO;->b(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 718526
    :cond_7
    const-string v17, "frame"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 718527
    invoke-static/range {p0 .. p1}, LX/4Tb;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 718528
    :cond_8
    const-string v17, "frame_pack"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 718529
    invoke-static/range {p0 .. p1}, LX/4Tc;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 718530
    :cond_9
    const-string v17, "link_attachment"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 718531
    invoke-static/range {p0 .. p1}, LX/4LO;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 718532
    :cond_a
    const-string v17, "mask_effect"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 718533
    invoke-static/range {p0 .. p1}, LX/4PE;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 718534
    :cond_b
    const-string v17, "meme_category"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_c

    .line 718535
    invoke-static/range {p0 .. p1}, LX/4PR;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 718536
    :cond_c
    const-string v17, "particle_effect"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_d

    .line 718537
    invoke-static/range {p0 .. p1}, LX/4Qs;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 718538
    :cond_d
    const-string v17, "shader_filter"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_e

    .line 718539
    invoke-static/range {p0 .. p1}, LX/4Sq;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 718540
    :cond_e
    const-string v17, "style_transfer"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 718541
    invoke-static/range {p0 .. p1}, LX/4TT;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 718542
    :cond_f
    const/16 v16, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 718543
    const/16 v16, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 718544
    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 718545
    const/4 v14, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 718546
    const/4 v13, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 718547
    const/4 v12, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 718548
    const/4 v11, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 718549
    const/4 v10, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 718550
    const/4 v9, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 718551
    const/16 v8, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 718552
    const/16 v7, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 718553
    const/16 v6, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 718554
    const/16 v5, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 718555
    const/16 v4, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 718556
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 718557
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 718558
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 718559
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 718560
    if-eqz v0, :cond_0

    .line 718561
    const-string v1, "checkin_location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718562
    invoke-static {p0, v0, p2, p3}, LX/2um;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 718563
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 718564
    if-eqz v0, :cond_1

    .line 718565
    const-string v1, "minutiae_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718566
    invoke-static {p0, v0, p2, p3}, LX/2uh;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 718567
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 718568
    if-eqz v0, :cond_2

    .line 718569
    const-string v1, "profile_picture_overlay"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718570
    invoke-static {p0, v0, p2, p3}, LX/4Ob;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 718571
    :cond_2
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 718572
    if-eqz v0, :cond_3

    .line 718573
    const-string v0, "suggested_cover_photos"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718574
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 718575
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 718576
    if-eqz v0, :cond_4

    .line 718577
    const-string v1, "thumbnail_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718578
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 718579
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 718580
    if-eqz v0, :cond_5

    .line 718581
    const-string v1, "tagging_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718582
    invoke-static {p0, v0, p2, p3}, LX/2bO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 718583
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 718584
    if-eqz v0, :cond_6

    .line 718585
    const-string v1, "frame"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718586
    invoke-static {p0, v0, p2, p3}, LX/4Tb;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 718587
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 718588
    if-eqz v0, :cond_7

    .line 718589
    const-string v1, "frame_pack"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718590
    invoke-static {p0, v0, p2, p3}, LX/4Tc;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 718591
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 718592
    if-eqz v0, :cond_8

    .line 718593
    const-string v1, "link_attachment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718594
    invoke-static {p0, v0, p2}, LX/4LO;->a(LX/15i;ILX/0nX;)V

    .line 718595
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 718596
    if-eqz v0, :cond_9

    .line 718597
    const-string v1, "mask_effect"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718598
    invoke-static {p0, v0, p2, p3}, LX/4PE;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 718599
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 718600
    if-eqz v0, :cond_a

    .line 718601
    const-string v1, "meme_category"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718602
    invoke-static {p0, v0, p2, p3}, LX/4PR;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 718603
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 718604
    if-eqz v0, :cond_b

    .line 718605
    const-string v1, "particle_effect"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718606
    invoke-static {p0, v0, p2, p3}, LX/4Qs;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 718607
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 718608
    if-eqz v0, :cond_c

    .line 718609
    const-string v1, "shader_filter"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718610
    invoke-static {p0, v0, p2}, LX/4Sq;->a(LX/15i;ILX/0nX;)V

    .line 718611
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 718612
    if-eqz v0, :cond_d

    .line 718613
    const-string v1, "style_transfer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718614
    invoke-static {p0, v0, p2, p3}, LX/4TT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 718615
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 718616
    return-void
.end method
