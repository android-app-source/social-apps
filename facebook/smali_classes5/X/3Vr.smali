.class public abstract LX/3Vr;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/3Vs;


# instance fields
.field public a:LX/0tJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/C5g;

.field private final c:Landroid/os/Handler;

.field public d:Landroid/view/View;

.field public e:Landroid/view/ViewStub;

.field private f:Lcom/facebook/fbui/facepile/FacepileView;

.field public g:Landroid/view/ViewStub;

.field private h:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field public i:Landroid/view/ViewGroup;

.field public j:LX/C60;

.field public k:LX/C5w;

.field public l:Ljava/lang/Runnable;

.field public m:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 588749
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 588750
    const/4 v0, 0x0

    iput-object v0, p0, LX/3Vr;->b:LX/C5g;

    .line 588751
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/3Vr;->c:Landroid/os/Handler;

    .line 588752
    sget-object v0, LX/C5w;->DISABLED:LX/C5w;

    iput-object v0, p0, LX/3Vr;->k:LX/C5w;

    .line 588753
    const-class v0, LX/3Vr;

    invoke-static {v0, p0}, LX/3Vr;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 588754
    invoke-virtual {p0}, LX/3Vr;->getLayout()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 588755
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/3Vr;->setClipChildren(Z)V

    .line 588756
    const v0, 0x7f0d1101

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/3Vr;->d:Landroid/view/View;

    .line 588757
    const v0, 0x7f0d1100

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/3Vr;->i:Landroid/view/ViewGroup;

    .line 588758
    const v0, 0x7f0d1102

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/3Vr;->e:Landroid/view/ViewStub;

    .line 588759
    const v0, 0x7f0d1103

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/3Vr;->g:Landroid/view/ViewStub;

    .line 588760
    iget-object v0, p0, LX/3Vr;->a:LX/0tJ;

    .line 588761
    iget-object v1, v0, LX/0tJ;->a:LX/0ad;

    sget-short p1, LX/0wn;->Q:S

    const/4 p2, 0x0

    invoke-interface {v1, p1, p2}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 588762
    if-eqz v0, :cond_0

    .line 588763
    invoke-static {p0}, LX/3Vr;->getFacepileView(LX/3Vr;)Lcom/facebook/fbui/facepile/FacepileView;

    .line 588764
    invoke-static {p0}, LX/3Vr;->getRealTimeActivityInfoTextView(LX/3Vr;)Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 588765
    :cond_0
    new-instance v0, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$1;

    invoke-direct {v0, p0}, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$1;-><init>(LX/3Vr;)V

    iput-object v0, p0, LX/3Vr;->l:Ljava/lang/Runnable;

    .line 588766
    new-instance v0, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$2;

    invoke-direct {v0, p0}, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$2;-><init>(LX/3Vr;)V

    iput-object v0, p0, LX/3Vr;->m:Ljava/lang/Runnable;

    .line 588767
    invoke-virtual {p0}, LX/3Vr;->a()V

    .line 588768
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/3Vr;

    invoke-static {p0}, LX/0tJ;->a(LX/0QB;)LX/0tJ;

    move-result-object p0

    check-cast p0, LX/0tJ;

    iput-object p0, p1, LX/3Vr;->a:LX/0tJ;

    return-void
.end method

.method public static a$redex0(LX/3Vr;I)V
    .locals 5

    .prologue
    .line 588769
    iget-object v0, p0, LX/3Vr;->c:Landroid/os/Handler;

    iget-object v1, p0, LX/3Vr;->l:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 588770
    iget-object v0, p0, LX/3Vr;->c:Landroid/os/Handler;

    iget-object v1, p0, LX/3Vr;->l:Ljava/lang/Runnable;

    int-to-long v2, p1

    const v4, 0x7d67f23b

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 588771
    return-void
.end method

.method public static b$redex0(LX/3Vr;I)V
    .locals 5

    .prologue
    .line 588772
    iget-object v0, p0, LX/3Vr;->c:Landroid/os/Handler;

    iget-object v1, p0, LX/3Vr;->m:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 588773
    iget-object v0, p0, LX/3Vr;->c:Landroid/os/Handler;

    iget-object v1, p0, LX/3Vr;->m:Ljava/lang/Runnable;

    int-to-long v2, p1

    const v4, -0x7fbece58

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 588774
    return-void
.end method

.method public static getFacepileView(LX/3Vr;)Lcom/facebook/fbui/facepile/FacepileView;
    .locals 1

    .prologue
    .line 588775
    iget-object v0, p0, LX/3Vr;->f:Lcom/facebook/fbui/facepile/FacepileView;

    if-nez v0, :cond_0

    .line 588776
    iget-object v0, p0, LX/3Vr;->e:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, LX/3Vr;->f:Lcom/facebook/fbui/facepile/FacepileView;

    .line 588777
    :cond_0
    iget-object v0, p0, LX/3Vr;->f:Lcom/facebook/fbui/facepile/FacepileView;

    return-object v0
.end method

.method public static getRealTimeActivityInfoTextView(LX/3Vr;)Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;
    .locals 1

    .prologue
    .line 588778
    iget-object v0, p0, LX/3Vr;->h:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    if-nez v0, :cond_0

    .line 588779
    iget-object v0, p0, LX/3Vr;->g:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, LX/3Vr;->h:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 588780
    :cond_0
    iget-object v0, p0, LX/3Vr;->h:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 588781
    iget-object v0, p0, LX/3Vr;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 588782
    iget-object v0, p0, LX/3Vr;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 588783
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 588784
    invoke-direct {p0}, LX/3Vr;->j()V

    .line 588785
    iget-object v0, p0, LX/3Vr;->d:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 588786
    iget-object v0, p0, LX/3Vr;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 588787
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 588788
    invoke-direct {p0}, LX/3Vr;->j()V

    .line 588789
    iget-object v0, p0, LX/3Vr;->i:Landroid/view/ViewGroup;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 588790
    iget-object v0, p0, LX/3Vr;->i:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setTranslationY(F)V

    .line 588791
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 588741
    iget-object v0, p0, LX/3Vr;->j:LX/C60;

    if-eqz v0, :cond_0

    .line 588742
    iget-object v0, p0, LX/3Vr;->j:LX/C60;

    invoke-virtual {v0}, LX/C60;->d()V

    .line 588743
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/3Vr;->j:LX/C60;

    .line 588744
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 588745
    sget-object v0, LX/C5w;->ENABLED:LX/C5w;

    .line 588746
    iput-object v0, p0, LX/3Vr;->k:LX/C5w;

    .line 588747
    iget-object v0, p0, LX/3Vr;->a:LX/0tJ;

    invoke-virtual {v0}, LX/0tJ;->d()I

    move-result v0

    invoke-static {p0, v0}, LX/3Vr;->a$redex0(LX/3Vr;I)V

    .line 588748
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 588713
    sget-object v0, LX/C5w;->ENABLED:LX/C5w;

    .line 588714
    iput-object v0, p0, LX/3Vr;->k:LX/C5w;

    .line 588715
    iget-object v0, p0, LX/3Vr;->a:LX/0tJ;

    invoke-virtual {v0}, LX/0tJ;->d()I

    move-result v0

    invoke-static {p0, v0}, LX/3Vr;->b$redex0(LX/3Vr;I)V

    .line 588716
    return-void
.end method

.method public abstract getLayout()I
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 588717
    sget-object v0, LX/C5w;->DISABLED:LX/C5w;

    .line 588718
    iput-object v0, p0, LX/3Vr;->k:LX/C5w;

    .line 588719
    iget-object v0, p0, LX/3Vr;->c:Landroid/os/Handler;

    iget-object v1, p0, LX/3Vr;->m:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 588720
    iget-object v0, p0, LX/3Vr;->c:Landroid/os/Handler;

    iget-object v1, p0, LX/3Vr;->l:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 588721
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 588722
    const/4 v0, 0x1

    return v0
.end method

.method public setAnimationController(LX/C60;)V
    .locals 2

    .prologue
    .line 588723
    iput-object p1, p0, LX/3Vr;->j:LX/C60;

    .line 588724
    iget-object v0, p0, LX/3Vr;->j:LX/C60;

    .line 588725
    invoke-virtual {v0}, LX/C60;->d()V

    .line 588726
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, LX/C60;->m:Ljava/lang/ref/WeakReference;

    .line 588727
    iget-object v1, v0, LX/C60;->d:LX/C5z;

    const/4 p1, 0x1

    invoke-virtual {v0, v1, p1}, LX/C60;->a(LX/C5z;Z)V

    .line 588728
    return-void
.end method

.method public setAnimationEventListener(LX/C5g;)V
    .locals 0

    .prologue
    .line 588729
    iput-object p1, p0, LX/3Vr;->b:LX/C5g;

    .line 588730
    return-void
.end method

.method public setAnimatorTargetToBlingBar(Landroid/animation/ObjectAnimator;)V
    .locals 1

    .prologue
    .line 588731
    iget-object v0, p0, LX/3Vr;->d:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 588732
    return-void
.end method

.method public setAnimatorTargetToRealTimeActivity(Landroid/animation/ObjectAnimator;)V
    .locals 1

    .prologue
    .line 588733
    iget-object v0, p0, LX/3Vr;->i:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 588734
    return-void
.end method

.method public setFacepileStrings(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 588735
    invoke-static {p0}, LX/3Vr;->getFacepileView(LX/3Vr;)Lcom/facebook/fbui/facepile/FacepileView;

    move-result-object v0

    .line 588736
    invoke-virtual {v0, p1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceStrings(Ljava/util/List;)V

    .line 588737
    return-void
.end method

.method public setText(LX/175;)V
    .locals 3

    .prologue
    .line 588738
    invoke-static {p0}, LX/3Vr;->getRealTimeActivityInfoTextView(LX/3Vr;)Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    move-result-object v0

    .line 588739
    invoke-interface {p1}, LX/175;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/175;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Ljava/lang/String;LX/0Px;)V

    .line 588740
    return-void
.end method
