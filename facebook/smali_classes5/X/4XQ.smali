.class public final LX/4XQ;
.super LX/0ur;
.source ""


# instance fields
.field public b:Z

.field public c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 768043
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 768044
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    iput-object v0, p0, LX/4XQ;->g:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 768045
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

    iput-object v0, p0, LX/4XQ;->j:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

    .line 768046
    instance-of v0, p0, LX/4XQ;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 768047
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;
    .locals 2

    .prologue
    .line 768048
    new-instance v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;-><init>(LX/4XQ;)V

    .line 768049
    return-object v0
.end method
