.class public LX/3Zu;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private final a:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

.field private final b:Lcom/facebook/maps/FbStaticMapView;

.field private c:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 602369
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 602370
    new-instance v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    const-string v1, "pages_multi_locations_map"

    invoke-direct {v0, v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/3Zu;->a:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 602371
    const v0, 0x7f030e05

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 602372
    const v0, 0x7f0d223e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/maps/FbStaticMapView;

    iput-object v0, p0, LX/3Zu;->b:Lcom/facebook/maps/FbStaticMapView;

    .line 602373
    const v0, 0x7f0d223f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/3Zu;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 602374
    invoke-virtual {p0}, LX/3Zu;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 602375
    if-eqz v0, :cond_0

    .line 602376
    iget-object v1, p0, LX/3Zu;->b:Lcom/facebook/maps/FbStaticMapView;

    sget-object v2, LX/0yY;->VIEW_MAP_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/facebook/maps/FbStaticMapView;->a(LX/0yY;LX/0gc;LX/6Zs;)V

    .line 602377
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(ZLjava/util/ArrayList;Ljava/lang/String;Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;ILcom/facebook/graphql/enums/GraphQLPlaceType;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/android/maps/model/LatLng;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLInterfaces$ReactionGeoRectangleFields;",
            "I",
            "Lcom/facebook/graphql/enums/GraphQLPlaceType;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 602378
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 602379
    iget-object v0, p0, LX/3Zu;->a:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    invoke-virtual {v0}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a()Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v2

    .line 602380
    if-nez p1, :cond_1

    .line 602381
    const-string v0, "red"

    invoke-virtual {v2, p2, v0}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(Ljava/util/List;Ljava/lang/String;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 602382
    :goto_1
    iget-object v0, p0, LX/3Zu;->b:Lcom/facebook/maps/FbStaticMapView;

    invoke-virtual {v0, v2}, LX/3BP;->setMapOptions(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    .line 602383
    return-void

    :cond_0
    move v0, v1

    .line 602384
    goto :goto_0

    .line 602385
    :cond_1
    const/4 v0, 0x0

    .line 602386
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPlaceType;->PLACE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    if-ne p6, v3, :cond_2

    .line 602387
    iget-object v0, p0, LX/3Zu;->b:Lcom/facebook/maps/FbStaticMapView;

    invoke-virtual {v0}, Lcom/facebook/maps/FbStaticMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f020e90

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 602388
    :cond_2
    iget-object v3, p0, LX/3Zu;->b:Lcom/facebook/maps/FbStaticMapView;

    const/high16 v4, 0x3f000000    # 0.5f

    const v5, 0x3f6e147b    # 0.93f

    invoke-virtual {v3, v0, v4, v5}, LX/3BP;->a(Landroid/graphics/drawable/Drawable;FF)V

    .line 602389
    invoke-virtual {v2, p5}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(I)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v3

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/maps/model/LatLng;

    iget-wide v4, v0, Lcom/facebook/android/maps/model/LatLng;->a:D

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/maps/model/LatLng;

    iget-wide v0, v0, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-virtual {v3, v4, v5, v0, v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(DD)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 602390
    if-eqz p4, :cond_3

    .line 602391
    new-instance v0, Landroid/graphics/RectF;

    invoke-virtual {p4}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;->d()D

    move-result-wide v4

    double-to-float v1, v4

    invoke-virtual {p4}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;->b()D

    move-result-wide v4

    double-to-float v3, v4

    invoke-virtual {p4}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;->a()D

    move-result-wide v4

    double-to-float v4, v4

    invoke-virtual {p4}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;->c()D

    move-result-wide v6

    double-to-float v5, v6

    invoke-direct {v0, v1, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v2, v0}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(Landroid/graphics/RectF;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 602392
    :cond_3
    invoke-virtual {p0, p3}, LX/3Zu;->setTextView(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public setTextView(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 602393
    iget-object v1, p0, LX/3Zu;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 602394
    iget-object v0, p0, LX/3Zu;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 602395
    return-void

    .line 602396
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
