.class public final LX/4gP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/4gQ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/ipc/media/data/MimeType;

.field public e:I

.field public f:I

.field public g:I

.field public h:F

.field public i:Lcom/facebook/ipc/media/data/FocusPoint;

.field public j:D

.field public k:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 799443
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799444
    sget-object v0, Lcom/facebook/ipc/media/data/MimeType;->e:Lcom/facebook/ipc/media/data/MimeType;

    iput-object v0, p0, LX/4gP;->d:Lcom/facebook/ipc/media/data/MimeType;

    .line 799445
    iput v1, p0, LX/4gP;->f:I

    .line 799446
    iput v1, p0, LX/4gP;->g:I

    .line 799447
    sget v0, Lcom/facebook/ipc/media/data/MediaData;->a:F

    iput v0, p0, LX/4gP;->h:F

    .line 799448
    sget-object v0, Lcom/facebook/ipc/media/data/FocusPoint;->a:Lcom/facebook/ipc/media/data/FocusPoint;

    iput-object v0, p0, LX/4gP;->i:Lcom/facebook/ipc/media/data/FocusPoint;

    .line 799449
    sget-wide v0, Lcom/facebook/ipc/media/data/MediaData;->b:D

    iput-wide v0, p0, LX/4gP;->j:D

    .line 799450
    sget-wide v0, Lcom/facebook/ipc/media/data/MediaData;->b:D

    iput-wide v0, p0, LX/4gP;->k:D

    .line 799451
    return-void
.end method


# virtual methods
.method public final a(D)LX/4gP;
    .locals 3

    .prologue
    .line 799452
    const-wide v0, -0x3fa9800000000000L    # -90.0

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x4056800000000000L    # 90.0

    cmpg-double v0, p1, v0

    if-lez v0, :cond_1

    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 799453
    iput-wide p1, p0, LX/4gP;->j:D

    .line 799454
    return-object p0

    .line 799455
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/4gQ;)LX/4gP;
    .locals 1

    .prologue
    .line 799456
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4gQ;

    iput-object v0, p0, LX/4gP;->b:LX/4gQ;

    .line 799457
    return-object p0
.end method

.method public final a(Landroid/net/Uri;)LX/4gP;
    .locals 1

    .prologue
    .line 799458
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 799459
    invoke-virtual {p1}, Landroid/net/Uri;->isHierarchical()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 799460
    iput-object p1, p0, LX/4gP;->c:Landroid/net/Uri;

    .line 799461
    return-object p0
.end method

.method public final a(Lcom/facebook/ipc/media/data/MimeType;)LX/4gP;
    .locals 1

    .prologue
    .line 799462
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/data/MimeType;

    iput-object v0, p0, LX/4gP;->d:Lcom/facebook/ipc/media/data/MimeType;

    .line 799463
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/4gP;
    .locals 1

    .prologue
    .line 799464
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/4gP;->a:Ljava/lang/String;

    .line 799465
    return-object p0
.end method

.method public final a()Lcom/facebook/ipc/media/data/MediaData;
    .locals 2

    .prologue
    .line 799466
    new-instance v0, Lcom/facebook/ipc/media/data/MediaData;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/media/data/MediaData;-><init>(LX/4gP;)V

    return-object v0
.end method

.method public final b(D)LX/4gP;
    .locals 3

    .prologue
    .line 799467
    const-wide v0, -0x3f99800000000000L    # -180.0

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x4066800000000000L    # 180.0

    cmpg-double v0, p1, v0

    if-lez v0, :cond_1

    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 799468
    iput-wide p1, p0, LX/4gP;->k:D

    .line 799469
    return-object p0

    .line 799470
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
