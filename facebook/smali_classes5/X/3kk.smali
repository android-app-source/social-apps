.class public LX/3kk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0i1;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 633257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/3kk;
    .locals 1

    .prologue
    .line 633240
    invoke-static {}, LX/3kk;->d()LX/3kk;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/view/View;Landroid/view/View;Landroid/content/Context;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 633249
    new-instance v0, LX/0hs;

    const/4 v1, 0x2

    invoke-direct {v0, p2, v1}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 633250
    invoke-virtual {v0, p3}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 633251
    sget-object v1, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 633252
    const/4 v1, -0x1

    .line 633253
    iput v1, v0, LX/0hs;->t:I

    .line 633254
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, LX/0ht;->a(Landroid/view/View;IIII)V

    .line 633255
    invoke-virtual {v0}, LX/0ht;->d()V

    .line 633256
    return-void
.end method

.method private static d()LX/3kk;
    .locals 1

    .prologue
    .line 633247
    new-instance v0, LX/3kk;

    invoke-direct {v0}, LX/3kk;-><init>()V

    .line 633248
    return-object v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 633246
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 633245
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    return-object v0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 633244
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 633243
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 633242
    const-string v0, "3795"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 633241
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_CALL_TO_ACTION_BUTTON:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
