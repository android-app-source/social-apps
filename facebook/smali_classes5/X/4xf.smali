.class public final LX/4xf;
.super LX/1M3;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1M3",
        "<TK;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4xg;


# direct methods
.method public constructor <init>(LX/4xg;)V
    .locals 0

    .prologue
    .line 821217
    iput-object p1, p0, LX/4xf;->a:LX/4xg;

    invoke-direct {p0}, LX/1M3;-><init>()V

    return-void
.end method

.method private a(LX/0Rl;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rl",
            "<-",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TK;>;>;)Z"
        }
    .end annotation

    .prologue
    .line 821212
    iget-object v0, p0, LX/4xf;->a:LX/4xg;

    iget-object v0, v0, LX/4xg;->a:LX/4xj;

    new-instance v1, LX/4xe;

    invoke-direct {v1, p0, p1}, LX/4xe;-><init>(LX/4xf;LX/0Rl;)V

    invoke-virtual {v0, v1}, LX/4xj;->a(LX/0Rl;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()LX/1M1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1M1",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 821216
    iget-object v0, p0, LX/4xf;->a:LX/4xg;

    return-object v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TK;>;>;"
        }
    .end annotation

    .prologue
    .line 821218
    iget-object v0, p0, LX/4xf;->a:LX/4xg;

    invoke-virtual {v0}, LX/2Tb;->b()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 821215
    invoke-static {p1}, LX/0Rj;->in(Ljava/util/Collection;)LX/0Rl;

    move-result-object v0

    invoke-direct {p0, v0}, LX/4xf;->a(LX/0Rl;)Z

    move-result v0

    return v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 821214
    invoke-static {p1}, LX/0Rj;->in(Ljava/util/Collection;)LX/0Rl;

    move-result-object v0

    invoke-static {v0}, LX/0Rj;->not(LX/0Rl;)LX/0Rl;

    move-result-object v0

    invoke-direct {p0, v0}, LX/4xf;->a(LX/0Rl;)Z

    move-result v0

    return v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 821213
    iget-object v0, p0, LX/4xf;->a:LX/4xg;

    iget-object v0, v0, LX/4xg;->a:LX/4xj;

    invoke-virtual {v0}, LX/4xj;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method
