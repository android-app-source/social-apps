.class public final LX/4fC;
.super LX/1eP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1eP",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4fG;

.field private final b:LX/1BV;

.field private final c:Ljava/lang/String;

.field private final d:LX/33B;

.field private e:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "PostprocessorConsumer.this"
    .end annotation
.end field

.field public f:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "PostprocessorConsumer.this"
    .end annotation
.end field

.field public g:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "PostprocessorConsumer.this"
    .end annotation
.end field

.field public h:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "PostprocessorConsumer.this"
    .end annotation
.end field

.field private i:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "PostprocessorConsumer.this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/4fG;LX/1cd;LX/1BV;Ljava/lang/String;LX/33B;LX/1cW;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;",
            "LX/1BV;",
            "Ljava/lang/String;",
            "LX/33B;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 798147
    iput-object p1, p0, LX/4fC;->a:LX/4fG;

    .line 798148
    invoke-direct {p0, p2}, LX/1eP;-><init>(LX/1cd;)V

    .line 798149
    const/4 v0, 0x0

    iput-object v0, p0, LX/4fC;->f:LX/1FJ;

    .line 798150
    iput-boolean v1, p0, LX/4fC;->g:Z

    .line 798151
    iput-boolean v1, p0, LX/4fC;->h:Z

    .line 798152
    iput-boolean v1, p0, LX/4fC;->i:Z

    .line 798153
    iput-object p3, p0, LX/4fC;->b:LX/1BV;

    .line 798154
    iput-object p4, p0, LX/4fC;->c:Ljava/lang/String;

    .line 798155
    iput-object p5, p0, LX/4fC;->d:LX/33B;

    .line 798156
    new-instance v0, LX/4fB;

    invoke-direct {v0, p0, p1}, LX/4fB;-><init>(LX/4fC;LX/4fG;)V

    invoke-virtual {p6, v0}, LX/1cW;->a(LX/1cg;)V

    .line 798157
    return-void
.end method

.method private static a(LX/1BV;Ljava/lang/String;LX/33B;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1BV;",
            "Ljava/lang/String;",
            "LX/33B;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 798109
    invoke-interface {p0, p1}, LX/1BV;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 798110
    const/4 v0, 0x0

    .line 798111
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Postprocessor"

    invoke-interface {p2}, LX/33B;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/2oC;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(LX/4fC;LX/1ln;)LX/1FJ;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ln;",
            ")",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;"
        }
    .end annotation

    .prologue
    .line 798112
    move-object v0, p1

    check-cast v0, LX/1ll;

    .line 798113
    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 798114
    iget-object v2, p0, LX/4fC;->d:LX/33B;

    iget-object v3, p0, LX/4fC;->a:LX/4fG;

    iget-object v3, v3, LX/4fG;->b:LX/1FZ;

    invoke-interface {v2, v1, v3}, LX/33B;->a(Landroid/graphics/Bitmap;LX/1FZ;)LX/1FJ;

    move-result-object v1

    .line 798115
    iget v2, v0, LX/1ll;->d:I

    move v0, v2

    .line 798116
    :try_start_0
    new-instance v2, LX/1ll;

    invoke-virtual {p1}, LX/1ln;->d()LX/1lk;

    move-result-object v3

    invoke-direct {v2, v1, v3, v0}, LX/1ll;-><init>(LX/1FJ;LX/1lk;I)V

    invoke-static {v2}, LX/1FJ;->a(Ljava/io/Closeable;)LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 798117
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method

.method private b(LX/1FJ;Z)V
    .locals 2
    .param p1    # LX/1FJ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 798118
    monitor-enter p0

    .line 798119
    :try_start_0
    iget-boolean v0, p0, LX/4fC;->e:Z

    if-eqz v0, :cond_1

    .line 798120
    monitor-exit p0

    .line 798121
    :cond_0
    :goto_0
    return-void

    .line 798122
    :cond_1
    iget-object v0, p0, LX/4fC;->f:LX/1FJ;

    .line 798123
    invoke-static {p1}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;

    move-result-object v1

    iput-object v1, p0, LX/4fC;->f:LX/1FJ;

    .line 798124
    iput-boolean p2, p0, LX/4fC;->g:Z

    .line 798125
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/4fC;->h:Z

    .line 798126
    invoke-static {p0}, LX/4fC;->f(LX/4fC;)Z

    move-result v1

    .line 798127
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 798128
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 798129
    if-eqz v1, :cond_0

    .line 798130
    invoke-static {p0}, LX/4fC;->c(LX/4fC;)V

    goto :goto_0

    .line 798131
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static c(LX/4fC;)V
    .locals 3

    .prologue
    .line 798158
    iget-object v0, p0, LX/4fC;->a:LX/4fG;

    iget-object v0, v0, LX/4fG;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/imagepipeline/producers/PostprocessorProducer$PostprocessorConsumer$2;

    invoke-direct {v1, p0}, Lcom/facebook/imagepipeline/producers/PostprocessorProducer$PostprocessorConsumer$2;-><init>(LX/4fC;)V

    const v2, 0x65230699

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 798159
    return-void
.end method

.method public static c(LX/4fC;LX/1FJ;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 798132
    invoke-static {p1}, LX/1FJ;->a(LX/1FJ;)Z

    move-result v0

    invoke-static {v0}, LX/03g;->a(Z)V

    .line 798133
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ln;

    .line 798134
    instance-of v2, v0, LX/1ll;

    move v0, v2

    .line 798135
    if-nez v0, :cond_0

    .line 798136
    invoke-static {p0, p1, p2}, LX/4fC;->d(LX/4fC;LX/1FJ;Z)V

    .line 798137
    :goto_0
    return-void

    .line 798138
    :cond_0
    iget-object v0, p0, LX/4fC;->b:LX/1BV;

    iget-object v2, p0, LX/4fC;->c:Ljava/lang/String;

    const-string v3, "PostprocessorProducer"

    invoke-interface {v0, v2, v3}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 798139
    :try_start_0
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ln;

    invoke-static {p0, v0}, LX/4fC;->b(LX/4fC;LX/1ln;)LX/1FJ;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 798140
    :try_start_1
    iget-object v0, p0, LX/4fC;->b:LX/1BV;

    iget-object v2, p0, LX/4fC;->c:Ljava/lang/String;

    const-string v3, "PostprocessorProducer"

    iget-object v4, p0, LX/4fC;->b:LX/1BV;

    iget-object v5, p0, LX/4fC;->c:Ljava/lang/String;

    iget-object v6, p0, LX/4fC;->d:LX/33B;

    invoke-static {v4, v5, v6}, LX/4fC;->a(LX/1BV;Ljava/lang/String;LX/33B;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v0, v2, v3, v4}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 798141
    invoke-static {p0, v1, p2}, LX/4fC;->d(LX/4fC;LX/1FJ;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 798142
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    .line 798143
    :catch_0
    move-exception v0

    .line 798144
    :try_start_2
    iget-object v2, p0, LX/4fC;->b:LX/1BV;

    iget-object v3, p0, LX/4fC;->c:Ljava/lang/String;

    const-string v4, "PostprocessorProducer"

    iget-object v5, p0, LX/4fC;->b:LX/1BV;

    iget-object v6, p0, LX/4fC;->c:Ljava/lang/String;

    iget-object v7, p0, LX/4fC;->d:LX/33B;

    invoke-static {v5, v6, v7}, LX/4fC;->a(LX/1BV;Ljava/lang/String;LX/33B;)Ljava/util/Map;

    move-result-object v5

    invoke-interface {v2, v3, v4, v0, v5}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    .line 798145
    invoke-static {p0, v0}, LX/4fC;->c(LX/4fC;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 798146
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method

.method private static c(LX/4fC;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 798101
    invoke-direct {p0}, LX/4fC;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 798102
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 798103
    invoke-virtual {v0, p1}, LX/1cd;->b(Ljava/lang/Throwable;)V

    .line 798104
    :cond_0
    return-void
.end method

.method private static d(LX/4fC;LX/1FJ;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 798105
    if-nez p2, :cond_0

    invoke-direct {p0}, LX/4fC;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    if-eqz p2, :cond_2

    invoke-direct {p0}, LX/4fC;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 798106
    :cond_1
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 798107
    invoke-virtual {v0, p1, p2}, LX/1cd;->b(Ljava/lang/Object;Z)V

    .line 798108
    :cond_2
    return-void
.end method

.method public static e(LX/4fC;)V
    .locals 1

    .prologue
    .line 798093
    monitor-enter p0

    .line 798094
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/4fC;->i:Z

    .line 798095
    invoke-static {p0}, LX/4fC;->f(LX/4fC;)Z

    move-result v0

    .line 798096
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 798097
    if-eqz v0, :cond_0

    .line 798098
    invoke-static {p0}, LX/4fC;->c(LX/4fC;)V

    .line 798099
    :cond_0
    return-void

    .line 798100
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static declared-synchronized f(LX/4fC;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 798089
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, LX/4fC;->e:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, LX/4fC;->h:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, LX/4fC;->i:Z

    if-nez v1, :cond_0

    iget-object v1, p0, LX/4fC;->f:LX/1FJ;

    invoke-static {v1}, LX/1FJ;->a(LX/1FJ;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 798090
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/4fC;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 798091
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 798092
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static g(LX/4fC;)V
    .locals 1

    .prologue
    .line 798064
    invoke-direct {p0}, LX/4fC;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 798065
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 798066
    invoke-virtual {v0}, LX/1cd;->b()V

    .line 798067
    :cond_0
    return-void
.end method

.method private declared-synchronized h()Z
    .locals 1

    .prologue
    .line 798088
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/4fC;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private i()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 798078
    monitor-enter p0

    .line 798079
    :try_start_0
    iget-boolean v1, p0, LX/4fC;->e:Z

    if-eqz v1, :cond_0

    .line 798080
    const/4 v0, 0x0

    monitor-exit p0

    .line 798081
    :goto_0
    return v0

    .line 798082
    :cond_0
    iget-object v1, p0, LX/4fC;->f:LX/1FJ;

    .line 798083
    const/4 v2, 0x0

    iput-object v2, p0, LX/4fC;->f:LX/1FJ;

    .line 798084
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/4fC;->e:Z

    .line 798085
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 798086
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    .line 798087
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 798076
    invoke-static {p0}, LX/4fC;->g(LX/4fC;)V

    .line 798077
    return-void
.end method

.method public final a(Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 798070
    check-cast p1, LX/1FJ;

    .line 798071
    invoke-static {p1}, LX/1FJ;->a(LX/1FJ;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 798072
    if-eqz p2, :cond_0

    .line 798073
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, LX/4fC;->d(LX/4fC;LX/1FJ;Z)V

    .line 798074
    :cond_0
    :goto_0
    return-void

    .line 798075
    :cond_1
    invoke-direct {p0, p1, p2}, LX/4fC;->b(LX/1FJ;Z)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 798068
    invoke-static {p0, p1}, LX/4fC;->c(LX/4fC;Ljava/lang/Throwable;)V

    .line 798069
    return-void
.end method
