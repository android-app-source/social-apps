.class public final LX/3pQ;
.super LX/3pP;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 641564
    invoke-direct {p0}, LX/3pP;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 641565
    invoke-virtual {p1}, Landroid/app/Activity;->getParentActivityIntent()Landroid/content/Intent;

    move-result-object v0

    move-object v0, v0

    .line 641566
    if-nez v0, :cond_0

    .line 641567
    invoke-super {p0, p1}, LX/3pP;->a(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    move-object v0, v0

    .line 641568
    :cond_0
    return-object v0
.end method

.method public final a(Landroid/content/Context;Landroid/content/pm/ActivityInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 641569
    iget-object v0, p2, Landroid/content/pm/ActivityInfo;->parentActivityName:Ljava/lang/String;

    move-object v0, v0

    .line 641570
    if-nez v0, :cond_0

    .line 641571
    invoke-super {p0, p1, p2}, LX/3pP;->a(Landroid/content/Context;Landroid/content/pm/ActivityInfo;)Ljava/lang/String;

    move-result-object v0

    .line 641572
    :cond_0
    return-object v0
.end method

.method public final a(Landroid/app/Activity;Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 641573
    invoke-virtual {p1, p2}, Landroid/app/Activity;->shouldUpRecreateTask(Landroid/content/Intent;)Z

    move-result v0

    move v0, v0

    .line 641574
    return v0
.end method
