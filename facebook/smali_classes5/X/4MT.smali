.class public LX/4MT;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 688296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 53

    .prologue
    .line 687990
    const/16 v49, 0x0

    .line 687991
    const/16 v48, 0x0

    .line 687992
    const/16 v47, 0x0

    .line 687993
    const/16 v46, 0x0

    .line 687994
    const/16 v45, 0x0

    .line 687995
    const/16 v44, 0x0

    .line 687996
    const/16 v43, 0x0

    .line 687997
    const/16 v42, 0x0

    .line 687998
    const/16 v41, 0x0

    .line 687999
    const/16 v40, 0x0

    .line 688000
    const/16 v39, 0x0

    .line 688001
    const/16 v38, 0x0

    .line 688002
    const/16 v37, 0x0

    .line 688003
    const/16 v36, 0x0

    .line 688004
    const/16 v35, 0x0

    .line 688005
    const/16 v34, 0x0

    .line 688006
    const/16 v33, 0x0

    .line 688007
    const/16 v32, 0x0

    .line 688008
    const/16 v31, 0x0

    .line 688009
    const/16 v30, 0x0

    .line 688010
    const/16 v29, 0x0

    .line 688011
    const/16 v28, 0x0

    .line 688012
    const/16 v27, 0x0

    .line 688013
    const/16 v26, 0x0

    .line 688014
    const/16 v25, 0x0

    .line 688015
    const/16 v24, 0x0

    .line 688016
    const/16 v23, 0x0

    .line 688017
    const/16 v22, 0x0

    .line 688018
    const/16 v21, 0x0

    .line 688019
    const/16 v20, 0x0

    .line 688020
    const/16 v19, 0x0

    .line 688021
    const/16 v18, 0x0

    .line 688022
    const/16 v17, 0x0

    .line 688023
    const/16 v16, 0x0

    .line 688024
    const/4 v15, 0x0

    .line 688025
    const/4 v14, 0x0

    .line 688026
    const/4 v13, 0x0

    .line 688027
    const/4 v12, 0x0

    .line 688028
    const/4 v11, 0x0

    .line 688029
    const/4 v10, 0x0

    .line 688030
    const/4 v9, 0x0

    .line 688031
    const/4 v8, 0x0

    .line 688032
    const/4 v7, 0x0

    .line 688033
    const/4 v6, 0x0

    .line 688034
    const/4 v5, 0x0

    .line 688035
    const/4 v4, 0x0

    .line 688036
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v50

    sget-object v51, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v50

    move-object/from16 v1, v51

    if-eq v0, v1, :cond_1

    .line 688037
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 688038
    const/4 v4, 0x0

    .line 688039
    :goto_0
    return v4

    .line 688040
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 688041
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v50

    sget-object v51, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v50

    move-object/from16 v1, v51

    if-eq v0, v1, :cond_1f

    .line 688042
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v50

    .line 688043
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 688044
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v51

    sget-object v52, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    if-eq v0, v1, :cond_1

    if-eqz v50, :cond_1

    .line 688045
    const-string v51, "composer_placeholder_text"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_2

    .line 688046
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, p1

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v49

    goto :goto_1

    .line 688047
    :cond_2
    const-string v51, "customizable"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_3

    .line 688048
    const/16 v19, 0x1

    .line 688049
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v48

    goto :goto_1

    .line 688050
    :cond_3
    const-string v51, "disabled_favorite_icon"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_4

    .line 688051
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v47

    goto :goto_1

    .line 688052
    :cond_4
    const-string v51, "enabled_favorite_icon"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_5

    .line 688053
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v46

    goto :goto_1

    .line 688054
    :cond_5
    const-string v51, "header_image"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_6

    .line 688055
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v45

    goto :goto_1

    .line 688056
    :cond_6
    const-string v51, "id"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_7

    .line 688057
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v44

    goto :goto_1

    .line 688058
    :cond_7
    const-string v51, "is_favorited"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_8

    .line 688059
    const/16 v18, 0x1

    .line 688060
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v43

    goto/16 :goto_1

    .line 688061
    :cond_8
    const-string v51, "live_video_count"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_9

    .line 688062
    const/16 v17, 0x1

    .line 688063
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v42

    goto/16 :goto_1

    .line 688064
    :cond_9
    const-string v51, "name"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_a

    .line 688065
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v41

    goto/16 :goto_1

    .line 688066
    :cond_a
    const-string v51, "searchable"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_b

    .line 688067
    const/16 v16, 0x1

    .line 688068
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v40

    goto/16 :goto_1

    .line 688069
    :cond_b
    const-string v51, "square_header_image"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_c

    .line 688070
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v39

    goto/16 :goto_1

    .line 688071
    :cond_c
    const-string v51, "url"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_d

    .line 688072
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v38

    goto/16 :goto_1

    .line 688073
    :cond_d
    const-string v51, "video_channel_can_viewer_follow"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_e

    .line 688074
    const/4 v15, 0x1

    .line 688075
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v37

    goto/16 :goto_1

    .line 688076
    :cond_e
    const-string v51, "video_channel_can_viewer_pin"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_f

    .line 688077
    const/4 v14, 0x1

    .line 688078
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v36

    goto/16 :goto_1

    .line 688079
    :cond_f
    const-string v51, "video_channel_can_viewer_subscribe"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_10

    .line 688080
    const/4 v13, 0x1

    .line 688081
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v35

    goto/16 :goto_1

    .line 688082
    :cond_10
    const-string v51, "video_channel_curator_profile"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_11

    .line 688083
    invoke-static/range {p0 .. p1}, LX/2aw;->a(LX/15w;LX/186;)I

    move-result v34

    goto/16 :goto_1

    .line 688084
    :cond_11
    const-string v51, "video_channel_has_new"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_12

    .line 688085
    const/4 v12, 0x1

    .line 688086
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v33

    goto/16 :goto_1

    .line 688087
    :cond_12
    const-string v51, "video_channel_has_viewer_subscribed"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_13

    .line 688088
    const/4 v11, 0x1

    .line 688089
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v32

    goto/16 :goto_1

    .line 688090
    :cond_13
    const-string v51, "video_channel_is_viewer_following"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_14

    .line 688091
    const/4 v10, 0x1

    .line 688092
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v31

    goto/16 :goto_1

    .line 688093
    :cond_14
    const-string v51, "video_channel_is_viewer_pinned"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_15

    .line 688094
    const/4 v9, 0x1

    .line 688095
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v30

    goto/16 :goto_1

    .line 688096
    :cond_15
    const-string v51, "video_channel_max_new_count"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_16

    .line 688097
    const/4 v8, 0x1

    .line 688098
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v29

    goto/16 :goto_1

    .line 688099
    :cond_16
    const-string v51, "video_channel_new_count"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_17

    .line 688100
    const/4 v7, 0x1

    .line 688101
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v28

    goto/16 :goto_1

    .line 688102
    :cond_17
    const-string v51, "video_channel_subtitle"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_18

    .line 688103
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v27

    goto/16 :goto_1

    .line 688104
    :cond_18
    const-string v51, "video_channel_title"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_19

    .line 688105
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v26

    goto/16 :goto_1

    .line 688106
    :cond_19
    const-string v51, "audience_social_sentence"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_1a

    .line 688107
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v25

    goto/16 :goto_1

    .line 688108
    :cond_1a
    const-string v51, "show_audience_header"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_1b

    .line 688109
    const/4 v6, 0x1

    .line 688110
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v24

    goto/16 :goto_1

    .line 688111
    :cond_1b
    const-string v51, "composer_actions"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_1c

    .line 688112
    invoke-static/range {p0 .. p1}, LX/4U0;->a(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 688113
    :cond_1c
    const-string v51, "should_prefetch"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_1d

    .line 688114
    const/4 v5, 0x1

    .line 688115
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v22

    goto/16 :goto_1

    .line 688116
    :cond_1d
    const-string v51, "feed_section_type"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_1e

    .line 688117
    const/4 v4, 0x1

    .line 688118
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    move-result-object v21

    goto/16 :goto_1

    .line 688119
    :cond_1e
    const-string v51, "video_channel_curator"

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_0

    .line 688120
    invoke-static/range {p0 .. p1}, LX/2bR;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 688121
    :cond_1f
    const/16 v50, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 688122
    const/16 v50, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v50

    move/from16 v2, v49

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 688123
    if-eqz v19, :cond_20

    .line 688124
    const/16 v19, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 688125
    :cond_20
    const/16 v19, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v47

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 688126
    const/16 v19, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v46

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 688127
    const/16 v19, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v45

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 688128
    const/16 v19, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v44

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 688129
    if-eqz v18, :cond_21

    .line 688130
    const/16 v18, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v43

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 688131
    :cond_21
    if-eqz v17, :cond_22

    .line 688132
    const/16 v17, 0x8

    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v42

    move/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, LX/186;->a(III)V

    .line 688133
    :cond_22
    const/16 v17, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 688134
    if-eqz v16, :cond_23

    .line 688135
    const/16 v16, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 688136
    :cond_23
    const/16 v16, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v39

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 688137
    const/16 v16, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v38

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 688138
    if-eqz v15, :cond_24

    .line 688139
    const/16 v15, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v15, v1}, LX/186;->a(IZ)V

    .line 688140
    :cond_24
    if-eqz v14, :cond_25

    .line 688141
    const/16 v14, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v14, v1}, LX/186;->a(IZ)V

    .line 688142
    :cond_25
    if-eqz v13, :cond_26

    .line 688143
    const/16 v13, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v13, v1}, LX/186;->a(IZ)V

    .line 688144
    :cond_26
    const/16 v13, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v13, v1}, LX/186;->b(II)V

    .line 688145
    if-eqz v12, :cond_27

    .line 688146
    const/16 v12, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 688147
    :cond_27
    if-eqz v11, :cond_28

    .line 688148
    const/16 v11, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 688149
    :cond_28
    if-eqz v10, :cond_29

    .line 688150
    const/16 v10, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 688151
    :cond_29
    if-eqz v9, :cond_2a

    .line 688152
    const/16 v9, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 688153
    :cond_2a
    if-eqz v8, :cond_2b

    .line 688154
    const/16 v8, 0x15

    const/4 v9, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v8, v1, v9}, LX/186;->a(III)V

    .line 688155
    :cond_2b
    if-eqz v7, :cond_2c

    .line 688156
    const/16 v7, 0x16

    const/4 v8, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v7, v1, v8}, LX/186;->a(III)V

    .line 688157
    :cond_2c
    const/16 v7, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 688158
    const/16 v7, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 688159
    const/16 v7, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 688160
    if-eqz v6, :cond_2d

    .line 688161
    const/16 v6, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 688162
    :cond_2d
    const/16 v6, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 688163
    if-eqz v5, :cond_2e

    .line 688164
    const/16 v5, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 688165
    :cond_2e
    if-eqz v4, :cond_2f

    .line 688166
    const/16 v4, 0x1d

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 688167
    :cond_2f
    const/16 v4, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 688168
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/16 v3, 0x1d

    const/4 v2, 0x0

    .line 688169
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 688170
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 688171
    if-eqz v0, :cond_0

    .line 688172
    const-string v1, "composer_placeholder_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688173
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 688174
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 688175
    if-eqz v0, :cond_1

    .line 688176
    const-string v1, "customizable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688177
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 688178
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688179
    if-eqz v0, :cond_2

    .line 688180
    const-string v1, "disabled_favorite_icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688181
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 688182
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688183
    if-eqz v0, :cond_3

    .line 688184
    const-string v1, "enabled_favorite_icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688185
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 688186
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688187
    if-eqz v0, :cond_4

    .line 688188
    const-string v1, "header_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688189
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 688190
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 688191
    if-eqz v0, :cond_5

    .line 688192
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688193
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 688194
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 688195
    if-eqz v0, :cond_6

    .line 688196
    const-string v1, "is_favorited"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688197
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 688198
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 688199
    if-eqz v0, :cond_7

    .line 688200
    const-string v1, "live_video_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688201
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 688202
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 688203
    if-eqz v0, :cond_8

    .line 688204
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688205
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 688206
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 688207
    if-eqz v0, :cond_9

    .line 688208
    const-string v1, "searchable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688209
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 688210
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688211
    if-eqz v0, :cond_a

    .line 688212
    const-string v1, "square_header_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688213
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 688214
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 688215
    if-eqz v0, :cond_b

    .line 688216
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688217
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 688218
    :cond_b
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 688219
    if-eqz v0, :cond_c

    .line 688220
    const-string v1, "video_channel_can_viewer_follow"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688221
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 688222
    :cond_c
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 688223
    if-eqz v0, :cond_d

    .line 688224
    const-string v1, "video_channel_can_viewer_pin"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688225
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 688226
    :cond_d
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 688227
    if-eqz v0, :cond_e

    .line 688228
    const-string v1, "video_channel_can_viewer_subscribe"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688229
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 688230
    :cond_e
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688231
    if-eqz v0, :cond_f

    .line 688232
    const-string v1, "video_channel_curator_profile"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688233
    invoke-static {p0, v0, p2, p3}, LX/2aw;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 688234
    :cond_f
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 688235
    if-eqz v0, :cond_10

    .line 688236
    const-string v1, "video_channel_has_new"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688237
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 688238
    :cond_10
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 688239
    if-eqz v0, :cond_11

    .line 688240
    const-string v1, "video_channel_has_viewer_subscribed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688241
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 688242
    :cond_11
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 688243
    if-eqz v0, :cond_12

    .line 688244
    const-string v1, "video_channel_is_viewer_following"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688245
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 688246
    :cond_12
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 688247
    if-eqz v0, :cond_13

    .line 688248
    const-string v1, "video_channel_is_viewer_pinned"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688249
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 688250
    :cond_13
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 688251
    if-eqz v0, :cond_14

    .line 688252
    const-string v1, "video_channel_max_new_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688253
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 688254
    :cond_14
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 688255
    if-eqz v0, :cond_15

    .line 688256
    const-string v1, "video_channel_new_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688257
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 688258
    :cond_15
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688259
    if-eqz v0, :cond_16

    .line 688260
    const-string v1, "video_channel_subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688261
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 688262
    :cond_16
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688263
    if-eqz v0, :cond_17

    .line 688264
    const-string v1, "video_channel_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688265
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 688266
    :cond_17
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688267
    if-eqz v0, :cond_18

    .line 688268
    const-string v1, "audience_social_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688269
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 688270
    :cond_18
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 688271
    if-eqz v0, :cond_19

    .line 688272
    const-string v1, "show_audience_header"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688273
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 688274
    :cond_19
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688275
    if-eqz v0, :cond_1b

    .line 688276
    const-string v1, "composer_actions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688277
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 688278
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_1a

    .line 688279
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2, p3}, LX/4U0;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 688280
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 688281
    :cond_1a
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 688282
    :cond_1b
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 688283
    if-eqz v0, :cond_1c

    .line 688284
    const-string v1, "should_prefetch"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688285
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 688286
    :cond_1c
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 688287
    if-eqz v0, :cond_1d

    .line 688288
    const-string v0, "feed_section_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688289
    const-class v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 688290
    :cond_1d
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688291
    if-eqz v0, :cond_1e

    .line 688292
    const-string v1, "video_channel_curator"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688293
    invoke-static {p0, v0, p2, p3}, LX/2bR;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 688294
    :cond_1e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 688295
    return-void
.end method
