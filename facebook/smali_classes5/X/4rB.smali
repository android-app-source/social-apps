.class public LX/4rB;
.super LX/4rA;
.source ""


# direct methods
.method public constructor <init>(LX/0lJ;LX/0li;)V
    .locals 0

    .prologue
    .line 815200
    invoke-direct {p0, p1, p2}, LX/4rA;-><init>(LX/0lJ;LX/0li;)V

    .line 815201
    return-void
.end method

.method private b(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 815202
    const-class v0, Ljava/lang/Enum;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 815203
    invoke-virtual {p2}, Ljava/lang/Class;->isEnum()Z

    move-result v0

    if-nez v0, :cond_0

    .line 815204
    invoke-virtual {p2}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object p2

    .line 815205
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 815206
    const-string v1, "java.util"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 815207
    instance-of v1, p1, Ljava/util/EnumSet;

    if-eqz v1, :cond_2

    .line 815208
    check-cast p1, Ljava/util/EnumSet;

    invoke-static {p1}, LX/1Xw;->a(Ljava/util/EnumSet;)Ljava/lang/Class;

    move-result-object v0

    .line 815209
    sget-object v1, LX/0li;->a:LX/0li;

    move-object v1, v1

    .line 815210
    const-class v2, Ljava/util/EnumSet;

    invoke-virtual {v1, v2, v0}, LX/0li;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/267;

    move-result-object v0

    invoke-virtual {v0}, LX/0lK;->a()Ljava/lang/String;

    move-result-object v0

    .line 815211
    :cond_1
    :goto_0
    return-object v0

    .line 815212
    :cond_2
    instance-of v1, p1, Ljava/util/EnumMap;

    if-eqz v1, :cond_3

    .line 815213
    check-cast p1, Ljava/util/EnumMap;

    invoke-static {p1}, LX/1Xw;->a(Ljava/util/EnumMap;)Ljava/lang/Class;

    move-result-object v0

    .line 815214
    const-class v1, Ljava/lang/Object;

    .line 815215
    sget-object v2, LX/0li;->a:LX/0li;

    move-object v2, v2

    .line 815216
    const-class v3, Ljava/util/EnumMap;

    invoke-virtual {v2, v3, v0, v1}, LX/0li;->a(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;)LX/1Xn;

    move-result-object v0

    invoke-virtual {v0}, LX/0lK;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 815217
    :cond_3
    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 815218
    const-string v2, ".Arrays$"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, ".Collections$"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_4
    const-string v1, "List"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_1

    .line 815219
    const-string v0, "java.util.ArrayList"

    goto :goto_0

    .line 815220
    :cond_5
    const/16 v1, 0x24

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-ltz v1, :cond_1

    .line 815221
    invoke-static {p2}, LX/1Xw;->b(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    .line 815222
    if-eqz v1, :cond_1

    .line 815223
    iget-object v1, p0, LX/4rA;->d:LX/0lJ;

    .line 815224
    iget-object v2, v1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v1, v2

    .line 815225
    invoke-static {v1}, LX/1Xw;->b(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_1

    .line 815226
    iget-object v0, p0, LX/4rA;->d:LX/0lJ;

    .line 815227
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 815228
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)LX/0lJ;
    .locals 4

    .prologue
    .line 815229
    const/16 v0, 0x3c

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-lez v0, :cond_0

    .line 815230
    iget-object v0, p0, LX/4rA;->c:LX/0li;

    invoke-virtual {v0, p1}, LX/0li;->a(Ljava/lang/String;)LX/0lJ;

    move-result-object v0

    .line 815231
    :goto_0
    return-object v0

    .line 815232
    :cond_0
    :try_start_0
    invoke-static {p1}, LX/1Xw;->a(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 815233
    iget-object v1, p0, LX/4rA;->c:LX/0li;

    iget-object v2, p0, LX/4rA;->d:LX/0lJ;

    invoke-virtual {v1, v2, v0}, LX/0li;->a(LX/0lJ;Ljava/lang/Class;)LX/0lJ;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 815234
    :catch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid type id \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' (for id type \'Id.class\'): no such class found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 815235
    :catch_1
    move-exception v0

    .line 815236
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid type id \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' (for id type \'Id.class\'): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 815237
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/4rB;->b(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 815238
    invoke-direct {p0, p1, p2}, LX/4rB;->b(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
