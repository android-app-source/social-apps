.class public final LX/4er;
.super LX/1eS;
.source ""


# instance fields
.field public final synthetic a:LX/1cQ;


# direct methods
.method public constructor <init>(LX/1cQ;LX/1cd;LX/1cW;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 797580
    iput-object p1, p0, LX/4er;->a:LX/1cQ;

    .line 797581
    invoke-direct {p0, p1, p2, p3, p4}, LX/1eS;-><init>(LX/1cQ;LX/1cd;LX/1cW;Z)V

    .line 797582
    return-void
.end method


# virtual methods
.method public final a(LX/1FL;)I
    .locals 1

    .prologue
    .line 797583
    invoke-virtual {p1}, LX/1FL;->i()I

    move-result v0

    return v0
.end method

.method public final declared-synchronized a(LX/1FL;Z)Z
    .locals 1

    .prologue
    .line 797584
    monitor-enter p0

    if-nez p2, :cond_0

    .line 797585
    const/4 v0, 0x0

    .line 797586
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_0
    invoke-super {p0, p1, p2}, LX/1eS;->a(LX/1FL;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    goto :goto_0

    .line 797587
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()LX/1lk;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 797588
    invoke-static {v0, v0, v0}, LX/1lk;->a(IZZ)LX/1lk;

    move-result-object v0

    return-object v0
.end method
