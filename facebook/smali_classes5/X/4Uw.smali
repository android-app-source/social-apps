.class public final LX/4Uw;
.super LX/4Uv;
.source ""


# instance fields
.field private final a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "LX/4Zs;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .param p1    # Ljava/util/Collection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LX/4Zs;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 741820
    invoke-direct {p0}, LX/4Uv;-><init>()V

    .line 741821
    iput-object p1, p0, LX/4Uw;->a:Ljava/util/Collection;

    .line 741822
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 741823
    invoke-virtual {p0}, LX/4Uv;->f()I

    move-result v0

    invoke-static {v0}, LX/4V0;->d(I)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 741824
    invoke-virtual {p0}, LX/4Uv;->f()I

    move-result v0

    invoke-static {v0}, LX/4V0;->c(I)I

    move-result v0

    move v5, v0

    .line 741825
    if-ltz v5, :cond_2

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 741826
    iget-object v0, p0, LX/4Uw;->c:Ljava/lang/String;

    invoke-static {v0, p1}, LX/2lj;->a(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 741827
    iget-object v0, p0, LX/4Uw;->a:Ljava/util/Collection;

    iget-object v1, p0, LX/4Uw;->c:Ljava/lang/String;

    iget-object v2, p0, LX/4Uw;->b:Ljava/lang/String;

    invoke-virtual {p0}, LX/4Uv;->e()I

    move-result v4

    move-object v3, p1

    .line 741828
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 741829
    new-instance v6, LX/4Zs;

    move-object v7, v1

    move-object v8, v2

    move-object v9, v3

    move v10, v4

    move v11, v5

    invoke-direct/range {v6 .. v11}, LX/4Zs;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    invoke-interface {v0, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 741830
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 741831
    goto :goto_0

    :cond_2
    move v1, v2

    .line 741832
    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 741833
    iput-object p1, p0, LX/4Uw;->b:Ljava/lang/String;

    .line 741834
    iput-object p2, p0, LX/4Uw;->c:Ljava/lang/String;

    .line 741835
    iget-object v0, p0, LX/4Uv;->d:LX/446;

    invoke-virtual {v0}, LX/446;->c()V

    .line 741836
    iget-object v0, p0, LX/4Uv;->e:LX/446;

    invoke-virtual {v0}, LX/446;->c()V

    .line 741837
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 741838
    iget-object v0, p0, LX/4Uw;->a:Ljava/util/Collection;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
