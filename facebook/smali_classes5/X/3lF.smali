.class public LX/3lF;
.super LX/16T;
.source ""

# interfaces
.implements LX/16E;


# instance fields
.field private final a:LX/19m;


# direct methods
.method public constructor <init>(LX/19m;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 633657
    invoke-direct {p0}, LX/16T;-><init>()V

    .line 633658
    iput-object p1, p0, LX/3lF;->a:LX/19m;

    .line 633659
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 633660
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 633661
    iget-object v0, p0, LX/3lF;->a:LX/19m;

    .line 633662
    iget-object p1, v0, LX/19m;->p:Ljava/lang/String;

    move-object v0, p1

    .line 633663
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3lF;->a:LX/19m;

    .line 633664
    iget-object p0, v0, LX/19m;->o:Ljava/lang/String;

    move-object v0, p0

    .line 633665
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 633666
    if-eqz p2, :cond_0

    instance-of v0, p2, Landroid/view/View;

    if-nez v0, :cond_1

    .line 633667
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v1, p2

    .line 633668
    check-cast v1, Landroid/view/View;

    .line 633669
    new-instance v0, LX/0hs;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v0, v2, v3}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 633670
    const/4 v2, -0x1

    .line 633671
    iput v2, v0, LX/0hs;->t:I

    .line 633672
    iget-object v2, p0, LX/3lF;->a:LX/19m;

    .line 633673
    iget-object v3, v2, LX/19m;->o:Ljava/lang/String;

    move-object v2, v3

    .line 633674
    invoke-virtual {v0, v2}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 633675
    iget-object v2, p0, LX/3lF;->a:LX/19m;

    .line 633676
    iget-object v3, v2, LX/19m;->p:Ljava/lang/String;

    move-object v2, v3

    .line 633677
    invoke-virtual {v0, v2}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 633678
    sget-object v2, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v0, v2}, LX/0ht;->a(LX/3AV;)V

    .line 633679
    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0650

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v5

    invoke-virtual/range {v0 .. v5}, LX/0ht;->a(Landroid/view/View;IIII)V

    .line 633680
    invoke-virtual {v0}, LX/0ht;->d()V

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 633681
    const-string v0, "4320"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 633682
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->HEADING_INDICATOR_SHOWN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
