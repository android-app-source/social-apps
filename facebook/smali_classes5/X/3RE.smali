.class public LX/3RE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/13O;

.field private final b:LX/0SG;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/13O;LX/0SG;LX/0Or;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/chatheads/ipc/annotations/IsChatHeadsMutePopupEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/13O;",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 579233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 579234
    iput-object p1, p0, LX/3RE;->a:LX/13O;

    .line 579235
    iput-object p2, p0, LX/3RE;->b:LX/0SG;

    .line 579236
    iput-object p3, p0, LX/3RE;->c:LX/0Or;

    .line 579237
    return-void
.end method

.method public static a(LX/0QB;)LX/3RE;
    .locals 1

    .prologue
    .line 579238
    invoke-static {p0}, LX/3RE;->b(LX/0QB;)LX/3RE;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3RE;
    .locals 4

    .prologue
    .line 579239
    new-instance v2, LX/3RE;

    invoke-static {p0}, LX/13O;->a(LX/0QB;)LX/13O;

    move-result-object v0

    check-cast v0, LX/13O;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    const/16 v3, 0x150c

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-direct {v2, v0, v1, v3}, LX/3RE;-><init>(LX/13O;LX/0SG;LX/0Or;)V

    .line 579240
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    .locals 8
    .param p1    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 579241
    iget-object v0, p0, LX/3RE;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 579242
    :cond_0
    :goto_0
    return v2

    .line 579243
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v3

    .line 579244
    iget-object v0, p0, LX/3RE;->a:LX/13O;

    const-string v4, "chat_head_mute_state"

    invoke-virtual {v0, v4, v3}, LX/13O;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 579245
    const/4 v4, 0x2

    if-lt v0, v4, :cond_2

    move v0, v1

    .line 579246
    :goto_1
    iget-object v4, p0, LX/3RE;->b:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    .line 579247
    iget-object v6, p0, LX/3RE;->a:LX/13O;

    const-string v7, "chat_head_mute_state"

    invoke-virtual {v6, v7, v3}, LX/13O;->b(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v6

    .line 579248
    sub-long/2addr v4, v6

    const-wide/32 v6, 0x5265c00

    cmp-long v3, v4, v6

    if-gez v3, :cond_3

    move v3, v1

    .line 579249
    :goto_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 579250
    goto :goto_1

    :cond_3
    move v3, v2

    .line 579251
    goto :goto_2
.end method
