.class public LX/3qf;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/3qb;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 642342
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 642343
    const/16 v1, 0xf

    if-lt v0, v1, :cond_0

    .line 642344
    new-instance v0, LX/3qe;

    invoke-direct {v0}, LX/3qe;-><init>()V

    sput-object v0, LX/3qf;->a:LX/3qb;

    .line 642345
    :goto_0
    return-void

    .line 642346
    :cond_0
    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 642347
    new-instance v0, LX/3qd;

    invoke-direct {v0}, LX/3qd;-><init>()V

    sput-object v0, LX/3qf;->a:LX/3qb;

    goto :goto_0

    .line 642348
    :cond_1
    new-instance v0, LX/3qc;

    invoke-direct {v0}, LX/3qc;-><init>()V

    sput-object v0, LX/3qf;->a:LX/3qb;

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 642349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 642350
    return-void
.end method

.method public static a(Landroid/content/ComponentName;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 642351
    sget-object v0, LX/3qf;->a:LX/3qb;

    invoke-interface {v0, p0}, LX/3qb;->a(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
