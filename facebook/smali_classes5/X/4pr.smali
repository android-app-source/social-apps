.class public LX/4pr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Closeable;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# static fields
.field public static final a:LX/4pr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4pr",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0lJ;

.field public final c:LX/0n3;

.field public final d:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<TT;>;"
        }
    .end annotation
.end field

.field public e:LX/15w;

.field public final f:Z

.field public g:Z

.field public final h:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 812010
    new-instance v0, LX/4pr;

    const/4 v5, 0x0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v6, v1

    invoke-direct/range {v0 .. v6}, LX/4pr;-><init>(LX/0lJ;LX/15w;LX/0n3;Lcom/fasterxml/jackson/databind/JsonDeserializer;ZLjava/lang/Object;)V

    sput-object v0, LX/4pr;->a:LX/4pr;

    return-void
.end method

.method public constructor <init>(LX/0lJ;LX/15w;LX/0n3;Lcom/fasterxml/jackson/databind/JsonDeserializer;ZLjava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "LX/15w;",
            "LX/0n3;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;Z",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 812011
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 812012
    iput-object p1, p0, LX/4pr;->b:LX/0lJ;

    .line 812013
    iput-object p2, p0, LX/4pr;->e:LX/15w;

    .line 812014
    iput-object p3, p0, LX/4pr;->c:LX/0n3;

    .line 812015
    iput-object p4, p0, LX/4pr;->d:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 812016
    iput-boolean p5, p0, LX/4pr;->f:Z

    .line 812017
    if-nez p6, :cond_1

    .line 812018
    const/4 v0, 0x0

    iput-object v0, p0, LX/4pr;->h:Ljava/lang/Object;

    .line 812019
    :goto_0
    if-eqz p5, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->START_ARRAY:LX/15z;

    if-ne v0, v1, :cond_0

    .line 812020
    invoke-virtual {p2}, LX/15w;->n()V

    .line 812021
    :cond_0
    return-void

    .line 812022
    :cond_1
    iput-object p6, p0, LX/4pr;->h:Ljava/lang/Object;

    goto :goto_0
.end method

.method private a()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 811974
    iget-object v2, p0, LX/4pr;->e:LX/15w;

    if-nez v2, :cond_1

    .line 811975
    :cond_0
    :goto_0
    return v0

    .line 811976
    :cond_1
    iget-boolean v2, p0, LX/4pr;->g:Z

    if-nez v2, :cond_3

    .line 811977
    iget-object v2, p0, LX/4pr;->e:LX/15w;

    invoke-virtual {v2}, LX/15w;->g()LX/15z;

    move-result-object v2

    .line 811978
    iput-boolean v1, p0, LX/4pr;->g:Z

    .line 811979
    if-nez v2, :cond_3

    .line 811980
    iget-object v2, p0, LX/4pr;->e:LX/15w;

    invoke-virtual {v2}, LX/15w;->c()LX/15z;

    move-result-object v2

    .line 811981
    if-eqz v2, :cond_2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-ne v2, v3, :cond_3

    .line 811982
    :cond_2
    iget-object v1, p0, LX/4pr;->e:LX/15w;

    .line 811983
    const/4 v2, 0x0

    iput-object v2, p0, LX/4pr;->e:LX/15w;

    .line 811984
    iget-boolean v2, p0, LX/4pr;->f:Z

    if-eqz v2, :cond_0

    .line 811985
    invoke-virtual {v1}, LX/15w;->close()V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 811986
    goto :goto_0
.end method

.method private b()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 811998
    iget-boolean v0, p0, LX/4pr;->g:Z

    if-nez v0, :cond_0

    .line 811999
    invoke-direct {p0}, LX/4pr;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 812000
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 812001
    :cond_0
    iget-object v0, p0, LX/4pr;->e:LX/15w;

    if-nez v0, :cond_1

    .line 812002
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 812003
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4pr;->g:Z

    .line 812004
    iget-object v0, p0, LX/4pr;->h:Ljava/lang/Object;

    if-nez v0, :cond_2

    .line 812005
    iget-object v0, p0, LX/4pr;->d:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    iget-object v1, p0, LX/4pr;->e:LX/15w;

    iget-object v2, p0, LX/4pr;->c:LX/0n3;

    invoke-virtual {v0, v1, v2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    .line 812006
    :goto_0
    iget-object v1, p0, LX/4pr;->e:LX/15w;

    invoke-virtual {v1}, LX/15w;->n()V

    .line 812007
    return-object v0

    .line 812008
    :cond_2
    iget-object v0, p0, LX/4pr;->d:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    iget-object v1, p0, LX/4pr;->e:LX/15w;

    iget-object v2, p0, LX/4pr;->c:LX/0n3;

    iget-object v3, p0, LX/4pr;->h:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    .line 812009
    iget-object v0, p0, LX/4pr;->h:Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 812023
    iget-object v0, p0, LX/4pr;->e:LX/15w;

    if-eqz v0, :cond_0

    .line 812024
    iget-object v0, p0, LX/4pr;->e:LX/15w;

    invoke-virtual {v0}, LX/15w;->close()V

    .line 812025
    :cond_0
    return-void
.end method

.method public final hasNext()Z
    .locals 3

    .prologue
    .line 811993
    :try_start_0
    invoke-direct {p0}, LX/4pr;->a()Z
    :try_end_0
    .catch LX/28E; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    return v0

    .line 811994
    :catch_0
    move-exception v0

    .line 811995
    new-instance v1, LX/4px;

    invoke-virtual {v0}, LX/28E;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/4px;-><init>(Ljava/lang/String;LX/28E;)V

    throw v1

    .line 811996
    :catch_1
    move-exception v0

    .line 811997
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final next()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 811988
    :try_start_0
    invoke-direct {p0}, LX/4pr;->b()Ljava/lang/Object;
    :try_end_0
    .catch LX/28E; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 811989
    :catch_0
    move-exception v0

    .line 811990
    new-instance v1, LX/4px;

    invoke-virtual {v0}, LX/28E;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/4px;-><init>(Ljava/lang/String;LX/28E;)V

    throw v1

    .line 811991
    :catch_1
    move-exception v0

    .line 811992
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 811987
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
