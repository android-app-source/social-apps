.class public final LX/47Q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/47M;


# instance fields
.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 672274
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/47Q;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 672275
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;Landroid/os/Bundle;)V
    .locals 0
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 672276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 672277
    iput-object p1, p0, LX/47Q;->a:Ljava/lang/Class;

    .line 672278
    iput-object p2, p0, LX/47Q;->b:Landroid/os/Bundle;

    .line 672279
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 672280
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/47Q;->a:Ljava/lang/Class;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 672281
    iget-object v1, p0, LX/47Q;->b:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    .line 672282
    iget-object v1, p0, LX/47Q;->b:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 672283
    :cond_0
    if-eqz p2, :cond_1

    .line 672284
    invoke-virtual {v0, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 672285
    :cond_1
    return-object v0
.end method
