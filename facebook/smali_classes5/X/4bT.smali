.class public final LX/4bT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4bR;


# instance fields
.field public final synthetic a:LX/4bU;

.field private b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/4bU;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 793750
    iput-object p1, p0, LX/4bT;->a:LX/4bU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 793751
    iput-object p2, p0, LX/4bT;->b:Ljava/lang/String;

    .line 793752
    iput-object p3, p0, LX/4bT;->c:Ljava/lang/String;

    .line 793753
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/4bV;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 793754
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 793755
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4bV;

    .line 793756
    iget-object v2, p0, LX/4bT;->b:Ljava/lang/String;

    iget-object v3, v0, LX/4bV;->c:LX/15D;

    invoke-static {v3}, Lcom/facebook/http/common/FbHttpUtils;->a(LX/15D;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 793757
    iget-object v2, p0, LX/4bT;->a:LX/4bU;

    iget-object v2, v2, LX/4bU;->c:LX/4bW;

    iget-object v3, v0, LX/4bV;->c:LX/15D;

    invoke-virtual {v2, v3}, LX/4bW;->a(LX/15D;)LX/4bV;

    .line 793758
    iget-object v2, p0, LX/4bT;->a:LX/4bU;

    iget-object v2, v2, LX/4bU;->c:LX/4bW;

    iget-object v3, p0, LX/4bT;->a:LX/4bU;

    .line 793759
    iget-object v4, v3, LX/4bU;->j:LX/4b8;

    iget-object p1, v0, LX/4bV;->c:LX/15D;

    sget-object p2, LX/4bU;->b:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v4, p1, p2}, LX/4b8;->a(LX/15D;Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 793760
    iget-object v4, v0, LX/4bV;->c:LX/15D;

    .line 793761
    iget-object p1, v4, LX/15D;->i:LX/15F;

    move-object v4, p1

    .line 793762
    sget-object p1, LX/4bU;->b:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v4, p1}, LX/15F;->b(Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 793763
    sget-object v4, LX/4bU;->b:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v4}, LX/4bV;->b(Lcom/facebook/http/interfaces/RequestPriority;)LX/4bV;

    move-result-object v4

    move-object v0, v4

    .line 793764
    invoke-virtual {v2, v0}, LX/4bW;->a(LX/4bV;)V

    goto :goto_0

    .line 793765
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/4bV;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/15D",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 793766
    iget-object v0, p0, LX/4bT;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 793767
    iget-object v0, p0, LX/4bT;->b:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, LX/4bT;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 793768
    :cond_0
    iget-object v0, p0, LX/4bT;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 793769
    iget-object v0, p0, LX/4bT;->c:Ljava/lang/String;

    .line 793770
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 793771
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4bV;

    .line 793772
    iget-object v3, p0, LX/4bT;->c:Ljava/lang/String;

    iget-object v4, v1, LX/4bV;->c:LX/15D;

    invoke-static {v4}, Lcom/facebook/http/common/FbHttpUtils;->a(LX/15D;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 793773
    iget-object v3, p0, LX/4bT;->a:LX/4bU;

    iget-object v3, v3, LX/4bU;->c:LX/4bW;

    iget-object v4, v1, LX/4bV;->c:LX/15D;

    invoke-virtual {v3, v4}, LX/4bW;->a(LX/15D;)LX/4bV;

    .line 793774
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, LX/4bV;->b(Lcom/facebook/http/interfaces/RequestPriority;)LX/4bV;

    move-result-object v3

    .line 793775
    iget-object v4, p0, LX/4bT;->a:LX/4bU;

    iget-object v4, v4, LX/4bU;->j:LX/4b8;

    iget-object v5, v1, LX/4bV;->c:LX/15D;

    invoke-virtual {v3}, LX/4bV;->a()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object p2

    invoke-virtual {v4, v5, p2}, LX/4b8;->a(LX/15D;Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 793776
    iget-object v1, v1, LX/4bV;->c:LX/15D;

    .line 793777
    iget-object v4, v1, LX/15D;->i:LX/15F;

    move-object v1, v4

    .line 793778
    invoke-virtual {v3}, LX/4bV;->a()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/15F;->b(Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 793779
    iget-object v1, p0, LX/4bT;->a:LX/4bU;

    iget-object v1, v1, LX/4bU;->c:LX/4bW;

    invoke-virtual {v1, v3}, LX/4bW;->a(LX/4bV;)V

    goto :goto_0

    .line 793780
    :cond_2
    return-void
.end method
