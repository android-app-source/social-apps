.class public final LX/4qD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final deserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final generator:LX/4pS;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4pS",
            "<*>;"
        }
    .end annotation
.end field

.field public final idProperty:LX/32s;

.field public final idType:LX/0lJ;

.field public final propertyName:Ljava/lang/String;


# direct methods
.method private constructor <init>(LX/0lJ;Ljava/lang/String;LX/4pS;Lcom/fasterxml/jackson/databind/JsonDeserializer;LX/32s;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "Ljava/lang/String;",
            "LX/4pS",
            "<*>;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;",
            "LX/32s;",
            ")V"
        }
    .end annotation

    .prologue
    .line 813184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 813185
    iput-object p1, p0, LX/4qD;->idType:LX/0lJ;

    .line 813186
    iput-object p2, p0, LX/4qD;->propertyName:Ljava/lang/String;

    .line 813187
    iput-object p3, p0, LX/4qD;->generator:LX/4pS;

    .line 813188
    iput-object p4, p0, LX/4qD;->deserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 813189
    iput-object p5, p0, LX/4qD;->idProperty:LX/32s;

    .line 813190
    return-void
.end method

.method public static a(LX/0lJ;Ljava/lang/String;LX/4pS;Lcom/fasterxml/jackson/databind/JsonDeserializer;LX/32s;)LX/4qD;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "Ljava/lang/String;",
            "LX/4pS",
            "<*>;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;",
            "LX/32s;",
            ")",
            "LX/4qD;"
        }
    .end annotation

    .prologue
    .line 813191
    new-instance v0, LX/4qD;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/4qD;-><init>(LX/0lJ;Ljava/lang/String;LX/4pS;Lcom/fasterxml/jackson/databind/JsonDeserializer;LX/32s;)V

    return-object v0
.end method
