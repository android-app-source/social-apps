.class public final LX/51Z;
.super LX/0Rr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Rr",
        "<",
        "Ljava/util/Map$Entry",
        "<",
        "LX/4xM",
        "<TC;>;",
        "LX/50M",
        "<TC;>;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/Iterator;

.field public final synthetic b:LX/51b;


# direct methods
.method public constructor <init>(LX/51b;Ljava/util/Iterator;)V
    .locals 0

    .prologue
    .line 825262
    iput-object p1, p0, LX/51Z;->b:LX/51b;

    iput-object p2, p0, LX/51Z;->a:Ljava/util/Iterator;

    invoke-direct {p0}, LX/0Rr;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 825263
    iget-object v0, p0, LX/51Z;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 825264
    invoke-virtual {p0}, LX/0Rr;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 825265
    :goto_0
    return-object v0

    .line 825266
    :cond_0
    iget-object v0, p0, LX/51Z;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/50M;

    .line 825267
    iget-object v1, p0, LX/51Z;->b:LX/51b;

    iget-object v1, v1, LX/51b;->b:LX/50M;

    iget-object v1, v1, LX/50M;->upperBound:LX/4xM;

    iget-object v2, v0, LX/50M;->upperBound:LX/4xM;

    invoke-virtual {v1, v2}, LX/4xM;->a(Ljava/lang/Comparable;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 825268
    invoke-virtual {p0}, LX/0Rr;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    goto :goto_0

    .line 825269
    :cond_1
    iget-object v1, v0, LX/50M;->upperBound:LX/4xM;

    invoke-static {v1, v0}, LX/0PM;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    goto :goto_0
.end method
