.class public LX/4oJ;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Landroid/graphics/Paint;

.field private b:Landroid/graphics/Paint;

.field private final c:[F

.field private final d:Landroid/graphics/Path;

.field private final e:Landroid/graphics/RectF;

.field private f:Z

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 809408
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 809409
    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, LX/4oJ;->c:[F

    .line 809410
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/4oJ;->d:Landroid/graphics/Path;

    .line 809411
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/4oJ;->e:Landroid/graphics/RectF;

    .line 809412
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/4oJ;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 809413
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 809402
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 809403
    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, LX/4oJ;->c:[F

    .line 809404
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/4oJ;->d:Landroid/graphics/Path;

    .line 809405
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/4oJ;->e:Landroid/graphics/RectF;

    .line 809406
    invoke-direct {p0, p1, p2}, LX/4oJ;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 809407
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 809396
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 809397
    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, LX/4oJ;->c:[F

    .line 809398
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/4oJ;->d:Landroid/graphics/Path;

    .line 809399
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/4oJ;->e:Landroid/graphics/RectF;

    .line 809400
    invoke-direct {p0, p1, p2}, LX/4oJ;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 809401
    return-void
.end method

.method private a(II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 809390
    iget-object v0, p0, LX/4oJ;->d:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 809391
    iget-object v1, p0, LX/4oJ;->d:Landroid/graphics/Path;

    iget-boolean v0, p0, LX/4oJ;->g:Z

    if-eqz v0, :cond_0

    sget-object v0, Landroid/graphics/Path$FillType;->WINDING:Landroid/graphics/Path$FillType;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 809392
    iget-object v0, p0, LX/4oJ;->e:Landroid/graphics/RectF;

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 809393
    iget-object v0, p0, LX/4oJ;->d:Landroid/graphics/Path;

    iget-object v1, p0, LX/4oJ;->e:Landroid/graphics/RectF;

    iget-object v2, p0, LX/4oJ;->c:[F

    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    .line 809394
    return-void

    .line 809395
    :cond_0
    sget-object v0, Landroid/graphics/Path$FillType;->INVERSE_WINDING:Landroid/graphics/Path$FillType;

    goto :goto_0
.end method

.method public static a(LX/4oJ;FFFF)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 809384
    iget-object v2, p0, LX/4oJ;->c:[F

    iget-object v3, p0, LX/4oJ;->c:[F

    aput p1, v3, v1

    aput p1, v2, v0

    .line 809385
    iget-object v2, p0, LX/4oJ;->c:[F

    const/4 v3, 0x2

    iget-object v4, p0, LX/4oJ;->c:[F

    const/4 v5, 0x3

    aput p2, v4, v5

    aput p2, v2, v3

    .line 809386
    iget-object v2, p0, LX/4oJ;->c:[F

    const/4 v3, 0x4

    iget-object v4, p0, LX/4oJ;->c:[F

    const/4 v5, 0x5

    aput p3, v4, v5

    aput p3, v2, v3

    .line 809387
    iget-object v2, p0, LX/4oJ;->c:[F

    const/4 v3, 0x6

    iget-object v4, p0, LX/4oJ;->c:[F

    const/4 v5, 0x7

    aput p4, v4, v5

    aput p4, v2, v3

    .line 809388
    cmpl-float v2, p1, v6

    if-nez v2, :cond_0

    cmpl-float v2, p2, v6

    if-nez v2, :cond_0

    cmpl-float v2, p3, v6

    if-nez v2, :cond_0

    cmpl-float v2, p4, v6

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    iput-boolean v0, p0, LX/4oJ;->f:Z

    .line 809389
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 809366
    invoke-virtual {p0, v1}, LX/4oJ;->setWillNotDraw(Z)V

    .line 809367
    if-eqz p2, :cond_0

    .line 809368
    sget-object v0, LX/03r;->RoundedView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 809369
    const/16 v0, 0xb

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 809370
    const/16 v3, 0x5

    invoke-virtual {v2, v3, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    invoke-virtual {p0, v3}, LX/4oJ;->setChildClippingEnabled(Z)V

    .line 809371
    const/16 v3, 0x6

    invoke-virtual {v2, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 809372
    const/16 v3, 0x7

    invoke-virtual {v2, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    .line 809373
    const/16 v4, 0x8

    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    .line 809374
    const/16 v5, 0x9

    invoke-virtual {v2, v5, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    .line 809375
    const/16 v6, 0xa

    invoke-virtual {v2, v6, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 809376
    int-to-float v3, v3

    int-to-float v4, v4

    int-to-float v5, v5

    int-to-float v1, v1

    invoke-static {p0, v3, v4, v5, v1}, LX/4oJ;->a(LX/4oJ;FFFF)V

    .line 809377
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 809378
    :goto_0
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, LX/4oJ;->a:Landroid/graphics/Paint;

    .line 809379
    iget-object v1, p0, LX/4oJ;->a:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/PorterDuffXfermode;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v3}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 809380
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, LX/4oJ;->b:Landroid/graphics/Paint;

    .line 809381
    iget-object v1, p0, LX/4oJ;->b:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 809382
    iget-object v1, p0, LX/4oJ;->b:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 809383
    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 809414
    const-string v0, "RoundedCornersFrameLayout applying mask"

    const v1, -0x30ee279a

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 809415
    :try_start_0
    iget-object v0, p0, LX/4oJ;->d:Landroid/graphics/Path;

    iget-object v1, p0, LX/4oJ;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 809416
    iget-object v0, p0, LX/4oJ;->b:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 809417
    iget-object v0, p0, LX/4oJ;->d:Landroid/graphics/Path;

    iget-object v1, p0, LX/4oJ;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 809418
    :cond_0
    const v0, 0xfdebdca

    invoke-static {v0}, LX/02m;->a(I)V

    .line 809419
    return-void

    .line 809420
    :catchall_0
    move-exception v0

    const v1, -0x1d36977f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 809359
    iget-boolean v0, p0, LX/4oJ;->g:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/4oJ;->f:Z

    if-eqz v0, :cond_0

    .line 809360
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 809361
    iget-object v0, p0, LX/4oJ;->d:Landroid/graphics/Path;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 809362
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 809363
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 809364
    :goto_0
    return-void

    .line 809365
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 809355
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 809356
    iget-boolean v0, p0, LX/4oJ;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/4oJ;->g:Z

    if-nez v0, :cond_0

    .line 809357
    invoke-direct {p0, p1}, LX/4oJ;->a(Landroid/graphics/Canvas;)V

    .line 809358
    :cond_0
    return-void
.end method

.method public onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x68792da7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 809352
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/widget/CustomFrameLayout;->onSizeChanged(IIII)V

    .line 809353
    invoke-direct {p0, p1, p2}, LX/4oJ;->a(II)V

    .line 809354
    const/16 v1, 0x2d

    const v2, 0x4a191490    # 2508068.0f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setChildClippingEnabled(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 809346
    iget-boolean v0, p0, LX/4oJ;->g:Z

    if-ne v0, p1, :cond_0

    .line 809347
    :goto_0
    return-void

    .line 809348
    :cond_0
    iput-boolean p1, p0, LX/4oJ;->g:Z

    .line 809349
    iget-boolean v0, p0, LX/4oJ;->g:Z

    if-eqz v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_1

    .line 809350
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/4oJ;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_0

    .line 809351
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v2}, LX/4oJ;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public setCornerRadius(F)V
    .locals 0

    .prologue
    .line 809344
    invoke-static {p0, p1, p1, p1, p1}, LX/4oJ;->a(LX/4oJ;FFFF)V

    .line 809345
    return-void
.end method

.method public setRoundedCornerBackgroundColor(I)V
    .locals 1

    .prologue
    .line 809341
    iget-object v0, p0, LX/4oJ;->b:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    .line 809342
    iget-object v0, p0, LX/4oJ;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 809343
    :cond_0
    return-void
.end method
