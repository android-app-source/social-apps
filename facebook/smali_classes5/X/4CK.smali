.class public LX/4CK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1c8;


# instance fields
.field public final a:LX/1G9;

.field public final b:LX/1c2;

.field public final c:Ljava/util/concurrent/ScheduledExecutorService;

.field public final d:LX/0So;


# direct methods
.method public constructor <init>(LX/1G9;LX/1c2;Ljava/util/concurrent/ScheduledExecutorService;LX/0So;)V
    .locals 0

    .prologue
    .line 678695
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 678696
    iput-object p1, p0, LX/4CK;->a:LX/1G9;

    .line 678697
    iput-object p2, p0, LX/4CK;->b:LX/1c2;

    .line 678698
    iput-object p3, p0, LX/4CK;->c:Ljava/util/concurrent/ScheduledExecutorService;

    .line 678699
    iput-object p4, p0, LX/4CK;->d:LX/0So;

    .line 678700
    return-void
.end method


# virtual methods
.method public final a(LX/1ln;)Z
    .locals 1

    .prologue
    .line 678701
    instance-of v0, p1, LX/4e7;

    return v0
.end method

.method public final b(LX/1ln;)Landroid/graphics/drawable/Drawable;
    .locals 5

    .prologue
    .line 678702
    new-instance v0, LX/4CI;

    check-cast p1, LX/4e7;

    invoke-virtual {p1}, LX/4e7;->a()LX/4dO;

    move-result-object v1

    const/4 p1, 0x0

    .line 678703
    iget-object v2, v1, LX/4dO;->a:LX/1GB;

    move-object v2, v2

    .line 678704
    new-instance v3, Landroid/graphics/Rect;

    invoke-interface {v2}, LX/1GB;->a()I

    move-result v4

    invoke-interface {v2}, LX/1GB;->b()I

    move-result v2

    invoke-direct {v3, p1, p1, v4, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 678705
    iget-object v2, p0, LX/4CK;->a:LX/1G9;

    invoke-interface {v2, v1, v3}, LX/1G9;->a(LX/4dO;Landroid/graphics/Rect;)LX/4dG;

    move-result-object v2

    .line 678706
    iget-object v3, p0, LX/4CK;->b:LX/1c2;

    sget-object v4, LX/4dM;->a:LX/4dM;

    invoke-interface {v3, v2, v4}, LX/1c2;->a(LX/4dG;LX/4dM;)LX/4dZ;

    move-result-object v2

    .line 678707
    new-instance v3, LX/4CO;

    invoke-direct {v3, v2}, LX/4CO;-><init>(LX/4dH;)V

    .line 678708
    iget-object v2, p0, LX/4CK;->d:LX/0So;

    iget-object v4, p0, LX/4CK;->c:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v3, v2, v4}, LX/4C8;->a(LX/4C5;LX/0So;Ljava/util/concurrent/ScheduledExecutorService;)LX/4C6;

    move-result-object v2

    move-object v1, v2

    .line 678709
    invoke-direct {v0, v1}, LX/4CI;-><init>(LX/4C5;)V

    return-object v0
.end method
