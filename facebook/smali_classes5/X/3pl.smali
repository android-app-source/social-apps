.class public final LX/3pl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3pk;


# instance fields
.field private a:Landroid/graphics/Bitmap;

.field private b:LX/3pj;

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 641712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 641713
    const/4 v0, 0x0

    iput v0, p0, LX/3pl;->c:I

    .line 641714
    return-void
.end method


# virtual methods
.method public final a(LX/2HB;)LX/2HB;
    .locals 3

    .prologue
    .line 641715
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    .line 641716
    :goto_0
    return-object p1

    .line 641717
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 641718
    iget-object v1, p0, LX/3pl;->a:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 641719
    const-string v1, "large_icon"

    iget-object v2, p0, LX/3pl;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 641720
    :cond_1
    iget v1, p0, LX/3pl;->c:I

    if-eqz v1, :cond_2

    .line 641721
    const-string v1, "app_color"

    iget v2, p0, LX/3pl;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 641722
    :cond_2
    iget-object v1, p0, LX/3pl;->b:LX/3pj;

    if-eqz v1, :cond_3

    .line 641723
    sget-object v1, LX/3px;->a:LX/3pn;

    iget-object v2, p0, LX/3pl;->b:LX/3pj;

    invoke-interface {v1, v2}, LX/3pn;->a(LX/3pi;)Landroid/os/Bundle;

    move-result-object v1

    .line 641724
    const-string v2, "car_conversation"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 641725
    :cond_3
    invoke-virtual {p1}, LX/2HB;->a()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "android.car.EXTENSIONS"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(I)LX/3pl;
    .locals 0

    .prologue
    .line 641726
    iput p1, p0, LX/3pl;->c:I

    .line 641727
    return-object p0
.end method

.method public final a(LX/3pj;)LX/3pl;
    .locals 0

    .prologue
    .line 641728
    iput-object p1, p0, LX/3pl;->b:LX/3pj;

    .line 641729
    return-object p0
.end method

.method public final a(Landroid/graphics/Bitmap;)LX/3pl;
    .locals 0

    .prologue
    .line 641730
    iput-object p1, p0, LX/3pl;->a:Landroid/graphics/Bitmap;

    .line 641731
    return-object p0
.end method
