.class public final LX/3rH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3rF;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 642836
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 642837
    const/4 v1, 0x0

    .line 642838
    :try_start_0
    sget-object v0, LX/3rJ;->a:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    .line 642839
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v0, v2

    .line 642840
    sget-object v2, LX/3rJ;->a:Ljava/lang/reflect/Method;

    const/4 p0, 0x0

    invoke-virtual {v2, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    .line 642841
    :goto_0
    move-object v0, v0

    .line 642842
    return-object v0

    .line 642843
    :catch_0
    move-exception v0

    .line 642844
    const-string v2, "ICUCompatIcs"

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_1
    move-object v0, v1

    .line 642845
    goto :goto_0

    .line 642846
    :catch_1
    move-exception v0

    .line 642847
    const-string v2, "ICUCompatIcs"

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 642848
    :try_start_0
    sget-object v0, LX/3rJ;->b:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    .line 642849
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 642850
    sget-object v1, LX/3rJ;->b:Ljava/lang/reflect/Method;

    const/4 p0, 0x0

    invoke-virtual {v1, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    .line 642851
    :goto_0
    move-object v0, v0

    .line 642852
    return-object v0

    .line 642853
    :catch_0
    move-exception v0

    .line 642854
    const-string v1, "ICUCompatIcs"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_1
    move-object v0, p1

    .line 642855
    goto :goto_0

    .line 642856
    :catch_1
    move-exception v0

    .line 642857
    const-string v1, "ICUCompatIcs"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
