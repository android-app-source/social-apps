.class public LX/4NA;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 690974
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 76

    .prologue
    .line 690975
    const/16 v67, 0x0

    .line 690976
    const/16 v66, 0x0

    .line 690977
    const/16 v65, 0x0

    .line 690978
    const/16 v64, 0x0

    .line 690979
    const/16 v63, 0x0

    .line 690980
    const/16 v62, 0x0

    .line 690981
    const/16 v61, 0x0

    .line 690982
    const/16 v60, 0x0

    .line 690983
    const/16 v59, 0x0

    .line 690984
    const/16 v58, 0x0

    .line 690985
    const/16 v57, 0x0

    .line 690986
    const/16 v56, 0x0

    .line 690987
    const/16 v55, 0x0

    .line 690988
    const/16 v54, 0x0

    .line 690989
    const/16 v53, 0x0

    .line 690990
    const/16 v52, 0x0

    .line 690991
    const/16 v51, 0x0

    .line 690992
    const/16 v50, 0x0

    .line 690993
    const/16 v49, 0x0

    .line 690994
    const/16 v48, 0x0

    .line 690995
    const/16 v47, 0x0

    .line 690996
    const/16 v46, 0x0

    .line 690997
    const-wide/16 v44, 0x0

    .line 690998
    const/16 v43, 0x0

    .line 690999
    const/16 v42, 0x0

    .line 691000
    const/16 v41, 0x0

    .line 691001
    const/16 v40, 0x0

    .line 691002
    const/16 v39, 0x0

    .line 691003
    const/16 v38, 0x0

    .line 691004
    const/16 v37, 0x0

    .line 691005
    const/16 v36, 0x0

    .line 691006
    const/16 v35, 0x0

    .line 691007
    const/16 v34, 0x0

    .line 691008
    const/16 v33, 0x0

    .line 691009
    const/16 v32, 0x0

    .line 691010
    const/16 v31, 0x0

    .line 691011
    const/16 v30, 0x0

    .line 691012
    const/16 v29, 0x0

    .line 691013
    const/16 v28, 0x0

    .line 691014
    const/16 v27, 0x0

    .line 691015
    const/16 v26, 0x0

    .line 691016
    const/16 v25, 0x0

    .line 691017
    const/16 v24, 0x0

    .line 691018
    const/16 v23, 0x0

    .line 691019
    const/16 v22, 0x0

    .line 691020
    const-wide/16 v20, 0x0

    .line 691021
    const/16 v19, 0x0

    .line 691022
    const/16 v18, 0x0

    .line 691023
    const/16 v17, 0x0

    .line 691024
    const/16 v16, 0x0

    .line 691025
    const/4 v15, 0x0

    .line 691026
    const/4 v14, 0x0

    .line 691027
    const/4 v13, 0x0

    .line 691028
    const/4 v12, 0x0

    .line 691029
    const/4 v11, 0x0

    .line 691030
    const/4 v10, 0x0

    .line 691031
    const/4 v9, 0x0

    .line 691032
    const/4 v8, 0x0

    .line 691033
    const/4 v7, 0x0

    .line 691034
    const/4 v6, 0x0

    .line 691035
    const/4 v5, 0x0

    .line 691036
    const/4 v4, 0x0

    .line 691037
    const/4 v3, 0x0

    .line 691038
    const/4 v2, 0x0

    .line 691039
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v68

    sget-object v69, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v68

    move-object/from16 v1, v69

    if-eq v0, v1, :cond_42

    .line 691040
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 691041
    const/4 v2, 0x0

    .line 691042
    :goto_0
    return v2

    .line 691043
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v69, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v69

    if-eq v2, v0, :cond_35

    .line 691044
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 691045
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 691046
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v69

    sget-object v70, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v69

    move-object/from16 v1, v70

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 691047
    const-string v69, "campaign_title"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_1

    .line 691048
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v68, v2

    goto :goto_1

    .line 691049
    :cond_1
    const-string v69, "can_donate"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_2

    .line 691050
    const/4 v2, 0x1

    .line 691051
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v16

    move/from16 v67, v16

    move/from16 v16, v2

    goto :goto_1

    .line 691052
    :cond_2
    const-string v69, "can_invite_to_campaign"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_3

    .line 691053
    const/4 v2, 0x1

    .line 691054
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v15

    move/from16 v66, v15

    move v15, v2

    goto :goto_1

    .line 691055
    :cond_3
    const-string v69, "can_viewer_post"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_4

    .line 691056
    const/4 v2, 0x1

    .line 691057
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    move/from16 v65, v14

    move v14, v2

    goto :goto_1

    .line 691058
    :cond_4
    const-string v69, "can_viewer_report"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_5

    .line 691059
    const/4 v2, 0x1

    .line 691060
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    move/from16 v64, v13

    move v13, v2

    goto/16 :goto_1

    .line 691061
    :cond_5
    const-string v69, "donors"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_6

    .line 691062
    invoke-static/range {p0 .. p1}, LX/4NB;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v63, v2

    goto/16 :goto_1

    .line 691063
    :cond_6
    const-string v69, "feedAwesomizerProfilePicture"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_7

    .line 691064
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v62, v2

    goto/16 :goto_1

    .line 691065
    :cond_7
    const-string v69, "full_width_post_donation_image"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_8

    .line 691066
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v61, v2

    goto/16 :goto_1

    .line 691067
    :cond_8
    const-string v69, "fundraiser_detailed_progress_text"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_9

    .line 691068
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v60, v2

    goto/16 :goto_1

    .line 691069
    :cond_9
    const-string v69, "fundraiser_for_charity_text"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_a

    .line 691070
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v59, v2

    goto/16 :goto_1

    .line 691071
    :cond_a
    const-string v69, "fundraiser_page_subtitle"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_b

    .line 691072
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v58, v2

    goto/16 :goto_1

    .line 691073
    :cond_b
    const-string v69, "fundraiser_progress_text"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_c

    .line 691074
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v57, v2

    goto/16 :goto_1

    .line 691075
    :cond_c
    const-string v69, "has_goal_amount"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_d

    .line 691076
    const/4 v2, 0x1

    .line 691077
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v56, v7

    move v7, v2

    goto/16 :goto_1

    .line 691078
    :cond_d
    const-string v69, "header_photo"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_e

    .line 691079
    invoke-static/range {p0 .. p1}, LX/2sY;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v55, v2

    goto/16 :goto_1

    .line 691080
    :cond_e
    const-string v69, "id"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_f

    .line 691081
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v54, v2

    goto/16 :goto_1

    .line 691082
    :cond_f
    const-string v69, "imageHighOrig"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_10

    .line 691083
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v53, v2

    goto/16 :goto_1

    .line 691084
    :cond_10
    const-string v69, "invited_you_to_donate_text"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_11

    .line 691085
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v52, v2

    goto/16 :goto_1

    .line 691086
    :cond_11
    const-string v69, "is_viewer_following"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_12

    .line 691087
    const/4 v2, 0x1

    .line 691088
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v51, v6

    move v6, v2

    goto/16 :goto_1

    .line 691089
    :cond_12
    const-string v69, "logo_image"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_13

    .line 691090
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v50, v2

    goto/16 :goto_1

    .line 691091
    :cond_13
    const-string v69, "mobile_donate_url"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_14

    .line 691092
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v49, v2

    goto/16 :goto_1

    .line 691093
    :cond_14
    const-string v69, "name"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_15

    .line 691094
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v48, v2

    goto/16 :goto_1

    .line 691095
    :cond_15
    const-string v69, "owner"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_16

    .line 691096
    invoke-static/range {p0 .. p1}, LX/2bM;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v47, v2

    goto/16 :goto_1

    .line 691097
    :cond_16
    const-string v69, "percent_of_goal_reached"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_17

    .line 691098
    const/4 v2, 0x1

    .line 691099
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 691100
    :cond_17
    const-string v69, "posted_item_privacy_scope"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_18

    .line 691101
    invoke-static/range {p0 .. p1}, LX/2bo;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v46, v2

    goto/16 :goto_1

    .line 691102
    :cond_18
    const-string v69, "privacy_scope"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_19

    .line 691103
    invoke-static/range {p0 .. p1}, LX/2bo;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v45, v2

    goto/16 :goto_1

    .line 691104
    :cond_19
    const-string v69, "profileImageLarge"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_1a

    .line 691105
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v44, v2

    goto/16 :goto_1

    .line 691106
    :cond_1a
    const-string v69, "profileImageSmall"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_1b

    .line 691107
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v43, v2

    goto/16 :goto_1

    .line 691108
    :cond_1b
    const-string v69, "profilePicture50"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_1c

    .line 691109
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v42, v2

    goto/16 :goto_1

    .line 691110
    :cond_1c
    const-string v69, "profilePictureHighRes"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_1d

    .line 691111
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v41, v2

    goto/16 :goto_1

    .line 691112
    :cond_1d
    const-string v69, "profilePictureLarge"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_1e

    .line 691113
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v40, v2

    goto/16 :goto_1

    .line 691114
    :cond_1e
    const-string v69, "profile_photo"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_1f

    .line 691115
    invoke-static/range {p0 .. p1}, LX/2sY;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v39, v2

    goto/16 :goto_1

    .line 691116
    :cond_1f
    const-string v69, "profile_picture"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_20

    .line 691117
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v38, v2

    goto/16 :goto_1

    .line 691118
    :cond_20
    const-string v69, "profile_picture_is_silhouette"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_21

    .line 691119
    const/4 v2, 0x1

    .line 691120
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    move/from16 v37, v12

    move v12, v2

    goto/16 :goto_1

    .line 691121
    :cond_21
    const-string v69, "streaming_profile_picture"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_22

    .line 691122
    invoke-static/range {p0 .. p1}, LX/4TN;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v36, v2

    goto/16 :goto_1

    .line 691123
    :cond_22
    const-string v69, "taggable_object_profile_picture"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_23

    .line 691124
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v35, v2

    goto/16 :goto_1

    .line 691125
    :cond_23
    const-string v69, "url"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_24

    .line 691126
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v34, v2

    goto/16 :goto_1

    .line 691127
    :cond_24
    const-string v69, "charity_interface"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_25

    .line 691128
    invoke-static/range {p0 .. p1}, LX/4LB;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v33, v2

    goto/16 :goto_1

    .line 691129
    :cond_25
    const-string v69, "amount_raised_text"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_26

    .line 691130
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v32, v2

    goto/16 :goto_1

    .line 691131
    :cond_26
    const-string v69, "campaign_goal_text"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_27

    .line 691132
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v31, v2

    goto/16 :goto_1

    .line 691133
    :cond_27
    const-string v69, "days_left_text"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_28

    .line 691134
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v30, v2

    goto/16 :goto_1

    .line 691135
    :cond_28
    const-string v69, "description"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_29

    .line 691136
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v29, v2

    goto/16 :goto_1

    .line 691137
    :cond_29
    const-string v69, "fundraiser_by_owner_text"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_2a

    .line 691138
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v28, v2

    goto/16 :goto_1

    .line 691139
    :cond_2a
    const-string v69, "has_viewer_donated"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_2b

    .line 691140
    const/4 v2, 0x1

    .line 691141
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    move/from16 v27, v11

    move v11, v2

    goto/16 :goto_1

    .line 691142
    :cond_2b
    const-string v69, "total_donated_amount_by_viewer"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_2c

    .line 691143
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v26, v2

    goto/16 :goto_1

    .line 691144
    :cond_2c
    const-string v69, "can_viewer_edit"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_2d

    .line 691145
    const/4 v2, 0x1

    .line 691146
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    move/from16 v23, v10

    move v10, v2

    goto/16 :goto_1

    .line 691147
    :cond_2d
    const-string v69, "end_time"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_2e

    .line 691148
    const/4 v2, 0x1

    .line 691149
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v24

    move v9, v2

    goto/16 :goto_1

    .line 691150
    :cond_2e
    const-string v69, "can_viewer_delete"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_2f

    .line 691151
    const/4 v2, 0x1

    .line 691152
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    move/from16 v22, v8

    move v8, v2

    goto/16 :goto_1

    .line 691153
    :cond_2f
    const-string v69, "goal_amount"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_30

    .line 691154
    invoke-static/range {p0 .. p1}, LX/4Le;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 691155
    :cond_30
    const-string v69, "donors_social_context_text"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_31

    .line 691156
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 691157
    :cond_31
    const-string v69, "user_donors"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_32

    .line 691158
    invoke-static/range {p0 .. p1}, LX/4N6;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 691159
    :cond_32
    const-string v69, "profilePicture180"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    if-eqz v69, :cond_33

    .line 691160
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 691161
    :cond_33
    const-string v69, "publisher_profile_image"

    move-object/from16 v0, v69

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_34

    .line 691162
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 691163
    :cond_34
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 691164
    :cond_35
    const/16 v2, 0x38

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 691165
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v68

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691166
    if-eqz v16, :cond_36

    .line 691167
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v67

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 691168
    :cond_36
    if-eqz v15, :cond_37

    .line 691169
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v66

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 691170
    :cond_37
    if-eqz v14, :cond_38

    .line 691171
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v65

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 691172
    :cond_38
    if-eqz v13, :cond_39

    .line 691173
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v64

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 691174
    :cond_39
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v63

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691175
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v62

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691176
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v61

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691177
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v60

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691178
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v59

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691179
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v58

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691180
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691181
    if-eqz v7, :cond_3a

    .line 691182
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v56

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 691183
    :cond_3a
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691184
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691185
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691186
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691187
    if-eqz v6, :cond_3b

    .line 691188
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 691189
    :cond_3b
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691190
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691191
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691192
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691193
    if-eqz v3, :cond_3c

    .line 691194
    const/16 v3, 0x18

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 691195
    :cond_3c
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691196
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691197
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691198
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691199
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691200
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691201
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691202
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691203
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691204
    if-eqz v12, :cond_3d

    .line 691205
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 691206
    :cond_3d
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691207
    const/16 v2, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691208
    const/16 v2, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691209
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691210
    const/16 v2, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691211
    const/16 v2, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691212
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691213
    const/16 v2, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691214
    const/16 v2, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691215
    if-eqz v11, :cond_3e

    .line 691216
    const/16 v2, 0x2d

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 691217
    :cond_3e
    const/16 v2, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691218
    if-eqz v10, :cond_3f

    .line 691219
    const/16 v2, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 691220
    :cond_3f
    if-eqz v9, :cond_40

    .line 691221
    const/16 v3, 0x30

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v24

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 691222
    :cond_40
    if-eqz v8, :cond_41

    .line 691223
    const/16 v2, 0x31

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 691224
    :cond_41
    const/16 v2, 0x33

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691225
    const/16 v2, 0x34

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691226
    const/16 v2, 0x35

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691227
    const/16 v2, 0x36

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691228
    const/16 v2, 0x37

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 691229
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_42
    move/from16 v68, v67

    move/from16 v67, v66

    move/from16 v66, v65

    move/from16 v65, v64

    move/from16 v64, v63

    move/from16 v63, v62

    move/from16 v62, v61

    move/from16 v61, v60

    move/from16 v60, v59

    move/from16 v59, v58

    move/from16 v58, v57

    move/from16 v57, v56

    move/from16 v56, v55

    move/from16 v55, v54

    move/from16 v54, v53

    move/from16 v53, v52

    move/from16 v52, v51

    move/from16 v51, v50

    move/from16 v50, v49

    move/from16 v49, v48

    move/from16 v48, v47

    move/from16 v47, v46

    move/from16 v46, v43

    move/from16 v43, v40

    move/from16 v40, v37

    move/from16 v37, v34

    move/from16 v34, v31

    move/from16 v31, v28

    move/from16 v28, v25

    move/from16 v71, v32

    move/from16 v32, v29

    move/from16 v29, v26

    move/from16 v26, v23

    move/from16 v23, v22

    move/from16 v22, v19

    move/from16 v19, v16

    move/from16 v16, v13

    move v13, v10

    move v10, v4

    move/from16 v72, v41

    move/from16 v41, v38

    move/from16 v38, v35

    move/from16 v35, v71

    move-wide/from16 v73, v44

    move/from16 v45, v42

    move/from16 v44, v72

    move/from16 v42, v39

    move/from16 v39, v36

    move/from16 v36, v33

    move/from16 v33, v30

    move/from16 v30, v27

    move/from16 v27, v24

    move-wide/from16 v24, v20

    move/from16 v20, v17

    move/from16 v21, v18

    move/from16 v18, v15

    move/from16 v17, v14

    move v14, v11

    move v15, v12

    move v12, v6

    move v11, v5

    move v6, v8

    move-wide/from16 v4, v73

    move v8, v2

    move/from16 v75, v9

    move v9, v3

    move v3, v7

    move/from16 v7, v75

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const-wide/16 v2, 0x0

    .line 691230
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 691231
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691232
    if-eqz v0, :cond_0

    .line 691233
    const-string v1, "campaign_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691234
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691235
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 691236
    if-eqz v0, :cond_1

    .line 691237
    const-string v1, "can_donate"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691238
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 691239
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 691240
    if-eqz v0, :cond_2

    .line 691241
    const-string v1, "can_invite_to_campaign"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691242
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 691243
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 691244
    if-eqz v0, :cond_3

    .line 691245
    const-string v1, "can_viewer_post"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691246
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 691247
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 691248
    if-eqz v0, :cond_4

    .line 691249
    const-string v1, "can_viewer_report"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691250
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 691251
    :cond_4
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691252
    if-eqz v0, :cond_5

    .line 691253
    const-string v1, "donors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691254
    invoke-static {p0, v0, p2, p3}, LX/4NB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 691255
    :cond_5
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691256
    if-eqz v0, :cond_6

    .line 691257
    const-string v1, "feedAwesomizerProfilePicture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691258
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 691259
    :cond_6
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691260
    if-eqz v0, :cond_7

    .line 691261
    const-string v1, "full_width_post_donation_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691262
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 691263
    :cond_7
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691264
    if-eqz v0, :cond_8

    .line 691265
    const-string v1, "fundraiser_detailed_progress_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691266
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 691267
    :cond_8
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691268
    if-eqz v0, :cond_9

    .line 691269
    const-string v1, "fundraiser_for_charity_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691270
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 691271
    :cond_9
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691272
    if-eqz v0, :cond_a

    .line 691273
    const-string v1, "fundraiser_page_subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691274
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 691275
    :cond_a
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691276
    if-eqz v0, :cond_b

    .line 691277
    const-string v1, "fundraiser_progress_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691278
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 691279
    :cond_b
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 691280
    if-eqz v0, :cond_c

    .line 691281
    const-string v1, "has_goal_amount"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691282
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 691283
    :cond_c
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691284
    if-eqz v0, :cond_d

    .line 691285
    const-string v1, "header_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691286
    invoke-static {p0, v0, p2, p3}, LX/2sY;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 691287
    :cond_d
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691288
    if-eqz v0, :cond_e

    .line 691289
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691290
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691291
    :cond_e
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691292
    if-eqz v0, :cond_f

    .line 691293
    const-string v1, "imageHighOrig"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691294
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 691295
    :cond_f
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691296
    if-eqz v0, :cond_10

    .line 691297
    const-string v1, "invited_you_to_donate_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691298
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 691299
    :cond_10
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 691300
    if-eqz v0, :cond_11

    .line 691301
    const-string v1, "is_viewer_following"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691302
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 691303
    :cond_11
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691304
    if-eqz v0, :cond_12

    .line 691305
    const-string v1, "logo_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691306
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 691307
    :cond_12
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691308
    if-eqz v0, :cond_13

    .line 691309
    const-string v1, "mobile_donate_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691310
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691311
    :cond_13
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691312
    if-eqz v0, :cond_14

    .line 691313
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691314
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691315
    :cond_14
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691316
    if-eqz v0, :cond_15

    .line 691317
    const-string v1, "owner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691318
    invoke-static {p0, v0, p2, p3}, LX/2bM;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 691319
    :cond_15
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 691320
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_16

    .line 691321
    const-string v2, "percent_of_goal_reached"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691322
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 691323
    :cond_16
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691324
    if-eqz v0, :cond_17

    .line 691325
    const-string v1, "posted_item_privacy_scope"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691326
    invoke-static {p0, v0, p2, p3}, LX/2bo;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 691327
    :cond_17
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691328
    if-eqz v0, :cond_18

    .line 691329
    const-string v1, "privacy_scope"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691330
    invoke-static {p0, v0, p2, p3}, LX/2bo;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 691331
    :cond_18
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691332
    if-eqz v0, :cond_19

    .line 691333
    const-string v1, "profileImageLarge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691334
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 691335
    :cond_19
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691336
    if-eqz v0, :cond_1a

    .line 691337
    const-string v1, "profileImageSmall"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691338
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 691339
    :cond_1a
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691340
    if-eqz v0, :cond_1b

    .line 691341
    const-string v1, "profilePicture50"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691342
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 691343
    :cond_1b
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691344
    if-eqz v0, :cond_1c

    .line 691345
    const-string v1, "profilePictureHighRes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691346
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 691347
    :cond_1c
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691348
    if-eqz v0, :cond_1d

    .line 691349
    const-string v1, "profilePictureLarge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691350
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 691351
    :cond_1d
    const/16 v0, 0x20

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691352
    if-eqz v0, :cond_1e

    .line 691353
    const-string v1, "profile_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691354
    invoke-static {p0, v0, p2, p3}, LX/2sY;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 691355
    :cond_1e
    const/16 v0, 0x21

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691356
    if-eqz v0, :cond_1f

    .line 691357
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691358
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 691359
    :cond_1f
    const/16 v0, 0x22

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 691360
    if-eqz v0, :cond_20

    .line 691361
    const-string v1, "profile_picture_is_silhouette"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691362
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 691363
    :cond_20
    const/16 v0, 0x24

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691364
    if-eqz v0, :cond_21

    .line 691365
    const-string v1, "streaming_profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691366
    invoke-static {p0, v0, p2}, LX/4TN;->a(LX/15i;ILX/0nX;)V

    .line 691367
    :cond_21
    const/16 v0, 0x25

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691368
    if-eqz v0, :cond_22

    .line 691369
    const-string v1, "taggable_object_profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691370
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 691371
    :cond_22
    const/16 v0, 0x26

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691372
    if-eqz v0, :cond_23

    .line 691373
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691374
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691375
    :cond_23
    const/16 v0, 0x27

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691376
    if-eqz v0, :cond_24

    .line 691377
    const-string v1, "charity_interface"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691378
    invoke-static {p0, v0, p2, p3}, LX/4LB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 691379
    :cond_24
    const/16 v0, 0x28

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691380
    if-eqz v0, :cond_25

    .line 691381
    const-string v1, "amount_raised_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691382
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691383
    :cond_25
    const/16 v0, 0x29

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691384
    if-eqz v0, :cond_26

    .line 691385
    const-string v1, "campaign_goal_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691386
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691387
    :cond_26
    const/16 v0, 0x2a

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691388
    if-eqz v0, :cond_27

    .line 691389
    const-string v1, "days_left_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691390
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 691391
    :cond_27
    const/16 v0, 0x2b

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691392
    if-eqz v0, :cond_28

    .line 691393
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691394
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691395
    :cond_28
    const/16 v0, 0x2c

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691396
    if-eqz v0, :cond_29

    .line 691397
    const-string v1, "fundraiser_by_owner_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691398
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 691399
    :cond_29
    const/16 v0, 0x2d

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 691400
    if-eqz v0, :cond_2a

    .line 691401
    const-string v1, "has_viewer_donated"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691402
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 691403
    :cond_2a
    const/16 v0, 0x2e

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691404
    if-eqz v0, :cond_2b

    .line 691405
    const-string v1, "total_donated_amount_by_viewer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691406
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691407
    :cond_2b
    const/16 v0, 0x2f

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 691408
    if-eqz v0, :cond_2c

    .line 691409
    const-string v1, "can_viewer_edit"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691410
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 691411
    :cond_2c
    const/16 v0, 0x30

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 691412
    cmp-long v2, v0, v4

    if-eqz v2, :cond_2d

    .line 691413
    const-string v2, "end_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691414
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 691415
    :cond_2d
    const/16 v0, 0x31

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 691416
    if-eqz v0, :cond_2e

    .line 691417
    const-string v1, "can_viewer_delete"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691418
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 691419
    :cond_2e
    const/16 v0, 0x33

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691420
    if-eqz v0, :cond_2f

    .line 691421
    const-string v1, "goal_amount"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691422
    invoke-static {p0, v0, p2}, LX/4Le;->a(LX/15i;ILX/0nX;)V

    .line 691423
    :cond_2f
    const/16 v0, 0x34

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691424
    if-eqz v0, :cond_30

    .line 691425
    const-string v1, "donors_social_context_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691426
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 691427
    :cond_30
    const/16 v0, 0x35

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691428
    if-eqz v0, :cond_31

    .line 691429
    const-string v1, "user_donors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691430
    invoke-static {p0, v0, p2, p3}, LX/4N6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 691431
    :cond_31
    const/16 v0, 0x36

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691432
    if-eqz v0, :cond_32

    .line 691433
    const-string v1, "profilePicture180"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691434
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 691435
    :cond_32
    const/16 v0, 0x37

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691436
    if-eqz v0, :cond_33

    .line 691437
    const-string v1, "publisher_profile_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691438
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 691439
    :cond_33
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 691440
    return-void
.end method
