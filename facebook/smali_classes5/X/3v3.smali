.class public final LX/3v3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3qv;


# static fields
.field public static w:Ljava/lang/String;

.field public static x:Ljava/lang/String;

.field public static y:Ljava/lang/String;

.field public static z:Ljava/lang/String;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field public final d:I

.field private e:Ljava/lang/CharSequence;

.field private f:Ljava/lang/CharSequence;

.field public g:Landroid/content/Intent;

.field private h:C

.field private i:C

.field private j:Landroid/graphics/drawable/Drawable;

.field private k:I

.field public l:LX/3v0;

.field private m:LX/3vG;

.field public n:Ljava/lang/Runnable;

.field public o:Landroid/view/MenuItem$OnMenuItemClickListener;

.field private p:I

.field public q:I

.field private r:Landroid/view/View;

.field public s:LX/3rR;

.field private t:LX/3rk;

.field private u:Z

.field public v:Landroid/view/ContextMenu$ContextMenuInfo;


# direct methods
.method public constructor <init>(LX/3v0;IIIILjava/lang/CharSequence;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 650652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 650653
    iput v1, p0, LX/3v3;->k:I

    .line 650654
    const/16 v0, 0x10

    iput v0, p0, LX/3v3;->p:I

    .line 650655
    iput v1, p0, LX/3v3;->q:I

    .line 650656
    iput-boolean v1, p0, LX/3v3;->u:Z

    .line 650657
    iput-object p1, p0, LX/3v3;->l:LX/3v0;

    .line 650658
    iput p3, p0, LX/3v3;->a:I

    .line 650659
    iput p2, p0, LX/3v3;->b:I

    .line 650660
    iput p4, p0, LX/3v3;->c:I

    .line 650661
    iput p5, p0, LX/3v3;->d:I

    .line 650662
    iput-object p6, p0, LX/3v3;->e:Ljava/lang/CharSequence;

    .line 650663
    iput p7, p0, LX/3v3;->q:I

    .line 650664
    return-void
.end method

.method private a(Landroid/view/View;)LX/3qv;
    .locals 2

    .prologue
    .line 650573
    iput-object p1, p0, LX/3v3;->r:Landroid/view/View;

    .line 650574
    const/4 v0, 0x0

    iput-object v0, p0, LX/3v3;->s:LX/3rR;

    .line 650575
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, LX/3v3;->a:I

    if-lez v0, :cond_0

    .line 650576
    iget v0, p0, LX/3v3;->a:I

    invoke-virtual {p1, v0}, Landroid/view/View;->setId(I)V

    .line 650577
    :cond_0
    iget-object v0, p0, LX/3v3;->l:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->i()V

    .line 650578
    return-object p0
.end method


# virtual methods
.method public final a(LX/3rR;)LX/3qv;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 650579
    iget-object v0, p0, LX/3v3;->s:LX/3rR;

    if-eqz v0, :cond_0

    .line 650580
    iget-object v0, p0, LX/3v3;->s:LX/3rR;

    invoke-virtual {v0, v1}, LX/3rR;->a(LX/3rQ;)V

    .line 650581
    :cond_0
    iput-object v1, p0, LX/3v3;->r:Landroid/view/View;

    .line 650582
    iput-object p1, p0, LX/3v3;->s:LX/3rR;

    .line 650583
    iget-object v0, p0, LX/3v3;->l:LX/3v0;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/3v0;->b(Z)V

    .line 650584
    iget-object v0, p0, LX/3v3;->s:LX/3rR;

    if-eqz v0, :cond_1

    .line 650585
    iget-object v0, p0, LX/3v3;->s:LX/3rR;

    new-instance v1, LX/3v2;

    invoke-direct {v1, p0}, LX/3v2;-><init>(LX/3v3;)V

    invoke-virtual {v0, v1}, LX/3rR;->a(LX/3rQ;)V

    .line 650586
    :cond_1
    return-object p0
.end method

.method public final a(LX/3rk;)LX/3qv;
    .locals 0

    .prologue
    .line 650587
    iput-object p1, p0, LX/3v3;->t:LX/3rk;

    .line 650588
    return-object p0
.end method

.method public final a()LX/3rR;
    .locals 1

    .prologue
    .line 650589
    iget-object v0, p0, LX/3v3;->s:LX/3rR;

    return-object v0
.end method

.method public final a(LX/3uq;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 650590
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/3uq;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/3v3;->getTitleCondensed()Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/3v3;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/3vG;)V
    .locals 1

    .prologue
    .line 650591
    iput-object p1, p0, LX/3v3;->m:LX/3vG;

    .line 650592
    invoke-virtual {p0}, LX/3v3;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/3vG;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    .line 650593
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 650594
    iget v0, p0, LX/3v3;->p:I

    and-int/lit8 v1, v0, -0x5

    if-eqz p1, :cond_0

    const/4 v0, 0x4

    :goto_0
    or-int/2addr v0, v1

    iput v0, p0, LX/3v3;->p:I

    .line 650595
    return-void

    .line 650596
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 650597
    iget v2, p0, LX/3v3;->p:I

    .line 650598
    iget v0, p0, LX/3v3;->p:I

    and-int/lit8 v3, v0, -0x3

    if-eqz p1, :cond_1

    const/4 v0, 0x2

    :goto_0
    or-int/2addr v0, v3

    iput v0, p0, LX/3v3;->p:I

    .line 650599
    iget v0, p0, LX/3v3;->p:I

    if-eq v2, v0, :cond_0

    .line 650600
    iget-object v0, p0, LX/3v3;->l:LX/3v0;

    invoke-virtual {v0, v1}, LX/3v0;->b(Z)V

    .line 650601
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 650602
    goto :goto_0
.end method

.method public final c(Z)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 650603
    iget v2, p0, LX/3v3;->p:I

    .line 650604
    iget v0, p0, LX/3v3;->p:I

    and-int/lit8 v3, v0, -0x9

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    or-int/2addr v0, v3

    iput v0, p0, LX/3v3;->p:I

    .line 650605
    iget v0, p0, LX/3v3;->p:I

    if-eq v2, v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    .line 650606
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final collapseActionView()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 650607
    iget v1, p0, LX/3v3;->q:I

    and-int/lit8 v1, v1, 0x8

    if-nez v1, :cond_1

    .line 650608
    :cond_0
    :goto_0
    return v0

    .line 650609
    :cond_1
    iget-object v1, p0, LX/3v3;->r:Landroid/view/View;

    if-nez v1, :cond_2

    .line 650610
    const/4 v0, 0x1

    goto :goto_0

    .line 650611
    :cond_2
    iget-object v1, p0, LX/3v3;->t:LX/3rk;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/3v3;->t:LX/3rk;

    invoke-interface {v1, p0}, LX/3rk;->b(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 650612
    :cond_3
    iget-object v0, p0, LX/3v3;->l:LX/3v0;

    invoke-virtual {v0, p0}, LX/3v0;->b(LX/3v3;)Z

    move-result v0

    goto :goto_0
.end method

.method public final d()C
    .locals 1

    .prologue
    .line 650613
    iget-object v0, p0, LX/3v3;->l:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-char v0, p0, LX/3v3;->i:C

    :goto_0
    return v0

    :cond_0
    iget-char v0, p0, LX/3v3;->h:C

    goto :goto_0
.end method

.method public final d(Z)V
    .locals 1

    .prologue
    .line 650614
    if-eqz p1, :cond_0

    .line 650615
    iget v0, p0, LX/3v3;->p:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, LX/3v3;->p:I

    .line 650616
    :goto_0
    return-void

    .line 650617
    :cond_0
    iget v0, p0, LX/3v3;->p:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, LX/3v3;->p:I

    goto :goto_0
.end method

.method public final e(Z)V
    .locals 2

    .prologue
    .line 650618
    iput-boolean p1, p0, LX/3v3;->u:Z

    .line 650619
    iget-object v0, p0, LX/3v3;->l:LX/3v0;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/3v0;->b(Z)V

    .line 650620
    return-void
.end method

.method public final expandActionView()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 650621
    invoke-virtual {p0}, LX/3v3;->n()Z

    move-result v1

    if-nez v1, :cond_1

    .line 650622
    :cond_0
    :goto_0
    return v0

    .line 650623
    :cond_1
    iget-object v1, p0, LX/3v3;->t:LX/3rk;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/3v3;->t:LX/3rk;

    invoke-interface {v1, p0}, LX/3rk;->a(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 650624
    :cond_2
    iget-object v0, p0, LX/3v3;->l:LX/3v0;

    invoke-virtual {v0, p0}, LX/3v0;->a(LX/3v3;)Z

    move-result v0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 650625
    iget-object v0, p0, LX/3v3;->l:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/3v3;->d()C

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 650626
    iget v0, p0, LX/3v3;->p:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getActionProvider()Landroid/view/ActionProvider;
    .locals 2

    .prologue
    .line 650627
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is not supported, use MenuItemCompat.getActionProvider()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getActionView()Landroid/view/View;
    .locals 1

    .prologue
    .line 650628
    iget-object v0, p0, LX/3v3;->r:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 650629
    iget-object v0, p0, LX/3v3;->r:Landroid/view/View;

    .line 650630
    :goto_0
    return-object v0

    .line 650631
    :cond_0
    iget-object v0, p0, LX/3v3;->s:LX/3rR;

    if-eqz v0, :cond_1

    .line 650632
    iget-object v0, p0, LX/3v3;->s:LX/3rR;

    invoke-virtual {v0, p0}, LX/3rR;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/3v3;->r:Landroid/view/View;

    .line 650633
    iget-object v0, p0, LX/3v3;->r:Landroid/view/View;

    goto :goto_0

    .line 650634
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getAlphabeticShortcut()C
    .locals 1

    .prologue
    .line 650635
    iget-char v0, p0, LX/3v3;->i:C

    return v0
.end method

.method public final getGroupId()I
    .locals 1

    .prologue
    .line 650636
    iget v0, p0, LX/3v3;->b:I

    return v0
.end method

.method public final getIcon()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 650637
    iget-object v0, p0, LX/3v3;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 650638
    iget-object v0, p0, LX/3v3;->j:Landroid/graphics/drawable/Drawable;

    .line 650639
    :goto_0
    return-object v0

    .line 650640
    :cond_0
    iget v0, p0, LX/3v3;->k:I

    if-eqz v0, :cond_1

    .line 650641
    iget-object v0, p0, LX/3v3;->l:LX/3v0;

    .line 650642
    iget-object v1, v0, LX/3v0;->e:Landroid/content/Context;

    move-object v0, v1

    .line 650643
    iget v1, p0, LX/3v3;->k:I

    invoke-static {v0, v1}, LX/3wA;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 650644
    const/4 v1, 0x0

    iput v1, p0, LX/3v3;->k:I

    .line 650645
    iput-object v0, p0, LX/3v3;->j:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 650646
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 650647
    iget-object v0, p0, LX/3v3;->g:Landroid/content/Intent;

    return-object v0
.end method

.method public final getItemId()I
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    .prologue
    .line 650648
    iget v0, p0, LX/3v3;->a:I

    return v0
.end method

.method public final getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    .prologue
    .line 650649
    iget-object v0, p0, LX/3v3;->v:Landroid/view/ContextMenu$ContextMenuInfo;

    return-object v0
.end method

.method public final getNumericShortcut()C
    .locals 1

    .prologue
    .line 650650
    iget-char v0, p0, LX/3v3;->h:C

    return v0
.end method

.method public final getOrder()I
    .locals 1

    .prologue
    .line 650651
    iget v0, p0, LX/3v3;->c:I

    return v0
.end method

.method public final getSubMenu()Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 650566
    iget-object v0, p0, LX/3v3;->m:LX/3vG;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    .prologue
    .line 650665
    iget-object v0, p0, LX/3v3;->e:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getTitleCondensed()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 650567
    iget-object v0, p0, LX/3v3;->f:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3v3;->f:Ljava/lang/CharSequence;

    .line 650568
    :goto_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-ge v1, v2, :cond_0

    if-eqz v0, :cond_0

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 650569
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 650570
    :cond_0
    return-object v0

    .line 650571
    :cond_1
    iget-object v0, p0, LX/3v3;->e:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public final hasSubMenu()Z
    .locals 1

    .prologue
    .line 650572
    iget-object v0, p0, LX/3v3;->m:LX/3vG;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isActionViewExpanded()Z
    .locals 1

    .prologue
    .line 650485
    iget-boolean v0, p0, LX/3v3;->u:Z

    return v0
.end method

.method public final isCheckable()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 650518
    iget v1, p0, LX/3v3;->p:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isChecked()Z
    .locals 2

    .prologue
    .line 650517
    iget v0, p0, LX/3v3;->p:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEnabled()Z
    .locals 1

    .prologue
    .line 650516
    iget v0, p0, LX/3v3;->p:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isVisible()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 650511
    iget-object v2, p0, LX/3v3;->s:LX/3rR;

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/3v3;->s:LX/3rR;

    invoke-virtual {v2}, LX/3rR;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 650512
    iget v2, p0, LX/3v3;->p:I

    and-int/lit8 v2, v2, 0x8

    if-nez v2, :cond_1

    iget-object v2, p0, LX/3v3;->s:LX/3rR;

    invoke-virtual {v2}, LX/3rR;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 650513
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 650514
    goto :goto_0

    .line 650515
    :cond_2
    iget v2, p0, LX/3v3;->p:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 650510
    iget v0, p0, LX/3v3;->p:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 650509
    iget v1, p0, LX/3v3;->q:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 650508
    iget v0, p0, LX/3v3;->q:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 650503
    iget v1, p0, LX/3v3;->q:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_1

    .line 650504
    iget-object v1, p0, LX/3v3;->r:Landroid/view/View;

    if-nez v1, :cond_0

    iget-object v1, p0, LX/3v3;->s:LX/3rR;

    if-eqz v1, :cond_0

    .line 650505
    iget-object v1, p0, LX/3v3;->s:LX/3rR;

    invoke-virtual {v1, p0}, LX/3rR;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, LX/3v3;->r:Landroid/view/View;

    .line 650506
    :cond_0
    iget-object v1, p0, LX/3v3;->r:Landroid/view/View;

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 650507
    :cond_1
    return v0
.end method

.method public final setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 650502
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is not supported, use MenuItemCompat.setActionProvider()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setActionView(I)Landroid/view/MenuItem;
    .locals 3

    .prologue
    .line 650497
    iget-object v0, p0, LX/3v3;->l:LX/3v0;

    .line 650498
    iget-object v1, v0, LX/3v0;->e:Landroid/content/Context;

    move-object v0, v1

    .line 650499
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 650500
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    invoke-virtual {v1, p1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, LX/3v3;->a(Landroid/view/View;)LX/3qv;

    .line 650501
    return-object p0
.end method

.method public final synthetic setActionView(Landroid/view/View;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650496
    invoke-direct {p0, p1}, LX/3v3;->a(Landroid/view/View;)LX/3qv;

    move-result-object v0

    return-object v0
.end method

.method public final setAlphabeticShortcut(C)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 650492
    iget-char v0, p0, LX/3v3;->i:C

    if-ne v0, p1, :cond_0

    .line 650493
    :goto_0
    return-object p0

    .line 650494
    :cond_0
    invoke-static {p1}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    iput-char v0, p0, LX/3v3;->i:C

    .line 650495
    iget-object v0, p0, LX/3v3;->l:LX/3v0;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/3v0;->b(Z)V

    goto :goto_0
.end method

.method public final setCheckable(Z)Landroid/view/MenuItem;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 650486
    iget v2, p0, LX/3v3;->p:I

    .line 650487
    iget v0, p0, LX/3v3;->p:I

    and-int/lit8 v3, v0, -0x2

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v3

    iput v0, p0, LX/3v3;->p:I

    .line 650488
    iget v0, p0, LX/3v3;->p:I

    if-eq v2, v0, :cond_0

    .line 650489
    iget-object v0, p0, LX/3v3;->l:LX/3v0;

    invoke-virtual {v0, v1}, LX/3v0;->b(Z)V

    .line 650490
    :cond_0
    return-object p0

    :cond_1
    move v0, v1

    .line 650491
    goto :goto_0
.end method

.method public final setChecked(Z)Landroid/view/MenuItem;
    .locals 6

    .prologue
    .line 650471
    iget v0, p0, LX/3v3;->p:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 650472
    iget-object v0, p0, LX/3v3;->l:LX/3v0;

    const/4 v3, 0x0

    .line 650473
    invoke-interface {p0}, Landroid/view/MenuItem;->getGroupId()I

    move-result v5

    .line 650474
    iget-object v1, v0, LX/3v0;->j:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result p1

    move v4, v3

    .line 650475
    :goto_0
    if-ge v4, p1, :cond_2

    .line 650476
    iget-object v1, v0, LX/3v0;->j:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3v3;

    .line 650477
    invoke-virtual {v1}, LX/3v3;->getGroupId()I

    move-result v2

    if-ne v2, v5, :cond_0

    .line 650478
    invoke-virtual {v1}, LX/3v3;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 650479
    invoke-virtual {v1}, LX/3v3;->isCheckable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 650480
    if-ne v1, p0, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v1, v2}, LX/3v3;->b(Z)V

    .line 650481
    :cond_0
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    :cond_1
    move v2, v3

    .line 650482
    goto :goto_1

    .line 650483
    :cond_2
    :goto_2
    return-object p0

    .line 650484
    :cond_3
    invoke-virtual {p0, p1}, LX/3v3;->b(Z)V

    goto :goto_2
.end method

.method public final setEnabled(Z)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 650519
    if-eqz p1, :cond_0

    .line 650520
    iget v0, p0, LX/3v3;->p:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, LX/3v3;->p:I

    .line 650521
    :goto_0
    iget-object v0, p0, LX/3v3;->l:LX/3v0;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/3v0;->b(Z)V

    .line 650522
    return-object p0

    .line 650523
    :cond_0
    iget v0, p0, LX/3v3;->p:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, LX/3v3;->p:I

    goto :goto_0
.end method

.method public final setIcon(I)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 650524
    const/4 v0, 0x0

    iput-object v0, p0, LX/3v3;->j:Landroid/graphics/drawable/Drawable;

    .line 650525
    iput p1, p0, LX/3v3;->k:I

    .line 650526
    iget-object v0, p0, LX/3v3;->l:LX/3v0;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/3v0;->b(Z)V

    .line 650527
    return-object p0
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 650528
    iput v1, p0, LX/3v3;->k:I

    .line 650529
    iput-object p1, p0, LX/3v3;->j:Landroid/graphics/drawable/Drawable;

    .line 650530
    iget-object v0, p0, LX/3v3;->l:LX/3v0;

    invoke-virtual {v0, v1}, LX/3v0;->b(Z)V

    .line 650531
    return-object p0
.end method

.method public final setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 650532
    iput-object p1, p0, LX/3v3;->g:Landroid/content/Intent;

    .line 650533
    return-object p0
.end method

.method public final setNumericShortcut(C)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 650534
    iget-char v0, p0, LX/3v3;->h:C

    if-ne v0, p1, :cond_0

    .line 650535
    :goto_0
    return-object p0

    .line 650536
    :cond_0
    iput-char p1, p0, LX/3v3;->h:C

    .line 650537
    iget-object v0, p0, LX/3v3;->l:LX/3v0;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/3v0;->b(Z)V

    goto :goto_0
.end method

.method public final setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 650538
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is not supported, use MenuItemCompat.setOnActionExpandListener()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 650539
    iput-object p1, p0, LX/3v3;->o:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 650540
    return-object p0
.end method

.method public final setShortcut(CC)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 650541
    iput-char p1, p0, LX/3v3;->h:C

    .line 650542
    invoke-static {p2}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    iput-char v0, p0, LX/3v3;->i:C

    .line 650543
    iget-object v0, p0, LX/3v3;->l:LX/3v0;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/3v0;->b(Z)V

    .line 650544
    return-object p0
.end method

.method public final setShowAsAction(I)V
    .locals 2

    .prologue
    .line 650545
    and-int/lit8 v0, p1, 0x3

    packed-switch v0, :pswitch_data_0

    .line 650546
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 650547
    :pswitch_0
    iput p1, p0, LX/3v3;->q:I

    .line 650548
    iget-object v0, p0, LX/3v3;->l:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->i()V

    .line 650549
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final setShowAsActionFlags(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650550
    invoke-virtual {p0, p1}, LX/3v3;->setShowAsAction(I)V

    .line 650551
    return-object p0
.end method

.method public final setTitle(I)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 650552
    iget-object v0, p0, LX/3v3;->l:LX/3v0;

    .line 650553
    iget-object v1, v0, LX/3v0;->e:Landroid/content/Context;

    move-object v0, v1

    .line 650554
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3v3;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 650555
    iput-object p1, p0, LX/3v3;->e:Ljava/lang/CharSequence;

    .line 650556
    iget-object v0, p0, LX/3v3;->l:LX/3v0;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/3v0;->b(Z)V

    .line 650557
    iget-object v0, p0, LX/3v3;->m:LX/3vG;

    if-eqz v0, :cond_0

    .line 650558
    iget-object v0, p0, LX/3v3;->m:LX/3vG;

    invoke-virtual {v0, p1}, LX/3vG;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    .line 650559
    :cond_0
    return-object p0
.end method

.method public final setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 650560
    iput-object p1, p0, LX/3v3;->f:Ljava/lang/CharSequence;

    .line 650561
    iget-object v0, p0, LX/3v3;->l:LX/3v0;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/3v0;->b(Z)V

    .line 650562
    return-object p0
.end method

.method public final setVisible(Z)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650563
    invoke-virtual {p0, p1}, LX/3v3;->c(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3v3;->l:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->h()V

    .line 650564
    :cond_0
    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 650565
    iget-object v0, p0, LX/3v3;->e:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
