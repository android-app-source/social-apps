.class public LX/4h4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/performancelogger/PerformanceLogger;

.field public final b:Ljava/lang/String;

.field private final c:Z

.field private final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;D)V
    .locals 2

    .prologue
    .line 800383
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    cmpg-double v0, v0, p2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/4h4;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;ZLjava/lang/String;)V

    .line 800384
    return-void

    .line 800385
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;ZLjava/lang/String;)V
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 800386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 800387
    iput-object p1, p0, LX/4h4;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 800388
    iput-boolean p2, p0, LX/4h4;->c:Z

    .line 800389
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 800390
    iput-object v0, p0, LX/4h4;->b:Ljava/lang/String;

    .line 800391
    iput-object p3, p0, LX/4h4;->d:Ljava/lang/String;

    .line 800392
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;)LX/0Yj;
    .locals 4

    .prologue
    .line 800393
    new-instance v2, LX/0Yj;

    invoke-direct {v2, p1, p2}, LX/0Yj;-><init>(ILjava/lang/String;)V

    .line 800394
    iget-object v0, p0, LX/4h4;->b:Ljava/lang/String;

    .line 800395
    iput-object v0, v2, LX/0Yj;->e:Ljava/lang/String;

    .line 800396
    iget-boolean v0, p0, LX/4h4;->c:Z

    if-eqz v0, :cond_1

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    :goto_0
    invoke-virtual {v2, v0, v1}, LX/0Yj;->a(D)LX/0Yj;

    .line 800397
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 800398
    const-string v1, "session_id"

    iget-object v3, p0, LX/4h4;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 800399
    iget-object v1, p0, LX/4h4;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 800400
    const-string v1, "parent_session_id"

    iget-object v3, p0, LX/4h4;->d:Ljava/lang/String;

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 800401
    :cond_0
    invoke-virtual {v2, v0}, LX/0Yj;->a(Ljava/util/Map;)LX/0Yj;

    .line 800402
    return-object v2

    .line 800403
    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0
.end method
