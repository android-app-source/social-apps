.class public LX/3gV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "LX/0lF;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 624512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 624513
    return-void
.end method

.method public static a(LX/0QB;)LX/3gV;
    .locals 1

    .prologue
    .line 624514
    new-instance v0, LX/3gV;

    invoke-direct {v0}, LX/3gV;-><init>()V

    .line 624515
    move-object v0, v0

    .line 624516
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 624517
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 624518
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "JSON"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 624519
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "config_sections[]"

    const-string v3, "mqtt_config"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 624520
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    sget-object v2, LX/11I;->MQTT_CONFIG:LX/11I;

    iget-object v2, v2, LX/11I;->requestNameString:Ljava/lang/String;

    .line 624521
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 624522
    move-object v1, v1

    .line 624523
    const-string v2, "GET"

    .line 624524
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 624525
    move-object v1, v1

    .line 624526
    const-string v2, "/me/mobile_configs"

    .line 624527
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 624528
    move-object v1, v1

    .line 624529
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 624530
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 624531
    move-object v1, v1

    .line 624532
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 624533
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 624534
    move-object v0, v1

    .line 624535
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 624536
    const/4 v1, 0x0

    .line 624537
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 624538
    if-nez v0, :cond_0

    .line 624539
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 624540
    :cond_0
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "mqtt_config"

    aput-object v3, v2, v1

    .line 624541
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 624542
    const-string v4, "data"

    invoke-virtual {v0, v4}, LX/0lF;->f(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    move v0, v1

    .line 624543
    :goto_0
    if-gtz v0, :cond_3

    aget-object v5, v2, v1

    .line 624544
    invoke-virtual {v4}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0lF;

    .line 624545
    const-string p0, "section_name"

    invoke-virtual {v6, p0}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object p0

    invoke-virtual {p0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 624546
    const-string v7, "value"

    invoke-virtual {v6, v7}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    .line 624547
    :goto_1
    move-object v6, v6

    .line 624548
    invoke-virtual {v6}, LX/0lF;->g()Z

    move-result v7

    if-nez v7, :cond_2

    .line 624549
    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 624550
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 624551
    :cond_3
    return-object v3

    .line 624552
    :cond_4
    sget-object v6, LX/2AT;->a:LX/2AT;

    move-object v6, v6

    .line 624553
    goto :goto_1
.end method
