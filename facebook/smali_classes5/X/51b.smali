.class public final LX/51b;
.super LX/4x4;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C::",
        "Ljava/lang/Comparable",
        "<*>;>",
        "LX/4x4",
        "<",
        "LX/4xM",
        "<TC;>;",
        "LX/50M",
        "<TC;>;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/NavigableMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/NavigableMap",
            "<",
            "LX/4xM",
            "<TC;>;",
            "LX/50M",
            "<TC;>;>;"
        }
    .end annotation
.end field

.field public final b:LX/50M;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/50M",
            "<",
            "LX/4xM",
            "<TC;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/NavigableMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/NavigableMap",
            "<",
            "LX/4xM",
            "<TC;>;",
            "LX/50M",
            "<TC;>;>;)V"
        }
    .end annotation

    .prologue
    .line 825308
    invoke-direct {p0}, LX/4x4;-><init>()V

    .line 825309
    iput-object p1, p0, LX/51b;->a:Ljava/util/NavigableMap;

    .line 825310
    sget-object v0, LX/50M;->d:LX/50M;

    move-object v0, v0

    .line 825311
    iput-object v0, p0, LX/51b;->b:LX/50M;

    .line 825312
    return-void
.end method

.method private constructor <init>(Ljava/util/NavigableMap;LX/50M;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/NavigableMap",
            "<",
            "LX/4xM",
            "<TC;>;",
            "LX/50M",
            "<TC;>;>;",
            "LX/50M",
            "<",
            "LX/4xM",
            "<TC;>;>;)V"
        }
    .end annotation

    .prologue
    .line 825313
    invoke-direct {p0}, LX/4x4;-><init>()V

    .line 825314
    iput-object p1, p0, LX/51b;->a:Ljava/util/NavigableMap;

    .line 825315
    iput-object p2, p0, LX/51b;->b:LX/50M;

    .line 825316
    return-void
.end method

.method private a(Ljava/lang/Object;)LX/50M;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "LX/50M",
            "<TC;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 825324
    instance-of v0, p1, LX/4xM;

    if-eqz v0, :cond_1

    .line 825325
    :try_start_0
    check-cast p1, LX/4xM;

    .line 825326
    iget-object v0, p0, LX/51b;->b:LX/50M;

    invoke-virtual {v0, p1}, LX/50M;->a(Ljava/lang/Comparable;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 825327
    :goto_0
    return-object v0

    .line 825328
    :cond_0
    iget-object v0, p0, LX/51b;->a:Ljava/util/NavigableMap;

    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v2

    .line 825329
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/50M;

    iget-object v0, v0, LX/50M;->upperBound:LX/4xM;

    invoke-virtual {v0, p1}, LX/4xM;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 825330
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/50M;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 825331
    :catch_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 825332
    goto :goto_0
.end method

.method private a(LX/50M;)Ljava/util/NavigableMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/50M",
            "<",
            "LX/4xM",
            "<TC;>;>;)",
            "Ljava/util/NavigableMap",
            "<",
            "LX/4xM",
            "<TC;>;",
            "LX/50M",
            "<TC;>;>;"
        }
    .end annotation

    .prologue
    .line 825317
    iget-object v0, p0, LX/51b;->b:LX/50M;

    invoke-virtual {p1, v0}, LX/50M;->b(LX/50M;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 825318
    new-instance v0, LX/51b;

    iget-object v1, p0, LX/51b;->a:Ljava/util/NavigableMap;

    iget-object v2, p0, LX/51b;->b:LX/50M;

    invoke-virtual {p1, v2}, LX/50M;->c(LX/50M;)LX/50M;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/51b;-><init>(Ljava/util/NavigableMap;LX/50M;)V

    .line 825319
    :goto_0
    return-object v0

    .line 825320
    :cond_0
    sget-object v0, LX/146;->b:LX/146;

    move-object v0, v0

    .line 825321
    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/Iterator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "LX/4xM",
            "<TC;>;",
            "LX/50M",
            "<TC;>;>;>;"
        }
    .end annotation

    .prologue
    .line 825288
    iget-object v0, p0, LX/51b;->b:LX/50M;

    invoke-virtual {v0}, LX/50M;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 825289
    iget-object v0, p0, LX/51b;->a:Ljava/util/NavigableMap;

    iget-object v1, p0, LX/51b;->b:LX/50M;

    invoke-virtual {v1}, LX/50M;->g()Ljava/lang/Comparable;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ljava/util/NavigableMap;->headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->descendingMap()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v0

    .line 825290
    :goto_0
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0RZ;->i(Ljava/util/Iterator;)LX/4yu;

    move-result-object v1

    .line 825291
    invoke-interface {v1}, LX/4yu;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/51b;->b:LX/50M;

    iget-object v2, v0, LX/50M;->upperBound:LX/4xM;

    invoke-interface {v1}, LX/4yu;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/50M;

    iget-object v0, v0, LX/50M;->upperBound:LX/4xM;

    invoke-virtual {v2, v0}, LX/4xM;->a(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 825292
    invoke-interface {v1}, LX/4yu;->next()Ljava/lang/Object;

    .line 825293
    :cond_0
    new-instance v0, LX/51a;

    invoke-direct {v0, p0, v1}, LX/51a;-><init>(LX/51b;LX/4yu;)V

    return-object v0

    .line 825294
    :cond_1
    iget-object v0, p0, LX/51b;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->descendingMap()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method public final comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-",
            "LX/4xM",
            "<TC;>;>;"
        }
    .end annotation

    .prologue
    .line 825322
    sget-object v0, LX/1zb;->a:LX/1zb;

    move-object v0, v0

    .line 825323
    return-object v0
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 825298
    invoke-direct {p0, p1}, LX/51b;->a(Ljava/lang/Object;)LX/50M;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/util/Iterator;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "LX/4xM",
            "<TC;>;",
            "LX/50M",
            "<TC;>;>;>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 825299
    iget-object v0, p0, LX/51b;->b:LX/50M;

    invoke-virtual {v0}, LX/50M;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 825300
    iget-object v0, p0, LX/51b;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 825301
    :goto_0
    new-instance v1, LX/51Z;

    invoke-direct {v1, p0, v0}, LX/51Z;-><init>(LX/51b;Ljava/util/Iterator;)V

    return-object v1

    .line 825302
    :cond_0
    iget-object v0, p0, LX/51b;->a:Ljava/util/NavigableMap;

    iget-object v1, p0, LX/51b;->b:LX/50M;

    invoke-virtual {v1}, LX/50M;->d()Ljava/lang/Comparable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v1

    .line 825303
    if-nez v1, :cond_1

    .line 825304
    iget-object v0, p0, LX/51b;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0

    .line 825305
    :cond_1
    iget-object v0, p0, LX/51b;->b:LX/50M;

    iget-object v2, v0, LX/50M;->lowerBound:LX/4xM;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/50M;

    iget-object v0, v0, LX/50M;->upperBound:LX/4xM;

    invoke-virtual {v2, v0}, LX/4xM;->a(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 825306
    iget-object v0, p0, LX/51b;->a:Ljava/util/NavigableMap;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Ljava/util/NavigableMap;->tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0

    .line 825307
    :cond_2
    iget-object v0, p0, LX/51b;->a:Ljava/util/NavigableMap;

    iget-object v1, p0, LX/51b;->b:LX/50M;

    invoke-virtual {v1}, LX/50M;->d()Ljava/lang/Comparable;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Ljava/util/NavigableMap;->tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 825297
    invoke-direct {p0, p1}, LX/51b;->a(Ljava/lang/Object;)LX/50M;

    move-result-object v0

    return-object v0
.end method

.method public final headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .locals 1

    .prologue
    .line 825295
    check-cast p1, LX/4xM;

    .line 825296
    invoke-static {p2}, LX/4xG;->forBoolean(Z)LX/4xG;

    move-result-object v0

    invoke-static {p1, v0}, LX/50M;->a(Ljava/lang/Comparable;LX/4xG;)LX/50M;

    move-result-object v0

    invoke-direct {p0, v0}, LX/51b;->a(LX/50M;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public final isEmpty()Z
    .locals 2

    .prologue
    .line 825285
    iget-object v0, p0, LX/51b;->b:LX/50M;

    .line 825286
    sget-object v1, LX/50M;->d:LX/50M;

    move-object v1, v1

    .line 825287
    invoke-virtual {v0, v1}, LX/50M;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/51b;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->isEmpty()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/51b;->d()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 2

    .prologue
    .line 825280
    iget-object v0, p0, LX/51b;->b:LX/50M;

    .line 825281
    sget-object v1, LX/50M;->d:LX/50M;

    move-object v1, v1

    .line 825282
    invoke-virtual {v0, v1}, LX/50M;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 825283
    iget-object v0, p0, LX/51b;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->size()I

    move-result v0

    .line 825284
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/51b;->d()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0RZ;->b(Ljava/util/Iterator;)I

    move-result v0

    goto :goto_0
.end method

.method public final subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;
    .locals 2

    .prologue
    .line 825278
    check-cast p1, LX/4xM;

    check-cast p3, LX/4xM;

    .line 825279
    invoke-static {p2}, LX/4xG;->forBoolean(Z)LX/4xG;

    move-result-object v0

    invoke-static {p4}, LX/4xG;->forBoolean(Z)LX/4xG;

    move-result-object v1

    invoke-static {p1, v0, p3, v1}, LX/50M;->a(Ljava/lang/Comparable;LX/4xG;Ljava/lang/Comparable;LX/4xG;)LX/50M;

    move-result-object v0

    invoke-direct {p0, v0}, LX/51b;->a(LX/50M;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public final tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .locals 1

    .prologue
    .line 825276
    check-cast p1, LX/4xM;

    .line 825277
    invoke-static {p2}, LX/4xG;->forBoolean(Z)LX/4xG;

    move-result-object v0

    invoke-static {p1, v0}, LX/50M;->b(Ljava/lang/Comparable;LX/4xG;)LX/50M;

    move-result-object v0

    invoke-direct {p0, v0}, LX/51b;->a(LX/50M;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method
