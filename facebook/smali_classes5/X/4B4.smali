.class public abstract LX/4B4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QB;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/inject/ComponentProviderWithInjector",
        "<TT;>;",
        "LX/0QB;"
    }
.end annotation


# instance fields
.field public mInjector:LX/0QB;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 677440
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getApplicationInjector()LX/0QA;
    .locals 1

    .prologue
    .line 677429
    iget-object v0, p0, LX/4B4;->mInjector:LX/0QB;

    invoke-interface {v0}, LX/0QB;->getApplicationInjector()LX/0QA;

    move-result-object v0

    return-object v0
.end method

.method public getBinders()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/0Q4;",
            ">;",
            "Lcom/facebook/inject/Binder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 677430
    iget-object v0, p0, LX/4B4;->mInjector:LX/0QB;

    invoke-interface {v0}, LX/0QB;->getBinders()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getInjectorThreadStack()LX/0S7;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 677431
    iget-object v0, p0, LX/4B4;->mInjector:LX/0QB;

    invoke-interface {v0}, LX/0QB;->getInjectorThreadStack()LX/0S7;

    move-result-object v0

    return-object v0
.end method

.method public getInstance(LX/0RI;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TS;>;)TS;"
        }
    .end annotation

    .prologue
    .line 677427
    iget-object v0, p0, LX/4B4;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QC;->getInstance(LX/0RI;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getInstance(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;)TS;"
        }
    .end annotation

    .prologue
    .line 677432
    iget-object v0, p0, LX/4B4;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)TS;"
        }
    .end annotation

    .prologue
    .line 677433
    iget-object v0, p0, LX/4B4;->mInjector:LX/0QB;

    invoke-interface {v0, p1, p2}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getLazy(LX/0RI;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TS;>;)",
            "LX/0Ot",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 677434
    iget-object v0, p0, LX/4B4;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QC;->getLazy(LX/0RI;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public getLazy(Ljava/lang/Class;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;)",
            "LX/0Ot",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 677435
    iget-object v0, p0, LX/4B4;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public getLazy(Ljava/lang/Class;Ljava/lang/Class;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LX/0Ot",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 677436
    iget-object v0, p0, LX/4B4;->mInjector:LX/0QB;

    invoke-interface {v0, p1, p2}, LX/0QC;->getLazy(Ljava/lang/Class;Ljava/lang/Class;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public getLazySet(LX/0RI;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 677437
    iget-object v0, p0, LX/4B4;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QC;->getLazySet(LX/0RI;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public getLazySet(Ljava/lang/Class;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;)",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<TS;>;>;"
        }
    .end annotation

    .prologue
    .line 677438
    iget-object v0, p0, LX/4B4;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QC;->getLazySet(Ljava/lang/Class;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public getLazySet(Ljava/lang/Class;Ljava/lang/Class;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<TS;>;>;"
        }
    .end annotation

    .prologue
    .line 677439
    iget-object v0, p0, LX/4B4;->mInjector:LX/0QB;

    invoke-interface {v0, p1, p2}, LX/0QC;->getLazySet(Ljava/lang/Class;Ljava/lang/Class;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public getModuleInjector(Ljava/lang/Class;)LX/0QA;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/0Q4;",
            ">;)",
            "LX/0QA;"
        }
    .end annotation

    .prologue
    .line 677426
    iget-object v0, p0, LX/4B4;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QB;->getModuleInjector(Ljava/lang/Class;)LX/0QA;

    move-result-object v0

    return-object v0
.end method

.method public getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;>;)",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 677441
    iget-object v0, p0, LX/4B4;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    return-object v0
.end method

.method public getProcessIdentifier()I
    .locals 1

    .prologue
    .line 677428
    iget-object v0, p0, LX/4B4;->mInjector:LX/0QB;

    invoke-interface {v0}, LX/0QB;->getProcessIdentifier()I

    move-result v0

    return v0
.end method

.method public getProvider(LX/0RI;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TS;>;)",
            "LX/0Or",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 677420
    invoke-virtual {p0}, LX/4B4;->getScopeAwareInjectorInternal()LX/0QC;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0QC;->getProvider(LX/0RI;)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public getProvider(Ljava/lang/Class;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;)",
            "LX/0Or",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 677419
    invoke-virtual {p0}, LX/4B4;->getScopeAwareInjectorInternal()LX/0QC;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0QC;->getProvider(Ljava/lang/Class;)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public getProvider(Ljava/lang/Class;Ljava/lang/Class;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LX/0Or",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 677418
    invoke-virtual {p0}, LX/4B4;->getScopeAwareInjectorInternal()LX/0QC;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/0QC;->getProvider(Ljava/lang/Class;Ljava/lang/Class;)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public getScopeAwareInjector()LX/0R6;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 677417
    iget-object v0, p0, LX/4B4;->mInjector:LX/0QB;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v0

    return-object v0
.end method

.method public getScopeAwareInjectorInternal()LX/0QC;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 677416
    iget-object v0, p0, LX/4B4;->mInjector:LX/0QB;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v0

    return-object v0
.end method

.method public getScopeUnawareInjector()LX/0QD;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 677415
    iget-object v0, p0, LX/4B4;->mInjector:LX/0QB;

    invoke-interface {v0}, LX/0QB;->getScopeUnawareInjector()LX/0QD;

    move-result-object v0

    return-object v0
.end method

.method public getSet(LX/0RI;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TS;>;)",
            "Ljava/util/Set",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 677413
    iget-object v0, p0, LX/4B4;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QC;->getSet(LX/0RI;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getSet(Ljava/lang/Class;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;)",
            "Ljava/util/Set",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 677421
    iget-object v0, p0, LX/4B4;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QC;->getSet(Ljava/lang/Class;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getSet(Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "Ljava/util/Set",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 677422
    iget-object v0, p0, LX/4B4;->mInjector:LX/0QB;

    invoke-interface {v0, p1, p2}, LX/0QC;->getSet(Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getSetProvider(LX/0RI;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 677423
    invoke-virtual {p0}, LX/4B4;->getScopeAwareInjectorInternal()LX/0QC;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0QC;->getSetProvider(LX/0RI;)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public getSetProvider(Ljava/lang/Class;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;)",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<TS;>;>;"
        }
    .end annotation

    .prologue
    .line 677424
    invoke-virtual {p0}, LX/4B4;->getScopeAwareInjectorInternal()LX/0QC;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0QC;->getSetProvider(Ljava/lang/Class;)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public getSetProvider(Ljava/lang/Class;Ljava/lang/Class;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<TS;>;>;"
        }
    .end annotation

    .prologue
    .line 677414
    invoke-virtual {p0}, LX/4B4;->getScopeAwareInjectorInternal()LX/0QC;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/0QC;->getSetProvider(Ljava/lang/Class;Ljava/lang/Class;)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public hasBinding(Ljava/lang/Class;Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 677425
    iget-object v0, p0, LX/4B4;->mInjector:LX/0QB;

    invoke-interface {v0, p1, p2}, LX/0QC;->hasBinding(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method public abstract inject(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method
