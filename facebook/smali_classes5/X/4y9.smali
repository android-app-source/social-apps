.class public final LX/4y9;
.super LX/0Px;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Px",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final transient a:I

.field public final transient b:I

.field public final synthetic this$0:LX/0Px;


# direct methods
.method public constructor <init>(LX/0Px;II)V
    .locals 0

    .prologue
    .line 821619
    iput-object p1, p0, LX/4y9;->this$0:LX/0Px;

    invoke-direct {p0}, LX/0Px;-><init>()V

    .line 821620
    iput p2, p0, LX/4y9;->a:I

    .line 821621
    iput p3, p0, LX/4y9;->b:I

    .line 821622
    return-void
.end method


# virtual methods
.method public final get(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 821617
    iget v0, p0, LX/4y9;->b:I

    invoke-static {p1, v0}, LX/0PB;->checkElementIndex(II)I

    .line 821618
    iget-object v0, p0, LX/4y9;->this$0:LX/0Px;

    iget v1, p0, LX/4y9;->a:I

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final isPartialView()Z
    .locals 1

    .prologue
    .line 821616
    const/4 v0, 0x1

    return v0
.end method

.method public final bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 821615
    invoke-super {p0}, LX/0Px;->iterator()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic listIterator()Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 821614
    invoke-super {p0}, LX/0Px;->listIterator()LX/0Rb;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 821609
    invoke-super {p0, p1}, LX/0Px;->listIterator(I)LX/0Rb;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 821613
    iget v0, p0, LX/4y9;->b:I

    return v0
.end method

.method public final subList(II)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 821611
    iget v0, p0, LX/4y9;->b:I

    invoke-static {p1, p2, v0}, LX/0PB;->checkPositionIndexes(III)V

    .line 821612
    iget-object v0, p0, LX/4y9;->this$0:LX/0Px;

    iget v1, p0, LX/4y9;->a:I

    add-int/2addr v1, p1

    iget v2, p0, LX/4y9;->a:I

    add-int/2addr v2, p2

    invoke-virtual {v0, v1, v2}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic subList(II)Ljava/util/List;
    .locals 1

    .prologue
    .line 821610
    invoke-virtual {p0, p1, p2}, LX/4y9;->subList(II)LX/0Px;

    move-result-object v0

    return-object v0
.end method
