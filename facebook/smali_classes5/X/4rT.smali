.class public final LX/4rT;
.super LX/2B6;
.source ""


# instance fields
.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private final c:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 815866
    invoke-direct {p0}, LX/2B6;-><init>()V

    .line 815867
    iput-object p1, p0, LX/4rT;->a:Ljava/lang/Class;

    .line 815868
    iput-object p2, p0, LX/4rT;->c:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 815869
    iput-object p3, p0, LX/4rT;->b:Ljava/lang/Class;

    .line 815870
    iput-object p4, p0, LX/4rT;->d:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 815871
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)LX/2B6;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "LX/2B6;"
        }
    .end annotation

    .prologue
    .line 815872
    const/4 v0, 0x2

    new-array v0, v0, [LX/4rV;

    .line 815873
    const/4 v1, 0x0

    new-instance v2, LX/4rV;

    iget-object v3, p0, LX/4rT;->a:Ljava/lang/Class;

    iget-object v4, p0, LX/4rT;->c:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-direct {v2, v3, v4}, LX/4rV;-><init>(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    aput-object v2, v0, v1

    .line 815874
    const/4 v1, 0x1

    new-instance v2, LX/4rV;

    iget-object v3, p0, LX/4rT;->b:Ljava/lang/Class;

    iget-object v4, p0, LX/4rT;->d:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-direct {v2, v3, v4}, LX/4rV;-><init>(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    aput-object v2, v0, v1

    .line 815875
    new-instance v1, LX/4rU;

    invoke-direct {v1, v0}, LX/4rU;-><init>([LX/4rV;)V

    return-object v1
.end method

.method public final a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 815876
    iget-object v0, p0, LX/4rT;->a:Ljava/lang/Class;

    if-ne p1, v0, :cond_0

    .line 815877
    iget-object v0, p0, LX/4rT;->c:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 815878
    :goto_0
    return-object v0

    .line 815879
    :cond_0
    iget-object v0, p0, LX/4rT;->b:Ljava/lang/Class;

    if-ne p1, v0, :cond_1

    .line 815880
    iget-object v0, p0, LX/4rT;->d:Lcom/fasterxml/jackson/databind/JsonSerializer;

    goto :goto_0

    .line 815881
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
