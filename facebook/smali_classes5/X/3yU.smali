.class public LX/3yU;
.super LX/1SG;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1SG",
        "<",
        "LX/0cb;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3yU;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0cb;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0cb;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 661087
    invoke-direct {p0}, LX/1SG;-><init>()V

    .line 661088
    new-instance v0, LX/3yT;

    invoke-direct {v0, p0, p1}, LX/3yT;-><init>(LX/3yU;LX/0Ot;)V

    iput-object v0, p0, LX/3yU;->a:LX/0Ot;

    .line 661089
    return-void
.end method

.method public static a(LX/0QB;)LX/3yU;
    .locals 4

    .prologue
    .line 661093
    sget-object v0, LX/3yU;->b:LX/3yU;

    if-nez v0, :cond_1

    .line 661094
    const-class v1, LX/3yU;

    monitor-enter v1

    .line 661095
    :try_start_0
    sget-object v0, LX/3yU;->b:LX/3yU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 661096
    if-eqz v2, :cond_0

    .line 661097
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 661098
    new-instance v3, LX/3yU;

    const/16 p0, 0x48

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/3yU;-><init>(LX/0Ot;)V

    .line 661099
    move-object v0, v3

    .line 661100
    sput-object v0, LX/3yU;->b:LX/3yU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 661101
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 661102
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 661103
    :cond_1
    sget-object v0, LX/3yU;->b:LX/3yU;

    return-object v0

    .line 661104
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 661105
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final synthetic a()Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 661092
    invoke-virtual {p0}, LX/3yU;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0cb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 661091
    iget-object v0, p0, LX/3yU;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 661090
    invoke-virtual {p0}, LX/3yU;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
