.class public LX/41Q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/auth/login/CheckApprovedMachineParams;",
        "Lcom/facebook/auth/login/CheckApprovedMachineMethod$Result;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 666251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 666252
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 666253
    check-cast p1, Lcom/facebook/auth/login/CheckApprovedMachineParams;

    .line 666254
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 666255
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "u"

    .line 666256
    iget-wide v6, p1, Lcom/facebook/auth/login/CheckApprovedMachineParams;->a:J

    move-wide v4, v6

    .line 666257
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 666258
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "m"

    .line 666259
    iget-object v3, p1, Lcom/facebook/auth/login/CheckApprovedMachineParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 666260
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 666261
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "checkApprovedMachine"

    .line 666262
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 666263
    move-object v1, v1

    .line 666264
    const-string v2, "GET"

    .line 666265
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 666266
    move-object v1, v1

    .line 666267
    const-string v2, "check_approved_machine"

    .line 666268
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 666269
    move-object v1, v1

    .line 666270
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 666271
    move-object v0, v1

    .line 666272
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 666273
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 666274
    move-object v0, v0

    .line 666275
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 666276
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 666277
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    const-class v1, Lcom/facebook/auth/login/CheckApprovedMachineMethod$Result;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/login/CheckApprovedMachineMethod$Result;

    return-object v0
.end method
