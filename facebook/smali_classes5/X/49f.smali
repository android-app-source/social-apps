.class public final LX/49f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/4hA;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/2Dr;


# direct methods
.method public constructor <init>(LX/2Dr;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 674623
    iput-object p1, p0, LX/49f;->b:LX/2Dr;

    iput-object p2, p0, LX/49f;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 674609
    iget-object v0, p0, LX/49f;->b:LX/2Dr;

    iget-object v0, v0, LX/2Dr;->e:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 674610
    iget-object v0, p0, LX/49f;->b:LX/2Dr;

    iget-object v0, v0, LX/2Dr;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27v;

    invoke-virtual {v0}, LX/27v;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 674611
    const/4 v0, 0x0

    .line 674612
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/49f;->b:LX/2Dr;

    iget-object v1, p0, LX/49f;->a:Ljava/lang/String;

    .line 674613
    iget-object v2, v0, LX/2Dr;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/27v;

    invoke-virtual {v2}, LX/27v;->f()LX/4hA;

    move-result-object v2

    .line 674614
    if-eqz v2, :cond_2

    .line 674615
    :cond_1
    :goto_1
    move-object v0, v2

    .line 674616
    goto :goto_0

    .line 674617
    :cond_2
    iget-object v4, v0, LX/2Dr;->c:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/280;

    invoke-virtual {v4}, LX/280;->a()V

    .line 674618
    iget-object v4, v0, LX/2Dr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    sget-object v5, LX/0dH;->f:LX/0Tn;

    iget-object v6, v0, LX/2Dr;->h:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    invoke-interface {v4, v5, v6, v7}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v4

    invoke-interface {v4}, LX/0hN;->commit()V

    .line 674619
    iget-object v2, v0, LX/2Dr;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/27v;

    invoke-virtual {v2}, LX/27v;->f()LX/4hA;

    move-result-object v2

    .line 674620
    if-nez v2, :cond_1

    .line 674621
    new-instance v4, LX/4hA;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, LX/2Dr;->h:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    iget-object v8, v0, LX/2Dr;->g:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    move-object v9, v1

    invoke-direct/range {v4 .. v9}, LX/4hA;-><init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    move-object v3, v4

    .line 674622
    iget-object v2, v0, LX/2Dr;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/27v;

    invoke-virtual {v2, v3}, LX/27v;->b(LX/4hA;)V

    move-object v2, v3

    goto :goto_1
.end method
