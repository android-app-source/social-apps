.class public LX/3li;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2d7;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/3li;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1SP;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3lk;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2d8;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1SP;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3lk;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2d8;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 634847
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 634848
    iput-object p1, p0, LX/3li;->a:LX/0Or;

    .line 634849
    iput-object p2, p0, LX/3li;->b:LX/0Ot;

    .line 634850
    iput-object p3, p0, LX/3li;->c:LX/0Ot;

    .line 634851
    return-void
.end method

.method public static a(LX/0QB;)LX/3li;
    .locals 6

    .prologue
    .line 634852
    sget-object v0, LX/3li;->d:LX/3li;

    if-nez v0, :cond_1

    .line 634853
    const-class v1, LX/3li;

    monitor-enter v1

    .line 634854
    :try_start_0
    sget-object v0, LX/3li;->d:LX/3li;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 634855
    if-eqz v2, :cond_0

    .line 634856
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 634857
    new-instance v3, LX/3li;

    const/16 v4, 0xf5d

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0xf62

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0xf66

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, LX/3li;-><init>(LX/0Or;LX/0Ot;LX/0Ot;)V

    .line 634858
    move-object v0, v3

    .line 634859
    sput-object v0, LX/3li;->d:LX/3li;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 634860
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 634861
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 634862
    :cond_1
    sget-object v0, LX/3li;->d:LX/3li;

    return-object v0

    .line 634863
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 634864
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/3li;Z)V
    .locals 2

    .prologue
    .line 634865
    if-nez p1, :cond_0

    .line 634866
    iget-object v0, p0, LX/3li;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3lk;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/3lk;->a(Z)V

    .line 634867
    :cond_0
    iget-object v0, p0, LX/3li;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3lk;

    .line 634868
    invoke-static {p1}, LX/3lk;->c(Z)I

    move-result v1

    iget-object p0, v0, LX/3lk;->c:Landroid/content/ComponentName;

    invoke-static {v0, v1, p0}, LX/3lk;->a(LX/3lk;ILandroid/content/ComponentName;)V

    .line 634869
    return-void
.end method

.method public static c(LX/3li;)V
    .locals 3

    .prologue
    .line 634870
    const/4 v0, 0x2

    new-array v1, v0, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x0

    iget-object v0, p0, LX/3li;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    iget-object v0, p0, LX/3li;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    aput-object v0, v1, v2

    invoke-static {v1}, LX/0Vg;->a([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/3lj;

    invoke-direct {v1, p0}, LX/3lj;-><init>(LX/3li;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 634871
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)V
    .locals 0

    .prologue
    .line 634872
    invoke-static {p0}, LX/3li;->c(LX/3li;)V

    .line 634873
    return-void
.end method
