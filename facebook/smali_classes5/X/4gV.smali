.class public LX/4gV;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final c:LX/0lC;


# instance fields
.field public a:Ljava/util/Map;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/4gU;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 799744
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v0

    sput-object v0, LX/4gV;->c:LX/0lC;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 799702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799703
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/4gV;->a:Ljava/util/Map;

    return-void
.end method

.method public static a(LX/0m9;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 799734
    if-eqz p1, :cond_0

    .line 799735
    :try_start_0
    sget-object v0, LX/4gV;->c:LX/0lC;

    invoke-virtual {v0, p1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 799736
    if-nez v0, :cond_1

    .line 799737
    const-string v0, "metadata"

    invoke-virtual {p0, v0, p1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 799738
    :cond_0
    :goto_0
    return-void

    .line 799739
    :cond_1
    const-string v1, "metadata"

    invoke-virtual {p0, v1, v0}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;
    :try_end_0
    .catch LX/2aQ; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 799740
    :catch_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v3

    .line 799741
    const-string v0, "metadata"

    invoke-virtual {p0, v0, p1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_0

    .line 799742
    :catch_1
    move-exception v0

    .line 799743
    const-string v1, "HistogramMap.class"

    const-string v2, "IO Exception in readTree()."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static declared-synchronized a(LX/4gV;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 799722
    monitor-enter p0

    if-eqz p2, :cond_2

    .line 799723
    :try_start_0
    iget-object v0, p0, LX/4gV;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 799724
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 799725
    if-eqz v0, :cond_0

    .line 799726
    new-instance v0, Ljava/security/InvalidParameterException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No histogram found with ID:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 799727
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 799728
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/4gV;->a:Ljava/util/Map;

    new-instance v1, LX/4gU;

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-direct {v1, v2, v3}, LX/4gU;-><init>(J)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 799729
    :cond_1
    monitor-exit p0

    return-void

    .line 799730
    :cond_2
    iget-object v0, p0, LX/4gV;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 799731
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 799732
    if-eqz v0, :cond_1

    .line 799733
    new-instance v0, Ljava/security/InvalidParameterException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Histogram already present with ID:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(I)LX/0lF;
    .locals 4

    .prologue
    .line 799717
    monitor-enter p0

    :try_start_0
    new-instance v2, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 799718
    iget-object v0, p0, LX/4gV;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 799719
    iget-object v1, p0, LX/4gV;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4gU;

    invoke-virtual {v1, p1}, LX/4gU;->a(I)LX/0lF;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 799720
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 799721
    :cond_0
    monitor-exit p0

    return-object v2
.end method

.method public final declared-synchronized a(Ljava/util/Map;)LX/0lF;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)",
            "LX/0lF;"
        }
    .end annotation

    .prologue
    .line 799712
    monitor-enter p0

    :try_start_0
    new-instance v3, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 799713
    iget-object v0, p0, LX/4gV;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 799714
    iget-object v1, p0, LX/4gV;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4gU;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, LX/4gU;->a(I)LX/0lF;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 799715
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 799716
    :cond_0
    monitor-exit p0

    return-object v3
.end method

.method public final declared-synchronized a(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 799708
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0, p1, v0}, LX/4gV;->a(LX/4gV;Ljava/lang/String;Z)V

    .line 799709
    iget-object v0, p0, LX/4gV;->a:Ljava/util/Map;

    new-instance v1, LX/4gU;

    invoke-direct {v1, p2, p3}, LX/4gU;-><init>(J)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 799710
    monitor-exit p0

    return-void

    .line 799711
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;JJ)V
    .locals 2

    .prologue
    .line 799704
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0, p1, v0}, LX/4gV;->a(LX/4gV;Ljava/lang/String;Z)V

    .line 799705
    iget-object v0, p0, LX/4gV;->a:Ljava/util/Map;

    new-instance v1, LX/4gU;

    invoke-direct {v1, p2, p3, p4, p5}, LX/4gU;-><init>(JJ)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 799706
    monitor-exit p0

    return-void

    .line 799707
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()LX/4gV;
    .locals 3

    .prologue
    .line 799696
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/4gV;

    invoke-direct {v0}, LX/4gV;-><init>()V

    .line 799697
    iget-object v1, v0, LX/4gV;->a:Ljava/util/Map;

    .line 799698
    iget-object v2, p0, LX/4gV;->a:Ljava/util/Map;

    iput-object v2, v0, LX/4gV;->a:Ljava/util/Map;

    .line 799699
    iput-object v1, p0, LX/4gV;->a:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 799700
    monitor-exit p0

    return-object v0

    .line 799701
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
