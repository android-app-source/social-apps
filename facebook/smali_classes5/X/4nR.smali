.class public final enum LX/4nR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4nR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4nR;

.field public static final enum SNACKBAR:LX/4nR;

.field public static final enum STANDARD:LX/4nR;


# instance fields
.field private final mHeight:I

.field private final mWidth:I

.field private final mYOffsetResId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    const/4 v4, -0x2

    .line 806801
    new-instance v0, LX/4nR;

    const-string v1, "STANDARD"

    const v3, 0x7f0b0284

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/4nR;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, LX/4nR;->STANDARD:LX/4nR;

    .line 806802
    new-instance v5, LX/4nR;

    const-string v6, "SNACKBAR"

    const v8, 0x7f0b0285

    const/4 v9, -0x1

    move v10, v4

    invoke-direct/range {v5 .. v10}, LX/4nR;-><init>(Ljava/lang/String;IIII)V

    sput-object v5, LX/4nR;->SNACKBAR:LX/4nR;

    .line 806803
    const/4 v0, 0x2

    new-array v0, v0, [LX/4nR;

    sget-object v1, LX/4nR;->STANDARD:LX/4nR;

    aput-object v1, v0, v2

    sget-object v1, LX/4nR;->SNACKBAR:LX/4nR;

    aput-object v1, v0, v7

    sput-object v0, LX/4nR;->$VALUES:[LX/4nR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)V"
        }
    .end annotation

    .prologue
    .line 806796
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 806797
    iput p3, p0, LX/4nR;->mYOffsetResId:I

    .line 806798
    iput p4, p0, LX/4nR;->mWidth:I

    .line 806799
    iput p5, p0, LX/4nR;->mHeight:I

    .line 806800
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4nR;
    .locals 1

    .prologue
    .line 806791
    const-class v0, LX/4nR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4nR;

    return-object v0
.end method

.method public static values()[LX/4nR;
    .locals 1

    .prologue
    .line 806795
    sget-object v0, LX/4nR;->$VALUES:[LX/4nR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4nR;

    return-object v0
.end method


# virtual methods
.method public final getHeight()I
    .locals 1

    .prologue
    .line 806794
    iget v0, p0, LX/4nR;->mHeight:I

    return v0
.end method

.method public final getWidth()I
    .locals 1

    .prologue
    .line 806793
    iget v0, p0, LX/4nR;->mWidth:I

    return v0
.end method

.method public final getYOffsetResId()I
    .locals 1

    .prologue
    .line 806792
    iget v0, p0, LX/4nR;->mYOffsetResId:I

    return v0
.end method
