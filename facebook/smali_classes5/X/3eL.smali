.class public LX/3eL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile g:LX/3eL;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:Ljava/lang/Boolean;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/8m8;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 619634
    const-class v0, LX/3eL;

    sput-object v0, LX/3eL;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Boolean;LX/0Or;LX/0Or;)V
    .locals 0
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/stickers/service/IsWebpInStickerEnabled;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/stickers/service/IsFbaStickersEnabled;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/stickers/abtest/IsSmallerPreviewEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Boolean;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 619628
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 619629
    iput-object p1, p0, LX/3eL;->b:Landroid/content/Context;

    .line 619630
    iput-object p2, p0, LX/3eL;->c:Ljava/lang/Boolean;

    .line 619631
    iput-object p3, p0, LX/3eL;->d:LX/0Or;

    .line 619632
    iput-object p4, p0, LX/3eL;->e:LX/0Or;

    .line 619633
    return-void
.end method

.method public static a(LX/0QB;)LX/3eL;
    .locals 7

    .prologue
    .line 619613
    sget-object v0, LX/3eL;->g:LX/3eL;

    if-nez v0, :cond_1

    .line 619614
    const-class v1, LX/3eL;

    monitor-enter v1

    .line 619615
    :try_start_0
    sget-object v0, LX/3eL;->g:LX/3eL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 619616
    if-eqz v2, :cond_0

    .line 619617
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 619618
    new-instance v5, LX/3eL;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    .line 619619
    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    const/16 v6, 0x563

    const/4 p0, 0x0

    invoke-virtual {v4, v6, p0}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object v4, v4

    .line 619620
    check-cast v4, Ljava/lang/Boolean;

    const/16 v6, 0x365

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 p0, 0x1573

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v3, v4, v6, p0}, LX/3eL;-><init>(Landroid/content/Context;Ljava/lang/Boolean;LX/0Or;LX/0Or;)V

    .line 619621
    move-object v0, v5

    .line 619622
    sput-object v0, LX/3eL;->g:LX/3eL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 619623
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 619624
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 619625
    :cond_1
    sget-object v0, LX/3eL;->g:LX/3eL;

    return-object v0

    .line 619626
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 619627
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerImageModel;)Landroid/net/Uri;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 619428
    if-nez p0, :cond_1

    .line 619429
    :cond_0
    :goto_0
    return-object v0

    .line 619430
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerImageModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 619431
    if-eqz v1, :cond_0

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsModel;)Lcom/facebook/stickers/model/Sticker;
    .locals 3

    .prologue
    .line 619589
    invoke-static {}, LX/4m0;->newBuilder()LX/4m0;

    move-result-object v0

    .line 619590
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsModel;->l()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsModel$PackModel;

    move-result-object v1

    .line 619591
    if-nez v1, :cond_0

    .line 619592
    new-instance v0, LX/8m7;

    const-string v1, "node pack is missing"

    invoke-direct {v0, v1}, LX/8m7;-><init>(Ljava/lang/String;)V

    throw v0

    .line 619593
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 619594
    iput-object v2, v0, LX/4m0;->a:Ljava/lang/String;

    .line 619595
    move-object v0, v0

    .line 619596
    invoke-virtual {v1}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsModel$PackModel;->j()Ljava/lang/String;

    move-result-object v2

    .line 619597
    iput-object v2, v0, LX/4m0;->b:Ljava/lang/String;

    .line 619598
    move-object v0, v0

    .line 619599
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsModel;->m()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerImageModel;

    move-result-object v2

    invoke-static {v2}, LX/3eL;->a(Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerImageModel;)Landroid/net/Uri;

    move-result-object v2

    .line 619600
    iput-object v2, v0, LX/4m0;->c:Landroid/net/Uri;

    .line 619601
    move-object v0, v0

    .line 619602
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsModel;->j()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerImageModel;

    move-result-object v2

    invoke-static {v2}, LX/3eL;->a(Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerImageModel;)Landroid/net/Uri;

    move-result-object v2

    .line 619603
    iput-object v2, v0, LX/4m0;->e:Landroid/net/Uri;

    .line 619604
    move-object v0, v0

    .line 619605
    const/4 v2, 0x0

    .line 619606
    iput-object v2, v0, LX/4m0;->g:Landroid/net/Uri;

    .line 619607
    move-object v0, v0

    .line 619608
    invoke-static {v1}, LX/3eL;->a(Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsModel$PackModel;)Lcom/facebook/stickers/model/StickerCapabilities;

    move-result-object v1

    .line 619609
    iput-object v1, v0, LX/4m0;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    .line 619610
    move-object v0, v0

    .line 619611
    invoke-virtual {v0}, LX/4m0;->a()Lcom/facebook/stickers/model/Sticker;

    move-result-object v0

    .line 619612
    return-object v0
.end method

.method public static a(Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsWithPreviewModel;)Lcom/facebook/stickers/model/Sticker;
    .locals 3

    .prologue
    .line 619565
    invoke-static {}, LX/4m0;->newBuilder()LX/4m0;

    move-result-object v0

    .line 619566
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsWithPreviewModel;->l()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsModel$PackModel;

    move-result-object v1

    .line 619567
    if-nez v1, :cond_0

    .line 619568
    new-instance v0, LX/8m7;

    const-string v1, "node pack is missing"

    invoke-direct {v0, v1}, LX/8m7;-><init>(Ljava/lang/String;)V

    throw v0

    .line 619569
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsWithPreviewModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 619570
    iput-object v2, v0, LX/4m0;->a:Ljava/lang/String;

    .line 619571
    move-object v0, v0

    .line 619572
    invoke-virtual {v1}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsModel$PackModel;->j()Ljava/lang/String;

    move-result-object v2

    .line 619573
    iput-object v2, v0, LX/4m0;->b:Ljava/lang/String;

    .line 619574
    move-object v0, v0

    .line 619575
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsWithPreviewModel;->n()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerImageModel;

    move-result-object v2

    invoke-static {v2}, LX/3eL;->a(Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerImageModel;)Landroid/net/Uri;

    move-result-object v2

    .line 619576
    iput-object v2, v0, LX/4m0;->c:Landroid/net/Uri;

    .line 619577
    move-object v0, v0

    .line 619578
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsWithPreviewModel;->j()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerImageModel;

    move-result-object v2

    invoke-static {v2}, LX/3eL;->a(Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerImageModel;)Landroid/net/Uri;

    move-result-object v2

    .line 619579
    iput-object v2, v0, LX/4m0;->e:Landroid/net/Uri;

    .line 619580
    move-object v0, v0

    .line 619581
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsWithPreviewModel;->m()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerImageModel;

    move-result-object v2

    invoke-static {v2}, LX/3eL;->a(Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerImageModel;)Landroid/net/Uri;

    move-result-object v2

    .line 619582
    iput-object v2, v0, LX/4m0;->g:Landroid/net/Uri;

    .line 619583
    move-object v0, v0

    .line 619584
    invoke-static {v1}, LX/3eL;->a(Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsModel$PackModel;)Lcom/facebook/stickers/model/StickerCapabilities;

    move-result-object v1

    .line 619585
    iput-object v1, v0, LX/4m0;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    .line 619586
    move-object v0, v0

    .line 619587
    invoke-virtual {v0}, LX/4m0;->a()Lcom/facebook/stickers/model/Sticker;

    move-result-object v0

    .line 619588
    return-object v0
.end method

.method private static a(Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsModel$PackModel;)Lcom/facebook/stickers/model/StickerCapabilities;
    .locals 2

    .prologue
    .line 619546
    invoke-static {}, Lcom/facebook/stickers/model/StickerCapabilities;->newBuilder()LX/4m3;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsModel$PackModel;->k()Z

    move-result v1

    invoke-static {v1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v1

    .line 619547
    iput-object v1, v0, LX/4m3;->a:LX/03R;

    .line 619548
    move-object v0, v0

    .line 619549
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsModel$PackModel;->l()Z

    move-result v1

    invoke-static {v1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v1

    .line 619550
    iput-object v1, v0, LX/4m3;->b:LX/03R;

    .line 619551
    move-object v0, v0

    .line 619552
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsModel$PackModel;->m()Z

    move-result v1

    invoke-static {v1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v1

    .line 619553
    iput-object v1, v0, LX/4m3;->c:LX/03R;

    .line 619554
    move-object v0, v0

    .line 619555
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsModel$PackModel;->p()Z

    move-result v1

    invoke-static {v1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v1

    .line 619556
    iput-object v1, v0, LX/4m3;->d:LX/03R;

    .line 619557
    move-object v0, v0

    .line 619558
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsModel$PackModel;->o()Z

    move-result v1

    invoke-static {v1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v1

    .line 619559
    iput-object v1, v0, LX/4m3;->e:LX/03R;

    .line 619560
    move-object v0, v0

    .line 619561
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsModel$PackModel;->n()Z

    move-result v1

    invoke-static {v1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v1

    .line 619562
    iput-object v1, v0, LX/4m3;->f:LX/03R;

    .line 619563
    move-object v0, v0

    .line 619564
    invoke-virtual {v0}, LX/4m3;->a()Lcom/facebook/stickers/model/StickerCapabilities;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;)Lcom/facebook/stickers/model/StickerPack;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 619468
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->D()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_0

    .line 619469
    new-instance v0, LX/8m7;

    const-string v1, "node tray_button is missing"

    invoke-direct {v0, v1}, LX/8m7;-><init>(Ljava/lang/String;)V

    throw v0

    .line 619470
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->D()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 619471
    new-instance v3, LX/4m6;

    invoke-direct {v3}, LX/4m6;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->n()Ljava/lang/String;

    move-result-object v4

    .line 619472
    iput-object v4, v3, LX/4m6;->a:Ljava/lang/String;

    .line 619473
    move-object v3, v3

    .line 619474
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->y()Ljava/lang/String;

    move-result-object v4

    .line 619475
    iput-object v4, v3, LX/4m6;->b:Ljava/lang/String;

    .line 619476
    move-object v3, v3

    .line 619477
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->j()Ljava/lang/String;

    move-result-object v4

    .line 619478
    iput-object v4, v3, LX/4m6;->c:Ljava/lang/String;

    .line 619479
    move-object v3, v3

    .line 619480
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->m()Ljava/lang/String;

    move-result-object v4

    .line 619481
    iput-object v4, v3, LX/4m6;->d:Ljava/lang/String;

    .line 619482
    move-object v3, v3

    .line 619483
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->C()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerImageModel;

    move-result-object v4

    invoke-static {v4}, LX/3eL;->b(Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerImageModel;)Landroid/net/Uri;

    move-result-object v4

    .line 619484
    iput-object v4, v3, LX/4m6;->e:Landroid/net/Uri;

    .line 619485
    move-object v3, v3

    .line 619486
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->z()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerImageModel;

    move-result-object v4

    invoke-static {v4}, LX/3eL;->b(Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerImageModel;)Landroid/net/Uri;

    move-result-object v4

    .line 619487
    iput-object v4, v3, LX/4m6;->g:Landroid/net/Uri;

    .line 619488
    move-object v3, v3

    .line 619489
    const-class v4, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerImageModel;

    invoke-virtual {v2, v0, v1, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerImageModel;

    invoke-static {v0}, LX/3eL;->b(Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerImageModel;)Landroid/net/Uri;

    move-result-object v0

    .line 619490
    iput-object v0, v3, LX/4m6;->h:Landroid/net/Uri;

    .line 619491
    move-object v0, v3

    .line 619492
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->A()I

    move-result v2

    .line 619493
    iput v2, v0, LX/4m6;->i:I

    .line 619494
    move-object v0, v0

    .line 619495
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->E()J

    move-result-wide v2

    .line 619496
    iput-wide v2, v0, LX/4m6;->j:J

    .line 619497
    move-object v0, v0

    .line 619498
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->p()Z

    move-result v2

    .line 619499
    iput-boolean v2, v0, LX/4m6;->l:Z

    .line 619500
    move-object v0, v0

    .line 619501
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->s()Z

    move-result v2

    .line 619502
    iput-boolean v2, v0, LX/4m6;->m:Z

    .line 619503
    move-object v0, v0

    .line 619504
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->w()Z

    move-result v2

    .line 619505
    iput-boolean v2, v0, LX/4m6;->n:Z

    .line 619506
    move-object v0, v0

    .line 619507
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->o()Z

    move-result v2

    .line 619508
    iput-boolean v2, v0, LX/4m6;->o:Z

    .line 619509
    move-object v0, v0

    .line 619510
    invoke-static {}, Lcom/facebook/stickers/model/StickerCapabilities;->newBuilder()LX/4m3;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->q()Z

    move-result v3

    invoke-static {v3}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v3

    .line 619511
    iput-object v3, v2, LX/4m3;->a:LX/03R;

    .line 619512
    move-object v2, v2

    .line 619513
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->r()Z

    move-result v3

    invoke-static {v3}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v3

    .line 619514
    iput-object v3, v2, LX/4m3;->b:LX/03R;

    .line 619515
    move-object v2, v2

    .line 619516
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->t()Z

    move-result v3

    invoke-static {v3}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v3

    .line 619517
    iput-object v3, v2, LX/4m3;->c:LX/03R;

    .line 619518
    move-object v2, v2

    .line 619519
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->x()Z

    move-result v3

    invoke-static {v3}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v3

    .line 619520
    iput-object v3, v2, LX/4m3;->d:LX/03R;

    .line 619521
    move-object v2, v2

    .line 619522
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->v()Z

    move-result v3

    invoke-static {v3}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v3

    .line 619523
    iput-object v3, v2, LX/4m3;->e:LX/03R;

    .line 619524
    move-object v2, v2

    .line 619525
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->u()Z

    move-result v3

    invoke-static {v3}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v3

    .line 619526
    iput-object v3, v2, LX/4m3;->f:LX/03R;

    .line 619527
    move-object v2, v2

    .line 619528
    invoke-virtual {v2}, LX/4m3;->a()Lcom/facebook/stickers/model/StickerCapabilities;

    move-result-object v2

    move-object v2, v2

    .line 619529
    iput-object v2, v0, LX/4m6;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    .line 619530
    move-object v0, v0

    .line 619531
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->l()LX/0Px;

    move-result-object v2

    .line 619532
    iput-object v2, v0, LX/4m6;->p:Ljava/util/List;

    .line 619533
    move-object v3, v0

    .line 619534
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 619535
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->B()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->B()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v5, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel$StickersModel$NodesModel;

    invoke-virtual {v2, v0, v1, v5}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_5

    .line 619536
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->B()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v5, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel$StickersModel$NodesModel;

    invoke-virtual {v2, v0, v1, v5}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v2, v0

    :goto_2
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    :goto_3
    if-ge v1, v5, :cond_5

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel$StickersModel$NodesModel;

    .line 619537
    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel$StickersModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 619538
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 619539
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 619540
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1

    .line 619541
    :cond_4
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 619542
    move-object v2, v0

    goto :goto_2

    .line 619543
    :cond_5
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 619544
    iput-object v0, v3, LX/4m6;->q:Ljava/util/List;

    .line 619545
    invoke-virtual {v3}, LX/4m6;->s()Lcom/facebook/stickers/model/StickerPack;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/3eL;LX/0gW;)V
    .locals 5

    .prologue
    .line 619449
    const-string v0, "preview_size"

    .line 619450
    iget-object v1, p0, LX/3eL;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 619451
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 619452
    sget-object v3, LX/4m4;->MESSENGER:LX/4m4;

    invoke-static {v3}, LX/8kW;->a(LX/4m4;)LX/7kb;

    move-result-object v3

    .line 619453
    new-instance v4, LX/7kd;

    invoke-direct {v4, v1, v3}, LX/7kd;-><init>(Landroid/content/res/Resources;LX/7kb;)V

    const v3, 0x7f0b025b

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int v1, v2, v1

    const/4 v3, 0x0

    invoke-virtual {v4, v2, v1, v3}, LX/7kd;->a(IIZ)LX/7kc;

    move-result-object v3

    .line 619454
    iget v1, v3, LX/7kc;->i:I

    move v2, v1

    .line 619455
    iget-object v1, p0, LX/3eL;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 619456
    iget v1, v3, LX/7kc;->c:I

    move v1, v1

    .line 619457
    sub-int v1, v2, v1

    .line 619458
    iget v2, v3, LX/7kc;->d:I

    move v2, v2

    .line 619459
    sub-int/2addr v1, v2

    .line 619460
    :goto_0
    move v1, v1

    .line 619461
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 619462
    iget-object v0, p0, LX/3eL;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 619463
    const-string v0, "animated_media_type"

    const-string v1, "image/x-fba"

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 619464
    :goto_1
    const-string v0, "media_type"

    invoke-virtual {p0}, LX/3eL;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 619465
    const-string v0, "scaling_factor"

    invoke-virtual {p0}, LX/3eL;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 619466
    return-void

    .line 619467
    :cond_0
    const-string v0, "animated_media_type"

    const-string v1, "image/webp"

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    goto :goto_1

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method private static b(Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerImageModel;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 619446
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerImageModel;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 619447
    :cond_0
    new-instance v0, LX/8m7;

    const-string v1, "node uri is missing"

    invoke-direct {v0, v1}, LX/8m7;-><init>(Ljava/lang/String;)V

    throw v0

    .line 619448
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 619443
    iget-object v0, p0, LX/3eL;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 619444
    const-string v0, "image/webp"

    .line 619445
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "image/png"

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 14

    .prologue
    const/4 v0, 0x0

    .line 619432
    iget-object v1, p0, LX/3eL;->f:LX/8m8;

    if-nez v1, :cond_1

    .line 619433
    invoke-static {}, LX/8m8;->values()[LX/8m8;

    move-result-object v1

    aget-object v1, v1, v0

    .line 619434
    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 619435
    iget-object v4, p0, LX/3eL;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    .line 619436
    invoke-static {}, LX/8m8;->values()[LX/8m8;

    move-result-object v6

    array-length v7, v6

    move v4, v0

    move-wide v12, v2

    move-object v3, v1

    move-wide v0, v12

    :goto_0
    if-ge v4, v7, :cond_0

    aget-object v2, v6, v4

    .line 619437
    iget v8, v2, LX/8m8;->numericValue:F

    iget v9, v5, Landroid/util/DisplayMetrics;->density:F

    sub-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    .line 619438
    float-to-double v10, v8

    cmpg-double v9, v10, v0

    if-gez v9, :cond_2

    .line 619439
    float-to-double v0, v8

    .line 619440
    :goto_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move-object v3, v2

    goto :goto_0

    .line 619441
    :cond_0
    iput-object v3, p0, LX/3eL;->f:LX/8m8;

    .line 619442
    :cond_1
    iget-object v0, p0, LX/3eL;->f:LX/8m8;

    iget-object v0, v0, LX/8m8;->stringValue:Ljava/lang/String;

    return-object v0

    :cond_2
    move-object v2, v3

    goto :goto_1
.end method
