.class public LX/3iS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/3iT;

.field private final c:LX/3iM;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/3iT;LX/3iM;)V
    .locals 0
    .param p1    # Landroid/content/res/Resources;
        .annotation build Lcom/facebook/inject/NeedsApplicationInjector;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 629480
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 629481
    iput-object p1, p0, LX/3iS;->a:Landroid/content/res/Resources;

    .line 629482
    iput-object p2, p0, LX/3iS;->b:LX/3iT;

    .line 629483
    iput-object p3, p0, LX/3iS;->c:LX/3iM;

    .line 629484
    return-void
.end method

.method public static a(LX/0QB;)LX/3iS;
    .locals 1

    .prologue
    .line 629485
    invoke-static {p0}, LX/3iS;->b(LX/0QB;)LX/3iS;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3iS;
    .locals 4

    .prologue
    .line 629486
    new-instance v3, LX/3iS;

    invoke-interface {p0}, LX/0QB;->getApplicationInjector()LX/0QA;

    move-result-object v0

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {p0}, LX/3iT;->b(LX/0QB;)LX/3iT;

    move-result-object v1

    check-cast v1, LX/3iT;

    invoke-static {p0}, LX/3iM;->a(LX/0QB;)LX/3iM;

    move-result-object v2

    check-cast v2, LX/3iM;

    invoke-direct {v3, v0, v1, v2}, LX/3iS;-><init>(Landroid/content/res/Resources;LX/3iT;LX/3iM;)V

    .line 629487
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)Lcom/facebook/graphql/model/GraphQLComment;
    .locals 7

    .prologue
    .line 629488
    iget-object v0, p3, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->c:Ljava/lang/String;

    iget-object v1, p0, LX/3iS;->b:LX/3iT;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/8of;->a(Ljava/lang/CharSequence;LX/3iT;LX/8oi;)LX/8of;

    move-result-object v0

    .line 629489
    invoke-virtual {v0}, LX/8of;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, LX/8oj;->a(Landroid/text/Editable;)Ljava/util/List;

    move-result-object v2

    iget-object v4, p3, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->f:Lcom/facebook/ipc/media/MediaItem;

    iget-object v5, p3, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->g:Lcom/facebook/ipc/media/StickerItem;

    iget-object v6, p0, LX/3iS;->c:LX/3iM;

    move-object v0, p2

    move-object v3, p1

    invoke-static/range {v0 .. v6}, LX/8sE;->a(Lcom/facebook/graphql/model/GraphQLActor;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/facebook/ipc/media/MediaItem;Lcom/facebook/ipc/media/StickerItem;LX/3iM;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    return-object v0
.end method
