.class public LX/3Uy;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field public b:Landroid/view/ViewStub;

.field public c:Landroid/widget/LinearLayout;

.field public d:Lcom/facebook/resources/ui/FbButton;

.field private e:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587443
    new-instance v0, LX/3Uz;

    invoke-direct {v0}, LX/3Uz;-><init>()V

    sput-object v0, LX/3Uy;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 587444
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 587445
    const p1, 0x7f030c5a

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 587446
    const p1, 0x7f0d1e66

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewStub;

    iput-object p1, p0, LX/3Uy;->b:Landroid/view/ViewStub;

    .line 587447
    return-void
.end method

.method public static b(LX/3Uy;)V
    .locals 2

    .prologue
    .line 587448
    iget-object v0, p0, LX/3Uy;->c:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 587449
    iget-object v0, p0, LX/3Uy;->c:Landroid/widget/LinearLayout;

    iget-object v1, p0, LX/3Uy;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 587450
    :cond_0
    iget-object v0, p0, LX/3Uy;->d:Lcom/facebook/resources/ui/FbButton;

    if-eqz v0, :cond_1

    .line 587451
    iget-object v0, p0, LX/3Uy;->d:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p0, LX/3Uy;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 587452
    :cond_1
    return-void
.end method


# virtual methods
.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 587453
    iput-object p1, p0, LX/3Uy;->e:Landroid/view/View$OnClickListener;

    .line 587454
    invoke-static {p0}, LX/3Uy;->b(LX/3Uy;)V

    .line 587455
    return-void
.end method
