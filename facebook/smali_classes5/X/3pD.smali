.class public final LX/3pD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field public final synthetic a:Landroid/support/v4/app/Fragment;

.field public final synthetic b:LX/0jz;


# direct methods
.method public constructor <init>(LX/0jz;Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 641288
    iput-object p1, p0, LX/3pD;->b:LX/0jz;

    iput-object p2, p0, LX/3pD;->a:Landroid/support/v4/app/Fragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 641290
    iget-object v0, p0, LX/3pD;->a:Landroid/support/v4/app/Fragment;

    iget-object v0, v0, Landroid/support/v4/app/Fragment;->mAnimatingAway:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 641291
    iget-object v0, p0, LX/3pD;->a:Landroid/support/v4/app/Fragment;

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v4/app/Fragment;->mAnimatingAway:Landroid/view/View;

    .line 641292
    iget-object v0, p0, LX/3pD;->b:LX/0jz;

    iget-object v1, p0, LX/3pD;->a:Landroid/support/v4/app/Fragment;

    iget-object v2, p0, LX/3pD;->a:Landroid/support/v4/app/Fragment;

    iget v2, v2, Landroid/support/v4/app/Fragment;->mStateAfterAnimating:I

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, LX/0jz;->a(Landroid/support/v4/app/Fragment;IIIZ)V

    .line 641293
    :cond_0
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 641294
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 641289
    return-void
.end method
