.class public LX/4li;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lorg/apache/http/conn/ssl/X509HostnameVerifier;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Lorg/apache/http/conn/ssl/X509HostnameVerifier;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 804687
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lorg/apache/http/conn/ssl/X509HostnameVerifier;
    .locals 3

    .prologue
    .line 804688
    sget-object v0, LX/4li;->a:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    if-nez v0, :cond_1

    .line 804689
    const-class v1, LX/4li;

    monitor-enter v1

    .line 804690
    :try_start_0
    sget-object v0, LX/4li;->a:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 804691
    if-eqz v2, :cond_0

    .line 804692
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 804693
    invoke-static {v0}, LX/4le;->a(LX/0QB;)LX/4le;

    move-result-object p0

    check-cast p0, LX/0Iv;

    invoke-static {p0}, LX/4lg;->a(LX/0Iv;)Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    move-result-object p0

    move-object v0, p0

    .line 804694
    sput-object v0, LX/4li;->a:Lorg/apache/http/conn/ssl/X509HostnameVerifier;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 804695
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 804696
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 804697
    :cond_1
    sget-object v0, LX/4li;->a:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    return-object v0

    .line 804698
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 804699
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 804700
    invoke-static {p0}, LX/4le;->a(LX/0QB;)LX/4le;

    move-result-object v0

    check-cast v0, LX/0Iv;

    invoke-static {v0}, LX/4lg;->a(LX/0Iv;)Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    move-result-object v0

    return-object v0
.end method
