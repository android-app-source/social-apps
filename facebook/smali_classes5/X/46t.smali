.class public LX/46t;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;


# instance fields
.field private a:LX/46s;

.field private b:LX/46r;

.field private c:[Landroid/graphics/drawable/Drawable;

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method public varargs constructor <init>(LX/46s;LX/46r;I[Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 671752
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 671753
    invoke-static {p1, p2}, LX/46t;->a(LX/46s;LX/46r;)V

    .line 671754
    iput-object p1, p0, LX/46t;->a:LX/46s;

    .line 671755
    iput-object p2, p0, LX/46t;->b:LX/46r;

    .line 671756
    iput-object p4, p0, LX/46t;->c:[Landroid/graphics/drawable/Drawable;

    .line 671757
    iput p3, p0, LX/46t;->f:I

    .line 671758
    invoke-direct {p0}, LX/46t;->a()V

    .line 671759
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/46t;->c:[Landroid/graphics/drawable/Drawable;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 671760
    iget-object v1, p0, LX/46t;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v1, v1, v0

    .line 671761
    if-eqz v1, :cond_0

    .line 671762
    invoke-virtual {v1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 671763
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 671764
    :cond_1
    return-void
.end method

.method private a()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 671730
    move v1, v0

    move v2, v0

    .line 671731
    :goto_0
    iget-object v3, p0, LX/46t;->c:[Landroid/graphics/drawable/Drawable;

    array-length v3, v3

    if-ge v0, v3, :cond_6

    .line 671732
    iget-object v3, p0, LX/46t;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v3, v3, v0

    .line 671733
    if-eqz v3, :cond_4

    .line 671734
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    .line 671735
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    .line 671736
    if-eqz v4, :cond_4

    if-eqz v5, :cond_4

    .line 671737
    if-ltz v4, :cond_0

    if-gez v5, :cond_1

    .line 671738
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can\'t combine drawables without intrinsic dimensions."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 671739
    :cond_1
    iget-object v3, p0, LX/46t;->a:LX/46s;

    sget-object v6, LX/46s;->HORIZONTAL:LX/46s;

    if-ne v3, v6, :cond_5

    move v3, v4

    .line 671740
    :goto_1
    iget-object v6, p0, LX/46t;->a:LX/46s;

    sget-object v7, LX/46s;->HORIZONTAL:LX/46s;

    if-ne v6, v7, :cond_2

    move v4, v5

    .line 671741
    :cond_2
    if-lez v2, :cond_3

    .line 671742
    iget v5, p0, LX/46t;->f:I

    add-int/2addr v2, v5

    .line 671743
    :cond_3
    add-int/2addr v2, v3

    .line 671744
    if-le v4, v1, :cond_4

    move v1, v4

    .line 671745
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v3, v5

    .line 671746
    goto :goto_1

    .line 671747
    :cond_6
    iget-object v0, p0, LX/46t;->a:LX/46s;

    sget-object v3, LX/46s;->HORIZONTAL:LX/46s;

    if-ne v0, v3, :cond_7

    move v0, v2

    :goto_2
    iput v0, p0, LX/46t;->d:I

    .line 671748
    iget-object v0, p0, LX/46t;->a:LX/46s;

    sget-object v3, LX/46s;->HORIZONTAL:LX/46s;

    if-ne v0, v3, :cond_8

    :goto_3
    iput v1, p0, LX/46t;->e:I

    .line 671749
    return-void

    :cond_7
    move v0, v1

    .line 671750
    goto :goto_2

    :cond_8
    move v1, v2

    .line 671751
    goto :goto_3
.end method

.method private static a(LX/46s;LX/46r;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 671722
    const/4 v0, 0x1

    .line 671723
    sget-object v2, LX/46s;->HORIZONTAL:LX/46s;

    if-ne p0, v2, :cond_1

    .line 671724
    sget-object v2, LX/46r;->LEFT:LX/46r;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/46r;->RIGHT:LX/46r;

    if-ne p1, v2, :cond_1

    :cond_0
    move v0, v1

    .line 671725
    :cond_1
    sget-object v2, LX/46s;->VERTICAL:LX/46s;

    if-ne p0, v2, :cond_3

    .line 671726
    sget-object v2, LX/46r;->TOP:LX/46r;

    if-eq p1, v2, :cond_2

    sget-object v2, LX/46r;->BOTTOM:LX/46r;

    if-ne p1, v2, :cond_3

    :cond_2
    move v0, v1

    .line 671727
    :cond_3
    if-nez v0, :cond_4

    .line 671728
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid alignment type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for given combination option: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 671729
    :cond_4
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 26

    .prologue
    .line 671688
    invoke-virtual/range {p0 .. p0}, LX/46t;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    .line 671689
    invoke-virtual/range {p0 .. p0}, LX/46t;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v16

    .line 671690
    move-object/from16 v0, p0

    iget v2, v0, LX/46t;->d:I

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, LX/46t;->e:I

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    if-nez v16, :cond_1

    .line 671691
    :cond_0
    return-void

    .line 671692
    :cond_1
    int-to-double v4, v3

    move-object/from16 v0, p0

    iget v2, v0, LX/46t;->d:I

    int-to-double v6, v2

    div-double v18, v4, v6

    .line 671693
    move/from16 v0, v16

    int-to-double v4, v0

    move-object/from16 v0, p0

    iget v2, v0, LX/46t;->e:I

    int-to-double v6, v2

    div-double v20, v4, v6

    .line 671694
    const-wide/16 v4, 0x0

    .line 671695
    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v6, v0, LX/46t;->c:[Landroid/graphics/drawable/Drawable;

    array-length v6, v6

    if-ge v2, v6, :cond_0

    .line 671696
    move-object/from16 v0, p0

    iget-object v6, v0, LX/46t;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v17, v6, v2

    .line 671697
    if-eqz v17, :cond_3

    .line 671698
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    int-to-double v6, v6

    mul-double v10, v6, v18

    .line 671699
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    int-to-double v6, v6

    mul-double v8, v6, v20

    .line 671700
    const-wide/16 v6, 0x0

    cmpl-double v6, v10, v6

    if-eqz v6, :cond_3

    const-wide/16 v6, 0x0

    cmpl-double v6, v8, v6

    if-eqz v6, :cond_3

    .line 671701
    move-object/from16 v0, p0

    iget-object v6, v0, LX/46t;->a:LX/46s;

    sget-object v7, LX/46s;->HORIZONTAL:LX/46s;

    if-ne v6, v7, :cond_5

    .line 671702
    move-object/from16 v0, p0

    iget v6, v0, LX/46t;->f:I

    int-to-double v6, v6

    mul-double v6, v6, v18

    add-double v12, v10, v6

    .line 671703
    add-double/2addr v10, v4

    .line 671704
    invoke-virtual/range {p0 .. p0}, LX/46t;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Rect;->top:I

    int-to-double v6, v6

    .line 671705
    move-object/from16 v0, p0

    iget-object v14, v0, LX/46t;->b:LX/46r;

    sget-object v15, LX/46r;->CENTER:LX/46r;

    if-ne v14, v15, :cond_4

    .line 671706
    move/from16 v0, v16

    int-to-double v14, v0

    sub-double/2addr v14, v8

    const-wide/high16 v22, 0x4000000000000000L    # 2.0

    div-double v14, v14, v22

    add-double/2addr v6, v14

    .line 671707
    :cond_2
    :goto_1
    add-double/2addr v8, v6

    move-wide v14, v12

    move-wide v12, v4

    move-wide/from16 v24, v6

    move-wide v6, v8

    move-wide/from16 v8, v24

    .line 671708
    :goto_2
    add-double/2addr v4, v14

    .line 671709
    double-to-int v12, v12

    double-to-int v8, v8

    double-to-int v9, v10

    double-to-int v6, v6

    move-object/from16 v0, v17

    invoke-virtual {v0, v12, v8, v9, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 671710
    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 671711
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 671712
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, LX/46t;->b:LX/46r;

    sget-object v15, LX/46r;->BOTTOM:LX/46r;

    if-ne v14, v15, :cond_2

    .line 671713
    move/from16 v0, v16

    int-to-double v14, v0

    sub-double/2addr v14, v8

    add-double/2addr v6, v14

    goto :goto_1

    .line 671714
    :cond_5
    move-object/from16 v0, p0

    iget v6, v0, LX/46t;->f:I

    int-to-double v6, v6

    mul-double v6, v6, v20

    add-double v12, v8, v6

    .line 671715
    add-double/2addr v8, v4

    .line 671716
    invoke-virtual/range {p0 .. p0}, LX/46t;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Rect;->left:I

    int-to-double v6, v6

    .line 671717
    move-object/from16 v0, p0

    iget-object v14, v0, LX/46t;->b:LX/46r;

    sget-object v15, LX/46r;->CENTER:LX/46r;

    if-ne v14, v15, :cond_7

    .line 671718
    int-to-double v14, v3

    sub-double/2addr v14, v10

    const-wide/high16 v22, 0x4000000000000000L    # 2.0

    div-double v14, v14, v22

    add-double/2addr v6, v14

    .line 671719
    :cond_6
    :goto_3
    add-double/2addr v10, v6

    move-wide v14, v12

    move-wide v12, v6

    move-wide v6, v8

    move-wide v8, v4

    goto :goto_2

    .line 671720
    :cond_7
    move-object/from16 v0, p0

    iget-object v14, v0, LX/46t;->b:LX/46r;

    sget-object v15, LX/46r;->RIGHT:LX/46r;

    if-ne v14, v15, :cond_6

    .line 671721
    int-to-double v14, v3

    sub-double/2addr v14, v10

    add-double/2addr v6, v14

    goto :goto_3
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 671687
    iget v0, p0, LX/46t;->e:I

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 671686
    iget v0, p0, LX/46t;->d:I

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 671685
    const/4 v0, 0x0

    return v0
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 671683
    invoke-virtual {p0}, LX/46t;->invalidateSelf()V

    .line 671684
    return-void
.end method

.method public final mutate()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 671765
    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 671766
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/46t;->c:[Landroid/graphics/drawable/Drawable;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 671767
    iget-object v1, p0, LX/46t;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v1, v1, v0

    .line 671768
    if-eqz v1, :cond_0

    .line 671769
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 671770
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 671771
    :cond_1
    return-object p0
.end method

.method public final scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 671681
    invoke-virtual {p0, p2, p3, p4}, LX/46t;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 671682
    return-void
.end method

.method public final setAlpha(I)V
    .locals 2

    .prologue
    .line 671675
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/46t;->c:[Landroid/graphics/drawable/Drawable;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 671676
    iget-object v1, p0, LX/46t;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v1, v1, v0

    .line 671677
    if-eqz v1, :cond_0

    .line 671678
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 671679
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 671680
    :cond_1
    return-void
.end method

.method public final setChangingConfigurations(I)V
    .locals 2

    .prologue
    .line 671669
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/46t;->c:[Landroid/graphics/drawable/Drawable;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 671670
    iget-object v1, p0, LX/46t;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v1, v1, v0

    .line 671671
    if-eqz v1, :cond_0

    .line 671672
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->setChangingConfigurations(I)V

    .line 671673
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 671674
    :cond_1
    return-void
.end method

.method public final setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V
    .locals 2

    .prologue
    .line 671663
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/46t;->c:[Landroid/graphics/drawable/Drawable;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 671664
    iget-object v1, p0, LX/46t;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v1, v1, v0

    .line 671665
    if-eqz v1, :cond_0

    .line 671666
    invoke-virtual {v1, p1, p2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 671667
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 671668
    :cond_1
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 2

    .prologue
    .line 671641
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/46t;->c:[Landroid/graphics/drawable/Drawable;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 671642
    iget-object v1, p0, LX/46t;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v1, v1, v0

    .line 671643
    if-eqz v1, :cond_0

    .line 671644
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 671645
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 671646
    :cond_1
    return-void
.end method

.method public final setFilterBitmap(Z)V
    .locals 2

    .prologue
    .line 671657
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/46t;->c:[Landroid/graphics/drawable/Drawable;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 671658
    iget-object v1, p0, LX/46t;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v1, v1, v0

    .line 671659
    if-eqz v1, :cond_0

    .line 671660
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 671661
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 671662
    :cond_1
    return-void
.end method

.method public final setVisible(ZZ)Z
    .locals 3

    .prologue
    .line 671649
    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    move-result v1

    .line 671650
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, LX/46t;->c:[Landroid/graphics/drawable/Drawable;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 671651
    iget-object v2, p0, LX/46t;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v2, v2, v0

    .line 671652
    if-eqz v2, :cond_0

    .line 671653
    invoke-virtual {v2, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 671654
    const/4 v1, 0x1

    .line 671655
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 671656
    :cond_1
    return v1
.end method

.method public final unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 671647
    invoke-virtual {p0, p2}, LX/46t;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 671648
    return-void
.end method
