.class public final LX/4lC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/0Ze;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/0Ze;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 803827
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 803828
    iput-object p1, p0, LX/4lC;->a:LX/0QB;

    .line 803829
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 803830
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/4lC;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 803831
    packed-switch p2, :pswitch_data_0

    .line 803832
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 803833
    :pswitch_0
    invoke-static {p1}, LX/3mH;->a(LX/0QB;)LX/3mH;

    move-result-object v0

    .line 803834
    :goto_0
    return-object v0

    .line 803835
    :pswitch_1
    invoke-static {p1}, LX/0pV;->a(LX/0QB;)LX/0pV;

    move-result-object v0

    goto :goto_0

    .line 803836
    :pswitch_2
    invoke-static {p1}, LX/DqE;->b(LX/0QB;)LX/DqE;

    move-result-object v0

    goto :goto_0

    .line 803837
    :pswitch_3
    invoke-static {p1}, LX/74M;->b(LX/0QB;)LX/74M;

    move-result-object v0

    goto :goto_0

    .line 803838
    :pswitch_4
    new-instance p0, LX/79A;

    invoke-static {p1}, LX/1Mn;->a(LX/0QB;)LX/1gy;

    move-result-object v0

    check-cast v0, LX/1gy;

    invoke-static {p1}, LX/0XV;->b(LX/0QB;)LX/01U;

    move-result-object v1

    check-cast v1, LX/01U;

    invoke-direct {p0, v0, v1}, LX/79A;-><init>(LX/1gy;LX/01U;)V

    .line 803839
    move-object v0, p0

    .line 803840
    goto :goto_0

    .line 803841
    :pswitch_5
    invoke-static {p1}, LX/0Zc;->a(LX/0QB;)LX/0Zc;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 803842
    const/4 v0, 0x6

    return v0
.end method
