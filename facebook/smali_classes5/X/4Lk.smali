.class public LX/4Lk;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 684860
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 56

    .prologue
    .line 684861
    const/16 v52, 0x0

    .line 684862
    const/16 v51, 0x0

    .line 684863
    const/16 v50, 0x0

    .line 684864
    const/16 v49, 0x0

    .line 684865
    const/16 v48, 0x0

    .line 684866
    const/16 v47, 0x0

    .line 684867
    const/16 v46, 0x0

    .line 684868
    const/16 v45, 0x0

    .line 684869
    const/16 v44, 0x0

    .line 684870
    const/16 v43, 0x0

    .line 684871
    const/16 v42, 0x0

    .line 684872
    const/16 v41, 0x0

    .line 684873
    const/16 v40, 0x0

    .line 684874
    const/16 v39, 0x0

    .line 684875
    const/16 v38, 0x0

    .line 684876
    const/16 v37, 0x0

    .line 684877
    const/16 v36, 0x0

    .line 684878
    const/16 v35, 0x0

    .line 684879
    const/16 v34, 0x0

    .line 684880
    const/16 v33, 0x0

    .line 684881
    const/16 v32, 0x0

    .line 684882
    const/16 v31, 0x0

    .line 684883
    const/16 v30, 0x0

    .line 684884
    const/16 v29, 0x0

    .line 684885
    const/16 v28, 0x0

    .line 684886
    const/16 v27, 0x0

    .line 684887
    const/16 v26, 0x0

    .line 684888
    const/16 v25, 0x0

    .line 684889
    const/16 v24, 0x0

    .line 684890
    const/16 v23, 0x0

    .line 684891
    const/16 v22, 0x0

    .line 684892
    const/16 v21, 0x0

    .line 684893
    const/16 v20, 0x0

    .line 684894
    const/16 v19, 0x0

    .line 684895
    const/16 v18, 0x0

    .line 684896
    const/16 v17, 0x0

    .line 684897
    const/16 v16, 0x0

    .line 684898
    const/4 v15, 0x0

    .line 684899
    const/4 v14, 0x0

    .line 684900
    const/4 v13, 0x0

    .line 684901
    const/4 v12, 0x0

    .line 684902
    const/4 v11, 0x0

    .line 684903
    const/4 v10, 0x0

    .line 684904
    const/4 v9, 0x0

    .line 684905
    const/4 v8, 0x0

    .line 684906
    const/4 v7, 0x0

    .line 684907
    const/4 v6, 0x0

    .line 684908
    const/4 v5, 0x0

    .line 684909
    const/4 v4, 0x0

    .line 684910
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v53

    sget-object v54, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v53

    move-object/from16 v1, v54

    if-eq v0, v1, :cond_1

    .line 684911
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 684912
    const/4 v4, 0x0

    .line 684913
    :goto_0
    return v4

    .line 684914
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 684915
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v53

    sget-object v54, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v53

    move-object/from16 v1, v54

    if-eq v0, v1, :cond_23

    .line 684916
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v53

    .line 684917
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 684918
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v54

    sget-object v55, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v54

    move-object/from16 v1, v55

    if-eq v0, v1, :cond_1

    if-eqz v53, :cond_1

    .line 684919
    const-string v54, "__type__"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-nez v54, :cond_2

    const-string v54, "__typename"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_3

    .line 684920
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v52

    invoke-virtual/range {v52 .. v52}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, p1

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/String;)I

    move-result v52

    goto :goto_1

    .line 684921
    :cond_3
    const-string v54, "audio_play_mode"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_4

    .line 684922
    const/16 v19, 0x1

    .line 684923
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v51 .. v51}, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    move-result-object v51

    goto :goto_1

    .line 684924
    :cond_4
    const-string v54, "audio_url"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_5

    .line 684925
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v50

    goto :goto_1

    .line 684926
    :cond_5
    const-string v54, "base_url"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_6

    .line 684927
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, p1

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v49

    goto :goto_1

    .line 684928
    :cond_6
    const-string v54, "block_title"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_7

    .line 684929
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v48

    goto/16 :goto_1

    .line 684930
    :cond_7
    const-string v54, "display_height"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_8

    .line 684931
    const/16 v18, 0x1

    .line 684932
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v47

    goto/16 :goto_1

    .line 684933
    :cond_8
    const-string v54, "display_width"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_9

    .line 684934
    const/16 v17, 0x1

    .line 684935
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v46

    goto/16 :goto_1

    .line 684936
    :cond_9
    const-string v54, "document_element_type"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_a

    .line 684937
    const/16 v16, 0x1

    .line 684938
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v45 .. v45}, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v45

    goto/16 :goto_1

    .line 684939
    :cond_a
    const-string v54, "element_video"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_b

    .line 684940
    invoke-static/range {p0 .. p1}, LX/4UG;->a(LX/15w;LX/186;)I

    move-result v44

    goto/16 :goto_1

    .line 684941
    :cond_b
    const-string v54, "enable_ad_network_bridging"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_c

    .line 684942
    const/4 v15, 0x1

    .line 684943
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v43

    goto/16 :goto_1

    .line 684944
    :cond_c
    const-string v54, "event"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_d

    .line 684945
    invoke-static/range {p0 .. p1}, LX/4M6;->a(LX/15w;LX/186;)I

    move-result v42

    goto/16 :goto_1

    .line 684946
    :cond_d
    const-string v54, "feedback"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_e

    .line 684947
    invoke-static/range {p0 .. p1}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v41

    goto/16 :goto_1

    .line 684948
    :cond_e
    const-string v54, "feedback_options"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_f

    .line 684949
    const/4 v14, 0x1

    .line 684950
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v40 .. v40}, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v40

    goto/16 :goto_1

    .line 684951
    :cond_f
    const-string v54, "html_source"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_10

    .line 684952
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v39

    goto/16 :goto_1

    .line 684953
    :cond_10
    const-string v54, "id"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_11

    .line 684954
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v38

    goto/16 :goto_1

    .line 684955
    :cond_11
    const-string v54, "list_style"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_12

    .line 684956
    const/4 v13, 0x1

    .line 684957
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v37 .. v37}, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    move-result-object v37

    goto/16 :goto_1

    .line 684958
    :cond_12
    const-string v54, "map_style"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_13

    .line 684959
    const/4 v12, 0x1

    .line 684960
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v36 .. v36}, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    move-result-object v36

    goto/16 :goto_1

    .line 684961
    :cond_13
    const-string v54, "margin_style"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_14

    .line 684962
    const/4 v11, 0x1

    .line 684963
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v35 .. v35}, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    move-result-object v35

    goto/16 :goto_1

    .line 684964
    :cond_14
    const-string v54, "option_call_to_action"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_15

    .line 684965
    const/4 v10, 0x1

    .line 684966
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v34

    invoke-static/range {v34 .. v34}, Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    move-result-object v34

    goto/16 :goto_1

    .line 684967
    :cond_15
    const-string v54, "photo"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_16

    .line 684968
    invoke-static/range {p0 .. p1}, LX/2sY;->a(LX/15w;LX/186;)I

    move-result v33

    goto/16 :goto_1

    .line 684969
    :cond_16
    const-string v54, "poster_image"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_17

    .line 684970
    invoke-static/range {p0 .. p1}, LX/2sY;->a(LX/15w;LX/186;)I

    move-result v32

    goto/16 :goto_1

    .line 684971
    :cond_17
    const-string v54, "presentation_state"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_18

    .line 684972
    const/4 v9, 0x1

    .line 684973
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v31

    goto/16 :goto_1

    .line 684974
    :cond_18
    const-string v54, "text_background_color"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_19

    .line 684975
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    goto/16 :goto_1

    .line 684976
    :cond_19
    const-string v54, "video_autoplay_style"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_1a

    .line 684977
    const/4 v8, 0x1

    .line 684978
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v29 .. v29}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    move-result-object v29

    goto/16 :goto_1

    .line 684979
    :cond_1a
    const-string v54, "video_control_style"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_1b

    .line 684980
    const/4 v7, 0x1

    .line 684981
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v28 .. v28}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    move-result-object v28

    goto/16 :goto_1

    .line 684982
    :cond_1b
    const-string v54, "video_looping_style"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_1c

    .line 684983
    const/4 v6, 0x1

    .line 684984
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    move-result-object v27

    goto/16 :goto_1

    .line 684985
    :cond_1c
    const-string v54, "webview_presentation_style"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_1d

    .line 684986
    const/4 v5, 0x1

    .line 684987
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    move-result-object v26

    goto/16 :goto_1

    .line 684988
    :cond_1d
    const-string v54, "date"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_1e

    .line 684989
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    goto/16 :goto_1

    .line 684990
    :cond_1e
    const-string v54, "weather"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_1f

    .line 684991
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    goto/16 :goto_1

    .line 684992
    :cond_1f
    const-string v54, "weather_uri"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_20

    .line 684993
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    goto/16 :goto_1

    .line 684994
    :cond_20
    const-string v54, "blocks"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_21

    .line 684995
    invoke-static/range {p0 .. p1}, LX/4LK;->b(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 684996
    :cond_21
    const-string v54, "element_text"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v54

    if-eqz v54, :cond_22

    .line 684997
    invoke-static/range {p0 .. p1}, LX/4LK;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 684998
    :cond_22
    const-string v54, "visual_style"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_0

    .line 684999
    const/4 v4, 0x1

    .line 685000
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    move-result-object v20

    goto/16 :goto_1

    .line 685001
    :cond_23
    const/16 v53, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 685002
    const/16 v53, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v53

    move/from16 v2, v52

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 685003
    if-eqz v19, :cond_24

    .line 685004
    const/16 v19, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    move-object/from16 v2, v51

    invoke-virtual {v0, v1, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 685005
    :cond_24
    const/16 v19, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v50

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 685006
    const/16 v19, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v49

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 685007
    const/16 v19, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 685008
    if-eqz v18, :cond_25

    .line 685009
    const/16 v18, 0x5

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v47

    move/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, LX/186;->a(III)V

    .line 685010
    :cond_25
    if-eqz v17, :cond_26

    .line 685011
    const/16 v17, 0x6

    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v46

    move/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, LX/186;->a(III)V

    .line 685012
    :cond_26
    if-eqz v16, :cond_27

    .line 685013
    const/16 v16, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v16

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 685014
    :cond_27
    const/16 v16, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v44

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 685015
    if-eqz v15, :cond_28

    .line 685016
    const/16 v15, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v15, v1}, LX/186;->a(IZ)V

    .line 685017
    :cond_28
    const/16 v15, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v15, v1}, LX/186;->b(II)V

    .line 685018
    const/16 v15, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v15, v1}, LX/186;->b(II)V

    .line 685019
    if-eqz v14, :cond_29

    .line 685020
    const/16 v14, 0xc

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v14, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 685021
    :cond_29
    const/16 v14, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v14, v1}, LX/186;->b(II)V

    .line 685022
    const/16 v14, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v14, v1}, LX/186;->b(II)V

    .line 685023
    if-eqz v13, :cond_2a

    .line 685024
    const/16 v13, 0xf

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-virtual {v0, v13, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 685025
    :cond_2a
    if-eqz v12, :cond_2b

    .line 685026
    const/16 v12, 0x10

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v12, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 685027
    :cond_2b
    if-eqz v11, :cond_2c

    .line 685028
    const/16 v11, 0x11

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-virtual {v0, v11, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 685029
    :cond_2c
    if-eqz v10, :cond_2d

    .line 685030
    const/16 v10, 0x12

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v10, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 685031
    :cond_2d
    const/16 v10, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 685032
    const/16 v10, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 685033
    if-eqz v9, :cond_2e

    .line 685034
    const/16 v9, 0x15

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v9, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 685035
    :cond_2e
    const/16 v9, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 685036
    if-eqz v8, :cond_2f

    .line 685037
    const/16 v8, 0x17

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v8, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 685038
    :cond_2f
    if-eqz v7, :cond_30

    .line 685039
    const/16 v7, 0x18

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v7, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 685040
    :cond_30
    if-eqz v6, :cond_31

    .line 685041
    const/16 v6, 0x19

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v6, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 685042
    :cond_31
    if-eqz v5, :cond_32

    .line 685043
    const/16 v5, 0x1a

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v5, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 685044
    :cond_32
    const/16 v5, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 685045
    const/16 v5, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 685046
    const/16 v5, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 685047
    const/16 v5, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 685048
    const/16 v5, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 685049
    if-eqz v4, :cond_33

    .line 685050
    const/16 v4, 0x20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 685051
    :cond_33
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/16 v5, 0xf

    const/16 v4, 0xc

    const/4 v3, 0x7

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 685052
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 685053
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 685054
    if-eqz v0, :cond_0

    .line 685055
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685056
    invoke-static {p0, p1, v2, p2}, LX/2bt;->a(LX/15i;IILX/0nX;)V

    .line 685057
    :cond_0
    invoke-virtual {p0, p1, v1, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 685058
    if-eqz v0, :cond_1

    .line 685059
    const-string v0, "audio_play_mode"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685060
    const-class v0, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685061
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 685062
    if-eqz v0, :cond_2

    .line 685063
    const-string v1, "audio_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685064
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685065
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 685066
    if-eqz v0, :cond_3

    .line 685067
    const-string v1, "base_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685068
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685069
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 685070
    if-eqz v0, :cond_4

    .line 685071
    const-string v1, "block_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685072
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685073
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 685074
    if-eqz v0, :cond_5

    .line 685075
    const-string v1, "display_height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685076
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 685077
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 685078
    if-eqz v0, :cond_6

    .line 685079
    const-string v1, "display_width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685080
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 685081
    :cond_6
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 685082
    if-eqz v0, :cond_7

    .line 685083
    const-string v0, "document_element_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685084
    const-class v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685085
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 685086
    if-eqz v0, :cond_8

    .line 685087
    const-string v1, "element_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685088
    invoke-static {p0, v0, p2, p3}, LX/4UG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 685089
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 685090
    if-eqz v0, :cond_9

    .line 685091
    const-string v1, "enable_ad_network_bridging"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685092
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 685093
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 685094
    if-eqz v0, :cond_a

    .line 685095
    const-string v1, "event"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685096
    invoke-static {p0, v0, p2, p3}, LX/4M6;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 685097
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 685098
    if-eqz v0, :cond_b

    .line 685099
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685100
    invoke-static {p0, v0, p2, p3}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 685101
    :cond_b
    invoke-virtual {p0, p1, v4, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 685102
    if-eqz v0, :cond_c

    .line 685103
    const-string v0, "feedback_options"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685104
    const-class v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685105
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 685106
    if-eqz v0, :cond_d

    .line 685107
    const-string v1, "html_source"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685108
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685109
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 685110
    if-eqz v0, :cond_e

    .line 685111
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685112
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685113
    :cond_e
    invoke-virtual {p0, p1, v5, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 685114
    if-eqz v0, :cond_f

    .line 685115
    const-string v0, "list_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685116
    const-class v0, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    invoke-virtual {p0, p1, v5, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685117
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 685118
    if-eqz v0, :cond_10

    .line 685119
    const-string v0, "map_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685120
    const/16 v0, 0x10

    const-class v1, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685121
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 685122
    if-eqz v0, :cond_11

    .line 685123
    const-string v0, "margin_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685124
    const/16 v0, 0x11

    const-class v1, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685125
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 685126
    if-eqz v0, :cond_12

    .line 685127
    const-string v0, "option_call_to_action"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685128
    const/16 v0, 0x12

    const-class v1, Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685129
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 685130
    if-eqz v0, :cond_13

    .line 685131
    const-string v1, "photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685132
    invoke-static {p0, v0, p2, p3}, LX/2sY;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 685133
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 685134
    if-eqz v0, :cond_14

    .line 685135
    const-string v1, "poster_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685136
    invoke-static {p0, v0, p2, p3}, LX/2sY;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 685137
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 685138
    if-eqz v0, :cond_15

    .line 685139
    const-string v0, "presentation_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685140
    const/16 v0, 0x15

    const-class v1, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685141
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 685142
    if-eqz v0, :cond_16

    .line 685143
    const-string v1, "text_background_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685144
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685145
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 685146
    if-eqz v0, :cond_17

    .line 685147
    const-string v0, "video_autoplay_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685148
    const/16 v0, 0x17

    const-class v1, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685149
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 685150
    if-eqz v0, :cond_18

    .line 685151
    const-string v0, "video_control_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685152
    const/16 v0, 0x18

    const-class v1, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685153
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 685154
    if-eqz v0, :cond_19

    .line 685155
    const-string v0, "video_looping_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685156
    const/16 v0, 0x19

    const-class v1, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685157
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 685158
    if-eqz v0, :cond_1a

    .line 685159
    const-string v0, "webview_presentation_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685160
    const/16 v0, 0x1a

    const-class v1, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685161
    :cond_1a
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 685162
    if-eqz v0, :cond_1b

    .line 685163
    const-string v1, "date"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685164
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685165
    :cond_1b
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 685166
    if-eqz v0, :cond_1c

    .line 685167
    const-string v1, "weather"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685168
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685169
    :cond_1c
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 685170
    if-eqz v0, :cond_1d

    .line 685171
    const-string v1, "weather_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685172
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685173
    :cond_1d
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 685174
    if-eqz v0, :cond_1e

    .line 685175
    const-string v1, "blocks"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685176
    invoke-static {p0, v0, p2, p3}, LX/4LK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 685177
    :cond_1e
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 685178
    if-eqz v0, :cond_1f

    .line 685179
    const-string v1, "element_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685180
    invoke-static {p0, v0, p2, p3}, LX/4LK;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 685181
    :cond_1f
    const/16 v0, 0x20

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 685182
    if-eqz v0, :cond_20

    .line 685183
    const-string v0, "visual_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685184
    const/16 v0, 0x20

    const-class v1, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685185
    :cond_20
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 685186
    return-void
.end method
