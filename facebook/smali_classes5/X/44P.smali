.class public LX/44P;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0YG",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0So;

.field private final b:LX/0YG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YG",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final c:J

.field private final d:J

.field private final e:Ljava/util/concurrent/TimeUnit;


# direct methods
.method public constructor <init>(LX/0So;LX/0YG;JJLjava/util/concurrent/TimeUnit;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0So;",
            "LX/0YG",
            "<TV;>;JJ",
            "Ljava/util/concurrent/TimeUnit;",
            ")V"
        }
    .end annotation

    .prologue
    .line 669686
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 669687
    iput-object p1, p0, LX/44P;->a:LX/0So;

    .line 669688
    iput-object p2, p0, LX/44P;->b:LX/0YG;

    .line 669689
    iput-wide p3, p0, LX/44P;->c:J

    .line 669690
    iput-wide p5, p0, LX/44P;->d:J

    .line 669691
    iput-object p7, p0, LX/44P;->e:Ljava/util/concurrent/TimeUnit;

    .line 669692
    return-void
.end method


# virtual methods
.method public final addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 669694
    iget-object v0, p0, LX/44P;->b:LX/0YG;

    invoke-interface {v0, p1, p2}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 669695
    return-void
.end method

.method public final cancel(Z)Z
    .locals 1

    .prologue
    .line 669693
    iget-object v0, p0, LX/44P;->b:LX/0YG;

    invoke-interface {v0, p1}, LX/0YG;->cancel(Z)Z

    move-result v0

    return v0
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 669683
    check-cast p1, Ljava/util/concurrent/Delayed;

    .line 669684
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v0}, LX/44P;->getDelay(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p1, v2}, Ljava/util/concurrent/Delayed;->getDelay(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, LX/1YA;->a(JJ)I

    move-result v0

    return v0
.end method

.method public final get()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 669685
    iget-object v0, p0, LX/44P;->b:LX/0YG;

    const v1, 0x4b3a45bb    # 1.2207547E7f

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 669682
    iget-object v0, p0, LX/44P;->b:LX/0YG;

    const v1, -0x10960c03

    invoke-static {v0, p1, p2, p3, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getDelay(Ljava/util/concurrent/TimeUnit;)J
    .locals 6

    .prologue
    .line 669680
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v2, p0, LX/44P;->d:J

    iget-object v1, p0, LX/44P;->e:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    .line 669681
    iget-object v2, p0, LX/44P;->a:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/44P;->c:J

    sub-long/2addr v2, v4

    sub-long/2addr v0, v2

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final isCancelled()Z
    .locals 1

    .prologue
    .line 669679
    iget-object v0, p0, LX/44P;->b:LX/0YG;

    invoke-interface {v0}, LX/0YG;->isCancelled()Z

    move-result v0

    return v0
.end method

.method public final isDone()Z
    .locals 1

    .prologue
    .line 669678
    iget-object v0, p0, LX/44P;->b:LX/0YG;

    invoke-interface {v0}, LX/0YG;->isDone()Z

    move-result v0

    return v0
.end method
