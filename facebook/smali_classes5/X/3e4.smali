.class public LX/3e4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3e4;


# instance fields
.field private final a:LX/3e5;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3ed;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3e5;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3e5;",
            "LX/0Or",
            "<",
            "LX/3ed;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 619159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 619160
    iput-object p1, p0, LX/3e4;->a:LX/3e5;

    .line 619161
    iput-object p2, p0, LX/3e4;->b:LX/0Or;

    .line 619162
    return-void
.end method

.method public static a(LX/0QB;)LX/3e4;
    .locals 5

    .prologue
    .line 619163
    sget-object v0, LX/3e4;->c:LX/3e4;

    if-nez v0, :cond_1

    .line 619164
    const-class v1, LX/3e4;

    monitor-enter v1

    .line 619165
    :try_start_0
    sget-object v0, LX/3e4;->c:LX/3e4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 619166
    if-eqz v2, :cond_0

    .line 619167
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 619168
    new-instance v4, LX/3e4;

    invoke-static {v0}, LX/3e5;->a(LX/0QB;)LX/3e5;

    move-result-object v3

    check-cast v3, LX/3e5;

    const/16 p0, 0x120a

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/3e4;-><init>(LX/3e5;LX/0Or;)V

    .line 619169
    move-object v0, v4

    .line 619170
    sput-object v0, LX/3e4;->c:LX/3e4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 619171
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 619172
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 619173
    :cond_1
    sget-object v0, LX/3e4;->c:LX/3e4;

    return-object v0

    .line 619174
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 619175
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5

    .prologue
    .line 619176
    const-string v0, "StickersAssetTablePopulator.populate"

    const v1, 0x51510190

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 619177
    :try_start_0
    iget-object v0, p0, LX/3e4;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3ed;

    .line 619178
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "INSERT OR REPLACE INTO sticker_asserts ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, LX/3e0;->c:LX/0U1;

    .line 619179
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 619180
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/3e0;->a:LX/0U1;

    .line 619181
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 619182
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/3e0;->b:LX/0U1;

    .line 619183
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 619184
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/3e0;->h:LX/0U1;

    .line 619185
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 619186
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/3e0;->g:LX/0U1;

    .line 619187
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 619188
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") VALUES (?, ?, ?, ?, ?)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    iput-object v1, v0, LX/3ed;->c:Landroid/database/sqlite/SQLiteStatement;

    .line 619189
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/3ed;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 619190
    const v1, 0x68639b7c

    :try_start_1
    invoke-static {p1, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 619191
    :try_start_2
    iget-object v1, p0, LX/3e4;->a:LX/3e5;

    const/4 v2, 0x0

    .line 619192
    iget-object v3, v1, LX/3e5;->b:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    if-nez v3, :cond_2

    .line 619193
    sget-object v3, LX/3e5;->a:Ljava/lang/Class;

    const-string v4, "not external file dir"

    invoke-static {v3, v4}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 619194
    :cond_0
    :goto_0
    move-object v1, v2

    .line 619195
    if-eqz v1, :cond_1

    .line 619196
    invoke-static {v1, v0}, LX/2W9;->a(Ljava/io/File;LX/32Z;)V

    .line 619197
    :cond_1
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 619198
    const v1, 0xedcbb95

    :try_start_3
    invoke-static {p1, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 619199
    :try_start_4
    invoke-virtual {v0}, LX/3ed;->a()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 619200
    const v0, 0x1c892d82

    invoke-static {v0}, LX/02m;->a(I)V

    .line 619201
    return-void

    .line 619202
    :catchall_0
    move-exception v1

    const v2, 0x4261ced7

    :try_start_5
    invoke-static {p1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 619203
    :catchall_1
    move-exception v1

    :try_start_6
    invoke-virtual {v0}, LX/3ed;->a()V

    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 619204
    :catchall_2
    move-exception v0

    const v1, -0x21e942c

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 619205
    :cond_2
    new-instance v3, Ljava/io/File;

    iget-object v4, v1, LX/3e5;->b:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    const-string p0, "stickers"

    invoke-direct {v3, v4, p0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 619206
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    move-object v2, v3

    .line 619207
    goto :goto_0
.end method
