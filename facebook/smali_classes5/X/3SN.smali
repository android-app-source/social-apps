.class public LX/3SN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ug;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3SN;


# instance fields
.field private final a:LX/1xJ;


# direct methods
.method public constructor <init>(LX/1xJ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 582195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 582196
    iput-object p1, p0, LX/3SN;->a:LX/1xJ;

    .line 582197
    return-void
.end method

.method public static a(LX/0QB;)LX/3SN;
    .locals 4

    .prologue
    .line 582182
    sget-object v0, LX/3SN;->b:LX/3SN;

    if-nez v0, :cond_1

    .line 582183
    const-class v1, LX/3SN;

    monitor-enter v1

    .line 582184
    :try_start_0
    sget-object v0, LX/3SN;->b:LX/3SN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 582185
    if-eqz v2, :cond_0

    .line 582186
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 582187
    new-instance p0, LX/3SN;

    invoke-static {v0}, LX/1xJ;->a(LX/0QB;)LX/1xJ;

    move-result-object v3

    check-cast v3, LX/1xJ;

    invoke-direct {p0, v3}, LX/3SN;-><init>(LX/1xJ;)V

    .line 582188
    move-object v0, p0

    .line 582189
    sput-object v0, LX/3SN;->b:LX/3SN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 582190
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 582191
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 582192
    :cond_1
    sget-object v0, LX/3SN;->b:LX/3SN;

    return-object v0

    .line 582193
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 582194
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 582179
    const/16 v0, 0x0

    return v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 582180
    iget-object v0, p0, LX/3SN;->a:LX/1xJ;

    invoke-virtual {v0}, LX/1xJ;->d()V

    .line 582181
    return-void
.end method
