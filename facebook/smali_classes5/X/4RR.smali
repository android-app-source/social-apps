.class public LX/4RR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 708800
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 708801
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 708802
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 708803
    :goto_0
    return v1

    .line 708804
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_4

    .line 708805
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 708806
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 708807
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v9, :cond_0

    .line 708808
    const-string v10, "confidence"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 708809
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v6

    goto :goto_1

    .line 708810
    :cond_1
    const-string v10, "place_entities"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 708811
    invoke-static {p0, p1}, LX/2bc;->b(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 708812
    :cond_2
    const-string v10, "funnel_logging_tags"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 708813
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 708814
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 708815
    :cond_4
    const/4 v9, 0x3

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 708816
    if-eqz v0, :cond_5

    move-object v0, p1

    .line 708817
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 708818
    :cond_5
    invoke-virtual {p1, v6, v8}, LX/186;->b(II)V

    .line 708819
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 708820
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v7, v1

    move v8, v1

    move-wide v2, v4

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const-wide/16 v2, 0x0

    .line 708821
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 708822
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 708823
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_0

    .line 708824
    const-string v2, "confidence"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708825
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 708826
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 708827
    if-eqz v0, :cond_1

    .line 708828
    const-string v1, "place_entities"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708829
    invoke-static {p0, v0, p2, p3}, LX/2bc;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 708830
    :cond_1
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 708831
    if-eqz v0, :cond_2

    .line 708832
    const-string v0, "funnel_logging_tags"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708833
    invoke-virtual {p0, p1, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 708834
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 708835
    return-void
.end method
