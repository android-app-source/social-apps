.class public LX/4mX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0wY;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private final a:Landroid/view/Choreographer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 806009
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 806010
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Expected to run on the UI thread!"

    invoke-static {v0, v1}, LX/0nE;->a(ZLjava/lang/String;)V

    .line 806011
    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object v0

    iput-object v0, p0, LX/4mX;->a:Landroid/view/Choreographer;

    .line 806012
    return-void

    .line 806013
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0wa;)V
    .locals 2

    .prologue
    .line 806007
    iget-object v0, p0, LX/4mX;->a:Landroid/view/Choreographer;

    invoke-virtual {p1}, LX/0wa;->a()Landroid/view/Choreographer$FrameCallback;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 806008
    return-void
.end method

.method public final a(LX/0wa;J)V
    .locals 2

    .prologue
    .line 806005
    iget-object v0, p0, LX/4mX;->a:Landroid/view/Choreographer;

    invoke-virtual {p1}, LX/0wa;->a()Landroid/view/Choreographer$FrameCallback;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, Landroid/view/Choreographer;->postFrameCallbackDelayed(Landroid/view/Choreographer$FrameCallback;J)V

    .line 806006
    return-void
.end method

.method public final b(LX/0wa;)V
    .locals 2

    .prologue
    .line 806003
    iget-object v0, p0, LX/4mX;->a:Landroid/view/Choreographer;

    invoke-virtual {p1}, LX/0wa;->a()Landroid/view/Choreographer$FrameCallback;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->removeFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 806004
    return-void
.end method
