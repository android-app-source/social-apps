.class public LX/47i;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0PO;

.field private static final b:LX/0PQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 672415
    const-string v0, ", "

    invoke-static {v0}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v0

    .line 672416
    sput-object v0, LX/47i;->a:LX/0PO;

    const-string v1, "="

    invoke-virtual {v0, v1}, LX/0PO;->withKeyValueSeparator(Ljava/lang/String;)LX/0PQ;

    move-result-object v0

    const-string v1, "null"

    .line 672417
    new-instance v2, LX/0PQ;

    iget-object v3, v0, LX/0PQ;->joiner:LX/0PO;

    invoke-virtual {v3, v1}, LX/0PO;->useForNull(Ljava/lang/String;)LX/0PO;

    move-result-object v3

    iget-object v4, v0, LX/0PQ;->keyValueSeparator:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, LX/0PQ;-><init>(LX/0PO;Ljava/lang/String;)V

    move-object v0, v2

    .line 672418
    sput-object v0, LX/47i;->b:LX/0PQ;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 672419
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/Map;)Z
    .locals 1
    .param p0    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<**>;)Z"
        }
    .end annotation

    .prologue
    .line 672420
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
