.class public final LX/4Ys;
.super LX/0ur;
.source ""


# instance fields
.field public A:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTarotDigest;",
            ">;"
        }
    .end annotation
.end field

.field public B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

.field public H:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;",
            ">;"
        }
    .end annotation
.end field

.field public I:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/graphql/model/GraphQLEvent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Lcom/facebook/graphql/model/GraphQLDocumentElement;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public S:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public Y:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public Z:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aA:Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aB:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public aC:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aD:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aE:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aF:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aG:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aH:Lcom/facebook/graphql/model/GraphQLOffer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aI:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aJ:Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aK:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aL:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aM:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aN:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aO:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aP:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aQ:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aR:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aS:Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aT:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aU:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aV:I

.field public aW:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aX:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aY:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public aZ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProductionPrompt;",
            ">;"
        }
    .end annotation
.end field

.field public ac:Lcom/facebook/graphql/model/GraphQLInstantArticle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:Z

.field public ae:Z

.field public af:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ag:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ah:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ai:Lcom/facebook/graphql/model/GraphQLLeadGenData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aj:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ak:Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public al:Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public am:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public an:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ao:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ap:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

.field public aq:Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ar:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public as:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public at:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public au:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public av:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public aw:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ax:Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ay:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public az:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

.field public bA:Lcom/facebook/graphql/model/GraphQLTopic;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bB:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bC:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bD:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bE:Lcom/facebook/graphql/model/GraphQLVideo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bF:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLVideoAnnotation;",
            ">;"
        }
    .end annotation
.end field

.field public bG:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bH:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ba:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bb:Lcom/facebook/graphql/model/GraphQLContactRecommendationField;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bc:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public bd:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public be:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bf:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bg:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bh:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bi:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bj:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bk:Z

.field public bl:Z

.field public bm:Z

.field public bn:Z

.field public bo:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bp:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bq:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public br:Lcom/facebook/graphql/model/GraphQLImageOverlay;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bs:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bt:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bu:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bv:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bw:Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bx:Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public by:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bz:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMisinformationAction;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/model/GraphQLAd;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLAlbum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:I

.field public l:I

.field public m:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:I

.field public p:Z

.field public q:Z

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLFundraiserCharity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLCoupon;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:J

.field public y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 784386
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 784387
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    iput-object v0, p0, LX/4Ys;->b:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    .line 784388
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    iput-object v0, p0, LX/4Ys;->z:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    .line 784389
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    iput-object v0, p0, LX/4Ys;->G:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    .line 784390
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    iput-object v0, p0, LX/4Ys;->ap:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    .line 784391
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    iput-object v0, p0, LX/4Ys;->as:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 784392
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    iput-object v0, p0, LX/4Ys;->aY:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 784393
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    iput-object v0, p0, LX/4Ys;->bc:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 784394
    const/4 v0, 0x0

    iput-object v0, p0, LX/4Ys;->bH:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 784395
    instance-of v0, p0, LX/4Ys;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 784396
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/4Ys;
    .locals 4

    .prologue
    .line 784397
    new-instance v0, LX/4Ys;

    invoke-direct {v0}, LX/4Ys;-><init>()V

    .line 784398
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 784399
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->j()Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->b:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    .line 784400
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bO()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->c:LX/0Px;

    .line 784401
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->k()Lcom/facebook/graphql/model/GraphQLAd;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->d:Lcom/facebook/graphql/model/GraphQLAd;

    .line 784402
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->l()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->e:Ljava/lang/String;

    .line 784403
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->f:Ljava/lang/String;

    .line 784404
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bg()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->g:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 784405
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bH()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->h:Ljava/lang/String;

    .line 784406
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->n()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->i:Ljava/lang/String;

    .line 784407
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->o()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->j:Ljava/lang/String;

    .line 784408
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->p()I

    move-result v1

    iput v1, v0, LX/4Ys;->k:I

    .line 784409
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->q()I

    move-result v1

    iput v1, v0, LX/4Ys;->l:I

    .line 784410
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bo()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->m:Lcom/facebook/graphql/model/GraphQLImage;

    .line 784411
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->r()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->n:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 784412
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bE()I

    move-result v1

    iput v1, v0, LX/4Ys;->o:I

    .line 784413
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->s()Z

    move-result v1

    iput-boolean v1, v0, LX/4Ys;->p:Z

    .line 784414
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->t()Z

    move-result v1

    iput-boolean v1, v0, LX/4Ys;->q:Z

    .line 784415
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bp()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->r:Ljava/lang/String;

    .line 784416
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bs()Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->s:Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    .line 784417
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->u()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->t:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 784418
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bj()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->u:Ljava/lang/String;

    .line 784419
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->v()Lcom/facebook/graphql/model/GraphQLCoupon;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->v:Lcom/facebook/graphql/model/GraphQLCoupon;

    .line 784420
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bI()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->w:Ljava/lang/String;

    .line 784421
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->w()J

    move-result-wide v2

    iput-wide v2, v0, LX/4Ys;->x:J

    .line 784422
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 784423
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->y()Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->z:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    .line 784424
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bB()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->A:LX/0Px;

    .line 784425
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bA()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->B:Ljava/lang/String;

    .line 784426
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->z()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->C:Ljava/lang/String;

    .line 784427
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->A()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->D:Ljava/lang/String;

    .line 784428
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bJ()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->E:Ljava/lang/String;

    .line 784429
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bK()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->F:Ljava/lang/String;

    .line 784430
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bx()Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->G:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    .line 784431
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->B()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->H:LX/0Px;

    .line 784432
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->C()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->I:Ljava/lang/String;

    .line 784433
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->D()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->J:Ljava/lang/String;

    .line 784434
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->K:Lcom/facebook/graphql/model/GraphQLEvent;

    .line 784435
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->F()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->L:Ljava/lang/String;

    .line 784436
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->G()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->M:Ljava/lang/String;

    .line 784437
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->H()Lcom/facebook/graphql/model/GraphQLDocumentElement;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->N:Lcom/facebook/graphql/model/GraphQLDocumentElement;

    .line 784438
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->I()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->O:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 784439
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->J()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->P:Ljava/lang/String;

    .line 784440
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->K()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->Q:Ljava/lang/String;

    .line 784441
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->L()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->R:LX/0Px;

    .line 784442
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->M()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->S:Ljava/lang/String;

    .line 784443
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->N()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->T:Ljava/lang/String;

    .line 784444
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->U:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 784445
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->P()Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->V:Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersConnection;

    .line 784446
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Q()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->W:Ljava/lang/String;

    .line 784447
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bF()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->X:LX/0Px;

    .line 784448
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->R()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->Y:LX/0Px;

    .line 784449
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bz()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 784450
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->S()Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aa:Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    .line 784451
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bC()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->ab:LX/0Px;

    .line 784452
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->T()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->ac:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 784453
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bD()Z

    move-result v1

    iput-boolean v1, v0, LX/4Ys;->ad:Z

    .line 784454
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bG()Z

    move-result v1

    iput-boolean v1, v0, LX/4Ys;->ae:Z

    .line 784455
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->af:Lcom/facebook/graphql/model/GraphQLNode;

    .line 784456
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->V()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->ag:Ljava/lang/String;

    .line 784457
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->W()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->ah:Ljava/lang/String;

    .line 784458
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->X()Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->ai:Lcom/facebook/graphql/model/GraphQLLeadGenData;

    .line 784459
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Y()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aj:Ljava/lang/String;

    .line 784460
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Z()Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->ak:Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    .line 784461
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aa()Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->al:Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    .line 784462
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ab()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->am:Ljava/lang/String;

    .line 784463
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ac()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->an:Ljava/lang/String;

    .line 784464
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ad()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->ao:Lcom/facebook/graphql/model/GraphQLImage;

    .line 784465
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ae()Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->ap:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    .line 784466
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->af()Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aq:Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    .line 784467
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ag()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->ar:Ljava/lang/String;

    .line 784468
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ah()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->as:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 784469
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->at:Lcom/facebook/graphql/model/GraphQLImage;

    .line 784470
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aj()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->au:Ljava/lang/String;

    .line 784471
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ak()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->av:LX/0Px;

    .line 784472
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->al()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aw:Ljava/lang/String;

    .line 784473
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->am()Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->ax:Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    .line 784474
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->an()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->ay:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 784475
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bh()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->az:Ljava/lang/String;

    .line 784476
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bk()Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aA:Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    .line 784477
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bi()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aB:LX/0Px;

    .line 784478
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ao()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aC:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 784479
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ap()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aD:Ljava/lang/String;

    .line 784480
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bt()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aE:Ljava/lang/String;

    .line 784481
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aq()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aF:Ljava/lang/String;

    .line 784482
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ar()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aG:Ljava/lang/String;

    .line 784483
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->by()Lcom/facebook/graphql/model/GraphQLOffer;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aH:Lcom/facebook/graphql/model/GraphQLOffer;

    .line 784484
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aI:Lcom/facebook/graphql/model/GraphQLPage;

    .line 784485
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->at()Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aJ:Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;

    .line 784486
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->au()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aK:Lcom/facebook/graphql/model/GraphQLStory;

    .line 784487
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bf()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aL:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    .line 784488
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->av()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aM:Ljava/lang/String;

    .line 784489
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aw()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aN:Ljava/lang/String;

    .line 784490
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ax()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aO:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 784491
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ay()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aP:Ljava/lang/String;

    .line 784492
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aQ:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 784493
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aA()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aR:Ljava/lang/String;

    .line 784494
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bv()Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aS:Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    .line 784495
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aB()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aT:Ljava/lang/String;

    .line 784496
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aC()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aU:Ljava/lang/String;

    .line 784497
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aD()I

    move-result v1

    iput v1, v0, LX/4Ys;->aV:I

    .line 784498
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bl()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aW:Ljava/lang/String;

    .line 784499
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bm()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aX:Ljava/lang/String;

    .line 784500
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aE()Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aY:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 784501
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bL()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->aZ:Ljava/lang/String;

    .line 784502
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bM()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->ba:Ljava/lang/String;

    .line 784503
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aF()Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bb:Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    .line 784504
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aG()Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bc:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 784505
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aH()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bd:Ljava/lang/String;

    .line 784506
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aI()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->be:Ljava/lang/String;

    .line 784507
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bw()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bf:Ljava/lang/String;

    .line 784508
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aJ()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bg:Ljava/lang/String;

    .line 784509
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aK()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bh:Ljava/lang/String;

    .line 784510
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aL()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bi:Ljava/lang/String;

    .line 784511
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aM()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bj:Ljava/lang/String;

    .line 784512
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aN()Z

    move-result v1

    iput-boolean v1, v0, LX/4Ys;->bk:Z

    .line 784513
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aO()Z

    move-result v1

    iput-boolean v1, v0, LX/4Ys;->bl:Z

    .line 784514
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aP()Z

    move-result v1

    iput-boolean v1, v0, LX/4Ys;->bm:Z

    .line 784515
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aQ()Z

    move-result v1

    iput-boolean v1, v0, LX/4Ys;->bn:Z

    .line 784516
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aR()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bo:Ljava/lang/String;

    .line 784517
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aS()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bp:Ljava/lang/String;

    .line 784518
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aT()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bq:Ljava/lang/String;

    .line 784519
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aU()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->br:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    .line 784520
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aV()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bs:Lcom/facebook/graphql/model/GraphQLStory;

    .line 784521
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aW()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bt:Ljava/lang/String;

    .line 784522
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bN()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bu:Ljava/lang/String;

    .line 784523
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->br()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bv:Lcom/facebook/graphql/model/GraphQLNode;

    .line 784524
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aX()Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bw:Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection;

    .line 784525
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aY()Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bx:Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    .line 784526
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bn()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->by:Ljava/lang/String;

    .line 784527
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bz:Ljava/lang/String;

    .line 784528
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ba()Lcom/facebook/graphql/model/GraphQLTopic;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bA:Lcom/facebook/graphql/model/GraphQLTopic;

    .line 784529
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bb()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bB:Ljava/lang/String;

    .line 784530
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bu()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bC:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 784531
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bD:Ljava/lang/String;

    .line 784532
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bd()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bE:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 784533
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->be()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bF:LX/0Px;

    .line 784534
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bq()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bG:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 784535
    invoke-static {v0, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 784536
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/4Ys;->bH:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 784537
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 2

    .prologue
    .line 784538
    new-instance v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;-><init>(LX/4Ys;)V

    .line 784539
    return-object v0
.end method
