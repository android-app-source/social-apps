.class public LX/3VP;
.super Lcom/facebook/attachments/angora/AngoraAttachmentView;
.source ""

# interfaces
.implements LX/3VQ;
.implements LX/3VR;


# static fields
.field public static final c:LX/1Cz;


# instance fields
.field private final e:Lcom/facebook/attachments/angora/InstantArticleIconView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587825
    new-instance v0, LX/3VS;

    invoke-direct {v0}, LX/3VS;-><init>()V

    sput-object v0, LX/3VP;->c:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 587823
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3VP;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 587824
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587821
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3VP;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 587822
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587818
    const v0, 0x7f0300df

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/facebook/attachments/angora/AngoraAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 587819
    const v0, 0x7f0d0537

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/angora/InstantArticleIconView;

    iput-object v0, p0, LX/3VP;->e:Lcom/facebook/attachments/angora/InstantArticleIconView;

    .line 587820
    return-void
.end method


# virtual methods
.method public getTooltipAnchor()Landroid/view/View;
    .locals 1

    .prologue
    .line 587817
    iget-object v0, p0, LX/3VP;->e:Lcom/facebook/attachments/angora/InstantArticleIconView;

    return-object v0
.end method

.method public setCoverPhotoArticleIconVisibility(I)V
    .locals 1

    .prologue
    .line 587806
    iget-object v0, p0, LX/3VP;->e:Lcom/facebook/attachments/angora/InstantArticleIconView;

    invoke-virtual {v0, p1}, Lcom/facebook/attachments/angora/InstantArticleIconView;->setVisibility(I)V

    .line 587807
    return-void
.end method

.method public setLargeImageAspectRatio(F)V
    .locals 1

    .prologue
    .line 587814
    invoke-super {p0, p1}, Lcom/facebook/attachments/angora/AngoraAttachmentView;->setLargeImageAspectRatio(F)V

    .line 587815
    iget-object v0, p0, LX/3VP;->e:Lcom/facebook/attachments/angora/InstantArticleIconView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 587816
    return-void
.end method

.method public setLargeImageController(LX/1aZ;)V
    .locals 2
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587808
    invoke-super {p0, p1}, Lcom/facebook/attachments/angora/AngoraAttachmentView;->setLargeImageController(LX/1aZ;)V

    .line 587809
    iget-object v0, p0, LX/3VP;->e:Lcom/facebook/attachments/angora/InstantArticleIconView;

    invoke-virtual {v0}, Lcom/facebook/attachments/angora/InstantArticleIconView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 587810
    iget-object v0, p0, LX/3VP;->e:Lcom/facebook/attachments/angora/InstantArticleIconView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/angora/InstantArticleIconView;->setVisibility(I)V

    .line 587811
    iget-object v0, p0, LX/3VP;->e:Lcom/facebook/attachments/angora/InstantArticleIconView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 587812
    :goto_0
    return-void

    .line 587813
    :cond_0
    iget-object v0, p0, LX/3VP;->e:Lcom/facebook/attachments/angora/InstantArticleIconView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/angora/InstantArticleIconView;->setVisibility(I)V

    goto :goto_0
.end method
