.class public final LX/4gU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:J

.field public b:J

.field public c:I

.field public d:[J

.field public e:J

.field public f:Ljava/lang/String;

.field private final g:Ljava/lang/String;


# direct methods
.method public constructor <init>(J)V
    .locals 3

    .prologue
    .line 799646
    const-wide/16 v0, 0x2

    invoke-direct {p0, p1, p2, v0, v1}, LX/4gU;-><init>(JJ)V

    .line 799647
    return-void
.end method

.method public constructor <init>(JJ)V
    .locals 3

    .prologue
    .line 799681
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799682
    const-string v0, "HistogramBuckets.class"

    iput-object v0, p0, LX/4gU;->g:Ljava/lang/String;

    .line 799683
    invoke-static {p1, p2}, LX/4gU;->b(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 799684
    new-instance v0, Ljava/security/InvalidParameterException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not a power of 2:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 799685
    :cond_0
    invoke-static {p3, p4}, LX/4gU;->b(J)Z

    move-result v0

    if-nez v0, :cond_1

    .line 799686
    new-instance v0, Ljava/security/InvalidParameterException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not a power of 2:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 799687
    :cond_1
    cmp-long v0, p1, p3

    if-gez v0, :cond_2

    .line 799688
    new-instance v0, Ljava/security/InvalidParameterException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Maximum value cannot be less than first bucket range:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 799689
    :cond_2
    iput-wide p1, p0, LX/4gU;->a:J

    .line 799690
    iput-wide p3, p0, LX/4gU;->b:J

    .line 799691
    cmp-long v0, p1, p3

    if-nez v0, :cond_3

    .line 799692
    const/4 v0, 0x1

    iput v0, p0, LX/4gU;->c:I

    .line 799693
    :goto_0
    iget v0, p0, LX/4gU;->c:I

    new-array v0, v0, [J

    iput-object v0, p0, LX/4gU;->d:[J

    .line 799694
    return-void

    .line 799695
    :cond_3
    invoke-static {p3, p4}, Ljava/lang/Long;->numberOfLeadingZeros(J)I

    move-result v0

    invoke-static {p1, p2}, Ljava/lang/Long;->numberOfLeadingZeros(J)I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4gU;->c:I

    goto :goto_0
.end method

.method private static b(J)Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 799680
    cmp-long v0, p0, v2

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x1

    sub-long v0, p0, v0

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(J)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 799672
    iget-wide v2, p0, LX/4gU;->a:J

    iget-wide v4, p0, LX/4gU;->b:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 799673
    :cond_0
    :goto_0
    return v0

    .line 799674
    :cond_1
    const-wide/16 v2, 0x1

    cmp-long v1, p1, v2

    if-ltz v1, :cond_0

    .line 799675
    iget-wide v0, p0, LX/4gU;->a:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_2

    .line 799676
    iget v0, p0, LX/4gU;->c:I

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 799677
    :cond_2
    iget-wide v0, p0, LX/4gU;->b:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_3

    .line 799678
    const/4 v0, 0x1

    goto :goto_0

    .line 799679
    :cond_3
    iget-wide v0, p0, LX/4gU;->b:J

    invoke-static {v0, v1}, Ljava/lang/Long;->numberOfLeadingZeros(J)I

    move-result v0

    invoke-static {p1, p2}, Ljava/lang/Long;->numberOfLeadingZeros(J)I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(I)LX/0lF;
    .locals 8

    .prologue
    const/4 v4, 0x1

    .line 799655
    monitor-enter p0

    :try_start_0
    new-instance v1, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 799656
    const-string v0, "num_buckets"

    iget v2, p0, LX/4gU;->c:I

    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 799657
    iget v0, p0, LX/4gU;->c:I

    if-eq v0, v4, :cond_0

    .line 799658
    const-string v0, "first_bucket"

    iget-wide v2, p0, LX/4gU;->b:J

    invoke-virtual {v1, v0, v2, v3}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 799659
    const-string v0, "max_bucket_value"

    iget-wide v2, p0, LX/4gU;->a:J

    invoke-virtual {v1, v0, v2, v3}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 799660
    :cond_0
    iget-object v0, p0, LX/4gU;->f:Ljava/lang/String;

    invoke-static {v1, v0}, LX/4gV;->a(LX/0m9;Ljava/lang/String;)V

    .line 799661
    iget v0, p0, LX/4gU;->c:I

    if-le v0, v4, :cond_3

    .line 799662
    new-instance v2, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 799663
    const/4 v0, 0x0

    :goto_0
    iget v3, p0, LX/4gU;->c:I

    if-ge v0, v3, :cond_2

    .line 799664
    iget-object v3, p0, LX/4gU;->d:[J

    aget-wide v4, v3, v0

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_1

    .line 799665
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/4gU;->d:[J

    aget-wide v4, v4, v0

    invoke-virtual {v2, v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 799666
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 799667
    :cond_2
    const-string v0, "bucket_counts"

    invoke-virtual {v1, v0, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 799668
    :cond_3
    const-string v0, "total_count"

    iget-wide v2, p0, LX/4gU;->e:J

    invoke-virtual {v1, v0, v2, v3}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 799669
    const-string v0, "sample_rate"

    invoke-virtual {v1, v0, p1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 799670
    monitor-exit p0

    return-object v1

    .line 799671
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(JJ)V
    .locals 5

    .prologue
    .line 799651
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4gU;->d:[J

    invoke-direct {p0, p1, p2}, LX/4gU;->c(J)I

    move-result v1

    aget-wide v2, v0, v1

    add-long/2addr v2, p3

    aput-wide v2, v0, v1

    .line 799652
    iget-wide v0, p0, LX/4gU;->e:J

    add-long/2addr v0, p3

    iput-wide v0, p0, LX/4gU;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 799653
    monitor-exit p0

    return-void

    .line 799654
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 799648
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/4gU;->f:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 799649
    monitor-exit p0

    return-void

    .line 799650
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
