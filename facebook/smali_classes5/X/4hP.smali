.class public LX/4hP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Externalizable;


# instance fields
.field public exampleNumber_:Ljava/lang/String;

.field public hasExampleNumber:Z

.field public hasNationalNumberPattern:Z

.field public hasPossibleNumberPattern:Z

.field public nationalNumberPattern_:Ljava/lang/String;

.field public possibleNumberPattern_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 801266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 801267
    const-string v0, ""

    iput-object v0, p0, LX/4hP;->nationalNumberPattern_:Ljava/lang/String;

    .line 801268
    const-string v0, ""

    iput-object v0, p0, LX/4hP;->possibleNumberPattern_:Ljava/lang/String;

    .line 801269
    const-string v0, ""

    iput-object v0, p0, LX/4hP;->exampleNumber_:Ljava/lang/String;

    .line 801270
    return-void
.end method

.method public static newBuilder()LX/4hQ;
    .locals 1

    .prologue
    .line 801271
    new-instance v0, LX/4hQ;

    invoke-direct {v0}, LX/4hQ;-><init>()V

    return-object v0
.end method


# virtual methods
.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 2

    .prologue
    .line 801272
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 801273
    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v0

    .line 801274
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/4hP;->hasNationalNumberPattern:Z

    .line 801275
    iput-object v0, p0, LX/4hP;->nationalNumberPattern_:Ljava/lang/String;

    .line 801276
    :cond_0
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 801277
    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v0

    .line 801278
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/4hP;->hasPossibleNumberPattern:Z

    .line 801279
    iput-object v0, p0, LX/4hP;->possibleNumberPattern_:Ljava/lang/String;

    .line 801280
    :cond_1
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 801281
    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v0

    .line 801282
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/4hP;->hasExampleNumber:Z

    .line 801283
    iput-object v0, p0, LX/4hP;->exampleNumber_:Ljava/lang/String;

    .line 801284
    :cond_2
    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    .prologue
    .line 801285
    iget-boolean v0, p0, LX/4hP;->hasNationalNumberPattern:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 801286
    iget-boolean v0, p0, LX/4hP;->hasNationalNumberPattern:Z

    if-eqz v0, :cond_0

    .line 801287
    iget-object v0, p0, LX/4hP;->nationalNumberPattern_:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 801288
    :cond_0
    iget-boolean v0, p0, LX/4hP;->hasPossibleNumberPattern:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 801289
    iget-boolean v0, p0, LX/4hP;->hasPossibleNumberPattern:Z

    if-eqz v0, :cond_1

    .line 801290
    iget-object v0, p0, LX/4hP;->possibleNumberPattern_:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 801291
    :cond_1
    iget-boolean v0, p0, LX/4hP;->hasExampleNumber:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 801292
    iget-boolean v0, p0, LX/4hP;->hasExampleNumber:Z

    if-eqz v0, :cond_2

    .line 801293
    iget-object v0, p0, LX/4hP;->exampleNumber_:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 801294
    :cond_2
    return-void
.end method
