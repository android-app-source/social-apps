.class public final LX/3nX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3nY;


# instance fields
.field public final a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public final c:I

.field public final d:J

.field public final e:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:J

.field public g:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:Z

.field public final synthetic i:LX/3nV;

.field private j:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/3nV;Ljava/lang/String;Ljava/lang/String;JLX/0P1;Ljava/lang/Boolean;)V
    .locals 6
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 638943
    iput-object p1, p0, LX/3nX;->i:LX/3nV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 638944
    iput-object p2, p0, LX/3nX;->a:Ljava/lang/String;

    .line 638945
    iget-object v0, p1, LX/3nV;->f:Ljava/lang/String;

    .line 638946
    if-nez p3, :cond_0

    .line 638947
    const-string v1, "%s (Seq: %s)"

    invoke-static {v1, p2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 638948
    :goto_0
    move-object v0, v1

    .line 638949
    iput-object v0, p0, LX/3nX;->b:Ljava/lang/String;

    .line 638950
    invoke-static {}, LX/3nV;->g()I

    move-result v0

    iput v0, p0, LX/3nX;->c:I

    .line 638951
    iput-wide p4, p0, LX/3nX;->d:J

    .line 638952
    iput-object p6, p0, LX/3nX;->e:LX/0P1;

    .line 638953
    iput-object p7, p0, LX/3nX;->j:Ljava/lang/Boolean;

    .line 638954
    const-wide/16 v0, 0x2

    iget-object v2, p0, LX/3nX;->b:Ljava/lang/String;

    iget v3, p0, LX/3nX;->c:I

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, p4, p5}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, LX/018;->a(JLjava/lang/String;IJ)V

    .line 638955
    return-void

    :cond_0
    const-string v1, "%s(%s) (Seq: %s)"

    invoke-static {v1, p2, p3, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public synthetic constructor <init>(LX/3nV;Ljava/lang/String;Ljava/lang/String;JLX/0P1;Ljava/lang/Boolean;B)V
    .locals 0

    .prologue
    .line 638942
    invoke-direct/range {p0 .. p7}, LX/3nX;-><init>(LX/3nV;Ljava/lang/String;Ljava/lang/String;JLX/0P1;Ljava/lang/Boolean;)V

    return-void
.end method

.method public static synthetic a(LX/3nX;JLjava/lang/Boolean;LX/0P1;Z)J
    .locals 10

    .prologue
    .line 638909
    const-wide/16 v3, 0x2

    .line 638910
    iput-wide p1, p0, LX/3nX;->f:J

    .line 638911
    iput-object p4, p0, LX/3nX;->g:LX/0P1;

    .line 638912
    iput-boolean p5, p0, LX/3nX;->h:Z

    .line 638913
    invoke-static {p0, p3}, LX/3nX;->a$redex0(LX/3nX;Ljava/lang/Boolean;)V

    .line 638914
    iget-object v5, p0, LX/3nX;->b:Ljava/lang/String;

    iget v6, p0, LX/3nX;->c:I

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v7, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v7

    invoke-static/range {v3 .. v8}, LX/018;->b(JLjava/lang/String;IJ)V

    .line 638915
    iget-boolean v5, p0, LX/3nX;->h:Z

    if-eqz v5, :cond_0

    .line 638916
    iget-object v5, p0, LX/3nX;->b:Ljava/lang/String;

    .line 638917
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "FAILED: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, LX/3nX;->b:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, LX/3nX;->b:Ljava/lang/String;

    .line 638918
    iget-object v6, p0, LX/3nX;->b:Ljava/lang/String;

    iget v7, p0, LX/3nX;->c:I

    invoke-static {v3, v4, v5, v6, v7}, LX/018;->b(JLjava/lang/String;Ljava/lang/String;I)V

    .line 638919
    :cond_0
    iget-wide v3, p0, LX/3nX;->f:J

    iget-wide v5, p0, LX/3nX;->d:J

    sub-long/2addr v3, v5

    move-wide v0, v3

    .line 638920
    return-wide v0
.end method

.method public static a$redex0(LX/3nX;Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 638938
    if-nez p1, :cond_1

    .line 638939
    :cond_0
    :goto_0
    return-void

    .line 638940
    :cond_1
    iget-object v0, p0, LX/3nX;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3nX;->j:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 638941
    :cond_2
    iput-object p1, p0, LX/3nX;->j:Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 638937
    iget-wide v0, p0, LX/3nX;->d:J

    return-wide v0
.end method

.method public final b()LX/0lF;
    .locals 6

    .prologue
    .line 638923
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 638924
    const-string v1, "name"

    iget-object v2, p0, LX/3nX;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 638925
    const-string v1, "type"

    const-string v2, "s"

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 638926
    const-string v1, "failed"

    iget-boolean v2, p0, LX/3nX;->h:Z

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 638927
    const-string v1, "relative_start_ms"

    iget-wide v2, p0, LX/3nX;->d:J

    iget-object v4, p0, LX/3nX;->i:LX/3nV;

    iget-wide v4, v4, LX/3nV;->k:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 638928
    iget-object v1, p0, LX/3nX;->i:LX/3nV;

    iget-wide v2, v1, LX/3nV;->l:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 638929
    const-string v1, "duration_ms"

    iget-wide v2, p0, LX/3nX;->f:J

    iget-wide v4, p0, LX/3nX;->d:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 638930
    :cond_0
    iget-object v1, p0, LX/3nX;->e:LX/0P1;

    if-eqz v1, :cond_1

    .line 638931
    const-string v1, "start_extra"

    iget-object v2, p0, LX/3nX;->e:LX/0P1;

    invoke-static {v2}, LX/16N;->a(Ljava/util/Map;)LX/0m9;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 638932
    :cond_1
    iget-object v1, p0, LX/3nX;->g:LX/0P1;

    if-eqz v1, :cond_2

    .line 638933
    const-string v1, "stop_extra"

    iget-object v2, p0, LX/3nX;->g:LX/0P1;

    invoke-static {v2}, LX/16N;->a(Ljava/util/Map;)LX/0m9;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 638934
    :cond_2
    iget-object v1, p0, LX/3nX;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 638935
    const-string v1, "guess_was_bg"

    iget-object v2, p0, LX/3nX;->j:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 638936
    :cond_3
    return-object v0
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 638921
    const-wide/16 v0, 0x2

    iget-object v2, p0, LX/3nX;->b:Ljava/lang/String;

    iget v3, p0, LX/3nX;->c:I

    invoke-static {v0, v1, v2, v3}, LX/018;->f(JLjava/lang/String;I)V

    .line 638922
    return-void
.end method
