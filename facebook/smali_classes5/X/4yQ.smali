.class public final LX/4yQ;
.super LX/0Py;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Py",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final transient a:LX/18f;
    .annotation build Lcom/google/j2objc/annotations/Weak;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/18f",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/18f;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/18f",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 821769
    invoke-direct {p0}, LX/0Py;-><init>()V

    .line 821770
    iput-object p1, p0, LX/4yQ;->a:LX/18f;

    .line 821771
    return-void
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 821768
    iget-object v0, p0, LX/4yQ;->a:LX/18f;

    invoke-virtual {v0, p1}, LX/0Xt;->g(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final copyIntoArray([Ljava/lang/Object;I)I
    .locals 2
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "not present in emulated superclass"
    .end annotation

    .prologue
    .line 821772
    iget-object v0, p0, LX/4yQ;->a:LX/18f;

    iget-object v0, v0, LX/18f;->b:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Py;

    .line 821773
    invoke-virtual {v0, p1, p2}, LX/0Py;->copyIntoArray([Ljava/lang/Object;I)I

    move-result p2

    goto :goto_0

    .line 821774
    :cond_0
    return p2
.end method

.method public final isPartialView()Z
    .locals 1

    .prologue
    .line 821767
    const/4 v0, 0x1

    return v0
.end method

.method public final iterator()LX/0Rc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 821766
    iget-object v0, p0, LX/4yQ;->a:LX/18f;

    invoke-virtual {v0}, LX/18f;->y()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 821765
    invoke-virtual {p0}, LX/4yQ;->iterator()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 821764
    iget-object v0, p0, LX/4yQ;->a:LX/18f;

    invoke-virtual {v0}, LX/18f;->f()I

    move-result v0

    return v0
.end method
