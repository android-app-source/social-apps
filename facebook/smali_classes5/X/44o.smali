.class public LX/44o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0X9;
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0X9;",
        "Ljava/util/concurrent/Callable",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private final c:LX/0So;

.field public final d:J

.field public e:J

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Callable;Ljava/lang/Class;LX/0So;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<TV;>;",
            "Ljava/lang/Class",
            "<*>;",
            "LX/0So;",
            ")V"
        }
    .end annotation

    .prologue
    .line 669957
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 669958
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/44o;->e:J

    .line 669959
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Callable;

    iput-object v0, p0, LX/44o;->a:Ljava/util/concurrent/Callable;

    .line 669960
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, LX/44o;->b:Ljava/lang/Class;

    .line 669961
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    iput-object v0, p0, LX/44o;->c:LX/0So;

    .line 669962
    invoke-interface {p3}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/44o;->d:J

    .line 669963
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 669964
    iget-object v0, p0, LX/44o;->a:Ljava/util/concurrent/Callable;

    instance-of v0, v0, LX/0X9;

    if-eqz v0, :cond_0

    .line 669965
    iget-object v0, p0, LX/44o;->a:Ljava/util/concurrent/Callable;

    check-cast v0, LX/0X9;

    invoke-interface {v0}, LX/0X9;->a()Ljava/lang/String;

    move-result-object v0

    .line 669966
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/44o;->b:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 669967
    iget-object v0, p0, LX/44o;->a:Ljava/util/concurrent/Callable;

    instance-of v0, v0, LX/0X9;

    if-eqz v0, :cond_0

    .line 669968
    iget-object v0, p0, LX/44o;->a:Ljava/util/concurrent/Callable;

    check-cast v0, LX/0X9;

    invoke-interface {v0}, LX/0X9;->b()Ljava/lang/String;

    move-result-object v0

    .line 669969
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/44o;->b:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final call()Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x3

    .line 669970
    iget-object v0, p0, LX/44o;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/44o;->e:J

    .line 669971
    iget-wide v6, p0, LX/44o;->e:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-ltz v6, :cond_3

    const/4 v6, 0x1

    :goto_0
    const-string v7, "Job has not been run yet"

    invoke-static {v6, v7}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 669972
    iget-wide v6, p0, LX/44o;->e:J

    iget-wide v8, p0, LX/44o;->d:J

    sub-long/2addr v6, v8

    move-wide v0, v6

    .line 669973
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 669974
    iget-object v3, p0, LX/44o;->b:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 669975
    iget-object v3, p0, LX/44o;->f:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 669976
    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, LX/44o;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 669977
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const v3, 0x4632d740    # 11445.8125f

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 669978
    :try_start_0
    const-string v2, "queuedTime: %d ms"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, LX/0PR;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 669979
    iget-object v0, p0, LX/44o;->a:Ljava/util/concurrent/Callable;

    invoke-interface {v0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 669980
    const v1, 0x5f235acd

    invoke-static {v1}, LX/02m;->a(I)V

    .line 669981
    invoke-static {v5}, LX/01m;->b(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 669982
    iget-object v1, p0, LX/44o;->b:Ljava/lang/Class;

    invoke-static {v1}, LX/03P;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0PR;->c(Ljava/lang/String;)V

    :cond_1
    return-object v0

    .line 669983
    :catchall_0
    move-exception v0

    const v1, -0xb653326

    invoke-static {v1}, LX/02m;->a(I)V

    .line 669984
    invoke-static {v5}, LX/01m;->b(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 669985
    iget-object v1, p0, LX/44o;->b:Ljava/lang/Class;

    invoke-static {v1}, LX/03P;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0PR;->c(Ljava/lang/String;)V

    :cond_2
    throw v0

    .line 669986
    :cond_3
    const/4 v6, 0x0

    goto :goto_0
.end method
