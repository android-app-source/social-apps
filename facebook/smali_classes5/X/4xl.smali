.class public final LX/4xl;
.super Ljava/util/AbstractCollection;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractCollection",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final a:LX/4xi;
    .annotation build Lcom/google/j2objc/annotations/Weak;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4xi",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/4xi;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4xi",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 821296
    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    .line 821297
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4xi;

    iput-object v0, p0, LX/4xl;->a:LX/4xi;

    .line 821298
    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 1

    .prologue
    .line 821294
    iget-object v0, p0, LX/4xl;->a:LX/4xi;

    invoke-interface {v0}, LX/0Xu;->g()V

    .line 821295
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 821289
    iget-object v0, p0, LX/4xl;->a:LX/4xi;

    invoke-interface {v0, p1}, LX/0Xu;->g(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 821293
    iget-object v0, p0, LX/4xl;->a:LX/4xi;

    invoke-interface {v0}, LX/0Xu;->k()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0PM;->b(Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 821299
    iget-object v0, p0, LX/4xl;->a:LX/4xi;

    invoke-interface {v0}, LX/4xi;->c()LX/0Rl;

    move-result-object v1

    .line 821300
    iget-object v0, p0, LX/4xl;->a:LX/4xi;

    invoke-interface {v0}, LX/4xi;->a()LX/0Xu;

    move-result-object v0

    invoke-interface {v0}, LX/0Xu;->k()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 821301
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 821302
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 821303
    invoke-interface {v1, v0}, LX/0Rl;->apply(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 821304
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 821305
    const/4 v0, 0x1

    .line 821306
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 821292
    iget-object v0, p0, LX/4xl;->a:LX/4xi;

    invoke-interface {v0}, LX/4xi;->a()LX/0Xu;

    move-result-object v0

    invoke-interface {v0}, LX/0Xu;->k()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, LX/4xl;->a:LX/4xi;

    invoke-interface {v1}, LX/4xi;->c()LX/0Rl;

    move-result-object v1

    invoke-static {p1}, LX/0Rj;->in(Ljava/util/Collection;)LX/0Rl;

    move-result-object v2

    invoke-static {v2}, LX/0PM;->b(LX/0Rl;)LX/0Rl;

    move-result-object v2

    invoke-static {v1, v2}, LX/0Rj;->and(LX/0Rl;LX/0Rl;)LX/0Rl;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0Rl;)Z

    move-result v0

    return v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 821291
    iget-object v0, p0, LX/4xl;->a:LX/4xi;

    invoke-interface {v0}, LX/4xi;->a()LX/0Xu;

    move-result-object v0

    invoke-interface {v0}, LX/0Xu;->k()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, LX/4xl;->a:LX/4xi;

    invoke-interface {v1}, LX/4xi;->c()LX/0Rl;

    move-result-object v1

    invoke-static {p1}, LX/0Rj;->in(Ljava/util/Collection;)LX/0Rl;

    move-result-object v2

    invoke-static {v2}, LX/0Rj;->not(LX/0Rl;)LX/0Rl;

    move-result-object v2

    invoke-static {v2}, LX/0PM;->b(LX/0Rl;)LX/0Rl;

    move-result-object v2

    invoke-static {v1, v2}, LX/0Rj;->and(LX/0Rl;LX/0Rl;)LX/0Rl;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0Rl;)Z

    move-result v0

    return v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 821290
    iget-object v0, p0, LX/4xl;->a:LX/4xi;

    invoke-interface {v0}, LX/0Xu;->f()I

    move-result v0

    return v0
.end method
