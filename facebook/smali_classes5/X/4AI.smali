.class public final LX/4AI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/dracula/runtime/jdk/DraculaMap$1$Dracula$Entry$1$Dracula",
        "<TK;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TK;"
        }
    .end annotation
.end field

.field public b:LX/15i;

.field public c:I

.field public d:I

.field public final e:I

.field public f:LX/4AI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4AI",
            "<TK;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;LX/15i;IIILX/4AI;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "LX/15i;",
            "III",
            "LX/4AI",
            "<TK;>;)V"
        }
    .end annotation

    .prologue
    .line 675788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 675789
    iput-object p1, p0, LX/4AI;->a:Ljava/lang/Object;

    .line 675790
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 675791
    :try_start_0
    iput-object p2, p0, LX/4AI;->b:LX/15i;

    .line 675792
    iput p3, p0, LX/4AI;->c:I

    .line 675793
    iput p4, p0, LX/4AI;->d:I

    .line 675794
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 675795
    iput p5, p0, LX/4AI;->e:I

    .line 675796
    iput-object p6, p0, LX/4AI;->f:LX/4AI;

    .line 675797
    return-void

    .line 675798
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final b()LX/1vs;
    .locals 4

    .prologue
    .line 675799
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 675800
    :try_start_0
    iget-object v0, p0, LX/4AI;->b:LX/15i;

    .line 675801
    iget v2, p0, LX/4AI;->c:I

    .line 675802
    iget v3, p0, LX/4AI;->d:I

    .line 675803
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 675804
    invoke-static {v0, v2, v3}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    return-object v0

    .line 675805
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 675806
    :try_start_0
    check-cast p1, LX/4AI;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 675807
    iget-object v1, p1, LX/4AI;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 675808
    iget-object v2, p0, LX/4AI;->a:Ljava/lang/Object;

    .line 675809
    if-nez v1, :cond_2

    if-nez v2, :cond_1

    const/4 v3, 0x1

    :goto_0
    move v1, v3

    .line 675810
    if-eqz v1, :cond_0

    .line 675811
    invoke-virtual {p1}, LX/4AI;->b()LX/1vs;

    move-result-object v0

    .line 675812
    iget-object v1, v0, LX/1vs;->a:LX/15i;

    .line 675813
    iget v2, v0, LX/1vs;->b:I

    .line 675814
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    .line 675815
    :try_start_1
    iget-object v0, p0, LX/4AI;->b:LX/15i;

    .line 675816
    iget v4, p0, LX/4AI;->c:I

    .line 675817
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 675818
    invoke-static {v1, v2, v0, v4}, LX/4A1;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    .line 675819
    :cond_0
    :goto_1
    return v0

    .line 675820
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 675821
    :catch_0
    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 675822
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 675823
    :try_start_0
    iget v0, p0, LX/4AI;->c:I

    .line 675824
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 675825
    if-nez v0, :cond_0

    move v0, v1

    .line 675826
    :goto_0
    iget-object v2, p0, LX/4AI;->a:Ljava/lang/Object;

    if-nez v2, :cond_1

    :goto_1
    xor-int/2addr v0, v1

    return v0

    .line 675827
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 675828
    :cond_0
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 675829
    :try_start_2
    iget-object v0, p0, LX/4AI;->b:LX/15i;

    .line 675830
    iget v3, p0, LX/4AI;->c:I

    .line 675831
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 675832
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v0, v3

    goto :goto_0

    .line 675833
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 675834
    :cond_1
    iget-object v1, p0, LX/4AI;->a:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 675835
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 675836
    :try_start_0
    iget-object v0, p0, LX/4AI;->b:LX/15i;

    .line 675837
    iget v2, p0, LX/4AI;->c:I

    .line 675838
    iget v3, p0, LX/4AI;->d:I

    .line 675839
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 675840
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, LX/4AI;->a:Ljava/lang/Object;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0, v2, v3}, LX/1vu;->a(LX/15i;II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 675841
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
