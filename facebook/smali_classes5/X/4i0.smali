.class public abstract LX/4i0;
.super LX/0Pq;
.source ""


# direct methods
.method public constructor <init>(ILjava/lang/String;LX/0Rf;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 802363
    const/4 v0, 0x0

    .line 802364
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v2

    .line 802365
    invoke-virtual {v2, p3}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    .line 802366
    if-eqz p4, :cond_0

    .line 802367
    const-string v1, "\\|"

    invoke-virtual {p4, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 802368
    array-length v4, v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 802369
    invoke-virtual {v2, v5}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 802370
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 802371
    :cond_0
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v1

    move-object v1, v1

    .line 802372
    invoke-direct {p0, p1, p2, v0, v1}, LX/0Pq;-><init>(ILjava/lang/String;ZLX/0Rf;)V

    .line 802373
    return-void
.end method
