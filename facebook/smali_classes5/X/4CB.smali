.class public LX/4CB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4CA;


# instance fields
.field private final a:LX/4de;

.field private final b:Z

.field private c:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/4de;Z)V
    .locals 0

    .prologue
    .line 678510
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 678511
    iput-object p1, p0, LX/4CB;->a:LX/4de;

    .line 678512
    iput-boolean p2, p0, LX/4CB;->b:Z

    .line 678513
    return-void
.end method

.method private static a(LX/1FJ;)LX/1FJ;
    .locals 1
    .param p0    # LX/1FJ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;)",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 678514
    :try_start_0
    invoke-static {p0}, LX/1FJ;->a(LX/1FJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/1ll;

    if-eqz v0, :cond_0

    .line 678515
    invoke-virtual {p0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ll;

    invoke-virtual {v0}, LX/1ll;->f()LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 678516
    invoke-static {p0}, LX/1FJ;->c(LX/1FJ;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/1FJ;->c(LX/1FJ;)V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {p0}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()LX/1FJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 678517
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4CB;->c:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;

    move-result-object v0

    invoke-static {v0}, LX/4CB;->a(LX/1FJ;)LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)LX/1FJ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 678518
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4CB;->a:LX/4de;

    .line 678519
    iget-object v1, v0, LX/4de;->b:LX/1Fg;

    invoke-static {v0, p1}, LX/4de;->b(LX/4de;I)LX/4dd;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Fg;->a(Ljava/lang/Object;)LX/1FJ;

    move-result-object v1

    move-object v0, v1

    .line 678520
    invoke-static {v0}, LX/4CB;->a(LX/1FJ;)LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(ILX/1FJ;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 678521
    monitor-enter p0

    :try_start_0
    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 678522
    const/4 v1, 0x0

    .line 678523
    :try_start_1
    new-instance v0, LX/1ll;

    sget-object v2, LX/1lk;->a:LX/1lk;

    const/4 v3, 0x0

    invoke-direct {v0, p2, v2, v3}, LX/1ll;-><init>(LX/1FJ;LX/1lk;I)V

    .line 678524
    invoke-static {v0}, LX/1FJ;->a(Ljava/io/Closeable;)LX/1FJ;

    move-result-object v1

    .line 678525
    iget-object v0, p0, LX/4CB;->c:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 678526
    iget-object v0, p0, LX/4CB;->a:LX/4de;

    .line 678527
    iget-object v2, v0, LX/4de;->b:LX/1Fg;

    invoke-static {v0, p1}, LX/4de;->b(LX/4de;I)LX/4dd;

    move-result-object v3

    iget-object p2, v0, LX/4de;->c:LX/4dc;

    invoke-virtual {v2, v3, v1, p2}, LX/1Fg;->a(Ljava/lang/Object;LX/1FJ;LX/4dc;)LX/1FJ;

    move-result-object v2

    move-object v0, v2

    .line 678528
    iput-object v0, p0, LX/4CB;->c:LX/1FJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 678529
    :try_start_2
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 678530
    monitor-exit p0

    return-void

    .line 678531
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 678532
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()LX/1FJ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 678533
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/4CB;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 678534
    const/4 v0, 0x0

    .line 678535
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/4CB;->a:LX/4de;

    .line 678536
    :cond_1
    invoke-static {v0}, LX/4de;->b(LX/4de;)LX/1bh;

    move-result-object v1

    .line 678537
    if-nez v1, :cond_2

    .line 678538
    const/4 v1, 0x0

    .line 678539
    :goto_1
    move-object v0, v1

    .line 678540
    invoke-static {v0}, LX/4CB;->a(LX/1FJ;)LX/1FJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 678541
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 678542
    :cond_2
    iget-object v2, v0, LX/4de;->b:LX/1Fg;

    invoke-virtual {v2, v1}, LX/1Fg;->b(Ljava/lang/Object;)LX/1FJ;

    move-result-object v1

    .line 678543
    if-eqz v1, :cond_1

    goto :goto_1
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 678544
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4CB;->c:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 678545
    const/4 v0, 0x0

    iput-object v0, p0, LX/4CB;->c:LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 678546
    monitor-exit p0

    return-void

    .line 678547
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
