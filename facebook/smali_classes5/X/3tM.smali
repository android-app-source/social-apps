.class public final LX/3tM;
.super LX/0vn;
.source ""


# instance fields
.field public final synthetic b:LX/3tW;

.field public final c:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(LX/3tW;)V
    .locals 1

    .prologue
    .line 644840
    iput-object p1, p0, LX/3tM;->b:LX/3tW;

    invoke-direct {p0}, LX/0vn;-><init>()V

    .line 644841
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/3tM;->c:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/3sp;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 644801
    sget-boolean v0, LX/3tW;->c:Z

    if-eqz v0, :cond_0

    .line 644802
    invoke-super {p0, p1, p2}, LX/0vn;->a(Landroid/view/View;LX/3sp;)V

    .line 644803
    :goto_0
    const-class v0, LX/3tW;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/3sp;->b(Ljava/lang/CharSequence;)V

    .line 644804
    invoke-virtual {p2, v3}, LX/3sp;->a(Z)V

    .line 644805
    invoke-virtual {p2, v3}, LX/3sp;->b(Z)V

    .line 644806
    return-void

    .line 644807
    :cond_0
    sget-object v0, LX/3sp;->a:LX/3sg;

    iget-object v1, p2, LX/3sp;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, LX/3sg;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/3sp;->c(Ljava/lang/Object;)LX/3sp;

    move-result-object v0

    move-object v1, v0

    .line 644808
    invoke-super {p0, p1, v1}, LX/0vn;->a(Landroid/view/View;LX/3sp;)V

    .line 644809
    sget-object v0, LX/3sp;->a:LX/3sg;

    iget-object v2, p2, LX/3sp;->b:Ljava/lang/Object;

    invoke-interface {v0, v2, p1}, LX/3sg;->c(Ljava/lang/Object;Landroid/view/View;)V

    .line 644810
    sget-object v0, LX/0vv;->a:LX/0w2;

    invoke-interface {v0, p1}, LX/0w2;->i(Landroid/view/View;)Landroid/view/ViewParent;

    move-result-object v0

    move-object v0, v0

    .line 644811
    instance-of v2, v0, Landroid/view/View;

    if-eqz v2, :cond_1

    .line 644812
    check-cast v0, Landroid/view/View;

    invoke-virtual {p2, v0}, LX/3sp;->d(Landroid/view/View;)V

    .line 644813
    :cond_1
    iget-object v0, p0, LX/3tM;->c:Landroid/graphics/Rect;

    .line 644814
    invoke-virtual {v1, v0}, LX/3sp;->a(Landroid/graphics/Rect;)V

    .line 644815
    invoke-virtual {p2, v0}, LX/3sp;->b(Landroid/graphics/Rect;)V

    .line 644816
    invoke-virtual {v1, v0}, LX/3sp;->c(Landroid/graphics/Rect;)V

    .line 644817
    invoke-virtual {p2, v0}, LX/3sp;->d(Landroid/graphics/Rect;)V

    .line 644818
    sget-object v0, LX/3sp;->a:LX/3sg;

    iget-object p0, v1, LX/3sp;->b:Ljava/lang/Object;

    invoke-interface {v0, p0}, LX/3sg;->r(Ljava/lang/Object;)Z

    move-result v0

    move v0, v0

    .line 644819
    invoke-virtual {p2, v0}, LX/3sp;->c(Z)V

    .line 644820
    invoke-virtual {v1}, LX/3sp;->l()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/3sp;->a(Ljava/lang/CharSequence;)V

    .line 644821
    invoke-virtual {v1}, LX/3sp;->m()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/3sp;->b(Ljava/lang/CharSequence;)V

    .line 644822
    invoke-virtual {v1}, LX/3sp;->o()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/3sp;->d(Ljava/lang/CharSequence;)V

    .line 644823
    invoke-virtual {v1}, LX/3sp;->k()Z

    move-result v0

    invoke-virtual {p2, v0}, LX/3sp;->h(Z)V

    .line 644824
    invoke-virtual {v1}, LX/3sp;->i()Z

    move-result v0

    invoke-virtual {p2, v0}, LX/3sp;->f(Z)V

    .line 644825
    invoke-virtual {v1}, LX/3sp;->d()Z

    move-result v0

    invoke-virtual {p2, v0}, LX/3sp;->a(Z)V

    .line 644826
    invoke-virtual {v1}, LX/3sp;->e()Z

    move-result v0

    invoke-virtual {p2, v0}, LX/3sp;->b(Z)V

    .line 644827
    invoke-virtual {v1}, LX/3sp;->g()Z

    move-result v0

    invoke-virtual {p2, v0}, LX/3sp;->d(Z)V

    .line 644828
    invoke-virtual {v1}, LX/3sp;->h()Z

    move-result v0

    invoke-virtual {p2, v0}, LX/3sp;->e(Z)V

    .line 644829
    invoke-virtual {v1}, LX/3sp;->j()Z

    move-result v0

    invoke-virtual {p2, v0}, LX/3sp;->g(Z)V

    .line 644830
    invoke-virtual {v1}, LX/3sp;->c()I

    move-result v0

    invoke-virtual {p2, v0}, LX/3sp;->a(I)V

    .line 644831
    invoke-virtual {v1}, LX/3sp;->p()V

    .line 644832
    check-cast p1, Landroid/view/ViewGroup;

    .line 644833
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 644834
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_3

    .line 644835
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 644836
    invoke-static {v2}, LX/3tW;->m(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 644837
    sget-object v4, LX/3sp;->a:LX/3sg;

    iget-object p0, p2, LX/3sp;->b:Ljava/lang/Object;

    invoke-interface {v4, p0, v2}, LX/3sg;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 644838
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 644839
    :cond_3
    goto/16 :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .prologue
    .line 644798
    sget-boolean v0, LX/3tW;->c:Z

    if-nez v0, :cond_0

    invoke-static {p2}, LX/3tW;->m(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 644799
    :cond_0
    invoke-super {p0, p1, p2, p3}, LX/0vn;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    .line 644800
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 3

    .prologue
    .line 644778
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_1

    .line 644779
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    .line 644780
    iget-object v1, p0, LX/3tM;->b:LX/3tW;

    invoke-static {v1}, LX/3tW;->h(LX/3tW;)Landroid/view/View;

    move-result-object v1

    .line 644781
    if-eqz v1, :cond_0

    .line 644782
    iget-object v2, p0, LX/3tM;->b:LX/3tW;

    invoke-virtual {v2, v1}, LX/3tW;->c(Landroid/view/View;)I

    move-result v1

    .line 644783
    iget-object v2, p0, LX/3tM;->b:LX/3tW;

    .line 644784
    invoke-static {v2}, LX/0vv;->h(Landroid/view/View;)I

    move-result p0

    invoke-static {v1, p0}, LX/1uf;->a(II)I

    move-result p0

    .line 644785
    const/4 p1, 0x3

    if-ne p0, p1, :cond_2

    .line 644786
    iget-object p0, v2, LX/3tW;->z:Ljava/lang/CharSequence;

    .line 644787
    :goto_0
    move-object v1, p0

    .line 644788
    if-eqz v1, :cond_0

    .line 644789
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 644790
    :cond_0
    const/4 v0, 0x1

    .line 644791
    :goto_1
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, LX/0vn;->b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    goto :goto_1

    .line 644792
    :cond_2
    const/4 p1, 0x5

    if-ne p0, p1, :cond_3

    .line 644793
    iget-object p0, v2, LX/3tW;->A:Ljava/lang/CharSequence;

    goto :goto_0

    .line 644794
    :cond_3
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 644795
    invoke-super {p0, p1, p2}, LX/0vn;->d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 644796
    const-class v0, LX/3tW;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 644797
    return-void
.end method
