.class public final LX/3SG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0zS;

.field public final synthetic b:LX/0qd;


# direct methods
.method public constructor <init>(LX/0qd;LX/0zS;)V
    .locals 0

    .prologue
    .line 582111
    iput-object p1, p0, LX/3SG;->b:LX/0qd;

    iput-object p2, p0, LX/3SG;->a:LX/0zS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 582112
    iget-object v0, p0, LX/3SG;->b:LX/0qd;

    const/4 v1, 0x0

    .line 582113
    iput-boolean v1, v0, LX/0qd;->g:Z

    .line 582114
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 582115
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v8, 0x0

    .line 582116
    if-eqz p1, :cond_1

    .line 582117
    :try_start_0
    iget-object v1, p0, LX/3SG;->b:LX/0qd;

    .line 582118
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 582119
    check-cast v0, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel;

    .line 582120
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 582121
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel;->a()Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel;

    move-result-object v2

    if-nez v2, :cond_2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 582122
    :cond_0
    :goto_0
    iget-object v0, p0, LX/3SG;->b:LX/0qd;

    iget-object v1, p0, LX/3SG;->b:LX/0qd;

    iget-object v1, v1, LX/0qd;->e:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 582123
    iput-wide v2, v0, LX/0qd;->h:J

    .line 582124
    :cond_1
    iget-object v0, p0, LX/3SG;->b:LX/0qd;

    .line 582125
    iput-boolean v8, v0, LX/0qd;->g:Z

    .line 582126
    return-void

    .line 582127
    :catch_0
    move-exception v0

    .line 582128
    const-string v1, "ClashUnitDataMaintenanceHelper"

    const-string v2, "Error getting ClashUnit GraphQL result, cache_policy=\'%s\', data_freshness=\'%s\', client_time=\'%d\'"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, LX/3SG;->a:LX/0zS;

    iget-object v4, v4, LX/0zS;->f:Ljava/lang/String;

    aput-object v4, v3, v8

    const/4 v4, 0x1

    .line 582129
    iget-object v5, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v5, v5

    .line 582130
    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-wide v6, p1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 582131
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel;->a()Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_4

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel$EdgesModel;

    .line 582132
    invoke-virtual {v2}, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel$EdgesModel;->a()Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel$EdgesModel$NodeModel;

    move-result-object v2

    .line 582133
    if-eqz v2, :cond_3

    .line 582134
    invoke-virtual {v2}, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v7, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 582135
    :cond_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 582136
    :cond_4
    iget-object v2, v1, LX/0qd;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0qb;

    invoke-virtual {v2, v4}, LX/0qb;->a(Ljava/util/Map;)V

    goto :goto_0
.end method
