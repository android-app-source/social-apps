.class public final LX/4qW;
.super LX/3CB;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 814502
    const-class v0, Ljava/lang/Byte;

    invoke-direct {p0, v0}, LX/3CB;-><init>(Ljava/lang/Class;)V

    return-void
.end method

.method private c(Ljava/lang/String;LX/0n3;)Ljava/lang/Byte;
    .locals 2

    .prologue
    .line 814503
    invoke-static {p1}, LX/3CB;->a(Ljava/lang/String;)I

    move-result v0

    .line 814504
    const/16 v1, -0x80

    if-lt v0, v1, :cond_0

    const/16 v1, 0xff

    if-le v0, v1, :cond_1

    .line 814505
    :cond_0
    iget-object v0, p0, LX/3CB;->_keyClass:Ljava/lang/Class;

    const-string v1, "overflow, value can not be represented as 8-bit value"

    invoke-virtual {p2, v0, p1, v1}, LX/0n3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 814506
    :cond_1
    int-to-byte v0, v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic b(Ljava/lang/String;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 814507
    invoke-direct {p0, p1, p2}, LX/4qW;->c(Ljava/lang/String;LX/0n3;)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method
