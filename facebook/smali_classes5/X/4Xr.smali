.class public final LX/4Xr;
.super LX/0ur;
.source ""


# instance fields
.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 772535
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 772536
    instance-of v0, p0, LX/4Xr;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 772537
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;)LX/4Xr;
    .locals 2

    .prologue
    .line 772538
    new-instance v0, LX/4Xr;

    invoke-direct {v0}, LX/4Xr;-><init>()V

    .line 772539
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 772540
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;->a()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Xr;->b:LX/0Px;

    .line 772541
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    iput-object v1, v0, LX/4Xr;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 772542
    invoke-static {v0, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 772543
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;
    .locals 2

    .prologue
    .line 772544
    new-instance v0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;-><init>(LX/4Xr;)V

    .line 772545
    return-object v0
.end method
