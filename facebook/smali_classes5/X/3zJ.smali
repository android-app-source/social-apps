.class public final enum LX/3zJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3zJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3zJ;

.field public static final enum ADD_TO_TRAY:LX/3zJ;

.field public static final enum CLICK_FROM_TRAY:LX/3zJ;

.field public static final enum DISMISS_FROM_TRAY:LX/3zJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 662357
    new-instance v0, LX/3zJ;

    const-string v1, "ADD_TO_TRAY"

    invoke-direct {v0, v1, v2}, LX/3zJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3zJ;->ADD_TO_TRAY:LX/3zJ;

    .line 662358
    new-instance v0, LX/3zJ;

    const-string v1, "CLICK_FROM_TRAY"

    invoke-direct {v0, v1, v3}, LX/3zJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3zJ;->CLICK_FROM_TRAY:LX/3zJ;

    .line 662359
    new-instance v0, LX/3zJ;

    const-string v1, "DISMISS_FROM_TRAY"

    invoke-direct {v0, v1, v4}, LX/3zJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3zJ;->DISMISS_FROM_TRAY:LX/3zJ;

    .line 662360
    const/4 v0, 0x3

    new-array v0, v0, [LX/3zJ;

    sget-object v1, LX/3zJ;->ADD_TO_TRAY:LX/3zJ;

    aput-object v1, v0, v2

    sget-object v1, LX/3zJ;->CLICK_FROM_TRAY:LX/3zJ;

    aput-object v1, v0, v3

    sget-object v1, LX/3zJ;->DISMISS_FROM_TRAY:LX/3zJ;

    aput-object v1, v0, v4

    sput-object v0, LX/3zJ;->$VALUES:[LX/3zJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 662361
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3zJ;
    .locals 1

    .prologue
    .line 662362
    const-class v0, LX/3zJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3zJ;

    return-object v0
.end method

.method public static values()[LX/3zJ;
    .locals 1

    .prologue
    .line 662363
    sget-object v0, LX/3zJ;->$VALUES:[LX/3zJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3zJ;

    return-object v0
.end method
