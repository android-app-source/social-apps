.class public final LX/4rX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/4pS;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4pS",
            "<*>;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/Object;

.field public c:Z


# direct methods
.method public constructor <init>(LX/4pS;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4pS",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 816115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 816116
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4rX;->c:Z

    .line 816117
    iput-object p1, p0, LX/4rX;->a:LX/4pS;

    .line 816118
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 816119
    iget-object v0, p0, LX/4rX;->a:LX/4pS;

    invoke-virtual {v0, p1}, LX/4pS;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/4rX;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public final a(LX/0nX;LX/0my;LX/4rR;)Z
    .locals 2

    .prologue
    .line 816120
    iget-object v0, p0, LX/4rX;->b:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/4rX;->c:Z

    if-nez v0, :cond_0

    iget-boolean v0, p3, LX/4rR;->e:Z

    if-eqz v0, :cond_1

    .line 816121
    :cond_0
    iget-object v0, p3, LX/4rR;->d:Lcom/fasterxml/jackson/databind/JsonSerializer;

    iget-object v1, p0, LX/4rX;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1, p2}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 816122
    const/4 v0, 0x1

    .line 816123
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/0nX;LX/0my;LX/4rR;)V
    .locals 2

    .prologue
    .line 816124
    iget-object v0, p3, LX/4rR;->b:LX/0lb;

    .line 816125
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/4rX;->c:Z

    .line 816126
    if-eqz v0, :cond_0

    .line 816127
    invoke-virtual {p1, v0}, LX/0nX;->b(LX/0lc;)V

    .line 816128
    iget-object v0, p3, LX/4rR;->d:Lcom/fasterxml/jackson/databind/JsonSerializer;

    iget-object v1, p0, LX/4rX;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1, p2}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 816129
    :cond_0
    return-void
.end method
