.class public abstract enum LX/50g;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/50g;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/50g;

.field public static final enum ANY_PRESENT:LX/50g;

.field public static final enum FIRST_AFTER:LX/50g;

.field public static final enum FIRST_PRESENT:LX/50g;

.field public static final enum LAST_BEFORE:LX/50g;

.field public static final enum LAST_PRESENT:LX/50g;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 824011
    new-instance v0, LX/50h;

    const-string v1, "ANY_PRESENT"

    invoke-direct {v0, v1, v2}, LX/50h;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/50g;->ANY_PRESENT:LX/50g;

    .line 824012
    new-instance v0, LX/50i;

    const-string v1, "LAST_PRESENT"

    invoke-direct {v0, v1, v3}, LX/50i;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/50g;->LAST_PRESENT:LX/50g;

    .line 824013
    new-instance v0, LX/50j;

    const-string v1, "FIRST_PRESENT"

    invoke-direct {v0, v1, v4}, LX/50j;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/50g;->FIRST_PRESENT:LX/50g;

    .line 824014
    new-instance v0, LX/50k;

    const-string v1, "FIRST_AFTER"

    invoke-direct {v0, v1, v5}, LX/50k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/50g;->FIRST_AFTER:LX/50g;

    .line 824015
    new-instance v0, LX/50l;

    const-string v1, "LAST_BEFORE"

    invoke-direct {v0, v1, v6}, LX/50l;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/50g;->LAST_BEFORE:LX/50g;

    .line 824016
    const/4 v0, 0x5

    new-array v0, v0, [LX/50g;

    sget-object v1, LX/50g;->ANY_PRESENT:LX/50g;

    aput-object v1, v0, v2

    sget-object v1, LX/50g;->LAST_PRESENT:LX/50g;

    aput-object v1, v0, v3

    sget-object v1, LX/50g;->FIRST_PRESENT:LX/50g;

    aput-object v1, v0, v4

    sget-object v1, LX/50g;->FIRST_AFTER:LX/50g;

    aput-object v1, v0, v5

    sget-object v1, LX/50g;->LAST_BEFORE:LX/50g;

    aput-object v1, v0, v6

    sput-object v0, LX/50g;->$VALUES:[LX/50g;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 824010
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/50g;
    .locals 1

    .prologue
    .line 824009
    const-class v0, LX/50g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/50g;

    return-object v0
.end method

.method public static values()[LX/50g;
    .locals 1

    .prologue
    .line 824008
    sget-object v0, LX/50g;->$VALUES:[LX/50g;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/50g;

    return-object v0
.end method


# virtual methods
.method public abstract resultIndex(Ljava/util/Comparator;Ljava/lang/Object;Ljava/util/List;I)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TE;>;TE;",
            "Ljava/util/List",
            "<+TE;>;I)I"
        }
    .end annotation
.end method
