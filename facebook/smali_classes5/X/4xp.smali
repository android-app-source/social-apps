.class public final LX/4xp;
.super LX/0P5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0P5",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public a:LX/1Ek;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ek",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/4xr;


# direct methods
.method public constructor <init>(LX/4xr;LX/1Ek;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ek",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 821424
    iput-object p1, p0, LX/4xp;->b:LX/4xr;

    invoke-direct {p0}, LX/0P5;-><init>()V

    .line 821425
    iput-object p2, p0, LX/4xp;->a:LX/1Ek;

    .line 821426
    return-void
.end method


# virtual methods
.method public final getKey()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 821427
    iget-object v0, p0, LX/4xp;->a:LX/1Ek;

    iget-object v0, v0, LX/0P4;->key:Ljava/lang/Object;

    return-object v0
.end method

.method public final getValue()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 821428
    iget-object v0, p0, LX/4xp;->a:LX/1Ek;

    iget-object v0, v0, LX/0P4;->value:Ljava/lang/Object;

    return-object v0
.end method

.method public final setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)TV;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 821429
    iget-object v0, p0, LX/4xp;->a:LX/1Ek;

    iget-object v3, v0, LX/0P4;->value:Ljava/lang/Object;

    .line 821430
    invoke-static {p1}, LX/0PC;->a(Ljava/lang/Object;)I

    move-result v4

    .line 821431
    iget-object v0, p0, LX/4xp;->a:LX/1Ek;

    iget v0, v0, LX/1Ek;->valueHash:I

    if-ne v4, v0, :cond_0

    invoke-static {p1, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 821432
    :goto_0
    return-object p1

    .line 821433
    :cond_0
    iget-object v0, p0, LX/4xp;->b:LX/4xr;

    iget-object v0, v0, LX/4xr;->a:LX/1Ei;

    invoke-static {v0, p1, v4}, LX/1Ei;->b$redex0(LX/1Ei;Ljava/lang/Object;I)LX/1Ek;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    const-string v5, "value already present: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v5, v1}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 821434
    iget-object v0, p0, LX/4xp;->b:LX/4xr;

    iget-object v0, v0, LX/4xr;->a:LX/1Ei;

    iget-object v1, p0, LX/4xp;->a:LX/1Ek;

    invoke-static {v0, v1}, LX/1Ei;->a$redex0(LX/1Ei;LX/1Ek;)V

    .line 821435
    new-instance v0, LX/1Ek;

    iget-object v1, p0, LX/4xp;->a:LX/1Ek;

    iget-object v1, v1, LX/0P4;->key:Ljava/lang/Object;

    iget-object v2, p0, LX/4xp;->a:LX/1Ek;

    iget v2, v2, LX/1Ek;->keyHash:I

    invoke-direct {v0, v1, v2, p1, v4}, LX/1Ek;-><init>(Ljava/lang/Object;ILjava/lang/Object;I)V

    .line 821436
    iget-object v1, p0, LX/4xp;->b:LX/4xr;

    iget-object v1, v1, LX/4xr;->a:LX/1Ei;

    iget-object v2, p0, LX/4xp;->a:LX/1Ek;

    invoke-static {v1, v0, v2}, LX/1Ei;->a$redex0(LX/1Ei;LX/1Ek;LX/1Ek;)V

    .line 821437
    iget-object v1, p0, LX/4xp;->a:LX/1Ek;

    iput-object v6, v1, LX/1Ek;->prevInKeyInsertionOrder:LX/1Ek;

    .line 821438
    iget-object v1, p0, LX/4xp;->a:LX/1Ek;

    iput-object v6, v1, LX/1Ek;->nextInKeyInsertionOrder:LX/1Ek;

    .line 821439
    iget-object v1, p0, LX/4xp;->b:LX/4xr;

    iget-object v2, p0, LX/4xp;->b:LX/4xr;

    iget-object v2, v2, LX/4xr;->a:LX/1Ei;

    iget v2, v2, LX/1Ei;->g:I

    iput v2, v1, LX/4xr;->d:I

    .line 821440
    iget-object v1, p0, LX/4xp;->b:LX/4xr;

    iget-object v1, v1, LX/4xq;->c:LX/1Ek;

    iget-object v2, p0, LX/4xp;->a:LX/1Ek;

    if-ne v1, v2, :cond_1

    .line 821441
    iget-object v1, p0, LX/4xp;->b:LX/4xr;

    iput-object v0, v1, LX/4xr;->c:LX/1Ek;

    .line 821442
    :cond_1
    iput-object v0, p0, LX/4xp;->a:LX/1Ek;

    move-object p1, v3

    .line 821443
    goto :goto_0

    :cond_2
    move v0, v2

    .line 821444
    goto :goto_1
.end method
