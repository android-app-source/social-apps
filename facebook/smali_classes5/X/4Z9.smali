.class public final LX/4Z9;
.super LX/0ur;
.source ""


# instance fields
.field public b:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/model/GraphQLTimelineAppSection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLMediaSet;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;",
            ">;"
        }
    .end annotation
.end field

.field public p:Z

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 787252
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 787253
    instance-of v0, p0, LX/4Z9;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 787254
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;
    .locals 2

    .prologue
    .line 787255
    new-instance v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;-><init>(LX/4Z9;)V

    .line 787256
    return-object v0
.end method
