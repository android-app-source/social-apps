.class public LX/4Q3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 702692
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 702693
    const-wide/16 v8, 0x0

    .line 702694
    const/4 v6, 0x0

    .line 702695
    const/4 v5, 0x0

    .line 702696
    const/4 v4, 0x0

    .line 702697
    const/4 v3, 0x0

    .line 702698
    const/4 v2, 0x0

    .line 702699
    const/4 v1, 0x0

    .line 702700
    const/4 v0, 0x0

    .line 702701
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v10, :cond_a

    .line 702702
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 702703
    const/4 v0, 0x0

    .line 702704
    :goto_0
    return v0

    .line 702705
    :cond_0
    const-string v4, "id"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 702706
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v11, v1

    .line 702707
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 702708
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 702709
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 702710
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v1, :cond_1

    .line 702711
    const-string v4, "creation_time"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 702712
    const/4 v0, 0x1

    .line 702713
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto :goto_1

    .line 702714
    :cond_2
    const-string v4, "offer"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 702715
    invoke-static {p0, p1}, LX/4Q2;->a(LX/15w;LX/186;)I

    move-result v1

    move v10, v1

    goto :goto_1

    .line 702716
    :cond_3
    const-string v4, "photos"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 702717
    invoke-static {p0, p1}, LX/2sY;->b(LX/15w;LX/186;)I

    move-result v1

    move v9, v1

    goto :goto_1

    .line 702718
    :cond_4
    const-string v4, "root_share_story"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 702719
    invoke-static {p0, p1}, LX/2aD;->a(LX/15w;LX/186;)I

    move-result v1

    move v8, v1

    goto :goto_1

    .line 702720
    :cond_5
    const-string v4, "url"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 702721
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v7, v1

    goto :goto_1

    .line 702722
    :cond_6
    const-string v4, "videos"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 702723
    invoke-static {p0, p1}, LX/4UG;->b(LX/15w;LX/186;)I

    move-result v1

    move v6, v1

    goto :goto_1

    .line 702724
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 702725
    :cond_8
    const/4 v1, 0x7

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 702726
    if-eqz v0, :cond_9

    .line 702727
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 702728
    :cond_9
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 702729
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 702730
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 702731
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 702732
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 702733
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 702734
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_a
    move v7, v2

    move v10, v5

    move v11, v6

    move v6, v1

    move v12, v3

    move-wide v2, v8

    move v9, v4

    move v8, v12

    goto/16 :goto_1
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 702686
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 702687
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 702688
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 702689
    invoke-static {p0, p1}, LX/4Q3;->a(LX/15w;LX/186;)I

    move-result v1

    .line 702690
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 702691
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 702655
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 702656
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 702657
    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 702658
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702659
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 702660
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 702661
    if-eqz v0, :cond_1

    .line 702662
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702663
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 702664
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 702665
    if-eqz v0, :cond_2

    .line 702666
    const-string v1, "offer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702667
    invoke-static {p0, v0, p2, p3}, LX/4Q2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 702668
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 702669
    if-eqz v0, :cond_3

    .line 702670
    const-string v1, "photos"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702671
    invoke-static {p0, v0, p2, p3}, LX/2sY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 702672
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 702673
    if-eqz v0, :cond_4

    .line 702674
    const-string v1, "root_share_story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702675
    invoke-static {p0, v0, p2, p3}, LX/2aD;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 702676
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 702677
    if-eqz v0, :cond_5

    .line 702678
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702679
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 702680
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 702681
    if-eqz v0, :cond_6

    .line 702682
    const-string v1, "videos"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702683
    invoke-static {p0, v0, p2, p3}, LX/4UG;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 702684
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 702685
    return-void
.end method
