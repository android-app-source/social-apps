.class public LX/4AP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/content/res/Resources;

.field private b:LX/1br;

.field private c:LX/1c3;

.field private d:Ljava/util/concurrent/Executor;

.field private e:LX/1Fh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "LX/1ln;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/1c9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1c9",
            "<",
            "LX/1c8;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:LX/1Gd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Gd",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/1br;LX/1c3;Ljava/util/concurrent/Executor;LX/1Fh;LX/1c9;LX/1Gd;)V
    .locals 0
    .param p6    # LX/1c9;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/1Gd;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/1br;",
            "Lcom/facebook/imagepipeline/animated/factory/AnimatedDrawableFactory;",
            "Ljava/util/concurrent/Executor;",
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "LX/1ln;",
            ">;",
            "LX/1c9",
            "<",
            "LX/1c8;",
            ">;",
            "LX/1Gd",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 676160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 676161
    iput-object p1, p0, LX/4AP;->a:Landroid/content/res/Resources;

    .line 676162
    iput-object p2, p0, LX/4AP;->b:LX/1br;

    .line 676163
    iput-object p3, p0, LX/4AP;->c:LX/1c3;

    .line 676164
    iput-object p4, p0, LX/4AP;->d:Ljava/util/concurrent/Executor;

    .line 676165
    iput-object p5, p0, LX/4AP;->e:LX/1Fh;

    .line 676166
    iput-object p6, p0, LX/4AP;->f:LX/1c9;

    .line 676167
    iput-object p7, p0, LX/4AP;->g:LX/1Gd;

    .line 676168
    return-void
.end method


# virtual methods
.method public final a(LX/1Gd;Ljava/lang/String;LX/1bh;Ljava/lang/Object;)LX/1bo;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;",
            "Ljava/lang/String;",
            "LX/1bh;",
            "Ljava/lang/Object;",
            ")",
            "LX/1bo;"
        }
    .end annotation

    .prologue
    .line 676169
    new-instance v0, LX/1bo;

    iget-object v1, p0, LX/4AP;->a:Landroid/content/res/Resources;

    iget-object v2, p0, LX/4AP;->b:LX/1br;

    iget-object v3, p0, LX/4AP;->c:LX/1c3;

    iget-object v4, p0, LX/4AP;->d:Ljava/util/concurrent/Executor;

    iget-object v5, p0, LX/4AP;->e:LX/1Fh;

    iget-object v10, p0, LX/4AP;->f:LX/1c9;

    move-object v6, p1

    move-object v7, p2

    move-object v8, p3

    move-object v9, p4

    invoke-direct/range {v0 .. v10}, LX/1bo;-><init>(Landroid/content/res/Resources;LX/1br;LX/1c3;Ljava/util/concurrent/Executor;LX/1Fh;LX/1Gd;Ljava/lang/String;LX/1bh;Ljava/lang/Object;LX/1c9;)V

    .line 676170
    iget-object v1, p0, LX/4AP;->g:LX/1Gd;

    if-eqz v1, :cond_0

    .line 676171
    iget-object v1, p0, LX/4AP;->g:LX/1Gd;

    invoke-interface {v1}, LX/1Gd;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 676172
    iput-boolean v1, v0, LX/1bo;->h:Z

    .line 676173
    :cond_0
    return-object v0
.end method
