.class public final enum LX/4nX;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4nX;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4nX;

.field public static final enum COMMERCE_FAQ_ENABLED:LX/4nX;

.field public static final enum COMMERCE_NUX_ENABLED:LX/4nX;

.field public static final enum IN_MESSENGER_SHOPPING_ENABLED:LX/4nX;

.field public static final enum NULL_STATE_CTA_BUTTON_ALWAYS_ENABLED:LX/4nX;

.field public static final enum STRUCTURED_MENU_ENABLED:LX/4nX;

.field public static final enum USER_CONTROL_TOPIC_MANAGE_ENABLED:LX/4nX;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 806978
    new-instance v0, LX/4nX;

    const-string v1, "COMMERCE_FAQ_ENABLED"

    invoke-direct {v0, v1, v3}, LX/4nX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4nX;->COMMERCE_FAQ_ENABLED:LX/4nX;

    .line 806979
    new-instance v0, LX/4nX;

    const-string v1, "IN_MESSENGER_SHOPPING_ENABLED"

    invoke-direct {v0, v1, v4}, LX/4nX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4nX;->IN_MESSENGER_SHOPPING_ENABLED:LX/4nX;

    .line 806980
    new-instance v0, LX/4nX;

    const-string v1, "COMMERCE_NUX_ENABLED"

    invoke-direct {v0, v1, v5}, LX/4nX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4nX;->COMMERCE_NUX_ENABLED:LX/4nX;

    .line 806981
    new-instance v0, LX/4nX;

    const-string v1, "STRUCTURED_MENU_ENABLED"

    invoke-direct {v0, v1, v6}, LX/4nX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4nX;->STRUCTURED_MENU_ENABLED:LX/4nX;

    .line 806982
    new-instance v0, LX/4nX;

    const-string v1, "USER_CONTROL_TOPIC_MANAGE_ENABLED"

    invoke-direct {v0, v1, v7}, LX/4nX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4nX;->USER_CONTROL_TOPIC_MANAGE_ENABLED:LX/4nX;

    .line 806983
    new-instance v0, LX/4nX;

    const-string v1, "NULL_STATE_CTA_BUTTON_ALWAYS_ENABLED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/4nX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4nX;->NULL_STATE_CTA_BUTTON_ALWAYS_ENABLED:LX/4nX;

    .line 806984
    const/4 v0, 0x6

    new-array v0, v0, [LX/4nX;

    sget-object v1, LX/4nX;->COMMERCE_FAQ_ENABLED:LX/4nX;

    aput-object v1, v0, v3

    sget-object v1, LX/4nX;->IN_MESSENGER_SHOPPING_ENABLED:LX/4nX;

    aput-object v1, v0, v4

    sget-object v1, LX/4nX;->COMMERCE_NUX_ENABLED:LX/4nX;

    aput-object v1, v0, v5

    sget-object v1, LX/4nX;->STRUCTURED_MENU_ENABLED:LX/4nX;

    aput-object v1, v0, v6

    sget-object v1, LX/4nX;->USER_CONTROL_TOPIC_MANAGE_ENABLED:LX/4nX;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/4nX;->NULL_STATE_CTA_BUTTON_ALWAYS_ENABLED:LX/4nX;

    aput-object v2, v0, v1

    sput-object v0, LX/4nX;->$VALUES:[LX/4nX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 806985
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4nX;
    .locals 1

    .prologue
    .line 806986
    const-class v0, LX/4nX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4nX;

    return-object v0
.end method

.method public static values()[LX/4nX;
    .locals 1

    .prologue
    .line 806987
    sget-object v0, LX/4nX;->$VALUES:[LX/4nX;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4nX;

    return-object v0
.end method
