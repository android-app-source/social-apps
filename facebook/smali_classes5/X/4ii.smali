.class public LX/4ii;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final mMessage:Ljava/lang/String;

.field private final mTimeStamp:J


# direct methods
.method public constructor <init>(JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 803469
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 803470
    iput-wide p1, p0, LX/4ii;->mTimeStamp:J

    .line 803471
    iput-object p3, p0, LX/4ii;->mMessage:Ljava/lang/String;

    .line 803472
    return-void
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 803467
    iget-object v0, p0, LX/4ii;->mMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeStamp()J
    .locals 2

    .prologue
    .line 803468
    iget-wide v0, p0, LX/4ii;->mTimeStamp:J

    return-wide v0
.end method
