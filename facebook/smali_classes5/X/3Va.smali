.class public abstract LX/3Va;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements LX/2yW;
.implements LX/2yX;
.implements LX/2yY;


# instance fields
.field public final j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final k:Landroid/widget/TextView;

.field private final l:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 587973
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 587974
    invoke-virtual {p0, p2}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 587975
    const v0, 0x7f0d2661

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/3Va;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 587976
    const v0, 0x7f0d052c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3Va;->k:Landroid/widget/TextView;

    .line 587977
    const v0, 0x7f0d052d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3Va;->l:Landroid/widget/TextView;

    .line 587978
    return-void
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 587979
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 587980
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 587981
    return-void

    .line 587982
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 587983
    return-void
.end method

.method public setContextText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587984
    iget-object v0, p0, LX/3Va;->l:Landroid/widget/TextView;

    invoke-static {v0, p1}, LX/3Va;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 587985
    return-void
.end method

.method public setSideImageController(LX/1aZ;)V
    .locals 2
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587986
    iget-object v1, p0, LX/3Va;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 587987
    iget-object v0, p0, LX/3Va;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 587988
    return-void

    .line 587989
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587990
    iget-object v0, p0, LX/3Va;->k:Landroid/widget/TextView;

    invoke-static {v0, p1}, LX/3Va;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 587991
    return-void
.end method
