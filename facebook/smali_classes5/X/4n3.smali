.class public final enum LX/4n3;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4n3;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4n3;

.field public static final enum ANIMATED:LX/4n3;

.field public static final enum BITMAP:LX/4n3;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 806471
    new-instance v0, LX/4n3;

    const-string v1, "BITMAP"

    invoke-direct {v0, v1, v2}, LX/4n3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4n3;->BITMAP:LX/4n3;

    .line 806472
    new-instance v0, LX/4n3;

    const-string v1, "ANIMATED"

    invoke-direct {v0, v1, v3}, LX/4n3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4n3;->ANIMATED:LX/4n3;

    .line 806473
    const/4 v0, 0x2

    new-array v0, v0, [LX/4n3;

    sget-object v1, LX/4n3;->BITMAP:LX/4n3;

    aput-object v1, v0, v2

    sget-object v1, LX/4n3;->ANIMATED:LX/4n3;

    aput-object v1, v0, v3

    sput-object v0, LX/4n3;->$VALUES:[LX/4n3;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 806474
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4n3;
    .locals 1

    .prologue
    .line 806475
    const-class v0, LX/4n3;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4n3;

    return-object v0
.end method

.method public static values()[LX/4n3;
    .locals 1

    .prologue
    .line 806476
    sget-object v0, LX/4n3;->$VALUES:[LX/4n3;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4n3;

    return-object v0
.end method
