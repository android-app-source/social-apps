.class public LX/4q3;
.super LX/32s;
.source ""


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final _annotated:LX/2Vd;

.field public final _creatorIndex:I

.field public final _injectableValueId:Ljava/lang/Object;


# direct methods
.method private constructor <init>(LX/4q3;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4q3;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 812546
    invoke-direct {p0, p1, p2}, LX/32s;-><init>(LX/32s;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 812547
    iget-object v0, p1, LX/4q3;->_annotated:LX/2Vd;

    iput-object v0, p0, LX/4q3;->_annotated:LX/2Vd;

    .line 812548
    iget v0, p1, LX/4q3;->_creatorIndex:I

    iput v0, p0, LX/4q3;->_creatorIndex:I

    .line 812549
    iget-object v0, p1, LX/4q3;->_injectableValueId:Ljava/lang/Object;

    iput-object v0, p0, LX/4q3;->_injectableValueId:Ljava/lang/Object;

    .line 812550
    return-void
.end method

.method private constructor <init>(LX/4q3;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 812557
    invoke-direct {p0, p1, p2}, LX/32s;-><init>(LX/32s;Ljava/lang/String;)V

    .line 812558
    iget-object v0, p1, LX/4q3;->_annotated:LX/2Vd;

    iput-object v0, p0, LX/4q3;->_annotated:LX/2Vd;

    .line 812559
    iget v0, p1, LX/4q3;->_creatorIndex:I

    iput v0, p0, LX/4q3;->_creatorIndex:I

    .line 812560
    iget-object v0, p1, LX/4q3;->_injectableValueId:Ljava/lang/Object;

    iput-object v0, p0, LX/4q3;->_injectableValueId:Ljava/lang/Object;

    .line 812561
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0lJ;LX/2Vb;LX/4qw;LX/0lQ;LX/2Vd;ILjava/lang/Object;Z)V
    .locals 7

    .prologue
    .line 812552
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move/from16 v6, p9

    invoke-direct/range {v0 .. v6}, LX/32s;-><init>(Ljava/lang/String;LX/0lJ;LX/2Vb;LX/4qw;LX/0lQ;Z)V

    .line 812553
    iput-object p6, p0, LX/4q3;->_annotated:LX/2Vd;

    .line 812554
    iput p7, p0, LX/4q3;->_creatorIndex:I

    .line 812555
    iput-object p8, p0, LX/4q3;->_injectableValueId:Ljava/lang/Object;

    .line 812556
    return-void
.end method

.method private c(Ljava/lang/String;)LX/4q3;
    .locals 1

    .prologue
    .line 812551
    new-instance v0, LX/4q3;

    invoke-direct {v0, p0, p1}, LX/4q3;-><init>(LX/4q3;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;)LX/32s;
    .locals 1

    .prologue
    .line 812545
    invoke-direct {p0, p1}, LX/4q3;->c(Ljava/lang/String;)LX/4q3;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/4q3;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)",
            "LX/4q3;"
        }
    .end annotation

    .prologue
    .line 812544
    new-instance v0, LX/4q3;

    invoke-direct {v0, p0, p1}, LX/4q3;-><init>(LX/4q3;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    return-object v0
.end method

.method public final a(LX/15w;LX/0n3;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 812542
    invoke-virtual {p0, p1, p2}, LX/32s;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, p3, v0}, LX/32s;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 812543
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 812562
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Method should never be called on a "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b()LX/2An;
    .locals 1

    .prologue
    .line 812541
    iget-object v0, p0, LX/4q3;->_annotated:LX/2Vd;

    return-object v0
.end method

.method public final synthetic b(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/32s;
    .locals 1

    .prologue
    .line 812540
    invoke-virtual {p0, p1}, LX/4q3;->a(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/4q3;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 812539
    invoke-virtual {p0, p1, p2}, LX/32s;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, p3, v0}, LX/32s;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 812533
    return-object p1
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 812538
    iget v0, p0, LX/4q3;->_creatorIndex:I

    return v0
.end method

.method public final d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 812537
    iget-object v0, p0, LX/4q3;->_injectableValueId:Ljava/lang/Object;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 812534
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[creator property, name \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 812535
    iget-object v1, p0, LX/32s;->_propName:Ljava/lang/String;

    move-object v1, v1

    .line 812536
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'; inject id \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/4q3;->_injectableValueId:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\']"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
