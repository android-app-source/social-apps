.class public LX/4gu;
.super Ljava/lang/Exception;
.source ""


# direct methods
.method public constructor <init>(LX/1MF;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 800265
    invoke-interface {p1}, LX/1MF;->getOperationType()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, LX/4gu;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 800266
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 800267
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "An operation of type "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " was attempted while offline"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 800268
    return-void
.end method
