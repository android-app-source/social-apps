.class public final LX/4Xk;
.super LX/0ur;
.source ""


# instance fields
.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 772265
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 772266
    instance-of v0, p0, LX/4Xk;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 772267
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;)LX/4Xk;
    .locals 2

    .prologue
    .line 772257
    new-instance v0, LX/4Xk;

    invoke-direct {v0}, LX/4Xk;-><init>()V

    .line 772258
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 772259
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;->a()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Xk;->b:LX/0Px;

    .line 772260
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    iput-object v1, v0, LX/4Xk;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 772261
    invoke-static {v0, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 772262
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;
    .locals 2

    .prologue
    .line 772263
    new-instance v0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;-><init>(LX/4Xk;)V

    .line 772264
    return-object v0
.end method
