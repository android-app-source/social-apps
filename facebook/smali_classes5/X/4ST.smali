.class public LX/4ST;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 714584
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 714608
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_7

    .line 714609
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 714610
    :goto_0
    return v0

    .line 714611
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 714612
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_6

    .line 714613
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 714614
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 714615
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 714616
    const-string v6, "answer"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 714617
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 714618
    :cond_2
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 714619
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 714620
    :cond_3
    const-string v6, "next_question"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 714621
    invoke-static {p0, p1}, LX/4SS;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 714622
    :cond_4
    const-string v6, "response_responders"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 714623
    invoke-static {p0, p1}, LX/4SW;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 714624
    :cond_5
    const-string v6, "url"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 714625
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 714626
    :cond_6
    const/4 v5, 0x6

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 714627
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 714628
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 714629
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 714630
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 714631
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 714632
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_7
    move v1, v0

    move v2, v0

    move v3, v0

    move v4, v0

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 714585
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 714586
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 714587
    if-eqz v0, :cond_0

    .line 714588
    const-string v1, "answer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714589
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 714590
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 714591
    if-eqz v0, :cond_1

    .line 714592
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714593
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 714594
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 714595
    if-eqz v0, :cond_2

    .line 714596
    const-string v1, "next_question"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714597
    invoke-static {p0, v0, p2, p3}, LX/4SS;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 714598
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 714599
    if-eqz v0, :cond_3

    .line 714600
    const-string v1, "response_responders"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714601
    invoke-static {p0, v0, p2}, LX/4SW;->a(LX/15i;ILX/0nX;)V

    .line 714602
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 714603
    if-eqz v0, :cond_4

    .line 714604
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714605
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 714606
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 714607
    return-void
.end method
