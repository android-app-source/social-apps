.class public LX/3eX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/stickers/service/StickerPacksHandler;

.field public final b:LX/3dt;

.field public final c:LX/3dx;

.field public final d:LX/3eY;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/service/StickerPacksHandler;LX/3dt;LX/3dx;LX/3eY;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 620291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 620292
    iput-object p1, p0, LX/3eX;->a:Lcom/facebook/stickers/service/StickerPacksHandler;

    .line 620293
    iput-object p2, p0, LX/3eX;->b:LX/3dt;

    .line 620294
    iput-object p3, p0, LX/3eX;->c:LX/3dx;

    .line 620295
    iput-object p4, p0, LX/3eX;->d:LX/3eY;

    .line 620296
    return-void
.end method


# virtual methods
.method public final a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 620297
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 620298
    const-string v2, "stickerPack"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 620299
    iget-object v2, p0, LX/3eX;->c:LX/3dx;

    invoke-virtual {v2, v0}, LX/3dx;->b(Lcom/facebook/stickers/model/StickerPack;)V

    .line 620300
    iget-object v2, p0, LX/3eX;->b:LX/3dt;

    invoke-virtual {v2}, LX/3dt;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 620301
    iget-object v2, p0, LX/3eX;->b:LX/3dt;

    invoke-virtual {v2}, LX/3dt;->c()LX/0Px;

    move-result-object v3

    .line 620302
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v2}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v4

    .line 620303
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/model/StickerPack;

    .line 620304
    iget-object v6, v1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v6, v6

    .line 620305
    iget-object v7, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v7, v7

    .line 620306
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 620307
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 620308
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 620309
    :cond_1
    iget-object v0, p0, LX/3eX;->b:LX/3dt;

    invoke-virtual {v0, v4}, LX/3dt;->b(Ljava/util/List;)V

    .line 620310
    :cond_2
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 620311
    return-object v0
.end method
