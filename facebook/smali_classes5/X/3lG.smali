.class public LX/3lG;
.super LX/3ko;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/3kp;

.field public c:LX/0hs;

.field public d:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/3kp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 633700
    invoke-direct {p0}, LX/3ko;-><init>()V

    .line 633701
    iput-object p1, p0, LX/3lG;->a:Landroid/content/Context;

    .line 633702
    iput-object p2, p0, LX/3lG;->b:LX/3kp;

    .line 633703
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 633697
    iget-object v0, p0, LX/3lG;->b:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 633698
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    .line 633699
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 633696
    const-string v0, "4369"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 633683
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PHOTO_PICKER_HIGHLIGHT_CLUSTER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 633694
    const/4 v0, 0x0

    iput-object v0, p0, LX/3lG;->d:Landroid/view/View;

    .line 633695
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 633684
    new-instance v0, LX/BIA;

    iget-object v1, p0, LX/3lG;->a:Landroid/content/Context;

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/BIA;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, LX/3lG;->c:LX/0hs;

    .line 633685
    iget-object v0, p0, LX/3lG;->c:LX/0hs;

    const/4 v1, -0x1

    .line 633686
    iput v1, v0, LX/0hs;->t:I

    .line 633687
    iget-object v0, p0, LX/3lG;->c:LX/0hs;

    new-instance v1, LX/BI1;

    invoke-direct {v1, p0}, LX/BI1;-><init>(LX/3lG;)V

    invoke-virtual {v0, v1}, LX/0hs;->a(LX/5Od;)V

    .line 633688
    iget-object v0, p0, LX/3lG;->c:LX/0hs;

    new-instance v1, LX/BI2;

    invoke-direct {v1, p0}, LX/BI2;-><init>(LX/3lG;)V

    .line 633689
    iput-object v1, v0, LX/0ht;->H:LX/2dD;

    .line 633690
    iget-object v0, p0, LX/3lG;->c:LX/0hs;

    sget-object v1, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 633691
    iget-object v0, p0, LX/3lG;->c:LX/0hs;

    iget-object v1, p0, LX/3lG;->a:Landroid/content/Context;

    const v2, 0x7f081375

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 633692
    iget-object v0, p0, LX/3lG;->c:LX/0hs;

    iget-object v1, p0, LX/3lG;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0ht;->f(Landroid/view/View;)V

    .line 633693
    return-void
.end method
