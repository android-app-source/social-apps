.class public LX/3wd;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source ""


# instance fields
.field public g:F

.field public h:I


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 655668
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 655669
    const/4 v0, -0x1

    iput v0, p0, LX/3wd;->h:I

    .line 655670
    const/4 v0, 0x0

    iput v0, p0, LX/3wd;->g:F

    .line 655671
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 655675
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 655676
    iput v3, p0, LX/3wd;->h:I

    .line 655677
    sget-object v0, LX/03r;->LinearLayoutCompat_Layout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 655678
    const/16 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, LX/3wd;->g:F

    .line 655679
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, LX/3wd;->h:I

    .line 655680
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 655681
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 655672
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 655673
    const/4 v0, -0x1

    iput v0, p0, LX/3wd;->h:I

    .line 655674
    return-void
.end method
