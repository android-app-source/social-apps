.class public final LX/505;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1sm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1sm",
            "<TE;>;"
        }
    .end annotation
.end field

.field public b:LX/505;
    .annotation build Lcom/google/j2objc/annotations/Weak;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/508",
            "<TE;>.Heap;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/508;


# direct methods
.method public constructor <init>(LX/508;LX/1sm;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1sm",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 823315
    iput-object p1, p0, LX/505;->c:LX/508;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 823316
    iput-object p2, p0, LX/505;->a:LX/1sm;

    .line 823317
    return-void
.end method

.method public static b(LX/505;II)I
    .locals 4

    .prologue
    .line 823306
    iget-object v0, p0, LX/505;->c:LX/508;

    iget v0, v0, LX/508;->e:I

    if-lt p1, v0, :cond_1

    .line 823307
    const/4 v0, -0x1

    .line 823308
    :cond_0
    return v0

    .line 823309
    :cond_1
    if-lez p1, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 823310
    iget-object v0, p0, LX/505;->c:LX/508;

    iget v0, v0, LX/508;->e:I

    sub-int/2addr v0, p2

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int v2, v0, p2

    .line 823311
    add-int/lit8 v1, p1, 0x1

    move v0, p1

    :goto_1
    if-ge v1, v2, :cond_0

    .line 823312
    invoke-virtual {p0, v1, v0}, LX/505;->a(II)I

    move-result v3

    if-gez v3, :cond_2

    move v0, v1

    .line 823313
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 823314
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/505;ILjava/lang/Object;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 823290
    if-nez p1, :cond_0

    .line 823291
    iget-object v0, p0, LX/505;->c:LX/508;

    iget-object v0, v0, LX/508;->d:[Ljava/lang/Object;

    aput-object p2, v0, v1

    .line 823292
    :goto_0
    return v1

    .line 823293
    :cond_0
    invoke-static {p1}, LX/505;->f(I)I

    move-result v3

    .line 823294
    iget-object v0, p0, LX/505;->c:LX/508;

    invoke-virtual {v0, v3}, LX/508;->a(I)Ljava/lang/Object;

    move-result-object v1

    .line 823295
    if-eqz v3, :cond_2

    .line 823296
    invoke-static {v3}, LX/505;->f(I)I

    move-result v0

    .line 823297
    invoke-static {v0}, LX/505;->e(I)I

    move-result v2

    .line 823298
    if-eq v2, v3, :cond_2

    invoke-static {v2}, LX/505;->d(I)I

    move-result v0

    iget-object v4, p0, LX/505;->c:LX/508;

    iget v4, v4, LX/508;->e:I

    if-lt v0, v4, :cond_2

    .line 823299
    iget-object v0, p0, LX/505;->c:LX/508;

    invoke-virtual {v0, v2}, LX/508;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 823300
    iget-object v4, p0, LX/505;->a:LX/1sm;

    invoke-virtual {v4, v0, v1}, LX/1sm;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v4

    if-gez v4, :cond_2

    move v1, v2

    .line 823301
    :goto_1
    iget-object v2, p0, LX/505;->a:LX/1sm;

    invoke-virtual {v2, v0, p2}, LX/1sm;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-gez v2, :cond_1

    .line 823302
    iget-object v2, p0, LX/505;->c:LX/508;

    iget-object v2, v2, LX/508;->d:[Ljava/lang/Object;

    aput-object v0, v2, p1

    .line 823303
    iget-object v0, p0, LX/505;->c:LX/508;

    iget-object v0, v0, LX/508;->d:[Ljava/lang/Object;

    aput-object p2, v0, v1

    goto :goto_0

    .line 823304
    :cond_1
    iget-object v0, p0, LX/505;->c:LX/508;

    iget-object v0, v0, LX/508;->d:[Ljava/lang/Object;

    aput-object p2, v0, p1

    move v1, p1

    .line 823305
    goto :goto_0

    :cond_2
    move-object v0, v1

    move v1, v3

    goto :goto_1
.end method

.method public static d(I)I
    .locals 1

    .prologue
    .line 823289
    mul-int/lit8 v0, p0, 0x2

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public static e(I)I
    .locals 1

    .prologue
    .line 823318
    mul-int/lit8 v0, p0, 0x2

    add-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public static f(I)I
    .locals 1

    .prologue
    .line 823271
    add-int/lit8 v0, p0, -0x1

    div-int/lit8 v0, v0, 0x2

    return v0
.end method


# virtual methods
.method public final a(I)I
    .locals 3

    .prologue
    .line 823272
    :goto_0
    invoke-static {p1}, LX/505;->d(I)I

    move-result v0

    .line 823273
    if-gez v0, :cond_1

    .line 823274
    const/4 v0, -0x1

    .line 823275
    :goto_1
    move v0, v0

    .line 823276
    if-lez v0, :cond_0

    .line 823277
    iget-object v1, p0, LX/505;->c:LX/508;

    iget-object v1, v1, LX/508;->d:[Ljava/lang/Object;

    iget-object v2, p0, LX/505;->c:LX/508;

    invoke-virtual {v2, v0}, LX/508;->a(I)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v1, p1

    move p1, v0

    .line 823278
    goto :goto_0

    .line 823279
    :cond_0
    return p1

    :cond_1
    invoke-static {v0}, LX/505;->d(I)I

    move-result v0

    const/4 v1, 0x4

    invoke-static {p0, v0, v1}, LX/505;->b(LX/505;II)I

    move-result v0

    goto :goto_1
.end method

.method public final a(II)I
    .locals 3

    .prologue
    .line 823280
    iget-object v0, p0, LX/505;->a:LX/1sm;

    iget-object v1, p0, LX/505;->c:LX/508;

    invoke-virtual {v1, p1}, LX/508;->a(I)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LX/505;->c:LX/508;

    invoke-virtual {v2, p2}, LX/508;->a(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1sm;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final b(ILjava/lang/Object;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)I"
        }
    .end annotation

    .prologue
    .line 823281
    :goto_0
    const/4 v0, 0x2

    if-le p1, v0, :cond_0

    .line 823282
    invoke-static {p1}, LX/505;->f(I)I

    move-result v0

    invoke-static {v0}, LX/505;->f(I)I

    move-result v0

    move v0, v0

    .line 823283
    iget-object v1, p0, LX/505;->c:LX/508;

    invoke-virtual {v1, v0}, LX/508;->a(I)Ljava/lang/Object;

    move-result-object v1

    .line 823284
    iget-object v2, p0, LX/505;->a:LX/1sm;

    invoke-virtual {v2, v1, p2}, LX/1sm;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_0

    .line 823285
    iget-object v2, p0, LX/505;->c:LX/508;

    iget-object v2, v2, LX/508;->d:[Ljava/lang/Object;

    aput-object v1, v2, p1

    move p1, v0

    .line 823286
    goto :goto_0

    .line 823287
    :cond_0
    iget-object v0, p0, LX/505;->c:LX/508;

    iget-object v0, v0, LX/508;->d:[Ljava/lang/Object;

    aput-object p2, v0, p1

    .line 823288
    return p1
.end method
