.class public final LX/3z1;
.super LX/1Mt;
.source ""


# instance fields
.field public final synthetic a:Landroid/os/Bundle;

.field public final synthetic b:I

.field public final synthetic c:Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;Landroid/os/Bundle;I)V
    .locals 0

    .prologue
    .line 661971
    iput-object p1, p0, LX/3z1;->c:Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;

    iput-object p2, p0, LX/3z1;->a:Landroid/os/Bundle;

    iput p3, p0, LX/3z1;->b:I

    invoke-direct {p0}, LX/1Mt;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 661972
    sget-object v0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->p:Ljava/lang/Class;

    const-string v1, "Failed to fetch QEs"

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 661973
    iget-object v0, p0, LX/3z1;->c:Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;

    iget-object v0, v0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->o:LX/0kL;

    new-instance v1, LX/27k;

    const-string v2, "Failed to fetch QEs"

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 661974
    iget-object v0, p0, LX/3z1;->c:Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;

    const/4 v1, 0x1

    .line 661975
    iput-boolean v1, v0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->r:Z

    .line 661976
    iget-object v0, p0, LX/3z1;->c:Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;

    invoke-static {v0}, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->l(Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;)V

    .line 661977
    return-void
.end method

.method public final onSuccessfulResult(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 3

    .prologue
    .line 661978
    iget-object v0, p0, LX/3z1;->c:Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;

    iget-object v1, p0, LX/3z1;->a:Landroid/os/Bundle;

    iget v2, p0, LX/3z1;->b:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->a$redex0(Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;Landroid/os/Bundle;I)V

    .line 661979
    return-void
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 661980
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {p0, p1}, LX/3z1;->onSuccessfulResult(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
