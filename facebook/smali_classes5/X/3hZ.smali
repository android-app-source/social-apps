.class public LX/3hZ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 627077
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 627078
    if-eqz p0, :cond_0

    invoke-static {p0}, LX/17E;->i(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v2, v4

    .line 627079
    :cond_1
    :goto_0
    return v2

    .line 627080
    :cond_2
    invoke-static {p0}, LX/17E;->j(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    move v3, v2

    :cond_3
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 627081
    invoke-static {v0}, LX/3hZ;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 627082
    add-int/lit8 v3, v3, 0x1

    .line 627083
    const/4 v6, 0x0

    .line 627084
    invoke-static {v0}, LX/3hZ;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 627085
    :cond_4
    :goto_2
    move v0, v6

    .line 627086
    if-eqz v0, :cond_6

    .line 627087
    add-int/lit8 v0, v1, 0x1

    :goto_3
    move v1, v0

    .line 627088
    goto :goto_1

    .line 627089
    :cond_5
    if-ne v3, v1, :cond_1

    move v2, v4

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_3

    .line 627090
    :cond_7
    invoke-static {v0}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v7

    .line 627091
    if-eqz v7, :cond_4

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->w()I

    move-result p0

    if-lez p0, :cond_4

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->v()I

    move-result v7

    if-lez v7, :cond_4

    const/4 v6, 0x1

    goto :goto_2
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 627092
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-nez v1, :cond_1

    .line 627093
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x4984e12

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
