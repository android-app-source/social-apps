.class public LX/4Oj;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 698057
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 698058
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 698059
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 698060
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 698061
    invoke-static {p0, p1}, LX/4Oj;->b(LX/15w;LX/186;)I

    move-result v1

    .line 698062
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 698063
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 698064
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 698065
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 698066
    if-eqz v0, :cond_0

    .line 698067
    const-string v1, "time_offset_in_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698068
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 698069
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 698070
    if-eqz v0, :cond_1

    .line 698071
    const-string v1, "max_duration_in_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698072
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 698073
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 698074
    if-eqz v0, :cond_2

    .line 698075
    const-string v1, "min_duration_in_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698076
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 698077
    :cond_2
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 698078
    if-eqz v0, :cond_3

    .line 698079
    const-string v0, "instream_placement"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698080
    const-class v0, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 698081
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 698082
    if-eqz v0, :cond_4

    .line 698083
    const-string v1, "index"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698084
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 698085
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 698086
    return-void
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 698087
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 698088
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 698089
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2}, LX/4Oj;->a(LX/15i;ILX/0nX;)V

    .line 698090
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 698091
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 698092
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 698093
    const/4 v9, 0x0

    .line 698094
    const/4 v8, 0x0

    .line 698095
    const/4 v7, 0x0

    .line 698096
    const/4 v6, 0x0

    .line 698097
    const/4 v5, 0x0

    .line 698098
    const/4 v4, 0x0

    .line 698099
    const/4 v3, 0x0

    .line 698100
    const/4 v2, 0x0

    .line 698101
    const/4 v1, 0x0

    .line 698102
    const/4 v0, 0x0

    .line 698103
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 698104
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 698105
    const/4 v0, 0x0

    .line 698106
    :goto_0
    return v0

    .line 698107
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 698108
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_6

    .line 698109
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 698110
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 698111
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 698112
    const-string v11, "time_offset_in_ms"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 698113
    const/4 v4, 0x1

    .line 698114
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v9

    goto :goto_1

    .line 698115
    :cond_2
    const-string v11, "max_duration_in_ms"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 698116
    const/4 v3, 0x1

    .line 698117
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v8

    goto :goto_1

    .line 698118
    :cond_3
    const-string v11, "min_duration_in_ms"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 698119
    const/4 v2, 0x1

    .line 698120
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v7

    goto :goto_1

    .line 698121
    :cond_4
    const-string v11, "instream_placement"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 698122
    const/4 v1, 0x1

    .line 698123
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    move-result-object v6

    goto :goto_1

    .line 698124
    :cond_5
    const-string v11, "index"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 698125
    const/4 v0, 0x1

    .line 698126
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    goto :goto_1

    .line 698127
    :cond_6
    const/4 v10, 0x5

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 698128
    if-eqz v4, :cond_7

    .line 698129
    const/4 v4, 0x0

    const/4 v10, 0x0

    invoke-virtual {p1, v4, v9, v10}, LX/186;->a(III)V

    .line 698130
    :cond_7
    if-eqz v3, :cond_8

    .line 698131
    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v8, v4}, LX/186;->a(III)V

    .line 698132
    :cond_8
    if-eqz v2, :cond_9

    .line 698133
    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v7, v3}, LX/186;->a(III)V

    .line 698134
    :cond_9
    if-eqz v1, :cond_a

    .line 698135
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v6}, LX/186;->a(ILjava/lang/Enum;)V

    .line 698136
    :cond_a
    if-eqz v0, :cond_b

    .line 698137
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v5, v1}, LX/186;->a(III)V

    .line 698138
    :cond_b
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method
