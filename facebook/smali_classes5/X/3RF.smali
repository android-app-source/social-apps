.class public final enum LX/3RF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3RF;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3RF;

.field public static final enum CONTACTS_UPLOAD:LX/3RF;

.field public static final enum EVENT_REMINDER:LX/3RF;

.field public static final enum FAILED_TO_SEND:LX/3RF;

.field public static final enum FAILED_TO_SET_PROFILE_PICTURE:LX/3RF;

.field public static final enum FRIEND_INSTALL:LX/3RF;

.field public static final enum INCOMING_CALL:LX/3RF;

.field public static final enum INTERNAL:LX/3RF;

.field public static final enum JOIN_REQUEST:LX/3RF;

.field public static final enum LOCAL:LX/3RF;

.field public static final enum LOGGED_OUT_MESSAGE:LX/3RF;

.field public static final enum MENTION:LX/3RF;

.field public static final enum MESSAGE_REACTION:LX/3RF;

.field public static final enum MESSAGE_REQUEST:LX/3RF;

.field public static final enum MESSENGER_MONTAGE_DAILY_DIGEST:LX/3RF;

.field public static final enum MESSENGER_MONTAGE_FIRST_POST:LX/3RF;

.field public static final enum MESSENGER_MONTAGE_MESSAGE_EXPIRING:LX/3RF;

.field public static final enum MESSENGER_MONTAGE_MESSAGE_VIEWING_STATUS:LX/3RF;

.field public static final enum MISSED_CALL:LX/3RF;

.field public static final enum MULTIPLE_ACCOUNTS_NEW_MESSAGES:LX/3RF;

.field public static final enum NEW_BUILD:LX/3RF;

.field public static final enum NEW_MESSAGE:LX/3RF;

.field public static final enum P2P_PAYMENT:LX/3RF;

.field public static final enum PRE_REG_PUSH:LX/3RF;

.field public static final enum PROMOTION:LX/3RF;

.field public static final enum READ_THREAD:LX/3RF;

.field public static final enum RTC_CALLEE_READY:LX/3RF;

.field public static final enum STALE:LX/3RF;

.field public static final enum SWITCH_TO_FB_ACCOUNT:LX/3RF;

.field public static final enum TINCAN_MESSAGE_REQUEST:LX/3RF;

.field public static final enum USER_LOGGED_OUT:LX/3RF;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 579252
    new-instance v0, LX/3RF;

    const-string v1, "NEW_MESSAGE"

    invoke-direct {v0, v1, v3}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->NEW_MESSAGE:LX/3RF;

    .line 579253
    new-instance v0, LX/3RF;

    const-string v1, "LOGGED_OUT_MESSAGE"

    invoke-direct {v0, v1, v4}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->LOGGED_OUT_MESSAGE:LX/3RF;

    .line 579254
    new-instance v0, LX/3RF;

    const-string v1, "FRIEND_INSTALL"

    invoke-direct {v0, v1, v5}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->FRIEND_INSTALL:LX/3RF;

    .line 579255
    new-instance v0, LX/3RF;

    const-string v1, "FAILED_TO_SEND"

    invoke-direct {v0, v1, v6}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->FAILED_TO_SEND:LX/3RF;

    .line 579256
    new-instance v0, LX/3RF;

    const-string v1, "READ_THREAD"

    invoke-direct {v0, v1, v7}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->READ_THREAD:LX/3RF;

    .line 579257
    new-instance v0, LX/3RF;

    const-string v1, "NEW_BUILD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->NEW_BUILD:LX/3RF;

    .line 579258
    new-instance v0, LX/3RF;

    const-string v1, "P2P_PAYMENT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->P2P_PAYMENT:LX/3RF;

    .line 579259
    new-instance v0, LX/3RF;

    const-string v1, "PROMOTION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->PROMOTION:LX/3RF;

    .line 579260
    new-instance v0, LX/3RF;

    const-string v1, "STALE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->STALE:LX/3RF;

    .line 579261
    new-instance v0, LX/3RF;

    const-string v1, "INTERNAL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->INTERNAL:LX/3RF;

    .line 579262
    new-instance v0, LX/3RF;

    const-string v1, "MISSED_CALL"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->MISSED_CALL:LX/3RF;

    .line 579263
    new-instance v0, LX/3RF;

    const-string v1, "RTC_CALLEE_READY"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->RTC_CALLEE_READY:LX/3RF;

    .line 579264
    new-instance v0, LX/3RF;

    const-string v1, "MESSAGE_REQUEST"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->MESSAGE_REQUEST:LX/3RF;

    .line 579265
    new-instance v0, LX/3RF;

    const-string v1, "TINCAN_MESSAGE_REQUEST"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->TINCAN_MESSAGE_REQUEST:LX/3RF;

    .line 579266
    new-instance v0, LX/3RF;

    const-string v1, "PRE_REG_PUSH"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->PRE_REG_PUSH:LX/3RF;

    .line 579267
    new-instance v0, LX/3RF;

    const-string v1, "LOCAL"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->LOCAL:LX/3RF;

    .line 579268
    new-instance v0, LX/3RF;

    const-string v1, "CONTACTS_UPLOAD"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->CONTACTS_UPLOAD:LX/3RF;

    .line 579269
    new-instance v0, LX/3RF;

    const-string v1, "MULTIPLE_ACCOUNTS_NEW_MESSAGES"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->MULTIPLE_ACCOUNTS_NEW_MESSAGES:LX/3RF;

    .line 579270
    new-instance v0, LX/3RF;

    const-string v1, "MENTION"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->MENTION:LX/3RF;

    .line 579271
    new-instance v0, LX/3RF;

    const-string v1, "MESSAGE_REACTION"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->MESSAGE_REACTION:LX/3RF;

    .line 579272
    new-instance v0, LX/3RF;

    const-string v1, "USER_LOGGED_OUT"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->USER_LOGGED_OUT:LX/3RF;

    .line 579273
    new-instance v0, LX/3RF;

    const-string v1, "INCOMING_CALL"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->INCOMING_CALL:LX/3RF;

    .line 579274
    new-instance v0, LX/3RF;

    const-string v1, "JOIN_REQUEST"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->JOIN_REQUEST:LX/3RF;

    .line 579275
    new-instance v0, LX/3RF;

    const-string v1, "SWITCH_TO_FB_ACCOUNT"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->SWITCH_TO_FB_ACCOUNT:LX/3RF;

    .line 579276
    new-instance v0, LX/3RF;

    const-string v1, "EVENT_REMINDER"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->EVENT_REMINDER:LX/3RF;

    .line 579277
    new-instance v0, LX/3RF;

    const-string v1, "FAILED_TO_SET_PROFILE_PICTURE"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->FAILED_TO_SET_PROFILE_PICTURE:LX/3RF;

    .line 579278
    new-instance v0, LX/3RF;

    const-string v1, "MESSENGER_MONTAGE_FIRST_POST"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->MESSENGER_MONTAGE_FIRST_POST:LX/3RF;

    .line 579279
    new-instance v0, LX/3RF;

    const-string v1, "MESSENGER_MONTAGE_MESSAGE_EXPIRING"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->MESSENGER_MONTAGE_MESSAGE_EXPIRING:LX/3RF;

    .line 579280
    new-instance v0, LX/3RF;

    const-string v1, "MESSENGER_MONTAGE_MESSAGE_VIEWING_STATUS"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->MESSENGER_MONTAGE_MESSAGE_VIEWING_STATUS:LX/3RF;

    .line 579281
    new-instance v0, LX/3RF;

    const-string v1, "MESSENGER_MONTAGE_DAILY_DIGEST"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, LX/3RF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3RF;->MESSENGER_MONTAGE_DAILY_DIGEST:LX/3RF;

    .line 579282
    const/16 v0, 0x1e

    new-array v0, v0, [LX/3RF;

    sget-object v1, LX/3RF;->NEW_MESSAGE:LX/3RF;

    aput-object v1, v0, v3

    sget-object v1, LX/3RF;->LOGGED_OUT_MESSAGE:LX/3RF;

    aput-object v1, v0, v4

    sget-object v1, LX/3RF;->FRIEND_INSTALL:LX/3RF;

    aput-object v1, v0, v5

    sget-object v1, LX/3RF;->FAILED_TO_SEND:LX/3RF;

    aput-object v1, v0, v6

    sget-object v1, LX/3RF;->READ_THREAD:LX/3RF;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/3RF;->NEW_BUILD:LX/3RF;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/3RF;->P2P_PAYMENT:LX/3RF;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/3RF;->PROMOTION:LX/3RF;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/3RF;->STALE:LX/3RF;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/3RF;->INTERNAL:LX/3RF;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/3RF;->MISSED_CALL:LX/3RF;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/3RF;->RTC_CALLEE_READY:LX/3RF;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/3RF;->MESSAGE_REQUEST:LX/3RF;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/3RF;->TINCAN_MESSAGE_REQUEST:LX/3RF;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/3RF;->PRE_REG_PUSH:LX/3RF;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/3RF;->LOCAL:LX/3RF;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/3RF;->CONTACTS_UPLOAD:LX/3RF;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/3RF;->MULTIPLE_ACCOUNTS_NEW_MESSAGES:LX/3RF;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/3RF;->MENTION:LX/3RF;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/3RF;->MESSAGE_REACTION:LX/3RF;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/3RF;->USER_LOGGED_OUT:LX/3RF;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/3RF;->INCOMING_CALL:LX/3RF;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/3RF;->JOIN_REQUEST:LX/3RF;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/3RF;->SWITCH_TO_FB_ACCOUNT:LX/3RF;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/3RF;->EVENT_REMINDER:LX/3RF;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/3RF;->FAILED_TO_SET_PROFILE_PICTURE:LX/3RF;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/3RF;->MESSENGER_MONTAGE_FIRST_POST:LX/3RF;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/3RF;->MESSENGER_MONTAGE_MESSAGE_EXPIRING:LX/3RF;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/3RF;->MESSENGER_MONTAGE_MESSAGE_VIEWING_STATUS:LX/3RF;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/3RF;->MESSENGER_MONTAGE_DAILY_DIGEST:LX/3RF;

    aput-object v2, v0, v1

    sput-object v0, LX/3RF;->$VALUES:[LX/3RF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 579284
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3RF;
    .locals 1

    .prologue
    .line 579285
    const-class v0, LX/3RF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3RF;

    return-object v0
.end method

.method public static values()[LX/3RF;
    .locals 1

    .prologue
    .line 579283
    sget-object v0, LX/3RF;->$VALUES:[LX/3RF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3RF;

    return-object v0
.end method
