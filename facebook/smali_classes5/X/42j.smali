.class public LX/42j;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ew;


# instance fields
.field public a:Landroid/app/Activity;

.field public b:LX/42g;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 667931
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 667929
    iget-object v0, p0, LX/42j;->b:LX/42g;

    invoke-interface {v0, p1, p2, p3}, LX/42g;->a(IILandroid/content/Intent;)V

    .line 667930
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 667927
    iget-object v0, p0, LX/42j;->b:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->a(Landroid/content/Intent;)V

    .line 667928
    return-void
.end method

.method public a(Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 667925
    iget-object v0, p0, LX/42j;->b:LX/42g;

    invoke-interface {v0, p1, p2}, LX/42g;->a(Landroid/content/Intent;I)V

    .line 667926
    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 667923
    iget-object v0, p0, LX/42j;->b:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->a(Landroid/content/res/Configuration;)V

    .line 667924
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 667901
    iget-object v0, p0, LX/42j;->b:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->b(Landroid/os/Bundle;)V

    .line 667902
    return-void
.end method

.method public b(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 667922
    iget-object v0, p0, LX/42j;->b:LX/42g;

    invoke-interface {v0, p1, p2}, LX/42g;->b(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final c(I)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 667921
    iget-object v0, p0, LX/42j;->b:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->c(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public c(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 667919
    iget-object v0, p0, LX/42j;->b:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->c(Landroid/content/Intent;)V

    .line 667920
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 667917
    iget-object v0, p0, LX/42j;->b:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->c(Landroid/os/Bundle;)V

    .line 667918
    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 667915
    iget-object v0, p0, LX/42j;->b:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->d(Landroid/os/Bundle;)V

    .line 667916
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 667913
    iget-object v0, p0, LX/42j;->b:LX/42g;

    invoke-interface {v0}, LX/42g;->a()V

    .line 667914
    return-void
.end method

.method public e(I)V
    .locals 1

    .prologue
    .line 667911
    iget-object v0, p0, LX/42j;->b:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->e(I)V

    .line 667912
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 667891
    iget-object v0, p0, LX/42j;->b:LX/42g;

    invoke-interface {v0}, LX/42g;->b()V

    .line 667892
    return-void
.end method

.method public final iA_()V
    .locals 1

    .prologue
    .line 667893
    iget-object v0, p0, LX/42j;->b:LX/42g;

    invoke-interface {v0}, LX/42g;->p()V

    .line 667894
    return-void
.end method

.method public final iC_()LX/0gc;
    .locals 1

    .prologue
    .line 667895
    iget-object v0, p0, LX/42j;->b:LX/42g;

    invoke-interface {v0}, LX/42g;->r()LX/0gc;

    move-result-object v0

    return-object v0
.end method

.method public j()V
    .locals 1

    .prologue
    .line 667896
    iget-object v0, p0, LX/42j;->b:LX/42g;

    invoke-interface {v0}, LX/42g;->d()V

    .line 667897
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 667898
    iget-object v0, p0, LX/42j;->b:LX/42g;

    invoke-interface {v0}, LX/42g;->e()V

    .line 667899
    return-void
.end method

.method public final mt_()Landroid/view/Window;
    .locals 1

    .prologue
    .line 667900
    iget-object v0, p0, LX/42j;->b:LX/42g;

    invoke-interface {v0}, LX/42g;->s()Landroid/view/Window;

    move-result-object v0

    return-object v0
.end method

.method public final mu_()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 667903
    iget-object v0, p0, LX/42j;->b:LX/42g;

    invoke-interface {v0}, LX/42g;->t()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public n()V
    .locals 1

    .prologue
    .line 667904
    iget-object v0, p0, LX/42j;->b:LX/42g;

    invoke-interface {v0}, LX/42g;->h()V

    .line 667905
    return-void
.end method

.method public q()V
    .locals 1

    .prologue
    .line 667906
    iget-object v0, p0, LX/42j;->b:LX/42g;

    invoke-interface {v0}, LX/42g;->k()V

    .line 667907
    return-void
.end method

.method public s()V
    .locals 1

    .prologue
    .line 667908
    iget-object v0, p0, LX/42j;->b:LX/42g;

    invoke-interface {v0}, LX/42g;->m()V

    .line 667909
    return-void
.end method

.method public final y()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 667910
    iget-object v0, p0, LX/42j;->b:LX/42g;

    invoke-interface {v0}, LX/42g;->u()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method
