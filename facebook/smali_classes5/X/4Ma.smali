.class public LX/4Ma;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 688800
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 688741
    const/4 v10, 0x0

    .line 688742
    const/4 v9, 0x0

    .line 688743
    const/4 v8, 0x0

    .line 688744
    const/4 v7, 0x0

    .line 688745
    const/4 v6, 0x0

    .line 688746
    const/4 v5, 0x0

    .line 688747
    const/4 v4, 0x0

    .line 688748
    const/4 v3, 0x0

    .line 688749
    const/4 v2, 0x0

    .line 688750
    const/4 v1, 0x0

    .line 688751
    const/4 v0, 0x0

    .line 688752
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 688753
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 688754
    const/4 v0, 0x0

    .line 688755
    :goto_0
    return v0

    .line 688756
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 688757
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_b

    .line 688758
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 688759
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 688760
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 688761
    const-string v12, "approximate_new_unit_count"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 688762
    const/4 v1, 0x1

    .line 688763
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v10

    goto :goto_1

    .line 688764
    :cond_2
    const-string v12, "debug_info"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 688765
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 688766
    :cond_3
    const-string v12, "edges"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 688767
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 688768
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, v12, :cond_4

    .line 688769
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, v12, :cond_4

    .line 688770
    invoke-static {p0, p1}, LX/4Mc;->b(LX/15w;LX/186;)I

    move-result v11

    .line 688771
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 688772
    :cond_4
    invoke-static {v8, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v8

    move v8, v8

    .line 688773
    goto :goto_1

    .line 688774
    :cond_5
    const-string v12, "low_engagement_deduplication_keys"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 688775
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 688776
    :cond_6
    const-string v12, "no_feed_polling"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 688777
    const/4 v0, 0x1

    .line 688778
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v6

    goto/16 :goto_1

    .line 688779
    :cond_7
    const-string v12, "page_info"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 688780
    invoke-static {p0, p1}, LX/264;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 688781
    :cond_8
    const-string v12, "promotion_unit_at_top"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 688782
    invoke-static {p0, p1}, LX/4Rt;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 688783
    :cond_9
    const-string v12, "query_function"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 688784
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 688785
    :cond_a
    const-string v12, "query_title"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 688786
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 688787
    :cond_b
    const/16 v11, 0x9

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 688788
    if-eqz v1, :cond_c

    .line 688789
    const/4 v1, 0x0

    const/4 v11, 0x0

    invoke-virtual {p1, v1, v10, v11}, LX/186;->a(III)V

    .line 688790
    :cond_c
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 688791
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 688792
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 688793
    if-eqz v0, :cond_d

    .line 688794
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->a(IZ)V

    .line 688795
    :cond_d
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 688796
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 688797
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 688798
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 688799
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method
