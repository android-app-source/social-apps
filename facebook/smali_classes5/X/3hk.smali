.class public LX/3hk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/1nA;

.field public final b:LX/17V;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3i4;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/17Q;


# direct methods
.method public constructor <init>(LX/1nA;LX/17V;LX/0Ot;LX/17Q;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nA;",
            "LX/17V;",
            "LX/0Ot",
            "<",
            "LX/3i4;",
            ">;",
            "LX/17Q;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 627885
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 627886
    iput-object p1, p0, LX/3hk;->a:LX/1nA;

    .line 627887
    iput-object p2, p0, LX/3hk;->b:LX/17V;

    .line 627888
    iput-object p3, p0, LX/3hk;->c:LX/0Ot;

    .line 627889
    iput-object p4, p0, LX/3hk;->d:LX/17Q;

    .line 627890
    return-void
.end method

.method public static a(LX/0QB;)LX/3hk;
    .locals 7

    .prologue
    .line 627891
    const-class v1, LX/3hk;

    monitor-enter v1

    .line 627892
    :try_start_0
    sget-object v0, LX/3hk;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 627893
    sput-object v2, LX/3hk;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 627894
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 627895
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 627896
    new-instance v6, LX/3hk;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v3

    check-cast v3, LX/1nA;

    invoke-static {v0}, LX/17V;->b(LX/0QB;)LX/17V;

    move-result-object v4

    check-cast v4, LX/17V;

    const/16 v5, 0x1ff

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v5

    check-cast v5, LX/17Q;

    invoke-direct {v6, v3, v4, p0, v5}, LX/3hk;-><init>(LX/1nA;LX/17V;LX/0Ot;LX/17Q;)V

    .line 627897
    move-object v0, v6

    .line 627898
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 627899
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3hk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 627900
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 627901
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;I)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 627902
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 627903
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 627904
    invoke-static {v0}, LX/3i4;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v1

    .line 627905
    if-eqz v1, :cond_0

    .line 627906
    iget-object v0, p0, LX/3hk;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3i4;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, LX/3i4;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;)V

    .line 627907
    :goto_0
    return-void

    .line 627908
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v1

    .line 627909
    invoke-static {v1}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 627910
    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 627911
    const-string v3, "app_id"

    invoke-virtual {v1, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v4

    invoke-static {v2}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    invoke-static {v3, v4, v2}, LX/17Q;->a(Ljava/lang/String;ZLX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 627912
    if-eqz v2, :cond_1

    .line 627913
    const-string v3, "item_index"

    invoke-virtual {v2, v3, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 627914
    :cond_1
    move-object v1, v2

    .line 627915
    :goto_1
    iget-object v2, p0, LX/3hk;->a:LX/1nA;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v1, v3}, LX/1nA;->a(Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/util/Map;)Landroid/view/View$OnClickListener;

    move-result-object v0

    .line 627916
    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 627917
    :cond_2
    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v7

    .line 627918
    iget-object v4, p0, LX/3hk;->b:LX/17V;

    .line 627919
    iget-object v5, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 627920
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v7}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v6

    invoke-static {v7}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v7

    const-string v8, "native_newsfeed"

    move v9, p3

    invoke-virtual/range {v4 .. v9}, LX/17V;->a(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    move-object v1, v4

    .line 627921
    goto :goto_1
.end method
