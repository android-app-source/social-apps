.class public LX/4ql;
.super LX/28E;
.source ""


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final _targetType:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public final _value:Ljava/lang/Object;


# direct methods
.method private constructor <init>(Ljava/lang/String;LX/28G;Ljava/lang/Object;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/28G;",
            "Ljava/lang/Object;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 814707
    invoke-direct {p0, p1, p2}, LX/28E;-><init>(Ljava/lang/String;LX/28G;)V

    .line 814708
    iput-object p3, p0, LX/4ql;->_value:Ljava/lang/Object;

    .line 814709
    iput-object p4, p0, LX/4ql;->_targetType:Ljava/lang/Class;

    .line 814710
    return-void
.end method

.method public static a(LX/15w;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)LX/4ql;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/4ql;"
        }
    .end annotation

    .prologue
    .line 814706
    new-instance v0, LX/4ql;

    invoke-virtual {p0}, LX/15w;->k()LX/28G;

    move-result-object v1

    invoke-direct {v0, p1, v1, p2, p3}, LX/4ql;-><init>(Ljava/lang/String;LX/28G;Ljava/lang/Object;Ljava/lang/Class;)V

    return-object v0
.end method
