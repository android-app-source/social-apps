.class public LX/4Mj;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 689128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 689129
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 689130
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 689131
    :goto_0
    return v1

    .line 689132
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 689133
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 689134
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 689135
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 689136
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 689137
    const-string v4, "node"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 689138
    invoke-static {p0, p1}, LX/2ao;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 689139
    :cond_2
    const-string v4, "supported_action_types"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 689140
    const-class v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    invoke-static {p0, p1, v0}, LX/2gu;->a(LX/15w;LX/186;Ljava/lang/Class;)I

    move-result v0

    goto :goto_1

    .line 689141
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 689142
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 689143
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 689144
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 689145
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 689146
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 689147
    if-eqz v0, :cond_0

    .line 689148
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689149
    invoke-static {p0, v0, p2, p3}, LX/2ao;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 689150
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 689151
    if-eqz v0, :cond_1

    .line 689152
    const-string v0, "supported_action_types"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689153
    const-class v0, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->b(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->b(Ljava/util/Iterator;LX/0nX;)V

    .line 689154
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 689155
    return-void
.end method
