.class public LX/3gP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/3gQ;

.field private final b:LX/14w;


# direct methods
.method public constructor <init>(LX/3gQ;LX/14w;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 624406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 624407
    iput-object p1, p0, LX/3gP;->a:LX/3gQ;

    .line 624408
    iput-object p2, p0, LX/3gP;->b:LX/14w;

    .line 624409
    return-void
.end method

.method public static a(LX/0QB;)LX/3gP;
    .locals 5

    .prologue
    .line 624395
    const-class v1, LX/3gP;

    monitor-enter v1

    .line 624396
    :try_start_0
    sget-object v0, LX/3gP;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 624397
    sput-object v2, LX/3gP;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 624398
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 624399
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 624400
    new-instance p0, LX/3gP;

    invoke-static {v0}, LX/3gQ;->a(LX/0QB;)LX/3gQ;

    move-result-object v3

    check-cast v3, LX/3gQ;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v4

    check-cast v4, LX/14w;

    invoke-direct {p0, v3, v4}, LX/3gP;-><init>(LX/3gQ;LX/14w;)V

    .line 624401
    move-object v0, p0

    .line 624402
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 624403
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3gP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 624404
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 624405
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/text/Spannable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Landroid/text/Spannable;"
        }
    .end annotation

    .prologue
    .line 624381
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 624382
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 624383
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aS()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 624384
    iget-object v0, p0, LX/3gP;->a:LX/3gQ;

    invoke-virtual {v0, p1}, LX/3gQ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/text/Spannable;

    move-result-object v0

    .line 624385
    :goto_0
    return-object v0

    .line 624386
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->V()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 624387
    new-instance v1, Landroid/text/SpannableString;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->V()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    move-object v0, v1

    goto :goto_0

    .line 624388
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 624389
    new-instance v1, Landroid/text/SpannableString;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    move-object v0, v1

    goto :goto_0

    .line 624390
    :cond_2
    new-instance v1, Landroid/text/SpannableString;

    const/4 p1, 0x0

    .line 624391
    invoke-static {v0}, LX/14w;->j(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 624392
    invoke-static {v0}, LX/0x1;->f(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->j()LX/0Px;

    move-result-object p0

    invoke-virtual {p0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;->a()LX/0Px;

    move-result-object p0

    invoke-virtual {p0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p0

    .line 624393
    :goto_1
    move-object v0, p0

    .line 624394
    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    move-object v0, v1

    goto :goto_0

    :cond_3
    const/4 p0, 0x0

    goto :goto_1
.end method
