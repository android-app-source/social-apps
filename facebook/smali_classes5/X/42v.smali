.class public LX/42v;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private final a:Z

.field private final b:Landroid/content/Context;

.field private final c:Landroid/content/pm/PackageManager;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/PackageManager;LX/0WJ;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 668296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 668297
    const-string v0, "BatteryUsageMonitor"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    iput-boolean v0, p0, LX/42v;->a:Z

    .line 668298
    iput-object p1, p0, LX/42v;->b:Landroid/content/Context;

    .line 668299
    iput-object p2, p0, LX/42v;->c:Landroid/content/pm/PackageManager;

    .line 668300
    if-eqz p3, :cond_0

    .line 668301
    invoke-virtual {p3}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v0

    .line 668302
    if-eqz v0, :cond_1

    .line 668303
    iget-boolean v1, v0, Lcom/facebook/user/model/User;->o:Z

    move v0, v1

    .line 668304
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/42v;->d:Z

    .line 668305
    :cond_0
    return-void

    .line 668306
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
