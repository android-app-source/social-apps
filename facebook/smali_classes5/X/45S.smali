.class public final LX/45S;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/45Q;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 670329
    new-instance v0, LX/45R;

    invoke-direct {v0}, LX/45R;-><init>()V

    sput-object v0, LX/45S;->a:LX/45Q;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 670330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 670331
    return-void
.end method

.method public static a(Ljava/lang/Object;LX/45T;LX/45Q;)LX/45U;
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 670332
    if-nez p0, :cond_1

    move-object v0, v1

    .line 670333
    :cond_0
    :goto_0
    return-object v0

    .line 670334
    :cond_1
    if-eqz p2, :cond_2

    .line 670335
    :goto_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 670336
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 670337
    const-string v3, "facebook"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    move v0, v3

    .line 670338
    if-nez v0, :cond_3

    .line 670339
    const-string v0, "Class %s is not eligible for instrumentation"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v7

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, LX/45Q;->a(Ljava/lang/String;)V

    move-object v0, v1

    .line 670340
    goto :goto_0

    .line 670341
    :cond_2
    sget-object p2, LX/45S;->a:LX/45Q;

    goto :goto_1

    .line 670342
    :cond_3
    const/4 v9, 0x1

    const/4 v6, 0x0

    .line 670343
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 670344
    const-string v3, "addInstrumentationListener"

    const/16 v10, 0x5f

    .line 670345
    const/16 v4, 0x2e

    invoke-virtual {v0, v4, v10}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x2f

    invoke-virtual {v4, v5, v10}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x24

    invoke-virtual {v4, v5, v10}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v4

    .line 670346
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, "_UNIQUE_NAME_"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 670347
    move-object v3, v3

    .line 670348
    const/4 v0, 0x1

    :try_start_0
    new-array v0, v0, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, LX/45T;

    aput-object v5, v0, v4

    invoke-virtual {v2, v3, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 670349
    :goto_2
    move-object v0, v0

    .line 670350
    if-nez v0, :cond_4

    move-object v0, v1

    .line 670351
    goto :goto_0

    .line 670352
    :cond_4
    const/4 v3, 0x1

    :try_start_1
    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/45U;

    .line 670353
    const-string v3, "Added AddInstrumentationListener method for class %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v3}, LX/45Q;->a(Ljava/lang/String;)V

    .line 670354
    if-nez v0, :cond_0

    .line 670355
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v3, "We currently do not support multiple collectors inflight"

    invoke-direct {v0, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 670356
    :catch_0
    move-exception v0

    .line 670357
    invoke-virtual {v0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 670358
    const-string v3, "Cannot call AddInstrumentationListener method for class %s. Cause:"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-interface {p2, v3, v4}, LX/45Q;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 670359
    :cond_5
    const-string v3, "Cannot call AddInstrumentationListener method for class %s. Top Level:"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2, v0}, LX/45Q;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 670360
    goto/16 :goto_0

    .line 670361
    :catch_1
    move-exception v0

    .line 670362
    const-string v4, "Cannot get method %s for class %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v6

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v3, v0}, LX/45Q;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 670363
    const/4 v0, 0x0

    goto :goto_2
.end method
