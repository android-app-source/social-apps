.class public final LX/488;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2FC;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/2FC;",
        "LX/0Or",
        "<",
        "Lcom/facebook/compactdisk/DiskCache;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/facebook/compactdisk/DiskCache;

.field private final b:LX/0Tn;

.field private final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:Lcom/facebook/compactdisk/StoreManagerFactory;

.field private final e:Lcom/facebook/compactdisk/DiskCacheConfig;


# direct methods
.method public constructor <init>(LX/0Tn;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/compactdisk/StoreManagerFactory;Lcom/facebook/compactdisk/DiskCacheConfig;)V
    .locals 0

    .prologue
    .line 672734
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 672735
    iput-object p1, p0, LX/488;->b:LX/0Tn;

    .line 672736
    iput-object p2, p0, LX/488;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 672737
    iput-object p3, p0, LX/488;->d:Lcom/facebook/compactdisk/StoreManagerFactory;

    .line 672738
    iput-object p4, p0, LX/488;->e:Lcom/facebook/compactdisk/DiskCacheConfig;

    .line 672739
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 672740
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/488;->a:Lcom/facebook/compactdisk/DiskCache;

    if-nez v0, :cond_0

    .line 672741
    iget-object v0, p0, LX/488;->d:Lcom/facebook/compactdisk/StoreManagerFactory;

    iget-object v1, p0, LX/488;->e:Lcom/facebook/compactdisk/DiskCacheConfig;

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/StoreManagerFactory;->a(Lcom/facebook/compactdisk/DiskCacheConfig;)Lcom/facebook/compactdisk/StoreManager;

    move-result-object v0

    iget-object v1, p0, LX/488;->e:Lcom/facebook/compactdisk/DiskCacheConfig;

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/StoreManager;->a(Lcom/facebook/compactdisk/DiskCacheConfig;)Lcom/facebook/compactdisk/DiskCache;

    move-result-object v0

    iput-object v0, p0, LX/488;->a:Lcom/facebook/compactdisk/DiskCache;

    .line 672742
    iget-object v0, p0, LX/488;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/488;->b:LX/0Tn;

    iget-object v2, p0, LX/488;->a:Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v2}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->getDirectoryPath()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 672743
    :cond_0
    iget-object v0, p0, LX/488;->a:Lcom/facebook/compactdisk/DiskCache;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 672744
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized invalidate()V
    .locals 1

    .prologue
    .line 672745
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/488;->e:Lcom/facebook/compactdisk/DiskCacheConfig;

    invoke-virtual {v0}, Lcom/facebook/compactdisk/DiskCacheConfig;->getSessionScoped()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 672746
    const/4 v0, 0x0

    iput-object v0, p0, LX/488;->a:Lcom/facebook/compactdisk/DiskCache;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 672747
    :cond_0
    monitor-exit p0

    return-void

    .line 672748
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
