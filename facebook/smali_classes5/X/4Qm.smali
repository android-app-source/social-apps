.class public LX/4Qm;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 706321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 24

    .prologue
    .line 706241
    const/16 v20, 0x0

    .line 706242
    const/16 v19, 0x0

    .line 706243
    const/16 v18, 0x0

    .line 706244
    const-wide/16 v16, 0x0

    .line 706245
    const/4 v15, 0x0

    .line 706246
    const/4 v14, 0x0

    .line 706247
    const/4 v13, 0x0

    .line 706248
    const/4 v12, 0x0

    .line 706249
    const/4 v11, 0x0

    .line 706250
    const/4 v10, 0x0

    .line 706251
    const/4 v9, 0x0

    .line 706252
    const/4 v8, 0x0

    .line 706253
    const/4 v7, 0x0

    .line 706254
    const/4 v6, 0x0

    .line 706255
    const/4 v5, 0x0

    .line 706256
    const/4 v4, 0x0

    .line 706257
    const/4 v3, 0x0

    .line 706258
    const/4 v2, 0x0

    .line 706259
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_14

    .line 706260
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 706261
    const/4 v2, 0x0

    .line 706262
    :goto_0
    return v2

    .line 706263
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_10

    .line 706264
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 706265
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 706266
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v23, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v23

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 706267
    const-string v6, "all_users"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 706268
    invoke-static/range {p0 .. p1}, LX/4Qn;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto :goto_1

    .line 706269
    :cond_1
    const-string v6, "cache_id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 706270
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v21, v2

    goto :goto_1

    .line 706271
    :cond_2
    const-string v6, "debug_info"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 706272
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 706273
    :cond_3
    const-string v6, "fetchTimeMs"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 706274
    const/4 v2, 0x1

    .line 706275
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 706276
    :cond_4
    const-string v6, "gap_rule"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 706277
    const/4 v2, 0x1

    .line 706278
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v9, v2

    move/from16 v20, v6

    goto :goto_1

    .line 706279
    :cond_5
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 706280
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 706281
    :cond_6
    const-string v6, "short_term_cache_key"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 706282
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 706283
    :cond_7
    const-string v6, "title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 706284
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 706285
    :cond_8
    const-string v6, "titleForSummary"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 706286
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 706287
    :cond_9
    const-string v6, "tracking"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 706288
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 706289
    :cond_a
    const-string v6, "url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 706290
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 706291
    :cond_b
    const-string v6, "hideable_token"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 706292
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 706293
    :cond_c
    const-string v6, "local_story_visibility"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 706294
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 706295
    :cond_d
    const-string v6, "local_story_visible_height"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 706296
    const/4 v2, 0x1

    .line 706297
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move v11, v6

    goto/16 :goto_1

    .line 706298
    :cond_e
    const-string v6, "local_removed_items"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 706299
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 706300
    :cond_f
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 706301
    :cond_10
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 706302
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 706303
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 706304
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 706305
    if-eqz v3, :cond_11

    .line 706306
    const/4 v3, 0x4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 706307
    :cond_11
    if-eqz v9, :cond_12

    .line 706308
    const/4 v2, 0x5

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 706309
    :cond_12
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 706310
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 706311
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 706312
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 706313
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 706314
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 706315
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 706316
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 706317
    if-eqz v8, :cond_13

    .line 706318
    const/16 v2, 0xe

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11, v3}, LX/186;->a(III)V

    .line 706319
    :cond_13
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 706320
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_14
    move/from16 v21, v19

    move/from16 v22, v20

    move/from16 v19, v14

    move/from16 v20, v15

    move v14, v9

    move v15, v10

    move v10, v5

    move v9, v3

    move v3, v4

    move-wide/from16 v4, v16

    move/from16 v17, v12

    move/from16 v16, v11

    move v12, v7

    move v11, v6

    move/from16 v7, v18

    move/from16 v18, v13

    move v13, v8

    move v8, v2

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 706230
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 706231
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 706232
    invoke-static {p0, v2}, LX/4Qm;->a(LX/15w;LX/186;)I

    move-result v1

    .line 706233
    if-eqz v0, :cond_0

    .line 706234
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 706235
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 706236
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 706237
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 706238
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 706239
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 706240
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/16 v4, 0xf

    const/4 v3, 0x0

    .line 706163
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 706164
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706165
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 706166
    const-string v0, "name"

    const-string v1, "PaginatedPeopleYouMayKnowFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 706167
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 706168
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 706169
    if-eqz v0, :cond_0

    .line 706170
    const-string v1, "all_users"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706171
    invoke-static {p0, v0, p2, p3}, LX/4Qn;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 706172
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 706173
    if-eqz v0, :cond_1

    .line 706174
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706175
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 706176
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 706177
    if-eqz v0, :cond_2

    .line 706178
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706179
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 706180
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 706181
    cmp-long v2, v0, v6

    if-eqz v2, :cond_3

    .line 706182
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706183
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 706184
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 706185
    if-eqz v0, :cond_4

    .line 706186
    const-string v1, "gap_rule"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706187
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 706188
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 706189
    if-eqz v0, :cond_5

    .line 706190
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706191
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 706192
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 706193
    if-eqz v0, :cond_6

    .line 706194
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706195
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 706196
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 706197
    if-eqz v0, :cond_7

    .line 706198
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706199
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 706200
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 706201
    if-eqz v0, :cond_8

    .line 706202
    const-string v1, "titleForSummary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706203
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 706204
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 706205
    if-eqz v0, :cond_9

    .line 706206
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706207
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 706208
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 706209
    if-eqz v0, :cond_a

    .line 706210
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706211
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 706212
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 706213
    if-eqz v0, :cond_b

    .line 706214
    const-string v1, "hideable_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706215
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 706216
    :cond_b
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 706217
    if-eqz v0, :cond_c

    .line 706218
    const-string v1, "local_story_visibility"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706219
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 706220
    :cond_c
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 706221
    if-eqz v0, :cond_d

    .line 706222
    const-string v1, "local_story_visible_height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706223
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 706224
    :cond_d
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 706225
    if-eqz v0, :cond_e

    .line 706226
    const-string v0, "local_removed_items"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706227
    invoke-virtual {p0, p1, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 706228
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 706229
    return-void
.end method
