.class public final LX/3vK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/3vN;

.field private final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3vN;)V
    .locals 1

    .prologue
    .line 651140
    iput-object p1, p0, LX/3vK;->a:LX/3vN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 651141
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/3vK;->b:Landroid/util/SparseArray;

    return-void
.end method


# virtual methods
.method public final a(I)Landroid/view/View;
    .locals 2

    .prologue
    .line 651142
    iget-object v0, p0, LX/3vK;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 651143
    if-eqz v0, :cond_0

    .line 651144
    iget-object v1, p0, LX/3vK;->b:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->delete(I)V

    .line 651145
    :cond_0
    return-object v0
.end method

.method public final a()V
    .locals 6

    .prologue
    .line 651146
    iget-object v2, p0, LX/3vK;->b:Landroid/util/SparseArray;

    .line 651147
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v3

    .line 651148
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 651149
    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 651150
    if-eqz v0, :cond_0

    .line 651151
    iget-object v4, p0, LX/3vK;->a:LX/3vN;

    const/4 v5, 0x1

    invoke-static {v4, v0, v5}, LX/3vN;->a(LX/3vN;Landroid/view/View;Z)V

    .line 651152
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 651153
    :cond_1
    invoke-virtual {v2}, Landroid/util/SparseArray;->clear()V

    .line 651154
    return-void
.end method

.method public final a(ILandroid/view/View;)V
    .locals 1

    .prologue
    .line 651155
    iget-object v0, p0, LX/3vK;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 651156
    return-void
.end method
