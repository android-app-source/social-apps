.class public LX/4bD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final b:Lorg/apache/http/client/RedirectHandler;

.field public static final c:Lorg/apache/http/client/ResponseHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/http/client/ResponseHandler",
            "<",
            "Ljava/net/URI;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile d:LX/4bD;


# instance fields
.field public final a:Lcom/facebook/http/common/FbHttpRequestProcessor;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 793466
    new-instance v0, LX/4bB;

    invoke-direct {v0}, LX/4bB;-><init>()V

    sput-object v0, LX/4bD;->b:Lorg/apache/http/client/RedirectHandler;

    .line 793467
    new-instance v0, LX/4bC;

    invoke-direct {v0}, LX/4bC;-><init>()V

    sput-object v0, LX/4bD;->c:Lorg/apache/http/client/ResponseHandler;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/http/common/FbHttpRequestProcessor;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 793468
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 793469
    iput-object p1, p0, LX/4bD;->a:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 793470
    return-void
.end method

.method public static a(LX/0QB;)LX/4bD;
    .locals 4

    .prologue
    .line 793471
    sget-object v0, LX/4bD;->d:LX/4bD;

    if-nez v0, :cond_1

    .line 793472
    const-class v1, LX/4bD;

    monitor-enter v1

    .line 793473
    :try_start_0
    sget-object v0, LX/4bD;->d:LX/4bD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 793474
    if-eqz v2, :cond_0

    .line 793475
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 793476
    new-instance p0, LX/4bD;

    invoke-static {v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v3

    check-cast v3, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-direct {p0, v3}, LX/4bD;-><init>(Lcom/facebook/http/common/FbHttpRequestProcessor;)V

    .line 793477
    move-object v0, p0

    .line 793478
    sput-object v0, LX/4bD;->d:LX/4bD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 793479
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 793480
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 793481
    :cond_1
    sget-object v0, LX/4bD;->d:LX/4bD;

    return-object v0

    .line 793482
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 793483
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
