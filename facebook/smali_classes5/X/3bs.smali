.class public LX/3bs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/3bs;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 612434
    return-void
.end method

.method public static a(LX/0QB;)LX/3bs;
    .locals 3

    .prologue
    .line 612435
    sget-object v0, LX/3bs;->a:LX/3bs;

    if-nez v0, :cond_1

    .line 612436
    const-class v1, LX/3bs;

    monitor-enter v1

    .line 612437
    :try_start_0
    sget-object v0, LX/3bs;->a:LX/3bs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 612438
    if-eqz v2, :cond_0

    .line 612439
    :try_start_1
    new-instance v0, LX/3bs;

    invoke-direct {v0}, LX/3bs;-><init>()V

    .line 612440
    move-object v0, v0

    .line 612441
    sput-object v0, LX/3bs;->a:LX/3bs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612442
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 612443
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 612444
    :cond_1
    sget-object v0, LX/3bs;->a:LX/3bs;

    return-object v0

    .line 612445
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 612446
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 612447
    sget-object v0, Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 612448
    sget-object v0, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 612449
    sget-object v0, Lcom/facebook/timeline/feed/parts/TimelineInvisibleLoadingIndicatorPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 612450
    sget-object v0, Lcom/facebook/timeline/feed/parts/TimelineNoStoriesComponentPartDefinition;->d:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 612451
    sget-object v0, Lcom/facebook/timeline/feed/parts/TimelinePostsLabelComponentPartDefinition;->d:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 612452
    return-void
.end method
