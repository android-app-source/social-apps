.class public LX/3gi;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Lcom/facebook/video/downloadmanager/graphql/OfflineVideoServerCheckQueryModels$OfflineVideoServerCheckQueryModel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 624768
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 624769
    return-void
.end method

.method private static a(Ljava/util/List;)LX/0gW;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0gW;"
        }
    .end annotation

    .prologue
    .line 624765
    new-instance v0, LX/BUN;

    invoke-direct {v0}, LX/BUN;-><init>()V

    .line 624766
    const-string v1, "video_ids"

    invoke-virtual {v0, v1, p0}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 624767
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 624750
    check-cast p1, Ljava/util/List;

    .line 624751
    iget-object v0, p0, LX/0ro;->a:LX/0sO;

    invoke-static {p1}, LX/3gi;->a(Ljava/util/List;)LX/0gW;

    move-result-object v1

    .line 624752
    invoke-virtual {v1}, LX/0gW;->l()Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;

    move-result-object v2

    .line 624753
    if-eqz v2, :cond_2

    .line 624754
    iget-object v3, v0, LX/0sO;->k:LX/0sT;

    invoke-virtual {v3}, LX/0sT;->j()Z

    move-result v3

    .line 624755
    iput-boolean v3, v2, Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;->d:Z

    .line 624756
    const/4 v3, 0x0

    invoke-virtual {v2, p3, v3}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 624757
    :goto_0
    move-object v0, v2

    .line 624758
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 624759
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoServerCheckQueryModels$OfflineVideoServerCheckQueryModel;

    .line 624760
    if-eqz v0, :cond_0

    .line 624761
    invoke-virtual {v0}, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoServerCheckQueryModels$OfflineVideoServerCheckQueryModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 624762
    :cond_1
    return-object v1

    .line 624763
    :cond_2
    iget-object v2, v1, LX/0gW;->a:LX/0w5;

    move-object v2, v2

    .line 624764
    iget-object v3, v0, LX/0sO;->c:LX/0lC;

    iget-object p0, v0, LX/0sO;->k:LX/0sT;

    invoke-virtual {p0}, LX/0sT;->j()Z

    move-result p0

    invoke-static {v2, p3, v3, p0}, LX/261;->a(LX/0w5;LX/15w;LX/0lC;Z)Ljava/util/List;

    move-result-object v2

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 624748
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic f(Ljava/lang/Object;)LX/0gW;
    .locals 1

    .prologue
    .line 624749
    check-cast p1, Ljava/util/List;

    invoke-static {p1}, LX/3gi;->a(Ljava/util/List;)LX/0gW;

    move-result-object v0

    return-object v0
.end method
