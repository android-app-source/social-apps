.class public final LX/51O;
.super LX/4xC;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/4xC",
        "<TE;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field public final transient a:LX/51N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/51N",
            "<",
            "LX/51M",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field public final transient b:LX/4xo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4xo",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final transient c:LX/51M;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/51M",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/51N;LX/4xo;LX/51M;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/51N",
            "<",
            "LX/51M",
            "<TE;>;>;",
            "LX/4xo",
            "<TE;>;",
            "LX/51M",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 824971
    iget-object v0, p2, LX/4xo;->comparator:Ljava/util/Comparator;

    move-object v0, v0

    .line 824972
    invoke-direct {p0, v0}, LX/4xC;-><init>(Ljava/util/Comparator;)V

    .line 824973
    iput-object p1, p0, LX/51O;->a:LX/51N;

    .line 824974
    iput-object p2, p0, LX/51O;->b:LX/4xo;

    .line 824975
    iput-object p3, p0, LX/51O;->c:LX/51M;

    .line 824976
    return-void
.end method

.method private constructor <init>(Ljava/util/Comparator;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TE;>;)V"
        }
    .end annotation

    .prologue
    .line 824912
    invoke-direct {p0, p1}, LX/4xC;-><init>(Ljava/util/Comparator;)V

    .line 824913
    invoke-static {p1}, LX/4xo;->a(Ljava/util/Comparator;)LX/4xo;

    move-result-object v0

    iput-object v0, p0, LX/51O;->b:LX/4xo;

    .line 824914
    new-instance v0, LX/51M;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, LX/51M;-><init>(Ljava/lang/Object;I)V

    iput-object v0, p0, LX/51O;->c:LX/51M;

    .line 824915
    iget-object v0, p0, LX/51O;->c:LX/51M;

    iget-object v1, p0, LX/51O;->c:LX/51M;

    invoke-static {v0, v1}, LX/51O;->b(LX/51M;LX/51M;)V

    .line 824916
    new-instance v0, LX/51N;

    invoke-direct {v0}, LX/51N;-><init>()V

    iput-object v0, p0, LX/51O;->a:LX/51N;

    .line 824917
    return-void
.end method

.method public static a(LX/51M;)I
    .locals 1
    .param p0    # LX/51M;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/51M",
            "<*>;)I"
        }
    .end annotation

    .prologue
    .line 824918
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/51M;->c:I

    goto :goto_0
.end method

.method private a(LX/51J;)J
    .locals 6

    .prologue
    .line 824919
    iget-object v0, p0, LX/51O;->a:LX/51N;

    .line 824920
    iget-object v1, v0, LX/51N;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 824921
    check-cast v0, LX/51M;

    .line 824922
    invoke-virtual {p1, v0}, LX/51J;->treeAggregate(LX/51M;)J

    move-result-wide v2

    .line 824923
    iget-object v1, p0, LX/51O;->b:LX/4xo;

    .line 824924
    iget-boolean v4, v1, LX/4xo;->hasLowerBound:Z

    move v1, v4

    .line 824925
    if-eqz v1, :cond_0

    .line 824926
    invoke-direct {p0, p1, v0}, LX/51O;->a(LX/51J;LX/51M;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 824927
    :cond_0
    iget-object v1, p0, LX/51O;->b:LX/4xo;

    .line 824928
    iget-boolean v4, v1, LX/4xo;->hasUpperBound:Z

    move v1, v4

    .line 824929
    if-eqz v1, :cond_1

    .line 824930
    invoke-direct {p0, p1, v0}, LX/51O;->b(LX/51J;LX/51M;)J

    move-result-wide v0

    sub-long/2addr v2, v0

    .line 824931
    :cond_1
    return-wide v2
.end method

.method private a(LX/51J;LX/51M;)J
    .locals 4
    .param p2    # LX/51M;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/51J;",
            "LX/51M",
            "<TE;>;)J"
        }
    .end annotation

    .prologue
    .line 824932
    if-nez p2, :cond_0

    .line 824933
    const-wide/16 v0, 0x0

    .line 824934
    :goto_0
    return-wide v0

    .line 824935
    :cond_0
    invoke-virtual {p0}, LX/4xC;->comparator()Ljava/util/Comparator;

    move-result-object v0

    iget-object v1, p0, LX/51O;->b:LX/4xo;

    .line 824936
    iget-object v2, v1, LX/4xo;->lowerEndpoint:Ljava/lang/Object;

    move-object v1, v2

    .line 824937
    iget-object v2, p2, LX/51M;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 824938
    if-gez v0, :cond_1

    .line 824939
    iget-object v0, p2, LX/51M;->f:LX/51M;

    invoke-direct {p0, p1, v0}, LX/51O;->a(LX/51J;LX/51M;)J

    move-result-wide v0

    goto :goto_0

    .line 824940
    :cond_1
    if-nez v0, :cond_2

    .line 824941
    sget-object v0, LX/51I;->a:[I

    iget-object v1, p0, LX/51O;->b:LX/4xo;

    .line 824942
    iget-object v2, v1, LX/4xo;->lowerBoundType:LX/4xG;

    move-object v1, v2

    .line 824943
    invoke-virtual {v1}, LX/4xG;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 824944
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 824945
    :pswitch_0
    invoke-virtual {p1, p2}, LX/51J;->nodeAggregate(LX/51M;)I

    move-result v0

    int-to-long v0, v0

    iget-object v2, p2, LX/51M;->f:LX/51M;

    invoke-virtual {p1, v2}, LX/51J;->treeAggregate(LX/51M;)J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_0

    .line 824946
    :pswitch_1
    iget-object v0, p2, LX/51M;->f:LX/51M;

    invoke-virtual {p1, v0}, LX/51J;->treeAggregate(LX/51M;)J

    move-result-wide v0

    goto :goto_0

    .line 824947
    :cond_2
    iget-object v0, p2, LX/51M;->f:LX/51M;

    invoke-virtual {p1, v0}, LX/51J;->treeAggregate(LX/51M;)J

    move-result-wide v0

    invoke-virtual {p1, p2}, LX/51J;->nodeAggregate(LX/51M;)I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iget-object v2, p2, LX/51M;->g:LX/51M;

    invoke-direct {p0, p1, v2}, LX/51O;->a(LX/51J;LX/51M;)J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b(LX/51J;LX/51M;)J
    .locals 4
    .param p2    # LX/51M;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/51J;",
            "LX/51M",
            "<TE;>;)J"
        }
    .end annotation

    .prologue
    .line 824948
    if-nez p2, :cond_0

    .line 824949
    const-wide/16 v0, 0x0

    .line 824950
    :goto_0
    return-wide v0

    .line 824951
    :cond_0
    invoke-virtual {p0}, LX/4xC;->comparator()Ljava/util/Comparator;

    move-result-object v0

    iget-object v1, p0, LX/51O;->b:LX/4xo;

    .line 824952
    iget-object v2, v1, LX/4xo;->upperEndpoint:Ljava/lang/Object;

    move-object v1, v2

    .line 824953
    iget-object v2, p2, LX/51M;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 824954
    if-lez v0, :cond_1

    .line 824955
    iget-object v0, p2, LX/51M;->g:LX/51M;

    invoke-direct {p0, p1, v0}, LX/51O;->b(LX/51J;LX/51M;)J

    move-result-wide v0

    goto :goto_0

    .line 824956
    :cond_1
    if-nez v0, :cond_2

    .line 824957
    sget-object v0, LX/51I;->a:[I

    iget-object v1, p0, LX/51O;->b:LX/4xo;

    .line 824958
    iget-object v2, v1, LX/4xo;->upperBoundType:LX/4xG;

    move-object v1, v2

    .line 824959
    invoke-virtual {v1}, LX/4xG;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 824960
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 824961
    :pswitch_0
    invoke-virtual {p1, p2}, LX/51J;->nodeAggregate(LX/51M;)I

    move-result v0

    int-to-long v0, v0

    iget-object v2, p2, LX/51M;->g:LX/51M;

    invoke-virtual {p1, v2}, LX/51J;->treeAggregate(LX/51M;)J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_0

    .line 824962
    :pswitch_1
    iget-object v0, p2, LX/51M;->g:LX/51M;

    invoke-virtual {p1, v0}, LX/51J;->treeAggregate(LX/51M;)J

    move-result-wide v0

    goto :goto_0

    .line 824963
    :cond_2
    iget-object v0, p2, LX/51M;->g:LX/51M;

    invoke-virtual {p1, v0}, LX/51J;->treeAggregate(LX/51M;)J

    move-result-wide v0

    invoke-virtual {p1, p2}, LX/51J;->nodeAggregate(LX/51M;)I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iget-object v2, p2, LX/51M;->f:LX/51M;

    invoke-direct {p0, p1, v2}, LX/51O;->b(LX/51J;LX/51M;)J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(LX/51O;LX/51M;)LX/4wx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/51M",
            "<TE;>;)",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 824964
    new-instance v0, LX/51F;

    invoke-direct {v0, p0, p1}, LX/51F;-><init>(LX/51O;LX/51M;)V

    return-object v0
.end method

.method public static b(LX/51M;LX/51M;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/51M",
            "<TT;>;",
            "LX/51M",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 824965
    iput-object p1, p0, LX/51M;->i:LX/51M;

    .line 824966
    iput-object p0, p1, LX/51M;->h:LX/51M;

    .line 824967
    return-void
.end method

.method public static b(LX/51M;LX/51M;LX/51M;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/51M",
            "<TT;>;",
            "LX/51M",
            "<TT;>;",
            "LX/51M",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 824968
    invoke-static {p0, p1}, LX/51O;->b(LX/51M;LX/51M;)V

    .line 824969
    invoke-static {p1, p2}, LX/51O;->b(LX/51M;LX/51M;)V

    .line 824970
    return-void
.end method

.method public static o()LX/51O;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Ljava/lang/Comparable;",
            ">()",
            "LX/51O",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 824907
    new-instance v0, LX/51O;

    .line 824908
    sget-object v1, LX/1zb;->a:LX/1zb;

    move-object v1, v1

    .line 824909
    invoke-direct {v0, v1}, LX/51O;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 3
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    .line 824977
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 824978
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    .line 824979
    const-class v1, LX/4xC;

    const-string v2, "comparator"

    invoke-static {v1, v2}, LX/50X;->a(Ljava/lang/Class;Ljava/lang/String;)LX/50W;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, LX/50W;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 824980
    const-class v1, LX/51O;

    const-string v2, "range"

    invoke-static {v1, v2}, LX/50X;->a(Ljava/lang/Class;Ljava/lang/String;)LX/50W;

    move-result-object v1

    invoke-static {v0}, LX/4xo;->a(Ljava/util/Comparator;)LX/4xo;

    move-result-object v0

    invoke-virtual {v1, p0, v0}, LX/50W;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 824981
    const-class v0, LX/51O;

    const-string v1, "rootReference"

    invoke-static {v0, v1}, LX/50X;->a(Ljava/lang/Class;Ljava/lang/String;)LX/50W;

    move-result-object v0

    new-instance v1, LX/51N;

    invoke-direct {v1}, LX/51N;-><init>()V

    invoke-virtual {v0, p0, v1}, LX/50W;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 824982
    new-instance v0, LX/51M;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, LX/51M;-><init>(Ljava/lang/Object;I)V

    .line 824983
    const-class v1, LX/51O;

    const-string v2, "header"

    invoke-static {v1, v2}, LX/50X;->a(Ljava/lang/Class;Ljava/lang/String;)LX/50W;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, LX/50W;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 824984
    invoke-static {v0, v0}, LX/51O;->b(LX/51M;LX/51M;)V

    .line 824985
    invoke-static {p0, p1}, LX/50X;->a(LX/1M1;Ljava/io/ObjectInputStream;)V

    .line 824986
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 824987
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 824988
    invoke-virtual {p0}, LX/4xC;->g()Ljava/util/NavigableSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableSet;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 824989
    invoke-static {p0, p1}, LX/50X;->a(LX/1M1;Ljava/io/ObjectOutputStream;)V

    .line 824990
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 824991
    :try_start_0
    iget-object v0, p0, LX/51O;->a:LX/51N;

    .line 824992
    iget-object v2, v0, LX/51N;->a:Ljava/lang/Object;

    move-object v0, v2

    .line 824993
    check-cast v0, LX/51M;

    .line 824994
    iget-object v2, p0, LX/51O;->b:LX/4xo;

    invoke-virtual {v2, p1}, LX/4xo;->c(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 824995
    :goto_0
    return v0

    .line 824996
    :cond_1
    invoke-virtual {p0}, LX/4xC;->comparator()Ljava/util/Comparator;

    move-result-object v2

    invoke-virtual {v0, v2, p1}, LX/51M;->a(Ljava/util/Comparator;Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    goto :goto_0

    .line 824997
    :catch_0
    move v0, v1

    goto :goto_0

    .line 824998
    :catch_1
    move v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;I)I
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;I)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 824999
    const-string v0, "occurrences"

    invoke-static {p2, v0}, LX/0P6;->a(ILjava/lang/String;)I

    .line 825000
    if-nez p2, :cond_0

    .line 825001
    invoke-virtual {p0, p1}, LX/1M0;->a(Ljava/lang/Object;)I

    move-result v0

    .line 825002
    :goto_0
    return v0

    .line 825003
    :cond_0
    iget-object v0, p0, LX/51O;->b:LX/4xo;

    invoke-virtual {v0, p1}, LX/4xo;->c(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 825004
    iget-object v0, p0, LX/51O;->a:LX/51N;

    .line 825005
    iget-object v2, v0, LX/51N;->a:Ljava/lang/Object;

    move-object v0, v2

    .line 825006
    check-cast v0, LX/51M;

    .line 825007
    if-nez v0, :cond_1

    .line 825008
    invoke-virtual {p0}, LX/4xC;->comparator()Ljava/util/Comparator;

    move-result-object v2

    invoke-interface {v2, p1, p1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    .line 825009
    new-instance v2, LX/51M;

    invoke-direct {v2, p1, p2}, LX/51M;-><init>(Ljava/lang/Object;I)V

    .line 825010
    iget-object v3, p0, LX/51O;->c:LX/51M;

    iget-object v4, p0, LX/51O;->c:LX/51M;

    invoke-static {v3, v2, v4}, LX/51O;->b(LX/51M;LX/51M;LX/51M;)V

    .line 825011
    iget-object v3, p0, LX/51O;->a:LX/51N;

    invoke-virtual {v3, v0, v2}, LX/51N;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    move v0, v1

    .line 825012
    goto :goto_0

    .line 825013
    :cond_1
    const/4 v2, 0x1

    new-array v2, v2, [I

    .line 825014
    invoke-virtual {p0}, LX/4xC;->comparator()Ljava/util/Comparator;

    move-result-object v3

    invoke-virtual {v0, v3, p1, p2, v2}, LX/51M;->a(Ljava/util/Comparator;Ljava/lang/Object;I[I)LX/51M;

    move-result-object v3

    .line 825015
    iget-object v4, p0, LX/51O;->a:LX/51N;

    invoke-virtual {v4, v0, v3}, LX/51N;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 825016
    aget v0, v2, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/4xG;)LX/4x9;
    .locals 12
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "LX/4xG;",
            ")",
            "LX/4x9",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 825017
    new-instance v0, LX/51O;

    iget-object v1, p0, LX/51O;->a:LX/51N;

    iget-object v2, p0, LX/51O;->b:LX/4xo;

    invoke-virtual {p0}, LX/4xC;->comparator()Ljava/util/Comparator;

    move-result-object v3

    .line 825018
    new-instance v4, LX/4xo;

    const/4 v6, 0x0

    const/4 v7, 0x0

    sget-object v8, LX/4xG;->OPEN:LX/4xG;

    const/4 v9, 0x1

    move-object v5, v3

    move-object v10, p1

    move-object v11, p2

    invoke-direct/range {v4 .. v11}, LX/4xo;-><init>(Ljava/util/Comparator;ZLjava/lang/Object;LX/4xG;ZLjava/lang/Object;LX/4xG;)V

    move-object v3, v4

    .line 825019
    invoke-virtual {v2, v3}, LX/4xo;->a(LX/4xo;)LX/4xo;

    move-result-object v2

    iget-object v3, p0, LX/51O;->c:LX/51M;

    invoke-direct {v0, v1, v2, v3}, LX/51O;-><init>(LX/51N;LX/4xo;LX/51M;)V

    return-object v0
.end method

.method public final bridge synthetic a()Ljava/util/Set;
    .locals 1

    .prologue
    .line 825020
    invoke-super {p0}, LX/4xC;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;II)Z
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;II)Z"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 825021
    const-string v0, "newCount"

    invoke-static {p3, v0}, LX/0P6;->a(ILjava/lang/String;)I

    .line 825022
    const-string v0, "oldCount"

    invoke-static {p2, v0}, LX/0P6;->a(ILjava/lang/String;)I

    .line 825023
    iget-object v0, p0, LX/51O;->b:LX/4xo;

    invoke-virtual {v0, p1}, LX/4xo;->c(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 825024
    iget-object v0, p0, LX/51O;->a:LX/51N;

    .line 825025
    iget-object v1, v0, LX/51N;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 825026
    check-cast v0, LX/51M;

    .line 825027
    if-nez v0, :cond_2

    .line 825028
    if-nez p2, :cond_1

    .line 825029
    if-lez p3, :cond_0

    .line 825030
    invoke-virtual {p0, p1, p3}, LX/1M0;->a(Ljava/lang/Object;I)I

    :cond_0
    move v0, v6

    .line 825031
    :goto_0
    return v0

    :cond_1
    move v0, v7

    .line 825032
    goto :goto_0

    .line 825033
    :cond_2
    new-array v5, v6, [I

    .line 825034
    invoke-virtual {p0}, LX/4xC;->comparator()Ljava/util/Comparator;

    move-result-object v1

    move-object v2, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, LX/51M;->a(Ljava/util/Comparator;Ljava/lang/Object;II[I)LX/51M;

    move-result-object v1

    .line 825035
    iget-object v2, p0, LX/51O;->a:LX/51N;

    invoke-virtual {v2, v0, v1}, LX/51N;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 825036
    aget v0, v5, v7

    if-ne v0, p2, :cond_3

    move v0, v6

    goto :goto_0

    :cond_3
    move v0, v7

    goto :goto_0
.end method

.method public final bridge synthetic add(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 824910
    invoke-super {p0, p1}, LX/4xC;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic addAll(Ljava/util/Collection;)Z
    .locals 1

    .prologue
    .line 824911
    invoke-super {p0, p1}, LX/4xC;->addAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;I)I
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 824860
    const-string v0, "occurrences"

    invoke-static {p2, v0}, LX/0P6;->a(ILjava/lang/String;)I

    .line 824861
    if-nez p2, :cond_0

    .line 824862
    invoke-virtual {p0, p1}, LX/1M0;->a(Ljava/lang/Object;)I

    move-result v0

    .line 824863
    :goto_0
    return v0

    .line 824864
    :cond_0
    iget-object v0, p0, LX/51O;->a:LX/51N;

    .line 824865
    iget-object v2, v0, LX/51N;->a:Ljava/lang/Object;

    move-object v0, v2

    .line 824866
    check-cast v0, LX/51M;

    .line 824867
    const/4 v2, 0x1

    new-array v2, v2, [I

    .line 824868
    :try_start_0
    iget-object v3, p0, LX/51O;->b:LX/4xo;

    invoke-virtual {v3, p1}, LX/4xo;->c(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    if-nez v0, :cond_2

    :cond_1
    move v0, v1

    .line 824869
    goto :goto_0

    .line 824870
    :cond_2
    invoke-virtual {p0}, LX/4xC;->comparator()Ljava/util/Comparator;

    move-result-object v3

    invoke-virtual {v0, v3, p1, p2, v2}, LX/51M;->b(Ljava/util/Comparator;Ljava/lang/Object;I[I)LX/51M;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 824871
    iget-object v4, p0, LX/51O;->a:LX/51N;

    invoke-virtual {v4, v0, v3}, LX/51N;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 824872
    aget v0, v2, v1

    goto :goto_0

    .line 824873
    :catch_0
    move v0, v1

    goto :goto_0

    .line 824874
    :catch_1
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;LX/4xG;)LX/4x9;
    .locals 12
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "LX/4xG;",
            ")",
            "LX/4x9",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 824875
    new-instance v0, LX/51O;

    iget-object v1, p0, LX/51O;->a:LX/51N;

    iget-object v2, p0, LX/51O;->b:LX/4xo;

    invoke-virtual {p0}, LX/4xC;->comparator()Ljava/util/Comparator;

    move-result-object v3

    .line 824876
    new-instance v4, LX/4xo;

    const/4 v6, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    sget-object v11, LX/4xG;->OPEN:LX/4xG;

    move-object v5, v3

    move-object v7, p1

    move-object v8, p2

    invoke-direct/range {v4 .. v11}, LX/4xo;-><init>(Ljava/util/Comparator;ZLjava/lang/Object;LX/4xG;ZLjava/lang/Object;LX/4xG;)V

    move-object v3, v4

    .line 824877
    invoke-virtual {v2, v3}, LX/4xo;->a(LX/4xo;)LX/4xo;

    move-result-object v2

    iget-object v3, p0, LX/51O;->c:LX/51M;

    invoke-direct {v0, v1, v2, v3}, LX/51O;-><init>(LX/51N;LX/4xo;LX/51M;)V

    return-object v0
.end method

.method public final b()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;>;"
        }
    .end annotation

    .prologue
    .line 824878
    new-instance v0, LX/51G;

    invoke-direct {v0, p0}, LX/51G;-><init>(LX/51O;)V

    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 824879
    sget-object v0, LX/51J;->DISTINCT:LX/51J;

    invoke-direct {p0, v0}, LX/51O;->a(LX/51J;)J

    move-result-wide v0

    invoke-static {v0, v1}, LX/0a4;->b(J)I

    move-result v0

    return v0
.end method

.method public final c(Ljava/lang/Object;I)I
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;I)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 824880
    const-string v0, "count"

    invoke-static {p2, v0}, LX/0P6;->a(ILjava/lang/String;)I

    .line 824881
    iget-object v0, p0, LX/51O;->b:LX/4xo;

    invoke-virtual {v0, p1}, LX/4xo;->c(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 824882
    if-nez p2, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 824883
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 824884
    goto :goto_0

    .line 824885
    :cond_2
    iget-object v0, p0, LX/51O;->a:LX/51N;

    .line 824886
    iget-object v3, v0, LX/51N;->a:Ljava/lang/Object;

    move-object v0, v3

    .line 824887
    check-cast v0, LX/51M;

    .line 824888
    if-nez v0, :cond_3

    .line 824889
    if-lez p2, :cond_0

    .line 824890
    invoke-virtual {p0, p1, p2}, LX/1M0;->a(Ljava/lang/Object;I)I

    goto :goto_1

    .line 824891
    :cond_3
    new-array v1, v1, [I

    .line 824892
    invoke-virtual {p0}, LX/4xC;->comparator()Ljava/util/Comparator;

    move-result-object v3

    invoke-virtual {v0, v3, p1, p2, v1}, LX/51M;->c(Ljava/util/Comparator;Ljava/lang/Object;I[I)LX/51M;

    move-result-object v3

    .line 824893
    iget-object v4, p0, LX/51O;->a:LX/51N;

    invoke-virtual {v4, v0, v3}, LX/51N;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 824894
    aget v2, v1, v2

    goto :goto_1
.end method

.method public final bridge synthetic clear()V
    .locals 0

    .prologue
    .line 824895
    invoke-super {p0}, LX/4xC;->clear()V

    return-void
.end method

.method public final bridge synthetic contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 824896
    invoke-super {p0, p1}, LX/4xC;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 824897
    invoke-super {p0, p1}, LX/4xC;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic hashCode()I
    .locals 1

    .prologue
    .line 824898
    invoke-super {p0}, LX/4xC;->hashCode()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic isEmpty()Z
    .locals 1

    .prologue
    .line 824899
    invoke-super {p0}, LX/4xC;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 824900
    invoke-super {p0}, LX/4xC;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;>;"
        }
    .end annotation

    .prologue
    .line 824901
    new-instance v0, LX/51H;

    invoke-direct {v0, p0}, LX/51H;-><init>(LX/51O;)V

    return-object v0
.end method

.method public final bridge synthetic remove(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 824902
    invoke-super {p0, p1}, LX/4xC;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic removeAll(Ljava/util/Collection;)Z
    .locals 1

    .prologue
    .line 824903
    invoke-super {p0, p1}, LX/4xC;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic retainAll(Ljava/util/Collection;)Z
    .locals 1

    .prologue
    .line 824904
    invoke-super {p0, p1}, LX/4xC;->retainAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final size()I
    .locals 2

    .prologue
    .line 824905
    sget-object v0, LX/51J;->SIZE:LX/51J;

    invoke-direct {p0, v0}, LX/51O;->a(LX/51J;)J

    move-result-wide v0

    invoke-static {v0, v1}, LX/0a4;->b(J)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 824906
    invoke-super {p0}, LX/4xC;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
