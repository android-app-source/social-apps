.class public final enum LX/52i;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/52i;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/52i;

.field public static final enum DEFAULT:LX/52i;

.field public static final enum NO:LX/52i;

.field public static final enum YES:LX/52i;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 826227
    new-instance v0, LX/52i;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, LX/52i;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/52i;->DEFAULT:LX/52i;

    .line 826228
    new-instance v0, LX/52i;

    const-string v1, "YES"

    invoke-direct {v0, v1, v3}, LX/52i;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/52i;->YES:LX/52i;

    .line 826229
    new-instance v0, LX/52i;

    const-string v1, "NO"

    invoke-direct {v0, v1, v4}, LX/52i;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/52i;->NO:LX/52i;

    .line 826230
    const/4 v0, 0x3

    new-array v0, v0, [LX/52i;

    sget-object v1, LX/52i;->DEFAULT:LX/52i;

    aput-object v1, v0, v2

    sget-object v1, LX/52i;->YES:LX/52i;

    aput-object v1, v0, v3

    sget-object v1, LX/52i;->NO:LX/52i;

    aput-object v1, v0, v4

    sput-object v0, LX/52i;->$VALUES:[LX/52i;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 826231
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/52i;
    .locals 1

    .prologue
    .line 826232
    const-class v0, LX/52i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/52i;

    return-object v0
.end method

.method public static values()[LX/52i;
    .locals 1

    .prologue
    .line 826233
    sget-object v0, LX/52i;->$VALUES:[LX/52i;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/52i;

    return-object v0
.end method
