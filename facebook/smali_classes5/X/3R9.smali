.class public LX/3R9;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 579130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 579131
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 579132
    return-void
.end method

.method public a(LX/3RF;)V
    .locals 0

    .prologue
    .line 579133
    return-void
.end method

.method public a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 0

    .prologue
    .line 579134
    return-void
.end method

.method public a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 579135
    return-void
.end method

.method public a(Lcom/facebook/messaging/notify/CalleeReadyNotification;)V
    .locals 0

    .prologue
    .line 579127
    return-void
.end method

.method public a(Lcom/facebook/messaging/notify/EventReminderNotification;)V
    .locals 0

    .prologue
    .line 579136
    return-void
.end method

.method public a(Lcom/facebook/messaging/notify/FailedToSendMessageNotification;)V
    .locals 0

    .prologue
    .line 579137
    return-void
.end method

.method public a(Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;)V
    .locals 0

    .prologue
    .line 579138
    return-void
.end method

.method public a(Lcom/facebook/messaging/notify/FriendInstallNotification;)V
    .locals 0

    .prologue
    .line 579139
    return-void
.end method

.method public a(Lcom/facebook/messaging/notify/IncomingCallNotification;)V
    .locals 0

    .prologue
    .line 579151
    return-void
.end method

.method public a(Lcom/facebook/messaging/notify/JoinRequestNotification;)V
    .locals 0

    .prologue
    .line 579140
    return-void
.end method

.method public a(Lcom/facebook/messaging/notify/LoggedOutMessageNotification;)V
    .locals 0

    .prologue
    .line 579141
    return-void
.end method

.method public a(Lcom/facebook/messaging/notify/MentionNotification;)V
    .locals 0

    .prologue
    .line 579142
    return-void
.end method

.method public a(Lcom/facebook/messaging/notify/MessageReactionNotification;)V
    .locals 0

    .prologue
    .line 579143
    return-void
.end method

.method public a(Lcom/facebook/messaging/notify/MessageRequestNotification;)V
    .locals 0

    .prologue
    .line 579144
    return-void
.end method

.method public final a(Lcom/facebook/messaging/notify/MessagingNotification;)V
    .locals 2

    .prologue
    .line 579145
    sget-object v0, LX/JuL;->a:[I

    iget-object v1, p1, Lcom/facebook/messaging/notify/MessagingNotification;->j:LX/3RF;

    invoke-virtual {v1}, LX/3RF;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 579146
    :goto_0
    :pswitch_0
    return-void

    .line 579147
    :pswitch_1
    check-cast p1, Lcom/facebook/messaging/notify/NewMessageNotification;

    invoke-virtual {p0, p1}, LX/3R9;->a(Lcom/facebook/messaging/notify/NewMessageNotification;)V

    goto :goto_0

    .line 579148
    :pswitch_2
    check-cast p1, Lcom/facebook/orca/notify/LoggedOutNotification;

    invoke-virtual {p0, p1}, LX/3R9;->a(Lcom/facebook/orca/notify/LoggedOutNotification;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lcom/facebook/messaging/notify/MissedCallNotification;)V
    .locals 0

    .prologue
    .line 579149
    return-void
.end method

.method public a(Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;)V
    .locals 0

    .prologue
    .line 579150
    return-void
.end method

.method public a(Lcom/facebook/messaging/notify/NewBuildNotification;)V
    .locals 0

    .prologue
    .line 579128
    return-void
.end method

.method public a(Lcom/facebook/messaging/notify/NewMessageNotification;)V
    .locals 0

    .prologue
    .line 579129
    return-void
.end method

.method public a(Lcom/facebook/messaging/notify/PaymentNotification;)V
    .locals 0

    .prologue
    .line 579088
    return-void
.end method

.method public a(Lcom/facebook/messaging/notify/PromotionNotification;)V
    .locals 0

    .prologue
    .line 579079
    return-void
.end method

.method public a(Lcom/facebook/messaging/notify/ReadThreadNotification;)V
    .locals 0

    .prologue
    .line 579087
    return-void
.end method

.method public a(Lcom/facebook/messaging/notify/SimpleMessageNotification;)V
    .locals 0

    .prologue
    .line 579086
    return-void
.end method

.method public a(Lcom/facebook/messaging/notify/StaleNotification;)V
    .locals 0

    .prologue
    .line 579085
    return-void
.end method

.method public a(Lcom/facebook/messaging/notify/TincanMessageRequestNotification;)V
    .locals 0

    .prologue
    .line 579084
    return-void
.end method

.method public a(Lcom/facebook/orca/notify/LoggedOutNotification;)V
    .locals 0

    .prologue
    .line 579083
    return-void
.end method

.method public a(Lcom/facebook/orca/notify/SwitchToFbAccountNotification;)V
    .locals 0

    .prologue
    .line 579082
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 579081
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 579080
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 579089
    return-void
.end method

.method public final b(Lcom/facebook/messaging/notify/MessagingNotification;)V
    .locals 2

    .prologue
    .line 579090
    sget-object v0, LX/JuL;->a:[I

    iget-object v1, p1, Lcom/facebook/messaging/notify/MessagingNotification;->j:LX/3RF;

    invoke-virtual {v1}, LX/3RF;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 579091
    :goto_0
    return-void

    .line 579092
    :pswitch_0
    check-cast p1, Lcom/facebook/messaging/notify/NewMessageNotification;

    invoke-virtual {p0, p1}, LX/3R9;->b(Lcom/facebook/messaging/notify/NewMessageNotification;)V

    goto :goto_0

    .line 579093
    :pswitch_1
    check-cast p1, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;

    invoke-virtual {p0, p1}, LX/3R9;->a(Lcom/facebook/messaging/notify/LoggedOutMessageNotification;)V

    goto :goto_0

    .line 579094
    :pswitch_2
    check-cast p1, Lcom/facebook/orca/notify/LoggedOutNotification;

    invoke-virtual {p0, p1}, LX/3R9;->a(Lcom/facebook/orca/notify/LoggedOutNotification;)V

    goto :goto_0

    .line 579095
    :pswitch_3
    check-cast p1, Lcom/facebook/messaging/notify/FriendInstallNotification;

    invoke-virtual {p0, p1}, LX/3R9;->a(Lcom/facebook/messaging/notify/FriendInstallNotification;)V

    goto :goto_0

    .line 579096
    :pswitch_4
    check-cast p1, Lcom/facebook/messaging/notify/PaymentNotification;

    invoke-virtual {p0, p1}, LX/3R9;->a(Lcom/facebook/messaging/notify/PaymentNotification;)V

    goto :goto_0

    .line 579097
    :pswitch_5
    check-cast p1, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;

    invoke-virtual {p0, p1}, LX/3R9;->a(Lcom/facebook/messaging/notify/FailedToSendMessageNotification;)V

    goto :goto_0

    .line 579098
    :pswitch_6
    check-cast p1, Lcom/facebook/messaging/notify/ReadThreadNotification;

    invoke-virtual {p0, p1}, LX/3R9;->a(Lcom/facebook/messaging/notify/ReadThreadNotification;)V

    goto :goto_0

    .line 579099
    :pswitch_7
    check-cast p1, Lcom/facebook/messaging/notify/NewBuildNotification;

    invoke-virtual {p0, p1}, LX/3R9;->a(Lcom/facebook/messaging/notify/NewBuildNotification;)V

    goto :goto_0

    .line 579100
    :pswitch_8
    check-cast p1, Lcom/facebook/messaging/notify/PromotionNotification;

    invoke-virtual {p0, p1}, LX/3R9;->a(Lcom/facebook/messaging/notify/PromotionNotification;)V

    goto :goto_0

    .line 579101
    :pswitch_9
    check-cast p1, Lcom/facebook/messaging/notify/StaleNotification;

    invoke-virtual {p0, p1}, LX/3R9;->a(Lcom/facebook/messaging/notify/StaleNotification;)V

    goto :goto_0

    .line 579102
    :pswitch_a
    check-cast p1, Lcom/facebook/messaging/notify/SimpleMessageNotification;

    invoke-virtual {p0, p1}, LX/3R9;->a(Lcom/facebook/messaging/notify/SimpleMessageNotification;)V

    goto :goto_0

    .line 579103
    :pswitch_b
    check-cast p1, Lcom/facebook/messaging/notify/MissedCallNotification;

    invoke-virtual {p0, p1}, LX/3R9;->a(Lcom/facebook/messaging/notify/MissedCallNotification;)V

    goto :goto_0

    .line 579104
    :pswitch_c
    check-cast p1, Lcom/facebook/messaging/notify/IncomingCallNotification;

    invoke-virtual {p0, p1}, LX/3R9;->a(Lcom/facebook/messaging/notify/IncomingCallNotification;)V

    goto :goto_0

    .line 579105
    :pswitch_d
    check-cast p1, Lcom/facebook/messaging/notify/CalleeReadyNotification;

    invoke-virtual {p0, p1}, LX/3R9;->a(Lcom/facebook/messaging/notify/CalleeReadyNotification;)V

    goto :goto_0

    .line 579106
    :pswitch_e
    check-cast p1, Lcom/facebook/messaging/notify/MessageRequestNotification;

    invoke-virtual {p0, p1}, LX/3R9;->a(Lcom/facebook/messaging/notify/MessageRequestNotification;)V

    goto :goto_0

    .line 579107
    :pswitch_f
    check-cast p1, Lcom/facebook/messaging/notify/TincanMessageRequestNotification;

    invoke-virtual {p0, p1}, LX/3R9;->a(Lcom/facebook/messaging/notify/TincanMessageRequestNotification;)V

    goto :goto_0

    .line 579108
    :pswitch_10
    check-cast p1, Lcom/facebook/messaging/notify/SimpleMessageNotification;

    invoke-virtual {p0, p1}, LX/3R9;->b(Lcom/facebook/messaging/notify/SimpleMessageNotification;)V

    goto :goto_0

    .line 579109
    :pswitch_11
    check-cast p1, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;

    invoke-virtual {p0, p1}, LX/3R9;->a(Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;)V

    goto :goto_0

    .line 579110
    :pswitch_12
    check-cast p1, Lcom/facebook/messaging/notify/JoinRequestNotification;

    invoke-virtual {p0, p1}, LX/3R9;->a(Lcom/facebook/messaging/notify/JoinRequestNotification;)V

    goto :goto_0

    .line 579111
    :pswitch_13
    check-cast p1, Lcom/facebook/orca/notify/SwitchToFbAccountNotification;

    invoke-virtual {p0, p1}, LX/3R9;->a(Lcom/facebook/orca/notify/SwitchToFbAccountNotification;)V

    goto :goto_0

    .line 579112
    :pswitch_14
    check-cast p1, Lcom/facebook/messaging/notify/EventReminderNotification;

    invoke-virtual {p0, p1}, LX/3R9;->a(Lcom/facebook/messaging/notify/EventReminderNotification;)V

    goto :goto_0

    .line 579113
    :pswitch_15
    check-cast p1, Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;

    invoke-virtual {p0, p1}, LX/3R9;->a(Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;)V

    goto/16 :goto_0

    .line 579114
    :pswitch_16
    check-cast p1, Lcom/facebook/messaging/notify/MentionNotification;

    invoke-virtual {p0, p1}, LX/3R9;->a(Lcom/facebook/messaging/notify/MentionNotification;)V

    goto/16 :goto_0

    .line 579115
    :pswitch_17
    check-cast p1, Lcom/facebook/messaging/notify/SimpleMessageNotification;

    invoke-virtual {p0, p1}, LX/3R9;->c(Lcom/facebook/messaging/notify/SimpleMessageNotification;)V

    goto/16 :goto_0

    .line 579116
    :pswitch_18
    check-cast p1, Lcom/facebook/messaging/notify/SimpleMessageNotification;

    invoke-virtual {p0, p1}, LX/3R9;->d(Lcom/facebook/messaging/notify/SimpleMessageNotification;)V

    goto/16 :goto_0

    .line 579117
    :pswitch_19
    check-cast p1, Lcom/facebook/messaging/notify/SimpleMessageNotification;

    invoke-virtual {p0, p1}, LX/3R9;->e(Lcom/facebook/messaging/notify/SimpleMessageNotification;)V

    goto/16 :goto_0

    .line 579118
    :pswitch_1a
    check-cast p1, Lcom/facebook/messaging/notify/SimpleMessageNotification;

    invoke-virtual {p0, p1}, LX/3R9;->f(Lcom/facebook/messaging/notify/SimpleMessageNotification;)V

    goto/16 :goto_0

    .line 579119
    :pswitch_1b
    check-cast p1, Lcom/facebook/messaging/notify/MessageReactionNotification;

    invoke-virtual {p0, p1}, LX/3R9;->a(Lcom/facebook/messaging/notify/MessageReactionNotification;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_e
        :pswitch_f
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_10
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
    .end packed-switch
.end method

.method public b(Lcom/facebook/messaging/notify/NewMessageNotification;)V
    .locals 0

    .prologue
    .line 579120
    return-void
.end method

.method public b(Lcom/facebook/messaging/notify/SimpleMessageNotification;)V
    .locals 0

    .prologue
    .line 579121
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 579122
    return-void
.end method

.method public c(Lcom/facebook/messaging/notify/SimpleMessageNotification;)V
    .locals 0

    .prologue
    .line 579123
    return-void
.end method

.method public d(Lcom/facebook/messaging/notify/SimpleMessageNotification;)V
    .locals 0

    .prologue
    .line 579124
    return-void
.end method

.method public e(Lcom/facebook/messaging/notify/SimpleMessageNotification;)V
    .locals 0

    .prologue
    .line 579125
    return-void
.end method

.method public f(Lcom/facebook/messaging/notify/SimpleMessageNotification;)V
    .locals 0

    .prologue
    .line 579126
    return-void
.end method
