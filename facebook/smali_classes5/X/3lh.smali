.class public LX/3lh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/2d7;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2ct;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2ct;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 634844
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 634845
    iput-object p1, p0, LX/3lh;->a:LX/0Ot;

    .line 634846
    return-void
.end method

.method public static b(LX/0QB;)LX/3lh;
    .locals 2

    .prologue
    .line 634833
    new-instance v0, LX/3lh;

    const/16 v1, 0xf59

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3lh;-><init>(LX/0Ot;)V

    .line 634834
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)V
    .locals 4
    .param p1    # Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 634837
    invoke-static {p2}, LX/2dT;->a(Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)Z

    move-result v0

    .line 634838
    invoke-static {p2}, LX/2dT;->b(Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)Z

    move-result v1

    .line 634839
    invoke-static {p1}, LX/2dT;->a(Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)Z

    move-result v2

    .line 634840
    invoke-static {p1}, LX/2dT;->b(Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)Z

    move-result v3

    .line 634841
    if-nez v1, :cond_0

    if-nez v3, :cond_1

    :cond_0
    if-nez v0, :cond_2

    if-eqz v2, :cond_2

    .line 634842
    :cond_1
    iget-object v0, p0, LX/3lh;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2ct;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2ct;->a(Z)V

    .line 634843
    :cond_2
    return-void
.end method

.method public final clearUserData()V
    .locals 2

    .prologue
    .line 634835
    iget-object v0, p0, LX/3lh;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2ct;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/2ct;->a(Z)V

    .line 634836
    return-void
.end method
