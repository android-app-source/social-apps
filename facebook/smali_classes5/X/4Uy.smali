.class public final LX/4Uy;
.super LX/4Uv;
.source ""


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/4Ux;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field public final c:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 3
    .param p1    # Ljava/util/Collection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 741866
    invoke-direct {p0}, LX/4Uv;-><init>()V

    .line 741867
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/4Uy;->a:Ljava/util/List;

    .line 741868
    iput v2, p0, LX/4Uy;->b:I

    .line 741869
    iput-object p1, p0, LX/4Uy;->c:Ljava/util/Collection;

    .line 741870
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 741848
    if-eqz p1, :cond_0

    .line 741849
    instance-of v0, p1, Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 741850
    invoke-virtual {p0}, LX/4Uv;->e()I

    move-result v1

    .line 741851
    if-lez v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    move-object v0, p1

    .line 741852
    check-cast v0, Ljava/lang/String;

    .line 741853
    iget v2, p0, LX/4Uy;->b:I

    iget-object v3, p0, LX/4Uy;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 741854
    iget-object v2, p0, LX/4Uy;->a:Ljava/util/List;

    iget v3, p0, LX/4Uy;->b:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/4Ux;

    .line 741855
    iput-object v0, v2, LX/4Ux;->a:Ljava/lang/String;

    .line 741856
    iput v1, v2, LX/4Ux;->b:I

    .line 741857
    :goto_1
    iget v2, p0, LX/4Uy;->b:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/4Uy;->b:I

    .line 741858
    iget-object v0, p0, LX/4Uy;->c:Ljava/util/Collection;

    if-eqz v0, :cond_0

    .line 741859
    iget-object v0, p0, LX/4Uy;->c:Ljava/util/Collection;

    check-cast p1, Ljava/lang/String;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 741860
    :cond_0
    return-void

    .line 741861
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 741862
    :cond_2
    new-instance v2, LX/4Ux;

    invoke-direct {v2}, LX/4Ux;-><init>()V

    .line 741863
    iput-object v0, v2, LX/4Ux;->a:Ljava/lang/String;

    .line 741864
    iput v1, v2, LX/4Ux;->b:I

    .line 741865
    iget-object v3, p0, LX/4Uy;->a:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final c(I)LX/4Ux;
    .locals 1

    .prologue
    .line 741845
    if-ltz p1, :cond_0

    iget v0, p0, LX/4Uy;->b:I

    if-lt p1, v0, :cond_1

    .line 741846
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 741847
    :cond_1
    iget-object v0, p0, LX/4Uy;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4Ux;

    return-object v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 741840
    const/4 v0, 0x0

    iput v0, p0, LX/4Uy;->b:I

    .line 741841
    iget-object v0, p0, LX/4Uv;->d:LX/446;

    invoke-virtual {v0}, LX/446;->c()V

    .line 741842
    iget-object v0, p0, LX/4Uv;->e:LX/446;

    invoke-virtual {v0}, LX/446;->c()V

    .line 741843
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 741844
    iget v0, p0, LX/4Uy;->b:I

    return v0
.end method
