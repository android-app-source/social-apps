.class public LX/3kb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0i1;


# instance fields
.field public a:J

.field private final b:LX/0SG;

.field public c:Z


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 633162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 633163
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3kb;->a:J

    .line 633164
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3kb;->c:Z

    .line 633165
    iput-object p1, p0, LX/3kb;->b:LX/0SG;

    .line 633166
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 633167
    const-wide/32 v0, 0xf731400

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 4

    .prologue
    .line 633168
    iget-boolean v0, p0, LX/3kb;->c:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, LX/3kb;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3kb;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iget-wide v2, p0, LX/3kb;->a:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xed4e00

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 633169
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    .line 633170
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 633171
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 633172
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 633173
    const-string v0, "3336"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 633174
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
