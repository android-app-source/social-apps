.class public final LX/4yn;
.super LX/0wv;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0wv",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/Iterable;

.field public final synthetic b:Ljava/util/Comparator;


# direct methods
.method public constructor <init>(Ljava/lang/Iterable;Ljava/util/Comparator;)V
    .locals 0

    .prologue
    .line 822029
    iput-object p1, p0, LX/4yn;->a:Ljava/lang/Iterable;

    iput-object p2, p0, LX/4yn;->b:Ljava/util/Comparator;

    invoke-direct {p0}, LX/0wv;-><init>()V

    return-void
.end method


# virtual methods
.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 822030
    iget-object v0, p0, LX/4yn;->a:Ljava/lang/Iterable;

    .line 822031
    new-instance v1, LX/4yo;

    invoke-direct {v1}, LX/4yo;-><init>()V

    move-object v1, v1

    .line 822032
    invoke-static {v0, v1}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0QK;)Ljava/lang/Iterable;

    move-result-object v0

    iget-object v1, p0, LX/4yn;->b:Ljava/util/Comparator;

    .line 822033
    const-string p0, "iterators"

    invoke-static {v0, p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 822034
    const-string p0, "comparator"

    invoke-static {v1, p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 822035
    new-instance p0, LX/4yt;

    invoke-direct {p0, v0, v1}, LX/4yt;-><init>(Ljava/lang/Iterable;Ljava/util/Comparator;)V

    move-object v0, p0

    .line 822036
    return-object v0
.end method
