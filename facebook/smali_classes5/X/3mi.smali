.class public final LX/3mi;
.super LX/1OX;
.source ""


# instance fields
.field private final a:LX/3mX;


# direct methods
.method public constructor <init>(LX/3mX;)V
    .locals 0

    .prologue
    .line 637129
    invoke-direct {p0}, LX/1OX;-><init>()V

    .line 637130
    iput-object p1, p0, LX/3mi;->a:LX/3mX;

    .line 637131
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 5

    .prologue
    .line 637132
    invoke-super {p0, p1, p2}, LX/1OX;->a(Landroid/support/v7/widget/RecyclerView;I)V

    .line 637133
    iget-object v0, p0, LX/3mi;->a:LX/3mX;

    .line 637134
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v1

    check-cast v1, LX/25T;

    .line 637135
    invoke-virtual {v1}, LX/1P1;->l()I

    move-result v1

    .line 637136
    if-nez p2, :cond_0

    .line 637137
    invoke-static {v0, v1}, LX/3mX;->h(LX/3mX;I)V

    .line 637138
    :cond_0
    iget-object v1, v0, LX/3mX;->f:LX/25M;

    iget-object v1, v1, LX/25M;->f:LX/HJK;

    if-eqz v1, :cond_1

    .line 637139
    iget-object v1, v0, LX/3mX;->f:LX/25M;

    iget-object v1, v1, LX/25M;->f:LX/HJK;

    const/4 p0, 0x0

    .line 637140
    iget-object v2, v1, LX/HJK;->a:LX/2km;

    check-cast v2, LX/1Pr;

    iget-object v3, v1, LX/HJK;->b:LX/HLF;

    iget-object v4, v1, LX/HJK;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-interface {v2, v3, v4}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/HLG;

    .line 637141
    iget-boolean v3, v2, LX/HLG;->b:Z

    if-eqz v3, :cond_1

    if-nez p2, :cond_1

    .line 637142
    iput-boolean p0, v2, LX/HLG;->b:Z

    .line 637143
    iget-object v3, v1, LX/HJK;->a:LX/2km;

    check-cast v3, LX/1Pr;

    iget-object v4, v1, LX/HJK;->b:LX/HLF;

    invoke-interface {v3, v4, v2}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 637144
    iget-object v2, v1, LX/HJK;->a:LX/2km;

    check-cast v2, LX/1Pq;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, v1, LX/HJK;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 637145
    iget-object v1, v4, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v4, v1

    .line 637146
    aput-object v4, v3, p0

    invoke-interface {v2, v3}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 637147
    :cond_1
    check-cast p1, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-static {v0, p1}, LX/3mX;->d(LX/3mX;Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    .line 637148
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 5

    .prologue
    .line 637149
    iget-object v0, p0, LX/3mi;->a:LX/3mX;

    .line 637150
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v1

    check-cast v1, LX/25T;

    .line 637151
    invoke-virtual {v1}, LX/1P1;->l()I

    move-result v2

    .line 637152
    invoke-virtual {v1}, LX/1P1;->n()I

    move-result v3

    .line 637153
    invoke-virtual {v1, v2}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/components/ComponentView;

    .line 637154
    if-eqz v2, :cond_1

    .line 637155
    invoke-virtual {v2}, Lcom/facebook/components/ComponentView;->k()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 637156
    invoke-virtual {v2}, Lcom/facebook/components/ComponentView;->j()V

    .line 637157
    :cond_0
    iget-boolean v4, v0, LX/3mX;->h:Z

    if-nez v4, :cond_1

    .line 637158
    invoke-virtual {v2}, Lcom/facebook/components/ComponentView;->getWidth()I

    move-result v4

    move-object v2, p1

    .line 637159
    check-cast v2, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result p0

    invoke-virtual {v2, v4, p0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->h(II)V

    .line 637160
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/3mX;->h:Z

    .line 637161
    :cond_1
    invoke-virtual {v1, v3}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/components/ComponentView;

    .line 637162
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/components/ComponentView;->k()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 637163
    invoke-virtual {v2}, Lcom/facebook/components/ComponentView;->j()V

    .line 637164
    :cond_2
    invoke-virtual {v1}, LX/1P1;->m()I

    move-result v1

    .line 637165
    const/4 v2, -0x1

    if-eq v1, v2, :cond_4

    iget v2, v0, LX/3mX;->j:I

    if-eq v1, v2, :cond_4

    .line 637166
    invoke-virtual {v0, v1}, LX/3mX;->g(I)V

    .line 637167
    iput v1, v0, LX/3mX;->j:I

    .line 637168
    iget-object v2, v0, LX/3mX;->f:LX/25M;

    iget-object v2, v2, LX/25M;->g:LX/25K;

    if-eqz v2, :cond_3

    .line 637169
    iget-object v2, v0, LX/3mX;->f:LX/25M;

    iget-object v2, v2, LX/25M;->g:LX/25K;

    iget-object v3, v0, LX/3mX;->a:LX/0Px;

    invoke-interface {v2, v1, v3}, LX/25K;->a(ILX/0Px;)V

    .line 637170
    :cond_3
    iget-object v2, v0, LX/3mX;->k:LX/8yz;

    if-eqz v2, :cond_4

    .line 637171
    iget-object v2, v0, LX/3mX;->k:LX/8yz;

    invoke-virtual {v2, v1}, LX/8yz;->a(I)V

    .line 637172
    :cond_4
    check-cast p1, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-static {v0, p1}, LX/3mX;->d(LX/3mX;Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    .line 637173
    return-void
.end method
