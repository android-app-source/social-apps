.class public final LX/3TX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

.field private b:I


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V
    .locals 1

    .prologue
    .line 584310
    iput-object p1, p0, LX/3TX;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 584311
    const/4 v0, 0x0

    iput v0, p0, LX/3TX;->b:I

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 1

    .prologue
    .line 584325
    iget-object v0, p0, LX/3TX;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v0, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->I:LX/2ix;

    invoke-virtual {v0, p1, p2}, LX/2ix;->a(LX/0g8;I)V

    .line 584326
    iput p2, p0, LX/3TX;->b:I

    .line 584327
    return-void
.end method

.method public final a(LX/0g8;III)V
    .locals 3

    .prologue
    .line 584312
    iget-object v0, p0, LX/3TX;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    .line 584313
    iput p2, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->av:I

    .line 584314
    iget-object v0, p0, LX/3TX;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    .line 584315
    iput p3, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aD:I

    .line 584316
    iget-object v1, p0, LX/3TX;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    add-int v0, p2, p3

    if-ne v0, p4, :cond_2

    add-int v0, p2, p3

    add-int/lit8 v0, v0, -0x1

    iget-object v2, p0, LX/3TX;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget v2, v2, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aA:I

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 584317
    :goto_0
    iput v0, v1, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aA:I

    .line 584318
    iget v0, p0, LX/3TX;->b:I

    if-eqz v0, :cond_0

    .line 584319
    iget-object v0, p0, LX/3TX;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    const/4 v1, 0x1

    .line 584320
    iput-boolean v1, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aB:Z

    .line 584321
    :cond_0
    add-int v0, p2, p3

    add-int/lit8 v0, v0, 0x3

    if-lt v0, p4, :cond_1

    .line 584322
    iget-object v0, p0, LX/3TX;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->b$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;Z)V

    .line 584323
    :cond_1
    return-void

    .line 584324
    :cond_2
    add-int v0, p2, p3

    iget-object v2, p0, LX/3TX;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget v2, v2, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aA:I

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method
