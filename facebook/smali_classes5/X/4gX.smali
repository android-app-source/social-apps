.class public LX/4gX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/4gV;

.field public b:LX/2GX;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private c:LX/4gV;

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1iq;",
            ">;"
        }
    .end annotation
.end field

.field private f:I


# direct methods
.method public constructor <init>(LX/13S;LX/0W3;Ljava/util/Random;)V
    .locals 1
    .param p3    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param

    .prologue
    .line 799751
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799752
    new-instance v0, LX/4gV;

    invoke-direct {v0}, LX/4gV;-><init>()V

    iput-object v0, p0, LX/4gX;->a:LX/4gV;

    .line 799753
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/4gX;->e:Ljava/util/Set;

    .line 799754
    const/4 v0, -0x1

    iput v0, p0, LX/4gX;->f:I

    .line 799755
    new-instance v0, LX/2GX;

    invoke-direct {v0, p2, p3}, LX/2GX;-><init>(LX/0W3;Ljava/util/Random;)V

    iput-object v0, p0, LX/4gX;->b:LX/2GX;

    .line 799756
    const-string v0, "localstats_histogram"

    const-string p2, "histogram_data"

    new-instance p3, LX/4gW;

    invoke-direct {p3, p0}, LX/4gW;-><init>(LX/4gX;)V

    invoke-virtual {p1, v0, p2, p3}, LX/13S;->a(Ljava/lang/String;Ljava/lang/String;LX/13W;)LX/13S;

    .line 799757
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 799758
    iget-object v1, p0, LX/4gX;->e:Ljava/util/Set;

    monitor-enter v1

    .line 799759
    :try_start_0
    iget-object v0, p0, LX/4gX;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1iq;

    .line 799760
    invoke-interface {v0}, LX/1iq;->a()V

    goto :goto_0

    .line 799761
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method public final a(I)LX/0lF;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 799762
    invoke-virtual {p0, p1}, LX/4gX;->b(I)V

    .line 799763
    iget-object v0, p0, LX/4gX;->c:LX/4gV;

    .line 799764
    iget-object v1, v0, LX/4gV;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    move v0, v1

    .line 799765
    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/4gX;->c:LX/4gV;

    iget-object v1, p0, LX/4gX;->d:Ljava/util/Map;

    invoke-virtual {v0, v1}, LX/4gV;->a(Ljava/util/Map;)LX/0lF;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 799766
    iget v0, p0, LX/4gX;->f:I

    if-eq v0, p1, :cond_0

    .line 799767
    iput p1, p0, LX/4gX;->f:I

    .line 799768
    invoke-direct {p0}, LX/4gX;->a()V

    .line 799769
    iget-object v0, p0, LX/4gX;->a:LX/4gV;

    invoke-virtual {v0}, LX/4gV;->b()LX/4gV;

    move-result-object v0

    iput-object v0, p0, LX/4gX;->c:LX/4gV;

    .line 799770
    iget-object v0, p0, LX/4gX;->b:LX/2GX;

    invoke-virtual {v0}, LX/2GX;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/4gX;->d:Ljava/util/Map;

    .line 799771
    :cond_0
    return-void
.end method
