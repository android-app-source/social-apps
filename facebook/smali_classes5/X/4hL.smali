.class public LX/4hL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Externalizable;


# instance fields
.field private carrierSpecific_:LX/4hP;

.field public countryCode_:I

.field private emergency_:LX/4hP;

.field public fixedLine_:LX/4hP;

.field public generalDesc_:LX/4hP;

.field private hasCarrierSpecific:Z

.field public hasCountryCode:Z

.field private hasEmergency:Z

.field private hasFixedLine:Z

.field private hasGeneralDesc:Z

.field public hasId:Z

.field private hasInternationalPrefix:Z

.field public hasLeadingDigits:Z

.field public hasLeadingZeroPossible:Z

.field public hasMainCountryForCode:Z

.field private hasMobile:Z

.field public hasMobileNumberPortableRegion:Z

.field public hasNationalPrefix:Z

.field public hasNationalPrefixForParsing:Z

.field public hasNationalPrefixTransformRule:Z

.field private hasNoInternationalDialling:Z

.field private hasPager:Z

.field private hasPersonalNumber:Z

.field public hasPreferredExtnPrefix:Z

.field public hasPreferredInternationalPrefix:Z

.field private hasPremiumRate:Z

.field public hasSameMobileAndFixedLinePattern:Z

.field private hasSharedCost:Z

.field private hasShortCode:Z

.field private hasStandardRate:Z

.field private hasTollFree:Z

.field private hasUan:Z

.field private hasVoicemail:Z

.field private hasVoip:Z

.field public id_:Ljava/lang/String;

.field public internationalPrefix_:Ljava/lang/String;

.field public intlNumberFormat_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/4hJ;",
            ">;"
        }
    .end annotation
.end field

.field public leadingDigits_:Ljava/lang/String;

.field public leadingZeroPossible_:Z

.field public mainCountryForCode_:Z

.field public mobileNumberPortableRegion_:Z

.field public mobile_:LX/4hP;

.field public nationalPrefixForParsing_:Ljava/lang/String;

.field public nationalPrefixTransformRule_:Ljava/lang/String;

.field public nationalPrefix_:Ljava/lang/String;

.field private noInternationalDialling_:LX/4hP;

.field public numberFormat_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/4hJ;",
            ">;"
        }
    .end annotation
.end field

.field public pager_:LX/4hP;

.field public personalNumber_:LX/4hP;

.field public preferredExtnPrefix_:Ljava/lang/String;

.field public preferredInternationalPrefix_:Ljava/lang/String;

.field public premiumRate_:LX/4hP;

.field public sameMobileAndFixedLinePattern_:Z

.field public sharedCost_:LX/4hP;

.field private shortCode_:LX/4hP;

.field private standardRate_:LX/4hP;

.field public tollFree_:LX/4hP;

.field public uan_:LX/4hP;

.field public voicemail_:LX/4hP;

.field public voip_:LX/4hP;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 801209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 801210
    iput-object v0, p0, LX/4hL;->generalDesc_:LX/4hP;

    .line 801211
    iput-object v0, p0, LX/4hL;->fixedLine_:LX/4hP;

    .line 801212
    iput-object v0, p0, LX/4hL;->mobile_:LX/4hP;

    .line 801213
    iput-object v0, p0, LX/4hL;->tollFree_:LX/4hP;

    .line 801214
    iput-object v0, p0, LX/4hL;->premiumRate_:LX/4hP;

    .line 801215
    iput-object v0, p0, LX/4hL;->sharedCost_:LX/4hP;

    .line 801216
    iput-object v0, p0, LX/4hL;->personalNumber_:LX/4hP;

    .line 801217
    iput-object v0, p0, LX/4hL;->voip_:LX/4hP;

    .line 801218
    iput-object v0, p0, LX/4hL;->pager_:LX/4hP;

    .line 801219
    iput-object v0, p0, LX/4hL;->uan_:LX/4hP;

    .line 801220
    iput-object v0, p0, LX/4hL;->emergency_:LX/4hP;

    .line 801221
    iput-object v0, p0, LX/4hL;->voicemail_:LX/4hP;

    .line 801222
    iput-object v0, p0, LX/4hL;->shortCode_:LX/4hP;

    .line 801223
    iput-object v0, p0, LX/4hL;->standardRate_:LX/4hP;

    .line 801224
    iput-object v0, p0, LX/4hL;->carrierSpecific_:LX/4hP;

    .line 801225
    iput-object v0, p0, LX/4hL;->noInternationalDialling_:LX/4hP;

    .line 801226
    const-string v0, ""

    iput-object v0, p0, LX/4hL;->id_:Ljava/lang/String;

    .line 801227
    iput v1, p0, LX/4hL;->countryCode_:I

    .line 801228
    const-string v0, ""

    iput-object v0, p0, LX/4hL;->internationalPrefix_:Ljava/lang/String;

    .line 801229
    const-string v0, ""

    iput-object v0, p0, LX/4hL;->preferredInternationalPrefix_:Ljava/lang/String;

    .line 801230
    const-string v0, ""

    iput-object v0, p0, LX/4hL;->nationalPrefix_:Ljava/lang/String;

    .line 801231
    const-string v0, ""

    iput-object v0, p0, LX/4hL;->preferredExtnPrefix_:Ljava/lang/String;

    .line 801232
    const-string v0, ""

    iput-object v0, p0, LX/4hL;->nationalPrefixForParsing_:Ljava/lang/String;

    .line 801233
    const-string v0, ""

    iput-object v0, p0, LX/4hL;->nationalPrefixTransformRule_:Ljava/lang/String;

    .line 801234
    iput-boolean v1, p0, LX/4hL;->sameMobileAndFixedLinePattern_:Z

    .line 801235
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/4hL;->numberFormat_:Ljava/util/List;

    .line 801236
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/4hL;->intlNumberFormat_:Ljava/util/List;

    .line 801237
    iput-boolean v1, p0, LX/4hL;->mainCountryForCode_:Z

    .line 801238
    const-string v0, ""

    iput-object v0, p0, LX/4hL;->leadingDigits_:Ljava/lang/String;

    .line 801239
    iput-boolean v1, p0, LX/4hL;->leadingZeroPossible_:Z

    .line 801240
    iput-boolean v1, p0, LX/4hL;->mobileNumberPortableRegion_:Z

    .line 801241
    return-void
.end method

.method public static newBuilder()LX/4hM;
    .locals 1

    .prologue
    .line 801208
    new-instance v0, LX/4hM;

    invoke-direct {v0}, LX/4hM;-><init>()V

    return-object v0
.end method


# virtual methods
.method public intlNumberFormatSize()I
    .locals 1

    .prologue
    .line 801207
    iget-object v0, p0, LX/4hL;->intlNumberFormat_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 801065
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v1

    .line 801066
    if-eqz v1, :cond_0

    .line 801067
    new-instance v1, LX/4hP;

    invoke-direct {v1}, LX/4hP;-><init>()V

    .line 801068
    invoke-virtual {v1, p1}, LX/4hP;->readExternal(Ljava/io/ObjectInput;)V

    .line 801069
    invoke-virtual {p0, v1}, LX/4hL;->setGeneralDesc(LX/4hP;)LX/4hL;

    .line 801070
    :cond_0
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v1

    .line 801071
    if-eqz v1, :cond_1

    .line 801072
    new-instance v1, LX/4hP;

    invoke-direct {v1}, LX/4hP;-><init>()V

    .line 801073
    invoke-virtual {v1, p1}, LX/4hP;->readExternal(Ljava/io/ObjectInput;)V

    .line 801074
    invoke-virtual {p0, v1}, LX/4hL;->setFixedLine(LX/4hP;)LX/4hL;

    .line 801075
    :cond_1
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v1

    .line 801076
    if-eqz v1, :cond_2

    .line 801077
    new-instance v1, LX/4hP;

    invoke-direct {v1}, LX/4hP;-><init>()V

    .line 801078
    invoke-virtual {v1, p1}, LX/4hP;->readExternal(Ljava/io/ObjectInput;)V

    .line 801079
    invoke-virtual {p0, v1}, LX/4hL;->setMobile(LX/4hP;)LX/4hL;

    .line 801080
    :cond_2
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v1

    .line 801081
    if-eqz v1, :cond_3

    .line 801082
    new-instance v1, LX/4hP;

    invoke-direct {v1}, LX/4hP;-><init>()V

    .line 801083
    invoke-virtual {v1, p1}, LX/4hP;->readExternal(Ljava/io/ObjectInput;)V

    .line 801084
    invoke-virtual {p0, v1}, LX/4hL;->setTollFree(LX/4hP;)LX/4hL;

    .line 801085
    :cond_3
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v1

    .line 801086
    if-eqz v1, :cond_4

    .line 801087
    new-instance v1, LX/4hP;

    invoke-direct {v1}, LX/4hP;-><init>()V

    .line 801088
    invoke-virtual {v1, p1}, LX/4hP;->readExternal(Ljava/io/ObjectInput;)V

    .line 801089
    invoke-virtual {p0, v1}, LX/4hL;->setPremiumRate(LX/4hP;)LX/4hL;

    .line 801090
    :cond_4
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v1

    .line 801091
    if-eqz v1, :cond_5

    .line 801092
    new-instance v1, LX/4hP;

    invoke-direct {v1}, LX/4hP;-><init>()V

    .line 801093
    invoke-virtual {v1, p1}, LX/4hP;->readExternal(Ljava/io/ObjectInput;)V

    .line 801094
    invoke-virtual {p0, v1}, LX/4hL;->setSharedCost(LX/4hP;)LX/4hL;

    .line 801095
    :cond_5
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v1

    .line 801096
    if-eqz v1, :cond_6

    .line 801097
    new-instance v1, LX/4hP;

    invoke-direct {v1}, LX/4hP;-><init>()V

    .line 801098
    invoke-virtual {v1, p1}, LX/4hP;->readExternal(Ljava/io/ObjectInput;)V

    .line 801099
    invoke-virtual {p0, v1}, LX/4hL;->setPersonalNumber(LX/4hP;)LX/4hL;

    .line 801100
    :cond_6
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v1

    .line 801101
    if-eqz v1, :cond_7

    .line 801102
    new-instance v1, LX/4hP;

    invoke-direct {v1}, LX/4hP;-><init>()V

    .line 801103
    invoke-virtual {v1, p1}, LX/4hP;->readExternal(Ljava/io/ObjectInput;)V

    .line 801104
    invoke-virtual {p0, v1}, LX/4hL;->setVoip(LX/4hP;)LX/4hL;

    .line 801105
    :cond_7
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v1

    .line 801106
    if-eqz v1, :cond_8

    .line 801107
    new-instance v1, LX/4hP;

    invoke-direct {v1}, LX/4hP;-><init>()V

    .line 801108
    invoke-virtual {v1, p1}, LX/4hP;->readExternal(Ljava/io/ObjectInput;)V

    .line 801109
    invoke-virtual {p0, v1}, LX/4hL;->setPager(LX/4hP;)LX/4hL;

    .line 801110
    :cond_8
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v1

    .line 801111
    if-eqz v1, :cond_9

    .line 801112
    new-instance v1, LX/4hP;

    invoke-direct {v1}, LX/4hP;-><init>()V

    .line 801113
    invoke-virtual {v1, p1}, LX/4hP;->readExternal(Ljava/io/ObjectInput;)V

    .line 801114
    invoke-virtual {p0, v1}, LX/4hL;->setUan(LX/4hP;)LX/4hL;

    .line 801115
    :cond_9
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v1

    .line 801116
    if-eqz v1, :cond_a

    .line 801117
    new-instance v1, LX/4hP;

    invoke-direct {v1}, LX/4hP;-><init>()V

    .line 801118
    invoke-virtual {v1, p1}, LX/4hP;->readExternal(Ljava/io/ObjectInput;)V

    .line 801119
    invoke-virtual {p0, v1}, LX/4hL;->setEmergency(LX/4hP;)LX/4hL;

    .line 801120
    :cond_a
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v1

    .line 801121
    if-eqz v1, :cond_b

    .line 801122
    new-instance v1, LX/4hP;

    invoke-direct {v1}, LX/4hP;-><init>()V

    .line 801123
    invoke-virtual {v1, p1}, LX/4hP;->readExternal(Ljava/io/ObjectInput;)V

    .line 801124
    invoke-virtual {p0, v1}, LX/4hL;->setVoicemail(LX/4hP;)LX/4hL;

    .line 801125
    :cond_b
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v1

    .line 801126
    if-eqz v1, :cond_c

    .line 801127
    new-instance v1, LX/4hP;

    invoke-direct {v1}, LX/4hP;-><init>()V

    .line 801128
    invoke-virtual {v1, p1}, LX/4hP;->readExternal(Ljava/io/ObjectInput;)V

    .line 801129
    invoke-virtual {p0, v1}, LX/4hL;->setShortCode(LX/4hP;)LX/4hL;

    .line 801130
    :cond_c
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v1

    .line 801131
    if-eqz v1, :cond_d

    .line 801132
    new-instance v1, LX/4hP;

    invoke-direct {v1}, LX/4hP;-><init>()V

    .line 801133
    invoke-virtual {v1, p1}, LX/4hP;->readExternal(Ljava/io/ObjectInput;)V

    .line 801134
    invoke-virtual {p0, v1}, LX/4hL;->setStandardRate(LX/4hP;)LX/4hL;

    .line 801135
    :cond_d
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v1

    .line 801136
    if-eqz v1, :cond_e

    .line 801137
    new-instance v1, LX/4hP;

    invoke-direct {v1}, LX/4hP;-><init>()V

    .line 801138
    invoke-virtual {v1, p1}, LX/4hP;->readExternal(Ljava/io/ObjectInput;)V

    .line 801139
    invoke-virtual {p0, v1}, LX/4hL;->setCarrierSpecific(LX/4hP;)LX/4hL;

    .line 801140
    :cond_e
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v1

    .line 801141
    if-eqz v1, :cond_f

    .line 801142
    new-instance v1, LX/4hP;

    invoke-direct {v1}, LX/4hP;-><init>()V

    .line 801143
    invoke-virtual {v1, p1}, LX/4hP;->readExternal(Ljava/io/ObjectInput;)V

    .line 801144
    invoke-virtual {p0, v1}, LX/4hL;->setNoInternationalDialling(LX/4hP;)LX/4hL;

    .line 801145
    :cond_f
    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v1

    .line 801146
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/4hL;->hasId:Z

    .line 801147
    iput-object v1, p0, LX/4hL;->id_:Ljava/lang/String;

    .line 801148
    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v1

    .line 801149
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/4hL;->hasCountryCode:Z

    .line 801150
    iput v1, p0, LX/4hL;->countryCode_:I

    .line 801151
    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/4hL;->setInternationalPrefix(Ljava/lang/String;)LX/4hL;

    .line 801152
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v1

    .line 801153
    if-eqz v1, :cond_10

    .line 801154
    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v1

    .line 801155
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/4hL;->hasPreferredInternationalPrefix:Z

    .line 801156
    iput-object v1, p0, LX/4hL;->preferredInternationalPrefix_:Ljava/lang/String;

    .line 801157
    :cond_10
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v1

    .line 801158
    if-eqz v1, :cond_11

    .line 801159
    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v1

    .line 801160
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/4hL;->hasNationalPrefix:Z

    .line 801161
    iput-object v1, p0, LX/4hL;->nationalPrefix_:Ljava/lang/String;

    .line 801162
    :cond_11
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v1

    .line 801163
    if-eqz v1, :cond_12

    .line 801164
    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v1

    .line 801165
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/4hL;->hasPreferredExtnPrefix:Z

    .line 801166
    iput-object v1, p0, LX/4hL;->preferredExtnPrefix_:Ljava/lang/String;

    .line 801167
    :cond_12
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v1

    .line 801168
    if-eqz v1, :cond_13

    .line 801169
    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v1

    .line 801170
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/4hL;->hasNationalPrefixForParsing:Z

    .line 801171
    iput-object v1, p0, LX/4hL;->nationalPrefixForParsing_:Ljava/lang/String;

    .line 801172
    :cond_13
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v1

    .line 801173
    if-eqz v1, :cond_14

    .line 801174
    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v1

    .line 801175
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/4hL;->hasNationalPrefixTransformRule:Z

    .line 801176
    iput-object v1, p0, LX/4hL;->nationalPrefixTransformRule_:Ljava/lang/String;

    .line 801177
    :cond_14
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v1

    .line 801178
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/4hL;->hasSameMobileAndFixedLinePattern:Z

    .line 801179
    iput-boolean v1, p0, LX/4hL;->sameMobileAndFixedLinePattern_:Z

    .line 801180
    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v2

    move v1, v0

    .line 801181
    :goto_0
    if-ge v1, v2, :cond_15

    .line 801182
    new-instance v3, LX/4hJ;

    invoke-direct {v3}, LX/4hJ;-><init>()V

    .line 801183
    invoke-virtual {v3, p1}, LX/4hJ;->readExternal(Ljava/io/ObjectInput;)V

    .line 801184
    iget-object v4, p0, LX/4hL;->numberFormat_:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 801185
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 801186
    :cond_15
    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v1

    .line 801187
    :goto_1
    if-ge v0, v1, :cond_16

    .line 801188
    new-instance v2, LX/4hJ;

    invoke-direct {v2}, LX/4hJ;-><init>()V

    .line 801189
    invoke-virtual {v2, p1}, LX/4hJ;->readExternal(Ljava/io/ObjectInput;)V

    .line 801190
    iget-object v3, p0, LX/4hL;->intlNumberFormat_:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 801191
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 801192
    :cond_16
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    .line 801193
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/4hL;->hasMainCountryForCode:Z

    .line 801194
    iput-boolean v0, p0, LX/4hL;->mainCountryForCode_:Z

    .line 801195
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    .line 801196
    if-eqz v0, :cond_17

    .line 801197
    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v0

    .line 801198
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/4hL;->hasLeadingDigits:Z

    .line 801199
    iput-object v0, p0, LX/4hL;->leadingDigits_:Ljava/lang/String;

    .line 801200
    :cond_17
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    .line 801201
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/4hL;->hasLeadingZeroPossible:Z

    .line 801202
    iput-boolean v0, p0, LX/4hL;->leadingZeroPossible_:Z

    .line 801203
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    .line 801204
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/4hL;->hasMobileNumberPortableRegion:Z

    .line 801205
    iput-boolean v0, p0, LX/4hL;->mobileNumberPortableRegion_:Z

    .line 801206
    return-void
.end method

.method public setCarrierSpecific(LX/4hP;)LX/4hL;
    .locals 1

    .prologue
    .line 801060
    if-nez p1, :cond_0

    .line 801061
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 801062
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hL;->hasCarrierSpecific:Z

    .line 801063
    iput-object p1, p0, LX/4hL;->carrierSpecific_:LX/4hP;

    .line 801064
    return-object p0
.end method

.method public setEmergency(LX/4hP;)LX/4hL;
    .locals 1

    .prologue
    .line 801055
    if-nez p1, :cond_0

    .line 801056
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 801057
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hL;->hasEmergency:Z

    .line 801058
    iput-object p1, p0, LX/4hL;->emergency_:LX/4hP;

    .line 801059
    return-object p0
.end method

.method public setFixedLine(LX/4hP;)LX/4hL;
    .locals 1

    .prologue
    .line 801050
    if-nez p1, :cond_0

    .line 801051
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 801052
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hL;->hasFixedLine:Z

    .line 801053
    iput-object p1, p0, LX/4hL;->fixedLine_:LX/4hP;

    .line 801054
    return-object p0
.end method

.method public setGeneralDesc(LX/4hP;)LX/4hL;
    .locals 1

    .prologue
    .line 801045
    if-nez p1, :cond_0

    .line 801046
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 801047
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hL;->hasGeneralDesc:Z

    .line 801048
    iput-object p1, p0, LX/4hL;->generalDesc_:LX/4hP;

    .line 801049
    return-object p0
.end method

.method public setInternationalPrefix(Ljava/lang/String;)LX/4hL;
    .locals 1

    .prologue
    .line 801042
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hL;->hasInternationalPrefix:Z

    .line 801043
    iput-object p1, p0, LX/4hL;->internationalPrefix_:Ljava/lang/String;

    .line 801044
    return-object p0
.end method

.method public setMobile(LX/4hP;)LX/4hL;
    .locals 1

    .prologue
    .line 801037
    if-nez p1, :cond_0

    .line 801038
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 801039
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hL;->hasMobile:Z

    .line 801040
    iput-object p1, p0, LX/4hL;->mobile_:LX/4hP;

    .line 801041
    return-object p0
.end method

.method public setNoInternationalDialling(LX/4hP;)LX/4hL;
    .locals 1

    .prologue
    .line 801242
    if-nez p1, :cond_0

    .line 801243
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 801244
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hL;->hasNoInternationalDialling:Z

    .line 801245
    iput-object p1, p0, LX/4hL;->noInternationalDialling_:LX/4hP;

    .line 801246
    return-object p0
.end method

.method public setPager(LX/4hP;)LX/4hL;
    .locals 1

    .prologue
    .line 801032
    if-nez p1, :cond_0

    .line 801033
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 801034
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hL;->hasPager:Z

    .line 801035
    iput-object p1, p0, LX/4hL;->pager_:LX/4hP;

    .line 801036
    return-object p0
.end method

.method public setPersonalNumber(LX/4hP;)LX/4hL;
    .locals 1

    .prologue
    .line 801027
    if-nez p1, :cond_0

    .line 801028
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 801029
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hL;->hasPersonalNumber:Z

    .line 801030
    iput-object p1, p0, LX/4hL;->personalNumber_:LX/4hP;

    .line 801031
    return-object p0
.end method

.method public setPremiumRate(LX/4hP;)LX/4hL;
    .locals 1

    .prologue
    .line 801022
    if-nez p1, :cond_0

    .line 801023
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 801024
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hL;->hasPremiumRate:Z

    .line 801025
    iput-object p1, p0, LX/4hL;->premiumRate_:LX/4hP;

    .line 801026
    return-object p0
.end method

.method public setSharedCost(LX/4hP;)LX/4hL;
    .locals 1

    .prologue
    .line 801017
    if-nez p1, :cond_0

    .line 801018
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 801019
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hL;->hasSharedCost:Z

    .line 801020
    iput-object p1, p0, LX/4hL;->sharedCost_:LX/4hP;

    .line 801021
    return-object p0
.end method

.method public setShortCode(LX/4hP;)LX/4hL;
    .locals 1

    .prologue
    .line 801012
    if-nez p1, :cond_0

    .line 801013
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 801014
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hL;->hasShortCode:Z

    .line 801015
    iput-object p1, p0, LX/4hL;->shortCode_:LX/4hP;

    .line 801016
    return-object p0
.end method

.method public setStandardRate(LX/4hP;)LX/4hL;
    .locals 1

    .prologue
    .line 801007
    if-nez p1, :cond_0

    .line 801008
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 801009
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hL;->hasStandardRate:Z

    .line 801010
    iput-object p1, p0, LX/4hL;->standardRate_:LX/4hP;

    .line 801011
    return-object p0
.end method

.method public setTollFree(LX/4hP;)LX/4hL;
    .locals 1

    .prologue
    .line 801002
    if-nez p1, :cond_0

    .line 801003
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 801004
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hL;->hasTollFree:Z

    .line 801005
    iput-object p1, p0, LX/4hL;->tollFree_:LX/4hP;

    .line 801006
    return-object p0
.end method

.method public setUan(LX/4hP;)LX/4hL;
    .locals 1

    .prologue
    .line 800903
    if-nez p1, :cond_0

    .line 800904
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 800905
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hL;->hasUan:Z

    .line 800906
    iput-object p1, p0, LX/4hL;->uan_:LX/4hP;

    .line 800907
    return-object p0
.end method

.method public setVoicemail(LX/4hP;)LX/4hL;
    .locals 1

    .prologue
    .line 800997
    if-nez p1, :cond_0

    .line 800998
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 800999
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hL;->hasVoicemail:Z

    .line 801000
    iput-object p1, p0, LX/4hL;->voicemail_:LX/4hP;

    .line 801001
    return-object p0
.end method

.method public setVoip(LX/4hP;)LX/4hL;
    .locals 1

    .prologue
    .line 800992
    if-nez p1, :cond_0

    .line 800993
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 800994
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hL;->hasVoip:Z

    .line 800995
    iput-object p1, p0, LX/4hL;->voip_:LX/4hP;

    .line 800996
    return-object p0
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 800908
    iget-boolean v0, p0, LX/4hL;->hasGeneralDesc:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800909
    iget-boolean v0, p0, LX/4hL;->hasGeneralDesc:Z

    if-eqz v0, :cond_0

    .line 800910
    iget-object v0, p0, LX/4hL;->generalDesc_:LX/4hP;

    invoke-virtual {v0, p1}, LX/4hP;->writeExternal(Ljava/io/ObjectOutput;)V

    .line 800911
    :cond_0
    iget-boolean v0, p0, LX/4hL;->hasFixedLine:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800912
    iget-boolean v0, p0, LX/4hL;->hasFixedLine:Z

    if-eqz v0, :cond_1

    .line 800913
    iget-object v0, p0, LX/4hL;->fixedLine_:LX/4hP;

    invoke-virtual {v0, p1}, LX/4hP;->writeExternal(Ljava/io/ObjectOutput;)V

    .line 800914
    :cond_1
    iget-boolean v0, p0, LX/4hL;->hasMobile:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800915
    iget-boolean v0, p0, LX/4hL;->hasMobile:Z

    if-eqz v0, :cond_2

    .line 800916
    iget-object v0, p0, LX/4hL;->mobile_:LX/4hP;

    invoke-virtual {v0, p1}, LX/4hP;->writeExternal(Ljava/io/ObjectOutput;)V

    .line 800917
    :cond_2
    iget-boolean v0, p0, LX/4hL;->hasTollFree:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800918
    iget-boolean v0, p0, LX/4hL;->hasTollFree:Z

    if-eqz v0, :cond_3

    .line 800919
    iget-object v0, p0, LX/4hL;->tollFree_:LX/4hP;

    invoke-virtual {v0, p1}, LX/4hP;->writeExternal(Ljava/io/ObjectOutput;)V

    .line 800920
    :cond_3
    iget-boolean v0, p0, LX/4hL;->hasPremiumRate:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800921
    iget-boolean v0, p0, LX/4hL;->hasPremiumRate:Z

    if-eqz v0, :cond_4

    .line 800922
    iget-object v0, p0, LX/4hL;->premiumRate_:LX/4hP;

    invoke-virtual {v0, p1}, LX/4hP;->writeExternal(Ljava/io/ObjectOutput;)V

    .line 800923
    :cond_4
    iget-boolean v0, p0, LX/4hL;->hasSharedCost:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800924
    iget-boolean v0, p0, LX/4hL;->hasSharedCost:Z

    if-eqz v0, :cond_5

    .line 800925
    iget-object v0, p0, LX/4hL;->sharedCost_:LX/4hP;

    invoke-virtual {v0, p1}, LX/4hP;->writeExternal(Ljava/io/ObjectOutput;)V

    .line 800926
    :cond_5
    iget-boolean v0, p0, LX/4hL;->hasPersonalNumber:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800927
    iget-boolean v0, p0, LX/4hL;->hasPersonalNumber:Z

    if-eqz v0, :cond_6

    .line 800928
    iget-object v0, p0, LX/4hL;->personalNumber_:LX/4hP;

    invoke-virtual {v0, p1}, LX/4hP;->writeExternal(Ljava/io/ObjectOutput;)V

    .line 800929
    :cond_6
    iget-boolean v0, p0, LX/4hL;->hasVoip:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800930
    iget-boolean v0, p0, LX/4hL;->hasVoip:Z

    if-eqz v0, :cond_7

    .line 800931
    iget-object v0, p0, LX/4hL;->voip_:LX/4hP;

    invoke-virtual {v0, p1}, LX/4hP;->writeExternal(Ljava/io/ObjectOutput;)V

    .line 800932
    :cond_7
    iget-boolean v0, p0, LX/4hL;->hasPager:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800933
    iget-boolean v0, p0, LX/4hL;->hasPager:Z

    if-eqz v0, :cond_8

    .line 800934
    iget-object v0, p0, LX/4hL;->pager_:LX/4hP;

    invoke-virtual {v0, p1}, LX/4hP;->writeExternal(Ljava/io/ObjectOutput;)V

    .line 800935
    :cond_8
    iget-boolean v0, p0, LX/4hL;->hasUan:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800936
    iget-boolean v0, p0, LX/4hL;->hasUan:Z

    if-eqz v0, :cond_9

    .line 800937
    iget-object v0, p0, LX/4hL;->uan_:LX/4hP;

    invoke-virtual {v0, p1}, LX/4hP;->writeExternal(Ljava/io/ObjectOutput;)V

    .line 800938
    :cond_9
    iget-boolean v0, p0, LX/4hL;->hasEmergency:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800939
    iget-boolean v0, p0, LX/4hL;->hasEmergency:Z

    if-eqz v0, :cond_a

    .line 800940
    iget-object v0, p0, LX/4hL;->emergency_:LX/4hP;

    invoke-virtual {v0, p1}, LX/4hP;->writeExternal(Ljava/io/ObjectOutput;)V

    .line 800941
    :cond_a
    iget-boolean v0, p0, LX/4hL;->hasVoicemail:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800942
    iget-boolean v0, p0, LX/4hL;->hasVoicemail:Z

    if-eqz v0, :cond_b

    .line 800943
    iget-object v0, p0, LX/4hL;->voicemail_:LX/4hP;

    invoke-virtual {v0, p1}, LX/4hP;->writeExternal(Ljava/io/ObjectOutput;)V

    .line 800944
    :cond_b
    iget-boolean v0, p0, LX/4hL;->hasShortCode:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800945
    iget-boolean v0, p0, LX/4hL;->hasShortCode:Z

    if-eqz v0, :cond_c

    .line 800946
    iget-object v0, p0, LX/4hL;->shortCode_:LX/4hP;

    invoke-virtual {v0, p1}, LX/4hP;->writeExternal(Ljava/io/ObjectOutput;)V

    .line 800947
    :cond_c
    iget-boolean v0, p0, LX/4hL;->hasStandardRate:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800948
    iget-boolean v0, p0, LX/4hL;->hasStandardRate:Z

    if-eqz v0, :cond_d

    .line 800949
    iget-object v0, p0, LX/4hL;->standardRate_:LX/4hP;

    invoke-virtual {v0, p1}, LX/4hP;->writeExternal(Ljava/io/ObjectOutput;)V

    .line 800950
    :cond_d
    iget-boolean v0, p0, LX/4hL;->hasCarrierSpecific:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800951
    iget-boolean v0, p0, LX/4hL;->hasCarrierSpecific:Z

    if-eqz v0, :cond_e

    .line 800952
    iget-object v0, p0, LX/4hL;->carrierSpecific_:LX/4hP;

    invoke-virtual {v0, p1}, LX/4hP;->writeExternal(Ljava/io/ObjectOutput;)V

    .line 800953
    :cond_e
    iget-boolean v0, p0, LX/4hL;->hasNoInternationalDialling:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800954
    iget-boolean v0, p0, LX/4hL;->hasNoInternationalDialling:Z

    if-eqz v0, :cond_f

    .line 800955
    iget-object v0, p0, LX/4hL;->noInternationalDialling_:LX/4hP;

    invoke-virtual {v0, p1}, LX/4hP;->writeExternal(Ljava/io/ObjectOutput;)V

    .line 800956
    :cond_f
    iget-object v0, p0, LX/4hL;->id_:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 800957
    iget v0, p0, LX/4hL;->countryCode_:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    .line 800958
    iget-object v0, p0, LX/4hL;->internationalPrefix_:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 800959
    iget-boolean v0, p0, LX/4hL;->hasPreferredInternationalPrefix:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800960
    iget-boolean v0, p0, LX/4hL;->hasPreferredInternationalPrefix:Z

    if-eqz v0, :cond_10

    .line 800961
    iget-object v0, p0, LX/4hL;->preferredInternationalPrefix_:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 800962
    :cond_10
    iget-boolean v0, p0, LX/4hL;->hasNationalPrefix:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800963
    iget-boolean v0, p0, LX/4hL;->hasNationalPrefix:Z

    if-eqz v0, :cond_11

    .line 800964
    iget-object v0, p0, LX/4hL;->nationalPrefix_:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 800965
    :cond_11
    iget-boolean v0, p0, LX/4hL;->hasPreferredExtnPrefix:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800966
    iget-boolean v0, p0, LX/4hL;->hasPreferredExtnPrefix:Z

    if-eqz v0, :cond_12

    .line 800967
    iget-object v0, p0, LX/4hL;->preferredExtnPrefix_:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 800968
    :cond_12
    iget-boolean v0, p0, LX/4hL;->hasNationalPrefixForParsing:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800969
    iget-boolean v0, p0, LX/4hL;->hasNationalPrefixForParsing:Z

    if-eqz v0, :cond_13

    .line 800970
    iget-object v0, p0, LX/4hL;->nationalPrefixForParsing_:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 800971
    :cond_13
    iget-boolean v0, p0, LX/4hL;->hasNationalPrefixTransformRule:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800972
    iget-boolean v0, p0, LX/4hL;->hasNationalPrefixTransformRule:Z

    if-eqz v0, :cond_14

    .line 800973
    iget-object v0, p0, LX/4hL;->nationalPrefixTransformRule_:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 800974
    :cond_14
    iget-boolean v0, p0, LX/4hL;->sameMobileAndFixedLinePattern_:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800975
    iget-object v0, p0, LX/4hL;->numberFormat_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    move v3, v0

    .line 800976
    invoke-interface {p1, v3}, Ljava/io/ObjectOutput;->writeInt(I)V

    move v2, v1

    .line 800977
    :goto_0
    if-ge v2, v3, :cond_15

    .line 800978
    iget-object v0, p0, LX/4hL;->numberFormat_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4hJ;

    invoke-virtual {v0, p1}, LX/4hJ;->writeExternal(Ljava/io/ObjectOutput;)V

    .line 800979
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 800980
    :cond_15
    invoke-virtual {p0}, LX/4hL;->intlNumberFormatSize()I

    move-result v2

    .line 800981
    invoke-interface {p1, v2}, Ljava/io/ObjectOutput;->writeInt(I)V

    .line 800982
    :goto_1
    if-ge v1, v2, :cond_16

    .line 800983
    iget-object v0, p0, LX/4hL;->intlNumberFormat_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4hJ;

    invoke-virtual {v0, p1}, LX/4hJ;->writeExternal(Ljava/io/ObjectOutput;)V

    .line 800984
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 800985
    :cond_16
    iget-boolean v0, p0, LX/4hL;->mainCountryForCode_:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800986
    iget-boolean v0, p0, LX/4hL;->hasLeadingDigits:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800987
    iget-boolean v0, p0, LX/4hL;->hasLeadingDigits:Z

    if-eqz v0, :cond_17

    .line 800988
    iget-object v0, p0, LX/4hL;->leadingDigits_:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 800989
    :cond_17
    iget-boolean v0, p0, LX/4hL;->leadingZeroPossible_:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800990
    iget-boolean v0, p0, LX/4hL;->mobileNumberPortableRegion_:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800991
    return-void
.end method
