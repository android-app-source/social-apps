.class public LX/4oL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/4oN;

.field public final c:LX/4oP;

.field public final d:Landroid/graphics/RectF;

.field private final e:Landroid/graphics/Paint;

.field public f:Landroid/graphics/Paint;

.field public g:Landroid/graphics/ColorFilter;

.field private h:Landroid/graphics/Paint;

.field private i:I

.field public j:Landroid/graphics/Matrix;

.field public k:Landroid/graphics/Bitmap;

.field private l:Landroid/graphics/Path;

.field public m:Landroid/graphics/RectF;

.field public n:Landroid/graphics/Canvas;

.field public o:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public p:Landroid/graphics/BitmapShader;

.field public q:Landroid/graphics/Matrix;

.field public r:Landroid/graphics/Paint;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 809571
    const-class v0, LX/4oL;

    sput-object v0, LX/4oL;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/4oN;LX/4oP;)V
    .locals 6

    .prologue
    .line 809556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 809557
    const/16 v0, 0xff

    iput v0, p0, LX/4oL;->i:I

    .line 809558
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/4oL;->j:Landroid/graphics/Matrix;

    .line 809559
    iput-object p1, p0, LX/4oL;->b:LX/4oN;

    .line 809560
    iput-object p2, p0, LX/4oL;->c:LX/4oP;

    .line 809561
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, LX/4oP;->c:I

    int-to-float v1, v1

    iget v2, p2, LX/4oP;->d:I

    int-to-float v2, v2

    iget v3, p2, LX/4oP;->a:I

    iget v4, p2, LX/4oP;->e:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    iget v4, p2, LX/4oP;->b:I

    iget v5, p2, LX/4oP;->f:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, LX/4oL;->d:Landroid/graphics/RectF;

    .line 809562
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    .line 809563
    invoke-static {p0}, LX/4oL;->e(LX/4oL;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 809564
    iget-object v1, p0, LX/4oL;->b:LX/4oN;

    iget v1, v1, LX/4oN;->g:I

    .line 809565
    :goto_0
    move v1, v1

    .line 809566
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 809567
    move-object v0, v0

    .line 809568
    iput-object v0, p0, LX/4oL;->e:Landroid/graphics/Paint;

    .line 809569
    invoke-direct {p0}, LX/4oL;->i()Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, LX/4oL;->f:Landroid/graphics/Paint;

    .line 809570
    return-void

    :cond_0
    const/high16 v1, -0x1000000

    goto :goto_0
.end method

.method public static a(Landroid/graphics/Bitmap;FFLX/4oO;)F
    .locals 5

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 809550
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float v1, p1, v1

    .line 809551
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v2, p2, v2

    .line 809552
    sget-object v3, LX/4oK;->a:[I

    invoke-virtual {p3}, LX/4oO;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 809553
    :goto_0
    return v0

    .line 809554
    :pswitch_0
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_0

    .line 809555
    :pswitch_1
    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(LX/4oL;Landroid/graphics/Canvas;LX/4oQ;)V
    .locals 2

    .prologue
    .line 809542
    invoke-virtual {p2, p1}, LX/4oQ;->a(Landroid/graphics/Canvas;)V

    .line 809543
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/4oL;->e(Landroid/graphics/Bitmap;)Landroid/graphics/Path;

    move-result-object v0

    .line 809544
    iget-object v1, p0, LX/4oL;->h:Landroid/graphics/Paint;

    if-eqz v1, :cond_0

    .line 809545
    sget-object v1, Landroid/graphics/Path$FillType;->WINDING:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 809546
    iget-object v1, p0, LX/4oL;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 809547
    :cond_0
    sget-object v1, Landroid/graphics/Path$FillType;->INVERSE_WINDING:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 809548
    iget-object v1, p0, LX/4oL;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 809549
    return-void
.end method

.method public static d(LX/4oL;)Z
    .locals 2

    .prologue
    .line 809422
    iget-object v0, p0, LX/4oL;->b:LX/4oN;

    iget-boolean v0, v0, LX/4oN;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/4oL;->b:LX/4oN;

    iget-boolean v0, v0, LX/4oN;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/4oL;->b:LX/4oN;

    iget-boolean v0, v0, LX/4oN;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/4oL;->b:LX/4oN;

    iget-boolean v0, v0, LX/4oN;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/4oL;->b:LX/4oN;

    iget-boolean v0, v0, LX/4oN;->e:Z

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, LX/4oL;->b:LX/4oN;

    iget v0, v0, LX/4oN;->f:F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-lez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(Landroid/graphics/Bitmap;)Landroid/graphics/Path;
    .locals 11
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 809499
    iget-object v0, p0, LX/4oL;->l:Landroid/graphics/Path;

    if-nez v0, :cond_3

    .line 809500
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/4oL;->l:Landroid/graphics/Path;

    .line 809501
    :goto_0
    iget-object v0, p0, LX/4oL;->l:Landroid/graphics/Path;

    .line 809502
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, 0x0

    .line 809503
    iget-object v1, p0, LX/4oL;->m:Landroid/graphics/RectF;

    if-nez v1, :cond_9

    .line 809504
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, LX/4oL;->d:Landroid/graphics/RectF;

    invoke-direct {v1, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v1, p0, LX/4oL;->m:Landroid/graphics/RectF;

    .line 809505
    :goto_1
    iget-object v1, p0, LX/4oL;->m:Landroid/graphics/RectF;

    .line 809506
    if-eqz p1, :cond_0

    iget-object v2, p0, LX/4oL;->c:LX/4oP;

    iget-object v2, v2, LX/4oP;->g:LX/4oO;

    sget-object v3, LX/4oO;->CENTER:LX/4oO;

    if-eq v2, v3, :cond_a

    .line 809507
    :cond_0
    :goto_2
    move-object v3, v1

    .line 809508
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 809509
    invoke-static {p0}, LX/4oL;->d(LX/4oL;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 809510
    :cond_1
    :goto_3
    move v1, v1

    .line 809511
    if-nez v1, :cond_4

    .line 809512
    sget-object v1, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v3, v1}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 809513
    :cond_2
    :goto_4
    return-object v0

    .line 809514
    :cond_3
    iget-object v0, p0, LX/4oL;->l:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    goto :goto_0

    .line 809515
    :cond_4
    iget-object v1, p0, LX/4oL;->b:LX/4oN;

    iget-boolean v1, v1, LX/4oN;->a:Z

    if-eqz v1, :cond_5

    .line 809516
    sget-object v1, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v3, v1}, Landroid/graphics/Path;->addOval(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    goto :goto_4

    .line 809517
    :cond_5
    iget v1, v3, Landroid/graphics/RectF;->left:F

    .line 809518
    iget v6, v3, Landroid/graphics/RectF;->right:F

    .line 809519
    iget v2, v3, Landroid/graphics/RectF;->top:F

    .line 809520
    iget v9, v3, Landroid/graphics/RectF;->bottom:F

    .line 809521
    iget-object v4, p0, LX/4oL;->b:LX/4oN;

    iget v4, v4, LX/4oN;->f:F

    iget-object v5, p0, LX/4oL;->b:LX/4oN;

    iget v5, v5, LX/4oN;->f:F

    sget-object v7, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v3, v4, v5, v7}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    .line 809522
    iget-object v3, p0, LX/4oL;->b:LX/4oN;

    iget v3, v3, LX/4oN;->f:F

    const/high16 v4, 0x40000000    # 2.0f

    mul-float v10, v3, v4

    .line 809523
    iget-object v3, p0, LX/4oL;->b:LX/4oN;

    iget-boolean v3, v3, LX/4oN;->b:Z

    if-nez v3, :cond_6

    .line 809524
    add-float v3, v1, v10

    add-float v4, v2, v10

    sget-object v5, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 809525
    :cond_6
    iget-object v3, p0, LX/4oL;->b:LX/4oN;

    iget-boolean v3, v3, LX/4oN;->c:Z

    if-nez v3, :cond_7

    .line 809526
    sub-float v4, v6, v10

    add-float v7, v2, v10

    sget-object v8, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    move-object v3, v0

    move v5, v2

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 809527
    :cond_7
    iget-object v2, p0, LX/4oL;->b:LX/4oN;

    iget-boolean v2, v2, LX/4oN;->d:Z

    if-nez v2, :cond_8

    .line 809528
    sub-float v2, v9, v10

    add-float v3, v1, v10

    sget-object v5, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    move v4, v9

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 809529
    :cond_8
    iget-object v1, p0, LX/4oL;->b:LX/4oN;

    iget-boolean v1, v1, LX/4oN;->e:Z

    if-nez v1, :cond_2

    .line 809530
    sub-float v1, v6, v10

    sub-float v2, v9, v10

    sget-object v5, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    move v3, v6

    move v4, v9

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    goto :goto_4

    .line 809531
    :cond_9
    iget-object v1, p0, LX/4oL;->m:Landroid/graphics/RectF;

    iget-object v2, p0, LX/4oL;->d:Landroid/graphics/RectF;

    invoke-virtual {v1, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    goto/16 :goto_1

    .line 809532
    :cond_a
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v3

    iget-object v4, p0, LX/4oL;->c:LX/4oP;

    iget-object v4, v4, LX/4oP;->g:LX/4oO;

    invoke-static {p1, v2, v3, v4}, LX/4oL;->a(Landroid/graphics/Bitmap;FFLX/4oO;)F

    move-result v2

    .line 809533
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_b

    .line 809534
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v2

    .line 809535
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v4

    sub-float v3, v4, v3

    div-float/2addr v3, v6

    invoke-virtual {v1, v3, v5}, Landroid/graphics/RectF;->inset(FF)V

    .line 809536
    :cond_b
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_0

    .line 809537
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    .line 809538
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v3

    sub-float v2, v3, v2

    div-float/2addr v2, v6

    invoke-virtual {v1, v5, v2}, Landroid/graphics/RectF;->inset(FF)V

    goto/16 :goto_2

    .line 809539
    :cond_c
    if-eqz p1, :cond_d

    iget-object v4, p0, LX/4oL;->c:LX/4oP;

    iget-object v4, v4, LX/4oP;->g:LX/4oO;

    sget-object v5, LX/4oO;->CENTER:LX/4oO;

    if-eq v4, v5, :cond_e

    :cond_d
    move v1, v2

    .line 809540
    goto/16 :goto_3

    .line 809541
    :cond_e
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, LX/4oL;->d:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    cmpg-float v4, v4, v5

    if-ltz v4, :cond_f

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, LX/4oL;->d:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_1

    :cond_f
    move v1, v2

    goto/16 :goto_3
.end method

.method public static e(LX/4oL;)Z
    .locals 1

    .prologue
    .line 809498
    iget-object v0, p0, LX/4oL;->b:LX/4oN;

    iget v0, v0, LX/4oN;->g:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()Landroid/graphics/Paint;
    .locals 2

    .prologue
    .line 809493
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    .line 809494
    iget-object v1, p0, LX/4oL;->b:LX/4oN;

    iget v1, v1, LX/4oN;->i:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 809495
    iget-object v1, p0, LX/4oL;->b:LX/4oN;

    iget v1, v1, LX/4oN;->h:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 809496
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 809497
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 809482
    iget-object v0, p0, LX/4oL;->k:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 809483
    iget-object v0, p0, LX/4oL;->k:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 809484
    iput-object v1, p0, LX/4oL;->k:Landroid/graphics/Bitmap;

    .line 809485
    :cond_0
    iput-object v1, p0, LX/4oL;->l:Landroid/graphics/Path;

    .line 809486
    iput-object v1, p0, LX/4oL;->m:Landroid/graphics/RectF;

    .line 809487
    iput-object v1, p0, LX/4oL;->n:Landroid/graphics/Canvas;

    .line 809488
    iput-object v1, p0, LX/4oL;->o:Ljava/lang/ref/WeakReference;

    .line 809489
    iput-object v1, p0, LX/4oL;->p:Landroid/graphics/BitmapShader;

    .line 809490
    iput-object v1, p0, LX/4oL;->q:Landroid/graphics/Matrix;

    .line 809491
    iput-object v1, p0, LX/4oL;->r:Landroid/graphics/Paint;

    .line 809492
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 809478
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/4oL;->h:Landroid/graphics/Paint;

    .line 809479
    iget-object v0, p0, LX/4oL;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 809480
    iget-object v0, p0, LX/4oL;->h:Landroid/graphics/Paint;

    iget v1, p0, LX/4oL;->i:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 809481
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V
    .locals 9

    .prologue
    .line 809433
    invoke-direct {p0, p2}, LX/4oL;->e(Landroid/graphics/Bitmap;)Landroid/graphics/Path;

    move-result-object v0

    .line 809434
    if-eqz p2, :cond_0

    .line 809435
    iget-object v1, p0, LX/4oL;->r:Landroid/graphics/Paint;

    if-eqz v1, :cond_4

    .line 809436
    iget-object v1, p0, LX/4oL;->o:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/4oL;->o:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p2, :cond_3

    .line 809437
    iget-object v1, p0, LX/4oL;->r:Landroid/graphics/Paint;

    .line 809438
    :goto_0
    move-object v1, v1

    .line 809439
    sget-object v2, Landroid/graphics/Path$FillType;->WINDING:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v2}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 809440
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 809441
    :cond_0
    iget-object v1, p0, LX/4oL;->h:Landroid/graphics/Paint;

    if-eqz v1, :cond_1

    .line 809442
    iget-object v1, p0, LX/4oL;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 809443
    :cond_1
    iget-object v0, p0, LX/4oL;->b:LX/4oN;

    iget-boolean v0, v0, LX/4oN;->a:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, LX/4oL;->b:LX/4oN;

    iget v0, v0, LX/4oN;->i:I

    if-eqz v0, :cond_9

    iget-object v0, p0, LX/4oL;->b:LX/4oN;

    iget v0, v0, LX/4oN;->h:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_9

    iget-object v0, p0, LX/4oL;->f:Landroid/graphics/Paint;

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 809444
    if-eqz v0, :cond_2

    .line 809445
    iget-object v0, p0, LX/4oL;->m:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    iget-object v1, p0, LX/4oL;->m:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    iget-object v2, p0, LX/4oL;->m:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    iget-object v3, p0, LX/4oL;->b:LX/4oN;

    iget v3, v3, LX/4oN;->h:F

    sub-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    iget-object v3, p0, LX/4oL;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 809446
    :cond_2
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/4oL;->o:Ljava/lang/ref/WeakReference;

    .line 809447
    return-void

    .line 809448
    :cond_3
    iget-object v1, p0, LX/4oL;->r:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->reset()V

    .line 809449
    :goto_2
    iget-object v1, p0, LX/4oL;->o:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_5

    iget-object v1, p0, LX/4oL;->o:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p2, :cond_5

    iget-object v1, p0, LX/4oL;->p:Landroid/graphics/BitmapShader;

    if-eqz v1, :cond_5

    .line 809450
    iget-object v1, p0, LX/4oL;->p:Landroid/graphics/BitmapShader;

    .line 809451
    :goto_3
    move-object v2, v1

    .line 809452
    const/high16 v8, 0x40000000    # 2.0f

    .line 809453
    iget-object v1, p0, LX/4oL;->o:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_6

    iget-object v1, p0, LX/4oL;->o:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p2, :cond_6

    iget-object v1, p0, LX/4oL;->q:Landroid/graphics/Matrix;

    if-eqz v1, :cond_6

    .line 809454
    iget-object v1, p0, LX/4oL;->q:Landroid/graphics/Matrix;

    .line 809455
    :goto_4
    move-object v1, v1

    .line 809456
    invoke-virtual {v2, v1}, Landroid/graphics/BitmapShader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 809457
    iget-object v1, p0, LX/4oL;->r:Landroid/graphics/Paint;

    .line 809458
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 809459
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 809460
    iget-object v2, p0, LX/4oL;->g:Landroid/graphics/ColorFilter;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto/16 :goto_0

    .line 809461
    :cond_4
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, LX/4oL;->r:Landroid/graphics/Paint;

    goto :goto_2

    .line 809462
    :cond_5
    new-instance v1, Landroid/graphics/BitmapShader;

    sget-object v2, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v3, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {v1, p2, v2, v3}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    iput-object v1, p0, LX/4oL;->p:Landroid/graphics/BitmapShader;

    .line 809463
    iget-object v1, p0, LX/4oL;->p:Landroid/graphics/BitmapShader;

    goto :goto_3

    .line 809464
    :cond_6
    iget-object v1, p0, LX/4oL;->q:Landroid/graphics/Matrix;

    if-eqz v1, :cond_7

    .line 809465
    iget-object v1, p0, LX/4oL;->q:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 809466
    :goto_5
    iget-object v1, p0, LX/4oL;->q:Landroid/graphics/Matrix;

    .line 809467
    iget-object v3, p0, LX/4oL;->d:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    .line 809468
    iget-object v4, p0, LX/4oL;->d:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    .line 809469
    iget-object v5, p0, LX/4oL;->c:LX/4oP;

    iget-object v5, v5, LX/4oP;->g:LX/4oO;

    invoke-static {p2, v3, v4, v5}, LX/4oL;->a(Landroid/graphics/Bitmap;FFLX/4oO;)F

    move-result v5

    .line 809470
    sget-object v6, LX/4oK;->a:[I

    iget-object v7, p0, LX/4oL;->c:LX/4oP;

    iget-object v7, v7, LX/4oP;->g:LX/4oO;

    invoke-virtual {v7}, LX/4oO;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    goto :goto_4

    .line 809471
    :pswitch_0
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v5

    sub-float/2addr v3, v6

    div-float/2addr v3, v8

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v5

    sub-float/2addr v4, v6

    div-float/2addr v4, v8

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 809472
    invoke-virtual {v1, v5, v5}, Landroid/graphics/Matrix;->postScale(FF)Z

    goto :goto_4

    .line 809473
    :cond_7
    iget-object v1, p0, LX/4oL;->c:LX/4oP;

    iget-object v1, v1, LX/4oP;->g:LX/4oO;

    sget-object v3, LX/4oO;->MATRIX:LX/4oO;

    if-ne v1, v3, :cond_8

    iget-object v1, p0, LX/4oL;->j:Landroid/graphics/Matrix;

    if-eqz v1, :cond_8

    .line 809474
    iget-object v1, p0, LX/4oL;->j:Landroid/graphics/Matrix;

    iput-object v1, p0, LX/4oL;->q:Landroid/graphics/Matrix;

    goto :goto_5

    .line 809475
    :cond_8
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, LX/4oL;->q:Landroid/graphics/Matrix;

    goto :goto_5

    .line 809476
    :pswitch_1
    invoke-virtual {v1, v5, v5}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 809477
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v5

    sub-float/2addr v3, v6

    div-float/2addr v3, v8

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    div-float/2addr v4, v8

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto/16 :goto_4

    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 809430
    iput-object p1, p0, LX/4oL;->g:Landroid/graphics/ColorFilter;

    .line 809431
    const/4 v0, 0x0

    iput-object v0, p0, LX/4oL;->r:Landroid/graphics/Paint;

    .line 809432
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 809426
    iput p1, p0, LX/4oL;->i:I

    .line 809427
    iget-object v0, p0, LX/4oL;->h:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    .line 809428
    iget-object v0, p0, LX/4oL;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 809429
    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 809423
    iget-object v0, p0, LX/4oL;->b:LX/4oN;

    iput p1, v0, LX/4oN;->i:I

    .line 809424
    invoke-direct {p0}, LX/4oL;->i()Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, LX/4oL;->f:Landroid/graphics/Paint;

    .line 809425
    return-void
.end method
