.class public LX/3v1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Landroid/content/DialogInterface$OnKeyListener;
.implements LX/3uE;


# instance fields
.field public a:LX/3uz;

.field public b:LX/3v0;

.field public c:Landroid/app/AlertDialog;

.field private d:LX/3uE;


# direct methods
.method public constructor <init>(LX/3v0;)V
    .locals 0

    .prologue
    .line 650462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 650463
    iput-object p1, p0, LX/3v1;->b:LX/3v0;

    .line 650464
    return-void
.end method


# virtual methods
.method public final a(LX/3v0;Z)V
    .locals 1

    .prologue
    .line 650456
    if-nez p2, :cond_0

    iget-object v0, p0, LX/3v1;->b:LX/3v0;

    if-ne p1, v0, :cond_1

    .line 650457
    :cond_0
    iget-object v0, p0, LX/3v1;->c:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 650458
    iget-object v0, p0, LX/3v1;->c:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 650459
    :cond_1
    iget-object v0, p0, LX/3v1;->d:LX/3uE;

    if-eqz v0, :cond_2

    .line 650460
    iget-object v0, p0, LX/3v1;->d:LX/3uE;

    invoke-interface {v0, p1, p2}, LX/3uE;->a(LX/3v0;Z)V

    .line 650461
    :cond_2
    return-void
.end method

.method public final a_(LX/3v0;)Z
    .locals 1

    .prologue
    .line 650465
    iget-object v0, p0, LX/3v1;->d:LX/3uE;

    if-eqz v0, :cond_0

    .line 650466
    iget-object v0, p0, LX/3v1;->d:LX/3uE;

    invoke-interface {v0, p1}, LX/3uE;->a_(LX/3v0;)Z

    move-result v0

    .line 650467
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 650454
    iget-object v1, p0, LX/3v1;->b:LX/3v0;

    iget-object v0, p0, LX/3v1;->a:LX/3uz;

    invoke-virtual {v0}, LX/3uz;->a()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p2}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/3v0;->a(Landroid/view/MenuItem;I)Z

    .line 650455
    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 3

    .prologue
    .line 650452
    iget-object v0, p0, LX/3v1;->a:LX/3uz;

    iget-object v1, p0, LX/3v1;->b:LX/3v0;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/3uz;->a(LX/3v0;Z)V

    .line 650453
    return-void
.end method

.method public final onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 650432
    const/16 v1, 0x52

    if-eq p2, v1, :cond_0

    const/4 v1, 0x4

    if-ne p2, v1, :cond_2

    .line 650433
    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 650434
    iget-object v1, p0, LX/3v1;->c:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 650435
    if-eqz v1, :cond_2

    .line 650436
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    .line 650437
    if-eqz v1, :cond_2

    .line 650438
    invoke-virtual {v1}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    .line 650439
    if-eqz v1, :cond_2

    .line 650440
    invoke-virtual {v1, p3, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    .line 650441
    :goto_0
    return v0

    .line 650442
    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 650443
    iget-object v1, p0, LX/3v1;->c:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 650444
    if-eqz v1, :cond_2

    .line 650445
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    .line 650446
    if-eqz v1, :cond_2

    .line 650447
    invoke-virtual {v1}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    .line 650448
    if-eqz v1, :cond_2

    invoke-virtual {v1, p3}, Landroid/view/KeyEvent$DispatcherState;->isTracking(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 650449
    iget-object v1, p0, LX/3v1;->b:LX/3v0;

    invoke-virtual {v1, v0}, LX/3v0;->a(Z)V

    .line 650450
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    .line 650451
    :cond_2
    iget-object v0, p0, LX/3v1;->b:LX/3v0;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, p3, v1}, LX/3v0;->performShortcut(ILandroid/view/KeyEvent;I)Z

    move-result v0

    goto :goto_0
.end method
