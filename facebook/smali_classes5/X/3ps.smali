.class public final LX/3ps;
.super LX/3pr;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 641826
    invoke-direct {p0}, LX/3pr;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2HB;)Landroid/app/Notification;
    .locals 29

    .prologue
    .line 641822
    new-instance v1, LX/3q0;

    move-object/from16 v0, p1

    iget-object v2, v0, LX/2HB;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, LX/2HB;->B:Landroid/app/Notification;

    move-object/from16 v0, p1

    iget-object v4, v0, LX/2HB;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v5, v0, LX/2HB;->c:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v6, v0, LX/2HB;->h:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v7, v0, LX/2HB;->f:Landroid/widget/RemoteViews;

    move-object/from16 v0, p1

    iget v8, v0, LX/2HB;->i:I

    move-object/from16 v0, p1

    iget-object v9, v0, LX/2HB;->d:Landroid/app/PendingIntent;

    move-object/from16 v0, p1

    iget-object v10, v0, LX/2HB;->e:Landroid/app/PendingIntent;

    move-object/from16 v0, p1

    iget-object v11, v0, LX/2HB;->g:Landroid/graphics/Bitmap;

    move-object/from16 v0, p1

    iget v12, v0, LX/2HB;->o:I

    move-object/from16 v0, p1

    iget v13, v0, LX/2HB;->p:I

    move-object/from16 v0, p1

    iget-boolean v14, v0, LX/2HB;->q:Z

    move-object/from16 v0, p1

    iget-boolean v15, v0, LX/2HB;->k:Z

    move-object/from16 v0, p1

    iget-boolean v0, v0, LX/2HB;->l:Z

    move/from16 v16, v0

    move-object/from16 v0, p1

    iget v0, v0, LX/2HB;->j:I

    move/from16 v17, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/2HB;->n:Ljava/lang/CharSequence;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, LX/2HB;->v:Z

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/2HB;->w:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/2HB;->C:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/2HB;->x:Landroid/os/Bundle;

    move-object/from16 v22, v0

    move-object/from16 v0, p1

    iget v0, v0, LX/2HB;->y:I

    move/from16 v23, v0

    move-object/from16 v0, p1

    iget v0, v0, LX/2HB;->z:I

    move/from16 v24, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/2HB;->A:Landroid/app/Notification;

    move-object/from16 v25, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/2HB;->r:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, LX/2HB;->s:Z

    move/from16 v27, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/2HB;->t:Ljava/lang/String;

    move-object/from16 v28, v0

    invoke-direct/range {v1 .. v28}, LX/3q0;-><init>(Landroid/content/Context;Landroid/app/Notification;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/graphics/Bitmap;IIZZZILjava/lang/CharSequence;ZLjava/lang/String;Ljava/util/ArrayList;Landroid/os/Bundle;IILandroid/app/Notification;Ljava/lang/String;ZLjava/lang/String;)V

    .line 641823
    move-object/from16 v0, p1

    iget-object v2, v0, LX/2HB;->u:Ljava/util/ArrayList;

    invoke-static {v1, v2}, LX/3px;->b(LX/3pT;Ljava/util/ArrayList;)V

    .line 641824
    move-object/from16 v0, p1

    iget-object v2, v0, LX/2HB;->m:LX/3pc;

    invoke-static {v1, v2}, LX/3px;->b(LX/3pU;LX/3pc;)V

    .line 641825
    invoke-virtual {v1}, LX/3q0;->b()Landroid/app/Notification;

    move-result-object v1

    return-object v1
.end method

.method public final a(LX/3pi;)Landroid/os/Bundle;
    .locals 8

    .prologue
    .line 641797
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 641798
    if-nez p1, :cond_0

    .line 641799
    :goto_0
    move-object v0, v1

    .line 641800
    return-object v0

    .line 641801
    :cond_0
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 641802
    invoke-virtual {p1}, LX/3pi;->d()[Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {p1}, LX/3pi;->d()[Ljava/lang/String;

    move-result-object v4

    array-length v4, v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_1

    .line 641803
    invoke-virtual {p1}, LX/3pi;->d()[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v2

    .line 641804
    :cond_1
    invoke-virtual {p1}, LX/3pi;->a()[Ljava/lang/String;

    move-result-object v4

    array-length v4, v4

    new-array v4, v4, [Landroid/os/Parcelable;

    .line 641805
    :goto_1
    array-length v5, v4

    if-ge v2, v5, :cond_2

    .line 641806
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 641807
    const-string v6, "text"

    invoke-virtual {p1}, LX/3pi;->a()[Ljava/lang/String;

    move-result-object v7

    aget-object v7, v7, v2

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 641808
    const-string v6, "author"

    invoke-virtual {v5, v6, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 641809
    aput-object v5, v4, v2

    .line 641810
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 641811
    :cond_2
    const-string v1, "messages"

    invoke-virtual {v3, v1, v4}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 641812
    invoke-virtual {p1}, LX/3pi;->f()LX/3qK;

    move-result-object v1

    .line 641813
    if-eqz v1, :cond_3

    .line 641814
    const-string v2, "remote_input"

    .line 641815
    new-instance v4, Landroid/app/RemoteInput$Builder;

    invoke-virtual {v1}, LX/3qK;->a()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/app/RemoteInput$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, LX/3qK;->b()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/RemoteInput$Builder;->setLabel(Ljava/lang/CharSequence;)Landroid/app/RemoteInput$Builder;

    move-result-object v4

    invoke-virtual {v1}, LX/3qK;->c()[Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/RemoteInput$Builder;->setChoices([Ljava/lang/CharSequence;)Landroid/app/RemoteInput$Builder;

    move-result-object v4

    invoke-virtual {v1}, LX/3qK;->d()Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/app/RemoteInput$Builder;->setAllowFreeFormInput(Z)Landroid/app/RemoteInput$Builder;

    move-result-object v4

    invoke-virtual {v1}, LX/3qK;->e()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/RemoteInput$Builder;->addExtras(Landroid/os/Bundle;)Landroid/app/RemoteInput$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/RemoteInput$Builder;->build()Landroid/app/RemoteInput;

    move-result-object v4

    move-object v1, v4

    .line 641816
    invoke-virtual {v3, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 641817
    :cond_3
    const-string v1, "on_reply"

    invoke-virtual {p1}, LX/3pi;->b()Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 641818
    const-string v1, "on_read"

    invoke-virtual {p1}, LX/3pi;->c()Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 641819
    const-string v1, "participants"

    invoke-virtual {p1}, LX/3pi;->d()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 641820
    const-string v1, "timestamp"

    invoke-virtual {p1}, LX/3pi;->e()J

    move-result-wide v5

    invoke-virtual {v3, v1, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    move-object v1, v3

    .line 641821
    goto/16 :goto_0
.end method
