.class public LX/3SR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/10M;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 582388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 582389
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 582390
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 582391
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "access_token"

    iget-object v0, p0, LX/3SR;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 582392
    iget-object p0, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v0, p0

    .line 582393
    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 582394
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "new_app_id"

    const-string v3, "1993267864233146"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 582395
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 582396
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v2, "fetch_logged_out_api_access_token"

    .line 582397
    iput-object v2, v0, LX/14O;->b:Ljava/lang/String;

    .line 582398
    move-object v0, v0

    .line 582399
    const-string v2, "GET"

    .line 582400
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 582401
    move-object v0, v0

    .line 582402
    const-string v2, "method/auth.getSessionForApp"

    .line 582403
    iput-object v2, v0, LX/14O;->d:Ljava/lang/String;

    .line 582404
    move-object v0, v0

    .line 582405
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 582406
    move-object v0, v0

    .line 582407
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 582408
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 582409
    move-object v0, v0

    .line 582410
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 582411
    const/4 v11, 0x0

    .line 582412
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 582413
    const-string v1, "uid"

    invoke-virtual {v0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "access_token"

    invoke-virtual {v0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 582414
    :cond_0
    :goto_0
    return-object v11

    .line 582415
    :cond_1
    const-string v1, "uid"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v1

    .line 582416
    const-string v2, "access_token"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v9

    .line 582417
    iget-object v0, p0, LX/3SR;->a:LX/10M;

    invoke-virtual {v0, v1}, LX/10M;->b(Ljava/lang/String;)Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    move-result-object v10

    .line 582418
    if-eqz v10, :cond_0

    .line 582419
    new-instance v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v1, v10, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    iget v2, v10, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mTime:I

    iget-object v3, v10, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mName:Ljava/lang/String;

    iget-object v4, v10, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mFullName:Ljava/lang/String;

    iget-object v5, v10, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUsername:Ljava/lang/String;

    iget-object v6, v10, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mPicUrl:Ljava/lang/String;

    iget-object v7, v10, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mNonce:Ljava/lang/String;

    iget-object v8, v10, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mIsPinSet:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    invoke-direct/range {v0 .. v9}, Lcom/facebook/auth/credentials/DBLFacebookCredentials;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 582420
    const-string v1, "password_account"

    iget-object v2, v10, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mNonce:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 582421
    iget-object v1, p0, LX/3SR;->a:LX/10M;

    invoke-virtual {v1, v0}, LX/10M;->b(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V

    goto :goto_0

    .line 582422
    :cond_2
    iget-object v1, p0, LX/3SR;->a:LX/10M;

    invoke-virtual {v1, v0}, LX/10M;->a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V

    goto :goto_0
.end method
