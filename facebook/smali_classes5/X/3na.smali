.class public final LX/3na;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/215;


# direct methods
.method public constructor <init>(LX/215;)V
    .locals 0

    .prologue
    .line 638986
    iput-object p1, p0, LX/3na;->a:LX/215;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 4

    .prologue
    .line 638987
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 638988
    iget-object v1, p0, LX/3na;->a:LX/215;

    iget v1, v1, LX/215;->a:F

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_0

    .line 638989
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {p1, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 638990
    :cond_0
    iget-object v1, p0, LX/3na;->a:LX/215;

    invoke-static {v1}, LX/215;->c(LX/215;)LX/20T;

    move-result-object v1

    .line 638991
    if-eqz v1, :cond_1

    .line 638992
    invoke-interface {v1, v0}, LX/20T;->a(F)V

    .line 638993
    :cond_1
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 1

    .prologue
    .line 638994
    iget-object v0, p0, LX/3na;->a:LX/215;

    invoke-static {v0}, LX/215;->c(LX/215;)LX/20T;

    move-result-object v0

    .line 638995
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/20T;->isPressed()Z

    .line 638996
    :cond_0
    return-void
.end method

.method public final c(LX/0wd;)V
    .locals 1

    .prologue
    .line 638997
    iget-object v0, p0, LX/3na;->a:LX/215;

    invoke-static {v0}, LX/215;->c(LX/215;)LX/20T;

    move-result-object v0

    .line 638998
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/20T;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3na;->a:LX/215;

    iget-object v0, v0, LX/215;->k:LX/Ams;

    if-eqz v0, :cond_0

    .line 638999
    iget-object v0, p0, LX/3na;->a:LX/215;

    iget-object v0, v0, LX/215;->k:LX/Ams;

    invoke-virtual {v0}, LX/Ams;->a()V

    .line 639000
    :cond_0
    return-void
.end method
