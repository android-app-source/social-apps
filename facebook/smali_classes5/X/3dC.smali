.class public final LX/3dC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 616135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;)I
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 616148
    if-nez p1, :cond_0

    .line 616149
    :goto_0
    return v2

    .line 616150
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;->a()LX/0Px;

    move-result-object v3

    .line 616151
    if-eqz v3, :cond_2

    .line 616152
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 616153
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 616154
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;

    const/4 v6, 0x0

    .line 616155
    if-nez v0, :cond_3

    .line 616156
    :goto_2
    move v0, v6

    .line 616157
    aput v0, v4, v1

    .line 616158
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 616159
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 616160
    :goto_3
    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 616161
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 616162
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 616163
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 616164
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v7

    const/4 v8, 0x0

    .line 616165
    if-nez v7, :cond_4

    .line 616166
    :goto_4
    move v7, v8

    .line 616167
    const/4 v8, 0x2

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 616168
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 616169
    const/4 v7, 0x1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->j()I

    move-result v8

    invoke-virtual {p0, v7, v8, v6}, LX/186;->a(III)V

    .line 616170
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 616171
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2

    .line 616172
    :cond_4
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 616173
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->j()I

    move-result p1

    invoke-virtual {p0, v8, p1, v8}, LX/186;->a(III)V

    .line 616174
    invoke-virtual {p0}, LX/186;->d()I

    move-result v8

    .line 616175
    invoke-virtual {p0, v8}, LX/186;->d(I)V

    goto :goto_4
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;)Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 616136
    if-nez p0, :cond_1

    .line 616137
    :cond_0
    :goto_0
    return-object v2

    .line 616138
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 616139
    invoke-static {v0, p0}, LX/3dC;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;)I

    move-result v1

    .line 616140
    if-eqz v1, :cond_0

    .line 616141
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 616142
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 616143
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 616144
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 616145
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 616146
    const-string v1, "ReactionsModelConversionHelper.getReactionsCountFieldsTopReactions"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 616147
    :cond_2
    new-instance v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    invoke-direct {v2, v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;-><init>(LX/15i;)V

    goto :goto_0
.end method
