.class public LX/4g6;
.super LX/4g5;
.source ""


# static fields
.field public static final a:LX/4g6;

.field public static final b:LX/4g6;

.field public static final c:LX/4g6;

.field public static final d:LX/4g6;

.field public static final e:LX/4g6;

.field public static final f:LX/4g6;

.field public static final g:LX/4g6;

.field public static final h:LX/4g6;

.field public static final i:LX/4g6;

.field public static final j:LX/4g6;

.field public static final k:LX/4g6;

.field public static final l:LX/4g6;

.field public static final m:LX/4g6;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 798994
    new-instance v0, LX/4g6;

    const-string v1, "upsell_standard_data_impression"

    invoke-direct {v0, v1}, LX/4g6;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/4g6;->a:LX/4g6;

    .line 798995
    new-instance v0, LX/4g6;

    const-string v1, "upsell_show_loan_impression"

    invoke-direct {v0, v1}, LX/4g6;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/4g6;->b:LX/4g6;

    .line 798996
    new-instance v0, LX/4g6;

    const-string v1, "upsell_buy_attempt"

    invoke-direct {v0, v1}, LX/4g6;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/4g6;->c:LX/4g6;

    .line 798997
    new-instance v0, LX/4g6;

    const-string v1, "upsell_buy_confirm_impression"

    invoke-direct {v0, v1}, LX/4g6;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/4g6;->d:LX/4g6;

    .line 798998
    new-instance v0, LX/4g6;

    const-string v1, "upsell_buy_maybe_impression"

    invoke-direct {v0, v1}, LX/4g6;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/4g6;->e:LX/4g6;

    .line 798999
    new-instance v0, LX/4g6;

    const-string v1, "upsell_buy_failure_impression"

    invoke-direct {v0, v1}, LX/4g6;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/4g6;->f:LX/4g6;

    .line 799000
    new-instance v0, LX/4g6;

    const-string v1, "upsell_buy_success_impression"

    invoke-direct {v0, v1}, LX/4g6;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/4g6;->g:LX/4g6;

    .line 799001
    new-instance v0, LX/4g6;

    const-string v1, "upsell_interstitial_impression"

    invoke-direct {v0, v1}, LX/4g6;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/4g6;->h:LX/4g6;

    .line 799002
    new-instance v0, LX/4g6;

    const-string v1, "upsell_continue_with_current_promo"

    invoke-direct {v0, v1}, LX/4g6;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/4g6;->i:LX/4g6;

    .line 799003
    new-instance v0, LX/4g6;

    const-string v1, "upsell_borrow_loan_confirm_impression"

    invoke-direct {v0, v1}, LX/4g6;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/4g6;->j:LX/4g6;

    .line 799004
    new-instance v0, LX/4g6;

    const-string v1, "click"

    const-string v2, "zero_extra_charges_dialog"

    invoke-direct {v0, v1, v2}, LX/4g6;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/4g6;->k:LX/4g6;

    .line 799005
    new-instance v0, LX/4g6;

    const-string v1, "click"

    const-string v2, "zero_upsell_dialog"

    invoke-direct {v0, v1, v2}, LX/4g6;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/4g6;->l:LX/4g6;

    .line 799006
    new-instance v0, LX/4g6;

    const-string v1, "upsell_carrier_external_portal_click"

    invoke-direct {v0, v1}, LX/4g6;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/4g6;->m:LX/4g6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 798992
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/4g5;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 798993
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 798990
    invoke-direct {p0, p1, p2}, LX/4g5;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 798991
    return-void
.end method
