.class public LX/3hP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public a:I

.field private final b:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 626664
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 626665
    const/4 v0, 0x0

    iput v0, p0, LX/3hP;->a:I

    .line 626666
    iput-object p1, p0, LX/3hP;->b:Ljava/lang/String;

    .line 626667
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/3hP;
    .locals 1

    .prologue
    .line 626668
    new-instance v0, LX/3hP;

    invoke-direct {v0, p0}, LX/3hP;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 626669
    iget v0, p0, LX/3hP;->a:I

    iget-object v1, p0, LX/3hP;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 626670
    iget-object v0, p0, LX/3hP;->b:Ljava/lang/String;

    iget v1, p0, LX/3hP;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    .line 626671
    iget v1, p0, LX/3hP;->a:I

    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, LX/3hP;->a:I

    .line 626672
    return v0
.end method
