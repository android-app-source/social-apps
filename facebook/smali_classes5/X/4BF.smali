.class public LX/4BF;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public static $ul_$xXXandroid_content_Context$xXXFACTORY_METHOD(LX/0QB;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 677493
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_analytics_logger_AnalyticsLogger$xXXFACTORY_METHOD(LX/0QB;)LX/0Zb;
    .locals 1

    .prologue
    .line 677492
    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_auth_viewercontext_ViewerContextManager$xXXFACTORY_METHOD(LX/0QB;)LX/0SI;
    .locals 1

    .prologue
    .line 677491
    invoke-static {p0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v0

    check-cast v0, LX/0SI;

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_common_errorreporting_FbErrorReporter$xXXFACTORY_METHOD(LX/0QB;)LX/03V;
    .locals 1

    .prologue
    .line 677490
    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_common_errorreporting_SoftErrorHelper$xXXFACTORY_METHOD(LX/0QB;)LX/1mP;
    .locals 1

    .prologue
    .line 677489
    invoke-static {p0}, LX/1mP;->a(LX/0QB;)LX/1mP;

    move-result-object v0

    check-cast v0, LX/1mP;

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_common_executors_BackgroundWorkLogger$xXXFACTORY_METHOD(LX/0QB;)LX/0Sj;
    .locals 1

    .prologue
    .line 677488
    invoke-static {p0}, LX/0Si;->a(LX/0QB;)LX/0Si;

    move-result-object v0

    check-cast v0, LX/0Sj;

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_common_executors_HandlerExecutorServiceFactory$xXXFACTORY_METHOD(LX/0QB;)LX/0YF;
    .locals 1

    .prologue
    .line 677487
    invoke-static {p0}, LX/0YF;->a(LX/0QB;)LX/0YF;

    move-result-object v0

    check-cast v0, LX/0YF;

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_common_init_AppInitLock$xXXFACTORY_METHOD(LX/0QB;)LX/0Uq;
    .locals 1

    .prologue
    .line 677494
    invoke-static {p0}, LX/0Uq;->a(LX/0QB;)LX/0Uq;

    move-result-object v0

    check-cast v0, LX/0Uq;

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_common_init_FbSharedPreferencesInitLock$xXXFACTORY_METHOD(LX/0QB;)LX/0cZ;
    .locals 1

    .prologue
    .line 677486
    invoke-static {p0}, LX/0cZ;->b(LX/0QB;)LX/0cZ;

    move-result-object v0

    check-cast v0, LX/0cZ;

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_common_init_GatekeeperInitLock$xXXFACTORY_METHOD(LX/0QB;)LX/0YO;
    .locals 1

    .prologue
    .line 677485
    invoke-static {p0}, LX/0YO;->b(LX/0QB;)LX/0YO;

    move-result-object v0

    check-cast v0, LX/0YO;

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_fbservice_service_BlueServiceQueueManager$xXXFACTORY_METHOD(LX/0QB;)LX/1mM;
    .locals 1

    .prologue
    .line 677484
    invoke-static {p0}, LX/1mM;->getInstance__com_facebook_fbservice_service_BlueServiceQueueManager__INJECTED_BY_TemplateInjector(LX/0QB;)LX/1mM;

    move-result-object v0

    check-cast v0, LX/1mM;

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_inject_Lazy$x3Ccom_facebook_fbservice_service_BlueServiceLogic$x3E$xXXFACTORY_METHOD(LX/0QB;)LX/0Ot;
    .locals 1

    .prologue
    .line 677483
    const/16 v0, 0x5c0

    invoke-static {p0, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public static $ul_$xXXjavax_inject_Provider$x3Cjava_lang_Boolean$x3E$xXXcom_facebook_fbservice_service_OrcaServiceSoftErrorReportingGk$xXXFACTORY_METHOD(LX/0QB;)LX/0Or;
    .locals 1

    .prologue
    .line 677482
    const/16 v0, 0x1490

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 677480
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 677481
    return-void
.end method


# virtual methods
.method public configure()V
    .locals 1

    .prologue
    .line 677479
    return-void
.end method
