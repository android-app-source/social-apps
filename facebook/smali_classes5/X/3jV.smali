.class public LX/3jV;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/3j8;",
            ">;"
        }
    .end annotation
.end field

.field public static b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/3j8;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 632247
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "NT:ACTIVITY_INDICATOR"

    new-instance v2, LX/3jW;

    invoke-direct {v2}, LX/3jW;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "NT:BUTTON"

    new-instance v2, LX/3jX;

    invoke-direct {v2}, LX/3jX;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "NT:BOX"

    new-instance v2, LX/3jY;

    invoke-direct {v2}, LX/3jY;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "NT:STACK_LAYOUT_ABSOLUTE_CHILD"

    new-instance v2, LX/3jZ;

    invoke-direct {v2}, LX/3jZ;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "NT:BOX_CHILD"

    new-instance v2, LX/3ja;

    invoke-direct {v2}, LX/3ja;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "NT:CHECKBOX_INPUT"

    new-instance v2, LX/3jb;

    invoke-direct {v2}, LX/3jb;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "NT:GRADIENT"

    new-instance v2, LX/3jc;

    invoke-direct {v2}, LX/3jc;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "NT:IMAGE"

    new-instance v2, LX/3jd;

    invoke-direct {v2}, LX/3jd;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "NT:RATIO_LAYOUT"

    new-instance v2, LX/3je;

    invoke-direct {v2}, LX/3je;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "NT:SWITCH"

    new-instance v2, LX/3jf;

    invoke-direct {v2}, LX/3jf;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "NT:TEXT"

    new-instance v2, LX/3jg;

    invoke-direct {v2}, LX/3jg;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "NT:TOGGLE_BUTTON"

    new-instance v2, LX/3jh;

    invoke-direct {v2}, LX/3jh;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 632248
    sput-object v0, LX/3jV;->a:LX/0P1;

    sput-object v0, LX/3jV;->b:LX/0P1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 632250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)LX/3j8;
    .locals 1

    .prologue
    .line 632249
    sget-object v0, LX/3jV;->b:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3j8;

    return-object v0
.end method
