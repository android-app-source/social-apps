.class public final LX/4w2;
.super LX/29W;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/29W",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final iterator:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<+",
            "LX/0am",
            "<+TT;>;>;"
        }
    .end annotation
.end field

.field public final synthetic this$0:LX/4w3;


# direct methods
.method public constructor <init>(LX/4w3;)V
    .locals 1

    .prologue
    .line 819806
    iput-object p1, p0, LX/4w2;->this$0:LX/4w3;

    invoke-direct {p0}, LX/29W;-><init>()V

    .line 819807
    iget-object v0, p0, LX/4w2;->this$0:LX/4w3;

    iget-object v0, v0, LX/4w3;->val$optionals:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Iterator;

    iput-object v0, p0, LX/4w2;->iterator:Ljava/util/Iterator;

    return-void
.end method


# virtual methods
.method public computeNext()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 819808
    :cond_0
    iget-object v0, p0, LX/4w2;->iterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 819809
    iget-object v0, p0, LX/4w2;->iterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0am;

    .line 819810
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 819811
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    .line 819812
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, LX/29W;->endOfData()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method
