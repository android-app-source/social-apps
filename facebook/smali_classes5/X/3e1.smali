.class public final enum LX/3e1;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3e1;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3e1;

.field public static final enum ANIMATED:LX/3e1;

.field private static final DB_VALUE_TO_INSTANCE:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/3e1;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum PREVIEW:LX/3e1;

.field public static final enum STATIC:LX/3e1;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 618987
    new-instance v0, LX/3e1;

    const-string v1, "STATIC"

    const-string v2, "static"

    invoke-direct {v0, v1, v3, v2}, LX/3e1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3e1;->STATIC:LX/3e1;

    .line 618988
    new-instance v0, LX/3e1;

    const-string v1, "ANIMATED"

    const-string v2, "animated"

    invoke-direct {v0, v1, v4, v2}, LX/3e1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3e1;->ANIMATED:LX/3e1;

    .line 618989
    new-instance v0, LX/3e1;

    const-string v1, "PREVIEW"

    const-string v2, "preview"

    invoke-direct {v0, v1, v5, v2}, LX/3e1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3e1;->PREVIEW:LX/3e1;

    .line 618990
    const/4 v0, 0x3

    new-array v0, v0, [LX/3e1;

    sget-object v1, LX/3e1;->STATIC:LX/3e1;

    aput-object v1, v0, v3

    sget-object v1, LX/3e1;->ANIMATED:LX/3e1;

    aput-object v1, v0, v4

    sget-object v1, LX/3e1;->PREVIEW:LX/3e1;

    aput-object v1, v0, v5

    sput-object v0, LX/3e1;->$VALUES:[LX/3e1;

    .line 618991
    sget-object v0, LX/3e1;->STATIC:LX/3e1;

    invoke-virtual {v0}, LX/3e1;->getDbName()Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/3e1;->STATIC:LX/3e1;

    sget-object v2, LX/3e1;->ANIMATED:LX/3e1;

    invoke-virtual {v2}, LX/3e1;->getDbName()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/3e1;->ANIMATED:LX/3e1;

    sget-object v4, LX/3e1;->PREVIEW:LX/3e1;

    invoke-virtual {v4}, LX/3e1;->getDbName()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/3e1;->PREVIEW:LX/3e1;

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, LX/3e1;->DB_VALUE_TO_INSTANCE:LX/0P1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 618992
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 618993
    iput-object p3, p0, LX/3e1;->mValue:Ljava/lang/String;

    .line 618994
    return-void
.end method

.method public static fromDbString(Ljava/lang/String;)LX/3e1;
    .locals 1

    .prologue
    .line 618995
    sget-object v0, LX/3e1;->DB_VALUE_TO_INSTANCE:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3e1;

    return-object v0
.end method

.method public static isDbString(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 618996
    sget-object v0, LX/3e1;->DB_VALUE_TO_INSTANCE:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/3e1;
    .locals 1

    .prologue
    .line 618997
    const-class v0, LX/3e1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3e1;

    return-object v0
.end method

.method public static values()[LX/3e1;
    .locals 1

    .prologue
    .line 618998
    sget-object v0, LX/3e1;->$VALUES:[LX/3e1;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3e1;

    return-object v0
.end method


# virtual methods
.method public final getDbName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 618999
    iget-object v0, p0, LX/3e1;->mValue:Ljava/lang/String;

    return-object v0
.end method
