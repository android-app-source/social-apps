.class public LX/4NH;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 691924
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 691865
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_9

    .line 691866
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 691867
    :goto_0
    return v1

    .line 691868
    :cond_0
    const-string v9, "unread_count"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 691869
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    .line 691870
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_7

    .line 691871
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 691872
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 691873
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 691874
    const-string v9, "edges"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 691875
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 691876
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_ARRAY:LX/15z;

    if-ne v8, v9, :cond_2

    .line 691877
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_ARRAY:LX/15z;

    if-eq v8, v9, :cond_2

    .line 691878
    invoke-static {p0, p1}, LX/4NJ;->b(LX/15w;LX/186;)I

    move-result v8

    .line 691879
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 691880
    :cond_2
    invoke-static {v7, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v7

    move v7, v7

    .line 691881
    goto :goto_1

    .line 691882
    :cond_3
    const-string v9, "page_info"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 691883
    invoke-static {p0, p1}, LX/264;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 691884
    :cond_4
    const-string v9, "session_id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 691885
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 691886
    :cond_5
    const-string v9, "title"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 691887
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 691888
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 691889
    :cond_7
    const/4 v8, 0x5

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 691890
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 691891
    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 691892
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 691893
    if-eqz v0, :cond_8

    .line 691894
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v4, v1}, LX/186;->a(III)V

    .line 691895
    :cond_8
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 691896
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 691897
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 691898
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 691899
    if-eqz v0, :cond_1

    .line 691900
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691901
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 691902
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 691903
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2, p3}, LX/4NJ;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 691904
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 691905
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 691906
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691907
    if-eqz v0, :cond_2

    .line 691908
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691909
    invoke-static {p0, v0, p2}, LX/264;->a(LX/15i;ILX/0nX;)V

    .line 691910
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691911
    if-eqz v0, :cond_3

    .line 691912
    const-string v1, "session_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691913
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691914
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 691915
    if-eqz v0, :cond_4

    .line 691916
    const-string v1, "unread_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691917
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 691918
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691919
    if-eqz v0, :cond_5

    .line 691920
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691921
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 691922
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 691923
    return-void
.end method
