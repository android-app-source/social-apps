.class public final LX/3qF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field public b:Ljava/lang/CharSequence;

.field public c:[Ljava/lang/CharSequence;

.field public d:Z

.field private e:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 642123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 642124
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3qF;->d:Z

    .line 642125
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LX/3qF;->e:Landroid/os/Bundle;

    .line 642126
    if-nez p1, :cond_0

    .line 642127
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Result key can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 642128
    :cond_0
    iput-object p1, p0, LX/3qF;->a:Ljava/lang/String;

    .line 642129
    return-void
.end method


# virtual methods
.method public final a()LX/3qL;
    .locals 6

    .prologue
    .line 642130
    new-instance v0, LX/3qL;

    iget-object v1, p0, LX/3qF;->a:Ljava/lang/String;

    iget-object v2, p0, LX/3qF;->b:Ljava/lang/CharSequence;

    iget-object v3, p0, LX/3qF;->c:[Ljava/lang/CharSequence;

    iget-boolean v4, p0, LX/3qF;->d:Z

    iget-object v5, p0, LX/3qF;->e:Landroid/os/Bundle;

    invoke-direct/range {v0 .. v5}, LX/3qL;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/CharSequence;ZLandroid/os/Bundle;)V

    return-object v0
.end method
