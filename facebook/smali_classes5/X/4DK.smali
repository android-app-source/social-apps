.class public final LX/4DK;
.super LX/0gS;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 678992
    invoke-direct {p0}, LX/0gS;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/3Aj;)LX/4DK;
    .locals 1

    .prologue
    .line 678993
    const-string v0, "viewer_coordinates"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 678994
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/4DK;
    .locals 1

    .prologue
    .line 678995
    const-string v0, "query"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 678996
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/4DK;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/AddressTypeaheadProvider;
        .end annotation
    .end param

    .prologue
    .line 678997
    const-string v0, "provider"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 678998
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/4DK;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/AddressTypeaheadType;
        .end annotation
    .end param

    .prologue
    .line 678999
    const-string v0, "search_type"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679000
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/4DK;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/AddressTypeaheadCaller;
        .end annotation
    .end param

    .prologue
    .line 679001
    const-string v0, "caller"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679002
    return-object p0
.end method
