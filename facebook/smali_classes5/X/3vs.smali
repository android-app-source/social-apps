.class public final LX/3vs;
.super Landroid/support/v7/widget/LinearLayoutCompat;
.source ""

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field public final synthetic a:LX/3vu;

.field private final b:[I

.field public c:LX/3u0;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/view/View;


# direct methods
.method public constructor <init>(LX/3vu;Landroid/content/Context;LX/3u0;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 653754
    iput-object p1, p0, LX/3vs;->a:LX/3vu;

    .line 653755
    const v0, 0x7f01000b

    invoke-direct {p0, p2, v3, v0}, Landroid/support/v7/widget/LinearLayoutCompat;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 653756
    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x10100d4

    aput v1, v0, v2

    iput-object v0, p0, LX/3vs;->b:[I

    .line 653757
    iput-object p3, p0, LX/3vs;->c:LX/3u0;

    .line 653758
    iget-object v0, p0, LX/3vs;->b:[I

    const v1, 0x7f01000b

    invoke-static {p2, v3, v0, v1, v2}, LX/3wC;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)LX/3wC;

    move-result-object v0

    .line 653759
    invoke-virtual {v0, v2}, LX/3wC;->d(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 653760
    invoke-virtual {v0, v2}, LX/3wC;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/3vs;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 653761
    :cond_0
    invoke-virtual {v0}, LX/3wC;->b()V

    .line 653762
    if-eqz p4, :cond_1

    .line 653763
    const v0, 0x800013

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/LinearLayoutCompat;->setGravity(I)V

    .line 653764
    :cond_1
    invoke-static {p0}, LX/3vs;->b(LX/3vs;)V

    .line 653765
    return-void
.end method

.method public static b(LX/3vs;)V
    .locals 10

    .prologue
    const/16 v9, 0x10

    const/16 v6, 0x8

    const/4 v8, -0x2

    const/4 v1, 0x0

    const/4 v7, 0x0

    .line 653701
    iget-object v2, p0, LX/3vs;->c:LX/3u0;

    .line 653702
    invoke-virtual {v2}, LX/3u0;->d()Landroid/view/View;

    move-result-object v3

    .line 653703
    if-eqz v3, :cond_4

    .line 653704
    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 653705
    if-eq v0, p0, :cond_1

    .line 653706
    if-eqz v0, :cond_0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 653707
    :cond_0
    invoke-virtual {p0, v3}, LX/3vs;->addView(Landroid/view/View;)V

    .line 653708
    :cond_1
    iput-object v3, p0, LX/3vs;->f:Landroid/view/View;

    .line 653709
    iget-object v0, p0, LX/3vs;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3vs;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 653710
    :cond_2
    iget-object v0, p0, LX/3vs;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 653711
    iget-object v0, p0, LX/3vs;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 653712
    iget-object v0, p0, LX/3vs;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 653713
    :cond_3
    :goto_0
    return-void

    .line 653714
    :cond_4
    iget-object v0, p0, LX/3vs;->f:Landroid/view/View;

    if-eqz v0, :cond_5

    .line 653715
    iget-object v0, p0, LX/3vs;->f:Landroid/view/View;

    invoke-virtual {p0, v0}, LX/3vs;->removeView(Landroid/view/View;)V

    .line 653716
    iput-object v7, p0, LX/3vs;->f:Landroid/view/View;

    .line 653717
    :cond_5
    invoke-virtual {v2}, LX/3u0;->b()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 653718
    invoke-virtual {v2}, LX/3u0;->c()Ljava/lang/CharSequence;

    move-result-object v3

    .line 653719
    if-eqz v0, :cond_b

    .line 653720
    iget-object v4, p0, LX/3vs;->e:Landroid/widget/ImageView;

    if-nez v4, :cond_6

    .line 653721
    new-instance v4, Landroid/widget/ImageView;

    invoke-virtual {p0}, LX/3vs;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 653722
    new-instance v5, LX/3wd;

    invoke-direct {v5, v8, v8}, LX/3wd;-><init>(II)V

    .line 653723
    iput v9, v5, LX/3wd;->h:I

    .line 653724
    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 653725
    invoke-virtual {p0, v4, v1}, LX/3vs;->addView(Landroid/view/View;I)V

    .line 653726
    iput-object v4, p0, LX/3vs;->e:Landroid/widget/ImageView;

    .line 653727
    :cond_6
    iget-object v4, p0, LX/3vs;->e:Landroid/widget/ImageView;

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 653728
    iget-object v0, p0, LX/3vs;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 653729
    :cond_7
    :goto_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    .line 653730
    :goto_2
    if-eqz v0, :cond_d

    .line 653731
    iget-object v4, p0, LX/3vs;->d:Landroid/widget/TextView;

    if-nez v4, :cond_8

    .line 653732
    new-instance v4, LX/3up;

    invoke-virtual {p0}, LX/3vs;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f01000d

    invoke-direct {v4, v5, v7, v6}, LX/3up;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 653733
    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 653734
    new-instance v5, LX/3wd;

    invoke-direct {v5, v8, v8}, LX/3wd;-><init>(II)V

    .line 653735
    iput v9, v5, LX/3wd;->h:I

    .line 653736
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 653737
    invoke-virtual {p0, v4}, LX/3vs;->addView(Landroid/view/View;)V

    .line 653738
    iput-object v4, p0, LX/3vs;->d:Landroid/widget/TextView;

    .line 653739
    :cond_8
    iget-object v4, p0, LX/3vs;->d:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 653740
    iget-object v3, p0, LX/3vs;->d:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 653741
    :cond_9
    :goto_3
    iget-object v3, p0, LX/3vs;->e:Landroid/widget/ImageView;

    if-eqz v3, :cond_a

    .line 653742
    iget-object v3, p0, LX/3vs;->e:Landroid/widget/ImageView;

    invoke-virtual {v2}, LX/3u0;->f()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 653743
    :cond_a
    if-nez v0, :cond_e

    invoke-virtual {v2}, LX/3u0;->f()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 653744
    invoke-virtual {p0, p0}, LX/3vs;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto/16 :goto_0

    .line 653745
    :cond_b
    iget-object v0, p0, LX/3vs;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_7

    .line 653746
    iget-object v0, p0, LX/3vs;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 653747
    iget-object v0, p0, LX/3vs;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_c
    move v0, v1

    .line 653748
    goto :goto_2

    .line 653749
    :cond_d
    iget-object v3, p0, LX/3vs;->d:Landroid/widget/TextView;

    if-eqz v3, :cond_9

    .line 653750
    iget-object v3, p0, LX/3vs;->d:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 653751
    iget-object v3, p0, LX/3vs;->d:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 653752
    :cond_e
    invoke-virtual {p0, v7}, LX/3vs;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 653753
    invoke-virtual {p0, v1}, LX/3vs;->setLongClickable(Z)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 653698
    invoke-super {p0, p1}, Landroid/support/v7/widget/LinearLayoutCompat;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 653699
    const-class v0, LX/3u0;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 653700
    return-void
.end method

.method public final onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 2

    .prologue
    .line 653674
    invoke-super {p0, p1}, Landroid/support/v7/widget/LinearLayoutCompat;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 653675
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 653676
    const-class v0, LX/3u0;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 653677
    :cond_0
    return-void
.end method

.method public final onLongClick(Landroid/view/View;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 653688
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 653689
    invoke-virtual {p0, v0}, LX/3vs;->getLocationOnScreen([I)V

    .line 653690
    invoke-virtual {p0}, LX/3vs;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 653691
    invoke-virtual {p0}, LX/3vs;->getWidth()I

    move-result v2

    .line 653692
    invoke-virtual {p0}, LX/3vs;->getHeight()I

    move-result v3

    .line 653693
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 653694
    iget-object v5, p0, LX/3vs;->c:LX/3u0;

    invoke-virtual {v5}, LX/3u0;->f()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v1, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 653695
    const/16 v5, 0x31

    aget v0, v0, v6

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    div-int/lit8 v2, v4, 0x2

    sub-int/2addr v0, v2

    invoke-virtual {v1, v5, v0, v3}, Landroid/widget/Toast;->setGravity(III)V

    .line 653696
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 653697
    const/4 v0, 0x1

    return v0
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 653684
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/LinearLayoutCompat;->onMeasure(II)V

    .line 653685
    iget-object v0, p0, LX/3vs;->a:LX/3vu;

    iget v0, v0, LX/3vu;->b:I

    if-lez v0, :cond_0

    invoke-virtual {p0}, LX/3vs;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, LX/3vs;->a:LX/3vu;

    iget v1, v1, LX/3vu;->b:I

    if-le v0, v1, :cond_0

    .line 653686
    iget-object v0, p0, LX/3vs;->a:LX/3vu;

    iget v0, v0, LX/3vu;->b:I

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, v0, p2}, Landroid/support/v7/widget/LinearLayoutCompat;->onMeasure(II)V

    .line 653687
    :cond_0
    return-void
.end method

.method public final setSelected(Z)V
    .locals 1

    .prologue
    .line 653678
    invoke-virtual {p0}, LX/3vs;->isSelected()Z

    move-result v0

    if-eq v0, p1, :cond_1

    const/4 v0, 0x1

    .line 653679
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v7/widget/LinearLayoutCompat;->setSelected(Z)V

    .line 653680
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 653681
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, LX/3vs;->sendAccessibilityEvent(I)V

    .line 653682
    :cond_0
    return-void

    .line 653683
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
