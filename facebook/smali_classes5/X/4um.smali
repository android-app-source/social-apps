.class public LX/4um;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Handler$Callback;


# static fields
.field public static final d:Ljava/lang/Object;

.field private static e:LX/4um;


# instance fields
.field public a:J

.field public b:J

.field public c:J

.field public final f:Landroid/content/Context;

.field public final g:LX/1vX;

.field public h:I

.field public final i:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/3Kb",
            "<*>;>;"
        }
    .end annotation
.end field

.field public final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/4uL",
            "<*>;",
            "LX/3Kb",
            "<*>;>;"
        }
    .end annotation
.end field

.field private k:LX/4uX;

.field public final l:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/4uL",
            "<*>;>;"
        }
    .end annotation
.end field

.field public final m:Landroid/os/Handler;

.field public final n:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<",
            "LX/4sV",
            "<*>;>;"
        }
    .end annotation
.end field

.field public final o:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/3KT;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lcom/google/android/gms/internal/zzqc$zzb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/4um;->d:Ljava/lang/Object;

    return-void
.end method

.method public static a()LX/4um;
    .locals 2

    sget-object v1, LX/4um;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/4um;->e:LX/4um;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(LX/4uX;)V
    .locals 2

    sget-object v1, LX/4um;->d:Ljava/lang/Object;

    monitor-enter v1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/4um;->k:LX/4uX;

    iget-object v0, p0, LX/4um;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/ConnectionResult;I)Z
    .locals 13

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/4um;->g:LX/1vX;

    iget v1, p1, Lcom/google/android/gms/common/ConnectionResult;->c:I

    move v1, v1

    invoke-virtual {v0, v1}, LX/1od;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    iget-object v0, p0, LX/4um;->g:LX/1vX;

    iget-object v1, p0, LX/4um;->f:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p1, Lcom/google/android/gms/common/ConnectionResult;->d:Landroid/app/PendingIntent;

    move-object v2, v2

    :goto_0
    move-object v2, v2

    if-eqz v2, :cond_2

    iget v3, p1, Lcom/google/android/gms/common/ConnectionResult;->c:I

    move v3, v3

    invoke-static {v1, v2, p2}, Lcom/google/android/gms/common/api/GoogleApiActivity;->a(Landroid/content/Context;Landroid/app/PendingIntent;I)Landroid/app/PendingIntent;

    move-result-object v2

    const/4 v4, 0x0

    const v9, 0x108008a

    const/4 v11, 0x1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {v1}, LX/1oW;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    const/4 v5, 0x6

    if-ne v3, v5, :cond_d

    const-string v5, "common_google_play_services_resolution_required_title"

    invoke-static {v1, v5}, LX/2Gy;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :goto_1
    move-object v5, v5

    if-nez v5, :cond_1

    const v5, 0x7f0800b9

    invoke-virtual {v6, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    :cond_1
    const/4 v8, 0x6

    if-ne v3, v8, :cond_e

    const-string v8, "common_google_play_services_resolution_required_text"

    invoke-static {v1, v8, v7}, LX/2Gy;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    :goto_2
    move-object v7, v8

    invoke-static {v1}, LX/1oX;->a(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-static {}, LX/1oe;->e()Z

    move-result v8

    invoke-static {v8}, LX/1ol;->a(Z)V

    new-instance v8, Landroid/app/Notification$Builder;

    invoke-direct {v8, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v9, 0x7f0202ca

    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v8

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-virtual {v8, v11}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v8

    new-instance v9, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v9}, Landroid/app/Notification$BigTextStyle;-><init>()V

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v11, v12

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, " "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9, v5}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v5

    invoke-virtual {v8, v5}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    move-result-object v5

    const v7, 0x7f0202b5

    const v8, 0x7f0800c2

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v7, v6, v2}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v5

    move-object v6, v5

    :goto_3
    sparse-switch v3, :sswitch_data_0

    const/4 v5, 0x0

    :goto_4
    move v5, v5

    if-eqz v5, :cond_b

    const/16 v5, 0x28c4

    sget-object v7, LX/1oW;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    move v7, v5

    :goto_5
    const-string v5, "notification"

    invoke-virtual {v1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/NotificationManager;

    if-eqz v4, :cond_c

    invoke-virtual {v5, v4, v7, v6}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    :cond_2
    :goto_6
    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_7

    :cond_4
    iget v2, p1, Lcom/google/android/gms/common/ConnectionResult;->c:I

    move v2, v2

    invoke-static {v1}, LX/1oX;->a(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    const/16 v2, 0x2a

    :cond_5
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/1od;->a(Landroid/content/Context;II)Landroid/app/PendingIntent;

    move-result-object v2

    goto/16 :goto_0

    :cond_6
    const v8, 0x7f0800b9

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {}, LX/1oe;->a()Z

    move-result v8

    if-eqz v8, :cond_a

    new-instance v8, Landroid/app/Notification$Builder;

    invoke-direct {v8, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-virtual {v8, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v5

    invoke-virtual {v5, v11}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v5

    invoke-static {}, LX/1oe;->h()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-virtual {v5, v11}, Landroid/app/Notification$Builder;->setLocalOnly(Z)Landroid/app/Notification$Builder;

    :cond_7
    invoke-static {}, LX/1oe;->e()Z

    move-result v6

    if-eqz v6, :cond_9

    new-instance v6, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v6}, Landroid/app/Notification$BigTextStyle;-><init>()V

    invoke-virtual {v6, v7}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    invoke-virtual {v5}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v5

    :goto_8
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x13

    if-ne v6, v7, :cond_8

    iget-object v6, v5, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    const-string v7, "android.support.localOnly"

    invoke-virtual {v6, v7, v11}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_8
    move-object v6, v5

    goto/16 :goto_3

    :cond_9
    invoke-virtual {v5}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v5

    goto :goto_8

    :cond_a
    new-instance v8, LX/2HB;

    invoke-direct {v8, v1}, LX/2HB;-><init>(Landroid/content/Context;)V

    invoke-virtual {v8, v9}, LX/2HB;->a(I)LX/2HB;

    move-result-object v8

    invoke-virtual {v8, v6}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-virtual {v6, v9, v10}, LX/2HB;->a(J)LX/2HB;

    move-result-object v6

    invoke-virtual {v6, v11}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v6

    iput-object v2, v6, LX/2HB;->d:Landroid/app/PendingIntent;

    move-object v6, v6

    invoke-virtual {v6, v5}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v5

    invoke-virtual {v5}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v5

    move-object v6, v5

    goto/16 :goto_3

    :cond_b
    const v5, 0x9b6d

    move v7, v5

    goto/16 :goto_5

    :cond_c
    invoke-virtual {v5, v7, v6}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_6

    :cond_d
    invoke-static {v1, v3}, LX/2Gy;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :cond_e
    invoke-static {v1, v3, v7}, LX/2Gy;->a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_2

    :sswitch_0
    const/4 v5, 0x1

    goto/16 :goto_4

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x12 -> :sswitch_0
        0x2a -> :sswitch_0
    .end sparse-switch
.end method

.method public final b()V
    .locals 3

    iget-object v0, p0, LX/4um;->m:Landroid/os/Handler;

    iget-object v1, p0, LX/4um;->m:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public final b(Lcom/google/android/gms/common/ConnectionResult;I)V
    .locals 4

    invoke-virtual {p0, p1, p2}, LX/4um;->a(Lcom/google/android/gms/common/ConnectionResult;I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/4um;->m:Landroid/os/Handler;

    iget-object v1, p0, LX/4um;->m:Landroid/os/Handler;

    const/4 v2, 0x5

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 7
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    const-string v1, "GoogleApiManager"

    iget v2, p1, Landroid/os/Message;->what:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x1f

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unknown message id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/4uQ;

    const/4 v2, 0x0

    invoke-virtual {v2}, LX/026;->keySet()Ljava/util/Set;

    move-result-object v2

    move-object v2, v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/4uL;

    iget-object v3, p0, LX/4um;->j:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3Kb;

    if-nez v3, :cond_b

    invoke-virtual {v0}, LX/2wf;->h()V

    :cond_0
    :goto_2
    move v0, v1

    goto :goto_0

    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/4sV;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget-object v3, v0, LX/4sV;->e:LX/4uL;

    move-object v3, v3

    iget-object v4, p0, LX/4um;->j:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, LX/4um;->j:Ljava/util/Map;

    new-instance v5, LX/3Kb;

    invoke-direct {v5, p0, v0}, LX/3Kb;-><init>(LX/4um;LX/4sV;)V

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v4, p0, LX/4um;->j:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3Kb;

    iget-object v4, v3, LX/3Kb;->f:Landroid/util/SparseArray;

    new-instance v5, LX/2wd;

    iget-object v6, v3, LX/3Kb;->e:LX/4uL;

    iget-object p1, v6, LX/4uL;->a:LX/2vs;

    invoke-virtual {p1}, LX/2vs;->d()LX/2vo;

    move-result-object p1

    move-object v6, p1

    iget-object p1, v3, LX/3Kb;->c:LX/2wJ;

    invoke-direct {v5, v6, p1}, LX/2wd;-><init>(LX/2vo;LX/2wJ;)V

    invoke-virtual {v4, v2, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v4, p0, LX/4um;->i:Landroid/util/SparseArray;

    invoke-virtual {v4, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    invoke-static {v3}, LX/3Kb;->j(LX/3Kb;)V

    iget-object v3, p0, LX/4um;->o:Landroid/util/SparseArray;

    new-instance v4, LX/3KT;

    iget-object v5, p0, LX/4um;->n:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v4, p0, v0, v2, v5}, LX/3KT;-><init>(LX/4um;LX/4sV;ILjava/lang/ref/ReferenceQueue;)V

    invoke-virtual {v3, v2, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v3, p0, LX/4um;->p:Lcom/google/android/gms/internal/zzqc$zzb;

    if-eqz v3, :cond_2

    iget-object v3, p0, LX/4um;->p:Lcom/google/android/gms/internal/zzqc$zzb;

    iget-object v3, v3, Lcom/google/android/gms/internal/zzqc$zzb;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    if-nez v3, :cond_3

    :cond_2
    new-instance v3, Lcom/google/android/gms/internal/zzqc$zzb;

    iget-object v4, p0, LX/4um;->n:Ljava/lang/ref/ReferenceQueue;

    iget-object v5, p0, LX/4um;->o:Landroid/util/SparseArray;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/internal/zzqc$zzb;-><init>(Ljava/lang/ref/ReferenceQueue;Landroid/util/SparseArray;)V

    iput-object v3, p0, LX/4um;->p:Lcom/google/android/gms/internal/zzqc$zzb;

    iget-object v3, p0, LX/4um;->p:Lcom/google/android/gms/internal/zzqc$zzb;

    invoke-virtual {v3}, Lcom/google/android/gms/internal/zzqc$zzb;->start()V

    :cond_3
    goto :goto_2

    :pswitch_2
    iget-object v0, p0, LX/4um;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Kb;

    invoke-virtual {v0}, LX/3Kb;->a()V

    invoke-static {v0}, LX/3Kb;->j(LX/3Kb;)V

    goto :goto_3

    :cond_4
    goto/16 :goto_2

    :pswitch_3
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget-object v2, p0, LX/4um;->i:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3Kb;

    if-eqz v2, :cond_e

    iget-object v3, p0, LX/4um;->i:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->delete(I)V

    iget-object v3, v2, LX/3Kb;->f:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/2wd;

    new-instance v4, LX/3KY;

    invoke-direct {v4, v2, v0}, LX/3KY;-><init>(LX/3Kb;I)V

    iget-object v2, v3, LX/2wd;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v4}, LX/4v4;->a()V

    :cond_5
    iput-object v4, v3, LX/2wd;->e:LX/4v4;

    :goto_4
    goto/16 :goto_2

    :pswitch_4
    iget v2, p1, Landroid/os/Message;->arg1:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    if-ne v3, v1, :cond_6

    move v0, v1

    :cond_6
    iget-object v3, p0, LX/4um;->i:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3Kb;

    if-eqz v3, :cond_f

    if-nez v0, :cond_7

    iget-object v4, p0, LX/4um;->i:Landroid/util/SparseArray;

    invoke-virtual {v4, v2}, Landroid/util/SparseArray;->delete(I)V

    :cond_7
    invoke-virtual {v3, v2, v0}, LX/3Kb;->a(IZ)V

    :goto_5
    goto/16 :goto_2

    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/4uJ;

    iget-object v2, p0, LX/4um;->i:Landroid/util/SparseArray;

    iget v3, v0, LX/4uJ;->a:I

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3Kb;

    iget-object v3, v2, LX/3Kb;->c:LX/2wJ;

    invoke-interface {v3}, LX/2wJ;->d()Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-static {v2, v0}, LX/3Kb;->b(LX/3Kb;LX/4uJ;)V

    invoke-static {v2}, LX/3Kb;->h(LX/3Kb;)V

    :goto_6
    goto/16 :goto_2

    :pswitch_6
    iget-object v0, p0, LX/4um;->i:Landroid/util/SparseArray;

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4um;->i:Landroid/util/SparseArray;

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Kb;

    new-instance v2, Lcom/google/android/gms/common/api/Status;

    const/16 v3, 0x11

    const-string v4, "Error resolution was canceled by the user."

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    invoke-static {v0, v2}, LX/3Kb;->a$redex0(LX/3Kb;Lcom/google/android/gms/common/api/Status;)V

    goto/16 :goto_2

    :pswitch_7
    iget-object v0, p0, LX/4um;->j:Ljava/util/Map;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4um;->j:Ljava/util/Map;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Kb;

    iget-boolean v2, v0, LX/3Kb;->i:Z

    if-eqz v2, :cond_8

    invoke-static {v0}, LX/3Kb;->j(LX/3Kb;)V

    :cond_8
    goto/16 :goto_2

    :pswitch_8
    iget-object v0, p0, LX/4um;->j:Ljava/util/Map;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4um;->j:Ljava/util/Map;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Kb;

    const/16 v4, 0x8

    iget-boolean v2, v0, LX/3Kb;->i:Z

    if-eqz v2, :cond_9

    invoke-static {v0}, LX/3Kb;->f(LX/3Kb;)V

    iget-object v2, v0, LX/3Kb;->a:LX/4um;

    iget-object v2, v2, LX/4um;->g:LX/1vX;

    iget-object v3, v0, LX/3Kb;->a:LX/4um;

    iget-object v3, v3, LX/4um;->f:Landroid/content/Context;

    invoke-virtual {v2, v3}, LX/1od;->a(Landroid/content/Context;)I

    move-result v2

    const/16 v3, 0x12

    if-ne v2, v3, :cond_12

    new-instance v2, Lcom/google/android/gms/common/api/Status;

    const-string v3, "Connection timed out while waiting for Google Play services update to complete."

    invoke-direct {v2, v4, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    :goto_7
    invoke-static {v0, v2}, LX/3Kb;->a$redex0(LX/3Kb;Lcom/google/android/gms/common/api/Status;)V

    iget-object v2, v0, LX/3Kb;->c:LX/2wJ;

    invoke-interface {v2}, LX/2wJ;->f()V

    :cond_9
    goto/16 :goto_2

    :pswitch_9
    iget-object v0, p0, LX/4um;->j:Ljava/util/Map;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4um;->j:Ljava/util/Map;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Kb;

    iget-object v2, v0, LX/3Kb;->c:LX/2wJ;

    invoke-interface {v2}, LX/2wJ;->d()Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v2, v0, LX/3Kb;->h:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-nez v2, :cond_a

    const/4 v2, 0x0

    move v3, v2

    :goto_8
    iget-object v2, v0, LX/3Kb;->f:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v3, v2, :cond_14

    iget-object v2, v0, LX/3Kb;->f:Landroid/util/SparseArray;

    iget-object v4, v0, LX/3Kb;->f:Landroid/util/SparseArray;

    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2wd;

    const/4 v5, 0x0

    iget-object v4, v2, LX/2wd;->a:Ljava/util/Set;

    sget-object v6, LX/2wd;->b:[LX/2we;

    invoke-interface {v4, v6}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [LX/2we;

    array-length p0, v4

    move v6, v5

    :goto_9
    if-ge v6, p0, :cond_16

    aget-object p1, v4, v6

    invoke-virtual {p1}, LX/2wf;->g()Z

    move-result p1

    if-nez p1, :cond_15

    const/4 v4, 0x1

    :goto_a
    move v2, v4

    if-eqz v2, :cond_13

    invoke-static {v0}, LX/3Kb;->h(LX/3Kb;)V

    :cond_a
    :goto_b
    goto/16 :goto_2

    :cond_b
    iget-object p1, v3, LX/3Kb;->c:LX/2wJ;

    invoke-interface {p1}, LX/2wJ;->d()Z

    move-result p1

    move p1, p1

    if-eqz p1, :cond_c

    sget-object v3, Lcom/google/android/gms/common/ConnectionResult;->a:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {v0, v2, v3}, LX/4uQ;->a(LX/4uL;Lcom/google/android/gms/common/ConnectionResult;)V

    goto/16 :goto_1

    :cond_c
    iget-object p1, v3, LX/3Kb;->j:Lcom/google/android/gms/common/ConnectionResult;

    move-object p1, p1

    if-eqz p1, :cond_d

    iget-object p1, v3, LX/3Kb;->j:Lcom/google/android/gms/common/ConnectionResult;

    move-object v3, p1

    invoke-virtual {v0, v2, v3}, LX/4uQ;->a(LX/4uL;Lcom/google/android/gms/common/ConnectionResult;)V

    goto/16 :goto_1

    :cond_d
    iget-object v2, v3, LX/3Kb;->g:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_e
    const-string v2, "GoogleApiManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x40

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "onCleanupLeakInternal received for unknown instance: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-static {v2, v3, v4}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_4

    :cond_f
    const-string v3, "GoogleApiManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x34

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "onRelease received for unknown instance: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/Exception;

    invoke-direct {v5}, Ljava/lang/Exception;-><init>()V

    invoke-static {v3, v4, v5}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_5

    :cond_10
    iget-object v3, v2, LX/3Kb;->b:Ljava/util/Queue;

    invoke-interface {v3, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    iget-object v3, v2, LX/3Kb;->j:Lcom/google/android/gms/common/ConnectionResult;

    if-eqz v3, :cond_11

    iget-object v3, v2, LX/3Kb;->j:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {v3}, Lcom/google/android/gms/common/ConnectionResult;->a()Z

    move-result v3

    if-eqz v3, :cond_11

    iget-object v3, v2, LX/3Kb;->j:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {v2, v3}, LX/3Kb;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    goto/16 :goto_6

    :cond_11
    invoke-static {v2}, LX/3Kb;->j(LX/3Kb;)V

    goto/16 :goto_6

    :cond_12
    new-instance v2, Lcom/google/android/gms/common/api/Status;

    const-string v3, "API failed to connect while resuming due to an unknown error."

    invoke-direct {v2, v4, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    goto/16 :goto_7

    :cond_13
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto/16 :goto_8

    :cond_14
    iget-object v2, v0, LX/3Kb;->c:LX/2wJ;

    invoke-interface {v2}, LX/2wJ;->f()V

    goto/16 :goto_b

    :cond_15
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_9

    :cond_16
    move v4, v5

    goto/16 :goto_a

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_1
        :pswitch_4
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
