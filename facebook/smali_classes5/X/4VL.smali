.class public LX/4VL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Bq;


# instance fields
.field public final b:[LX/4VT;

.field private final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Uh;


# direct methods
.method public varargs constructor <init>(LX/0Uh;[LX/4VT;)V
    .locals 4

    .prologue
    .line 742258
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 742259
    array-length v0, p2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 742260
    iput-object p2, p0, LX/4VL;->b:[LX/4VT;

    .line 742261
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v1

    .line 742262
    array-length v2, p2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_0

    aget-object v3, p2, v0

    .line 742263
    invoke-interface {v3}, LX/4VT;->a()LX/0Rf;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    .line 742264
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 742265
    :cond_0
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    move-object v0, v0

    .line 742266
    iput-object v0, p0, LX/4VL;->c:LX/0Rf;

    .line 742267
    iput-object p1, p0, LX/4VL;->d:LX/0Uh;

    .line 742268
    return-void

    .line 742269
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Z)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Ljava/lang/Object;",
            ">(TM;Z)TM;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 742270
    iget-object v0, p0, LX/4VL;->d:LX/0Uh;

    const/16 v3, 0x38a

    invoke-virtual {v0, v3, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 742271
    :try_start_0
    new-instance v3, LX/3d2;

    const-class v4, LX/16f;

    new-instance v5, LX/4VK;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {v5, p0, v0}, LX/4VK;-><init>(LX/4VL;Z)V

    invoke-direct {v3, v4, v5}, LX/3d2;-><init>(Ljava/lang/Class;LX/3d4;)V

    .line 742272
    invoke-virtual {v3, p1}, LX/3d2;->a(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 742273
    goto :goto_0

    .line 742274
    :catch_0
    move-exception v0

    .line 742275
    const-string v3, "GraphQLMutatingVisitorAdapter"

    const-string v4, "Exception in visitor: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, LX/4VL;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-static {v3, v0, v4, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 742276
    throw v0
.end method

.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 742277
    iget-object v0, p0, LX/4VL;->c:LX/0Rf;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 742278
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 742279
    const-string v0, "GraphQLMutatingVisitorAdapter["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 742280
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, LX/4VL;->b:[LX/4VT;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 742281
    if-eqz v0, :cond_0

    .line 742282
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 742283
    :cond_0
    iget-object v2, p0, LX/4VL;->b:[LX/4VT;

    aget-object v2, v2, v0

    invoke-interface {v2}, LX/4VT;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 742284
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 742285
    :cond_1
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 742286
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
