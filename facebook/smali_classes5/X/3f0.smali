.class public LX/3f0;
.super LX/0SQ;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0SQ",
        "<",
        "LX/2YS;",
        ">;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 621187
    invoke-direct {p0}, LX/0SQ;-><init>()V

    .line 621188
    iput-object p1, p0, LX/3f0;->a:Ljava/lang/Class;

    .line 621189
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 621190
    new-instance v0, LX/2YS;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/2YS;-><init>(Z)V

    invoke-virtual {p0, v0}, LX/0SQ;->set(Ljava/lang/Object;)Z

    .line 621191
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 621192
    new-instance v0, LX/2YS;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/2YS;-><init>(Z)V

    invoke-virtual {p0, v0}, LX/0SQ;->set(Ljava/lang/Object;)Z

    .line 621193
    return-void
.end method
