.class public LX/4g7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/4g7;


# instance fields
.field private a:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final b:LX/2oA;


# direct methods
.method public constructor <init>(LX/2oA;)V
    .locals 7
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 799007
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799008
    iput-object p1, p0, LX/4g7;->b:LX/2oA;

    .line 799009
    new-instance v0, Ljava/util/EnumMap;

    const-class v2, Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;

    invoke-direct {v0, v2}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, LX/4g7;->a:Ljava/util/EnumMap;

    .line 799010
    invoke-static {}, Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;->values()[Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 799011
    iget-object v5, p0, LX/4g7;->a:Ljava/util/EnumMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 799012
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 799013
    :cond_0
    return-void
.end method

.method public static a(LX/0QB;)LX/4g7;
    .locals 4

    .prologue
    .line 799014
    sget-object v0, LX/4g7;->c:LX/4g7;

    if-nez v0, :cond_1

    .line 799015
    const-class v1, LX/4g7;

    monitor-enter v1

    .line 799016
    :try_start_0
    sget-object v0, LX/4g7;->c:LX/4g7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 799017
    if-eqz v2, :cond_0

    .line 799018
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 799019
    new-instance p0, LX/4g7;

    invoke-static {v0}, LX/2oA;->b(LX/0QB;)LX/2oA;

    move-result-object v3

    check-cast v3, LX/2oA;

    invoke-direct {p0, v3}, LX/4g7;-><init>(LX/2oA;)V

    .line 799020
    move-object v0, p0

    .line 799021
    sput-object v0, LX/4g7;->c:LX/4g7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 799022
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 799023
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 799024
    :cond_1
    sget-object v0, LX/4g7;->c:LX/4g7;

    return-object v0

    .line 799025
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 799026
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;)V
    .locals 2
    .param p1    # Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 799027
    monitor-enter p0

    if-nez p1, :cond_0

    .line 799028
    :goto_0
    monitor-exit p0

    return-void

    .line 799029
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/4g7;->a:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 799030
    iget-object v1, p0, LX/4g7;->a:Ljava/util/EnumMap;

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 799031
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;)V
    .locals 3
    .param p1    # Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 799032
    monitor-enter p0

    if-nez p1, :cond_0

    .line 799033
    :goto_0
    monitor-exit p0

    return-void

    .line 799034
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/4g7;->a:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 799035
    add-int/lit8 v0, v0, -0x1

    if-gez v0, :cond_1

    .line 799036
    invoke-virtual {p1}, Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;->name()Ljava/lang/String;

    .line 799037
    iget-object v0, p0, LX/4g7;->b:LX/2oA;

    const-class v1, LX/4g7;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mDialogDisplayMap contained negative value for context"

    invoke-virtual {v0, v1, v2}, LX/2oA;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 799038
    const/4 v0, 0x0

    .line 799039
    :cond_1
    iget-object v1, p0, LX/4g7;->a:Ljava/util/EnumMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 799040
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
