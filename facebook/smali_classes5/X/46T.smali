.class public LX/46T;
.super LX/46S;
.source ""


# instance fields
.field public a:Landroid/os/PersistableBundle;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 671219
    invoke-direct {p0}, LX/46S;-><init>()V

    .line 671220
    new-instance v0, Landroid/os/PersistableBundle;

    invoke-direct {v0}, Landroid/os/PersistableBundle;-><init>()V

    iput-object v0, p0, LX/46T;->a:Landroid/os/PersistableBundle;

    .line 671221
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 671222
    iget-object v0, p0, LX/46T;->a:Landroid/os/PersistableBundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/PersistableBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 671223
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 671224
    instance-of v0, p1, LX/46T;

    if-nez v0, :cond_0

    .line 671225
    const/4 v0, 0x0

    .line 671226
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/46T;->a:Landroid/os/PersistableBundle;

    check-cast p1, LX/46T;

    iget-object v1, p1, LX/46T;->a:Landroid/os/PersistableBundle;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 671227
    iget-object v0, p0, LX/46T;->a:Landroid/os/PersistableBundle;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 671228
    iget-object v0, p0, LX/46T;->a:Landroid/os/PersistableBundle;

    invoke-virtual {v0}, Landroid/os/PersistableBundle;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
