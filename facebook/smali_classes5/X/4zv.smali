.class public final LX/4zv;
.super LX/4zu;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/4zu",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4zw;


# direct methods
.method public constructor <init>(LX/4zw;Ljava/util/NavigableMap;)V
    .locals 0

    .prologue
    .line 823140
    iput-object p1, p0, LX/4zv;->a:LX/4zw;

    invoke-direct {p0, p2}, LX/4zu;-><init>(Ljava/util/NavigableMap;)V

    return-void
.end method


# virtual methods
.method public final removeAll(Ljava/util/Collection;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 823139
    iget-object v0, p0, LX/4zv;->a:LX/4zw;

    iget-object v0, v0, LX/4zw;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iget-object v1, p0, LX/4zv;->a:LX/4zw;

    iget-object v1, v1, LX/4zw;->b:LX/0Rl;

    invoke-static {p1}, LX/0Rj;->in(Ljava/util/Collection;)LX/0Rl;

    move-result-object v2

    invoke-static {v2}, LX/0PM;->a(LX/0Rl;)LX/0Rl;

    move-result-object v2

    invoke-static {v1, v2}, LX/0Rj;->and(LX/0Rl;LX/0Rl;)LX/0Rl;

    move-result-object v1

    invoke-static {v0, v1}, LX/0RZ;->a(Ljava/util/Iterator;LX/0Rl;)Z

    move-result v0

    return v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 823138
    iget-object v0, p0, LX/4zv;->a:LX/4zw;

    iget-object v0, v0, LX/4zw;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iget-object v1, p0, LX/4zv;->a:LX/4zw;

    iget-object v1, v1, LX/4zw;->b:LX/0Rl;

    invoke-static {p1}, LX/0Rj;->in(Ljava/util/Collection;)LX/0Rl;

    move-result-object v2

    invoke-static {v2}, LX/0Rj;->not(LX/0Rl;)LX/0Rl;

    move-result-object v2

    invoke-static {v2}, LX/0PM;->a(LX/0Rl;)LX/0Rl;

    move-result-object v2

    invoke-static {v1, v2}, LX/0Rj;->and(LX/0Rl;LX/0Rl;)LX/0Rl;

    move-result-object v1

    invoke-static {v0, v1}, LX/0RZ;->a(Ljava/util/Iterator;LX/0Rl;)Z

    move-result v0

    return v0
.end method
