.class public final LX/4J3;
.super LX/0gS;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 679766
    invoke-direct {p0}, LX/0gS;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/4J3;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 679769
    const-string v0, "surface"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679770
    return-object p0
.end method

.method public final a(Ljava/util/List;)LX/4J3;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/4J3;"
        }
    .end annotation

    .prologue
    .line 679767
    const-string v0, "unit_styles"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 679768
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/4J3;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionRequestTypeValue;
        .end annotation
    .end param

    .prologue
    .line 679762
    const-string v0, "request_type"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679763
    return-object p0
.end method

.method public final b(Ljava/util/List;)LX/4J3;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/4J3;"
        }
    .end annotation

    .prologue
    .line 679764
    const-string v0, "action_styles"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 679765
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/4J3;
    .locals 1

    .prologue
    .line 679760
    const-string v0, "session_id"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679761
    return-object p0
.end method

.method public final c(Ljava/util/List;)LX/4J3;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/4J3;"
        }
    .end annotation

    .prologue
    .line 679758
    const-string v0, "component_styles"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 679759
    return-object p0
.end method

.method public final d(Ljava/util/List;)LX/4J3;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/4J3;"
        }
    .end annotation

    .prologue
    .line 679756
    const-string v0, "story_attachment_styles"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 679757
    return-object p0
.end method

.method public final e(Ljava/util/List;)LX/4J3;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/4J3;"
        }
    .end annotation

    .prologue
    .line 679754
    const-string v0, "story_header_styles"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 679755
    return-object p0
.end method

.method public final f(Ljava/util/List;)LX/4J3;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/4J3;"
        }
    .end annotation

    .prologue
    .line 679752
    const-string v0, "client_capabilities"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 679753
    return-object p0
.end method
