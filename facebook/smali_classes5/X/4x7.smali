.class public abstract LX/4x7;
.super LX/306;
.source ""

# interfaces
.implements LX/1M1;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/306",
        "<TE;>;",
        "LX/1M1",
        "<TE;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 820788
    invoke-direct {p0}, LX/306;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 820795
    invoke-virtual {p0}, LX/4x7;->l()LX/1M1;

    move-result-object v0

    invoke-interface {v0, p1}, LX/1M1;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;I)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;I)I"
        }
    .end annotation

    .prologue
    .line 820794
    invoke-virtual {p0}, LX/4x7;->l()LX/1M1;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/1M1;->a(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;>;"
        }
    .end annotation

    .prologue
    .line 820793
    invoke-virtual {p0}, LX/4x7;->l()LX/1M1;

    move-result-object v0

    invoke-interface {v0}, LX/1M1;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;II)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;II)Z"
        }
    .end annotation

    .prologue
    .line 820792
    invoke-virtual {p0}, LX/4x7;->l()LX/1M1;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, LX/1M1;->a(Ljava/lang/Object;II)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;I)I
    .locals 1

    .prologue
    .line 820791
    invoke-virtual {p0}, LX/4x7;->l()LX/1M1;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/1M1;->b(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method public synthetic b()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 820790
    invoke-virtual {p0}, LX/4x7;->l()LX/1M1;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/util/Collection;)Z
    .locals 1
    .annotation build Lcom/google/common/annotations/Beta;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .prologue
    .line 820789
    invoke-static {p0, p1}, LX/2Tc;->a(LX/1M1;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final c(Ljava/lang/Object;I)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;I)I"
        }
    .end annotation

    .prologue
    .line 820796
    invoke-virtual {p0}, LX/4x7;->l()LX/1M1;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/1M1;->c(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method public final c(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 820781
    invoke-static {p0, p1}, LX/2Tc;->b(LX/1M1;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public d()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 820782
    invoke-virtual {p0}, LX/4x7;->l()LX/1M1;

    move-result-object v0

    invoke-interface {v0}, LX/1M1;->d()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 820783
    invoke-static {p0, p1}, LX/2Tc;->c(LX/1M1;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 820784
    invoke-virtual {p0}, LX/4x7;->l()LX/1M1;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 820785
    if-eq p1, p0, :cond_0

    invoke-virtual {p0}, LX/4x7;->l()LX/1M1;

    move-result-object v0

    invoke-interface {v0, p1}, LX/1M1;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 820786
    invoke-virtual {p0}, LX/4x7;->l()LX/1M1;

    move-result-object v0

    invoke-interface {v0}, LX/1M1;->hashCode()I

    move-result v0

    return v0
.end method

.method public abstract l()LX/1M1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1M1",
            "<TE;>;"
        }
    .end annotation
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 820787
    invoke-virtual {p0}, LX/4x7;->a()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
