.class public final LX/42t;
.super Landroid/os/Handler;
.source ""


# instance fields
.field private final a:LX/42u;

.field private volatile b:Z


# direct methods
.method public constructor <init>(LX/42u;Landroid/os/Looper;)V
    .locals 1

    .prologue
    .line 668221
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 668222
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/42u;

    iput-object v0, p0, LX/42t;->a:LX/42u;

    .line 668223
    return-void
.end method

.method public static declared-synchronized b(LX/42t;J)Z
    .locals 3

    .prologue
    .line 668217
    monitor-enter p0

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 668218
    :try_start_0
    invoke-static {p0, p1, p2}, LX/42t;->c(LX/42t;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 668219
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-static {p0}, LX/42t;->f(LX/42t;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 668220
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized c(LX/42t;J)Z
    .locals 7

    .prologue
    .line 668207
    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    add-long/2addr v0, p1

    .line 668208
    :goto_0
    iget-boolean v2, p0, LX/42t;->b:Z

    if-nez v2, :cond_1

    .line 668209
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    sub-long v2, v0, v2

    .line 668210
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-gtz v4, :cond_0

    .line 668211
    const/4 v0, 0x0

    .line 668212
    :goto_1
    monitor-exit p0

    return v0

    .line 668213
    :cond_0
    const v4, -0x1b3a1a48

    :try_start_1
    invoke-static {p0, v2, v3, v4}, LX/02L;->a(Ljava/lang/Object;JI)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 668214
    :catch_0
    goto :goto_0

    .line 668215
    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    .line 668216
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized e()V
    .locals 1

    .prologue
    .line 668203
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/42t;->b:Z

    .line 668204
    const v0, 0x604e388b

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 668205
    monitor-exit p0

    return-void

    .line 668206
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized f(LX/42t;)Z
    .locals 1

    .prologue
    .line 668184
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-boolean v0, p0, LX/42t;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 668185
    const v0, -0x3fc62d7d

    :try_start_1
    invoke-static {p0, v0}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 668186
    :catch_0
    goto :goto_0

    .line 668187
    :cond_0
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    .line 668188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;II)Z
    .locals 1

    .prologue
    .line 668202
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p2, p3, p1}, LX/42t;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/42t;->sendMessage(Landroid/os/Message;)Z

    move-result v0

    return v0
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 668189
    if-nez p1, :cond_0

    .line 668190
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Message is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 668191
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 668192
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Message what %d is not supported"

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 668193
    :pswitch_0
    iget-object v0, p0, LX/42t;->a:LX/42u;

    invoke-virtual {v0}, LX/42u;->a()V

    .line 668194
    :goto_0
    return-void

    .line 668195
    :pswitch_1
    iget-object v1, p0, LX/42t;->a:LX/42u;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {v1, v0}, LX/42u;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 668196
    :pswitch_2
    goto :goto_0

    .line 668197
    :pswitch_3
    goto :goto_0

    .line 668198
    :pswitch_4
    goto :goto_0

    .line 668199
    :pswitch_5
    goto :goto_0

    .line 668200
    :pswitch_6
    iget-object v0, p0, LX/42t;->a:LX/42u;

    invoke-virtual {v0}, LX/42u;->h()V

    .line 668201
    invoke-direct {p0}, LX/42t;->e()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
