.class public final LX/4zi;
.super LX/0qE;
.source ""

# interfaces
.implements LX/0qF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0qE",
        "<TK;TV;>;",
        "LX/0qF",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public volatile d:J

.field public e:LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public f:LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public g:LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public h:LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILX/0qF;)V
    .locals 2
    .param p4    # LX/0qF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TK;>;TK;I",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 823056
    invoke-direct {p0, p1, p2, p3, p4}, LX/0qE;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILX/0qF;)V

    .line 823057
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LX/4zi;->d:J

    .line 823058
    sget-object v0, LX/4zX;->INSTANCE:LX/4zX;

    move-object v0, v0

    .line 823059
    iput-object v0, p0, LX/4zi;->e:LX/0qF;

    .line 823060
    sget-object v0, LX/4zX;->INSTANCE:LX/4zX;

    move-object v0, v0

    .line 823061
    iput-object v0, p0, LX/4zi;->f:LX/0qF;

    .line 823062
    sget-object v0, LX/4zX;->INSTANCE:LX/4zX;

    move-object v0, v0

    .line 823063
    iput-object v0, p0, LX/4zi;->g:LX/0qF;

    .line 823064
    sget-object v0, LX/4zX;->INSTANCE:LX/4zX;

    move-object v0, v0

    .line 823065
    iput-object v0, p0, LX/4zi;->h:LX/0qF;

    .line 823066
    return-void
.end method


# virtual methods
.method public final getExpirationTime()J
    .locals 2

    .prologue
    .line 823055
    iget-wide v0, p0, LX/4zi;->d:J

    return-wide v0
.end method

.method public final getNextEvictable()LX/0qF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 823041
    iget-object v0, p0, LX/4zi;->g:LX/0qF;

    return-object v0
.end method

.method public final getNextExpirable()LX/0qF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 823054
    iget-object v0, p0, LX/4zi;->e:LX/0qF;

    return-object v0
.end method

.method public final getPreviousEvictable()LX/0qF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 823053
    iget-object v0, p0, LX/4zi;->h:LX/0qF;

    return-object v0
.end method

.method public final getPreviousExpirable()LX/0qF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 823052
    iget-object v0, p0, LX/4zi;->f:LX/0qF;

    return-object v0
.end method

.method public final setExpirationTime(J)V
    .locals 1

    .prologue
    .line 823050
    iput-wide p1, p0, LX/4zi;->d:J

    .line 823051
    return-void
.end method

.method public final setNextEvictable(LX/0qF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 823048
    iput-object p1, p0, LX/4zi;->g:LX/0qF;

    .line 823049
    return-void
.end method

.method public final setNextExpirable(LX/0qF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 823046
    iput-object p1, p0, LX/4zi;->e:LX/0qF;

    .line 823047
    return-void
.end method

.method public final setPreviousEvictable(LX/0qF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 823044
    iput-object p1, p0, LX/4zi;->h:LX/0qF;

    .line 823045
    return-void
.end method

.method public final setPreviousExpirable(LX/0qF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 823042
    iput-object p1, p0, LX/4zi;->f:LX/0qF;

    .line 823043
    return-void
.end method
