.class public LX/4Mg;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 689015
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 689016
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 689017
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 689018
    :goto_0
    return v1

    .line 689019
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_3

    .line 689020
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 689021
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 689022
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v9, :cond_0

    .line 689023
    const-string v10, "fetchTimeMs"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 689024
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v7

    goto :goto_1

    .line 689025
    :cond_1
    const-string v10, "isEmptyFeed"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 689026
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v6

    move v8, v6

    move v6, v7

    goto :goto_1

    .line 689027
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 689028
    :cond_3
    const/4 v9, 0x2

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 689029
    if-eqz v0, :cond_4

    move-object v0, p1

    .line 689030
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 689031
    :cond_4
    if-eqz v6, :cond_5

    .line 689032
    invoke-virtual {p1, v7, v8}, LX/186;->a(IZ)V

    .line 689033
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v6, v1

    move v0, v1

    move v8, v1

    move-wide v2, v4

    goto :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 689034
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 689035
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 689036
    invoke-static {p0, v2}, LX/4Mg;->a(LX/15w;LX/186;)I

    move-result v1

    .line 689037
    if-eqz v0, :cond_0

    .line 689038
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 689039
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 689040
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 689041
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 689042
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 689043
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 689044
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 689045
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 689046
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689047
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 689048
    const-string v0, "name"

    const-string v1, "FindGroupsFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 689049
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 689050
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 689051
    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 689052
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689053
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 689054
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 689055
    if-eqz v0, :cond_1

    .line 689056
    const-string v1, "isEmptyFeed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689057
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 689058
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 689059
    return-void
.end method
