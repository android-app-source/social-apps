.class public LX/3sQ;
.super LX/3sO;
.source ""


# instance fields
.field public b:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 643942
    invoke-direct {p0}, LX/3sO;-><init>()V

    .line 643943
    const/4 v0, 0x0

    iput-object v0, p0, LX/3sQ;->b:Ljava/util/WeakHashMap;

    .line 643944
    return-void
.end method


# virtual methods
.method public final a(LX/3sU;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 643918
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    invoke-virtual {p0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 643919
    return-void
.end method

.method public final a(LX/3sU;Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 643940
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    invoke-virtual {p0, p3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 643941
    return-void
.end method

.method public a(LX/3sU;Landroid/view/View;LX/3oQ;)V
    .locals 1

    .prologue
    .line 643934
    const/high16 v0, 0x7e000000

    invoke-virtual {p2, v0, p3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 643935
    new-instance v0, LX/3sP;

    invoke-direct {v0, p1}, LX/3sP;-><init>(LX/3sU;)V

    .line 643936
    if-eqz v0, :cond_0

    .line 643937
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    new-instance p1, LX/3sV;

    invoke-direct {p1, v0, p2}, LX/3sV;-><init>(LX/3oQ;Landroid/view/View;)V

    invoke-virtual {p0, p1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 643938
    :goto_0
    return-void

    .line 643939
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method public final a(Landroid/view/View;J)V
    .locals 0

    .prologue
    .line 643932
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    invoke-virtual {p0, p2, p3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 643933
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/animation/Interpolator;)V
    .locals 0

    .prologue
    .line 643930
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    invoke-virtual {p0, p2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 643931
    return-void
.end method

.method public final b(LX/3sU;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 643945
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    invoke-virtual {p0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 643946
    return-void
.end method

.method public final b(LX/3sU;Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 643928
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    invoke-virtual {p0, p3}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    .line 643929
    return-void
.end method

.method public final b(Landroid/view/View;J)V
    .locals 0

    .prologue
    .line 643926
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    invoke-virtual {p0, p2, p3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 643927
    return-void
.end method

.method public final c(LX/3sU;Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 643924
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    invoke-virtual {p0, p3}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 643925
    return-void
.end method

.method public final d(LX/3sU;Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 643922
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    invoke-virtual {p0, p3}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    .line 643923
    return-void
.end method

.method public final e(LX/3sU;Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 643920
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    invoke-virtual {p0, p3}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    .line 643921
    return-void
.end method
