.class public final LX/4bS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4bR;


# instance fields
.field public final synthetic a:LX/4bU;


# direct methods
.method public constructor <init>(LX/4bU;)V
    .locals 0

    .prologue
    .line 793724
    iput-object p1, p0, LX/4bS;->a:LX/4bU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/4bV;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/15D",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 793725
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4bV;

    .line 793726
    iget-object v2, p0, LX/4bS;->a:LX/4bU;

    iget-object v2, v2, LX/4bU;->c:LX/4bW;

    iget-object v3, v0, LX/4bV;->c:LX/15D;

    invoke-virtual {v2, v3}, LX/4bW;->a(LX/15D;)LX/4bV;

    .line 793727
    iget-object v2, v0, LX/4bV;->a:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Aborted request for: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, LX/4bV;->c:LX/15D;

    invoke-static {v0}, Lcom/facebook/http/common/FbHttpUtils;->b(LX/15D;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0

    .line 793728
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15D;

    .line 793729
    iget-object v2, v0, LX/15D;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    move-object v0, v2

    .line 793730
    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    goto :goto_1

    .line 793731
    :cond_1
    return-void
.end method
