.class public final LX/4W8;
.super LX/0ur;
.source ""


# instance fields
.field public A:J

.field public B:Lcom/facebook/graphql/model/GraphQLEventCategoryData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:I

.field public G:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Lcom/facebook/graphql/model/GraphQLEventCategoryData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Lcom/facebook/graphql/model/GraphQLEventHostsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

.field public Q:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

.field public U:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

.field public V:Lcom/facebook/graphql/model/GraphQLEventTicketProvider;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Lcom/facebook/graphql/enums/GraphQLEventType;

.field public Y:I

.field public Z:I

.field public aA:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aB:Lcom/facebook/graphql/model/GraphQLLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aC:I

.field public aD:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aE:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aF:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aG:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public aH:Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aI:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aJ:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aK:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aL:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aM:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aN:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aO:I

.field public aP:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aQ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aR:Lcom/facebook/graphql/enums/GraphQLPlaceType;

.field public aS:Z

.field public aT:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aU:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aV:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aW:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aX:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aY:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aZ:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Lcom/facebook/graphql/model/GraphQLEventViewerCapability;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

.field public ac:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ae:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public af:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ag:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ah:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ai:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aj:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ak:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public al:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public am:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public an:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ao:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ap:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aq:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ar:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public as:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public at:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public au:Z

.field public av:Z

.field public aw:Z

.field public ax:Z

.field public ay:Z

.field public az:Z

.field public b:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

.field public bA:Z

.field public bB:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public bC:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

.field public bD:Lcom/facebook/graphql/enums/GraphQLSavedState;

.field public bE:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;",
            ">;"
        }
    .end annotation
.end field

.field public bF:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;",
            ">;"
        }
    .end annotation
.end field

.field public bG:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

.field public ba:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bb:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bc:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bd:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public be:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bf:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bg:Z

.field public bh:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bi:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bj:J

.field public bk:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bl:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bm:J

.field public bn:Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bo:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bp:Z

.field public bq:Z

.field public br:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bs:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bt:Lcom/facebook/graphql/model/GraphQLEventTimeRange;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bu:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bv:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bw:I

.field public bx:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public by:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bz:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

.field public c:Lcom/facebook/graphql/model/GraphQLAlbum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLInlineActivity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

.field public v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 748497
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 748498
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    iput-object v0, p0, LX/4W8;->b:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 748499
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    iput-object v0, p0, LX/4W8;->u:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 748500
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    iput-object v0, p0, LX/4W8;->P:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 748501
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    iput-object v0, p0, LX/4W8;->T:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 748502
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    iput-object v0, p0, LX/4W8;->U:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 748503
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventType;

    iput-object v0, p0, LX/4W8;->X:Lcom/facebook/graphql/enums/GraphQLEventType;

    .line 748504
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    iput-object v0, p0, LX/4W8;->ab:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    .line 748505
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    iput-object v0, p0, LX/4W8;->aR:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 748506
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iput-object v0, p0, LX/4W8;->bz:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 748507
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    iput-object v0, p0, LX/4W8;->bC:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    .line 748508
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, LX/4W8;->bD:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 748509
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iput-object v0, p0, LX/4W8;->bG:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 748510
    instance-of v0, p0, LX/4W8;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 748511
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLEvent;)LX/4W8;
    .locals 4

    .prologue
    .line 748512
    new-instance v0, LX/4W8;

    invoke-direct {v0}, LX/4W8;-><init>()V

    .line 748513
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 748514
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->j()Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->b:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 748515
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->k()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->c:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 748516
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->l()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->d:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 748517
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->m()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->e:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 748518
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->n()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->f:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 748519
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->o()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->g:LX/0Px;

    .line 748520
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->p()Lcom/facebook/graphql/model/GraphQLInlineActivity;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->h:Lcom/facebook/graphql/model/GraphQLInlineActivity;

    .line 748521
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->q()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->i:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 748522
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->r()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->j:Lcom/facebook/graphql/model/GraphQLStory;

    .line 748523
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->s()Z

    move-result v1

    iput-boolean v1, v0, LX/4W8;->k:Z

    .line 748524
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->t()Z

    move-result v1

    iput-boolean v1, v0, LX/4W8;->l:Z

    .line 748525
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->u()Z

    move-result v1

    iput-boolean v1, v0, LX/4W8;->m:Z

    .line 748526
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->v()Z

    move-result v1

    iput-boolean v1, v0, LX/4W8;->n:Z

    .line 748527
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->w()Z

    move-result v1

    iput-boolean v1, v0, LX/4W8;->o:Z

    .line 748528
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->x()Z

    move-result v1

    iput-boolean v1, v0, LX/4W8;->p:Z

    .line 748529
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->y()Z

    move-result v1

    iput-boolean v1, v0, LX/4W8;->q:Z

    .line 748530
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->z()Z

    move-result v1

    iput-boolean v1, v0, LX/4W8;->r:Z

    .line 748531
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->A()Z

    move-result v1

    iput-boolean v1, v0, LX/4W8;->s:Z

    .line 748532
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->B()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->t:Lcom/facebook/graphql/model/GraphQLImage;

    .line 748533
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->C()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->u:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 748534
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->D()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->v:Ljava/lang/String;

    .line 748535
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->E()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->w:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 748536
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->F()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->x:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 748537
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->G()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->y:Lcom/facebook/graphql/model/GraphQLStory;

    .line 748538
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->H()J

    move-result-wide v2

    iput-wide v2, v0, LX/4W8;->z:J

    .line 748539
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->I()J

    move-result-wide v2

    iput-wide v2, v0, LX/4W8;->A:J

    .line 748540
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->J()Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->B:Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    .line 748541
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->C:Lcom/facebook/graphql/model/GraphQLImage;

    .line 748542
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->L()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 748543
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->M()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->E:Ljava/lang/String;

    .line 748544
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bI()I

    move-result v1

    iput v1, v0, LX/4W8;->F:I

    .line 748545
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->N()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->G:Ljava/lang/String;

    .line 748546
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->O()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->H:Ljava/lang/String;

    .line 748547
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->P()Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->I:Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    .line 748548
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->Q()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->J:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 748549
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->R()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->K:Lcom/facebook/graphql/model/GraphQLActor;

    .line 748550
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->S()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->L:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 748551
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->T()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->M:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 748552
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->U()Lcom/facebook/graphql/model/GraphQLEventHostsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->N:Lcom/facebook/graphql/model/GraphQLEventHostsConnection;

    .line 748553
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->V()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->O:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 748554
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->W()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->P:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 748555
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->X()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->Q:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 748556
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->Y()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->R:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 748557
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->Z()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->S:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 748558
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aa()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->T:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 748559
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ab()Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->U:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 748560
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ac()Lcom/facebook/graphql/model/GraphQLEventTicketProvider;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->V:Lcom/facebook/graphql/model/GraphQLEventTicketProvider;

    .line 748561
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ad()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->W:Ljava/lang/String;

    .line 748562
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ae()Lcom/facebook/graphql/enums/GraphQLEventType;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->X:Lcom/facebook/graphql/enums/GraphQLEventType;

    .line 748563
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->af()I

    move-result v1

    iput v1, v0, LX/4W8;->Y:I

    .line 748564
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ag()I

    move-result v1

    iput v1, v0, LX/4W8;->Z:I

    .line 748565
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ah()Lcom/facebook/graphql/model/GraphQLEventViewerCapability;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aa:Lcom/facebook/graphql/model/GraphQLEventViewerCapability;

    .line 748566
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ai()Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->ab:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    .line 748567
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aj()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->ac:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 748568
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ak()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->ad:Lcom/facebook/graphql/model/GraphQLImage;

    .line 748569
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->al()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->ae:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 748570
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->am()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->af:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 748571
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->an()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->ag:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 748572
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ao()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->ah:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 748573
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ap()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->ai:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 748574
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aq()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aj:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 748575
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bK()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->ak:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 748576
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ar()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->al:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 748577
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->as()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->am:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 748578
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bL()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->an:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 748579
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->at()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->ao:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 748580
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->au()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->ap:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 748581
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->av()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aq:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 748582
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aw()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->ar:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 748583
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->as:Ljava/lang/String;

    .line 748584
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ay()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->at:Lcom/facebook/graphql/model/GraphQLImage;

    .line 748585
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->az()Z

    move-result v1

    iput-boolean v1, v0, LX/4W8;->au:Z

    .line 748586
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aA()Z

    move-result v1

    iput-boolean v1, v0, LX/4W8;->av:Z

    .line 748587
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aB()Z

    move-result v1

    iput-boolean v1, v0, LX/4W8;->aw:Z

    .line 748588
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aC()Z

    move-result v1

    iput-boolean v1, v0, LX/4W8;->ax:Z

    .line 748589
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aD()Z

    move-result v1

    iput-boolean v1, v0, LX/4W8;->ay:Z

    .line 748590
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aE()Z

    move-result v1

    iput-boolean v1, v0, LX/4W8;->az:Z

    .line 748591
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aF()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aA:Ljava/lang/String;

    .line 748592
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aG()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aB:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 748593
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aH()I

    move-result v1

    iput v1, v0, LX/4W8;->aC:I

    .line 748594
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bG()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aD:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 748595
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bH()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aE:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 748596
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aI()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aF:Ljava/lang/String;

    .line 748597
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aJ()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aG:LX/0Px;

    .line 748598
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aK()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aH:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 748599
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aL()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aI:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 748600
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aM()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aJ:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 748601
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aN()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aK:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 748602
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aO()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aL:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 748603
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aP()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aM:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 748604
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aQ()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aN:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 748605
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aR()I

    move-result v1

    iput v1, v0, LX/4W8;->aO:I

    .line 748606
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aS()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aP:Lcom/facebook/graphql/model/GraphQLImage;

    .line 748607
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aT()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aQ:Ljava/lang/String;

    .line 748608
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aU()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aR:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 748609
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aV()Z

    move-result v1

    iput-boolean v1, v0, LX/4W8;->aS:Z

    .line 748610
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aW()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aT:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 748611
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aX()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aU:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 748612
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aY()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aV:Lcom/facebook/graphql/model/GraphQLImage;

    .line 748613
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aZ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aW:Lcom/facebook/graphql/model/GraphQLImage;

    .line 748614
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bM()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aX:Lcom/facebook/graphql/model/GraphQLImage;

    .line 748615
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ba()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aY:Lcom/facebook/graphql/model/GraphQLImage;

    .line 748616
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bb()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->aZ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 748617
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bc()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->ba:Lcom/facebook/graphql/model/GraphQLImage;

    .line 748618
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bd()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->bb:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 748619
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->be()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->bc:Lcom/facebook/graphql/model/GraphQLImage;

    .line 748620
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bf()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->bd:Lcom/facebook/graphql/model/GraphQLImage;

    .line 748621
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bg()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->be:Lcom/facebook/graphql/model/GraphQLImage;

    .line 748622
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bh()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->bf:Lcom/facebook/graphql/model/GraphQLImage;

    .line 748623
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bi()Z

    move-result v1

    iput-boolean v1, v0, LX/4W8;->bg:Z

    .line 748624
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bN()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->bh:Lcom/facebook/graphql/model/GraphQLImage;

    .line 748625
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bj()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->bi:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 748626
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bk()J

    move-result-wide v2

    iput-wide v2, v0, LX/4W8;->bj:J

    .line 748627
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bl()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->bk:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 748628
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bm()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->bl:Ljava/lang/String;

    .line 748629
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bn()J

    move-result-wide v2

    iput-wide v2, v0, LX/4W8;->bm:J

    .line 748630
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bo()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->bn:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 748631
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bp()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->bo:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 748632
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bq()Z

    move-result v1

    iput-boolean v1, v0, LX/4W8;->bp:Z

    .line 748633
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bJ()Z

    move-result v1

    iput-boolean v1, v0, LX/4W8;->bq:Z

    .line 748634
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->br()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->br:Lcom/facebook/graphql/model/GraphQLImage;

    .line 748635
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bO()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->bs:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 748636
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bs()Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->bt:Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    .line 748637
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bt()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->bu:Ljava/lang/String;

    .line 748638
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bu()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->bv:Ljava/lang/String;

    .line 748639
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bv()I

    move-result v1

    iput v1, v0, LX/4W8;->bw:I

    .line 748640
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bw()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->bx:Ljava/lang/String;

    .line 748641
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bx()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->by:Ljava/lang/String;

    .line 748642
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->by()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->bz:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 748643
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bz()Z

    move-result v1

    iput-boolean v1, v0, LX/4W8;->bA:Z

    .line 748644
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bA()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->bB:LX/0Px;

    .line 748645
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bB()Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->bC:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    .line 748646
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bC()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->bD:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 748647
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bD()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->bE:LX/0Px;

    .line 748648
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bE()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->bF:LX/0Px;

    .line 748649
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bF()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v1

    iput-object v1, v0, LX/4W8;->bG:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 748650
    invoke-static {v0, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 748651
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLEvent;
    .locals 2

    .prologue
    .line 748652
    new-instance v0, Lcom/facebook/graphql/model/GraphQLEvent;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLEvent;-><init>(LX/4W8;)V

    .line 748653
    return-object v0
.end method
