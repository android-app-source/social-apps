.class public LX/4le;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Iv;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/4le;


# instance fields
.field private a:LX/03V;


# direct methods
.method public constructor <init>(LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 804593
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 804594
    iput-object p1, p0, LX/4le;->a:LX/03V;

    .line 804595
    return-void
.end method

.method public static a(LX/0QB;)LX/4le;
    .locals 4

    .prologue
    .line 804596
    sget-object v0, LX/4le;->b:LX/4le;

    if-nez v0, :cond_1

    .line 804597
    const-class v1, LX/4le;

    monitor-enter v1

    .line 804598
    :try_start_0
    sget-object v0, LX/4le;->b:LX/4le;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 804599
    if-eqz v2, :cond_0

    .line 804600
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 804601
    new-instance p0, LX/4le;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {p0, v3}, LX/4le;-><init>(LX/03V;)V

    .line 804602
    move-object v0, p0

    .line 804603
    sput-object v0, LX/4le;->b:LX/4le;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 804604
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 804605
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 804606
    :cond_1
    sget-object v0, LX/4le;->b:LX/4le;

    return-object v0

    .line 804607
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 804608
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 804609
    iget-object v0, p0, LX/4le;->a:LX/03V;

    const-string v1, "BadVerifyInvocationNotifierImpl"

    const-string v2, "Bad version of FbHostnameVerifierAdaptor.verify invoked as verify(%s, %s, %s)"

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-static {v2, p1, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 804610
    return-void
.end method
