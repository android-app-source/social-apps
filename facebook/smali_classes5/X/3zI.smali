.class public final enum LX/3zI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3zI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3zI;

.field public static final enum LOCAL_DATA:LX/3zI;

.field public static final enum NETWORK_DATA:LX/3zI;

.field public static final enum NO_DATA:LX/3zI;

.field public static final enum UNDEFINED:LX/3zI;


# instance fields
.field private final mFlags:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 662352
    new-instance v0, LX/3zI;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1, v2, v2}, LX/3zI;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/3zI;->UNDEFINED:LX/3zI;

    .line 662353
    new-instance v0, LX/3zI;

    const-string v1, "NO_DATA"

    invoke-direct {v0, v1, v3, v3}, LX/3zI;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/3zI;->NO_DATA:LX/3zI;

    .line 662354
    new-instance v0, LX/3zI;

    const-string v1, "LOCAL_DATA"

    invoke-direct {v0, v1, v4, v4}, LX/3zI;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/3zI;->LOCAL_DATA:LX/3zI;

    .line 662355
    new-instance v0, LX/3zI;

    const-string v1, "NETWORK_DATA"

    invoke-direct {v0, v1, v5, v5}, LX/3zI;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/3zI;->NETWORK_DATA:LX/3zI;

    .line 662356
    const/4 v0, 0x4

    new-array v0, v0, [LX/3zI;

    sget-object v1, LX/3zI;->UNDEFINED:LX/3zI;

    aput-object v1, v0, v2

    sget-object v1, LX/3zI;->NO_DATA:LX/3zI;

    aput-object v1, v0, v3

    sget-object v1, LX/3zI;->LOCAL_DATA:LX/3zI;

    aput-object v1, v0, v4

    sget-object v1, LX/3zI;->NETWORK_DATA:LX/3zI;

    aput-object v1, v0, v5

    sput-object v0, LX/3zI;->$VALUES:[LX/3zI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 662346
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 662347
    iput p3, p0, LX/3zI;->mFlags:I

    .line 662348
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3zI;
    .locals 1

    .prologue
    .line 662351
    const-class v0, LX/3zI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3zI;

    return-object v0
.end method

.method public static values()[LX/3zI;
    .locals 1

    .prologue
    .line 662350
    sget-object v0, LX/3zI;->$VALUES:[LX/3zI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3zI;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    .prologue
    .line 662349
    iget v0, p0, LX/3zI;->mFlags:I

    return v0
.end method
