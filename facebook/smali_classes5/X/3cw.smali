.class public LX/3cw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/notifications/server/FetchNotificationSeenStatesParams;",
        "Lcom/facebook/notifications/protocol/methods/FetchNotificationSeenStatesResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0So;


# direct methods
.method public constructor <init>(LX/0So;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 615874
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 615875
    iput-object p1, p0, LX/3cw;->a:LX/0So;

    .line 615876
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 615877
    check-cast p1, Lcom/facebook/notifications/server/FetchNotificationSeenStatesParams;

    .line 615878
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 615879
    iget-object v0, p1, Lcom/facebook/notifications/server/FetchNotificationSeenStatesParams;->a:LX/0Px;

    move-object v2, v0

    .line 615880
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 615881
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "notif_ids[]"

    invoke-direct {v5, v6, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 615882
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 615883
    :cond_0
    new-instance v0, LX/14N;

    sget-object v1, LX/11I;->NOTIFICATION_GET_SEEN_STATES:LX/11I;

    iget-object v1, v1, LX/11I;->requestNameString:Ljava/lang/String;

    const-string v2, "GET"

    const-string v3, "me/notification_seen_states"

    sget-object v5, LX/14S;->JSONPARSER:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 615884
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    const-class v1, Lcom/facebook/notifications/model/NotificationSeenStates;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/model/NotificationSeenStates;

    .line 615885
    new-instance v1, Lcom/facebook/notifications/protocol/methods/FetchNotificationSeenStatesResult;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v3, p0, LX/3cw;->a:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    invoke-direct {v1, v2, v0, v4, v5}, Lcom/facebook/notifications/protocol/methods/FetchNotificationSeenStatesResult;-><init>(LX/0ta;Lcom/facebook/notifications/model/NotificationSeenStates;J)V

    return-object v1
.end method
