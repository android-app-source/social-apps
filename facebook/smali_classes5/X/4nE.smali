.class public LX/4nE;
.super LX/1La;
.source ""


# instance fields
.field public a:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 806653
    invoke-direct {p0}, LX/1La;-><init>()V

    .line 806654
    const/4 v0, -0x1

    iput v0, p0, LX/4nE;->a:I

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/Fragment;Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 806645
    check-cast p1, Lcom/facebook/base/fragment/FbFragment;

    .line 806646
    iget p2, p0, LX/4nE;->a:I

    const/4 p3, -0x1

    if-ne p2, p3, :cond_0

    const/4 p2, 0x1

    :goto_0
    const-string p3, "Previous soft input mode was never reset!"

    invoke-static {p2, p3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 806647
    invoke-virtual {p1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object p2

    invoke-virtual {p2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p2

    .line 806648
    invoke-virtual {p2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object p3

    iget p3, p3, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    iput p3, p0, LX/4nE;->a:I

    .line 806649
    iget p3, p0, LX/4nE;->a:I

    and-int/lit16 p3, p3, -0xf1

    .line 806650
    or-int/lit8 p3, p3, 0x20

    invoke-virtual {p2, p3}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 806651
    return-void

    .line 806652
    :cond_0
    const/4 p2, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/support/v4/app/Fragment;)V
    .locals 3

    .prologue
    .line 806639
    check-cast p1, Lcom/facebook/base/fragment/FbFragment;

    const/4 v2, -0x1

    .line 806640
    iget v0, p0, LX/4nE;->a:I

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Previous soft input mode was never recorded!"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 806641
    invoke-virtual {p1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget v1, p0, LX/4nE;->a:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 806642
    iput v2, p0, LX/4nE;->a:I

    .line 806643
    return-void

    .line 806644
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
