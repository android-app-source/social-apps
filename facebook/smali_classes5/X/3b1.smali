.class public LX/3b1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 608898
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 608899
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 608900
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 608901
    if-eqz v0, :cond_0

    .line 608902
    const-string v1, "length"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 608903
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 608904
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 608905
    if-eqz v0, :cond_1

    .line 608906
    const-string v1, "offset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 608907
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 608908
    :cond_1
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 608909
    if-eqz v0, :cond_2

    .line 608910
    const-string v0, "part"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 608911
    const-class v0, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 608912
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 608913
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 608914
    const/4 v0, 0x0

    .line 608915
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_8

    .line 608916
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 608917
    :goto_0
    return v1

    .line 608918
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_4

    .line 608919
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 608920
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 608921
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_0

    if-eqz v8, :cond_0

    .line 608922
    const-string v9, "length"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 608923
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v7, v4

    move v4, v2

    goto :goto_1

    .line 608924
    :cond_1
    const-string v9, "offset"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 608925
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v6, v3

    move v3, v2

    goto :goto_1

    .line 608926
    :cond_2
    const-string v9, "part"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 608927
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    move-result-object v0

    move-object v5, v0

    move v0, v2

    goto :goto_1

    .line 608928
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 608929
    :cond_4
    const/4 v8, 0x3

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 608930
    if-eqz v4, :cond_5

    .line 608931
    invoke-virtual {p1, v1, v7, v1}, LX/186;->a(III)V

    .line 608932
    :cond_5
    if-eqz v3, :cond_6

    .line 608933
    invoke-virtual {p1, v2, v6, v1}, LX/186;->a(III)V

    .line 608934
    :cond_6
    if-eqz v0, :cond_7

    .line 608935
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v5}, LX/186;->a(ILjava/lang/Enum;)V

    .line 608936
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v3, v1

    move v4, v1

    move-object v5, v0

    move v6, v1

    move v7, v1

    move v0, v1

    goto :goto_1
.end method
