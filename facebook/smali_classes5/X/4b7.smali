.class public final enum LX/4b7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4b7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4b7;

.field public static final enum ARRIVE:LX/4b7;

.field public static final enum CANCEL:LX/4b7;

.field public static final enum CANCELLED:LX/4b7;

.field public static final enum DROP:LX/4b7;

.field public static final enum FAILED:LX/4b7;

.field public static final enum FINISHED:LX/4b7;

.field public static final enum NO_CHANGE:LX/4b7;

.field public static final enum PRIORITY:LX/4b7;

.field public static final enum START:LX/4b7;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 793339
    new-instance v0, LX/4b7;

    const-string v1, "ARRIVE"

    invoke-direct {v0, v1, v3}, LX/4b7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4b7;->ARRIVE:LX/4b7;

    .line 793340
    new-instance v0, LX/4b7;

    const-string v1, "START"

    invoke-direct {v0, v1, v4}, LX/4b7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4b7;->START:LX/4b7;

    .line 793341
    new-instance v0, LX/4b7;

    const-string v1, "PRIORITY"

    invoke-direct {v0, v1, v5}, LX/4b7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4b7;->PRIORITY:LX/4b7;

    .line 793342
    new-instance v0, LX/4b7;

    const-string v1, "NO_CHANGE"

    invoke-direct {v0, v1, v6}, LX/4b7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4b7;->NO_CHANGE:LX/4b7;

    .line 793343
    new-instance v0, LX/4b7;

    const-string v1, "DROP"

    invoke-direct {v0, v1, v7}, LX/4b7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4b7;->DROP:LX/4b7;

    .line 793344
    new-instance v0, LX/4b7;

    const-string v1, "CANCEL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/4b7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4b7;->CANCEL:LX/4b7;

    .line 793345
    new-instance v0, LX/4b7;

    const-string v1, "CANCELLED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/4b7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4b7;->CANCELLED:LX/4b7;

    .line 793346
    new-instance v0, LX/4b7;

    const-string v1, "FINISHED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/4b7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4b7;->FINISHED:LX/4b7;

    .line 793347
    new-instance v0, LX/4b7;

    const-string v1, "FAILED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/4b7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4b7;->FAILED:LX/4b7;

    .line 793348
    const/16 v0, 0x9

    new-array v0, v0, [LX/4b7;

    sget-object v1, LX/4b7;->ARRIVE:LX/4b7;

    aput-object v1, v0, v3

    sget-object v1, LX/4b7;->START:LX/4b7;

    aput-object v1, v0, v4

    sget-object v1, LX/4b7;->PRIORITY:LX/4b7;

    aput-object v1, v0, v5

    sget-object v1, LX/4b7;->NO_CHANGE:LX/4b7;

    aput-object v1, v0, v6

    sget-object v1, LX/4b7;->DROP:LX/4b7;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/4b7;->CANCEL:LX/4b7;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/4b7;->CANCELLED:LX/4b7;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/4b7;->FINISHED:LX/4b7;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/4b7;->FAILED:LX/4b7;

    aput-object v2, v0, v1

    sput-object v0, LX/4b7;->$VALUES:[LX/4b7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 793349
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4b7;
    .locals 1

    .prologue
    .line 793350
    const-class v0, LX/4b7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4b7;

    return-object v0
.end method

.method public static values()[LX/4b7;
    .locals 1

    .prologue
    .line 793351
    sget-object v0, LX/4b7;->$VALUES:[LX/4b7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4b7;

    return-object v0
.end method
