.class public LX/4hw;
.super LX/4hr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/4hr",
        "<",
        "LX/4hv;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final c:LX/25w;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 802194
    const-class v0, LX/4hw;

    sput-object v0, LX/4hw;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/25w;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/4hv;",
            ">;",
            "Lcom/facebook/platform/common/config/PlatformConfig;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 802195
    const v0, 0x10002

    invoke-direct {p0, p1, v0}, LX/4hr;-><init>(LX/0Or;I)V

    .line 802196
    iput-object p2, p0, LX/4hw;->c:LX/25w;

    .line 802197
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Message;LX/4ht;)V
    .locals 4

    .prologue
    .line 802198
    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v0

    .line 802199
    iget v1, p1, Landroid/os/Message;->arg1:I

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 802200
    iget v1, p1, Landroid/os/Message;->arg2:I

    iput v1, v0, Landroid/os/Message;->arg2:I

    .line 802201
    const v1, 0x10003

    iput v1, v0, Landroid/os/Message;->what:I

    .line 802202
    move-object v0, v0

    .line 802203
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 802204
    const-string v2, "com.facebook.platform.extra.PROTOCOL_VERSIONS"

    .line 802205
    sget-object p0, LX/25y;->a:Ljava/util/List;

    move-object v3, p0

    .line 802206
    invoke-static {v3}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 802207
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 802208
    :try_start_0
    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 802209
    :goto_0
    return-void

    .line 802210
    :catch_0
    move-exception v0

    .line 802211
    sget-object v1, LX/4hw;->b:Ljava/lang/Class;

    const-string v2, "Unable to respond to protocol version request"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
