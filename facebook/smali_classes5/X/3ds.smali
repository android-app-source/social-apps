.class public LX/3ds;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# instance fields
.field public final a:LX/3dt;

.field private final b:LX/3dw;

.field private final c:LX/3eJ;

.field public final d:Lcom/facebook/stickers/service/StickerPacksHandler;

.field public final e:LX/3eX;

.field public final f:LX/3eU;

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8CH;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3eh;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/stickers/service/StickerAssetsHandler;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3dt;LX/3dw;LX/3eJ;Lcom/facebook/stickers/service/StickerPacksHandler;LX/3eX;LX/3eU;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 617879
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 617880
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 617881
    iput-object v0, p0, LX/3ds;->g:LX/0Ot;

    .line 617882
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 617883
    iput-object v0, p0, LX/3ds;->h:LX/0Ot;

    .line 617884
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 617885
    iput-object v0, p0, LX/3ds;->i:LX/0Ot;

    .line 617886
    iput-object p1, p0, LX/3ds;->a:LX/3dt;

    .line 617887
    iput-object p2, p0, LX/3ds;->b:LX/3dw;

    .line 617888
    iput-object p3, p0, LX/3ds;->c:LX/3eJ;

    .line 617889
    iput-object p4, p0, LX/3ds;->d:Lcom/facebook/stickers/service/StickerPacksHandler;

    .line 617890
    iput-object p5, p0, LX/3ds;->e:LX/3eX;

    .line 617891
    iput-object p6, p0, LX/3ds;->f:LX/3eU;

    .line 617892
    return-void
.end method

.method private a()Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 617851
    iget-object v0, p0, LX/3ds;->b:LX/3dw;

    .line 617852
    iget-object v1, v0, LX/3dw;->a:LX/3dt;

    invoke-virtual {v1}, LX/3dt;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 617853
    iget-object v1, v0, LX/3dw;->a:LX/3dt;

    invoke-virtual {v1}, LX/3dt;->a()LX/0Px;

    move-result-object v1

    .line 617854
    :goto_0
    new-instance p0, Lcom/facebook/stickers/service/FetchRecentStickersResult;

    invoke-direct {p0, v1}, Lcom/facebook/stickers/service/FetchRecentStickersResult;-><init>(Ljava/util/List;)V

    move-object v0, p0

    .line 617855
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    .line 617856
    :cond_0
    iget-object v1, v0, LX/3dw;->b:LX/3dx;

    invoke-virtual {v1}, LX/3dx;->a()LX/0Px;

    move-result-object v1

    .line 617857
    iget-object p0, v0, LX/3dw;->a:LX/3dt;

    invoke-virtual {p0, v1}, LX/3dt;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method public static a(LX/3ds;LX/3do;Lcom/facebook/stickers/model/StickerPack;)V
    .locals 1

    .prologue
    .line 617848
    iget-object v0, p0, LX/3ds;->a:LX/3dt;

    invoke-virtual {v0, p1}, LX/3dt;->a(LX/3do;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 617849
    iget-object v0, p0, LX/3ds;->a:LX/3dt;

    invoke-virtual {v0, p1, p2}, LX/3dt;->a(LX/3do;Lcom/facebook/stickers/model/StickerPack;)V

    .line 617850
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/3ds;
    .locals 14

    .prologue
    .line 617835
    new-instance v0, LX/3ds;

    invoke-static {p0}, LX/3dt;->a(LX/0QB;)LX/3dt;

    move-result-object v1

    check-cast v1, LX/3dt;

    .line 617836
    new-instance v4, LX/3dw;

    invoke-static {p0}, LX/3dt;->a(LX/0QB;)LX/3dt;

    move-result-object v2

    check-cast v2, LX/3dt;

    invoke-static {p0}, LX/3dx;->a(LX/0QB;)LX/3dx;

    move-result-object v3

    check-cast v3, LX/3dx;

    invoke-direct {v4, v2, v3}, LX/3dw;-><init>(LX/3dt;LX/3dx;)V

    .line 617837
    move-object v2, v4

    .line 617838
    check-cast v2, LX/3dw;

    .line 617839
    new-instance v7, LX/3eJ;

    invoke-static {p0}, LX/3dt;->a(LX/0QB;)LX/3dt;

    move-result-object v8

    check-cast v8, LX/3dt;

    invoke-static {p0}, LX/3dx;->a(LX/0QB;)LX/3dx;

    move-result-object v9

    check-cast v9, LX/3dx;

    invoke-static {p0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v10

    check-cast v10, LX/18V;

    invoke-static {p0}, LX/3eK;->a(LX/0QB;)LX/3eK;

    move-result-object v11

    check-cast v11, LX/3eK;

    invoke-static {p0}, LX/3eN;->a(LX/0QB;)LX/3eN;

    move-result-object v12

    check-cast v12, LX/3eN;

    invoke-static {p0}, LX/3eO;->a(LX/0QB;)LX/3eO;

    move-result-object v13

    check-cast v13, LX/3eO;

    invoke-direct/range {v7 .. v13}, LX/3eJ;-><init>(LX/3dt;LX/3dx;LX/18V;LX/3eK;LX/3eN;LX/3eO;)V

    .line 617840
    move-object v3, v7

    .line 617841
    check-cast v3, LX/3eJ;

    invoke-static {p0}, Lcom/facebook/stickers/service/StickerPacksHandler;->b(LX/0QB;)Lcom/facebook/stickers/service/StickerPacksHandler;

    move-result-object v4

    check-cast v4, Lcom/facebook/stickers/service/StickerPacksHandler;

    .line 617842
    new-instance v9, LX/3eX;

    invoke-static {p0}, Lcom/facebook/stickers/service/StickerPacksHandler;->b(LX/0QB;)Lcom/facebook/stickers/service/StickerPacksHandler;

    move-result-object v5

    check-cast v5, Lcom/facebook/stickers/service/StickerPacksHandler;

    invoke-static {p0}, LX/3dt;->a(LX/0QB;)LX/3dt;

    move-result-object v6

    check-cast v6, LX/3dt;

    invoke-static {p0}, LX/3dx;->a(LX/0QB;)LX/3dx;

    move-result-object v7

    check-cast v7, LX/3dx;

    invoke-static {p0}, LX/3eY;->a(LX/0QB;)LX/3eY;

    move-result-object v8

    check-cast v8, LX/3eY;

    invoke-direct {v9, v5, v6, v7, v8}, LX/3eX;-><init>(Lcom/facebook/stickers/service/StickerPacksHandler;LX/3dt;LX/3dx;LX/3eY;)V

    .line 617843
    move-object v5, v9

    .line 617844
    check-cast v5, LX/3eX;

    invoke-static {p0}, LX/3eU;->a(LX/0QB;)LX/3eU;

    move-result-object v6

    check-cast v6, LX/3eU;

    invoke-direct/range {v0 .. v6}, LX/3ds;-><init>(LX/3dt;LX/3dw;LX/3eJ;Lcom/facebook/stickers/service/StickerPacksHandler;LX/3eX;LX/3eU;)V

    .line 617845
    const/16 v1, 0x27d7

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x1222

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x3598

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    .line 617846
    iput-object v1, v0, LX/3ds;->g:LX/0Ot;

    iput-object v2, v0, LX/3ds;->h:LX/0Ot;

    iput-object v3, v0, LX/3ds;->i:LX/0Ot;

    .line 617847
    return-object v0
.end method

.method private b()Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 617832
    iget-object v0, p0, LX/3ds;->e:LX/3eX;

    .line 617833
    new-instance v1, Lcom/facebook/stickers/service/FetchStickerPackIdsResult;

    iget-object p0, v0, LX/3eX;->c:LX/3dx;

    invoke-virtual {p0}, LX/3dx;->b()LX/0Px;

    move-result-object p0

    invoke-direct {v1, p0}, Lcom/facebook/stickers/service/FetchStickerPackIdsResult;-><init>(LX/0Px;)V

    move-object v0, v1

    .line 617834
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method private b(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 5

    .prologue
    .line 617801
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 617802
    const-string v1, "fetchStickerPacksParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickerPacksParams;

    .line 617803
    iget-object v1, v0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->a:LX/3do;

    move-object v3, v1

    .line 617804
    iget-object v1, v0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->b:LX/0rS;

    move-object v2, v1

    .line 617805
    iget-object v1, p0, LX/3ds;->a:LX/3dt;

    invoke-virtual {v1, v3}, LX/3dt;->a(LX/3do;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-ne v2, v1, :cond_3

    .line 617806
    :cond_0
    iget-object v1, p0, LX/3ds;->d:Lcom/facebook/stickers/service/StickerPacksHandler;

    invoke-virtual {v1, p1}, Lcom/facebook/stickers/service/StickerPacksHandler;->c(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    .line 617807
    iget-object v4, v1, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v4, v4

    .line 617808
    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v4

    if-nez v4, :cond_1

    .line 617809
    sget-object v0, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    if-eq v2, v0, :cond_2

    .line 617810
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Undefined sticker pack fetch results from server, this should never happen!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 617811
    :cond_1
    iget-object v4, p0, LX/3ds;->a:LX/3dt;

    .line 617812
    iget-object v2, v1, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v2, v2

    .line 617813
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-virtual {v4, v3, v2}, LX/3dt;->a(LX/3do;Ljava/util/List;)V

    .line 617814
    iget-object v2, v0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->h:LX/3eb;

    move-object v2, v2

    .line 617815
    sget-object v3, LX/3eb;->DO_NOT_UPDATE:LX/3eb;

    if-eq v2, v3, :cond_2

    .line 617816
    new-instance v2, LX/3ea;

    sget-object v3, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    sget-object v4, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    invoke-direct {v2, v3, v4}, LX/3ea;-><init>(LX/3do;LX/0rS;)V

    .line 617817
    iget-object v3, v0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->c:Ljava/lang/String;

    move-object v0, v3

    .line 617818
    iput-object v0, v2, LX/3ea;->c:Ljava/lang/String;

    .line 617819
    move-object v0, v2

    .line 617820
    invoke-virtual {v0}, LX/3ea;->a()Lcom/facebook/stickers/service/FetchStickerPacksParams;

    move-result-object v0

    .line 617821
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 617822
    const-string v3, "fetchStickerPacksParams"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 617823
    new-instance v0, LX/1qK;

    const-string v3, "fetch_sticker_packs"

    invoke-direct {v0, v3, v2}, LX/1qK;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 617824
    iget-object v2, p0, LX/3ds;->d:Lcom/facebook/stickers/service/StickerPacksHandler;

    invoke-virtual {v2, v0}, Lcom/facebook/stickers/service/StickerPacksHandler;->c(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    .line 617825
    iget-object v2, v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v2, v2

    .line 617826
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 617827
    iget-object v2, p0, LX/3ds;->a:LX/3dt;

    sget-object v3, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    .line 617828
    iget-object v4, v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v0, v4

    .line 617829
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v2, v3, v0}, LX/3dt;->a(LX/3do;Ljava/util/List;)V

    .line 617830
    :cond_2
    :goto_0
    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    .line 617831
    :cond_3
    new-instance v1, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    iget-object v0, p0, LX/3ds;->a:LX/3dt;

    invoke-virtual {v0, v3}, LX/3dt;->b(LX/3do;)LX/0Px;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/stickers/service/FetchStickerPacksResult;-><init>(Ljava/util/List;)V

    goto :goto_0
.end method

.method private c()Lcom/facebook/fbservice/service/OperationResult;
    .locals 11

    .prologue
    .line 617486
    iget-object v0, p0, LX/3ds;->e:LX/3eX;

    .line 617487
    iget-object v1, v0, LX/3eX;->b:LX/3dt;

    invoke-virtual {v1}, LX/3dt;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 617488
    new-instance v1, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    iget-object v2, v0, LX/3eX;->b:LX/3dt;

    invoke-virtual {v2}, LX/3dt;->c()LX/0Px;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/stickers/service/FetchStickerPacksResult;-><init>(Ljava/util/List;)V

    .line 617489
    :goto_0
    move-object v0, v1

    .line 617490
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    .line 617491
    :cond_0
    iget-object v1, v0, LX/3eX;->d:LX/3eY;

    .line 617492
    iget-object v7, v1, LX/3eY;->a:LX/0W3;

    sget-wide v9, LX/0X5;->gj:J

    const-string v8, ""

    invoke-interface {v7, v9, v10, v8}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 617493
    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_3

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 617494
    :goto_1
    move-object v1, v7

    .line 617495
    iget-object v2, v0, LX/3eX;->c:LX/3dx;

    invoke-virtual {v2}, LX/3dx;->b()LX/0Px;

    move-result-object v2

    .line 617496
    invoke-interface {v1, v2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 617497
    new-instance v2, Lcom/facebook/stickers/service/FetchStickerPacksByIdParams;

    invoke-direct {v2, v1}, Lcom/facebook/stickers/service/FetchStickerPacksByIdParams;-><init>(Ljava/util/Collection;)V

    .line 617498
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 617499
    const-string v3, "fetchStickerPacksByIdParams"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 617500
    new-instance v2, LX/1qK;

    const-string v3, "fetch_sticker_packs_by_id"

    invoke-direct {v2, v3, v1}, LX/1qK;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 617501
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 617502
    iget-object v1, v0, LX/3eX;->a:Lcom/facebook/stickers/service/StickerPacksHandler;

    invoke-virtual {v1, v2}, Lcom/facebook/stickers/service/StickerPacksHandler;->b(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    .line 617503
    iget-object v2, v1, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v2, v2

    .line 617504
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 617505
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/stickers/model/StickerPack;

    .line 617506
    invoke-virtual {v1, v2}, Lcom/facebook/stickers/service/FetchStickerPacksResult;->a(Lcom/facebook/stickers/model/StickerPack;)LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8m2;

    sget-object v6, LX/8m2;->IN_STORE:LX/8m2;

    invoke-virtual {v3, v6}, LX/8m2;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 617507
    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 617508
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 617509
    iget-object v1, v0, LX/3eX;->b:LX/3dt;

    invoke-virtual {v1, v2}, LX/3dt;->b(Ljava/util/List;)V

    .line 617510
    new-instance v1, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    invoke-direct {v1, v2}, Lcom/facebook/stickers/service/FetchStickerPacksResult;-><init>(Ljava/util/List;)V

    goto/16 :goto_0

    .line 617511
    :cond_3
    const/16 v8, 0x2c

    invoke-static {v7, v8}, LX/0YN;->a(Ljava/lang/String;C)Ljava/util/List;

    move-result-object v7

    goto :goto_1
.end method

.method private f(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 617858
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 617859
    const-string v1, "fetchStickerTagsParam"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickerTagsParams;

    .line 617860
    iget-object v1, p0, LX/3ds;->c:LX/3eJ;

    .line 617861
    iget-object v2, v0, Lcom/facebook/stickers/service/FetchStickerTagsParams;->a:LX/0rS;

    move-object v2, v2

    .line 617862
    sget-object p0, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    invoke-virtual {v2, p0}, LX/0rS;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 617863
    iget-object p0, v0, Lcom/facebook/stickers/service/FetchStickerTagsParams;->b:LX/8m4;

    move-object p0, p0

    .line 617864
    sget-object p1, LX/8m4;->FEATURED:LX/8m4;

    invoke-virtual {p0, p1}, LX/8m4;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 617865
    iget-object p0, v1, LX/3eJ;->a:LX/3dt;

    invoke-virtual {p0}, LX/3dt;->f()Z

    move-result p0

    if-eqz p0, :cond_0

    if-nez v2, :cond_0

    .line 617866
    new-instance v2, Lcom/facebook/stickers/service/FetchStickerTagsResult;

    iget-object p0, v1, LX/3eJ;->a:LX/3dt;

    invoke-virtual {p0}, LX/3dt;->g()LX/0Px;

    move-result-object p0

    invoke-direct {v2, p0}, Lcom/facebook/stickers/service/FetchStickerTagsResult;-><init>(Ljava/util/List;)V

    .line 617867
    :goto_0
    move-object v0, v2

    .line 617868
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    .line 617869
    :cond_0
    iget-object p0, v1, LX/3eJ;->b:LX/3dx;

    invoke-virtual {p0}, LX/3dx;->c()LX/0Px;

    move-result-object p0

    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_1

    if-nez v2, :cond_1

    .line 617870
    new-instance v2, Lcom/facebook/stickers/service/FetchStickerTagsResult;

    iget-object p0, v1, LX/3eJ;->b:LX/3dx;

    invoke-virtual {p0}, LX/3dx;->c()LX/0Px;

    move-result-object p0

    invoke-direct {v2, p0}, Lcom/facebook/stickers/service/FetchStickerTagsResult;-><init>(Ljava/util/List;)V

    .line 617871
    :goto_1
    iget-object p0, v1, LX/3eJ;->a:LX/3dt;

    .line 617872
    iget-object p1, v2, Lcom/facebook/stickers/service/FetchStickerTagsResult;->a:LX/0Px;

    move-object p1, p1

    .line 617873
    invoke-virtual {p0, p1}, LX/3dt;->a(LX/0Px;)V

    goto :goto_0

    .line 617874
    :cond_1
    iget-object v2, v1, LX/3eJ;->c:LX/18V;

    iget-object p0, v1, LX/3eJ;->d:LX/3eK;

    invoke-virtual {v2, p0, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/stickers/service/FetchStickerTagsResult;

    .line 617875
    iget-object p0, v1, LX/3eJ;->b:LX/3dx;

    .line 617876
    iget-object p1, v2, Lcom/facebook/stickers/service/FetchStickerTagsResult;->a:LX/0Px;

    move-object p1, p1

    .line 617877
    invoke-virtual {p0, p1}, LX/3dx;->a(LX/0Px;)V

    goto :goto_1

    .line 617878
    :cond_2
    iget-object v2, v1, LX/3eJ;->c:LX/18V;

    iget-object p0, v1, LX/3eJ;->d:LX/3eK;

    invoke-virtual {v2, p0, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/stickers/service/FetchStickerTagsResult;

    goto :goto_0
.end method

.method private g(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 617795
    iget-object v0, p0, LX/3ds;->c:LX/3eJ;

    .line 617796
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 617797
    const-string v2, "fetchTaggedStickerIdsParams"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/service/FetchTaggedStickersParams;

    .line 617798
    iget-object v2, v0, LX/3eJ;->c:LX/18V;

    iget-object p0, v0, LX/3eJ;->e:LX/3eN;

    invoke-virtual {v2, p0, v1}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/service/FetchTaggedStickersResult;

    .line 617799
    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    move-object v0, v1

    .line 617800
    return-object v0
.end method

.method private h(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 11

    .prologue
    .line 617750
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 617751
    const-string v1, "SaveStickerAssetParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/SaveStickerAssetParams;

    .line 617752
    iget-object v1, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v1

    .line 617753
    iget-object v1, p0, LX/3ds;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3eh;

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 617754
    iget-object v3, v0, Lcom/facebook/stickers/service/SaveStickerAssetParams;->a:Ljava/lang/String;

    move-object v3, v3

    .line 617755
    iget-object v6, v0, Lcom/facebook/stickers/service/SaveStickerAssetParams;->c:Landroid/net/Uri;

    move-object v7, v6

    .line 617756
    iget-object v6, v0, Lcom/facebook/stickers/service/SaveStickerAssetParams;->b:Ljava/lang/String;

    move-object v6, v6

    .line 617757
    invoke-static {v6}, LX/3e1;->fromDbString(Ljava/lang/String;)LX/3e1;

    move-result-object v6

    .line 617758
    iget-object v8, v1, LX/3eh;->c:LX/3dx;

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-virtual {v8, v3}, LX/3dx;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    .line 617759
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 617760
    sget-object v3, LX/1nY;->CANCELLED:LX/1nY;

    const-string v4, "The given sticker id is not valid"

    invoke-static {v3, v4}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v3

    .line 617761
    :goto_0
    move-object v0, v3

    .line 617762
    return-object v0

    .line 617763
    :cond_0
    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v9, v3

    check-cast v9, Lcom/facebook/stickers/model/Sticker;

    .line 617764
    sget-object v3, LX/3eo;->b:[I

    invoke-virtual {v6}, LX/3e1;->ordinal()I

    move-result v8

    aget v3, v3, v8

    packed-switch v3, :pswitch_data_0

    move v4, v5

    .line 617765
    :cond_1
    :goto_1
    if-eqz v4, :cond_3

    .line 617766
    invoke-static {}, LX/4m0;->newBuilder()LX/4m0;

    move-result-object v3

    invoke-virtual {v3, v9}, LX/4m0;->a(Lcom/facebook/stickers/model/Sticker;)LX/4m0;

    move-result-object v10

    .line 617767
    iget-object v3, v1, LX/3eh;->f:Lcom/facebook/stickers/data/StickerAssetDownloader;

    iget-object v4, v9, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    iget-object v5, v9, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    move-object v8, v2

    invoke-virtual/range {v3 .. v8}, Lcom/facebook/stickers/data/StickerAssetDownloader;->a(Ljava/lang/String;Ljava/lang/String;LX/3e1;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)Ljava/io/File;

    move-result-object v3

    .line 617768
    iget-object v4, v1, LX/3eh;->c:LX/3dx;

    iget-object v5, v9, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-virtual {v4, v5, v6, v3}, LX/3dx;->a(Ljava/lang/String;LX/3e1;Ljava/io/File;)V

    .line 617769
    sget-object v4, LX/3e1;->STATIC:LX/3e1;

    invoke-virtual {v6, v4}, LX/3e1;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 617770
    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    .line 617771
    iput-object v3, v10, LX/4m0;->d:Landroid/net/Uri;

    .line 617772
    iget-object v3, v9, Lcom/facebook/stickers/model/Sticker;->h:Landroid/net/Uri;

    if-eqz v3, :cond_2

    .line 617773
    iget-object v3, v1, LX/3eh;->c:LX/3dx;

    iget-object v4, v9, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    sget-object v5, LX/3e1;->PREVIEW:LX/3e1;

    invoke-virtual {v3, v4, v5}, LX/3dx;->a(Ljava/lang/String;LX/3e1;)V

    .line 617774
    iget-object v3, v9, Lcom/facebook/stickers/model/Sticker;->h:Landroid/net/Uri;

    .line 617775
    new-instance v4, Ljava/io/File;

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 617776
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 617777
    const/4 v3, 0x0

    .line 617778
    iput-object v3, v10, LX/4m0;->h:Landroid/net/Uri;

    .line 617779
    :cond_2
    :goto_2
    iget-object v3, v1, LX/3eh;->a:LX/3dt;

    invoke-virtual {v10}, LX/4m0;->a()Lcom/facebook/stickers/model/Sticker;

    move-result-object v4

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/3dt;->b(Ljava/util/Collection;)V

    .line 617780
    :cond_3
    sget-object v3, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v3, v3

    .line 617781
    goto :goto_0

    .line 617782
    :pswitch_0
    iget-object v3, v9, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    invoke-static {v3}, LX/3eh;->a(Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_4

    move v3, v4

    :goto_3
    move v4, v3

    .line 617783
    goto :goto_1

    :cond_4
    move v3, v5

    .line 617784
    goto :goto_3

    .line 617785
    :pswitch_1
    iget-object v3, v9, Lcom/facebook/stickers/model/Sticker;->f:Landroid/net/Uri;

    invoke-static {v3}, LX/3eh;->a(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v4, v5

    goto :goto_1

    .line 617786
    :pswitch_2
    iget-object v3, v9, Lcom/facebook/stickers/model/Sticker;->h:Landroid/net/Uri;

    invoke-static {v3}, LX/3eh;->a(Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, v9, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    invoke-static {v3}, LX/3eh;->a(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_5
    move v4, v5

    goto/16 :goto_1

    .line 617787
    :cond_6
    sget-object v4, LX/3e1;->ANIMATED:LX/3e1;

    invoke-virtual {v6, v4}, LX/3e1;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 617788
    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    .line 617789
    iput-object v3, v10, LX/4m0;->f:Landroid/net/Uri;

    .line 617790
    goto :goto_2

    .line 617791
    :cond_7
    sget-object v4, LX/3e1;->PREVIEW:LX/3e1;

    invoke-virtual {v6, v4}, LX/3e1;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 617792
    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    .line 617793
    iput-object v3, v10, LX/4m0;->h:Landroid/net/Uri;

    .line 617794
    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private k(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 14

    .prologue
    .line 617682
    iget-object v0, p0, LX/3ds;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/StickerAssetsHandler;

    .line 617683
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 617684
    const-string v2, "stickerPack"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/facebook/stickers/model/StickerPack;

    .line 617685
    iget-object v1, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v6, v1

    .line 617686
    new-instance v2, Lcom/facebook/stickers/service/FetchStickersParams;

    .line 617687
    iget-object v1, v3, Lcom/facebook/stickers/model/StickerPack;->q:LX/0Px;

    move-object v1, v1

    .line 617688
    sget-object v4, LX/3eg;->DO_NOT_UPDATE_IF_CACHED:LX/3eg;

    invoke-direct {v2, v1, v4}, Lcom/facebook/stickers/service/FetchStickersParams;-><init>(Ljava/util/Collection;LX/3eg;)V

    .line 617689
    iget-object v1, v0, Lcom/facebook/stickers/service/StickerAssetsHandler;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3eh;

    invoke-virtual {v1, v2}, LX/3eh;->a(Lcom/facebook/stickers/service/FetchStickersParams;)Lcom/facebook/stickers/service/FetchStickersResult;

    move-result-object v4

    .line 617690
    iget-object v1, p1, LX/1qK;->mCallback:LX/1qH;

    move-object v5, v1

    .line 617691
    if-eqz v5, :cond_0

    .line 617692
    const-string v1, "0"

    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    invoke-interface {v5, v1}, LX/1qH;->onOperationProgress(Lcom/facebook/fbservice/service/OperationResult;)V

    .line 617693
    :cond_0
    iget-object v1, v0, Lcom/facebook/stickers/service/StickerAssetsHandler;->h:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v1, v0

    move-object v2, p1

    .line 617694
    invoke-static {v1, v2, v4, v5, v6}, Lcom/facebook/stickers/service/StickerAssetsHandler;->a(Lcom/facebook/stickers/service/StickerAssetsHandler;LX/1qK;Lcom/facebook/stickers/service/FetchStickersResult;LX/1qH;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/stickers/service/FetchStickersResult;

    move-result-object v7

    .line 617695
    iget-object v8, v1, Lcom/facebook/stickers/service/StickerAssetsHandler;->c:LX/3dt;

    .line 617696
    iget-object v9, v7, Lcom/facebook/stickers/service/FetchStickersResult;->a:LX/0Px;

    move-object v9, v9

    .line 617697
    invoke-virtual {v8, v9}, LX/3dt;->b(Ljava/util/Collection;)V

    .line 617698
    new-instance v8, LX/4m6;

    invoke-direct {v8}, LX/4m6;-><init>()V

    .line 617699
    iget-object v12, v3, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v12, v12

    .line 617700
    iput-object v12, v8, LX/4m6;->a:Ljava/lang/String;

    .line 617701
    iget-object v12, v3, Lcom/facebook/stickers/model/StickerPack;->b:Ljava/lang/String;

    move-object v12, v12

    .line 617702
    iput-object v12, v8, LX/4m6;->b:Ljava/lang/String;

    .line 617703
    iget-object v12, v3, Lcom/facebook/stickers/model/StickerPack;->c:Ljava/lang/String;

    move-object v12, v12

    .line 617704
    iput-object v12, v8, LX/4m6;->c:Ljava/lang/String;

    .line 617705
    iget-object v12, v3, Lcom/facebook/stickers/model/StickerPack;->d:Ljava/lang/String;

    move-object v12, v12

    .line 617706
    iput-object v12, v8, LX/4m6;->d:Ljava/lang/String;

    .line 617707
    iget-object v12, v3, Lcom/facebook/stickers/model/StickerPack;->e:Landroid/net/Uri;

    move-object v12, v12

    .line 617708
    iput-object v12, v8, LX/4m6;->e:Landroid/net/Uri;

    .line 617709
    iget-object v12, v3, Lcom/facebook/stickers/model/StickerPack;->f:Landroid/net/Uri;

    move-object v12, v12

    .line 617710
    iput-object v12, v8, LX/4m6;->f:Landroid/net/Uri;

    .line 617711
    iget-object v12, v3, Lcom/facebook/stickers/model/StickerPack;->g:Landroid/net/Uri;

    move-object v12, v12

    .line 617712
    iput-object v12, v8, LX/4m6;->g:Landroid/net/Uri;

    .line 617713
    iget-object v12, v3, Lcom/facebook/stickers/model/StickerPack;->h:Landroid/net/Uri;

    move-object v12, v12

    .line 617714
    iput-object v12, v8, LX/4m6;->h:Landroid/net/Uri;

    .line 617715
    iget v12, v3, Lcom/facebook/stickers/model/StickerPack;->i:I

    move v12, v12

    .line 617716
    iput v12, v8, LX/4m6;->i:I

    .line 617717
    invoke-virtual {v3}, Lcom/facebook/stickers/model/StickerPack;->j()J

    move-result-wide v12

    iput-wide v12, v8, LX/4m6;->j:J

    .line 617718
    iget-boolean v12, v3, Lcom/facebook/stickers/model/StickerPack;->k:Z

    move v12, v12

    .line 617719
    iput-boolean v12, v8, LX/4m6;->k:Z

    .line 617720
    iget-boolean v12, v3, Lcom/facebook/stickers/model/StickerPack;->l:Z

    move v12, v12

    .line 617721
    iput-boolean v12, v8, LX/4m6;->l:Z

    .line 617722
    iget-boolean v12, v3, Lcom/facebook/stickers/model/StickerPack;->m:Z

    move v12, v12

    .line 617723
    iput-boolean v12, v8, LX/4m6;->m:Z

    .line 617724
    iget-boolean v12, v3, Lcom/facebook/stickers/model/StickerPack;->n:Z

    move v12, v12

    .line 617725
    iput-boolean v12, v8, LX/4m6;->n:Z

    .line 617726
    iget-boolean v12, v3, Lcom/facebook/stickers/model/StickerPack;->o:Z

    move v12, v12

    .line 617727
    iput-boolean v12, v8, LX/4m6;->o:Z

    .line 617728
    iget-object v12, v3, Lcom/facebook/stickers/model/StickerPack;->p:LX/0Px;

    move-object v12, v12

    .line 617729
    iput-object v12, v8, LX/4m6;->p:Ljava/util/List;

    .line 617730
    iget-object v12, v3, Lcom/facebook/stickers/model/StickerPack;->q:LX/0Px;

    move-object v12, v12

    .line 617731
    iput-object v12, v8, LX/4m6;->q:Ljava/util/List;

    .line 617732
    iget-object v12, v3, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v12, v12

    .line 617733
    iput-object v12, v8, LX/4m6;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    .line 617734
    move-object v8, v8

    .line 617735
    const/4 v9, 0x1

    .line 617736
    iput-boolean v9, v8, LX/4m6;->k:Z

    .line 617737
    move-object v8, v8

    .line 617738
    iget-object v9, v1, Lcom/facebook/stickers/service/StickerAssetsHandler;->i:LX/0Uh;

    const/16 v10, 0x291

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, LX/0Uh;->a(IZ)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 617739
    invoke-static {v1, v3, v6}, Lcom/facebook/stickers/service/StickerAssetsHandler;->a(Lcom/facebook/stickers/service/StickerAssetsHandler;Lcom/facebook/stickers/model/StickerPack;Lcom/facebook/common/callercontext/CallerContext;)Landroid/net/Uri;

    move-result-object v9

    .line 617740
    iput-object v9, v8, LX/4m6;->f:Landroid/net/Uri;

    .line 617741
    :cond_1
    invoke-virtual {v8}, LX/4m6;->s()Lcom/facebook/stickers/model/StickerPack;

    move-result-object v8

    .line 617742
    iget-object v9, v1, Lcom/facebook/stickers/service/StickerAssetsHandler;->d:LX/3dx;

    invoke-virtual {v9, v8}, LX/3dx;->a(Lcom/facebook/stickers/model/StickerPack;)V

    .line 617743
    iget-object v9, v1, Lcom/facebook/stickers/service/StickerAssetsHandler;->c:LX/3dt;

    invoke-virtual {v9, v8}, LX/3dt;->a(Lcom/facebook/stickers/model/StickerPack;)V

    .line 617744
    move-object v1, v7

    .line 617745
    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 617746
    :goto_0
    move-object v0, v1

    .line 617747
    return-object v0

    .line 617748
    :cond_2
    sget-object v1, Lcom/facebook/stickers/service/StickerAssetsHandler;->a:Ljava/lang/Class;

    const-string v2, "cannot save assets to disk"

    invoke-static {v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 617749
    sget-object v1, LX/1nY;->CACHE_DISK_ERROR:LX/1nY;

    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    goto :goto_0
.end method

.method private m(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 617677
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 617678
    const-string v1, "stickerSearchParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/StickerSearchParams;

    .line 617679
    iget-object v1, p0, LX/3ds;->c:LX/3eJ;

    .line 617680
    iget-object p0, v1, LX/3eJ;->c:LX/18V;

    iget-object p1, v1, LX/3eJ;->f:LX/3eO;

    invoke-virtual {p0, p1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/stickers/service/StickerSearchResult;

    move-object v0, p0

    .line 617681
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method private n(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    .line 617662
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 617663
    const-string v1, "sticker"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 617664
    iget-object v1, p0, LX/3ds;->b:LX/3dw;

    const/16 p1, 0x10

    .line 617665
    iget-object v2, v1, LX/3dw;->b:LX/3dx;

    invoke-virtual {v2}, LX/3dx;->a()LX/0Px;

    move-result-object v2

    .line 617666
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3, p1}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 617667
    invoke-static {v3}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v3

    .line 617668
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 617669
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/stickers/model/Sticker;

    .line 617670
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-ge v5, p1, :cond_1

    .line 617671
    iget-object v5, v2, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    iget-object p0, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 617672
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 617673
    :cond_1
    iget-object v2, v1, LX/3dw;->b:LX/3dx;

    invoke-virtual {v2, v3}, LX/3dx;->a(Ljava/util/List;)V

    .line 617674
    iget-object v2, v1, LX/3dw;->a:LX/3dt;

    invoke-virtual {v2, v3}, LX/3dt;->a(Ljava/util/List;)V

    .line 617675
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 617676
    return-object v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 13

    .prologue
    .line 617512
    :try_start_0
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 617513
    const-string v1, "fetch_sticker_pack_ids"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 617514
    iget-object v0, p0, LX/3ds;->d:Lcom/facebook/stickers/service/StickerPacksHandler;

    invoke-virtual {v0, p1}, Lcom/facebook/stickers/service/StickerPacksHandler;->a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 617515
    :goto_0
    return-object v0

    .line 617516
    :cond_0
    const-string v1, "fetch_sticker_packs"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 617517
    invoke-direct {p0, p1}, LX/3ds;->b(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 617518
    :cond_1
    const-string v1, "fetch_sticker_packs_by_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 617519
    iget-object v0, p0, LX/3ds;->d:Lcom/facebook/stickers/service/StickerPacksHandler;

    invoke-virtual {v0, p1}, Lcom/facebook/stickers/service/StickerPacksHandler;->b(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 617520
    goto :goto_0

    .line 617521
    :cond_2
    const-string v1, "fetch_recent_stickers"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 617522
    invoke-direct {p0}, LX/3ds;->a()Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 617523
    :cond_3
    const-string v1, "fetch_stickers"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 617524
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 617525
    const-string v1, "fetchStickersParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickersParams;

    .line 617526
    iget-object v1, p0, LX/3ds;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3eh;

    invoke-virtual {v1, v0}, LX/3eh;->a(Lcom/facebook/stickers/service/FetchStickersParams;)Lcom/facebook/stickers/service/FetchStickersResult;

    move-result-object v0

    .line 617527
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 617528
    goto :goto_0

    .line 617529
    :cond_4
    const-string v1, "fetch_sticker_packs_and_stickers"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 617530
    const/4 v12, 0x0

    .line 617531
    iget-object v4, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v4, v4

    .line 617532
    const-string v5, "fetchStickerPacksAndStickersParams"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersParams;

    .line 617533
    new-instance v5, LX/3ea;

    iget-object v6, v4, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersParams;->a:LX/3do;

    iget-object v4, v4, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersParams;->b:LX/0rS;

    invoke-direct {v5, v6, v4}, LX/3ea;-><init>(LX/3do;LX/0rS;)V

    invoke-virtual {v5}, LX/3ea;->a()Lcom/facebook/stickers/service/FetchStickerPacksParams;

    move-result-object v4

    .line 617534
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 617535
    const-string v5, "fetchStickerPacksParams"

    invoke-virtual {v6, v5, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 617536
    new-instance v4, LX/1qK;

    const-string v5, "fetch_sticker_packs"

    .line 617537
    iget-object v7, p1, LX/1qK;->mId:Ljava/lang/String;

    move-object v7, v7

    .line 617538
    iget-object v8, p1, LX/1qK;->mRequestState:LX/0zW;

    move-object v8, v8

    .line 617539
    iget-object v9, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v9, v9

    .line 617540
    iget-object v10, p1, LX/1qK;->mCallback:LX/1qH;

    move-object v10, v10

    .line 617541
    invoke-direct/range {v4 .. v10}, LX/1qK;-><init>(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;LX/0zW;Lcom/facebook/common/callercontext/CallerContext;LX/1qH;)V

    .line 617542
    invoke-virtual {p0, v4}, LX/3ds;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v4

    .line 617543
    iget-boolean v5, v4, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v5, v5

    .line 617544
    if-nez v5, :cond_13

    .line 617545
    :cond_5
    :goto_1
    move-object v0, v4

    .line 617546
    goto/16 :goto_0

    .line 617547
    :cond_6
    const-string v1, "fetch_sticker_tags"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 617548
    invoke-direct {p0, p1}, LX/3ds;->f(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 617549
    :cond_7
    const-string v1, "fetch_tagged_sticker_ids"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 617550
    invoke-direct {p0, p1}, LX/3ds;->g(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 617551
    :cond_8
    const-string v1, "add_sticker_pack"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 617552
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 617553
    const-string v1, "stickerPack"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 617554
    iget-object v1, p0, LX/3ds;->d:Lcom/facebook/stickers/service/StickerPacksHandler;

    .line 617555
    iget-object v2, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v2, v2

    .line 617556
    const-string v3, "stickerPack"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/stickers/model/StickerPack;

    .line 617557
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    .line 617558
    new-instance v3, LX/3ea;

    sget-object v5, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    sget-object p1, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    invoke-direct {v3, v5, p1}, LX/3ea;-><init>(LX/3do;LX/0rS;)V

    invoke-virtual {v3}, LX/3ea;->a()Lcom/facebook/stickers/service/FetchStickerPacksParams;

    move-result-object v3

    .line 617559
    invoke-static {v1, v3}, Lcom/facebook/stickers/service/StickerPacksHandler;->a(Lcom/facebook/stickers/service/StickerPacksHandler;Lcom/facebook/stickers/service/FetchStickerPacksParams;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    move-object v3, v3

    .line 617560
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 617561
    invoke-virtual {v5, v4}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 617562
    iget-object p1, v3, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v3, p1

    .line 617563
    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    invoke-virtual {v5, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 617564
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 617565
    iget-object v5, v1, Lcom/facebook/stickers/service/StickerPacksHandler;->g:LX/18V;

    invoke-virtual {v5}, LX/18V;->a()LX/2VK;

    move-result-object v5

    .line 617566
    iget-object v6, v1, Lcom/facebook/stickers/service/StickerPacksHandler;->l:LX/3eT;

    invoke-static {v6, v2}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v6

    const-string p1, "add-sticker-pack"

    .line 617567
    iput-object p1, v6, LX/2Vk;->c:Ljava/lang/String;

    .line 617568
    move-object v6, v6

    .line 617569
    invoke-virtual {v6}, LX/2Vk;->a()LX/2Vj;

    move-result-object v6

    invoke-interface {v5, v6}, LX/2VK;->a(LX/2Vj;)V

    .line 617570
    iget-object v6, v1, Lcom/facebook/stickers/service/StickerPacksHandler;->h:LX/3eP;

    invoke-static {v6, v3}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v6

    const-string p1, "set-downloaded-packs"

    .line 617571
    iput-object p1, v6, LX/2Vk;->c:Ljava/lang/String;

    .line 617572
    move-object v6, v6

    .line 617573
    const-string p1, "add-sticker-pack"

    .line 617574
    iput-object p1, v6, LX/2Vk;->d:Ljava/lang/String;

    .line 617575
    move-object v6, v6

    .line 617576
    invoke-virtual {v6}, LX/2Vk;->a()LX/2Vj;

    move-result-object v6

    invoke-interface {v5, v6}, LX/2VK;->a(LX/2Vj;)V

    .line 617577
    const-string v6, "add-packs"

    sget-object p1, Lcom/facebook/stickers/service/StickerPacksHandler;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v5, v6, p1}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 617578
    invoke-static {v1, v4}, Lcom/facebook/stickers/service/StickerPacksHandler;->a(Lcom/facebook/stickers/service/StickerPacksHandler;LX/0Px;)V

    .line 617579
    sget-object v1, LX/3do;->OWNED_PACKS:LX/3do;

    invoke-static {p0, v1, v0}, LX/3ds;->a(LX/3ds;LX/3do;Lcom/facebook/stickers/model/StickerPack;)V

    .line 617580
    sget-object v1, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    invoke-static {p0, v1, v0}, LX/3ds;->a(LX/3ds;LX/3do;Lcom/facebook/stickers/model/StickerPack;)V

    .line 617581
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 617582
    const-string v2, "stickerPack"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 617583
    new-instance v0, LX/1qK;

    const-string v2, "add_closed_download_preview_sticker_pack"

    invoke-direct {v0, v2, v1}, LX/1qK;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 617584
    iget-object v1, p0, LX/3ds;->e:LX/3eX;

    invoke-virtual {v1, v0}, LX/3eX;->a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    .line 617585
    iget-object v0, p0, LX/3ds;->f:LX/3eU;

    invoke-virtual {v0}, LX/3eU;->a()V

    .line 617586
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 617587
    move-object v0, v0

    .line 617588
    goto/16 :goto_0

    .line 617589
    :cond_9
    const-string v1, "fetch_closed_download_preview_pack_ids"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 617590
    invoke-direct {p0}, LX/3ds;->b()Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 617591
    :cond_a
    const-string v1, "fetch_download_preview_sticker_packs"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 617592
    invoke-direct {p0}, LX/3ds;->c()Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 617593
    :cond_b
    const-string v1, "add_closed_download_preview_sticker_pack"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 617594
    iget-object v0, p0, LX/3ds;->e:LX/3eX;

    invoke-virtual {v0, p1}, LX/3eX;->a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    .line 617595
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 617596
    move-object v0, v0

    .line 617597
    goto/16 :goto_0

    .line 617598
    :cond_c
    const-string v1, "download_sticker_pack_assets"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 617599
    invoke-direct {p0, p1}, LX/3ds;->k(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 617600
    :cond_d
    const-string v1, "set_downloaded_sticker_packs"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 617601
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 617602
    const-string v1, "stickerPacks"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 617603
    const-string v2, "deletedStickerPacks"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 617604
    iget-object v2, p0, LX/3ds;->d:Lcom/facebook/stickers/service/StickerPacksHandler;

    invoke-virtual {v2, v1, v0}, Lcom/facebook/stickers/service/StickerPacksHandler;->a(Ljava/util/List;Ljava/util/List;)V

    .line 617605
    iget-object v0, p0, LX/3ds;->a:LX/3dt;

    invoke-virtual {v0}, LX/3dt;->e()V

    .line 617606
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 617607
    move-object v0, v0

    .line 617608
    goto/16 :goto_0

    .line 617609
    :cond_e
    const-string v1, "sticker_search"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 617610
    invoke-direct {p0, p1}, LX/3ds;->m(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 617611
    :cond_f
    const-string v1, "update_recent_stickers"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 617612
    invoke-direct {p0, p1}, LX/3ds;->n(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 617613
    :cond_10
    const-string v1, "download_sticker_asset"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 617614
    invoke-direct {p0, p1}, LX/3ds;->h(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 617615
    :cond_11
    const-string v1, "clear_sticker_cache"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 617616
    iget-object v0, p0, LX/3ds;->a:LX/3dt;

    invoke-virtual {v0}, LX/3dt;->h()V

    .line 617617
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 617618
    move-object v0, v0

    .line 617619
    goto/16 :goto_0

    .line 617620
    :cond_12
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 617621
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 617622
    iget-object v0, p0, LX/3ds;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8CH;

    invoke-virtual {v0, v1}, LX/8CH;->a(Ljava/lang/Exception;)V

    .line 617623
    throw v1

    .line 617624
    :cond_13
    invoke-virtual {v4}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    .line 617625
    iget-object v5, v4, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v5, v5

    .line 617626
    invoke-virtual {v5}, LX/0am;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_14

    .line 617627
    iget-object v5, v4, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v4, v5

    .line 617628
    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0Px;

    move-object v11, v4

    .line 617629
    :goto_2
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 617630
    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v7

    move v5, v12

    :goto_3
    if-ge v5, v7, :cond_15

    invoke-virtual {v11, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/stickers/model/StickerPack;

    .line 617631
    iget-object v8, v4, Lcom/facebook/stickers/model/StickerPack;->q:LX/0Px;

    move-object v4, v8

    .line 617632
    invoke-interface {v6, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 617633
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_3

    .line 617634
    :cond_14
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 617635
    move-object v11, v4

    goto :goto_2

    .line 617636
    :cond_15
    new-instance v4, Lcom/facebook/stickers/service/FetchStickersParams;

    sget-object v5, LX/3eg;->DO_NOT_UPDATE_IF_CACHED:LX/3eg;

    invoke-direct {v4, v6, v5}, Lcom/facebook/stickers/service/FetchStickersParams;-><init>(Ljava/util/Collection;LX/3eg;)V

    .line 617637
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 617638
    const-string v5, "fetchStickersParams"

    invoke-virtual {v6, v5, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 617639
    new-instance v4, LX/1qK;

    const-string v5, "fetch_stickers"

    .line 617640
    iget-object v7, p1, LX/1qK;->mId:Ljava/lang/String;

    move-object v7, v7

    .line 617641
    iget-object v8, p1, LX/1qK;->mRequestState:LX/0zW;

    move-object v8, v8

    .line 617642
    iget-object v9, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v9, v9

    .line 617643
    iget-object v10, p1, LX/1qK;->mCallback:LX/1qH;

    move-object v10, v10

    .line 617644
    invoke-direct/range {v4 .. v10}, LX/1qK;-><init>(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;LX/0zW;Lcom/facebook/common/callercontext/CallerContext;LX/1qH;)V

    .line 617645
    invoke-virtual {p0, v4}, LX/3ds;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v4

    .line 617646
    iget-boolean v5, v4, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v5, v5

    .line 617647
    if-eqz v5, :cond_5

    .line 617648
    invoke-virtual {v4}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/stickers/service/FetchStickersResult;

    .line 617649
    invoke-static {}, LX/0Xq;->t()LX/0Xq;

    move-result-object v6

    .line 617650
    iget-object v5, v4, Lcom/facebook/stickers/service/FetchStickersResult;->a:LX/0Px;

    move-object v7, v5

    .line 617651
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v5, v12

    :goto_4
    if-ge v5, v8, :cond_16

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/stickers/model/Sticker;

    .line 617652
    iget-object v9, v4, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    invoke-interface {v6, v9, v4}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 617653
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_4

    .line 617654
    :cond_16
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v7

    .line 617655
    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v8

    move v5, v12

    :goto_5
    if-ge v5, v8, :cond_17

    invoke-virtual {v11, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/stickers/model/StickerPack;

    .line 617656
    iget-object v9, v4, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v9, v9

    .line 617657
    iget-object v10, v4, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v4, v10

    .line 617658
    invoke-interface {v6, v4}, LX/0Xv;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    invoke-virtual {v7, v9, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 617659
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_5

    .line 617660
    :cond_17
    new-instance v4, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersResult;

    invoke-virtual {v7}, LX/0P2;->b()LX/0P1;

    move-result-object v5

    invoke-direct {v4, v11, v5}, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersResult;-><init>(Ljava/util/List;LX/0P1;)V

    .line 617661
    invoke-static {v4}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v4

    goto/16 :goto_1
.end method
