.class public LX/4ej;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 797408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized a()V
    .locals 3

    .prologue
    .line 797409
    const-class v1, LX/4ej;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, LX/4ej;->a:Z

    if-nez v0, :cond_1

    .line 797410
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v2, 0x10

    if-gt v0, v2, :cond_0

    .line 797411
    :try_start_1
    const-string v0, "fb_jpegturbo"

    invoke-static {v0}, LX/02C;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 797412
    :cond_0
    :goto_0
    :try_start_2
    const-string v0, "static-webp"

    invoke-static {v0}, LX/02C;->a(Ljava/lang/String;)V

    .line 797413
    const/4 v0, 0x1

    sput-boolean v0, LX/4ej;->a:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 797414
    :cond_1
    monitor-exit v1

    return-void

    .line 797415
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    goto :goto_0
.end method
