.class public final enum LX/49z;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/49z;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/49z;

.field public static final enum FACEWEB:LX/49z;

.field public static final enum PHOTO:LX/49z;

.field public static final enum URI:LX/49z;

.field public static final enum VIDEO:LX/49z;


# instance fields
.field private mMobileConfigSpecifier:J

.field private mWhitePatternList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    .line 675209
    new-instance v1, LX/49z;

    const-string v2, "FACEWEB"

    const/4 v3, 0x0

    sget-wide v4, LX/0X5;->bV:J

    sget-object v6, LX/0ye;->k:Ljava/util/List;

    invoke-direct/range {v1 .. v6}, LX/49z;-><init>(Ljava/lang/String;IJLjava/util/List;)V

    sput-object v1, LX/49z;->FACEWEB:LX/49z;

    .line 675210
    new-instance v1, LX/49z;

    const-string v2, "PHOTO"

    const/4 v3, 0x1

    sget-wide v4, LX/0X5;->bS:J

    sget-object v6, LX/0ye;->i:Ljava/util/List;

    invoke-direct/range {v1 .. v6}, LX/49z;-><init>(Ljava/lang/String;IJLjava/util/List;)V

    sput-object v1, LX/49z;->PHOTO:LX/49z;

    .line 675211
    new-instance v1, LX/49z;

    const-string v2, "URI"

    const/4 v3, 0x2

    sget-wide v4, LX/0X5;->bT:J

    sget-object v6, LX/0ye;->l:Ljava/util/List;

    invoke-direct/range {v1 .. v6}, LX/49z;-><init>(Ljava/lang/String;IJLjava/util/List;)V

    sput-object v1, LX/49z;->URI:LX/49z;

    .line 675212
    new-instance v1, LX/49z;

    const-string v2, "VIDEO"

    const/4 v3, 0x3

    sget-wide v4, LX/0X5;->bU:J

    sget-object v6, LX/0ye;->j:Ljava/util/List;

    invoke-direct/range {v1 .. v6}, LX/49z;-><init>(Ljava/lang/String;IJLjava/util/List;)V

    sput-object v1, LX/49z;->VIDEO:LX/49z;

    .line 675213
    const/4 v0, 0x4

    new-array v0, v0, [LX/49z;

    const/4 v1, 0x0

    sget-object v2, LX/49z;->FACEWEB:LX/49z;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/49z;->PHOTO:LX/49z;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/49z;->URI:LX/49z;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/49z;->VIDEO:LX/49z;

    aput-object v2, v0, v1

    sput-object v0, LX/49z;->$VALUES:[LX/49z;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IJLjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 675205
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 675206
    iput-wide p3, p0, LX/49z;->mMobileConfigSpecifier:J

    .line 675207
    iput-object p5, p0, LX/49z;->mWhitePatternList:Ljava/util/List;

    .line 675208
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/49z;
    .locals 1

    .prologue
    .line 675204
    const-class v0, LX/49z;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/49z;

    return-object v0
.end method

.method public static values()[LX/49z;
    .locals 1

    .prologue
    .line 675201
    sget-object v0, LX/49z;->$VALUES:[LX/49z;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/49z;

    return-object v0
.end method


# virtual methods
.method public final getMobileConfigSpecifier()J
    .locals 2

    .prologue
    .line 675203
    iget-wide v0, p0, LX/49z;->mMobileConfigSpecifier:J

    return-wide v0
.end method

.method public final getWhitePatternList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 675202
    iget-object v0, p0, LX/49z;->mWhitePatternList:Ljava/util/List;

    return-object v0
.end method
