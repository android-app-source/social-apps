.class public LX/3vD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3us;
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/PopupWindow$OnDismissListener;


# static fields
.field public static final a:I


# instance fields
.field public b:Z

.field public final c:Landroid/content/Context;

.field public final d:Landroid/view/LayoutInflater;

.field public final e:LX/3v0;

.field public final f:LX/3vC;

.field public final g:Z

.field public final h:I

.field private final i:I

.field private final j:I

.field public k:Landroid/view/View;

.field public l:LX/3w1;

.field private m:Landroid/view/ViewTreeObserver;

.field public n:LX/3uE;

.field public o:Landroid/view/ViewGroup;

.field private p:Z

.field private q:I

.field public r:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 650943
    const v0, 0x7f03000f

    sput v0, LX/3vD;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/3v0;Landroid/view/View;)V
    .locals 6

    .prologue
    .line 650941
    const/4 v4, 0x0

    const v5, 0x7f01003b

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/3vD;-><init>(Landroid/content/Context;LX/3v0;Landroid/view/View;ZI)V

    .line 650942
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/3v0;Landroid/view/View;ZI)V
    .locals 7

    .prologue
    .line 650939
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, LX/3vD;-><init>(Landroid/content/Context;LX/3v0;Landroid/view/View;ZII)V

    .line 650940
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/3v0;Landroid/view/View;ZII)V
    .locals 3

    .prologue
    .line 650925
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 650926
    const/4 v0, 0x0

    iput v0, p0, LX/3vD;->r:I

    .line 650927
    iput-object p1, p0, LX/3vD;->c:Landroid/content/Context;

    .line 650928
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/3vD;->d:Landroid/view/LayoutInflater;

    .line 650929
    iput-object p2, p0, LX/3vD;->e:LX/3v0;

    .line 650930
    new-instance v0, LX/3vC;

    iget-object v1, p0, LX/3vD;->e:LX/3v0;

    invoke-direct {v0, p0, v1}, LX/3vC;-><init>(LX/3vD;LX/3v0;)V

    iput-object v0, p0, LX/3vD;->f:LX/3vC;

    .line 650931
    iput-boolean p4, p0, LX/3vD;->g:Z

    .line 650932
    iput p5, p0, LX/3vD;->i:I

    .line 650933
    iput p6, p0, LX/3vD;->j:I

    .line 650934
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 650935
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v1, v1, 0x2

    const v2, 0x7f0b0002

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/3vD;->h:I

    .line 650936
    iput-object p3, p0, LX/3vD;->k:Landroid/view/View;

    .line 650937
    invoke-virtual {p2, p0, p1}, LX/3v0;->a(LX/3us;Landroid/content/Context;)V

    .line 650938
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 650922
    invoke-virtual {p0}, LX/3vD;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 650923
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MenuPopupHelper cannot be used without an anchor"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 650924
    :cond_0
    return-void
.end method

.method public final a(LX/3v0;Z)V
    .locals 1

    .prologue
    .line 650917
    iget-object v0, p0, LX/3vD;->e:LX/3v0;

    if-eq p1, v0, :cond_1

    .line 650918
    :cond_0
    :goto_0
    return-void

    .line 650919
    :cond_1
    invoke-virtual {p0}, LX/3vD;->e()V

    .line 650920
    iget-object v0, p0, LX/3vD;->n:LX/3uE;

    if-eqz v0, :cond_0

    .line 650921
    iget-object v0, p0, LX/3vD;->n:LX/3uE;

    invoke-interface {v0, p1, p2}, LX/3uE;->a(LX/3v0;Z)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;LX/3v0;)V
    .locals 0

    .prologue
    .line 650916
    return-void
.end method

.method public final a(LX/3vG;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 650901
    invoke-virtual {p1}, LX/3v0;->hasVisibleItems()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 650902
    new-instance v3, LX/3vD;

    iget-object v0, p0, LX/3vD;->c:Landroid/content/Context;

    iget-object v4, p0, LX/3vD;->k:Landroid/view/View;

    invoke-direct {v3, v0, p1, v4}, LX/3vD;-><init>(Landroid/content/Context;LX/3v0;Landroid/view/View;)V

    .line 650903
    iget-object v0, p0, LX/3vD;->n:LX/3uE;

    .line 650904
    iput-object v0, v3, LX/3vD;->n:LX/3uE;

    .line 650905
    invoke-virtual {p1}, LX/3v0;->size()I

    move-result v4

    move v0, v2

    .line 650906
    :goto_0
    if-ge v0, v4, :cond_3

    .line 650907
    invoke-virtual {p1, v0}, LX/3v0;->getItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 650908
    invoke-interface {v5}, Landroid/view/MenuItem;->isVisible()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    if-eqz v5, :cond_1

    move v0, v1

    .line 650909
    :goto_1
    iput-boolean v0, v3, LX/3vD;->b:Z

    .line 650910
    invoke-virtual {v3}, LX/3vD;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 650911
    iget-object v0, p0, LX/3vD;->n:LX/3uE;

    if-eqz v0, :cond_0

    .line 650912
    iget-object v0, p0, LX/3vD;->n:LX/3uE;

    invoke-interface {v0, p1}, LX/3uE;->a_(LX/3v0;)Z

    .line 650913
    :cond_0
    :goto_2
    return v1

    .line 650914
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v1, v2

    .line 650915
    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 650897
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3vD;->p:Z

    .line 650898
    iget-object v0, p0, LX/3vD;->f:LX/3vC;

    if-eqz v0, :cond_0

    .line 650899
    iget-object v0, p0, LX/3vD;->f:LX/3vC;

    const v1, 0x226451ec

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 650900
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 650944
    const/4 v0, 0x0

    return v0
.end method

.method public final b(LX/3v3;)Z
    .locals 1

    .prologue
    .line 650896
    const/4 v0, 0x0

    return v0
.end method

.method public final c(LX/3v3;)Z
    .locals 1

    .prologue
    .line 650895
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Z
    .locals 12

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 650850
    new-instance v2, LX/3w1;

    iget-object v3, p0, LX/3vD;->c:Landroid/content/Context;

    const/4 v4, 0x0

    iget v5, p0, LX/3vD;->i:I

    iget v6, p0, LX/3vD;->j:I

    invoke-direct {v2, v3, v4, v5, v6}, LX/3w1;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    iput-object v2, p0, LX/3vD;->l:LX/3w1;

    .line 650851
    iget-object v2, p0, LX/3vD;->l:LX/3w1;

    invoke-virtual {v2, p0}, LX/3w1;->a(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 650852
    iget-object v2, p0, LX/3vD;->l:LX/3w1;

    .line 650853
    iput-object p0, v2, LX/3w1;->t:Landroid/widget/AdapterView$OnItemClickListener;

    .line 650854
    iget-object v2, p0, LX/3vD;->l:LX/3w1;

    iget-object v3, p0, LX/3vD;->f:LX/3vC;

    invoke-virtual {v2, v3}, LX/3w1;->a(Landroid/widget/ListAdapter;)V

    .line 650855
    iget-object v2, p0, LX/3vD;->l:LX/3w1;

    invoke-virtual {v2, v1}, LX/3w1;->a(Z)V

    .line 650856
    iget-object v2, p0, LX/3vD;->k:Landroid/view/View;

    .line 650857
    if-eqz v2, :cond_5

    .line 650858
    iget-object v3, p0, LX/3vD;->m:Landroid/view/ViewTreeObserver;

    if-nez v3, :cond_0

    move v0, v1

    .line 650859
    :cond_0
    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    iput-object v3, p0, LX/3vD;->m:Landroid/view/ViewTreeObserver;

    .line 650860
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3vD;->m:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 650861
    :cond_1
    iget-object v0, p0, LX/3vD;->l:LX/3w1;

    .line 650862
    iput-object v2, v0, LX/3w1;->r:Landroid/view/View;

    .line 650863
    iget-object v0, p0, LX/3vD;->l:LX/3w1;

    iget v2, p0, LX/3vD;->r:I

    .line 650864
    iput v2, v0, LX/3w1;->l:I

    .line 650865
    iget-boolean v0, p0, LX/3vD;->p:Z

    if-nez v0, :cond_4

    .line 650866
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 650867
    iget-object v7, p0, LX/3vD;->f:LX/3vC;

    .line 650868
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 650869
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    .line 650870
    invoke-interface {v7}, Landroid/widget/ListAdapter;->getCount()I

    move-result v10

    move v6, v0

    move v3, v0

    move-object v5, v4

    move v2, v0

    .line 650871
    :goto_0
    if-ge v6, v10, :cond_3

    .line 650872
    invoke-interface {v7, v6}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    .line 650873
    if-eq v0, v3, :cond_7

    move v3, v0

    move-object v0, v4

    .line 650874
    :goto_1
    iget-object v5, p0, LX/3vD;->o:Landroid/view/ViewGroup;

    if-nez v5, :cond_2

    .line 650875
    new-instance v5, Landroid/widget/FrameLayout;

    iget-object v11, p0, LX/3vD;->c:Landroid/content/Context;

    invoke-direct {v5, v11}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, LX/3vD;->o:Landroid/view/ViewGroup;

    .line 650876
    :cond_2
    iget-object v5, p0, LX/3vD;->o:Landroid/view/ViewGroup;

    invoke-interface {v7, v6, v0, v5}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 650877
    invoke-virtual {v5, v8, v9}, Landroid/view/View;->measure(II)V

    .line 650878
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 650879
    iget v11, p0, LX/3vD;->h:I

    if-lt v0, v11, :cond_6

    .line 650880
    iget v2, p0, LX/3vD;->h:I

    .line 650881
    :cond_3
    move v0, v2

    .line 650882
    iput v0, p0, LX/3vD;->q:I

    .line 650883
    iput-boolean v1, p0, LX/3vD;->p:Z

    .line 650884
    :cond_4
    iget-object v0, p0, LX/3vD;->l:LX/3w1;

    iget v2, p0, LX/3vD;->q:I

    invoke-virtual {v0, v2}, LX/3w1;->c(I)V

    .line 650885
    iget-object v0, p0, LX/3vD;->l:LX/3w1;

    const/4 v2, 0x2

    .line 650886
    iget-object v3, v0, LX/3w1;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v3, v2}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 650887
    iget-object v0, p0, LX/3vD;->l:LX/3w1;

    invoke-virtual {v0}, LX/3w1;->c()V

    .line 650888
    iget-object v0, p0, LX/3vD;->l:LX/3w1;

    .line 650889
    iget-object v2, v0, LX/3w1;->f:LX/3wy;

    move-object v0, v2

    .line 650890
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 650891
    :goto_2
    return v1

    :cond_5
    move v1, v0

    .line 650892
    goto :goto_2

    .line 650893
    :cond_6
    if-le v0, v2, :cond_8

    .line 650894
    :goto_3
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move v2, v0

    goto :goto_0

    :cond_7
    move-object v0, v5

    goto :goto_1

    :cond_8
    move v0, v2

    goto :goto_3
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 650847
    invoke-virtual {p0}, LX/3vD;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 650848
    iget-object v0, p0, LX/3vD;->l:LX/3w1;

    invoke-virtual {v0}, LX/3w1;->a()V

    .line 650849
    :cond_0
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 650846
    iget-object v0, p0, LX/3vD;->l:LX/3w1;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3vD;->l:LX/3w1;

    invoke-virtual {v0}, LX/3w1;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDismiss()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 650839
    iput-object v1, p0, LX/3vD;->l:LX/3w1;

    .line 650840
    iget-object v0, p0, LX/3vD;->e:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->close()V

    .line 650841
    iget-object v0, p0, LX/3vD;->m:Landroid/view/ViewTreeObserver;

    if-eqz v0, :cond_1

    .line 650842
    iget-object v0, p0, LX/3vD;->m:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3vD;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iput-object v0, p0, LX/3vD;->m:Landroid/view/ViewTreeObserver;

    .line 650843
    :cond_0
    iget-object v0, p0, LX/3vD;->m:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 650844
    iput-object v1, p0, LX/3vD;->m:Landroid/view/ViewTreeObserver;

    .line 650845
    :cond_1
    return-void
.end method

.method public final onGlobalLayout()V
    .locals 1

    .prologue
    .line 650832
    invoke-virtual {p0}, LX/3vD;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 650833
    iget-object v0, p0, LX/3vD;->k:Landroid/view/View;

    .line 650834
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_2

    .line 650835
    :cond_0
    invoke-virtual {p0}, LX/3vD;->e()V

    .line 650836
    :cond_1
    :goto_0
    return-void

    .line 650837
    :cond_2
    invoke-virtual {p0}, LX/3vD;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 650838
    iget-object v0, p0, LX/3vD;->l:LX/3w1;

    invoke-virtual {v0}, LX/3w1;->c()V

    goto :goto_0
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 650829
    iget-object v0, p0, LX/3vD;->f:LX/3vC;

    .line 650830
    iget-object v1, v0, LX/3vC;->b:LX/3v0;

    invoke-virtual {v0, p3}, LX/3vC;->a(I)LX/3v3;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/3v0;->a(Landroid/view/MenuItem;I)Z

    .line 650831
    return-void
.end method

.method public final onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 650826
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    const/16 v1, 0x52

    if-ne p2, v1, :cond_0

    .line 650827
    invoke-virtual {p0}, LX/3vD;->e()V

    .line 650828
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
