.class public final synthetic LX/4rD;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 815260
    invoke-static {}, LX/4pP;->values()[LX/4pP;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/4rD;->b:[I

    :try_start_0
    sget-object v0, LX/4rD;->b:[I

    sget-object v1, LX/4pP;->CLASS:LX/4pP;

    invoke-virtual {v1}, LX/4pP;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    :goto_0
    :try_start_1
    sget-object v0, LX/4rD;->b:[I

    sget-object v1, LX/4pP;->MINIMAL_CLASS:LX/4pP;

    invoke-virtual {v1}, LX/4pP;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_1
    :try_start_2
    sget-object v0, LX/4rD;->b:[I

    sget-object v1, LX/4pP;->NAME:LX/4pP;

    invoke-virtual {v1}, LX/4pP;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    :goto_2
    :try_start_3
    sget-object v0, LX/4rD;->b:[I

    sget-object v1, LX/4pP;->NONE:LX/4pP;

    invoke-virtual {v1}, LX/4pP;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_3
    :try_start_4
    sget-object v0, LX/4rD;->b:[I

    sget-object v1, LX/4pP;->CUSTOM:LX/4pP;

    invoke-virtual {v1}, LX/4pP;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    .line 815261
    :goto_4
    invoke-static {}, LX/4pO;->values()[LX/4pO;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/4rD;->a:[I

    :try_start_5
    sget-object v0, LX/4rD;->a:[I

    sget-object v1, LX/4pO;->WRAPPER_ARRAY:LX/4pO;

    invoke-virtual {v1}, LX/4pO;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_5
    :try_start_6
    sget-object v0, LX/4rD;->a:[I

    sget-object v1, LX/4pO;->PROPERTY:LX/4pO;

    invoke-virtual {v1}, LX/4pO;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_6
    :try_start_7
    sget-object v0, LX/4rD;->a:[I

    sget-object v1, LX/4pO;->WRAPPER_OBJECT:LX/4pO;

    invoke-virtual {v1}, LX/4pO;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_7
    :try_start_8
    sget-object v0, LX/4rD;->a:[I

    sget-object v1, LX/4pO;->EXTERNAL_PROPERTY:LX/4pO;

    invoke-virtual {v1}, LX/4pO;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_8
    return-void

    :catch_0
    goto :goto_8

    :catch_1
    goto :goto_7

    :catch_2
    goto :goto_6

    :catch_3
    goto :goto_5

    :catch_4
    goto :goto_4

    :catch_5
    goto :goto_3

    :catch_6
    goto :goto_2

    :catch_7
    goto :goto_1

    :catch_8
    goto :goto_0
.end method
