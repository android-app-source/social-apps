.class public LX/4V0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 742024
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 742025
    return-void
.end method

.method private static a(LX/15i;LX/0t8;ILX/4Ud;LX/4Uy;LX/4Uw;IZ[[II)I
    .locals 9

    .prologue
    .line 742015
    aget-object v8, p8, p9

    .line 742016
    const/4 v0, 0x1

    aget v0, v8, v0

    .line 742017
    invoke-interface {p3, v0}, LX/4Ud;->a(I)[[I

    move-result-object v7

    .line 742018
    if-eqz v7, :cond_0

    array-length v0, v7

    if-nez v0, :cond_1

    .line 742019
    :cond_0
    const/4 v0, 0x1

    .line 742020
    :goto_0
    return v0

    .line 742021
    :cond_1
    new-instance v0, LX/4Uu;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, LX/4Uu;-><init>(LX/15i;LX/0t8;ILX/4Ud;LX/4Uy;LX/4Uw;[[I)V

    .line 742022
    const/4 v5, 0x2

    const-class v6, LX/4Uz;

    move-object v1, p0

    move v2, p6

    move/from16 v3, p7

    move-object v4, v8

    invoke-static/range {v0 .. v6}, LX/4V0;->a(LX/4Us;LX/15i;IZ[IILjava/lang/Class;)V

    .line 742023
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(LX/15i;LX/0t8;ILX/4Uy;LX/4Uw;IZ[[II)I
    .locals 13

    .prologue
    .line 741993
    invoke-virtual/range {p4 .. p4}, LX/4Uw;->c()Z

    move-result v11

    .line 741994
    aget-object v5, p7, p8

    .line 741995
    invoke-virtual/range {p3 .. p3}, LX/4Uy;->c()V

    .line 741996
    const/4 v6, 0x2

    const-class v7, Ljava/lang/String;

    move-object/from16 v1, p3

    move-object v2, p0

    move/from16 v3, p5

    move/from16 v4, p6

    invoke-static/range {v1 .. v7}, LX/4V0;->a(LX/4Us;LX/15i;IZ[IILjava/lang/Class;)V

    .line 741997
    const/4 v2, 0x1

    .line 741998
    add-int/lit8 v1, p8, 0x1

    move v8, v1

    move v9, v2

    .line 741999
    :goto_0
    move-object/from16 v0, p7

    array-length v1, v0

    if-ge v8, v1, :cond_1

    aget-object v1, p7, v8

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-static {v1}, LX/4V0;->e(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 742000
    aget-object v5, p7, v8

    .line 742001
    const/4 v1, 0x0

    aget v1, v5, v1

    .line 742002
    const/4 v2, 0x1

    aget v2, v5, v2

    .line 742003
    invoke-virtual {p1, v2}, LX/0t8;->b(I)Ljava/lang/String;

    move-result-object v12

    .line 742004
    invoke-virtual {p1, v2}, LX/0t8;->c(I)Ljava/lang/Class;

    move-result-object v7

    .line 742005
    if-eqz v11, :cond_0

    invoke-static {v1, p2}, LX/4V0;->a(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 742006
    const/4 v1, 0x0

    move v10, v1

    :goto_1
    invoke-virtual/range {p3 .. p3}, LX/4Uy;->d()I

    move-result v1

    if-ge v10, v1, :cond_0

    .line 742007
    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, LX/4Uy;->c(I)LX/4Ux;

    move-result-object v1

    .line 742008
    iget-object v2, v1, LX/4Ux;->a:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v0, v12, v2}, LX/4Uw;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 742009
    iget v3, v1, LX/4Ux;->b:I

    const/4 v4, 0x0

    const/4 v6, 0x2

    move-object/from16 v1, p4

    move-object v2, p0

    invoke-static/range {v1 .. v7}, LX/4V0;->a(LX/4Us;LX/15i;IZ[IILjava/lang/Class;)V

    .line 742010
    add-int/lit8 v1, v10, 0x1

    move v10, v1

    goto :goto_1

    .line 742011
    :cond_0
    add-int/lit8 v1, v8, 0x1

    .line 742012
    add-int/lit8 v2, v9, 0x1

    move v8, v1

    move v9, v2

    .line 742013
    goto :goto_0

    .line 742014
    :cond_1
    return v9
.end method

.method private static a(LX/15i;Z)I
    .locals 2

    .prologue
    .line 741988
    invoke-virtual {p0}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 741989
    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 741990
    if-eqz p1, :cond_0

    .line 741991
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v0

    .line 741992
    :cond_0
    return v0
.end method

.method private static a(LX/4Ud;)LX/4Ud;
    .locals 1

    .prologue
    .line 741986
    instance-of v0, p0, LX/4Ue;

    if-eqz v0, :cond_0

    .line 741987
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/4Ue;

    invoke-direct {v0, p0}, LX/4Ue;-><init>(LX/4Ud;)V

    move-object p0, v0

    goto :goto_0
.end method

.method private static a(LX/15i;IIZLjava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 741960
    const-class v0, Ljava/lang/Boolean;

    if-ne p4, v0, :cond_0

    .line 741961
    invoke-virtual {p0, p1, p2}, LX/15i;->b(II)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 741962
    :goto_0
    return-object v0

    .line 741963
    :cond_0
    const-class v0, Ljava/lang/Integer;

    if-ne p4, v0, :cond_1

    .line 741964
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/15i;->a(III)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 741965
    :cond_1
    const-class v0, Ljava/lang/String;

    if-ne p4, v0, :cond_2

    .line 741966
    invoke-virtual {p0, p1, p2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 741967
    :cond_2
    const-class v0, Ljava/lang/Long;

    if-ne p4, v0, :cond_3

    .line 741968
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, LX/15i;->a(IIJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 741969
    :cond_3
    const-class v0, Ljava/lang/Double;

    if-ne p4, v0, :cond_4

    .line 741970
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, LX/15i;->a(IID)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 741971
    :cond_4
    const-class v0, Ljava/lang/Float;

    if-ne p4, v0, :cond_5

    .line 741972
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/15i;->a(IIF)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0

    .line 741973
    :cond_5
    const-class v0, Ljava/lang/Enum;

    invoke-virtual {v0, p4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 741974
    if-eqz p3, :cond_6

    .line 741975
    invoke-virtual {p0, p1, p2, p4}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    goto :goto_0

    .line 741976
    :cond_6
    invoke-virtual {p0, p1, p2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 741977
    const/4 v1, 0x0

    .line 741978
    if-nez v0, :cond_9

    .line 741979
    :goto_1
    move-object v0, v1

    .line 741980
    goto :goto_0

    .line 741981
    :cond_7
    const-class v0, LX/4Uz;

    if-ne p4, v0, :cond_8

    .line 741982
    invoke-virtual {p0, p1, p2}, LX/15i;->g(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 741983
    :cond_8
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 741984
    :cond_9
    :try_start_0
    invoke-static {p4, v0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_1

    .line 741985
    :catch_0
    goto :goto_1
.end method

.method public static a(LX/15i;IZLX/4Ud;LX/0t8;ILjava/util/Collection;Ljava/util/Collection;)V
    .locals 10
    .param p6    # Ljava/util/Collection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/util/Collection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15i;",
            "IZ",
            "LX/4Ud;",
            "Lcom/facebook/graphql/executor/cache/ConsistencyConfig;",
            "I",
            "Ljava/util/Collection",
            "<",
            "LX/4Zs;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 741949
    if-nez p7, :cond_0

    if-nez p6, :cond_0

    .line 741950
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Should specify one or more output collections"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 741951
    :cond_0
    if-nez p0, :cond_2

    .line 741952
    :cond_1
    :goto_0
    return-void

    .line 741953
    :cond_2
    invoke-static {p3}, LX/4V0;->a(LX/4Ud;)LX/4Ud;

    move-result-object v4

    .line 741954
    invoke-interface {v4, p1}, LX/4Ud;->a(I)[[I

    move-result-object v9

    .line 741955
    if-eqz v9, :cond_1

    array-length v1, v9

    if-eqz v1, :cond_1

    .line 741956
    invoke-static {p0, p2}, LX/4V0;->a(LX/15i;Z)I

    move-result v7

    .line 741957
    new-instance v5, LX/4Uy;

    move-object/from16 v0, p7

    invoke-direct {v5, v0}, LX/4Uy;-><init>(Ljava/util/Collection;)V

    .line 741958
    new-instance v6, LX/4Uw;

    move-object/from16 v0, p6

    invoke-direct {v6, v0}, LX/4Uw;-><init>(Ljava/util/Collection;)V

    move-object v1, p0

    move-object v2, p4

    move v3, p5

    move v8, p2

    .line 741959
    invoke-static/range {v1 .. v9}, LX/4V0;->b(LX/15i;LX/0t8;ILX/4Ud;LX/4Uy;LX/4Uw;IZ[[I)V

    goto :goto_0
.end method

.method private static a(LX/4Us;LX/15i;IZ[IILjava/lang/Class;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 741930
    array-length v1, p4

    add-int/lit8 v1, v1, -0x1

    if-ne p5, v1, :cond_1

    const/4 v1, 0x1

    move v7, v1

    .line 741931
    :goto_0
    if-eqz p3, :cond_3

    .line 741932
    invoke-virtual {p1, p2}, LX/15i;->c(I)I

    move-result v8

    move v6, v0

    .line 741933
    :goto_1
    if-ge v6, v8, :cond_4

    .line 741934
    invoke-virtual {p1, p2, v6}, LX/15i;->q(II)I

    move-result v2

    .line 741935
    if-eqz v2, :cond_0

    .line 741936
    invoke-interface {p0, v2}, LX/4Us;->a(I)V

    .line 741937
    if-eqz v7, :cond_2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p4

    move v4, p5

    move-object v5, p6

    .line 741938
    invoke-static/range {v0 .. v5}, LX/4V0;->b(LX/4Us;LX/15i;I[IILjava/lang/Class;)V

    .line 741939
    :goto_2
    invoke-interface {p0}, LX/4Us;->a()V

    .line 741940
    :cond_0
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    :cond_1
    move v7, v0

    .line 741941
    goto :goto_0

    :cond_2
    move-object v0, p0

    move-object v1, p1

    move-object v3, p4

    move v4, p5

    move-object v5, p6

    .line 741942
    invoke-static/range {v0 .. v5}, LX/4V0;->a(LX/4Us;LX/15i;I[IILjava/lang/Class;)V

    goto :goto_2

    .line 741943
    :cond_3
    invoke-interface {p0, p2}, LX/4Us;->a(I)V

    .line 741944
    if-eqz v7, :cond_5

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p4

    move v4, p5

    move-object v5, p6

    .line 741945
    invoke-static/range {v0 .. v5}, LX/4V0;->b(LX/4Us;LX/15i;I[IILjava/lang/Class;)V

    .line 741946
    :goto_3
    invoke-interface {p0}, LX/4Us;->a()V

    .line 741947
    :cond_4
    return-void

    :cond_5
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p4

    move v4, p5

    move-object v5, p6

    .line 741948
    invoke-static/range {v0 .. v5}, LX/4V0;->a(LX/4Us;LX/15i;I[IILjava/lang/Class;)V

    goto :goto_3
.end method

.method private static a(LX/4Us;LX/15i;I[IILjava/lang/Class;)V
    .locals 7

    .prologue
    .line 741872
    aget v0, p3, p4

    .line 741873
    invoke-static {v0}, LX/4V0;->c(I)I

    move-result v1

    .line 741874
    invoke-static {v0}, LX/4V0;->d(I)Z

    move-result v3

    .line 741875
    invoke-virtual {p1, p2, v1}, LX/15i;->g(II)I

    move-result v2

    .line 741876
    if-eqz v2, :cond_0

    .line 741877
    invoke-interface {p0, v0}, LX/4Us;->b(I)V

    .line 741878
    add-int/lit8 v5, p4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    move-object v6, p5

    invoke-static/range {v0 .. v6}, LX/4V0;->a(LX/4Us;LX/15i;IZ[IILjava/lang/Class;)V

    .line 741879
    invoke-interface {p0}, LX/4Us;->b()V

    .line 741880
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/flatbuffers/MutableFlattenable;Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 3
    .param p1    # Ljava/util/Collection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/Collection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            "Ljava/util/Collection",
            "<",
            "LX/4Zs;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 741925
    if-nez p2, :cond_0

    if-nez p1, :cond_0

    .line 741926
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Should specify one or more output collections"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 741927
    :cond_0
    instance-of v0, p0, Lcom/facebook/graphql/modelutil/FragmentModel;

    if-nez v0, :cond_1

    .line 741928
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Consistency not supported on anonymous/inner models. Please hoist "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " up to a named fragment"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 741929
    :cond_1
    return-void
.end method

.method public static a(Lcom/facebook/flatbuffers/MutableFlattenable;[[ILX/4Ud;LX/0t8;ILjava/util/Collection;Ljava/util/Collection;)V
    .locals 9
    .param p5    # Ljava/util/Collection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/util/Collection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            "[[I",
            "LX/4Ud;",
            "Lcom/facebook/graphql/executor/cache/ConsistencyConfig;",
            "I",
            "Ljava/util/Collection",
            "<",
            "LX/4Zs;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 741917
    if-nez p0, :cond_1

    .line 741918
    :cond_0
    :goto_0
    return-void

    .line 741919
    :cond_1
    invoke-static {p0, p5, p6}, LX/4V0;->a(Lcom/facebook/flatbuffers/MutableFlattenable;Ljava/util/Collection;Ljava/util/Collection;)V

    .line 741920
    invoke-interface {p0}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v0

    .line 741921
    if-eqz v0, :cond_0

    .line 741922
    new-instance v4, LX/4Uy;

    invoke-direct {v4, p6}, LX/4Uy;-><init>(Ljava/util/Collection;)V

    .line 741923
    new-instance v5, LX/4Uw;

    invoke-direct {v5, p5}, LX/4Uw;-><init>(Ljava/util/Collection;)V

    .line 741924
    invoke-static {p2}, LX/4V0;->a(LX/4Ud;)LX/4Ud;

    move-result-object v3

    invoke-interface {p0}, Lcom/facebook/flatbuffers/MutableFlattenable;->o_()I

    move-result v6

    const/4 v7, 0x0

    move-object v1, p3

    move v2, p4

    move-object v8, p1

    invoke-static/range {v0 .. v8}, LX/4V0;->b(LX/15i;LX/0t8;ILX/4Ud;LX/4Uy;LX/4Uw;IZ[[I)V

    goto :goto_0
.end method

.method private static a(II)Z
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 741910
    if-nez p1, :cond_1

    .line 741911
    :cond_0
    :goto_0
    return v0

    .line 741912
    :cond_1
    if-ne p1, v2, :cond_2

    .line 741913
    const/4 v2, 0x4

    if-eq p0, v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 741914
    :cond_2
    if-ne p1, v0, :cond_3

    .line 741915
    if-eq p0, v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 741916
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public static b(LX/15i;LX/0t8;ILX/4Ud;LX/4Uy;LX/4Uw;IZ[[I)V
    .locals 20

    .prologue
    .line 741893
    if-lez p6, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 741894
    if-eqz p8, :cond_0

    move-object/from16 v0, p8

    array-length v1, v0

    if-nez v1, :cond_2

    .line 741895
    :cond_0
    return-void

    .line 741896
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 741897
    :cond_2
    const/4 v9, 0x0

    .line 741898
    :goto_1
    move-object/from16 v0, p8

    array-length v1, v0

    if-ge v9, v1, :cond_0

    .line 741899
    aget-object v2, p8, v9

    .line 741900
    array-length v1, v2

    const/4 v3, 0x2

    if-le v1, v3, :cond_3

    const/4 v1, 0x1

    :goto_2
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 741901
    const/4 v1, 0x0

    aget v1, v2, v1

    .line 741902
    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v3, p2

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    .line 741903
    invoke-static/range {v1 .. v9}, LX/4V0;->a(LX/15i;LX/0t8;ILX/4Uy;LX/4Uw;IZ[[II)I

    move-result v1

    .line 741904
    :goto_3
    add-int/2addr v9, v1

    .line 741905
    goto :goto_1

    .line 741906
    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    .line 741907
    :cond_4
    const/4 v2, 0x3

    if-ne v1, v2, :cond_5

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move/from16 v12, p2

    move-object/from16 v13, p3

    move-object/from16 v14, p4

    move-object/from16 v15, p5

    move/from16 v16, p6

    move/from16 v17, p7

    move-object/from16 v18, p8

    move/from16 v19, v9

    .line 741908
    invoke-static/range {v10 .. v19}, LX/4V0;->a(LX/15i;LX/0t8;ILX/4Ud;LX/4Uy;LX/4Uw;IZ[[II)I

    move-result v1

    goto :goto_3

    .line 741909
    :cond_5
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1
.end method

.method private static b(LX/4Us;LX/15i;I[IILjava/lang/Class;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 741884
    array-length v0, p3

    add-int/lit8 v0, v0, -0x1

    if-ne p4, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 741885
    aget v0, p3, p4

    .line 741886
    invoke-static {v0}, LX/4V0;->c(I)I

    move-result v2

    .line 741887
    invoke-static {p1, p2, v2, v1, p5}, LX/4V0;->a(LX/15i;IIZLjava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    .line 741888
    invoke-interface {p0, v0}, LX/4Us;->b(I)V

    .line 741889
    invoke-interface {p0, v1}, LX/4Us;->a(Ljava/lang/Object;)V

    .line 741890
    invoke-interface {p0}, LX/4Us;->b()V

    .line 741891
    return-void

    :cond_0
    move v0, v1

    .line 741892
    goto :goto_0
.end method

.method public static c(I)I
    .locals 1

    .prologue
    .line 741883
    const v0, 0xfffffff

    and-int/2addr v0, p0

    return v0
.end method

.method public static d(I)Z
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 741882
    and-int v0, p0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static e(I)Z
    .locals 1

    .prologue
    .line 741881
    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 v0, 0x4

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
