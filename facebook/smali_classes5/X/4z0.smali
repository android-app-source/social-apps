.class public final LX/4z0;
.super LX/1M4;
.source ""

# interfaces
.implements LX/4yx;


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1M4",
        "<TV;>;",
        "LX/4yx",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public a:[LX/4yy;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LX/4yy",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/4z1;

.field private final c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TK;"
        }
    .end annotation
.end field

.field public d:I

.field public e:I

.field public f:LX/4yx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4yx",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private g:LX/4yx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4yx",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/4z1;Ljava/lang/Object;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 822209
    iput-object p1, p0, LX/4z0;->b:LX/4z1;

    invoke-direct {p0}, LX/1M4;-><init>()V

    .line 822210
    iput v0, p0, LX/4z0;->d:I

    .line 822211
    iput v0, p0, LX/4z0;->e:I

    .line 822212
    iput-object p2, p0, LX/4z0;->c:Ljava/lang/Object;

    .line 822213
    iput-object p0, p0, LX/4z0;->f:LX/4yx;

    .line 822214
    iput-object p0, p0, LX/4z0;->g:LX/4yx;

    .line 822215
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-static {p3, v0, v1}, LX/0PC;->a(ID)I

    move-result v0

    .line 822216
    new-array v0, v0, [LX/4yy;

    .line 822217
    iput-object v0, p0, LX/4z0;->a:[LX/4yy;

    .line 822218
    return-void
.end method

.method private c()I
    .locals 1

    .prologue
    .line 822208
    iget-object v0, p0, LX/4z0;->a:[LX/4yy;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method


# virtual methods
.method public final a()LX/4yx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4yx",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 822207
    iget-object v0, p0, LX/4z0;->g:LX/4yx;

    return-object v0
.end method

.method public final a(LX/4yx;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4yx",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822205
    iput-object p1, p0, LX/4z0;->g:LX/4yx;

    .line 822206
    return-void
.end method

.method public final add(Ljava/lang/Object;)Z
    .locals 11
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)Z"
        }
    .end annotation

    .prologue
    .line 822137
    invoke-static {p1}, LX/0PC;->a(Ljava/lang/Object;)I

    move-result v2

    .line 822138
    invoke-direct {p0}, LX/4z0;->c()I

    move-result v0

    and-int v3, v2, v0

    .line 822139
    iget-object v0, p0, LX/4z0;->a:[LX/4yy;

    aget-object v1, v0, v3

    move-object v0, v1

    .line 822140
    :goto_0
    if-eqz v0, :cond_1

    .line 822141
    invoke-virtual {v0, p1, v2}, LX/4yy;->a(Ljava/lang/Object;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 822142
    const/4 v0, 0x0

    .line 822143
    :goto_1
    return v0

    .line 822144
    :cond_0
    iget-object v0, v0, LX/4yy;->nextInValueBucket:LX/4yy;

    goto :goto_0

    .line 822145
    :cond_1
    new-instance v0, LX/4yy;

    iget-object v4, p0, LX/4z0;->c:Ljava/lang/Object;

    invoke-direct {v0, v4, p1, v2, v1}, LX/4yy;-><init>(Ljava/lang/Object;Ljava/lang/Object;ILX/4yy;)V

    .line 822146
    iget-object v1, p0, LX/4z0;->g:LX/4yx;

    invoke-static {v1, v0}, LX/4z1;->b(LX/4yx;LX/4yx;)V

    .line 822147
    invoke-static {v0, p0}, LX/4z1;->b(LX/4yx;LX/4yx;)V

    .line 822148
    iget-object v1, p0, LX/4z0;->b:LX/4z1;

    iget-object v1, v1, LX/4z1;->b:LX/4yy;

    .line 822149
    iget-object v2, v1, LX/4yy;->predecessorInMultimap:LX/4yy;

    move-object v1, v2

    .line 822150
    invoke-static {v1, v0}, LX/4z1;->b(LX/4yy;LX/4yy;)V

    .line 822151
    iget-object v1, p0, LX/4z0;->b:LX/4z1;

    iget-object v1, v1, LX/4z1;->b:LX/4yy;

    invoke-static {v0, v1}, LX/4z1;->b(LX/4yy;LX/4yy;)V

    .line 822152
    iget-object v1, p0, LX/4z0;->a:[LX/4yy;

    aput-object v0, v1, v3

    .line 822153
    iget v0, p0, LX/4z0;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4z0;->d:I

    .line 822154
    iget v0, p0, LX/4z0;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4z0;->e:I

    .line 822155
    iget v5, p0, LX/4z0;->d:I

    iget-object v6, p0, LX/4z0;->a:[LX/4yy;

    array-length v6, v6

    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    invoke-static {v5, v6, v7, v8}, LX/0PC;->a(IID)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 822156
    iget-object v5, p0, LX/4z0;->a:[LX/4yy;

    array-length v5, v5

    mul-int/lit8 v5, v5, 0x2

    new-array v7, v5, [LX/4yy;

    .line 822157
    iput-object v7, p0, LX/4z0;->a:[LX/4yy;

    .line 822158
    array-length v5, v7

    add-int/lit8 v8, v5, -0x1

    .line 822159
    iget-object v6, p0, LX/4z0;->f:LX/4yx;

    .line 822160
    :goto_2
    if-eq v6, p0, :cond_2

    move-object v5, v6

    .line 822161
    check-cast v5, LX/4yy;

    .line 822162
    iget v9, v5, LX/4yy;->smearedValueHash:I

    and-int/2addr v9, v8

    .line 822163
    aget-object v10, v7, v9

    iput-object v10, v5, LX/4yy;->nextInValueBucket:LX/4yy;

    .line 822164
    aput-object v5, v7, v9

    .line 822165
    invoke-interface {v6}, LX/4yx;->b()LX/4yx;

    move-result-object v6

    goto :goto_2

    .line 822166
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public final b()LX/4yx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4yx",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 822204
    iget-object v0, p0, LX/4z0;->f:LX/4yx;

    return-object v0
.end method

.method public final b(LX/4yx;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4yx",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822219
    iput-object p1, p0, LX/4z0;->f:LX/4yx;

    .line 822220
    return-void
.end method

.method public final clear()V
    .locals 2

    .prologue
    .line 822194
    iget-object v0, p0, LX/4z0;->a:[LX/4yy;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 822195
    const/4 v0, 0x0

    iput v0, p0, LX/4z0;->d:I

    .line 822196
    iget-object v1, p0, LX/4z0;->f:LX/4yx;

    .line 822197
    :goto_0
    if-eq v1, p0, :cond_0

    move-object v0, v1

    .line 822198
    check-cast v0, LX/4yy;

    .line 822199
    invoke-static {v0}, LX/4z1;->b(LX/4yy;)V

    .line 822200
    invoke-interface {v1}, LX/4yx;->b()LX/4yx;

    move-result-object v1

    goto :goto_0

    .line 822201
    :cond_0
    invoke-static {p0, p0}, LX/4z1;->b(LX/4yx;LX/4yx;)V

    .line 822202
    iget v0, p0, LX/4z0;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4z0;->e:I

    .line 822203
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 822186
    invoke-static {p1}, LX/0PC;->a(Ljava/lang/Object;)I

    move-result v1

    .line 822187
    iget-object v0, p0, LX/4z0;->a:[LX/4yy;

    invoke-direct {p0}, LX/4z0;->c()I

    move-result v2

    and-int/2addr v2, v1

    aget-object v0, v0, v2

    .line 822188
    :goto_0
    if-eqz v0, :cond_1

    .line 822189
    invoke-virtual {v0, p1, v1}, LX/4yy;->a(Ljava/lang/Object;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 822190
    const/4 v0, 0x1

    .line 822191
    :goto_1
    return v0

    .line 822192
    :cond_0
    iget-object v0, v0, LX/4yy;->nextInValueBucket:LX/4yy;

    goto :goto_0

    .line 822193
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 822185
    new-instance v0, LX/4yz;

    invoke-direct {v0, p0}, LX/4yz;-><init>(LX/4z0;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 822168
    invoke-static {p1}, LX/0PC;->a(Ljava/lang/Object;)I

    move-result v2

    .line 822169
    invoke-direct {p0}, LX/4z0;->c()I

    move-result v0

    and-int v3, v2, v0

    .line 822170
    const/4 v1, 0x0

    .line 822171
    iget-object v0, p0, LX/4z0;->a:[LX/4yy;

    aget-object v0, v0, v3

    .line 822172
    :goto_0
    if-eqz v0, :cond_2

    .line 822173
    invoke-virtual {v0, p1, v2}, LX/4yy;->a(Ljava/lang/Object;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 822174
    if-nez v1, :cond_0

    .line 822175
    iget-object v1, p0, LX/4z0;->a:[LX/4yy;

    iget-object v2, v0, LX/4yy;->nextInValueBucket:LX/4yy;

    aput-object v2, v1, v3

    .line 822176
    :goto_1
    invoke-interface {v0}, LX/4yx;->a()LX/4yx;

    move-result-object v1

    invoke-interface {v0}, LX/4yx;->b()LX/4yx;

    move-result-object v2

    invoke-static {v1, v2}, LX/4z1;->b(LX/4yx;LX/4yx;)V

    .line 822177
    invoke-static {v0}, LX/4z1;->b(LX/4yy;)V

    .line 822178
    iget v0, p0, LX/4z0;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/4z0;->d:I

    .line 822179
    iget v0, p0, LX/4z0;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4z0;->e:I

    .line 822180
    const/4 v0, 0x1

    .line 822181
    :goto_2
    return v0

    .line 822182
    :cond_0
    iget-object v2, v0, LX/4yy;->nextInValueBucket:LX/4yy;

    iput-object v2, v1, LX/4yy;->nextInValueBucket:LX/4yy;

    goto :goto_1

    .line 822183
    :cond_1
    iget-object v1, v0, LX/4yy;->nextInValueBucket:LX/4yy;

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_0

    .line 822184
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 822167
    iget v0, p0, LX/4z0;->d:I

    return v0
.end method
