.class public final LX/3db;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/friends/model/PersonYouMayKnow;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;Z)V
    .locals 0

    .prologue
    .line 617189
    iput-object p1, p0, LX/3db;->b:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iput-boolean p2, p0, LX/3db;->a:Z

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 617204
    iget-object v0, p0, LX/3db;->b:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    .line 617205
    iget-object v1, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    .line 617206
    iget-object v2, v1, LX/3Te;->g:LX/3UK;

    invoke-virtual {v2}, LX/3UK;->g()I

    move-result v2

    if-lez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 617207
    if-nez v1, :cond_0

    .line 617208
    invoke-static {v0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->O(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    .line 617209
    :goto_1
    iget-object v0, p0, LX/3db;->b:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v0, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    .line 617210
    iget-object v1, v0, LX/3Te;->g:LX/3UK;

    .line 617211
    sget-object v0, LX/3UL;->FAILURE:LX/3UL;

    iput-object v0, v1, LX/3UK;->h:LX/3UL;

    .line 617212
    iget-object v0, v1, LX/3UK;->e:LX/3Tg;

    invoke-interface {v0}, LX/3Tg;->e()V

    .line 617213
    return-void

    .line 617214
    :cond_0
    iget-object v1, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ai:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const v2, 0x7f08006f

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance p1, LX/Ivs;

    invoke-direct {p1, v0}, LX/Ivs;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    invoke-virtual {v1, v2, p1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 617190
    check-cast p1, Ljava/util/List;

    .line 617191
    iget-boolean v0, p0, LX/3db;->a:Z

    if-eqz v0, :cond_0

    .line 617192
    iget-object v0, p0, LX/3db;->b:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->b(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;Ljava/util/List;)V

    .line 617193
    :cond_0
    iget-object v0, p0, LX/3db;->b:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v0, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    .line 617194
    iget-object v2, v0, LX/3Te;->g:LX/3UK;

    .line 617195
    sget-object v3, LX/3UL;->SUCCESS:LX/3UL;

    iput-object v3, v2, LX/3UK;->h:LX/3UL;

    .line 617196
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/friends/model/PersonYouMayKnow;

    .line 617197
    iget-object v5, v2, LX/3UK;->g:Ljava/util/Set;

    invoke-virtual {v3}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 617198
    iget-object v5, v2, LX/3UK;->f:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 617199
    iget-object v5, v2, LX/3UK;->g:Ljava/util/Set;

    invoke-virtual {v3}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 617200
    :cond_2
    iget-object v3, v2, LX/3UK;->e:LX/3Tg;

    invoke-interface {v3}, LX/3Tg;->e()V

    .line 617201
    iget-object v0, p0, LX/3db;->b:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v0, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    invoke-virtual {v0}, LX/3Te;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 617202
    iget-object v0, p0, LX/3db;->b:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-static {v0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->O(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    .line 617203
    :cond_3
    return-void
.end method
