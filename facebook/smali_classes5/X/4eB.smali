.class public LX/4eB;
.super LX/1bZ;
.source ""


# static fields
.field private static final i:LX/4eB;


# instance fields
.field public final g:I

.field public final h:Ljava/lang/Integer;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "forcedAnimationCode"
        processor = "com.facebook.thecount.transformer.Transformer"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 796967
    invoke-static {}, LX/4eB;->newBuilder()LX/4eC;

    move-result-object v0

    invoke-virtual {v0}, LX/4eC;->l()LX/4eB;

    move-result-object v0

    sput-object v0, LX/4eB;->i:LX/4eB;

    return-void
.end method

.method public constructor <init>(LX/4eC;)V
    .locals 1

    .prologue
    .line 796959
    invoke-direct {p0, p1}, LX/1bZ;-><init>(LX/1ba;)V

    .line 796960
    iget-object v0, p1, LX/4eC;->c:Ljava/lang/Integer;

    move-object v0, v0

    .line 796961
    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 796962
    iget v0, p1, LX/4eC;->b:I

    move v0, v0

    .line 796963
    iput v0, p0, LX/4eB;->g:I

    .line 796964
    iget-object v0, p1, LX/4eC;->c:Ljava/lang/Integer;

    move-object v0, v0

    .line 796965
    iput-object v0, p0, LX/4eB;->h:Ljava/lang/Integer;

    .line 796966
    return-void
.end method

.method public static newBuilder()LX/4eC;
    .locals 1

    .prologue
    .line 796974
    new-instance v0, LX/4eC;

    invoke-direct {v0}, LX/4eC;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 796975
    if-ne p0, p1, :cond_1

    const/4 v0, 0x1

    .line 796976
    :cond_0
    :goto_0
    return v0

    .line 796977
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 796978
    invoke-super {p0, p1}, LX/1bZ;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 796979
    check-cast p1, LX/4eB;

    .line 796980
    iget v1, p0, LX/4eB;->g:I

    iget v2, p1, LX/4eB;->g:I

    if-ne v1, v2, :cond_0

    .line 796981
    iget-object v0, p0, LX/4eB;->h:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p1, LX/4eB;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, LX/3CW;->c(II)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 796969
    invoke-super {p0}, LX/1bZ;->hashCode()I

    move-result v0

    .line 796970
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/4eB;->g:I

    add-int/2addr v0, v1

    .line 796971
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, LX/4eB;->h:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, -0x1

    invoke-static {v0, v2}, LX/3CW;->c(II)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/4eB;->h:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, LX/3CW;->b(I)I

    move-result v0

    :goto_0
    add-int/2addr v0, v1

    .line 796972
    return v0

    .line 796973
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 796968
    const/4 v0, 0x0

    const-string v1, "%x-%s %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, LX/4eB;->g:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LX/4eB;->h:Ljava/lang/Integer;

    invoke-static {v4}, LX/4tq;->a(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-super {p0}, LX/1bZ;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
