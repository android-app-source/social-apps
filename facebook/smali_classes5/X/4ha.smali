.class public final LX/4ha;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:Z

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 801522
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 801523
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 801524
    if-nez v1, :cond_0

    .line 801525
    const-string v0, "Unable to proceed with no extras in Intent"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/4hf;->a(Ljava/lang/String;[Ljava/lang/Object;)LX/4hf;

    move-result-object v0

    .line 801526
    new-instance v1, LX/4he;

    .line 801527
    iget-object v2, v0, LX/4hf;->a:Landroid/os/Bundle;

    move-object v0, v2

    .line 801528
    invoke-direct {v1, v0}, LX/4he;-><init>(Landroid/os/Bundle;)V

    throw v1

    .line 801529
    :cond_0
    const-string v0, "com.facebook.platform.protocol.PROTOCOL_VERSION"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 801530
    instance-of v2, v0, Ljava/lang/Integer;

    if-nez v2, :cond_1

    .line 801531
    const-string v1, "com.facebook.platform.protocol.PROTOCOL_VERSION"

    const-class v2, Ljava/lang/Integer;

    invoke-static {v1, v2, v0}, LX/4hf;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)LX/4hf;

    move-result-object v0

    .line 801532
    new-instance v1, LX/4he;

    .line 801533
    iget-object v2, v0, LX/4hf;->a:Landroid/os/Bundle;

    move-object v0, v2

    .line 801534
    invoke-direct {v1, v0}, LX/4he;-><init>(Landroid/os/Bundle;)V

    throw v1

    .line 801535
    :cond_1
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/4ha;->b:I

    .line 801536
    sget-object v0, LX/25y;->a:Ljava/util/List;

    iget v2, p0, LX/4ha;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 801537
    const-string v0, "Unknown protocol version extra \'%s\': %d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "com.facebook.platform.protocol.PROTOCOL_VERSION"

    aput-object v2, v1, v3

    iget v2, p0, LX/4ha;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, LX/4hf;->a(Ljava/lang/String;[Ljava/lang/Object;)LX/4hf;

    move-result-object v0

    .line 801538
    new-instance v1, LX/4he;

    .line 801539
    iget-object v2, v0, LX/4hf;->a:Landroid/os/Bundle;

    move-object v0, v2

    .line 801540
    invoke-direct {v1, v0}, LX/4he;-><init>(Landroid/os/Bundle;)V

    throw v1

    .line 801541
    :cond_2
    const-string v0, "com.facebook.platform.extra.APPLICATION_ID"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 801542
    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_3

    .line 801543
    const-string v1, "com.facebook.platform.extra.APPLICATION_ID"

    const-class v2, Ljava/lang/String;

    invoke-static {v1, v2, v0}, LX/4hf;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)LX/4hf;

    move-result-object v0

    .line 801544
    new-instance v1, LX/4he;

    .line 801545
    iget-object v2, v0, LX/4hf;->a:Landroid/os/Bundle;

    move-object v0, v2

    .line 801546
    invoke-direct {v1, v0}, LX/4he;-><init>(Landroid/os/Bundle;)V

    throw v1

    .line 801547
    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/4ha;->e:Ljava/lang/String;

    .line 801548
    const-string v0, "com.facebook.platform.extra.METADATA"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 801549
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 801550
    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/4ha;->i:Ljava/lang/String;

    .line 801551
    :cond_4
    const-string v0, "com.facebook.platform.protocol.PROTOCOL_ACTION"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/4ha;->h:Ljava/lang/String;

    .line 801552
    const-string v0, "composer_session_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/4ha;->j:Ljava/lang/String;

    .line 801553
    const-string v0, "com.facebook.platform.protocol.BRIDGE_ARGS"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 801554
    if-eqz v0, :cond_6

    iget v1, p0, LX/4ha;->b:I

    const v2, 0x133529d

    if-lt v1, v2, :cond_6

    .line 801555
    iput-boolean v4, p0, LX/4ha;->c:Z

    .line 801556
    const-string v1, "app_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/4ha;->f:Ljava/lang/String;

    .line 801557
    const-string v1, "action_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/4ha;->a:Ljava/lang/String;

    .line 801558
    :goto_0
    iget-object v0, p0, LX/4ha;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 801559
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/4ha;->a:Ljava/lang/String;

    .line 801560
    :cond_5
    return-void

    .line 801561
    :cond_6
    const-string v0, "com.facebook.platform.extra.APPLICATION_NAME"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/4ha;->f:Ljava/lang/String;

    .line 801562
    const-string v0, "com.facebook.platform.protocol.CALL_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/4ha;->a:Ljava/lang/String;

    goto :goto_0
.end method
