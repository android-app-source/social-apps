.class public final LX/3xa;
.super LX/3tz;
.source ""


# instance fields
.field public b:I


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 658918
    invoke-direct {p0, p1, p2}, LX/3tz;-><init>(II)V

    .line 658919
    const/4 v0, 0x0

    iput v0, p0, LX/3xa;->b:I

    .line 658920
    const v0, 0x800013

    iput v0, p0, LX/3xa;->a:I

    .line 658921
    return-void
.end method

.method public constructor <init>(III)V
    .locals 1

    .prologue
    .line 658914
    invoke-direct {p0, p1, p2}, LX/3tz;-><init>(II)V

    .line 658915
    const/4 v0, 0x0

    iput v0, p0, LX/3xa;->b:I

    .line 658916
    iput p3, p0, LX/3xa;->a:I

    .line 658917
    return-void
.end method

.method public constructor <init>(LX/3tz;)V
    .locals 1

    .prologue
    .line 658892
    invoke-direct {p0, p1}, LX/3tz;-><init>(LX/3tz;)V

    .line 658893
    const/4 v0, 0x0

    iput v0, p0, LX/3xa;->b:I

    .line 658894
    return-void
.end method

.method public constructor <init>(LX/3xa;)V
    .locals 1

    .prologue
    .line 658910
    invoke-direct {p0, p1}, LX/3tz;-><init>(LX/3tz;)V

    .line 658911
    const/4 v0, 0x0

    iput v0, p0, LX/3xa;->b:I

    .line 658912
    iget v0, p1, LX/3xa;->b:I

    iput v0, p0, LX/3xa;->b:I

    .line 658913
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 658907
    invoke-direct {p0, p1, p2}, LX/3tz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 658908
    const/4 v0, 0x0

    iput v0, p0, LX/3xa;->b:I

    .line 658909
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 658904
    invoke-direct {p0, p1}, LX/3tz;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 658905
    const/4 v0, 0x0

    iput v0, p0, LX/3xa;->b:I

    .line 658906
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .locals 1

    .prologue
    .line 658900
    invoke-direct {p0, p1}, LX/3tz;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 658901
    const/4 v0, 0x0

    iput v0, p0, LX/3xa;->b:I

    .line 658902
    invoke-direct {p0, p1}, LX/3xa;->a(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 658903
    return-void
.end method

.method private a(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .locals 1

    .prologue
    .line 658895
    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iput v0, p0, LX/3xa;->leftMargin:I

    .line 658896
    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iput v0, p0, LX/3xa;->topMargin:I

    .line 658897
    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iput v0, p0, LX/3xa;->rightMargin:I

    .line 658898
    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    iput v0, p0, LX/3xa;->bottomMargin:I

    .line 658899
    return-void
.end method
