.class public LX/3l1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 633477
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 633478
    iput-object p1, p0, LX/3l1;->a:LX/0Zb;

    .line 633479
    return-void
.end method

.method public static a(LX/0QB;)LX/3l1;
    .locals 1

    .prologue
    .line 633495
    invoke-static {p0}, LX/3l1;->b(LX/0QB;)LX/3l1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/BOR;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 633494
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {p0}, LX/BOR;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/3l1;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 633487
    const-string v0, "slideshow"

    .line 633488
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 633489
    iget-object v0, p0, LX/3l1;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 633490
    iget-object v0, p0, LX/3l1;->b:Ljava/lang/String;

    .line 633491
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 633492
    :cond_0
    iget-object v0, p0, LX/3l1;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 633493
    return-void
.end method

.method public static b(LX/0QB;)LX/3l1;
    .locals 2

    .prologue
    .line 633485
    new-instance v1, LX/3l1;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/3l1;-><init>(LX/0Zb;)V

    .line 633486
    return-object v1
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 633483
    sget-object v0, LX/BOR;->SLIDESHOW_PREVIEW_SAVED:LX/BOR;

    invoke-static {v0}, LX/3l1;->a(LX/BOR;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "photo_count"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-static {p0, v0}, LX/3l1;->a(LX/3l1;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 633484
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 633480
    iput-object p1, p0, LX/3l1;->b:Ljava/lang/String;

    .line 633481
    sget-object v0, LX/BOR;->SLIDESHOW_POSTED:LX/BOR;

    invoke-static {v0}, LX/3l1;->a(LX/BOR;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "photo_count"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-static {p0, v0}, LX/3l1;->a(LX/3l1;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 633482
    return-void
.end method
