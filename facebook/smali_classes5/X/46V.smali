.class public LX/46V;
.super LX/46S;
.source ""


# instance fields
.field public final a:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 671230
    invoke-direct {p0}, LX/46S;-><init>()V

    .line 671231
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LX/46V;->a:Landroid/os/Bundle;

    .line 671232
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 671233
    iget-object v0, p0, LX/46V;->a:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 671234
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 671235
    instance-of v0, p1, LX/46V;

    if-nez v0, :cond_0

    .line 671236
    const/4 v0, 0x0

    .line 671237
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/46V;->a:Landroid/os/Bundle;

    check-cast p1, LX/46V;

    iget-object v1, p1, LX/46V;->a:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 671238
    iget-object v0, p0, LX/46V;->a:Landroid/os/Bundle;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 671239
    iget-object v0, p0, LX/46V;->a:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
