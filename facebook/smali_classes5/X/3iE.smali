.class public final LX/3iE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;

.field private final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/1Ps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)V"
        }
    .end annotation

    .prologue
    .line 628911
    iput-object p1, p0, LX/3iE;->a:Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 628912
    iput-object p2, p0, LX/3iE;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 628913
    iput-object p3, p0, LX/3iE;->c:LX/1Ps;

    .line 628914
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x62d4a39f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 628915
    iget-object v0, p0, LX/3iE;->a:Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3iF;

    iget-object v2, p0, LX/3iE;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/3iE;->c:LX/1Ps;

    .line 628916
    new-instance v5, LX/4At;

    iget-object v6, v0, LX/3iF;->j:Landroid/content/Context;

    const v7, 0x7f080037

    invoke-direct {v5, v6, v7}, LX/4At;-><init>(Landroid/content/Context;I)V

    iput-object v5, v0, LX/3iF;->l:LX/4At;

    .line 628917
    iget-object v5, v0, LX/3iF;->l:LX/4At;

    invoke-virtual {v5}, LX/4At;->beginShowingProgress()V

    .line 628918
    iput-object v2, v0, LX/3iF;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 628919
    iput-object v3, v0, LX/3iF;->o:LX/1Ps;

    .line 628920
    iget-object v5, v0, LX/3iF;->k:Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;

    invoke-virtual {v5}, LX/1OM;->ij_()I

    move-result v5

    if-eqz v5, :cond_1

    .line 628921
    invoke-static {v0}, LX/3iF;->d$redex0(LX/3iF;)V

    .line 628922
    iget-object v5, v0, LX/3iF;->k:Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;

    new-instance v6, LX/CDo;

    invoke-direct {v6, v0}, LX/CDo;-><init>(LX/3iF;)V

    .line 628923
    iput-object v6, v5, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;->c:LX/CDo;

    .line 628924
    invoke-static {v0}, LX/3iF;->b$redex0(LX/3iF;)V

    .line 628925
    :cond_0
    :goto_0
    const v0, -0x783ca6f3

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 628926
    :cond_1
    invoke-static {v2}, LX/3iF;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 628927
    iget-object v5, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 628928
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    .line 628929
    iget-object v6, v0, LX/3iF;->b:LX/3iK;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v5

    sget-object v7, Lcom/facebook/common/callercontext/CallerContext;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 628930
    new-instance v8, LX/CDl;

    invoke-direct {v8, v0}, LX/CDl;-><init>(LX/3iF;)V

    move-object v8, v8

    .line 628931
    iget-object p0, v6, LX/3iK;->e:LX/3H7;

    .line 628932
    new-instance p1, LX/5Gz;

    invoke-direct {p1}, LX/5Gz;-><init>()V

    .line 628933
    iput-object v5, p1, LX/5Gz;->a:Ljava/lang/String;

    .line 628934
    move-object p1, p1

    .line 628935
    iget-object v0, p1, LX/5Gz;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 628936
    new-instance v0, Lcom/facebook/api/ufiservices/common/FetchPagesYouCanActParams;

    invoke-direct {v0, p1}, Lcom/facebook/api/ufiservices/common/FetchPagesYouCanActParams;-><init>(LX/5Gz;)V

    move-object p1, v0

    .line 628937
    iget-object v0, p0, LX/3H7;->j:LX/0tX;

    iget-object v2, p0, LX/3H7;->d:LX/3H9;

    .line 628938
    new-instance v3, LX/6Ae;

    invoke-direct {v3}, LX/6Ae;-><init>()V

    move-object v3, v3

    .line 628939
    const-string p0, "feedback_id"

    .line 628940
    iget-object v5, p1, Lcom/facebook/api/ufiservices/common/FetchPagesYouCanActParams;->a:Ljava/lang/String;

    move-object v5, v5

    .line 628941
    invoke-virtual {v3, p0, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 628942
    move-object v3, v3

    .line 628943
    const-class p0, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {p0}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object p0

    move-object p0, p0

    .line 628944
    invoke-static {v3, p0}, LX/0zO;->a(LX/0gW;LX/0w5;)LX/0zO;

    move-result-object v3

    .line 628945
    iput-object v7, v3, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 628946
    move-object v3, v3

    .line 628947
    sget-object p0, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v3, p0}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v3

    sget-object p0, LX/0zS;->a:LX/0zS;

    invoke-virtual {v3, p0}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    iget-object p0, v2, LX/3H9;->a:LX/0SI;

    invoke-interface {p0}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object p0

    .line 628948
    iput-object p0, v3, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 628949
    move-object v3, v3

    .line 628950
    move-object p1, v3

    .line 628951
    invoke-virtual {v0, p1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object p1

    invoke-static {p1}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p1

    move-object p0, p1

    .line 628952
    iget-object p1, v6, LX/3iK;->j:LX/1Ck;

    const-string v0, "fetch_pages_you_can_act"

    invoke-virtual {p1, v0, p0, v8}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 628953
    goto :goto_0
.end method
