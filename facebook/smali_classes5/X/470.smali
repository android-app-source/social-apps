.class public LX/470;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 671825
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/view/Window;)V
    .locals 2

    .prologue
    .line 671842
    const/16 v0, 0x10

    invoke-static {v0}, LX/470;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 671843
    const/16 v0, 0x504

    .line 671844
    const/16 v1, 0x13

    invoke-static {v1}, LX/470;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 671845
    const/16 v0, 0x1504

    .line 671846
    :cond_0
    invoke-virtual {p0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 671847
    :goto_0
    return-void

    .line 671848
    :cond_1
    const/16 v0, 0x800

    invoke-virtual {p0, v0}, Landroid/view/Window;->clearFlags(I)V

    .line 671849
    const/16 v0, 0x400

    invoke-virtual {p0, v0}, Landroid/view/Window;->addFlags(I)V

    goto :goto_0
.end method

.method public static a(Landroid/view/Window;I)V
    .locals 1

    .prologue
    .line 671833
    const/16 v0, 0x15

    invoke-static {v0}, LX/470;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 671834
    :goto_0
    return-void

    .line 671835
    :cond_0
    const/high16 v0, -0x80000000

    invoke-virtual {p0, v0}, Landroid/view/Window;->addFlags(I)V

    .line 671836
    invoke-virtual {p0, p1}, Landroid/view/Window;->setStatusBarColor(I)V

    goto :goto_0
.end method

.method public static b(Landroid/view/Window;)V
    .locals 2

    .prologue
    .line 671837
    const/16 v0, 0x10

    invoke-static {v0}, LX/470;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 671838
    invoke-virtual {p0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 671839
    :goto_0
    return-void

    .line 671840
    :cond_0
    const/16 v0, 0x400

    invoke-virtual {p0, v0}, Landroid/view/Window;->clearFlags(I)V

    .line 671841
    const/16 v0, 0x800

    invoke-virtual {p0, v0}, Landroid/view/Window;->addFlags(I)V

    goto :goto_0
.end method

.method public static b(Landroid/view/Window;I)V
    .locals 2

    .prologue
    .line 671827
    const/16 v0, 0x15

    invoke-static {v0}, LX/470;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 671828
    :goto_0
    return-void

    .line 671829
    :cond_0
    invoke-virtual {p0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 671830
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 671831
    invoke-virtual {v0, p1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 671832
    new-instance v1, LX/46z;

    invoke-direct {v1, p1, p0}, LX/46z;-><init>(ILandroid/view/Window;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    goto :goto_0
.end method

.method private static b(I)Z
    .locals 1

    .prologue
    .line 671826
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
