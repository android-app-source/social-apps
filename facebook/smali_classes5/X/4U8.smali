.class public LX/4U8;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 720789
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 720769
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 720770
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 720771
    :goto_0
    return v1

    .line 720772
    :cond_0
    const-string v10, "fetchTimeMs"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 720773
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v6

    .line 720774
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_4

    .line 720775
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 720776
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 720777
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 720778
    const-string v10, "cache_id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 720779
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 720780
    :cond_2
    const-string v10, "debug_info"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 720781
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 720782
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 720783
    :cond_4
    const/4 v9, 0x3

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 720784
    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 720785
    invoke-virtual {p1, v6, v7}, LX/186;->b(II)V

    .line 720786
    if-eqz v0, :cond_5

    .line 720787
    const/4 v1, 0x2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 720788
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move-wide v2, v4

    move v7, v1

    move v8, v1

    goto :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 720739
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 720740
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 720741
    invoke-static {p0, v2}, LX/4U8;->a(LX/15w;LX/186;)I

    move-result v1

    .line 720742
    if-eqz v0, :cond_0

    .line 720743
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 720744
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 720745
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 720746
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 720747
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 720748
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 720749
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 720750
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 720751
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720752
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 720753
    const-string v0, "name"

    const-string v1, "UnknownFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 720754
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 720755
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 720756
    if-eqz v0, :cond_0

    .line 720757
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720758
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 720759
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 720760
    if-eqz v0, :cond_1

    .line 720761
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720762
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 720763
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 720764
    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    .line 720765
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720766
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 720767
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 720768
    return-void
.end method
