.class public LX/3Ub;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/3Ub;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 586548
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/3Ub;
    .locals 3

    .prologue
    .line 586549
    sget-object v0, LX/3Ub;->a:LX/3Ub;

    if-nez v0, :cond_1

    .line 586550
    const-class v1, LX/3Ub;

    monitor-enter v1

    .line 586551
    :try_start_0
    sget-object v0, LX/3Ub;->a:LX/3Ub;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 586552
    if-eqz v2, :cond_0

    .line 586553
    :try_start_1
    new-instance v0, LX/3Ub;

    invoke-direct {v0}, LX/3Ub;-><init>()V

    .line 586554
    move-object v0, v0

    .line 586555
    sput-object v0, LX/3Ub;->a:LX/3Ub;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 586556
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 586557
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 586558
    :cond_1
    sget-object v0, LX/3Ub;->a:LX/3Ub;

    return-object v0

    .line 586559
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 586560
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 16

    .prologue
    .line 586561
    sget-object v1, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->a:LX/1Cz;

    sget-object v2, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->a:LX/1Cz;

    sget-object v3, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkHeaderPartDefinition;->a:LX/1Cz;

    sget-object v4, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;->a:LX/1Cz;

    sget-object v5, Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;->b:LX/1Cz;

    sget-object v6, Lcom/facebook/productionprompts/ClipboardPromptsPromptPartDefinition;->a:LX/1Cz;

    sget-object v7, Lcom/facebook/feed/photoreminder/MediaReminderPromptsPromptPartDefinition;->a:LX/1Cz;

    sget-object v8, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptsPromptPartDefinition;->a:LX/1Cz;

    sget-object v9, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;->a:LX/1Cz;

    sget-object v10, Lcom/facebook/friendsharing/meme/prompt/MemePromptPartDefinition;->a:LX/1Cz;

    sget-object v11, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;->a:LX/1Cz;

    sget-object v12, Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;->a:LX/1Cz;

    const/16 v13, 0xb

    new-array v13, v13, [LX/1Cz;

    const/4 v14, 0x0

    sget-object v15, Lcom/facebook/productionprompts/v3/ProductionPromptSmallPartDefintion;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x1

    sget-object v15, Lcom/facebook/productionprompts/v3/ClipboardPromptSmallPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x2

    sget-object v15, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSmallPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x3

    sget-object v15, Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x4

    sget-object v15, Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x5

    sget-object v15, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x6

    sget-object v15, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptLargePartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x7

    sget-object v15, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/v3/SuggestedCoverPhotosPromptLargePartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x8

    sget-object v15, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptLargePartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x9

    sget-object v15, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptTombstonePartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0xa

    sget-object v15, Lcom/facebook/goodfriends/camera/GoodFriendsComposerCameraPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    invoke-static/range {v1 .. v13}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 586562
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Cz;

    .line 586563
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, LX/1KB;->a(LX/1Cz;)V

    .line 586564
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 586565
    :cond_0
    return-void
.end method
