.class public LX/3Vc;
.super LX/3Va;
.source ""


# static fields
.field public static final j:LX/1Cz;


# instance fields
.field public k:LX/C01;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final l:Landroid/widget/Space;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587994
    new-instance v0, LX/3Vd;

    invoke-direct {v0}, LX/3Vd;-><init>()V

    sput-object v0, LX/3Vc;->j:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 587995
    const v0, 0x7f0313a8

    invoke-direct {p0, p1, v0}, LX/3Va;-><init>(Landroid/content/Context;I)V

    .line 587996
    const-class v0, LX/3Vc;

    invoke-static {v0, p0}, LX/3Vc;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 587997
    invoke-virtual {p0}, LX/3Vc;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const v0, 0x7f0d052c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/EllipsizingTextView;

    const v1, 0x7f0d052d

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/EllipsizingTextView;

    invoke-static {v3, v0, v1}, LX/C01;->a(ILcom/facebook/resources/ui/EllipsizingTextView;Lcom/facebook/resources/ui/EllipsizingTextView;)V

    .line 587998
    const v0, 0x7f0d2d5e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Space;

    iput-object v0, p0, LX/3Vc;->l:Landroid/widget/Space;

    .line 587999
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/3Vc;

    invoke-static {p0}, LX/C01;->a(LX/0QB;)LX/C01;

    move-result-object p0

    check-cast p0, LX/C01;

    iput-object p0, p1, LX/3Vc;->k:LX/C01;

    return-void
.end method


# virtual methods
.method public setSideImageController(LX/1aZ;)V
    .locals 2
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 588000
    invoke-super {p0, p1}, LX/3Va;->setSideImageController(LX/1aZ;)V

    .line 588001
    iget-object v1, p0, LX/3Vc;->l:Landroid/widget/Space;

    .line 588002
    iget-object v0, p0, LX/3Va;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/3Va;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 588003
    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/Space;->setVisibility(I)V

    .line 588004
    return-void

    .line 588005
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
