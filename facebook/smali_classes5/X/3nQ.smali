.class public LX/3nQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/16E;
.implements LX/0i1;


# static fields
.field public static a:Z


# instance fields
.field public b:Landroid/view/View;

.field private c:LX/0wM;

.field private d:LX/0hL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 638689
    const/4 v0, 0x0

    sput-boolean v0, LX/3nQ;->a:Z

    return-void
.end method

.method public constructor <init>(LX/0wM;LX/0hL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 638690
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 638691
    iput-object p1, p0, LX/3nQ;->c:LX/0wM;

    .line 638692
    iput-object p2, p0, LX/3nQ;->d:LX/0hL;

    .line 638693
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 638694
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 638695
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    return-object v0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 638696
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v3, -0x1

    .line 638697
    new-instance v0, LX/0hs;

    const/4 v1, 0x2

    invoke-direct {v0, p1, v1}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 638698
    const v1, 0x7f081b90

    invoke-virtual {v0, v1}, LX/0hs;->a(I)V

    .line 638699
    const v1, 0x7f081b91

    invoke-virtual {v0, v1}, LX/0hs;->b(I)V

    .line 638700
    iget-object v1, p0, LX/3nQ;->c:LX/0wM;

    const v2, 0x7f020765

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->b(Landroid/graphics/drawable/Drawable;)V

    .line 638701
    sget-object v1, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 638702
    iput v3, v0, LX/0hs;->t:I

    .line 638703
    const v1, 0x3e99999a    # 0.3f

    invoke-virtual {v0, v1}, LX/0ht;->b(F)V

    .line 638704
    iget-object v1, p0, LX/3nQ;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 638705
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 638706
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 638707
    iget-object v1, p0, LX/3nQ;->b:Landroid/view/View;

    iget-object v3, p0, LX/3nQ;->d:LX/0hL;

    invoke-virtual {v3}, LX/0hL;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    iget v2, v2, Landroid/graphics/Point;->x:I

    div-int/lit8 v2, v2, 0x2

    :goto_0
    const/4 v3, 0x0

    iget-object v4, p0, LX/3nQ;->b:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    iget-object v5, p0, LX/3nQ;->b:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    invoke-virtual/range {v0 .. v5}, LX/0ht;->a(Landroid/view/View;IIII)V

    .line 638708
    invoke-virtual {v0}, LX/0ht;->d()V

    .line 638709
    return-void

    .line 638710
    :cond_0
    iget v2, v2, Landroid/graphics/Point;->x:I

    mul-int/lit8 v2, v2, -0x1

    div-int/lit8 v2, v2, 0x2

    goto :goto_0
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 638711
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 638712
    const-string v0, "4475"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 638713
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_MALL_VISIT_WITH_INTEREST_COMMUNITY_RANK_FEED_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
