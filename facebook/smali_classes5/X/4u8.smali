.class public final LX/4u8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final a:LX/4u9;


# instance fields
.field private b:Z

.field public c:[I

.field public d:[LX/4u9;

.field public e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, LX/4u9;

    invoke-direct {v0}, LX/4u9;-><init>()V

    sput-object v0, LX/4u8;->a:LX/4u9;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/16 v0, 0xa

    invoke-direct {p0, v0}, LX/4u8;-><init>(I)V

    return-void
.end method

.method private constructor <init>(I)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, LX/4u8;->b:Z

    invoke-static {p1}, LX/4u8;->c(I)I

    move-result v0

    new-array v1, v0, [I

    iput-object v1, p0, LX/4u8;->c:[I

    new-array v0, v0, [LX/4u9;

    iput-object v0, p0, LX/4u8;->d:[LX/4u9;

    iput v2, p0, LX/4u8;->e:I

    return-void
.end method

.method public static c(I)I
    .locals 3

    mul-int/lit8 v0, p0, 0x4

    const/4 p0, 0x1

    const/4 v1, 0x4

    :goto_0
    const/16 v2, 0x20

    if-ge v1, v2, :cond_0

    shl-int v2, p0, v1

    add-int/lit8 v2, v2, -0xc

    if-gt v0, v2, :cond_1

    shl-int v1, p0, v1

    add-int/lit8 v0, v1, -0xc

    :cond_0
    move v0, v0

    div-int/lit8 v0, v0, 0x4

    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static e(LX/4u8;I)I
    .locals 4

    const/4 v1, 0x0

    iget v0, p0, LX/4u8;->e:I

    add-int/lit8 v0, v0, -0x1

    move v2, v1

    move v1, v0

    :goto_0
    if-gt v2, v1, :cond_1

    add-int v0, v2, v1

    ushr-int/lit8 v0, v0, 0x1

    iget-object v3, p0, LX/4u8;->c:[I

    aget v3, v3, v0

    if-ge v3, p1, :cond_0

    add-int/lit8 v0, v0, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    if-le v3, p1, :cond_2

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    xor-int/lit8 v0, v2, -0x1

    :cond_2
    return v0
.end method


# virtual methods
.method public final b(I)LX/4u9;
    .locals 1

    iget-object v0, p0, LX/4u8;->d:[LX/4u9;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final b()Z
    .locals 1

    iget v0, p0, LX/4u8;->e:I

    move v0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final clone()Ljava/lang/Object;
    .locals 5

    const/4 v0, 0x0

    iget v1, p0, LX/4u8;->e:I

    move v2, v1

    new-instance v3, LX/4u8;

    invoke-direct {v3, v2}, LX/4u8;-><init>(I)V

    iget-object v1, p0, LX/4u8;->c:[I

    iget-object v4, v3, LX/4u8;->c:[I

    invoke-static {v1, v0, v4, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, LX/4u8;->d:[LX/4u9;

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    iget-object v4, v3, LX/4u8;->d:[LX/4u9;

    iget-object v0, p0, LX/4u8;->d:[LX/4u9;

    aget-object v0, v0, v1

    invoke-virtual {v0}, LX/4u9;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4u9;

    aput-object v0, v4, v1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iput v2, v3, LX/4u8;->e:I

    return-object v3
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 9

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, LX/4u8;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, LX/4u8;

    iget v2, p0, LX/4u8;->e:I

    move v2, v2

    iget v3, p1, LX/4u8;->e:I

    move v3, v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, LX/4u8;->c:[I

    iget-object v3, p1, LX/4u8;->c:[I

    iget v4, p0, LX/4u8;->e:I

    const/4 v5, 0x0

    move v6, v5

    :goto_1
    if-ge v6, v4, :cond_6

    aget v7, v2, v6

    aget v8, v3, v6

    if-eq v7, v8, :cond_5

    :goto_2
    move v2, v5

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/4u8;->d:[LX/4u9;

    iget-object v3, p1, LX/4u8;->d:[LX/4u9;

    iget v4, p0, LX/4u8;->e:I

    const/4 v5, 0x0

    move v6, v5

    :goto_3
    if-ge v6, v4, :cond_8

    aget-object v7, v2, v6

    aget-object v8, v3, v6

    invoke-virtual {v7, v8}, LX/4u9;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_7

    :goto_4
    move v2, v5

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_6
    const/4 v5, 0x1

    goto :goto_2

    :cond_7
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_8
    const/4 v5, 0x1

    goto :goto_4
.end method

.method public final hashCode()I
    .locals 3

    const/16 v1, 0x11

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, LX/4u8;->e:I

    if-ge v0, v2, :cond_0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v2, p0, LX/4u8;->c:[I

    aget v2, v2, v0

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x1f

    iget-object v2, p0, LX/4u8;->d:[LX/4u9;

    aget-object v2, v2, v0

    invoke-virtual {v2}, LX/4u9;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method
