.class public LX/3en;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/3en;


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/0SG;

.field private final c:LX/0So;

.field private final d:LX/0kb;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0SG;LX/0So;LX/0kb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 620960
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 620961
    iput-object p1, p0, LX/3en;->a:LX/0Zb;

    .line 620962
    iput-object p2, p0, LX/3en;->b:LX/0SG;

    .line 620963
    iput-object p3, p0, LX/3en;->c:LX/0So;

    .line 620964
    iput-object p4, p0, LX/3en;->d:LX/0kb;

    .line 620965
    return-void
.end method

.method public static a(LX/0QB;)LX/3en;
    .locals 7

    .prologue
    .line 620966
    sget-object v0, LX/3en;->e:LX/3en;

    if-nez v0, :cond_1

    .line 620967
    const-class v1, LX/3en;

    monitor-enter v1

    .line 620968
    :try_start_0
    sget-object v0, LX/3en;->e:LX/3en;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 620969
    if-eqz v2, :cond_0

    .line 620970
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 620971
    new-instance p0, LX/3en;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v6

    check-cast v6, LX/0kb;

    invoke-direct {p0, v3, v4, v5, v6}, LX/3en;-><init>(LX/0Zb;LX/0SG;LX/0So;LX/0kb;)V

    .line 620972
    move-object v0, p0

    .line 620973
    sput-object v0, LX/3en;->e:LX/3en;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 620974
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 620975
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 620976
    :cond_1
    sget-object v0, LX/3en;->e:LX/3en;

    return-object v0

    .line 620977
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 620978
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/3en;Ljava/lang/String;LX/3e1;J)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 5

    .prologue
    .line 620979
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "sticker_asset"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 620980
    const-string v1, "event_type"

    const-string v2, "download"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 620981
    const-string v1, "sticker_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 620982
    const-string v1, "asset_type"

    invoke-virtual {p2}, LX/3e1;->getDbName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 620983
    const-string v1, "timestamp"

    iget-object v2, p0, LX/3en;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 620984
    const-string v1, "download_time_ms"

    iget-object v2, p0, LX/3en;->c:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    sub-long/2addr v2, p3

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 620985
    const-string v1, "appears_to_be_connected_on_wifi"

    iget-object v2, p0, LX/3en;->d:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->v()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 620986
    const-string v1, "asset_type"

    invoke-virtual {p2}, LX/3e1;->getDbName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 620987
    return-object v0
.end method
