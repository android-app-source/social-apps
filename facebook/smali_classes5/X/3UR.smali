.class public LX/3UR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/3UR;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 586022
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 586023
    return-void
.end method

.method public static a(LX/0QB;)LX/3UR;
    .locals 3

    .prologue
    .line 586037
    sget-object v0, LX/3UR;->a:LX/3UR;

    if-nez v0, :cond_1

    .line 586038
    const-class v1, LX/3UR;

    monitor-enter v1

    .line 586039
    :try_start_0
    sget-object v0, LX/3UR;->a:LX/3UR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 586040
    if-eqz v2, :cond_0

    .line 586041
    :try_start_1
    new-instance v0, LX/3UR;

    invoke-direct {v0}, LX/3UR;-><init>()V

    .line 586042
    move-object v0, v0

    .line 586043
    sput-object v0, LX/3UR;->a:LX/3UR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 586044
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 586045
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 586046
    :cond_1
    sget-object v0, LX/3UR;->a:LX/3UR;

    return-object v0

    .line 586047
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 586048
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 586024
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventCollectionsCarouselPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 586025
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventCollectionPagePartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 586026
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardDateBucketHeaderPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 586027
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardEventRowPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 586028
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardLoadingRowPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 586029
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardNullStateRowPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 586030
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewMoreEventsRowPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 586031
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 586032
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowComponentPartDefinition;->d:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 586033
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsBirthdayRowPartDefinition;->b:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 586034
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardPromptRowPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 586035
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardErrorMessageRowPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 586036
    return-void
.end method
