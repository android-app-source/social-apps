.class public LX/3jU;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 632230
    const/4 v0, 0x0

    sput-boolean v0, LX/3jU;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 632231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/CNb;)LX/3j9;
    .locals 2

    .prologue
    .line 632232
    invoke-virtual {p0}, LX/CNb;->a()Ljava/lang/String;

    move-result-object v1

    .line 632233
    invoke-static {v1}, LX/3jV;->a(Ljava/lang/String;)LX/3j8;

    move-result-object v0

    .line 632234
    if-eqz v0, :cond_1

    .line 632235
    :cond_0
    :goto_0
    return-object v0

    .line 632236
    :cond_1
    invoke-static {v1}, LX/3jj;->a(Ljava/lang/String;)LX/3jN;

    move-result-object v0

    .line 632237
    if-nez v0, :cond_0

    .line 632238
    invoke-static {v1}, LX/3ji;->a(Ljava/lang/String;)LX/3jJ;

    move-result-object v0

    .line 632239
    if-nez v0, :cond_0

    .line 632240
    new-instance v0, LX/CQ8;

    invoke-direct {v0}, LX/CQ8;-><init>()V

    goto :goto_0
.end method

.method public static a(LX/0P1;LX/0P1;LX/0P1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/3j8;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/3jJ;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/3jN;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 632241
    sget-boolean v0, LX/3jU;->a:Z

    if-eqz v0, :cond_0

    .line 632242
    :goto_0
    return-void

    .line 632243
    :cond_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    sget-object v1, LX/3jV;->a:LX/0P1;

    invoke-virtual {v0, v1}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/3jV;->b:LX/0P1;

    .line 632244
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    sget-object v1, LX/3ji;->a:LX/0P1;

    invoke-virtual {v0, v1}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/3ji;->b:LX/0P1;

    .line 632245
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    sget-object v1, LX/3jj;->a:LX/0P1;

    invoke-virtual {v0, v1}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/3jj;->b:LX/0P1;

    .line 632246
    const/4 v0, 0x1

    sput-boolean v0, LX/3jU;->a:Z

    goto :goto_0
.end method
