.class public LX/3bw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 612648
    return-void
.end method

.method public static a(LX/0QB;)LX/3bw;
    .locals 3

    .prologue
    .line 612649
    const-class v1, LX/3bw;

    monitor-enter v1

    .line 612650
    :try_start_0
    sget-object v0, LX/3bw;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 612651
    sput-object v2, LX/3bw;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612652
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612653
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 612654
    new-instance v0, LX/3bw;

    invoke-direct {v0}, LX/3bw;-><init>()V

    .line 612655
    move-object v0, v0

    .line 612656
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 612657
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3bw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612658
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 612659
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 612660
    sget-object v0, Lcom/facebook/timeline/protiles/rows/TimelinePendingFriendRequestsPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 612661
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 612662
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 612663
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesLoadingIndicatorPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 612664
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 612665
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 612666
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 612667
    return-void
.end method
