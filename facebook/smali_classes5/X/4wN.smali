.class public LX/4wN;
.super LX/0R0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0R0",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public final g:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TK;"
        }
    .end annotation
.end field

.field public final h:I

.field public final i:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public volatile j:LX/0Qf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Qf",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;ILX/0R1;)V
    .locals 1
    .param p3    # LX/0R1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 820073
    invoke-direct {p0}, LX/0R0;-><init>()V

    .line 820074
    sget-object v0, LX/0Qd;->u:LX/0Qf;

    move-object v0, v0

    .line 820075
    iput-object v0, p0, LX/4wN;->j:LX/0Qf;

    .line 820076
    iput-object p1, p0, LX/4wN;->g:Ljava/lang/Object;

    .line 820077
    iput p2, p0, LX/4wN;->h:I

    .line 820078
    iput-object p3, p0, LX/4wN;->i:LX/0R1;

    .line 820079
    return-void
.end method


# virtual methods
.method public final getHash()I
    .locals 1

    .prologue
    .line 820080
    iget v0, p0, LX/4wN;->h:I

    return v0
.end method

.method public final getKey()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 820081
    iget-object v0, p0, LX/4wN;->g:Ljava/lang/Object;

    return-object v0
.end method

.method public final getNext()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 820082
    iget-object v0, p0, LX/4wN;->i:LX/0R1;

    return-object v0
.end method

.method public final getValueReference()LX/0Qf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Qf",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 820083
    iget-object v0, p0, LX/4wN;->j:LX/0Qf;

    return-object v0
.end method

.method public final setValueReference(LX/0Qf;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Qf",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 820084
    iput-object p1, p0, LX/4wN;->j:LX/0Qf;

    .line 820085
    return-void
.end method
