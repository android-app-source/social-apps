.class public abstract LX/4yO;
.super LX/0Py;
.source ""

# interfaces
.implements LX/1M1;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Py",
        "<TE;>;",
        "LX/1M1",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private transient a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 821715
    invoke-direct {p0}, LX/0Py;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Iterable;)LX/4yO;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<+TE;>;)",
            "LX/4yO",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 821736
    instance-of v0, p0, LX/4yO;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 821737
    check-cast v0, LX/4yO;

    .line 821738
    invoke-virtual {v0}, LX/0Py;->isPartialView()Z

    move-result v1

    if-nez v1, :cond_0

    .line 821739
    :goto_0
    return-object v0

    .line 821740
    :cond_0
    instance-of v0, p0, LX/1M1;

    if-eqz v0, :cond_1

    .line 821741
    check-cast p0, LX/1M1;

    move-object v0, p0

    .line 821742
    :goto_1
    invoke-interface {v0}, LX/1M1;->a()Ljava/util/Set;

    move-result-object v0

    .line 821743
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 821744
    sget-object v1, LX/50T;->a:LX/50T;

    move-object v1, v1

    .line 821745
    :goto_2
    move-object v0, v1

    .line 821746
    goto :goto_0

    .line 821747
    :cond_1
    invoke-static {p0}, LX/2Tc;->a(Ljava/lang/Iterable;)I

    move-result v0

    invoke-static {v0}, LX/4z2;->a(I)LX/4z2;

    move-result-object v0

    .line 821748
    invoke-static {v0, p0}, LX/0Ph;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 821749
    move-object v0, v0

    .line 821750
    goto :goto_1

    :cond_2
    new-instance v1, LX/50T;

    invoke-direct {v1, v0}, LX/50T;-><init>(Ljava/util/Collection;)V

    goto :goto_2
.end method


# virtual methods
.method public final a(Ljava/lang/Object;I)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;I)I"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 821751
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public abstract a(I)LX/4wx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;"
        }
    .end annotation
.end method

.method public final synthetic a()Ljava/util/Set;
    .locals 1

    .prologue
    .line 821752
    invoke-virtual {p0}, LX/4yO;->b()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;II)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;II)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 821753
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(Ljava/lang/Object;I)I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 821754
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b()LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;>;"
        }
    .end annotation

    .prologue
    .line 821729
    iget-object v0, p0, LX/4yO;->a:LX/0Rf;

    .line 821730
    if-nez v0, :cond_0

    .line 821731
    invoke-virtual {p0}, LX/4yO;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 821732
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 821733
    :goto_0
    move-object v0, v0

    .line 821734
    iput-object v0, p0, LX/4yO;->a:LX/0Rf;

    :cond_0
    return-object v0

    :cond_1
    new-instance v0, LX/4yT;

    invoke-direct {v0, p0}, LX/4yT;-><init>(LX/4yO;)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/Object;I)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;I)I"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 821735
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 821728
    invoke-virtual {p0, p1}, LX/4yO;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final copyIntoArray([Ljava/lang/Object;I)I
    .locals 4
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "not present in emulated superclass"
    .end annotation

    .prologue
    .line 821723
    invoke-virtual {p0}, LX/4yO;->b()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4wx;

    .line 821724
    invoke-virtual {v0}, LX/4wx;->b()I

    move-result v2

    add-int/2addr v2, p2

    invoke-virtual {v0}, LX/4wx;->a()Ljava/lang/Object;

    move-result-object v3

    invoke-static {p1, p2, v2, v3}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    .line 821725
    invoke-virtual {v0}, LX/4wx;->b()I

    move-result v0

    add-int/2addr p2, v0

    .line 821726
    goto :goto_0

    .line 821727
    :cond_0
    return p2
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 821722
    invoke-static {p0, p1}, LX/2Tc;->a(LX/1M1;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 821721
    invoke-virtual {p0}, LX/4yO;->b()LX/0Rf;

    move-result-object v0

    invoke-static {v0}, LX/0RA;->a(Ljava/util/Set;)I

    move-result v0

    return v0
.end method

.method public final iterator()LX/0Rc;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 821719
    invoke-virtual {p0}, LX/4yO;->b()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v0

    .line 821720
    new-instance v1, LX/4yR;

    invoke-direct {v1, p0, v0}, LX/4yR;-><init>(LX/4yO;Ljava/util/Iterator;)V

    return-object v1
.end method

.method public final bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 821718
    invoke-virtual {p0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 821717
    invoke-virtual {p0}, LX/4yO;->b()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 821716
    new-instance v0, LX/4yV;

    invoke-direct {v0, p0}, LX/4yV;-><init>(LX/1M1;)V

    return-object v0
.end method
