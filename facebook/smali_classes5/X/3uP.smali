.class public LX/3uP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3uM;


# instance fields
.field private final a:Landroid/view/Window;


# direct methods
.method public constructor <init>(Landroid/view/Window;)V
    .locals 0

    .prologue
    .line 648408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 648409
    iput-object p1, p0, LX/3uP;->a:Landroid/view/Window;

    .line 648410
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 648407
    iget-object v0, p0, LX/3uP;->a:Landroid/view/Window;

    invoke-virtual {v0, p1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a()Landroid/view/Window$Callback;
    .locals 1

    .prologue
    .line 648406
    iget-object v0, p0, LX/3uP;->a:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    return-object v0
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 648411
    iget-object v0, p0, LX/3uP;->a:Landroid/view/Window;

    invoke-virtual {v0, p1, p2}, Landroid/view/Window;->setLayout(II)V

    .line 648412
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 648404
    iget-object v0, p0, LX/3uP;->a:Landroid/view/Window;

    invoke-virtual {v0, p1}, Landroid/view/Window;->setContentView(Landroid/view/View;)V

    .line 648405
    return-void
.end method

.method public final a(Landroid/view/Window$Callback;)V
    .locals 1

    .prologue
    .line 648402
    iget-object v0, p0, LX/3uP;->a:Landroid/view/Window;

    invoke-virtual {v0, p1}, Landroid/view/Window;->setCallback(Landroid/view/Window$Callback;)V

    .line 648403
    return-void
.end method

.method public final b()Landroid/view/View;
    .locals 1

    .prologue
    .line 648400
    iget-object v0, p0, LX/3uP;->a:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 648401
    iget-object v0, p0, LX/3uP;->a:Landroid/view/Window;

    invoke-virtual {v0, p1}, Landroid/view/Window;->requestFeature(I)Z

    move-result v0

    return v0
.end method
