.class public final LX/4xd;
.super LX/1q0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1q0",
        "<TK;",
        "Ljava/util/Collection",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4xj;


# direct methods
.method public constructor <init>(LX/4xj;)V
    .locals 0

    .prologue
    .line 821208
    iput-object p1, p0, LX/4xd;->a:LX/4xj;

    invoke-direct {p0}, LX/1q0;-><init>()V

    return-void
.end method

.method private b(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 821203
    iget-object v0, p0, LX/4xd;->a:LX/4xj;

    iget-object v0, v0, LX/4xj;->a:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 821204
    if-nez v0, :cond_1

    move-object v0, v1

    .line 821205
    :cond_0
    :goto_0
    return-object v0

    .line 821206
    :cond_1
    new-instance v2, LX/4xh;

    iget-object v3, p0, LX/4xd;->a:LX/4xj;

    invoke-direct {v2, v3, p1}, LX/4xh;-><init>(LX/4xj;Ljava/lang/Object;)V

    invoke-static {v0, v2}, LX/4xj;->a(Ljava/util/Collection;LX/0Rl;)Ljava/util/Collection;

    move-result-object v0

    .line 821207
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 821188
    iget-object v0, p0, LX/4xd;->a:LX/4xj;

    iget-object v0, v0, LX/4xj;->a:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 821189
    if-nez v0, :cond_0

    move-object v0, v1

    .line 821190
    :goto_0
    return-object v0

    .line 821191
    :cond_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 821192
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 821193
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 821194
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 821195
    iget-object v4, p0, LX/4xd;->a:LX/4xj;

    invoke-static {v4, p1, v3}, LX/4xj;->d(LX/4xj;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 821196
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 821197
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 821198
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 821199
    goto :goto_0

    .line 821200
    :cond_3
    iget-object v0, p0, LX/4xd;->a:LX/4xj;

    iget-object v0, v0, LX/4xj;->a:LX/0Xu;

    instance-of v0, v0, LX/0vX;

    if-eqz v0, :cond_4

    .line 821201
    invoke-static {v2}, LX/0RA;->c(Ljava/lang/Iterable;)Ljava/util/LinkedHashSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0

    .line 821202
    :cond_4
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;>;"
        }
    .end annotation

    .prologue
    .line 821187
    new-instance v0, LX/4xZ;

    invoke-direct {v0, p0}, LX/4xZ;-><init>(LX/4xd;)V

    return-object v0
.end method

.method public final b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 821180
    new-instance v0, LX/4xa;

    invoke-direct {v0, p0}, LX/4xa;-><init>(LX/4xd;)V

    return-object v0
.end method

.method public final c()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/util/Collection",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 821186
    new-instance v0, LX/4xc;

    invoke-direct {v0, p0}, LX/4xc;-><init>(LX/4xd;)V

    return-object v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 821184
    iget-object v0, p0, LX/4xd;->a:LX/4xj;

    invoke-virtual {v0}, LX/4xj;->g()V

    .line 821185
    return-void
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 821183
    invoke-direct {p0, p1}, LX/4xd;->b(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 821182
    invoke-direct {p0, p1}, LX/4xd;->b(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 821181
    invoke-virtual {p0, p1}, LX/4xd;->a(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
