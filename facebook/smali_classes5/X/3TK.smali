.class public LX/3TK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 584085
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 584086
    iput-object p1, p0, LX/3TK;->a:LX/0Zb;

    .line 584087
    return-void
.end method

.method public static a(LX/0QB;)LX/3TK;
    .locals 4

    .prologue
    .line 584074
    const-class v1, LX/3TK;

    monitor-enter v1

    .line 584075
    :try_start_0
    sget-object v0, LX/3TK;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 584076
    sput-object v2, LX/3TK;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 584077
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 584078
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 584079
    new-instance p0, LX/3TK;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/3TK;-><init>(LX/0Zb;)V

    .line 584080
    move-object v0, p0

    .line 584081
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 584082
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3TK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 584083
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 584084
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/3TK;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 584090
    const-string v0, "see_all_click"

    const-string v1, "section"

    const-string v2, "item_count"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, p1, v2, v3}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/3TK;->a(LX/3TK;Ljava/lang/String;Ljava/util/Map;)V

    .line 584091
    return-void
.end method

.method public static a(LX/3TK;Ljava/lang/String;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 584092
    iget-object v0, p0, LX/3TK;->a:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "notifications_friending"

    .line 584093
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 584094
    move-object v1, v1

    .line 584095
    invoke-virtual {v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 584096
    return-void
.end method


# virtual methods
.method public final b(I)V
    .locals 1

    .prologue
    .line 584088
    const-string v0, "friend_requests"

    invoke-static {p0, v0, p1}, LX/3TK;->a(LX/3TK;Ljava/lang/String;I)V

    .line 584089
    return-void
.end method
