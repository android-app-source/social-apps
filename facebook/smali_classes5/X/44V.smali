.class public final LX/44V;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0X9;
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/common/executors/ExecutorsUserScopeHelper$NamedCallable",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/concurrent/Callable;

.field public final synthetic b:LX/0SI;

.field public final synthetic c:Lcom/facebook/auth/viewercontext/ViewerContext;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Callable;LX/0SI;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 0

    .prologue
    .line 669753
    iput-object p1, p0, LX/44V;->a:Ljava/util/concurrent/Callable;

    iput-object p2, p0, LX/44V;->b:LX/0SI;

    iput-object p3, p0, LX/44V;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 669754
    iget-object v0, p0, LX/44V;->a:Ljava/util/concurrent/Callable;

    invoke-static {v0}, LX/22f;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 669755
    iget-object v0, p0, LX/44V;->a:Ljava/util/concurrent/Callable;

    invoke-static {v0}, LX/22f;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final call()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 669756
    iget-object v0, p0, LX/44V;->b:LX/0SI;

    iget-object v1, p0, LX/44V;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-interface {v0, v1}, LX/0SI;->b(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/1mW;

    .line 669757
    :try_start_0
    iget-object v0, p0, LX/44V;->a:Ljava/util/concurrent/Callable;

    invoke-interface {v0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 669758
    iget-object v1, p0, LX/44V;->b:LX/0SI;

    invoke-interface {v1}, LX/0SI;->f()V

    return-object v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/44V;->b:LX/0SI;

    invoke-interface {v1}, LX/0SI;->f()V

    throw v0
.end method
