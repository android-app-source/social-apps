.class public final LX/3rU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3rS;


# static fields
.field private static final e:I

.field private static final f:I

.field private static final g:I


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field public final h:Landroid/os/Handler;

.field public final i:Landroid/view/GestureDetector$OnGestureListener;

.field public j:Landroid/view/GestureDetector$OnDoubleTapListener;

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Landroid/view/MotionEvent;

.field private q:Landroid/view/MotionEvent;

.field public r:Z

.field private s:F

.field private t:F

.field private u:F

.field private v:F

.field private w:Z

.field public x:Landroid/view/VelocityTracker;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 643221
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    sput v0, LX/3rU;->e:I

    .line 643222
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    sput v0, LX/3rU;->f:I

    .line 643223
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v0

    sput v0, LX/3rU;->g:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 643211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 643212
    if-eqz p3, :cond_1

    .line 643213
    new-instance v0, LX/3rT;

    invoke-direct {v0, p0, p3}, LX/3rT;-><init>(LX/3rU;Landroid/os/Handler;)V

    iput-object v0, p0, LX/3rU;->h:Landroid/os/Handler;

    .line 643214
    :goto_0
    iput-object p2, p0, LX/3rU;->i:Landroid/view/GestureDetector$OnGestureListener;

    .line 643215
    instance-of v0, p2, Landroid/view/GestureDetector$OnDoubleTapListener;

    if-eqz v0, :cond_0

    .line 643216
    check-cast p2, Landroid/view/GestureDetector$OnDoubleTapListener;

    .line 643217
    iput-object p2, p0, LX/3rU;->j:Landroid/view/GestureDetector$OnDoubleTapListener;

    .line 643218
    :cond_0
    invoke-direct {p0, p1}, LX/3rU;->a(Landroid/content/Context;)V

    .line 643219
    return-void

    .line 643220
    :cond_1
    new-instance v0, LX/3rT;

    invoke-direct {v0, p0}, LX/3rT;-><init>(LX/3rU;)V

    iput-object v0, p0, LX/3rU;->h:Landroid/os/Handler;

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 643051
    if-nez p1, :cond_0

    .line 643052
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Context must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 643053
    :cond_0
    iget-object v0, p0, LX/3rU;->i:Landroid/view/GestureDetector$OnGestureListener;

    if-nez v0, :cond_1

    .line 643054
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "OnGestureListener must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 643055
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3rU;->w:Z

    .line 643056
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 643057
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    .line 643058
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    move-result v2

    .line 643059
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v3

    iput v3, p0, LX/3rU;->c:I

    .line 643060
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, LX/3rU;->d:I

    .line 643061
    mul-int v0, v1, v1

    iput v0, p0, LX/3rU;->a:I

    .line 643062
    mul-int v0, v2, v2

    iput v0, p0, LX/3rU;->b:I

    .line 643063
    return-void
.end method

.method private a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 643205
    iget-boolean v1, p0, LX/3rU;->o:Z

    if-nez v1, :cond_1

    .line 643206
    :cond_0
    :goto_0
    return v0

    .line 643207
    :cond_1
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    sget v1, LX/3rU;->g:I

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    .line 643208
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    sub-int/2addr v1, v2

    .line 643209
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    sub-int/2addr v2, v3

    .line 643210
    mul-int/2addr v1, v1

    mul-int/2addr v2, v2

    add-int/2addr v1, v2

    iget v2, p0, LX/3rU;->b:I

    if-ge v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Z)V
    .locals 0

    .prologue
    .line 643203
    iput-boolean p1, p0, LX/3rU;->w:Z

    .line 643204
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 13

    .prologue
    const/4 v6, 0x0

    const/4 v12, 0x2

    const/4 v11, 0x3

    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 643064
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    .line 643065
    iget-object v0, p0, LX/3rU;->x:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    .line 643066
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, LX/3rU;->x:Landroid/view/VelocityTracker;

    .line 643067
    :cond_0
    iget-object v0, p0, LX/3rU;->x:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 643068
    and-int/lit16 v0, v9, 0xff

    const/4 v1, 0x6

    if-ne v0, v1, :cond_2

    move v7, v8

    .line 643069
    :goto_0
    if-eqz v7, :cond_3

    invoke-static {p1}, LX/2xd;->b(Landroid/view/MotionEvent;)I

    move-result v0

    .line 643070
    :goto_1
    invoke-static {p1}, LX/2xd;->c(Landroid/view/MotionEvent;)I

    move-result v4

    move v5, v3

    move v1, v6

    move v2, v6

    .line 643071
    :goto_2
    if-ge v5, v4, :cond_4

    .line 643072
    if-eq v0, v5, :cond_1

    .line 643073
    invoke-static {p1, v5}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v10

    add-float/2addr v2, v10

    .line 643074
    invoke-static {p1, v5}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v10

    add-float/2addr v1, v10

    .line 643075
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_2
    move v7, v3

    .line 643076
    goto :goto_0

    .line 643077
    :cond_3
    const/4 v0, -0x1

    goto :goto_1

    .line 643078
    :cond_4
    if-eqz v7, :cond_6

    add-int/lit8 v0, v4, -0x1

    .line 643079
    :goto_3
    int-to-float v5, v0

    div-float/2addr v2, v5

    .line 643080
    int-to-float v0, v0

    div-float/2addr v1, v0

    .line 643081
    and-int/lit16 v0, v9, 0xff

    packed-switch v0, :pswitch_data_0

    .line 643082
    :cond_5
    :goto_4
    :pswitch_0
    return v3

    :cond_6
    move v0, v4

    .line 643083
    goto :goto_3

    .line 643084
    :pswitch_1
    iput v2, p0, LX/3rU;->s:F

    iput v2, p0, LX/3rU;->u:F

    .line 643085
    iput v1, p0, LX/3rU;->t:F

    iput v1, p0, LX/3rU;->v:F

    .line 643086
    const/4 v2, 0x0

    .line 643087
    iget-object v0, p0, LX/3rU;->h:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 643088
    iget-object v0, p0, LX/3rU;->h:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 643089
    iget-object v0, p0, LX/3rU;->h:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 643090
    iput-boolean v2, p0, LX/3rU;->r:Z

    .line 643091
    iput-boolean v2, p0, LX/3rU;->n:Z

    .line 643092
    iput-boolean v2, p0, LX/3rU;->o:Z

    .line 643093
    iput-boolean v2, p0, LX/3rU;->l:Z

    .line 643094
    iget-boolean v0, p0, LX/3rU;->m:Z

    if-eqz v0, :cond_7

    .line 643095
    iput-boolean v2, p0, LX/3rU;->m:Z

    .line 643096
    :cond_7
    goto :goto_4

    .line 643097
    :pswitch_2
    iput v2, p0, LX/3rU;->s:F

    iput v2, p0, LX/3rU;->u:F

    .line 643098
    iput v1, p0, LX/3rU;->t:F

    iput v1, p0, LX/3rU;->v:F

    .line 643099
    iget-object v0, p0, LX/3rU;->x:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    iget v2, p0, LX/3rU;->d:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 643100
    invoke-static {p1}, LX/2xd;->b(Landroid/view/MotionEvent;)I

    move-result v1

    .line 643101
    invoke-static {p1, v1}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 643102
    iget-object v2, p0, LX/3rU;->x:Landroid/view/VelocityTracker;

    invoke-static {v2, v0}, LX/2uG;->a(Landroid/view/VelocityTracker;I)F

    move-result v2

    .line 643103
    iget-object v5, p0, LX/3rU;->x:Landroid/view/VelocityTracker;

    invoke-static {v5, v0}, LX/2uG;->b(Landroid/view/VelocityTracker;I)F

    move-result v5

    move v0, v3

    .line 643104
    :goto_5
    if-ge v0, v4, :cond_5

    .line 643105
    if-eq v0, v1, :cond_8

    .line 643106
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v7

    .line 643107
    iget-object v8, p0, LX/3rU;->x:Landroid/view/VelocityTracker;

    invoke-static {v8, v7}, LX/2uG;->a(Landroid/view/VelocityTracker;I)F

    move-result v8

    mul-float/2addr v8, v2

    .line 643108
    iget-object v9, p0, LX/3rU;->x:Landroid/view/VelocityTracker;

    invoke-static {v9, v7}, LX/2uG;->b(Landroid/view/VelocityTracker;I)F

    move-result v7

    mul-float/2addr v7, v5

    .line 643109
    add-float/2addr v7, v8

    .line 643110
    cmpg-float v7, v7, v6

    if-gez v7, :cond_8

    .line 643111
    iget-object v0, p0, LX/3rU;->x:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_4

    .line 643112
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 643113
    :pswitch_3
    iget-object v0, p0, LX/3rU;->j:Landroid/view/GestureDetector$OnDoubleTapListener;

    if-eqz v0, :cond_d

    .line 643114
    iget-object v0, p0, LX/3rU;->h:Landroid/os/Handler;

    invoke-virtual {v0, v11}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    .line 643115
    if-eqz v0, :cond_9

    iget-object v4, p0, LX/3rU;->h:Landroid/os/Handler;

    invoke-virtual {v4, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 643116
    :cond_9
    iget-object v4, p0, LX/3rU;->p:Landroid/view/MotionEvent;

    if-eqz v4, :cond_c

    iget-object v4, p0, LX/3rU;->q:Landroid/view/MotionEvent;

    if-eqz v4, :cond_c

    if-eqz v0, :cond_c

    iget-object v0, p0, LX/3rU;->p:Landroid/view/MotionEvent;

    iget-object v4, p0, LX/3rU;->q:Landroid/view/MotionEvent;

    invoke-direct {p0, v0, v4, p1}, LX/3rU;->a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 643117
    iput-boolean v8, p0, LX/3rU;->r:Z

    .line 643118
    iget-object v0, p0, LX/3rU;->j:Landroid/view/GestureDetector$OnDoubleTapListener;

    iget-object v4, p0, LX/3rU;->p:Landroid/view/MotionEvent;

    invoke-interface {v0, v4}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 643119
    iget-object v4, p0, LX/3rU;->j:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v4, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    or-int/2addr v0, v4

    .line 643120
    :goto_6
    iput v2, p0, LX/3rU;->s:F

    iput v2, p0, LX/3rU;->u:F

    .line 643121
    iput v1, p0, LX/3rU;->t:F

    iput v1, p0, LX/3rU;->v:F

    .line 643122
    iget-object v1, p0, LX/3rU;->p:Landroid/view/MotionEvent;

    if-eqz v1, :cond_a

    .line 643123
    iget-object v1, p0, LX/3rU;->p:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    .line 643124
    :cond_a
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    iput-object v1, p0, LX/3rU;->p:Landroid/view/MotionEvent;

    .line 643125
    iput-boolean v8, p0, LX/3rU;->n:Z

    .line 643126
    iput-boolean v8, p0, LX/3rU;->o:Z

    .line 643127
    iput-boolean v8, p0, LX/3rU;->k:Z

    .line 643128
    iput-boolean v3, p0, LX/3rU;->m:Z

    .line 643129
    iput-boolean v3, p0, LX/3rU;->l:Z

    .line 643130
    iget-boolean v1, p0, LX/3rU;->w:Z

    if-eqz v1, :cond_b

    .line 643131
    iget-object v1, p0, LX/3rU;->h:Landroid/os/Handler;

    invoke-virtual {v1, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 643132
    iget-object v1, p0, LX/3rU;->h:Landroid/os/Handler;

    iget-object v2, p0, LX/3rU;->p:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    sget v4, LX/3rU;->f:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    sget v4, LX/3rU;->e:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    invoke-virtual {v1, v12, v2, v3}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 643133
    :cond_b
    iget-object v1, p0, LX/3rU;->h:Landroid/os/Handler;

    iget-object v2, p0, LX/3rU;->p:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    sget v4, LX/3rU;->f:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    invoke-virtual {v1, v8, v2, v3}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 643134
    iget-object v1, p0, LX/3rU;->i:Landroid/view/GestureDetector$OnGestureListener;

    invoke-interface {v1, p1}, Landroid/view/GestureDetector$OnGestureListener;->onDown(Landroid/view/MotionEvent;)Z

    move-result v1

    or-int v3, v0, v1

    .line 643135
    goto/16 :goto_4

    .line 643136
    :cond_c
    iget-object v0, p0, LX/3rU;->h:Landroid/os/Handler;

    sget v4, LX/3rU;->g:I

    int-to-long v4, v4

    invoke-virtual {v0, v11, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_d
    move v0, v3

    goto :goto_6

    .line 643137
    :pswitch_4
    iget-boolean v0, p0, LX/3rU;->m:Z

    if-nez v0, :cond_5

    .line 643138
    iget v0, p0, LX/3rU;->s:F

    sub-float/2addr v0, v2

    .line 643139
    iget v4, p0, LX/3rU;->t:F

    sub-float/2addr v4, v1

    .line 643140
    iget-boolean v5, p0, LX/3rU;->r:Z

    if-eqz v5, :cond_e

    .line 643141
    iget-object v0, p0, LX/3rU;->j:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    or-int/lit8 v3, v0, 0x0

    goto/16 :goto_4

    .line 643142
    :cond_e
    iget-boolean v5, p0, LX/3rU;->n:Z

    if-eqz v5, :cond_10

    .line 643143
    iget v5, p0, LX/3rU;->u:F

    sub-float v5, v2, v5

    float-to-int v5, v5

    .line 643144
    iget v6, p0, LX/3rU;->v:F

    sub-float v6, v1, v6

    float-to-int v6, v6

    .line 643145
    mul-int/2addr v5, v5

    mul-int/2addr v6, v6

    add-int/2addr v5, v6

    .line 643146
    iget v6, p0, LX/3rU;->a:I

    if-le v5, v6, :cond_1b

    .line 643147
    iget-object v6, p0, LX/3rU;->i:Landroid/view/GestureDetector$OnGestureListener;

    iget-object v7, p0, LX/3rU;->p:Landroid/view/MotionEvent;

    invoke-interface {v6, v7, p1, v0, v4}, Landroid/view/GestureDetector$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    .line 643148
    iput v2, p0, LX/3rU;->s:F

    .line 643149
    iput v1, p0, LX/3rU;->t:F

    .line 643150
    iput-boolean v3, p0, LX/3rU;->n:Z

    .line 643151
    iget-object v1, p0, LX/3rU;->h:Landroid/os/Handler;

    invoke-virtual {v1, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 643152
    iget-object v1, p0, LX/3rU;->h:Landroid/os/Handler;

    invoke-virtual {v1, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 643153
    iget-object v1, p0, LX/3rU;->h:Landroid/os/Handler;

    invoke-virtual {v1, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 643154
    :goto_7
    iget v1, p0, LX/3rU;->a:I

    if-le v5, v1, :cond_f

    .line 643155
    iput-boolean v3, p0, LX/3rU;->o:Z

    :cond_f
    move v3, v0

    .line 643156
    goto/16 :goto_4

    :cond_10
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v5, v5, v6

    if-gez v5, :cond_11

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_5

    .line 643157
    :cond_11
    iget-object v3, p0, LX/3rU;->i:Landroid/view/GestureDetector$OnGestureListener;

    iget-object v5, p0, LX/3rU;->p:Landroid/view/MotionEvent;

    invoke-interface {v3, v5, p1, v0, v4}, Landroid/view/GestureDetector$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v3

    .line 643158
    iput v2, p0, LX/3rU;->s:F

    .line 643159
    iput v1, p0, LX/3rU;->t:F

    goto/16 :goto_4

    .line 643160
    :pswitch_5
    iput-boolean v3, p0, LX/3rU;->k:Z

    .line 643161
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    .line 643162
    iget-boolean v0, p0, LX/3rU;->r:Z

    if-eqz v0, :cond_15

    .line 643163
    iget-object v0, p0, LX/3rU;->j:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 643164
    :cond_12
    :goto_8
    iget-object v2, p0, LX/3rU;->q:Landroid/view/MotionEvent;

    if-eqz v2, :cond_13

    .line 643165
    iget-object v2, p0, LX/3rU;->q:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    .line 643166
    :cond_13
    iput-object v1, p0, LX/3rU;->q:Landroid/view/MotionEvent;

    .line 643167
    iget-object v1, p0, LX/3rU;->x:Landroid/view/VelocityTracker;

    if-eqz v1, :cond_14

    .line 643168
    iget-object v1, p0, LX/3rU;->x:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->recycle()V

    .line 643169
    const/4 v1, 0x0

    iput-object v1, p0, LX/3rU;->x:Landroid/view/VelocityTracker;

    .line 643170
    :cond_14
    iput-boolean v3, p0, LX/3rU;->r:Z

    .line 643171
    iput-boolean v3, p0, LX/3rU;->l:Z

    .line 643172
    iget-object v1, p0, LX/3rU;->h:Landroid/os/Handler;

    invoke-virtual {v1, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 643173
    iget-object v1, p0, LX/3rU;->h:Landroid/os/Handler;

    invoke-virtual {v1, v12}, Landroid/os/Handler;->removeMessages(I)V

    move v3, v0

    .line 643174
    goto/16 :goto_4

    .line 643175
    :cond_15
    iget-boolean v0, p0, LX/3rU;->m:Z

    if-eqz v0, :cond_16

    .line 643176
    iget-object v0, p0, LX/3rU;->h:Landroid/os/Handler;

    invoke-virtual {v0, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 643177
    iput-boolean v3, p0, LX/3rU;->m:Z

    move v0, v3

    goto :goto_8

    .line 643178
    :cond_16
    iget-boolean v0, p0, LX/3rU;->n:Z

    if-eqz v0, :cond_17

    .line 643179
    iget-object v0, p0, LX/3rU;->i:Landroid/view/GestureDetector$OnGestureListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnGestureListener;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 643180
    iget-boolean v2, p0, LX/3rU;->l:Z

    if-eqz v2, :cond_12

    iget-object v2, p0, LX/3rU;->j:Landroid/view/GestureDetector$OnDoubleTapListener;

    if-eqz v2, :cond_12

    .line 643181
    iget-object v2, p0, LX/3rU;->j:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v2, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    goto :goto_8

    .line 643182
    :cond_17
    iget-object v0, p0, LX/3rU;->x:Landroid/view/VelocityTracker;

    .line 643183
    invoke-static {p1, v3}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v2

    .line 643184
    const/16 v4, 0x3e8

    iget v5, p0, LX/3rU;->d:I

    int-to-float v5, v5

    invoke-virtual {v0, v4, v5}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 643185
    invoke-static {v0, v2}, LX/2uG;->b(Landroid/view/VelocityTracker;I)F

    move-result v4

    .line 643186
    invoke-static {v0, v2}, LX/2uG;->a(Landroid/view/VelocityTracker;I)F

    move-result v0

    .line 643187
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v5, p0, LX/3rU;->c:I

    int-to-float v5, v5

    cmpl-float v2, v2, v5

    if-gtz v2, :cond_18

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v5, p0, LX/3rU;->c:I

    int-to-float v5, v5

    cmpl-float v2, v2, v5

    if-lez v2, :cond_1a

    .line 643188
    :cond_18
    iget-object v2, p0, LX/3rU;->i:Landroid/view/GestureDetector$OnGestureListener;

    iget-object v5, p0, LX/3rU;->p:Landroid/view/MotionEvent;

    invoke-interface {v2, v5, p1, v0, v4}, Landroid/view/GestureDetector$OnGestureListener;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    goto/16 :goto_8

    .line 643189
    :pswitch_6
    const/4 v2, 0x0

    .line 643190
    iget-object v0, p0, LX/3rU;->h:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 643191
    iget-object v0, p0, LX/3rU;->h:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 643192
    iget-object v0, p0, LX/3rU;->h:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 643193
    iget-object v0, p0, LX/3rU;->x:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 643194
    const/4 v0, 0x0

    iput-object v0, p0, LX/3rU;->x:Landroid/view/VelocityTracker;

    .line 643195
    iput-boolean v2, p0, LX/3rU;->r:Z

    .line 643196
    iput-boolean v2, p0, LX/3rU;->k:Z

    .line 643197
    iput-boolean v2, p0, LX/3rU;->n:Z

    .line 643198
    iput-boolean v2, p0, LX/3rU;->o:Z

    .line 643199
    iput-boolean v2, p0, LX/3rU;->l:Z

    .line 643200
    iget-boolean v0, p0, LX/3rU;->m:Z

    if-eqz v0, :cond_19

    .line 643201
    iput-boolean v2, p0, LX/3rU;->m:Z

    .line 643202
    :cond_19
    goto/16 :goto_4

    :cond_1a
    move v0, v3

    goto/16 :goto_8

    :cond_1b
    move v0, v3

    goto/16 :goto_7

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
