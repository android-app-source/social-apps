.class public LX/49K;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:[B

.field public c:[B

.field public d:I

.field private e:Ljava/lang/String;

.field public f:[Ljava/lang/String;

.field private g:I

.field public h:Ljava/lang/String;

.field public i:LX/49K;

.field public j:LX/49J;

.field public k:I

.field private l:Z


# direct methods
.method public constructor <init>(ILjava/lang/String;Lcom/facebook/flatbuffers/MutableFlattenable;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 674250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 674251
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/49K;->a:Z

    .line 674252
    shr-int/lit8 v0, p1, 0x3

    add-int/lit8 v0, v0, 0x1

    .line 674253
    new-array v1, v0, [B

    iput-object v1, p0, LX/49K;->b:[B

    .line 674254
    new-array v0, v0, [B

    iput-object v0, p0, LX/49K;->c:[B

    .line 674255
    new-array v0, p1, [Ljava/lang/String;

    iput-object v0, p0, LX/49K;->f:[Ljava/lang/String;

    .line 674256
    iput p1, p0, LX/49K;->g:I

    .line 674257
    iput-object p2, p0, LX/49K;->h:Ljava/lang/String;

    .line 674258
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/49K;->e:Ljava/lang/String;

    .line 674259
    iput-object v3, p0, LX/49K;->j:LX/49J;

    .line 674260
    instance-of v0, p3, LX/0jS;

    iput-boolean v0, p0, LX/49K;->l:Z

    .line 674261
    invoke-interface {p3}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 674262
    invoke-interface {p3}, Lcom/facebook/flatbuffers/MutableFlattenable;->u_()I

    move-result v0

    iput v0, p0, LX/49K;->d:I

    .line 674263
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_1

    .line 674264
    invoke-interface {p3}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v1

    invoke-interface {p3}, Lcom/facebook/flatbuffers/MutableFlattenable;->o_()I

    move-result v2

    invoke-virtual {v1, v2, v0}, LX/15i;->a(II)Z

    move-result v1

    move v1, v1

    .line 674265
    if-eqz v1, :cond_0

    .line 674266
    shr-int/lit8 v1, v0, 0x3

    and-int/lit8 v2, v0, 0x7

    .line 674267
    iget-boolean v4, p0, LX/49K;->a:Z

    if-eqz v4, :cond_0

    .line 674268
    iget-object v4, p0, LX/49K;->c:[B

    aget-byte v5, v4, v1

    const/4 p2, 0x1

    shl-int/2addr p2, v2

    or-int/2addr v5, p2

    int-to-byte v5, v5

    aput-byte v5, v4, v1

    .line 674269
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 674270
    :cond_1
    iput-object v3, p0, LX/49K;->i:LX/49K;

    .line 674271
    return-void
.end method

.method private static c(LX/49K;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 674315
    iget-object v1, p0, LX/49K;->b:[B

    shr-int/lit8 v2, p1, 0x3

    aget-byte v1, v1, v2

    and-int/lit8 v2, p1, 0x7

    shl-int v2, v0, v2

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 674316
    iget-object v0, p0, LX/49K;->i:LX/49K;

    if-eqz v0, :cond_1

    .line 674317
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 674318
    iget-object v1, p0, LX/49K;->i:LX/49K;

    invoke-virtual {v1, v0}, LX/49K;->a(Ljava/lang/StringBuilder;)V

    .line 674319
    iget-boolean v1, p0, LX/49K;->l:Z

    if-eqz v1, :cond_0

    .line 674320
    iget-object v1, p0, LX/49K;->i:LX/49K;

    .line 674321
    iget v2, v1, LX/49K;->d:I

    move v1, v2

    .line 674322
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 674323
    :cond_0
    iget v1, p0, LX/49K;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 674324
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 674325
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/49K;->h:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 674305
    iget-object v0, p0, LX/49K;->i:LX/49K;

    if-eqz v0, :cond_2

    .line 674306
    iget-object v0, p0, LX/49K;->i:LX/49K;

    invoke-virtual {v0, p1}, LX/49K;->a(Ljava/lang/StringBuilder;)V

    .line 674307
    iget-object v0, p0, LX/49K;->i:LX/49K;

    .line 674308
    iget v1, v0, LX/49K;->d:I

    move v0, v1

    .line 674309
    iget-boolean v1, p0, LX/49K;->l:Z

    if-eqz v1, :cond_0

    .line 674310
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 674311
    :cond_0
    iget v0, p0, LX/49K;->k:I

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 674312
    :cond_1
    :goto_0
    return-void

    .line 674313
    :cond_2
    iget-object v0, p0, LX/49K;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 674314
    iget-object v0, p0, LX/49K;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public final a(Ljava/util/Map;Z)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;>;Z)V"
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x1

    .line 674272
    invoke-virtual {p0}, LX/49K;->a()Ljava/lang/String;

    move-result-object v6

    .line 674273
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x14

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 674274
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 674275
    const/4 v0, 0x0

    move v3, v0

    :goto_0
    iget v0, p0, LX/49K;->g:I

    if-ge v3, v0, :cond_5

    .line 674276
    const/4 v0, 0x1

    .line 674277
    iget-object v1, p0, LX/49K;->c:[B

    shr-int/lit8 v2, v3, 0x3

    aget-byte v1, v1, v2

    and-int/lit8 v2, v3, 0x7

    shl-int v2, v0, v2

    and-int/2addr v1, v2

    if-eqz v1, :cond_6

    :goto_1
    move v0, v0

    .line 674278
    if-eqz v0, :cond_1

    .line 674279
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 674280
    const-string v0, "::"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 674281
    iget-object v0, p0, LX/49K;->f:[Ljava/lang/String;

    aget-object v0, v0, v3

    if-eqz v0, :cond_7

    .line 674282
    iget-object v0, p0, LX/49K;->f:[Ljava/lang/String;

    aget-object v0, v0, v3

    .line 674283
    :goto_2
    move-object v0, v0

    .line 674284
    iget-boolean v1, p0, LX/49K;->l:Z

    if-eqz v1, :cond_0

    .line 674285
    if-eqz p2, :cond_2

    .line 674286
    iget v1, p0, LX/49K;->d:I

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 674287
    :goto_3
    const-string v1, "."

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 674288
    :cond_0
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 674289
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 674290
    invoke-interface {p1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 674291
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 674292
    const-string v1, "set_count"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 674293
    const-string v2, "used_count"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 674294
    const-string v8, "set_count"

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    add-long/2addr v10, v4

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v8, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 674295
    invoke-static {p0, v3}, LX/49K;->c(LX/49K;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 674296
    const-string v1, "used_count"

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    add-long/2addr v8, v4

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 674297
    :cond_1
    :goto_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    .line 674298
    :cond_2
    iget-object v1, p0, LX/49K;->e:Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 674299
    :cond_3
    new-instance v8, LX/026;

    invoke-direct {v8}, LX/026;-><init>()V

    .line 674300
    const-string v0, "set_count"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v8, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 674301
    const-string v9, "used_count"

    invoke-static {p0, v3}, LX/49K;->c(LX/49K;I)Z

    move-result v0

    if-eqz v0, :cond_4

    move-wide v0, v4

    :goto_5
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v8, v9, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 674302
    invoke-interface {p1, v2, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 674303
    :cond_4
    const-wide/16 v0, 0x0

    goto :goto_5

    .line 674304
    :cond_5
    return-void

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_7
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2
.end method
