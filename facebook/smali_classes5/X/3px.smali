.class public LX/3px;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/3pn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 641901
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 641902
    new-instance v0, LX/3ps;

    invoke-direct {v0}, LX/3ps;-><init>()V

    sput-object v0, LX/3px;->a:LX/3pn;

    .line 641903
    :goto_0
    return-void

    .line 641904
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_1

    .line 641905
    new-instance v0, LX/3pr;

    invoke-direct {v0}, LX/3pr;-><init>()V

    sput-object v0, LX/3px;->a:LX/3pn;

    goto :goto_0

    .line 641906
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_2

    .line 641907
    new-instance v0, LX/3pq;

    invoke-direct {v0}, LX/3pq;-><init>()V

    sput-object v0, LX/3px;->a:LX/3pn;

    goto :goto_0

    .line 641908
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_3

    .line 641909
    new-instance v0, LX/3pp;

    invoke-direct {v0}, LX/3pp;-><init>()V

    sput-object v0, LX/3px;->a:LX/3pn;

    goto :goto_0

    .line 641910
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_4

    .line 641911
    new-instance v0, LX/3pv;

    invoke-direct {v0}, LX/3pv;-><init>()V

    sput-object v0, LX/3px;->a:LX/3pn;

    goto :goto_0

    .line 641912
    :cond_4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_5

    .line 641913
    new-instance v0, LX/3pu;

    invoke-direct {v0}, LX/3pu;-><init>()V

    sput-object v0, LX/3px;->a:LX/3pn;

    goto :goto_0

    .line 641914
    :cond_5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_6

    .line 641915
    new-instance v0, LX/3pt;

    invoke-direct {v0}, LX/3pt;-><init>()V

    sput-object v0, LX/3px;->a:LX/3pn;

    goto :goto_0

    .line 641916
    :cond_6
    new-instance v0, LX/3po;

    invoke-direct {v0}, LX/3po;-><init>()V

    sput-object v0, LX/3px;->a:LX/3pn;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 641946
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 641947
    return-void
.end method

.method public static b(LX/3pT;Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3pT;",
            "Ljava/util/ArrayList",
            "<",
            "LX/3pb;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 641943
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3pb;

    .line 641944
    invoke-interface {p0, v0}, LX/3pT;->a(LX/3pa;)V

    goto :goto_0

    .line 641945
    :cond_0
    return-void
.end method

.method public static b(LX/3pU;LX/3pc;)V
    .locals 7

    .prologue
    .line 641917
    if-eqz p1, :cond_0

    .line 641918
    instance-of v0, p1, LX/3pe;

    if-eqz v0, :cond_1

    .line 641919
    check-cast p1, LX/3pe;

    .line 641920
    iget-object v0, p1, LX/3pc;->e:Ljava/lang/CharSequence;

    iget-boolean v1, p1, LX/3pc;->g:Z

    iget-object v2, p1, LX/3pc;->f:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/3pe;->a:Ljava/lang/CharSequence;

    .line 641921
    new-instance v4, Landroid/app/Notification$BigTextStyle;

    invoke-interface {p0}, LX/3pU;->a()Landroid/app/Notification$Builder;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/app/Notification$BigTextStyle;-><init>(Landroid/app/Notification$Builder;)V

    invoke-virtual {v4, v0}, Landroid/app/Notification$BigTextStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v4

    .line 641922
    if-eqz v1, :cond_0

    .line 641923
    invoke-virtual {v4, v2}, Landroid/app/Notification$BigTextStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    .line 641924
    :cond_0
    :goto_0
    return-void

    .line 641925
    :cond_1
    instance-of v0, p1, LX/3pm;

    if-eqz v0, :cond_4

    .line 641926
    check-cast p1, LX/3pm;

    .line 641927
    iget-object v0, p1, LX/3pc;->e:Ljava/lang/CharSequence;

    iget-boolean v1, p1, LX/3pc;->g:Z

    iget-object v2, p1, LX/3pc;->f:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/3pm;->a:Ljava/util/ArrayList;

    .line 641928
    new-instance v4, Landroid/app/Notification$InboxStyle;

    invoke-interface {p0}, LX/3pU;->a()Landroid/app/Notification$Builder;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/app/Notification$InboxStyle;-><init>(Landroid/app/Notification$Builder;)V

    invoke-virtual {v4, v0}, Landroid/app/Notification$InboxStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    move-result-object v5

    .line 641929
    if-eqz v1, :cond_2

    .line 641930
    invoke-virtual {v5, v2}, Landroid/app/Notification$InboxStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    .line 641931
    :cond_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    .line 641932
    invoke-virtual {v5, v4}, Landroid/app/Notification$InboxStyle;->addLine(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    goto :goto_1

    .line 641933
    :cond_3
    goto :goto_0

    .line 641934
    :cond_4
    instance-of v0, p1, LX/3pd;

    if-eqz v0, :cond_0

    .line 641935
    check-cast p1, LX/3pd;

    .line 641936
    iget-object v1, p1, LX/3pc;->e:Ljava/lang/CharSequence;

    iget-boolean v2, p1, LX/3pc;->g:Z

    iget-object v3, p1, LX/3pc;->f:Ljava/lang/CharSequence;

    iget-object v4, p1, LX/3pd;->a:Landroid/graphics/Bitmap;

    iget-object v5, p1, LX/3pd;->b:Landroid/graphics/Bitmap;

    iget-boolean v6, p1, LX/3pd;->c:Z

    move-object v0, p0

    .line 641937
    new-instance p0, Landroid/app/Notification$BigPictureStyle;

    invoke-interface {v0}, LX/3pU;->a()Landroid/app/Notification$Builder;

    move-result-object p1

    invoke-direct {p0, p1}, Landroid/app/Notification$BigPictureStyle;-><init>(Landroid/app/Notification$Builder;)V

    invoke-virtual {p0, v1}, Landroid/app/Notification$BigPictureStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$BigPictureStyle;

    move-result-object p0

    invoke-virtual {p0, v4}, Landroid/app/Notification$BigPictureStyle;->bigPicture(Landroid/graphics/Bitmap;)Landroid/app/Notification$BigPictureStyle;

    move-result-object p0

    .line 641938
    if-eqz v6, :cond_5

    .line 641939
    invoke-virtual {p0, v5}, Landroid/app/Notification$BigPictureStyle;->bigLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$BigPictureStyle;

    .line 641940
    :cond_5
    if-eqz v2, :cond_6

    .line 641941
    invoke-virtual {p0, v3}, Landroid/app/Notification$BigPictureStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigPictureStyle;

    .line 641942
    :cond_6
    goto :goto_0
.end method
