.class public final LX/4dq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/4dr",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/4dr",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/graphics/Bitmap;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(IILX/1HR;)V
    .locals 1

    .prologue
    .line 796474
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 796475
    iget v0, p3, LX/1HR;->a:I

    iput v0, p0, LX/4dq;->a:I

    .line 796476
    iget v0, p3, LX/1HR;->b:I

    iput v0, p0, LX/4dq;->b:I

    .line 796477
    iget v0, p3, LX/1HR;->e:I

    iput v0, p0, LX/4dq;->c:I

    .line 796478
    iput p1, p0, LX/4dq;->d:I

    .line 796479
    iput p2, p0, LX/4dq;->e:I

    .line 796480
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/4dq;->f:Ljava/util/List;

    .line 796481
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/4dq;->g:Ljava/util/List;

    .line 796482
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/4dq;->h:Ljava/util/Map;

    .line 796483
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 796484
    iget-object v0, p0, LX/4dq;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4dr;

    .line 796485
    invoke-virtual {v0}, LX/4dr;->a()V

    goto :goto_0

    .line 796486
    :cond_0
    iget-object v0, p0, LX/4dq;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4dr;

    .line 796487
    invoke-virtual {v0}, LX/4dr;->a()V

    goto :goto_1

    .line 796488
    :cond_1
    return-void
.end method
