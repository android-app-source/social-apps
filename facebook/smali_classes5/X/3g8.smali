.class public LX/3g8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "Lcom/facebook/appirater/api/FetchISRConfigResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/2CB;

.field private final b:LX/0SG;


# direct methods
.method public constructor <init>(LX/2CB;LX/0SF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 624078
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 624079
    iput-object p1, p0, LX/3g8;->a:LX/2CB;

    .line 624080
    iput-object p2, p0, LX/3g8;->b:LX/0SG;

    .line 624081
    return-void
.end method

.method public static b(LX/0QB;)LX/3g8;
    .locals 3

    .prologue
    .line 624082
    new-instance v2, LX/3g8;

    invoke-static {p0}, LX/2CB;->a(LX/0QB;)LX/2CB;

    move-result-object v0

    check-cast v0, LX/2CB;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SF;

    invoke-direct {v2, v0, v1}, LX/3g8;-><init>(LX/2CB;LX/0SF;)V

    .line 624083
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 10

    .prologue
    .line 624084
    const/4 v0, 0x2

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v4

    .line 624085
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 624086
    iget-object v0, p0, LX/3g8;->a:LX/2CB;

    .line 624087
    iget-object v6, v0, LX/2CB;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/2tq;->b:LX/0Tn;

    iget-object v8, v0, LX/2CB;->e:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    invoke-interface {v6, v7, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    move-wide v0, v6

    .line 624088
    iget-object v2, p0, LX/3g8;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sub-long v0, v2, v0

    .line 624089
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "time_since_last_install_millisecs"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 624090
    new-instance v0, LX/14N;

    const-string v1, "app_rater_should_ask_user"

    const-string v2, "GET"

    const-string v3, "method/app_rater.should_ask_user"

    sget-object v5, LX/14S;->JSONPARSER:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 624091
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 624092
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    const-class v1, Lcom/facebook/appirater/api/FetchISRConfigResult;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/appirater/api/FetchISRConfigResult;

    return-object v0
.end method
