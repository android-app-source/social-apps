.class public final LX/49o;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    .line 674952
    const-wide/16 v6, 0x0

    .line 674953
    const/4 v5, 0x0

    .line 674954
    const/4 v4, 0x0

    .line 674955
    const/4 v3, 0x0

    .line 674956
    const/4 v2, 0x0

    .line 674957
    const/4 v1, 0x0

    .line 674958
    const/4 v0, 0x0

    .line 674959
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v9, :cond_9

    .line 674960
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 674961
    const/4 v0, 0x0

    .line 674962
    :goto_0
    return v0

    .line 674963
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v4, :cond_5

    .line 674964
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 674965
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 674966
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_0

    if-eqz v0, :cond_0

    .line 674967
    const-string v4, "expiration"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 674968
    const/4 v0, 0x1

    .line 674969
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 674970
    :cond_1
    const-string v4, "free_photos"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 674971
    invoke-static {p0, p1}, LX/49n;->a(LX/15w;LX/186;)I

    move-result v0

    move v10, v0

    goto :goto_1

    .line 674972
    :cond_2
    const-string v4, "remaining_quota"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 674973
    const/4 v0, 0x1

    .line 674974
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v7, v0

    move v9, v4

    goto :goto_1

    .line 674975
    :cond_3
    const-string v4, "total_quota"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 674976
    const/4 v0, 0x1

    .line 674977
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v6, v0

    move v8, v4

    goto :goto_1

    .line 674978
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 674979
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 674980
    if-eqz v1, :cond_6

    .line 674981
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 674982
    :cond_6
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 674983
    if-eqz v7, :cond_7

    .line 674984
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v9, v1}, LX/186;->a(III)V

    .line 674985
    :cond_7
    if-eqz v6, :cond_8

    .line 674986
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v8, v1}, LX/186;->a(III)V

    .line 674987
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :cond_9
    move v8, v3

    move v9, v4

    move v10, v5

    move v11, v1

    move v1, v2

    move-wide v2, v6

    move v7, v11

    move v6, v0

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 674988
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 674989
    invoke-virtual {p0, p1, v3, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 674990
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 674991
    const-string v2, "expiration"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 674992
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 674993
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 674994
    if-eqz v0, :cond_4

    .line 674995
    const-string v1, "free_photos"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 674996
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 674997
    const/4 v6, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v7

    if-ge v6, v7, :cond_3

    .line 674998
    invoke-virtual {p0, v0, v6}, LX/15i;->q(II)I

    move-result v7

    const-wide/16 v10, 0x0

    .line 674999
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 675000
    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8, v10, v11}, LX/15i;->a(IIJ)J

    move-result-wide v8

    .line 675001
    cmp-long v10, v8, v10

    if-eqz v10, :cond_1

    .line 675002
    const-string v10, "expiration"

    invoke-virtual {p2, v10}, LX/0nX;->a(Ljava/lang/String;)V

    .line 675003
    invoke-virtual {p2, v8, v9}, LX/0nX;->a(J)V

    .line 675004
    :cond_1
    const/4 v8, 0x1

    invoke-virtual {p0, v7, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v8

    .line 675005
    if-eqz v8, :cond_2

    .line 675006
    const-string v9, "photo_regex"

    invoke-virtual {p2, v9}, LX/0nX;->a(Ljava/lang/String;)V

    .line 675007
    invoke-virtual {p2, v8}, LX/0nX;->b(Ljava/lang/String;)V

    .line 675008
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 675009
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 675010
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 675011
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 675012
    if-eqz v0, :cond_5

    .line 675013
    const-string v1, "remaining_quota"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 675014
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 675015
    :cond_5
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 675016
    if-eqz v0, :cond_6

    .line 675017
    const-string v1, "total_quota"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 675018
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 675019
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 675020
    return-void
.end method
