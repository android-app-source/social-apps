.class public LX/4Os;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 698848
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 698849
    const/4 v12, 0x0

    .line 698850
    const/4 v11, 0x0

    .line 698851
    const/4 v10, 0x0

    .line 698852
    const/4 v9, 0x0

    .line 698853
    const/4 v8, 0x0

    .line 698854
    const/4 v7, 0x0

    .line 698855
    const/4 v6, 0x0

    .line 698856
    const/4 v5, 0x0

    .line 698857
    const/4 v4, 0x0

    .line 698858
    const/4 v3, 0x0

    .line 698859
    const/4 v2, 0x0

    .line 698860
    const/4 v1, 0x0

    .line 698861
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 698862
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 698863
    const/4 v1, 0x0

    .line 698864
    :goto_0
    return v1

    .line 698865
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 698866
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_b

    .line 698867
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 698868
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 698869
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 698870
    const-string v14, "context_page"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 698871
    invoke-static/range {p0 .. p1}, LX/4Or;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 698872
    :cond_2
    const-string v14, "follow_up_title"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 698873
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 698874
    :cond_3
    const-string v14, "info_fields_data"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 698875
    invoke-static/range {p0 .. p1}, LX/4Ov;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 698876
    :cond_4
    const-string v14, "legal_content"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 698877
    invoke-static/range {p0 .. p1}, LX/4Ox;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 698878
    :cond_5
    const-string v14, "need_split_flow"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 698879
    const/4 v2, 0x1

    .line 698880
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto :goto_1

    .line 698881
    :cond_6
    const-string v14, "pages"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 698882
    invoke-static/range {p0 .. p1}, LX/4Oy;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 698883
    :cond_7
    const-string v14, "policy_url"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 698884
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 698885
    :cond_8
    const-string v14, "split_flow_request_method"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 698886
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 698887
    :cond_9
    const-string v14, "split_flow_use_post"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 698888
    const/4 v1, 0x1

    .line 698889
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v4

    goto/16 :goto_1

    .line 698890
    :cond_a
    const-string v14, "thank_you_page"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 698891
    invoke-static/range {p0 .. p1}, LX/4P1;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 698892
    :cond_b
    const/16 v13, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 698893
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 698894
    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 698895
    const/4 v11, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 698896
    const/4 v10, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 698897
    if-eqz v2, :cond_c

    .line 698898
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->a(IZ)V

    .line 698899
    :cond_c
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 698900
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 698901
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 698902
    if-eqz v1, :cond_d

    .line 698903
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->a(IZ)V

    .line 698904
    :cond_d
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 698905
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 698801
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 698802
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 698803
    if-eqz v0, :cond_0

    .line 698804
    const-string v1, "context_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698805
    invoke-static {p0, v0, p2, p3}, LX/4Or;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 698806
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 698807
    if-eqz v0, :cond_1

    .line 698808
    const-string v1, "follow_up_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698809
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 698810
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 698811
    if-eqz v0, :cond_2

    .line 698812
    const-string v1, "info_fields_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698813
    invoke-static {p0, v0, p2, p3}, LX/4Ov;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 698814
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 698815
    if-eqz v0, :cond_3

    .line 698816
    const-string v1, "legal_content"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698817
    invoke-static {p0, v0, p2, p3}, LX/4Ox;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 698818
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 698819
    if-eqz v0, :cond_4

    .line 698820
    const-string v1, "need_split_flow"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698821
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 698822
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 698823
    if-eqz v0, :cond_6

    .line 698824
    const-string v1, "pages"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698825
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 698826
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_5

    .line 698827
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/4Oy;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 698828
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 698829
    :cond_5
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 698830
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 698831
    if-eqz v0, :cond_7

    .line 698832
    const-string v1, "policy_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698833
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 698834
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 698835
    if-eqz v0, :cond_8

    .line 698836
    const-string v1, "split_flow_request_method"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698837
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 698838
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 698839
    if-eqz v0, :cond_9

    .line 698840
    const-string v1, "split_flow_use_post"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698841
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 698842
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 698843
    if-eqz v0, :cond_a

    .line 698844
    const-string v1, "thank_you_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698845
    invoke-static {p0, v0, p2}, LX/4P1;->a(LX/15i;ILX/0nX;)V

    .line 698846
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 698847
    return-void
.end method
