.class public LX/3rb;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/3rX;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 643274
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 643275
    new-instance v0, LX/3ra;

    invoke-direct {v0}, LX/3ra;-><init>()V

    sput-object v0, LX/3rb;->a:LX/3rX;

    .line 643276
    :goto_0
    return-void

    .line 643277
    :cond_0
    new-instance v0, LX/3rY;

    invoke-direct {v0}, LX/3rY;-><init>()V

    sput-object v0, LX/3rb;->a:LX/3rX;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 643278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 643279
    return-void
.end method

.method public static a(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 643280
    sget-object v0, LX/3rb;->a:LX/3rX;

    invoke-virtual {p0}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v1

    invoke-interface {v0, v1}, LX/3rX;->b(I)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/KeyEvent;I)Z
    .locals 2

    .prologue
    .line 643281
    sget-object v0, LX/3rb;->a:LX/3rX;

    invoke-virtual {p0}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v1

    invoke-interface {v0, v1, p1}, LX/3rX;->a(II)Z

    move-result v0

    return v0
.end method
