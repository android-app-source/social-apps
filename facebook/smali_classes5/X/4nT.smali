.class public LX/4nT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 0
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 806870
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 806871
    iput-object p1, p0, LX/4nT;->a:Landroid/content/Context;

    .line 806872
    iput-object p2, p0, LX/4nT;->b:Landroid/os/Handler;

    .line 806873
    return-void
.end method

.method public static a(LX/0QB;)LX/4nT;
    .locals 1

    .prologue
    .line 806874
    invoke-static {p0}, LX/4nT;->b(LX/0QB;)LX/4nT;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/4nT;
    .locals 3

    .prologue
    .line 806875
    new-instance v2, LX/4nT;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v1

    check-cast v1, Landroid/os/Handler;

    invoke-direct {v2, v0, v1}, LX/4nT;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    .line 806876
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 806877
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/4nT;->a(Ljava/lang/String;I)V

    .line 806878
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 806879
    iget-object v0, p0, LX/4nT;->b:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/ui/toaster/ToastThreadUtil$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/ui/toaster/ToastThreadUtil$1;-><init>(LX/4nT;Ljava/lang/String;I)V

    const v2, 0x5eda5f07

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 806880
    return-void
.end method
