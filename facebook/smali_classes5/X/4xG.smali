.class public abstract enum LX/4xG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4xG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4xG;

.field public static final enum CLOSED:LX/4xG;

.field public static final enum OPEN:LX/4xG;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 820904
    new-instance v0, LX/4xH;

    const-string v1, "OPEN"

    invoke-direct {v0, v1, v2}, LX/4xH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4xG;->OPEN:LX/4xG;

    .line 820905
    new-instance v0, LX/4xI;

    const-string v1, "CLOSED"

    invoke-direct {v0, v1, v3}, LX/4xI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4xG;->CLOSED:LX/4xG;

    .line 820906
    const/4 v0, 0x2

    new-array v0, v0, [LX/4xG;

    sget-object v1, LX/4xG;->OPEN:LX/4xG;

    aput-object v1, v0, v2

    sget-object v1, LX/4xG;->CLOSED:LX/4xG;

    aput-object v1, v0, v3

    sput-object v0, LX/4xG;->$VALUES:[LX/4xG;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 820903
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static forBoolean(Z)LX/4xG;
    .locals 1

    .prologue
    .line 820907
    if-eqz p0, :cond_0

    sget-object v0, LX/4xG;->CLOSED:LX/4xG;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/4xG;->OPEN:LX/4xG;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/4xG;
    .locals 1

    .prologue
    .line 820901
    const-class v0, LX/4xG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4xG;

    return-object v0
.end method

.method public static values()[LX/4xG;
    .locals 1

    .prologue
    .line 820902
    sget-object v0, LX/4xG;->$VALUES:[LX/4xG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4xG;

    return-object v0
.end method


# virtual methods
.method public abstract flip()LX/4xG;
.end method
