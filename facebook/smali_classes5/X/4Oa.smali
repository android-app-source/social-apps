.class public LX/4Oa;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 697364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 697365
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_4

    .line 697366
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 697367
    :goto_0
    return v1

    .line 697368
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_2

    .line 697369
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 697370
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 697371
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_0

    if-eqz v4, :cond_0

    .line 697372
    const-string v5, "last_comment_age"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 697373
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v3, v0

    move v0, v2

    goto :goto_1

    .line 697374
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 697375
    :cond_2
    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 697376
    if-eqz v0, :cond_3

    .line 697377
    invoke-virtual {p1, v1, v3, v1}, LX/186;->a(III)V

    .line 697378
    :cond_3
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 697379
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 697380
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 697381
    if-eqz v0, :cond_0

    .line 697382
    const-string v1, "last_comment_age"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697383
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 697384
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 697385
    return-void
.end method
