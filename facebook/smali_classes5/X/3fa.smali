.class public LX/3fa;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/contacts/server/FetchPaymentEligibleContactsParams;",
        "Lcom/facebook/contacts/server/FetchContactsResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final c:LX/3fb;

.field public final d:LX/3fc;

.field private final e:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 622592
    const-class v0, LX/3fa;

    sput-object v0, LX/3fa;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/3fb;LX/3fc;LX/0SG;LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 622640
    invoke-direct {p0, p4}, LX/0ro;-><init>(LX/0sO;)V

    .line 622641
    iput-object p1, p0, LX/3fa;->c:LX/3fb;

    .line 622642
    iput-object p2, p0, LX/3fa;->d:LX/3fc;

    .line 622643
    iput-object p3, p0, LX/3fa;->e:LX/0SG;

    .line 622644
    return-void
.end method

.method public static a(LX/0QB;)LX/3fa;
    .locals 1

    .prologue
    .line 622637
    invoke-static {p0}, LX/3fa;->b(LX/0QB;)LX/3fa;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3fa;
    .locals 5

    .prologue
    .line 622638
    new-instance v4, LX/3fa;

    invoke-static {p0}, LX/3fb;->a(LX/0QB;)LX/3fb;

    move-result-object v0

    check-cast v0, LX/3fb;

    invoke-static {p0}, LX/3fc;->b(LX/0QB;)LX/3fc;

    move-result-object v1

    check-cast v1, LX/3fc;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-static {p0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v3

    check-cast v3, LX/0sO;

    invoke-direct {v4, v0, v1, v2, v3}, LX/3fa;-><init>(LX/3fb;LX/3fc;LX/0SG;LX/0sO;)V

    .line 622639
    return-object v4
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 622621
    check-cast p1, Lcom/facebook/contacts/server/FetchPaymentEligibleContactsParams;

    .line 622622
    iget-object v0, p1, Lcom/facebook/contacts/server/FetchPaymentEligibleContactsParams;->a:Ljava/lang/String;

    move-object v0, v0

    .line 622623
    invoke-static {v0}, Lcom/facebook/contacts/server/FetchPaymentEligibleContactsParams;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 622624
    const-class v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchPaymentEligibleContactsSearchQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchPaymentEligibleContactsSearchQueryModel;

    .line 622625
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchPaymentEligibleContactsSearchQueryModel;->a()LX/3h7;

    move-result-object v0

    .line 622626
    :goto_0
    invoke-interface {v0}, LX/3h7;->b()LX/0Px;

    move-result-object v2

    .line 622627
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 622628
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 622629
    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;

    .line 622630
    iget-object v4, p0, LX/3fa;->c:LX/3fb;

    invoke-virtual {v4, v0}, LX/3fb;->a(Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;)LX/3hB;

    move-result-object v0

    .line 622631
    invoke-virtual {v0}, LX/3hB;->O()Lcom/facebook/contacts/graphql/Contact;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 622632
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 622633
    :cond_0
    const-class v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchPaymentEligibleContactsQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchPaymentEligibleContactsQueryModel;

    .line 622634
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchPaymentEligibleContactsQueryModel;->a()LX/3h7;

    move-result-object v0

    goto :goto_0

    .line 622635
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 622636
    new-instance v1, Lcom/facebook/contacts/server/FetchContactsResult;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v3, p0, LX/3fa;->e:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-direct {v1, v2, v4, v5, v0}, Lcom/facebook/contacts/server/FetchContactsResult;-><init>(LX/0ta;JLX/0Px;)V

    return-object v1
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 622620
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 3

    .prologue
    .line 622593
    check-cast p1, Lcom/facebook/contacts/server/FetchPaymentEligibleContactsParams;

    .line 622594
    iget-object v0, p1, Lcom/facebook/contacts/server/FetchPaymentEligibleContactsParams;->a:Ljava/lang/String;

    move-object v0, v0

    .line 622595
    invoke-static {v0}, Lcom/facebook/contacts/server/FetchPaymentEligibleContactsParams;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 622596
    new-instance v0, LX/6MH;

    invoke-direct {v0}, LX/6MH;-><init>()V

    move-object v0, v0

    .line 622597
    const-string v1, "search_constraint"

    .line 622598
    iget-object v2, p1, Lcom/facebook/contacts/server/FetchPaymentEligibleContactsParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 622599
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "limit"

    .line 622600
    iget v2, p1, Lcom/facebook/contacts/server/FetchPaymentEligibleContactsParams;->b:I

    move v2, v2

    .line 622601
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    .line 622602
    iget-object v1, p0, LX/3fa;->d:LX/3fc;

    invoke-virtual {v1, v0}, LX/3fc;->a(LX/0gW;)V

    .line 622603
    iget-object v1, p0, LX/3fa;->d:LX/3fc;

    invoke-virtual {v1, v0}, LX/3fc;->c(LX/0gW;)V

    .line 622604
    const/4 v1, 0x1

    .line 622605
    iput-boolean v1, v0, LX/0gW;->l:Z

    .line 622606
    move-object v0, v0

    .line 622607
    move-object v0, v0

    .line 622608
    :goto_0
    return-object v0

    .line 622609
    :cond_0
    new-instance v0, LX/6MG;

    invoke-direct {v0}, LX/6MG;-><init>()V

    move-object v0, v0

    .line 622610
    const-string v1, "limit"

    .line 622611
    iget v2, p1, Lcom/facebook/contacts/server/FetchPaymentEligibleContactsParams;->b:I

    move v2, v2

    .line 622612
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    .line 622613
    iget-object v1, p0, LX/3fa;->d:LX/3fc;

    invoke-virtual {v1, v0}, LX/3fc;->a(LX/0gW;)V

    .line 622614
    iget-object v1, p0, LX/3fa;->d:LX/3fc;

    invoke-virtual {v1, v0}, LX/3fc;->c(LX/0gW;)V

    .line 622615
    const/4 v1, 0x1

    .line 622616
    iput-boolean v1, v0, LX/0gW;->l:Z

    .line 622617
    move-object v0, v0

    .line 622618
    move-object v0, v0

    .line 622619
    goto :goto_0
.end method
