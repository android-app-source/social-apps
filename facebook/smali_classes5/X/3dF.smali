.class public LX/3dF;
.super LX/3dG;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final b:LX/20j;

.field private final c:LX/20i;

.field private final d:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;


# direct methods
.method public constructor <init>(LX/20j;LX/20i;Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;Ljava/lang/String;)V
    .locals 2
    .param p3    # Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 616477
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p4, v0, v1

    invoke-direct {p0, v0}, LX/3dG;-><init>([Ljava/lang/String;)V

    .line 616478
    iput-object p3, p0, LX/3dF;->d:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;

    .line 616479
    iput-object p1, p0, LX/3dF;->b:LX/20j;

    .line 616480
    iput-object p2, p0, LX/3dF;->c:LX/20i;

    .line 616481
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3

    .prologue
    .line 616482
    iget-object v0, p0, LX/3dF;->d:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;

    move-result-object v0

    .line 616483
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 616484
    :cond_0
    :goto_0
    return-object p1

    .line 616485
    :cond_1
    iget-object v1, p0, LX/3dF;->b:LX/20j;

    iget-object v2, p0, LX/3dF;->c:LX/20i;

    invoke-virtual {v2}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    invoke-virtual {v1, v2, p1, v0}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 616486
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 616487
    new-instance v1, LX/3dR;

    invoke-direct {v1, v0}, LX/3dR;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    move-object v1, v1

    .line 616488
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->O()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v0

    .line 616489
    new-instance v2, Lcom/facebook/graphql/model/ConsistentFeedbackTopReactionsConnection;

    invoke-direct {v2}, Lcom/facebook/graphql/model/ConsistentFeedbackTopReactionsConnection;-><init>()V

    .line 616490
    iput-object v0, v2, Lcom/facebook/graphql/model/ConsistentFeedbackTopReactionsConnection;->a:Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    .line 616491
    move-object v0, v2

    .line 616492
    iget-object v2, v1, LX/3dR;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v2, v0}, LX/3dS;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/ConsistentFeedbackTopReactionsConnection;)V

    .line 616493
    move-object v0, v1

    .line 616494
    iget-object v1, v0, LX/3dR;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object p1, v1

    .line 616495
    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 616496
    const-string v0, "ReactionsMutateCacheVisitor"

    return-object v0
.end method
