.class public final LX/4Vh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/399;

.field public final synthetic b:LX/2L0;


# direct methods
.method public constructor <init>(LX/2L0;LX/399;)V
    .locals 0

    .prologue
    .line 742816
    iput-object p1, p0, LX/4Vh;->b:LX/2L0;

    iput-object p2, p0, LX/4Vh;->a:LX/399;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7

    .prologue
    .line 742817
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 742818
    iget-object v0, p1, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    move-object v0, v0

    .line 742819
    new-instance v1, LX/4D4;

    invoke-direct {v1}, LX/4D4;-><init>()V

    invoke-virtual {v1, v0}, LX/4D4;->a(Ljava/lang/String;)LX/4D4;

    move-result-object v0

    .line 742820
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 742821
    new-instance v1, LX/4DM;

    invoke-direct {v1}, LX/4DM;-><init>()V

    invoke-virtual {v1, v0}, LX/4DM;->a(LX/4D4;)LX/4DM;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 742822
    iget-object v0, p0, LX/4Vh;->a:LX/399;

    iget-object v0, v0, LX/399;->a:LX/0zP;

    .line 742823
    iget-object v1, v0, LX/0gW;->e:LX/0w7;

    move-object v0, v1

    .line 742824
    invoke-virtual {v0}, LX/0w7;->e()Ljava/util/Map;

    move-result-object v0

    const-string v1, "0"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 742825
    iget-object v1, p0, LX/4Vh;->b:LX/2L0;

    iget-object v1, v1, LX/2L0;->m:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    .line 742826
    if-eqz v0, :cond_1

    .line 742827
    new-instance v4, LX/4DN;

    invoke-direct {v4}, LX/4DN;-><init>()V

    .line 742828
    const-string v1, "client_mutation_id"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 742829
    const-string v5, "client_mutation_id"

    invoke-virtual {v4, v5, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 742830
    const-string v1, "feedback_id"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v1}, LX/4DN;->d(Ljava/lang/String;)LX/4DN;

    .line 742831
    const-string v1, "message"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 742832
    if-eqz v1, :cond_0

    .line 742833
    new-instance v5, LX/4Jk;

    invoke-direct {v5}, LX/4Jk;-><init>()V

    const-string v6, "text"

    invoke-interface {v1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v5, v1}, LX/4Jk;->a(Ljava/lang/String;)LX/4Jk;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/4DN;->a(LX/4Jk;)LX/4DN;

    .line 742834
    :cond_0
    invoke-virtual {v4, v2}, LX/4DN;->a(Ljava/util/List;)LX/4DN;

    .line 742835
    iget-object v1, v3, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v1

    .line 742836
    const-string v2, "actor_id"

    invoke-virtual {v4, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 742837
    const-string v1, "vod_video_timestamp"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v4, v0}, LX/4DN;->b(Ljava/lang/Integer;)LX/4DN;

    .line 742838
    iget-object v0, p0, LX/4Vh;->a:LX/399;

    iget-object v0, v0, LX/399;->a:LX/0zP;

    .line 742839
    iget-object v1, v0, LX/0gW;->e:LX/0w7;

    move-object v0, v1

    .line 742840
    const-string v1, "input"

    invoke-virtual {v0, v1}, LX/0w7;->a(Ljava/lang/String;)LX/0w7;

    .line 742841
    iget-object v0, p0, LX/4Vh;->a:LX/399;

    iget-object v0, v0, LX/399;->a:LX/0zP;

    const-string v1, "input"

    invoke-virtual {v0, v1, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 742842
    :cond_1
    iget-object v0, p0, LX/4Vh;->a:LX/399;

    .line 742843
    iput-object v3, v0, LX/399;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 742844
    new-instance v0, LX/3G1;

    invoke-direct {v0}, LX/3G1;-><init>()V

    iget-object v1, p0, LX/4Vh;->a:LX/399;

    invoke-virtual {v0, v1}, LX/3G1;->a(LX/399;)LX/3G1;

    move-result-object v0

    invoke-virtual {v0}, LX/3G1;->b()LX/3G4;

    move-result-object v0

    .line 742845
    iget-object v1, p0, LX/4Vh;->b:LX/2L0;

    iget-object v1, v1, LX/2L0;->h:LX/0tX;

    sget-object v2, LX/3Fz;->c:LX/3Fz;

    invoke-virtual {v1, v0, v2}, LX/0tX;->a(LX/3G4;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
