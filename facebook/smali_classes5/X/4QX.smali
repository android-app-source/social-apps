.class public LX/4QX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 705003
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 27

    .prologue
    .line 705004
    const/16 v22, 0x0

    .line 705005
    const/16 v21, 0x0

    .line 705006
    const/16 v20, 0x0

    .line 705007
    const/16 v17, 0x0

    .line 705008
    const-wide/16 v18, 0x0

    .line 705009
    const/16 v16, 0x0

    .line 705010
    const/4 v15, 0x0

    .line 705011
    const/4 v14, 0x0

    .line 705012
    const/4 v13, 0x0

    .line 705013
    const/4 v12, 0x0

    .line 705014
    const/4 v11, 0x0

    .line 705015
    const/4 v10, 0x0

    .line 705016
    const/4 v9, 0x0

    .line 705017
    const/4 v8, 0x0

    .line 705018
    const/4 v7, 0x0

    .line 705019
    const/4 v6, 0x0

    .line 705020
    const/4 v5, 0x0

    .line 705021
    const/4 v4, 0x0

    .line 705022
    const/4 v3, 0x0

    .line 705023
    const/4 v2, 0x0

    .line 705024
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v23

    sget-object v24, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_16

    .line 705025
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 705026
    const/4 v2, 0x0

    .line 705027
    :goto_0
    return v2

    .line 705028
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v7, :cond_12

    .line 705029
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 705030
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 705031
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v25, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v25

    if-eq v7, v0, :cond_0

    if-eqz v2, :cond_0

    .line 705032
    const-string v7, "action_links"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 705033
    invoke-static/range {p0 .. p1}, LX/2bb;->b(LX/15w;LX/186;)I

    move-result v2

    move/from16 v24, v2

    goto :goto_1

    .line 705034
    :cond_1
    const-string v7, "cache_id"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 705035
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v23, v2

    goto :goto_1

    .line 705036
    :cond_2
    const-string v7, "category"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 705037
    const/4 v2, 0x1

    .line 705038
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLPYMACategory;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPYMACategory;

    move-result-object v6

    move-object/from16 v22, v6

    move v6, v2

    goto :goto_1

    .line 705039
    :cond_3
    const-string v7, "debug_info"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 705040
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v21, v2

    goto :goto_1

    .line 705041
    :cond_4
    const-string v7, "fetchTimeMs"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 705042
    const/4 v2, 0x1

    .line 705043
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 705044
    :cond_5
    const-string v7, "hideable_token"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 705045
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 705046
    :cond_6
    const-string v7, "items"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 705047
    invoke-static/range {p0 .. p1}, LX/4QY;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 705048
    :cond_7
    const-string v7, "local_last_negative_feedback_action_type"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 705049
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 705050
    :cond_8
    const-string v7, "local_story_visibility"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 705051
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 705052
    :cond_9
    const-string v7, "local_story_visible_height"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 705053
    const/4 v2, 0x1

    .line 705054
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    move v8, v2

    move/from16 v16, v7

    goto/16 :goto_1

    .line 705055
    :cond_a
    const-string v7, "negative_feedback_actions"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 705056
    invoke-static/range {p0 .. p1}, LX/2bY;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 705057
    :cond_b
    const-string v7, "privacy_scope"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 705058
    invoke-static/range {p0 .. p1}, LX/2bo;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 705059
    :cond_c
    const-string v7, "short_term_cache_key"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 705060
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 705061
    :cond_d
    const-string v7, "title"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 705062
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 705063
    :cond_e
    const-string v7, "tracking"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 705064
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 705065
    :cond_f
    const-string v7, "nt_pyma_footer"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 705066
    invoke-static/range {p0 .. p1}, LX/4Pn;->a(LX/15w;LX/186;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 705067
    :cond_10
    const-string v7, "nt_pyma_header"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 705068
    invoke-static/range {p0 .. p1}, LX/4Pn;->a(LX/15w;LX/186;)I

    move-result v2

    move v9, v2

    goto/16 :goto_1

    .line 705069
    :cond_11
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 705070
    :cond_12
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 705071
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 705072
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 705073
    if-eqz v6, :cond_13

    .line 705074
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 705075
    :cond_13
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 705076
    if-eqz v3, :cond_14

    .line 705077
    const/4 v3, 0x4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 705078
    :cond_14
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 705079
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 705080
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 705081
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 705082
    if-eqz v8, :cond_15

    .line 705083
    const/16 v2, 0x9

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 705084
    :cond_15
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 705085
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 705086
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 705087
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 705088
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 705089
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 705090
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 705091
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_16
    move/from16 v23, v21

    move/from16 v24, v22

    move/from16 v21, v17

    move-object/from16 v22, v20

    move/from16 v17, v13

    move/from16 v20, v16

    move v13, v9

    move/from16 v16, v12

    move v12, v8

    move v9, v5

    move v8, v2

    move/from16 v26, v14

    move v14, v10

    move v10, v6

    move v6, v4

    move-wide/from16 v4, v18

    move/from16 v18, v26

    move/from16 v19, v15

    move v15, v11

    move v11, v7

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 705092
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 705093
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 705094
    invoke-static {p0, v2}, LX/4QX;->a(LX/15w;LX/186;)I

    move-result v1

    .line 705095
    if-eqz v0, :cond_0

    .line 705096
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 705097
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 705098
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 705099
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 705100
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 705101
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 705102
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    .line 705103
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 705104
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705105
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 705106
    const-string v0, "name"

    const-string v1, "PagesYouMayAdvertiseFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 705107
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 705108
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 705109
    if-eqz v0, :cond_0

    .line 705110
    const-string v1, "action_links"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705111
    invoke-static {p0, v0, p2, p3}, LX/2bb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 705112
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 705113
    if-eqz v0, :cond_1

    .line 705114
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705115
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 705116
    :cond_1
    invoke-virtual {p0, p1, v2, v3}, LX/15i;->a(IIS)S

    move-result v0

    .line 705117
    if-eqz v0, :cond_2

    .line 705118
    const-string v0, "category"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705119
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPYMACategory;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPYMACategory;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPYMACategory;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 705120
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 705121
    if-eqz v0, :cond_3

    .line 705122
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705123
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 705124
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 705125
    cmp-long v2, v0, v4

    if-eqz v2, :cond_4

    .line 705126
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705127
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 705128
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 705129
    if-eqz v0, :cond_5

    .line 705130
    const-string v1, "hideable_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705131
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 705132
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 705133
    if-eqz v0, :cond_7

    .line 705134
    const-string v1, "items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705135
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 705136
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_6

    .line 705137
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/4QY;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 705138
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 705139
    :cond_6
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 705140
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 705141
    if-eqz v0, :cond_8

    .line 705142
    const-string v1, "local_last_negative_feedback_action_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705143
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 705144
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 705145
    if-eqz v0, :cond_9

    .line 705146
    const-string v1, "local_story_visibility"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705147
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 705148
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 705149
    if-eqz v0, :cond_a

    .line 705150
    const-string v1, "local_story_visible_height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705151
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 705152
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 705153
    if-eqz v0, :cond_b

    .line 705154
    const-string v1, "negative_feedback_actions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705155
    invoke-static {p0, v0, p2, p3}, LX/2bY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 705156
    :cond_b
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 705157
    if-eqz v0, :cond_c

    .line 705158
    const-string v1, "privacy_scope"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705159
    invoke-static {p0, v0, p2, p3}, LX/2bo;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 705160
    :cond_c
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 705161
    if-eqz v0, :cond_d

    .line 705162
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705163
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 705164
    :cond_d
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 705165
    if-eqz v0, :cond_e

    .line 705166
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705167
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 705168
    :cond_e
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 705169
    if-eqz v0, :cond_f

    .line 705170
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705171
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 705172
    :cond_f
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 705173
    if-eqz v0, :cond_10

    .line 705174
    const-string v1, "nt_pyma_footer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705175
    invoke-static {p0, v0, p2, p3}, LX/4Pn;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 705176
    :cond_10
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 705177
    if-eqz v0, :cond_11

    .line 705178
    const-string v1, "nt_pyma_header"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705179
    invoke-static {p0, v0, p2, p3}, LX/4Pn;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 705180
    :cond_11
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 705181
    return-void
.end method
