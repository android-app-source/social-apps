.class public LX/4T5;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 716323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 716324
    const/4 v2, 0x0

    .line 716325
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_9

    .line 716326
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 716327
    :goto_0
    return v0

    .line 716328
    :cond_0
    const-string v9, "is_highlighted"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 716329
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v2

    move v6, v2

    move v2, v1

    .line 716330
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_6

    .line 716331
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 716332
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 716333
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 716334
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 716335
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 716336
    :cond_2
    const-string v9, "souvenir_media"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 716337
    invoke-static {p0, p1}, LX/4T6;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 716338
    :cond_3
    const-string v9, "souvenir_media_type"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 716339
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    move-result-object v0

    move-object v4, v0

    move v0, v1

    goto :goto_1

    .line 716340
    :cond_4
    const-string v9, "url"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 716341
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 716342
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 716343
    :cond_6
    const/4 v8, 0x6

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 716344
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 716345
    if-eqz v2, :cond_7

    .line 716346
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v6}, LX/186;->a(IZ)V

    .line 716347
    :cond_7
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 716348
    if-eqz v0, :cond_8

    .line 716349
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 716350
    :cond_8
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 716351
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_9
    move v3, v0

    move-object v4, v2

    move v5, v0

    move v6, v0

    move v7, v0

    move v2, v0

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 716352
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 716353
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 716354
    if-eqz v0, :cond_0

    .line 716355
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716356
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716357
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 716358
    if-eqz v0, :cond_1

    .line 716359
    const-string v1, "is_highlighted"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716360
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 716361
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 716362
    if-eqz v0, :cond_2

    .line 716363
    const-string v1, "souvenir_media"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716364
    invoke-static {p0, v0, p2, p3}, LX/4T6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 716365
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->a(IIS)S

    move-result v0

    .line 716366
    if-eqz v0, :cond_3

    .line 716367
    const-string v0, "souvenir_media_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716368
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716369
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 716370
    if-eqz v0, :cond_4

    .line 716371
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716372
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716373
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 716374
    return-void
.end method
