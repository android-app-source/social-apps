.class public LX/4mF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/4mG;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 805484
    const-class v0, LX/4mF;

    sput-object v0, LX/4mF;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 805525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 805486
    check-cast p1, LX/4mG;

    .line 805487
    iget-object v0, p1, LX/4mG;->a:Ljava/io/File;

    move-object v0, v0

    .line 805488
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 805489
    new-instance v2, LX/4ct;

    const-string v3, "application/octet-stream"

    invoke-direct {v2, v0, v3, v1}, LX/4ct;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    .line 805490
    new-instance v1, LX/4cQ;

    const-string v3, "TraceFile"

    invoke-direct {v1, v3, v2}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    .line 805491
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 805492
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "perf_name"

    invoke-virtual {p1}, LX/4mG;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 805493
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "timestamp"

    invoke-virtual {p1}, LX/4mG;->c()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 805494
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "phone_model"

    .line 805495
    iget-object v5, p1, LX/4mG;->c:Ljava/lang/String;

    move-object v5, v5

    .line 805496
    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 805497
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "android_version"

    .line 805498
    iget-object v5, p1, LX/4mG;->d:Ljava/lang/String;

    move-object v5, v5

    .line 805499
    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 805500
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "app_version"

    .line 805501
    iget-object v5, p1, LX/4mG;->e:Ljava/lang/String;

    move-object v5, v5

    .line 805502
    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 805503
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    .line 805504
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v3, "PerfTraceUpload"

    .line 805505
    iput-object v3, v0, LX/14O;->b:Ljava/lang/String;

    .line 805506
    move-object v0, v0

    .line 805507
    const-string v3, "POST"

    .line 805508
    iput-object v3, v0, LX/14O;->c:Ljava/lang/String;

    .line 805509
    move-object v0, v0

    .line 805510
    const-string v3, "me/mobile_perftraces"

    .line 805511
    iput-object v3, v0, LX/14O;->d:Ljava/lang/String;

    .line 805512
    move-object v0, v0

    .line 805513
    iput-object v2, v0, LX/14O;->g:Ljava/util/List;

    .line 805514
    move-object v0, v0

    .line 805515
    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v2}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 805516
    iput-object v2, v0, LX/14O;->k:LX/14S;

    .line 805517
    move-object v0, v0

    .line 805518
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 805519
    iput-object v1, v0, LX/14O;->l:Ljava/util/List;

    .line 805520
    move-object v0, v0

    .line 805521
    sget-object v1, LX/14R;->MULTI_PART_ENTITY:LX/14R;

    .line 805522
    iput-object v1, v0, LX/14O;->w:LX/14R;

    .line 805523
    move-object v0, v0

    .line 805524
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 805485
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "success"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
