.class public LX/43M;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private a:Landroid/content/ContentResolver;

.field private final b:Landroid/content/res/Resources;

.field public final c:LX/43C;

.field private final d:LX/11i;

.field private final e:LX/0So;

.field private final f:Ljava/util/Random;

.field private g:LX/11o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/11o",
            "<",
            "LX/43R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/43C;LX/11i;LX/0So;Ljava/util/Random;)V
    .locals 1
    .param p5    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 668879
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 668880
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, LX/43M;->a:Landroid/content/ContentResolver;

    .line 668881
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/43M;->b:Landroid/content/res/Resources;

    .line 668882
    iput-object p2, p0, LX/43M;->c:LX/43C;

    .line 668883
    iput-object p3, p0, LX/43M;->d:LX/11i;

    .line 668884
    iput-object p4, p0, LX/43M;->e:LX/0So;

    .line 668885
    iput-object p5, p0, LX/43M;->f:Ljava/util/Random;

    .line 668886
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/media/MediaItem;LX/431;)Landroid/graphics/Bitmap;
    .locals 12
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 668887
    iget-object v0, p0, LX/43M;->f:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextDouble()D

    move-result-wide v0

    const-wide v2, 0x3fa999999999999aL    # 0.05

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 668888
    iget-object v6, p0, LX/43M;->d:LX/11i;

    sget-object v8, LX/43S;->a:LX/43R;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v9

    const-string v0, "MediaType"

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->l()LX/4gF;

    move-result-object v1

    invoke-virtual {v1}, LX/4gF;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ThumbnailWidth"

    iget v3, p2, LX/431;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "ThumbnailHeight"

    iget v5, p2, LX/431;->c:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v3

    iget-object v0, p0, LX/43M;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    move-object v0, v6

    move-object v1, v8

    move-object v2, v9

    invoke-interface/range {v0 .. v5}, LX/11i;->a(LX/0Pq;Ljava/lang/String;LX/0P1;J)LX/11o;

    move-result-object v0

    iput-object v0, p0, LX/43M;->g:LX/11o;

    .line 668889
    :cond_0
    iget-object v0, p0, LX/43M;->g:LX/11o;

    if-nez v0, :cond_1

    .line 668890
    sget-object v0, LX/43S;->b:LX/11n;

    iput-object v0, p0, LX/43M;->g:LX/11o;

    .line 668891
    :cond_1
    :try_start_0
    sget-object v0, LX/43L;->a:[I

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->l()LX/4gF;

    move-result-object v1

    invoke-virtual {v1}, LX/4gF;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 668892
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 668893
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 668894
    const/4 v1, 0x2

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 668895
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 668896
    iget-object v1, p0, LX/43M;->g:LX/11o;

    const-string v2, "GetThumbnail"

    const v3, 0x66a62ade

    invoke-static {v1, v2, v3}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2

    .line 668897
    :try_start_1
    iget-object v1, p0, LX/43M;->a:Landroid/content/ContentResolver;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->c()J

    move-result-wide v2

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4, v0}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 668898
    if-nez v0, :cond_2

    .line 668899
    :try_start_2
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Landroid/media/ThumbnailUtils;->createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 668900
    :cond_2
    :goto_0
    iget-object v1, p0, LX/43M;->g:LX/11o;

    const-string v2, "GetThumbnail"

    const/4 v3, 0x0

    const-string v4, "GenerateThumbnailMethod"

    sget-object v5, LX/43Q;->VIDEO:LX/43Q;

    invoke-virtual {v5}, LX/43Q;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v4

    const v5, 0x3a84e97f

    invoke-static {v1, v2, v3, v4, v5}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 668901
    if-nez v0, :cond_3

    .line 668902
    iget-object v1, p0, LX/43M;->g:LX/11o;

    const-string v2, "ReturnDefaultDrawable"

    const v3, 0x7647522e

    invoke-static {v1, v2, v3}, LX/096;->e(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 668903
    sget-object v1, LX/46I;->a:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->i()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsValue(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 668904
    iget-object v1, p0, LX/43M;->b:Landroid/content/res/Resources;

    const v2, 0x7f0203b5

    invoke-static {v1, v2}, LX/436;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v0

    :cond_3
    move-object v6, v0

    .line 668905
    :goto_1
    iget-object v0, p0, LX/43M;->d:LX/11i;

    sget-object v1, LX/43S;->a:LX/43R;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/43M;->e:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    move-object v3, v7

    invoke-interface/range {v0 .. v5}, LX/11i;->b(LX/0Pq;Ljava/lang/String;LX/0P1;J)V

    .line 668906
    return-object v6

    .line 668907
    :pswitch_0
    :try_start_3
    iget-boolean v0, p2, LX/431;->d:Z

    if-eqz v0, :cond_5

    .line 668908
    iget-wide v10, p1, Lcom/facebook/ipc/media/MediaItem;->d:J

    move-wide v0, v10

    .line 668909
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_5

    .line 668910
    iget-object v0, p0, LX/43M;->g:LX/11o;

    const-string v1, "GetThumbnail"

    const v2, 0x4fee53f7

    invoke-static {v0, v1, v2}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 668911
    iget-object v0, p0, LX/43M;->a:Landroid/content/ContentResolver;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->c()J

    move-result-wide v2

    const/4 v1, 0x3

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v1, v4}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v0

    .line 668912
    if-eqz v0, :cond_4

    :try_start_4
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->g()I

    move-result v1

    if-eqz v1, :cond_4

    .line 668913
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->g()I

    move-result v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, LX/2Qx;->a(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 668914
    :cond_4
    iget-object v1, p0, LX/43M;->g:LX/11o;

    const-string v2, "GetThumbnail"

    const/4 v3, 0x0

    const-string v4, "GenerateThumbnailMethod"

    sget-object v5, LX/43Q;->MEDIASTORE_IMAGE:LX/43Q;

    invoke-virtual {v5}, LX/43Q;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v4

    const v5, 0x76443d4

    invoke-static {v1, v2, v3, v4, v5}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_3

    move-object v6, v0

    goto :goto_1

    .line 668915
    :cond_5
    :try_start_5
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v0

    .line 668916
    const/4 v1, 0x0
    :try_end_5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_2

    .line 668917
    :try_start_6
    iget-object v2, p0, LX/43M;->c:LX/43C;

    invoke-interface {v2, v0, p2}, LX/43C;->a(Ljava/lang/String;LX/431;)Landroid/graphics/Bitmap;
    :try_end_6
    .catch Lcom/facebook/bitmaps/ImageResizer$ImageResizingException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_2

    :try_start_7
    move-result-object v1

    .line 668918
    :goto_2
    move-object v0, v1

    .line 668919
    move-object v6, v0

    .line 668920
    goto :goto_1

    .line 668921
    :catch_0
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/media/ThumbnailUtils;->createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_0

    .line 668922
    :catchall_0
    move-exception v0

    .line 668923
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Landroid/media/ThumbnailUtils;->createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    :try_end_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_2

    move-result-object v1

    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_8} :catch_1

    .line 668924
    :catch_1
    move-object v0, v1

    :goto_3
    iget-object v1, p0, LX/43M;->g:LX/11o;

    const-string v2, "HasOutOfMemoryError"

    const v3, 0x78c1fb58

    invoke-static {v1, v2, v3}, LX/096;->e(LX/11o;Ljava/lang/String;I)LX/11o;

    move-object v6, v0

    goto/16 :goto_1

    :catch_2
    move-object v0, v7

    goto :goto_3

    :catch_3
    goto :goto_3

    :catch_4
    goto :goto_2

    .line 668925
    :catch_5
    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
