.class public final LX/3lQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 633843
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 633844
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 633845
    :goto_0
    return v1

    .line 633846
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_5

    .line 633847
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 633848
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 633849
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_0

    if-eqz v7, :cond_0

    .line 633850
    const-string v8, "eligible_for_survey"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 633851
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v6, v0

    move v0, v2

    goto :goto_1

    .line 633852
    :cond_1
    const-string v8, "surveys_first_privacy_option"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 633853
    invoke-static {p0, p1}, LX/39L;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 633854
    :cond_2
    const-string v8, "surveys_second_privacy_option"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 633855
    invoke-static {p0, p1}, LX/39L;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 633856
    :cond_3
    const-string v8, "trigger_privacy_option"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 633857
    invoke-static {p0, p1}, LX/39L;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 633858
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 633859
    :cond_5
    const/4 v7, 0x4

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 633860
    if-eqz v0, :cond_6

    .line 633861
    invoke-virtual {p1, v1, v6}, LX/186;->a(IZ)V

    .line 633862
    :cond_6
    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 633863
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 633864
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 633865
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 633866
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 633867
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 633868
    if-eqz v0, :cond_0

    .line 633869
    const-string v1, "eligible_for_survey"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 633870
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 633871
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 633872
    if-eqz v0, :cond_1

    .line 633873
    const-string v1, "surveys_first_privacy_option"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 633874
    invoke-static {p0, v0, p2, p3}, LX/39L;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 633875
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 633876
    if-eqz v0, :cond_2

    .line 633877
    const-string v1, "surveys_second_privacy_option"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 633878
    invoke-static {p0, v0, p2, p3}, LX/39L;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 633879
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 633880
    if-eqz v0, :cond_3

    .line 633881
    const-string v1, "trigger_privacy_option"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 633882
    invoke-static {p0, v0, p2, p3}, LX/39L;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 633883
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 633884
    return-void
.end method
