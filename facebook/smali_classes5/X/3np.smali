.class public final enum LX/3np;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3np;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3np;

.field public static final enum ABR_CONFIG:LX/3np;

.field public static final enum API_CONFIG:LX/3np;

.field public static final enum AUDIO_CHANNEL_LAYOUT:LX/3np;

.field public static final enum CURRENT_STATE:LX/3np;

.field public static final enum DASH_STREAM:LX/3np;

.field public static final enum NEW_PLAYER:LX/3np;

.field public static final enum NEW_START_TIME:LX/3np;

.field public static final enum PROJECTION_TYPE:LX/3np;

.field public static final enum RELATED_VIDEO:LX/3np;

.field public static final enum SOURCE:LX/3np;

.field public static final enum STREAMING_FORMAT:LX/3np;

.field public static final enum STREAM_TYPE:LX/3np;

.field public static final enum TARGET_STATE:LX/3np;

.field public static final enum VIDEO_MIME:LX/3np;

.field public static final enum VIDEO_REUSE:LX/3np;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 639173
    new-instance v0, LX/3np;

    const-string v1, "SOURCE"

    const-string v2, "Source"

    invoke-direct {v0, v1, v4, v2}, LX/3np;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3np;->SOURCE:LX/3np;

    .line 639174
    new-instance v0, LX/3np;

    const-string v1, "CURRENT_STATE"

    const-string v2, "Current State"

    invoke-direct {v0, v1, v5, v2}, LX/3np;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3np;->CURRENT_STATE:LX/3np;

    .line 639175
    new-instance v0, LX/3np;

    const-string v1, "TARGET_STATE"

    const-string v2, "Target State"

    invoke-direct {v0, v1, v6, v2}, LX/3np;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3np;->TARGET_STATE:LX/3np;

    .line 639176
    new-instance v0, LX/3np;

    const-string v1, "NEW_START_TIME"

    const-string v2, "Stall Time"

    invoke-direct {v0, v1, v7, v2}, LX/3np;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3np;->NEW_START_TIME:LX/3np;

    .line 639177
    new-instance v0, LX/3np;

    const-string v1, "VIDEO_MIME"

    const-string v2, "Mime Type"

    invoke-direct {v0, v1, v8, v2}, LX/3np;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3np;->VIDEO_MIME:LX/3np;

    .line 639178
    new-instance v0, LX/3np;

    const-string v1, "API_CONFIG"

    const/4 v2, 0x5

    const-string v3, "Api Config"

    invoke-direct {v0, v1, v2, v3}, LX/3np;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3np;->API_CONFIG:LX/3np;

    .line 639179
    new-instance v0, LX/3np;

    const-string v1, "RELATED_VIDEO"

    const/4 v2, 0x6

    const-string v3, "Channels Eligibility"

    invoke-direct {v0, v1, v2, v3}, LX/3np;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3np;->RELATED_VIDEO:LX/3np;

    .line 639180
    new-instance v0, LX/3np;

    const-string v1, "NEW_PLAYER"

    const/4 v2, 0x7

    const-string v3, "New Player"

    invoke-direct {v0, v1, v2, v3}, LX/3np;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3np;->NEW_PLAYER:LX/3np;

    .line 639181
    new-instance v0, LX/3np;

    const-string v1, "VIDEO_REUSE"

    const/16 v2, 0x8

    const-string v3, "Reuse"

    invoke-direct {v0, v1, v2, v3}, LX/3np;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3np;->VIDEO_REUSE:LX/3np;

    .line 639182
    new-instance v0, LX/3np;

    const-string v1, "STREAMING_FORMAT"

    const/16 v2, 0x9

    const-string v3, "Streaming Format"

    invoke-direct {v0, v1, v2, v3}, LX/3np;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3np;->STREAMING_FORMAT:LX/3np;

    .line 639183
    new-instance v0, LX/3np;

    const-string v1, "DASH_STREAM"

    const/16 v2, 0xa

    const-string v3, "DASH Stream"

    invoke-direct {v0, v1, v2, v3}, LX/3np;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3np;->DASH_STREAM:LX/3np;

    .line 639184
    new-instance v0, LX/3np;

    const-string v1, "STREAM_TYPE"

    const/16 v2, 0xb

    const-string v3, "Stream Type"

    invoke-direct {v0, v1, v2, v3}, LX/3np;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3np;->STREAM_TYPE:LX/3np;

    .line 639185
    new-instance v0, LX/3np;

    const-string v1, "PROJECTION_TYPE"

    const/16 v2, 0xc

    const-string v3, "Projection Type"

    invoke-direct {v0, v1, v2, v3}, LX/3np;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3np;->PROJECTION_TYPE:LX/3np;

    .line 639186
    new-instance v0, LX/3np;

    const-string v1, "AUDIO_CHANNEL_LAYOUT"

    const/16 v2, 0xd

    const-string v3, "Audio Channel Layout"

    invoke-direct {v0, v1, v2, v3}, LX/3np;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3np;->AUDIO_CHANNEL_LAYOUT:LX/3np;

    .line 639187
    new-instance v0, LX/3np;

    const-string v1, "ABR_CONFIG"

    const/16 v2, 0xe

    const-string v3, "Abr Config"

    invoke-direct {v0, v1, v2, v3}, LX/3np;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3np;->ABR_CONFIG:LX/3np;

    .line 639188
    const/16 v0, 0xf

    new-array v0, v0, [LX/3np;

    sget-object v1, LX/3np;->SOURCE:LX/3np;

    aput-object v1, v0, v4

    sget-object v1, LX/3np;->CURRENT_STATE:LX/3np;

    aput-object v1, v0, v5

    sget-object v1, LX/3np;->TARGET_STATE:LX/3np;

    aput-object v1, v0, v6

    sget-object v1, LX/3np;->NEW_START_TIME:LX/3np;

    aput-object v1, v0, v7

    sget-object v1, LX/3np;->VIDEO_MIME:LX/3np;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/3np;->API_CONFIG:LX/3np;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/3np;->RELATED_VIDEO:LX/3np;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/3np;->NEW_PLAYER:LX/3np;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/3np;->VIDEO_REUSE:LX/3np;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/3np;->STREAMING_FORMAT:LX/3np;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/3np;->DASH_STREAM:LX/3np;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/3np;->STREAM_TYPE:LX/3np;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/3np;->PROJECTION_TYPE:LX/3np;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/3np;->AUDIO_CHANNEL_LAYOUT:LX/3np;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/3np;->ABR_CONFIG:LX/3np;

    aput-object v2, v0, v1

    sput-object v0, LX/3np;->$VALUES:[LX/3np;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 639189
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 639190
    iput-object p3, p0, LX/3np;->value:Ljava/lang/String;

    .line 639191
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3np;
    .locals 1

    .prologue
    .line 639192
    const-class v0, LX/3np;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3np;

    return-object v0
.end method

.method public static values()[LX/3np;
    .locals 1

    .prologue
    .line 639193
    sget-object v0, LX/3np;->$VALUES:[LX/3np;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3np;

    return-object v0
.end method
