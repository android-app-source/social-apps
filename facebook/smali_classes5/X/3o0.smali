.class public final LX/3o0;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Z

.field private static final b:Landroid/graphics/Paint;


# instance fields
.field private A:Z

.field public B:Landroid/graphics/Bitmap;

.field public C:Landroid/graphics/Paint;

.field public D:F

.field public E:F

.field private F:F

.field public G:F

.field private H:Z

.field public final I:Landroid/text/TextPaint;

.field public J:Landroid/view/animation/Interpolator;

.field public K:Landroid/view/animation/Interpolator;

.field private L:F

.field private M:F

.field private N:F

.field private O:I

.field public P:F

.field public Q:F

.field public R:F

.field public S:I

.field public final c:Landroid/view/View;

.field private d:Z

.field public e:F

.field public final f:Landroid/graphics/Rect;

.field public final g:Landroid/graphics/Rect;

.field public final h:Landroid/graphics/RectF;

.field public i:I

.field public j:I

.field public k:F

.field public l:F

.field public m:I

.field public n:I

.field public o:F

.field public p:F

.field public q:F

.field public r:F

.field private s:F

.field private t:F

.field public u:Landroid/graphics/Typeface;

.field public v:Landroid/graphics/Typeface;

.field private w:Landroid/graphics/Typeface;

.field public x:Ljava/lang/CharSequence;

.field public y:Ljava/lang/CharSequence;

.field public z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 639522
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-ge v0, v2, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, LX/3o0;->a:Z

    .line 639523
    const/4 v0, 0x0

    .line 639524
    sput-object v0, LX/3o0;->b:Landroid/graphics/Paint;

    goto :goto_1

    .line 639525
    :goto_1
    return-void

    .line 639526
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    const/16 v1, 0x10

    const/high16 v0, 0x41700000    # 15.0f

    .line 639499
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 639500
    iput v1, p0, LX/3o0;->i:I

    .line 639501
    iput v1, p0, LX/3o0;->j:I

    .line 639502
    iput v0, p0, LX/3o0;->k:F

    .line 639503
    iput v0, p0, LX/3o0;->l:F

    .line 639504
    iput-object p1, p0, LX/3o0;->c:Landroid/view/View;

    .line 639505
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, LX/3o0;->I:Landroid/text/TextPaint;

    .line 639506
    iget-object v0, p0, LX/3o0;->I:Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 639507
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/3o0;->g:Landroid/graphics/Rect;

    .line 639508
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/3o0;->f:Landroid/graphics/Rect;

    .line 639509
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/3o0;->h:Landroid/graphics/RectF;

    .line 639510
    return-void
.end method

.method public static a(FFFLandroid/view/animation/Interpolator;)F
    .locals 1

    .prologue
    .line 639511
    if-eqz p3, :cond_0

    .line 639512
    invoke-interface {p3, p2}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result p2

    .line 639513
    :cond_0
    invoke-static {p0, p1, p2}, LX/3ns;->a(FFF)F

    move-result v0

    return v0
.end method

.method private static a(IIF)I
    .locals 5

    .prologue
    .line 639514
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p2

    .line 639515
    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, p2

    add-float/2addr v1, v2

    .line 639516
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, p2

    add-float/2addr v2, v3

    .line 639517
    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v0

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, p2

    add-float/2addr v3, v4

    .line 639518
    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v0, v4

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, p2

    add-float/2addr v0, v4

    .line 639519
    float-to-int v1, v1

    float-to-int v2, v2

    float-to-int v3, v3

    float-to-int v0, v0

    invoke-static {v1, v2, v3, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method private static a(FF)Z
    .locals 2

    .prologue
    .line 639520
    sub-float v0, p0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3a83126f    # 0.001f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/graphics/Rect;IIII)Z
    .locals 1

    .prologue
    .line 639521
    iget v0, p0, Landroid/graphics/Rect;->left:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Landroid/graphics/Rect;->top:I

    if-ne v0, p2, :cond_0

    iget v0, p0, Landroid/graphics/Rect;->right:I

    if-ne v0, p3, :cond_0

    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    if-ne v0, p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/3o0;F)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 639478
    iget-object v0, p0, LX/3o0;->h:Landroid/graphics/RectF;

    iget-object v1, p0, LX/3o0;->f:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget-object v2, p0, LX/3o0;->g:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget-object v3, p0, LX/3o0;->J:Landroid/view/animation/Interpolator;

    invoke-static {v1, v2, p1, v3}, LX/3o0;->a(FFFLandroid/view/animation/Interpolator;)F

    move-result v1

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 639479
    iget-object v0, p0, LX/3o0;->h:Landroid/graphics/RectF;

    iget v1, p0, LX/3o0;->o:F

    iget v2, p0, LX/3o0;->p:F

    iget-object v3, p0, LX/3o0;->J:Landroid/view/animation/Interpolator;

    invoke-static {v1, v2, p1, v3}, LX/3o0;->a(FFFLandroid/view/animation/Interpolator;)F

    move-result v1

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 639480
    iget-object v0, p0, LX/3o0;->h:Landroid/graphics/RectF;

    iget-object v1, p0, LX/3o0;->f:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    iget-object v2, p0, LX/3o0;->g:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    iget-object v3, p0, LX/3o0;->J:Landroid/view/animation/Interpolator;

    invoke-static {v1, v2, p1, v3}, LX/3o0;->a(FFFLandroid/view/animation/Interpolator;)F

    move-result v1

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 639481
    iget-object v0, p0, LX/3o0;->h:Landroid/graphics/RectF;

    iget-object v1, p0, LX/3o0;->f:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    iget-object v2, p0, LX/3o0;->g:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    iget-object v3, p0, LX/3o0;->J:Landroid/view/animation/Interpolator;

    invoke-static {v1, v2, p1, v3}, LX/3o0;->a(FFFLandroid/view/animation/Interpolator;)F

    move-result v1

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 639482
    iget v0, p0, LX/3o0;->q:F

    iget v1, p0, LX/3o0;->r:F

    iget-object v2, p0, LX/3o0;->J:Landroid/view/animation/Interpolator;

    invoke-static {v0, v1, p1, v2}, LX/3o0;->a(FFFLandroid/view/animation/Interpolator;)F

    move-result v0

    iput v0, p0, LX/3o0;->s:F

    .line 639483
    iget v0, p0, LX/3o0;->o:F

    iget v1, p0, LX/3o0;->p:F

    iget-object v2, p0, LX/3o0;->J:Landroid/view/animation/Interpolator;

    invoke-static {v0, v1, p1, v2}, LX/3o0;->a(FFFLandroid/view/animation/Interpolator;)F

    move-result v0

    iput v0, p0, LX/3o0;->t:F

    .line 639484
    iget v0, p0, LX/3o0;->k:F

    iget v1, p0, LX/3o0;->l:F

    iget-object v2, p0, LX/3o0;->K:Landroid/view/animation/Interpolator;

    invoke-static {v0, v1, p1, v2}, LX/3o0;->a(FFFLandroid/view/animation/Interpolator;)F

    move-result v0

    invoke-static {p0, v0}, LX/3o0;->e(LX/3o0;F)V

    .line 639485
    iget v0, p0, LX/3o0;->n:I

    iget v1, p0, LX/3o0;->m:I

    if-eq v0, v1, :cond_0

    .line 639486
    iget-object v0, p0, LX/3o0;->I:Landroid/text/TextPaint;

    iget v1, p0, LX/3o0;->m:I

    iget v2, p0, LX/3o0;->n:I

    invoke-static {v1, v2, p1}, LX/3o0;->a(IIF)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 639487
    :goto_0
    iget-object v0, p0, LX/3o0;->I:Landroid/text/TextPaint;

    iget v1, p0, LX/3o0;->P:F

    iget v2, p0, LX/3o0;->L:F

    invoke-static {v1, v2, p1, v5}, LX/3o0;->a(FFFLandroid/view/animation/Interpolator;)F

    move-result v1

    iget v2, p0, LX/3o0;->Q:F

    iget v3, p0, LX/3o0;->M:F

    invoke-static {v2, v3, p1, v5}, LX/3o0;->a(FFFLandroid/view/animation/Interpolator;)F

    move-result v2

    iget v3, p0, LX/3o0;->R:F

    iget v4, p0, LX/3o0;->N:F

    invoke-static {v3, v4, p1, v5}, LX/3o0;->a(FFFLandroid/view/animation/Interpolator;)F

    move-result v3

    iget v4, p0, LX/3o0;->S:I

    iget v5, p0, LX/3o0;->O:I

    invoke-static {v4, v5, p1}, LX/3o0;->a(IIF)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 639488
    iget-object v0, p0, LX/3o0;->c:Landroid/view/View;

    invoke-static {v0}, LX/0vv;->d(Landroid/view/View;)V

    .line 639489
    return-void

    .line 639490
    :cond_0
    iget-object v0, p0, LX/3o0;->I:Landroid/text/TextPaint;

    iget v1, p0, LX/3o0;->n:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    goto :goto_0
.end method

.method public static e(LX/3o0;F)V
    .locals 9

    .prologue
    .line 639527
    invoke-static {p0, p1}, LX/3o0;->f(LX/3o0;F)V

    .line 639528
    sget-boolean v0, LX/3o0;->a:Z

    if-eqz v0, :cond_1

    iget v0, p0, LX/3o0;->F:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/3o0;->A:Z

    .line 639529
    iget-boolean v0, p0, LX/3o0;->A:Z

    if-eqz v0, :cond_0

    .line 639530
    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 639531
    iget-object v2, p0, LX/3o0;->B:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    iget-object v2, p0, LX/3o0;->f:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/3o0;->y:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 639532
    :cond_0
    :goto_1
    iget-object v0, p0, LX/3o0;->c:Landroid/view/View;

    invoke-static {v0}, LX/0vv;->d(Landroid/view/View;)V

    .line 639533
    return-void

    .line 639534
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 639535
    :cond_2
    invoke-static {p0, v6}, LX/3o0;->c(LX/3o0;F)V

    .line 639536
    iget-object v2, p0, LX/3o0;->I:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v2

    iput v2, p0, LX/3o0;->D:F

    .line 639537
    iget-object v2, p0, LX/3o0;->I:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->descent()F

    move-result v2

    iput v2, p0, LX/3o0;->E:F

    .line 639538
    iget-object v2, p0, LX/3o0;->I:Landroid/text/TextPaint;

    iget-object v3, p0, LX/3o0;->y:Ljava/lang/CharSequence;

    iget-object v5, p0, LX/3o0;->y:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 639539
    iget v3, p0, LX/3o0;->E:F

    iget v5, p0, LX/3o0;->D:F

    sub-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 639540
    if-gtz v2, :cond_3

    if-lez v7, :cond_0

    .line 639541
    :cond_3
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v7, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, LX/3o0;->B:Landroid/graphics/Bitmap;

    .line 639542
    new-instance v2, Landroid/graphics/Canvas;

    iget-object v3, p0, LX/3o0;->B:Landroid/graphics/Bitmap;

    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 639543
    iget-object v3, p0, LX/3o0;->y:Ljava/lang/CharSequence;

    iget-object v5, p0, LX/3o0;->y:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v5

    int-to-float v7, v7

    iget-object v8, p0, LX/3o0;->I:Landroid/text/TextPaint;

    invoke-virtual {v8}, Landroid/text/TextPaint;->descent()F

    move-result v8

    sub-float/2addr v7, v8

    iget-object v8, p0, LX/3o0;->I:Landroid/text/TextPaint;

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 639544
    iget-object v2, p0, LX/3o0;->C:Landroid/graphics/Paint;

    if-nez v2, :cond_0

    .line 639545
    new-instance v2, Landroid/graphics/Paint;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, LX/3o0;->C:Landroid/graphics/Paint;

    goto :goto_1
.end method

.method public static f(LX/3o0;F)V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 639546
    iget-object v0, p0, LX/3o0;->x:Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    .line 639547
    :cond_0
    :goto_0
    return-void

    .line 639548
    :cond_1
    iget v0, p0, LX/3o0;->l:F

    invoke-static {p1, v0}, LX/3o0;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 639549
    iget-object v0, p0, LX/3o0;->g:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v3, v0

    .line 639550
    iget v0, p0, LX/3o0;->l:F

    .line 639551
    iput v6, p0, LX/3o0;->F:F

    .line 639552
    iget-object v4, p0, LX/3o0;->w:Landroid/graphics/Typeface;

    iget-object v5, p0, LX/3o0;->u:Landroid/graphics/Typeface;

    if-eq v4, v5, :cond_9

    .line 639553
    iget-object v4, p0, LX/3o0;->u:Landroid/graphics/Typeface;

    iput-object v4, p0, LX/3o0;->w:Landroid/graphics/Typeface;

    move v4, v3

    move v3, v0

    move v0, v1

    .line 639554
    :goto_1
    const/4 v5, 0x0

    cmpl-float v5, v4, v5

    if-lez v5, :cond_7

    .line 639555
    iget v5, p0, LX/3o0;->G:F

    cmpl-float v5, v5, v3

    if-nez v5, :cond_2

    iget-boolean v5, p0, LX/3o0;->H:Z

    if-nez v5, :cond_2

    if-eqz v0, :cond_6

    .line 639556
    :cond_2
    :goto_2
    iput v3, p0, LX/3o0;->G:F

    .line 639557
    iput-boolean v2, p0, LX/3o0;->H:Z

    .line 639558
    :goto_3
    iget-object v0, p0, LX/3o0;->y:Ljava/lang/CharSequence;

    if-eqz v0, :cond_3

    if-eqz v1, :cond_0

    .line 639559
    :cond_3
    iget-object v0, p0, LX/3o0;->I:Landroid/text/TextPaint;

    iget v1, p0, LX/3o0;->G:F

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 639560
    iget-object v0, p0, LX/3o0;->I:Landroid/text/TextPaint;

    iget-object v1, p0, LX/3o0;->w:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 639561
    iget-object v0, p0, LX/3o0;->x:Ljava/lang/CharSequence;

    iget-object v1, p0, LX/3o0;->I:Landroid/text/TextPaint;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v0, v1, v4, v2}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 639562
    iget-object v1, p0, LX/3o0;->y:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 639563
    iput-object v0, p0, LX/3o0;->y:Ljava/lang/CharSequence;

    .line 639564
    iget-object v0, p0, LX/3o0;->y:Ljava/lang/CharSequence;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 639565
    iget-object v3, p0, LX/3o0;->c:Landroid/view/View;

    invoke-static {v3}, LX/0vv;->h(Landroid/view/View;)I

    move-result v3

    if-ne v3, v1, :cond_a

    .line 639566
    :goto_4
    if-eqz v1, :cond_b

    sget-object v1, LX/0zo;->d:LX/0zr;

    :goto_5
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    invoke-interface {v1, v0, v2, v3}, LX/0zr;->a(Ljava/lang/CharSequence;II)Z

    move-result v1

    move v0, v1

    .line 639567
    iput-boolean v0, p0, LX/3o0;->z:Z

    goto :goto_0

    .line 639568
    :cond_4
    iget-object v0, p0, LX/3o0;->f:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v4, v0

    .line 639569
    iget v3, p0, LX/3o0;->k:F

    .line 639570
    iget-object v0, p0, LX/3o0;->w:Landroid/graphics/Typeface;

    iget-object v5, p0, LX/3o0;->v:Landroid/graphics/Typeface;

    if-eq v0, v5, :cond_8

    .line 639571
    iget-object v0, p0, LX/3o0;->v:Landroid/graphics/Typeface;

    iput-object v0, p0, LX/3o0;->w:Landroid/graphics/Typeface;

    move v0, v1

    .line 639572
    :goto_6
    iget v5, p0, LX/3o0;->k:F

    invoke-static {p1, v5}, LX/3o0;->a(FF)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 639573
    iput v6, p0, LX/3o0;->F:F

    goto :goto_1

    .line 639574
    :cond_5
    iget v5, p0, LX/3o0;->k:F

    div-float v5, p1, v5

    iput v5, p0, LX/3o0;->F:F

    goto :goto_1

    :cond_6
    move v1, v2

    .line 639575
    goto :goto_2

    :cond_7
    move v1, v0

    goto :goto_3

    :cond_8
    move v0, v2

    goto :goto_6

    :cond_9
    move v4, v3

    move v3, v0

    move v0, v2

    goto/16 :goto_1

    :cond_a
    move v1, v2

    .line 639576
    goto :goto_4

    .line 639577
    :cond_b
    sget-object v1, LX/0zo;->c:LX/0zr;

    goto :goto_5
.end method

.method public static g(LX/3o0;I)Landroid/graphics/Typeface;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 639578
    iget-object v0, p0, LX/3o0;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [I

    const v2, 0x10103ac

    aput v2, v1, v3

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 639579
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 639580
    if-eqz v0, :cond_0

    .line 639581
    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 639582
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 639583
    :goto_0
    return-object v0

    .line 639584
    :cond_0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 639585
    const/4 v0, 0x0

    goto :goto_0

    .line 639586
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private j()V
    .locals 1

    .prologue
    .line 639587
    iget-object v0, p0, LX/3o0;->g:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, LX/3o0;->g:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, LX/3o0;->f:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, LX/3o0;->f:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/3o0;->d:Z

    .line 639588
    return-void

    .line 639589
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()V
    .locals 1

    .prologue
    .line 639590
    iget v0, p0, LX/3o0;->e:F

    invoke-static {p0, v0}, LX/3o0;->c(LX/3o0;F)V

    .line 639591
    return-void
.end method

.method public static n(LX/3o0;)V
    .locals 1

    .prologue
    .line 639491
    iget-object v0, p0, LX/3o0;->B:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 639492
    iget-object v0, p0, LX/3o0;->B:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 639493
    const/4 v0, 0x0

    iput-object v0, p0, LX/3o0;->B:Landroid/graphics/Bitmap;

    .line 639494
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 639495
    iget v0, p0, LX/3o0;->n:I

    if-eq v0, p1, :cond_0

    .line 639496
    iput p1, p0, LX/3o0;->n:I

    .line 639497
    invoke-virtual {p0}, LX/3o0;->g()V

    .line 639498
    :cond_0
    return-void
.end method

.method public final a(IIII)V
    .locals 1

    .prologue
    .line 639370
    iget-object v0, p0, LX/3o0;->f:Landroid/graphics/Rect;

    invoke-static {v0, p1, p2, p3, p4}, LX/3o0;->a(Landroid/graphics/Rect;IIII)Z

    move-result v0

    if-nez v0, :cond_0

    .line 639371
    iget-object v0, p0, LX/3o0;->f:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 639372
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3o0;->H:Z

    .line 639373
    invoke-direct {p0}, LX/3o0;->j()V

    .line 639374
    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 639375
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v7

    .line 639376
    iget-object v0, p0, LX/3o0;->y:Ljava/lang/CharSequence;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/3o0;->d:Z

    if-eqz v0, :cond_2

    .line 639377
    iget v4, p0, LX/3o0;->s:F

    .line 639378
    iget v5, p0, LX/3o0;->t:F

    .line 639379
    iget-boolean v0, p0, LX/3o0;->A:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/3o0;->B:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 639380
    :goto_0
    iget-object v1, p0, LX/3o0;->I:Landroid/text/TextPaint;

    iget v3, p0, LX/3o0;->G:F

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 639381
    if-eqz v0, :cond_4

    .line 639382
    iget v1, p0, LX/3o0;->D:F

    iget v3, p0, LX/3o0;->F:F

    mul-float/2addr v1, v3

    .line 639383
    :goto_1
    if-eqz v0, :cond_0

    .line 639384
    add-float/2addr v5, v1

    .line 639385
    :cond_0
    iget v1, p0, LX/3o0;->F:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_1

    .line 639386
    iget v1, p0, LX/3o0;->F:F

    iget v3, p0, LX/3o0;->F:F

    invoke-virtual {p1, v1, v3, v4, v5}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 639387
    :cond_1
    if-eqz v0, :cond_5

    .line 639388
    iget-object v0, p0, LX/3o0;->B:Landroid/graphics/Bitmap;

    iget-object v1, p0, LX/3o0;->C:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v4, v5, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 639389
    :cond_2
    :goto_2
    invoke-virtual {p1, v7}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 639390
    return-void

    :cond_3
    move v0, v2

    .line 639391
    goto :goto_0

    .line 639392
    :cond_4
    iget-object v1, p0, LX/3o0;->I:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->ascent()F

    const/4 v1, 0x0

    .line 639393
    iget-object v3, p0, LX/3o0;->I:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->descent()F

    goto :goto_1

    .line 639394
    :cond_5
    iget-object v1, p0, LX/3o0;->y:Ljava/lang/CharSequence;

    iget-object v0, p0, LX/3o0;->y:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    iget-object v6, p0, LX/3o0;->I:Landroid/text/TextPaint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    goto :goto_2
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 639395
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/3o0;->x:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 639396
    :cond_0
    iput-object p1, p0, LX/3o0;->x:Ljava/lang/CharSequence;

    .line 639397
    const/4 v0, 0x0

    iput-object v0, p0, LX/3o0;->y:Ljava/lang/CharSequence;

    .line 639398
    invoke-static {p0}, LX/3o0;->n(LX/3o0;)V

    .line 639399
    invoke-virtual {p0}, LX/3o0;->g()V

    .line 639400
    :cond_1
    return-void
.end method

.method public final b(F)V
    .locals 3

    .prologue
    .line 639401
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 639402
    cmpg-float v2, p1, v0

    if-gez v2, :cond_1

    :goto_0
    move v0, v0

    .line 639403
    iget v1, p0, LX/3o0;->e:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    .line 639404
    iput v0, p0, LX/3o0;->e:F

    .line 639405
    invoke-direct {p0}, LX/3o0;->k()V

    .line 639406
    :cond_0
    return-void

    :cond_1
    cmpl-float v2, p1, v1

    if-lez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, p1

    goto :goto_0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 639407
    iget v0, p0, LX/3o0;->m:I

    if-eq v0, p1, :cond_0

    .line 639408
    iput p1, p0, LX/3o0;->m:I

    .line 639409
    invoke-virtual {p0}, LX/3o0;->g()V

    .line 639410
    :cond_0
    return-void
.end method

.method public final b(IIII)V
    .locals 1

    .prologue
    .line 639411
    iget-object v0, p0, LX/3o0;->g:Landroid/graphics/Rect;

    invoke-static {v0, p1, p2, p3, p4}, LX/3o0;->a(Landroid/graphics/Rect;IIII)Z

    move-result v0

    if-nez v0, :cond_0

    .line 639412
    iget-object v0, p0, LX/3o0;->g:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 639413
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3o0;->H:Z

    .line 639414
    invoke-direct {p0}, LX/3o0;->j()V

    .line 639415
    :cond_0
    return-void
.end method

.method public final c()Landroid/graphics/Typeface;
    .locals 1

    .prologue
    .line 639416
    iget-object v0, p0, LX/3o0;->u:Landroid/graphics/Typeface;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3o0;->u:Landroid/graphics/Typeface;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    goto :goto_0
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 639417
    iget v0, p0, LX/3o0;->i:I

    if-eq v0, p1, :cond_0

    .line 639418
    iput p1, p0, LX/3o0;->i:I

    .line 639419
    invoke-virtual {p0}, LX/3o0;->g()V

    .line 639420
    :cond_0
    return-void
.end method

.method public final c(Landroid/graphics/Typeface;)V
    .locals 0

    .prologue
    .line 639421
    iput-object p1, p0, LX/3o0;->v:Landroid/graphics/Typeface;

    iput-object p1, p0, LX/3o0;->u:Landroid/graphics/Typeface;

    .line 639422
    invoke-virtual {p0}, LX/3o0;->g()V

    .line 639423
    return-void
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 639424
    iget v0, p0, LX/3o0;->j:I

    if-eq v0, p1, :cond_0

    .line 639425
    iput p1, p0, LX/3o0;->j:I

    .line 639426
    invoke-virtual {p0}, LX/3o0;->g()V

    .line 639427
    :cond_0
    return-void
.end method

.method public final e(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 639428
    iget-object v0, p0, LX/3o0;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->TextAppearance:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 639429
    const/16 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 639430
    const/16 v1, 0x3

    iget v2, p0, LX/3o0;->n:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, LX/3o0;->n:I

    .line 639431
    :cond_0
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 639432
    const/16 v1, 0x0

    iget v2, p0, LX/3o0;->l:F

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, LX/3o0;->l:F

    .line 639433
    :cond_1
    const/16 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, LX/3o0;->O:I

    .line 639434
    const/16 v1, 0x5

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, LX/3o0;->M:F

    .line 639435
    const/16 v1, 0x6

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, LX/3o0;->N:F

    .line 639436
    const/16 v1, 0x7

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, LX/3o0;->L:F

    .line 639437
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 639438
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_2

    .line 639439
    invoke-static {p0, p1}, LX/3o0;->g(LX/3o0;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, LX/3o0;->u:Landroid/graphics/Typeface;

    .line 639440
    :cond_2
    invoke-virtual {p0}, LX/3o0;->g()V

    .line 639441
    return-void
.end method

.method public final g()V
    .locals 9

    .prologue
    .line 639442
    iget-object v0, p0, LX/3o0;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, LX/3o0;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    if-lez v0, :cond_1

    .line 639443
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    .line 639444
    iget v5, p0, LX/3o0;->G:F

    .line 639445
    iget v0, p0, LX/3o0;->l:F

    invoke-static {p0, v0}, LX/3o0;->f(LX/3o0;F)V

    .line 639446
    iget-object v0, p0, LX/3o0;->y:Ljava/lang/CharSequence;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3o0;->I:Landroid/text/TextPaint;

    iget-object v2, p0, LX/3o0;->y:Ljava/lang/CharSequence;

    iget-object v6, p0, LX/3o0;->y:Ljava/lang/CharSequence;

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    invoke-virtual {v0, v2, v4, v6}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v0

    .line 639447
    :goto_0
    iget v6, p0, LX/3o0;->j:I

    iget-boolean v2, p0, LX/3o0;->z:Z

    if-eqz v2, :cond_3

    move v2, v3

    :goto_1
    invoke-static {v6, v2}, LX/1uf;->a(II)I

    move-result v2

    .line 639448
    and-int/lit8 v6, v2, 0x70

    sparse-switch v6, :sswitch_data_0

    .line 639449
    iget-object v6, p0, LX/3o0;->I:Landroid/text/TextPaint;

    invoke-virtual {v6}, Landroid/text/TextPaint;->descent()F

    move-result v6

    iget-object v7, p0, LX/3o0;->I:Landroid/text/TextPaint;

    invoke-virtual {v7}, Landroid/text/TextPaint;->ascent()F

    move-result v7

    sub-float/2addr v6, v7

    .line 639450
    div-float/2addr v6, v8

    iget-object v7, p0, LX/3o0;->I:Landroid/text/TextPaint;

    invoke-virtual {v7}, Landroid/text/TextPaint;->descent()F

    move-result v7

    sub-float/2addr v6, v7

    .line 639451
    iget-object v7, p0, LX/3o0;->g:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->centerY()I

    move-result v7

    int-to-float v7, v7

    add-float/2addr v6, v7

    iput v6, p0, LX/3o0;->p:F

    .line 639452
    :goto_2
    and-int/lit8 v2, v2, 0x7

    sparse-switch v2, :sswitch_data_1

    .line 639453
    iget-object v0, p0, LX/3o0;->g:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iput v0, p0, LX/3o0;->r:F

    .line 639454
    :goto_3
    iget v0, p0, LX/3o0;->k:F

    invoke-static {p0, v0}, LX/3o0;->f(LX/3o0;F)V

    .line 639455
    iget-object v0, p0, LX/3o0;->y:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3o0;->I:Landroid/text/TextPaint;

    iget-object v1, p0, LX/3o0;->y:Ljava/lang/CharSequence;

    iget-object v2, p0, LX/3o0;->y:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-virtual {v0, v1, v4, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v1

    .line 639456
    :cond_0
    iget v0, p0, LX/3o0;->i:I

    iget-boolean v2, p0, LX/3o0;->z:Z

    if-eqz v2, :cond_4

    :goto_4
    invoke-static {v0, v3}, LX/1uf;->a(II)I

    move-result v0

    .line 639457
    and-int/lit8 v2, v0, 0x70

    sparse-switch v2, :sswitch_data_2

    .line 639458
    iget-object v2, p0, LX/3o0;->I:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->descent()F

    move-result v2

    iget-object v3, p0, LX/3o0;->I:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->ascent()F

    move-result v3

    sub-float/2addr v2, v3

    .line 639459
    div-float/2addr v2, v8

    iget-object v3, p0, LX/3o0;->I:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->descent()F

    move-result v3

    sub-float/2addr v2, v3

    .line 639460
    iget-object v3, p0, LX/3o0;->f:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    iput v2, p0, LX/3o0;->o:F

    .line 639461
    :goto_5
    and-int/lit8 v0, v0, 0x7

    sparse-switch v0, :sswitch_data_3

    .line 639462
    iget-object v0, p0, LX/3o0;->f:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iput v0, p0, LX/3o0;->q:F

    .line 639463
    :goto_6
    invoke-static {p0}, LX/3o0;->n(LX/3o0;)V

    .line 639464
    invoke-static {p0, v5}, LX/3o0;->e(LX/3o0;F)V

    .line 639465
    invoke-direct {p0}, LX/3o0;->k()V

    .line 639466
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 639467
    goto/16 :goto_0

    :cond_3
    move v2, v4

    .line 639468
    goto/16 :goto_1

    .line 639469
    :sswitch_0
    iget-object v6, p0, LX/3o0;->g:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    int-to-float v6, v6

    iput v6, p0, LX/3o0;->p:F

    goto :goto_2

    .line 639470
    :sswitch_1
    iget-object v6, p0, LX/3o0;->g:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    iget-object v7, p0, LX/3o0;->I:Landroid/text/TextPaint;

    invoke-virtual {v7}, Landroid/text/TextPaint;->ascent()F

    move-result v7

    sub-float/2addr v6, v7

    iput v6, p0, LX/3o0;->p:F

    goto/16 :goto_2

    .line 639471
    :sswitch_2
    iget-object v2, p0, LX/3o0;->g:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v8

    sub-float v0, v2, v0

    iput v0, p0, LX/3o0;->r:F

    goto/16 :goto_3

    .line 639472
    :sswitch_3
    iget-object v2, p0, LX/3o0;->g:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    sub-float v0, v2, v0

    iput v0, p0, LX/3o0;->r:F

    goto/16 :goto_3

    :cond_4
    move v3, v4

    .line 639473
    goto :goto_4

    .line 639474
    :sswitch_4
    iget-object v2, p0, LX/3o0;->f:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    iput v2, p0, LX/3o0;->o:F

    goto :goto_5

    .line 639475
    :sswitch_5
    iget-object v2, p0, LX/3o0;->f:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget-object v3, p0, LX/3o0;->I:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->ascent()F

    move-result v3

    sub-float/2addr v2, v3

    iput v2, p0, LX/3o0;->o:F

    goto :goto_5

    .line 639476
    :sswitch_6
    iget-object v0, p0, LX/3o0;->f:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v1, v8

    sub-float/2addr v0, v1

    iput v0, p0, LX/3o0;->q:F

    goto :goto_6

    .line 639477
    :sswitch_7
    iget-object v0, p0, LX/3o0;->f:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    sub-float/2addr v0, v1

    iput v0, p0, LX/3o0;->q:F

    goto :goto_6

    nop

    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_1
        0x50 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_2
        0x5 -> :sswitch_3
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x30 -> :sswitch_5
        0x50 -> :sswitch_4
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0x1 -> :sswitch_6
        0x5 -> :sswitch_7
    .end sparse-switch
.end method
