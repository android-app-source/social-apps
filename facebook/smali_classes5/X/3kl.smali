.class public LX/3kl;
.super LX/16T;
.source ""


# static fields
.field public static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 633263
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENTS_COVER_PHOTO_SELECTOR_THEME_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/3kl;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 633264
    invoke-direct {p0}, LX/16T;-><init>()V

    .line 633265
    return-void
.end method

.method public static a(LX/0QB;)LX/3kl;
    .locals 1

    .prologue
    .line 633266
    invoke-static {}, LX/3kl;->d()LX/3kl;

    move-result-object v0

    return-object v0
.end method

.method private static d()LX/3kl;
    .locals 1

    .prologue
    .line 633261
    new-instance v0, LX/3kl;

    invoke-direct {v0}, LX/3kl;-><init>()V

    .line 633262
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 633258
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 633260
    const-string v0, "3819"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 633259
    sget-object v0, LX/3kl;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
