.class public final LX/40w;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 665294
    const/16 v16, 0x0

    .line 665295
    const/4 v15, 0x0

    .line 665296
    const/4 v14, 0x0

    .line 665297
    const-wide/16 v12, 0x0

    .line 665298
    const/4 v11, 0x0

    .line 665299
    const/4 v10, 0x0

    .line 665300
    const/4 v9, 0x0

    .line 665301
    const/4 v8, 0x0

    .line 665302
    const/4 v7, 0x0

    .line 665303
    const/4 v6, 0x0

    .line 665304
    const/4 v5, 0x0

    .line 665305
    const/4 v4, 0x0

    .line 665306
    const/4 v3, 0x0

    .line 665307
    const/4 v2, 0x0

    .line 665308
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_10

    .line 665309
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 665310
    const/4 v2, 0x0

    .line 665311
    :goto_0
    return v2

    .line 665312
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v18, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v18

    if-eq v3, v0, :cond_e

    .line 665313
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 665314
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 665315
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_0

    if-eqz v3, :cond_0

    .line 665316
    const-string v18, "android_app_config"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 665317
    invoke-static/range {p0 .. p1}, LX/40x;->a(LX/15w;LX/186;)I

    move-result v3

    move/from16 v17, v3

    goto :goto_1

    .line 665318
    :cond_1
    const-string v18, "android_store_url"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 665319
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v7, v3

    goto :goto_1

    .line 665320
    :cond_2
    const-string v18, "app_center_cover_image"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 665321
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v3

    move v6, v3

    goto :goto_1

    .line 665322
    :cond_3
    const-string v18, "average_star_rating"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 665323
    const/4 v2, 0x1

    .line 665324
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    goto :goto_1

    .line 665325
    :cond_4
    const-string v18, "canvas_url"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 665326
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move/from16 v16, v3

    goto :goto_1

    .line 665327
    :cond_5
    const-string v18, "global_usage_summary_sentence"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_6

    .line 665328
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v3

    move v15, v3

    goto/16 :goto_1

    .line 665329
    :cond_6
    const-string v18, "id"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_7

    .line 665330
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v14, v3

    goto/16 :goto_1

    .line 665331
    :cond_7
    const-string v18, "instant_experiences_settings"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_8

    .line 665332
    invoke-static/range {p0 .. p1}, LX/40O;->a(LX/15w;LX/186;)I

    move-result v3

    move v13, v3

    goto/16 :goto_1

    .line 665333
    :cond_8
    const-string v18, "name"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_9

    .line 665334
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v12, v3

    goto/16 :goto_1

    .line 665335
    :cond_9
    const-string v18, "privacy_url"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_a

    .line 665336
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v11, v3

    goto/16 :goto_1

    .line 665337
    :cond_a
    const-string v18, "social_usage_summary_sentence"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_b

    .line 665338
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v3

    move v10, v3

    goto/16 :goto_1

    .line 665339
    :cond_b
    const-string v18, "square_logo"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_c

    .line 665340
    invoke-static/range {p0 .. p1}, LX/4aW;->a(LX/15w;LX/186;)I

    move-result v3

    move v9, v3

    goto/16 :goto_1

    .line 665341
    :cond_c
    const-string v18, "terms_of_service_url"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 665342
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 665343
    :cond_d
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 665344
    :cond_e
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 665345
    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 665346
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 665347
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 665348
    if-eqz v2, :cond_f

    .line 665349
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 665350
    :cond_f
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 665351
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 665352
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 665353
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 665354
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 665355
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 665356
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 665357
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 665358
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 665359
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_10
    move/from16 v17, v16

    move/from16 v16, v11

    move v11, v6

    move v6, v14

    move v14, v9

    move v9, v4

    move/from16 v20, v7

    move v7, v15

    move v15, v10

    move v10, v5

    move-wide v4, v12

    move v13, v8

    move/from16 v12, v20

    move v8, v3

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 665360
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 665361
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 665362
    if-eqz v0, :cond_0

    .line 665363
    const-string v1, "android_app_config"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 665364
    invoke-static {p0, v0, p2}, LX/40x;->a(LX/15i;ILX/0nX;)V

    .line 665365
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 665366
    if-eqz v0, :cond_1

    .line 665367
    const-string v1, "android_store_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 665368
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 665369
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 665370
    if-eqz v0, :cond_2

    .line 665371
    const-string v1, "app_center_cover_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 665372
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 665373
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 665374
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_3

    .line 665375
    const-string v2, "average_star_rating"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 665376
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 665377
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 665378
    if-eqz v0, :cond_4

    .line 665379
    const-string v1, "canvas_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 665380
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 665381
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 665382
    if-eqz v0, :cond_5

    .line 665383
    const-string v1, "global_usage_summary_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 665384
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 665385
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 665386
    if-eqz v0, :cond_6

    .line 665387
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 665388
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 665389
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 665390
    if-eqz v0, :cond_7

    .line 665391
    const-string v1, "instant_experiences_settings"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 665392
    invoke-static {p0, v0, p2, p3}, LX/40O;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 665393
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 665394
    if-eqz v0, :cond_8

    .line 665395
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 665396
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 665397
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 665398
    if-eqz v0, :cond_9

    .line 665399
    const-string v1, "privacy_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 665400
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 665401
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 665402
    if-eqz v0, :cond_a

    .line 665403
    const-string v1, "social_usage_summary_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 665404
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 665405
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 665406
    if-eqz v0, :cond_b

    .line 665407
    const-string v1, "square_logo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 665408
    invoke-static {p0, v0, p2}, LX/4aW;->a(LX/15i;ILX/0nX;)V

    .line 665409
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 665410
    if-eqz v0, :cond_c

    .line 665411
    const-string v1, "terms_of_service_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 665412
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 665413
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 665414
    return-void
.end method
