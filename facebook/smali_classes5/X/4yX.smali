.class public final LX/4yX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K::",
        "Ljava/lang/Comparable",
        "<*>;V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field public final mapOfRanges:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/50M",
            "<TK;>;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0P1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "LX/50M",
            "<TK;>;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 821860
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 821861
    iput-object p1, p0, LX/4yX;->mapOfRanges:LX/0P1;

    .line 821862
    return-void
.end method


# virtual methods
.method public readResolve()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 821863
    iget-object v0, p0, LX/4yX;->mapOfRanges:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 821864
    sget-object v0, LX/4yZ;->a:LX/4yZ;

    move-object v0, v0

    .line 821865
    :goto_0
    return-object v0

    .line 821866
    :cond_0
    new-instance v2, LX/4yW;

    invoke-direct {v2}, LX/4yW;-><init>()V

    .line 821867
    iget-object v0, p0, LX/4yX;->mapOfRanges:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 821868
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/50M;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, LX/4yW;->a(LX/50M;Ljava/lang/Object;)LX/4yW;

    goto :goto_1

    .line 821869
    :cond_1
    invoke-virtual {v2}, LX/4yW;->a()LX/4yZ;

    move-result-object v0

    move-object v0, v0

    .line 821870
    goto :goto_0
.end method
