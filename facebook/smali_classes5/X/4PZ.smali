.class public LX/4PZ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 701031
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 701032
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 701033
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 701034
    :goto_0
    return v1

    .line 701035
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_3

    .line 701036
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 701037
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 701038
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v9, :cond_0

    .line 701039
    const-string v10, "height_ratio"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 701040
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v7

    goto :goto_1

    .line 701041
    :cond_1
    const-string v10, "hide_share_button"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 701042
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v6

    move v8, v6

    move v6, v7

    goto :goto_1

    .line 701043
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 701044
    :cond_3
    const/4 v9, 0x2

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 701045
    if-eqz v0, :cond_4

    move-object v0, p1

    .line 701046
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 701047
    :cond_4
    if-eqz v6, :cond_5

    .line 701048
    invoke-virtual {p1, v7, v8}, LX/186;->a(IZ)V

    .line 701049
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v6, v1

    move v0, v1

    move v8, v1

    move-wide v2, v4

    goto :goto_1
.end method
