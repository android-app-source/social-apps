.class public final LX/4xE;
.super Ljava/util/AbstractSet;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractSet",
        "<",
        "Lcom/google/common/collect/Table$Cell",
        "<TR;TC;TV;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0kC;


# direct methods
.method public constructor <init>(LX/0kC;)V
    .locals 0

    .prologue
    .line 820892
    iput-object p1, p0, LX/4xE;->a:LX/0kC;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 1

    .prologue
    .line 820890
    iget-object v0, p0, LX/4xE;->a:LX/0kC;

    invoke-virtual {v0}, LX/0kC;->a()V

    .line 820891
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 820883
    instance-of v0, p1, LX/51C;

    if-eqz v0, :cond_1

    .line 820884
    check-cast p1, LX/51C;

    .line 820885
    iget-object v0, p0, LX/4xE;->a:LX/0kC;

    invoke-virtual {v0}, LX/0kC;->g()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, LX/51C;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0, v2}, LX/0PM;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 820886
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1}, LX/51C;->b()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1}, LX/51C;->c()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2, v3}, LX/0PM;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v2

    invoke-static {v0, v2}, LX/0PN;->a(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 820887
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 820888
    goto :goto_0

    :cond_1
    move v0, v1

    .line 820889
    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/common/collect/Table$Cell",
            "<TR;TC;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 820868
    iget-object v0, p0, LX/4xE;->a:LX/0kC;

    invoke-virtual {v0}, LX/0kC;->c()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 820870
    instance-of v0, p1, LX/51C;

    if-eqz v0, :cond_1

    .line 820871
    check-cast p1, LX/51C;

    .line 820872
    iget-object v0, p0, LX/4xE;->a:LX/0kC;

    invoke-virtual {v0}, LX/0kC;->g()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, LX/51C;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0, v2}, LX/0PM;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 820873
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1}, LX/51C;->b()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1}, LX/51C;->c()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2, v3}, LX/0PM;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v2

    const/4 v3, 0x0

    .line 820874
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 820875
    :try_start_0
    invoke-interface {v0, v2}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    .line 820876
    :goto_0
    move v0, v3

    .line 820877
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 820878
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 820879
    goto :goto_1

    :cond_1
    move v0, v1

    .line 820880
    goto :goto_1

    .line 820881
    :catch_0
    goto :goto_0

    .line 820882
    :catch_1
    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 820869
    iget-object v0, p0, LX/4xE;->a:LX/0kC;

    invoke-virtual {v0}, LX/0kC;->h()I

    move-result v0

    return v0
.end method
