.class public LX/4eP;
.super Ljava/io/InputStream;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field private final a:Ljava/io/InputStream;

.field private final b:[B

.field private final c:LX/1FN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FN",
            "<[B>;"
        }
    .end annotation
.end field

.field private d:I

.field private e:I

.field private f:Z


# direct methods
.method public constructor <init>(Ljava/io/InputStream;[BLX/1FN;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "[B",
            "LX/1FN",
            "<[B>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 797288
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 797289
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    iput-object v0, p0, LX/4eP;->a:Ljava/io/InputStream;

    .line 797290
    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, LX/4eP;->b:[B

    .line 797291
    invoke-static {p3}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FN;

    iput-object v0, p0, LX/4eP;->c:LX/1FN;

    .line 797292
    iput v1, p0, LX/4eP;->d:I

    .line 797293
    iput v1, p0, LX/4eP;->e:I

    .line 797294
    iput-boolean v1, p0, LX/4eP;->f:Z

    .line 797295
    return-void
.end method

.method private a()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 797281
    iget v2, p0, LX/4eP;->e:I

    iget v3, p0, LX/4eP;->d:I

    if-ge v2, v3, :cond_0

    .line 797282
    :goto_0
    return v0

    .line 797283
    :cond_0
    iget-object v2, p0, LX/4eP;->a:Ljava/io/InputStream;

    iget-object v3, p0, LX/4eP;->b:[B

    invoke-virtual {v2, v3}, Ljava/io/InputStream;->read([B)I

    move-result v2

    .line 797284
    if-gtz v2, :cond_1

    move v0, v1

    .line 797285
    goto :goto_0

    .line 797286
    :cond_1
    iput v2, p0, LX/4eP;->d:I

    .line 797287
    iput v1, p0, LX/4eP;->e:I

    goto :goto_0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 797278
    iget-boolean v0, p0, LX/4eP;->f:Z

    if-eqz v0, :cond_0

    .line 797279
    new-instance v0, Ljava/io/IOException;

    const-string v1, "stream already closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 797280
    :cond_0
    return-void
.end method


# virtual methods
.method public final available()I
    .locals 2

    .prologue
    .line 797274
    iget v0, p0, LX/4eP;->e:I

    iget v1, p0, LX/4eP;->d:I

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->b(Z)V

    .line 797275
    invoke-direct {p0}, LX/4eP;->b()V

    .line 797276
    iget v0, p0, LX/4eP;->d:I

    iget v1, p0, LX/4eP;->e:I

    sub-int/2addr v0, v1

    iget-object v1, p0, LX/4eP;->a:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->available()I

    move-result v1

    add-int/2addr v0, v1

    return v0

    .line 797277
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 797269
    iget-boolean v0, p0, LX/4eP;->f:Z

    if-nez v0, :cond_0

    .line 797270
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4eP;->f:Z

    .line 797271
    iget-object v0, p0, LX/4eP;->c:LX/1FN;

    iget-object v1, p0, LX/4eP;->b:[B

    invoke-interface {v0, v1}, LX/1FN;->a(Ljava/lang/Object;)V

    .line 797272
    invoke-super {p0}, Ljava/io/InputStream;->close()V

    .line 797273
    :cond_0
    return-void
.end method

.method public final finalize()V
    .locals 2

    .prologue
    .line 797239
    iget-boolean v0, p0, LX/4eP;->f:Z

    if-nez v0, :cond_0

    .line 797240
    const-string v0, "PooledByteInputStream"

    const-string v1, "Finalized without closing"

    invoke-static {v0, v1}, LX/03J;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 797241
    invoke-virtual {p0}, LX/4eP;->close()V

    .line 797242
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 797243
    return-void
.end method

.method public final read()I
    .locals 3

    .prologue
    .line 797262
    iget v0, p0, LX/4eP;->e:I

    iget v1, p0, LX/4eP;->d:I

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->b(Z)V

    .line 797263
    invoke-direct {p0}, LX/4eP;->b()V

    .line 797264
    invoke-direct {p0}, LX/4eP;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 797265
    const/4 v0, -0x1

    .line 797266
    :goto_1
    return v0

    .line 797267
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 797268
    :cond_1
    iget-object v0, p0, LX/4eP;->b:[B

    iget v1, p0, LX/4eP;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4eP;->e:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    goto :goto_1
.end method

.method public final read([BII)I
    .locals 3

    .prologue
    .line 797253
    iget v0, p0, LX/4eP;->e:I

    iget v1, p0, LX/4eP;->d:I

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->b(Z)V

    .line 797254
    invoke-direct {p0}, LX/4eP;->b()V

    .line 797255
    invoke-direct {p0}, LX/4eP;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 797256
    const/4 v0, -0x1

    .line 797257
    :goto_1
    return v0

    .line 797258
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 797259
    :cond_1
    iget v0, p0, LX/4eP;->d:I

    iget v1, p0, LX/4eP;->e:I

    sub-int/2addr v0, v1

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 797260
    iget-object v1, p0, LX/4eP;->b:[B

    iget v2, p0, LX/4eP;->e:I

    invoke-static {v1, v2, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 797261
    iget v1, p0, LX/4eP;->e:I

    add-int/2addr v1, v0

    iput v1, p0, LX/4eP;->e:I

    goto :goto_1
.end method

.method public final skip(J)J
    .locals 7

    .prologue
    .line 797244
    iget v0, p0, LX/4eP;->e:I

    iget v1, p0, LX/4eP;->d:I

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->b(Z)V

    .line 797245
    invoke-direct {p0}, LX/4eP;->b()V

    .line 797246
    iget v0, p0, LX/4eP;->d:I

    iget v1, p0, LX/4eP;->e:I

    sub-int/2addr v0, v1

    .line 797247
    int-to-long v2, v0

    cmp-long v1, v2, p1

    if-ltz v1, :cond_1

    .line 797248
    iget v0, p0, LX/4eP;->e:I

    int-to-long v0, v0

    add-long/2addr v0, p1

    long-to-int v0, v0

    iput v0, p0, LX/4eP;->e:I

    .line 797249
    :goto_1
    return-wide p1

    .line 797250
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 797251
    :cond_1
    iget v1, p0, LX/4eP;->d:I

    iput v1, p0, LX/4eP;->e:I

    .line 797252
    int-to-long v2, v0

    iget-object v1, p0, LX/4eP;->a:Ljava/io/InputStream;

    int-to-long v4, v0

    sub-long v4, p1, v4

    invoke-virtual {v1, v4, v5}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    add-long p1, v2, v0

    goto :goto_1
.end method
