.class public LX/3v9;
.super LX/3uv;
.source ""

# interfaces
.implements Landroid/view/MenuItem;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3uv",
        "<",
        "LX/3qv;",
        ">;",
        "Landroid/view/MenuItem;"
    }
.end annotation


# instance fields
.field public c:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/3qv;)V
    .locals 0

    .prologue
    .line 650742
    invoke-direct {p0, p1, p2}, LX/3uv;-><init>(Landroid/content/Context;Ljava/lang/Object;)V

    .line 650743
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ActionProvider;)LX/3v4;
    .locals 2

    .prologue
    .line 650744
    new-instance v0, LX/3v4;

    iget-object v1, p0, LX/3uv;->a:Landroid/content/Context;

    invoke-direct {v0, p0, v1, p1}, LX/3v4;-><init>(LX/3v9;Landroid/content/Context;Landroid/view/ActionProvider;)V

    return-object v0
.end method

.method public final collapseActionView()Z
    .locals 1

    .prologue
    .line 650745
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0}, LX/3qv;->collapseActionView()Z

    move-result v0

    return v0
.end method

.method public final expandActionView()Z
    .locals 1

    .prologue
    .line 650746
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0}, LX/3qv;->expandActionView()Z

    move-result v0

    return v0
.end method

.method public final getActionProvider()Landroid/view/ActionProvider;
    .locals 2

    .prologue
    .line 650747
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0}, LX/3qv;->a()LX/3rR;

    move-result-object v0

    .line 650748
    instance-of v1, v0, LX/3v4;

    if-eqz v1, :cond_0

    .line 650749
    check-cast v0, LX/3v4;

    iget-object v0, v0, LX/3v4;->a:Landroid/view/ActionProvider;

    .line 650750
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getActionView()Landroid/view/View;
    .locals 2

    .prologue
    .line 650751
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0}, LX/3qv;->getActionView()Landroid/view/View;

    move-result-object v0

    .line 650752
    instance-of v1, v0, LX/3v6;

    if-eqz v1, :cond_0

    .line 650753
    check-cast v0, LX/3v6;

    .line 650754
    iget-object v1, v0, LX/3v6;->a:Landroid/view/CollapsibleActionView;

    check-cast v1, Landroid/view/View;

    move-object v0, v1

    .line 650755
    :cond_0
    return-object v0
.end method

.method public final getAlphabeticShortcut()C
    .locals 1

    .prologue
    .line 650756
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0}, LX/3qv;->getAlphabeticShortcut()C

    move-result v0

    return v0
.end method

.method public final getGroupId()I
    .locals 1

    .prologue
    .line 650757
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0}, LX/3qv;->getGroupId()I

    move-result v0

    return v0
.end method

.method public final getIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 650758
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0}, LX/3qv;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 650759
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0}, LX/3qv;->getIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId()I
    .locals 1

    .prologue
    .line 650760
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0}, LX/3qv;->getItemId()I

    move-result v0

    return v0
.end method

.method public final getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    .prologue
    .line 650761
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0}, LX/3qv;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    return-object v0
.end method

.method public final getNumericShortcut()C
    .locals 1

    .prologue
    .line 650762
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0}, LX/3qv;->getNumericShortcut()C

    move-result v0

    return v0
.end method

.method public final getOrder()I
    .locals 1

    .prologue
    .line 650763
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0}, LX/3qv;->getOrder()I

    move-result v0

    return v0
.end method

.method public final getSubMenu()Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 650764
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0}, LX/3qv;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3uv;->a(Landroid/view/SubMenu;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 650765
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0}, LX/3qv;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final getTitleCondensed()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 650766
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0}, LX/3qv;->getTitleCondensed()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final hasSubMenu()Z
    .locals 1

    .prologue
    .line 650767
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0}, LX/3qv;->hasSubMenu()Z

    move-result v0

    return v0
.end method

.method public final isActionViewExpanded()Z
    .locals 1

    .prologue
    .line 650768
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0}, LX/3qv;->isActionViewExpanded()Z

    move-result v0

    return v0
.end method

.method public final isCheckable()Z
    .locals 1

    .prologue
    .line 650769
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0}, LX/3qv;->isCheckable()Z

    move-result v0

    return v0
.end method

.method public final isChecked()Z
    .locals 1

    .prologue
    .line 650740
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0}, LX/3qv;->isChecked()Z

    move-result v0

    return v0
.end method

.method public final isEnabled()Z
    .locals 1

    .prologue
    .line 650741
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0}, LX/3qv;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public final isVisible()Z
    .locals 1

    .prologue
    .line 650718
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0}, LX/3qv;->isVisible()Z

    move-result v0

    return v0
.end method

.method public final setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 650715
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, LX/3v9;->a(Landroid/view/ActionProvider;)LX/3v4;

    move-result-object v1

    :goto_0
    invoke-interface {v0, v1}, LX/3qv;->a(LX/3rR;)LX/3qv;

    .line 650716
    return-object p0

    .line 650717
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final setActionView(I)Landroid/view/MenuItem;
    .locals 3

    .prologue
    .line 650710
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0, p1}, LX/3qv;->setActionView(I)Landroid/view/MenuItem;

    .line 650711
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0}, LX/3qv;->getActionView()Landroid/view/View;

    move-result-object v1

    .line 650712
    instance-of v0, v1, Landroid/view/CollapsibleActionView;

    if-eqz v0, :cond_0

    .line 650713
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    new-instance v2, LX/3v6;

    invoke-direct {v2, v1}, LX/3v6;-><init>(Landroid/view/View;)V

    invoke-interface {v0, v2}, LX/3qv;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    .line 650714
    :cond_0
    return-object p0
.end method

.method public final setActionView(Landroid/view/View;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650706
    instance-of v0, p1, Landroid/view/CollapsibleActionView;

    if-eqz v0, :cond_0

    .line 650707
    new-instance v0, LX/3v6;

    invoke-direct {v0, p1}, LX/3v6;-><init>(Landroid/view/View;)V

    move-object p1, v0

    .line 650708
    :cond_0
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0, p1}, LX/3qv;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    .line 650709
    return-object p0
.end method

.method public final setAlphabeticShortcut(C)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650704
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0, p1}, LX/3qv;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    .line 650705
    return-object p0
.end method

.method public final setCheckable(Z)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650692
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0, p1}, LX/3qv;->setCheckable(Z)Landroid/view/MenuItem;

    .line 650693
    return-object p0
.end method

.method public final setChecked(Z)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650696
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0, p1}, LX/3qv;->setChecked(Z)Landroid/view/MenuItem;

    .line 650697
    return-object p0
.end method

.method public final setEnabled(Z)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650698
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0, p1}, LX/3qv;->setEnabled(Z)Landroid/view/MenuItem;

    .line 650699
    return-object p0
.end method

.method public final setIcon(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650700
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0, p1}, LX/3qv;->setIcon(I)Landroid/view/MenuItem;

    .line 650701
    return-object p0
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650694
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0, p1}, LX/3qv;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 650695
    return-object p0
.end method

.method public final setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650719
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0, p1}, LX/3qv;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;

    .line 650720
    return-object p0
.end method

.method public final setNumericShortcut(C)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650721
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0, p1}, LX/3qv;->setNumericShortcut(C)Landroid/view/MenuItem;

    .line 650722
    return-object p0
.end method

.method public final setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 650723
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    if-eqz p1, :cond_0

    new-instance v1, LX/3v7;

    invoke-direct {v1, p0, p1}, LX/3v7;-><init>(LX/3v9;Landroid/view/MenuItem$OnActionExpandListener;)V

    :goto_0
    invoke-interface {v0, v1}, LX/3qv;->a(LX/3rk;)LX/3qv;

    .line 650724
    return-object p0

    .line 650725
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 650726
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    if-eqz p1, :cond_0

    new-instance v1, LX/3v8;

    invoke-direct {v1, p0, p1}, LX/3v8;-><init>(LX/3v9;Landroid/view/MenuItem$OnMenuItemClickListener;)V

    :goto_0
    invoke-interface {v0, v1}, LX/3qv;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 650727
    return-object p0

    .line 650728
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final setShortcut(CC)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650729
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0, p1, p2}, LX/3qv;->setShortcut(CC)Landroid/view/MenuItem;

    .line 650730
    return-object p0
.end method

.method public final setShowAsAction(I)V
    .locals 1

    .prologue
    .line 650731
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0, p1}, LX/3qv;->setShowAsAction(I)V

    .line 650732
    return-void
.end method

.method public final setShowAsActionFlags(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650702
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0, p1}, LX/3qv;->setShowAsActionFlags(I)Landroid/view/MenuItem;

    .line 650703
    return-object p0
.end method

.method public final setTitle(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650733
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0, p1}, LX/3qv;->setTitle(I)Landroid/view/MenuItem;

    .line 650734
    return-object p0
.end method

.method public final setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650735
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0, p1}, LX/3qv;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 650736
    return-object p0
.end method

.method public final setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650737
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0, p1}, LX/3qv;->setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 650738
    return-object p0
.end method

.method public final setVisible(Z)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650739
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qv;

    invoke-interface {v0, p1}, LX/3qv;->setVisible(Z)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method
