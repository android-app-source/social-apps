.class public LX/3Y4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 595227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 595228
    return-void
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 595229
    sget-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 595230
    sget-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 595231
    sget-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 595232
    sget-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 595233
    sget-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;->d:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 595234
    return-void
.end method
