.class public LX/3Rd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/3Rc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 580961
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 580962
    return-void
.end method

.method public static a(LX/8ue;LX/1bf;)Z
    .locals 1
    .param p0    # LX/8ue;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/1bf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 580963
    sget-object v0, LX/8ue;->SMS:LX/8ue;

    if-ne p0, v0, :cond_0

    if-eqz p1, :cond_0

    .line 580964
    iget-object v0, p1, LX/1bf;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 580965
    invoke-static {v0}, LX/1be;->g(Landroid/net/Uri;)Z

    move-result v0

    .line 580966
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/3Rd;
    .locals 2

    .prologue
    .line 580967
    new-instance v1, LX/3Rd;

    invoke-direct {v1}, LX/3Rd;-><init>()V

    .line 580968
    invoke-static {p0}, LX/3Rc;->a(LX/0QB;)LX/3Rc;

    move-result-object v0

    check-cast v0, LX/3Rc;

    .line 580969
    iput-object v0, v1, LX/3Rd;->a:LX/3Rc;

    .line 580970
    return-object v1
.end method


# virtual methods
.method public final a(LX/8Vc;)I
    .locals 2
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 580971
    invoke-interface {p1}, LX/8Vc;->b()LX/8ue;

    move-result-object v0

    sget-object v1, LX/8ue;->SMS:LX/8ue;

    if-ne v0, v1, :cond_0

    .line 580972
    iget-object v0, p0, LX/3Rd;->a:LX/3Rc;

    invoke-interface {p1}, LX/8Vc;->f()I

    move-result v1

    invoke-virtual {v0, v1}, LX/3Rc;->a(I)I

    move-result v0

    .line 580973
    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, LX/8Vc;->f()I

    move-result v0

    goto :goto_0
.end method
