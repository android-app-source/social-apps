.class public final enum LX/4gI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4gI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4gI;

.field public static final enum ALL:LX/4gI;

.field public static final enum PHOTO_ONLY:LX/4gI;

.field public static final enum PHOTO_ONLY_EXCLUDING_GIFS:LX/4gI;

.field public static final enum VIDEO_ONLY:LX/4gI;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 799342
    new-instance v0, LX/4gI;

    const-string v1, "PHOTO_ONLY"

    invoke-direct {v0, v1, v2}, LX/4gI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4gI;->PHOTO_ONLY:LX/4gI;

    .line 799343
    new-instance v0, LX/4gI;

    const-string v1, "VIDEO_ONLY"

    invoke-direct {v0, v1, v3}, LX/4gI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4gI;->VIDEO_ONLY:LX/4gI;

    .line 799344
    new-instance v0, LX/4gI;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v4}, LX/4gI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4gI;->ALL:LX/4gI;

    .line 799345
    new-instance v0, LX/4gI;

    const-string v1, "PHOTO_ONLY_EXCLUDING_GIFS"

    invoke-direct {v0, v1, v5}, LX/4gI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4gI;->PHOTO_ONLY_EXCLUDING_GIFS:LX/4gI;

    .line 799346
    const/4 v0, 0x4

    new-array v0, v0, [LX/4gI;

    sget-object v1, LX/4gI;->PHOTO_ONLY:LX/4gI;

    aput-object v1, v0, v2

    sget-object v1, LX/4gI;->VIDEO_ONLY:LX/4gI;

    aput-object v1, v0, v3

    sget-object v1, LX/4gI;->ALL:LX/4gI;

    aput-object v1, v0, v4

    sget-object v1, LX/4gI;->PHOTO_ONLY_EXCLUDING_GIFS:LX/4gI;

    aput-object v1, v0, v5

    sput-object v0, LX/4gI;->$VALUES:[LX/4gI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 799351
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4gI;
    .locals 1

    .prologue
    .line 799350
    const-class v0, LX/4gI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4gI;

    return-object v0
.end method

.method public static values()[LX/4gI;
    .locals 1

    .prologue
    .line 799349
    sget-object v0, LX/4gI;->$VALUES:[LX/4gI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4gI;

    return-object v0
.end method


# virtual methods
.method public final supportsPhotos()Z
    .locals 1

    .prologue
    .line 799348
    sget-object v0, LX/4gI;->PHOTO_ONLY:LX/4gI;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/4gI;->ALL:LX/4gI;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final supportsVideos()Z
    .locals 1

    .prologue
    .line 799347
    sget-object v0, LX/4gI;->VIDEO_ONLY:LX/4gI;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/4gI;->ALL:LX/4gI;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
