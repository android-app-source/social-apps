.class public final LX/3SC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/notifications/protocol/FetchJewelCountsGraphQLModels$FetchJewelCountsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/jewel/JewelCountHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/jewel/JewelCountHelper;)V
    .locals 0

    .prologue
    .line 582047
    iput-object p1, p0, LX/3SC;->a:Lcom/facebook/notifications/jewel/JewelCountHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 582048
    iget-object v0, p0, LX/3SC;->a:Lcom/facebook/notifications/jewel/JewelCountHelper;

    iget-object v0, v0, Lcom/facebook/notifications/jewel/JewelCountHelper;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x350004

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 582049
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 582050
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 582051
    if-eqz p1, :cond_0

    .line 582052
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 582053
    if-nez v0, :cond_1

    .line 582054
    :cond_0
    :goto_0
    return-void

    .line 582055
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 582056
    check-cast v0, Lcom/facebook/notifications/protocol/FetchJewelCountsGraphQLModels$FetchJewelCountsModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchJewelCountsGraphQLModels$FetchJewelCountsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 582057
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 582058
    if-eqz v0, :cond_5

    .line 582059
    iget-object v3, p0, LX/3SC;->a:Lcom/facebook/notifications/jewel/JewelCountHelper;

    iget-object v3, v3, Lcom/facebook/notifications/jewel/JewelCountHelper;->j:LX/0ad;

    sget-short v4, LX/15r;->G:S

    invoke-interface {v3, v4, v2}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 582060
    invoke-virtual {v1, v0, v5}, LX/15i;->j(II)I

    move-result v0

    move v1, v0

    .line 582061
    :goto_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 582062
    check-cast v0, Lcom/facebook/notifications/protocol/FetchJewelCountsGraphQLModels$FetchJewelCountsModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchJewelCountsGraphQLModels$FetchJewelCountsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 582063
    if-eqz v0, :cond_4

    .line 582064
    iget-object v4, p0, LX/3SC;->a:Lcom/facebook/notifications/jewel/JewelCountHelper;

    iget-object v4, v4, Lcom/facebook/notifications/jewel/JewelCountHelper;->g:LX/2gd;

    invoke-virtual {v4}, LX/2gd;->a()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3, v0, v2}, LX/15i;->j(II)I

    move-result v0

    .line 582065
    :goto_2
    iget-object v2, p0, LX/3SC;->a:Lcom/facebook/notifications/jewel/JewelCountHelper;

    iget-object v2, v2, Lcom/facebook/notifications/jewel/JewelCountHelper;->c:LX/0xB;

    sget-object v3, LX/12j;->FRIEND_REQUESTS:LX/12j;

    invoke-virtual {v2, v3, v1}, LX/0xB;->a(LX/12j;I)V

    .line 582066
    iget-object v1, p0, LX/3SC;->a:Lcom/facebook/notifications/jewel/JewelCountHelper;

    iget-object v1, v1, Lcom/facebook/notifications/jewel/JewelCountHelper;->i:LX/2PR;

    invoke-virtual {v1, v0}, LX/2PR;->a(I)V

    .line 582067
    iget-object v0, p0, LX/3SC;->a:Lcom/facebook/notifications/jewel/JewelCountHelper;

    iget-object v0, v0, Lcom/facebook/notifications/jewel/JewelCountHelper;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x350004

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto :goto_0

    .line 582068
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 582069
    :cond_2
    invoke-virtual {v1, v0, v2}, LX/15i;->j(II)I

    move-result v0

    move v1, v0

    goto :goto_1

    .line 582070
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 582071
    :cond_3
    invoke-virtual {v3, v0, v5}, LX/15i;->j(II)I

    move-result v0

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_1
.end method
