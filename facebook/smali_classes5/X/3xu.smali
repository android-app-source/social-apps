.class public LX/3xu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final a:Ljava/lang/Object;

.field private b:LX/3xv;

.field private c:Ljava/lang/Runnable;

.field private d:Z


# virtual methods
.method public final close()V
    .locals 2

    .prologue
    .line 660428
    iget-object v1, p0, LX/3xu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 660429
    :try_start_0
    iget-boolean v0, p0, LX/3xu;->d:Z

    if-eqz v0, :cond_0

    .line 660430
    monitor-exit v1

    .line 660431
    :goto_0
    return-void

    .line 660432
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3xu;->d:Z

    .line 660433
    iget-object v0, p0, LX/3xu;->b:LX/3xv;

    invoke-virtual {v0, p0}, LX/3xv;->a(LX/3xu;)V

    .line 660434
    const/4 v0, 0x0

    iput-object v0, p0, LX/3xu;->b:LX/3xv;

    .line 660435
    const/4 v0, 0x0

    iput-object v0, p0, LX/3xu;->c:Ljava/lang/Runnable;

    .line 660436
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
