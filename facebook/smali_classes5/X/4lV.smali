.class public LX/4lV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0xi;


# instance fields
.field public final a:LX/0wW;

.field public final b:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "LX/0xi;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "LX/0wd;",
            ">;"
        }
    .end annotation
.end field

.field public d:I

.field public final e:LX/0wT;

.field public final f:LX/0wT;


# direct methods
.method public constructor <init>(LX/0wW;LX/4lU;)V
    .locals 1
    .param p2    # LX/4lU;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 804496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 804497
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/4lV;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 804498
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/4lV;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 804499
    const/4 v0, -0x1

    iput v0, p0, LX/4lV;->d:I

    .line 804500
    iput-object p1, p0, LX/4lV;->a:LX/0wW;

    .line 804501
    if-nez p2, :cond_0

    .line 804502
    sget-object p2, LX/4lU;->c:LX/4lU;

    .line 804503
    :cond_0
    iget-object v0, p2, LX/4lU;->a:LX/0wT;

    iput-object v0, p0, LX/4lV;->e:LX/0wT;

    .line 804504
    iget-object v0, p2, LX/4lU;->b:LX/0wT;

    iput-object v0, p0, LX/4lV;->f:LX/0wT;

    .line 804505
    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 7

    .prologue
    const/4 v2, -0x1

    .line 804506
    iget-object v0, p0, LX/4lV;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 804507
    iget-object v0, p0, LX/4lV;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xi;

    .line 804508
    iget v1, p0, LX/4lV;->d:I

    if-ne v3, v1, :cond_2

    .line 804509
    add-int/lit8 v1, v3, -0x1

    .line 804510
    add-int/lit8 v2, v3, 0x1

    move v6, v1

    move v1, v2

    move v2, v6

    .line 804511
    :goto_0
    if-ltz v1, :cond_0

    iget-object v3, p0, LX/4lV;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 804512
    iget-object v3, p0, LX/4lV;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0wd;

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, LX/0wd;->b(D)LX/0wd;

    .line 804513
    :cond_0
    if-ltz v2, :cond_1

    iget-object v1, p0, LX/4lV;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_1

    .line 804514
    iget-object v1, p0, LX/4lV;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0wd;

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 804515
    :cond_1
    invoke-interface {v0, p1}, LX/0xi;->a(LX/0wd;)V

    .line 804516
    return-void

    .line 804517
    :cond_2
    iget v1, p0, LX/4lV;->d:I

    if-ge v3, v1, :cond_3

    .line 804518
    add-int/lit8 v1, v3, -0x1

    move v6, v1

    move v1, v2

    move v2, v6

    goto :goto_0

    .line 804519
    :cond_3
    iget v1, p0, LX/4lV;->d:I

    if-le v3, v1, :cond_4

    .line 804520
    add-int/lit8 v1, v3, 0x1

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public final b()LX/0wd;
    .locals 2

    .prologue
    .line 804521
    iget-object v0, p0, LX/4lV;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    iget v1, p0, LX/4lV;->d:I

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wd;

    return-object v0
.end method

.method public final b(LX/0wd;)V
    .locals 2

    .prologue
    .line 804522
    iget-object v0, p0, LX/4lV;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 804523
    iget-object v1, p0, LX/4lV;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xi;

    invoke-interface {v0, p1}, LX/0xi;->b(LX/0wd;)V

    .line 804524
    return-void
.end method

.method public final c(LX/0wd;)V
    .locals 2

    .prologue
    .line 804525
    iget-object v0, p0, LX/4lV;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 804526
    iget-object v1, p0, LX/4lV;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xi;

    invoke-interface {v0, p1}, LX/0xi;->c(LX/0wd;)V

    .line 804527
    return-void
.end method

.method public final d(LX/0wd;)V
    .locals 2

    .prologue
    .line 804528
    iget-object v0, p0, LX/4lV;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 804529
    iget-object v1, p0, LX/4lV;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xi;

    invoke-interface {v0, p1}, LX/0xi;->d(LX/0wd;)V

    .line 804530
    return-void
.end method
