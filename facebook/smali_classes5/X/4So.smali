.class public LX/4So;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 715668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 29

    .prologue
    .line 715736
    const/16 v25, 0x0

    .line 715737
    const/16 v24, 0x0

    .line 715738
    const/16 v23, 0x0

    .line 715739
    const/16 v22, 0x0

    .line 715740
    const/16 v21, 0x0

    .line 715741
    const/16 v20, 0x0

    .line 715742
    const/16 v19, 0x0

    .line 715743
    const/16 v18, 0x0

    .line 715744
    const/16 v17, 0x0

    .line 715745
    const/16 v16, 0x0

    .line 715746
    const/4 v15, 0x0

    .line 715747
    const/4 v14, 0x0

    .line 715748
    const/4 v13, 0x0

    .line 715749
    const/4 v12, 0x0

    .line 715750
    const/4 v11, 0x0

    .line 715751
    const/4 v10, 0x0

    .line 715752
    const/4 v9, 0x0

    .line 715753
    const/4 v8, 0x0

    .line 715754
    const/4 v7, 0x0

    .line 715755
    const/4 v6, 0x0

    .line 715756
    const/4 v5, 0x0

    .line 715757
    const/4 v4, 0x0

    .line 715758
    const/4 v3, 0x0

    .line 715759
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v26

    sget-object v27, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-eq v0, v1, :cond_1

    .line 715760
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 715761
    const/4 v3, 0x0

    .line 715762
    :goto_0
    return v3

    .line 715763
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 715764
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v26

    sget-object v27, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-eq v0, v1, :cond_12

    .line 715765
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v26

    .line 715766
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 715767
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_1

    if-eqz v26, :cond_1

    .line 715768
    const-string v27, "__type__"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-nez v27, :cond_2

    const-string v27, "__typename"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_3

    .line 715769
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/String;)I

    move-result v25

    goto :goto_1

    .line 715770
    :cond_3
    const-string v27, "android_urls"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_4

    .line 715771
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v24

    goto :goto_1

    .line 715772
    :cond_4
    const-string v27, "does_viewer_like"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_5

    .line 715773
    const/4 v9, 0x1

    .line 715774
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v23

    goto :goto_1

    .line 715775
    :cond_5
    const-string v27, "icon_image"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_6

    .line 715776
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v22

    goto :goto_1

    .line 715777
    :cond_6
    const-string v27, "id"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_7

    .line 715778
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    goto :goto_1

    .line 715779
    :cond_7
    const-string v27, "is_verified"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_8

    .line 715780
    const/4 v8, 0x1

    .line 715781
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v20

    goto/16 :goto_1

    .line 715782
    :cond_8
    const-string v27, "name"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_9

    .line 715783
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    goto/16 :goto_1

    .line 715784
    :cond_9
    const-string v27, "profile_picture"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_a

    .line 715785
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 715786
    :cond_a
    const-string v27, "query_title"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_b

    .line 715787
    invoke-static/range {p0 .. p1}, LX/4Nx;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 715788
    :cond_b
    const-string v27, "search_result_style_list"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_c

    .line 715789
    const-class v16, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, LX/2gu;->a(LX/15w;LX/186;Ljava/lang/Class;)I

    move-result v16

    goto/16 :goto_1

    .line 715790
    :cond_c
    const-string v27, "url"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_d

    .line 715791
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 715792
    :cond_d
    const-string v27, "viewer_saved_state"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_e

    .line 715793
    const/4 v7, 0x1

    .line 715794
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/facebook/graphql/enums/GraphQLSavedState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v14

    goto/16 :goto_1

    .line 715795
    :cond_e
    const-string v27, "friendship_status"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_f

    .line 715796
    const/4 v6, 0x1

    .line 715797
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v13

    goto/16 :goto_1

    .line 715798
    :cond_f
    const-string v27, "viewer_join_state"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_10

    .line 715799
    const/4 v5, 0x1

    .line 715800
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v12

    goto/16 :goto_1

    .line 715801
    :cond_10
    const-string v27, "community_category"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_11

    .line 715802
    const/4 v4, 0x1

    .line 715803
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v11

    goto/16 :goto_1

    .line 715804
    :cond_11
    const-string v27, "expressed_as_place"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_0

    .line 715805
    const/4 v3, 0x1

    .line 715806
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto/16 :goto_1

    .line 715807
    :cond_12
    const/16 v26, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 715808
    const/16 v26, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v26

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 715809
    const/16 v25, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 715810
    if-eqz v9, :cond_13

    .line 715811
    const/4 v9, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 715812
    :cond_13
    const/4 v9, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 715813
    const/4 v9, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 715814
    if-eqz v8, :cond_14

    .line 715815
    const/4 v8, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 715816
    :cond_14
    const/4 v8, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 715817
    const/16 v8, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 715818
    const/16 v8, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 715819
    const/16 v8, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 715820
    const/16 v8, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v15}, LX/186;->b(II)V

    .line 715821
    if-eqz v7, :cond_15

    .line 715822
    const/16 v7, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v14}, LX/186;->a(ILjava/lang/Enum;)V

    .line 715823
    :cond_15
    if-eqz v6, :cond_16

    .line 715824
    const/16 v6, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v13}, LX/186;->a(ILjava/lang/Enum;)V

    .line 715825
    :cond_16
    if-eqz v5, :cond_17

    .line 715826
    const/16 v5, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v12}, LX/186;->a(ILjava/lang/Enum;)V

    .line 715827
    :cond_17
    if-eqz v4, :cond_18

    .line 715828
    const/16 v4, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->a(ILjava/lang/Enum;)V

    .line 715829
    :cond_18
    if-eqz v3, :cond_19

    .line 715830
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->a(IZ)V

    .line 715831
    :cond_19
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/16 v5, 0xd

    const/16 v4, 0xc

    const/16 v3, 0xa

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 715669
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 715670
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 715671
    if-eqz v0, :cond_0

    .line 715672
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715673
    invoke-static {p0, p1, v2, p2}, LX/2bt;->a(LX/15i;IILX/0nX;)V

    .line 715674
    :cond_0
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 715675
    if-eqz v0, :cond_1

    .line 715676
    const-string v0, "android_urls"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715677
    invoke-virtual {p0, p1, v1}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 715678
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 715679
    if-eqz v0, :cond_2

    .line 715680
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715681
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 715682
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 715683
    if-eqz v0, :cond_3

    .line 715684
    const-string v1, "icon_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715685
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 715686
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 715687
    if-eqz v0, :cond_4

    .line 715688
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715689
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 715690
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 715691
    if-eqz v0, :cond_5

    .line 715692
    const-string v1, "is_verified"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715693
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 715694
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 715695
    if-eqz v0, :cond_6

    .line 715696
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715697
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 715698
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 715699
    if-eqz v0, :cond_7

    .line 715700
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715701
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 715702
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 715703
    if-eqz v0, :cond_8

    .line 715704
    const-string v1, "query_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715705
    invoke-static {p0, v0, p2}, LX/4Nx;->a(LX/15i;ILX/0nX;)V

    .line 715706
    :cond_8
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 715707
    if-eqz v0, :cond_9

    .line 715708
    const-string v0, "search_result_style_list"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715709
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->b(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->b(Ljava/util/Iterator;LX/0nX;)V

    .line 715710
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 715711
    if-eqz v0, :cond_a

    .line 715712
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715713
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 715714
    :cond_a
    invoke-virtual {p0, p1, v4, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 715715
    if-eqz v0, :cond_b

    .line 715716
    const-string v0, "viewer_saved_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715717
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLSavedState;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 715718
    :cond_b
    invoke-virtual {p0, p1, v5, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 715719
    if-eqz v0, :cond_c

    .line 715720
    const-string v0, "friendship_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715721
    const-class v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {p0, p1, v5, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 715722
    :cond_c
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 715723
    if-eqz v0, :cond_d

    .line 715724
    const-string v0, "viewer_join_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715725
    const/16 v0, 0xe

    const-class v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 715726
    :cond_d
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 715727
    if-eqz v0, :cond_e

    .line 715728
    const-string v0, "community_category"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715729
    const/16 v0, 0xf

    const-class v1, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 715730
    :cond_e
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 715731
    if-eqz v0, :cond_f

    .line 715732
    const-string v1, "expressed_as_place"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715733
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 715734
    :cond_f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 715735
    return-void
.end method
