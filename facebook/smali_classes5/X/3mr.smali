.class public final LX/3mr;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/3mZ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/3mR;

.field public b:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/3mZ;


# direct methods
.method public constructor <init>(LX/3mZ;)V
    .locals 1

    .prologue
    .line 637336
    iput-object p1, p0, LX/3mr;->c:LX/3mZ;

    .line 637337
    move-object v0, p1

    .line 637338
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 637339
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 637340
    const-string v0, "GroupsYouShouldJoinPageComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 637341
    if-ne p0, p1, :cond_1

    .line 637342
    :cond_0
    :goto_0
    return v0

    .line 637343
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 637344
    goto :goto_0

    .line 637345
    :cond_3
    check-cast p1, LX/3mr;

    .line 637346
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 637347
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 637348
    if-eq v2, v3, :cond_0

    .line 637349
    iget-object v2, p0, LX/3mr;->a:LX/3mR;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/3mr;->a:LX/3mR;

    iget-object v3, p1, LX/3mr;->a:LX/3mR;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 637350
    goto :goto_0

    .line 637351
    :cond_5
    iget-object v2, p1, LX/3mr;->a:LX/3mR;

    if-nez v2, :cond_4

    .line 637352
    :cond_6
    iget-object v2, p0, LX/3mr;->b:LX/1Po;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/3mr;->b:LX/1Po;

    iget-object v3, p1, LX/3mr;->b:LX/1Po;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 637353
    goto :goto_0

    .line 637354
    :cond_7
    iget-object v2, p1, LX/3mr;->b:LX/1Po;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
