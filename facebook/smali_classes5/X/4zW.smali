.class public final LX/4zW;
.super Ljava/util/AbstractQueue;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractQueue",
        "<",
        "LX/0qF",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field public final a:LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 822875
    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    .line 822876
    new-instance v0, LX/4zU;

    invoke-direct {v0, p0}, LX/4zU;-><init>(LX/4zW;)V

    iput-object v0, p0, LX/4zW;->a:LX/0qF;

    return-void
.end method

.method private a()LX/0qF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 822873
    iget-object v0, p0, LX/4zW;->a:LX/0qF;

    invoke-interface {v0}, LX/0qF;->getNextExpirable()LX/0qF;

    move-result-object v0

    .line 822874
    iget-object v1, p0, LX/4zW;->a:LX/0qF;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final clear()V
    .locals 2

    .prologue
    .line 822865
    iget-object v0, p0, LX/4zW;->a:LX/0qF;

    invoke-interface {v0}, LX/0qF;->getNextExpirable()LX/0qF;

    move-result-object v0

    .line 822866
    :goto_0
    iget-object v1, p0, LX/4zW;->a:LX/0qF;

    if-eq v0, v1, :cond_0

    .line 822867
    invoke-interface {v0}, LX/0qF;->getNextExpirable()LX/0qF;

    move-result-object v1

    .line 822868
    invoke-static {v0}, LX/0cn;->d(LX/0qF;)V

    move-object v0, v1

    .line 822869
    goto :goto_0

    .line 822870
    :cond_0
    iget-object v0, p0, LX/4zW;->a:LX/0qF;

    iget-object v1, p0, LX/4zW;->a:LX/0qF;

    invoke-interface {v0, v1}, LX/0qF;->setNextExpirable(LX/0qF;)V

    .line 822871
    iget-object v0, p0, LX/4zW;->a:LX/0qF;

    iget-object v1, p0, LX/4zW;->a:LX/0qF;

    invoke-interface {v0, v1}, LX/0qF;->setPreviousExpirable(LX/0qF;)V

    .line 822872
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 822863
    check-cast p1, LX/0qF;

    .line 822864
    invoke-interface {p1}, LX/0qF;->getNextExpirable()LX/0qF;

    move-result-object v0

    sget-object v1, LX/4zX;->INSTANCE:LX/4zX;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 2

    .prologue
    .line 822862
    iget-object v0, p0, LX/4zW;->a:LX/0qF;

    invoke-interface {v0}, LX/0qF;->getNextExpirable()LX/0qF;

    move-result-object v0

    iget-object v1, p0, LX/4zW;->a:LX/0qF;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "LX/0qF",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 822861
    new-instance v0, LX/4zV;

    invoke-direct {p0}, LX/4zW;->a()LX/0qF;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/4zV;-><init>(LX/4zW;LX/0qF;)V

    return-object v0
.end method

.method public final offer(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 822856
    check-cast p1, LX/0qF;

    .line 822857
    invoke-interface {p1}, LX/0qF;->getPreviousExpirable()LX/0qF;

    move-result-object v0

    invoke-interface {p1}, LX/0qF;->getNextExpirable()LX/0qF;

    move-result-object v1

    invoke-static {v0, v1}, LX/0cn;->a(LX/0qF;LX/0qF;)V

    .line 822858
    iget-object v0, p0, LX/4zW;->a:LX/0qF;

    invoke-interface {v0}, LX/0qF;->getPreviousExpirable()LX/0qF;

    move-result-object v0

    invoke-static {v0, p1}, LX/0cn;->a(LX/0qF;LX/0qF;)V

    .line 822859
    iget-object v0, p0, LX/4zW;->a:LX/0qF;

    invoke-static {p1, v0}, LX/0cn;->a(LX/0qF;LX/0qF;)V

    .line 822860
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic peek()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 822839
    invoke-direct {p0}, LX/4zW;->a()LX/0qF;

    move-result-object v0

    return-object v0
.end method

.method public final poll()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 822851
    iget-object v0, p0, LX/4zW;->a:LX/0qF;

    invoke-interface {v0}, LX/0qF;->getNextExpirable()LX/0qF;

    move-result-object v0

    .line 822852
    iget-object v1, p0, LX/4zW;->a:LX/0qF;

    if-ne v0, v1, :cond_0

    .line 822853
    const/4 v0, 0x0

    .line 822854
    :goto_0
    return-object v0

    .line 822855
    :cond_0
    invoke-virtual {p0, v0}, LX/4zW;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 822845
    check-cast p1, LX/0qF;

    .line 822846
    invoke-interface {p1}, LX/0qF;->getPreviousExpirable()LX/0qF;

    move-result-object v0

    .line 822847
    invoke-interface {p1}, LX/0qF;->getNextExpirable()LX/0qF;

    move-result-object v1

    .line 822848
    invoke-static {v0, v1}, LX/0cn;->a(LX/0qF;LX/0qF;)V

    .line 822849
    invoke-static {p1}, LX/0cn;->d(LX/0qF;)V

    .line 822850
    sget-object v0, LX/4zX;->INSTANCE:LX/4zX;

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 3

    .prologue
    .line 822840
    const/4 v1, 0x0

    .line 822841
    iget-object v0, p0, LX/4zW;->a:LX/0qF;

    invoke-interface {v0}, LX/0qF;->getNextExpirable()LX/0qF;

    move-result-object v0

    :goto_0
    iget-object v2, p0, LX/4zW;->a:LX/0qF;

    if-eq v0, v2, :cond_0

    .line 822842
    add-int/lit8 v1, v1, 0x1

    .line 822843
    invoke-interface {v0}, LX/0qF;->getNextExpirable()LX/0qF;

    move-result-object v0

    goto :goto_0

    .line 822844
    :cond_0
    return v1
.end method
