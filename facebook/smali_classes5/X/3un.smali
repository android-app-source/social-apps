.class public final LX/3un;
.super LX/3um;
.source ""


# instance fields
.field public final synthetic a:Landroid/support/v7/internal/view/menu/ActionMenuItemView;


# direct methods
.method public constructor <init>(Landroid/support/v7/internal/view/menu/ActionMenuItemView;)V
    .locals 0

    .prologue
    .line 649505
    iput-object p1, p0, LX/3un;->a:Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    .line 649506
    invoke-direct {p0, p1}, LX/3um;-><init>(Landroid/view/View;)V

    .line 649507
    return-void
.end method


# virtual methods
.method public final a()LX/3w1;
    .locals 1

    .prologue
    .line 649508
    iget-object v0, p0, LX/3un;->a:Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    iget-object v0, v0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->f:LX/3uo;

    if-eqz v0, :cond_0

    .line 649509
    iget-object v0, p0, LX/3un;->a:Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    iget-object v0, v0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->f:LX/3uo;

    invoke-virtual {v0}, LX/3uo;->a()LX/3w1;

    move-result-object v0

    .line 649510
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 649511
    iget-object v1, p0, LX/3un;->a:Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    iget-object v1, v1, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->d:LX/3uw;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/3un;->a:Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    iget-object v1, v1, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->d:LX/3uw;

    iget-object v2, p0, LX/3un;->a:Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    iget-object v2, v2, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->a:LX/3v3;

    invoke-interface {v1, v2}, LX/3uw;->a(LX/3v3;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 649512
    invoke-virtual {p0}, LX/3un;->a()LX/3w1;

    move-result-object v1

    .line 649513
    if-eqz v1, :cond_0

    invoke-virtual {v1}, LX/3w1;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 649514
    :cond_0
    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 649515
    invoke-virtual {p0}, LX/3un;->a()LX/3w1;

    move-result-object v0

    .line 649516
    if-eqz v0, :cond_0

    .line 649517
    invoke-virtual {v0}, LX/3w1;->a()V

    .line 649518
    const/4 v0, 0x1

    .line 649519
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
