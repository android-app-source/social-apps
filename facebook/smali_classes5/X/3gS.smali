.class public final LX/3gS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KL",
        "<",
        "Ljava/lang/String;",
        "LX/1yT;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3gR;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3gR;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 624443
    iput-object p1, p0, LX/3gS;->a:LX/3gR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 624444
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 624445
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 624446
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    .line 624447
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "story:explanation:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/3gS;->b:Ljava/lang/String;

    .line 624448
    iput-object p2, p0, LX/3gS;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 624449
    return-void

    .line 624450
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 624451
    new-instance v0, LX/1yT;

    iget-object v1, p0, LX/3gS;->a:LX/3gR;

    iget-object v1, v1, LX/3gR;->b:LX/3gP;

    iget-object v2, p0, LX/3gS;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1, v2}, LX/3gP;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/text/Spannable;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/1yT;-><init>(Landroid/text/Spannable;Z)V

    return-object v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 624452
    iget-object v0, p0, LX/3gS;->b:Ljava/lang/String;

    return-object v0
.end method
