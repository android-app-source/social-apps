.class public LX/3yK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z

.field private c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/2Ff;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3yL;)V
    .locals 5

    .prologue
    .line 660956
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 660957
    iget-object v0, p1, LX/3yL;->a:Ljava/lang/String;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 660958
    const-string v1, "You have to supply a name for your QuickExperimentSpecification"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 660959
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "Invalid name: \"%s\" - experiment names may not contain spaces"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-static {v1, v4, v2}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 660960
    move-object v0, v0

    .line 660961
    iput-object v0, p0, LX/3yK;->a:Ljava/lang/String;

    .line 660962
    iget-object v0, p1, LX/3yL;->b:Ljava/util/Set;

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/3yK;->c:LX/0Rf;

    .line 660963
    iget-boolean v0, p1, LX/3yL;->c:Z

    iput-boolean v0, p0, LX/3yK;->b:Z

    .line 660964
    return-void

    :cond_0
    move v1, v3

    .line 660965
    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/2Ff;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 660966
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3yK;->c:LX/0Rf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
