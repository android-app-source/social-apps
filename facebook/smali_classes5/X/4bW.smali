.class public LX/4bW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/4c7;

.field private final b:Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;

.field private final c:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "LX/4bV;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final d:LX/4bP;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private e:LX/4c8;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/4bP;LX/4c7;Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;LX/4c8;LX/4c6;)V
    .locals 1
    .param p4    # LX/4c8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 793991
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 793992
    iput-object p1, p0, LX/4bW;->d:LX/4bP;

    .line 793993
    iput-object p2, p0, LX/4bW;->a:LX/4c7;

    .line 793994
    iput-object p3, p0, LX/4bW;->b:Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;

    .line 793995
    iput-object p4, p0, LX/4bW;->e:LX/4c8;

    .line 793996
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0, p5}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, LX/4bW;->c:Ljava/util/TreeSet;

    .line 793997
    return-void
.end method

.method private declared-synchronized c(LX/4bV;)V
    .locals 10

    .prologue
    .line 793951
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4bW;->b:Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;

    .line 793952
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v6

    const-wide v8, 0x3f847ae147ae147bL    # 0.01

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_2

    .line 793953
    iget-object v6, v0, Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;->a:LX/0Zb;

    const-string v7, "request_queue_snapshot"

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v6

    .line 793954
    invoke-virtual {v6}, LX/0oG;->a()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 793955
    :goto_0
    move-object v1, v6

    .line 793956
    if-eqz v1, :cond_0

    .line 793957
    iget-object v0, p0, LX/4bW;->b:Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;

    iget-object v2, p0, LX/4bW;->a:LX/4c7;

    iget-object v3, p0, LX/4bW;->d:LX/4bP;

    iget-object v4, p0, LX/4bW;->e:LX/4c8;

    invoke-virtual {v2, p1, v3, v4}, LX/4c7;->a(LX/4bV;LX/4bP;LX/4c8;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v3, 0x1

    :goto_1
    invoke-virtual {p0}, LX/4bW;->c()LX/2Ee;

    move-result-object v4

    iget-object v5, p0, LX/4bW;->e:LX/4c8;

    move-object v2, p1

    .line 793958
    iget-object v6, v2, LX/4bV;->c:LX/15D;

    const-string v7, "request"

    invoke-static {v1, v6, v7}, Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;->a(LX/0oG;LX/15D;Ljava/lang/String;)V

    .line 793959
    const-string v6, "request_blocked"

    invoke-virtual {v1, v6, v3}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 793960
    const-string v6, "big_limit"

    iget v7, v5, LX/4c8;->a:I

    invoke-virtual {v1, v6, v7}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 793961
    const-string v6, "interactive_limit"

    iget v7, v5, LX/4c8;->b:I

    invoke-virtual {v1, v6, v7}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 793962
    const-string v6, "below_interactive_limit"

    iget v7, v5, LX/4c8;->c:I

    invoke-virtual {v1, v6, v7}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 793963
    const-string v6, "total_limit"

    iget v7, v5, LX/4c8;->d:I

    invoke-virtual {v1, v6, v7}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 793964
    const-string v6, "connection_class"

    iget-object v7, v0, Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;->b:LX/0oz;

    invoke-virtual {v7}, LX/0oz;->c()LX/0p3;

    move-result-object v7

    invoke-virtual {v7}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 793965
    const-string v6, "in_flight_count"

    .line 793966
    iget-object v7, v4, LX/2Ee;->a:Ljava/util/ArrayList;

    move-object v7, v7

    .line 793967
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v1, v6, v7}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 793968
    const-string v6, "in_flight_big_count"

    .line 793969
    iget-object v7, v4, LX/2Ee;->a:Ljava/util/ArrayList;

    move-object v7, v7

    .line 793970
    invoke-static {v7}, Lcom/facebook/http/common/FbHttpUtils;->b(Ljava/util/ArrayList;)I

    move-result v7

    invoke-virtual {v1, v6, v7}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 793971
    const-string v6, "in_flight_snapshot"

    .line 793972
    iget-object v7, v4, LX/2Ee;->a:Ljava/util/ArrayList;

    move-object v7, v7

    .line 793973
    invoke-static {v7}, Lcom/facebook/http/common/FbHttpUtils;->a(Ljava/util/ArrayList;)LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 793974
    iget-object v6, v4, LX/2Ee;->a:Ljava/util/ArrayList;

    move-object v6, v6

    .line 793975
    const-string v7, "in_flight"

    invoke-static {v1, v6, v7}, Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;->a(LX/0oG;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 793976
    const-string v6, "in_queue_count"

    .line 793977
    iget-object v7, v4, LX/2Ee;->b:Ljava/util/ArrayList;

    move-object v7, v7

    .line 793978
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v1, v6, v7}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 793979
    const-string v6, "in_queue_big_count"

    .line 793980
    iget-object v7, v4, LX/2Ee;->b:Ljava/util/ArrayList;

    move-object v7, v7

    .line 793981
    invoke-static {v7}, Lcom/facebook/http/common/FbHttpUtils;->b(Ljava/util/ArrayList;)I

    move-result v7

    invoke-virtual {v1, v6, v7}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 793982
    const-string v6, "in_queue_snapshot"

    .line 793983
    iget-object v7, v4, LX/2Ee;->b:Ljava/util/ArrayList;

    move-object v7, v7

    .line 793984
    invoke-static {v7}, Lcom/facebook/http/common/FbHttpUtils;->a(Ljava/util/ArrayList;)LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 793985
    iget-object v6, v4, LX/2Ee;->b:Ljava/util/ArrayList;

    move-object v6, v6

    .line 793986
    const-string v7, "in_queue"

    invoke-static {v1, v6, v7}, Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;->a(LX/0oG;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 793987
    invoke-virtual {v1}, LX/0oG;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 793988
    :cond_0
    monitor-exit p0

    return-void

    .line 793989
    :cond_1
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 793990
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    const/4 v6, 0x0

    goto/16 :goto_0
.end method

.method private static declared-synchronized d(LX/4bW;)LX/4bV;
    .locals 6

    .prologue
    .line 793908
    monitor-enter p0

    const/4 v1, 0x0

    .line 793909
    :try_start_0
    iget-object v0, p0, LX/4bW;->c:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 793910
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 793911
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4bV;

    .line 793912
    iget-object v3, p0, LX/4bW;->a:LX/4c7;

    iget-object v4, p0, LX/4bW;->d:LX/4bP;

    iget-object v5, p0, LX/4bW;->e:LX/4c8;

    invoke-virtual {v3, v0, v4, v5}, LX/4c7;->a(LX/4bV;LX/4bP;LX/4c8;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    .line 793913
    :goto_0
    monitor-exit p0

    return-object v0

    .line 793914
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(LX/15D;)LX/4bV;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15D",
            "<*>;)",
            "LX/4bV;"
        }
    .end annotation

    .prologue
    .line 793944
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4bW;->c:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 793945
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 793946
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4bV;

    .line 793947
    iget-object v2, v0, LX/4bV;->c:LX/15D;

    if-ne v2, p1, :cond_0

    .line 793948
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 793949
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 793950
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()LX/4c8;
    .locals 1

    .prologue
    .line 793943
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4bW;->e:LX/4c8;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/4bR;)V
    .locals 2

    .prologue
    .line 793940
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4bW;->c:Ljava/util/TreeSet;

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, LX/4bW;->d:LX/4bP;

    invoke-virtual {v1}, LX/4bP;->c()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/4bR;->a(Ljava/util/List;Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 793941
    monitor-exit p0

    return-void

    .line 793942
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/4bV;)V
    .locals 1

    .prologue
    .line 793935
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, LX/4bW;->c(LX/4bV;)V

    .line 793936
    iget-object v0, p0, LX/4bW;->c:Ljava/util/TreeSet;

    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 793937
    const v0, -0x1e9c77ab

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 793938
    monitor-exit p0

    return-void

    .line 793939
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/4c8;)V
    .locals 1

    .prologue
    .line 793931
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/4bW;->e:LX/4c8;

    .line 793932
    const v0, -0x12a0e3ff

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 793933
    monitor-exit p0

    return-void

    .line 793934
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()LX/4bV;
    .locals 4

    .prologue
    .line 793924
    monitor-enter p0

    :goto_0
    :try_start_0
    invoke-static {p0}, LX/4bW;->d(LX/4bW;)LX/4bV;

    move-result-object v0

    if-nez v0, :cond_0

    .line 793925
    const v0, 0x656f6501

    invoke-static {p0, v0}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 793926
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 793927
    :cond_0
    :try_start_1
    iget-object v1, p0, LX/4bW;->c:Ljava/util/TreeSet;

    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 793928
    iget-object v1, p0, LX/4bW;->d:LX/4bP;

    iget-object v2, v0, LX/4bV;->c:LX/15D;

    .line 793929
    iget-object v3, v1, LX/4bP;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 793930
    monitor-exit p0

    return-object v0
.end method

.method public final declared-synchronized b(LX/4bV;)V
    .locals 2

    .prologue
    .line 793920
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4bW;->d:LX/4bP;

    iget-object v1, p1, LX/4bV;->c:LX/15D;

    .line 793921
    iget-object p1, v0, LX/4bP;->a:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 793922
    monitor-exit p0

    return-void

    .line 793923
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()LX/2Ee;
    .locals 3

    .prologue
    .line 793915
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 793916
    iget-object v0, p0, LX/4bW;->c:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4bV;

    .line 793917
    iget-object v0, v0, LX/4bV;->c:LX/15D;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 793918
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 793919
    :cond_0
    :try_start_1
    new-instance v0, LX/2Ee;

    iget-object v2, p0, LX/4bW;->d:LX/4bP;

    invoke-virtual {v2}, LX/4bP;->c()Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {v0, v2, v1}, LX/2Ee;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method
