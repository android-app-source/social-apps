.class public LX/4hC;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final CHARACTER_CLASS_PATTERN:Ljava/util/regex/Pattern;

.field private static final DIGIT_PATTERN:Ljava/util/regex/Pattern;

.field public static final ELIGIBLE_FORMAT_PATTERN:Ljava/util/regex/Pattern;

.field private static final EMPTY_METADATA:LX/4hL;

.field private static final NATIONAL_PREFIX_SEPARATORS_PATTERN:Ljava/util/regex/Pattern;

.field private static final STANDALONE_DIGIT_PATTERN:Ljava/util/regex/Pattern;


# instance fields
.field public ableToFormat:Z

.field public accruedInput:Ljava/lang/StringBuilder;

.field public accruedInputWithoutFormatting:Ljava/lang/StringBuilder;

.field private currentFormattingPattern:Ljava/lang/String;

.field public currentMetadata:LX/4hL;

.field public currentOutput:Ljava/lang/String;

.field private defaultCountry:Ljava/lang/String;

.field private defaultMetadata:LX/4hL;

.field public extractedNationalPrefix:Ljava/lang/String;

.field private formattingTemplate:Ljava/lang/StringBuilder;

.field private inputHasFormatting:Z

.field public isCompleteNumber:Z

.field private isExpectingCountryCallingCode:Z

.field private lastMatchPosition:I

.field public nationalNumber:Ljava/lang/StringBuilder;

.field public originalPosition:I

.field private final phoneUtil:LX/3Lz;

.field public positionToRemember:I

.field public possibleFormats:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/4hJ;",
            ">;"
        }
    .end annotation
.end field

.field public prefixBeforeNationalNumber:Ljava/lang/StringBuilder;

.field public regexCache:LX/3MC;

.field private shouldAddSpaceAfterNationalPrefix:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 800530
    new-instance v0, LX/4hL;

    invoke-direct {v0}, LX/4hL;-><init>()V

    const-string v1, "NA"

    invoke-virtual {v0, v1}, LX/4hL;->setInternationalPrefix(Ljava/lang/String;)LX/4hL;

    move-result-object v0

    sput-object v0, LX/4hC;->EMPTY_METADATA:LX/4hL;

    .line 800531
    const-string v0, "\\[([^\\[\\]])*\\]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/4hC;->CHARACTER_CLASS_PATTERN:Ljava/util/regex/Pattern;

    .line 800532
    const-string v0, "\\d(?=[^,}][^,}])"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/4hC;->STANDALONE_DIGIT_PATTERN:Ljava/util/regex/Pattern;

    .line 800533
    const-string v0, "[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e]*(\\$\\d[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e]*)+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/4hC;->ELIGIBLE_FORMAT_PATTERN:Ljava/util/regex/Pattern;

    .line 800534
    const-string v0, "[- ]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/4hC;->NATIONAL_PREFIX_SEPARATORS_PATTERN:Ljava/util/regex/Pattern;

    .line 800535
    const-string v0, "\u2008"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/4hC;->DIGIT_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/3Lz;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 800781
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 800782
    const-string v0, ""

    iput-object v0, p0, LX/4hC;->currentOutput:Ljava/lang/String;

    .line 800783
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, LX/4hC;->formattingTemplate:Ljava/lang/StringBuilder;

    .line 800784
    const-string v0, ""

    iput-object v0, p0, LX/4hC;->currentFormattingPattern:Ljava/lang/String;

    .line 800785
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, LX/4hC;->accruedInput:Ljava/lang/StringBuilder;

    .line 800786
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, LX/4hC;->accruedInputWithoutFormatting:Ljava/lang/StringBuilder;

    .line 800787
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hC;->ableToFormat:Z

    .line 800788
    iput-boolean v1, p0, LX/4hC;->inputHasFormatting:Z

    .line 800789
    iput-boolean v1, p0, LX/4hC;->isCompleteNumber:Z

    .line 800790
    iput-boolean v1, p0, LX/4hC;->isExpectingCountryCallingCode:Z

    .line 800791
    iput v1, p0, LX/4hC;->lastMatchPosition:I

    .line 800792
    iput v1, p0, LX/4hC;->originalPosition:I

    .line 800793
    iput v1, p0, LX/4hC;->positionToRemember:I

    .line 800794
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, LX/4hC;->prefixBeforeNationalNumber:Ljava/lang/StringBuilder;

    .line 800795
    iput-boolean v1, p0, LX/4hC;->shouldAddSpaceAfterNationalPrefix:Z

    .line 800796
    const-string v0, ""

    iput-object v0, p0, LX/4hC;->extractedNationalPrefix:Ljava/lang/String;

    .line 800797
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    .line 800798
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/4hC;->possibleFormats:Ljava/util/List;

    .line 800799
    new-instance v0, LX/3MC;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, LX/3MC;-><init>(I)V

    iput-object v0, p0, LX/4hC;->regexCache:LX/3MC;

    .line 800800
    iput-object p2, p0, LX/4hC;->phoneUtil:LX/3Lz;

    .line 800801
    iput-object p1, p0, LX/4hC;->defaultCountry:Ljava/lang/String;

    .line 800802
    iget-object v0, p0, LX/4hC;->defaultCountry:Ljava/lang/String;

    invoke-direct {p0, v0}, LX/4hC;->getMetadataForRegion(Ljava/lang/String;)LX/4hL;

    move-result-object v0

    iput-object v0, p0, LX/4hC;->currentMetadata:LX/4hL;

    .line 800803
    iget-object v0, p0, LX/4hC;->currentMetadata:LX/4hL;

    iput-object v0, p0, LX/4hC;->defaultMetadata:LX/4hL;

    .line 800804
    return-void
.end method

.method private appendNationalNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x20

    .line 800777
    iget-object v0, p0, LX/4hC;->prefixBeforeNationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 800778
    iget-boolean v1, p0, LX/4hC;->shouldAddSpaceAfterNationalPrefix:Z

    if-eqz v1, :cond_0

    if-lez v0, :cond_0

    iget-object v1, p0, LX/4hC;->prefixBeforeNationalNumber:Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    if-eq v0, v3, :cond_0

    .line 800779
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, LX/4hC;->prefixBeforeNationalNumber:Ljava/lang/StringBuilder;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/StringBuilder;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 800780
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/4hC;->prefixBeforeNationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private attemptToChooseFormattingPattern()Ljava/lang/String;
    .locals 8

    .prologue
    .line 800749
    iget-object v0, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_7

    .line 800750
    iget-object v0, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 800751
    iget-boolean v1, p0, LX/4hC;->isCompleteNumber:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/4hC;->currentMetadata:LX/4hL;

    invoke-virtual {v1}, LX/4hL;->intlNumberFormatSize()I

    move-result v1

    if-lez v1, :cond_3

    iget-object v1, p0, LX/4hC;->currentMetadata:LX/4hL;

    .line 800752
    iget-object v2, v1, LX/4hL;->intlNumberFormat_:Ljava/util/List;

    move-object v1, v2

    .line 800753
    :goto_0
    iget-object v2, p0, LX/4hC;->currentMetadata:LX/4hL;

    .line 800754
    iget-boolean v3, v2, LX/4hL;->hasNationalPrefix:Z

    move v2, v3

    .line 800755
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4hJ;

    .line 800756
    if-eqz v2, :cond_2

    iget-boolean v4, p0, LX/4hC;->isCompleteNumber:Z

    if-nez v4, :cond_2

    .line 800757
    iget-boolean v4, v1, LX/4hJ;->nationalPrefixOptionalWhenFormatting_:Z

    move v4, v4

    .line 800758
    if-nez v4, :cond_2

    .line 800759
    iget-object v4, v1, LX/4hJ;->nationalPrefixFormattingRule_:Ljava/lang/String;

    move-object v4, v4

    .line 800760
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    .line 800761
    const-string v5, "getFirstGroupOnlyPrefixPattern()"

    sget-object v6, LX/3Lz;->LAZY_FIRST_GROUP_ONLY_PREFIX_PATTERN:Ljava/util/regex/Pattern;

    const-string v7, "\\(?\\$1\\)?"

    invoke-static {v5, v6, v7}, LX/3Lz;->maybeCompilePattern(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    sput-object v5, LX/3Lz;->LAZY_FIRST_GROUP_ONLY_PREFIX_PATTERN:Ljava/util/regex/Pattern;

    move-object v5, v5

    .line 800762
    invoke-virtual {v5, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/regex/Matcher;->matches()Z

    move-result v5

    if-eqz v5, :cond_8

    :cond_1
    const/4 v5, 0x1

    :goto_2
    move v4, v5

    .line 800763
    if-eqz v4, :cond_0

    .line 800764
    :cond_2
    iget-object v4, v1, LX/4hJ;->format_:Ljava/lang/String;

    move-object v4, v4

    .line 800765
    sget-object v5, LX/4hC;->ELIGIBLE_FORMAT_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v5, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/regex/Matcher;->matches()Z

    move-result v5

    move v4, v5

    .line 800766
    if-eqz v4, :cond_0

    .line 800767
    iget-object v4, p0, LX/4hC;->possibleFormats:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 800768
    :cond_3
    iget-object v1, p0, LX/4hC;->currentMetadata:LX/4hL;

    .line 800769
    iget-object v2, v1, LX/4hL;->numberFormat_:Ljava/util/List;

    move-object v1, v2

    .line 800770
    goto :goto_0

    .line 800771
    :cond_4
    invoke-static {p0, v0}, LX/4hC;->narrowDownPossibleFormats(LX/4hC;Ljava/lang/String;)V

    .line 800772
    invoke-virtual {p0}, LX/4hC;->attemptToFormatAccruedDigits()Ljava/lang/String;

    move-result-object v0

    .line 800773
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_5

    .line 800774
    :goto_3
    return-object v0

    .line 800775
    :cond_5
    invoke-direct {p0}, LX/4hC;->maybeCreateNewTemplate()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0}, LX/4hC;->inputAccruedNationalNumber()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_6
    iget-object v0, p0, LX/4hC;->accruedInput:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 800776
    :cond_7
    iget-object v0, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/4hC;->appendNationalNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_8
    const/4 v5, 0x0

    goto :goto_2
.end method

.method private attemptToChoosePatternWithPrefixExtracted()Ljava/lang/String;
    .locals 1

    .prologue
    .line 800745
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hC;->ableToFormat:Z

    .line 800746
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4hC;->isExpectingCountryCallingCode:Z

    .line 800747
    iget-object v0, p0, LX/4hC;->possibleFormats:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 800748
    invoke-direct {p0}, LX/4hC;->attemptToChooseFormattingPattern()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private attemptToExtractCountryCallingCode()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 800729
    iget-object v1, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 800730
    :cond_0
    :goto_0
    return v0

    .line 800731
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 800732
    iget-object v2, p0, LX/4hC;->phoneUtil:LX/3Lz;

    iget-object v3, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3, v1}, LX/3Lz;->extractCountryCode(Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;)I

    move-result v2

    .line 800733
    if-eqz v2, :cond_0

    .line 800734
    iget-object v3, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 800735
    iget-object v0, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 800736
    iget-object v0, p0, LX/4hC;->phoneUtil:LX/3Lz;

    invoke-virtual {v0, v2}, LX/3Lz;->getRegionCodeForCountryCode(I)Ljava/lang/String;

    move-result-object v0

    .line 800737
    const-string v1, "001"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 800738
    iget-object v0, p0, LX/4hC;->phoneUtil:LX/3Lz;

    invoke-virtual {v0, v2}, LX/3Lz;->getMetadataForNonGeographicalRegion(I)LX/4hL;

    move-result-object v0

    iput-object v0, p0, LX/4hC;->currentMetadata:LX/4hL;

    .line 800739
    :cond_2
    :goto_1
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 800740
    iget-object v1, p0, LX/4hC;->prefixBeforeNationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 800741
    const-string v0, ""

    iput-object v0, p0, LX/4hC;->extractedNationalPrefix:Ljava/lang/String;

    .line 800742
    const/4 v0, 0x1

    goto :goto_0

    .line 800743
    :cond_3
    iget-object v1, p0, LX/4hC;->defaultCountry:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 800744
    invoke-direct {p0, v0}, LX/4hC;->getMetadataForRegion(Ljava/lang/String;)LX/4hL;

    move-result-object v0

    iput-object v0, p0, LX/4hC;->currentMetadata:LX/4hL;

    goto :goto_1
.end method

.method private attemptToExtractIdd()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 800715
    iget-object v2, p0, LX/4hC;->regexCache:LX/3MC;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\\+|"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/4hC;->currentMetadata:LX/4hL;

    .line 800716
    iget-object v5, v4, LX/4hL;->internationalPrefix_:Ljava/lang/String;

    move-object v4, v5

    .line 800717
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/3MC;->getPatternForRegex(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 800718
    iget-object v3, p0, LX/4hC;->accruedInputWithoutFormatting:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 800719
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 800720
    iput-boolean v0, p0, LX/4hC;->isCompleteNumber:Z

    .line 800721
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->end()I

    move-result v2

    .line 800722
    iget-object v3, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 800723
    iget-object v3, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    iget-object v4, p0, LX/4hC;->accruedInputWithoutFormatting:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 800724
    iget-object v3, p0, LX/4hC;->prefixBeforeNationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 800725
    iget-object v3, p0, LX/4hC;->prefixBeforeNationalNumber:Ljava/lang/StringBuilder;

    iget-object v4, p0, LX/4hC;->accruedInputWithoutFormatting:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 800726
    iget-object v2, p0, LX/4hC;->accruedInputWithoutFormatting:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x2b

    if-eq v1, v2, :cond_0

    .line 800727
    iget-object v1, p0, LX/4hC;->prefixBeforeNationalNumber:Ljava/lang/StringBuilder;

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 800728
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private createFormattingTemplate(LX/4hJ;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 800696
    iget-object v1, p1, LX/4hJ;->pattern_:Ljava/lang/String;

    move-object v1, v1

    .line 800697
    const/16 v2, 0x7c

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 800698
    :cond_0
    :goto_0
    return v0

    .line 800699
    :cond_1
    sget-object v2, LX/4hC;->CHARACTER_CLASS_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    const-string v2, "\\\\d"

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 800700
    sget-object v2, LX/4hC;->STANDALONE_DIGIT_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    const-string v2, "\\\\d"

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 800701
    iget-object v2, p0, LX/4hC;->formattingTemplate:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 800702
    iget-object v2, p1, LX/4hJ;->format_:Ljava/lang/String;

    move-object v2, v2

    .line 800703
    const-string v3, "999999999999999"

    .line 800704
    iget-object v4, p0, LX/4hC;->regexCache:LX/3MC;

    invoke-virtual {v4, v1}, LX/3MC;->getPatternForRegex(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 800705
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    .line 800706
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v3

    .line 800707
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    iget-object p1, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result p1

    if-ge v4, p1, :cond_2

    .line 800708
    const-string v3, ""

    .line 800709
    :goto_1
    move-object v1, v3

    .line 800710
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 800711
    iget-object v0, p0, LX/4hC;->formattingTemplate:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 800712
    const/4 v0, 0x1

    goto :goto_0

    .line 800713
    :cond_2
    invoke-virtual {v3, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 800714
    const-string v4, "9"

    const-string p1, "\u2008"

    invoke-virtual {v3, v4, p1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method private getMetadataForRegion(Ljava/lang/String;)LX/4hL;
    .locals 2

    .prologue
    .line 800691
    iget-object v0, p0, LX/4hC;->phoneUtil:LX/3Lz;

    invoke-virtual {v0, p1}, LX/3Lz;->getCountryCodeForRegion(Ljava/lang/String;)I

    move-result v0

    .line 800692
    iget-object v1, p0, LX/4hC;->phoneUtil:LX/3Lz;

    invoke-virtual {v1, v0}, LX/3Lz;->getRegionCodeForCountryCode(I)Ljava/lang/String;

    move-result-object v0

    .line 800693
    iget-object v1, p0, LX/4hC;->phoneUtil:LX/3Lz;

    invoke-virtual {v1, v0}, LX/3Lz;->getMetadataForRegion(Ljava/lang/String;)LX/4hL;

    move-result-object v0

    .line 800694
    if-eqz v0, :cond_0

    .line 800695
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/4hC;->EMPTY_METADATA:LX/4hL;

    goto :goto_0
.end method

.method private inputAccruedNationalNumber()Ljava/lang/String;
    .locals 3

    .prologue
    .line 800681
    iget-object v0, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    .line 800682
    if-lez v2, :cond_2

    .line 800683
    const-string v1, ""

    .line 800684
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 800685
    iget-object v1, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    invoke-direct {p0, v1}, LX/4hC;->inputDigitHelper(C)Ljava/lang/String;

    move-result-object v1

    .line 800686
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 800687
    :cond_0
    iget-boolean v0, p0, LX/4hC;->ableToFormat:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, LX/4hC;->appendNationalNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 800688
    :goto_1
    return-object v0

    .line 800689
    :cond_1
    iget-object v0, p0, LX/4hC;->accruedInput:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 800690
    :cond_2
    iget-object v0, p0, LX/4hC;->prefixBeforeNationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private inputDigitHelper(C)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 800670
    sget-object v0, LX/4hC;->DIGIT_PATTERN:Ljava/util/regex/Pattern;

    iget-object v1, p0, LX/4hC;->formattingTemplate:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 800671
    iget v1, p0, LX/4hC;->lastMatchPosition:I

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->find(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 800672
    invoke-static {p1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 800673
    iget-object v2, p0, LX/4hC;->formattingTemplate:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2, v4, v3, v1}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 800674
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->start()I

    move-result v0

    iput v0, p0, LX/4hC;->lastMatchPosition:I

    .line 800675
    iget-object v0, p0, LX/4hC;->formattingTemplate:Ljava/lang/StringBuilder;

    iget v1, p0, LX/4hC;->lastMatchPosition:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v4, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 800676
    :goto_0
    return-object v0

    .line 800677
    :cond_0
    iget-object v0, p0, LX/4hC;->possibleFormats:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 800678
    iput-boolean v4, p0, LX/4hC;->ableToFormat:Z

    .line 800679
    :cond_1
    const-string v0, ""

    iput-object v0, p0, LX/4hC;->currentFormattingPattern:Ljava/lang/String;

    .line 800680
    iget-object v0, p0, LX/4hC;->accruedInput:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static inputDigitWithOptionToRememberPosition(LX/4hC;CZ)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 800616
    iget-object v0, p0, LX/4hC;->accruedInput:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 800617
    if-eqz p2, :cond_0

    .line 800618
    iget-object v0, p0, LX/4hC;->accruedInput:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    iput v0, p0, LX/4hC;->originalPosition:I

    .line 800619
    :cond_0
    const/4 v0, 0x1

    .line 800620
    invoke-static {p1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, LX/4hC;->accruedInput:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-ne v3, v0, :cond_11

    invoke-static {}, LX/3Lz;->getPlusCharsPattern()Ljava/util/regex/Pattern;

    move-result-object v3

    invoke-static {p1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_11

    :cond_1
    :goto_0
    move v0, v0

    .line 800621
    if-nez v0, :cond_3

    .line 800622
    iput-boolean v1, p0, LX/4hC;->ableToFormat:Z

    .line 800623
    iput-boolean v2, p0, LX/4hC;->inputHasFormatting:Z

    .line 800624
    :goto_1
    iget-boolean v0, p0, LX/4hC;->ableToFormat:Z

    if-nez v0, :cond_a

    .line 800625
    iget-boolean v0, p0, LX/4hC;->inputHasFormatting:Z

    if-eqz v0, :cond_5

    .line 800626
    iget-object v0, p0, LX/4hC;->accruedInput:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 800627
    :cond_2
    :goto_2
    return-object v0

    .line 800628
    :cond_3
    const/16 v3, 0xa

    .line 800629
    const/16 v0, 0x2b

    if-ne p1, v0, :cond_12

    .line 800630
    iget-object v0, p0, LX/4hC;->accruedInputWithoutFormatting:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 800631
    :goto_3
    if-eqz p2, :cond_4

    .line 800632
    iget-object v0, p0, LX/4hC;->accruedInputWithoutFormatting:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    iput v0, p0, LX/4hC;->positionToRemember:I

    .line 800633
    :cond_4
    move p1, p1

    .line 800634
    goto :goto_1

    .line 800635
    :cond_5
    invoke-direct {p0}, LX/4hC;->attemptToExtractIdd()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 800636
    invoke-direct {p0}, LX/4hC;->attemptToExtractCountryCallingCode()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 800637
    invoke-direct {p0}, LX/4hC;->attemptToChoosePatternWithPrefixExtracted()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 800638
    :cond_6
    const/4 v0, 0x0

    .line 800639
    iget-object v1, p0, LX/4hC;->extractedNationalPrefix:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_7

    .line 800640
    iget-object v1, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    iget-object v2, p0, LX/4hC;->extractedNationalPrefix:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 800641
    iget-object v1, p0, LX/4hC;->prefixBeforeNationalNumber:Ljava/lang/StringBuilder;

    iget-object v2, p0, LX/4hC;->extractedNationalPrefix:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 800642
    iget-object v2, p0, LX/4hC;->prefixBeforeNationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 800643
    :cond_7
    iget-object v1, p0, LX/4hC;->extractedNationalPrefix:Ljava/lang/String;

    invoke-static {p0}, LX/4hC;->removeNationalPrefixFromNationalNumber(LX/4hC;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    const/4 v0, 0x1

    :cond_8
    move v0, v0

    .line 800644
    if-eqz v0, :cond_9

    .line 800645
    iget-object v0, p0, LX/4hC;->prefixBeforeNationalNumber:Ljava/lang/StringBuilder;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 800646
    invoke-direct {p0}, LX/4hC;->attemptToChoosePatternWithPrefixExtracted()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 800647
    :cond_9
    iget-object v0, p0, LX/4hC;->accruedInput:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 800648
    :cond_a
    iget-object v0, p0, LX/4hC;->accruedInputWithoutFormatting:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 800649
    :goto_4
    iget-boolean v0, p0, LX/4hC;->isExpectingCountryCallingCode:Z

    if-eqz v0, :cond_d

    .line 800650
    invoke-direct {p0}, LX/4hC;->attemptToExtractCountryCallingCode()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 800651
    iput-boolean v1, p0, LX/4hC;->isExpectingCountryCallingCode:Z

    .line 800652
    :cond_b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/4hC;->prefixBeforeNationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 800653
    :pswitch_0
    iget-object v0, p0, LX/4hC;->accruedInput:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 800654
    :pswitch_1
    invoke-direct {p0}, LX/4hC;->attemptToExtractIdd()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 800655
    iput-boolean v2, p0, LX/4hC;->isExpectingCountryCallingCode:Z

    goto :goto_4

    .line 800656
    :cond_c
    invoke-static {p0}, LX/4hC;->removeNationalPrefixFromNationalNumber(LX/4hC;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/4hC;->extractedNationalPrefix:Ljava/lang/String;

    .line 800657
    invoke-direct {p0}, LX/4hC;->attemptToChooseFormattingPattern()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 800658
    :cond_d
    iget-object v0, p0, LX/4hC;->possibleFormats:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_10

    .line 800659
    invoke-direct {p0, p1}, LX/4hC;->inputDigitHelper(C)Ljava/lang/String;

    move-result-object v1

    .line 800660
    invoke-virtual {p0}, LX/4hC;->attemptToFormatAccruedDigits()Ljava/lang/String;

    move-result-object v0

    .line 800661
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_2

    .line 800662
    iget-object v0, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/4hC;->narrowDownPossibleFormats(LX/4hC;Ljava/lang/String;)V

    .line 800663
    invoke-direct {p0}, LX/4hC;->maybeCreateNewTemplate()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 800664
    invoke-direct {p0}, LX/4hC;->inputAccruedNationalNumber()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 800665
    :cond_e
    iget-boolean v0, p0, LX/4hC;->ableToFormat:Z

    if-eqz v0, :cond_f

    invoke-direct {p0, v1}, LX/4hC;->appendNationalNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_f
    iget-object v0, p0, LX/4hC;->accruedInput:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 800666
    :cond_10
    invoke-direct {p0}, LX/4hC;->attemptToChooseFormattingPattern()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_11
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 800667
    :cond_12
    invoke-static {p1, v3}, Ljava/lang/Character;->digit(CI)I

    move-result v0

    invoke-static {v0, v3}, Ljava/lang/Character;->forDigit(II)C

    move-result p1

    .line 800668
    iget-object v0, p0, LX/4hC;->accruedInputWithoutFormatting:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 800669
    iget-object v0, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private maybeCreateNewTemplate()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 800600
    iget-object v0, p0, LX/4hC;->possibleFormats:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 800601
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 800602
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4hJ;

    .line 800603
    iget-object v3, v0, LX/4hJ;->pattern_:Ljava/lang/String;

    move-object v3, v3

    .line 800604
    iget-object v4, p0, LX/4hC;->currentFormattingPattern:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v1

    .line 800605
    :goto_1
    return v0

    .line 800606
    :cond_0
    invoke-direct {p0, v0}, LX/4hC;->createFormattingTemplate(LX/4hJ;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 800607
    iput-object v3, p0, LX/4hC;->currentFormattingPattern:Ljava/lang/String;

    .line 800608
    sget-object v2, LX/4hC;->NATIONAL_PREFIX_SEPARATORS_PATTERN:Ljava/util/regex/Pattern;

    .line 800609
    iget-object v3, v0, LX/4hJ;->nationalPrefixFormattingRule_:Ljava/lang/String;

    move-object v0, v3

    .line 800610
    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    iput-boolean v0, p0, LX/4hC;->shouldAddSpaceAfterNationalPrefix:Z

    .line 800611
    iput v1, p0, LX/4hC;->lastMatchPosition:I

    .line 800612
    const/4 v0, 0x1

    goto :goto_1

    .line 800613
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 800614
    :cond_2
    iput-boolean v1, p0, LX/4hC;->ableToFormat:Z

    move v0, v1

    .line 800615
    goto :goto_1
.end method

.method public static narrowDownPossibleFormats(LX/4hC;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 800589
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v1, v0, -0x3

    .line 800590
    iget-object v0, p0, LX/4hC;->possibleFormats:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 800591
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 800592
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4hJ;

    .line 800593
    invoke-virtual {v0}, LX/4hJ;->leadingDigitsPatternSize()I

    move-result v3

    if-eqz v3, :cond_0

    .line 800594
    invoke-virtual {v0}, LX/4hJ;->leadingDigitsPatternSize()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 800595
    iget-object v4, p0, LX/4hC;->regexCache:LX/3MC;

    invoke-virtual {v0, v3}, LX/4hJ;->getLeadingDigitsPattern(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/3MC;->getPatternForRegex(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 800596
    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 800597
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v0

    if-nez v0, :cond_0

    .line 800598
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 800599
    :cond_1
    return-void
.end method

.method public static removeNationalPrefixFromNationalNumber(LX/4hC;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 800568
    const/16 v6, 0x31

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 800569
    iget-object v4, p0, LX/4hC;->currentMetadata:LX/4hL;

    .line 800570
    iget v5, v4, LX/4hL;->countryCode_:I

    move v4, v5

    .line 800571
    if-ne v4, v2, :cond_2

    iget-object v4, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    if-ne v4, v6, :cond_2

    iget-object v4, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    const/16 v5, 0x30

    if-eq v4, v5, :cond_2

    iget-object v4, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    if-eq v4, v6, :cond_2

    :goto_0
    move v2, v2

    .line 800572
    if-eqz v2, :cond_0

    .line 800573
    iget-object v2, p0, LX/4hC;->prefixBeforeNationalNumber:Ljava/lang/StringBuilder;

    const/16 v3, 0x31

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 800574
    iput-boolean v0, p0, LX/4hC;->isCompleteNumber:Z

    .line 800575
    :goto_1
    iget-object v2, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1, v0}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 800576
    iget-object v3, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1, v0}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 800577
    return-object v2

    .line 800578
    :cond_0
    iget-object v2, p0, LX/4hC;->currentMetadata:LX/4hL;

    .line 800579
    iget-boolean v3, v2, LX/4hL;->hasNationalPrefixForParsing:Z

    move v2, v3

    .line 800580
    if-eqz v2, :cond_1

    .line 800581
    iget-object v2, p0, LX/4hC;->regexCache:LX/3MC;

    iget-object v3, p0, LX/4hC;->currentMetadata:LX/4hL;

    .line 800582
    iget-object v4, v3, LX/4hL;->nationalPrefixForParsing_:Ljava/lang/String;

    move-object v3, v4

    .line 800583
    invoke-virtual {v2, v3}, LX/3MC;->getPatternForRegex(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 800584
    iget-object v3, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 800585
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->end()I

    move-result v3

    if-lez v3, :cond_1

    .line 800586
    iput-boolean v0, p0, LX/4hC;->isCompleteNumber:Z

    .line 800587
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->end()I

    move-result v0

    .line 800588
    iget-object v2, p0, LX/4hC;->prefixBeforeNationalNumber:Ljava/lang/StringBuilder;

    iget-object v3, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1, v0}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_0
.end method


# virtual methods
.method public attemptToFormatAccruedDigits()Ljava/lang/String;
    .locals 4

    .prologue
    .line 800556
    iget-object v0, p0, LX/4hC;->possibleFormats:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4hJ;

    .line 800557
    iget-object v2, p0, LX/4hC;->regexCache:LX/3MC;

    .line 800558
    iget-object v3, v0, LX/4hJ;->pattern_:Ljava/lang/String;

    move-object v3, v3

    .line 800559
    invoke-virtual {v2, v3}, LX/3MC;->getPatternForRegex(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    iget-object v3, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 800560
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 800561
    sget-object v1, LX/4hC;->NATIONAL_PREFIX_SEPARATORS_PATTERN:Ljava/util/regex/Pattern;

    .line 800562
    iget-object v3, v0, LX/4hJ;->nationalPrefixFormattingRule_:Ljava/lang/String;

    move-object v3, v3

    .line 800563
    invoke-virtual {v1, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    iput-boolean v1, p0, LX/4hC;->shouldAddSpaceAfterNationalPrefix:Z

    .line 800564
    iget-object v1, v0, LX/4hJ;->format_:Ljava/lang/String;

    move-object v0, v1

    .line 800565
    invoke-virtual {v2, v0}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 800566
    invoke-direct {p0, v0}, LX/4hC;->appendNationalNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 800567
    :goto_0
    return-object v0

    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 800536
    const-string v0, ""

    iput-object v0, p0, LX/4hC;->currentOutput:Ljava/lang/String;

    .line 800537
    iget-object v0, p0, LX/4hC;->accruedInput:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 800538
    iget-object v0, p0, LX/4hC;->accruedInputWithoutFormatting:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 800539
    iget-object v0, p0, LX/4hC;->formattingTemplate:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 800540
    iput v1, p0, LX/4hC;->lastMatchPosition:I

    .line 800541
    const-string v0, ""

    iput-object v0, p0, LX/4hC;->currentFormattingPattern:Ljava/lang/String;

    .line 800542
    iget-object v0, p0, LX/4hC;->prefixBeforeNationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 800543
    const-string v0, ""

    iput-object v0, p0, LX/4hC;->extractedNationalPrefix:Ljava/lang/String;

    .line 800544
    iget-object v0, p0, LX/4hC;->nationalNumber:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 800545
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hC;->ableToFormat:Z

    .line 800546
    iput-boolean v1, p0, LX/4hC;->inputHasFormatting:Z

    .line 800547
    iput v1, p0, LX/4hC;->positionToRemember:I

    .line 800548
    iput v1, p0, LX/4hC;->originalPosition:I

    .line 800549
    iput-boolean v1, p0, LX/4hC;->isCompleteNumber:Z

    .line 800550
    iput-boolean v1, p0, LX/4hC;->isExpectingCountryCallingCode:Z

    .line 800551
    iget-object v0, p0, LX/4hC;->possibleFormats:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 800552
    iput-boolean v1, p0, LX/4hC;->shouldAddSpaceAfterNationalPrefix:Z

    .line 800553
    iget-object v0, p0, LX/4hC;->currentMetadata:LX/4hL;

    iget-object v1, p0, LX/4hC;->defaultMetadata:LX/4hL;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 800554
    iget-object v0, p0, LX/4hC;->defaultCountry:Ljava/lang/String;

    invoke-direct {p0, v0}, LX/4hC;->getMetadataForRegion(Ljava/lang/String;)LX/4hL;

    move-result-object v0

    iput-object v0, p0, LX/4hC;->currentMetadata:LX/4hL;

    .line 800555
    :cond_0
    return-void
.end method
