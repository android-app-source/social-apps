.class public LX/3oZ;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/3oZ;


# instance fields
.field private final b:Ljava/lang/Object;

.field private final c:Landroid/os/Handler;

.field private d:LX/3oY;

.field private e:LX/3oY;


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    .line 640393
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 640394
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/3oZ;->b:Ljava/lang/Object;

    .line 640395
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, LX/3oX;

    invoke-direct {v2, p0}, LX/3oX;-><init>(LX/3oZ;)V

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, LX/3oZ;->c:Landroid/os/Handler;

    .line 640396
    return-void
.end method

.method public static a()LX/3oZ;
    .locals 1

    .prologue
    .line 640390
    sget-object v0, LX/3oZ;->a:LX/3oZ;

    if-nez v0, :cond_0

    .line 640391
    new-instance v0, LX/3oZ;

    invoke-direct {v0}, LX/3oZ;-><init>()V

    sput-object v0, LX/3oZ;->a:LX/3oZ;

    .line 640392
    :cond_0
    sget-object v0, LX/3oZ;->a:LX/3oZ;

    return-object v0
.end method

.method private static a(LX/3oY;I)Z
    .locals 1

    .prologue
    .line 640385
    iget-object v0, p0, LX/3oY;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3oI;

    .line 640386
    if-eqz v0, :cond_0

    .line 640387
    invoke-interface {v0, p1}, LX/3oI;->a(I)V

    .line 640388
    const/4 v0, 0x1

    .line 640389
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a$redex0(LX/3oZ;LX/3oY;)V
    .locals 6

    .prologue
    .line 640376
    iget v0, p1, LX/3oY;->b:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_0

    .line 640377
    :goto_0
    return-void

    .line 640378
    :cond_0
    const/16 v0, 0xabe

    .line 640379
    iget v1, p1, LX/3oY;->b:I

    if-lez v1, :cond_2

    .line 640380
    iget v0, p1, LX/3oY;->b:I

    .line 640381
    :cond_1
    :goto_1
    iget-object v1, p0, LX/3oZ;->c:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 640382
    iget-object v1, p0, LX/3oZ;->c:Landroid/os/Handler;

    iget-object v2, p0, LX/3oZ;->c:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-static {v2, v3, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 640383
    :cond_2
    iget v1, p1, LX/3oY;->b:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 640384
    const/16 v0, 0x5dc

    goto :goto_1
.end method

.method private static b(LX/3oZ;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 640368
    iget-object v0, p0, LX/3oZ;->e:LX/3oY;

    if-eqz v0, :cond_0

    .line 640369
    iget-object v0, p0, LX/3oZ;->e:LX/3oY;

    iput-object v0, p0, LX/3oZ;->d:LX/3oY;

    .line 640370
    iput-object v1, p0, LX/3oZ;->e:LX/3oY;

    .line 640371
    iget-object v0, p0, LX/3oZ;->d:LX/3oY;

    iget-object v0, v0, LX/3oY;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3oI;

    .line 640372
    if-eqz v0, :cond_1

    .line 640373
    invoke-interface {v0}, LX/3oI;->a()V

    .line 640374
    :cond_0
    :goto_0
    return-void

    .line 640375
    :cond_1
    iput-object v1, p0, LX/3oZ;->d:LX/3oY;

    goto :goto_0
.end method

.method public static b(LX/3oZ;LX/3oY;)V
    .locals 2

    .prologue
    .line 640364
    iget-object v1, p0, LX/3oZ;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 640365
    :try_start_0
    iget-object v0, p0, LX/3oZ;->d:LX/3oY;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, LX/3oZ;->e:LX/3oY;

    if-ne v0, p1, :cond_1

    .line 640366
    :cond_0
    const/4 v0, 0x2

    invoke-static {p1, v0}, LX/3oZ;->a(LX/3oY;I)Z

    .line 640367
    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static g(LX/3oZ;LX/3oI;)Z
    .locals 1

    .prologue
    .line 640363
    iget-object v0, p0, LX/3oZ;->d:LX/3oY;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3oZ;->d:LX/3oY;

    invoke-virtual {v0, p1}, LX/3oY;->a(LX/3oI;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static h(LX/3oZ;LX/3oI;)Z
    .locals 1

    .prologue
    .line 640397
    iget-object v0, p0, LX/3oZ;->e:LX/3oY;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3oZ;->e:LX/3oY;

    invoke-virtual {v0, p1}, LX/3oY;->a(LX/3oI;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILX/3oI;)V
    .locals 3

    .prologue
    .line 640345
    iget-object v1, p0, LX/3oZ;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 640346
    :try_start_0
    invoke-static {p0, p2}, LX/3oZ;->g(LX/3oZ;LX/3oI;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 640347
    iget-object v0, p0, LX/3oZ;->d:LX/3oY;

    .line 640348
    iput p1, v0, LX/3oY;->b:I

    .line 640349
    iget-object v0, p0, LX/3oZ;->c:Landroid/os/Handler;

    iget-object v2, p0, LX/3oZ;->d:LX/3oY;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 640350
    iget-object v0, p0, LX/3oZ;->d:LX/3oY;

    invoke-static {p0, v0}, LX/3oZ;->a$redex0(LX/3oZ;LX/3oY;)V

    .line 640351
    monitor-exit v1

    .line 640352
    :goto_0
    return-void

    .line 640353
    :cond_0
    invoke-static {p0, p2}, LX/3oZ;->h(LX/3oZ;LX/3oI;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 640354
    iget-object v0, p0, LX/3oZ;->e:LX/3oY;

    .line 640355
    iput p1, v0, LX/3oY;->b:I

    .line 640356
    :goto_1
    iget-object v0, p0, LX/3oZ;->d:LX/3oY;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3oZ;->d:LX/3oY;

    const/4 v2, 0x4

    invoke-static {v0, v2}, LX/3oZ;->a(LX/3oY;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 640357
    monitor-exit v1

    goto :goto_0

    .line 640358
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 640359
    :cond_1
    :try_start_1
    new-instance v0, LX/3oY;

    invoke-direct {v0, p1, p2}, LX/3oY;-><init>(ILX/3oI;)V

    iput-object v0, p0, LX/3oZ;->e:LX/3oY;

    goto :goto_1

    .line 640360
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, LX/3oZ;->d:LX/3oY;

    .line 640361
    invoke-static {p0}, LX/3oZ;->b(LX/3oZ;)V

    .line 640362
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final a(LX/3oI;)V
    .locals 2

    .prologue
    .line 640339
    iget-object v1, p0, LX/3oZ;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 640340
    :try_start_0
    invoke-static {p0, p1}, LX/3oZ;->g(LX/3oZ;LX/3oI;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 640341
    const/4 v0, 0x0

    iput-object v0, p0, LX/3oZ;->d:LX/3oY;

    .line 640342
    iget-object v0, p0, LX/3oZ;->e:LX/3oY;

    if-eqz v0, :cond_0

    .line 640343
    invoke-static {p0}, LX/3oZ;->b(LX/3oZ;)V

    .line 640344
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/3oI;I)V
    .locals 2

    .prologue
    .line 640332
    iget-object v1, p0, LX/3oZ;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 640333
    :try_start_0
    invoke-static {p0, p1}, LX/3oZ;->g(LX/3oZ;LX/3oI;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 640334
    iget-object v0, p0, LX/3oZ;->d:LX/3oY;

    invoke-static {v0, p2}, LX/3oZ;->a(LX/3oY;I)Z

    .line 640335
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 640336
    :cond_1
    invoke-static {p0, p1}, LX/3oZ;->h(LX/3oZ;LX/3oI;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 640337
    iget-object v0, p0, LX/3oZ;->e:LX/3oY;

    invoke-static {v0, p2}, LX/3oZ;->a(LX/3oY;I)Z

    goto :goto_0

    .line 640338
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(LX/3oI;)V
    .locals 2

    .prologue
    .line 640328
    iget-object v1, p0, LX/3oZ;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 640329
    :try_start_0
    invoke-static {p0, p1}, LX/3oZ;->g(LX/3oZ;LX/3oI;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 640330
    iget-object v0, p0, LX/3oZ;->d:LX/3oY;

    invoke-static {p0, v0}, LX/3oZ;->a$redex0(LX/3oZ;LX/3oY;)V

    .line 640331
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c(LX/3oI;)V
    .locals 3

    .prologue
    .line 640324
    iget-object v1, p0, LX/3oZ;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 640325
    :try_start_0
    invoke-static {p0, p1}, LX/3oZ;->g(LX/3oZ;LX/3oI;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 640326
    iget-object v0, p0, LX/3oZ;->c:Landroid/os/Handler;

    iget-object v2, p0, LX/3oZ;->d:LX/3oY;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 640327
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final d(LX/3oI;)V
    .locals 2

    .prologue
    .line 640314
    iget-object v1, p0, LX/3oZ;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 640315
    :try_start_0
    invoke-static {p0, p1}, LX/3oZ;->g(LX/3oZ;LX/3oI;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 640316
    iget-object v0, p0, LX/3oZ;->d:LX/3oY;

    invoke-static {p0, v0}, LX/3oZ;->a$redex0(LX/3oZ;LX/3oY;)V

    .line 640317
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final e(LX/3oI;)Z
    .locals 2

    .prologue
    .line 640321
    iget-object v1, p0, LX/3oZ;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 640322
    :try_start_0
    invoke-static {p0, p1}, LX/3oZ;->g(LX/3oZ;LX/3oI;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 640323
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final f(LX/3oI;)Z
    .locals 2

    .prologue
    .line 640318
    iget-object v1, p0, LX/3oZ;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 640319
    :try_start_0
    invoke-static {p0, p1}, LX/3oZ;->g(LX/3oZ;LX/3oI;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0, p1}, LX/3oZ;->h(LX/3oZ;LX/3oI;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 640320
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
