.class public final LX/41c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/auth/protocol/LoginApprovalMutationModels$LoginApprovalMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/auth/login/LoginApprovalNotificationService;


# direct methods
.method public constructor <init>(Lcom/facebook/auth/login/LoginApprovalNotificationService;)V
    .locals 0

    .prologue
    .line 666491
    iput-object p1, p0, LX/41c;->a:Lcom/facebook/auth/login/LoginApprovalNotificationService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 666486
    iget-object v0, p0, LX/41c;->a:Lcom/facebook/auth/login/LoginApprovalNotificationService;

    iget-object v0, v0, Lcom/facebook/auth/login/LoginApprovalNotificationService;->d:LX/4gS;

    .line 666487
    iget-object v1, v0, LX/4gS;->a:LX/0if;

    iget-object v2, v0, LX/4gS;->b:LX/0ih;

    const-string v3, "APPROVE_FROM_ACTION_SUCCESS"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 666488
    invoke-static {v0}, LX/4gS;->e(LX/4gS;)V

    .line 666489
    iget-object v0, p0, LX/41c;->a:Lcom/facebook/auth/login/LoginApprovalNotificationService;

    invoke-virtual {v0}, Lcom/facebook/auth/login/LoginApprovalNotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/41c;->a:Lcom/facebook/auth/login/LoginApprovalNotificationService;

    invoke-virtual {v1}, Lcom/facebook/auth/login/LoginApprovalNotificationService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080174

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 666490
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 666481
    iget-object v0, p0, LX/41c;->a:Lcom/facebook/auth/login/LoginApprovalNotificationService;

    iget-object v0, v0, Lcom/facebook/auth/login/LoginApprovalNotificationService;->d:LX/4gS;

    .line 666482
    iget-object v1, v0, LX/4gS;->a:LX/0if;

    iget-object p0, v0, LX/4gS;->b:LX/0ih;

    const-string p1, "APPROVE_FROM_ACTION_FAILURE"

    invoke-virtual {v1, p0, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 666483
    invoke-static {v0}, LX/4gS;->e(LX/4gS;)V

    .line 666484
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 666485
    invoke-direct {p0}, LX/41c;->a()V

    return-void
.end method
