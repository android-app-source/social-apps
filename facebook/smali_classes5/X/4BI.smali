.class public final LX/4BI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public mBundle:Landroid/os/Bundle;

.field public mCallback:LX/1qH;

.field public mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

.field public mId:Ljava/lang/String;

.field public mRequestState:LX/0zW;

.field public mType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 677649
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()LX/1qK;
    .locals 1

    .prologue
    .line 677650
    new-instance v0, LX/1qK;

    invoke-direct {v0, p0}, LX/1qK;-><init>(LX/4BI;)V

    return-object v0
.end method

.method public from(LX/1qK;)LX/4BI;
    .locals 1

    .prologue
    .line 677651
    iget-object v0, p1, LX/1qK;->mId:Ljava/lang/String;

    move-object v0, v0

    .line 677652
    iput-object v0, p0, LX/4BI;->mId:Ljava/lang/String;

    .line 677653
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 677654
    iput-object v0, p0, LX/4BI;->mType:Ljava/lang/String;

    .line 677655
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 677656
    iput-object v0, p0, LX/4BI;->mBundle:Landroid/os/Bundle;

    .line 677657
    iget-object v0, p1, LX/1qK;->mRequestState:LX/0zW;

    move-object v0, v0

    .line 677658
    iput-object v0, p0, LX/4BI;->mRequestState:LX/0zW;

    .line 677659
    iget-object v0, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v0, v0

    .line 677660
    iput-object v0, p0, LX/4BI;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    .line 677661
    iget-object v0, p1, LX/1qK;->mCallback:LX/1qH;

    move-object v0, v0

    .line 677662
    iput-object v0, p0, LX/4BI;->mCallback:LX/1qH;

    .line 677663
    return-object p0
.end method

.method public setCallerContext(Lcom/facebook/common/callercontext/CallerContext;)LX/4BI;
    .locals 0

    .prologue
    .line 677664
    iput-object p1, p0, LX/4BI;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    .line 677665
    return-object p0
.end method

.method public setId(Ljava/lang/String;)LX/4BI;
    .locals 0

    .prologue
    .line 677666
    iput-object p1, p0, LX/4BI;->mId:Ljava/lang/String;

    .line 677667
    return-object p0
.end method

.method public setRequestState(LX/0zW;)LX/4BI;
    .locals 0

    .prologue
    .line 677668
    iput-object p1, p0, LX/4BI;->mRequestState:LX/0zW;

    .line 677669
    return-object p0
.end method
