.class public LX/4L3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 681506
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 43

    .prologue
    .line 681507
    const/16 v36, 0x0

    .line 681508
    const/16 v35, 0x0

    .line 681509
    const/16 v34, 0x0

    .line 681510
    const/16 v33, 0x0

    .line 681511
    const/16 v32, 0x0

    .line 681512
    const/16 v31, 0x0

    .line 681513
    const/16 v30, 0x0

    .line 681514
    const/16 v29, 0x0

    .line 681515
    const/16 v28, 0x0

    .line 681516
    const/16 v27, 0x0

    .line 681517
    const/16 v26, 0x0

    .line 681518
    const/16 v25, 0x0

    .line 681519
    const/16 v24, 0x0

    .line 681520
    const-wide/16 v22, 0x0

    .line 681521
    const-wide/16 v20, 0x0

    .line 681522
    const/16 v19, 0x0

    .line 681523
    const/16 v18, 0x0

    .line 681524
    const/16 v17, 0x0

    .line 681525
    const/16 v16, 0x0

    .line 681526
    const/4 v15, 0x0

    .line 681527
    const/4 v14, 0x0

    .line 681528
    const/4 v13, 0x0

    .line 681529
    const/4 v12, 0x0

    .line 681530
    const/4 v11, 0x0

    .line 681531
    const/4 v10, 0x0

    .line 681532
    const/4 v9, 0x0

    .line 681533
    const/4 v8, 0x0

    .line 681534
    const/4 v7, 0x0

    .line 681535
    const/4 v6, 0x0

    .line 681536
    const/4 v5, 0x0

    .line 681537
    const/4 v4, 0x0

    .line 681538
    const/4 v3, 0x0

    .line 681539
    const/4 v2, 0x0

    .line 681540
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v37

    sget-object v38, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v37

    move-object/from16 v1, v38

    if-eq v0, v1, :cond_23

    .line 681541
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 681542
    const/4 v2, 0x0

    .line 681543
    :goto_0
    return v2

    .line 681544
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v38, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v38

    if-eq v2, v0, :cond_17

    .line 681545
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 681546
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 681547
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v38

    sget-object v39, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 681548
    const-string v38, "ad_account"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_1

    .line 681549
    invoke-static/range {p0 .. p1}, LX/4Ke;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v37, v2

    goto :goto_1

    .line 681550
    :cond_1
    const-string v38, "aymt_post_footer_channel"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_2

    .line 681551
    invoke-static/range {p0 .. p1}, LX/4KZ;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v36, v2

    goto :goto_1

    .line 681552
    :cond_2
    const-string v38, "bid_amount"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_3

    .line 681553
    const/4 v2, 0x1

    .line 681554
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v15

    move/from16 v35, v15

    move v15, v2

    goto :goto_1

    .line 681555
    :cond_3
    const-string v38, "boosting_status"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_4

    .line 681556
    const/4 v2, 0x1

    .line 681557
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    move-result-object v14

    move-object/from16 v34, v14

    move v14, v2

    goto :goto_1

    .line 681558
    :cond_4
    const-string v38, "budget"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_5

    .line 681559
    invoke-static/range {p0 .. p1}, LX/4Lf;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v33, v2

    goto :goto_1

    .line 681560
    :cond_5
    const-string v38, "budget_type"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_6

    .line 681561
    const/4 v2, 0x1

    .line 681562
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    move-result-object v13

    move-object/from16 v32, v13

    move v13, v2

    goto/16 :goto_1

    .line 681563
    :cond_6
    const-string v38, "feed_unit_preview"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_7

    .line 681564
    invoke-static/range {p0 .. p1}, LX/2ao;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v31, v2

    goto/16 :goto_1

    .line 681565
    :cond_7
    const-string v38, "ineligible_message"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_8

    .line 681566
    invoke-static/range {p0 .. p1}, LX/4L4;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v30, v2

    goto/16 :goto_1

    .line 681567
    :cond_8
    const-string v38, "is_pacing_editable"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_9

    .line 681568
    const/4 v2, 0x1

    .line 681569
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v29, v7

    move v7, v2

    goto/16 :goto_1

    .line 681570
    :cond_9
    const-string v38, "messages"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_a

    .line 681571
    invoke-static/range {p0 .. p1}, LX/4L4;->b(LX/15w;LX/186;)I

    move-result v2

    move/from16 v28, v2

    goto/16 :goto_1

    .line 681572
    :cond_a
    const-string v38, "pacing_type"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_b

    .line 681573
    const/4 v2, 0x1

    .line 681574
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    move-result-object v6

    move-object/from16 v27, v6

    move v6, v2

    goto/16 :goto_1

    .line 681575
    :cond_b
    const-string v38, "rejection_reason"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_c

    .line 681576
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v26, v2

    goto/16 :goto_1

    .line 681577
    :cond_c
    const-string v38, "spent"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_d

    .line 681578
    invoke-static/range {p0 .. p1}, LX/4Lf;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 681579
    :cond_d
    const-string v38, "start_time"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_e

    .line 681580
    const/4 v2, 0x1

    .line 681581
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 681582
    :cond_e
    const-string v38, "stop_time"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_f

    .line 681583
    const/4 v2, 0x1

    .line 681584
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v24

    move v12, v2

    goto/16 :goto_1

    .line 681585
    :cond_f
    const-string v38, "is_boosted_slideshow"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_10

    .line 681586
    const/4 v2, 0x1

    .line 681587
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    move/from16 v22, v11

    move v11, v2

    goto/16 :goto_1

    .line 681588
    :cond_10
    const-string v38, "is_eligible_for_action_buttons"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_11

    .line 681589
    const/4 v2, 0x1

    .line 681590
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    move/from16 v21, v10

    move v10, v2

    goto/16 :goto_1

    .line 681591
    :cond_11
    const-string v38, "aymt_lwi_channel"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_12

    .line 681592
    invoke-static/range {p0 .. p1}, LX/4KZ;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 681593
    :cond_12
    const-string v38, "post_attachment_type"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_13

    .line 681594
    const/4 v2, 0x1

    .line 681595
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    move-result-object v9

    move-object/from16 v19, v9

    move v9, v2

    goto/16 :goto_1

    .line 681596
    :cond_13
    const-string v38, "targeting_types"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_14

    .line 681597
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 681598
    :cond_14
    const-string v38, "eligible_objectives"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_15

    .line 681599
    const-class v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v2}, LX/2gu;->a(LX/15w;LX/186;Ljava/lang/Class;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 681600
    :cond_15
    const-string v38, "objective"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 681601
    const/4 v2, 0x1

    .line 681602
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    move-result-object v8

    move-object/from16 v16, v8

    move v8, v2

    goto/16 :goto_1

    .line 681603
    :cond_16
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 681604
    :cond_17
    const/16 v2, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 681605
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 681606
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 681607
    if-eqz v15, :cond_18

    .line 681608
    const/4 v2, 0x2

    const/4 v15, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1, v15}, LX/186;->a(III)V

    .line 681609
    :cond_18
    if-eqz v14, :cond_19

    .line 681610
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 681611
    :cond_19
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 681612
    if-eqz v13, :cond_1a

    .line 681613
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 681614
    :cond_1a
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 681615
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 681616
    if-eqz v7, :cond_1b

    .line 681617
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 681618
    :cond_1b
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 681619
    if-eqz v6, :cond_1c

    .line 681620
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 681621
    :cond_1c
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 681622
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 681623
    if-eqz v3, :cond_1d

    .line 681624
    const/16 v3, 0xd

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 681625
    :cond_1d
    if-eqz v12, :cond_1e

    .line 681626
    const/16 v3, 0xe

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v24

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 681627
    :cond_1e
    if-eqz v11, :cond_1f

    .line 681628
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 681629
    :cond_1f
    if-eqz v10, :cond_20

    .line 681630
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 681631
    :cond_20
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 681632
    if-eqz v9, :cond_21

    .line 681633
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 681634
    :cond_21
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 681635
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 681636
    if-eqz v8, :cond_22

    .line 681637
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 681638
    :cond_22
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_23
    move/from16 v37, v36

    move/from16 v36, v35

    move/from16 v35, v34

    move-object/from16 v34, v33

    move/from16 v33, v32

    move-object/from16 v32, v31

    move/from16 v31, v30

    move/from16 v30, v29

    move/from16 v29, v28

    move/from16 v28, v27

    move-object/from16 v27, v26

    move/from16 v26, v25

    move/from16 v40, v14

    move v14, v11

    move v11, v5

    move-object/from16 v41, v16

    move-object/from16 v16, v13

    move v13, v10

    move v10, v4

    move-wide/from16 v4, v22

    move/from16 v22, v19

    move/from16 v23, v24

    move-wide/from16 v24, v20

    move-object/from16 v19, v41

    move/from16 v20, v17

    move/from16 v21, v18

    move/from16 v18, v15

    move/from16 v17, v40

    move v15, v12

    move v12, v6

    move v6, v8

    move v8, v2

    move/from16 v42, v9

    move v9, v3

    move v3, v7

    move/from16 v7, v42

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const/16 v7, 0xa

    const/4 v6, 0x5

    const/4 v2, 0x3

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 681639
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 681640
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 681641
    if-eqz v0, :cond_0

    .line 681642
    const-string v1, "ad_account"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681643
    invoke-static {p0, v0, p2, p3}, LX/4Ke;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 681644
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 681645
    if-eqz v0, :cond_1

    .line 681646
    const-string v1, "aymt_post_footer_channel"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681647
    invoke-static {p0, v0, p2, p3}, LX/4KZ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 681648
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 681649
    if-eqz v0, :cond_2

    .line 681650
    const-string v1, "bid_amount"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681651
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 681652
    :cond_2
    invoke-virtual {p0, p1, v2, v3}, LX/15i;->a(IIS)S

    move-result v0

    .line 681653
    if-eqz v0, :cond_3

    .line 681654
    const-string v0, "boosting_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681655
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 681656
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 681657
    if-eqz v0, :cond_4

    .line 681658
    const-string v1, "budget"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681659
    invoke-static {p0, v0, p2}, LX/4Lf;->a(LX/15i;ILX/0nX;)V

    .line 681660
    :cond_4
    invoke-virtual {p0, p1, v6, v3}, LX/15i;->a(IIS)S

    move-result v0

    .line 681661
    if-eqz v0, :cond_5

    .line 681662
    const-string v0, "budget_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681663
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    invoke-virtual {p0, p1, v6, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 681664
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 681665
    if-eqz v0, :cond_6

    .line 681666
    const-string v1, "feed_unit_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681667
    invoke-static {p0, v0, p2, p3}, LX/2ao;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 681668
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 681669
    if-eqz v0, :cond_7

    .line 681670
    const-string v1, "ineligible_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681671
    invoke-static {p0, v0, p2, p3}, LX/4L4;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 681672
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 681673
    if-eqz v0, :cond_8

    .line 681674
    const-string v1, "is_pacing_editable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681675
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 681676
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 681677
    if-eqz v0, :cond_a

    .line 681678
    const-string v1, "messages"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681679
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 681680
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_9

    .line 681681
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/4L4;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 681682
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 681683
    :cond_9
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 681684
    :cond_a
    invoke-virtual {p0, p1, v7, v3}, LX/15i;->a(IIS)S

    move-result v0

    .line 681685
    if-eqz v0, :cond_b

    .line 681686
    const-string v0, "pacing_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681687
    const-class v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    invoke-virtual {p0, p1, v7, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 681688
    :cond_b
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 681689
    if-eqz v0, :cond_c

    .line 681690
    const-string v1, "rejection_reason"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681691
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 681692
    :cond_c
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 681693
    if-eqz v0, :cond_d

    .line 681694
    const-string v1, "spent"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681695
    invoke-static {p0, v0, p2}, LX/4Lf;->a(LX/15i;ILX/0nX;)V

    .line 681696
    :cond_d
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 681697
    cmp-long v2, v0, v4

    if-eqz v2, :cond_e

    .line 681698
    const-string v2, "start_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681699
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 681700
    :cond_e
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 681701
    cmp-long v2, v0, v4

    if-eqz v2, :cond_f

    .line 681702
    const-string v2, "stop_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681703
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 681704
    :cond_f
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 681705
    if-eqz v0, :cond_10

    .line 681706
    const-string v1, "is_boosted_slideshow"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681707
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 681708
    :cond_10
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 681709
    if-eqz v0, :cond_11

    .line 681710
    const-string v1, "is_eligible_for_action_buttons"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681711
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 681712
    :cond_11
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 681713
    if-eqz v0, :cond_12

    .line 681714
    const-string v1, "aymt_lwi_channel"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681715
    invoke-static {p0, v0, p2, p3}, LX/4KZ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 681716
    :cond_12
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(IIS)S

    move-result v0

    .line 681717
    if-eqz v0, :cond_13

    .line 681718
    const-string v0, "post_attachment_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681719
    const/16 v0, 0x12

    const-class v1, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 681720
    :cond_13
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 681721
    if-eqz v0, :cond_14

    .line 681722
    const-string v0, "targeting_types"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681723
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 681724
    :cond_14
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 681725
    if-eqz v0, :cond_15

    .line 681726
    const-string v0, "eligible_objectives"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681727
    const/16 v0, 0x14

    const-class v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->b(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->b(Ljava/util/Iterator;LX/0nX;)V

    .line 681728
    :cond_15
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(IIS)S

    move-result v0

    .line 681729
    if-eqz v0, :cond_16

    .line 681730
    const-string v0, "objective"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681731
    const/16 v0, 0x15

    const-class v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 681732
    :cond_16
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 681733
    return-void
.end method
