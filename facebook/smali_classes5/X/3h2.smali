.class public final LX/3h2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 625310
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 625311
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 625312
    :goto_0
    return v1

    .line 625313
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 625314
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 625315
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 625316
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 625317
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 625318
    const-string v4, "parts"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 625319
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 625320
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 625321
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_2

    .line 625322
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 625323
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v6, :cond_d

    .line 625324
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 625325
    :goto_3
    move v3, v4

    .line 625326
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 625327
    :cond_2
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 625328
    goto :goto_1

    .line 625329
    :cond_3
    const-string v4, "text"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 625330
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 625331
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 625332
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 625333
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 625334
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 625335
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_a

    .line 625336
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 625337
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 625338
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_6

    if-eqz v10, :cond_6

    .line 625339
    const-string v11, "length"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 625340
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v6

    move v9, v6

    move v6, v5

    goto :goto_4

    .line 625341
    :cond_7
    const-string v11, "offset"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 625342
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v8, v3

    move v3, v5

    goto :goto_4

    .line 625343
    :cond_8
    const-string v11, "part"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 625344
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    goto :goto_4

    .line 625345
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 625346
    :cond_a
    const/4 v10, 0x3

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 625347
    if-eqz v6, :cond_b

    .line 625348
    invoke-virtual {p1, v4, v9, v4}, LX/186;->a(III)V

    .line 625349
    :cond_b
    if-eqz v3, :cond_c

    .line 625350
    invoke-virtual {p1, v5, v8, v4}, LX/186;->a(III)V

    .line 625351
    :cond_c
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v7}, LX/186;->b(II)V

    .line 625352
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_3

    :cond_d
    move v3, v4

    move v6, v4

    move v7, v4

    move v8, v4

    move v9, v4

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 625353
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 625354
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 625355
    if-eqz v0, :cond_4

    .line 625356
    const-string v1, "parts"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625357
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 625358
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 625359
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    const/4 p3, 0x2

    const/4 v5, 0x0

    .line 625360
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 625361
    invoke-virtual {p0, v2, v5, v5}, LX/15i;->a(III)I

    move-result v3

    .line 625362
    if-eqz v3, :cond_0

    .line 625363
    const-string v4, "length"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625364
    invoke-virtual {p2, v3}, LX/0nX;->b(I)V

    .line 625365
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3, v5}, LX/15i;->a(III)I

    move-result v3

    .line 625366
    if-eqz v3, :cond_1

    .line 625367
    const-string v4, "offset"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625368
    invoke-virtual {p2, v3}, LX/0nX;->b(I)V

    .line 625369
    :cond_1
    invoke-virtual {p0, v2, p3}, LX/15i;->g(II)I

    move-result v3

    .line 625370
    if-eqz v3, :cond_2

    .line 625371
    const-string v3, "part"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625372
    invoke-virtual {p0, v2, p3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 625373
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 625374
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 625375
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 625376
    :cond_4
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 625377
    if-eqz v0, :cond_5

    .line 625378
    const-string v1, "text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625379
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 625380
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 625381
    return-void
.end method
