.class public LX/4QY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 705236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 705230
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 705231
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 705232
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 705233
    invoke-static {p0, p1}, LX/4QY;->b(LX/15w;LX/186;)I

    move-result v1

    .line 705234
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 705235
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 705182
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 705183
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 705184
    :goto_0
    return v1

    .line 705185
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 705186
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 705187
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 705188
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 705189
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 705190
    const-string v7, "action_link"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 705191
    invoke-static {p0, p1}, LX/2bb;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 705192
    :cond_2
    const-string v7, "feed_unit_preview"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 705193
    invoke-static {p0, p1}, LX/2ao;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 705194
    :cond_3
    const-string v7, "hideable_token"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 705195
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 705196
    :cond_4
    const-string v7, "profile"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 705197
    invoke-static {p0, p1}, LX/2aw;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 705198
    :cond_5
    const-string v7, "tracking"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 705199
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 705200
    :cond_6
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 705201
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 705202
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 705203
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 705204
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 705205
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 705206
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 705207
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 705208
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 705209
    if-eqz v0, :cond_0

    .line 705210
    const-string v1, "action_link"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705211
    invoke-static {p0, v0, p2, p3}, LX/2bb;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 705212
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 705213
    if-eqz v0, :cond_1

    .line 705214
    const-string v1, "feed_unit_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705215
    invoke-static {p0, v0, p2, p3}, LX/2ao;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 705216
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 705217
    if-eqz v0, :cond_2

    .line 705218
    const-string v1, "hideable_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705219
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 705220
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 705221
    if-eqz v0, :cond_3

    .line 705222
    const-string v1, "profile"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705223
    invoke-static {p0, v0, p2, p3}, LX/2aw;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 705224
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 705225
    if-eqz v0, :cond_4

    .line 705226
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705227
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 705228
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 705229
    return-void
.end method
