.class public final LX/4rv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/4ru;

.field private b:LX/4ru;

.field private c:I

.field private d:[Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 817007
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/Object;I[Ljava/lang/Object;I)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 817008
    iget-object v0, p0, LX/4rv;->a:LX/4ru;

    move v1, v2

    :goto_0
    if-eqz v0, :cond_0

    .line 817009
    iget-object v3, v0, LX/4ru;->a:[Ljava/lang/Object;

    move-object v3, v3

    .line 817010
    array-length v4, v3

    .line 817011
    invoke-static {v3, v2, p1, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 817012
    add-int/2addr v1, v4

    .line 817013
    iget-object v3, v0, LX/4ru;->b:LX/4ru;

    move-object v0, v3

    .line 817014
    goto :goto_0

    .line 817015
    :cond_0
    invoke-static {p3, v2, p1, v1, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 817016
    add-int v0, v1, p4

    .line 817017
    if-eq v0, p2, :cond_1

    .line 817018
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Should have gotten "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " entries, got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 817019
    :cond_1
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 817020
    iget-object v0, p0, LX/4rv;->b:LX/4ru;

    if-eqz v0, :cond_0

    .line 817021
    iget-object v0, p0, LX/4rv;->b:LX/4ru;

    .line 817022
    iget-object v1, v0, LX/4ru;->a:[Ljava/lang/Object;

    move-object v0, v1

    .line 817023
    iput-object v0, p0, LX/4rv;->d:[Ljava/lang/Object;

    .line 817024
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/4rv;->b:LX/4ru;

    iput-object v0, p0, LX/4rv;->a:LX/4ru;

    .line 817025
    const/4 v0, 0x0

    iput v0, p0, LX/4rv;->c:I

    .line 817026
    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;ILjava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Object;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 817027
    iget-object v1, p0, LX/4rv;->a:LX/4ru;

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_1

    .line 817028
    iget-object v1, v2, LX/4ru;->a:[Ljava/lang/Object;

    move-object v3, v1

    .line 817029
    array-length v4, v3

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_0

    .line 817030
    aget-object v5, v3, v1

    invoke-interface {p3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 817031
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 817032
    :cond_0
    iget-object v1, v2, LX/4ru;->b:LX/4ru;

    move-object v1, v1

    .line 817033
    move-object v2, v1

    goto :goto_0

    .line 817034
    :cond_1
    :goto_2
    if-ge v0, p2, :cond_2

    .line 817035
    aget-object v1, p1, v0

    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 817036
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 817037
    :cond_2
    return-void
.end method

.method public final a()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 817038
    invoke-direct {p0}, LX/4rv;->c()V

    .line 817039
    iget-object v0, p0, LX/4rv;->d:[Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 817040
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/Object;

    .line 817041
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/4rv;->d:[Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 2

    .prologue
    .line 817042
    new-instance v0, LX/4ru;

    invoke-direct {v0, p1}, LX/4ru;-><init>([Ljava/lang/Object;)V

    .line 817043
    iget-object v1, p0, LX/4rv;->a:LX/4ru;

    if-nez v1, :cond_0

    .line 817044
    iput-object v0, p0, LX/4rv;->b:LX/4ru;

    iput-object v0, p0, LX/4rv;->a:LX/4ru;

    .line 817045
    :goto_0
    array-length v0, p1

    .line 817046
    iget v1, p0, LX/4rv;->c:I

    add-int/2addr v1, v0

    iput v1, p0, LX/4rv;->c:I

    .line 817047
    const/16 v1, 0x4000

    if-ge v0, v1, :cond_1

    .line 817048
    add-int/2addr v0, v0

    .line 817049
    :goto_1
    new-array v0, v0, [Ljava/lang/Object;

    return-object v0

    .line 817050
    :cond_0
    iget-object v1, p0, LX/4rv;->b:LX/4ru;

    invoke-virtual {v1, v0}, LX/4ru;->a(LX/4ru;)V

    .line 817051
    iput-object v0, p0, LX/4rv;->b:LX/4ru;

    goto :goto_0

    .line 817052
    :cond_1
    shr-int/lit8 v1, v0, 0x2

    add-int/2addr v0, v1

    goto :goto_1
.end method

.method public final a([Ljava/lang/Object;I)[Ljava/lang/Object;
    .locals 2

    .prologue
    .line 817053
    iget v0, p0, LX/4rv;->c:I

    add-int/2addr v0, p2

    .line 817054
    new-array v1, v0, [Ljava/lang/Object;

    .line 817055
    invoke-direct {p0, v1, v0, p1, p2}, LX/4rv;->a(Ljava/lang/Object;I[Ljava/lang/Object;I)V

    .line 817056
    return-object v1
.end method

.method public final a([Ljava/lang/Object;ILjava/lang/Class;)[Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([",
            "Ljava/lang/Object;",
            "I",
            "Ljava/lang/Class",
            "<TT;>;)[TT;"
        }
    .end annotation

    .prologue
    .line 817057
    iget v0, p0, LX/4rv;->c:I

    add-int v1, p2, v0

    .line 817058
    invoke-static {p3, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 817059
    invoke-direct {p0, v0, v1, p1, p2}, LX/4rv;->a(Ljava/lang/Object;I[Ljava/lang/Object;I)V

    .line 817060
    invoke-direct {p0}, LX/4rv;->c()V

    .line 817061
    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 817062
    iget-object v0, p0, LX/4rv;->d:[Ljava/lang/Object;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/4rv;->d:[Ljava/lang/Object;

    array-length v0, v0

    goto :goto_0
.end method
