.class public LX/4Si;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 715372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 715373
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 715374
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 715375
    if-eqz v0, :cond_0

    .line 715376
    const-string v1, "electoral_vote_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715377
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 715378
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 715379
    if-eqz v0, :cond_1

    .line 715380
    const-string v1, "fbid"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715381
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 715382
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 715383
    if-eqz v0, :cond_2

    .line 715384
    const-string v1, "is_winner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715385
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 715386
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 715387
    if-eqz v0, :cond_3

    .line 715388
    const-string v1, "party"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715389
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 715390
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 715391
    if-eqz v0, :cond_4

    .line 715392
    const-string v1, "vote_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715393
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 715394
    :cond_4
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 715395
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_5

    .line 715396
    const-string v2, "unformatted_vote_percentage"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715397
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 715398
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 715399
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 18

    .prologue
    .line 715400
    const/4 v12, 0x0

    .line 715401
    const/4 v11, 0x0

    .line 715402
    const/4 v10, 0x0

    .line 715403
    const/4 v9, 0x0

    .line 715404
    const/4 v8, 0x0

    .line 715405
    const-wide/16 v6, 0x0

    .line 715406
    const/4 v5, 0x0

    .line 715407
    const/4 v4, 0x0

    .line 715408
    const/4 v3, 0x0

    .line 715409
    const/4 v2, 0x0

    .line 715410
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_c

    .line 715411
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 715412
    const/4 v2, 0x0

    .line 715413
    :goto_0
    return v2

    .line 715414
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 715415
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_7

    .line 715416
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 715417
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 715418
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 715419
    const-string v14, "electoral_vote_count"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 715420
    const/4 v7, 0x1

    .line 715421
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    goto :goto_1

    .line 715422
    :cond_2
    const-string v14, "fbid"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 715423
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 715424
    :cond_3
    const-string v14, "is_winner"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 715425
    const/4 v6, 0x1

    .line 715426
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto :goto_1

    .line 715427
    :cond_4
    const-string v14, "party"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 715428
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 715429
    :cond_5
    const-string v14, "vote_count"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 715430
    const/4 v3, 0x1

    .line 715431
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v8

    goto :goto_1

    .line 715432
    :cond_6
    const-string v14, "unformatted_vote_percentage"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 715433
    const/4 v2, 0x1

    .line 715434
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    goto :goto_1

    .line 715435
    :cond_7
    const/16 v13, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 715436
    if-eqz v7, :cond_8

    .line 715437
    const/4 v7, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v12, v13}, LX/186;->a(III)V

    .line 715438
    :cond_8
    const/4 v7, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v11}, LX/186;->b(II)V

    .line 715439
    if-eqz v6, :cond_9

    .line 715440
    const/4 v6, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v10}, LX/186;->a(IZ)V

    .line 715441
    :cond_9
    const/4 v6, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v9}, LX/186;->b(II)V

    .line 715442
    if-eqz v3, :cond_a

    .line 715443
    const/4 v3, 0x5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8, v6}, LX/186;->a(III)V

    .line 715444
    :cond_a
    if-eqz v2, :cond_b

    .line 715445
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 715446
    :cond_b
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_c
    move/from16 v16, v4

    move/from16 v17, v5

    move-wide v4, v6

    move/from16 v6, v16

    move/from16 v7, v17

    goto/16 :goto_1
.end method
