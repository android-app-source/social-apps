.class public final LX/3TR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

.field public final synthetic val$publishCallback:Lcom/facebook/omnistore/mqtt/PublishCallback;

.field public final synthetic val$topicName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;Ljava/lang/String;Lcom/facebook/omnistore/mqtt/PublishCallback;)V
    .locals 0

    .prologue
    .line 584275
    iput-object p1, p0, LX/3TR;->this$0:Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    iput-object p2, p0, LX/3TR;->val$topicName:Ljava/lang/String;

    iput-object p3, p0, LX/3TR;->val$publishCallback:Lcom/facebook/omnistore/mqtt/PublishCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 584279
    instance-of v0, p1, LX/6nE;

    if-nez v0, :cond_0

    instance-of v0, p1, Landroid/os/RemoteException;

    if-eqz v0, :cond_1

    .line 584280
    :cond_0
    const-string v0, "OmnistoreMqttJniHandler"

    const-string v1, "Publish on topic %s failed"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/3TR;->val$topicName:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, p1, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 584281
    :goto_0
    iget-object v0, p0, LX/3TR;->this$0:Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    iget-object v1, p0, LX/3TR;->val$publishCallback:Lcom/facebook/omnistore/mqtt/PublishCallback;

    # invokes: Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->onPublishFailed(Lcom/facebook/omnistore/mqtt/PublishCallback;)V
    invoke-static {v0, v1}, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->access$400(Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;Lcom/facebook/omnistore/mqtt/PublishCallback;)V

    .line 584282
    return-void

    .line 584283
    :cond_1
    iget-object v0, p0, LX/3TR;->this$0:Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    iget-object v0, v0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->mFbErrorReporter:LX/03V;

    const-string v1, "OmnistoreMqttJniHandler"

    const-string v2, "Unexpected publish failure"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 584276
    check-cast p1, Ljava/lang/Void;

    .line 584277
    iget-object v0, p0, LX/3TR;->this$0:Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    iget-object v1, p0, LX/3TR;->val$publishCallback:Lcom/facebook/omnistore/mqtt/PublishCallback;

    # invokes: Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->onPublishAcked(Lcom/facebook/omnistore/mqtt/PublishCallback;)V
    invoke-static {v0, v1}, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->access$200(Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;Lcom/facebook/omnistore/mqtt/PublishCallback;)V

    .line 584278
    return-void
.end method
