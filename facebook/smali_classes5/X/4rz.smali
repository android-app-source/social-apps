.class public final LX/4rz;
.super LX/15v;
.source ""


# instance fields
.field public b:LX/0lD;

.field public c:LX/4s0;

.field public d:I

.field public e:LX/15y;

.field public f:Z

.field public transient g:LX/2SG;

.field public h:LX/28G;


# direct methods
.method public constructor <init>(LX/4s0;LX/0lD;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 817234
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/15v;-><init>(I)V

    .line 817235
    const/4 v0, 0x0

    iput-object v0, p0, LX/4rz;->h:LX/28G;

    .line 817236
    iput-object p1, p0, LX/4rz;->c:LX/4s0;

    .line 817237
    iput v1, p0, LX/4rz;->d:I

    .line 817238
    iput-object p2, p0, LX/4rz;->b:LX/0lD;

    .line 817239
    invoke-static {v1, v1}, LX/15y;->a(II)LX/15y;

    move-result-object v0

    iput-object v0, p0, LX/4rz;->e:LX/15y;

    .line 817240
    return-void
.end method

.method private K()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 817233
    iget-object v0, p0, LX/4rz;->c:LX/4s0;

    iget v1, p0, LX/4rz;->d:I

    invoke-virtual {v0, v1}, LX/4s0;->b(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private L()V
    .locals 2

    .prologue
    .line 817230
    iget-object v0, p0, LX/15v;->K:LX/15z;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v0}, LX/15z;->isNumeric()Z

    move-result v0

    if-nez v0, :cond_1

    .line 817231
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Current token ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") not numeric, can not use numeric value accessors"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15w;->b(Ljava/lang/String;)LX/2aQ;

    move-result-object v0

    throw v0

    .line 817232
    :cond_1
    return-void
.end method


# virtual methods
.method public final A()F
    .locals 1

    .prologue
    .line 817229
    invoke-virtual {p0}, LX/15w;->t()Ljava/lang/Number;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    return v0
.end method

.method public final B()D
    .locals 2

    .prologue
    .line 817228
    invoke-virtual {p0}, LX/15w;->t()Ljava/lang/Number;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    return-wide v0
.end method

.method public final C()Ljava/math/BigDecimal;
    .locals 3

    .prologue
    .line 817220
    invoke-virtual {p0}, LX/15w;->t()Ljava/lang/Number;

    move-result-object v0

    .line 817221
    instance-of v1, v0, Ljava/math/BigDecimal;

    if-eqz v1, :cond_0

    .line 817222
    check-cast v0, Ljava/math/BigDecimal;

    .line 817223
    :goto_0
    return-object v0

    .line 817224
    :cond_0
    sget-object v1, LX/4ry;->b:[I

    invoke-virtual {p0}, LX/15w;->u()LX/16L;

    move-result-object v2

    invoke-virtual {v2}, LX/16L;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 817225
    :pswitch_0
    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0

    .line 817226
    :pswitch_1
    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0

    .line 817227
    :pswitch_2
    new-instance v1, Ljava/math/BigDecimal;

    check-cast v0, Ljava/math/BigInteger;

    invoke-direct {v1, v0}, Ljava/math/BigDecimal;-><init>(Ljava/math/BigInteger;)V

    move-object v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final D()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 817217
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_EMBEDDED_OBJECT:LX/15z;

    if-ne v0, v1, :cond_0

    .line 817218
    invoke-direct {p0}, LX/4rz;->K()Ljava/lang/Object;

    move-result-object v0

    .line 817219
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final P()V
    .locals 0

    .prologue
    .line 817215
    invoke-static {}, LX/4pl;->b()V

    .line 817216
    return-void
.end method

.method public final a()LX/0lD;
    .locals 1

    .prologue
    .line 817214
    iget-object v0, p0, LX/4rz;->b:LX/0lD;

    return-object v0
.end method

.method public final a(LX/0lD;)V
    .locals 0

    .prologue
    .line 817213
    iput-object p1, p0, LX/4rz;->b:LX/0lD;

    return-void
.end method

.method public final a(LX/0ln;)[B
    .locals 3

    .prologue
    .line 817112
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_EMBEDDED_OBJECT:LX/15z;

    if-ne v0, v1, :cond_0

    .line 817113
    invoke-direct {p0}, LX/4rz;->K()Ljava/lang/Object;

    move-result-object v0

    .line 817114
    instance-of v1, v0, [B

    if-eqz v1, :cond_0

    .line 817115
    check-cast v0, [B

    check-cast v0, [B

    .line 817116
    :goto_0
    return-object v0

    .line 817117
    :cond_0
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-eq v0, v1, :cond_1

    .line 817118
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Current token ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") not VALUE_STRING (or VALUE_EMBEDDED_OBJECT with byte[]), can not access as binary"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15w;->b(Ljava/lang/String;)LX/2aQ;

    move-result-object v0

    throw v0

    .line 817119
    :cond_1
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    .line 817120
    if-nez v1, :cond_2

    .line 817121
    const/4 v0, 0x0

    goto :goto_0

    .line 817122
    :cond_2
    iget-object v0, p0, LX/4rz;->g:LX/2SG;

    .line 817123
    if-nez v0, :cond_3

    .line 817124
    new-instance v0, LX/2SG;

    const/16 v2, 0x64

    invoke-direct {v0, v2}, LX/2SG;-><init>(I)V

    iput-object v0, p0, LX/4rz;->g:LX/2SG;

    .line 817125
    :goto_1
    invoke-virtual {p0, v1, v0, p1}, LX/15v;->a(Ljava/lang/String;LX/2SG;LX/0ln;)V

    .line 817126
    invoke-virtual {v0}, LX/2SG;->c()[B

    move-result-object v0

    goto :goto_0

    .line 817127
    :cond_3
    iget-object v2, p0, LX/4rz;->g:LX/2SG;

    invoke-virtual {v2}, LX/2SG;->a()V

    goto :goto_1
.end method

.method public final c()LX/15z;
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, -0x1

    .line 817187
    iget-boolean v1, p0, LX/4rz;->f:Z

    if-nez v1, :cond_0

    iget-object v1, p0, LX/4rz;->c:LX/4s0;

    if-nez v1, :cond_1

    .line 817188
    :cond_0
    :goto_0
    return-object v0

    .line 817189
    :cond_1
    iget v1, p0, LX/4rz;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/4rz;->d:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_2

    .line 817190
    const/4 v1, 0x0

    iput v1, p0, LX/4rz;->d:I

    .line 817191
    iget-object v1, p0, LX/4rz;->c:LX/4s0;

    .line 817192
    iget-object v2, v1, LX/4s0;->a:LX/4s0;

    move-object v1, v2

    .line 817193
    iput-object v1, p0, LX/4rz;->c:LX/4s0;

    .line 817194
    iget-object v1, p0, LX/4rz;->c:LX/4s0;

    if-eqz v1, :cond_0

    .line 817195
    :cond_2
    iget-object v0, p0, LX/4rz;->c:LX/4s0;

    iget v1, p0, LX/4rz;->d:I

    invoke-virtual {v0, v1}, LX/4s0;->a(I)LX/15z;

    move-result-object v0

    iput-object v0, p0, LX/4rz;->K:LX/15z;

    .line 817196
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v1, :cond_5

    .line 817197
    invoke-direct {p0}, LX/4rz;->K()Ljava/lang/Object;

    move-result-object v0

    .line 817198
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    .line 817199
    :goto_1
    iget-object v1, p0, LX/4rz;->e:LX/15y;

    .line 817200
    iput-object v0, v1, LX/15y;->f:Ljava/lang/String;

    .line 817201
    :cond_3
    :goto_2
    iget-object v0, p0, LX/15v;->K:LX/15z;

    goto :goto_0

    .line 817202
    :cond_4
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 817203
    :cond_5
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-ne v0, v1, :cond_6

    .line 817204
    iget-object v0, p0, LX/4rz;->e:LX/15y;

    invoke-virtual {v0, v3, v3}, LX/15y;->c(II)LX/15y;

    move-result-object v0

    iput-object v0, p0, LX/4rz;->e:LX/15y;

    goto :goto_2

    .line 817205
    :cond_6
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->START_ARRAY:LX/15z;

    if-ne v0, v1, :cond_7

    .line 817206
    iget-object v0, p0, LX/4rz;->e:LX/15y;

    invoke-virtual {v0, v3, v3}, LX/15y;->b(II)LX/15y;

    move-result-object v0

    iput-object v0, p0, LX/4rz;->e:LX/15y;

    goto :goto_2

    .line 817207
    :cond_7
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v1, :cond_8

    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->END_ARRAY:LX/15z;

    if-ne v0, v1, :cond_3

    .line 817208
    :cond_8
    iget-object v0, p0, LX/4rz;->e:LX/15y;

    .line 817209
    iget-object v1, v0, LX/15y;->c:LX/15y;

    move-object v0, v1

    .line 817210
    iput-object v0, p0, LX/4rz;->e:LX/15y;

    .line 817211
    iget-object v0, p0, LX/4rz;->e:LX/15y;

    if-nez v0, :cond_3

    .line 817212
    invoke-static {v3, v3}, LX/15y;->a(II)LX/15y;

    move-result-object v0

    iput-object v0, p0, LX/4rz;->e:LX/15y;

    goto :goto_2
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 817184
    iget-boolean v0, p0, LX/4rz;->f:Z

    if-nez v0, :cond_0

    .line 817185
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4rz;->f:Z

    .line 817186
    :cond_0
    return-void
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 817183
    iget-object v0, p0, LX/4rz;->e:LX/15y;

    invoke-virtual {v0}, LX/12V;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final j()LX/12V;
    .locals 1

    .prologue
    .line 817182
    iget-object v0, p0, LX/4rz;->e:LX/15y;

    return-object v0
.end method

.method public final k()LX/28G;
    .locals 1

    .prologue
    .line 817111
    invoke-virtual {p0}, LX/15w;->l()LX/28G;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/28G;
    .locals 1

    .prologue
    .line 817128
    iget-object v0, p0, LX/4rz;->h:LX/28G;

    if-nez v0, :cond_0

    sget-object v0, LX/28G;->a:LX/28G;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/4rz;->h:LX/28G;

    goto :goto_0
.end method

.method public final o()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 817129
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v2, LX/15z;->VALUE_STRING:LX/15z;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v2, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v2, :cond_3

    .line 817130
    :cond_0
    invoke-direct {p0}, LX/4rz;->K()Ljava/lang/Object;

    move-result-object v0

    .line 817131
    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 817132
    check-cast v0, Ljava/lang/String;

    .line 817133
    :goto_0
    return-object v0

    .line 817134
    :cond_1
    if-nez v0, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 817135
    :cond_3
    iget-object v0, p0, LX/15v;->K:LX/15z;

    if-nez v0, :cond_4

    move-object v0, v1

    .line 817136
    goto :goto_0

    .line 817137
    :cond_4
    sget-object v0, LX/4ry;->a:[I

    iget-object v2, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v2}, LX/15z;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 817138
    iget-object v0, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v0}, LX/15z;->asString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 817139
    :pswitch_0
    invoke-direct {p0}, LX/4rz;->K()Ljava/lang/Object;

    move-result-object v0

    .line 817140
    if-nez v0, :cond_5

    move-object v0, v1

    goto :goto_0

    :cond_5
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final p()[C
    .locals 1

    .prologue
    .line 817141
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    .line 817142
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    goto :goto_0
.end method

.method public final q()I
    .locals 1

    .prologue
    .line 817143
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    .line 817144
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 817145
    const/4 v0, 0x0

    return v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 817146
    const/4 v0, 0x0

    return v0
.end method

.method public final t()Ljava/lang/Number;
    .locals 4

    .prologue
    .line 817147
    invoke-direct {p0}, LX/4rz;->L()V

    .line 817148
    invoke-direct {p0}, LX/4rz;->K()Ljava/lang/Object;

    move-result-object v0

    .line 817149
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_0

    .line 817150
    check-cast v0, Ljava/lang/Number;

    .line 817151
    :goto_0
    return-object v0

    .line 817152
    :cond_0
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 817153
    check-cast v0, Ljava/lang/String;

    .line 817154
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-ltz v1, :cond_1

    .line 817155
    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 817156
    :cond_1
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 817157
    :cond_2
    if-nez v0, :cond_3

    .line 817158
    const/4 v0, 0x0

    goto :goto_0

    .line 817159
    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Internal error: entry should be a Number, but is of type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final u()LX/16L;
    .locals 2

    .prologue
    .line 817160
    invoke-virtual {p0}, LX/15w;->t()Ljava/lang/Number;

    move-result-object v0

    .line 817161
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_0

    sget-object v0, LX/16L;->INT:LX/16L;

    .line 817162
    :goto_0
    return-object v0

    .line 817163
    :cond_0
    instance-of v1, v0, Ljava/lang/Long;

    if-eqz v1, :cond_1

    sget-object v0, LX/16L;->LONG:LX/16L;

    goto :goto_0

    .line 817164
    :cond_1
    instance-of v1, v0, Ljava/lang/Double;

    if-eqz v1, :cond_2

    sget-object v0, LX/16L;->DOUBLE:LX/16L;

    goto :goto_0

    .line 817165
    :cond_2
    instance-of v1, v0, Ljava/math/BigDecimal;

    if-eqz v1, :cond_3

    sget-object v0, LX/16L;->BIG_DECIMAL:LX/16L;

    goto :goto_0

    .line 817166
    :cond_3
    instance-of v1, v0, Ljava/math/BigInteger;

    if-eqz v1, :cond_4

    sget-object v0, LX/16L;->BIG_INTEGER:LX/16L;

    goto :goto_0

    .line 817167
    :cond_4
    instance-of v1, v0, Ljava/lang/Float;

    if-eqz v1, :cond_5

    sget-object v0, LX/16L;->FLOAT:LX/16L;

    goto :goto_0

    .line 817168
    :cond_5
    instance-of v0, v0, Ljava/lang/Short;

    if-eqz v0, :cond_6

    sget-object v0, LX/16L;->INT:LX/16L;

    goto :goto_0

    .line 817169
    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final version()LX/0ne;
    .locals 1

    .prologue
    .line 817170
    sget-object v0, LX/4pz;->VERSION:LX/0ne;

    return-object v0
.end method

.method public final x()I
    .locals 2

    .prologue
    .line 817171
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    if-ne v0, v1, :cond_0

    .line 817172
    invoke-direct {p0}, LX/4rz;->K()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    .line 817173
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/15w;->t()Ljava/lang/Number;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final y()J
    .locals 2

    .prologue
    .line 817174
    invoke-virtual {p0}, LX/15w;->t()Ljava/lang/Number;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final z()Ljava/math/BigInteger;
    .locals 3

    .prologue
    .line 817175
    invoke-virtual {p0}, LX/15w;->t()Ljava/lang/Number;

    move-result-object v0

    .line 817176
    instance-of v1, v0, Ljava/math/BigInteger;

    if-eqz v1, :cond_0

    .line 817177
    check-cast v0, Ljava/math/BigInteger;

    .line 817178
    :goto_0
    return-object v0

    .line 817179
    :cond_0
    invoke-virtual {p0}, LX/15w;->u()LX/16L;

    move-result-object v1

    sget-object v2, LX/16L;->BIG_DECIMAL:LX/16L;

    if-ne v1, v2, :cond_1

    .line 817180
    check-cast v0, Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v0

    goto :goto_0

    .line 817181
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    goto :goto_0
.end method
