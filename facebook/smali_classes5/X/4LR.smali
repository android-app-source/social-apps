.class public LX/4LR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 683206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 24

    .prologue
    .line 683207
    const/16 v17, 0x0

    .line 683208
    const/16 v16, 0x0

    .line 683209
    const-wide/16 v14, 0x0

    .line 683210
    const/4 v13, 0x0

    .line 683211
    const/4 v12, 0x0

    .line 683212
    const/4 v11, 0x0

    .line 683213
    const/4 v10, 0x0

    .line 683214
    const/4 v9, 0x0

    .line 683215
    const/4 v8, 0x0

    .line 683216
    const/4 v7, 0x0

    .line 683217
    const/4 v6, 0x0

    .line 683218
    const/4 v5, 0x0

    .line 683219
    const/4 v4, 0x0

    .line 683220
    const/4 v3, 0x0

    .line 683221
    const/4 v2, 0x0

    .line 683222
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_11

    .line 683223
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 683224
    const/4 v2, 0x0

    .line 683225
    :goto_0
    return v2

    .line 683226
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_e

    .line 683227
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 683228
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 683229
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v20, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v20

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 683230
    const-string v6, "cache_id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 683231
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto :goto_1

    .line 683232
    :cond_1
    const-string v6, "debug_info"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 683233
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 683234
    :cond_2
    const-string v6, "fetchTimeMs"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 683235
    const/4 v2, 0x1

    .line 683236
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 683237
    :cond_3
    const-string v6, "hideable_token"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 683238
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 683239
    :cond_4
    const-string v6, "items"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 683240
    invoke-static/range {p0 .. p1}, LX/4LS;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto :goto_1

    .line 683241
    :cond_5
    const-string v6, "local_last_negative_feedback_action_type"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 683242
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 683243
    :cond_6
    const-string v6, "local_story_visibility"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 683244
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 683245
    :cond_7
    const-string v6, "local_story_visible_height"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 683246
    const/4 v2, 0x1

    .line 683247
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move v14, v6

    goto/16 :goto_1

    .line 683248
    :cond_8
    const-string v6, "negative_feedback_actions"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 683249
    invoke-static/range {p0 .. p1}, LX/2bY;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 683250
    :cond_9
    const-string v6, "short_term_cache_key"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 683251
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 683252
    :cond_a
    const-string v6, "title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 683253
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 683254
    :cond_b
    const-string v6, "tracking"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 683255
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 683256
    :cond_c
    const-string v6, "local_removed_items"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 683257
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    move v9, v2

    goto/16 :goto_1

    .line 683258
    :cond_d
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 683259
    :cond_e
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 683260
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 683261
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 683262
    if-eqz v3, :cond_f

    .line 683263
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 683264
    :cond_f
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 683265
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 683266
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 683267
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 683268
    if-eqz v8, :cond_10

    .line 683269
    const/4 v2, 0x7

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14, v3}, LX/186;->a(III)V

    .line 683270
    :cond_10
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 683271
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 683272
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 683273
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 683274
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 683275
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_11
    move/from16 v18, v13

    move/from16 v19, v17

    move v13, v8

    move/from16 v17, v12

    move v12, v7

    move v8, v2

    move/from16 v7, v16

    move/from16 v16, v11

    move v11, v6

    move/from16 v21, v10

    move v10, v5

    move-wide/from16 v22, v14

    move/from16 v15, v21

    move v14, v9

    move v9, v4

    move-wide/from16 v4, v22

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 683195
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 683196
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 683197
    invoke-static {p0, v2}, LX/4LR;->a(LX/15w;LX/186;)I

    move-result v1

    .line 683198
    if-eqz v0, :cond_0

    .line 683199
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 683200
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 683201
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 683202
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 683203
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 683204
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 683205
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/16 v4, 0xc

    const/4 v3, 0x0

    .line 683132
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 683133
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683134
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 683135
    const-string v0, "name"

    const-string v1, "ConnectWithFacebookFamilyFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 683136
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 683137
    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 683138
    if-eqz v0, :cond_0

    .line 683139
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683140
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 683141
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 683142
    if-eqz v0, :cond_1

    .line 683143
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683144
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 683145
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 683146
    cmp-long v2, v0, v6

    if-eqz v2, :cond_2

    .line 683147
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683148
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 683149
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 683150
    if-eqz v0, :cond_3

    .line 683151
    const-string v1, "hideable_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683152
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 683153
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 683154
    if-eqz v0, :cond_5

    .line 683155
    const-string v1, "items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683156
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 683157
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_4

    .line 683158
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/4LS;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 683159
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 683160
    :cond_4
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 683161
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 683162
    if-eqz v0, :cond_6

    .line 683163
    const-string v1, "local_last_negative_feedback_action_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683164
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 683165
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 683166
    if-eqz v0, :cond_7

    .line 683167
    const-string v1, "local_story_visibility"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683168
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 683169
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 683170
    if-eqz v0, :cond_8

    .line 683171
    const-string v1, "local_story_visible_height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683172
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 683173
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 683174
    if-eqz v0, :cond_9

    .line 683175
    const-string v1, "negative_feedback_actions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683176
    invoke-static {p0, v0, p2, p3}, LX/2bY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 683177
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 683178
    if-eqz v0, :cond_a

    .line 683179
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683180
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 683181
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 683182
    if-eqz v0, :cond_b

    .line 683183
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683184
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 683185
    :cond_b
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 683186
    if-eqz v0, :cond_c

    .line 683187
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683188
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 683189
    :cond_c
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 683190
    if-eqz v0, :cond_d

    .line 683191
    const-string v0, "local_removed_items"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683192
    invoke-virtual {p0, p1, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 683193
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 683194
    return-void
.end method
