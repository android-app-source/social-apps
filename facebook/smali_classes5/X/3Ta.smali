.class public final LX/3Ta;
.super LX/2Ip;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V
    .locals 0

    .prologue
    .line 584409
    iput-object p1, p0, LX/3Ta;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-direct {p0}, LX/2Ip;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 7

    .prologue
    .line 584372
    check-cast p1, LX/2f2;

    .line 584373
    if-nez p1, :cond_0

    .line 584374
    :goto_0
    return-void

    .line 584375
    :cond_0
    iget-object v0, p0, LX/3Ta;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v0, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    iget-wide v2, p1, LX/2f2;->a:J

    .line 584376
    iget-object v1, v0, LX/3Te;->f:LX/3UE;

    invoke-virtual {v1, v2, v3}, LX/3UE;->b(J)Z

    move-result v1

    move v0, v1

    .line 584377
    if-eqz v0, :cond_2

    .line 584378
    iget-wide v0, p1, LX/2f2;->a:J

    iget-object v2, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 584379
    iget-object v3, p0, LX/3Ta;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v3, v3, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    .line 584380
    iget-object v4, v3, LX/3Te;->f:LX/3UE;

    const/4 v6, 0x0

    .line 584381
    iget-object v5, v4, LX/3UE;->k:Ljava/util/Set;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    move v5, v6

    .line 584382
    :goto_1
    move v4, v5

    .line 584383
    move v3, v4

    .line 584384
    const/4 v4, 0x0

    .line 584385
    sget-object v5, LX/2nZ;->c:[I

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 584386
    :cond_1
    :goto_2
    move-object v3, v4

    .line 584387
    if-eqz v3, :cond_4

    .line 584388
    iget-object v4, p0, LX/3Ta;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v4, v4, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v3, v6}, LX/3Te;->a(Ljava/lang/String;LX/2lu;Z)V

    .line 584389
    :goto_3
    goto :goto_0

    .line 584390
    :cond_2
    iget-object v0, p0, LX/3Ta;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v0, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    iget-wide v2, p1, LX/2f2;->a:J

    iget-object v1, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 584391
    iget-object v4, v0, LX/3Te;->g:LX/3UK;

    .line 584392
    iget-object v5, v4, LX/3UK;->g:Ljava/util/Set;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 584393
    :cond_3
    :goto_4
    goto :goto_0

    .line 584394
    :cond_4
    iget-object v3, p0, LX/3Ta;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-static {v3}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->E(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    goto :goto_3

    .line 584395
    :cond_5
    invoke-static {v4, v0, v1}, LX/3UE;->d(LX/3UE;J)I

    move-result v5

    .line 584396
    const/4 v3, -0x1

    if-eq v5, v3, :cond_6

    iget-object v3, v4, LX/3UE;->j:Ljava/util/List;

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/friends/model/FriendRequest;

    invoke-virtual {v5}, Lcom/facebook/friends/model/FriendRequest;->k()Z

    move-result v5

    if-eqz v5, :cond_6

    const/4 v5, 0x1

    goto :goto_1

    :cond_6
    move v5, v6

    goto :goto_1

    .line 584397
    :pswitch_0
    if-nez v3, :cond_1

    sget-object v4, LX/2lu;->ACCEPTED:LX/2lu;

    goto :goto_2

    .line 584398
    :pswitch_1
    if-nez v3, :cond_1

    sget-object v4, LX/2lu;->NEEDS_RESPONSE:LX/2lu;

    goto :goto_2

    .line 584399
    :pswitch_2
    if-eqz v3, :cond_1

    sget-object v4, LX/2lu;->ACCEPTED:LX/2lu;

    goto :goto_2

    .line 584400
    :pswitch_3
    sget-object v4, LX/2lu;->REJECTED:LX/2lu;

    goto :goto_2

    .line 584401
    :cond_7
    invoke-static {v4, v2, v3}, LX/3UK;->c(LX/3UK;J)I

    move-result v5

    .line 584402
    const/4 v6, -0x1

    if-eq v5, v6, :cond_3

    .line 584403
    iget-object v6, v4, LX/3UK;->f:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/friends/model/PersonYouMayKnow;

    .line 584404
    invoke-virtual {v5}, Lcom/facebook/friends/model/PersonYouMayKnow;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v6

    .line 584405
    invoke-virtual {v6, v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 584406
    iput-object v6, v5, Lcom/facebook/friends/model/PersonYouMayKnow;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 584407
    invoke-virtual {v5, v1}, Lcom/facebook/friends/model/PersonYouMayKnow;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 584408
    iget-object v5, v4, LX/3UK;->e:LX/3Tg;

    invoke-interface {v5}, LX/3Tg;->e()V

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
