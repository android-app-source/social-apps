.class public LX/4S3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 711476
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 711413
    const/4 v13, 0x0

    .line 711414
    const/4 v12, 0x0

    .line 711415
    const/4 v11, 0x0

    .line 711416
    const/4 v10, 0x0

    .line 711417
    const/4 v9, 0x0

    .line 711418
    const/4 v8, 0x0

    .line 711419
    const/4 v7, 0x0

    .line 711420
    const/4 v6, 0x0

    .line 711421
    const/4 v5, 0x0

    .line 711422
    const/4 v4, 0x0

    .line 711423
    const/4 v3, 0x0

    .line 711424
    const/4 v2, 0x0

    .line 711425
    const/4 v1, 0x0

    .line 711426
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_1

    .line 711427
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 711428
    const/4 v1, 0x0

    .line 711429
    :goto_0
    return v1

    .line 711430
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 711431
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v14, v15, :cond_e

    .line 711432
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v14

    .line 711433
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 711434
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_1

    if-eqz v14, :cond_1

    .line 711435
    const-string v15, "attachment"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 711436
    invoke-static/range {p0 .. p1}, LX/2as;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 711437
    :cond_2
    const-string v15, "branding_image"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 711438
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 711439
    :cond_3
    const-string v15, "content"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 711440
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 711441
    :cond_4
    const-string v15, "dismiss_action"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 711442
    invoke-static/range {p0 .. p1}, LX/4S1;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 711443
    :cond_5
    const-string v15, "footer"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 711444
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 711445
    :cond_6
    const-string v15, "image"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 711446
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 711447
    :cond_7
    const-string v15, "largeImage"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 711448
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 711449
    :cond_8
    const-string v15, "primary_action"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 711450
    invoke-static/range {p0 .. p1}, LX/4S1;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 711451
    :cond_9
    const-string v15, "secondary_action"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 711452
    invoke-static/range {p0 .. p1}, LX/4S1;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 711453
    :cond_a
    const-string v15, "social_context"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 711454
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 711455
    :cond_b
    const-string v15, "title"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_c

    .line 711456
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 711457
    :cond_c
    const-string v15, "social_context_profiles"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_d

    .line 711458
    invoke-static/range {p0 .. p1}, LX/2aw;->b(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 711459
    :cond_d
    const-string v15, "social_context_counter"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 711460
    invoke-static/range {p0 .. p1}, LX/4S2;->a(LX/15w;LX/186;)I

    move-result v1

    goto/16 :goto_1

    .line 711461
    :cond_e
    const/16 v14, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 711462
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 711463
    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 711464
    const/4 v12, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 711465
    const/4 v11, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 711466
    const/4 v10, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 711467
    const/4 v9, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 711468
    const/4 v8, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 711469
    const/4 v7, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 711470
    const/16 v6, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 711471
    const/16 v5, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 711472
    const/16 v4, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 711473
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 711474
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 711475
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 711358
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 711359
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 711360
    if-eqz v0, :cond_0

    .line 711361
    const-string v1, "attachment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711362
    invoke-static {p0, v0, p2, p3}, LX/2as;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 711363
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 711364
    if-eqz v0, :cond_1

    .line 711365
    const-string v1, "branding_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711366
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 711367
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 711368
    if-eqz v0, :cond_2

    .line 711369
    const-string v1, "content"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711370
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 711371
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 711372
    if-eqz v0, :cond_3

    .line 711373
    const-string v1, "dismiss_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711374
    invoke-static {p0, v0, p2, p3}, LX/4S1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 711375
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 711376
    if-eqz v0, :cond_4

    .line 711377
    const-string v1, "footer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711378
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 711379
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 711380
    if-eqz v0, :cond_5

    .line 711381
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711382
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 711383
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 711384
    if-eqz v0, :cond_6

    .line 711385
    const-string v1, "largeImage"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711386
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 711387
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 711388
    if-eqz v0, :cond_7

    .line 711389
    const-string v1, "primary_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711390
    invoke-static {p0, v0, p2, p3}, LX/4S1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 711391
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 711392
    if-eqz v0, :cond_8

    .line 711393
    const-string v1, "secondary_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711394
    invoke-static {p0, v0, p2, p3}, LX/4S1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 711395
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 711396
    if-eqz v0, :cond_9

    .line 711397
    const-string v1, "social_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711398
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 711399
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 711400
    if-eqz v0, :cond_a

    .line 711401
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711402
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 711403
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 711404
    if-eqz v0, :cond_b

    .line 711405
    const-string v1, "social_context_profiles"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711406
    invoke-static {p0, v0, p2, p3}, LX/2aw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 711407
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 711408
    if-eqz v0, :cond_c

    .line 711409
    const-string v1, "social_context_counter"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711410
    invoke-static {p0, v0, p2, p3}, LX/4S2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 711411
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 711412
    return-void
.end method
