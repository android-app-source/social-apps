.class public final LX/3v7;
.super LX/3uu;
.source ""

# interfaces
.implements LX/3rk;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3uu",
        "<",
        "Landroid/view/MenuItem$OnActionExpandListener;",
        ">;",
        "LX/3rk;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3v9;


# direct methods
.method public constructor <init>(LX/3v9;Landroid/view/MenuItem$OnActionExpandListener;)V
    .locals 0

    .prologue
    .line 650683
    iput-object p1, p0, LX/3v7;->a:LX/3v9;

    .line 650684
    invoke-direct {p0, p2}, LX/3uu;-><init>(Ljava/lang/Object;)V

    .line 650685
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 650686
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, Landroid/view/MenuItem$OnActionExpandListener;

    iget-object v1, p0, LX/3v7;->a:LX/3v9;

    invoke-virtual {v1, p1}, LX/3uv;->a(Landroid/view/MenuItem;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem$OnActionExpandListener;->onMenuItemActionExpand(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 650687
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, Landroid/view/MenuItem$OnActionExpandListener;

    iget-object v1, p0, LX/3v7;->a:LX/3v9;

    invoke-virtual {v1, p1}, LX/3uv;->a(Landroid/view/MenuItem;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem$OnActionExpandListener;->onMenuItemActionCollapse(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method
