.class public final LX/4be;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2K8;


# direct methods
.method public constructor <init>(LX/2K8;)V
    .locals 0

    .prologue
    .line 794014
    iput-object p1, p0, LX/4be;->a:LX/2K8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 794017
    iget-object v0, p0, LX/4be;->a:LX/2K8;

    iget-object v0, v0, LX/2K8;->a:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Fallback stack exception"

    invoke-direct {v1, v2, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 794018
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 794015
    iget-object v0, p0, LX/4be;->a:LX/2K8;

    iget-object v0, v0, LX/2K8;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, 0x20468a78

    invoke-static {v0, p1, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 794016
    return-void
.end method
