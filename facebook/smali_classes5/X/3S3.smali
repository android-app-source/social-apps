.class public LX/3S3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/3S3;


# instance fields
.field public final a:LX/0Uh;

.field public final b:LX/23k;

.field private final c:LX/14x;

.field public final d:LX/23j;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:J

.field public volatile g:Z

.field public volatile h:J


# direct methods
.method public constructor <init>(LX/0Uh;LX/23k;LX/14x;LX/23j;LX/0Or;)V
    .locals 2
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/23k;",
            "LX/14x;",
            "LX/23j;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 581763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 581764
    iput-object p1, p0, LX/3S3;->a:LX/0Uh;

    .line 581765
    iput-object p2, p0, LX/3S3;->b:LX/23k;

    .line 581766
    iput-object p3, p0, LX/3S3;->c:LX/14x;

    .line 581767
    iput-object p5, p0, LX/3S3;->e:LX/0Or;

    .line 581768
    iput-object p4, p0, LX/3S3;->d:LX/23j;

    .line 581769
    iget-object v0, p0, LX/3S3;->c:LX/14x;

    invoke-virtual {v0}, LX/14x;->e()Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 581770
    if-nez v0, :cond_0

    .line 581771
    const-string v0, "MessengerLiteDedupeManager"

    const-string v1, "Could not get messenger package info"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 581772
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LX/3S3;->f:J

    .line 581773
    :goto_0
    return-void

    .line 581774
    :cond_0
    iget-wide v0, v0, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    iput-wide v0, p0, LX/3S3;->f:J

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/3S3;
    .locals 9

    .prologue
    .line 581775
    sget-object v0, LX/3S3;->i:LX/3S3;

    if-nez v0, :cond_1

    .line 581776
    const-class v1, LX/3S3;

    monitor-enter v1

    .line 581777
    :try_start_0
    sget-object v0, LX/3S3;->i:LX/3S3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 581778
    if-eqz v2, :cond_0

    .line 581779
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 581780
    new-instance v3, LX/3S3;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/23k;->a(LX/0QB;)LX/23k;

    move-result-object v5

    check-cast v5, LX/23k;

    invoke-static {v0}, LX/14x;->a(LX/0QB;)LX/14x;

    move-result-object v6

    check-cast v6, LX/14x;

    invoke-static {v0}, LX/23j;->a(LX/0QB;)LX/23j;

    move-result-object v7

    check-cast v7, LX/23j;

    const/16 v8, 0x15e7

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/3S3;-><init>(LX/0Uh;LX/23k;LX/14x;LX/23j;LX/0Or;)V

    .line 581781
    move-object v0, v3

    .line 581782
    sput-object v0, LX/3S3;->i:LX/3S3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 581783
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 581784
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 581785
    :cond_1
    sget-object v0, LX/3S3;->i:LX/3S3;

    return-object v0

    .line 581786
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 581787
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
