.class public final LX/3dN;
.super LX/0ur;
.source ""


# instance fields
.field public b:I

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 617118
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 617119
    instance-of v0, p0, LX/3dN;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 617120
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dN;
    .locals 2

    .prologue
    .line 617121
    new-instance v0, LX/3dN;

    invoke-direct {v0}, LX/3dN;-><init>()V

    .line 617122
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 617123
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->a()I

    move-result v1

    iput v1, v0, LX/3dN;->b:I

    .line 617124
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->j()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/3dN;->c:LX/0Px;

    .line 617125
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    iput-object v1, v0, LX/3dN;->d:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 617126
    invoke-static {v0, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 617127
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;
    .locals 2

    .prologue
    .line 617128
    new-instance v0, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;-><init>(LX/3dN;)V

    .line 617129
    return-object v0
.end method
