.class public final LX/47C;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 671991
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 671992
    sput-object v0, LX/47C;->a:LX/0Px;

    return-void
.end method

.method public static a(Ljava/lang/String;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 671984
    if-nez p0, :cond_0

    .line 671985
    sget-object v0, LX/47C;->a:LX/0Px;

    .line 671986
    :goto_0
    return-object v0

    .line 671987
    :cond_0
    const-string v0, "[]"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 671988
    sget-object v0, LX/47C;->a:LX/0Px;

    goto :goto_0

    .line 671989
    :cond_1
    const/16 v0, 0x2c

    invoke-static {v0}, LX/2Cb;->on(C)LX/2Cb;

    move-result-object v0

    invoke-virtual {v0}, LX/2Cb;->trimResults()LX/2Cb;

    move-result-object v0

    invoke-virtual {v0}, LX/2Cb;->omitEmptyStrings()LX/2Cb;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/2Cb;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    .line 671990
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 671978
    if-nez p0, :cond_0

    .line 671979
    const-string v0, "[]"

    .line 671980
    :goto_0
    return-object v0

    .line 671981
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 671982
    const-string v0, "[]"

    goto :goto_0

    .line 671983
    :cond_1
    const/16 v0, 0x2c

    invoke-static {v0}, LX/0PO;->on(C)LX/0PO;

    move-result-object v0

    invoke-virtual {v0}, LX/0PO;->skipNulls()LX/0PO;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
