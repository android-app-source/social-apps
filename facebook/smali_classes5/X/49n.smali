.class public final LX/49n;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 674929
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 674930
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 674931
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 674932
    const-wide/16 v7, 0x0

    const/4 v9, 0x1

    const/4 v4, 0x0

    .line 674933
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v5, :cond_6

    .line 674934
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 674935
    :goto_1
    move v1, v4

    .line 674936
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 674937
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 674938
    :cond_1
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_4

    .line 674939
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 674940
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 674941
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 674942
    const-string v12, "expiration"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 674943
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v5

    move v3, v9

    goto :goto_2

    .line 674944
    :cond_2
    const-string v12, "photo_regex"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 674945
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_2

    .line 674946
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 674947
    :cond_4
    const/4 v11, 0x2

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 674948
    if-eqz v3, :cond_5

    move-object v3, p1

    .line 674949
    invoke-virtual/range {v3 .. v8}, LX/186;->a(IJJ)V

    .line 674950
    :cond_5
    invoke-virtual {p1, v9, v10}, LX/186;->b(II)V

    .line 674951
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_1

    :cond_6
    move v3, v4

    move v10, v4

    move-wide v5, v7

    goto :goto_2
.end method
