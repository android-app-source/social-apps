.class public final LX/4bx;
.super Lorg/apache/http/impl/client/DefaultHttpClient;
.source ""


# instance fields
.field public final synthetic a:LX/1Gl;

.field public final synthetic b:LX/4by;


# direct methods
.method public constructor <init>(LX/4by;Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;LX/1Gl;)V
    .locals 0

    .prologue
    .line 794345
    iput-object p1, p0, LX/4bx;->b:LX/4by;

    iput-object p4, p0, LX/4bx;->a:LX/1Gl;

    invoke-direct {p0, p2, p3}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    return-void
.end method


# virtual methods
.method public final createConnectionReuseStrategy()Lorg/apache/http/ConnectionReuseStrategy;
    .locals 1

    .prologue
    .line 794344
    new-instance v0, LX/4bt;

    invoke-direct {v0}, LX/4bt;-><init>()V

    return-object v0
.end method

.method public final createHttpContext()Lorg/apache/http/protocol/HttpContext;
    .locals 3

    .prologue
    .line 794329
    new-instance v0, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v0}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    .line 794330
    const-string v1, "http.authscheme-registry"

    invoke-virtual {p0}, LX/4bx;->getAuthSchemes()Lorg/apache/http/auth/AuthSchemeRegistry;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/apache/http/protocol/HttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    .line 794331
    const-string v1, "http.cookiespec-registry"

    invoke-virtual {p0}, LX/4bx;->getCookieSpecs()Lorg/apache/http/cookie/CookieSpecRegistry;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/apache/http/protocol/HttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    .line 794332
    const-string v1, "http.auth.credentials-provider"

    invoke-virtual {p0}, LX/4bx;->getCredentialsProvider()Lorg/apache/http/client/CredentialsProvider;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/apache/http/protocol/HttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    .line 794333
    const-string v1, "http.cookie-store"

    iget-object v2, p0, LX/4bx;->b:LX/4by;

    iget-object v2, v2, LX/4by;->b:LX/4bu;

    invoke-interface {v0, v1, v2}, Lorg/apache/http/protocol/HttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    .line 794334
    return-object v0
.end method

.method public final createHttpProcessor()Lorg/apache/http/protocol/BasicHttpProcessor;
    .locals 3

    .prologue
    .line 794335
    invoke-super {p0}, Lorg/apache/http/impl/client/DefaultHttpClient;->createHttpProcessor()Lorg/apache/http/protocol/BasicHttpProcessor;

    move-result-object v0

    .line 794336
    new-instance v1, LX/4bn;

    invoke-direct {v1}, LX/4bn;-><init>()V

    move-object v1, v1

    .line 794337
    invoke-virtual {v0, v1}, Lorg/apache/http/protocol/BasicHttpProcessor;->addRequestInterceptor(Lorg/apache/http/HttpRequestInterceptor;)V

    .line 794338
    new-instance v1, LX/4bo;

    invoke-direct {v1}, LX/4bo;-><init>()V

    move-object v1, v1

    .line 794339
    invoke-virtual {v0, v1}, Lorg/apache/http/protocol/BasicHttpProcessor;->addResponseInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 794340
    new-instance v1, LX/4bl;

    iget-object v2, p0, LX/4bx;->a:LX/1Gl;

    invoke-direct {v1, v2}, LX/4bl;-><init>(LX/1Gl;)V

    .line 794341
    invoke-virtual {v0, v1}, Lorg/apache/http/protocol/BasicHttpProcessor;->addRequestInterceptor(Lorg/apache/http/HttpRequestInterceptor;)V

    .line 794342
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/protocol/BasicHttpProcessor;->addResponseInterceptor(Lorg/apache/http/HttpResponseInterceptor;I)V

    .line 794343
    return-object v0
.end method
