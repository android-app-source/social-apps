.class public final LX/4y6;
.super LX/4y5;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Enum",
        "<TK;>;V:",
        "Ljava/lang/Object;",
        ">",
        "LX/4y5",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final transient a:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/EnumMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumMap",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 821595
    invoke-direct {p0}, LX/4y5;-><init>()V

    .line 821596
    iput-object p1, p0, LX/4y6;->a:Ljava/util/EnumMap;

    .line 821597
    invoke-virtual {p1}, Ljava/util/EnumMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 821598
    return-void

    .line 821599
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0Rc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 821592
    iget-object v0, p0, LX/4y6;->a:Ljava/util/EnumMap;

    invoke-virtual {v0}, Ljava/util/EnumMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 821593
    new-instance p0, LX/4zo;

    invoke-direct {p0, v0}, LX/4zo;-><init>(Ljava/util/Iterator;)V

    move-object v0, p0

    .line 821594
    return-object v0
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 821591
    iget-object v0, p0, LX/4y6;->a:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 821585
    if-ne p1, p0, :cond_0

    .line 821586
    const/4 v0, 0x1

    .line 821587
    :goto_0
    return v0

    .line 821588
    :cond_0
    instance-of v0, p1, LX/4y6;

    if-eqz v0, :cond_1

    .line 821589
    check-cast p1, LX/4y6;

    iget-object p1, p1, LX/4y6;->a:Ljava/util/EnumMap;

    .line 821590
    :cond_1
    iget-object v0, p0, LX/4y6;->a:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 821600
    iget-object v0, p0, LX/4y6;->a:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final isPartialView()Z
    .locals 1

    .prologue
    .line 821584
    const/4 v0, 0x0

    return v0
.end method

.method public final keyIterator()LX/0Rc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 821583
    iget-object v0, p0, LX/4y6;->a:Ljava/util/EnumMap;

    invoke-virtual {v0}, Ljava/util/EnumMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0RZ;->a(Ljava/util/Iterator;)LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 821582
    iget-object v0, p0, LX/4y6;->a:Ljava/util/EnumMap;

    invoke-virtual {v0}, Ljava/util/EnumMap;->size()I

    move-result v0

    return v0
.end method

.method public final writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 821581
    new-instance v0, LX/4y4;

    iget-object v1, p0, LX/4y6;->a:Ljava/util/EnumMap;

    invoke-direct {v0, v1}, LX/4y4;-><init>(Ljava/util/EnumMap;)V

    return-object v0
.end method
