.class public LX/3zd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3zd;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/analytics/eventlisteners/AnalyticsNavigationListener;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/analytics/eventlisteners/AnalyticsNavigationListener;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 662547
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 662548
    iput-object p1, p0, LX/3zd;->a:LX/0Ot;

    .line 662549
    return-void
.end method

.method public static a(LX/0QB;)LX/3zd;
    .locals 5

    .prologue
    .line 662550
    sget-object v0, LX/3zd;->b:LX/3zd;

    if-nez v0, :cond_1

    .line 662551
    const-class v1, LX/3zd;

    monitor-enter v1

    .line 662552
    :try_start_0
    sget-object v0, LX/3zd;->b:LX/3zd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 662553
    if-eqz v2, :cond_0

    .line 662554
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 662555
    new-instance v3, LX/3zd;

    .line 662556
    new-instance v4, LX/3ze;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-direct {v4, p0}, LX/3ze;-><init>(LX/0QB;)V

    move-object v4, v4

    .line 662557
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-static {v4, p0}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v4

    move-object v4, v4

    .line 662558
    invoke-direct {v3, v4}, LX/3zd;-><init>(LX/0Ot;)V

    .line 662559
    move-object v0, v3

    .line 662560
    sput-object v0, LX/3zd;->b:LX/3zd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 662561
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 662562
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 662563
    :cond_1
    sget-object v0, LX/3zd;->b:LX/3zd;

    return-object v0

    .line 662564
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 662565
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
