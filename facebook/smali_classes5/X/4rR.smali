.class public final LX/4rR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0lJ;

.field public final b:LX/0lb;

.field public final c:LX/4pS;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4pS",
            "<*>;"
        }
    .end annotation
.end field

.field public final d:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Z


# direct methods
.method private constructor <init>(LX/0lJ;LX/0lb;LX/4pS;Lcom/fasterxml/jackson/databind/JsonSerializer;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "LX/0lb;",
            "LX/4pS",
            "<*>;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;Z)V"
        }
    .end annotation

    .prologue
    .line 815834
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 815835
    iput-object p1, p0, LX/4rR;->a:LX/0lJ;

    .line 815836
    iput-object p2, p0, LX/4rR;->b:LX/0lb;

    .line 815837
    iput-object p3, p0, LX/4rR;->c:LX/4pS;

    .line 815838
    iput-object p4, p0, LX/4rR;->d:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 815839
    iput-boolean p5, p0, LX/4rR;->e:Z

    .line 815840
    return-void
.end method

.method public static a(LX/0lJ;Ljava/lang/String;LX/4pS;Z)LX/4rR;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "Ljava/lang/String;",
            "LX/4pS",
            "<*>;Z)",
            "LX/4rR;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 815841
    if-nez p1, :cond_0

    move-object v2, v4

    .line 815842
    :goto_0
    new-instance v0, LX/4rR;

    move-object v1, p0

    move-object v3, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, LX/4rR;-><init>(LX/0lJ;LX/0lb;LX/4pS;Lcom/fasterxml/jackson/databind/JsonSerializer;Z)V

    return-object v0

    .line 815843
    :cond_0
    new-instance v2, LX/0lb;

    invoke-direct {v2, p1}, LX/0lb;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/fasterxml/jackson/databind/JsonSerializer;)LX/4rR;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;)",
            "LX/4rR;"
        }
    .end annotation

    .prologue
    .line 815844
    new-instance v0, LX/4rR;

    iget-object v1, p0, LX/4rR;->a:LX/0lJ;

    iget-object v2, p0, LX/4rR;->b:LX/0lb;

    iget-object v3, p0, LX/4rR;->c:LX/4pS;

    iget-boolean v5, p0, LX/4rR;->e:Z

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, LX/4rR;-><init>(LX/0lJ;LX/0lb;LX/4pS;Lcom/fasterxml/jackson/databind/JsonSerializer;Z)V

    return-object v0
.end method

.method public final a(Z)LX/4rR;
    .locals 6

    .prologue
    .line 815845
    iget-boolean v0, p0, LX/4rR;->e:Z

    if-ne p1, v0, :cond_0

    .line 815846
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/4rR;

    iget-object v1, p0, LX/4rR;->a:LX/0lJ;

    iget-object v2, p0, LX/4rR;->b:LX/0lb;

    iget-object v3, p0, LX/4rR;->c:LX/4pS;

    iget-object v4, p0, LX/4rR;->d:Lcom/fasterxml/jackson/databind/JsonSerializer;

    move v5, p1

    invoke-direct/range {v0 .. v5}, LX/4rR;-><init>(LX/0lJ;LX/0lb;LX/4pS;Lcom/fasterxml/jackson/databind/JsonSerializer;Z)V

    move-object p0, v0

    goto :goto_0
.end method
