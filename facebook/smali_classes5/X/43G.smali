.class public LX/43G;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:Z


# direct methods
.method public constructor <init>(III)V
    .locals 1

    .prologue
    .line 668517
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 668518
    iput p1, p0, LX/43G;->a:I

    .line 668519
    iput p2, p0, LX/43G;->b:I

    .line 668520
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/43G;->d:Z

    .line 668521
    iput p3, p0, LX/43G;->c:I

    .line 668522
    return-void
.end method

.method public constructor <init>(IIZI)V
    .locals 0

    .prologue
    .line 668523
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 668524
    iput p1, p0, LX/43G;->a:I

    .line 668525
    iput p2, p0, LX/43G;->b:I

    .line 668526
    iput-boolean p3, p0, LX/43G;->d:Z

    .line 668527
    iput p4, p0, LX/43G;->c:I

    .line 668528
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 668529
    iget v0, p0, LX/43G;->a:I

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 668530
    iget v0, p0, LX/43G;->b:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 668531
    iget v0, p0, LX/43G;->c:I

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 668532
    if-eqz p1, :cond_0

    instance-of v1, p1, LX/43G;

    if-nez v1, :cond_1

    .line 668533
    :cond_0
    :goto_0
    return v0

    .line 668534
    :cond_1
    check-cast p1, LX/43G;

    .line 668535
    iget v1, p0, LX/43G;->a:I

    .line 668536
    iget v2, p1, LX/43G;->a:I

    move v2, v2

    .line 668537
    if-ne v1, v2, :cond_0

    iget v1, p0, LX/43G;->b:I

    .line 668538
    iget v2, p1, LX/43G;->b:I

    move v2, v2

    .line 668539
    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, LX/43G;->d:Z

    .line 668540
    iget-boolean v2, p1, LX/43G;->d:Z

    move v2, v2

    .line 668541
    if-ne v1, v2, :cond_0

    iget v1, p0, LX/43G;->c:I

    .line 668542
    iget v2, p1, LX/43G;->c:I

    move v2, v2

    .line 668543
    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 668544
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, LX/43G;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/43G;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/43G;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
