.class public LX/3mK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/1LV;

.field public final b:LX/3mL;

.field public final c:LX/3mM;

.field public final d:LX/3mN;

.field public final e:LX/0ad;

.field public f:I


# direct methods
.method public constructor <init>(LX/3mL;LX/1LV;LX/3mM;LX/3mN;LX/0ad;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 636172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 636173
    const/4 v0, -0x2

    iput v0, p0, LX/3mK;->f:I

    .line 636174
    iput-object p1, p0, LX/3mK;->b:LX/3mL;

    .line 636175
    iput-object p2, p0, LX/3mK;->a:LX/1LV;

    .line 636176
    iput-object p3, p0, LX/3mK;->c:LX/3mM;

    .line 636177
    iput-object p4, p0, LX/3mK;->d:LX/3mN;

    .line 636178
    iput-object p5, p0, LX/3mK;->e:LX/0ad;

    .line 636179
    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;I)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;",
            ">;I)",
            "LX/0Px",
            "<",
            "LX/3mR;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 636191
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 636192
    check-cast v0, Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;

    .line 636193
    invoke-interface {v0}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->p()Ljava/util/List;

    move-result-object v5

    .line 636194
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v0, v4

    .line 636195
    :goto_0
    return-object v0

    .line 636196
    :cond_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 636197
    invoke-static {v0}, LX/3mQ;->b(Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;)Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;

    move-result-object v1

    .line 636198
    invoke-static {v0}, LX/3mQ;->a(Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;)Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    .line 636199
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->k()Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    move-result-object v1

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;->COLLEGE_COMMUNITY_SUGGESTED_GROUPS_COVER:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    if-ne v1, v7, :cond_1

    if-eqz v2, :cond_1

    .line 636200
    new-instance v1, LX/3mR;

    const/4 v2, 0x3

    invoke-direct {v1, v0, v4, v4, v2}, LX/3mR;-><init>(Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;LX/254;Lcom/facebook/graphql/model/GraphQLProfile;I)V

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    move v2, v3

    .line 636201
    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_3

    .line 636202
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/254;

    .line 636203
    if-eqz v1, :cond_2

    .line 636204
    invoke-static {v1}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v7

    .line 636205
    if-eqz v7, :cond_2

    .line 636206
    new-instance v8, LX/3mR;

    invoke-direct {v8, v0, v1, v7, v3}, LX/3mR;-><init>(Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;LX/254;Lcom/facebook/graphql/model/GraphQLProfile;I)V

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 636207
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 636208
    :cond_3
    iget-object v1, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 636209
    check-cast v1, Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;

    invoke-static {v1}, LX/3mQ;->a(Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;)Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v1

    .line 636210
    if-eqz v1, :cond_4

    .line 636211
    new-instance v1, LX/3mR;

    const/4 v2, 0x2

    invoke-direct {v1, v0, v4, v4, v2}, LX/3mR;-><init>(Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;LX/254;Lcom/facebook/graphql/model/GraphQLProfile;I)V

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 636212
    invoke-static {v6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 636213
    :cond_4
    const/4 v3, 0x0

    .line 636214
    if-ltz p1, :cond_6

    .line 636215
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    if-lt p1, v1, :cond_5

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result p1

    .line 636216
    :cond_5
    new-instance v1, LX/3mR;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v3, v3, v2}, LX/3mR;-><init>(Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;LX/254;Lcom/facebook/graphql/model/GraphQLProfile;I)V

    invoke-interface {v6, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 636217
    :cond_6
    invoke-static {v6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 636218
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/3mK;
    .locals 9

    .prologue
    .line 636180
    const-class v1, LX/3mK;

    monitor-enter v1

    .line 636181
    :try_start_0
    sget-object v0, LX/3mK;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 636182
    sput-object v2, LX/3mK;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 636183
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 636184
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 636185
    new-instance v3, LX/3mK;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v4

    check-cast v4, LX/3mL;

    invoke-static {v0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v5

    check-cast v5, LX/1LV;

    const-class v6, LX/3mM;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/3mM;

    invoke-static {v0}, LX/3mN;->a(LX/0QB;)LX/3mN;

    move-result-object v7

    check-cast v7, LX/3mN;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, LX/3mK;-><init>(LX/3mL;LX/1LV;LX/3mM;LX/3mN;LX/0ad;)V

    .line 636186
    move-object v0, v3

    .line 636187
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 636188
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3mK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 636189
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 636190
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
