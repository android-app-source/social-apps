.class public LX/4gs;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:LX/0oE;

.field public final g:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;IIILX/0oE;Z)V
    .locals 0

    .prologue
    .line 800231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 800232
    iput-object p1, p0, LX/4gs;->a:Ljava/lang/String;

    .line 800233
    iput-object p2, p0, LX/4gs;->b:Ljava/lang/String;

    .line 800234
    iput p3, p0, LX/4gs;->c:I

    .line 800235
    iput p4, p0, LX/4gs;->d:I

    .line 800236
    iput p5, p0, LX/4gs;->e:I

    .line 800237
    iput-object p6, p0, LX/4gs;->f:LX/0oE;

    .line 800238
    iput-boolean p7, p0, LX/4gs;->g:Z

    .line 800239
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/util/List;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/4gs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 800240
    new-instance v11, Ljava/util/LinkedList;

    invoke-direct {v11}, Ljava/util/LinkedList;-><init>()V

    .line 800241
    const/4 v8, 0x1

    .line 800242
    const/4 v9, 0x0

    .line 800243
    const-string v0, "\\r?\\n"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    array-length v13, v12

    const/4 v0, 0x0

    move v10, v0

    :goto_0
    if-ge v10, v13, :cond_5

    aget-object v0, v12, v10

    .line 800244
    if-eqz v8, :cond_0

    .line 800245
    const/4 v0, 0x0

    .line 800246
    :goto_1
    add-int/lit8 v1, v10, 0x1

    move v10, v1

    move v8, v0

    goto :goto_0

    .line 800247
    :cond_0
    const-string v1, "END"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 800248
    const/4 v0, 0x1

    .line 800249
    :goto_2
    if-nez v0, :cond_4

    .line 800250
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid paramsMapContent: no END marker found"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 800251
    :cond_1
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 800252
    array-length v1, v7

    const/4 v2, 0x7

    if-eq v1, v2, :cond_2

    .line 800253
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ParamsMap line entry should contain 7 tokens per line: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 800254
    :cond_2
    new-instance v0, LX/4gs;

    const/4 v1, 0x0

    aget-object v1, v7, v1

    const/4 v2, 0x1

    aget-object v2, v7, v2

    const/4 v3, 0x2

    aget-object v3, v7, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x3

    aget-object v4, v7, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x4

    aget-object v5, v7, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x5

    aget-object v6, v7, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, LX/0oE;->valueOf(I)LX/0oE;

    move-result-object v6

    const/4 v14, 0x6

    aget-object v7, v7, v14

    const-string v14, "1"

    invoke-virtual {v7, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v7, 0x1

    :goto_3
    invoke-direct/range {v0 .. v7}, LX/4gs;-><init>(Ljava/lang/String;Ljava/lang/String;IIILX/0oE;Z)V

    invoke-virtual {v11, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    move v0, v8

    goto :goto_1

    :cond_3
    const/4 v7, 0x0

    goto :goto_3

    .line 800255
    :cond_4
    invoke-static {v11}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_5
    move v0, v9

    goto :goto_2
.end method


# virtual methods
.method public final a()J
    .locals 8

    .prologue
    .line 800256
    iget-object v0, p0, LX/4gs;->f:LX/0oE;

    iget v1, p0, LX/4gs;->d:I

    iget v2, p0, LX/4gs;->e:I

    .line 800257
    invoke-virtual {v0}, LX/0oE;->getValue()I

    move-result v3

    int-to-long v3, v3

    const/16 v5, 0x30

    shl-long/2addr v3, v5

    .line 800258
    int-to-long v5, v1

    const/16 v7, 0x18

    shl-long/2addr v5, v7

    .line 800259
    or-long/2addr v3, v5

    int-to-long v5, v2

    or-long/2addr v3, v5

    move-wide v0, v3

    .line 800260
    return-wide v0
.end method
