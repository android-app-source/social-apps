.class public LX/3ij;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field private b:I

.field private c:I

.field public d:[F

.field public e:[F

.field public f:[F

.field public g:[F

.field public h:[I

.field public i:[I

.field public j:[I

.field public k:I

.field private l:Landroid/view/VelocityTracker;

.field private m:F

.field private n:F

.field public o:I

.field private p:I

.field public q:Landroid/widget/Scroller;

.field public r:I

.field public final s:LX/3il;

.field public t:Landroid/view/View;

.field private u:Z

.field public final v:Landroid/view/ViewGroup;

.field public final w:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;LX/3il;)V
    .locals 3

    .prologue
    .line 630499
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 630500
    const/4 v0, -0x1

    iput v0, p0, LX/3ij;->c:I

    .line 630501
    new-instance v0, Lcom/facebook/widget/touch/ViewDragHelper$1;

    invoke-direct {v0, p0}, Lcom/facebook/widget/touch/ViewDragHelper$1;-><init>(LX/3ij;)V

    iput-object v0, p0, LX/3ij;->w:Ljava/lang/Runnable;

    .line 630502
    if-nez p2, :cond_0

    .line 630503
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Parent view may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 630504
    :cond_0
    if-nez p3, :cond_1

    .line 630505
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Callback may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 630506
    :cond_1
    iput-object p2, p0, LX/3ij;->v:Landroid/view/ViewGroup;

    .line 630507
    iput-object p3, p0, LX/3ij;->s:LX/3il;

    .line 630508
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 630509
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 630510
    const/high16 v2, 0x41a00000    # 20.0f

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, LX/3ij;->o:I

    .line 630511
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, LX/3ij;->b:I

    .line 630512
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, LX/3ij;->m:F

    .line 630513
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LX/3ij;->n:F

    .line 630514
    new-instance v0, Landroid/widget/Scroller;

    invoke-direct {v0, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/3ij;->q:Landroid/widget/Scroller;

    .line 630515
    return-void
.end method

.method private static a(FFF)F
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 630395
    invoke-static {p0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 630396
    cmpg-float v2, v1, p1

    if-gez v2, :cond_1

    move p2, v0

    .line 630397
    :cond_0
    :goto_0
    return p2

    .line 630398
    :cond_1
    cmpl-float v1, v1, p2

    if-lez v1, :cond_2

    cmpl-float v0, p0, v0

    if-gtz v0, :cond_0

    neg-float p2, p2

    goto :goto_0

    :cond_2
    move p2, p0

    .line 630399
    goto :goto_0
.end method

.method private static a(LX/3ij;III)I
    .locals 8

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 630400
    if-nez p1, :cond_0

    .line 630401
    const/4 v0, 0x0

    .line 630402
    :goto_0
    return v0

    .line 630403
    :cond_0
    iget-object v0, p0, LX/3ij;->v:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    .line 630404
    div-int/lit8 v1, v0, 0x2

    .line 630405
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v2

    int-to-float v2, v2

    int-to-float v0, v0

    div-float v0, v2, v0

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 630406
    int-to-float v2, v1

    int-to-float v1, v1

    .line 630407
    const/high16 v4, 0x3f000000    # 0.5f

    sub-float v4, v0, v4

    .line 630408
    float-to-double v4, v4

    const-wide v6, 0x3fde28c7460698c7L    # 0.4712389167638204

    mul-double/2addr v4, v6

    double-to-float v4, v4

    .line 630409
    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v4, v4

    move v0, v4

    .line 630410
    mul-float/2addr v0, v1

    add-float/2addr v0, v2

    .line 630411
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 630412
    if-lez v1, :cond_1

    .line 630413
    const/high16 v2, 0x447a0000    # 1000.0f

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    .line 630414
    :goto_1
    const/16 v1, 0x258

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    .line 630415
    :cond_1
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    int-to-float v1, p3

    div-float/2addr v0, v1

    .line 630416
    add-float/2addr v0, v3

    const/high16 v1, 0x43800000    # 256.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_1
.end method

.method public static a(LX/3ij;Landroid/view/View;IIII)I
    .locals 8

    .prologue
    .line 630417
    iget v0, p0, LX/3ij;->n:F

    float-to-int v0, v0

    iget v1, p0, LX/3ij;->m:F

    float-to-int v1, v1

    invoke-static {p4, v0, v1}, LX/3ij;->b(III)I

    move-result v2

    .line 630418
    iget v0, p0, LX/3ij;->n:F

    float-to-int v0, v0

    iget v1, p0, LX/3ij;->m:F

    float-to-int v1, v1

    invoke-static {p5, v0, v1}, LX/3ij;->b(III)I

    move-result v3

    .line 630419
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 630420
    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v4

    .line 630421
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 630422
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v5

    .line 630423
    add-int v6, v1, v5

    .line 630424
    add-int v7, v0, v4

    .line 630425
    if-eqz v2, :cond_0

    int-to-float v0, v1

    int-to-float v1, v6

    div-float/2addr v0, v1

    move v1, v0

    .line 630426
    :goto_0
    if-eqz v3, :cond_1

    int-to-float v0, v5

    int-to-float v4, v6

    div-float/2addr v0, v4

    .line 630427
    :goto_1
    const/4 v4, 0x0

    move v4, v4

    .line 630428
    invoke-static {p0, p2, v2, v4}, LX/3ij;->a(LX/3ij;III)I

    move-result v2

    .line 630429
    iget-object v4, p0, LX/3ij;->s:LX/3il;

    invoke-virtual {v4}, LX/3il;->c()I

    move-result v4

    invoke-static {p0, p3, v3, v4}, LX/3ij;->a(LX/3ij;III)I

    move-result v3

    .line 630430
    int-to-float v2, v2

    mul-float/2addr v1, v2

    int-to-float v2, v3

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0

    .line 630431
    :cond_0
    int-to-float v0, v0

    int-to-float v1, v7

    div-float/2addr v0, v1

    move v1, v0

    goto :goto_0

    .line 630432
    :cond_1
    int-to-float v0, v4

    int-to-float v4, v7

    div-float/2addr v0, v4

    goto :goto_1
.end method

.method private a(FF)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 630433
    iput-boolean v3, p0, LX/3ij;->u:Z

    .line 630434
    iget-object v0, p0, LX/3ij;->s:LX/3il;

    iget-object v1, p0, LX/3ij;->t:Landroid/view/View;

    invoke-virtual {v0, v1, p2}, LX/3il;->a(Landroid/view/View;F)V

    .line 630435
    iput-boolean v2, p0, LX/3ij;->u:Z

    .line 630436
    iget v0, p0, LX/3ij;->a:I

    if-ne v0, v3, :cond_0

    .line 630437
    invoke-virtual {p0, v2}, LX/3ij;->b(I)V

    .line 630438
    :cond_0
    return-void
.end method

.method private static a(LX/3ij;FFI)V
    .locals 10

    .prologue
    .line 630439
    const/4 v9, 0x0

    .line 630440
    iget-object v0, p0, LX/3ij;->d:[F

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3ij;->d:[F

    array-length v0, v0

    if-gt v0, p3, :cond_2

    .line 630441
    :cond_0
    add-int/lit8 v0, p3, 0x1

    new-array v0, v0, [F

    .line 630442
    add-int/lit8 v1, p3, 0x1

    new-array v1, v1, [F

    .line 630443
    add-int/lit8 v2, p3, 0x1

    new-array v2, v2, [F

    .line 630444
    add-int/lit8 v3, p3, 0x1

    new-array v3, v3, [F

    .line 630445
    add-int/lit8 v4, p3, 0x1

    new-array v4, v4, [I

    .line 630446
    add-int/lit8 v5, p3, 0x1

    new-array v5, v5, [I

    .line 630447
    add-int/lit8 v6, p3, 0x1

    new-array v6, v6, [I

    .line 630448
    iget-object v7, p0, LX/3ij;->d:[F

    if-eqz v7, :cond_1

    .line 630449
    iget-object v7, p0, LX/3ij;->d:[F

    iget-object v8, p0, LX/3ij;->d:[F

    array-length v8, v8

    invoke-static {v7, v9, v0, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 630450
    iget-object v7, p0, LX/3ij;->e:[F

    iget-object v8, p0, LX/3ij;->e:[F

    array-length v8, v8

    invoke-static {v7, v9, v1, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 630451
    iget-object v7, p0, LX/3ij;->f:[F

    iget-object v8, p0, LX/3ij;->f:[F

    array-length v8, v8

    invoke-static {v7, v9, v2, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 630452
    iget-object v7, p0, LX/3ij;->g:[F

    iget-object v8, p0, LX/3ij;->g:[F

    array-length v8, v8

    invoke-static {v7, v9, v3, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 630453
    iget-object v7, p0, LX/3ij;->h:[I

    iget-object v8, p0, LX/3ij;->h:[I

    array-length v8, v8

    invoke-static {v7, v9, v4, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 630454
    iget-object v7, p0, LX/3ij;->i:[I

    iget-object v8, p0, LX/3ij;->i:[I

    array-length v8, v8

    invoke-static {v7, v9, v5, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 630455
    iget-object v7, p0, LX/3ij;->j:[I

    iget-object v8, p0, LX/3ij;->j:[I

    array-length v8, v8

    invoke-static {v7, v9, v6, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 630456
    :cond_1
    iput-object v0, p0, LX/3ij;->d:[F

    .line 630457
    iput-object v1, p0, LX/3ij;->e:[F

    .line 630458
    iput-object v2, p0, LX/3ij;->f:[F

    .line 630459
    iput-object v3, p0, LX/3ij;->g:[F

    .line 630460
    iput-object v4, p0, LX/3ij;->h:[I

    .line 630461
    iput-object v5, p0, LX/3ij;->i:[I

    .line 630462
    iput-object v6, p0, LX/3ij;->j:[I

    .line 630463
    :cond_2
    iget-object v0, p0, LX/3ij;->d:[F

    iget-object v1, p0, LX/3ij;->f:[F

    aput p1, v1, p3

    aput p1, v0, p3

    .line 630464
    iget-object v0, p0, LX/3ij;->e:[F

    iget-object v1, p0, LX/3ij;->g:[F

    aput p2, v1, p3

    aput p2, v0, p3

    .line 630465
    iget-object v0, p0, LX/3ij;->h:[I

    float-to-int v1, p1

    float-to-int v2, p2

    .line 630466
    const/4 v3, 0x0

    .line 630467
    iget-object v4, p0, LX/3ij;->v:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getLeft()I

    move-result v4

    iget v5, p0, LX/3ij;->o:I

    add-int/2addr v4, v5

    if-ge v1, v4, :cond_3

    const/4 v3, 0x1

    .line 630468
    :cond_3
    iget-object v4, p0, LX/3ij;->v:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getTop()I

    move-result v4

    iget v5, p0, LX/3ij;->o:I

    add-int/2addr v4, v5

    if-ge v2, v4, :cond_4

    or-int/lit8 v3, v3, 0x4

    .line 630469
    :cond_4
    iget-object v4, p0, LX/3ij;->v:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getRight()I

    move-result v4

    iget v5, p0, LX/3ij;->o:I

    sub-int/2addr v4, v5

    if-le v1, v4, :cond_5

    or-int/lit8 v3, v3, 0x2

    .line 630470
    :cond_5
    iget-object v4, p0, LX/3ij;->v:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getBottom()I

    move-result v4

    iget v5, p0, LX/3ij;->o:I

    sub-int/2addr v4, v5

    if-le v2, v4, :cond_6

    or-int/lit8 v3, v3, 0x8

    .line 630471
    :cond_6
    move v1, v3

    .line 630472
    aput v1, v0, p3

    .line 630473
    iget v0, p0, LX/3ij;->k:I

    const/4 v1, 0x1

    shl-int/2addr v1, p3

    or-int/2addr v0, v1

    iput v0, p0, LX/3ij;->k:I

    .line 630474
    return-void
.end method

.method private a(Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 630482
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, LX/3ij;->v:Landroid/view/ViewGroup;

    if-eq v0, v1, :cond_0

    .line 630483
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "captureChildView: parameter must be a descendant of the ViewDragHelper\'s tracked parent view ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/3ij;->v:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 630484
    :cond_0
    iput-object p1, p0, LX/3ij;->t:Landroid/view/View;

    .line 630485
    iput p2, p0, LX/3ij;->c:I

    .line 630486
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/3ij;->b(I)V

    .line 630487
    return-void
.end method

.method private a(FFII)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 630475
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 630476
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 630477
    iget-object v3, p0, LX/3ij;->h:[I

    aget v3, v3, p3

    and-int/2addr v3, p4

    if-ne v3, p4, :cond_0

    iget v3, p0, LX/3ij;->p:I

    and-int/2addr v3, p4

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/3ij;->j:[I

    aget v3, v3, p3

    and-int/2addr v3, p4

    if-eq v3, p4, :cond_0

    iget-object v3, p0, LX/3ij;->i:[I

    aget v3, v3, p3

    and-int/2addr v3, p4

    if-eq v3, p4, :cond_0

    iget v3, p0, LX/3ij;->b:I

    int-to-float v3, v3

    cmpg-float v3, v1, v3

    if-gtz v3, :cond_1

    iget v3, p0, LX/3ij;->b:I

    int-to-float v3, v3

    cmpg-float v3, v2, v3

    if-gtz v3, :cond_1

    .line 630478
    :cond_0
    :goto_0
    return v0

    .line 630479
    :cond_1
    const/high16 v3, 0x3f000000    # 0.5f

    mul-float/2addr v2, v3

    cmpg-float v2, v1, v2

    if-gez v2, :cond_2

    .line 630480
    goto :goto_1

    .line 630481
    :cond_2
    :goto_1
    iget-object v2, p0, LX/3ij;->i:[I

    aget v2, v2, p3

    and-int/2addr v2, p4

    if-nez v2, :cond_0

    iget v2, p0, LX/3ij;->b:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(LX/3ij;Landroid/view/View;FF)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 630516
    if-nez p1, :cond_1

    .line 630517
    :cond_0
    :goto_0
    return v2

    .line 630518
    :cond_1
    const/4 v0, 0x0

    move v0, v0

    .line 630519
    if-lez v0, :cond_2

    move v0, v1

    .line 630520
    :goto_1
    iget-object v3, p0, LX/3ij;->s:LX/3il;

    invoke-virtual {v3}, LX/3il;->c()I

    move-result v3

    if-lez v3, :cond_3

    move v3, v1

    .line 630521
    :goto_2
    if-eqz v0, :cond_4

    if-eqz v3, :cond_4

    .line 630522
    mul-float v0, p2, p2

    mul-float v3, p3, p3

    add-float/2addr v0, v3

    iget v3, p0, LX/3ij;->b:I

    iget v4, p0, LX/3ij;->b:I

    mul-int/2addr v3, v4

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 630523
    goto :goto_1

    :cond_3
    move v3, v2

    .line 630524
    goto :goto_2

    .line 630525
    :cond_4
    if-eqz v0, :cond_5

    .line 630526
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v3, p0, LX/3ij;->b:I

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    move v2, v1

    goto :goto_0

    .line 630527
    :cond_5
    if-eqz v3, :cond_0

    .line 630528
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v3, p0, LX/3ij;->b:I

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    move v2, v1

    goto :goto_0
.end method

.method private static b(III)I
    .locals 1

    .prologue
    .line 630174
    invoke-static {p0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 630175
    if-ge v0, p1, :cond_1

    const/4 p2, 0x0

    .line 630176
    :cond_0
    :goto_0
    return p2

    .line 630177
    :cond_1
    if-le v0, p2, :cond_2

    if-gtz p0, :cond_0

    neg-int p2, p2

    goto :goto_0

    :cond_2
    move p2, p0

    .line 630178
    goto :goto_0
.end method

.method private static b(LX/3ij;FFI)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 630488
    const/4 v1, 0x0

    .line 630489
    invoke-direct {p0, p1, p2, p3, v0}, LX/3ij;->a(FFII)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 630490
    :goto_0
    const/4 v1, 0x4

    invoke-direct {p0, p2, p1, p3, v1}, LX/3ij;->a(FFII)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 630491
    or-int/lit8 v0, v0, 0x4

    .line 630492
    :cond_0
    const/4 v1, 0x2

    invoke-direct {p0, p1, p2, p3, v1}, LX/3ij;->a(FFII)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 630493
    or-int/lit8 v0, v0, 0x2

    .line 630494
    :cond_1
    const/16 v1, 0x8

    invoke-direct {p0, p2, p1, p3, v1}, LX/3ij;->a(FFII)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 630495
    or-int/lit8 v0, v0, 0x8

    .line 630496
    :cond_2
    if-eqz v0, :cond_3

    .line 630497
    iget-object v1, p0, LX/3ij;->i:[I

    aget v2, v1, p3

    or-int/2addr v0, v2

    aput v0, v1, p3

    .line 630498
    :cond_3
    return-void

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method private static b(LX/3ij;Landroid/view/View;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 630374
    iget-object v1, p0, LX/3ij;->t:Landroid/view/View;

    if-ne p1, v1, :cond_0

    iget v1, p0, LX/3ij;->c:I

    if-ne v1, p2, :cond_0

    .line 630375
    :goto_0
    return v0

    .line 630376
    :cond_0
    if-eqz p1, :cond_1

    iget-object v1, p0, LX/3ij;->s:LX/3il;

    invoke-virtual {v1}, LX/3il;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 630377
    iput p2, p0, LX/3ij;->c:I

    .line 630378
    invoke-direct {p0, p1, p2}, LX/3ij;->a(Landroid/view/View;I)V

    goto :goto_0

    .line 630379
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(LX/3ij;)V
    .locals 3

    .prologue
    .line 630380
    const/4 v0, -0x1

    iput v0, p0, LX/3ij;->c:I

    .line 630381
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 630382
    iget-object v0, p0, LX/3ij;->d:[F

    if-nez v0, :cond_1

    .line 630383
    :goto_0
    iget-object v0, p0, LX/3ij;->l:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 630384
    iget-object v0, p0, LX/3ij;->l:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 630385
    const/4 v0, 0x0

    iput-object v0, p0, LX/3ij;->l:Landroid/view/VelocityTracker;

    .line 630386
    :cond_0
    return-void

    .line 630387
    :cond_1
    iget-object v0, p0, LX/3ij;->d:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 630388
    iget-object v0, p0, LX/3ij;->e:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 630389
    iget-object v0, p0, LX/3ij;->f:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 630390
    iget-object v0, p0, LX/3ij;->g:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 630391
    iget-object v0, p0, LX/3ij;->h:[I

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    .line 630392
    iget-object v0, p0, LX/3ij;->i:[I

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    .line 630393
    iget-object v0, p0, LX/3ij;->j:[I

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    .line 630394
    iput v2, p0, LX/3ij;->k:I

    goto :goto_0
.end method

.method private static c(LX/3ij;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 630364
    iget-object v0, p0, LX/3ij;->d:[F

    if-nez v0, :cond_0

    .line 630365
    :goto_0
    return-void

    .line 630366
    :cond_0
    iget-object v0, p0, LX/3ij;->d:[F

    aput v1, v0, p1

    .line 630367
    iget-object v0, p0, LX/3ij;->e:[F

    aput v1, v0, p1

    .line 630368
    iget-object v0, p0, LX/3ij;->f:[F

    aput v1, v0, p1

    .line 630369
    iget-object v0, p0, LX/3ij;->g:[F

    aput v1, v0, p1

    .line 630370
    iget-object v0, p0, LX/3ij;->h:[I

    aput v2, v0, p1

    .line 630371
    iget-object v0, p0, LX/3ij;->i:[I

    aput v2, v0, p1

    .line 630372
    iget-object v0, p0, LX/3ij;->j:[I

    aput v2, v0, p1

    .line 630373
    iget v0, p0, LX/3ij;->k:I

    const/4 v1, 0x1

    shl-int/2addr v1, p1

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, LX/3ij;->k:I

    goto :goto_0
.end method

.method private static c(LX/3ij;Landroid/view/MotionEvent;)V
    .locals 6

    .prologue
    .line 630355
    invoke-static {p1}, LX/2xd;->c(Landroid/view/MotionEvent;)I

    move-result v1

    .line 630356
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 630357
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v2

    .line 630358
    invoke-static {p1, v0}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v3

    .line 630359
    invoke-static {p1, v0}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v4

    .line 630360
    iget-object v5, p0, LX/3ij;->f:[F

    aput v3, v5, v2

    .line 630361
    iget-object v3, p0, LX/3ij;->g:[F

    aput v4, v3, v2

    .line 630362
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 630363
    :cond_0
    return-void
.end method

.method private c(II)Z
    .locals 2

    .prologue
    .line 630351
    iget-object v0, p0, LX/3ij;->t:Landroid/view/View;

    const/4 v1, 0x0

    .line 630352
    if-nez v0, :cond_1

    .line 630353
    :cond_0
    :goto_0
    move v0, v1

    .line 630354
    return v0

    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result p0

    if-lt p1, p0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result p0

    if-ge p1, p0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result p0

    if-lt p2, p0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result p0

    if-ge p2, p0, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private e()V
    .locals 4

    .prologue
    .line 630346
    iget-object v0, p0, LX/3ij;->l:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    iget v2, p0, LX/3ij;->m:F

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 630347
    iget-object v0, p0, LX/3ij;->l:Landroid/view/VelocityTracker;

    iget v1, p0, LX/3ij;->c:I

    invoke-static {v0, v1}, LX/2uG;->a(Landroid/view/VelocityTracker;I)F

    move-result v0

    iget v1, p0, LX/3ij;->n:F

    iget v2, p0, LX/3ij;->m:F

    invoke-static {v0, v1, v2}, LX/3ij;->a(FFF)F

    move-result v0

    .line 630348
    iget-object v1, p0, LX/3ij;->l:Landroid/view/VelocityTracker;

    iget v2, p0, LX/3ij;->c:I

    invoke-static {v1, v2}, LX/2uG;->b(Landroid/view/VelocityTracker;I)F

    move-result v1

    iget v2, p0, LX/3ij;->n:F

    iget v3, p0, LX/3ij;->m:F

    invoke-static {v1, v2, v3}, LX/3ij;->a(FFF)F

    move-result v1

    .line 630349
    invoke-direct {p0, v0, v1}, LX/3ij;->a(FF)V

    .line 630350
    return-void
.end method


# virtual methods
.method public final a(IIII)V
    .locals 9

    .prologue
    .line 630338
    iget-boolean v0, p0, LX/3ij;->u:Z

    if-nez v0, :cond_0

    .line 630339
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot flingCapturedView outside of a call to Callback#onViewReleased"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 630340
    :cond_0
    iget-object v0, p0, LX/3ij;->l:Landroid/view/VelocityTracker;

    iget v1, p0, LX/3ij;->c:I

    invoke-static {v0, v1}, LX/2uG;->b(Landroid/view/VelocityTracker;I)F

    move-result v0

    float-to-int v4, v0

    .line 630341
    iget-object v0, p0, LX/3ij;->q:Landroid/widget/Scroller;

    iget-object v1, p0, LX/3ij;->t:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget-object v2, p0, LX/3ij;->t:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    iget-object v3, p0, LX/3ij;->l:Landroid/view/VelocityTracker;

    iget v5, p0, LX/3ij;->c:I

    invoke-static {v3, v5}, LX/2uG;->a(Landroid/view/VelocityTracker;I)F

    move-result v3

    float-to-int v3, v3

    move v5, p1

    move v6, p3

    move v7, p2

    move v8, p4

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 630342
    if-gez v4, :cond_1

    :goto_0
    iput p2, p0, LX/3ij;->r:I

    .line 630343
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/3ij;->b(I)V

    .line 630344
    return-void

    :cond_1
    move p2, p4

    .line 630345
    goto :goto_0
.end method

.method public final a(II)Z
    .locals 13

    .prologue
    .line 630321
    iget-boolean v0, p0, LX/3ij;->u:Z

    if-nez v0, :cond_0

    .line 630322
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot settleCapturedViewAt outside of a call to Callback#onViewReleased"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 630323
    :cond_0
    iget-object v0, p0, LX/3ij;->l:Landroid/view/VelocityTracker;

    iget v1, p0, LX/3ij;->c:I

    invoke-static {v0, v1}, LX/2uG;->a(Landroid/view/VelocityTracker;I)F

    move-result v0

    float-to-int v0, v0

    iget-object v1, p0, LX/3ij;->l:Landroid/view/VelocityTracker;

    iget v2, p0, LX/3ij;->c:I

    invoke-static {v1, v2}, LX/2uG;->b(Landroid/view/VelocityTracker;I)F

    move-result v1

    float-to-int v1, v1

    const/4 v3, 0x0

    .line 630324
    iget-object v4, p0, LX/3ij;->t:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v10

    .line 630325
    iget-object v4, p0, LX/3ij;->t:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v9

    .line 630326
    sub-int v5, p1, v10

    .line 630327
    sub-int v6, p2, v9

    .line 630328
    if-nez v5, :cond_1

    if-nez v6, :cond_1

    .line 630329
    iget-object v4, p0, LX/3ij;->q:Landroid/widget/Scroller;

    invoke-virtual {v4}, Landroid/widget/Scroller;->abortAnimation()V

    .line 630330
    invoke-virtual {p0, v3}, LX/3ij;->b(I)V

    .line 630331
    :goto_0
    move v0, v3

    .line 630332
    return v0

    .line 630333
    :cond_1
    iget-object v4, p0, LX/3ij;->t:Landroid/view/View;

    move-object v3, p0

    move v7, v0

    move v8, v1

    invoke-static/range {v3 .. v8}, LX/3ij;->a(LX/3ij;Landroid/view/View;IIII)I

    move-result v12

    .line 630334
    iget-object v7, p0, LX/3ij;->q:Landroid/widget/Scroller;

    move v8, v10

    move v10, v5

    move v11, v6

    invoke-virtual/range {v7 .. v12}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 630335
    add-int v3, v9, v6

    iput v3, p0, LX/3ij;->r:I

    .line 630336
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/3ij;->b(I)V

    .line 630337
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 630277
    invoke-static {p1}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v2

    .line 630278
    invoke-static {p1}, LX/2xd;->b(Landroid/view/MotionEvent;)I

    move-result v3

    .line 630279
    if-nez v2, :cond_0

    .line 630280
    invoke-static {p0}, LX/3ij;->c(LX/3ij;)V

    .line 630281
    :cond_0
    iget-object v4, p0, LX/3ij;->l:Landroid/view/VelocityTracker;

    if-nez v4, :cond_1

    .line 630282
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v4

    iput-object v4, p0, LX/3ij;->l:Landroid/view/VelocityTracker;

    .line 630283
    :cond_1
    iget-object v4, p0, LX/3ij;->l:Landroid/view/VelocityTracker;

    invoke-virtual {v4, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 630284
    packed-switch v2, :pswitch_data_0

    .line 630285
    :cond_2
    :goto_0
    :pswitch_0
    iget v2, p0, LX/3ij;->a:I

    if-ne v2, v0, :cond_6

    :goto_1
    return v0

    .line 630286
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 630287
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 630288
    invoke-static {p1, v1}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v4

    .line 630289
    invoke-static {p0, v2, v3, v4}, LX/3ij;->a(LX/3ij;FFI)V

    .line 630290
    float-to-int v2, v2

    float-to-int v3, v3

    invoke-virtual {p0, v2, v3}, LX/3ij;->b(II)Landroid/view/View;

    move-result-object v2

    .line 630291
    iget-object v3, p0, LX/3ij;->t:Landroid/view/View;

    if-ne v2, v3, :cond_3

    iget v3, p0, LX/3ij;->a:I

    if-ne v3, v6, :cond_3

    .line 630292
    invoke-static {p0, v2, v4}, LX/3ij;->b(LX/3ij;Landroid/view/View;I)Z

    .line 630293
    :cond_3
    iget-object v2, p0, LX/3ij;->h:[I

    aget v2, v2, v4

    .line 630294
    iget v3, p0, LX/3ij;->p:I

    and-int/2addr v2, v3

    if-eqz v2, :cond_2

    goto :goto_0

    .line 630295
    :pswitch_2
    invoke-static {p1, v3}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v2

    .line 630296
    invoke-static {p1, v3}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v4

    .line 630297
    invoke-static {p1, v3}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v3

    .line 630298
    invoke-static {p0, v4, v3, v2}, LX/3ij;->a(LX/3ij;FFI)V

    .line 630299
    iget v5, p0, LX/3ij;->a:I

    if-eqz v5, :cond_2

    .line 630300
    iget v5, p0, LX/3ij;->a:I

    if-ne v5, v6, :cond_2

    .line 630301
    float-to-int v4, v4

    float-to-int v3, v3

    invoke-virtual {p0, v4, v3}, LX/3ij;->b(II)Landroid/view/View;

    move-result-object v3

    .line 630302
    iget-object v4, p0, LX/3ij;->t:Landroid/view/View;

    if-ne v3, v4, :cond_2

    .line 630303
    invoke-static {p0, v3, v2}, LX/3ij;->b(LX/3ij;Landroid/view/View;I)Z

    goto :goto_0

    .line 630304
    :pswitch_3
    invoke-static {p1}, LX/2xd;->c(Landroid/view/MotionEvent;)I

    move-result v3

    move v2, v1

    .line 630305
    :goto_2
    if-ge v2, v3, :cond_5

    .line 630306
    invoke-static {p1, v2}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v4

    .line 630307
    invoke-static {p1, v2}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v5

    .line 630308
    invoke-static {p1, v2}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v6

    .line 630309
    iget-object v7, p0, LX/3ij;->d:[F

    aget v7, v7, v4

    sub-float v7, v5, v7

    .line 630310
    iget-object v8, p0, LX/3ij;->e:[F

    aget v8, v8, v4

    sub-float v8, v6, v8

    .line 630311
    invoke-static {p0, v7, v8, v4}, LX/3ij;->b(LX/3ij;FFI)V

    .line 630312
    iget v9, p0, LX/3ij;->a:I

    if-eq v9, v0, :cond_5

    .line 630313
    float-to-int v5, v5

    float-to-int v6, v6

    invoke-virtual {p0, v5, v6}, LX/3ij;->b(II)Landroid/view/View;

    move-result-object v5

    .line 630314
    if-eqz v5, :cond_4

    invoke-static {p0, v5, v7, v8}, LX/3ij;->a(LX/3ij;Landroid/view/View;FF)Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-static {p0, v5, v4}, LX/3ij;->b(LX/3ij;Landroid/view/View;I)Z

    move-result v4

    if-nez v4, :cond_5

    .line 630315
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 630316
    :cond_5
    invoke-static {p0, p1}, LX/3ij;->c(LX/3ij;Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 630317
    :pswitch_4
    invoke-static {p1, v3}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v2

    .line 630318
    invoke-static {p0, v2}, LX/3ij;->c(LX/3ij;I)V

    goto/16 :goto_0

    .line 630319
    :pswitch_5
    invoke-static {p0}, LX/3ij;->c(LX/3ij;)V

    goto/16 :goto_0

    :cond_6
    move v0, v1

    .line 630320
    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public b(II)Landroid/view/View;
    .locals 3

    .prologue
    .line 630268
    iget-object v0, p0, LX/3ij;->v:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 630269
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 630270
    iget-object v0, p0, LX/3ij;->v:Landroid/view/ViewGroup;

    .line 630271
    move v2, v1

    .line 630272
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 630273
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    if-lt p1, v2, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v2

    if-ge p1, v2, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    if-lt p2, v2, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v2

    if-ge p2, v2, :cond_0

    .line 630274
    :goto_1
    return-object v0

    .line 630275
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 630276
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 630259
    invoke-static {p0}, LX/3ij;->c(LX/3ij;)V

    .line 630260
    iget v0, p0, LX/3ij;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 630261
    iget-object v0, p0, LX/3ij;->q:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    .line 630262
    iget-object v0, p0, LX/3ij;->q:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    .line 630263
    iget-object v0, p0, LX/3ij;->q:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 630264
    iget-object v0, p0, LX/3ij;->q:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    .line 630265
    iget-object v0, p0, LX/3ij;->q:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    .line 630266
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/3ij;->b(I)V

    .line 630267
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 630254
    iget v0, p0, LX/3ij;->a:I

    if-eq v0, p1, :cond_0

    .line 630255
    iput p1, p0, LX/3ij;->a:I

    .line 630256
    if-nez p1, :cond_0

    .line 630257
    const/4 v0, 0x0

    iput-object v0, p0, LX/3ij;->t:Landroid/view/View;

    .line 630258
    :cond_0
    return-void
.end method

.method public final b(Landroid/view/MotionEvent;)V
    .locals 9

    .prologue
    const/4 v1, -0x1

    const/4 v5, 0x0

    const/4 v0, 0x0

    const/4 v8, 0x1

    .line 630179
    invoke-static {p1}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v2

    .line 630180
    invoke-static {p1}, LX/2xd;->b(Landroid/view/MotionEvent;)I

    move-result v3

    .line 630181
    if-nez v2, :cond_0

    .line 630182
    invoke-static {p0}, LX/3ij;->c(LX/3ij;)V

    .line 630183
    :cond_0
    iget-object v4, p0, LX/3ij;->l:Landroid/view/VelocityTracker;

    if-nez v4, :cond_1

    .line 630184
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v4

    iput-object v4, p0, LX/3ij;->l:Landroid/view/VelocityTracker;

    .line 630185
    :cond_1
    iget-object v4, p0, LX/3ij;->l:Landroid/view/VelocityTracker;

    invoke-virtual {v4, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 630186
    packed-switch v2, :pswitch_data_0

    .line 630187
    :cond_2
    :goto_0
    :pswitch_0
    return-void

    .line 630188
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 630189
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 630190
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 630191
    float-to-int v3, v1

    float-to-int v4, v2

    invoke-virtual {p0, v3, v4}, LX/3ij;->b(II)Landroid/view/View;

    move-result-object v3

    .line 630192
    invoke-static {p0, v1, v2, v0}, LX/3ij;->a(LX/3ij;FFI)V

    .line 630193
    invoke-static {p0, v3, v0}, LX/3ij;->b(LX/3ij;Landroid/view/View;I)Z

    .line 630194
    iget-object v1, p0, LX/3ij;->h:[I

    aget v0, v1, v0

    .line 630195
    iget v1, p0, LX/3ij;->p:I

    and-int/2addr v0, v1

    if-eqz v0, :cond_2

    goto :goto_0

    .line 630196
    :pswitch_2
    invoke-static {p1, v3}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 630197
    invoke-static {p1, v3}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v1

    .line 630198
    invoke-static {p1, v3}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v2

    .line 630199
    invoke-static {p0, v1, v2, v0}, LX/3ij;->a(LX/3ij;FFI)V

    .line 630200
    iget v3, p0, LX/3ij;->a:I

    if-nez v3, :cond_3

    .line 630201
    float-to-int v1, v1

    float-to-int v2, v2

    invoke-virtual {p0, v1, v2}, LX/3ij;->b(II)Landroid/view/View;

    move-result-object v1

    .line 630202
    invoke-static {p0, v1, v0}, LX/3ij;->b(LX/3ij;Landroid/view/View;I)Z

    goto :goto_0

    .line 630203
    :cond_3
    float-to-int v1, v1

    float-to-int v2, v2

    invoke-direct {p0, v1, v2}, LX/3ij;->c(II)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 630204
    iget-object v1, p0, LX/3ij;->t:Landroid/view/View;

    invoke-static {p0, v1, v0}, LX/3ij;->b(LX/3ij;Landroid/view/View;I)Z

    goto :goto_0

    .line 630205
    :pswitch_3
    iget v1, p0, LX/3ij;->a:I

    if-ne v1, v8, :cond_6

    .line 630206
    iget v0, p0, LX/3ij;->c:I

    invoke-static {p1, v0}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 630207
    invoke-static {p1, v0}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v1

    .line 630208
    invoke-static {p1, v0}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    .line 630209
    iget-object v2, p0, LX/3ij;->f:[F

    iget v3, p0, LX/3ij;->c:I

    aget v2, v2, v3

    sub-float/2addr v1, v2

    float-to-int v1, v1

    .line 630210
    iget-object v2, p0, LX/3ij;->g:[F

    iget v3, p0, LX/3ij;->c:I

    aget v2, v2, v3

    sub-float/2addr v0, v2

    float-to-int v0, v0

    .line 630211
    iget-object v2, p0, LX/3ij;->t:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    iget-object v3, p0, LX/3ij;->t:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    add-int/2addr v3, v0

    .line 630212
    iget-object v4, p0, LX/3ij;->t:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    .line 630213
    iget-object v5, p0, LX/3ij;->t:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v5

    .line 630214
    if-eqz v1, :cond_4

    .line 630215
    const/4 v6, 0x0

    move v6, v6

    .line 630216
    iget-object v7, p0, LX/3ij;->t:Landroid/view/View;

    sub-int v4, v6, v4

    invoke-virtual {v7, v4}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 630217
    :cond_4
    if-eqz v0, :cond_5

    .line 630218
    iget-object v4, p0, LX/3ij;->s:LX/3il;

    invoke-virtual {v4, v3}, LX/3il;->b(I)I

    move-result v4

    .line 630219
    iget-object v6, p0, LX/3ij;->t:Landroid/view/View;

    sub-int/2addr v4, v5

    invoke-virtual {v6, v4}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 630220
    :cond_5
    invoke-static {p0, p1}, LX/3ij;->c(LX/3ij;Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 630221
    :cond_6
    invoke-static {p1}, LX/2xd;->c(Landroid/view/MotionEvent;)I

    move-result v1

    .line 630222
    :goto_1
    if-ge v0, v1, :cond_8

    .line 630223
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v2

    .line 630224
    invoke-static {p1, v0}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v3

    .line 630225
    invoke-static {p1, v0}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v4

    .line 630226
    iget-object v5, p0, LX/3ij;->d:[F

    aget v5, v5, v2

    sub-float v5, v3, v5

    .line 630227
    iget-object v6, p0, LX/3ij;->e:[F

    aget v6, v6, v2

    sub-float v6, v4, v6

    .line 630228
    invoke-static {p0, v5, v6, v2}, LX/3ij;->b(LX/3ij;FFI)V

    .line 630229
    iget v7, p0, LX/3ij;->a:I

    if-eq v7, v8, :cond_8

    .line 630230
    float-to-int v3, v3

    float-to-int v4, v4

    invoke-virtual {p0, v3, v4}, LX/3ij;->b(II)Landroid/view/View;

    move-result-object v3

    .line 630231
    invoke-static {p0, v3, v5, v6}, LX/3ij;->a(LX/3ij;Landroid/view/View;FF)Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-static {p0, v3, v2}, LX/3ij;->b(LX/3ij;Landroid/view/View;I)Z

    move-result v2

    if-nez v2, :cond_8

    .line 630232
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 630233
    :cond_8
    invoke-static {p0, p1}, LX/3ij;->c(LX/3ij;Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 630234
    :pswitch_4
    invoke-static {p1, v3}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v2

    .line 630235
    iget v3, p0, LX/3ij;->a:I

    if-ne v3, v8, :cond_9

    iget v3, p0, LX/3ij;->c:I

    if-ne v2, v3, :cond_9

    .line 630236
    invoke-static {p1}, LX/2xd;->c(Landroid/view/MotionEvent;)I

    move-result v3

    .line 630237
    :goto_2
    if-ge v0, v3, :cond_d

    .line 630238
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v4

    .line 630239
    iget v5, p0, LX/3ij;->c:I

    if-eq v4, v5, :cond_a

    .line 630240
    invoke-static {p1, v0}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v5

    .line 630241
    invoke-static {p1, v0}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v6

    .line 630242
    float-to-int v5, v5

    float-to-int v6, v6

    invoke-virtual {p0, v5, v6}, LX/3ij;->b(II)Landroid/view/View;

    move-result-object v5

    iget-object v6, p0, LX/3ij;->t:Landroid/view/View;

    if-ne v5, v6, :cond_a

    iget-object v5, p0, LX/3ij;->t:Landroid/view/View;

    invoke-static {p0, v5, v4}, LX/3ij;->b(LX/3ij;Landroid/view/View;I)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 630243
    iget v0, p0, LX/3ij;->c:I

    .line 630244
    :goto_3
    if-ne v0, v1, :cond_9

    .line 630245
    invoke-direct {p0}, LX/3ij;->e()V

    .line 630246
    :cond_9
    invoke-static {p0, v2}, LX/3ij;->c(LX/3ij;I)V

    goto/16 :goto_0

    .line 630247
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 630248
    :pswitch_5
    iget v0, p0, LX/3ij;->a:I

    if-ne v0, v8, :cond_b

    .line 630249
    invoke-direct {p0}, LX/3ij;->e()V

    .line 630250
    :cond_b
    invoke-static {p0}, LX/3ij;->c(LX/3ij;)V

    goto/16 :goto_0

    .line 630251
    :pswitch_6
    iget v0, p0, LX/3ij;->a:I

    if-ne v0, v8, :cond_c

    .line 630252
    invoke-direct {p0, v5, v5}, LX/3ij;->a(FF)V

    .line 630253
    :cond_c
    invoke-static {p0}, LX/3ij;->c(LX/3ij;)V

    goto/16 :goto_0

    :cond_d
    move v0, v1

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method
