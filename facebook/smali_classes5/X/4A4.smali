.class public final LX/4A4;
.super LX/2uF;
.source ""


# instance fields
.field public final transient a:I

.field public final transient b:I

.field public final synthetic c:LX/2uF;


# direct methods
.method public constructor <init>(LX/2uF;II)V
    .locals 0

    .prologue
    .line 675242
    iput-object p1, p0, LX/4A4;->c:LX/2uF;

    invoke-direct {p0}, LX/2uF;-><init>()V

    .line 675243
    iput p2, p0, LX/4A4;->a:I

    .line 675244
    iput p3, p0, LX/4A4;->b:I

    .line 675245
    return-void
.end method


# virtual methods
.method public final a(I)LX/1vs;
    .locals 3

    .prologue
    .line 675236
    iget v0, p0, LX/4A4;->b:I

    invoke-static {p1, v0}, LX/0PB;->checkElementIndex(II)I

    .line 675237
    iget-object v0, p0, LX/4A4;->c:LX/2uF;

    iget v1, p0, LX/4A4;->a:I

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    .line 675238
    iget-object v1, v0, LX/1vs;->a:LX/15i;

    .line 675239
    iget v2, v0, LX/1vs;->b:I

    .line 675240
    iget v0, v0, LX/1vs;->c:I

    .line 675241
    invoke-static {v1, v2, v0}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(II)LX/2uF;
    .locals 3

    .prologue
    .line 675234
    iget v0, p0, LX/4A4;->b:I

    invoke-static {p1, p2, v0}, LX/0PB;->checkPositionIndexes(III)V

    .line 675235
    iget-object v0, p0, LX/4A4;->c:LX/2uF;

    iget v1, p0, LX/4A4;->a:I

    add-int/2addr v1, p1

    iget v2, p0, LX/4A4;->a:I

    add-int/2addr v2, p2

    invoke-virtual {v0, v1, v2}, LX/2uF;->a(II)LX/2uF;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/2sN;
    .locals 1

    .prologue
    .line 675233
    invoke-super {p0}, LX/2uF;->e()LX/3Sh;

    move-result-object v0

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 675231
    iget v0, p0, LX/4A4;->b:I

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 675232
    const/4 v0, 0x1

    return v0
.end method
