.class public LX/3uf;
.super Landroid/view/ActionMode;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/3uV;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/3uV;)V
    .locals 0

    .prologue
    .line 649109
    invoke-direct {p0}, Landroid/view/ActionMode;-><init>()V

    .line 649110
    iput-object p1, p0, LX/3uf;->a:Landroid/content/Context;

    .line 649111
    iput-object p2, p0, LX/3uf;->b:LX/3uV;

    .line 649112
    return-void
.end method


# virtual methods
.method public final finish()V
    .locals 1

    .prologue
    .line 649107
    iget-object v0, p0, LX/3uf;->b:LX/3uV;

    invoke-virtual {v0}, LX/3uV;->c()V

    .line 649108
    return-void
.end method

.method public final getCustomView()Landroid/view/View;
    .locals 1

    .prologue
    .line 649106
    iget-object v0, p0, LX/3uf;->b:LX/3uV;

    invoke-virtual {v0}, LX/3uV;->i()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getMenu()Landroid/view/Menu;
    .locals 2

    .prologue
    .line 649105
    iget-object v1, p0, LX/3uf;->a:Landroid/content/Context;

    iget-object v0, p0, LX/3uf;->b:LX/3uV;

    invoke-virtual {v0}, LX/3uV;->b()Landroid/view/Menu;

    move-result-object v0

    check-cast v0, LX/3qu;

    invoke-static {v1, v0}, LX/3vE;->a(Landroid/content/Context;LX/3qu;)Landroid/view/Menu;

    move-result-object v0

    return-object v0
.end method

.method public final getMenuInflater()Landroid/view/MenuInflater;
    .locals 1

    .prologue
    .line 649104
    iget-object v0, p0, LX/3uf;->b:LX/3uV;

    invoke-virtual {v0}, LX/3uV;->a()Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method public final getSubtitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 649103
    iget-object v0, p0, LX/3uf;->b:LX/3uV;

    invoke-virtual {v0}, LX/3uV;->g()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final getTag()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 649100
    iget-object v0, p0, LX/3uf;->b:LX/3uV;

    .line 649101
    iget-object p0, v0, LX/3uV;->a:Ljava/lang/Object;

    move-object v0, p0

    .line 649102
    return-object v0
.end method

.method public final getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 649099
    iget-object v0, p0, LX/3uf;->b:LX/3uV;

    invoke-virtual {v0}, LX/3uV;->f()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final getTitleOptionalHint()Z
    .locals 1

    .prologue
    .line 649113
    iget-object v0, p0, LX/3uf;->b:LX/3uV;

    .line 649114
    iget-boolean p0, v0, LX/3uV;->b:Z

    move v0, p0

    .line 649115
    return v0
.end method

.method public final invalidate()V
    .locals 1

    .prologue
    .line 649097
    iget-object v0, p0, LX/3uf;->b:LX/3uV;

    invoke-virtual {v0}, LX/3uV;->d()V

    .line 649098
    return-void
.end method

.method public final isTitleOptional()Z
    .locals 1

    .prologue
    .line 649096
    iget-object v0, p0, LX/3uf;->b:LX/3uV;

    invoke-virtual {v0}, LX/3uV;->h()Z

    move-result v0

    return v0
.end method

.method public final setCustomView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 649094
    iget-object v0, p0, LX/3uf;->b:LX/3uV;

    invoke-virtual {v0, p1}, LX/3uV;->a(Landroid/view/View;)V

    .line 649095
    return-void
.end method

.method public final setSubtitle(I)V
    .locals 1

    .prologue
    .line 649092
    iget-object v0, p0, LX/3uf;->b:LX/3uV;

    invoke-virtual {v0, p1}, LX/3uV;->b(I)V

    .line 649093
    return-void
.end method

.method public final setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 649090
    iget-object v0, p0, LX/3uf;->b:LX/3uV;

    invoke-virtual {v0, p1}, LX/3uV;->a(Ljava/lang/CharSequence;)V

    .line 649091
    return-void
.end method

.method public final setTag(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 649087
    iget-object v0, p0, LX/3uf;->b:LX/3uV;

    .line 649088
    iput-object p1, v0, LX/3uV;->a:Ljava/lang/Object;

    .line 649089
    return-void
.end method

.method public final setTitle(I)V
    .locals 1

    .prologue
    .line 649085
    iget-object v0, p0, LX/3uf;->b:LX/3uV;

    invoke-virtual {v0, p1}, LX/3uV;->a(I)V

    .line 649086
    return-void
.end method

.method public final setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 649083
    iget-object v0, p0, LX/3uf;->b:LX/3uV;

    invoke-virtual {v0, p1}, LX/3uV;->b(Ljava/lang/CharSequence;)V

    .line 649084
    return-void
.end method

.method public final setTitleOptionalHint(Z)V
    .locals 1

    .prologue
    .line 649081
    iget-object v0, p0, LX/3uf;->b:LX/3uV;

    invoke-virtual {v0, p1}, LX/3uV;->a(Z)V

    .line 649082
    return-void
.end method
