.class public abstract LX/4rd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public b:LX/4rw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4rw",
            "<TT;>;"
        }
    .end annotation
.end field

.field public c:LX/4rw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4rw",
            "<TT;>;"
        }
    .end annotation
.end field

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 816884
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 816877
    iget-object v0, p0, LX/4rd;->c:LX/4rw;

    if-eqz v0, :cond_0

    .line 816878
    iget-object v0, p0, LX/4rd;->c:LX/4rw;

    .line 816879
    iget-object v1, v0, LX/4rw;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 816880
    iput-object v0, p0, LX/4rd;->a:Ljava/lang/Object;

    .line 816881
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/4rd;->c:LX/4rw;

    iput-object v0, p0, LX/4rd;->b:LX/4rw;

    .line 816882
    const/4 v0, 0x0

    iput v0, p0, LX/4rd;->d:I

    .line 816883
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 816885
    invoke-direct {p0}, LX/4rd;->b()V

    .line 816886
    iget-object v0, p0, LX/4rd;->a:Ljava/lang/Object;

    if-nez v0, :cond_0

    const/16 v0, 0xc

    invoke-virtual {p0, v0}, LX/4rd;->a(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/4rd;->a:Ljava/lang/Object;

    goto :goto_0
.end method

.method public abstract a(I)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation
.end method

.method public final a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)TT;"
        }
    .end annotation

    .prologue
    .line 816867
    new-instance v0, LX/4rw;

    invoke-direct {v0, p1, p2}, LX/4rw;-><init>(Ljava/lang/Object;I)V

    .line 816868
    iget-object v1, p0, LX/4rd;->b:LX/4rw;

    if-nez v1, :cond_0

    .line 816869
    iput-object v0, p0, LX/4rd;->c:LX/4rw;

    iput-object v0, p0, LX/4rd;->b:LX/4rw;

    .line 816870
    :goto_0
    iget v0, p0, LX/4rd;->d:I

    add-int/2addr v0, p2

    iput v0, p0, LX/4rd;->d:I

    .line 816871
    const/16 v0, 0x4000

    if-ge p2, v0, :cond_1

    .line 816872
    add-int v0, p2, p2

    .line 816873
    :goto_1
    invoke-virtual {p0, v0}, LX/4rd;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 816874
    :cond_0
    iget-object v1, p0, LX/4rd;->c:LX/4rw;

    invoke-virtual {v1, v0}, LX/4rw;->a(LX/4rw;)V

    .line 816875
    iput-object v0, p0, LX/4rd;->c:LX/4rw;

    goto :goto_0

    .line 816876
    :cond_1
    shr-int/lit8 v0, p2, 0x2

    add-int/2addr v0, p2

    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)TT;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 816856
    iget v0, p0, LX/4rd;->d:I

    add-int v3, p2, v0

    .line 816857
    invoke-virtual {p0, v3}, LX/4rd;->a(I)Ljava/lang/Object;

    move-result-object v4

    .line 816858
    iget-object v0, p0, LX/4rd;->b:LX/4rw;

    move v1, v2

    :goto_0
    if-eqz v0, :cond_0

    .line 816859
    invoke-virtual {v0, v4, v1}, LX/4rw;->a(Ljava/lang/Object;I)I

    move-result v1

    .line 816860
    iget-object p0, v0, LX/4rw;->c:LX/4rw;

    move-object v0, p0

    .line 816861
    goto :goto_0

    .line 816862
    :cond_0
    invoke-static {p1, v2, v4, v1, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 816863
    add-int v0, v1, p2

    .line 816864
    if-eq v0, v3, :cond_1

    .line 816865
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Should have gotten "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " entries, got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 816866
    :cond_1
    return-object v4
.end method
