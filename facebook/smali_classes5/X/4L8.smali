.class public LX/4L8;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 682053
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 682054
    const/4 v11, 0x0

    .line 682055
    const/4 v10, 0x0

    .line 682056
    const/4 v9, 0x0

    .line 682057
    const/4 v8, 0x0

    .line 682058
    const-wide/16 v6, 0x0

    .line 682059
    const/4 v5, 0x0

    .line 682060
    const/4 v4, 0x0

    .line 682061
    const/4 v3, 0x0

    .line 682062
    const/4 v2, 0x0

    .line 682063
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, v13, :cond_b

    .line 682064
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 682065
    const/4 v2, 0x0

    .line 682066
    :goto_0
    return v2

    .line 682067
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v13, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v13, :cond_9

    .line 682068
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 682069
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 682070
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v13, v14, :cond_0

    if-eqz v3, :cond_0

    .line 682071
    const-string v13, "cache_id"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 682072
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v12, v3

    goto :goto_1

    .line 682073
    :cond_1
    const-string v13, "celebsItems"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 682074
    invoke-static/range {p0 .. p1}, LX/4L9;->a(LX/15w;LX/186;)I

    move-result v3

    move v11, v3

    goto :goto_1

    .line 682075
    :cond_2
    const-string v13, "celebsTitle"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 682076
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    move v7, v3

    goto :goto_1

    .line 682077
    :cond_3
    const-string v13, "debug_info"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 682078
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v6, v3

    goto :goto_1

    .line 682079
    :cond_4
    const-string v13, "fetchTimeMs"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 682080
    const/4 v2, 0x1

    .line 682081
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto :goto_1

    .line 682082
    :cond_5
    const-string v13, "short_term_cache_key"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 682083
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v10, v3

    goto :goto_1

    .line 682084
    :cond_6
    const-string v13, "title"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 682085
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    move v9, v3

    goto/16 :goto_1

    .line 682086
    :cond_7
    const-string v13, "tracking"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 682087
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 682088
    :cond_8
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 682089
    :cond_9
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 682090
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 682091
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 682092
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 682093
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 682094
    if-eqz v2, :cond_a

    .line 682095
    const/4 v3, 0x4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 682096
    :cond_a
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 682097
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 682098
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 682099
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_b
    move v12, v11

    move v11, v10

    move v10, v5

    move-wide v15, v6

    move v6, v8

    move v7, v9

    move v9, v4

    move v8, v3

    move-wide v4, v15

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 682100
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 682101
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 682102
    invoke-static {p0, v2}, LX/4L8;->a(LX/15w;LX/186;)I

    move-result v1

    .line 682103
    if-eqz v0, :cond_0

    .line 682104
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 682105
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 682106
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 682107
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 682108
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 682109
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 682110
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 682111
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 682112
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682113
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 682114
    const-string v0, "name"

    const-string v1, "CelebrationsFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 682115
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 682116
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 682117
    if-eqz v0, :cond_0

    .line 682118
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682119
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 682120
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 682121
    if-eqz v0, :cond_2

    .line 682122
    const-string v1, "celebsItems"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682123
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 682124
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 682125
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2, p3}, LX/4L9;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 682126
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 682127
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 682128
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 682129
    if-eqz v0, :cond_3

    .line 682130
    const-string v1, "celebsTitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682131
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 682132
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 682133
    if-eqz v0, :cond_4

    .line 682134
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682135
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 682136
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 682137
    cmp-long v2, v0, v2

    if-eqz v2, :cond_5

    .line 682138
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682139
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 682140
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 682141
    if-eqz v0, :cond_6

    .line 682142
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682143
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 682144
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 682145
    if-eqz v0, :cond_7

    .line 682146
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682147
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 682148
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 682149
    if-eqz v0, :cond_8

    .line 682150
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682151
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 682152
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 682153
    return-void
.end method
