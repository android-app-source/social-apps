.class public LX/3V0;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements LX/3V1;
.implements LX/24a;


# static fields
.field public static final j:LX/1Cz;


# instance fields
.field public k:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final m:Lcom/facebook/feed/rows/views/ContentTextView;

.field private final n:Lcom/facebook/widget/text/TextViewWithFallback;

.field public final o:Landroid/widget/ImageView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587472
    new-instance v0, LX/3V3;

    invoke-direct {v0}, LX/3V3;-><init>()V

    sput-object v0, LX/3V0;->j:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 587484
    const/4 v0, 0x0

    const v1, 0x7f030876

    invoke-direct {p0, p1, v0, v1}, LX/3V0;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 587485
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587473
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 587474
    const-class v0, LX/3V0;

    invoke-static {v0, p0}, LX/3V0;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 587475
    invoke-virtual {p0, p3}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 587476
    const v0, 0x7f0d160d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/3V0;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 587477
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 587478
    iget-object v0, p0, LX/3V0;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImportantForAccessibility(I)V

    .line 587479
    :cond_0
    const v0, 0x7f0d1616

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/views/ContentTextView;

    iput-object v0, p0, LX/3V0;->m:Lcom/facebook/feed/rows/views/ContentTextView;

    .line 587480
    const v0, 0x7f0d1615

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/TextViewWithFallback;

    iput-object v0, p0, LX/3V0;->n:Lcom/facebook/widget/text/TextViewWithFallback;

    .line 587481
    iget-object v0, p0, LX/3V0;->n:Lcom/facebook/widget/text/TextViewWithFallback;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/TextViewWithFallback;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 587482
    const v0, 0x7f0d0bde

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/3V0;->o:Landroid/widget/ImageView;

    .line 587483
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/3V0;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object p0

    check-cast p0, LX/0wM;

    iput-object p0, p1, LX/3V0;->k:LX/0wM;

    return-void
.end method


# virtual methods
.method public final a(LX/0hs;)V
    .locals 1

    .prologue
    .line 587458
    iget-object v0, p0, LX/3V0;->o:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 587459
    return-void
.end method

.method public final a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587470
    iget-object v0, p0, LX/3V0;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 587471
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587486
    iget-object v1, p0, LX/3V0;->n:Lcom/facebook/widget/text/TextViewWithFallback;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/TextViewWithFallback;->setVisibility(I)V

    .line 587487
    iget-object v0, p0, LX/3V0;->n:Lcom/facebook/widget/text/TextViewWithFallback;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/TextViewWithFallback;->setText(Ljava/lang/CharSequence;)V

    .line 587488
    iget-object v0, p0, LX/3V0;->n:Lcom/facebook/widget/text/TextViewWithFallback;

    .line 587489
    iput-object p2, v0, Lcom/facebook/widget/text/TextViewWithFallback;->a:Ljava/lang/CharSequence;

    .line 587490
    return-void

    .line 587491
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 587468
    iget-object v0, p0, LX/3V0;->o:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 587469
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMenuButtonActive(Z)V
    .locals 2

    .prologue
    .line 587465
    iget-object v1, p0, LX/3V0;->o:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 587466
    return-void

    .line 587467
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setProfileImageOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587463
    iget-object v0, p0, LX/3V0;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 587464
    return-void
.end method

.method public setSubtitleIcon(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 587461
    iget-object v0, p0, LX/3V0;->n:Lcom/facebook/widget/text/TextViewWithFallback;

    iget-object v1, p0, LX/3V0;->k:LX/0wM;

    const v2, -0x6e685d

    invoke-virtual {v1, p1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v3, v3, v1, v3}, Lcom/facebook/widget/text/TextViewWithFallback;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 587462
    return-void
.end method

.method public setSubtitleWithLayout(Landroid/text/Layout;)V
    .locals 2

    .prologue
    .line 587460
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not a text layout header t6009510"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
