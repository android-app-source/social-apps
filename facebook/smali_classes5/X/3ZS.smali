.class public LX/3ZS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 599964
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 599965
    return-void
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 7

    .prologue
    .line 599966
    sget-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsGapPartDefinition;->a:LX/1Cz;

    sget-object v1, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsHeaderPartDefinition;->a:LX/1Cz;

    sget-object v2, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;->a:LX/1Cz;

    sget-object v3, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;->a:LX/1Cz;

    sget-object v4, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;->a:LX/1Cz;

    sget-object v5, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;->a:LX/1Cz;

    sget-object v6, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsWashTextPartDefinition;->a:LX/1Cz;

    invoke-static/range {v0 .. v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 599967
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cz;

    .line 599968
    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 599969
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 599970
    :cond_0
    return-void
.end method
