.class public LX/3oC;
.super LX/3oB;
.source ""


# annotations
.annotation runtime Landroid/support/design/widget/CoordinatorLayout$DefaultBehavior;
    value = Landroid/support/design/widget/FloatingActionButton$Behavior;
.end annotation


# instance fields
.field private a:Landroid/content/res/ColorStateList;

.field private b:Landroid/graphics/PorterDuff$Mode;

.field private c:I

.field public d:I

.field public final e:Landroid/graphics/Rect;

.field public final f:LX/3oD;


# direct methods
.method private static a(II)I
    .locals 2

    .prologue
    .line 639988
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 639989
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 639990
    sparse-switch v1, :sswitch_data_0

    .line 639991
    :goto_0
    :sswitch_0
    return p0

    .line 639992
    :sswitch_1
    invoke-static {p0, v0}, Ljava/lang/Math;->min(II)I

    move-result p0

    goto :goto_0

    :sswitch_2
    move p0, v0

    .line 639993
    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/3oC;LX/3oA;)LX/3o7;
    .locals 1
    .param p0    # LX/3oC;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 639985
    if-nez p1, :cond_0

    .line 639986
    const/4 v0, 0x0

    .line 639987
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/3o8;

    invoke-direct {v0, p0, p1}, LX/3o8;-><init>(LX/3oC;LX/3oA;)V

    goto :goto_0
.end method


# virtual methods
.method public final drawableStateChanged()V
    .locals 1

    .prologue
    .line 639982
    invoke-super {p0}, LX/3oB;->drawableStateChanged()V

    .line 639983
    iget-object v0, p0, LX/3oC;->f:LX/3oD;

    invoke-virtual {p0}, LX/3oC;->getDrawableState()[I

    invoke-virtual {v0}, LX/3oD;->d()V

    .line 639984
    return-void
.end method

.method public getBackgroundTintList()Landroid/content/res/ColorStateList;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 639981
    iget-object v0, p0, LX/3oC;->a:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method public getBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 639980
    iget-object v0, p0, LX/3oC;->b:Landroid/graphics/PorterDuff$Mode;

    return-object v0
.end method

.method public final jumpDrawablesToCurrentState()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 639977
    invoke-super {p0}, LX/3oB;->jumpDrawablesToCurrentState()V

    .line 639978
    iget-object v0, p0, LX/3oC;->f:LX/3oD;

    invoke-virtual {v0}, LX/3oD;->e()V

    .line 639979
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x1201fe57    # -9.8264E27f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 639974
    invoke-super {p0}, LX/3oB;->onAttachedToWindow()V

    .line 639975
    goto :goto_0

    .line 639976
    :goto_0
    const/16 v1, 0x2d

    const v2, -0x57a7b943

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x566b50bc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 639994
    invoke-super {p0}, LX/3oB;->onDetachedFromWindow()V

    .line 639995
    iget-object v1, p0, LX/3oC;->f:LX/3oD;

    .line 639996
    iget-object v2, v1, LX/3oD;->e:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    if-eqz v2, :cond_0

    .line 639997
    iget-object v2, v1, LX/3oD;->d:LX/3oB;

    invoke-virtual {v2}, LX/3oB;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    iget-object p0, v1, LX/3oD;->e:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v2, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 639998
    const/4 v2, 0x0

    iput-object v2, v1, LX/3oD;->e:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    .line 639999
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x2edacc74

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 639966
    iget v0, p0, LX/3oC;->d:I

    packed-switch v0, :pswitch_data_0

    .line 639967
    invoke-virtual {p0}, LX/3oC;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0125

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_0
    move v0, v0

    .line 639968
    invoke-static {v0, p1}, LX/3oC;->a(II)I

    move-result v1

    .line 639969
    invoke-static {v0, p2}, LX/3oC;->a(II)I

    move-result v0

    .line 639970
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 639971
    iget-object v1, p0, LX/3oC;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v0

    iget-object v2, p0, LX/3oC;->e:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v2

    iget-object v2, p0, LX/3oC;->e:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v2

    iget-object v2, p0, LX/3oC;->e:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v2

    invoke-virtual {p0, v1, v0}, LX/3oC;->setMeasuredDimension(II)V

    .line 639972
    return-void

    .line 639973
    :pswitch_0
    invoke-virtual {p0}, LX/3oC;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0126

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public setBackgroundColor(I)V
    .locals 0

    .prologue
    .line 639965
    return-void
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 639964
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 0

    .prologue
    .line 639963
    return-void
.end method

.method public setBackgroundTintList(Landroid/content/res/ColorStateList;)V
    .locals 1
    .param p1    # Landroid/content/res/ColorStateList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 639959
    iget-object v0, p0, LX/3oC;->a:Landroid/content/res/ColorStateList;

    if-eq v0, p1, :cond_0

    .line 639960
    iput-object p1, p0, LX/3oC;->a:Landroid/content/res/ColorStateList;

    .line 639961
    iget-object v0, p0, LX/3oC;->f:LX/3oD;

    invoke-virtual {v0}, LX/3oD;->a()V

    .line 639962
    :cond_0
    return-void
.end method

.method public setBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1
    .param p1    # Landroid/graphics/PorterDuff$Mode;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 639955
    iget-object v0, p0, LX/3oC;->b:Landroid/graphics/PorterDuff$Mode;

    if-eq v0, p1, :cond_0

    .line 639956
    iput-object p1, p0, LX/3oC;->b:Landroid/graphics/PorterDuff$Mode;

    .line 639957
    iget-object v0, p0, LX/3oC;->f:LX/3oD;

    invoke-virtual {v0}, LX/3oD;->b()V

    .line 639958
    :cond_0
    return-void
.end method

.method public setRippleColor(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 639951
    iget v0, p0, LX/3oC;->c:I

    if-eq v0, p1, :cond_0

    .line 639952
    iput p1, p0, LX/3oC;->c:I

    .line 639953
    iget-object v0, p0, LX/3oC;->f:LX/3oD;

    invoke-virtual {v0}, LX/3oD;->c()V

    .line 639954
    :cond_0
    return-void
.end method
