.class public LX/4da;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4dI;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/1bw;

.field private final c:Landroid/util/DisplayMetrics;

.field private final d:Landroid/text/TextPaint;

.field private final e:Ljava/lang/StringBuilder;

.field private final f:LX/4di;

.field private final g:LX/4di;

.field private h:LX/4dH;

.field private i:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 796253
    const-class v0, LX/4dI;

    sput-object v0, LX/4da;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1bw;Landroid/util/DisplayMetrics;)V
    .locals 2

    .prologue
    .line 796194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 796195
    iput-object p1, p0, LX/4da;->b:LX/1bw;

    .line 796196
    iput-object p2, p0, LX/4da;->c:Landroid/util/DisplayMetrics;

    .line 796197
    new-instance v0, LX/4di;

    invoke-direct {v0}, LX/4di;-><init>()V

    iput-object v0, p0, LX/4da;->f:LX/4di;

    .line 796198
    new-instance v0, LX/4di;

    invoke-direct {v0}, LX/4di;-><init>()V

    iput-object v0, p0, LX/4da;->g:LX/4di;

    .line 796199
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, LX/4da;->e:Ljava/lang/StringBuilder;

    .line 796200
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, LX/4da;->d:Landroid/text/TextPaint;

    .line 796201
    iget-object v0, p0, LX/4da;->d:Landroid/text/TextPaint;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 796202
    iget-object v0, p0, LX/4da;->d:Landroid/text/TextPaint;

    const/16 v1, 0xe

    invoke-direct {p0, v1}, LX/4da;->c(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 796203
    return-void
.end method

.method private c(I)I
    .locals 3

    .prologue
    .line 796252
    const/4 v0, 0x1

    int-to-float v1, p1

    iget-object v2, p0, LX/4da;->c:Landroid/util/DisplayMetrics;

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 796250
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/4da;->i:J

    .line 796251
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 796248
    iget-object v0, p0, LX/4da;->f:LX/4di;

    invoke-virtual {v0, p1}, LX/4di;->a(I)V

    .line 796249
    return-void
.end method

.method public final a(LX/4dH;)V
    .locals 0

    .prologue
    .line 796246
    iput-object p1, p0, LX/4da;->h:LX/4dH;

    .line 796247
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 12

    .prologue
    const/16 v3, 0xa

    const/4 v2, 0x0

    .line 796216
    iget-object v0, p0, LX/4da;->f:LX/4di;

    invoke-virtual {v0, v3}, LX/4di;->b(I)I

    move-result v0

    .line 796217
    iget-object v1, p0, LX/4da;->g:LX/4di;

    invoke-virtual {v1, v3}, LX/4di;->b(I)I

    move-result v1

    .line 796218
    add-int/2addr v0, v1

    .line 796219
    invoke-direct {p0, v3}, LX/4da;->c(I)I

    move-result v7

    .line 796220
    const/16 v3, 0x14

    invoke-direct {p0, v3}, LX/4da;->c(I)I

    move-result v8

    .line 796221
    const/4 v3, 0x5

    invoke-direct {p0, v3}, LX/4da;->c(I)I

    move-result v10

    .line 796222
    if-lez v0, :cond_2

    .line 796223
    mul-int/lit8 v1, v1, 0x64

    div-int v0, v1, v0

    .line 796224
    iget-object v1, p0, LX/4da;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 796225
    iget-object v1, p0, LX/4da;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 796226
    iget-object v0, p0, LX/4da;->e:Ljava/lang/StringBuilder;

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 796227
    iget-object v1, p0, LX/4da;->e:Ljava/lang/StringBuilder;

    iget-object v0, p0, LX/4da;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    int-to-float v4, v7

    int-to-float v5, v8

    iget-object v6, p0, LX/4da;->d:Landroid/text/TextPaint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 796228
    int-to-float v0, v7

    iget-object v1, p0, LX/4da;->d:Landroid/text/TextPaint;

    iget-object v3, p0, LX/4da;->e:Ljava/lang/StringBuilder;

    iget-object v4, p0, LX/4da;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    invoke-virtual {v1, v3, v2, v4}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 796229
    add-int/2addr v0, v10

    .line 796230
    :goto_0
    iget-object v1, p0, LX/4da;->h:LX/4dH;

    invoke-interface {v1}, LX/4dG;->j()I

    move-result v1

    .line 796231
    iget-object v3, p0, LX/4da;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 796232
    iget-object v3, p0, LX/4da;->e:Ljava/lang/StringBuilder;

    invoke-static {v3, v1}, LX/1bw;->a(Ljava/lang/StringBuilder;I)V

    .line 796233
    iget-object v1, p0, LX/4da;->d:Landroid/text/TextPaint;

    iget-object v3, p0, LX/4da;->e:Ljava/lang/StringBuilder;

    iget-object v4, p0, LX/4da;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    invoke-virtual {v1, v3, v2, v4}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v11

    .line 796234
    int-to-float v1, v0

    add-float/2addr v1, v11

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    cmpl-float v1, v1, v3

    if-lez v1, :cond_1

    .line 796235
    int-to-float v0, v8

    iget-object v1, p0, LX/4da;->d:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->getTextSize()F

    move-result v1

    int-to-float v3, v10

    add-float/2addr v1, v3

    add-float/2addr v0, v1

    float-to-int v0, v0

    move v8, v0

    move v9, v7

    .line 796236
    :goto_1
    iget-object v1, p0, LX/4da;->e:Ljava/lang/StringBuilder;

    iget-object v0, p0, LX/4da;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    int-to-float v4, v9

    int-to-float v5, v8

    iget-object v6, p0, LX/4da;->d:Landroid/text/TextPaint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 796237
    int-to-float v0, v9

    add-float/2addr v0, v11

    float-to-int v0, v0

    .line 796238
    add-int/2addr v0, v10

    .line 796239
    iget-object v1, p0, LX/4da;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 796240
    iget-object v1, p0, LX/4da;->h:LX/4dH;

    iget-object v3, p0, LX/4da;->e:Ljava/lang/StringBuilder;

    invoke-interface {v1, v3}, LX/4dH;->a(Ljava/lang/StringBuilder;)V

    .line 796241
    iget-object v1, p0, LX/4da;->d:Landroid/text/TextPaint;

    iget-object v3, p0, LX/4da;->e:Ljava/lang/StringBuilder;

    iget-object v4, p0, LX/4da;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    invoke-virtual {v1, v3, v2, v4}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v1

    .line 796242
    int-to-float v3, v0

    add-float/2addr v1, v3

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    cmpl-float v1, v1, v3

    if-lez v1, :cond_0

    .line 796243
    int-to-float v0, v8

    iget-object v1, p0, LX/4da;->d:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->getTextSize()F

    move-result v1

    int-to-float v3, v10

    add-float/2addr v1, v3

    add-float/2addr v0, v1

    float-to-int v8, v0

    .line 796244
    :goto_2
    iget-object v1, p0, LX/4da;->e:Ljava/lang/StringBuilder;

    iget-object v0, p0, LX/4da;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    int-to-float v4, v7

    int-to-float v5, v8

    iget-object v6, p0, LX/4da;->d:Landroid/text/TextPaint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 796245
    return-void

    :cond_0
    move v7, v0

    goto :goto_2

    :cond_1
    move v9, v0

    goto :goto_1

    :cond_2
    move v0, v7

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 796214
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    .line 796215
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 796212
    iget-object v0, p0, LX/4da;->g:LX/4di;

    invoke-virtual {v0, p1}, LX/4di;->a(I)V

    .line 796213
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 796210
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/4da;->i:J

    .line 796211
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 796208
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    .line 796209
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 796206
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/4da;->i:J

    .line 796207
    return-void
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 796204
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    .line 796205
    return-void
.end method
