.class public final LX/3ox;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3ow;


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3ot;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3ov;",
            ">;"
        }
    .end annotation
.end field

.field public c:Landroid/view/View;

.field public d:J

.field public e:J

.field public f:F

.field private g:Z

.field private h:Z

.field public i:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 641051
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 641052
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3ox;->a:Ljava/util/List;

    .line 641053
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3ox;->b:Ljava/util/List;

    .line 641054
    const-wide/16 v0, 0xc8

    iput-wide v0, p0, LX/3ox;->e:J

    .line 641055
    const/4 v0, 0x0

    iput v0, p0, LX/3ox;->f:F

    .line 641056
    iput-boolean v2, p0, LX/3ox;->g:Z

    .line 641057
    iput-boolean v2, p0, LX/3ox;->h:Z

    .line 641058
    new-instance v0, Landroid/support/v4/animation/DonutAnimatorCompatProvider$DonutFloatValueAnimator$1;

    invoke-direct {v0, p0}, Landroid/support/v4/animation/DonutAnimatorCompatProvider$DonutFloatValueAnimator$1;-><init>(LX/3ox;)V

    iput-object v0, p0, LX/3ox;->i:Ljava/lang/Runnable;

    .line 641059
    return-void
.end method

.method public static e(LX/3ox;)J
    .locals 2

    .prologue
    .line 641050
    iget-object v0, p0, LX/3ox;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getDrawingTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public static g(LX/3ox;)V
    .locals 2

    .prologue
    .line 641046
    iget-object v0, p0, LX/3ox;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 641047
    iget-object v0, p0, LX/3ox;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3ot;

    invoke-interface {v0, p0}, LX/3ot;->a(LX/3ow;)V

    .line 641048
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 641049
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 641037
    iget-boolean v0, p0, LX/3ox;->g:Z

    if-eqz v0, :cond_0

    .line 641038
    :goto_0
    return-void

    .line 641039
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3ox;->g:Z

    .line 641040
    iget-object v0, p0, LX/3ox;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_1

    .line 641041
    iget-object v1, p0, LX/3ox;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 641042
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 641043
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, LX/3ox;->f:F

    .line 641044
    invoke-static {p0}, LX/3ox;->e(LX/3ox;)J

    move-result-wide v0

    iput-wide v0, p0, LX/3ox;->d:J

    .line 641045
    iget-object v0, p0, LX/3ox;->c:Landroid/view/View;

    iget-object v1, p0, LX/3ox;->i:Ljava/lang/Runnable;

    const-wide/16 v2, 0x10

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 641034
    iget-boolean v0, p0, LX/3ox;->g:Z

    if-nez v0, :cond_0

    .line 641035
    iput-wide p1, p0, LX/3ox;->e:J

    .line 641036
    :cond_0
    return-void
.end method

.method public final a(LX/3ot;)V
    .locals 1

    .prologue
    .line 641032
    iget-object v0, p0, LX/3ox;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 641033
    return-void
.end method

.method public final a(LX/3ov;)V
    .locals 1

    .prologue
    .line 641030
    iget-object v0, p0, LX/3ox;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 641031
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 641028
    iput-object p1, p0, LX/3ox;->c:Landroid/view/View;

    .line 641029
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 641019
    iget-boolean v0, p0, LX/3ox;->h:Z

    if-eqz v0, :cond_0

    .line 641020
    :goto_0
    return-void

    .line 641021
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3ox;->h:Z

    .line 641022
    iget-boolean v0, p0, LX/3ox;->g:Z

    if-eqz v0, :cond_1

    .line 641023
    iget-object v0, p0, LX/3ox;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_1

    .line 641024
    iget-object v0, p0, LX/3ox;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3ot;

    invoke-interface {v0}, LX/3ot;->a()V

    .line 641025
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 641026
    :cond_1
    invoke-static {p0}, LX/3ox;->g(LX/3ox;)V

    goto :goto_0
.end method

.method public final c()F
    .locals 1

    .prologue
    .line 641027
    iget v0, p0, LX/3ox;->f:F

    return v0
.end method
