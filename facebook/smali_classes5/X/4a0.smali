.class public final enum LX/4a0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4a0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4a0;

.field public static final enum ALLOW:LX/4a0;

.field public static final enum ERROR:LX/4a0;

.field public static final enum NOT_SET:LX/4a0;

.field public static final enum SKIP:LX/4a0;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 790355
    new-instance v0, LX/4a0;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v2}, LX/4a0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4a0;->ERROR:LX/4a0;

    new-instance v0, LX/4a0;

    const-string v1, "SKIP"

    invoke-direct {v0, v1, v3}, LX/4a0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4a0;->SKIP:LX/4a0;

    new-instance v0, LX/4a0;

    const-string v1, "ALLOW"

    invoke-direct {v0, v1, v4}, LX/4a0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4a0;->ALLOW:LX/4a0;

    new-instance v0, LX/4a0;

    const-string v1, "NOT_SET"

    invoke-direct {v0, v1, v5}, LX/4a0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4a0;->NOT_SET:LX/4a0;

    const/4 v0, 0x4

    new-array v0, v0, [LX/4a0;

    sget-object v1, LX/4a0;->ERROR:LX/4a0;

    aput-object v1, v0, v2

    sget-object v1, LX/4a0;->SKIP:LX/4a0;

    aput-object v1, v0, v3

    sget-object v1, LX/4a0;->ALLOW:LX/4a0;

    aput-object v1, v0, v4

    sget-object v1, LX/4a0;->NOT_SET:LX/4a0;

    aput-object v1, v0, v5

    sput-object v0, LX/4a0;->$VALUES:[LX/4a0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 790356
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4a0;
    .locals 1

    .prologue
    .line 790357
    const-class v0, LX/4a0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4a0;

    return-object v0
.end method

.method public static values()[LX/4a0;
    .locals 1

    .prologue
    .line 790358
    sget-object v0, LX/4a0;->$VALUES:[LX/4a0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4a0;

    return-object v0
.end method
