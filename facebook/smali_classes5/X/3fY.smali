.class public LX/3fY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "Lcom/facebook/contacts/server/FetchMessagingFavoritesResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 622552
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 622553
    return-void
.end method

.method public static a(LX/0QB;)LX/3fY;
    .locals 1

    .prologue
    .line 622554
    new-instance v0, LX/3fY;

    invoke-direct {v0}, LX/3fY;-><init>()V

    .line 622555
    move-object v0, v0

    .line 622556
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 622557
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 622558
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 622559
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "query"

    .line 622560
    const-string v2, "SELECT favorite_id, ordering FROM messaging_favorite WHERE uid=me() ORDER BY ordering ASC"

    move-object v2, v2

    .line 622561
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 622562
    new-instance v0, LX/14N;

    const-string v1, "fetchMessagingFavorites"

    const-string v2, "GET"

    const-string v3, "method/fql.query"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 622563
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 622564
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    check-cast v0, LX/162;

    .line 622565
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 622566
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 622567
    invoke-virtual {v0, v1}, LX/0lF;->a(I)LX/0lF;

    move-result-object v3

    .line 622568
    new-instance v4, LX/0XI;

    invoke-direct {v4}, LX/0XI;-><init>()V

    .line 622569
    sget-object v5, LX/0XG;->FACEBOOK:LX/0XG;

    const-string v6, "favorite_id"

    invoke-virtual {v3, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    .line 622570
    invoke-virtual {v4}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 622571
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 622572
    :cond_0
    new-instance v0, Lcom/facebook/contacts/server/FetchMessagingFavoritesResult;

    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/facebook/contacts/server/FetchMessagingFavoritesResult;-><init>(LX/0ta;LX/0Px;J)V

    return-object v0
.end method
