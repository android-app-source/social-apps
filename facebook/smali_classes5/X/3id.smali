.class public LX/3id;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/20i;

.field public final b:LX/1Ck;

.field private final c:LX/3iV;

.field private final d:LX/25G;

.field private final e:LX/20j;


# direct methods
.method public constructor <init>(LX/20j;LX/20i;LX/1Ck;LX/3iV;LX/25G;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 630005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 630006
    iput-object p1, p0, LX/3id;->e:LX/20j;

    .line 630007
    iput-object p2, p0, LX/3id;->a:LX/20i;

    .line 630008
    iput-object p3, p0, LX/3id;->b:LX/1Ck;

    .line 630009
    iput-object p4, p0, LX/3id;->c:LX/3iV;

    .line 630010
    iput-object p5, p0, LX/3id;->d:LX/25G;

    .line 630011
    return-void
.end method

.method public static a(LX/0QB;)LX/3id;
    .locals 1

    .prologue
    .line 630004
    invoke-static {p0}, LX/3id;->b(LX/0QB;)LX/3id;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3id;
    .locals 6

    .prologue
    .line 630002
    new-instance v0, LX/3id;

    invoke-static {p0}, LX/20j;->a(LX/0QB;)LX/20j;

    move-result-object v1

    check-cast v1, LX/20j;

    invoke-static {p0}, LX/20i;->a(LX/0QB;)LX/20i;

    move-result-object v2

    check-cast v2, LX/20i;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {p0}, LX/3iV;->a(LX/0QB;)LX/3iV;

    move-result-object v4

    check-cast v4, LX/3iV;

    invoke-static {p0}, LX/25G;->b(LX/0QB;)LX/25G;

    move-result-object v5

    check-cast v5, LX/25G;

    invoke-direct/range {v0 .. v5}, LX/3id;-><init>(LX/20j;LX/20i;LX/1Ck;LX/3iV;LX/25G;)V

    .line 630003
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZLX/1L9;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;",
            "Z",
            "LX/1L9",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 629981
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 629982
    iget-object v0, p0, LX/3id;->e:LX/20j;

    iget-object v1, p0, LX/3id;->a:LX/20i;

    invoke-virtual {v1}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 629983
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 629984
    if-eqz p4, :cond_0

    .line 629985
    invoke-interface {p4, v0}, LX/1L9;->a(Ljava/lang/Object;)V

    .line 629986
    :cond_0
    invoke-static {}, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->a()LX/5HA;

    move-result-object v1

    .line 629987
    iput-object p2, v1, LX/5HA;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 629988
    move-object v1, v1

    .line 629989
    iput-object v0, v1, LX/5HA;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 629990
    move-object v1, v1

    .line 629991
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v0

    .line 629992
    iput-boolean v0, v1, LX/5HA;->b:Z

    .line 629993
    move-object v0, v1

    .line 629994
    iget-object v1, p0, LX/3id;->a:LX/20i;

    invoke-virtual {v1}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 629995
    iput-object v1, v0, LX/5HA;->c:Lcom/facebook/graphql/model/GraphQLActor;

    .line 629996
    move-object v0, v0

    .line 629997
    invoke-virtual {v0}, LX/5HA;->a()Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;

    move-result-object v1

    .line 629998
    if-eqz p3, :cond_1

    iget-object v0, p0, LX/3id;->d:LX/25G;

    .line 629999
    :goto_0
    iget-object v2, p0, LX/3id;->b:LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "toggle_like_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1}, LX/25H;->a(Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/95w;

    invoke-direct {v1, p0, p4, p1}, LX/95w;-><init>(LX/3id;LX/1L9;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 630000
    return-void

    .line 630001
    :cond_1
    iget-object v0, p0, LX/3id;->c:LX/3iV;

    goto :goto_0
.end method
