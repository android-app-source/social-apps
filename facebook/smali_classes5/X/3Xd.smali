.class public LX/3Xd;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/24a;


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field private final b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final c:Landroid/widget/ImageView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 593628
    new-instance v0, LX/3Xe;

    invoke-direct {v0}, LX/3Xe;-><init>()V

    sput-object v0, LX/3Xd;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 593629
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 593630
    const v0, 0x7f0307da

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 593631
    const v0, 0x7f0d124d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/3Xd;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 593632
    const v0, 0x7f0d0bde

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/3Xd;->c:Landroid/widget/ImageView;

    .line 593633
    iget-object v0, p0, LX/3Xd;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 593634
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 593635
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 593636
    return-void
.end method


# virtual methods
.method public final a(LX/0hs;)V
    .locals 1

    .prologue
    .line 593637
    iget-object v0, p0, LX/3Xd;->c:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 593638
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 593639
    iget-object v0, p0, LX/3Xd;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 593640
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMenuButtonActive(Z)V
    .locals 2

    .prologue
    .line 593641
    iget-object v1, p0, LX/3Xd;->c:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 593642
    return-void

    .line 593643
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
