.class public LX/4om;
.super Landroid/preference/EditTextPreference;
.source ""


# instance fields
.field private final a:LX/2qy;

.field public b:LX/2qx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 810383
    invoke-direct {p0, p1}, Landroid/preference/EditTextPreference;-><init>(Landroid/content/Context;)V

    .line 810384
    const-class v0, LX/4om;

    invoke-static {v0, p0}, LX/4om;->a(Ljava/lang/Class;Landroid/preference/Preference;)V

    .line 810385
    iget-object v0, p0, LX/4om;->b:LX/2qx;

    invoke-virtual {v0, p0}, LX/2qx;->a(Landroid/preference/Preference;)LX/2qy;

    move-result-object v0

    iput-object v0, p0, LX/4om;->a:LX/2qy;

    .line 810386
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/preference/Preference;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/preference/Preference;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/4om;

    const-class p0, LX/2qx;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/2qx;

    iput-object v1, p1, LX/4om;->b:LX/2qx;

    return-void
.end method


# virtual methods
.method public final a(LX/0Tn;)V
    .locals 1

    .prologue
    .line 810371
    iget-object v0, p0, LX/4om;->a:LX/2qy;

    invoke-virtual {v0, p1}, LX/2qy;->a(LX/0Tn;)V

    .line 810372
    return-void
.end method

.method public final getPersistedString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 810382
    iget-object v0, p0, LX/4om;->a:LX/2qy;

    invoke-virtual {v0, p1}, LX/2qy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 810379
    iget-object v0, p0, LX/4om;->a:LX/2qy;

    .line 810380
    iget-object p0, v0, LX/2qy;->b:Landroid/content/SharedPreferences;

    move-object v0, p0

    .line 810381
    return-object v0
.end method

.method public final onAddEditTextToDialogView(Landroid/view/View;Landroid/widget/EditText;)V
    .locals 3

    .prologue
    .line 810374
    const v0, 0x7f0d2679

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 810375
    if-eqz v0, :cond_0

    .line 810376
    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-virtual {v0, p2, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 810377
    :goto_0
    return-void

    .line 810378
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/preference/EditTextPreference;->onAddEditTextToDialogView(Landroid/view/View;Landroid/widget/EditText;)V

    goto :goto_0
.end method

.method public final persistString(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 810373
    iget-object v0, p0, LX/4om;->a:LX/2qy;

    invoke-virtual {v0, p1}, LX/2qy;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
