.class public LX/4rF;
.super LX/4rA;
.source ""


# instance fields
.field public final a:LX/0m4;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0m4",
            "<*>;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/0lJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/0m4;LX/0lJ;Ljava/util/HashMap;Ljava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m4",
            "<*>;",
            "LX/0lJ;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/0lJ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 815368
    invoke-virtual {p1}, LX/0m4;->n()LX/0li;

    move-result-object v0

    invoke-direct {p0, p2, v0}, LX/4rA;-><init>(LX/0lJ;LX/0li;)V

    .line 815369
    iput-object p1, p0, LX/4rF;->a:LX/0m4;

    .line 815370
    iput-object p3, p0, LX/4rF;->b:Ljava/util/HashMap;

    .line 815371
    iput-object p4, p0, LX/4rF;->e:Ljava/util/HashMap;

    .line 815372
    return-void
.end method

.method public static a(LX/0m4;LX/0lJ;Ljava/util/Collection;ZZ)LX/4rF;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m4",
            "<*>;",
            "LX/0lJ;",
            "Ljava/util/Collection",
            "<",
            "LX/4qu;",
            ">;ZZ)",
            "LX/4rF;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 815347
    if-ne p3, p4, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 815348
    :cond_0
    if-eqz p3, :cond_7

    .line 815349
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    move-object v3, v1

    .line 815350
    :goto_0
    if-eqz p4, :cond_6

    .line 815351
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    move-object v2, v0

    .line 815352
    :goto_1
    if-eqz p2, :cond_5

    .line 815353
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4qu;

    .line 815354
    iget-object v1, v0, LX/4qu;->_class:Ljava/lang/Class;

    move-object v5, v1

    .line 815355
    invoke-virtual {v0}, LX/4qu;->c()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 815356
    iget-object v1, v0, LX/4qu;->_name:Ljava/lang/String;

    move-object v0, v1

    .line 815357
    move-object v1, v0

    .line 815358
    :goto_3
    if-eqz p3, :cond_2

    .line 815359
    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 815360
    :cond_2
    if-eqz p4, :cond_1

    .line 815361
    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lJ;

    .line 815362
    if-eqz v0, :cond_3

    .line 815363
    iget-object p2, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, p2

    .line 815364
    invoke-virtual {v5, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 815365
    :cond_3
    invoke-virtual {p0, v5}, LX/0m4;->b(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 815366
    :cond_4
    invoke-static {v5}, LX/4rF;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_3

    .line 815367
    :cond_5
    new-instance v0, LX/4rF;

    invoke-direct {v0, p0, p1, v3, v2}, LX/4rF;-><init>(LX/0m4;LX/0lJ;Ljava/util/HashMap;Ljava/util/HashMap;)V

    return-object v0

    :cond_6
    move-object v2, v0

    goto :goto_1

    :cond_7
    move-object v3, v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 815344
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 815345
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 815346
    if-gez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0lJ;
    .locals 1

    .prologue
    .line 815342
    iget-object v0, p0, LX/4rF;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lJ;

    .line 815343
    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 815320
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 815321
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    .line 815322
    iget-object v3, p0, LX/4rF;->b:Ljava/util/HashMap;

    monitor-enter v3

    .line 815323
    :try_start_0
    iget-object v0, p0, LX/4rF;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 815324
    if-nez v0, :cond_2

    .line 815325
    iget-object v4, p0, LX/4rF;->a:LX/0m4;

    invoke-virtual {v4}, LX/0m4;->g()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 815326
    iget-object v0, p0, LX/4rF;->a:LX/0m4;

    invoke-virtual {v0, v1}, LX/0m4;->c(Ljava/lang/Class;)LX/0lS;

    move-result-object v0

    .line 815327
    iget-object v4, p0, LX/4rF;->a:LX/0m4;

    invoke-virtual {v4}, LX/0m4;->a()LX/0lU;

    move-result-object v4

    invoke-virtual {v0}, LX/0lS;->c()LX/0lN;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0lU;->f(LX/0lN;)Ljava/lang/String;

    move-result-object v0

    .line 815328
    :cond_0
    if-nez v0, :cond_1

    .line 815329
    invoke-static {v1}, LX/4rF;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    .line 815330
    :cond_1
    iget-object v1, p0, LX/4rF;->b:Ljava/util/HashMap;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 815331
    :cond_2
    monitor-exit v3

    .line 815332
    return-object v0

    .line 815333
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 815339
    if-nez p1, :cond_0

    .line 815340
    const/4 v0, 0x0

    .line 815341
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, LX/4rF;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 815334
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 815335
    const/16 v1, 0x5b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 815336
    const-string v1, "; id-to-type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/4rF;->e:Ljava/util/HashMap;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 815337
    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 815338
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
