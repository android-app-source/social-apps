.class public LX/4O0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 695279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 695280
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 695281
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 695282
    :goto_0
    return v1

    .line 695283
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 695284
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 695285
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 695286
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 695287
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 695288
    const-string v7, "cursor"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 695289
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 695290
    :cond_2
    const-string v7, "node"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 695291
    invoke-static {p0, p1}, LX/2bR;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 695292
    :cond_3
    const-string v7, "result_decoration"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 695293
    invoke-static {p0, p1}, LX/4Ny;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 695294
    :cond_4
    const-string v7, "sort_key"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 695295
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 695296
    :cond_5
    const-string v7, "logging_unit_id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 695297
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 695298
    :cond_6
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 695299
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 695300
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 695301
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 695302
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 695303
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 695304
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 695305
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 695306
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 695307
    if-eqz v0, :cond_0

    .line 695308
    const-string v1, "cursor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695309
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 695310
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 695311
    if-eqz v0, :cond_1

    .line 695312
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695313
    invoke-static {p0, v0, p2, p3}, LX/2bR;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 695314
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 695315
    if-eqz v0, :cond_2

    .line 695316
    const-string v1, "result_decoration"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695317
    invoke-static {p0, v0, p2, p3}, LX/4Ny;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 695318
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 695319
    if-eqz v0, :cond_3

    .line 695320
    const-string v1, "sort_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695321
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 695322
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 695323
    if-eqz v0, :cond_4

    .line 695324
    const-string v1, "logging_unit_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695325
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 695326
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 695327
    return-void
.end method
