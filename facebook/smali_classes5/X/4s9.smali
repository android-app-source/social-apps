.class public final enum LX/4s9;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4s9;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4s9;

.field public static final enum CHECK_SHARED_NAMES:LX/4s9;

.field public static final enum CHECK_SHARED_STRING_VALUES:LX/4s9;

.field public static final enum ENCODE_BINARY_AS_7BIT:LX/4s9;

.field public static final enum WRITE_END_MARKER:LX/4s9;

.field public static final enum WRITE_HEADER:LX/4s9;


# instance fields
.field public final _defaultState:Z

.field public final _mask:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 817390
    new-instance v0, LX/4s9;

    const-string v1, "WRITE_HEADER"

    invoke-direct {v0, v1, v3, v2}, LX/4s9;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/4s9;->WRITE_HEADER:LX/4s9;

    .line 817391
    new-instance v0, LX/4s9;

    const-string v1, "WRITE_END_MARKER"

    invoke-direct {v0, v1, v2, v3}, LX/4s9;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/4s9;->WRITE_END_MARKER:LX/4s9;

    .line 817392
    new-instance v0, LX/4s9;

    const-string v1, "ENCODE_BINARY_AS_7BIT"

    invoke-direct {v0, v1, v4, v2}, LX/4s9;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/4s9;->ENCODE_BINARY_AS_7BIT:LX/4s9;

    .line 817393
    new-instance v0, LX/4s9;

    const-string v1, "CHECK_SHARED_NAMES"

    invoke-direct {v0, v1, v5, v2}, LX/4s9;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/4s9;->CHECK_SHARED_NAMES:LX/4s9;

    .line 817394
    new-instance v0, LX/4s9;

    const-string v1, "CHECK_SHARED_STRING_VALUES"

    invoke-direct {v0, v1, v6, v3}, LX/4s9;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/4s9;->CHECK_SHARED_STRING_VALUES:LX/4s9;

    .line 817395
    const/4 v0, 0x5

    new-array v0, v0, [LX/4s9;

    sget-object v1, LX/4s9;->WRITE_HEADER:LX/4s9;

    aput-object v1, v0, v3

    sget-object v1, LX/4s9;->WRITE_END_MARKER:LX/4s9;

    aput-object v1, v0, v2

    sget-object v1, LX/4s9;->ENCODE_BINARY_AS_7BIT:LX/4s9;

    aput-object v1, v0, v4

    sget-object v1, LX/4s9;->CHECK_SHARED_NAMES:LX/4s9;

    aput-object v1, v0, v5

    sget-object v1, LX/4s9;->CHECK_SHARED_STRING_VALUES:LX/4s9;

    aput-object v1, v0, v6

    sput-object v0, LX/4s9;->$VALUES:[LX/4s9;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 817386
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 817387
    iput-boolean p3, p0, LX/4s9;->_defaultState:Z

    .line 817388
    const/4 v0, 0x1

    invoke-virtual {p0}, LX/4s9;->ordinal()I

    move-result v1

    shl-int/2addr v0, v1

    iput v0, p0, LX/4s9;->_mask:I

    .line 817389
    return-void
.end method

.method public static collectDefaults()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 817380
    invoke-static {}, LX/4s9;->values()[LX/4s9;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 817381
    invoke-virtual {v4}, LX/4s9;->enabledByDefault()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 817382
    invoke-virtual {v4}, LX/4s9;->getMask()I

    move-result v4

    or-int/2addr v0, v4

    .line 817383
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 817384
    :cond_1
    return v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/4s9;
    .locals 1

    .prologue
    .line 817385
    const-class v0, LX/4s9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4s9;

    return-object v0
.end method

.method public static values()[LX/4s9;
    .locals 1

    .prologue
    .line 817379
    sget-object v0, LX/4s9;->$VALUES:[LX/4s9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4s9;

    return-object v0
.end method


# virtual methods
.method public final enabledByDefault()Z
    .locals 1

    .prologue
    .line 817377
    iget-boolean v0, p0, LX/4s9;->_defaultState:Z

    return v0
.end method

.method public final getMask()I
    .locals 1

    .prologue
    .line 817378
    iget v0, p0, LX/4s9;->_mask:I

    return v0
.end method
