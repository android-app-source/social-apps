.class public LX/4UE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 721369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 37

    .prologue
    .line 721370
    const/16 v33, 0x0

    .line 721371
    const/16 v32, 0x0

    .line 721372
    const/16 v31, 0x0

    .line 721373
    const/16 v30, 0x0

    .line 721374
    const/16 v29, 0x0

    .line 721375
    const/16 v28, 0x0

    .line 721376
    const/16 v27, 0x0

    .line 721377
    const/16 v26, 0x0

    .line 721378
    const/16 v25, 0x0

    .line 721379
    const/16 v24, 0x0

    .line 721380
    const/16 v23, 0x0

    .line 721381
    const/16 v22, 0x0

    .line 721382
    const/16 v21, 0x0

    .line 721383
    const/16 v20, 0x0

    .line 721384
    const/16 v19, 0x0

    .line 721385
    const/16 v18, 0x0

    .line 721386
    const/16 v17, 0x0

    .line 721387
    const/16 v16, 0x0

    .line 721388
    const/4 v15, 0x0

    .line 721389
    const/4 v14, 0x0

    .line 721390
    const/4 v13, 0x0

    .line 721391
    const/4 v12, 0x0

    .line 721392
    const/4 v11, 0x0

    .line 721393
    const/4 v10, 0x0

    .line 721394
    const/4 v9, 0x0

    .line 721395
    const/4 v8, 0x0

    .line 721396
    const/4 v7, 0x0

    .line 721397
    const/4 v6, 0x0

    .line 721398
    const/4 v5, 0x0

    .line 721399
    const/4 v4, 0x0

    .line 721400
    const/4 v3, 0x0

    .line 721401
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v34

    sget-object v35, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    if-eq v0, v1, :cond_1

    .line 721402
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 721403
    const/4 v3, 0x0

    .line 721404
    :goto_0
    return v3

    .line 721405
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 721406
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v34

    sget-object v35, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    if-eq v0, v1, :cond_15

    .line 721407
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v34

    .line 721408
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 721409
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v35

    sget-object v36, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    if-eq v0, v1, :cond_1

    if-eqz v34, :cond_1

    .line 721410
    const-string v35, "__type__"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-nez v35, :cond_2

    const-string v35, "__typename"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_3

    .line 721411
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/String;)I

    move-result v33

    goto :goto_1

    .line 721412
    :cond_3
    const-string v35, "id"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_4

    .line 721413
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v32

    goto :goto_1

    .line 721414
    :cond_4
    const-string v35, "live_video_count"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_5

    .line 721415
    const/4 v14, 0x1

    .line 721416
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v31

    goto :goto_1

    .line 721417
    :cond_5
    const-string v35, "square_header_image"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_6

    .line 721418
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v30

    goto :goto_1

    .line 721419
    :cond_6
    const-string v35, "subscribe_status"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_7

    .line 721420
    const/4 v13, 0x1

    .line 721421
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v29 .. v29}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v29

    goto/16 :goto_1

    .line 721422
    :cond_7
    const-string v35, "video_channel_can_viewer_follow"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_8

    .line 721423
    const/4 v12, 0x1

    .line 721424
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v28

    goto/16 :goto_1

    .line 721425
    :cond_8
    const-string v35, "video_channel_can_viewer_pin"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_9

    .line 721426
    const/4 v11, 0x1

    .line 721427
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v27

    goto/16 :goto_1

    .line 721428
    :cond_9
    const-string v35, "video_channel_can_viewer_subscribe"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_a

    .line 721429
    const/4 v10, 0x1

    .line 721430
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v26

    goto/16 :goto_1

    .line 721431
    :cond_a
    const-string v35, "video_channel_curator_profile"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_b

    .line 721432
    invoke-static/range {p0 .. p1}, LX/2aw;->a(LX/15w;LX/186;)I

    move-result v25

    goto/16 :goto_1

    .line 721433
    :cond_b
    const-string v35, "video_channel_has_new"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_c

    .line 721434
    const/4 v9, 0x1

    .line 721435
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v24

    goto/16 :goto_1

    .line 721436
    :cond_c
    const-string v35, "video_channel_has_viewer_subscribed"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_d

    .line 721437
    const/4 v8, 0x1

    .line 721438
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v23

    goto/16 :goto_1

    .line 721439
    :cond_d
    const-string v35, "video_channel_is_viewer_following"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_e

    .line 721440
    const/4 v7, 0x1

    .line 721441
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v22

    goto/16 :goto_1

    .line 721442
    :cond_e
    const-string v35, "video_channel_is_viewer_pinned"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_f

    .line 721443
    const/4 v6, 0x1

    .line 721444
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v21

    goto/16 :goto_1

    .line 721445
    :cond_f
    const-string v35, "video_channel_max_new_count"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_10

    .line 721446
    const/4 v5, 0x1

    .line 721447
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v20

    goto/16 :goto_1

    .line 721448
    :cond_10
    const-string v35, "video_channel_new_count"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_11

    .line 721449
    const/4 v4, 0x1

    .line 721450
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v19

    goto/16 :goto_1

    .line 721451
    :cond_11
    const-string v35, "video_channel_subtitle"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_12

    .line 721452
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 721453
    :cond_12
    const-string v35, "video_channel_title"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_13

    .line 721454
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 721455
    :cond_13
    const-string v35, "live_video_subscription_status"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_14

    .line 721456
    const/4 v3, 0x1

    .line 721457
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    move-result-object v16

    goto/16 :goto_1

    .line 721458
    :cond_14
    const-string v35, "video_channel_curator"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_0

    .line 721459
    invoke-static/range {p0 .. p1}, LX/2bR;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 721460
    :cond_15
    const/16 v34, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 721461
    const/16 v34, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v34

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 721462
    const/16 v33, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v33

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 721463
    if-eqz v14, :cond_16

    .line 721464
    const/4 v14, 0x2

    const/16 v32, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v31

    move/from16 v2, v32

    invoke-virtual {v0, v14, v1, v2}, LX/186;->a(III)V

    .line 721465
    :cond_16
    const/4 v14, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v14, v1}, LX/186;->b(II)V

    .line 721466
    if-eqz v13, :cond_17

    .line 721467
    const/4 v13, 0x5

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v13, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 721468
    :cond_17
    if-eqz v12, :cond_18

    .line 721469
    const/4 v12, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 721470
    :cond_18
    if-eqz v11, :cond_19

    .line 721471
    const/4 v11, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 721472
    :cond_19
    if-eqz v10, :cond_1a

    .line 721473
    const/16 v10, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 721474
    :cond_1a
    const/16 v10, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 721475
    if-eqz v9, :cond_1b

    .line 721476
    const/16 v9, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 721477
    :cond_1b
    if-eqz v8, :cond_1c

    .line 721478
    const/16 v8, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 721479
    :cond_1c
    if-eqz v7, :cond_1d

    .line 721480
    const/16 v7, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 721481
    :cond_1d
    if-eqz v6, :cond_1e

    .line 721482
    const/16 v6, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 721483
    :cond_1e
    if-eqz v5, :cond_1f

    .line 721484
    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v5, v1, v6}, LX/186;->a(III)V

    .line 721485
    :cond_1f
    if-eqz v4, :cond_20

    .line 721486
    const/16 v4, 0xf

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1, v5}, LX/186;->a(III)V

    .line 721487
    :cond_20
    const/16 v4, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 721488
    const/16 v4, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 721489
    if-eqz v3, :cond_21

    .line 721490
    const/16 v3, 0x12

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 721491
    :cond_21
    const/16 v3, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 721492
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/16 v4, 0x12

    const/4 v3, 0x5

    const/4 v2, 0x0

    .line 721493
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 721494
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 721495
    if-eqz v0, :cond_0

    .line 721496
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721497
    invoke-static {p0, p1, v2, p2}, LX/2bt;->a(LX/15i;IILX/0nX;)V

    .line 721498
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 721499
    if-eqz v0, :cond_1

    .line 721500
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721501
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 721502
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 721503
    if-eqz v0, :cond_2

    .line 721504
    const-string v1, "live_video_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721505
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 721506
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 721507
    if-eqz v0, :cond_3

    .line 721508
    const-string v1, "square_header_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721509
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 721510
    :cond_3
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 721511
    if-eqz v0, :cond_4

    .line 721512
    const-string v0, "subscribe_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721513
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 721514
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 721515
    if-eqz v0, :cond_5

    .line 721516
    const-string v1, "video_channel_can_viewer_follow"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721517
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 721518
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 721519
    if-eqz v0, :cond_6

    .line 721520
    const-string v1, "video_channel_can_viewer_pin"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721521
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 721522
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 721523
    if-eqz v0, :cond_7

    .line 721524
    const-string v1, "video_channel_can_viewer_subscribe"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721525
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 721526
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 721527
    if-eqz v0, :cond_8

    .line 721528
    const-string v1, "video_channel_curator_profile"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721529
    invoke-static {p0, v0, p2, p3}, LX/2aw;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 721530
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 721531
    if-eqz v0, :cond_9

    .line 721532
    const-string v1, "video_channel_has_new"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721533
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 721534
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 721535
    if-eqz v0, :cond_a

    .line 721536
    const-string v1, "video_channel_has_viewer_subscribed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721537
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 721538
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 721539
    if-eqz v0, :cond_b

    .line 721540
    const-string v1, "video_channel_is_viewer_following"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721541
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 721542
    :cond_b
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 721543
    if-eqz v0, :cond_c

    .line 721544
    const-string v1, "video_channel_is_viewer_pinned"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721545
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 721546
    :cond_c
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 721547
    if-eqz v0, :cond_d

    .line 721548
    const-string v1, "video_channel_max_new_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721549
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 721550
    :cond_d
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 721551
    if-eqz v0, :cond_e

    .line 721552
    const-string v1, "video_channel_new_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721553
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 721554
    :cond_e
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 721555
    if-eqz v0, :cond_f

    .line 721556
    const-string v1, "video_channel_subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721557
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 721558
    :cond_f
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 721559
    if-eqz v0, :cond_10

    .line 721560
    const-string v1, "video_channel_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721561
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 721562
    :cond_10
    invoke-virtual {p0, p1, v4, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 721563
    if-eqz v0, :cond_11

    .line 721564
    const-string v0, "live_video_subscription_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721565
    const-class v0, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 721566
    :cond_11
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 721567
    if-eqz v0, :cond_12

    .line 721568
    const-string v1, "video_channel_curator"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721569
    invoke-static {p0, v0, p2, p3}, LX/2bR;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 721570
    :cond_12
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 721571
    return-void
.end method
