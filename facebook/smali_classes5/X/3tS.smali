.class public final LX/3tS;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source ""


# instance fields
.field public a:I

.field public b:F

.field public c:Z

.field public d:Z


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 644880
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 644881
    const/4 v0, 0x0

    iput v0, p0, LX/3tS;->a:I

    .line 644882
    return-void
.end method

.method public constructor <init>(LX/3tS;)V
    .locals 1

    .prologue
    .line 644895
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 644896
    const/4 v0, 0x0

    iput v0, p0, LX/3tS;->a:I

    .line 644897
    iget v0, p1, LX/3tS;->a:I

    iput v0, p0, LX/3tS;->a:I

    .line 644898
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 644889
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 644890
    iput v1, p0, LX/3tS;->a:I

    .line 644891
    sget-object v0, LX/3tW;->b:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 644892
    invoke-virtual {v0, v1, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, LX/3tS;->a:I

    .line 644893
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 644894
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 644886
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 644887
    const/4 v0, 0x0

    iput v0, p0, LX/3tS;->a:I

    .line 644888
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .locals 1

    .prologue
    .line 644883
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 644884
    const/4 v0, 0x0

    iput v0, p0, LX/3tS;->a:I

    .line 644885
    return-void
.end method
