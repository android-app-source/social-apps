.class public final LX/4tP;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:LX/3KW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3KW",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static b:LX/3KW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3KW",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static c:LX/3KW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3KW",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static d:LX/3KW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3KW",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static e:LX/3KW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3KW",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static f:LX/3KW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3KW",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const-string v0, "gms:common:stats:connections:level"

    sget v1, LX/1ov;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, LX/3KW;->a(Ljava/lang/String;Ljava/lang/Integer;)LX/3KW;

    move-result-object v0

    sput-object v0, LX/4tP;->a:LX/3KW;

    const-string v0, "gms:common:stats:connections:ignored_calling_processes"

    const-string v1, ""

    invoke-static {v0, v1}, LX/3KW;->a(Ljava/lang/String;Ljava/lang/String;)LX/3KW;

    move-result-object v0

    sput-object v0, LX/4tP;->b:LX/3KW;

    const-string v0, "gms:common:stats:connections:ignored_calling_services"

    const-string v1, ""

    invoke-static {v0, v1}, LX/3KW;->a(Ljava/lang/String;Ljava/lang/String;)LX/3KW;

    move-result-object v0

    sput-object v0, LX/4tP;->c:LX/3KW;

    const-string v0, "gms:common:stats:connections:ignored_target_processes"

    const-string v1, ""

    invoke-static {v0, v1}, LX/3KW;->a(Ljava/lang/String;Ljava/lang/String;)LX/3KW;

    move-result-object v0

    sput-object v0, LX/4tP;->d:LX/3KW;

    const-string v0, "gms:common:stats:connections:ignored_target_services"

    const-string v1, "com.google.android.gms.auth.GetToken"

    invoke-static {v0, v1}, LX/3KW;->a(Ljava/lang/String;Ljava/lang/String;)LX/3KW;

    move-result-object v0

    sput-object v0, LX/4tP;->e:LX/3KW;

    const-string v0, "gms:common:stats:connections:time_out_duration"

    const-wide/32 v2, 0x927c0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, LX/4v5;

    invoke-direct {v2, v0, v1}, LX/4v5;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    move-object v0, v2

    sput-object v0, LX/4tP;->f:LX/3KW;

    return-void
.end method
