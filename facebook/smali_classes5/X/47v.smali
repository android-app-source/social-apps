.class public LX/47v;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 672549
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/io/InputStream;J)J
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    .line 672550
    invoke-static {p0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 672551
    cmp-long v0, p1, v6

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->a(Z)V

    move-wide v0, p1

    .line 672552
    :goto_1
    cmp-long v2, v0, v6

    if-lez v2, :cond_3

    .line 672553
    invoke-virtual {p0, v0, v1}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v2

    .line 672554
    cmp-long v4, v2, v6

    if-lez v4, :cond_1

    .line 672555
    sub-long/2addr v0, v2

    .line 672556
    goto :goto_1

    .line 672557
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 672558
    :cond_1
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    .line 672559
    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    .line 672560
    goto :goto_1

    .line 672561
    :cond_2
    sub-long/2addr p1, v0

    .line 672562
    :cond_3
    return-wide p1
.end method

.method public static a(Ljava/io/InputStream;)[B
    .locals 2

    .prologue
    .line 672563
    invoke-virtual {p0}, Ljava/io/InputStream;->available()I

    move-result v0

    .line 672564
    new-instance v1, LX/47u;

    invoke-direct {v1, v0}, LX/47u;-><init>(I)V

    .line 672565
    invoke-static {p0, v1}, LX/0aP;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 672566
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    move-object v0, v1

    .line 672567
    return-object v0
.end method
