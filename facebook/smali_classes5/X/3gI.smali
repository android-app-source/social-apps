.class public final LX/3gI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ZE;


# instance fields
.field public final synthetic a:LX/3gG;


# direct methods
.method public constructor <init>(LX/3gG;)V
    .locals 0

    .prologue
    .line 624259
    iput-object p1, p0, LX/3gI;->a:LX/3gG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/2Vj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 624266
    iget-object v0, p0, LX/3gI;->a:LX/3gG;

    iget-object v0, v0, LX/3gG;->c:LX/3gH;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "contacts_upload_settings"

    .line 624267
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 624268
    move-object v0, v0

    .line 624269
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    .line 624270
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 624260
    iget-object v0, p0, LX/3gI;->a:LX/3gG;

    iget-object v0, v0, LX/3gG;->d:LX/30I;

    invoke-virtual {v0}, LX/30I;->a()Z

    move-result v1

    .line 624261
    const-string v0, "contacts_upload_settings"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3kP;

    .line 624262
    if-eqz v1, :cond_0

    iget-boolean v0, v0, LX/3kP;->a:Z

    if-nez v0, :cond_0

    .line 624263
    iget-object v0, p0, LX/3gI;->a:LX/3gG;

    iget-object v0, v0, LX/3gG;->d:LX/30I;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/30I;->a(Z)V

    .line 624264
    iget-object v0, p0, LX/3gI;->a:LX/3gG;

    iget-object v0, v0, LX/3gG;->b:LX/2UQ;

    invoke-virtual {v0}, LX/2UQ;->a()V

    .line 624265
    :cond_0
    return-void
.end method
