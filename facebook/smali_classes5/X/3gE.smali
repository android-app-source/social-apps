.class public final LX/3gE;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/2y0;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:LX/2ei;

.field public final synthetic d:LX/2y0;


# direct methods
.method public constructor <init>(LX/2y0;)V
    .locals 1

    .prologue
    .line 624156
    iput-object p1, p0, LX/3gE;->d:LX/2y0;

    .line 624157
    move-object v0, p1

    .line 624158
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 624159
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 624160
    const-string v0, "ExplanationComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 624161
    if-ne p0, p1, :cond_1

    .line 624162
    :cond_0
    :goto_0
    return v0

    .line 624163
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 624164
    goto :goto_0

    .line 624165
    :cond_3
    check-cast p1, LX/3gE;

    .line 624166
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 624167
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 624168
    if-eq v2, v3, :cond_0

    .line 624169
    iget-object v2, p0, LX/3gE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/3gE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/3gE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 624170
    goto :goto_0

    .line 624171
    :cond_5
    iget-object v2, p1, LX/3gE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 624172
    :cond_6
    iget-object v2, p0, LX/3gE;->b:LX/1Pq;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/3gE;->b:LX/1Pq;

    iget-object v3, p1, LX/3gE;->b:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 624173
    goto :goto_0

    .line 624174
    :cond_8
    iget-object v2, p1, LX/3gE;->b:LX/1Pq;

    if-nez v2, :cond_7

    .line 624175
    :cond_9
    iget-object v2, p0, LX/3gE;->c:LX/2ei;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/3gE;->c:LX/2ei;

    iget-object v3, p1, LX/3gE;->c:LX/2ei;

    invoke-virtual {v2, v3}, LX/2ei;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 624176
    goto :goto_0

    .line 624177
    :cond_a
    iget-object v2, p1, LX/3gE;->c:LX/2ei;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
