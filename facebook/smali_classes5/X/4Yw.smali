.class public final LX/4Yw;
.super LX/0ur;
.source ""


# instance fields
.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:I

.field public n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 784835
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 784836
    const/4 v0, 0x0

    iput-object v0, p0, LX/4Yw;->u:LX/0x2;

    .line 784837
    instance-of v0, p0, LX/4Yw;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 784838
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/4Yw;
    .locals 4

    .prologue
    .line 784841
    new-instance v1, LX/4Yw;

    invoke-direct {v1}, LX/4Yw;-><init>()V

    .line 784842
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 784843
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->w()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/4Yw;->b:LX/0Px;

    .line 784844
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->x()Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    move-result-object v0

    iput-object v0, v1, LX/4Yw;->c:Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    .line 784845
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Yw;->d:Ljava/lang/String;

    .line 784846
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->y()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/4Yw;->e:LX/0Px;

    .line 784847
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->E_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Yw;->f:Ljava/lang/String;

    .line 784848
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->z()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Yw;->g:Ljava/lang/String;

    .line 784849
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->F_()J

    move-result-wide v2

    iput-wide v2, v1, LX/4Yw;->h:J

    .line 784850
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->A()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Yw;->i:Ljava/lang/String;

    .line 784851
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->B()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Yw;->j:Ljava/lang/String;

    .line 784852
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->C()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Yw;->k:Ljava/lang/String;

    .line 784853
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->D()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Yw;->l:Ljava/lang/String;

    .line 784854
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->E()I

    move-result v0

    iput v0, v1, LX/4Yw;->m:I

    .line 784855
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->F()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    iput-object v0, v1, LX/4Yw;->n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 784856
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    iput-object v0, v1, LX/4Yw;->o:Lcom/facebook/graphql/model/GraphQLImage;

    .line 784857
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->H()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Yw;->p:Ljava/lang/String;

    .line 784858
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->I()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/4Yw;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 784859
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/4Yw;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 784860
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Yw;->s:Ljava/lang/String;

    .line 784861
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->K()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Yw;->t:Ljava/lang/String;

    .line 784862
    invoke-static {v1, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 784863
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->L_()LX/0x2;

    move-result-object v0

    invoke-virtual {v0}, LX/0x2;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x2;

    iput-object v0, v1, LX/4Yw;->u:LX/0x2;

    .line 784864
    return-object v1
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLStorySet;
    .locals 2

    .prologue
    .line 784839
    new-instance v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLStorySet;-><init>(LX/4Yw;)V

    .line 784840
    return-object v0
.end method
