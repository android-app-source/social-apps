.class public LX/4RG;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 708268
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 33

    .prologue
    .line 708269
    const/16 v25, 0x0

    .line 708270
    const/16 v24, 0x0

    .line 708271
    const/16 v23, 0x0

    .line 708272
    const/16 v22, 0x0

    .line 708273
    const/16 v21, 0x0

    .line 708274
    const/16 v20, 0x0

    .line 708275
    const-wide/16 v18, 0x0

    .line 708276
    const-wide/16 v16, 0x0

    .line 708277
    const-wide/16 v14, 0x0

    .line 708278
    const-wide/16 v12, 0x0

    .line 708279
    const/4 v11, 0x0

    .line 708280
    const/4 v10, 0x0

    .line 708281
    const/4 v9, 0x0

    .line 708282
    const/4 v8, 0x0

    .line 708283
    const/4 v7, 0x0

    .line 708284
    const/4 v6, 0x0

    .line 708285
    const/4 v5, 0x0

    .line 708286
    const/4 v4, 0x0

    .line 708287
    const/4 v3, 0x0

    .line 708288
    const/4 v2, 0x0

    .line 708289
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v26

    sget-object v27, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-eq v0, v1, :cond_16

    .line 708290
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 708291
    const/4 v2, 0x0

    .line 708292
    :goto_0
    return v2

    .line 708293
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v27, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v27

    if-eq v2, v0, :cond_b

    .line 708294
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 708295
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 708296
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 708297
    const-string v27, "cropped_area_image_height_pixels"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_1

    .line 708298
    const/4 v2, 0x1

    .line 708299
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    move/from16 v26, v14

    move v14, v2

    goto :goto_1

    .line 708300
    :cond_1
    const-string v27, "cropped_area_image_width_pixels"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_2

    .line 708301
    const/4 v2, 0x1

    .line 708302
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    move/from16 v25, v13

    move v13, v2

    goto :goto_1

    .line 708303
    :cond_2
    const-string v27, "cropped_area_left_pixels"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_3

    .line 708304
    const/4 v2, 0x1

    .line 708305
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    move/from16 v24, v12

    move v12, v2

    goto :goto_1

    .line 708306
    :cond_3
    const-string v27, "cropped_area_top_pixels"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_4

    .line 708307
    const/4 v2, 0x1

    .line 708308
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    move/from16 v23, v11

    move v11, v2

    goto :goto_1

    .line 708309
    :cond_4
    const-string v27, "full_pano_height_pixels"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_5

    .line 708310
    const/4 v2, 0x1

    .line 708311
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    move/from16 v22, v7

    move v7, v2

    goto :goto_1

    .line 708312
    :cond_5
    const-string v27, "full_pano_width_pixels"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_6

    .line 708313
    const/4 v2, 0x1

    .line 708314
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v15, v6

    move v6, v2

    goto/16 :goto_1

    .line 708315
    :cond_6
    const-string v27, "initial_view_heading_degrees"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_7

    .line 708316
    const/4 v2, 0x1

    .line 708317
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 708318
    :cond_7
    const-string v27, "initial_view_pitch_degrees"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_8

    .line 708319
    const/4 v2, 0x1

    .line 708320
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v20

    move v10, v2

    goto/16 :goto_1

    .line 708321
    :cond_8
    const-string v27, "initial_view_roll_degrees"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_9

    .line 708322
    const/4 v2, 0x1

    .line 708323
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v18

    move v9, v2

    goto/16 :goto_1

    .line 708324
    :cond_9
    const-string v27, "initial_view_vertical_fov_degrees"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 708325
    const/4 v2, 0x1

    .line 708326
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v16

    move v8, v2

    goto/16 :goto_1

    .line 708327
    :cond_a
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 708328
    :cond_b
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 708329
    if-eqz v14, :cond_c

    .line 708330
    const/4 v2, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1, v14}, LX/186;->a(III)V

    .line 708331
    :cond_c
    if-eqz v13, :cond_d

    .line 708332
    const/4 v2, 0x1

    const/4 v13, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1, v13}, LX/186;->a(III)V

    .line 708333
    :cond_d
    if-eqz v12, :cond_e

    .line 708334
    const/4 v2, 0x2

    const/4 v12, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1, v12}, LX/186;->a(III)V

    .line 708335
    :cond_e
    if-eqz v11, :cond_f

    .line 708336
    const/4 v2, 0x3

    const/4 v11, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1, v11}, LX/186;->a(III)V

    .line 708337
    :cond_f
    if-eqz v7, :cond_10

    .line 708338
    const/4 v2, 0x4

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1, v7}, LX/186;->a(III)V

    .line 708339
    :cond_10
    if-eqz v6, :cond_11

    .line 708340
    const/4 v2, 0x5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15, v6}, LX/186;->a(III)V

    .line 708341
    :cond_11
    if-eqz v3, :cond_12

    .line 708342
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 708343
    :cond_12
    if-eqz v10, :cond_13

    .line 708344
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v20

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 708345
    :cond_13
    if-eqz v9, :cond_14

    .line 708346
    const/16 v3, 0x8

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v18

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 708347
    :cond_14
    if-eqz v8, :cond_15

    .line 708348
    const/16 v3, 0x9

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 708349
    :cond_15
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_16
    move/from16 v26, v25

    move/from16 v25, v24

    move/from16 v24, v23

    move/from16 v23, v22

    move/from16 v22, v21

    move/from16 v29, v9

    move v9, v3

    move v3, v5

    move/from16 v30, v11

    move v11, v8

    move v8, v2

    move-wide/from16 v31, v14

    move/from16 v15, v20

    move/from16 v14, v30

    move-wide/from16 v20, v16

    move-wide/from16 v16, v12

    move v13, v10

    move/from16 v12, v29

    move v10, v4

    move-wide/from16 v4, v18

    move-wide/from16 v18, v31

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const-wide/16 v4, 0x0

    .line 708350
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 708351
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 708352
    if-eqz v0, :cond_0

    .line 708353
    const-string v1, "cropped_area_image_height_pixels"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708354
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 708355
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 708356
    if-eqz v0, :cond_1

    .line 708357
    const-string v1, "cropped_area_image_width_pixels"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708358
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 708359
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 708360
    if-eqz v0, :cond_2

    .line 708361
    const-string v1, "cropped_area_left_pixels"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708362
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 708363
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 708364
    if-eqz v0, :cond_3

    .line 708365
    const-string v1, "cropped_area_top_pixels"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708366
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 708367
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 708368
    if-eqz v0, :cond_4

    .line 708369
    const-string v1, "full_pano_height_pixels"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708370
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 708371
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 708372
    if-eqz v0, :cond_5

    .line 708373
    const-string v1, "full_pano_width_pixels"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708374
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 708375
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 708376
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_6

    .line 708377
    const-string v2, "initial_view_heading_degrees"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708378
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 708379
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 708380
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_7

    .line 708381
    const-string v2, "initial_view_pitch_degrees"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708382
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 708383
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 708384
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_8

    .line 708385
    const-string v2, "initial_view_roll_degrees"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708386
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 708387
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 708388
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_9

    .line 708389
    const-string v2, "initial_view_vertical_fov_degrees"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708390
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 708391
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 708392
    return-void
.end method
