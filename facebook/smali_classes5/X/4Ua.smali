.class public LX/4Ua;
.super LX/2Oo;
.source ""


# instance fields
.field public final error:Lcom/facebook/graphql/error/GraphQLError;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/error/GraphQLError;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 741600
    new-instance v0, LX/4UY;

    invoke-direct {v0}, LX/4UY;-><init>()V

    iget v1, p1, Lcom/facebook/graphql/error/GraphQLError;->code:I

    .line 741601
    iput v1, v0, LX/4UY;->a:I

    .line 741602
    move-object v0, v0

    .line 741603
    iget-object v1, p1, Lcom/facebook/graphql/error/GraphQLError;->description:Ljava/lang/String;

    .line 741604
    iput-object v1, v0, LX/4UY;->d:Ljava/lang/String;

    .line 741605
    move-object v0, v0

    .line 741606
    iget-object v1, p1, Lcom/facebook/graphql/error/GraphQLError;->debugInfo:Ljava/lang/String;

    .line 741607
    iput-object v1, v0, LX/4UY;->h:Ljava/lang/String;

    .line 741608
    move-object v0, v0

    .line 741609
    iget-object v1, p1, Lcom/facebook/graphql/error/GraphQLError;->severity:Ljava/lang/String;

    .line 741610
    iput-object v1, v0, LX/4UY;->j:Ljava/lang/String;

    .line 741611
    move-object v0, v0

    .line 741612
    invoke-virtual {v0}, LX/4UY;->a()Lcom/facebook/graphql/error/GraphQLError;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2Oo;-><init>(Lcom/facebook/http/protocol/ApiErrorResult;)V

    .line 741613
    iput-object p1, p0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    .line 741614
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/facebook/http/protocol/ApiErrorResult;
    .locals 1

    .prologue
    .line 741615
    iget-object v0, p0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v0, v0

    .line 741616
    return-object v0
.end method

.method public final synthetic getExtraData()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 741617
    iget-object v0, p0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v0, v0

    .line 741618
    return-object v0
.end method
