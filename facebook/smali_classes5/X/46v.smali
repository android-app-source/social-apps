.class public final LX/46v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:I

.field public final synthetic c:Ljava/util/List;

.field public final synthetic d:LX/46x;


# direct methods
.method public constructor <init>(LX/46x;Landroid/view/View;ILjava/util/List;)V
    .locals 0

    .prologue
    .line 671780
    iput-object p1, p0, LX/46v;->d:LX/46x;

    iput-object p2, p0, LX/46v;->a:Landroid/view/View;

    iput p3, p0, LX/46v;->b:I

    iput-object p4, p0, LX/46v;->c:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 4

    .prologue
    .line 671781
    iget-object v0, p0, LX/46v;->d:LX/46x;

    iget-object v0, v0, LX/46x;->a:Landroid/content/res/Resources;

    iget-object v1, p0, LX/46v;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0tP;->b(Landroid/content/res/Resources;F)I

    move-result v0

    int-to-float v0, v0

    .line 671782
    iget v1, p0, LX/46v;->b:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/16 v0, 0x8

    move v1, v0

    .line 671783
    :goto_0
    iget-object v0, p0, LX/46v;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 671784
    iget-object v3, p0, LX/46v;->a:Landroid/view/View;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 671785
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 671786
    :cond_1
    return-void
.end method
