.class public final LX/4aI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel$LikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel$TopLevelCommentsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 791027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v9, 0x0

    const/4 v2, 0x0

    .line 791028
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 791029
    iget-object v1, p0, LX/4aI;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 791030
    iget-object v3, p0, LX/4aI;->h:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 791031
    iget-object v5, p0, LX/4aI;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel$LikersModel;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 791032
    iget-object v6, p0, LX/4aI;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel$TopLevelCommentsModel;

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 791033
    const/16 v7, 0xa

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 791034
    iget-boolean v7, p0, LX/4aI;->a:Z

    invoke-virtual {v0, v9, v7}, LX/186;->a(IZ)V

    .line 791035
    iget-boolean v7, p0, LX/4aI;->b:Z

    invoke-virtual {v0, v4, v7}, LX/186;->a(IZ)V

    .line 791036
    const/4 v7, 0x2

    iget-boolean v8, p0, LX/4aI;->c:Z

    invoke-virtual {v0, v7, v8}, LX/186;->a(IZ)V

    .line 791037
    const/4 v7, 0x3

    iget-boolean v8, p0, LX/4aI;->d:Z

    invoke-virtual {v0, v7, v8}, LX/186;->a(IZ)V

    .line 791038
    const/4 v7, 0x4

    iget-boolean v8, p0, LX/4aI;->e:Z

    invoke-virtual {v0, v7, v8}, LX/186;->a(IZ)V

    .line 791039
    const/4 v7, 0x5

    iget-boolean v8, p0, LX/4aI;->f:Z

    invoke-virtual {v0, v7, v8}, LX/186;->a(IZ)V

    .line 791040
    const/4 v7, 0x6

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 791041
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 791042
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 791043
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 791044
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 791045
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 791046
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 791047
    invoke-virtual {v1, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 791048
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 791049
    new-instance v1, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;-><init>(LX/15i;)V

    .line 791050
    return-object v1
.end method
