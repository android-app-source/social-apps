.class public LX/3wC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:Landroid/content/res/TypedArray;

.field private c:LX/3wA;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/TypedArray;)V
    .locals 0

    .prologue
    .line 654653
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 654654
    iput-object p1, p0, LX/3wC;->a:Landroid/content/Context;

    .line 654655
    iput-object p2, p0, LX/3wC;->b:Landroid/content/res/TypedArray;

    .line 654656
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/util/AttributeSet;[III)LX/3wC;
    .locals 2

    .prologue
    .line 654651
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 654652
    new-instance v1, LX/3wC;

    invoke-direct {v1, p0, v0}, LX/3wC;-><init>(Landroid/content/Context;Landroid/content/res/TypedArray;)V

    return-object v1
.end method


# virtual methods
.method public final a(II)I
    .locals 1

    .prologue
    .line 654650
    iget-object v0, p0, LX/3wC;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1, p2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    return v0
.end method

.method public final a(I)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 654645
    iget-object v0, p0, LX/3wC;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 654646
    iget-object v0, p0, LX/3wC;->b:Landroid/content/res/TypedArray;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 654647
    if-eqz v0, :cond_0

    .line 654648
    invoke-virtual {p0}, LX/3wC;->c()LX/3wA;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/3wA;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 654649
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/3wC;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(IZ)Z
    .locals 1

    .prologue
    .line 654644
    iget-object v0, p0, LX/3wC;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1, p2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    return v0
.end method

.method public final b(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 654643
    iget-object v0, p0, LX/3wC;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 654630
    iget-object v0, p0, LX/3wC;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 654631
    return-void
.end method

.method public final c(II)I
    .locals 1

    .prologue
    .line 654642
    iget-object v0, p0, LX/3wC;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1, p2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    return v0
.end method

.method public final c()LX/3wA;
    .locals 2

    .prologue
    .line 654636
    iget-object v0, p0, LX/3wC;->c:LX/3wA;

    if-nez v0, :cond_0

    .line 654637
    iget-object v0, p0, LX/3wC;->a:Landroid/content/Context;

    instance-of v0, v0, LX/3w7;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3wC;->a:Landroid/content/Context;

    check-cast v0, LX/3w7;

    .line 654638
    iget-object v1, v0, LX/3w7;->a:LX/3wA;

    move-object v0, v1

    .line 654639
    :goto_0
    iput-object v0, p0, LX/3wC;->c:LX/3wA;

    .line 654640
    :cond_0
    iget-object v0, p0, LX/3wC;->c:LX/3wA;

    return-object v0

    .line 654641
    :cond_1
    new-instance v0, LX/3wA;

    iget-object v1, p0, LX/3wC;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/3wA;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final d(II)I
    .locals 1

    .prologue
    .line 654635
    iget-object v0, p0, LX/3wC;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1, p2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    return v0
.end method

.method public final d(I)Z
    .locals 1

    .prologue
    .line 654634
    iget-object v0, p0, LX/3wC;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    return v0
.end method

.method public final e(II)I
    .locals 1

    .prologue
    .line 654633
    iget-object v0, p0, LX/3wC;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1, p2}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v0

    return v0
.end method

.method public final f(II)I
    .locals 1

    .prologue
    .line 654632
    iget-object v0, p0, LX/3wC;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1, p2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    return v0
.end method
