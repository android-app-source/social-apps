.class public final LX/3ka;
.super LX/13D;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public static final b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public static final c:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static volatile d:LX/3ka;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 633158
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_PYMK_FRIEND_REQUEST_SENT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/3ka;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 633159
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_PYMK_SCROLLED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/3ka;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 633160
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_PYMK_XOUTED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/3ka;->c:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(LX/13J;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 633156
    invoke-direct {p0, p1}, LX/13D;-><init>(LX/13J;)V

    .line 633157
    return-void
.end method

.method public static a(LX/0QB;)LX/3ka;
    .locals 4

    .prologue
    .line 633143
    sget-object v0, LX/3ka;->d:LX/3ka;

    if-nez v0, :cond_1

    .line 633144
    const-class v1, LX/3ka;

    monitor-enter v1

    .line 633145
    :try_start_0
    sget-object v0, LX/3ka;->d:LX/3ka;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 633146
    if-eqz v2, :cond_0

    .line 633147
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 633148
    new-instance p0, LX/3ka;

    const-class v3, LX/13J;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/13J;

    invoke-direct {p0, v3}, LX/3ka;-><init>(LX/13J;)V

    .line 633149
    move-object v0, p0

    .line 633150
    sput-object v0, LX/3ka;->d:LX/3ka;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 633151
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 633152
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 633153
    :cond_1
    sget-object v0, LX/3ka;->d:LX/3ka;

    return-object v0

    .line 633154
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 633155
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 633142
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 633161
    const-string v0, "3279"

    return-object v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 633141
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 633140
    const-string v0, "Feed PYMK"

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 633139
    const/4 v0, 0x1

    return v0
.end method
