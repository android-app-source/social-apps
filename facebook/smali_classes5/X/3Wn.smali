.class public LX/3Wn;
.super LX/35n;
.source ""

# interfaces
.implements LX/35q;


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field public final b:Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 591391
    new-instance v0, LX/3Wo;

    invoke-direct {v0}, LX/3Wo;-><init>()V

    sput-object v0, LX/3Wn;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 591389
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3Wn;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 591390
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 591387
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3Wn;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 591388
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 591392
    const v0, 0x7f0300dd

    invoke-direct {p0, p1, p2, p3, v0}, LX/3Wn;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 591393
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 591372
    invoke-direct {p0, p1, p2, p3, p4}, LX/35n;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 591373
    const v0, 0x7f0d0532

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    iput-object v0, p0, LX/3Wn;->b:Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    .line 591374
    new-instance v1, Landroid/graphics/Paint;

    const/4 v0, 0x1

    invoke-direct {v1, v0}, Landroid/graphics/Paint;-><init>(I)V

    .line 591375
    invoke-virtual {p0}, LX/3Wn;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a05dc

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 591376
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 591377
    const v0, 0x7f0d0533

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 591378
    iget-object v2, p0, LX/3Wn;->b:Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    .line 591379
    iput-object v0, v2, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->f:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 591380
    const v0, 0x7f0d0535

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 591381
    iget-object v2, p0, LX/3Wn;->b:Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    .line 591382
    iput-object v0, v2, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->g:Landroid/widget/TextView;

    .line 591383
    iget-object v0, p0, LX/3Wn;->b:Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    .line 591384
    iput-object v1, v0, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->e:Landroid/graphics/Paint;

    .line 591385
    iget-object v0, p0, LX/3Wn;->b:Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->setIsOverlayVisible(Z)V

    .line 591386
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 591369
    invoke-super {p0}, LX/35n;->a()V

    .line 591370
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/3Wn;->setLargeImageController(LX/1aZ;)V

    .line 591371
    return-void
.end method

.method public setLargeImageAspectRatio(F)V
    .locals 1

    .prologue
    .line 591367
    iget-object v0, p0, LX/3Wn;->b:Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 591368
    return-void
.end method

.method public setLargeImageController(LX/1aZ;)V
    .locals 2
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 591363
    iget-object v1, p0, LX/3Wn;->b:Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->setVisibility(I)V

    .line 591364
    iget-object v0, p0, LX/3Wn;->b:Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 591365
    return-void

    .line 591366
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
