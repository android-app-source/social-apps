.class public LX/3sU;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/3sN;


# instance fields
.field private b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/Runnable;

.field public d:Ljava/lang/Runnable;

.field public e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 643983
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 643984
    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 643985
    new-instance v0, LX/3sT;

    invoke-direct {v0}, LX/3sT;-><init>()V

    sput-object v0, LX/3sU;->a:LX/3sN;

    .line 643986
    :goto_0
    return-void

    .line 643987
    :cond_0
    const/16 v1, 0x12

    if-lt v0, v1, :cond_1

    .line 643988
    new-instance v0, LX/3sS;

    invoke-direct {v0}, LX/3sS;-><init>()V

    sput-object v0, LX/3sU;->a:LX/3sN;

    goto :goto_0

    .line 643989
    :cond_1
    const/16 v1, 0x10

    if-lt v0, v1, :cond_2

    .line 643990
    new-instance v0, LX/3sR;

    invoke-direct {v0}, LX/3sR;-><init>()V

    sput-object v0, LX/3sU;->a:LX/3sN;

    goto :goto_0

    .line 643991
    :cond_2
    const/16 v1, 0xe

    if-lt v0, v1, :cond_3

    .line 643992
    new-instance v0, LX/3sQ;

    invoke-direct {v0}, LX/3sQ;-><init>()V

    sput-object v0, LX/3sU;->a:LX/3sN;

    goto :goto_0

    .line 643993
    :cond_3
    new-instance v0, LX/3sO;

    invoke-direct {v0}, LX/3sO;-><init>()V

    sput-object v0, LX/3sU;->a:LX/3sN;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 643994
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 643995
    iput-object v0, p0, LX/3sU;->c:Ljava/lang/Runnable;

    .line 643996
    iput-object v0, p0, LX/3sU;->d:Ljava/lang/Runnable;

    .line 643997
    const/4 v0, -0x1

    iput v0, p0, LX/3sU;->e:I

    .line 643998
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/3sU;->b:Ljava/lang/ref/WeakReference;

    .line 643999
    return-void
.end method


# virtual methods
.method public final a(F)LX/3sU;
    .locals 2

    .prologue
    .line 644000
    iget-object v0, p0, LX/3sU;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 644001
    sget-object v1, LX/3sU;->a:LX/3sN;

    invoke-interface {v1, p0, v0, p1}, LX/3sN;->a(LX/3sU;Landroid/view/View;F)V

    .line 644002
    :cond_0
    return-object p0
.end method

.method public final a(J)LX/3sU;
    .locals 3

    .prologue
    .line 643965
    iget-object v0, p0, LX/3sU;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 643966
    sget-object v1, LX/3sU;->a:LX/3sN;

    invoke-interface {v1, v0, p1, p2}, LX/3sN;->a(Landroid/view/View;J)V

    .line 643967
    :cond_0
    return-object p0
.end method

.method public final a(LX/3oQ;)LX/3sU;
    .locals 2

    .prologue
    .line 644003
    iget-object v0, p0, LX/3sU;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 644004
    sget-object v1, LX/3sU;->a:LX/3sN;

    invoke-interface {v1, p0, v0, p1}, LX/3sN;->a(LX/3sU;Landroid/view/View;LX/3oQ;)V

    .line 644005
    :cond_0
    return-object p0
.end method

.method public final a(LX/3sb;)LX/3sU;
    .locals 2

    .prologue
    .line 644006
    iget-object v0, p0, LX/3sU;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 644007
    sget-object v1, LX/3sU;->a:LX/3sN;

    invoke-interface {v1, v0, p1}, LX/3sN;->a(Landroid/view/View;LX/3sb;)V

    .line 644008
    :cond_0
    return-object p0
.end method

.method public final a(Landroid/view/animation/Interpolator;)LX/3sU;
    .locals 2

    .prologue
    .line 643977
    iget-object v0, p0, LX/3sU;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 643978
    sget-object v1, LX/3sU;->a:LX/3sN;

    invoke-interface {v1, v0, p1}, LX/3sN;->a(Landroid/view/View;Landroid/view/animation/Interpolator;)V

    .line 643979
    :cond_0
    return-object p0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 643980
    iget-object v0, p0, LX/3sU;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 643981
    sget-object v1, LX/3sU;->a:LX/3sN;

    invoke-interface {v1, p0, v0}, LX/3sN;->a(LX/3sU;Landroid/view/View;)V

    .line 643982
    :cond_0
    return-void
.end method

.method public final b(F)LX/3sU;
    .locals 2

    .prologue
    .line 643974
    iget-object v0, p0, LX/3sU;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 643975
    sget-object v1, LX/3sU;->a:LX/3sN;

    invoke-interface {v1, p0, v0, p1}, LX/3sN;->b(LX/3sU;Landroid/view/View;F)V

    .line 643976
    :cond_0
    return-object p0
.end method

.method public final b(J)LX/3sU;
    .locals 3

    .prologue
    .line 643971
    iget-object v0, p0, LX/3sU;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 643972
    sget-object v1, LX/3sU;->a:LX/3sN;

    invoke-interface {v1, v0, p1, p2}, LX/3sN;->b(Landroid/view/View;J)V

    .line 643973
    :cond_0
    return-object p0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 643968
    iget-object v0, p0, LX/3sU;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 643969
    sget-object v1, LX/3sU;->a:LX/3sN;

    invoke-interface {v1, p0, v0}, LX/3sN;->b(LX/3sU;Landroid/view/View;)V

    .line 643970
    :cond_0
    return-void
.end method

.method public final c(F)LX/3sU;
    .locals 2

    .prologue
    .line 643962
    iget-object v0, p0, LX/3sU;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 643963
    sget-object v1, LX/3sU;->a:LX/3sN;

    invoke-interface {v1, p0, v0, p1}, LX/3sN;->c(LX/3sU;Landroid/view/View;F)V

    .line 643964
    :cond_0
    return-object p0
.end method

.method public final d(F)LX/3sU;
    .locals 2

    .prologue
    .line 643959
    iget-object v0, p0, LX/3sU;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 643960
    sget-object v1, LX/3sU;->a:LX/3sN;

    invoke-interface {v1, p0, v0, p1}, LX/3sN;->d(LX/3sU;Landroid/view/View;F)V

    .line 643961
    :cond_0
    return-object p0
.end method

.method public final e(F)LX/3sU;
    .locals 2

    .prologue
    .line 643956
    iget-object v0, p0, LX/3sU;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 643957
    sget-object v1, LX/3sU;->a:LX/3sN;

    invoke-interface {v1, p0, v0, p1}, LX/3sN;->e(LX/3sU;Landroid/view/View;F)V

    .line 643958
    :cond_0
    return-object p0
.end method
