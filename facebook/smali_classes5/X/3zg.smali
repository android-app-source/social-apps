.class public final enum LX/3zg;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3zg;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3zg;

.field public static final enum IMMERSIVE:LX/3zg;

.field public static final enum MAINTAB:LX/3zg;


# instance fields
.field private typeName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 662582
    new-instance v0, LX/3zg;

    const-string v1, "MAINTAB"

    const-string v2, "maintab"

    invoke-direct {v0, v1, v3, v2}, LX/3zg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3zg;->MAINTAB:LX/3zg;

    .line 662583
    new-instance v0, LX/3zg;

    const-string v1, "IMMERSIVE"

    const-string v2, "immersive"

    invoke-direct {v0, v1, v4, v2}, LX/3zg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3zg;->IMMERSIVE:LX/3zg;

    .line 662584
    const/4 v0, 0x2

    new-array v0, v0, [LX/3zg;

    sget-object v1, LX/3zg;->MAINTAB:LX/3zg;

    aput-object v1, v0, v3

    sget-object v1, LX/3zg;->IMMERSIVE:LX/3zg;

    aput-object v1, v0, v4

    sput-object v0, LX/3zg;->$VALUES:[LX/3zg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 662585
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 662586
    iput-object p3, p0, LX/3zg;->typeName:Ljava/lang/String;

    .line 662587
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3zg;
    .locals 1

    .prologue
    .line 662581
    const-class v0, LX/3zg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3zg;

    return-object v0
.end method

.method public static values()[LX/3zg;
    .locals 1

    .prologue
    .line 662580
    sget-object v0, LX/3zg;->$VALUES:[LX/3zg;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3zg;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 662579
    iget-object v0, p0, LX/3zg;->typeName:Ljava/lang/String;

    return-object v0
.end method
