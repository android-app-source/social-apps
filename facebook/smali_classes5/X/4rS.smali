.class public LX/4rS;
.super LX/4pV;
.source ""


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final _property:LX/2Ax;


# direct methods
.method public constructor <init>(LX/4qt;LX/2Ax;)V
    .locals 1

    .prologue
    .line 815863
    iget-object v0, p1, LX/4qt;->c:Ljava/lang/Class;

    move-object v0, v0

    .line 815864
    invoke-direct {p0, v0, p2}, LX/4rS;-><init>(Ljava/lang/Class;LX/2Ax;)V

    .line 815865
    return-void
.end method

.method private constructor <init>(Ljava/lang/Class;LX/2Ax;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "LX/2Ax;",
            ")V"
        }
    .end annotation

    .prologue
    .line 815860
    invoke-direct {p0, p1}, LX/4pV;-><init>(Ljava/lang/Class;)V

    .line 815861
    iput-object p2, p0, LX/4rS;->_property:LX/2Ax;

    .line 815862
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/4pR;
    .locals 3

    .prologue
    .line 815859
    new-instance v0, LX/4pR;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    iget-object v2, p0, LX/4pT;->_scope:Ljava/lang/Class;

    invoke-direct {v0, v1, v2, p1}, LX/4pR;-><init>(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Object;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Class;)LX/4pS;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/4pS",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 815858
    iget-object v0, p0, LX/4pT;->_scope:Ljava/lang/Class;

    if-ne p1, v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/4rS;

    iget-object v1, p0, LX/4rS;->_property:LX/2Ax;

    invoke-direct {v0, p1, v1}, LX/4rS;-><init>(Ljava/lang/Class;LX/2Ax;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/4pS;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4pS",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 815847
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 815848
    check-cast p1, LX/4rS;

    .line 815849
    invoke-virtual {p1}, LX/4pS;->a()Ljava/lang/Class;

    move-result-object v1

    iget-object v2, p0, LX/4pT;->_scope:Ljava/lang/Class;

    if-ne v1, v2, :cond_0

    .line 815850
    iget-object v1, p1, LX/4rS;->_property:LX/2Ax;

    iget-object v2, p0, LX/4rS;->_property:LX/2Ax;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 815851
    :cond_0
    return v0
.end method

.method public final b()LX/4pS;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4pS",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 815857
    return-object p0
.end method

.method public final b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 815852
    :try_start_0
    iget-object v0, p0, LX/4rS;->_property:LX/2Ax;

    invoke-virtual {v0, p1}, LX/2Ax;->a(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 815853
    :catch_0
    move-exception v0

    .line 815854
    throw v0

    .line 815855
    :catch_1
    move-exception v0

    .line 815856
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Problem accessing property \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/4rS;->_property:LX/2Ax;

    invoke-virtual {v3}, LX/2Ax;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\': "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
