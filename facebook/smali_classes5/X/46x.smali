.class public LX/46x;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 671796
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 671797
    iput-object p1, p0, LX/46x;->a:Landroid/content/res/Resources;

    .line 671798
    return-void
.end method

.method public static b(LX/0QB;)LX/46x;
    .locals 2

    .prologue
    .line 671799
    new-instance v1, LX/46x;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-direct {v1, v0}, LX/46x;-><init>(Landroid/content/res/Resources;)V

    .line 671800
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/view/View;ILjava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 671801
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/46v;

    invoke-direct {v1, p0, p1, p2, p3}, LX/46v;-><init>(LX/46x;Landroid/view/View;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 671802
    return-void
.end method

.method public final a(Landroid/view/View;ILjava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 671803
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v3

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Expected linkedIds and smallDimenIds list lengths to match"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 671804
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result v3

    if-ne v0, v3, :cond_1

    :goto_1
    const-string v0, "Expected linkedIds and normalDimenIds list lengths to match"

    invoke-static {v1, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 671805
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v7

    new-instance v0, LX/46w;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move v4, p2

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/46w;-><init>(LX/46x;Landroid/view/View;Ljava/util/List;ILjava/util/List;Ljava/util/List;)V

    invoke-virtual {v7, v0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 671806
    return-void

    :cond_0
    move v0, v2

    .line 671807
    goto :goto_0

    :cond_1
    move v1, v2

    .line 671808
    goto :goto_1
.end method
