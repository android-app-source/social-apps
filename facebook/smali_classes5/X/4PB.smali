.class public LX/4PB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 699710
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 699711
    const/4 v10, 0x0

    .line 699712
    const/4 v9, 0x0

    .line 699713
    const/4 v8, 0x0

    .line 699714
    const/4 v7, 0x0

    .line 699715
    const/4 v6, 0x0

    .line 699716
    const/4 v5, 0x0

    .line 699717
    const/4 v4, 0x0

    .line 699718
    const/4 v3, 0x0

    .line 699719
    const/4 v2, 0x0

    .line 699720
    const/4 v1, 0x0

    .line 699721
    const/4 v0, 0x0

    .line 699722
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 699723
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 699724
    const/4 v0, 0x0

    .line 699725
    :goto_0
    return v0

    .line 699726
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 699727
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_a

    .line 699728
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 699729
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 699730
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 699731
    const-string v12, "address"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 699732
    invoke-static {p0, p1}, LX/2t0;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 699733
    :cond_2
    const-string v12, "city"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 699734
    invoke-static {p0, p1}, LX/2bc;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 699735
    :cond_3
    const-string v12, "city_name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 699736
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 699737
    :cond_4
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 699738
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 699739
    :cond_5
    const-string v12, "is_default"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 699740
    const/4 v1, 0x1

    .line 699741
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v6

    goto :goto_1

    .line 699742
    :cond_6
    const-string v12, "is_messenger_eligible"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 699743
    const/4 v0, 0x1

    .line 699744
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v5

    goto :goto_1

    .line 699745
    :cond_7
    const-string v12, "label"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 699746
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 699747
    :cond_8
    const-string v12, "region_name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 699748
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 699749
    :cond_9
    const-string v12, "url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 699750
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 699751
    :cond_a
    const/16 v11, 0xa

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 699752
    const/4 v11, 0x1

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 699753
    const/4 v10, 0x2

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 699754
    const/4 v9, 0x3

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 699755
    const/4 v8, 0x4

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 699756
    if-eqz v1, :cond_b

    .line 699757
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v6}, LX/186;->a(IZ)V

    .line 699758
    :cond_b
    if-eqz v0, :cond_c

    .line 699759
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 699760
    :cond_c
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 699761
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 699762
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 699763
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 699764
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 699765
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 699766
    if-eqz v0, :cond_0

    .line 699767
    const-string v1, "address"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699768
    invoke-static {p0, v0, p2}, LX/2t0;->a(LX/15i;ILX/0nX;)V

    .line 699769
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 699770
    if-eqz v0, :cond_1

    .line 699771
    const-string v1, "city"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699772
    invoke-static {p0, v0, p2, p3}, LX/2bc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 699773
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 699774
    if-eqz v0, :cond_2

    .line 699775
    const-string v1, "city_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699776
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 699777
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 699778
    if-eqz v0, :cond_3

    .line 699779
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699780
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 699781
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 699782
    if-eqz v0, :cond_4

    .line 699783
    const-string v1, "is_default"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699784
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 699785
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 699786
    if-eqz v0, :cond_5

    .line 699787
    const-string v1, "is_messenger_eligible"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699788
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 699789
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 699790
    if-eqz v0, :cond_6

    .line 699791
    const-string v1, "label"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699792
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 699793
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 699794
    if-eqz v0, :cond_7

    .line 699795
    const-string v1, "region_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699796
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 699797
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 699798
    if-eqz v0, :cond_8

    .line 699799
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699800
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 699801
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 699802
    return-void
.end method
