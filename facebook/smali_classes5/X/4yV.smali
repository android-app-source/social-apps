.class public final LX/4yV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public final counts:[I

.field public final elements:[Ljava/lang/Object;


# direct methods
.method public constructor <init>(LX/1M1;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1M1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 821819
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 821820
    invoke-interface {p1}, LX/1M1;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    .line 821821
    new-array v1, v0, [Ljava/lang/Object;

    iput-object v1, p0, LX/4yV;->elements:[Ljava/lang/Object;

    .line 821822
    new-array v0, v0, [I

    iput-object v0, p0, LX/4yV;->counts:[I

    .line 821823
    const/4 v0, 0x0

    .line 821824
    invoke-interface {p1}, LX/1M1;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4wx;

    .line 821825
    iget-object v3, p0, LX/4yV;->elements:[Ljava/lang/Object;

    invoke-virtual {v0}, LX/4wx;->a()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v1

    .line 821826
    iget-object v3, p0, LX/4yV;->counts:[I

    invoke-virtual {v0}, LX/4wx;->b()I

    move-result v0

    aput v0, v3, v1

    .line 821827
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 821828
    goto :goto_0

    .line 821829
    :cond_0
    return-void
.end method


# virtual methods
.method public readResolve()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 821830
    iget-object v0, p0, LX/4yV;->elements:[Ljava/lang/Object;

    array-length v0, v0

    invoke-static {v0}, LX/4z2;->a(I)LX/4z2;

    move-result-object v1

    .line 821831
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, LX/4yV;->elements:[Ljava/lang/Object;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 821832
    iget-object v2, p0, LX/4yV;->elements:[Ljava/lang/Object;

    aget-object v2, v2, v0

    iget-object v3, p0, LX/4yV;->counts:[I

    aget v3, v3, v0

    invoke-virtual {v1, v2, v3}, LX/1M0;->a(Ljava/lang/Object;I)I

    .line 821833
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 821834
    :cond_0
    invoke-static {v1}, LX/4yO;->a(Ljava/lang/Iterable;)LX/4yO;

    move-result-object v0

    return-object v0
.end method
