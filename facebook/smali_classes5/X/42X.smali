.class public final LX/42X;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 667388
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 667389
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 667390
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 667391
    const/4 v2, 0x0

    .line 667392
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_6

    .line 667393
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 667394
    :goto_1
    move v1, v2

    .line 667395
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 667396
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 667397
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 667398
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 667399
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 667400
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 667401
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_2

    if-eqz v5, :cond_2

    .line 667402
    const-string v6, "community"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 667403
    const/4 v5, 0x0

    .line 667404
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_d

    .line 667405
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 667406
    :goto_3
    move v4, v5

    .line 667407
    goto :goto_2

    .line 667408
    :cond_3
    const-string v6, "email"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 667409
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_2

    .line 667410
    :cond_4
    const-string v6, "name"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 667411
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_2

    .line 667412
    :cond_5
    const/4 v5, 0x3

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 667413
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 667414
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 667415
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 667416
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_6
    move v1, v2

    move v3, v2

    move v4, v2

    goto :goto_2

    .line 667417
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 667418
    :cond_8
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_c

    .line 667419
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 667420
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 667421
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_8

    if-eqz v9, :cond_8

    .line 667422
    const-string v10, "login_identifier"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 667423
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_4

    .line 667424
    :cond_9
    const-string v10, "logo"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 667425
    const/4 v9, 0x0

    .line 667426
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v10, :cond_11

    .line 667427
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 667428
    :goto_5
    move v7, v9

    .line 667429
    goto :goto_4

    .line 667430
    :cond_a
    const-string v10, "name"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 667431
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_4

    .line 667432
    :cond_b
    const-string v10, "subdomain"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 667433
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_4

    .line 667434
    :cond_c
    const/4 v9, 0x4

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 667435
    invoke-virtual {p1, v5, v8}, LX/186;->b(II)V

    .line 667436
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v7}, LX/186;->b(II)V

    .line 667437
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v6}, LX/186;->b(II)V

    .line 667438
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 667439
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_3

    :cond_d
    move v4, v5

    move v6, v5

    move v7, v5

    move v8, v5

    goto :goto_4

    .line 667440
    :cond_e
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 667441
    :cond_f
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_10

    .line 667442
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 667443
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 667444
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_f

    if-eqz v10, :cond_f

    .line 667445
    const-string v11, "uri"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 667446
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_6

    .line 667447
    :cond_10
    const/4 v10, 0x1

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 667448
    invoke-virtual {p1, v9, v7}, LX/186;->b(II)V

    .line 667449
    invoke-virtual {p1}, LX/186;->d()I

    move-result v9

    goto :goto_5

    :cond_11
    move v7, v9

    goto :goto_6
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 667450
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 667451
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 667452
    if-eqz v0, :cond_5

    .line 667453
    const-string v1, "community"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 667454
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 667455
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 667456
    if-eqz v1, :cond_0

    .line 667457
    const-string v2, "login_identifier"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 667458
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 667459
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 667460
    if-eqz v1, :cond_2

    .line 667461
    const-string v2, "logo"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 667462
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 667463
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 667464
    if-eqz v2, :cond_1

    .line 667465
    const-string p3, "uri"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 667466
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 667467
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 667468
    :cond_2
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 667469
    if-eqz v1, :cond_3

    .line 667470
    const-string v2, "name"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 667471
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 667472
    :cond_3
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 667473
    if-eqz v1, :cond_4

    .line 667474
    const-string v2, "subdomain"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 667475
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 667476
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 667477
    :cond_5
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 667478
    if-eqz v0, :cond_6

    .line 667479
    const-string v1, "email"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 667480
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 667481
    :cond_6
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 667482
    if-eqz v0, :cond_7

    .line 667483
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 667484
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 667485
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 667486
    return-void
.end method
