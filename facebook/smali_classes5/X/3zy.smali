.class public LX/3zy;
.super LX/2DD;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2DD",
        "<",
        "Ljava/io/ByteArrayOutputStream;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(IILX/0mm;LX/0Zh;)V
    .locals 0

    .prologue
    .line 662808
    invoke-direct {p0, p1, p2, p3, p4}, LX/2DD;-><init>(IILX/0mm;LX/0Zh;)V

    .line 662809
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/2Db;
    .locals 3
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 662810
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 662811
    invoke-static {}, LX/2D8;->a()LX/2D7;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/2D7;->a(Ljava/lang/Object;)LX/2DZ;

    move-result-object v1

    .line 662812
    invoke-virtual {v1, p0}, LX/2DZ;->d(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 662813
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Couldn\'t lock newly created batch"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 662814
    :cond_0
    new-instance v2, LX/3zx;

    invoke-direct {v2, p0, v0, v1}, LX/3zx;-><init>(LX/3zy;Ljava/io/ByteArrayOutputStream;LX/2DZ;)V

    return-object v2
.end method

.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 662815
    iget-object v0, p0, LX/2DD;->b:LX/2Db;

    if-nez v0, :cond_0

    .line 662816
    const/4 v0, 0x0

    .line 662817
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2DD;->b:LX/2Db;

    check-cast v0, LX/3zx;

    iget-object v0, v0, LX/3zx;->a:Ljava/io/ByteArrayOutputStream;

    goto :goto_0
.end method
