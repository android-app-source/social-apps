.class public final LX/3fe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/3gu;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/3gu;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 622788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 622789
    iput-object p1, p0, LX/3fe;->a:LX/0QB;

    .line 622790
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 622791
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/3fe;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 622792
    packed-switch p2, :pswitch_data_0

    .line 622793
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 622794
    :pswitch_0
    new-instance v0, LX/3gt;

    invoke-direct {v0}, LX/3gt;-><init>()V

    .line 622795
    move-object v0, v0

    .line 622796
    move-object v0, v0

    .line 622797
    :goto_0
    return-object v0

    .line 622798
    :pswitch_1
    new-instance v0, LX/3gv;

    invoke-direct {v0}, LX/3gv;-><init>()V

    .line 622799
    move-object v0, v0

    .line 622800
    move-object v0, v0

    .line 622801
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 622802
    const/4 v0, 0x2

    return v0
.end method
