.class public LX/4Al;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final TAG:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final bindingMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public autoDispose:Z

.field public autoReset:Z

.field public blueService:LX/1mL;

.field private calledBind:Z

.field public callerContext:Lcom/facebook/common/callercontext/CallerContext;

.field public final context:Landroid/content/Context;

.field public disposed:Z

.field public fireAndForget:Z

.field public handler:Landroid/os/Handler;

.field private final mContextForService:Landroid/content/Context;

.field private final mExecutorService:Ljava/util/concurrent/ExecutorService;

.field private final mProcessUtil:LX/0VT;

.field private final mViewerContextManager:LX/0SI;

.field public onCompletedListener:LX/4Ae;

.field public onProgressListener:LX/4Ag;

.field public operationId:Ljava/lang/String;

.field public operationProgressIndicator:LX/4At;

.field public operationState:LX/4Ak;

.field public operationType:Ljava/lang/String;

.field public param:Landroid/os/Bundle;

.field public registeredForCompletion:Z

.field private final serviceConnection:LX/4Aj;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 677056
    const-class v0, LX/4Al;

    sput-object v0, LX/4Al;->TAG:Ljava/lang/Class;

    .line 677057
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    sput-object v0, LX/4Al;->bindingMap:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;LX/0SI;LX/0VT;)V
    .locals 2
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 677047
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 677048
    sget-object v0, LX/4Ak;->INIT:LX/4Ak;

    iput-object v0, p0, LX/4Al;->operationState:LX/4Ak;

    .line 677049
    iput-object p1, p0, LX/4Al;->context:Landroid/content/Context;

    .line 677050
    new-instance v0, LX/4Aj;

    invoke-direct {v0, p0}, LX/4Aj;-><init>(LX/4Al;)V

    iput-object v0, p0, LX/4Al;->serviceConnection:LX/4Aj;

    .line 677051
    iput-object p2, p0, LX/4Al;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 677052
    iput-object p3, p0, LX/4Al;->mViewerContextManager:LX/0SI;

    .line 677053
    invoke-static {p1}, LX/0WH;->b(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/4Al;->mContextForService:Landroid/content/Context;

    .line 677054
    iput-object p4, p0, LX/4Al;->mProcessUtil:LX/0VT;

    .line 677055
    return-void
.end method

.method public static beginShowingProgress(LX/4Al;)V
    .locals 1

    .prologue
    .line 677044
    iget-object v0, p0, LX/4Al;->operationProgressIndicator:LX/4At;

    if-eqz v0, :cond_0

    .line 677045
    iget-object v0, p0, LX/4Al;->operationProgressIndicator:LX/4At;

    invoke-virtual {v0}, LX/4At;->beginShowingProgress()V

    .line 677046
    :cond_0
    return-void
.end method

.method public static bindToService(LX/4Al;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 677035
    iget-object v0, p0, LX/4Al;->blueService:LX/1mL;

    if-eqz v0, :cond_1

    .line 677036
    invoke-static {p0}, LX/4Al;->maybeStartAndRegister(LX/4Al;)V

    .line 677037
    :cond_0
    :goto_0
    return-void

    .line 677038
    :cond_1
    iget-boolean v0, p0, LX/4Al;->calledBind:Z

    if-nez v0, :cond_0

    .line 677039
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/4Al;->context:Landroid/content/Context;

    const-class v2, Lcom/facebook/fbservice/service/DefaultBlueService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 677040
    iget-object v1, p0, LX/4Al;->mContextForService:Landroid/content/Context;

    iget-object v2, p0, LX/4Al;->serviceConnection:LX/4Aj;

    const v3, -0x23df1427

    invoke-static {v1, v0, v2, v4, v3}, LX/04O;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 677041
    iput-boolean v4, p0, LX/4Al;->calledBind:Z

    goto :goto_0

    .line 677042
    :cond_2
    sget-object v0, LX/1nY;->ORCA_SERVICE_IPC_FAILURE:LX/1nY;

    const-string v1, "Bind to BlueService failed"

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 677043
    invoke-static {p0, v0}, LX/4Al;->callOnOperationCompleted(LX/4Al;Lcom/facebook/fbservice/service/OperationResult;)V

    goto :goto_0
.end method

.method public static callOnOperationCompleted(LX/4Al;Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 1

    .prologue
    .line 677031
    iget-boolean v0, p0, LX/4Al;->fireAndForget:Z

    if-eqz v0, :cond_0

    .line 677032
    invoke-virtual {p0}, LX/4Al;->dispose()V

    .line 677033
    :goto_0
    return-void

    .line 677034
    :cond_0
    new-instance v0, Lcom/facebook/fbservice/ops/BlueServiceOperation$2;

    invoke-direct {v0, p0, p1}, Lcom/facebook/fbservice/ops/BlueServiceOperation$2;-><init>(LX/4Al;Lcom/facebook/fbservice/service/OperationResult;)V

    invoke-static {p0, v0}, LX/4Al;->postToCallbackThread(LX/4Al;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static createInstance__com_facebook_fbservice_ops_BlueServiceOperation__INJECTED_BY_TemplateInjector(LX/0QB;)LX/4Al;
    .locals 5

    .prologue
    .line 677029
    new-instance v4, LX/4Al;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v2

    check-cast v2, LX/0SI;

    invoke-static {p0}, LX/0VT;->a(LX/0QB;)LX/0VT;

    move-result-object v3

    check-cast v3, LX/0VT;

    invoke-direct {v4, v0, v1, v2, v3}, LX/4Al;-><init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;LX/0SI;LX/0VT;)V

    .line 677030
    return-object v4
.end method

.method public static maybeStartAndRegister(LX/4Al;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 677007
    iget-object v0, p0, LX/4Al;->operationState:LX/4Ak;

    sget-object v3, LX/4Ak;->READY_TO_QUEUE:LX/4Ak;

    if-ne v0, v3, :cond_5

    .line 677008
    iget-object v0, p0, LX/4Al;->operationType:Ljava/lang/String;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Null operation type"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 677009
    iget-object v0, p0, LX/4Al;->operationId:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    const-string v3, "Non-null operation id"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 677010
    iget-boolean v0, p0, LX/4Al;->registeredForCompletion:Z

    if-nez v0, :cond_3

    :goto_2
    const-string v0, "Registered for completion and haven\'t yet sent"

    invoke-static {v1, v0}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 677011
    :try_start_0
    iget-object v0, p0, LX/4Al;->blueService:LX/1mL;

    iget-object v1, p0, LX/4Al;->operationType:Ljava/lang/String;

    iget-object v2, p0, LX/4Al;->param:Landroid/os/Bundle;

    const/4 v3, 0x0

    iget-object v4, p0, LX/4Al;->callerContext:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v0, v1, v2, v3, v4}, LX/1mL;->startOperation(Ljava/lang/String;Landroid/os/Bundle;ZLcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/4Al;->operationId:Ljava/lang/String;

    .line 677012
    iget-object v0, p0, LX/4Al;->blueService:LX/1mL;

    if-nez v0, :cond_4

    .line 677013
    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 677014
    :catch_0
    sget-object v0, LX/1nY;->ORCA_SERVICE_IPC_FAILURE:LX/1nY;

    const-string v1, "BlueService.<method> or registerCompletionHandler failed"

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 677015
    invoke-static {p0, v0}, LX/4Al;->callOnOperationCompleted(LX/4Al;Lcom/facebook/fbservice/service/OperationResult;)V

    .line 677016
    :cond_0
    :goto_3
    return-void

    :cond_1
    move v0, v2

    .line 677017
    goto :goto_0

    :cond_2
    move v0, v2

    .line 677018
    goto :goto_1

    :cond_3
    move v1, v2

    .line 677019
    goto :goto_2

    .line 677020
    :cond_4
    :try_start_1
    invoke-direct {p0}, LX/4Al;->registerCompletionHandler()V

    .line 677021
    sget-object v0, LX/4Ak;->OPERATION_QUEUED:LX/4Ak;

    iput-object v0, p0, LX/4Al;->operationState:LX/4Ak;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 677022
    :cond_5
    iget-object v0, p0, LX/4Al;->operationState:LX/4Ak;

    sget-object v3, LX/4Ak;->OPERATION_QUEUED:LX/4Ak;

    if-ne v0, v3, :cond_0

    .line 677023
    iget-object v0, p0, LX/4Al;->operationId:Ljava/lang/String;

    if-eqz v0, :cond_6

    :goto_4
    const-string v0, "null operation id"

    invoke-static {v1, v0}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 677024
    iget-boolean v0, p0, LX/4Al;->registeredForCompletion:Z

    if-nez v0, :cond_0

    .line 677025
    :try_start_2
    invoke-direct {p0}, LX/4Al;->registerCompletionHandler()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    .line 677026
    :catch_1
    sget-object v0, LX/1nY;->ORCA_SERVICE_IPC_FAILURE:LX/1nY;

    const-string v1, "BlueService.registerCompletionHandler failed"

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 677027
    invoke-static {p0, v0}, LX/4Al;->callOnOperationCompleted(LX/4Al;Lcom/facebook/fbservice/service/OperationResult;)V

    goto :goto_3

    :cond_6
    move v1, v2

    .line 677028
    goto :goto_4
.end method

.method public static postToCallbackThread(LX/4Al;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 677003
    iget-object v0, p0, LX/4Al;->handler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 677004
    iget-object v0, p0, LX/4Al;->handler:Landroid/os/Handler;

    const v1, 0x12557356

    invoke-static {v0, p1, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 677005
    :goto_0
    return-void

    .line 677006
    :cond_0
    iget-object v0, p0, LX/4Al;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    const v1, 0x538a7a99

    invoke-static {v0, p1, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method private registerCompletionHandler()V
    .locals 3

    .prologue
    .line 676997
    iget-object v0, p0, LX/4Al;->blueService:LX/1mL;

    iget-object v1, p0, LX/4Al;->operationId:Ljava/lang/String;

    new-instance v2, LX/4Ai;

    invoke-direct {v2, p0}, LX/4Ai;-><init>(LX/4Al;)V

    invoke-interface {v0, v1, v2}, LX/1mL;->registerCompletionHandler(Ljava/lang/String;LX/1qB;)Z

    move-result v0

    .line 676998
    if-eqz v0, :cond_0

    .line 676999
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4Al;->registeredForCompletion:Z

    .line 677000
    :goto_0
    return-void

    .line 677001
    :cond_0
    sget-object v0, LX/1nY;->ORCA_SERVICE_IPC_FAILURE:LX/1nY;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown operation: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/4Al;->operationId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 677002
    invoke-static {p0, v0}, LX/4Al;->callOnOperationCompleted(LX/4Al;Lcom/facebook/fbservice/service/OperationResult;)V

    goto :goto_0
.end method

.method private safeUnbind()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 676991
    iget-boolean v0, p0, LX/4Al;->calledBind:Z

    if-eqz v0, :cond_0

    .line 676992
    :try_start_0
    iget-object v0, p0, LX/4Al;->mContextForService:Landroid/content/Context;

    iget-object v1, p0, LX/4Al;->serviceConnection:LX/4Aj;

    const v2, 0x68dd1370

    invoke-static {v0, v1, v2}, LX/04O;->a(Landroid/content/Context;Landroid/content/ServiceConnection;I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 676993
    :goto_0
    iput-boolean v5, p0, LX/4Al;->calledBind:Z

    .line 676994
    :cond_0
    return-void

    .line 676995
    :catch_0
    move-exception v0

    .line 676996
    sget-object v1, LX/4Al;->TAG:Ljava/lang/Class;

    const-string v2, "Exception unbinding %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, LX/4Al;->operationType:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static stopShowingProgress(LX/4Al;)V
    .locals 1

    .prologue
    .line 676988
    iget-object v0, p0, LX/4Al;->operationProgressIndicator:LX/4At;

    if-eqz v0, :cond_0

    .line 676989
    iget-object v0, p0, LX/4Al;->operationProgressIndicator:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 676990
    :cond_0
    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 677058
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4Al;->disposed:Z

    .line 677059
    invoke-direct {p0}, LX/4Al;->safeUnbind()V

    .line 677060
    iput-object v1, p0, LX/4Al;->blueService:LX/1mL;

    .line 677061
    iput-object v1, p0, LX/4Al;->onProgressListener:LX/4Ag;

    .line 677062
    iput-object v1, p0, LX/4Al;->onCompletedListener:LX/4Ae;

    .line 677063
    invoke-static {p0}, LX/4Al;->stopShowingProgress(LX/4Al;)V

    .line 677064
    return-void
.end method

.method public getAutoDispose()Z
    .locals 1

    .prologue
    .line 676951
    iget-boolean v0, p0, LX/4Al;->autoDispose:Z

    return v0
.end method

.method public getAutoReset()Z
    .locals 1

    .prologue
    .line 676952
    iget-boolean v0, p0, LX/4Al;->autoReset:Z

    return v0
.end method

.method public getCallerContext()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 676953
    iget-object v0, p0, LX/4Al;->callerContext:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method

.method public getFireAndForget()Z
    .locals 1

    .prologue
    .line 676954
    iget-boolean v0, p0, LX/4Al;->fireAndForget:Z

    return v0
.end method

.method public getOnCompletedListener()LX/4Ae;
    .locals 1

    .prologue
    .line 676955
    iget-object v0, p0, LX/4Al;->onCompletedListener:LX/4Ae;

    return-object v0
.end method

.method public getOnProgressListener()LX/4Ag;
    .locals 1

    .prologue
    .line 676956
    iget-object v0, p0, LX/4Al;->onProgressListener:LX/4Ag;

    return-object v0
.end method

.method public getOperationType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 676957
    iget-object v0, p0, LX/4Al;->operationType:Ljava/lang/String;

    return-object v0
.end method

.method public getParam()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 676958
    iget-object v0, p0, LX/4Al;->param:Landroid/os/Bundle;

    return-object v0
.end method

.method public reset()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 676940
    iget-object v0, p0, LX/4Al;->operationState:LX/4Ak;

    sget-object v2, LX/4Ak;->INIT:LX/4Ak;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, LX/4Al;->operationState:LX/4Ak;

    sget-object v2, LX/4Ak;->COMPLETED:LX/4Ak;

    if-ne v0, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 676941
    sget-object v0, LX/4Ak;->INIT:LX/4Ak;

    iput-object v0, p0, LX/4Al;->operationState:LX/4Ak;

    .line 676942
    iput-object v3, p0, LX/4Al;->operationType:Ljava/lang/String;

    .line 676943
    iput-object v3, p0, LX/4Al;->param:Landroid/os/Bundle;

    .line 676944
    iput-object v3, p0, LX/4Al;->callerContext:Lcom/facebook/common/callercontext/CallerContext;

    .line 676945
    iput-object v3, p0, LX/4Al;->operationId:Ljava/lang/String;

    .line 676946
    iput-boolean v1, p0, LX/4Al;->registeredForCompletion:Z

    .line 676947
    invoke-direct {p0}, LX/4Al;->safeUnbind()V

    .line 676948
    iput-object v3, p0, LX/4Al;->blueService:LX/1mL;

    .line 676949
    return-void

    :cond_1
    move v0, v1

    .line 676950
    goto :goto_0
.end method

.method public setAutoDispose(Z)V
    .locals 0

    .prologue
    .line 676986
    iput-boolean p1, p0, LX/4Al;->autoDispose:Z

    .line 676987
    return-void
.end method

.method public setOperationProgressIndicator(LX/4At;)V
    .locals 2

    .prologue
    .line 676959
    iget-object v0, p0, LX/4Al;->operationState:LX/4Ak;

    sget-object v1, LX/4Ak;->READY_TO_QUEUE:LX/4Ak;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/4Al;->operationState:LX/4Ak;

    sget-object v1, LX/4Ak;->OPERATION_QUEUED:LX/4Ak;

    if-ne v0, v1, :cond_1

    .line 676960
    :cond_0
    invoke-static {p0}, LX/4Al;->stopShowingProgress(LX/4Al;)V

    .line 676961
    :cond_1
    iput-object p1, p0, LX/4Al;->operationProgressIndicator:LX/4At;

    .line 676962
    iget-object v0, p0, LX/4Al;->operationState:LX/4Ak;

    sget-object v1, LX/4Ak;->READY_TO_QUEUE:LX/4Ak;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, LX/4Al;->operationState:LX/4Ak;

    sget-object v1, LX/4Ak;->OPERATION_QUEUED:LX/4Ak;

    if-ne v0, v1, :cond_3

    .line 676963
    :cond_2
    invoke-static {p0}, LX/4Al;->beginShowingProgress(LX/4Al;)V

    .line 676964
    :cond_3
    return-void
.end method

.method public start(Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 676965
    iget-object v0, p0, LX/4Al;->operationState:LX/4Ak;

    sget-object v3, LX/4Ak;->INIT:LX/4Ak;

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "Incorrect operation state"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 676966
    iget-object v0, p0, LX/4Al;->operationType:Ljava/lang/String;

    if-nez v0, :cond_3

    :goto_1
    const-string v0, "Initially operationType should be null"

    invoke-static {v1, v0}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 676967
    const-string v0, "non-null operationType"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 676968
    sget-object v0, LX/4Ak;->READY_TO_QUEUE:LX/4Ak;

    iput-object v0, p0, LX/4Al;->operationState:LX/4Ak;

    .line 676969
    iput-object p1, p0, LX/4Al;->operationType:Ljava/lang/String;

    .line 676970
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, p2}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, LX/4Al;->param:Landroid/os/Bundle;

    .line 676971
    iput-object p3, p0, LX/4Al;->callerContext:Lcom/facebook/common/callercontext/CallerContext;

    .line 676972
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 676973
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/4Al;->handler:Landroid/os/Handler;

    .line 676974
    :cond_0
    iget-object v0, p0, LX/4Al;->param:Landroid/os/Bundle;

    const-string v1, "overridden_viewer_context"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 676975
    iget-object v0, p0, LX/4Al;->mViewerContextManager:LX/0SI;

    invoke-interface {v0}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 676976
    if-eqz v0, :cond_1

    .line 676977
    iget-object v1, p0, LX/4Al;->param:Landroid/os/Bundle;

    const-string v2, "overridden_viewer_context"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 676978
    :cond_1
    iget-object v0, p0, LX/4Al;->param:Landroid/os/Bundle;

    const-string v1, "calling_process_name"

    iget-object v2, p0, LX/4Al;->mProcessUtil:LX/0VT;

    invoke-virtual {v2}, LX/0VT;->a()LX/00G;

    move-result-object v2

    .line 676979
    iget-object v3, v2, LX/00G;->b:Ljava/lang/String;

    move-object v2, v3

    .line 676980
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 676981
    invoke-static {p0}, LX/4Al;->beginShowingProgress(LX/4Al;)V

    .line 676982
    invoke-static {p0}, LX/4Al;->bindToService(LX/4Al;)V

    .line 676983
    return-void

    :cond_2
    move v0, v2

    .line 676984
    goto :goto_0

    :cond_3
    move v1, v2

    .line 676985
    goto :goto_1
.end method
