.class public final LX/4Ug;
.super Ljava/util/AbstractCollection;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractCollection",
        "<",
        "LX/4Zs;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "LX/4Zs;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/4VM;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Collection;LX/4VM;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LX/4Zs;",
            ">;",
            "LX/4VM;",
            ")V"
        }
    .end annotation

    .prologue
    .line 741654
    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    .line 741655
    iput-object p1, p0, LX/4Ug;->a:Ljava/util/Collection;

    .line 741656
    iput-object p2, p0, LX/4Ug;->b:LX/4VM;

    .line 741657
    return-void
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 741658
    check-cast p1, LX/4Zs;

    .line 741659
    iget-object v0, p0, LX/4Ug;->b:LX/4VM;

    if-eqz v0, :cond_1

    .line 741660
    iget-object v0, p0, LX/4Ug;->b:LX/4VM;

    iget-object v1, p1, LX/4Zs;->a:Ljava/lang/String;

    iget-object v2, p1, LX/4Zs;->b:Ljava/lang/String;

    iget-object v3, p1, LX/4Zs;->c:Ljava/lang/Object;

    .line 741661
    invoke-static {v1, v3}, LX/2lj;->a(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 741662
    iget-object v4, v0, LX/4VM;->a:Ljava/util/Map;

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    .line 741663
    if-nez v4, :cond_0

    .line 741664
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 741665
    iget-object v5, v0, LX/4VM;->a:Ljava/util/Map;

    invoke-interface {v5, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 741666
    :cond_0
    invoke-interface {v4, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 741667
    iget v4, v0, LX/4VM;->b:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v0, LX/4VM;->b:I

    .line 741668
    :cond_1
    iget-object v0, p0, LX/4Ug;->a:Ljava/util/Collection;

    if-eqz v0, :cond_2

    .line 741669
    iget-object v0, p0, LX/4Ug;->a:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 741670
    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "LX/4Zs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 741671
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 741672
    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 741673
    const/4 v0, 0x0

    return v0
.end method
