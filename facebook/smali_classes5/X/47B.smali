.class public LX/47B;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/17Y;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/398;",
            ">;>;"
        }
    .end annotation
.end field

.field private b:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private c:LX/17c;


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/398;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 671943
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 671944
    iput-object p1, p0, LX/47B;->a:LX/0Or;

    .line 671945
    return-void
.end method

.method public constructor <init>(LX/0Or;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/17c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/398;",
            ">;>;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/17c;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 671946
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 671947
    iput-object p1, p0, LX/47B;->a:LX/0Or;

    .line 671948
    iput-object p2, p0, LX/47B;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 671949
    iput-object p3, p0, LX/47B;->c:LX/17c;

    .line 671950
    return-void
.end method

.method private a(ILjava/lang/String;)V
    .locals 5

    .prologue
    .line 671951
    iget-object v0, p0, LX/47B;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-eqz v0, :cond_0

    .line 671952
    iget-object v0, p0, LX/47B;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    const-string v2, "tag_name"

    const-string v3, "\\d+"

    const-string v4, "#"

    invoke-virtual {p2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, p1, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IILjava/lang/String;Ljava/lang/String;)V

    .line 671953
    :cond_0
    return-void
.end method

.method private a(ILjava/lang/String;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 671954
    iget-object v0, p0, LX/47B;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-eqz v0, :cond_0

    .line 671955
    if-eqz p3, :cond_1

    const/4 v0, 0x2

    .line 671956
    :goto_0
    iget-object v1, p0, LX/47B;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-interface {v1, p1, v2, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 671957
    :cond_0
    return-void

    .line 671958
    :cond_1
    const/4 v0, 0x3

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/47I;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 671959
    iget-object v0, p2, LX/47I;->a:Ljava/lang/String;

    move-object v0, v0

    .line 671960
    invoke-virtual {p0, p1, v0}, LX/47B;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v4, 0x3

    const v2, 0x570016

    const v3, 0x570015

    .line 671961
    invoke-direct {p0, v2, p2}, LX/47B;->a(ILjava/lang/String;)V

    .line 671962
    :try_start_0
    iget-object v0, p0, LX/47B;->c:LX/17c;

    invoke-static {p1, p2, v0}, LX/JHc;->a(Landroid/content/Context;Ljava/lang/String;LX/17c;)Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 671963
    invoke-direct {p0, v2, p2, v1}, LX/47B;->a(ILjava/lang/String;Landroid/content/Intent;)V

    .line 671964
    invoke-static {v4}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 671965
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    .line 671966
    :cond_0
    if-eqz v1, :cond_2

    move-object v0, v1

    .line 671967
    :cond_1
    :goto_0
    return-object v0

    .line 671968
    :catchall_0
    move-exception v0

    const/4 v1, 0x0

    invoke-direct {p0, v2, p2, v1}, LX/47B;->a(ILjava/lang/String;Landroid/content/Intent;)V

    throw v0

    .line 671969
    :cond_2
    invoke-direct {p0, v3, p2}, LX/47B;->a(ILjava/lang/String;)V

    .line 671970
    :try_start_1
    iget-object v0, p0, LX/47B;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/398;

    .line 671971
    invoke-virtual {v0, p1, p2}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 671972
    if-nez v0, :cond_4

    move-object v1, v0

    .line 671973
    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 671974
    :cond_4
    invoke-direct {p0, v3, p2, v0}, LX/47B;->a(ILjava/lang/String;Landroid/content/Intent;)V

    .line 671975
    invoke-static {v4}, LX/01m;->b(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 671976
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    goto :goto_0

    .line 671977
    :catchall_1
    move-exception v0

    invoke-direct {p0, v3, p2, v1}, LX/47B;->a(ILjava/lang/String;Landroid/content/Intent;)V

    throw v0
.end method
