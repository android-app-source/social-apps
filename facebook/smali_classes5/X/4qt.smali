.class public LX/4qt;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "LX/4pS",
            "<*>;>;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public final d:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Class",
            "<+",
            "LX/4pS",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 814846
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LX/4qt;-><init>(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Z)V

    .line 814847
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Class",
            "<+",
            "LX/4pS",
            "<*>;>;Z)V"
        }
    .end annotation

    .prologue
    .line 814840
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 814841
    iput-object p1, p0, LX/4qt;->a:Ljava/lang/String;

    .line 814842
    iput-object p2, p0, LX/4qt;->c:Ljava/lang/Class;

    .line 814843
    iput-object p3, p0, LX/4qt;->b:Ljava/lang/Class;

    .line 814844
    iput-boolean p4, p0, LX/4qt;->d:Z

    .line 814845
    return-void
.end method


# virtual methods
.method public final a(Z)LX/4qt;
    .locals 4

    .prologue
    .line 814837
    iget-boolean v0, p0, LX/4qt;->d:Z

    if-ne v0, p1, :cond_0

    .line 814838
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/4qt;

    iget-object v1, p0, LX/4qt;->a:Ljava/lang/String;

    iget-object v2, p0, LX/4qt;->c:Ljava/lang/Class;

    iget-object v3, p0, LX/4qt;->b:Ljava/lang/Class;

    invoke-direct {v0, v1, v2, v3, p1}, LX/4qt;-><init>(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Z)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 814839
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ObjectIdInfo: propName="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/4qt;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", scope="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, LX/4qt;->c:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "null"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", generatorType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, LX/4qt;->b:Ljava/lang/Class;

    if-nez v0, :cond_1

    const-string v0, "null"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", alwaysAsId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/4qt;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, LX/4qt;->c:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/4qt;->b:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
