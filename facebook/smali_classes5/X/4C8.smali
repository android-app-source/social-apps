.class public LX/4C8;
.super LX/4C6;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/4C5;",
        ">",
        "LX/4C6",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final a:LX/0So;

.field private final b:Ljava/util/concurrent/ScheduledExecutorService;

.field public c:Z

.field public d:J

.field public e:J

.field private f:J

.field public g:LX/4C7;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final h:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(LX/4C5;LX/4C7;LX/0So;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 2
    .param p1    # LX/4C5;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/4C7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "LX/4C7;",
            "LX/0So;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ")V"
        }
    .end annotation

    .prologue
    .line 678434
    invoke-direct {p0, p1}, LX/4C6;-><init>(LX/4C5;)V

    .line 678435
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4C8;->c:Z

    .line 678436
    const-wide/16 v0, 0x7d0

    iput-wide v0, p0, LX/4C8;->e:J

    .line 678437
    const-wide/16 v0, 0x3e8

    iput-wide v0, p0, LX/4C8;->f:J

    .line 678438
    new-instance v0, Lcom/facebook/fresco/animation/backend/AnimationBackendDelegateWithInactivityCheck$1;

    invoke-direct {v0, p0}, Lcom/facebook/fresco/animation/backend/AnimationBackendDelegateWithInactivityCheck$1;-><init>(LX/4C8;)V

    iput-object v0, p0, LX/4C8;->h:Ljava/lang/Runnable;

    .line 678439
    iput-object p2, p0, LX/4C8;->g:LX/4C7;

    .line 678440
    iput-object p3, p0, LX/4C8;->a:LX/0So;

    .line 678441
    iput-object p4, p0, LX/4C8;->b:Ljava/util/concurrent/ScheduledExecutorService;

    .line 678442
    return-void
.end method

.method public static a(LX/4C5;LX/0So;Ljava/util/concurrent/ScheduledExecutorService;)LX/4C6;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/4C5;",
            ":",
            "LX/4C7;",
            ">(TT;",
            "LX/0So;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ")",
            "LX/4C6",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 678422
    move-object v0, p0

    check-cast v0, LX/4C7;

    .line 678423
    new-instance v1, LX/4C8;

    invoke-direct {v1, p0, v0, p1, p2}, LX/4C8;-><init>(LX/4C5;LX/4C7;LX/0So;Ljava/util/concurrent/ScheduledExecutorService;)V

    move-object v0, v1

    .line 678424
    return-object v0
.end method

.method public static declared-synchronized g(LX/4C8;)V
    .locals 5

    .prologue
    .line 678429
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/4C8;->c:Z

    if-nez v0, :cond_0

    .line 678430
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4C8;->c:Z

    .line 678431
    iget-object v0, p0, LX/4C8;->b:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, LX/4C8;->h:Ljava/lang/Runnable;

    iget-wide v2, p0, LX/4C8;->f:J

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 678432
    :cond_0
    monitor-exit p0

    return-void

    .line 678433
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Canvas;I)Z
    .locals 2

    .prologue
    .line 678425
    iget-object v0, p0, LX/4C8;->a:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/4C8;->d:J

    .line 678426
    invoke-super {p0, p1, p2, p3}, LX/4C6;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Canvas;I)Z

    move-result v0

    .line 678427
    invoke-static {p0}, LX/4C8;->g(LX/4C8;)V

    .line 678428
    return v0
.end method
