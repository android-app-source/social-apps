.class public final LX/3vw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:LX/3w4;


# direct methods
.method public constructor <init>(LX/3w4;)V
    .locals 0

    .prologue
    .line 653894
    iput-object p1, p0, LX/3vw;->a:LX/3w4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 1

    .prologue
    .line 653888
    iget-object v0, p0, LX/3vw;->a:LX/3w4;

    iget-object v0, v0, LX/3w4;->G:LX/3vx;

    invoke-interface {v0}, LX/3vx;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 653889
    iget-object v0, p0, LX/3vw;->a:LX/3w4;

    iget-object v0, v0, LX/3w4;->G:LX/3vx;

    invoke-interface {v0}, LX/3vx;->c()V

    .line 653890
    :cond_0
    iget-object v0, p0, LX/3vw;->a:LX/3w4;

    invoke-virtual {v0}, LX/3w4;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 653891
    if-eqz v0, :cond_1

    .line 653892
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 653893
    :cond_1
    return-void
.end method
