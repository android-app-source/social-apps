.class public final LX/3uy;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field public final synthetic a:LX/3uz;

.field private b:I


# direct methods
.method public constructor <init>(LX/3uz;)V
    .locals 1

    .prologue
    .line 649935
    iput-object p1, p0, LX/3uy;->a:LX/3uz;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 649936
    const/4 v0, -0x1

    iput v0, p0, LX/3uy;->b:I

    .line 649937
    invoke-direct {p0}, LX/3uy;->a()V

    .line 649938
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 649923
    iget-object v0, p0, LX/3uy;->a:LX/3uz;

    iget-object v0, v0, LX/3uz;->c:LX/3v0;

    .line 649924
    iget-object v1, v0, LX/3v0;->x:LX/3v3;

    move-object v2, v1

    .line 649925
    if-eqz v2, :cond_1

    .line 649926
    iget-object v0, p0, LX/3uy;->a:LX/3uz;

    iget-object v0, v0, LX/3uz;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->m()Ljava/util/ArrayList;

    move-result-object v3

    .line 649927
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 649928
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    .line 649929
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    .line 649930
    if-ne v0, v2, :cond_0

    .line 649931
    iput v1, p0, LX/3uy;->b:I

    .line 649932
    :goto_1
    return-void

    .line 649933
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 649934
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, LX/3uy;->b:I

    goto :goto_1
.end method


# virtual methods
.method public final a(I)LX/3v3;
    .locals 3

    .prologue
    .line 649904
    iget-object v0, p0, LX/3uy;->a:LX/3uz;

    iget-object v0, v0, LX/3uz;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->m()Ljava/util/ArrayList;

    move-result-object v1

    .line 649905
    iget-object v0, p0, LX/3uy;->a:LX/3uz;

    iget v0, v0, LX/3uz;->h:I

    add-int/2addr v0, p1

    .line 649906
    iget v2, p0, LX/3uy;->b:I

    if-ltz v2, :cond_0

    iget v2, p0, LX/3uy;->b:I

    if-lt v0, v2, :cond_0

    .line 649907
    add-int/lit8 v0, v0, 0x1

    .line 649908
    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    return-object v0
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 649919
    iget-object v0, p0, LX/3uy;->a:LX/3uz;

    iget-object v0, v0, LX/3uz;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->m()Ljava/util/ArrayList;

    move-result-object v0

    .line 649920
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, LX/3uy;->a:LX/3uz;

    iget v1, v1, LX/3uz;->h:I

    sub-int/2addr v0, v1

    .line 649921
    iget v1, p0, LX/3uy;->b:I

    if-gez v1, :cond_0

    .line 649922
    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 649918
    invoke-virtual {p0, p1}, LX/3uy;->a(I)LX/3v3;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 649917
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 649912
    if-nez p2, :cond_0

    .line 649913
    iget-object v0, p0, LX/3uy;->a:LX/3uz;

    iget-object v0, v0, LX/3uz;->b:Landroid/view/LayoutInflater;

    iget-object v1, p0, LX/3uy;->a:LX/3uz;

    iget v1, v1, LX/3uz;->f:I

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    :goto_0
    move-object v0, v1

    .line 649914
    check-cast v0, LX/3uq;

    .line 649915
    invoke-virtual {p0, p1}, LX/3uy;->a(I)LX/3v3;

    move-result-object v2

    invoke-interface {v0, v2, v3}, LX/3uq;->a(LX/3v3;I)V

    .line 649916
    return-object v1

    :cond_0
    move-object v1, p2

    goto :goto_0
.end method

.method public final notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 649909
    invoke-direct {p0}, LX/3uy;->a()V

    .line 649910
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 649911
    return-void
.end method
