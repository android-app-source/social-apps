.class public LX/3xm;
.super LX/3x6;
.source ""

# interfaces
.implements LX/3x8;


# instance fields
.field private A:J

.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1a1;

.field public c:F

.field public d:F

.field public e:F

.field public f:F

.field public g:F

.field public h:F

.field public i:I

.field public j:LX/3xj;

.field public k:I

.field public l:I

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3xe;",
            ">;"
        }
    .end annotation
.end field

.field private final n:[F

.field public o:I

.field public p:Landroid/support/v7/widget/RecyclerView;

.field public final q:Ljava/lang/Runnable;

.field public r:Landroid/view/VelocityTracker;

.field private s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public u:LX/3x4;

.field public v:Landroid/view/View;

.field public w:I

.field public x:LX/3rW;

.field public final y:LX/1OH;

.field private z:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(LX/3xj;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 660327
    invoke-direct {p0}, LX/3x6;-><init>()V

    .line 660328
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3xm;->a:Ljava/util/List;

    .line 660329
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, LX/3xm;->n:[F

    .line 660330
    iput-object v1, p0, LX/3xm;->b:LX/1a1;

    .line 660331
    iput v2, p0, LX/3xm;->i:I

    .line 660332
    const/4 v0, 0x0

    iput v0, p0, LX/3xm;->k:I

    .line 660333
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3xm;->m:Ljava/util/List;

    .line 660334
    new-instance v0, Landroid/support/v7/widget/helper/ItemTouchHelper$1;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/helper/ItemTouchHelper$1;-><init>(LX/3xm;)V

    iput-object v0, p0, LX/3xm;->q:Ljava/lang/Runnable;

    .line 660335
    iput-object v1, p0, LX/3xm;->u:LX/3x4;

    .line 660336
    iput-object v1, p0, LX/3xm;->v:Landroid/view/View;

    .line 660337
    iput v2, p0, LX/3xm;->w:I

    .line 660338
    new-instance v0, LX/3xd;

    invoke-direct {v0, p0}, LX/3xd;-><init>(LX/3xm;)V

    iput-object v0, p0, LX/3xm;->y:LX/1OH;

    .line 660339
    iput-object p1, p0, LX/3xm;->j:LX/3xj;

    .line 660340
    return-void
.end method

.method private a([F)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 660320
    iget v0, p0, LX/3xm;->l:I

    and-int/lit8 v0, v0, 0xc

    if-eqz v0, :cond_0

    .line 660321
    iget v0, p0, LX/3xm;->g:F

    iget v1, p0, LX/3xm;->e:F

    add-float/2addr v0, v1

    iget-object v1, p0, LX/3xm;->b:LX/1a1;

    iget-object v1, v1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    aput v0, p1, v2

    .line 660322
    :goto_0
    iget v0, p0, LX/3xm;->l:I

    and-int/lit8 v0, v0, 0x3

    if-eqz v0, :cond_1

    .line 660323
    iget v0, p0, LX/3xm;->h:F

    iget v1, p0, LX/3xm;->f:F

    add-float/2addr v0, v1

    iget-object v1, p0, LX/3xm;->b:LX/1a1;

    iget-object v1, v1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    aput v0, p1, v3

    .line 660324
    :goto_1
    return-void

    .line 660325
    :cond_0
    iget-object v0, p0, LX/3xm;->b:LX/1a1;

    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0}, LX/0vv;->r(Landroid/view/View;)F

    move-result v0

    aput v0, p1, v2

    goto :goto_0

    .line 660326
    :cond_1
    iget-object v0, p0, LX/3xm;->b:LX/1a1;

    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0}, LX/0vv;->s(Landroid/view/View;)F

    move-result v0

    aput v0, p1, v3

    goto :goto_1
.end method

.method private static a(Landroid/view/View;FFFF)Z
    .locals 1

    .prologue
    .line 660319
    cmpl-float v0, p1, p3

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, p3

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    cmpl-float v0, p2, p4

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, p4

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/3xm;LX/1a1;Z)I
    .locals 3

    .prologue
    .line 660306
    iget-object v0, p0, LX/3xm;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 660307
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 660308
    iget-object v0, p0, LX/3xm;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3xe;

    .line 660309
    iget-object v2, v0, LX/3xe;->h:LX/1a1;

    if-ne v2, p1, :cond_1

    .line 660310
    iget-boolean v2, v0, LX/3xe;->m:Z

    or-int/2addr v2, p2

    iput-boolean v2, v0, LX/3xe;->m:Z

    .line 660311
    iget-boolean v2, v0, LX/3xe;->c:Z

    if-nez v2, :cond_0

    .line 660312
    iget-object v2, v0, LX/3xe;->a:LX/3ow;

    invoke-interface {v2}, LX/3ow;->b()V

    .line 660313
    :cond_0
    iget-object v2, p0, LX/3xm;->m:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 660314
    iget-object v1, v0, LX/3xe;->h:LX/1a1;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/1a1;->a(Z)V

    .line 660315
    iget v0, v0, LX/3xe;->b:I

    .line 660316
    :goto_1
    return v0

    .line 660317
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 660318
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a$redex0(LX/3xm;LX/1a1;I)V
    .locals 12

    .prologue
    .line 660223
    iget-object v0, p0, LX/3xm;->b:LX/1a1;

    if-ne p1, v0, :cond_0

    iget v0, p0, LX/3xm;->k:I

    if-ne p2, v0, :cond_0

    .line 660224
    :goto_0
    return-void

    .line 660225
    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LX/3xm;->A:J

    .line 660226
    iget v4, p0, LX/3xm;->k:I

    .line 660227
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, LX/3xm;->a$redex0(LX/3xm;LX/1a1;Z)I

    .line 660228
    iput p2, p0, LX/3xm;->k:I

    .line 660229
    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    .line 660230
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    iput-object v0, p0, LX/3xm;->v:Landroid/view/View;

    .line 660231
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_c

    .line 660232
    :cond_1
    :goto_1
    const/4 v0, 0x1

    mul-int/lit8 v1, p2, 0x8

    add-int/lit8 v1, v1, 0x8

    shl-int/2addr v0, v1

    add-int/lit8 v11, v0, -0x1

    .line 660233
    const/4 v0, 0x0

    .line 660234
    iget-object v1, p0, LX/3xm;->b:LX/1a1;

    if-eqz v1, :cond_2

    .line 660235
    iget-object v2, p0, LX/3xm;->b:LX/1a1;

    .line 660236
    iget-object v1, v2, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 660237
    const/4 v0, 0x2

    if-ne v4, v0, :cond_6

    const/4 v9, 0x0

    .line 660238
    :goto_2
    invoke-static {p0}, LX/3xm;->g(LX/3xm;)V

    .line 660239
    sparse-switch v9, :sswitch_data_0

    .line 660240
    const/4 v7, 0x0

    .line 660241
    const/4 v8, 0x0

    .line 660242
    :goto_3
    const/4 v0, 0x2

    if-ne v4, v0, :cond_8

    .line 660243
    const/16 v3, 0x8

    .line 660244
    :goto_4
    iget-object v0, p0, LX/3xm;->n:[F

    invoke-direct {p0, v0}, LX/3xm;->a([F)V

    .line 660245
    iget-object v0, p0, LX/3xm;->n:[F

    const/4 v1, 0x0

    aget v5, v0, v1

    .line 660246
    iget-object v0, p0, LX/3xm;->n:[F

    const/4 v1, 0x1

    aget v6, v0, v1

    .line 660247
    new-instance v0, LX/3xf;

    move-object v1, p0

    move-object v10, v2

    invoke-direct/range {v0 .. v10}, LX/3xf;-><init>(LX/3xm;LX/1a1;IIFFFFILX/1a1;)V

    .line 660248
    iget-object v1, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v1, v3}, LX/3xj;->a(Landroid/support/v7/widget/RecyclerView;I)J

    move-result-wide v2

    .line 660249
    iget-object v1, v0, LX/3xe;->a:LX/3ow;

    invoke-interface {v1, v2, v3}, LX/3ow;->a(J)V

    .line 660250
    iget-object v1, p0, LX/3xm;->m:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 660251
    iget-object v1, v0, LX/3xe;->h:LX/1a1;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/1a1;->a(Z)V

    .line 660252
    iget-object v1, v0, LX/3xe;->a:LX/3ow;

    invoke-interface {v1}, LX/3ow;->a()V

    .line 660253
    const/4 v0, 0x1

    .line 660254
    :goto_5
    const/4 v1, 0x0

    iput-object v1, p0, LX/3xm;->b:LX/1a1;

    :cond_2
    move v1, v0

    .line 660255
    if-eqz p1, :cond_3

    .line 660256
    iget-object v0, p0, LX/3xm;->j:LX/3xj;

    iget-object v2, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2, p1}, LX/3xj;->a(Landroid/support/v7/widget/RecyclerView;LX/1a1;)I

    move-result v0

    and-int/2addr v0, v11

    iget v2, p0, LX/3xm;->k:I

    mul-int/lit8 v2, v2, 0x8

    shr-int/2addr v0, v2

    iput v0, p0, LX/3xm;->l:I

    .line 660257
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LX/3xm;->g:F

    .line 660258
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LX/3xm;->h:F

    .line 660259
    iput-object p1, p0, LX/3xm;->b:LX/1a1;

    .line 660260
    const/4 v0, 0x2

    if-ne p2, v0, :cond_3

    .line 660261
    iget-object v0, p0, LX/3xm;->b:LX/1a1;

    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 660262
    :cond_3
    iget-object v0, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .line 660263
    if-eqz v2, :cond_4

    .line 660264
    iget-object v0, p0, LX/3xm;->b:LX/1a1;

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    :goto_6
    invoke-interface {v2, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 660265
    :cond_4
    if-nez v1, :cond_5

    .line 660266
    iget-object v0, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    invoke-virtual {v0}, LX/1OR;->H()V

    .line 660267
    :cond_5
    iget-object v0, p0, LX/3xm;->j:LX/3xj;

    iget-object v1, p0, LX/3xm;->b:LX/1a1;

    iget v2, p0, LX/3xm;->k:I

    invoke-virtual {v0, v1, v2}, LX/3xj;->a(LX/1a1;I)V

    .line 660268
    iget-object v0, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    goto/16 :goto_0

    .line 660269
    :cond_6
    const v5, 0xff00

    const/4 v0, 0x0

    .line 660270
    iget v1, p0, LX/3xm;->k:I

    const/4 v3, 0x2

    if-ne v1, v3, :cond_e

    .line 660271
    :cond_7
    :goto_7
    move v9, v0

    .line 660272
    goto/16 :goto_2

    .line 660273
    :sswitch_0
    const/4 v8, 0x0

    .line 660274
    iget v0, p0, LX/3xm;->e:F

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    iget-object v1, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float v7, v0, v1

    .line 660275
    goto/16 :goto_3

    .line 660276
    :sswitch_1
    const/4 v7, 0x0

    .line 660277
    iget v0, p0, LX/3xm;->f:F

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    iget-object v1, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float v8, v0, v1

    .line 660278
    goto/16 :goto_3

    .line 660279
    :cond_8
    if-lez v9, :cond_9

    .line 660280
    const/4 v3, 0x2

    goto/16 :goto_4

    .line 660281
    :cond_9
    const/4 v3, 0x4

    goto/16 :goto_4

    .line 660282
    :cond_a
    iget-object v1, v2, LX/1a1;->a:Landroid/view/View;

    invoke-static {p0, v1}, LX/3xm;->b(LX/3xm;Landroid/view/View;)V

    .line 660283
    iget-object v1, p0, LX/3xm;->j:LX/3xj;

    iget-object v3, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v3, v2}, LX/3xj;->b(Landroid/support/v7/widget/RecyclerView;LX/1a1;)V

    goto/16 :goto_5

    .line 660284
    :cond_b
    const/4 v0, 0x0

    goto :goto_6

    .line 660285
    :cond_c
    iget-object v0, p0, LX/3xm;->u:LX/3x4;

    if-nez v0, :cond_d

    .line 660286
    new-instance v0, LX/3xg;

    invoke-direct {v0, p0}, LX/3xg;-><init>(LX/3xm;)V

    iput-object v0, p0, LX/3xm;->u:LX/3x4;

    .line 660287
    :cond_d
    iget-object v0, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, LX/3xm;->u:LX/3x4;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setChildDrawingOrderCallback(LX/3x4;)V

    goto/16 :goto_1

    .line 660288
    :cond_e
    iget-object v1, p0, LX/3xm;->j:LX/3xj;

    invoke-virtual {v1, v2}, LX/3xj;->a(LX/1a1;)I

    move-result v1

    .line 660289
    iget-object v3, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v3}, LX/0vv;->h(Landroid/view/View;)I

    move-result v3

    invoke-static {v1, v3}, LX/3xj;->c(II)I

    move-result v3

    .line 660290
    and-int/2addr v3, v5

    shr-int/lit8 v3, v3, 0x8

    .line 660291
    if-eqz v3, :cond_7

    .line 660292
    and-int/2addr v1, v5

    shr-int/lit8 v5, v1, 0x8

    .line 660293
    iget v1, p0, LX/3xm;->e:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v6, p0, LX/3xm;->f:F

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpl-float v1, v1, v6

    if-lez v1, :cond_11

    .line 660294
    invoke-static {p0, v2, v3}, LX/3xm;->b(LX/3xm;LX/1a1;I)I

    move-result v1

    if-lez v1, :cond_10

    .line 660295
    and-int v0, v5, v1

    if-nez v0, :cond_f

    .line 660296
    iget-object v0, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v0

    invoke-static {v1, v0}, LX/3xj;->a(II)I

    move-result v0

    goto/16 :goto_7

    :cond_f
    move v0, v1

    .line 660297
    goto/16 :goto_7

    .line 660298
    :cond_10
    invoke-static {p0, v2, v3}, LX/3xm;->c(LX/3xm;LX/1a1;I)I

    move-result v1

    if-lez v1, :cond_7

    move v0, v1

    .line 660299
    goto/16 :goto_7

    .line 660300
    :cond_11
    invoke-static {p0, v2, v3}, LX/3xm;->c(LX/3xm;LX/1a1;I)I

    move-result v1

    if-lez v1, :cond_12

    move v0, v1

    .line 660301
    goto/16 :goto_7

    .line 660302
    :cond_12
    invoke-static {p0, v2, v3}, LX/3xm;->b(LX/3xm;LX/1a1;I)I

    move-result v1

    if-lez v1, :cond_7

    .line 660303
    and-int v0, v5, v1

    if-nez v0, :cond_13

    .line 660304
    iget-object v0, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v0

    invoke-static {v1, v0}, LX/3xj;->a(II)I

    move-result v0

    goto/16 :goto_7

    :cond_13
    move v0, v1

    .line 660305
    goto/16 :goto_7

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_1
        0x4 -> :sswitch_0
        0x8 -> :sswitch_0
        0x10 -> :sswitch_0
        0x20 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a$redex0(LX/3xm;Landroid/view/MotionEvent;II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 660210
    invoke-static {p1, p3}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v0

    .line 660211
    invoke-static {p1, p3}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v1

    .line 660212
    iget v2, p0, LX/3xm;->c:F

    sub-float/2addr v0, v2

    iput v0, p0, LX/3xm;->e:F

    .line 660213
    iget v0, p0, LX/3xm;->d:F

    sub-float v0, v1, v0

    iput v0, p0, LX/3xm;->f:F

    .line 660214
    and-int/lit8 v0, p2, 0x4

    if-nez v0, :cond_0

    .line 660215
    iget v0, p0, LX/3xm;->e:F

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LX/3xm;->e:F

    .line 660216
    :cond_0
    and-int/lit8 v0, p2, 0x8

    if-nez v0, :cond_1

    .line 660217
    iget v0, p0, LX/3xm;->e:F

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, LX/3xm;->e:F

    .line 660218
    :cond_1
    and-int/lit8 v0, p2, 0x1

    if-nez v0, :cond_2

    .line 660219
    iget v0, p0, LX/3xm;->f:F

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LX/3xm;->f:F

    .line 660220
    :cond_2
    and-int/lit8 v0, p2, 0x2

    if-nez v0, :cond_3

    .line 660221
    iget v0, p0, LX/3xm;->f:F

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, LX/3xm;->f:F

    .line 660222
    :cond_3
    return-void
.end method

.method public static a$redex0(LX/3xm;ILandroid/view/MotionEvent;I)Z
    .locals 10

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v9, 0x0

    const/4 v0, 0x0

    .line 660170
    iget-object v2, p0, LX/3xm;->b:LX/1a1;

    if-nez v2, :cond_0

    if-ne p1, v3, :cond_0

    iget v2, p0, LX/3xm;->k:I

    if-eq v2, v3, :cond_0

    iget-object v2, p0, LX/3xm;->j:LX/3xj;

    invoke-virtual {v2}, LX/3xj;->b()Z

    move-result v2

    if-nez v2, :cond_1

    .line 660171
    :cond_0
    :goto_0
    return v0

    .line 660172
    :cond_1
    iget-object v2, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    .line 660173
    iget v3, v2, Landroid/support/v7/widget/RecyclerView;->M:I

    move v2, v3

    .line 660174
    if-eq v2, v1, :cond_0

    .line 660175
    const/4 v2, 0x0

    .line 660176
    iget-object v3, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v3

    .line 660177
    iget v4, p0, LX/3xm;->i:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_8

    .line 660178
    :cond_2
    :goto_1
    move-object v2, v2

    .line 660179
    if-eqz v2, :cond_0

    .line 660180
    iget-object v3, p0, LX/3xm;->j:LX/3xj;

    iget-object v4, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3, v4, v2}, LX/3xj;->a(Landroid/support/v7/widget/RecyclerView;LX/1a1;)I

    move-result v3

    .line 660181
    const v4, 0xff00

    and-int/2addr v3, v4

    shr-int/lit8 v3, v3, 0x8

    .line 660182
    if-eqz v3, :cond_0

    .line 660183
    invoke-static {p2, p3}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v4

    .line 660184
    invoke-static {p2, p3}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v5

    .line 660185
    iget v6, p0, LX/3xm;->c:F

    sub-float/2addr v4, v6

    .line 660186
    iget v6, p0, LX/3xm;->d:F

    sub-float/2addr v5, v6

    .line 660187
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v6

    .line 660188
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v7

    .line 660189
    iget v8, p0, LX/3xm;->o:I

    int-to-float v8, v8

    cmpg-float v8, v6, v8

    if-gez v8, :cond_3

    iget v8, p0, LX/3xm;->o:I

    int-to-float v8, v8

    cmpg-float v8, v7, v8

    if-ltz v8, :cond_0

    .line 660190
    :cond_3
    cmpl-float v6, v6, v7

    if-lez v6, :cond_6

    .line 660191
    cmpg-float v5, v4, v9

    if-gez v5, :cond_4

    and-int/lit8 v5, v3, 0x4

    if-eqz v5, :cond_0

    .line 660192
    :cond_4
    cmpl-float v4, v4, v9

    if-lez v4, :cond_5

    and-int/lit8 v3, v3, 0x8

    if-eqz v3, :cond_0

    .line 660193
    :cond_5
    iput v9, p0, LX/3xm;->f:F

    iput v9, p0, LX/3xm;->e:F

    .line 660194
    invoke-static {p2, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, LX/3xm;->i:I

    .line 660195
    invoke-static {p0, v2, v1}, LX/3xm;->a$redex0(LX/3xm;LX/1a1;I)V

    move v0, v1

    .line 660196
    goto :goto_0

    .line 660197
    :cond_6
    cmpg-float v4, v5, v9

    if-gez v4, :cond_7

    and-int/lit8 v4, v3, 0x1

    if-eqz v4, :cond_0

    .line 660198
    :cond_7
    cmpl-float v4, v5, v9

    if-lez v4, :cond_5

    and-int/lit8 v3, v3, 0x2

    if-nez v3, :cond_5

    goto :goto_0

    .line 660199
    :cond_8
    iget v4, p0, LX/3xm;->i:I

    invoke-static {p2, v4}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v4

    .line 660200
    invoke-static {p2, v4}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v5

    iget v6, p0, LX/3xm;->c:F

    sub-float/2addr v5, v6

    .line 660201
    invoke-static {p2, v4}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v4

    iget v6, p0, LX/3xm;->d:F

    sub-float/2addr v4, v6

    .line 660202
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    .line 660203
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 660204
    iget v6, p0, LX/3xm;->o:I

    int-to-float v6, v6

    cmpg-float v6, v5, v6

    if-gez v6, :cond_9

    iget v6, p0, LX/3xm;->o:I

    int-to-float v6, v6

    cmpg-float v6, v4, v6

    if-ltz v6, :cond_2

    .line 660205
    :cond_9
    cmpl-float v6, v5, v4

    if-lez v6, :cond_a

    invoke-virtual {v3}, LX/1OR;->g()Z

    move-result v6

    if-nez v6, :cond_2

    .line 660206
    :cond_a
    cmpl-float v4, v4, v5

    if-lez v4, :cond_b

    invoke-virtual {v3}, LX/1OR;->h()Z

    move-result v3

    if-nez v3, :cond_2

    .line 660207
    :cond_b
    invoke-static {p0, p2}, LX/3xm;->b$redex0(LX/3xm;Landroid/view/MotionEvent;)Landroid/view/View;

    move-result-object v3

    .line 660208
    if-eqz v3, :cond_2

    .line 660209
    iget-object v2, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v2

    goto/16 :goto_1
.end method

.method public static b(LX/3xm;LX/1a1;I)I
    .locals 6

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x4

    const/4 v5, 0x0

    .line 660154
    and-int/lit8 v0, p2, 0xc

    if-eqz v0, :cond_4

    .line 660155
    iget v0, p0, LX/3xm;->e:F

    cmpl-float v0, v0, v5

    if-lez v0, :cond_1

    move v0, v1

    .line 660156
    :goto_0
    iget-object v3, p0, LX/3xm;->r:Landroid/view/VelocityTracker;

    if-eqz v3, :cond_3

    iget v3, p0, LX/3xm;->i:I

    if-ltz v3, :cond_3

    .line 660157
    iget-object v3, p0, LX/3xm;->r:Landroid/view/VelocityTracker;

    iget v4, p0, LX/3xm;->i:I

    invoke-static {v3, v4}, LX/2uG;->a(Landroid/view/VelocityTracker;I)F

    move-result v3

    .line 660158
    cmpl-float v4, v3, v5

    if-lez v4, :cond_2

    .line 660159
    :goto_1
    and-int v2, v1, p2

    if-eqz v2, :cond_3

    if-ne v0, v1, :cond_3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget-object v3, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    .line 660160
    iget v4, v3, Landroid/support/v7/widget/RecyclerView;->U:I

    move v3, v4

    .line 660161
    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_3

    move v0, v1

    .line 660162
    :cond_0
    :goto_2
    return v0

    :cond_1
    move v0, v2

    .line 660163
    goto :goto_0

    :cond_2
    move v1, v2

    .line 660164
    goto :goto_1

    .line 660165
    :cond_3
    iget-object v1, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    .line 660166
    const/high16 v2, 0x3f000000    # 0.5f

    move v2, v2

    .line 660167
    mul-float/2addr v1, v2

    .line 660168
    and-int v2, p2, v0

    if-eqz v2, :cond_4

    iget v2, p0, LX/3xm;->e:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v1, v2, v1

    if-gtz v1, :cond_0

    .line 660169
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private b(LX/1a1;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1a1;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation

    .prologue
    .line 660121
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3xm;->s:Ljava/util/List;

    if-nez v1, :cond_0

    .line 660122
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, LX/3xm;->s:Ljava/util/List;

    .line 660123
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, LX/3xm;->t:Ljava/util/List;

    .line 660124
    :goto_0
    const/16 v1, 0x0

    .line 660125
    move-object/from16 v0, p0

    iget v2, v0, LX/3xm;->g:F

    move-object/from16 v0, p0

    iget v3, v0, LX/3xm;->e:F

    add-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    sub-int v5, v2, v1

    .line 660126
    move-object/from16 v0, p0

    iget v2, v0, LX/3xm;->h:F

    move-object/from16 v0, p0

    iget v3, v0, LX/3xm;->f:F

    add-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    sub-int v6, v2, v1

    .line 660127
    move-object/from16 v0, p1

    iget-object v2, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int/2addr v2, v5

    mul-int/lit8 v3, v1, 0x2

    add-int v7, v2, v3

    .line 660128
    move-object/from16 v0, p1

    iget-object v2, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v2, v6

    mul-int/lit8 v1, v1, 0x2

    add-int v8, v2, v1

    .line 660129
    add-int v1, v5, v7

    div-int/lit8 v9, v1, 0x2

    .line 660130
    add-int v1, v6, v8

    div-int/lit8 v10, v1, 0x2

    .line 660131
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v11

    .line 660132
    invoke-virtual {v11}, LX/1OR;->v()I

    move-result v12

    .line 660133
    const/4 v1, 0x0

    move v4, v1

    :goto_1
    if-ge v4, v12, :cond_3

    .line 660134
    invoke-virtual {v11, v4}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v1

    .line 660135
    move-object/from16 v0, p1

    iget-object v2, v0, LX/1a1;->a:Landroid/view/View;

    if-eq v1, v2, :cond_2

    .line 660136
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v2

    if-lt v2, v6, :cond_2

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    if-gt v2, v8, :cond_2

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v2

    if-lt v2, v5, :cond_2

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    if-gt v2, v7, :cond_2

    .line 660137
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v13

    .line 660138
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3xm;->j:LX/3xj;

    invoke-virtual {v2, v13}, LX/3xj;->b(LX/1a1;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 660139
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v3

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v9, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 660140
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    add-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    sub-int v1, v10, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 660141
    mul-int/2addr v2, v2

    mul-int/2addr v1, v1

    add-int v14, v2, v1

    .line 660142
    const/4 v2, 0x0

    .line 660143
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3xm;->s:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v15

    .line 660144
    const/4 v1, 0x0

    move v3, v2

    move v2, v1

    :goto_2
    if-ge v2, v15, :cond_1

    .line 660145
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3xm;->t:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-le v14, v1, :cond_1

    .line 660146
    add-int/lit8 v3, v3, 0x1

    .line 660147
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 660148
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3xm;->s:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 660149
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3xm;->t:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    goto/16 :goto_0

    .line 660150
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3xm;->s:Ljava/util/List;

    invoke-interface {v1, v3, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 660151
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3xm;->t:Ljava/util/List;

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v3, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 660152
    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto/16 :goto_1

    .line 660153
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3xm;->s:Ljava/util/List;

    return-object v1
.end method

.method public static b(LX/3xm;Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 660116
    iget-object v0, p0, LX/3xm;->v:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 660117
    iput-object v1, p0, LX/3xm;->v:Landroid/view/View;

    .line 660118
    iget-object v0, p0, LX/3xm;->u:LX/3x4;

    if-eqz v0, :cond_0

    .line 660119
    iget-object v0, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setChildDrawingOrderCallback(LX/3x4;)V

    .line 660120
    :cond_0
    return-void
.end method

.method public static b$redex0(LX/3xm;Landroid/view/MotionEvent;)Landroid/view/View;
    .locals 6

    .prologue
    .line 660341
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 660342
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 660343
    iget-object v0, p0, LX/3xm;->b:LX/1a1;

    if-eqz v0, :cond_0

    .line 660344
    iget-object v0, p0, LX/3xm;->b:LX/1a1;

    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    .line 660345
    iget v1, p0, LX/3xm;->g:F

    iget v2, p0, LX/3xm;->e:F

    add-float/2addr v1, v2

    iget v2, p0, LX/3xm;->h:F

    iget v5, p0, LX/3xm;->f:F

    add-float/2addr v2, v5

    invoke-static {v0, v3, v4, v1, v2}, LX/3xm;->a(Landroid/view/View;FFFF)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 660346
    :goto_0
    return-object v0

    .line 660347
    :cond_0
    iget-object v0, p0, LX/3xm;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_1
    if-ltz v2, :cond_2

    .line 660348
    iget-object v0, p0, LX/3xm;->m:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3xe;

    .line 660349
    iget-object v1, v0, LX/3xe;->h:LX/1a1;

    iget-object v1, v1, LX/1a1;->a:Landroid/view/View;

    .line 660350
    iget v5, v0, LX/3xe;->k:F

    iget v0, v0, LX/3xe;->l:F

    invoke-static {v1, v3, v4, v5, v0}, LX/3xm;->a(Landroid/view/View;FFFF)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 660351
    goto :goto_0

    .line 660352
    :cond_1
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_1

    .line 660353
    :cond_2
    iget-object v0, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v3, v4}, Landroid/support/v7/widget/RecyclerView;->a(FF)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(LX/3xm;LX/1a1;I)I
    .locals 6

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 659972
    and-int/lit8 v0, p2, 0x3

    if-eqz v0, :cond_4

    .line 659973
    iget v0, p0, LX/3xm;->f:F

    cmpl-float v0, v0, v5

    if-lez v0, :cond_1

    move v0, v1

    .line 659974
    :goto_0
    iget-object v3, p0, LX/3xm;->r:Landroid/view/VelocityTracker;

    if-eqz v3, :cond_3

    iget v3, p0, LX/3xm;->i:I

    if-ltz v3, :cond_3

    .line 659975
    iget-object v3, p0, LX/3xm;->r:Landroid/view/VelocityTracker;

    iget v4, p0, LX/3xm;->i:I

    invoke-static {v3, v4}, LX/2uG;->b(Landroid/view/VelocityTracker;I)F

    move-result v3

    .line 659976
    cmpl-float v4, v3, v5

    if-lez v4, :cond_2

    .line 659977
    :goto_1
    and-int v2, v1, p2

    if-eqz v2, :cond_3

    if-ne v1, v0, :cond_3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget-object v3, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    .line 659978
    iget v4, v3, Landroid/support/v7/widget/RecyclerView;->U:I

    move v3, v4

    .line 659979
    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_3

    move v0, v1

    .line 659980
    :cond_0
    :goto_2
    return v0

    :cond_1
    move v0, v2

    .line 659981
    goto :goto_0

    :cond_2
    move v1, v2

    .line 659982
    goto :goto_1

    .line 659983
    :cond_3
    iget-object v1, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    .line 659984
    const/high16 v2, 0x3f000000    # 0.5f

    move v2, v2

    .line 659985
    mul-float/2addr v1, v2

    .line 659986
    and-int v2, p2, v0

    if-eqz v2, :cond_4

    iget v2, p0, LX/3xm;->f:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v1, v2, v1

    if-gtz v1, :cond_0

    .line 659987
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static c(LX/3xm;LX/1a1;)V
    .locals 8

    .prologue
    .line 660022
    iget-object v0, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->isLayoutRequested()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 660023
    :cond_0
    :goto_0
    return-void

    .line 660024
    :cond_1
    iget v0, p0, LX/3xm;->k:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 660025
    const/high16 v0, 0x3f000000    # 0.5f

    move v0, v0

    .line 660026
    iget v1, p0, LX/3xm;->g:F

    iget v2, p0, LX/3xm;->e:F

    add-float/2addr v1, v2

    float-to-int v4, v1

    .line 660027
    iget v1, p0, LX/3xm;->h:F

    iget v2, p0, LX/3xm;->f:F

    add-float/2addr v1, v2

    float-to-int v5, v1

    .line 660028
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int v1, v5, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int v1, v4, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v0, v2

    cmpg-float v0, v1, v0

    if-ltz v0, :cond_0

    .line 660029
    :cond_2
    invoke-direct {p0, p1}, LX/3xm;->b(LX/1a1;)Ljava/util/List;

    move-result-object v0

    .line 660030
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_0

    .line 660031
    iget-object v1, p0, LX/3xm;->j:LX/3xj;

    invoke-virtual {v1, p1, v0, v4, v5}, LX/3xj;->a(LX/1a1;Ljava/util/List;II)LX/1a1;

    move-result-object v2

    .line 660032
    if-nez v2, :cond_3

    .line 660033
    iget-object v0, p0, LX/3xm;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 660034
    iget-object v0, p0, LX/3xm;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0

    .line 660035
    :cond_3
    invoke-virtual {v2}, LX/1a1;->e()I

    move-result v3

    .line 660036
    invoke-virtual {p1}, LX/1a1;->e()I

    .line 660037
    iget-object v0, p0, LX/3xm;->j:LX/3xj;

    invoke-virtual {v0, p1, v2}, LX/3xj;->a(LX/1a1;LX/1a1;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 660038
    iget-object v0, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    move-object v1, p1

    .line 660039
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v6

    .line 660040
    instance-of v7, v6, LX/1P2;

    if-eqz v7, :cond_5

    .line 660041
    check-cast v6, LX/1P2;

    iget-object v7, v1, LX/1a1;->a:Landroid/view/View;

    iget-object p0, v2, LX/1a1;->a:Landroid/view/View;

    invoke-interface {v6, v7, p0}, LX/1P2;->a(Landroid/view/View;Landroid/view/View;)V

    .line 660042
    :cond_4
    :goto_1
    goto/16 :goto_0

    .line 660043
    :cond_5
    invoke-virtual {v6}, LX/1OR;->g()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 660044
    iget-object v7, v2, LX/1a1;->a:Landroid/view/View;

    invoke-static {v7}, LX/1OR;->i(Landroid/view/View;)I

    move-result v7

    .line 660045
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result p0

    if-gt v7, p0, :cond_6

    .line 660046
    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->g_(I)V

    .line 660047
    :cond_6
    iget-object v7, v2, LX/1a1;->a:Landroid/view/View;

    invoke-static {v7}, LX/1OR;->k(Landroid/view/View;)I

    move-result v7

    .line 660048
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result p0

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result p1

    sub-int/2addr p0, p1

    if-lt v7, p0, :cond_7

    .line 660049
    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->g_(I)V

    .line 660050
    :cond_7
    invoke-virtual {v6}, LX/1OR;->h()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 660051
    iget-object v7, v2, LX/1a1;->a:Landroid/view/View;

    invoke-static {v7}, LX/1OR;->j(Landroid/view/View;)I

    move-result v7

    .line 660052
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result p0

    if-gt v7, p0, :cond_8

    .line 660053
    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->g_(I)V

    .line 660054
    :cond_8
    iget-object v7, v2, LX/1a1;->a:Landroid/view/View;

    invoke-static {v7}, LX/1OR;->l(Landroid/view/View;)I

    move-result v6

    .line 660055
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v7

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result p0

    sub-int/2addr v7, p0

    if-lt v6, v7, :cond_4

    .line 660056
    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->g_(I)V

    goto :goto_1
.end method

.method public static e(LX/3xm;)Z
    .locals 14

    .prologue
    const-wide/high16 v12, -0x8000000000000000L

    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 659988
    iget-object v0, p0, LX/3xm;->b:LX/1a1;

    if-nez v0, :cond_0

    .line 659989
    iput-wide v12, p0, LX/3xm;->A:J

    .line 659990
    :goto_0
    return v6

    .line 659991
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 659992
    iget-wide v0, p0, LX/3xm;->A:J

    cmp-long v0, v0, v12

    if-nez v0, :cond_6

    const-wide/16 v4, 0x0

    .line 659993
    :goto_1
    iget-object v0, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    .line 659994
    iget-object v1, p0, LX/3xm;->z:Landroid/graphics/Rect;

    if-nez v1, :cond_1

    .line 659995
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, LX/3xm;->z:Landroid/graphics/Rect;

    .line 659996
    :cond_1
    iget-object v1, p0, LX/3xm;->b:LX/1a1;

    iget-object v1, v1, LX/1a1;->a:Landroid/view/View;

    iget-object v2, p0, LX/3xm;->z:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, v2}, LX/1OR;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 659997
    invoke-virtual {v0}, LX/1OR;->g()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 659998
    iget v1, p0, LX/3xm;->g:F

    iget v2, p0, LX/3xm;->e:F

    add-float/2addr v1, v2

    float-to-int v1, v1

    .line 659999
    iget-object v2, p0, LX/3xm;->z:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int v2, v1, v2

    iget-object v3, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v3

    sub-int v3, v2, v3

    .line 660000
    iget v2, p0, LX/3xm;->e:F

    cmpg-float v2, v2, v8

    if-gez v2, :cond_7

    if-gez v3, :cond_7

    .line 660001
    :cond_2
    :goto_2
    invoke-virtual {v0}, LX/1OR;->h()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 660002
    iget v0, p0, LX/3xm;->h:F

    iget v1, p0, LX/3xm;->f:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 660003
    iget-object v1, p0, LX/3xm;->z:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int v1, v0, v1

    iget-object v2, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v2

    sub-int v7, v1, v2

    .line 660004
    iget v1, p0, LX/3xm;->f:F

    cmpg-float v1, v1, v8

    if-gez v1, :cond_9

    if-gez v7, :cond_9

    .line 660005
    :cond_3
    :goto_3
    if-eqz v3, :cond_d

    .line 660006
    iget-object v0, p0, LX/3xm;->j:LX/3xj;

    iget-object v1, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, LX/3xm;->b:LX/1a1;

    iget-object v2, v2, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    iget-object v8, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v8}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    invoke-virtual/range {v0 .. v5}, LX/3xj;->a(Landroid/support/v7/widget/RecyclerView;IIJ)I

    move-result v3

    move v8, v3

    .line 660007
    :goto_4
    if-eqz v7, :cond_c

    .line 660008
    iget-object v0, p0, LX/3xm;->j:LX/3xj;

    iget-object v1, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, LX/3xm;->b:LX/1a1;

    iget-object v2, v2, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    iget-object v3, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move v3, v7

    invoke-virtual/range {v0 .. v5}, LX/3xj;->a(Landroid/support/v7/widget/RecyclerView;IIJ)I

    move-result v0

    .line 660009
    :goto_5
    if-nez v8, :cond_4

    if-eqz v0, :cond_b

    .line 660010
    :cond_4
    iget-wide v2, p0, LX/3xm;->A:J

    cmp-long v1, v2, v12

    if-nez v1, :cond_5

    .line 660011
    iput-wide v10, p0, LX/3xm;->A:J

    .line 660012
    :cond_5
    iget-object v1, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v8, v0}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    .line 660013
    const/4 v6, 0x1

    goto/16 :goto_0

    .line 660014
    :cond_6
    iget-wide v0, p0, LX/3xm;->A:J

    sub-long v4, v10, v0

    goto/16 :goto_1

    .line 660015
    :cond_7
    iget v2, p0, LX/3xm;->e:F

    cmpl-float v2, v2, v8

    if-lez v2, :cond_8

    .line 660016
    iget-object v2, p0, LX/3xm;->b:LX/1a1;

    iget-object v2, v2, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, LX/3xm;->z:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v2

    iget-object v2, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v2

    iget-object v3, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int v3, v1, v2

    .line 660017
    if-gtz v3, :cond_2

    :cond_8
    move v3, v6

    goto/16 :goto_2

    .line 660018
    :cond_9
    iget v1, p0, LX/3xm;->f:F

    cmpl-float v1, v1, v8

    if-lez v1, :cond_a

    .line 660019
    iget-object v1, p0, LX/3xm;->b:LX/1a1;

    iget-object v1, v1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LX/3xm;->z:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    iget-object v1, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v1

    iget-object v2, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    sub-int v7, v0, v1

    .line 660020
    if-gtz v7, :cond_3

    :cond_a
    move v7, v6

    goto/16 :goto_3

    .line 660021
    :cond_b
    iput-wide v12, p0, LX/3xm;->A:J

    goto/16 :goto_0

    :cond_c
    move v0, v7

    goto :goto_5

    :cond_d
    move v8, v3

    goto/16 :goto_4
.end method

.method public static f(LX/3xm;)V
    .locals 1

    .prologue
    .line 660057
    iget-object v0, p0, LX/3xm;->r:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 660058
    iget-object v0, p0, LX/3xm;->r:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 660059
    :cond_0
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, LX/3xm;->r:Landroid/view/VelocityTracker;

    .line 660060
    return-void
.end method

.method public static g(LX/3xm;)V
    .locals 1

    .prologue
    .line 660061
    iget-object v0, p0, LX/3xm;->r:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 660062
    iget-object v0, p0, LX/3xm;->r:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 660063
    const/4 v0, 0x0

    iput-object v0, p0, LX/3xm;->r:Landroid/view/VelocityTracker;

    .line 660064
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 660065
    const/4 v0, -0x1

    iput v0, p0, LX/3xm;->w:I

    .line 660066
    iget-object v0, p0, LX/3xm;->b:LX/1a1;

    if-eqz v0, :cond_0

    .line 660067
    iget-object v0, p0, LX/3xm;->n:[F

    invoke-direct {p0, v0}, LX/3xm;->a([F)V

    .line 660068
    iget-object v0, p0, LX/3xm;->n:[F

    const/4 v1, 0x0

    aget v6, v0, v1

    .line 660069
    iget-object v0, p0, LX/3xm;->n:[F

    const/4 v1, 0x1

    aget v7, v0, v1

    .line 660070
    :goto_0
    iget-object v3, p0, LX/3xm;->b:LX/1a1;

    iget-object v4, p0, LX/3xm;->m:Ljava/util/List;

    iget v5, p0, LX/3xm;->k:I

    move-object v1, p1

    move-object v2, p2

    .line 660071
    invoke-static/range {v1 .. v7}, LX/3xj;->a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1a1;Ljava/util/List;IFF)V

    .line 660072
    return-void

    :cond_0
    move v6, v7

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 0

    .prologue
    .line 660073
    invoke-virtual {p1}, Landroid/graphics/Rect;->setEmpty()V

    .line 660074
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;)V
    .locals 4

    .prologue
    .line 660075
    iget-object v0, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    if-ne v0, p1, :cond_1

    .line 660076
    :cond_0
    :goto_0
    return-void

    .line 660077
    :cond_1
    iget-object v0, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_3

    .line 660078
    iget-object v0, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView;->b(LX/3x6;)V

    .line 660079
    iget-object v0, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, LX/3xm;->y:LX/1OH;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->b(LX/1OH;)V

    .line 660080
    iget-object v0, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView;->b(LX/3x8;)V

    .line 660081
    iget-object v0, p0, LX/3xm;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 660082
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_2

    .line 660083
    iget-object v0, p0, LX/3xm;->m:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3xe;

    .line 660084
    iget-object v2, p0, LX/3xm;->j:LX/3xj;

    iget-object v3, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, LX/3xe;->h:LX/1a1;

    invoke-virtual {v2, v3, v0}, LX/3xj;->b(Landroid/support/v7/widget/RecyclerView;LX/1a1;)V

    .line 660085
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 660086
    :cond_2
    iget-object v0, p0, LX/3xm;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 660087
    const/4 v0, 0x0

    iput-object v0, p0, LX/3xm;->v:Landroid/view/View;

    .line 660088
    const/4 v0, -0x1

    iput v0, p0, LX/3xm;->w:I

    .line 660089
    invoke-static {p0}, LX/3xm;->g(LX/3xm;)V

    .line 660090
    :cond_3
    iput-object p1, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    .line 660091
    iget-object v0, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 660092
    iget-object v0, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 660093
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, LX/3xm;->o:I

    .line 660094
    iget-object v0, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 660095
    iget-object v0, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, LX/3xm;->y:LX/1OH;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OH;)V

    .line 660096
    iget-object v0, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x8;)V

    .line 660097
    iget-object v0, p0, LX/3xm;->x:LX/3rW;

    if-eqz v0, :cond_4

    .line 660098
    :goto_2
    goto :goto_0

    .line 660099
    :cond_4
    new-instance v0, LX/3rW;

    iget-object v1, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/3xk;

    invoke-direct {v2, p0}, LX/3xk;-><init>(LX/3xm;)V

    invoke-direct {v0, v1, v2}, LX/3rW;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/3xm;->x:LX/3rW;

    goto :goto_2
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 660100
    invoke-static {p0, p1}, LX/3xm;->b(LX/3xm;Landroid/view/View;)V

    .line 660101
    iget-object v0, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 660102
    if-nez v0, :cond_1

    .line 660103
    :cond_0
    :goto_0
    return-void

    .line 660104
    :cond_1
    iget-object v1, p0, LX/3xm;->b:LX/1a1;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/3xm;->b:LX/1a1;

    if-ne v0, v1, :cond_2

    .line 660105
    const/4 v0, 0x0

    invoke-static {p0, v0, v2}, LX/3xm;->a$redex0(LX/3xm;LX/1a1;I)V

    goto :goto_0

    .line 660106
    :cond_2
    invoke-static {p0, v0, v2}, LX/3xm;->a$redex0(LX/3xm;LX/1a1;Z)I

    .line 660107
    iget-object v1, p0, LX/3xm;->a:Ljava/util/List;

    iget-object v2, v0, LX/1a1;->a:Landroid/view/View;

    invoke-interface {v1, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 660108
    iget-object v1, p0, LX/3xm;->j:LX/3xj;

    iget-object v2, p0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v2, v0}, LX/3xj;->b(Landroid/support/v7/widget/RecyclerView;LX/1a1;)V

    goto :goto_0
.end method

.method public final b(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 660109
    iget-object v0, p0, LX/3xm;->b:LX/1a1;

    if-eqz v0, :cond_0

    .line 660110
    iget-object v0, p0, LX/3xm;->n:[F

    invoke-direct {p0, v0}, LX/3xm;->a([F)V

    .line 660111
    iget-object v0, p0, LX/3xm;->n:[F

    const/4 v1, 0x0

    aget v6, v0, v1

    .line 660112
    iget-object v0, p0, LX/3xm;->n:[F

    const/4 v1, 0x1

    aget v7, v0, v1

    .line 660113
    :goto_0
    iget-object v3, p0, LX/3xm;->b:LX/1a1;

    iget-object v4, p0, LX/3xm;->m:Ljava/util/List;

    iget v5, p0, LX/3xm;->k:I

    move-object v1, p1

    move-object v2, p2

    .line 660114
    invoke-static/range {v1 .. v7}, LX/3xj;->b(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1a1;Ljava/util/List;IFF)V

    .line 660115
    return-void

    :cond_0
    move v6, v7

    goto :goto_0
.end method
