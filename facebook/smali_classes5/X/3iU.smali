.class public LX/3iU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:LX/20i;

.field public final b:LX/0aG;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/3iV;

.field public final e:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final f:LX/0ad;


# direct methods
.method public constructor <init>(LX/20i;LX/0aG;LX/0Or;LX/3iV;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            "LX/0aG;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/3iV;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 629579
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 629580
    iput-object p1, p0, LX/3iU;->a:LX/20i;

    .line 629581
    iput-object p2, p0, LX/3iU;->b:LX/0aG;

    .line 629582
    iput-object p3, p0, LX/3iU;->c:LX/0Or;

    .line 629583
    iput-object p4, p0, LX/3iU;->d:LX/3iV;

    .line 629584
    iput-object p5, p0, LX/3iU;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 629585
    iput-object p6, p0, LX/3iU;->f:LX/0ad;

    .line 629586
    return-void
.end method

.method public static a(LX/0QB;)LX/3iU;
    .locals 10

    .prologue
    .line 629587
    const-class v1, LX/3iU;

    monitor-enter v1

    .line 629588
    :try_start_0
    sget-object v0, LX/3iU;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 629589
    sput-object v2, LX/3iU;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 629590
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629591
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 629592
    new-instance v3, LX/3iU;

    invoke-static {v0}, LX/20i;->a(LX/0QB;)LX/20i;

    move-result-object v4

    check-cast v4, LX/20i;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v5

    check-cast v5, LX/0aG;

    const/16 v6, 0x19e

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/3iV;->a(LX/0QB;)LX/3iV;

    move-result-object v7

    check-cast v7, LX/3iV;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v8

    check-cast v8, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-direct/range {v3 .. v9}, LX/3iU;-><init>(LX/20i;LX/0aG;LX/0Or;LX/3iV;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ad;)V

    .line 629593
    move-object v0, v3

    .line 629594
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 629595
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3iU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 629596
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 629597
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/3iU;)Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 2

    .prologue
    .line 629598
    iget-object v0, p0, LX/3iU;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 629599
    iget-boolean v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v1, v1

    .line 629600
    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;LX/5Gr;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;",
            "LX/5Gr;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 629601
    iget-object v0, p0, LX/3iU;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x39001d

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 629602
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 629603
    iget-object v4, p2, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->f:Lcom/facebook/ipc/media/MediaItem;

    if-nez v4, :cond_1

    .line 629604
    iget-object v3, p2, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->g:Lcom/facebook/ipc/media/StickerItem;

    if-eqz v3, :cond_0

    .line 629605
    iget-object v3, p2, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->g:Lcom/facebook/ipc/media/StickerItem;

    .line 629606
    iget-wide v7, v3, Lcom/facebook/ipc/media/StickerItem;->b:J

    move-wide v3, v7

    .line 629607
    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 629608
    iput-object v3, p3, LX/5Gr;->g:Ljava/lang/String;

    .line 629609
    :cond_0
    iget-object v3, p0, LX/3iU;->d:LX/3iV;

    invoke-virtual {p3}, LX/5Gr;->a()Lcom/facebook/api/ufiservices/common/AddCommentParams;

    move-result-object v4

    invoke-virtual {v3, v4, p4}, LX/3iV;->a(Lcom/facebook/api/ufiservices/common/AddCommentParams;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 629610
    :goto_0
    move-object v0, v3

    .line 629611
    new-instance v1, LX/8r5;

    invoke-direct {v1, p0, p1}, LX/8r5;-><init>(LX/3iU;Ljava/lang/String;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 629612
    return-object v0

    .line 629613
    :cond_1
    iget-object v4, p2, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->f:Lcom/facebook/ipc/media/MediaItem;

    .line 629614
    sget-object v5, Lcom/facebook/ipc/media/data/MimeType;->d:Lcom/facebook/ipc/media/data/MimeType;

    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v6

    .line 629615
    iget-object v7, v6, Lcom/facebook/ipc/media/data/MediaData;->mMimeType:Lcom/facebook/ipc/media/data/MimeType;

    move-object v6, v7

    .line 629616
    invoke-virtual {v5, v6}, Lcom/facebook/ipc/media/data/MimeType;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    instance-of v5, v4, Lcom/facebook/photos/base/media/VideoItem;

    if-eqz v5, :cond_3

    .line 629617
    :cond_2
    :goto_1
    move-object v4, v4

    .line 629618
    sget-object v5, LX/8r7;->a:[I

    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v6

    .line 629619
    iget-object v7, v6, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v6, v7

    .line 629620
    invoke-virtual {v6}, LX/4gQ;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 629621
    iget-object v3, p0, LX/3iU;->d:LX/3iV;

    invoke-virtual {p3}, LX/5Gr;->a()Lcom/facebook/api/ufiservices/common/AddCommentParams;

    move-result-object v4

    invoke-virtual {v3, v4, p4}, LX/3iV;->a(Lcom/facebook/api/ufiservices/common/AddCommentParams;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    goto :goto_0

    .line 629622
    :pswitch_0
    const-string v5, "addPhotoAttachmentParams"

    invoke-static {}, Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;->a()LX/6BG;

    move-result-object v6

    .line 629623
    iput-object p1, v6, LX/6BG;->b:Ljava/lang/String;

    .line 629624
    move-object v6, v6

    .line 629625
    iput-object v4, v6, LX/6BG;->a:Lcom/facebook/ipc/media/MediaItem;

    .line 629626
    move-object v4, v6

    .line 629627
    invoke-static {p0}, LX/3iU;->a(LX/3iU;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v6

    .line 629628
    iput-object v6, v4, LX/6BG;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 629629
    move-object v4, v4

    .line 629630
    invoke-virtual {v4}, LX/6BG;->a()Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 629631
    iget-object v4, p0, LX/3iU;->b:LX/0aG;

    const-string v5, "feed_add_photo"

    const v6, -0x7f7d432a

    invoke-static {v4, v5, v3, v6}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v3

    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    move-result-object v3

    .line 629632
    :goto_2
    new-instance v4, LX/8r6;

    invoke-direct {v4, p0, p3, p4}, LX/8r6;-><init>(LX/3iU;LX/5Gr;Z)V

    .line 629633
    invoke-static {v3, v4}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    goto :goto_0

    .line 629634
    :pswitch_1
    const-string v5, "addVideoAttachmentParams"

    invoke-static {}, Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;->a()LX/6BG;

    move-result-object v6

    .line 629635
    iput-object p1, v6, LX/6BG;->b:Ljava/lang/String;

    .line 629636
    move-object v6, v6

    .line 629637
    iput-object v4, v6, LX/6BG;->a:Lcom/facebook/ipc/media/MediaItem;

    .line 629638
    move-object v4, v6

    .line 629639
    invoke-static {p0}, LX/3iU;->a(LX/3iU;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v6

    .line 629640
    iput-object v6, v4, LX/6BG;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 629641
    move-object v4, v4

    .line 629642
    invoke-virtual {v4}, LX/6BG;->a()Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 629643
    iget-object v4, p0, LX/3iU;->b:LX/0aG;

    const-string v5, "feed_add_video"

    const v6, -0x25e45b0f

    invoke-static {v4, v5, v3, v6}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v3

    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    move-result-object v3

    goto :goto_2

    :cond_3
    check-cast v4, Lcom/facebook/photos/base/media/PhotoItem;

    .line 629644
    new-instance v5, LX/74m;

    invoke-direct {v5}, LX/74m;-><init>()V

    .line 629645
    iget-object v6, v4, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v6, v6

    .line 629646
    sget-object v7, LX/4gQ;->Video:LX/4gQ;

    invoke-static {v6, v7}, LX/74c;->a(Lcom/facebook/ipc/media/data/LocalMediaData;LX/4gQ;)Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v6

    .line 629647
    iput-object v6, v5, LX/74m;->e:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 629648
    move-object v5, v5

    .line 629649
    invoke-virtual {v5}, LX/74m;->a()Lcom/facebook/photos/base/media/VideoItem;

    move-result-object v5

    move-object v4, v5

    .line 629650
    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
