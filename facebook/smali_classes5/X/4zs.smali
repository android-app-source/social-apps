.class public final LX/4zs;
.super LX/301;
.source ""

# interfaces
.implements LX/0Ri;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/301",
        "<TK;TV;>;",
        "LX/0Ri",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final d:LX/0Ri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ri",
            "<TV;TK;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ri;LX/0Rl;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ri",
            "<TK;TV;>;",
            "LX/0Rl",
            "<-",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 823103
    invoke-direct {p0, p1, p2}, LX/301;-><init>(Ljava/util/Map;LX/0Rl;)V

    .line 823104
    new-instance v0, LX/4zs;

    invoke-interface {p1}, LX/0Ri;->a_()LX/0Ri;

    move-result-object v1

    .line 823105
    new-instance v2, LX/4zr;

    invoke-direct {v2, p2}, LX/4zr;-><init>(LX/0Rl;)V

    move-object v2, v2

    .line 823106
    invoke-direct {v0, v1, v2, p0}, LX/4zs;-><init>(LX/0Ri;LX/0Rl;LX/0Ri;)V

    iput-object v0, p0, LX/4zs;->d:LX/0Ri;

    .line 823107
    return-void
.end method

.method private constructor <init>(LX/0Ri;LX/0Rl;LX/0Ri;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ri",
            "<TK;TV;>;",
            "LX/0Rl",
            "<-",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;",
            "LX/0Ri",
            "<TV;TK;>;)V"
        }
    .end annotation

    .prologue
    .line 823100
    invoke-direct {p0, p1, p2}, LX/301;-><init>(Ljava/util/Map;LX/0Rl;)V

    .line 823101
    iput-object p3, p0, LX/4zs;->d:LX/0Ri;

    .line 823102
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 823097
    invoke-virtual {p0, p1, p2}, LX/300;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 823098
    invoke-virtual {p0}, LX/4zs;->d()LX/0Ri;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/0Ri;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a_()LX/0Ri;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Ri",
            "<TV;TK;>;"
        }
    .end annotation

    .prologue
    .line 823108
    iget-object v0, p0, LX/4zs;->d:LX/0Ri;

    return-object v0
.end method

.method public final b_()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 823099
    iget-object v0, p0, LX/4zs;->d:LX/0Ri;

    invoke-interface {v0}, LX/0Ri;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/0Ri;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Ri",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 823096
    iget-object v0, p0, LX/300;->a:Ljava/util/Map;

    check-cast v0, LX/0Ri;

    return-object v0
.end method

.method public final synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 823095
    invoke-virtual {p0}, LX/4zs;->b_()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
