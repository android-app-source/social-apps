.class public LX/4du;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Ft;


# instance fields
.field private final a:Ljava/util/concurrent/Executor;

.field private final b:Ljava/util/concurrent/Executor;

.field private final c:Ljava/util/concurrent/Executor;

.field private final d:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 796519
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 796520
    new-instance v0, LX/4e0;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, LX/4e0;-><init>(I)V

    .line 796521
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    iput-object v1, p0, LX/4du;->a:Ljava/util/concurrent/Executor;

    .line 796522
    invoke-static {p1, v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    iput-object v1, p0, LX/4du;->b:Ljava/util/concurrent/Executor;

    .line 796523
    invoke-static {p1, v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    iput-object v1, p0, LX/4du;->c:Ljava/util/concurrent/Executor;

    .line 796524
    const/4 v1, 0x1

    invoke-static {v1, v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, LX/4du;->d:Ljava/util/concurrent/Executor;

    .line 796525
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 796526
    iget-object v0, p0, LX/4du;->a:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public final b()Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 796527
    iget-object v0, p0, LX/4du;->a:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public final c()Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 796528
    iget-object v0, p0, LX/4du;->b:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public final d()Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 796529
    iget-object v0, p0, LX/4du;->c:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public final e()Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 796530
    iget-object v0, p0, LX/4du;->d:Ljava/util/concurrent/Executor;

    return-object v0
.end method
