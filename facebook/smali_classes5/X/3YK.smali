.class public LX/3YK;
.super LX/24d;
.source ""

# interfaces
.implements LX/3VQ;
.implements LX/3VR;


# static fields
.field public static final b:LX/1Cz;


# instance fields
.field public c:Lcom/facebook/attachments/angora/InstantArticleIconView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 595751
    new-instance v0, LX/3YL;

    invoke-direct {v0}, LX/3YL;-><init>()V

    sput-object v0, LX/3YK;->b:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 595752
    invoke-direct {p0, p1}, LX/24d;-><init>(Landroid/content/Context;)V

    .line 595753
    const/4 p1, 0x0

    .line 595754
    new-instance v0, Lcom/facebook/attachments/angora/InstantArticleIconView;

    invoke-virtual {p0}, LX/3YK;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/attachments/angora/InstantArticleIconView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/3YK;->c:Lcom/facebook/attachments/angora/InstantArticleIconView;

    .line 595755
    iget-object v0, p0, LX/3YK;->c:Lcom/facebook/attachments/angora/InstantArticleIconView;

    invoke-virtual {v0, p1}, Lcom/facebook/attachments/angora/InstantArticleIconView;->setBackgroundColor(I)V

    .line 595756
    iget-object v0, p0, LX/3YK;->c:Lcom/facebook/attachments/angora/InstantArticleIconView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/angora/InstantArticleIconView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 595757
    iget-object v0, p0, LX/3YK;->c:Lcom/facebook/attachments/angora/InstantArticleIconView;

    const v1, 0x7f0d0042

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/angora/InstantArticleIconView;->setId(I)V

    .line 595758
    invoke-virtual {p0}, LX/3YK;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b062a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 595759
    invoke-virtual {p0}, LX/3YK;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0108

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 595760
    invoke-virtual {p0}, LX/3YK;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0107

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 595761
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v0, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 595762
    invoke-virtual {v3, p1, v1, v2, p1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 595763
    const/16 v0, 0xb

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 595764
    const/16 v0, 0x15

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 595765
    const/16 v0, 0xa

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 595766
    iget-object v0, p0, LX/3YK;->c:Lcom/facebook/attachments/angora/InstantArticleIconView;

    invoke-virtual {p0, v0, v3}, LX/3YK;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 595767
    invoke-virtual {p0}, LX/24d;->getPhotoAttachmentView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setFocusable(Z)V

    .line 595768
    invoke-virtual {p0}, LX/24d;->getPhotoAttachmentView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    .line 595769
    return-void
.end method


# virtual methods
.method public getTooltipAnchor()Landroid/view/View;
    .locals 1

    .prologue
    .line 595770
    iget-object v0, p0, LX/3YK;->c:Lcom/facebook/attachments/angora/InstantArticleIconView;

    return-object v0
.end method

.method public setCoverPhotoArticleIconVisibility(I)V
    .locals 1

    .prologue
    .line 595771
    iget-object v0, p0, LX/3YK;->c:Lcom/facebook/attachments/angora/InstantArticleIconView;

    invoke-virtual {v0, p1}, Lcom/facebook/attachments/angora/InstantArticleIconView;->setVisibility(I)V

    .line 595772
    return-void
.end method
