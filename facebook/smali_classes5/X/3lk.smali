.class public LX/3lk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/pm/PackageManager;

.field private final b:Landroid/content/ComponentName;

.field public final c:Landroid/content/ComponentName;


# direct methods
.method public constructor <init>(Landroid/content/pm/PackageManager;Landroid/content/Context;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 634885
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 634886
    iput-object p1, p0, LX/3lk;->a:Landroid/content/pm/PackageManager;

    .line 634887
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/facebook/placetips/pulsarcore/service/BluetoothScanFinishedReceiver;

    invoke-direct {v0, p2, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, LX/3lk;->b:Landroid/content/ComponentName;

    .line 634888
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/facebook/placetips/pulsarcore/service/PulsarService;

    invoke-direct {v0, p2, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, LX/3lk;->c:Landroid/content/ComponentName;

    .line 634889
    return-void
.end method

.method public static a(LX/3lk;ILandroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 634890
    iget-object v0, p0, LX/3lk;->a:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, p2}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v0

    .line 634891
    if-ne v0, p1, :cond_0

    .line 634892
    :goto_0
    return-void

    .line 634893
    :cond_0
    iget-object v0, p0, LX/3lk;->a:Landroid/content/pm/PackageManager;

    const/4 v1, 0x1

    invoke-virtual {v0, p2, p1, v1}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/3lk;
    .locals 3

    .prologue
    .line 634894
    new-instance v2, LX/3lk;

    invoke-static {p0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManager;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-direct {v2, v0, v1}, LX/3lk;-><init>(Landroid/content/pm/PackageManager;Landroid/content/Context;)V

    .line 634895
    return-object v2
.end method

.method public static c(Z)I
    .locals 1

    .prologue
    .line 634896
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 634897
    invoke-static {p1}, LX/3lk;->c(Z)I

    move-result v0

    iget-object v1, p0, LX/3lk;->b:Landroid/content/ComponentName;

    invoke-static {p0, v0, v1}, LX/3lk;->a(LX/3lk;ILandroid/content/ComponentName;)V

    .line 634898
    return-void
.end method
