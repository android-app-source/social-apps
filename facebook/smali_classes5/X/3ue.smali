.class public final LX/3ue;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3uG;


# instance fields
.field public final a:Landroid/view/ActionMode$Callback;

.field public final b:Landroid/content/Context;

.field public final c:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "LX/3uV;",
            "LX/3uf;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Landroid/view/Menu;",
            "Landroid/view/Menu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ActionMode$Callback;)V
    .locals 1

    .prologue
    .line 649075
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 649076
    iput-object p1, p0, LX/3ue;->b:Landroid/content/Context;

    .line 649077
    iput-object p2, p0, LX/3ue;->a:Landroid/view/ActionMode$Callback;

    .line 649078
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/3ue;->c:LX/01J;

    .line 649079
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/3ue;->d:LX/01J;

    .line 649080
    return-void
.end method

.method private a(Landroid/view/Menu;)Landroid/view/Menu;
    .locals 2

    .prologue
    .line 649070
    iget-object v0, p0, LX/3ue;->d:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Menu;

    .line 649071
    if-nez v0, :cond_0

    .line 649072
    iget-object v1, p0, LX/3ue;->b:Landroid/content/Context;

    move-object v0, p1

    check-cast v0, LX/3qu;

    invoke-static {v1, v0}, LX/3vE;->a(Landroid/content/Context;LX/3qu;)Landroid/view/Menu;

    move-result-object v0

    .line 649073
    iget-object v1, p0, LX/3ue;->d:LX/01J;

    invoke-virtual {v1, p1, v0}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 649074
    :cond_0
    return-object v0
.end method

.method private b(LX/3uV;)Landroid/view/ActionMode;
    .locals 2

    .prologue
    .line 649065
    iget-object v0, p0, LX/3ue;->c:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3uf;

    .line 649066
    if-eqz v0, :cond_0

    .line 649067
    :goto_0
    return-object v0

    .line 649068
    :cond_0
    new-instance v0, LX/3uf;

    iget-object v1, p0, LX/3ue;->b:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, LX/3uf;-><init>(Landroid/content/Context;LX/3uV;)V

    .line 649069
    iget-object v1, p0, LX/3ue;->c:LX/01J;

    invoke-virtual {v1, p1, v0}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/3uV;)V
    .locals 2

    .prologue
    .line 649063
    iget-object v0, p0, LX/3ue;->a:Landroid/view/ActionMode$Callback;

    invoke-direct {p0, p1}, LX/3ue;->b(LX/3uV;)Landroid/view/ActionMode;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/ActionMode$Callback;->onDestroyActionMode(Landroid/view/ActionMode;)V

    .line 649064
    return-void
.end method

.method public final a(LX/3uV;Landroid/view/Menu;)Z
    .locals 3

    .prologue
    .line 649062
    iget-object v0, p0, LX/3ue;->a:Landroid/view/ActionMode$Callback;

    invoke-direct {p0, p1}, LX/3ue;->b(LX/3uV;)Landroid/view/ActionMode;

    move-result-object v1

    invoke-direct {p0, p2}, LX/3ue;->a(Landroid/view/Menu;)Landroid/view/Menu;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/view/ActionMode$Callback;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final a(LX/3uV;Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 649061
    iget-object v0, p0, LX/3ue;->a:Landroid/view/ActionMode$Callback;

    invoke-direct {p0, p1}, LX/3ue;->b(LX/3uV;)Landroid/view/ActionMode;

    move-result-object v1

    iget-object v2, p0, LX/3ue;->b:Landroid/content/Context;

    check-cast p2, LX/3qv;

    invoke-static {v2, p2}, LX/3vE;->a(Landroid/content/Context;LX/3qv;)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/view/ActionMode$Callback;->onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/3uV;Landroid/view/Menu;)Z
    .locals 3

    .prologue
    .line 649060
    iget-object v0, p0, LX/3ue;->a:Landroid/view/ActionMode$Callback;

    invoke-direct {p0, p1}, LX/3ue;->b(LX/3uV;)Landroid/view/ActionMode;

    move-result-object v1

    invoke-direct {p0, p2}, LX/3ue;->a(Landroid/view/Menu;)Landroid/view/Menu;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/view/ActionMode$Callback;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method
