.class public final LX/3xo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3xn;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 660354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/view/View;FF)V
    .locals 2

    .prologue
    .line 660355
    invoke-virtual {p0}, Landroid/graphics/Canvas;->save()I

    .line 660356
    invoke-virtual {p0, p3, p4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 660357
    const-wide/16 v0, 0x0

    invoke-virtual {p1, p0, p2, v0, v1}, Landroid/support/v7/widget/RecyclerView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    .line 660358
    invoke-virtual {p0}, Landroid/graphics/Canvas;->restore()V

    .line 660359
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/view/View;FFI)V
    .locals 1

    .prologue
    .line 660360
    const/4 v0, 0x2

    if-ne p6, v0, :cond_0

    .line 660361
    invoke-static {p1, p2, p3, p4, p5}, LX/3xo;->a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/view/View;FF)V

    .line 660362
    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/view/View;FFIZ)V
    .locals 1

    .prologue
    .line 660363
    const/4 v0, 0x2

    if-eq p6, v0, :cond_0

    .line 660364
    invoke-static {p1, p2, p3, p4, p5}, LX/3xo;->a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/view/View;FF)V

    .line 660365
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 660366
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 660367
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 660368
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 660369
    return-void
.end method
