.class public final LX/3gn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel;",
        ">;",
        "LX/0Px",
        "<",
        "Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1zr;


# direct methods
.method public constructor <init>(LX/1zr;)V
    .locals 0

    .prologue
    .line 624847
    iput-object p1, p0, LX/3gn;->a:LX/1zr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 624839
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 624840
    if-nez p1, :cond_0

    .line 624841
    const/4 v0, 0x0

    .line 624842
    :goto_0
    return-object v0

    .line 624843
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 624844
    check-cast v0, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel;

    invoke-virtual {v0}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 624845
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 624846
    goto :goto_0
.end method
