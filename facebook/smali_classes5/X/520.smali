.class public abstract LX/520;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 825678
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Ljava/io/Reader;
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 825671
    invoke-static {}, LX/1vJ;->a()LX/1vJ;

    move-result-object v1

    .line 825672
    :try_start_0
    invoke-virtual {p0}, LX/520;->a()Ljava/io/Reader;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1vJ;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/Reader;

    .line 825673
    invoke-static {v0}, LX/2Eb;->a(Ljava/lang/Readable;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 825674
    invoke-virtual {v1}, LX/1vJ;->close()V

    return-object v0

    .line 825675
    :catch_0
    move-exception v0

    .line 825676
    :try_start_1
    invoke-virtual {v1, v0}, LX/1vJ;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 825677
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/1vJ;->close()V

    throw v0
.end method
