.class public LX/3rW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/3rS;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V
    .locals 1

    .prologue
    .line 643230
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3rW;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    .line 643231
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V
    .locals 2

    .prologue
    .line 643232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 643233
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-le v0, v1, :cond_0

    .line 643234
    new-instance v0, LX/3rV;

    invoke-direct {v0, p1, p2, p3}, LX/3rV;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    iput-object v0, p0, LX/3rW;->a:LX/3rS;

    .line 643235
    :goto_0
    return-void

    .line 643236
    :cond_0
    new-instance v0, LX/3rU;

    invoke-direct {v0, p1, p2, p3}, LX/3rU;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    iput-object v0, p0, LX/3rW;->a:LX/3rS;

    goto :goto_0
.end method


# virtual methods
.method public final a(Z)V
    .locals 1

    .prologue
    .line 643237
    iget-object v0, p0, LX/3rW;->a:LX/3rS;

    invoke-interface {v0, p1}, LX/3rS;->a(Z)V

    .line 643238
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 643239
    iget-object v0, p0, LX/3rW;->a:LX/3rS;

    invoke-interface {v0, p1}, LX/3rS;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
