.class public final LX/4AA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2sN;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public final synthetic d:LX/4A8;


# direct methods
.method public constructor <init>(LX/4A8;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 675399
    iput-object p1, p0, LX/4AA;->d:LX/4A8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 675400
    iput v0, p0, LX/4AA;->a:I

    .line 675401
    iput v0, p0, LX/4AA;->c:I

    .line 675402
    iget v0, p1, LX/4A8;->a:I

    iput v0, p0, LX/4AA;->b:I

    .line 675403
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 675404
    iget v0, p0, LX/4AA;->a:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, LX/4AA;->d:LX/4A8;

    invoke-virtual {v1}, LX/39O;->c()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/1vs;
    .locals 4

    .prologue
    .line 675405
    iget v0, p0, LX/4AA;->b:I

    iget-object v1, p0, LX/4AA;->d:LX/4A8;

    iget v1, v1, LX/4A8;->a:I

    if-ne v0, v1, :cond_0

    .line 675406
    :try_start_0
    iget-object v0, p0, LX/4AA;->d:LX/4A8;

    iget v1, p0, LX/4AA;->a:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, LX/4A8;->a(I)LX/1vs;

    move-result-object v0

    .line 675407
    iget-object v1, v0, LX/1vs;->a:LX/15i;

    .line 675408
    iget v2, v0, LX/1vs;->b:I

    .line 675409
    iget v0, v0, LX/1vs;->c:I

    .line 675410
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 675411
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 675412
    :try_start_2
    iget v3, p0, LX/4AA;->a:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/4AA;->a:I

    iput v3, p0, LX/4AA;->c:I

    .line 675413
    invoke-static {v1, v2, v0}, LX/1vs;->a(LX/15i;II)LX/1vs;
    :try_end_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    return-object v0

    .line 675414
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_4 .. :try_end_4} :catch_0

    .line 675415
    :catch_0
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 675416
    :cond_0
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0
.end method
