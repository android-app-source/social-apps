.class public LX/4hi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static a:Ljava/lang/Integer;

.field private static b:J

.field private static volatile d:LX/4hi;


# instance fields
.field private final c:LX/0So;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 801806
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, LX/4hi;->a:Ljava/lang/Integer;

    .line 801807
    const-wide/16 v0, 0x0

    sput-wide v0, LX/4hi;->b:J

    return-void
.end method

.method public constructor <init>(LX/0So;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 801803
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 801804
    iput-object p1, p0, LX/4hi;->c:LX/0So;

    .line 801805
    return-void
.end method

.method public static a(LX/0QB;)LX/4hi;
    .locals 3

    .prologue
    .line 801793
    sget-object v0, LX/4hi;->d:LX/4hi;

    if-nez v0, :cond_1

    .line 801794
    const-class v1, LX/4hi;

    monitor-enter v1

    .line 801795
    :try_start_0
    sget-object v0, LX/4hi;->d:LX/4hi;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 801796
    if-eqz v2, :cond_0

    .line 801797
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/4hi;->b(LX/0QB;)LX/4hi;

    move-result-object v0

    sput-object v0, LX/4hi;->d:LX/4hi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 801798
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 801799
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 801800
    :cond_1
    sget-object v0, LX/4hi;->d:LX/4hi;

    return-object v0

    .line 801801
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 801802
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/4hi;
    .locals 2

    .prologue
    .line 801783
    new-instance v1, LX/4hi;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-direct {v1, v0}, LX/4hi;-><init>(LX/0So;)V

    .line 801784
    return-object v1
.end method


# virtual methods
.method public final a()Z
    .locals 8

    .prologue
    const-wide/32 v6, 0xea60

    const/4 v0, 0x0

    .line 801785
    sget-object v1, LX/4hi;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, LX/4hi;->a:Ljava/lang/Integer;

    .line 801786
    iget-object v1, p0, LX/4hi;->c:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    sget-wide v4, LX/4hi;->b:J

    sub-long/2addr v2, v4

    .line 801787
    sget-object v1, LX/4hi;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v4, 0x3e8

    if-le v1, v4, :cond_0

    cmp-long v1, v2, v6

    if-gez v1, :cond_0

    .line 801788
    :goto_0
    return v0

    .line 801789
    :cond_0
    cmp-long v1, v2, v6

    if-ltz v1, :cond_1

    .line 801790
    sget-wide v4, LX/4hi;->b:J

    add-long/2addr v2, v4

    sput-wide v2, LX/4hi;->b:J

    .line 801791
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, LX/4hi;->a:Ljava/lang/Integer;

    .line 801792
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
