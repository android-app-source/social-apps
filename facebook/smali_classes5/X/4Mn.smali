.class public LX/4Mn;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 689259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 689260
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 689261
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 689262
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 689263
    invoke-static {p0, p1}, LX/4Mn;->b(LX/15w;LX/186;)I

    move-result v1

    .line 689264
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 689265
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 689266
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 689267
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 689268
    if-eqz v0, :cond_0

    .line 689269
    const-string v1, "offset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689270
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 689271
    :cond_0
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 689272
    if-eqz v0, :cond_1

    .line 689273
    const-string v0, "text_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689274
    const-class v0, Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 689275
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 689276
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 689277
    const/4 v0, 0x0

    .line 689278
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_6

    .line 689279
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 689280
    :goto_0
    return v1

    .line 689281
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_3

    .line 689282
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 689283
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 689284
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_0

    if-eqz v6, :cond_0

    .line 689285
    const-string v7, "offset"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 689286
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v5, v3

    move v3, v2

    goto :goto_1

    .line 689287
    :cond_1
    const-string v7, "text_type"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 689288
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    move-result-object v0

    move-object v4, v0

    move v0, v2

    goto :goto_1

    .line 689289
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 689290
    :cond_3
    const/4 v6, 0x2

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 689291
    if-eqz v3, :cond_4

    .line 689292
    invoke-virtual {p1, v1, v5, v1}, LX/186;->a(III)V

    .line 689293
    :cond_4
    if-eqz v0, :cond_5

    .line 689294
    invoke-virtual {p1, v2, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 689295
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v3, v1

    move-object v4, v0

    move v5, v1

    move v0, v1

    goto :goto_1
.end method
