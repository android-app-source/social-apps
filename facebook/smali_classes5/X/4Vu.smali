.class public final LX/4Vu;
.super LX/0ur;
.source ""


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:I

.field public D:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:I

.field public F:Lcom/facebook/graphql/model/GraphQLPostTranslatability;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Z

.field public b:I

.field public c:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Lcom/facebook/graphql/model/GraphQLComment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:J

.field public o:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:I

.field public x:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 745862
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 745863
    instance-of v0, p0, LX/4Vu;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 745864
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLComment;)LX/4Vu;
    .locals 4

    .prologue
    .line 745865
    new-instance v0, LX/4Vu;

    invoke-direct {v0}, LX/4Vu;-><init>()V

    .line 745866
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 745867
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->n()I

    move-result v1

    iput v1, v0, LX/4Vu;->b:I

    .line 745868
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iput-object v1, v0, LX/4Vu;->c:Lcom/facebook/graphql/model/GraphQLStory;

    .line 745869
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->p()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Vu;->d:LX/0Px;

    .line 745870
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    iput-object v1, v0, LX/4Vu;->e:Lcom/facebook/graphql/model/GraphQLActor;

    .line 745871
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4Vu;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 745872
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4Vu;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 745873
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->P()Z

    move-result v1

    iput-boolean v1, v0, LX/4Vu;->h:Z

    .line 745874
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->t()Z

    move-result v1

    iput-boolean v1, v0, LX/4Vu;->i:Z

    .line 745875
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->u()Z

    move-result v1

    iput-boolean v1, v0, LX/4Vu;->j:Z

    .line 745876
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->T()Z

    move-result v1

    iput-boolean v1, v0, LX/4Vu;->k:Z

    .line 745877
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->v()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v1

    iput-object v1, v0, LX/4Vu;->l:Lcom/facebook/graphql/model/GraphQLComment;

    .line 745878
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->w()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Vu;->m:Ljava/lang/String;

    .line 745879
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->x()J

    move-result-wide v2

    iput-wide v2, v0, LX/4Vu;->n:J

    .line 745880
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->y()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v1

    iput-object v1, v0, LX/4Vu;->o:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    .line 745881
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    iput-object v1, v0, LX/4Vu;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 745882
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Vu;->q:Ljava/lang/String;

    .line 745883
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->A()Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4Vu;->r:Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    .line 745884
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->B()Z

    move-result v1

    iput-boolean v1, v0, LX/4Vu;->s:Z

    .line 745885
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->C()Z

    move-result v1

    iput-boolean v1, v0, LX/4Vu;->t:Z

    .line 745886
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->D()Z

    move-result v1

    iput-boolean v1, v0, LX/4Vu;->u:Z

    .line 745887
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->O()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Vu;->v:Ljava/lang/String;

    .line 745888
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->R()I

    move-result v1

    iput v1, v0, LX/4Vu;->w:I

    .line 745889
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->E()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    iput-object v1, v0, LX/4Vu;->x:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 745890
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4Vu;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 745891
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->G()Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    move-result-object v1

    iput-object v1, v0, LX/4Vu;->z:Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    .line 745892
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->Q()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v1

    iput-object v1, v0, LX/4Vu;->A:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 745893
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Vu;->B:Ljava/lang/String;

    .line 745894
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->I()I

    move-result v1

    iput v1, v0, LX/4Vu;->C:I

    .line 745895
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->S()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Vu;->D:Ljava/lang/String;

    .line 745896
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->J()I

    move-result v1

    iput v1, v0, LX/4Vu;->E:I

    .line 745897
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->K()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v1

    iput-object v1, v0, LX/4Vu;->F:Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    .line 745898
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->L()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4Vu;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 745899
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->M()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Vu;->H:Ljava/lang/String;

    .line 745900
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->N()Z

    move-result v1

    iput-boolean v1, v0, LX/4Vu;->I:Z

    .line 745901
    invoke-static {v0, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 745902
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLComment;
    .locals 2

    .prologue
    .line 745903
    new-instance v0, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLComment;-><init>(LX/4Vu;)V

    .line 745904
    return-object v0
.end method
