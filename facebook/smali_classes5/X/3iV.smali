.class public LX/3iV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/25H;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static v:LX/0Xm;


# instance fields
.field public final a:LX/0tc;

.field public final b:LX/0tX;

.field public final c:LX/20j;

.field private final d:Ljava/util/concurrent/ExecutorService;

.field public final e:LX/0aG;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1dw;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/03V;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/graphql/executor/iface/ConsistencyCacheFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/3iX;

.field public final j:LX/3iW;

.field private final k:LX/2xs;

.field public final l:LX/0tE;

.field private final m:LX/0ad;

.field public final n:LX/0Uh;

.field private final o:LX/1dy;

.field private final p:LX/0ti;

.field private final q:LX/3iY;

.field private final r:LX/3iZ;

.field private final s:LX/3ia;

.field private final t:LX/2xt;

.field private final u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/189;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tc;LX/0tX;LX/20j;Ljava/util/concurrent/ExecutorService;LX/0aG;LX/3iW;LX/0Ot;LX/03V;LX/0Ot;LX/3iX;LX/2xs;LX/0tE;LX/0ad;LX/0Uh;LX/1dy;LX/0ti;LX/3iY;LX/3iZ;LX/3ia;LX/2xt;LX/0Ot;)V
    .locals 1
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tc;",
            "LX/0tX;",
            "LX/20j;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0aG;",
            "LX/3iW;",
            "LX/0Ot",
            "<",
            "LX/1dw;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/graphql/executor/iface/ConsistencyCacheFactory;",
            ">;",
            "LX/3iX;",
            "LX/2xs;",
            "LX/0tE;",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/1dy;",
            "LX/0ti;",
            "LX/3iY;",
            "LX/3iZ;",
            "LX/3ia;",
            "LX/2xt;",
            "LX/0Ot",
            "<",
            "LX/189;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 629818
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 629819
    iput-object p1, p0, LX/3iV;->a:LX/0tc;

    .line 629820
    iput-object p2, p0, LX/3iV;->b:LX/0tX;

    .line 629821
    iput-object p3, p0, LX/3iV;->c:LX/20j;

    .line 629822
    iput-object p4, p0, LX/3iV;->d:Ljava/util/concurrent/ExecutorService;

    .line 629823
    iput-object p5, p0, LX/3iV;->e:LX/0aG;

    .line 629824
    iput-object p7, p0, LX/3iV;->f:LX/0Ot;

    .line 629825
    iput-object p6, p0, LX/3iV;->j:LX/3iW;

    .line 629826
    iput-object p8, p0, LX/3iV;->g:LX/03V;

    .line 629827
    iput-object p9, p0, LX/3iV;->h:LX/0Ot;

    .line 629828
    iput-object p10, p0, LX/3iV;->i:LX/3iX;

    .line 629829
    iput-object p11, p0, LX/3iV;->k:LX/2xs;

    .line 629830
    iput-object p12, p0, LX/3iV;->l:LX/0tE;

    .line 629831
    iput-object p13, p0, LX/3iV;->m:LX/0ad;

    .line 629832
    iput-object p14, p0, LX/3iV;->n:LX/0Uh;

    .line 629833
    move-object/from16 v0, p15

    iput-object v0, p0, LX/3iV;->o:LX/1dy;

    .line 629834
    move-object/from16 v0, p16

    iput-object v0, p0, LX/3iV;->p:LX/0ti;

    .line 629835
    move-object/from16 v0, p17

    iput-object v0, p0, LX/3iV;->q:LX/3iY;

    .line 629836
    move-object/from16 v0, p18

    iput-object v0, p0, LX/3iV;->r:LX/3iZ;

    .line 629837
    move-object/from16 v0, p19

    iput-object v0, p0, LX/3iV;->s:LX/3ia;

    .line 629838
    move-object/from16 v0, p20

    iput-object v0, p0, LX/3iV;->t:LX/2xt;

    .line 629839
    move-object/from16 v0, p21

    iput-object v0, p0, LX/3iV;->u:LX/0Ot;

    .line 629840
    return-void
.end method

.method private a(LX/2lk;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;Z)LX/3Bq;
    .locals 7
    .param p1    # LX/2lk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 629817
    new-instance v0, LX/6PG;

    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p2, v3, v1

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move v5, p4

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, LX/6PG;-><init>(LX/3iV;LX/2lk;[Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/graphql/model/GraphQLActor;)V

    return-object v0
.end method

.method public static a(LX/0QB;)LX/3iV;
    .locals 3

    .prologue
    .line 629809
    const-class v1, LX/3iV;

    monitor-enter v1

    .line 629810
    :try_start_0
    sget-object v0, LX/3iV;->v:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 629811
    sput-object v2, LX/3iV;->v:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 629812
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629813
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/3iV;->b(LX/0QB;)LX/3iV;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 629814
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3iV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 629815
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 629816
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/37X;LX/2lk;Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .param p2    # LX/2lk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/37X;",
            "LX/2lk;",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 629801
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v4

    .line 629802
    iget-object v6, p0, LX/3iV;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/facebook/controller/mutation/util/FeedbackGraphQLGenerator$10;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/controller/mutation/util/FeedbackGraphQLGenerator$10;-><init>(LX/3iV;LX/37X;Ljava/util/concurrent/Callable;Lcom/google/common/util/concurrent/SettableFuture;LX/2lk;)V

    const v1, -0x5fe6f95e

    invoke-static {v6, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 629803
    invoke-virtual {p1}, LX/37X;->h()LX/3Bq;

    move-result-object v0

    .line 629804
    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 629805
    invoke-interface {p2}, LX/2lk;->d()LX/3Bq;

    move-result-object v0

    .line 629806
    :cond_0
    if-eqz v0, :cond_1

    .line 629807
    iget-object v1, p0, LX/3iV;->o:LX/1dy;

    invoke-virtual {v1, v4, v0}, LX/1dy;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/3Bq;)V

    .line 629808
    :cond_1
    return-object v4
.end method

.method public static a(LX/3iV;LX/37X;Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/37X;",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 629800
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, LX/3iV;->a(LX/37X;LX/2lk;Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/3iV;Lcom/facebook/api/ufiservices/common/ToggleLikeParams;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/ufiservices/common/ToggleLikeParams;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 629790
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->c:Lcom/facebook/graphql/model/GraphQLActor;

    if-nez v0, :cond_0

    .line 629791
    iget-object v0, p0, LX/3iV;->g:LX/03V;

    const-string v1, "FeedbackGraphQLGenerator.togglePageLike"

    const-string v2, "null likerProfile"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 629792
    :cond_0
    iget-object v0, p0, LX/3iV;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0sg;

    invoke-virtual {v0}, LX/0sg;->a()LX/2lk;

    move-result-object v0

    .line 629793
    invoke-static {p1}, LX/25G;->a(Lcom/facebook/api/ufiservices/common/ToggleLikeParams;)LX/0jT;

    move-result-object v1

    .line 629794
    invoke-interface {v0, v1}, LX/2lk;->a(LX/0jT;)Z

    .line 629795
    move-object v1, v0

    .line 629796
    iget-object v0, p0, LX/3iV;->n:LX/0Uh;

    const/16 v2, 0x404

    invoke-virtual {v0, v2, v7}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 629797
    iget-object v0, p0, LX/3iV;->a:LX/0tc;

    const/4 v2, 0x1

    new-array v2, v2, [LX/4VT;

    new-instance v3, LX/6Ph;

    iget-object v4, p1, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->a:Ljava/lang/String;

    iget-object v5, p1, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->c:Lcom/facebook/graphql/model/GraphQLActor;

    iget-boolean v6, p1, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->b:Z

    invoke-direct {v3, v4, v5, v6}, LX/6Ph;-><init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;Z)V

    aput-object v3, v2, v7

    invoke-virtual {v0, v2}, LX/0tc;->a([LX/4VT;)LX/37X;

    move-result-object v0

    .line 629798
    :goto_0
    new-instance v2, LX/6PK;

    invoke-direct {v2, p0, p1}, LX/6PK;-><init>(LX/3iV;Lcom/facebook/api/ufiservices/common/ToggleLikeParams;)V

    invoke-direct {p0, v0, v1, v2}, LX/3iV;->a(LX/37X;LX/2lk;Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 629799
    :cond_1
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->c:Lcom/facebook/graphql/model/GraphQLActor;

    iget-boolean v3, p1, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->b:Z

    invoke-direct {p0, v1, v0, v2, v3}, LX/3iV;->b(LX/2lk;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;Z)LX/37X;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/3iV;LX/3Bq;)V
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 629781
    iget-object v0, p0, LX/3iV;->a:LX/0tc;

    invoke-virtual {v0, p1}, LX/0tc;->a(LX/3Bq;)LX/37X;

    move-result-object v1

    .line 629782
    :try_start_0
    sget-object v0, LX/1NE;->NETWORK:LX/1NE;

    invoke-virtual {v1, v0}, LX/1NB;->a(LX/1NE;)LX/1NB;

    .line 629783
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, LX/37X;->a(Z)V

    .line 629784
    iget-object v0, p0, LX/3iV;->b:LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/37X;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 629785
    invoke-virtual {v1}, LX/1NB;->e()V

    .line 629786
    :goto_0
    return-void

    .line 629787
    :catch_0
    move-exception v0

    .line 629788
    :try_start_1
    const-class v2, LX/3iV;

    const-string v3, "Failed to update caches"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 629789
    invoke-virtual {v1}, LX/1NB;->e()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/1NB;->e()V

    throw v0
.end method

.method private static varargs a(LX/3iV;[LX/4VT;)V
    .locals 3
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .prologue
    .line 629776
    :try_start_0
    iget-object v0, p0, LX/3iV;->p:LX/0ti;

    .line 629777
    new-instance v1, LX/4VL;

    iget-object v2, v0, LX/0ti;->h:LX/0Uh;

    invoke-direct {v1, v2, p1}, LX/4VL;-><init>(LX/0Uh;[LX/4VT;)V

    invoke-static {v0, v1}, LX/0ti;->c(LX/0ti;LX/3Bq;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 629778
    :goto_0
    return-void

    .line 629779
    :catch_0
    move-exception v0

    .line 629780
    const-class v1, LX/3iV;

    const-string v2, "Failed to update caches"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/3iV;Lcom/facebook/api/ufiservices/common/ToggleLikeParams;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    .line 629768
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 629769
    const-string v0, "toggleLikeParams"

    invoke-virtual {v1, v0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 629770
    :try_start_0
    iget-object v0, p0, LX/3iV;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dw;

    iget-object v2, p0, LX/3iV;->e:LX/0aG;

    const v3, -0x2a8a41d7

    invoke-static {v2, p2, v1, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    const/16 v4, 0x3e8

    sget-object v5, LX/4Vg;->THROW_CUSTOM_EXCEPTION:LX/4Vg;

    invoke-virtual/range {v0 .. v5}, LX/1dw;->a(LX/1MF;JILX/4Vg;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    const-wide/16 v2, 0x3c

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const v4, 0x4ea8062e    # 1.40948864E9f

    invoke-static {v0, v2, v3, v1, v4}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 629771
    :goto_0
    return-object v0

    .line 629772
    :catch_0
    move-exception v0

    .line 629773
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, LX/4gu;

    if-eqz v1, :cond_0

    .line 629774
    iget-boolean v0, p1, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 629775
    :cond_0
    throw v0
.end method

.method private b(LX/2lk;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;Z)LX/37X;
    .locals 2
    .param p1    # LX/2lk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 629766
    invoke-direct {p0, p1, p2, p3, p4}, LX/3iV;->a(LX/2lk;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;Z)LX/3Bq;

    move-result-object v0

    .line 629767
    iget-object v1, p0, LX/3iV;->a:LX/0tc;

    invoke-virtual {v1, v0}, LX/0tc;->a(LX/3Bq;)LX/37X;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/0QB;)LX/3iV;
    .locals 24

    .prologue
    .line 629764
    new-instance v2, LX/3iV;

    invoke-static/range {p0 .. p0}, LX/0tc;->a(LX/0QB;)LX/0tc;

    move-result-object v3

    check-cast v3, LX/0tc;

    invoke-static/range {p0 .. p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static/range {p0 .. p0}, LX/20j;->a(LX/0QB;)LX/20j;

    move-result-object v5

    check-cast v5, LX/20j;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/0aF;->getInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v7

    check-cast v7, LX/0aG;

    invoke-static/range {p0 .. p0}, LX/3iW;->a(LX/0QB;)LX/3iW;

    move-result-object v8

    check-cast v8, LX/3iW;

    const/16 v9, 0xb16

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    const/16 v11, 0xb08

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static/range {p0 .. p0}, LX/3iX;->a(LX/0QB;)LX/3iX;

    move-result-object v12

    check-cast v12, LX/3iX;

    invoke-static/range {p0 .. p0}, LX/2xs;->a(LX/0QB;)LX/2xs;

    move-result-object v13

    check-cast v13, LX/2xs;

    invoke-static/range {p0 .. p0}, LX/0tE;->a(LX/0QB;)LX/0tE;

    move-result-object v14

    check-cast v14, LX/0tE;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v15

    check-cast v15, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v16

    check-cast v16, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/1dy;->a(LX/0QB;)LX/1dy;

    move-result-object v17

    check-cast v17, LX/1dy;

    invoke-static/range {p0 .. p0}, LX/0ti;->a(LX/0QB;)LX/0ti;

    move-result-object v18

    check-cast v18, LX/0ti;

    const-class v19, LX/3iY;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v19

    check-cast v19, LX/3iY;

    const-class v20, LX/3iZ;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v20

    check-cast v20, LX/3iZ;

    const-class v21, LX/3ia;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v21

    check-cast v21, LX/3ia;

    const-class v22, LX/2xt;

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v22

    check-cast v22, LX/2xt;

    const/16 v23, 0x473

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v23

    invoke-direct/range {v2 .. v23}, LX/3iV;-><init>(LX/0tc;LX/0tX;LX/20j;Ljava/util/concurrent/ExecutorService;LX/0aG;LX/3iW;LX/0Ot;LX/03V;LX/0Ot;LX/3iX;LX/2xs;LX/0tE;LX/0ad;LX/0Uh;LX/1dy;LX/0ti;LX/3iY;LX/3iZ;LX/3ia;LX/2xt;LX/0Ot;)V

    .line 629765
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/api/ufiservices/common/AddCommentParams;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/ufiservices/common/AddCommentParams;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 629748
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 629749
    iget-object v1, p0, LX/3iV;->k:LX/2xs;

    invoke-virtual {v1, p1}, LX/2xs;->a(Lcom/facebook/api/ufiservices/common/AddCommentParams;)LX/399;

    move-result-object v1

    .line 629750
    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/AddCommentParams;->l:Lcom/facebook/auth/viewercontext/ViewerContext;

    new-instance v3, LX/6PN;

    invoke-direct {v3, p0, v0}, LX/6PN;-><init>(LX/3iV;Lcom/google/common/util/concurrent/SettableFuture;)V

    .line 629751
    if-eqz v2, :cond_0

    .line 629752
    iput-object v2, v1, LX/399;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 629753
    :cond_0
    if-eqz p2, :cond_1

    .line 629754
    iget-object v5, p0, LX/3iV;->b:LX/0tX;

    new-instance v4, LX/3G1;

    invoke-direct {v4}, LX/3G1;-><init>()V

    invoke-virtual {v4, v1}, LX/3G1;->a(LX/399;)LX/3G1;

    move-result-object v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0x1

    invoke-virtual {v6, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    .line 629755
    iput-wide v6, v4, LX/3G2;->d:J

    .line 629756
    move-object v4, v4

    .line 629757
    const/16 v6, 0x64

    .line 629758
    iput v6, v4, LX/3G2;->f:I

    .line 629759
    move-object v4, v4

    .line 629760
    invoke-virtual {v4}, LX/3G2;->a()LX/3G3;

    move-result-object v4

    check-cast v4, LX/3G4;

    sget-object v6, LX/3Fz;->c:LX/3Fz;

    invoke-virtual {v5, v4, v6}, LX/0tX;->a(LX/3G4;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 629761
    :goto_0
    invoke-static {v4, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 629762
    return-object v0

    .line 629763
    :cond_1
    iget-object v4, p0, LX/3iV;->b:LX/0tX;

    invoke-virtual {v4, v1}, LX/0tX;->b(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    goto :goto_0
.end method

.method public final a(Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 629841
    iget-object v0, p0, LX/3iV;->a:LX/0tc;

    const/4 v1, 0x1

    new-array v1, v1, [LX/4VT;

    const/4 v2, 0x0

    new-instance v3, LX/6Pb;

    .line 629842
    iget-object v4, p1, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->b:Ljava/lang/String;

    move-object v4, v4

    .line 629843
    iget-boolean v5, p1, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->e:Z

    move v5, v5

    .line 629844
    invoke-direct {v3, v4, v5}, LX/6Pb;-><init>(Ljava/lang/String;Z)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LX/0tc;->a([LX/4VT;)LX/37X;

    move-result-object v0

    new-instance v1, LX/6PO;

    invoke-direct {v1, p0, p1}, LX/6PO;-><init>(LX/3iV;Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;)V

    invoke-static {p0, v0, v1}, LX/3iV;->a(LX/3iV;LX/37X;Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 629651
    iget-object v1, p0, LX/3iV;->n:LX/0Uh;

    const/16 v2, 0x404

    invoke-virtual {v1, v2, v5}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 629652
    iget-object v0, p0, LX/3iV;->a:LX/0tc;

    const/4 v1, 0x1

    new-array v1, v1, [LX/4VT;

    new-instance v2, LX/6Pi;

    iget-object v3, p1, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->a:Ljava/lang/String;

    iget-boolean v4, p1, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->b:Z

    invoke-direct {v2, v3, v4}, LX/6Pi;-><init>(Ljava/lang/String;Z)V

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, LX/0tc;->a([LX/4VT;)LX/37X;

    move-result-object v0

    .line 629653
    :goto_0
    new-instance v1, LX/6PJ;

    invoke-direct {v1, p0, p1}, LX/6PJ;-><init>(LX/3iV;Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;)V

    invoke-static {p0, v0, v1}, LX/3iV;->a(LX/3iV;LX/37X;Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 629654
    :cond_0
    iget-object v1, p1, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->c:Lcom/facebook/graphql/model/GraphQLActor;

    iget-boolean v3, p1, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->b:Z

    invoke-direct {p0, v0, v1, v2, v3}, LX/3iV;->a(LX/2lk;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;Z)LX/3Bq;

    move-result-object v1

    .line 629655
    if-eqz p2, :cond_2

    .line 629656
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 629657
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 629658
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 629659
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    .line 629660
    :goto_1
    iget-object v2, p0, LX/3iV;->j:LX/3iW;

    iget-boolean v3, p1, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->b:Z

    .line 629661
    new-instance v4, LX/6PA;

    invoke-direct {v4, v2, v0, v3}, LX/6PA;-><init>(LX/3iW;Ljava/lang/String;Z)V

    move-object v0, v4

    .line 629662
    move-object v2, v0

    .line 629663
    :goto_2
    if-eqz v2, :cond_3

    .line 629664
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v0

    invoke-interface {v2}, LX/3Bq;->a()Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    move-result-object v0

    invoke-interface {v1}, LX/3Bq;->a()Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    move-result-object v0

    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v3

    .line 629665
    new-instance v0, LX/6PI;

    invoke-direct {v0, p0, v1, v2, v3}, LX/6PI;-><init>(LX/3iV;LX/3Bq;LX/3Bq;LX/0Rf;)V

    .line 629666
    :goto_3
    iget-object v1, p0, LX/3iV;->a:LX/0tc;

    invoke-virtual {v1, v0}, LX/0tc;->a(LX/3Bq;)LX/37X;

    move-result-object v0

    goto :goto_0

    .line 629667
    :cond_1
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 629668
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 629669
    :cond_2
    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 629670
    iget-object v0, p0, LX/3iV;->j:LX/3iW;

    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->a:Ljava/lang/String;

    iget-boolean v3, p1, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->b:Z

    .line 629671
    new-instance v4, LX/6PC;

    invoke-direct {v4, v0, v2, v3}, LX/6PC;-><init>(LX/3iW;Ljava/lang/String;Z)V

    move-object v0, v4

    .line 629672
    move-object v2, v0

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_3

    :cond_4
    move-object v2, v0

    goto :goto_2
.end method

.method public final a(Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 629675
    invoke-static {p1}, LX/25G;->b(Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;)V

    .line 629676
    invoke-virtual {p1}, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->b()Lcom/facebook/api/ufiservices/common/ToggleLikeParams;

    move-result-object v0

    invoke-static {p0, v0}, LX/3iV;->a(LX/3iV;Lcom/facebook/api/ufiservices/common/ToggleLikeParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    .locals 3
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 629677
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629678
    :goto_0
    return-void

    .line 629679
    :cond_0
    iget-object v0, p0, LX/3iV;->n:LX/0Uh;

    const/16 v1, 0x40a

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 629680
    const/4 v0, 0x2

    new-array v0, v0, [LX/4VT;

    new-instance v1, LX/6PY;

    invoke-direct {v1, p1, p2}, LX/6PY;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    aput-object v1, v0, v2

    const/4 v1, 0x1

    new-instance v2, LX/6PX;

    invoke-direct {v2, p1, p2}, LX/6PX;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    aput-object v2, v0, v1

    invoke-static {p0, v0}, LX/3iV;->a(LX/3iV;[LX/4VT;)V

    goto :goto_0

    .line 629681
    :cond_1
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 629682
    iget-object v0, p0, LX/3iV;->j:LX/3iW;

    invoke-virtual {v0, p1, p2}, LX/3iW;->b(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)LX/3Bq;

    move-result-object v0

    invoke-static {p0, v0}, LX/3iV;->a(LX/3iV;LX/3Bq;)V

    .line 629683
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V
    .locals 3
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .prologue
    .line 629673
    const/4 v0, 0x1

    new-array v0, v0, [LX/4VT;

    const/4 v1, 0x0

    new-instance v2, LX/6Pe;

    invoke-direct {v2, p1, p2, p3}, LX/6Pe;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    aput-object v2, v0, v1

    invoke-static {p0, v0}, LX/3iV;->a(LX/3iV;[LX/4VT;)V

    .line 629674
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 7
    .param p2    # Lcom/facebook/graphql/model/FeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 629684
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 629685
    iget-object v2, p0, LX/3iV;->n:LX/0Uh;

    const/16 v3, 0x403

    invoke-virtual {v2, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 629686
    iget-object v2, p0, LX/3iV;->p:LX/0ti;

    new-instance v3, LX/4VL;

    iget-object v4, p0, LX/3iV;->n:LX/0Uh;

    new-array v5, v0, [LX/4VT;

    new-instance v6, LX/6Pa;

    invoke-direct {v6, p1, p2}, LX/6Pa;-><init>(Ljava/lang/String;Lcom/facebook/graphql/model/FeedUnit;)V

    aput-object v6, v5, v1

    invoke-direct {v3, v4, v5}, LX/4VL;-><init>(LX/0Uh;[LX/4VT;)V

    invoke-virtual {v2, v3}, LX/0ti;->a(LX/3Bq;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 629687
    :goto_0
    iget-object v2, p0, LX/3iV;->i:LX/3iX;

    if-eqz p2, :cond_1

    :goto_1
    invoke-virtual {v2, p1, v0}, LX/3iX;->a(Ljava/lang/String;Z)V

    .line 629688
    return-void

    .line 629689
    :cond_0
    iget-object v2, p0, LX/3iV;->j:LX/3iW;

    .line 629690
    new-instance v3, LX/6P9;

    invoke-direct {v3, v2, p1, p2}, LX/6P9;-><init>(LX/3iW;Ljava/lang/String;Lcom/facebook/graphql/model/FeedUnit;)V

    move-object v2, v3

    .line 629691
    invoke-static {p0, v2}, LX/3iV;->a(LX/3iV;LX/3Bq;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 629692
    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;)V
    .locals 3
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .prologue
    .line 629693
    const/4 v0, 0x1

    new-array v0, v0, [LX/4VT;

    const/4 v1, 0x0

    new-instance v2, LX/6Pc;

    invoke-direct {v2, p1, p2}, LX/6Pc;-><init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;)V

    aput-object v2, v0, v1

    invoke-static {p0, v0}, LX/3iV;->a(LX/3iV;[LX/4VT;)V

    .line 629694
    return-void
.end method

.method public final declared-synchronized a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 5
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .prologue
    .line 629695
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3iV;->n:LX/0Uh;

    const/16 v1, 0x40b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629696
    const/4 v0, 0x1

    new-array v0, v0, [LX/4VT;

    const/4 v1, 0x0

    iget-object v2, p0, LX/3iV;->t:LX/2xt;

    .line 629697
    new-instance v3, LX/6Pk;

    const/16 v4, 0x473

    invoke-static {v2, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-direct {v3, v4, p1, p2}, LX/6Pk;-><init>(LX/0Ot;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 629698
    move-object v2, v3

    .line 629699
    aput-object v2, v0, v1

    invoke-static {p0, v0}, LX/3iV;->a(LX/3iV;[LX/4VT;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 629700
    :goto_0
    monitor-exit p0

    return-void

    .line 629701
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/3iV;->j:LX/3iW;

    .line 629702
    new-instance v1, LX/6P2;

    invoke-direct {v1, v0, p1, p2}, LX/6P2;-><init>(LX/3iW;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    move-object v0, v1

    .line 629703
    invoke-static {p0, v0}, LX/3iV;->a(LX/3iV;LX/3Bq;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 629704
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Lcom/facebook/ipc/composer/model/ProductItemAttachment;)V
    .locals 5
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .prologue
    .line 629705
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3iV;->n:LX/0Uh;

    const/16 v1, 0x40b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629706
    const/4 v0, 0x1

    new-array v0, v0, [LX/4VT;

    const/4 v1, 0x0

    iget-object v2, p0, LX/3iV;->r:LX/3iZ;

    .line 629707
    new-instance v4, LX/6Pl;

    invoke-static {v2}, LX/6Ot;->b(LX/0QB;)LX/6Ot;

    move-result-object v3

    check-cast v3, LX/6Ot;

    invoke-direct {v4, v3, p1, p2}, LX/6Pl;-><init>(LX/6Ot;Ljava/lang/String;Lcom/facebook/ipc/composer/model/ProductItemAttachment;)V

    .line 629708
    move-object v2, v4

    .line 629709
    aput-object v2, v0, v1

    invoke-static {p0, v0}, LX/3iV;->a(LX/3iV;[LX/4VT;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 629710
    :goto_0
    monitor-exit p0

    return-void

    .line 629711
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/3iV;->j:LX/3iW;

    .line 629712
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 629713
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 629714
    new-instance v1, LX/6P3;

    invoke-direct {v1, v0, p1, p2}, LX/6P3;-><init>(LX/3iW;Ljava/lang/String;Lcom/facebook/ipc/composer/model/ProductItemAttachment;)V

    move-object v0, v1

    .line 629715
    invoke-static {p0, v0}, LX/3iV;->a(LX/3iV;LX/3Bq;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 629716
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/5vL;)V
    .locals 8
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 629717
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 629718
    iget-object v0, p0, LX/3iV;->n:LX/0Uh;

    const/16 v1, 0x40a

    invoke-virtual {v0, v1, v7}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629719
    const/4 v0, 0x1

    new-array v6, v0, [LX/4VT;

    new-instance v0, LX/6Pj;

    iget-object v1, p0, LX/3iV;->u:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/189;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/6Pj;-><init>(LX/189;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/5vL;)V

    aput-object v0, v6, v7

    invoke-static {p0, v6}, LX/3iV;->a(LX/3iV;[LX/4VT;)V

    .line 629720
    :goto_0
    return-void

    .line 629721
    :cond_0
    iget-object v0, p0, LX/3iV;->j:LX/3iW;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/3iW;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/5vL;)LX/6P1;

    move-result-object v0

    invoke-static {p0, v0}, LX/3iV;->a(LX/3iV;LX/3Bq;)V

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Z)V
    .locals 9

    .prologue
    .line 629722
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3iV;->n:LX/0Uh;

    const/16 v1, 0x407

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629723
    iget-object v0, p0, LX/3iV;->p:LX/0ti;

    iget-object v1, p0, LX/3iV;->q:LX/3iY;

    .line 629724
    new-instance v3, LX/6PV;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v1}, LX/6Ou;->b(LX/0QB;)LX/6Ou;

    move-result-object v5

    check-cast v5, LX/6Ou;

    invoke-static {v1}, LX/6Ot;->b(LX/0QB;)LX/6Ot;

    move-result-object v6

    check-cast v6, LX/6Ot;

    move-object v7, p1

    move v8, p2

    invoke-direct/range {v3 .. v8}, LX/6PV;-><init>(LX/03V;LX/6Ou;LX/6Ot;Ljava/lang/String;Z)V

    .line 629725
    move-object v1, v3

    .line 629726
    invoke-virtual {v0, v1}, LX/0ti;->a(LX/4VT;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 629727
    :goto_0
    monitor-exit p0

    return-void

    .line 629728
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/3iV;->j:LX/3iW;

    .line 629729
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 629730
    new-instance v1, LX/6P4;

    invoke-direct {v1, v0, p1, p2}, LX/6P4;-><init>(LX/3iW;Ljava/lang/String;Z)V

    move-object v0, v1

    .line 629731
    invoke-static {p0, v0}, LX/3iV;->a(LX/3iV;LX/3Bq;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 629732
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 629733
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629734
    :goto_0
    return-void

    .line 629735
    :cond_0
    iget-object v0, p0, LX/3iV;->n:LX/0Uh;

    const/16 v1, 0x40a

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 629736
    const/4 v0, 0x1

    new-array v0, v0, [LX/4VT;

    iget-object v1, p0, LX/3iV;->s:LX/3ia;

    .line 629737
    new-instance v3, LX/6PZ;

    const/16 v4, 0x473

    invoke-static {v1, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-direct {v3, v4, p1, p2}, LX/6PZ;-><init>(LX/0Ot;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 629738
    move-object v1, v3

    .line 629739
    aput-object v1, v0, v2

    invoke-static {p0, v0}, LX/3iV;->a(LX/3iV;[LX/4VT;)V

    goto :goto_0

    .line 629740
    :cond_1
    iget-object v0, p0, LX/3iV;->j:LX/3iW;

    .line 629741
    new-instance v1, LX/6P5;

    invoke-direct {v1, v0, p1, p2}, LX/6P5;-><init>(LX/3iW;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    move-object v0, v1

    .line 629742
    invoke-static {p0, v0}, LX/3iV;->a(LX/3iV;LX/3Bq;)V

    goto :goto_0
.end method

.method public final declared-synchronized c(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 629743
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3iV;->n:LX/0Uh;

    const/16 v1, 0x40a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629744
    iget-object v0, p0, LX/3iV;->p:LX/0ti;

    new-instance v1, LX/6PX;

    invoke-direct {v1, p1, p2}, LX/6PX;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    invoke-virtual {v0, v1}, LX/0ti;->a(LX/4VT;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 629745
    :goto_0
    monitor-exit p0

    return-void

    .line 629746
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/3iV;->j:LX/3iW;

    invoke-virtual {v0, p1, p2}, LX/3iW;->b(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)LX/3Bq;

    move-result-object v0

    invoke-static {p0, v0}, LX/3iV;->a(LX/3iV;LX/3Bq;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 629747
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
