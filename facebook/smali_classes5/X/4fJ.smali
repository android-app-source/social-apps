.class public final LX/4fJ;
.super LX/1eP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1eP",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4fK;


# direct methods
.method public constructor <init>(LX/4fK;LX/1cd;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 798263
    iput-object p1, p0, LX/4fJ;->a:LX/4fK;

    .line 798264
    invoke-direct {p0, p2}, LX/1eP;-><init>(LX/1cd;)V

    .line 798265
    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    .line 798266
    iget-object v1, p0, LX/4fJ;->a:LX/4fK;

    monitor-enter v1

    .line 798267
    :try_start_0
    iget-object v0, p0, LX/4fJ;->a:LX/4fK;

    iget-object v0, v0, LX/4fK;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 798268
    if-nez v0, :cond_0

    .line 798269
    iget-object v2, p0, LX/4fJ;->a:LX/4fK;

    .line 798270
    iget v3, v2, LX/4fK;->c:I

    add-int/lit8 v4, v3, -0x1

    iput v4, v2, LX/4fK;->c:I

    .line 798271
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 798272
    if-eqz v0, :cond_1

    .line 798273
    iget-object v1, p0, LX/4fJ;->a:LX/4fK;

    iget-object v1, v1, LX/4fK;->e:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/facebook/imagepipeline/producers/ThrottlingProducer$ThrottlerConsumer$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/imagepipeline/producers/ThrottlingProducer$ThrottlerConsumer$1;-><init>(LX/4fJ;Landroid/util/Pair;)V

    const v0, -0x6ee1e28a

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 798274
    :cond_1
    return-void

    .line 798275
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 798276
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 798277
    invoke-virtual {v0}, LX/1cd;->b()V

    .line 798278
    invoke-direct {p0}, LX/4fJ;->c()V

    .line 798279
    return-void
.end method

.method public final a(Ljava/lang/Object;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)V"
        }
    .end annotation

    .prologue
    .line 798280
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 798281
    invoke-virtual {v0, p1, p2}, LX/1cd;->b(Ljava/lang/Object;Z)V

    .line 798282
    if-eqz p2, :cond_0

    .line 798283
    invoke-direct {p0}, LX/4fJ;->c()V

    .line 798284
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 798285
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 798286
    invoke-virtual {v0, p1}, LX/1cd;->b(Ljava/lang/Throwable;)V

    .line 798287
    invoke-direct {p0}, LX/4fJ;->c()V

    .line 798288
    return-void
.end method
