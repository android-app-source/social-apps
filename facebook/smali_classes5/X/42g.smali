.class public interface abstract LX/42g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ex;


# virtual methods
.method public abstract a(Ljava/lang/Class;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+TT;>;)TT;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public abstract a()V
.end method

.method public abstract a(I)V
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation
.end method

.method public abstract a(IILandroid/content/Intent;)V
.end method

.method public abstract a(ILandroid/app/Dialog;)V
.end method

.method public abstract a(LX/0je;)V
.end method

.method public abstract a(Landroid/app/Activity;)V
.end method

.method public abstract a(Landroid/content/Intent;)V
.end method

.method public abstract a(Landroid/content/Intent;I)V
.end method

.method public abstract a(Landroid/content/res/Configuration;)V
.end method

.method public abstract a(Landroid/os/Bundle;)V
.end method

.method public abstract a(Landroid/support/v4/app/Fragment;)V
.end method

.method public abstract a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
.end method

.method public abstract a(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
.end method

.method public abstract a(Ljava/lang/Object;Ljava/lang/Object;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract a(ILandroid/view/KeyEvent;)Z
.end method

.method public abstract a(Landroid/view/KeyEvent;)Z
.end method

.method public abstract a(Landroid/view/Menu;)Z
.end method

.method public abstract a(Landroid/view/MenuItem;)Z
.end method

.method public abstract a(Landroid/view/MotionEvent;)Z
.end method

.method public abstract a(Ljava/lang/Throwable;)Z
.end method

.method public abstract b(I)Landroid/app/Dialog;
.end method

.method public abstract b()V
.end method

.method public abstract b(Landroid/content/Intent;)V
.end method

.method public abstract b(Landroid/os/Bundle;)V
.end method

.method public abstract b(ILandroid/view/KeyEvent;)Z
.end method

.method public abstract b(Landroid/view/Menu;)Z
.end method

.method public abstract b(Landroid/view/MenuItem;)Z
.end method

.method public abstract c(I)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)TT;"
        }
    .end annotation
.end method

.method public abstract c()V
.end method

.method public abstract c(Landroid/content/Intent;)V
.end method

.method public abstract c(Landroid/os/Bundle;)V
.end method

.method public abstract d(I)Landroid/view/View;
.end method

.method public abstract d()V
.end method

.method public abstract d(Landroid/os/Bundle;)V
.end method

.method public abstract e()V
.end method

.method public abstract e(I)V
.end method

.method public abstract f()V
.end method

.method public abstract f(I)V
.end method

.method public abstract g()V
.end method

.method public abstract h()V
.end method

.method public abstract i()V
.end method

.method public abstract j()Z
.end method

.method public abstract k()V
.end method

.method public abstract l()V
.end method

.method public abstract m()V
.end method

.method public abstract n()LX/0QA;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract o()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/0T2;",
            ">;"
        }
    .end annotation
.end method

.method public abstract p()V
.end method

.method public abstract q()V
.end method

.method public abstract r()LX/0gc;
.end method

.method public abstract s()Landroid/view/Window;
.end method

.method public abstract t()Landroid/content/Intent;
.end method

.method public abstract u()Landroid/content/res/Resources;
.end method

.method public abstract v()Landroid/view/MenuInflater;
.end method

.method public abstract w()Z
.end method
