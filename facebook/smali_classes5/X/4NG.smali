.class public LX/4NG;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 691748
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 691749
    const/4 v15, 0x0

    .line 691750
    const/4 v14, 0x0

    .line 691751
    const/4 v13, 0x0

    .line 691752
    const/4 v12, 0x0

    .line 691753
    const/4 v11, 0x0

    .line 691754
    const/4 v10, 0x0

    .line 691755
    const/4 v9, 0x0

    .line 691756
    const/4 v8, 0x0

    .line 691757
    const/4 v7, 0x0

    .line 691758
    const/4 v6, 0x0

    .line 691759
    const/4 v5, 0x0

    .line 691760
    const/4 v4, 0x0

    .line 691761
    const/4 v3, 0x0

    .line 691762
    const/4 v2, 0x0

    .line 691763
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_1

    .line 691764
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 691765
    const/4 v2, 0x0

    .line 691766
    :goto_0
    return v2

    .line 691767
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 691768
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_d

    .line 691769
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v16

    .line 691770
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 691771
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    if-eqz v16, :cond_1

    .line 691772
    const-string v17, "game_description"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 691773
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto :goto_1

    .line 691774
    :cond_2
    const-string v17, "game_name"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 691775
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto :goto_1

    .line 691776
    :cond_3
    const-string v17, "game_orientation"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 691777
    const/4 v3, 0x1

    .line 691778
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    move-result-object v13

    goto :goto_1

    .line 691779
    :cond_4
    const-string v17, "game_uri"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 691780
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto :goto_1

    .line 691781
    :cond_5
    const-string v17, "icon_uri"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 691782
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 691783
    :cond_6
    const-string v17, "instant_game_id"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 691784
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 691785
    :cond_7
    const-string v17, "mobile_game_uri"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 691786
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto/16 :goto_1

    .line 691787
    :cond_8
    const-string v17, "splash_uri"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 691788
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 691789
    :cond_9
    const-string v17, "video_uri"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 691790
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 691791
    :cond_a
    const-string v17, "banner_image_uri"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 691792
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 691793
    :cond_b
    const-string v17, "game_detailed_description"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_c

    .line 691794
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 691795
    :cond_c
    const-string v17, "game_score_strategy"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 691796
    const/4 v2, 0x1

    .line 691797
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

    move-result-object v4

    goto/16 :goto_1

    .line 691798
    :cond_d
    const/16 v16, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 691799
    const/16 v16, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 691800
    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 691801
    if-eqz v3, :cond_e

    .line 691802
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->a(ILjava/lang/Enum;)V

    .line 691803
    :cond_e
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 691804
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 691805
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 691806
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 691807
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 691808
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 691809
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 691810
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 691811
    if-eqz v2, :cond_f

    .line 691812
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 691813
    :cond_f
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 5

    .prologue
    const/16 v4, 0xc

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 691814
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 691815
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691816
    if-eqz v0, :cond_0

    .line 691817
    const-string v1, "game_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691818
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691819
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691820
    if-eqz v0, :cond_1

    .line 691821
    const-string v1, "game_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691822
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691823
    :cond_1
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 691824
    if-eqz v0, :cond_2

    .line 691825
    const-string v0, "game_orientation"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691826
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691827
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691828
    if-eqz v0, :cond_3

    .line 691829
    const-string v1, "game_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691830
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691831
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691832
    if-eqz v0, :cond_4

    .line 691833
    const-string v1, "icon_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691834
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691835
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691836
    if-eqz v0, :cond_5

    .line 691837
    const-string v1, "instant_game_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691838
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691839
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691840
    if-eqz v0, :cond_6

    .line 691841
    const-string v1, "mobile_game_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691842
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691843
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691844
    if-eqz v0, :cond_7

    .line 691845
    const-string v1, "splash_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691846
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691847
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691848
    if-eqz v0, :cond_8

    .line 691849
    const-string v1, "video_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691850
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691851
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691852
    if-eqz v0, :cond_9

    .line 691853
    const-string v1, "banner_image_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691854
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691855
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691856
    if-eqz v0, :cond_a

    .line 691857
    const-string v1, "game_detailed_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691858
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691859
    :cond_a
    invoke-virtual {p0, p1, v4, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 691860
    if-eqz v0, :cond_b

    .line 691861
    const-string v0, "game_score_strategy"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691862
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691863
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 691864
    return-void
.end method
