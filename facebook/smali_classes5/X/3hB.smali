.class public LX/3hB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:LX/2RU;

.field public B:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$NameEntriesModel;",
            ">;"
        }
    .end annotation
.end field

.field public C:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public D:J

.field public E:I

.field public F:I

.field public G:Ljava/lang/String;

.field public H:Z

.field public I:J

.field public J:J

.field public K:Z

.field public L:F

.field public M:Ljava/lang/String;

.field public N:F

.field public O:Z

.field public P:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lcom/facebook/user/model/Name;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Lcom/facebook/user/model/Name;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:I

.field public m:I

.field public n:I

.field public o:F

.field public p:F

.field public q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/graphql/ContactPhone;",
            ">;"
        }
    .end annotation
.end field

.field public r:Z

.field public s:Z

.field public t:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public u:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public v:LX/03R;

.field public w:Z

.field public x:J

.field public y:Z

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 626064
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 626065
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/3hB;->v:LX/03R;

    .line 626066
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 626067
    iput-object v0, p0, LX/3hB;->q:LX/0Px;

    .line 626068
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 626069
    iput-object v0, p0, LX/3hB;->B:LX/0Px;

    .line 626070
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 626071
    iput-object v0, p0, LX/3hB;->C:LX/0Px;

    .line 626072
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    iput-object v0, p0, LX/3hB;->P:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    .line 626073
    return-void
.end method

.method public constructor <init>(Lcom/facebook/contacts/graphql/Contact;)V
    .locals 2

    .prologue
    .line 626074
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 626075
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/3hB;->v:LX/03R;

    .line 626076
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/3hB;->a:Ljava/lang/String;

    .line 626077
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/3hB;->b:Ljava/lang/String;

    .line 626078
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/3hB;->c:Ljava/lang/String;

    .line 626079
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v0

    iput-object v0, p0, LX/3hB;->d:Lcom/facebook/user/model/Name;

    .line 626080
    iget-object v0, p0, LX/3hB;->d:Lcom/facebook/user/model/Name;

    if-eqz v0, :cond_0

    .line 626081
    iget-object v0, p0, LX/3hB;->d:Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/3hB;->e:Ljava/lang/String;

    .line 626082
    iget-object v0, p0, LX/3hB;->d:Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/3hB;->f:Ljava/lang/String;

    .line 626083
    iget-object v0, p0, LX/3hB;->d:Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/3hB;->g:Ljava/lang/String;

    .line 626084
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->f()Lcom/facebook/user/model/Name;

    move-result-object v0

    iput-object v0, p0, LX/3hB;->h:Lcom/facebook/user/model/Name;

    .line 626085
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/3hB;->i:Ljava/lang/String;

    .line 626086
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/3hB;->j:Ljava/lang/String;

    .line 626087
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/3hB;->k:Ljava/lang/String;

    .line 626088
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->j()I

    move-result v0

    iput v0, p0, LX/3hB;->l:I

    .line 626089
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->k()I

    move-result v0

    iput v0, p0, LX/3hB;->m:I

    .line 626090
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->l()I

    move-result v0

    iput v0, p0, LX/3hB;->n:I

    .line 626091
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->m()F

    move-result v0

    iput v0, p0, LX/3hB;->o:F

    .line 626092
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->n()F

    move-result v0

    iput v0, p0, LX/3hB;->p:F

    .line 626093
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->o()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/3hB;->q:LX/0Px;

    .line 626094
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->q()Z

    move-result v0

    iput-boolean v0, p0, LX/3hB;->s:Z

    .line 626095
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->x()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p0, LX/3hB;->t:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 626096
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->y()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    iput-object v0, p0, LX/3hB;->u:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 626097
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->r()LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/3hB;->v:LX/03R;

    .line 626098
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->s()Z

    move-result v0

    iput-boolean v0, p0, LX/3hB;->w:Z

    .line 626099
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->u()Z

    move-result v0

    iput-boolean v0, p0, LX/3hB;->y:Z

    .line 626100
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->v()Z

    move-result v0

    iput-boolean v0, p0, LX/3hB;->z:Z

    .line 626101
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->A()LX/2RU;

    move-result-object v0

    iput-object v0, p0, LX/3hB;->A:LX/2RU;

    .line 626102
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->B()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/3hB;->B:LX/0Px;

    .line 626103
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->z()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/3hB;->C:LX/0Px;

    .line 626104
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->w()J

    move-result-wide v0

    iput-wide v0, p0, LX/3hB;->D:J

    .line 626105
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->E()Z

    move-result v0

    iput-boolean v0, p0, LX/3hB;->H:Z

    .line 626106
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->J()F

    move-result v0

    iput v0, p0, LX/3hB;->L:F

    .line 626107
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->K()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/3hB;->M:Ljava/lang/String;

    .line 626108
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->L()F

    move-result v0

    iput v0, p0, LX/3hB;->N:F

    .line 626109
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->M()Z

    move-result v0

    iput-boolean v0, p0, LX/3hB;->O:Z

    .line 626110
    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->N()Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    move-result-object v0

    iput-object v0, p0, LX/3hB;->P:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    .line 626111
    return-void
.end method


# virtual methods
.method public final O()Lcom/facebook/contacts/graphql/Contact;
    .locals 1

    .prologue
    .line 626112
    new-instance v0, Lcom/facebook/contacts/graphql/Contact;

    invoke-direct {v0, p0}, Lcom/facebook/contacts/graphql/Contact;-><init>(LX/3hB;)V

    return-object v0
.end method

.method public final a(F)LX/3hB;
    .locals 0

    .prologue
    .line 626113
    iput p1, p0, LX/3hB;->o:F

    .line 626114
    return-object p0
.end method

.method public final a(I)LX/3hB;
    .locals 0

    .prologue
    .line 626115
    iput p1, p0, LX/3hB;->l:I

    .line 626116
    return-object p0
.end method

.method public final a(II)LX/3hB;
    .locals 0

    .prologue
    .line 626117
    iput p1, p0, LX/3hB;->E:I

    .line 626118
    iput p2, p0, LX/3hB;->F:I

    .line 626119
    return-object p0
.end method

.method public final a(J)LX/3hB;
    .locals 1

    .prologue
    .line 626120
    iput-wide p1, p0, LX/3hB;->x:J

    .line 626121
    return-object p0
.end method

.method public final a(LX/03R;)LX/3hB;
    .locals 0

    .prologue
    .line 626122
    iput-object p1, p0, LX/3hB;->v:LX/03R;

    .line 626123
    return-object p0
.end method

.method public final a(LX/0Px;)LX/3hB;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/graphql/ContactPhone;",
            ">;)",
            "LX/3hB;"
        }
    .end annotation

    .prologue
    .line 626140
    iput-object p1, p0, LX/3hB;->q:LX/0Px;

    .line 626141
    return-object p0
.end method

.method public final a(LX/2RU;)LX/3hB;
    .locals 0

    .prologue
    .line 626124
    iput-object p1, p0, LX/3hB;->A:LX/2RU;

    .line 626125
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;)LX/3hB;
    .locals 0

    .prologue
    .line 626126
    iput-object p1, p0, LX/3hB;->P:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    .line 626127
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)LX/3hB;
    .locals 0

    .prologue
    .line 626128
    iput-object p1, p0, LX/3hB;->t:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 626129
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)LX/3hB;
    .locals 0

    .prologue
    .line 626130
    iput-object p1, p0, LX/3hB;->u:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 626131
    return-object p0
.end method

.method public final a(Lcom/facebook/user/model/Name;)LX/3hB;
    .locals 0

    .prologue
    .line 626132
    iput-object p1, p0, LX/3hB;->d:Lcom/facebook/user/model/Name;

    .line 626133
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/3hB;
    .locals 0

    .prologue
    .line 626058
    iput-object p1, p0, LX/3hB;->a:Ljava/lang/String;

    .line 626059
    return-object p0
.end method

.method public final a(Z)LX/3hB;
    .locals 0

    .prologue
    .line 626134
    iput-boolean p1, p0, LX/3hB;->r:Z

    .line 626135
    return-object p0
.end method

.method public final b(F)LX/3hB;
    .locals 0

    .prologue
    .line 626136
    iput p1, p0, LX/3hB;->p:F

    .line 626137
    return-object p0
.end method

.method public final b(I)LX/3hB;
    .locals 0

    .prologue
    .line 626138
    iput p1, p0, LX/3hB;->m:I

    .line 626139
    return-object p0
.end method

.method public final b(J)LX/3hB;
    .locals 1

    .prologue
    .line 626060
    iput-wide p1, p0, LX/3hB;->D:J

    .line 626061
    return-object p0
.end method

.method public final b(LX/0Px;)LX/3hB;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$NameEntriesModel;",
            ">;)",
            "LX/3hB;"
        }
    .end annotation

    .prologue
    .line 626062
    iput-object p1, p0, LX/3hB;->B:LX/0Px;

    .line 626063
    return-object p0
.end method

.method public final b(Lcom/facebook/user/model/Name;)LX/3hB;
    .locals 0

    .prologue
    .line 626028
    iput-object p1, p0, LX/3hB;->h:Lcom/facebook/user/model/Name;

    .line 626029
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/3hB;
    .locals 0

    .prologue
    .line 626022
    iput-object p1, p0, LX/3hB;->b:Ljava/lang/String;

    .line 626023
    return-object p0
.end method

.method public final b(Z)LX/3hB;
    .locals 0

    .prologue
    .line 626024
    iput-boolean p1, p0, LX/3hB;->s:Z

    .line 626025
    return-object p0
.end method

.method public final c(I)LX/3hB;
    .locals 0

    .prologue
    .line 626026
    iput p1, p0, LX/3hB;->n:I

    .line 626027
    return-object p0
.end method

.method public final c(J)LX/3hB;
    .locals 1

    .prologue
    .line 626030
    iput-wide p1, p0, LX/3hB;->I:J

    .line 626031
    return-object p0
.end method

.method public final c(LX/0Px;)LX/3hB;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/3hB;"
        }
    .end annotation

    .prologue
    .line 626032
    iput-object p1, p0, LX/3hB;->C:LX/0Px;

    .line 626033
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/3hB;
    .locals 0

    .prologue
    .line 626034
    iput-object p1, p0, LX/3hB;->c:Ljava/lang/String;

    .line 626035
    return-object p0
.end method

.method public final c(Z)LX/3hB;
    .locals 0

    .prologue
    .line 626036
    iput-boolean p1, p0, LX/3hB;->w:Z

    .line 626037
    return-object p0
.end method

.method public final d(F)LX/3hB;
    .locals 0

    .prologue
    .line 626038
    iput p1, p0, LX/3hB;->N:F

    .line 626039
    return-object p0
.end method

.method public final d(J)LX/3hB;
    .locals 1

    .prologue
    .line 626040
    iput-wide p1, p0, LX/3hB;->J:J

    .line 626041
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/3hB;
    .locals 0

    .prologue
    .line 626042
    iput-object p1, p0, LX/3hB;->i:Ljava/lang/String;

    .line 626043
    return-object p0
.end method

.method public final d(Z)LX/3hB;
    .locals 0

    .prologue
    .line 626044
    iput-boolean p1, p0, LX/3hB;->y:Z

    .line 626045
    return-object p0
.end method

.method public final e(Ljava/lang/String;)LX/3hB;
    .locals 0

    .prologue
    .line 626046
    iput-object p1, p0, LX/3hB;->j:Ljava/lang/String;

    .line 626047
    return-object p0
.end method

.method public final e(Z)LX/3hB;
    .locals 0

    .prologue
    .line 626048
    iput-boolean p1, p0, LX/3hB;->z:Z

    .line 626049
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/3hB;
    .locals 0

    .prologue
    .line 626050
    iput-object p1, p0, LX/3hB;->k:Ljava/lang/String;

    .line 626051
    return-object p0
.end method

.method public final f(Z)LX/3hB;
    .locals 0

    .prologue
    .line 626052
    iput-boolean p1, p0, LX/3hB;->H:Z

    .line 626053
    return-object p0
.end method

.method public final g(Ljava/lang/String;)LX/3hB;
    .locals 0

    .prologue
    .line 626054
    iput-object p1, p0, LX/3hB;->G:Ljava/lang/String;

    .line 626055
    return-object p0
.end method

.method public final g(Z)LX/3hB;
    .locals 0

    .prologue
    .line 626056
    iput-boolean p1, p0, LX/3hB;->K:Z

    .line 626057
    return-object p0
.end method
