.class public LX/3ud;
.super LX/3uV;
.source ""

# interfaces
.implements LX/3u7;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/support/v7/internal/widget/ActionBarContextView;

.field private c:LX/3uG;

.field private d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private f:Z

.field private g:LX/3v0;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/internal/widget/ActionBarContextView;LX/3uG;Z)V
    .locals 2

    .prologue
    .line 649043
    invoke-direct {p0}, LX/3uV;-><init>()V

    .line 649044
    iput-object p1, p0, LX/3ud;->a:Landroid/content/Context;

    .line 649045
    iput-object p2, p0, LX/3ud;->b:Landroid/support/v7/internal/widget/ActionBarContextView;

    .line 649046
    iput-object p3, p0, LX/3ud;->c:LX/3uG;

    .line 649047
    new-instance v0, LX/3v0;

    invoke-direct {v0, p1}, LX/3v0;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    .line 649048
    iput v1, v0, LX/3v0;->p:I

    .line 649049
    move-object v0, v0

    .line 649050
    iput-object v0, p0, LX/3ud;->g:LX/3v0;

    .line 649051
    iget-object v0, p0, LX/3ud;->g:LX/3v0;

    invoke-virtual {v0, p0}, LX/3v0;->a(LX/3u7;)V

    .line 649052
    iput-boolean p4, p0, LX/3ud;->f:Z

    .line 649053
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/MenuInflater;
    .locals 2

    .prologue
    .line 649036
    new-instance v0, Landroid/view/MenuInflater;

    iget-object v1, p0, LX/3ud;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 649037
    iget-object v0, p0, LX/3ud;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3uV;->b(Ljava/lang/CharSequence;)V

    .line 649038
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 649039
    iget-object v0, p0, LX/3ud;->b:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->setCustomView(Landroid/view/View;)V

    .line 649040
    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    :goto_0
    iput-object v0, p0, LX/3ud;->d:Ljava/lang/ref/WeakReference;

    .line 649041
    return-void

    .line 649042
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 649058
    iget-object v0, p0, LX/3ud;->b:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 649059
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 649054
    invoke-super {p0, p1}, LX/3uV;->a(Z)V

    .line 649055
    iget-object v0, p0, LX/3ud;->b:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->setTitleOptional(Z)V

    .line 649056
    return-void
.end method

.method public final a(LX/3v0;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 649057
    iget-object v0, p0, LX/3ud;->c:LX/3uG;

    invoke-interface {v0, p0, p2}, LX/3uG;->a(LX/3uV;Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public final b()Landroid/view/Menu;
    .locals 1

    .prologue
    .line 649033
    iget-object v0, p0, LX/3ud;->g:LX/3v0;

    return-object v0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 649034
    iget-object v0, p0, LX/3ud;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3uV;->a(Ljava/lang/CharSequence;)V

    .line 649035
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 649031
    iget-object v0, p0, LX/3ud;->b:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->setTitle(Ljava/lang/CharSequence;)V

    .line 649032
    return-void
.end method

.method public final b_(LX/3v0;)V
    .locals 1

    .prologue
    .line 649028
    invoke-virtual {p0}, LX/3uV;->d()V

    .line 649029
    iget-object v0, p0, LX/3ud;->b:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->a()Z

    .line 649030
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 649023
    iget-boolean v0, p0, LX/3ud;->e:Z

    if-eqz v0, :cond_0

    .line 649024
    :goto_0
    return-void

    .line 649025
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3ud;->e:Z

    .line 649026
    iget-object v0, p0, LX/3ud;->b:Landroid/support/v7/internal/widget/ActionBarContextView;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarContextView;->sendAccessibilityEvent(I)V

    .line 649027
    iget-object v0, p0, LX/3ud;->c:LX/3uG;

    invoke-interface {v0, p0}, LX/3uG;->a(LX/3uV;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 649011
    iget-object v0, p0, LX/3ud;->c:LX/3uG;

    iget-object v1, p0, LX/3ud;->g:LX/3v0;

    invoke-interface {v0, p0, v1}, LX/3uG;->b(LX/3uV;Landroid/view/Menu;)Z

    .line 649012
    return-void
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 649020
    iget-object v0, p0, LX/3ud;->b:Landroid/support/v7/internal/widget/ActionBarContextView;

    .line 649021
    iget-object p0, v0, Landroid/support/v7/internal/widget/ActionBarContextView;->j:Ljava/lang/CharSequence;

    move-object v0, p0

    .line 649022
    return-object v0
.end method

.method public final g()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 649017
    iget-object v0, p0, LX/3ud;->b:Landroid/support/v7/internal/widget/ActionBarContextView;

    .line 649018
    iget-object p0, v0, Landroid/support/v7/internal/widget/ActionBarContextView;->k:Ljava/lang/CharSequence;

    move-object v0, p0

    .line 649019
    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 649014
    iget-object v0, p0, LX/3ud;->b:Landroid/support/v7/internal/widget/ActionBarContextView;

    .line 649015
    iget-boolean p0, v0, Landroid/support/v7/internal/widget/ActionBarContextView;->t:Z

    move v0, p0

    .line 649016
    return v0
.end method

.method public final i()Landroid/view/View;
    .locals 1

    .prologue
    .line 649013
    iget-object v0, p0, LX/3ud;->d:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3ud;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
