.class public final LX/3iz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 630900
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_9

    .line 630901
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 630902
    :goto_0
    return v1

    .line 630903
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 630904
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_8

    .line 630905
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 630906
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 630907
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 630908
    const-string v9, "action_link"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 630909
    const/4 v8, 0x0

    .line 630910
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v9, :cond_10

    .line 630911
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 630912
    :goto_2
    move v7, v8

    .line 630913
    goto :goto_1

    .line 630914
    :cond_2
    const-string v9, "background_image"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 630915
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 630916
    :cond_3
    const-string v9, "description"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 630917
    invoke-static {p0, p1}, LX/DIa;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 630918
    :cond_4
    const-string v9, "icon_source"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 630919
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 630920
    :cond_5
    const-string v9, "style_list"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 630921
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 630922
    :cond_6
    const-string v9, "subtitle"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 630923
    invoke-static {p0, p1}, LX/DIb;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 630924
    :cond_7
    const-string v9, "title"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 630925
    const/4 v8, 0x0

    .line 630926
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v9, :cond_14

    .line 630927
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 630928
    :goto_3
    move v0, v8

    .line 630929
    goto/16 :goto_1

    .line 630930
    :cond_8
    const/4 v8, 0x7

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 630931
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 630932
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 630933
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 630934
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 630935
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 630936
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 630937
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 630938
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1

    .line 630939
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 630940
    :cond_b
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_f

    .line 630941
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 630942
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 630943
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_b

    if-eqz v11, :cond_b

    .line 630944
    const-string v12, "__type__"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_c

    const-string v12, "__typename"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_d

    .line 630945
    :cond_c
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    goto :goto_4

    .line 630946
    :cond_d
    const-string v12, "title"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_e

    .line 630947
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_4

    .line 630948
    :cond_e
    const-string v12, "url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 630949
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_4

    .line 630950
    :cond_f
    const/4 v11, 0x3

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 630951
    invoke-virtual {p1, v8, v10}, LX/186;->b(II)V

    .line 630952
    const/4 v8, 0x1

    invoke-virtual {p1, v8, v9}, LX/186;->b(II)V

    .line 630953
    const/4 v8, 0x2

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 630954
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto/16 :goto_2

    :cond_10
    move v7, v8

    move v9, v8

    move v10, v8

    goto :goto_4

    .line 630955
    :cond_11
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 630956
    :cond_12
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_13

    .line 630957
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 630958
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 630959
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_12

    if-eqz v9, :cond_12

    .line 630960
    const-string v10, "text"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_11

    .line 630961
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_5

    .line 630962
    :cond_13
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 630963
    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 630964
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto/16 :goto_3

    :cond_14
    move v0, v8

    goto :goto_5
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 630850
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 630851
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 630852
    if-eqz v0, :cond_3

    .line 630853
    const-string v1, "action_link"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 630854
    const/4 p3, 0x0

    .line 630855
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 630856
    invoke-virtual {p0, v0, p3}, LX/15i;->g(II)I

    move-result v1

    .line 630857
    if-eqz v1, :cond_0

    .line 630858
    const-string v1, "__type__"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 630859
    invoke-static {p0, v0, p3, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 630860
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 630861
    if-eqz v1, :cond_1

    .line 630862
    const-string p3, "title"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 630863
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 630864
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 630865
    if-eqz v1, :cond_2

    .line 630866
    const-string p3, "url"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 630867
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 630868
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 630869
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 630870
    if-eqz v0, :cond_4

    .line 630871
    const-string v1, "background_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 630872
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 630873
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 630874
    if-eqz v0, :cond_5

    .line 630875
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 630876
    invoke-static {p0, v0, p2}, LX/DIa;->a(LX/15i;ILX/0nX;)V

    .line 630877
    :cond_5
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 630878
    if-eqz v0, :cond_6

    .line 630879
    const-string v1, "icon_source"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 630880
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 630881
    :cond_6
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 630882
    if-eqz v0, :cond_7

    .line 630883
    const-string v0, "style_list"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 630884
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 630885
    :cond_7
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 630886
    if-eqz v0, :cond_8

    .line 630887
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 630888
    invoke-static {p0, v0, p2}, LX/DIb;->a(LX/15i;ILX/0nX;)V

    .line 630889
    :cond_8
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 630890
    if-eqz v0, :cond_a

    .line 630891
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 630892
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 630893
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 630894
    if-eqz v1, :cond_9

    .line 630895
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 630896
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 630897
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 630898
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 630899
    return-void
.end method
