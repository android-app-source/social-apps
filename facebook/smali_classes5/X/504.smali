.class public final LX/504;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/Beta;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<B:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<TB;>;"
        }
    .end annotation
.end field

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>(Ljava/util/Comparator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<TB;>;)V"
        }
    .end annotation

    .prologue
    .line 823256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 823257
    const/4 v0, -0x1

    iput v0, p0, LX/504;->b:I

    .line 823258
    const v0, 0x7fffffff

    iput v0, p0, LX/504;->c:I

    .line 823259
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, p0, LX/504;->a:Ljava/util/Comparator;

    .line 823260
    return-void
.end method


# virtual methods
.method public final a(I)LX/504;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/504",
            "<TB;>;"
        }
    .end annotation

    .prologue
    .line 823261
    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 823262
    iput p1, p0, LX/504;->c:I

    .line 823263
    return-object p0

    .line 823264
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()LX/508;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:TB;>()",
            "LX/508",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 823265
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 823266
    new-instance v1, LX/508;

    iget v2, p0, LX/504;->b:I

    iget v3, p0, LX/504;->c:I

    invoke-static {v2, v3, v0}, LX/508;->a(IILjava/lang/Iterable;)I

    move-result v2

    invoke-direct {v1, p0, v2}, LX/508;-><init>(LX/504;I)V

    .line 823267
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 823268
    invoke-virtual {v1, v3}, LX/508;->offer(Ljava/lang/Object;)Z

    goto :goto_0

    .line 823269
    :cond_0
    move-object v0, v1

    .line 823270
    return-object v0
.end method
