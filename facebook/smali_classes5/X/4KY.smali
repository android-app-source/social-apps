.class public final LX/4KY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public final a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/4Un;

.field public c:Lcom/facebook/flatbuffers/MutableFlattenable;

.field public volatile d:Lcom/facebook/flatbuffers/MutableFlattenable;

.field public final synthetic e:LX/1N8;

.field public volatile f:I


# direct methods
.method public constructor <init>(LX/1N8;Ljava/util/Collection;LX/4Un;Lcom/facebook/flatbuffers/MutableFlattenable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/graphql/consistency/observer/GraphQLObserverMemoryCache$Callback;",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 679890
    iput-object p1, p0, LX/4KY;->e:LX/1N8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 679891
    const/4 v0, 0x0

    iput v0, p0, LX/4KY;->f:I

    .line 679892
    invoke-static {p2}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/4KY;->a:Ljava/util/Collection;

    .line 679893
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4Un;

    iput-object v0, p0, LX/4KY;->b:LX/4Un;

    .line 679894
    iput-object p4, p0, LX/4KY;->c:Lcom/facebook/flatbuffers/MutableFlattenable;

    .line 679895
    iput-object p4, p0, LX/4KY;->d:Lcom/facebook/flatbuffers/MutableFlattenable;

    .line 679896
    iget-object v1, p1, LX/1N8;->a:Ljava/util/Set;

    monitor-enter v1

    .line 679897
    :try_start_0
    iget-object v0, p1, LX/1N8;->a:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 679898
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 679899
    iget-object v0, p0, LX/4KY;->b:LX/4Un;

    iget-object v1, p0, LX/4KY;->d:Lcom/facebook/flatbuffers/MutableFlattenable;

    iget v2, p0, LX/4KY;->f:I

    invoke-virtual {v0, v1, v2}, LX/4Un;->a(Lcom/facebook/flatbuffers/MutableFlattenable;I)V

    .line 679900
    return-void
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 679901
    iget-object v0, p0, LX/4KY;->e:LX/1N8;

    iget-object v1, v0, LX/1N8;->a:Ljava/util/Set;

    monitor-enter v1

    .line 679902
    :try_start_0
    iget-object v0, p0, LX/4KY;->e:LX/1N8;

    iget-object v0, v0, LX/1N8;->a:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 679903
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
