.class public LX/3zS;
.super LX/1qS;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/3zS;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/3zY;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 662436
    invoke-static {p4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    const-string v5, "analytics_db2"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/1qS;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/0Px;Ljava/lang/String;)V

    .line 662437
    return-void
.end method

.method public static a(LX/0QB;)LX/3zS;
    .locals 7

    .prologue
    .line 662438
    sget-object v0, LX/3zS;->a:LX/3zS;

    if-nez v0, :cond_1

    .line 662439
    const-class v1, LX/3zS;

    monitor-enter v1

    .line 662440
    :try_start_0
    sget-object v0, LX/3zS;->a:LX/3zS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 662441
    if-eqz v2, :cond_0

    .line 662442
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 662443
    new-instance p0, LX/3zS;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Ts;->a(LX/0QB;)LX/0Ts;

    move-result-object v4

    check-cast v4, LX/0Tt;

    invoke-static {v0}, LX/1qT;->a(LX/0QB;)LX/1qT;

    move-result-object v5

    check-cast v5, LX/1qU;

    invoke-static {v0}, LX/3zY;->a(LX/0QB;)LX/3zY;

    move-result-object v6

    check-cast v6, LX/3zY;

    invoke-direct {p0, v3, v4, v5, v6}, LX/3zS;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/3zY;)V

    .line 662444
    move-object v0, p0

    .line 662445
    sput-object v0, LX/3zS;->a:LX/3zS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 662446
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 662447
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 662448
    :cond_1
    sget-object v0, LX/3zS;->a:LX/3zS;

    return-object v0

    .line 662449
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 662450
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 0

    .prologue
    .line 662451
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 662452
    const v0, 0xc800

    return v0
.end method
