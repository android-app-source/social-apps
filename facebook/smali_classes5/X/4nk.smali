.class public LX/4nk;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/widget/NotificationTextSwitcher;

.field public final b:Landroid/view/View;

.field public final c:Landroid/os/Handler;

.field private d:Landroid/animation/ObjectAnimator;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 807404
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 807405
    const v0, 0x7f03079a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 807406
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/4nk;->setClickable(Z)V

    .line 807407
    const v0, 0x7f020ac8

    invoke-virtual {p0, v0}, LX/4nk;->setBackgroundResource(I)V

    .line 807408
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, LX/4nk;->setGravity(I)V

    .line 807409
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, LX/4nk;->setVisibility(I)V

    .line 807410
    const v0, 0x7f0d144c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/NotificationTextSwitcher;

    iput-object v0, p0, LX/4nk;->a:Lcom/facebook/widget/NotificationTextSwitcher;

    .line 807411
    const v0, 0x7f0d144b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/4nk;->b:Landroid/view/View;

    .line 807412
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/4nk;->c:Landroid/os/Handler;

    .line 807413
    new-instance v0, LX/4nh;

    invoke-direct {v0, p0}, LX/4nh;-><init>(LX/4nk;)V

    invoke-virtual {p0, v0}, LX/4nk;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 807414
    return-void
.end method

.method private a(FFLandroid/animation/Animator$AnimatorListener;)V
    .locals 4

    .prologue
    .line 807399
    const-string v0, "translationY"

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, LX/4nk;->d:Landroid/animation/ObjectAnimator;

    .line 807400
    iget-object v0, p0, LX/4nk;->d:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 807401
    iget-object v0, p0, LX/4nk;->d:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p3}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 807402
    iget-object v0, p0, LX/4nk;->d:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 807403
    return-void
.end method

.method private b(Ljava/lang/String;Z)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 807391
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 807392
    :goto_0
    const/4 v0, 0x0

    .line 807393
    iget-object v2, p0, LX/4nk;->d:Landroid/animation/ObjectAnimator;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/4nk;->d:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 807394
    iget-object v0, p0, LX/4nk;->d:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 807395
    iget-object v2, p0, LX/4nk;->d:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 807396
    :cond_0
    invoke-virtual {p0}, LX/4nk;->getHeight()I

    move-result v2

    mul-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    new-instance v3, LX/4nj;

    invoke-direct {v3, p0, v1, p1, p2}, LX/4nj;-><init>(LX/4nk;ZLjava/lang/String;Z)V

    invoke-direct {p0, v0, v2, v3}, LX/4nk;->a(FFLandroid/animation/Animator$AnimatorListener;)V

    .line 807397
    return-void

    .line 807398
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 807379
    iget-object v0, p0, LX/4nk;->a:Lcom/facebook/widget/NotificationTextSwitcher;

    invoke-virtual {v0}, Lcom/facebook/widget/NotificationTextSwitcher;->a()V

    .line 807380
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/4nk;->b(Ljava/lang/String;Z)V

    .line 807381
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 807389
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/4nk;->a(Ljava/lang/String;ZLX/2qq;)V

    .line 807390
    return-void
.end method

.method public final a(Ljava/lang/String;ZLX/2qq;)V
    .locals 3

    .prologue
    .line 807382
    invoke-virtual {p0}, LX/4nk;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 807383
    if-eqz v0, :cond_1

    .line 807384
    iget-object v0, p0, LX/4nk;->a:Lcom/facebook/widget/NotificationTextSwitcher;

    invoke-virtual {v0}, Lcom/facebook/widget/NotificationTextSwitcher;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 807385
    invoke-direct {p0, p1, p2}, LX/4nk;->b(Ljava/lang/String;Z)V

    .line 807386
    :cond_0
    :goto_1
    return-void

    .line 807387
    :cond_1
    iget-object v0, p0, LX/4nk;->a:Lcom/facebook/widget/NotificationTextSwitcher;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/NotificationTextSwitcher;->setText(Ljava/lang/CharSequence;)V

    .line 807388
    invoke-virtual {p0}, LX/4nk;->getHeight()I

    move-result v0

    mul-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    const/4 v1, 0x0

    new-instance v2, LX/4ni;

    invoke-direct {v2, p0, p3, p2}, LX/4ni;-><init>(LX/4nk;LX/2qq;Z)V

    invoke-direct {p0, v0, v1, v2}, LX/4nk;->a(FFLandroid/animation/Animator$AnimatorListener;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
