.class public LX/3cP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 613833
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 613834
    return-void
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 16

    .prologue
    .line 613835
    sget-object v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;->a:LX/1Cz;

    sget-object v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFooterPartDefinition;->a:LX/1Cz;

    sget-object v3, Lcom/facebook/video/videohome/partdefinitions/VideoHomeGapPartDefinition;->a:LX/1Cz;

    sget-object v4, Lcom/facebook/video/videohome/partdefinitions/VideoHomePublisherInfoPartDefinition;->a:LX/1Cz;

    sget-object v5, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBlockPartDefinition;->a:LX/1Cz;

    sget-object v6, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitVideoPartDefinition;->a:LX/1Cz;

    sget-object v7, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitBottomContextPartDefinition;->a:LX/1Cz;

    sget-object v8, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitRectangleVideoPartDefinition;->a:LX/1Cz;

    sget-object v9, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareGroupPartDefinition;->a:LX/1Cz;

    sget-object v10, Lcom/facebook/video/videohome/partdefinitions/VideoHomeVideoChannelFeedUnitSquareVideoPartDefinition;->a:LX/1Cz;

    sget-object v11, Lcom/facebook/video/videohome/partdefinitions/VideoHomeClippedPublisherInfoPartDefinition;->a:LX/1Cz;

    sget-object v12, Lcom/facebook/video/videohome/partdefinitions/VideoHomeLoadingPageHscrollLiveVideoPartDefinition;->a:LX/1Cz;

    const/4 v13, 0x6

    new-array v13, v13, [LX/1Cz;

    const/4 v14, 0x0

    sget-object v15, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSectionHeaderPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x1

    sget-object v15, Lcom/facebook/video/videohome/partdefinitions/VideoHomeShortcutPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x2

    sget-object v15, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSingleNotificationPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x3

    sget-object v15, Lcom/facebook/video/videohome/partdefinitions/VideoHomeSpinnerPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x4

    sget-object v15, Lcom/facebook/video/videohome/partdefinitions/VideoHomeTopBorderPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x5

    sget-object v15, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFeedbackPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    invoke-static/range {v1 .. v13}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 613836
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Cz;

    .line 613837
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, LX/1KB;->a(LX/1Cz;)V

    .line 613838
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 613839
    :cond_0
    return-void
.end method
