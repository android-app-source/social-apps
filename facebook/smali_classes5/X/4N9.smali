.class public LX/4N9;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 690862
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 37

    .prologue
    .line 690863
    const/16 v31, 0x0

    .line 690864
    const/16 v30, 0x0

    .line 690865
    const/16 v29, 0x0

    .line 690866
    const/16 v28, 0x0

    .line 690867
    const/16 v27, 0x0

    .line 690868
    const/16 v26, 0x0

    .line 690869
    const/16 v25, 0x0

    .line 690870
    const/16 v24, 0x0

    .line 690871
    const/16 v23, 0x0

    .line 690872
    const/16 v22, 0x0

    .line 690873
    const/16 v21, 0x0

    .line 690874
    const/16 v20, 0x0

    .line 690875
    const/16 v19, 0x0

    .line 690876
    const/16 v18, 0x0

    .line 690877
    const/16 v17, 0x0

    .line 690878
    const/16 v16, 0x0

    .line 690879
    const/4 v13, 0x0

    .line 690880
    const-wide/16 v14, 0x0

    .line 690881
    const/4 v12, 0x0

    .line 690882
    const/4 v11, 0x0

    .line 690883
    const/4 v10, 0x0

    .line 690884
    const/4 v9, 0x0

    .line 690885
    const/4 v8, 0x0

    .line 690886
    const/4 v7, 0x0

    .line 690887
    const/4 v6, 0x0

    .line 690888
    const/4 v5, 0x0

    .line 690889
    const/4 v4, 0x0

    .line 690890
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v32

    sget-object v33, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    if-eq v0, v1, :cond_1d

    .line 690891
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 690892
    const/4 v4, 0x0

    .line 690893
    :goto_0
    return v4

    .line 690894
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 690895
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v32

    sget-object v33, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    if-eq v0, v1, :cond_14

    .line 690896
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v32

    .line 690897
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 690898
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v33

    sget-object v34, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    if-eq v0, v1, :cond_1

    if-eqz v32, :cond_1

    .line 690899
    const-string v33, "campaign_title"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_2

    .line 690900
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    goto :goto_1

    .line 690901
    :cond_2
    const-string v33, "id"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_3

    .line 690902
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    goto :goto_1

    .line 690903
    :cond_3
    const-string v33, "owner"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_4

    .line 690904
    invoke-static/range {p0 .. p1}, LX/2bM;->a(LX/15w;LX/186;)I

    move-result v29

    goto :goto_1

    .line 690905
    :cond_4
    const-string v33, "posted_item_privacy_scope"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_5

    .line 690906
    invoke-static/range {p0 .. p1}, LX/2bo;->a(LX/15w;LX/186;)I

    move-result v28

    goto :goto_1

    .line 690907
    :cond_5
    const-string v33, "url"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_6

    .line 690908
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    goto :goto_1

    .line 690909
    :cond_6
    const-string v33, "can_invite_to_campaign"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_7

    .line 690910
    const/4 v13, 0x1

    .line 690911
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v26

    goto/16 :goto_1

    .line 690912
    :cond_7
    const-string v33, "can_viewer_delete"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_8

    .line 690913
    const/4 v12, 0x1

    .line 690914
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v25

    goto/16 :goto_1

    .line 690915
    :cond_8
    const-string v33, "can_viewer_edit"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_9

    .line 690916
    const/4 v11, 0x1

    .line 690917
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v24

    goto/16 :goto_1

    .line 690918
    :cond_9
    const-string v33, "can_viewer_post"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_a

    .line 690919
    const/4 v10, 0x1

    .line 690920
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v23

    goto/16 :goto_1

    .line 690921
    :cond_a
    const-string v33, "can_viewer_report"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_b

    .line 690922
    const/4 v9, 0x1

    .line 690923
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v22

    goto/16 :goto_1

    .line 690924
    :cond_b
    const-string v33, "fundraiser_page_subtitle"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_c

    .line 690925
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 690926
    :cond_c
    const-string v33, "is_viewer_following"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_d

    .line 690927
    const/4 v8, 0x1

    .line 690928
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v20

    goto/16 :goto_1

    .line 690929
    :cond_d
    const-string v33, "logo_image"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_e

    .line 690930
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 690931
    :cond_e
    const-string v33, "privacy_scope"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_f

    .line 690932
    invoke-static/range {p0 .. p1}, LX/2bo;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 690933
    :cond_f
    const-string v33, "donors_social_context_text"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_10

    .line 690934
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 690935
    :cond_10
    const-string v33, "fundraiser_progress_text"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_11

    .line 690936
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 690937
    :cond_11
    const-string v33, "has_goal_amount"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_12

    .line 690938
    const/4 v5, 0x1

    .line 690939
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v15

    goto/16 :goto_1

    .line 690940
    :cond_12
    const-string v33, "percent_of_goal_reached"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_13

    .line 690941
    const/4 v4, 0x1

    .line 690942
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    goto/16 :goto_1

    .line 690943
    :cond_13
    const-string v33, "user_donors"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_0

    .line 690944
    invoke-static/range {p0 .. p1}, LX/4N6;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 690945
    :cond_14
    const/16 v32, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 690946
    const/16 v32, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v32

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 690947
    const/16 v31, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v31

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 690948
    const/16 v30, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v30

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 690949
    const/16 v29, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v29

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 690950
    const/16 v28, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v28

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 690951
    if-eqz v13, :cond_15

    .line 690952
    const/4 v13, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v13, v1}, LX/186;->a(IZ)V

    .line 690953
    :cond_15
    if-eqz v12, :cond_16

    .line 690954
    const/4 v12, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 690955
    :cond_16
    if-eqz v11, :cond_17

    .line 690956
    const/4 v11, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 690957
    :cond_17
    if-eqz v10, :cond_18

    .line 690958
    const/16 v10, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 690959
    :cond_18
    if-eqz v9, :cond_19

    .line 690960
    const/16 v9, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 690961
    :cond_19
    const/16 v9, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 690962
    if-eqz v8, :cond_1a

    .line 690963
    const/16 v8, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 690964
    :cond_1a
    const/16 v8, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 690965
    const/16 v8, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 690966
    const/16 v8, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 690967
    const/16 v8, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 690968
    if-eqz v5, :cond_1b

    .line 690969
    const/16 v5, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v15}, LX/186;->a(IZ)V

    .line 690970
    :cond_1b
    if-eqz v4, :cond_1c

    .line 690971
    const/16 v5, 0x11

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 690972
    :cond_1c
    const/16 v4, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 690973
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_1d
    move/from16 v35, v6

    move/from16 v36, v7

    move-wide v6, v14

    move v14, v12

    move v15, v13

    move v12, v10

    move v13, v11

    move v11, v9

    move v10, v8

    move/from16 v9, v36

    move/from16 v8, v35

    goto/16 :goto_1
.end method
