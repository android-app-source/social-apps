.class public LX/43P;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 668994
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/InputStream;Z)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Z)",
            "Ljava/util/List",
            "<",
            "LX/43O;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v7, 0xff

    const/4 v0, 0x0

    const/4 v6, -0x1

    .line 668995
    :try_start_0
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v1

    if-ne v1, v7, :cond_0

    invoke-virtual {p0}, Ljava/io/InputStream;->read()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    const/16 v2, 0xd8

    if-eq v1, v2, :cond_2

    .line 668996
    :cond_0
    if-eqz p0, :cond_1

    .line 668997
    :try_start_1
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    .line 668998
    :cond_1
    :goto_0
    return-object v0

    .line 668999
    :cond_2
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 669000
    :goto_1
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    if-eq v2, v6, :cond_d

    .line 669001
    if-eq v2, v7, :cond_3

    .line 669002
    if-eqz p0, :cond_1

    .line 669003
    :try_start_3
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_0
    goto :goto_0

    .line 669004
    :cond_3
    :try_start_4
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v2

    if-eq v2, v7, :cond_3

    .line 669005
    if-ne v2, v6, :cond_4

    .line 669006
    if-eqz p0, :cond_1

    .line 669007
    :try_start_5
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_0

    :catch_1
    goto :goto_0

    .line 669008
    :cond_4
    const/16 v3, 0xda

    if-ne v2, v3, :cond_7

    .line 669009
    if-nez p1, :cond_5

    .line 669010
    :try_start_6
    new-instance v3, LX/43O;

    invoke-direct {v3}, LX/43O;-><init>()V

    .line 669011
    iput v2, v3, LX/43O;->a:I

    .line 669012
    const/4 v2, -0x1

    iput v2, v3, LX/43O;->b:I

    .line 669013
    invoke-virtual {p0}, Ljava/io/InputStream;->available()I

    move-result v2

    new-array v2, v2, [B

    iput-object v2, v3, LX/43O;->c:[B

    .line 669014
    iget-object v2, v3, LX/43O;->c:[B

    const/4 v4, 0x0

    iget-object v5, v3, LX/43O;->c:[B

    array-length v5, v5

    invoke-virtual {p0, v2, v4, v5}, Ljava/io/InputStream;->read([BII)I

    .line 669015
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 669016
    :cond_5
    if-eqz p0, :cond_6

    .line 669017
    :try_start_7
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    :cond_6
    :goto_2
    move-object v0, v1

    .line 669018
    goto :goto_0

    .line 669019
    :cond_7
    :try_start_8
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v3

    .line 669020
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result v4

    .line 669021
    if-eq v3, v6, :cond_8

    if-ne v4, v6, :cond_9

    .line 669022
    :cond_8
    if-eqz p0, :cond_1

    .line 669023
    :try_start_9
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2

    goto :goto_0

    :catch_2
    goto :goto_0

    .line 669024
    :cond_9
    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v3, v4

    .line 669025
    if-eqz p1, :cond_a

    const/16 v4, 0xe1

    if-ne v2, v4, :cond_b

    .line 669026
    :cond_a
    :try_start_a
    new-instance v4, LX/43O;

    invoke-direct {v4}, LX/43O;-><init>()V

    .line 669027
    iput v2, v4, LX/43O;->a:I

    .line 669028
    iput v3, v4, LX/43O;->b:I

    .line 669029
    add-int/lit8 v2, v3, -0x2

    new-array v2, v2, [B

    iput-object v2, v4, LX/43O;->c:[B

    .line 669030
    iget-object v2, v4, LX/43O;->c:[B

    const/4 v5, 0x0

    add-int/lit8 v3, v3, -0x2

    invoke-virtual {p0, v2, v5, v3}, Ljava/io/InputStream;->read([BII)I

    .line 669031
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_1

    .line 669032
    :catch_3
    if-eqz p0, :cond_1

    .line 669033
    :try_start_b
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    goto/16 :goto_0

    :catch_4
    goto/16 :goto_0

    .line 669034
    :cond_b
    add-int/lit8 v2, v3, -0x2

    int-to-long v2, v2

    :try_start_c
    invoke-virtual {p0, v2, v3}, Ljava/io/InputStream;->skip(J)J
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_1

    .line 669035
    :catchall_0
    move-exception v0

    if-eqz p0, :cond_c

    .line 669036
    :try_start_d
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_8

    .line 669037
    :cond_c
    :goto_3
    throw v0

    .line 669038
    :cond_d
    if-eqz p0, :cond_e

    .line 669039
    :try_start_e
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_7

    :cond_e
    :goto_4
    move-object v0, v1

    .line 669040
    goto/16 :goto_0

    :catch_5
    goto/16 :goto_0

    :catch_6
    goto :goto_2

    :catch_7
    goto :goto_4

    :catch_8
    goto :goto_3
.end method

.method private static a(Ljava/io/InputStream;)[B
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 668969
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/43P;->a(Ljava/io/InputStream;Z)Ljava/util/List;

    move-result-object v0

    .line 668970
    if-nez v0, :cond_0

    move-object v0, v1

    .line 668971
    :goto_0
    return-object v0

    .line 668972
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/43O;

    .line 668973
    iget-object v3, v0, LX/43O;->c:[B

    const/16 v6, 0x1d

    const/4 v4, 0x0

    .line 668974
    array-length v5, v3

    if-ge v5, v6, :cond_4

    .line 668975
    :cond_2
    :goto_1
    move v3, v4

    .line 668976
    if-eqz v3, :cond_1

    .line 668977
    iget-object v1, v0, LX/43O;->c:[B

    .line 668978
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    :goto_2
    if-lez v2, :cond_6

    .line 668979
    aget-byte v3, v1, v2

    const/16 v4, 0x3e

    if-ne v3, v4, :cond_5

    .line 668980
    add-int/lit8 v3, v2, -0x1

    aget-byte v3, v1, v3

    const/16 v4, 0x3f

    if-eq v3, v4, :cond_5

    .line 668981
    add-int/lit8 v2, v2, 0x1

    .line 668982
    :goto_3
    move v1, v2

    .line 668983
    add-int/lit8 v1, v1, -0x1d

    new-array v1, v1, [B

    .line 668984
    iget-object v0, v0, LX/43O;->c:[B

    const/16 v2, 0x1d

    const/4 v3, 0x0

    array-length v4, v1

    invoke-static {v0, v2, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v0, v1

    .line 668985
    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 668986
    goto :goto_0

    .line 668987
    :cond_4
    const/16 v5, 0x1d

    :try_start_0
    new-array v5, v5, [B

    .line 668988
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 p0, 0x1d

    invoke-static {v3, v6, v5, v7, p0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 668989
    new-instance v6, Ljava/lang/String;

    const-string v7, "UTF-8"

    invoke-direct {v6, v5, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    const-string v5, "http://ns.adobe.com/xap/1.0/\u0000"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_2

    .line 668990
    const/4 v4, 0x1

    goto :goto_1

    .line 668991
    :catch_0
    goto :goto_1

    .line 668992
    :cond_5
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 668993
    :cond_6
    array-length v2, v1

    goto :goto_3
.end method

.method public static a(Ljava/lang/String;)[B
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 668956
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 668957
    :try_start_1
    invoke-static {v2}, LX/43P;->a(Ljava/io/InputStream;)[B
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 668958
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 668959
    :cond_0
    :goto_0
    return-object v0

    .line 668960
    :catch_0
    move-exception v1

    move-object v2, v0

    .line 668961
    :goto_1
    :try_start_3
    const-string v3, "XMPUtil"

    const-string v4, "Could not read file: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p0, v5, v6

    invoke-static {v3, v1, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 668962
    if-eqz v2, :cond_0

    .line 668963
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    :catch_1
    goto :goto_0

    .line 668964
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_2
    if-eqz v2, :cond_1

    .line 668965
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 668966
    :cond_1
    :goto_3
    throw v0

    :catch_2
    goto :goto_0

    :catch_3
    goto :goto_3

    .line 668967
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 668968
    :catch_4
    move-exception v1

    goto :goto_1
.end method
