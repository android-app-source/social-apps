.class public final LX/3dq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;",
        "LX/2YS;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;)V
    .locals 0

    .prologue
    .line 617430
    iput-object p1, p0, LX/3dq;->a:Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 617431
    check-cast p1, Ljava/util/List;

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 617432
    const/4 v1, 0x0

    .line 617433
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v3

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/OperationResult;

    .line 617434
    iget-boolean v6, v0, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v6, v6

    .line 617435
    if-eqz v6, :cond_0

    move v2, v4

    .line 617436
    goto :goto_0

    .line 617437
    :cond_0
    iget-object v6, v0, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    move-object v6, v6

    .line 617438
    if-nez v6, :cond_4

    if-nez v1, :cond_4

    .line 617439
    iget-object v6, v0, Lcom/facebook/fbservice/service/OperationResult;->errorThrowable:Ljava/lang/Throwable;

    move-object v6, v6

    .line 617440
    if-eqz v6, :cond_4

    .line 617441
    iget-object v1, v0, Lcom/facebook/fbservice/service/OperationResult;->errorThrowable:Ljava/lang/Throwable;

    move-object v0, v1

    .line 617442
    :goto_1
    move-object v1, v0

    .line 617443
    goto :goto_0

    .line 617444
    :cond_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 617445
    if-eqz v2, :cond_2

    .line 617446
    new-instance v0, LX/2YS;

    invoke-direct {v0, v4}, LX/2YS;-><init>(Z)V

    .line 617447
    :goto_2
    return-object v0

    .line 617448
    :cond_2
    if-eqz v1, :cond_3

    .line 617449
    invoke-static {v1}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 617450
    :cond_3
    new-instance v0, LX/2YS;

    invoke-direct {v0, v3}, LX/2YS;-><init>(Z)V

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method
