.class public LX/3nP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26t;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3nP;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/69K;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0pk;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0pi;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/69K;",
            ">;",
            "LX/0pi;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 638685
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 638686
    iput-object p1, p0, LX/3nP;->a:LX/0Ot;

    .line 638687
    const-string v0, "feed_db_cleared"

    invoke-virtual {p2, v0}, LX/0pi;->b(Ljava/lang/String;)LX/0pk;

    move-result-object v0

    iput-object v0, p0, LX/3nP;->b:LX/0pk;

    .line 638688
    return-void
.end method

.method public static a(LX/0QB;)LX/3nP;
    .locals 5

    .prologue
    .line 638672
    sget-object v0, LX/3nP;->c:LX/3nP;

    if-nez v0, :cond_1

    .line 638673
    const-class v1, LX/3nP;

    monitor-enter v1

    .line 638674
    :try_start_0
    sget-object v0, LX/3nP;->c:LX/3nP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 638675
    if-eqz v2, :cond_0

    .line 638676
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 638677
    new-instance v4, LX/3nP;

    const/16 v3, 0x1758

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0pi;->a(LX/0QB;)LX/0pi;

    move-result-object v3

    check-cast v3, LX/0pi;

    invoke-direct {v4, p0, v3}, LX/3nP;-><init>(LX/0Ot;LX/0pi;)V

    .line 638678
    move-object v0, v4

    .line 638679
    sput-object v0, LX/3nP;->c:LX/3nP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 638680
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 638681
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 638682
    :cond_1
    sget-object v0, LX/3nP;->c:LX/3nP;

    return-object v0

    .line 638683
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 638684
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/fbservice/service/OperationResult;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 9

    .prologue
    .line 638659
    iget-boolean v0, p0, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 638660
    if-eqz v0, :cond_0

    .line 638661
    invoke-virtual {p0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/facebook/api/feed/FetchFeedResult;

    .line 638662
    if-eqz v6, :cond_1

    .line 638663
    iget-object v0, v6, Lcom/facebook/api/feed/FetchFeedResult;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-object v2, v0

    .line 638664
    new-instance v0, Lcom/facebook/api/feed/FetchFeedResult;

    .line 638665
    iget-object v1, v6, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v1, v1

    .line 638666
    iget-object v3, v6, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v3, v3

    .line 638667
    iget-wide v7, v6, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v4, v7

    .line 638668
    iget-boolean v7, v6, Lcom/facebook/api/feed/FetchFeedResult;->c:Z

    move v6, v7

    .line 638669
    invoke-direct/range {v0 .. v6}, Lcom/facebook/api/feed/FetchFeedResult;-><init>(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/model/GraphQLFeedHomeStories;LX/0ta;JZ)V

    .line 638670
    :goto_0
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object p0

    .line 638671
    :cond_0
    return-object p0

    :cond_1
    move-object v0, v6

    goto :goto_0
.end method

.method private d(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 638645
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 638646
    const-string v1, "fetchFeedParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/FetchFeedParams;

    .line 638647
    iget-boolean v1, v0, Lcom/facebook/api/feed/FetchFeedParams;->r:Z

    move v1, v1

    .line 638648
    iget-object v2, v0, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v0, v2

    .line 638649
    invoke-virtual {v0}, Lcom/facebook/api/feedtype/FeedType;->e()LX/0pL;

    move-result-object v0

    .line 638650
    sget-object v2, LX/0pL;->NO_CACHE:LX/0pL;

    if-eq v0, v2, :cond_0

    sget-object v2, LX/0pL;->MEMORY_ONLY_CACHE:LX/0pL;

    if-ne v0, v2, :cond_1

    .line 638651
    :cond_0
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 638652
    :goto_0
    return-object v0

    .line 638653
    :cond_1
    const-string v0, "NFDbServiceHandler.handleFetchNewsFeed"

    const v2, 0x3a4f35f7

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 638654
    if-nez v1, :cond_2

    :goto_1
    :try_start_0
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 638655
    invoke-static {v0}, LX/3nP;->a(Lcom/facebook/fbservice/service/OperationResult;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 638656
    const v1, -0x49724943

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 638657
    :cond_2
    :try_start_1
    new-instance v0, LX/69L;

    invoke-direct {v0, p0}, LX/69L;-><init>(LX/3nP;)V

    invoke-virtual {p1, v0}, LX/1qK;->chain(LX/4B6;)LX/1qK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object p1

    goto :goto_1

    .line 638658
    :catchall_0
    move-exception v0

    const v1, -0x95998cc

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 638569
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 638570
    invoke-static {v0}, LX/3nN;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 638571
    invoke-direct {p0, p1, p2}, LX/3nP;->d(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 638572
    :goto_0
    return-object v0

    .line 638573
    :cond_0
    const-string v1, "feed_clear_cache"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 638574
    iget-object v0, p0, LX/3nP;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/69K;

    .line 638575
    iget-object v1, v0, LX/69K;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0pn;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0pn;->a(Lcom/facebook/api/feedtype/FeedType;)V

    .line 638576
    iget-object v0, p0, LX/3nP;->b:LX/0pk;

    invoke-virtual {v0}, LX/0pk;->b()V

    .line 638577
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 638578
    goto :goto_0

    .line 638579
    :cond_1
    const-string v1, "feed_mark_impression_logged"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 638580
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 638581
    goto :goto_0

    .line 638582
    :cond_2
    const-string v1, "feed_mark_survey_completed"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 638583
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 638584
    const-string v1, "markSurveyCompletedParamsKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/MarkSurveyCompletedParams;

    .line 638585
    iget-object v1, v0, Lcom/facebook/api/feed/MarkSurveyCompletedParams;->a:Ljava/lang/String;

    if-eqz v1, :cond_9

    iget-object v1, v0, Lcom/facebook/api/feed/MarkSurveyCompletedParams;->b:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 638586
    iget-object v1, v0, Lcom/facebook/api/feed/MarkSurveyCompletedParams;->b:Ljava/lang/String;

    .line 638587
    const v2, -0x46f2ee24

    invoke-static {v2}, LX/38I;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 638588
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 638589
    :goto_1
    move-object v0, v0

    .line 638590
    goto :goto_0

    .line 638591
    :cond_3
    const-string v1, "feed_mark_research_poll_completed"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 638592
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 638593
    const-string v1, "markResearchPollCompletedParamsKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/MarkResearchPollCompletedParams;

    .line 638594
    iget-object v1, v0, Lcom/facebook/api/feed/MarkResearchPollCompletedParams;->a:Ljava/lang/String;

    if-eqz v1, :cond_a

    iget-object v1, v0, Lcom/facebook/api/feed/MarkResearchPollCompletedParams;->b:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 638595
    iget-object v1, v0, Lcom/facebook/api/feed/MarkResearchPollCompletedParams;->b:Ljava/lang/String;

    .line 638596
    const v2, -0x44774584

    invoke-static {v2}, LX/38I;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 638597
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 638598
    :goto_2
    move-object v0, v0

    .line 638599
    goto/16 :goto_0

    .line 638600
    :cond_4
    const-string v1, "set_hscroll_unit_visible_item_index"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 638601
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 638602
    const-string v1, "setHScrollUnitVisibleItemIndexKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/SetHScrollUnitVisibleItemIndexParams;

    .line 638603
    iget-object v1, v0, Lcom/facebook/api/feed/SetHScrollUnitVisibleItemIndexParams;->a:Ljava/lang/String;

    if-eqz v1, :cond_b

    iget-object v1, v0, Lcom/facebook/api/feed/SetHScrollUnitVisibleItemIndexParams;->b:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 638604
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 638605
    :goto_3
    move-object v0, v0

    .line 638606
    goto/16 :goto_0

    .line 638607
    :cond_5
    const-string v1, "update_story_saved_state"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 638608
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 638609
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 638610
    if-nez v0, :cond_c

    move-object v0, v1

    .line 638611
    :goto_4
    move-object v0, v0

    .line 638612
    goto/16 :goto_0

    .line 638613
    :cond_6
    const-string v1, "update_timeline_app_collection_in_newsfeed"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 638614
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 638615
    iget-boolean v0, v1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 638616
    if-nez v0, :cond_f

    move-object v0, v1

    .line 638617
    :goto_5
    move-object v0, v0

    .line 638618
    goto/16 :goto_0

    .line 638619
    :cond_7
    const-string v1, "xOutPlaceReviewItem"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 638620
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 638621
    goto/16 :goto_0

    .line 638622
    :cond_8
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    sget-object v1, LX/1nY;->OTHER:LX/1nY;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string p0, "feed_mark_survey_completed is not supported on "

    invoke-direct {v2, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/facebook/api/feed/MarkSurveyCompletedParams;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_1

    :cond_a
    sget-object v1, LX/1nY;->OTHER:LX/1nY;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string p0, "feed_mark_research_poll_completed is not supported on "

    invoke-direct {v2, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/facebook/api/feed/MarkResearchPollCompletedParams;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_2

    :cond_b
    sget-object v1, LX/1nY;->API_ERROR:LX/1nY;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string p0, "set_hscroll_unit_visible_item_index is not supported on "

    invoke-direct {v2, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/facebook/api/feed/SetHScrollUnitVisibleItemIndexParams;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_3

    .line 638623
    :cond_c
    const-string v2, "updateStorySavedStateParamsKey"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/server/UpdateSavedStateParams;

    .line 638624
    iget-object v2, v0, Lcom/facebook/saved/server/UpdateSavedStateParams;->i:LX/0Px;

    move-object v0, v2

    .line 638625
    if-eqz v0, :cond_d

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    .line 638626
    iget-boolean v0, v1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 638627
    if-nez v0, :cond_e

    .line 638628
    iget-object v0, v1, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    move-object v0, v0

    .line 638629
    sget-object v2, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-eq v0, v2, :cond_e

    :cond_d
    move-object v0, v1

    .line 638630
    goto/16 :goto_4

    :cond_e
    move-object v0, v1

    .line 638631
    goto/16 :goto_4

    .line 638632
    :cond_f
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 638633
    const-string v2, "timelineAppCollectionParamsKey"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;

    .line 638634
    if-eqz v0, :cond_10

    .line 638635
    iget-object v2, v0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->h:LX/0Px;

    move-object v2, v2

    .line 638636
    if-eqz v2, :cond_10

    .line 638637
    iget-object v2, v0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->h:LX/0Px;

    move-object v2, v2

    .line 638638
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_10

    .line 638639
    iget-object v2, v0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->f:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v2, v2

    .line 638640
    if-eqz v2, :cond_10

    .line 638641
    iget-object v2, v0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->a:Ljava/lang/String;

    move-object v0, v2

    .line 638642
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_11

    :cond_10
    move-object v0, v1

    .line 638643
    goto/16 :goto_5

    :cond_11
    move-object v0, v1

    .line 638644
    goto/16 :goto_5
.end method
