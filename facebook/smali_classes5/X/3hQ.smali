.class public LX/3hQ;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:[I


# instance fields
.field private final c:LX/2Rd;

.field private final d:LX/3Lp;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Lt;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/lang/StringBuilder;

.field public h:[Ljava/lang/String;

.field public i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 626792
    const-class v0, LX/3hQ;

    sput-object v0, LX/3hQ;->a:Ljava/lang/Class;

    .line 626793
    const/16 v0, 0x1e

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/3hQ;->b:[I

    return-void

    :array_0
    .array-data 4
        0x1100
        0x1101
        0x0
        0x1102
        0x0
        0x0
        0x1103
        0x1104
        0x1105
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x1106
        0x1107
        0x1108
        0x0
        0x1109
        0x110a
        0x110b
        0x110c
        0x110d
        0x110e
        0x110f
        0x1110
        0x1111
        0x1112
    .end array-data
.end method

.method public constructor <init>(LX/2Rd;LX/3Lp;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/user/names/Normalizer;",
            "LX/3Lp;",
            "LX/0Ot",
            "<",
            "LX/3Lt;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 626673
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 626674
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, LX/3hQ;->g:Ljava/lang/StringBuilder;

    .line 626675
    const/4 v0, 0x0

    iput-object v0, p0, LX/3hQ;->h:[Ljava/lang/String;

    .line 626676
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3hQ;->i:Z

    .line 626677
    iput-object p1, p0, LX/3hQ;->c:LX/2Rd;

    .line 626678
    iput-object p2, p0, LX/3hQ;->d:LX/3Lp;

    .line 626679
    iput-object p3, p0, LX/3hQ;->e:LX/0Ot;

    .line 626680
    new-instance v0, LX/0UE;

    invoke-direct {v0}, LX/0UE;-><init>()V

    iput-object v0, p0, LX/3hQ;->f:Ljava/util/Set;

    .line 626681
    return-void
.end method

.method public static a(LX/3hQ;I)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 626782
    iget-boolean v1, p0, LX/3hQ;->i:Z

    if-eqz v1, :cond_2

    .line 626783
    iget-object v1, p0, LX/3hQ;->g:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 626784
    :goto_0
    if-ge v0, p1, :cond_1

    .line 626785
    if-eqz v0, :cond_0

    .line 626786
    iget-object v1, p0, LX/3hQ;->g:Ljava/lang/StringBuilder;

    const/16 v2, 0x2e

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 626787
    :cond_0
    iget-object v1, p0, LX/3hQ;->g:Ljava/lang/StringBuilder;

    iget-object v2, p0, LX/3hQ;->h:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 626788
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 626789
    :cond_1
    iget-object v0, p0, LX/3hQ;->f:Ljava/util/Set;

    iget-object v1, p0, LX/3hQ;->g:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 626790
    :cond_2
    invoke-static {p0, p1}, LX/3hQ;->b(LX/3hQ;I)V

    .line 626791
    return-void
.end method

.method public static a(LX/3hQ;IIZ)V
    .locals 4

    .prologue
    .line 626770
    if-ne p1, p2, :cond_1

    .line 626771
    invoke-static {p0, p2}, LX/3hQ;->a(LX/3hQ;I)V

    .line 626772
    :cond_0
    return-void

    .line 626773
    :cond_1
    iget-object v0, p0, LX/3hQ;->h:[Ljava/lang/String;

    aget-object v2, v0, p1

    move v1, p1

    .line 626774
    :goto_0
    if-ge v1, p2, :cond_0

    .line 626775
    iget-object v0, p0, LX/3hQ;->h:[Ljava/lang/String;

    iget-object v3, p0, LX/3hQ;->h:[Ljava/lang/String;

    aget-object v3, v3, v1

    aput-object v3, v0, p1

    .line 626776
    iget-object v0, p0, LX/3hQ;->h:[Ljava/lang/String;

    aput-object v2, v0, v1

    .line 626777
    add-int/lit8 v3, p1, 0x1

    if-eqz p3, :cond_2

    if-ne v1, p1, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {p0, v3, p2, v0}, LX/3hQ;->a(LX/3hQ;IIZ)V

    .line 626778
    iget-object v0, p0, LX/3hQ;->h:[Ljava/lang/String;

    iget-object v3, p0, LX/3hQ;->h:[Ljava/lang/String;

    aget-object v3, v3, p1

    aput-object v3, v0, v1

    .line 626779
    iget-object v0, p0, LX/3hQ;->h:[Ljava/lang/String;

    aput-object v2, v0, p1

    .line 626780
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 626781
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(Lcom/facebook/user/model/Name;)LX/0Rf;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/user/model/Name;",
            ")",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 626747
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v1

    .line 626748
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 626749
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 626750
    const/16 v0, 0xa

    new-array v2, v0, [Ljava/lang/String;

    .line 626751
    iget-object v0, p0, LX/3hQ;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Lt;

    invoke-virtual {p1}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    .line 626752
    if-nez v3, :cond_4

    .line 626753
    :cond_0
    move v3, v5

    .line 626754
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    .line 626755
    aget-object v4, v2, v0

    invoke-virtual {v1, v4}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 626756
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 626757
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/user/model/Name;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 626758
    invoke-virtual {p1}, Lcom/facebook/user/model/Name;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 626759
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/user/model/Name;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 626760
    invoke-virtual {p1}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 626761
    :cond_3
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    return-object v0

    .line 626762
    :cond_4
    new-instance v7, LX/3hR;

    invoke-direct {v7, v3}, LX/3hR;-><init>(Ljava/lang/String;)V

    .line 626763
    iget v4, v7, LX/3hR;->d:I

    iget v6, v7, LX/3hR;->e:I

    if-eq v4, v6, :cond_0

    .line 626764
    iget-object v4, v7, LX/3hR;->a:[Ljava/lang/String;

    iget v6, v7, LX/3hR;->d:I

    aget-object v4, v4, v6

    .line 626765
    iget-object v6, v0, LX/3Lt;->d:Ljava/util/HashSet;

    iget-object p0, v0, LX/3Lt;->i:Ljava/util/Locale;

    invoke-virtual {v4, p0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 626766
    iget v4, v7, LX/3hR;->d:I

    add-int/lit8 v6, v4, 0x1

    iput v6, v7, LX/3hR;->d:I

    .line 626767
    :cond_5
    iget v4, v7, LX/3hR;->d:I

    :goto_1
    iget v6, v7, LX/3hR;->e:I

    if-ge v4, v6, :cond_0

    .line 626768
    add-int/lit8 v6, v5, 0x1

    iget-object p0, v7, LX/3hR;->a:[Ljava/lang/String;

    aget-object p0, p0, v4

    aput-object p0, v2, v5

    .line 626769
    add-int/lit8 v4, v4, 0x1

    move v5, v6

    goto :goto_1
.end method

.method public static b(LX/3hQ;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 626794
    iget-object v0, p0, LX/3hQ;->c:LX/2Rd;

    invoke-virtual {v0, p1}, LX/2Rd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/3hQ;I)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 626741
    iget-object v1, p0, LX/3hQ;->g:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 626742
    :goto_0
    if-ge v0, p1, :cond_0

    .line 626743
    iget-object v1, p0, LX/3hQ;->g:Ljava/lang/StringBuilder;

    iget-object v2, p0, LX/3hQ;->h:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 626744
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 626745
    :cond_0
    iget-object v0, p0, LX/3hQ;->f:Ljava/util/Set;

    iget-object v1, p0, LX/3hQ;->g:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 626746
    return-void
.end method

.method public static b(LX/3hQ;Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 626728
    iget-object v0, p0, LX/3hQ;->d:LX/3Lp;

    .line 626729
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 626730
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 626731
    invoke-static {v0, v3}, LX/3Lp;->a(LX/3Lp;I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 626732
    sget-object v4, LX/3Lp;->b:Ljava/lang/String;

    iget-object p2, v0, LX/3Lp;->g:Ljava/lang/String;

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 626733
    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 626734
    :cond_0
    invoke-static {v0, v2}, LX/3Lp;->b(LX/3Lp;Ljava/lang/Integer;)LX/3Lq;

    move-result-object v2

    move-object v1, v2

    .line 626735
    invoke-virtual {v1, p1}, LX/3Lq;->b(Ljava/lang/String;)Ljava/util/Iterator;

    move-result-object v1

    move-object v1, v1

    .line 626736
    if-eqz v1, :cond_1

    .line 626737
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 626738
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 626739
    iget-object v2, p0, LX/3hQ;->f:Ljava/util/Set;

    invoke-static {p0, v0}, LX/3hQ;->b(LX/3hQ;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 626740
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 626724
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 626725
    iget-object v3, p0, LX/3hQ;->f:Ljava/util/Set;

    invoke-static {p0, v0}, LX/3hQ;->b(LX/3hQ;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 626726
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 626727
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/user/model/Name;)V
    .locals 11
    .param p1    # Lcom/facebook/user/model/Name;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 626682
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 626683
    :cond_0
    :goto_0
    return-void

    .line 626684
    :cond_1
    invoke-direct {p0, p1}, LX/3hQ;->b(Lcom/facebook/user/model/Name;)LX/0Rf;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0Rf;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 626685
    invoke-virtual {p1}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, LX/3hQ;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3Lt;

    invoke-virtual {v1, v3}, LX/3Lt;->a(I)I

    move-result v1

    const/4 v5, 0x1

    const/4 v6, 0x4

    const/4 v4, 0x0

    .line 626686
    array-length v7, v0

    .line 626687
    iput-object v0, p0, LX/3hQ;->h:[Ljava/lang/String;

    move v3, v4

    .line 626688
    :goto_1
    iget-object v8, p0, LX/3hQ;->h:[Ljava/lang/String;

    array-length v8, v8

    if-ge v3, v8, :cond_2

    .line 626689
    iget-object v8, p0, LX/3hQ;->h:[Ljava/lang/String;

    iget-object v9, p0, LX/3hQ;->h:[Ljava/lang/String;

    aget-object v9, v9, v3

    invoke-static {p0, v9}, LX/3hQ;->b(LX/3hQ;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v3

    .line 626690
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 626691
    :cond_2
    if-le v7, v6, :cond_3

    move v8, v5

    .line 626692
    :goto_2
    if-eqz v8, :cond_c

    .line 626693
    invoke-static {p0, v7}, LX/3hQ;->a(LX/3hQ;I)V

    .line 626694
    iget-object v3, p0, LX/3hQ;->h:[Ljava/lang/String;

    new-instance v9, LX/7Ht;

    invoke-direct {v9, p0}, LX/7Ht;-><init>(LX/3hQ;)V

    invoke-static {v3, v4, v7, v9}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 626695
    iget-object v3, p0, LX/3hQ;->h:[Ljava/lang/String;

    aget-object v9, v3, v4

    move v3, v6

    .line 626696
    :goto_3
    if-ge v3, v7, :cond_4

    .line 626697
    iget-object v10, p0, LX/3hQ;->h:[Ljava/lang/String;

    iget-object p1, p0, LX/3hQ;->h:[Ljava/lang/String;

    aget-object p1, p1, v3

    aput-object p1, v10, v4

    .line 626698
    invoke-static {p0, v6}, LX/3hQ;->b(LX/3hQ;I)V

    .line 626699
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_3
    move v8, v4

    .line 626700
    goto :goto_2

    .line 626701
    :cond_4
    iget-object v3, p0, LX/3hQ;->h:[Ljava/lang/String;

    aput-object v9, v3, v4

    .line 626702
    :goto_4
    if-nez v8, :cond_b

    move v3, v5

    :goto_5
    invoke-static {p0, v4, v6, v3}, LX/3hQ;->a(LX/3hQ;IIZ)V

    .line 626703
    invoke-static {p0, v2, v1}, LX/3hQ;->b(LX/3hQ;Ljava/lang/String;I)V

    .line 626704
    const/4 v3, 0x5

    if-ne v1, v3, :cond_a

    .line 626705
    const/16 v1, 0x3131

    const v9, 0xac00

    const/4 v3, 0x0

    .line 626706
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    .line 626707
    iget-object v4, p0, LX/3hQ;->g:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    move v4, v3

    .line 626708
    :goto_6
    add-int/lit8 v5, v4, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/String;->codePointAt(I)I

    move-result v4

    .line 626709
    const/16 v7, 0x20

    if-eq v4, v7, :cond_8

    .line 626710
    const/16 v7, 0x1100

    if-lt v4, v7, :cond_9

    const/16 v7, 0x1112

    if-le v4, v7, :cond_5

    if-lt v4, v1, :cond_9

    :cond_5
    const/16 v7, 0x314e

    if-le v4, v7, :cond_6

    if-lt v4, v9, :cond_9

    :cond_6
    const v7, 0xd7a3

    if-gt v4, v7, :cond_9

    .line 626711
    if-lt v4, v9, :cond_d

    .line 626712
    sub-int/2addr v4, v9

    div-int/lit16 v4, v4, 0x24c

    add-int/lit16 v4, v4, 0x1100

    .line 626713
    :cond_7
    :goto_7
    iget-object v7, p0, LX/3hQ;->g:Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    .line 626714
    add-int/lit8 v3, v3, 0x1

    .line 626715
    :cond_8
    if-lt v5, v6, :cond_e

    .line 626716
    :cond_9
    const/4 v4, 0x1

    if-le v3, v4, :cond_a

    .line 626717
    iget-object v3, p0, LX/3hQ;->f:Ljava/util/Set;

    iget-object v4, p0, LX/3hQ;->g:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, LX/3hQ;->b(LX/3hQ;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 626718
    :cond_a
    goto/16 :goto_0

    :cond_b
    move v3, v4

    .line 626719
    goto :goto_5

    :cond_c
    move v6, v7

    goto :goto_4

    .line 626720
    :cond_d
    if-lt v4, v1, :cond_7

    .line 626721
    add-int/lit16 v7, v4, -0x3131

    const/16 v8, 0x1e

    if-ge v7, v8, :cond_9

    .line 626722
    sget-object v7, LX/3hQ;->b:[I

    add-int/lit16 v4, v4, -0x3131

    aget v4, v7, v4

    .line 626723
    if-eqz v4, :cond_9

    goto :goto_7

    :cond_e
    move v4, v5

    goto :goto_6
.end method
