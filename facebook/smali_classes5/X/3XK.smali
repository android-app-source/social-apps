.class public LX/3XK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 592785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 592786
    return-void
.end method

.method public static a(LX/0QB;)LX/3XK;
    .locals 3

    .prologue
    .line 592787
    const-class v1, LX/3XK;

    monitor-enter v1

    .line 592788
    :try_start_0
    sget-object v0, LX/3XK;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 592789
    sput-object v2, LX/3XK;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 592790
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592791
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 592792
    new-instance v0, LX/3XK;

    invoke-direct {v0}, LX/3XK;-><init>()V

    .line 592793
    move-object v0, v0

    .line 592794
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 592795
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3XK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 592796
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 592797
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 4

    .prologue
    .line 592798
    sget-object v0, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;->a:LX/1Cz;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 592799
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cz;

    .line 592800
    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 592801
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 592802
    :cond_0
    return-void
.end method
