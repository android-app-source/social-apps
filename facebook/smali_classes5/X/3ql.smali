.class public final LX/3ql;
.super LX/3jt;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 642448
    invoke-direct {p0}, LX/3jt;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/drawable/Drawable;FF)V
    .locals 0

    .prologue
    .line 642449
    invoke-virtual {p1, p2, p3}, Landroid/graphics/drawable/Drawable;->setHotspot(FF)V

    .line 642450
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;I)V
    .locals 0

    .prologue
    .line 642451
    instance-of p0, p1, LX/3qt;

    if-eqz p0, :cond_0

    .line 642452
    invoke-static {p1, p2}, LX/3qm;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 642453
    :goto_0
    return-void

    .line 642454
    :cond_0
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    goto :goto_0
.end method

.method public final a(Landroid/graphics/drawable/Drawable;IIII)V
    .locals 0

    .prologue
    .line 642455
    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/graphics/drawable/Drawable;->setHotspotBounds(IIII)V

    .line 642456
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
    .locals 0

    .prologue
    .line 642457
    instance-of p0, p1, LX/3qt;

    if-eqz p0, :cond_0

    .line 642458
    invoke-static {p1, p2}, LX/3qm;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 642459
    :goto_0
    return-void

    .line 642460
    :cond_0
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->setTintList(Landroid/content/res/ColorStateList;)V

    goto :goto_0
.end method

.method public final a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V
    .locals 0

    .prologue
    .line 642461
    instance-of p0, p1, Landroid/graphics/drawable/GradientDrawable;

    if-eqz p0, :cond_0

    .line 642462
    invoke-static {p1, p2}, LX/3qm;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V

    .line 642463
    :goto_0
    return-void

    .line 642464
    :cond_0
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->setTintMode(Landroid/graphics/PorterDuff$Mode;)V

    goto :goto_0
.end method

.method public final c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 642465
    instance-of v0, p1, Landroid/graphics/drawable/GradientDrawable;

    if-eqz v0, :cond_0

    .line 642466
    new-instance v0, LX/3qt;

    invoke-direct {v0, p1}, LX/3qt;-><init>(Landroid/graphics/drawable/Drawable;)V

    move-object p1, v0

    .line 642467
    :cond_0
    move-object v0, p1

    .line 642468
    return-object v0
.end method
