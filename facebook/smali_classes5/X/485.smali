.class public LX/485;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/compactdisk/DiskCacheEventListener;


# instance fields
.field private final a:LX/1GE;


# direct methods
.method public constructor <init>(LX/1GE;)V
    .locals 0

    .prologue
    .line 672676
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 672677
    iput-object p1, p0, LX/485;->a:LX/1GE;

    .line 672678
    return-void
.end method


# virtual methods
.method public final onEviction(Lcom/facebook/compactdisk/DiskCacheEvent;)V
    .locals 2

    .prologue
    .line 672691
    iget-object v0, p0, LX/485;->a:LX/1GE;

    new-instance v1, LX/484;

    invoke-direct {v1, p1}, LX/484;-><init>(Lcom/facebook/compactdisk/DiskCacheEvent;)V

    invoke-interface {v0, v1}, LX/1GE;->g(LX/1gC;)V

    .line 672692
    return-void
.end method

.method public final onHit(Lcom/facebook/compactdisk/DiskCacheEvent;)V
    .locals 2

    .prologue
    .line 672689
    iget-object v0, p0, LX/485;->a:LX/1GE;

    new-instance v1, LX/484;

    invoke-direct {v1, p1}, LX/484;-><init>(Lcom/facebook/compactdisk/DiskCacheEvent;)V

    invoke-interface {v0, v1}, LX/1GE;->a(LX/1gC;)V

    .line 672690
    return-void
.end method

.method public final onMiss(Lcom/facebook/compactdisk/DiskCacheEvent;)V
    .locals 2

    .prologue
    .line 672687
    iget-object v0, p0, LX/485;->a:LX/1GE;

    new-instance v1, LX/484;

    invoke-direct {v1, p1}, LX/484;-><init>(Lcom/facebook/compactdisk/DiskCacheEvent;)V

    invoke-interface {v0, v1}, LX/1GE;->b(LX/1gC;)V

    .line 672688
    return-void
.end method

.method public final onReadException(Lcom/facebook/compactdisk/DiskCacheEvent;)V
    .locals 2

    .prologue
    .line 672685
    iget-object v0, p0, LX/485;->a:LX/1GE;

    new-instance v1, LX/484;

    invoke-direct {v1, p1}, LX/484;-><init>(Lcom/facebook/compactdisk/DiskCacheEvent;)V

    invoke-interface {v0, v1}, LX/1GE;->e(LX/1gC;)V

    .line 672686
    return-void
.end method

.method public final onWrite(Lcom/facebook/compactdisk/DiskCacheEvent;)V
    .locals 2

    .prologue
    .line 672683
    iget-object v0, p0, LX/485;->a:LX/1GE;

    new-instance v1, LX/484;

    invoke-direct {v1, p1}, LX/484;-><init>(Lcom/facebook/compactdisk/DiskCacheEvent;)V

    invoke-interface {v0, v1}, LX/1GE;->d(LX/1gC;)V

    .line 672684
    return-void
.end method

.method public final onWriteException(Lcom/facebook/compactdisk/DiskCacheEvent;)V
    .locals 2

    .prologue
    .line 672679
    new-instance v0, LX/484;

    invoke-direct {v0, p1}, LX/484;-><init>(Lcom/facebook/compactdisk/DiskCacheEvent;)V

    .line 672680
    iget-object v1, p0, LX/485;->a:LX/1GE;

    invoke-interface {v1, v0}, LX/1GE;->c(LX/1gC;)V

    .line 672681
    iget-object v1, p0, LX/485;->a:LX/1GE;

    invoke-interface {v1, v0}, LX/1GE;->f(LX/1gC;)V

    .line 672682
    return-void
.end method
