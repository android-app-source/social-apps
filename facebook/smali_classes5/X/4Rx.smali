.class public LX/4Rx;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 711076
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    .line 711077
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 711078
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 711079
    :goto_0
    return v6

    .line 711080
    :cond_0
    const-string v9, "value"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 711081
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v1

    .line 711082
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_3

    .line 711083
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 711084
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 711085
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 711086
    const-string v9, "unit"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 711087
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 711088
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 711089
    :cond_3
    const/4 v8, 0x2

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 711090
    invoke-virtual {p1, v6, v7}, LX/186;->b(II)V

    .line 711091
    if-eqz v0, :cond_4

    move-object v0, p1

    .line 711092
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 711093
    :cond_4
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_0

    :cond_5
    move v0, v6

    move-wide v2, v4

    move v7, v6

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 711094
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 711095
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 711096
    if-eqz v0, :cond_0

    .line 711097
    const-string v1, "unit"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711098
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 711099
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 711100
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_1

    .line 711101
    const-string v2, "value"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711102
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 711103
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 711104
    return-void
.end method
