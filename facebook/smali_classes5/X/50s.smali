.class public final LX/50s;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/util/Map$Entry",
        "<TC;TV;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/Iterator;

.field public final synthetic b:LX/50t;


# direct methods
.method public constructor <init>(LX/50t;Ljava/util/Iterator;)V
    .locals 0

    .prologue
    .line 824134
    iput-object p1, p0, LX/50s;->b:LX/50t;

    iput-object p2, p0, LX/50s;->a:Ljava/util/Iterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 824135
    iget-object v0, p0, LX/50s;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 824136
    iget-object v0, p0, LX/50s;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 824137
    new-instance v1, LX/50r;

    invoke-direct {v1, p0, v0}, LX/50r;-><init>(LX/50s;Ljava/util/Map$Entry;)V

    return-object v1
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 824138
    iget-object v0, p0, LX/50s;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 824139
    iget-object v0, p0, LX/50s;->b:LX/50t;

    invoke-virtual {v0}, LX/50t;->a()V

    .line 824140
    return-void
.end method
