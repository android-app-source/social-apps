.class public LX/3gU;
.super LX/3gB;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/3gU;


# instance fields
.field public a:J
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final b:LX/3gV;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/3gV;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 624492
    invoke-direct {p0}, LX/3gB;-><init>()V

    .line 624493
    const-wide/16 v0, 0x18

    iput-wide v0, p0, LX/3gU;->a:J

    .line 624494
    iput-object p1, p0, LX/3gU;->b:LX/3gV;

    .line 624495
    iput-object p2, p0, LX/3gU;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 624496
    return-void
.end method

.method public static a(LX/0QB;)LX/3gU;
    .locals 5

    .prologue
    .line 624498
    sget-object v0, LX/3gU;->d:LX/3gU;

    if-nez v0, :cond_1

    .line 624499
    const-class v1, LX/3gU;

    monitor-enter v1

    .line 624500
    :try_start_0
    sget-object v0, LX/3gU;->d:LX/3gU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 624501
    if-eqz v2, :cond_0

    .line 624502
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 624503
    new-instance p0, LX/3gU;

    invoke-static {v0}, LX/3gV;->a(LX/0QB;)LX/3gV;

    move-result-object v3

    check-cast v3, LX/3gV;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3, v4}, LX/3gU;-><init>(LX/3gV;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 624504
    move-object v0, p0

    .line 624505
    sput-object v0, LX/3gU;->d:LX/3gU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 624506
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 624507
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 624508
    :cond_1
    sget-object v0, LX/3gU;->d:LX/3gU;

    return-object v0

    .line 624509
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 624510
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()LX/2ZE;
    .locals 2

    .prologue
    .line 624511
    new-instance v0, LX/3gW;

    invoke-direct {v0, p0}, LX/3gW;-><init>(LX/3gU;)V

    return-object v0
.end method

.method public final dr_()J
    .locals 4

    .prologue
    .line 624497
    iget-wide v0, p0, LX/3gU;->a:J

    const-wide/32 v2, 0x36ee80

    mul-long/2addr v0, v2

    return-wide v0
.end method
