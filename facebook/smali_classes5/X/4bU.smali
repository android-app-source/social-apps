.class public LX/4bU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1hk;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:Lcom/facebook/http/interfaces/RequestPriority;

.field private static volatile o:LX/4bU;


# instance fields
.field public final c:LX/4bW;

.field public final d:LX/1hl;

.field private final e:LX/0UR;

.field private final f:LX/0Xl;

.field public final g:LX/0oz;

.field public final h:LX/0ad;

.field public final i:Lcom/facebook/http/common/FbHttpRequestProcessorLogger;

.field public final j:LX/4b8;

.field private volatile k:LX/0TU;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private volatile l:LX/0Yb;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/0p3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 793840
    const-class v0, LX/4bU;

    sput-object v0, LX/4bU;->a:Ljava/lang/Class;

    .line 793841
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    sput-object v0, LX/4bU;->b:Lcom/facebook/http/interfaces/RequestPriority;

    return-void
.end method

.method public constructor <init>(LX/4bX;LX/1hl;LX/0UR;Lcom/facebook/http/common/FbHttpRequestProcessorLogger;LX/4b8;LX/0Xl;LX/0oz;LX/0ad;)V
    .locals 7
    .param p6    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 793842
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 793843
    const/4 v0, 0x0

    iput-object v0, p0, LX/4bU;->m:Ljava/lang/String;

    .line 793844
    sget-object v0, LX/0p3;->UNKNOWN:LX/0p3;

    iput-object v0, p0, LX/4bU;->n:LX/0p3;

    .line 793845
    sget-char v0, LX/0by;->M:C

    const-string v1, "5:5:10:15"

    invoke-interface {p8, v0, v1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "5:5:10:15"

    invoke-static {v0, v1}, LX/4cA;->a(Ljava/lang/String;Ljava/lang/String;)LX/4c8;

    move-result-object v0

    move-object v0, v0

    .line 793846
    new-instance v1, LX/4bW;

    invoke-static {p1}, LX/4bP;->a(LX/0QB;)LX/4bP;

    move-result-object v2

    check-cast v2, LX/4bP;

    invoke-static {p1}, LX/4c7;->a(LX/0QB;)LX/4c7;

    move-result-object v3

    check-cast v3, LX/4c7;

    invoke-static {p1}, Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;->a(LX/0QB;)Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;

    move-result-object v4

    check-cast v4, Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;

    invoke-static {p1}, LX/4c6;->a(LX/0QB;)LX/4c6;

    move-result-object v6

    check-cast v6, LX/4c6;

    move-object v5, v0

    invoke-direct/range {v1 .. v6}, LX/4bW;-><init>(LX/4bP;LX/4c7;Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;LX/4c8;LX/4c6;)V

    .line 793847
    move-object v0, v1

    .line 793848
    iput-object v0, p0, LX/4bU;->c:LX/4bW;

    .line 793849
    iput-object p2, p0, LX/4bU;->d:LX/1hl;

    .line 793850
    iput-object p3, p0, LX/4bU;->e:LX/0UR;

    .line 793851
    iput-object p4, p0, LX/4bU;->i:Lcom/facebook/http/common/FbHttpRequestProcessorLogger;

    .line 793852
    iput-object p5, p0, LX/4bU;->j:LX/4b8;

    .line 793853
    iput-object p6, p0, LX/4bU;->f:LX/0Xl;

    .line 793854
    iput-object p7, p0, LX/4bU;->g:LX/0oz;

    .line 793855
    iput-object p8, p0, LX/4bU;->h:LX/0ad;

    .line 793856
    return-void
.end method

.method public static a(LX/0QB;)LX/4bU;
    .locals 12

    .prologue
    .line 793872
    sget-object v0, LX/4bU;->o:LX/4bU;

    if-nez v0, :cond_1

    .line 793873
    const-class v1, LX/4bU;

    monitor-enter v1

    .line 793874
    :try_start_0
    sget-object v0, LX/4bU;->o:LX/4bU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 793875
    if-eqz v2, :cond_0

    .line 793876
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 793877
    new-instance v3, LX/4bU;

    const-class v4, LX/4bX;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/4bX;

    invoke-static {v0}, LX/1hl;->a(LX/0QB;)LX/1hl;

    move-result-object v5

    check-cast v5, LX/1hl;

    invoke-static {v0}, LX/0UR;->a(LX/0QB;)LX/0UR;

    move-result-object v6

    check-cast v6, LX/0UR;

    invoke-static {v0}, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessorLogger;

    move-result-object v7

    check-cast v7, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;

    invoke-static {v0}, LX/4b8;->a(LX/0QB;)LX/4b8;

    move-result-object v8

    check-cast v8, LX/4b8;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v9

    check-cast v9, LX/0Xl;

    invoke-static {v0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v10

    check-cast v10, LX/0oz;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-direct/range {v3 .. v11}, LX/4bU;-><init>(LX/4bX;LX/1hl;LX/0UR;Lcom/facebook/http/common/FbHttpRequestProcessorLogger;LX/4b8;LX/0Xl;LX/0oz;LX/0ad;)V

    .line 793878
    move-object v0, v3

    .line 793879
    sput-object v0, LX/4bU;->o:LX/4bU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 793880
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 793881
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 793882
    :cond_1
    sget-object v0, LX/4bU;->o:LX/4bU;

    return-object v0

    .line 793883
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 793884
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static g(LX/4bU;)LX/0TU;
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InvalidAccessToGuardedField"
        }
    .end annotation

    .prologue
    .line 793857
    iget-object v0, p0, LX/4bU;->k:LX/0TU;

    if-nez v0, :cond_1

    .line 793858
    monitor-enter p0

    .line 793859
    :try_start_0
    iget-object v0, p0, LX/4bU;->k:LX/0TU;

    if-nez v0, :cond_0

    .line 793860
    iget-object v0, p0, LX/4bU;->e:LX/0UR;

    const-string v1, "NetDisp"

    const/16 v2, 0x14

    const/16 v3, 0xc8

    const/16 v4, 0xa

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0UR;->a(Ljava/lang/String;III)LX/0TU;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0TU;

    iput-object v0, p0, LX/4bU;->k:LX/0TU;

    .line 793861
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 793862
    :cond_1
    iget-object v0, p0, LX/4bU;->k:LX/0TU;

    return-object v0

    .line 793863
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private h()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InvalidAccessToGuardedField"
        }
    .end annotation

    .prologue
    .line 793864
    iget-object v0, p0, LX/4bU;->l:LX/0Yb;

    if-nez v0, :cond_1

    .line 793865
    monitor-enter p0

    .line 793866
    :try_start_0
    iget-object v0, p0, LX/4bU;->l:LX/0Yb;

    if-nez v0, :cond_0

    .line 793867
    iget-object v0, p0, LX/4bU;->f:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    sget-object v1, LX/0oz;->a:Ljava/lang/String;

    new-instance v2, LX/4bQ;

    invoke-direct {v2, p0}, LX/4bQ;-><init>(LX/4bU;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/4bU;->l:LX/0Yb;

    .line 793868
    iget-object v0, p0, LX/4bU;->l:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 793869
    :cond_0
    monitor-exit p0

    .line 793870
    :cond_1
    return-void

    .line 793871
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(LX/15D;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/15D",
            "<TT;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 793825
    invoke-direct {p0}, LX/4bU;->h()V

    .line 793826
    new-instance v0, LX/4bV;

    invoke-direct {v0, p1}, LX/4bV;-><init>(LX/15D;)V

    .line 793827
    iget-object v1, p0, LX/4bU;->j:LX/4b8;

    iget-object v2, v0, LX/4bV;->a:Lcom/google/common/util/concurrent/SettableFuture;

    .line 793828
    invoke-static {v1}, LX/4b8;->b(LX/4b8;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 793829
    :goto_0
    iget-object v1, p0, LX/4bU;->c:LX/4bW;

    invoke-virtual {v1, v0}, LX/4bW;->a(LX/4bV;)V

    .line 793830
    invoke-static {p0}, LX/4bU;->g(LX/4bU;)LX/0TU;

    move-result-object v1

    invoke-interface {v1}, LX/0TU;->b()I

    move-result v1

    iget-object v2, p0, LX/4bU;->c:LX/4bW;

    invoke-virtual {v2}, LX/4bW;->a()LX/4c8;

    move-result-object v2

    iget v2, v2, LX/4c8;->d:I

    if-ge v1, v2, :cond_0

    .line 793831
    new-instance v1, Lcom/facebook/http/common/PriorityRequestEngine$ExecuteRequestsRunnable;

    invoke-direct {v1, p0}, Lcom/facebook/http/common/PriorityRequestEngine$ExecuteRequestsRunnable;-><init>(LX/4bU;)V

    .line 793832
    invoke-static {p0}, LX/4bU;->g(LX/4bU;)LX/0TU;

    move-result-object v2

    const v3, -0x5adc0a28

    invoke-static {v2, v1, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 793833
    :cond_0
    iget-object v0, v0, LX/4bV;->a:Lcom/google/common/util/concurrent/SettableFuture;

    return-object v0

    .line 793834
    :cond_1
    sget-object v3, LX/4b7;->ARRIVE:LX/4b7;

    invoke-static {v1, v3, p1}, LX/4b8;->a(LX/4b8;LX/4b7;LX/15D;)V

    .line 793835
    new-instance v3, LX/4b5;

    invoke-direct {v3, v1, p1}, LX/4b5;-><init>(LX/4b8;LX/15D;)V

    .line 793836
    sget-object v4, LX/131;->INSTANCE:LX/131;

    move-object v4, v4

    .line 793837
    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 793838
    iget-object v0, p0, LX/4bU;->c:LX/4bW;

    new-instance v1, LX/4bS;

    invoke-direct {v1, p0}, LX/4bS;-><init>(LX/4bU;)V

    invoke-virtual {v0, v1}, LX/4bW;->a(LX/4bR;)V

    .line 793839
    return-void
.end method

.method public final a(LX/15D;Lcom/facebook/http/interfaces/RequestPriority;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15D",
            "<*>;",
            "Lcom/facebook/http/interfaces/RequestPriority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 793785
    iget-object v0, p0, LX/4bU;->j:LX/4b8;

    invoke-virtual {v0, p1, p2}, LX/4b8;->a(LX/15D;Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 793786
    iget-object v0, p0, LX/4bU;->c:LX/4bW;

    invoke-virtual {v0, p1}, LX/4bW;->a(LX/15D;)LX/4bV;

    move-result-object v0

    .line 793787
    if-eqz v0, :cond_0

    .line 793788
    iget-object v1, v0, LX/4bV;->c:LX/15D;

    .line 793789
    iget-object p1, v1, LX/15D;->i:LX/15F;

    move-object v1, p1

    .line 793790
    invoke-virtual {v1, p2}, LX/15F;->b(Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 793791
    iget-object v1, p0, LX/4bU;->c:LX/4bW;

    .line 793792
    new-instance p0, LX/4bV;

    invoke-direct {p0, v0, p2}, LX/4bV;-><init>(LX/4bV;Lcom/facebook/http/interfaces/RequestPriority;)V

    move-object v0, p0

    .line 793793
    invoke-virtual {v1, v0}, LX/4bW;->a(LX/4bV;)V

    .line 793794
    :goto_0
    return-void

    .line 793795
    :cond_0
    invoke-virtual {p1}, LX/15D;->h()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v0

    if-ne v0, p2, :cond_1

    .line 793796
    iget-object v0, p0, LX/4bU;->j:LX/4b8;

    .line 793797
    sget-object v1, LX/4b7;->NO_CHANGE:LX/4b7;

    invoke-static {v0, v1, p1}, LX/4b8;->a(LX/4b8;LX/4b7;LX/15D;)V

    .line 793798
    goto :goto_0

    .line 793799
    :cond_1
    iget-object v0, p1, LX/15D;->i:LX/15F;

    move-object v0, v0

    .line 793800
    invoke-virtual {v0, p2}, LX/15F;->b(Lcom/facebook/http/interfaces/RequestPriority;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 793781
    iput-object p2, p0, LX/4bU;->m:Ljava/lang/String;

    .line 793782
    iget-object v0, p0, LX/4bU;->c:LX/4bW;

    new-instance v1, LX/4bT;

    invoke-direct {v1, p0, p1, p2}, LX/4bT;-><init>(LX/4bU;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/4bW;->a(LX/4bR;)V

    .line 793783
    return-void
.end method

.method public final b()LX/2Ee;
    .locals 1

    .prologue
    .line 793784
    iget-object v0, p0, LX/4bU;->c:LX/4bW;

    invoke-virtual {v0}, LX/4bW;->c()LX/2Ee;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15D;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15D",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 793816
    iget-object v0, p0, LX/4bU;->j:LX/4b8;

    .line 793817
    sget-object v1, LX/4b7;->CANCEL:LX/4b7;

    invoke-static {v0, v1, p1}, LX/4b8;->a(LX/4b8;LX/4b7;LX/15D;)V

    .line 793818
    iget-object v0, p0, LX/4bU;->c:LX/4bW;

    invoke-virtual {v0, p1}, LX/4bW;->a(LX/15D;)LX/4bV;

    move-result-object v0

    .line 793819
    if-eqz v0, :cond_0

    .line 793820
    iget-object v0, v0, LX/4bV;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0SQ;->cancel(Z)Z

    .line 793821
    :goto_0
    return v2

    .line 793822
    :cond_0
    iget-object v0, p0, LX/4bU;->j:LX/4b8;

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->UNNECESSARY:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, p1, v1}, LX/4b8;->a(LX/15D;Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 793823
    iget-object v0, p1, LX/15D;->i:LX/15F;

    move-object v0, v0

    .line 793824
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->UNNECESSARY:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/15F;->b(Lcom/facebook/http/interfaces/RequestPriority;)V

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 793801
    iget-object v1, p0, LX/4bU;->c:LX/4bW;

    invoke-virtual {v1}, LX/4bW;->c()LX/2Ee;

    move-result-object v3

    .line 793802
    iget-object v1, v3, LX/2Ee;->a:Ljava/util/ArrayList;

    move-object v4, v1

    .line 793803
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v5, :cond_0

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15D;

    .line 793804
    invoke-virtual {v0}, LX/15D;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 793805
    add-int/lit8 v0, v1, 0x1

    .line 793806
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 793807
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 793808
    sget-object v2, LX/4bU;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ": in-flight("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 793809
    iget-object v4, v3, LX/2Ee;->a:Ljava/util/ArrayList;

    move-object v4, v4

    .line 793810
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "), queued("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 793811
    iget-object v4, v3, LX/2Ee;->b:Ljava/util/ArrayList;

    move-object v3, v4

    .line 793812
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "), in-flight sublimited("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "), running threads("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, LX/4bU;->g(LX/4bU;)LX/0TU;

    move-result-object v2

    invoke-interface {v2}, LX/0TU;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "), waiting runnables("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, LX/4bU;->g(LX/4bU;)LX/0TU;

    move-result-object v2

    invoke-interface {v2}, LX/0TU;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793813
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v1

    goto/16 :goto_1
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 793814
    sget-object v0, LX/4bU;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 793815
    iget-object v0, p0, LX/4bU;->d:LX/1hl;

    invoke-virtual {v0}, LX/1hl;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
