.class public final LX/3oa;
.super LX/3nx;
.source ""


# instance fields
.field public final synthetic a:Landroid/support/design/widget/SwipeDismissBehavior;

.field public b:I


# direct methods
.method public constructor <init>(Landroid/support/design/widget/SwipeDismissBehavior;)V
    .locals 0

    .prologue
    .line 640460
    iput-object p1, p0, LX/3oa;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    invoke-direct {p0}, LX/3nx;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 640457
    iget-object v0, p0, LX/3oa;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    iget-object v0, v0, Landroid/support/design/widget/SwipeDismissBehavior;->b:LX/3oK;

    if-eqz v0, :cond_0

    .line 640458
    iget-object v0, p0, LX/3oa;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    iget-object v0, v0, Landroid/support/design/widget/SwipeDismissBehavior;->b:LX/3oK;

    invoke-interface {v0, p1}, LX/3oK;->a(I)V

    .line 640459
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;FF)V
    .locals 7

    .prologue
    .line 640431
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 640432
    const/4 v1, 0x0

    .line 640433
    const/4 p3, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 640434
    cmpl-float v2, p2, p3

    if-eqz v2, :cond_a

    .line 640435
    invoke-static {p1}, LX/0vv;->h(Landroid/view/View;)I

    move-result v2

    if-ne v2, v3, :cond_5

    move v2, v3

    .line 640436
    :goto_0
    iget-object v5, p0, LX/3oa;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    iget v5, v5, Landroid/support/design/widget/SwipeDismissBehavior;->f:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_6

    .line 640437
    :cond_0
    :goto_1
    move v2, v3

    .line 640438
    if-eqz v2, :cond_3

    .line 640439
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget v2, p0, LX/3oa;->b:I

    if-ge v1, v2, :cond_2

    iget v1, p0, LX/3oa;->b:I

    sub-int v0, v1, v0

    .line 640440
    :goto_2
    const/4 v1, 0x1

    .line 640441
    :goto_3
    iget-object v2, p0, LX/3oa;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    iget-object v2, v2, Landroid/support/design/widget/SwipeDismissBehavior;->a:LX/3ty;

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v2, v0, v3}, LX/3ty;->a(II)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 640442
    new-instance v0, Landroid/support/design/widget/SwipeDismissBehavior$SettleRunnable;

    iget-object v2, p0, LX/3oa;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    invoke-direct {v0, v2, p1, v1}, Landroid/support/design/widget/SwipeDismissBehavior$SettleRunnable;-><init>(Landroid/support/design/widget/SwipeDismissBehavior;Landroid/view/View;Z)V

    invoke-static {p1, v0}, LX/0vv;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 640443
    :cond_1
    :goto_4
    return-void

    .line 640444
    :cond_2
    iget v1, p0, LX/3oa;->b:I

    add-int/2addr v0, v1

    goto :goto_2

    .line 640445
    :cond_3
    iget v0, p0, LX/3oa;->b:I

    goto :goto_3

    .line 640446
    :cond_4
    if-eqz v1, :cond_1

    iget-object v0, p0, LX/3oa;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    iget-object v0, v0, Landroid/support/design/widget/SwipeDismissBehavior;->b:LX/3oK;

    if-eqz v0, :cond_1

    .line 640447
    iget-object v0, p0, LX/3oa;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    iget-object v0, v0, Landroid/support/design/widget/SwipeDismissBehavior;->b:LX/3oK;

    invoke-interface {v0}, LX/3oK;->a()V

    goto :goto_4

    :cond_5
    move v2, v4

    .line 640448
    goto :goto_0

    .line 640449
    :cond_6
    iget-object v5, p0, LX/3oa;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    iget v5, v5, Landroid/support/design/widget/SwipeDismissBehavior;->f:I

    if-nez v5, :cond_8

    .line 640450
    if-eqz v2, :cond_7

    cmpg-float v2, p2, p3

    if-ltz v2, :cond_0

    move v3, v4

    goto :goto_1

    :cond_7
    cmpl-float v2, p2, p3

    if-gtz v2, :cond_0

    move v3, v4

    goto :goto_1

    .line 640451
    :cond_8
    iget-object v5, p0, LX/3oa;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    iget v5, v5, Landroid/support/design/widget/SwipeDismissBehavior;->f:I

    if-ne v5, v3, :cond_b

    .line 640452
    if-eqz v2, :cond_9

    cmpl-float v2, p2, p3

    if-gtz v2, :cond_0

    move v3, v4

    goto :goto_1

    :cond_9
    cmpg-float v2, p2, p3

    if-ltz v2, :cond_0

    move v3, v4

    goto :goto_1

    .line 640453
    :cond_a
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    iget v5, p0, LX/3oa;->b:I

    sub-int/2addr v2, v5

    .line 640454
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    int-to-float v5, v5

    iget-object v6, p0, LX/3oa;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    iget v6, v6, Landroid/support/design/widget/SwipeDismissBehavior;->g:F

    mul-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 640455
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-ge v2, v5, :cond_0

    move v3, v4

    goto/16 :goto_1

    :cond_b
    move v3, v4

    .line 640456
    goto/16 :goto_1
.end method

.method public final a(Landroid/view/View;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 640421
    iget v0, p0, LX/3oa;->b:I

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, LX/3oa;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    iget v2, v2, Landroid/support/design/widget/SwipeDismissBehavior;->h:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 640422
    iget v1, p0, LX/3oa;->b:I

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, LX/3oa;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    iget v3, v3, Landroid/support/design/widget/SwipeDismissBehavior;->i:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    .line 640423
    int-to-float v2, p2

    cmpg-float v2, v2, v0

    if-gtz v2, :cond_0

    .line 640424
    invoke-static {p1, v4}, LX/0vv;->c(Landroid/view/View;F)V

    .line 640425
    :goto_0
    return-void

    .line 640426
    :cond_0
    int-to-float v2, p2

    cmpl-float v2, v2, v1

    if-ltz v2, :cond_1

    .line 640427
    invoke-static {p1, v5}, LX/0vv;->c(Landroid/view/View;F)V

    goto :goto_0

    .line 640428
    :cond_1
    int-to-float v2, p2

    .line 640429
    sub-float v3, v2, v0

    sub-float p0, v1, v0

    div-float/2addr v3, p0

    move v0, v3

    .line 640430
    sub-float v0, v4, v0

    invoke-static {v5, v0, v4}, Landroid/support/design/widget/SwipeDismissBehavior;->c(FFF)F

    move-result v0

    invoke-static {p1, v0}, LX/0vv;->c(Landroid/view/View;F)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 640420
    iget-object v0, p0, LX/3oa;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/SwipeDismissBehavior;->a(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/View;I)I
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 640402
    invoke-static {p1}, LX/0vv;->h(Landroid/view/View;)I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    .line 640403
    :goto_0
    iget-object v2, p0, LX/3oa;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    iget v2, v2, Landroid/support/design/widget/SwipeDismissBehavior;->f:I

    if-nez v2, :cond_2

    .line 640404
    if-eqz v0, :cond_1

    .line 640405
    iget v0, p0, LX/3oa;->b:I

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    sub-int v1, v0, v1

    .line 640406
    iget v0, p0, LX/3oa;->b:I

    .line 640407
    :goto_1
    invoke-static {v1, p2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    move v0, v2

    .line 640408
    return v0

    .line 640409
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 640410
    :cond_1
    iget v1, p0, LX/3oa;->b:I

    .line 640411
    iget v0, p0, LX/3oa;->b:I

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int/2addr v0, v2

    goto :goto_1

    .line 640412
    :cond_2
    iget-object v2, p0, LX/3oa;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    iget v2, v2, Landroid/support/design/widget/SwipeDismissBehavior;->f:I

    if-ne v2, v1, :cond_4

    .line 640413
    if-eqz v0, :cond_3

    .line 640414
    iget v1, p0, LX/3oa;->b:I

    .line 640415
    iget v0, p0, LX/3oa;->b:I

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int/2addr v0, v2

    goto :goto_1

    .line 640416
    :cond_3
    iget v0, p0, LX/3oa;->b:I

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    sub-int v1, v0, v1

    .line 640417
    iget v0, p0, LX/3oa;->b:I

    goto :goto_1

    .line 640418
    :cond_4
    iget v0, p0, LX/3oa;->b:I

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    sub-int v1, v0, v1

    .line 640419
    iget v0, p0, LX/3oa;->b:I

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int/2addr v0, v2

    goto :goto_1
.end method

.method public final b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 640398
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    iput v0, p0, LX/3oa;->b:I

    .line 640399
    return-void
.end method

.method public final c(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 640401
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    return v0
.end method

.method public final c(Landroid/view/View;I)I
    .locals 1

    .prologue
    .line 640400
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    return v0
.end method
