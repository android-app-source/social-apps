.class public final LX/4xN;
.super LX/4xM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/4xM",
        "<",
        "Ljava/lang/Comparable",
        "<*>;>;"
    }
.end annotation


# static fields
.field public static final a:LX/4xN;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 820972
    new-instance v0, LX/4xN;

    invoke-direct {v0}, LX/4xN;-><init>()V

    sput-object v0, LX/4xN;->a:LX/4xN;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 820970
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/4xM;-><init>(Ljava/lang/Comparable;)V

    .line 820971
    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 820969
    sget-object v0, LX/4xN;->a:LX/4xN;

    return-object v0
.end method


# virtual methods
.method public final a(LX/4xM;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4xM",
            "<",
            "Ljava/lang/Comparable",
            "<*>;>;)I"
        }
    .end annotation

    .prologue
    .line 820968
    if-ne p1, p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a()LX/4xG;
    .locals 2

    .prologue
    .line 820967
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "this statement should be unreachable"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public final a(Ljava/lang/StringBuilder;)V
    .locals 1

    .prologue
    .line 820959
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public final a(Ljava/lang/Comparable;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Comparable",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 820966
    const/4 v0, 0x0

    return v0
.end method

.method public final b()LX/4xG;
    .locals 1

    .prologue
    .line 820965
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public final b(Ljava/lang/StringBuilder;)V
    .locals 1

    .prologue
    .line 820963
    const-string v0, "+\u221e)"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 820964
    return-void
.end method

.method public final c()Ljava/lang/Comparable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Comparable",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 820962
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "range unbounded on this side"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 820961
    check-cast p1, LX/4xM;

    invoke-virtual {p0, p1}, LX/4xN;->a(LX/4xM;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 820960
    const-string v0, "+\u221e"

    return-object v0
.end method
