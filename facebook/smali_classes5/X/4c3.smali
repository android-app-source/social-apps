.class public LX/4c3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lorg/apache/http/protocol/HttpContext;

.field private b:Lorg/apache/http/HttpRequest;

.field public c:LX/1iW;

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1iZ;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/03V;

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1pD;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lorg/apache/http/HttpResponse;

.field private h:LX/4c2;

.field private i:LX/1iv;


# direct methods
.method public constructor <init>(Lorg/apache/http/protocol/HttpContext;Lorg/apache/http/HttpRequest;Ljava/util/Set;LX/03V;)V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/protocol/HttpContext;",
            "Lorg/apache/http/HttpRequest;",
            "Ljava/util/Set",
            "<",
            "LX/1iZ;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 794538
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 794539
    iput-object p1, p0, LX/4c3;->a:Lorg/apache/http/protocol/HttpContext;

    .line 794540
    iput-object p2, p0, LX/4c3;->b:Lorg/apache/http/HttpRequest;

    .line 794541
    iput-object p3, p0, LX/4c3;->d:Ljava/util/Set;

    .line 794542
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v1

    .line 794543
    iget-object v0, p0, LX/4c3;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1iZ;

    .line 794544
    instance-of v3, v0, LX/1pD;

    if-eqz v3, :cond_0

    .line 794545
    check-cast v0, LX/1pD;

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 794546
    :cond_1
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/4c3;->f:Ljava/util/Set;

    .line 794547
    iput-object p4, p0, LX/4c3;->e:LX/03V;

    .line 794548
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 2
    .param p1    # Ljava/io/InputStream;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 794485
    iget-object v0, p0, LX/4c3;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pD;

    .line 794486
    invoke-interface {v0}, LX/1pD;->a()Ljava/io/InputStream;

    move-result-object p1

    goto :goto_0

    .line 794487
    :cond_0
    return-object p1
.end method

.method public final a(Ljava/io/IOException;)V
    .locals 7

    .prologue
    .line 794488
    invoke-virtual {p0}, LX/4c3;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 794489
    :try_start_0
    iget-object v0, p0, LX/4c3;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1iZ;

    .line 794490
    sget-object v1, LX/1iv;->READ_RESPONSE_BODY:LX/1iv;

    iget-object v2, p0, LX/4c3;->b:Lorg/apache/http/HttpRequest;

    iget-object v3, p0, LX/4c3;->g:Lorg/apache/http/HttpResponse;

    iget-object v4, p0, LX/4c3;->a:Lorg/apache/http/protocol/HttpContext;

    move-object v5, p1

    invoke-interface/range {v0 .. v5}, LX/1iZ;->a(LX/1iv;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 794491
    :catchall_0
    move-exception v0

    sget-object v1, LX/4c2;->REPORTED_FAILURE:LX/4c2;

    iput-object v1, p0, LX/4c3;->h:LX/4c2;

    .line 794492
    sget-object v1, LX/1iv;->READ_RESPONSE_BODY:LX/1iv;

    iput-object v1, p0, LX/4c3;->i:LX/1iv;

    throw v0

    .line 794493
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 794494
    :cond_1
    sget-object v0, LX/4c2;->REPORTED_FAILURE:LX/4c2;

    iput-object v0, p0, LX/4c3;->h:LX/4c2;

    .line 794495
    sget-object v0, LX/1iv;->READ_RESPONSE_BODY:LX/1iv;

    iput-object v0, p0, LX/4c3;->i:LX/1iv;

    .line 794496
    return-void
.end method

.method public final a(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 5

    .prologue
    .line 794497
    iput-object p1, p0, LX/4c3;->g:Lorg/apache/http/HttpResponse;

    .line 794498
    iget-object v0, p0, LX/4c3;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1iZ;

    .line 794499
    invoke-interface {v0, p1, p2}, LX/1iZ;->a(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V

    goto :goto_0

    .line 794500
    :cond_0
    iget-object v0, p0, LX/4c3;->g:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 794501
    iget-object v0, p0, LX/4c3;->g:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->isRepeatable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 794502
    iget-object v0, p0, LX/4c3;->e:LX/03V;

    const-string v1, "HttpFlowState"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected isRepeatable for entity "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 794503
    iget-object v3, p0, LX/4c3;->a:Lorg/apache/http/protocol/HttpContext;

    invoke-static {v3}, LX/1iV;->a(Lorg/apache/http/protocol/HttpContext;)LX/1iV;

    move-result-object v3

    .line 794504
    iget-object v4, v3, LX/1iV;->f:Lcom/facebook/common/callercontext/CallerContext;

    move-object v4, v4

    .line 794505
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p2, p0, LX/4c3;->g:Lorg/apache/http/HttpResponse;

    invoke-interface {p2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    const-string p2, " for request "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    .line 794506
    iget-object p2, v3, LX/1iV;->a:Ljava/lang/String;

    move-object v3, p2

    .line 794507
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string p1, "::"

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    if-nez v4, :cond_3

    const-string v3, ""

    :goto_1
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 794508
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 794509
    :cond_1
    iget-object v0, p0, LX/4c3;->g:Lorg/apache/http/HttpResponse;

    new-instance v1, LX/4c5;

    iget-object v2, p0, LX/4c3;->g:Lorg/apache/http/HttpResponse;

    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    invoke-direct {v1, p0, v2}, LX/4c5;-><init>(LX/4c3;Lorg/apache/http/HttpEntity;)V

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 794510
    :goto_2
    return-void

    .line 794511
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/4c3;->a(Ljava/io/InputStream;)Ljava/io/InputStream;

    .line 794512
    invoke-virtual {p0}, LX/4c3;->b()V

    goto :goto_2

    .line 794513
    :cond_3
    iget-object v3, v4, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v3, v3

    .line 794514
    goto :goto_1
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 794515
    iget-object v0, p0, LX/4c3;->h:LX/4c2;

    if-nez v0, :cond_2

    .line 794516
    iget-object v0, p0, LX/4c3;->i:LX/1iv;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 794517
    :cond_0
    :goto_1
    iget-object v0, p0, LX/4c3;->h:LX/4c2;

    if-eqz v0, :cond_4

    :goto_2
    return v1

    :cond_1
    move v0, v2

    .line 794518
    goto :goto_0

    .line 794519
    :cond_2
    iget-object v0, p0, LX/4c3;->h:LX/4c2;

    sget-object v3, LX/4c2;->REPORTED_FAILURE:LX/4c2;

    if-ne v0, v3, :cond_0

    .line 794520
    iget-object v0, p0, LX/4c3;->i:LX/1iv;

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v1, v2

    .line 794521
    goto :goto_2
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 794522
    invoke-virtual {p0}, LX/4c3;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 794523
    :try_start_0
    iget-object v0, p0, LX/4c3;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1iZ;

    .line 794524
    iget-object v2, p0, LX/4c3;->g:Lorg/apache/http/HttpResponse;

    iget-object v3, p0, LX/4c3;->a:Lorg/apache/http/protocol/HttpContext;

    invoke-interface {v0, v2, v3}, LX/1iZ;->b(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 794525
    :catchall_0
    move-exception v0

    sget-object v1, LX/4c2;->REPORTED_SUCCESS:LX/4c2;

    iput-object v1, p0, LX/4c3;->h:LX/4c2;

    throw v0

    .line 794526
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 794527
    :cond_1
    sget-object v0, LX/4c2;->REPORTED_SUCCESS:LX/4c2;

    iput-object v0, p0, LX/4c3;->h:LX/4c2;

    .line 794528
    return-void
.end method

.method public final b(Ljava/io/IOException;)V
    .locals 7

    .prologue
    .line 794529
    invoke-virtual {p0}, LX/4c3;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 794530
    :try_start_0
    iget-object v0, p0, LX/4c3;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1iZ;

    .line 794531
    sget-object v1, LX/1iv;->HTTP_CLIENT_EXECUTE:LX/1iv;

    iget-object v2, p0, LX/4c3;->b:Lorg/apache/http/HttpRequest;

    iget-object v3, p0, LX/4c3;->g:Lorg/apache/http/HttpResponse;

    iget-object v4, p0, LX/4c3;->a:Lorg/apache/http/protocol/HttpContext;

    move-object v5, p1

    invoke-interface/range {v0 .. v5}, LX/1iZ;->a(LX/1iv;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 794532
    :catchall_0
    move-exception v0

    sget-object v1, LX/4c2;->REPORTED_FAILURE:LX/4c2;

    iput-object v1, p0, LX/4c3;->h:LX/4c2;

    .line 794533
    sget-object v1, LX/1iv;->HTTP_CLIENT_EXECUTE:LX/1iv;

    iput-object v1, p0, LX/4c3;->i:LX/1iv;

    throw v0

    .line 794534
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 794535
    :cond_1
    sget-object v0, LX/4c2;->REPORTED_FAILURE:LX/4c2;

    iput-object v0, p0, LX/4c3;->h:LX/4c2;

    .line 794536
    sget-object v0, LX/1iv;->HTTP_CLIENT_EXECUTE:LX/1iv;

    iput-object v0, p0, LX/4c3;->i:LX/1iv;

    .line 794537
    return-void
.end method
