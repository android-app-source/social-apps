.class public final LX/40i;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 664192
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_9

    .line 664193
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 664194
    :goto_0
    return v1

    .line 664195
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 664196
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_8

    .line 664197
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 664198
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 664199
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 664200
    const-string v9, "add_item_action_info"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 664201
    invoke-static {p0, p1}, LX/40f;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 664202
    :cond_2
    const-string v9, "added_item_state_info"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 664203
    invoke-static {p0, p1}, LX/40g;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 664204
    :cond_3
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 664205
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 664206
    :cond_4
    const-string v9, "name"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 664207
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 664208
    :cond_5
    const-string v9, "new_item_default_privacy"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 664209
    invoke-static {p0, p1}, LX/40e;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 664210
    :cond_6
    const-string v9, "saved_dashboard_section"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 664211
    invoke-static {p0, p1}, LX/40h;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 664212
    :cond_7
    const-string v9, "url"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 664213
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 664214
    :cond_8
    const/4 v8, 0x7

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 664215
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 664216
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 664217
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 664218
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 664219
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 664220
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 664221
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 664222
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 664223
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 664224
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 664225
    if-eqz v0, :cond_0

    .line 664226
    const-string v1, "add_item_action_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 664227
    invoke-static {p0, v0, p2, p3}, LX/40f;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 664228
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 664229
    if-eqz v0, :cond_1

    .line 664230
    const-string v1, "added_item_state_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 664231
    invoke-static {p0, v0, p2, p3}, LX/40g;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 664232
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 664233
    if-eqz v0, :cond_2

    .line 664234
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 664235
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 664236
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 664237
    if-eqz v0, :cond_3

    .line 664238
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 664239
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 664240
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 664241
    if-eqz v0, :cond_4

    .line 664242
    const-string v1, "new_item_default_privacy"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 664243
    invoke-static {p0, v0, p2}, LX/40e;->a(LX/15i;ILX/0nX;)V

    .line 664244
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 664245
    if-eqz v0, :cond_5

    .line 664246
    const-string v1, "saved_dashboard_section"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 664247
    invoke-static {p0, v0, p2}, LX/40h;->a(LX/15i;ILX/0nX;)V

    .line 664248
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 664249
    if-eqz v0, :cond_6

    .line 664250
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 664251
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 664252
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 664253
    return-void
.end method
