.class public LX/4lq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4lm;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/4lq;


# instance fields
.field private final a:LX/4lv;


# direct methods
.method public constructor <init>(LX/4lv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 804828
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 804829
    iput-object p1, p0, LX/4lq;->a:LX/4lv;

    .line 804830
    return-void
.end method

.method public static a(LX/0QB;)LX/4lq;
    .locals 4

    .prologue
    .line 804831
    sget-object v0, LX/4lq;->b:LX/4lq;

    if-nez v0, :cond_1

    .line 804832
    const-class v1, LX/4lq;

    monitor-enter v1

    .line 804833
    :try_start_0
    sget-object v0, LX/4lq;->b:LX/4lq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 804834
    if-eqz v2, :cond_0

    .line 804835
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 804836
    new-instance p0, LX/4lq;

    invoke-static {v0}, LX/4lv;->a(LX/0QB;)LX/4lv;

    move-result-object v3

    check-cast v3, LX/4lv;

    invoke-direct {p0, v3}, LX/4lq;-><init>(LX/4lv;)V

    .line 804837
    move-object v0, p0

    .line 804838
    sput-object v0, LX/4lq;->b:LX/4lq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 804839
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 804840
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 804841
    :cond_1
    sget-object v0, LX/4lq;->b:LX/4lq;

    return-object v0

    .line 804842
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 804843
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 804844
    sget-boolean v0, LX/4lv;->b:Z

    move v0, v0

    .line 804845
    return v0
.end method
