.class public final enum LX/4cV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4cV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4cV;

.field public static final enum BROWSER_COMPATIBLE:LX/4cV;

.field public static final enum STRICT:LX/4cV;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 795105
    new-instance v0, LX/4cV;

    const-string v1, "STRICT"

    invoke-direct {v0, v1, v2}, LX/4cV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4cV;->STRICT:LX/4cV;

    .line 795106
    new-instance v0, LX/4cV;

    const-string v1, "BROWSER_COMPATIBLE"

    invoke-direct {v0, v1, v3}, LX/4cV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4cV;->BROWSER_COMPATIBLE:LX/4cV;

    .line 795107
    const/4 v0, 0x2

    new-array v0, v0, [LX/4cV;

    sget-object v1, LX/4cV;->STRICT:LX/4cV;

    aput-object v1, v0, v2

    sget-object v1, LX/4cV;->BROWSER_COMPATIBLE:LX/4cV;

    aput-object v1, v0, v3

    sput-object v0, LX/4cV;->$VALUES:[LX/4cV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 795108
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4cV;
    .locals 1

    .prologue
    .line 795109
    const-class v0, LX/4cV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4cV;

    return-object v0
.end method

.method public static values()[LX/4cV;
    .locals 1

    .prologue
    .line 795110
    sget-object v0, LX/4cV;->$VALUES:[LX/4cV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4cV;

    return-object v0
.end method
