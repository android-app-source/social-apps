.class public LX/448;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0p0;


# instance fields
.field private final a:LX/1MP;


# direct methods
.method public constructor <init>(LX/1MP;)V
    .locals 0

    .prologue
    .line 669464
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 669465
    iput-object p1, p0, LX/448;->a:LX/1MP;

    .line 669466
    return-void
.end method


# virtual methods
.method public final h()I
    .locals 14

    .prologue
    const/4 v0, -0x1

    const-wide/16 v2, 0x0

    .line 669467
    iget-object v1, p0, LX/448;->a:LX/1MP;

    .line 669468
    iget-object v4, v1, LX/1MP;->s:LX/1hL;

    move-object v1, v4

    .line 669469
    if-nez v1, :cond_1

    .line 669470
    :cond_0
    :goto_0
    return v0

    .line 669471
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/proxygen/NetworkStatusMonitor;->getLatestObservedConnectionQualitiesNative()[Lcom/facebook/proxygen/ObservedConnQuality;

    move-result-object v1

    .line 669472
    if-eqz v1, :cond_0

    array-length v4, v1

    if-eqz v4, :cond_0

    .line 669473
    array-length v6, v1

    const/4 v0, 0x0

    move-wide v4, v2

    :goto_1
    if-ge v0, v6, :cond_2

    aget-object v7, v1, v0

    .line 669474
    iget-object v8, v7, Lcom/facebook/proxygen/ObservedConnQuality;->mConnQuality:Lcom/facebook/proxygen/ConnQuality;

    move-object v7, v8

    .line 669475
    invoke-virtual {v7}, Lcom/facebook/proxygen/ConnQuality;->getTotalBytesWritten()J

    move-result-wide v8

    long-to-double v8, v8

    .line 669476
    const-wide/high16 v10, 0x4020000000000000L    # 8.0

    invoke-virtual {v7}, Lcom/facebook/proxygen/ConnQuality;->getCwnd()J

    move-result-wide v12

    long-to-double v12, v12

    mul-double/2addr v10, v12

    invoke-virtual {v7}, Lcom/facebook/proxygen/ConnQuality;->getMss()J

    move-result-wide v12

    long-to-double v12, v12

    mul-double/2addr v10, v12

    .line 669477
    invoke-virtual {v7}, Lcom/facebook/proxygen/ConnQuality;->getRtt()J

    move-result-wide v12

    long-to-double v12, v12

    div-double/2addr v10, v12

    mul-double/2addr v10, v8

    add-double/2addr v4, v10

    .line 669478
    add-double/2addr v2, v8

    .line 669479
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 669480
    :cond_2
    div-double v0, v4, v2

    double-to-int v0, v0

    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 4

    .prologue
    .line 669481
    iget-object v0, p0, LX/448;->a:LX/1MP;

    .line 669482
    iget-object v1, v0, LX/1MP;->s:LX/1hL;

    move-object v1, v1

    .line 669483
    const/4 v0, 0x0

    .line 669484
    if-eqz v1, :cond_0

    .line 669485
    invoke-virtual {v1}, Lcom/facebook/proxygen/NetworkStatusMonitor;->getLatestObservedConnectionQualitiesNative()[Lcom/facebook/proxygen/ObservedConnQuality;

    move-result-object v0

    .line 669486
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v1, "{\"name\":\"proxygen_tcp_stats\", \"model\":["

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 669487
    if-eqz v0, :cond_2

    .line 669488
    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_2

    .line 669489
    if-eqz v1, :cond_1

    .line 669490
    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 669491
    :cond_1
    aget-object v3, v0, v1

    invoke-virtual {v3}, Lcom/facebook/proxygen/ObservedConnQuality;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 669492
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 669493
    :cond_2
    const-string v0, "]}"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 669494
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
