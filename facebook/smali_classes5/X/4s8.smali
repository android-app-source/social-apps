.class public LX/4s8;
.super LX/0lp;
.source ""


# static fields
.field public static final g:I

.field public static final h:I

.field private static final serialVersionUID:J = -0x178c2f2832fcfd2dL


# instance fields
.field public _cfgDelegateToTextual:Z

.field public _smileGeneratorFeatures:I

.field public _smileParserFeatures:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 817348
    invoke-static {}, LX/4sD;->collectDefaults()I

    move-result v0

    sput v0, LX/4s8;->g:I

    .line 817349
    invoke-static {}, LX/4s9;->collectDefaults()I

    move-result v0

    sput v0, LX/4s8;->h:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 817350
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/4s8;-><init>(LX/0lD;)V

    return-void
.end method

.method public constructor <init>(LX/0lD;)V
    .locals 1

    .prologue
    .line 817351
    invoke-direct {p0, p1}, LX/0lp;-><init>(LX/0lD;)V

    .line 817352
    sget v0, LX/4s8;->g:I

    iput v0, p0, LX/4s8;->_smileParserFeatures:I

    .line 817353
    sget v0, LX/4s8;->h:I

    iput v0, p0, LX/4s8;->_smileGeneratorFeatures:I

    .line 817354
    return-void
.end method

.method private constructor <init>(LX/4s8;LX/0lD;)V
    .locals 1

    .prologue
    .line 817355
    invoke-direct {p0, p1}, LX/0lp;-><init>(LX/0lp;)V

    .line 817356
    iget-boolean v0, p1, LX/4s8;->_cfgDelegateToTextual:Z

    iput-boolean v0, p0, LX/4s8;->_cfgDelegateToTextual:Z

    .line 817357
    iget v0, p1, LX/4s8;->_smileParserFeatures:I

    iput v0, p0, LX/4s8;->_smileParserFeatures:I

    .line 817358
    iget v0, p1, LX/4s8;->_smileGeneratorFeatures:I

    iput v0, p0, LX/4s8;->_smileGeneratorFeatures:I

    .line 817359
    return-void
.end method

.method private b(Ljava/io/OutputStream;LX/12A;)LX/4sB;
    .locals 6

    .prologue
    .line 817360
    iget v3, p0, LX/4s8;->_smileGeneratorFeatures:I

    .line 817361
    new-instance v0, LX/4sB;

    iget v2, p0, LX/0lp;->_generatorFeatures:I

    iget-object v4, p0, LX/0lp;->_objectCodec:LX/0lD;

    move-object v1, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/4sB;-><init>(LX/12A;IILX/0lD;Ljava/io/OutputStream;)V

    .line 817362
    sget-object v1, LX/4s9;->WRITE_HEADER:LX/4s9;

    invoke-virtual {v1}, LX/4s9;->getMask()I

    move-result v1

    and-int/2addr v1, v3

    if-eqz v1, :cond_1

    .line 817363
    invoke-virtual {v0}, LX/4sB;->l()V

    .line 817364
    :cond_0
    return-object v0

    .line 817365
    :cond_1
    sget-object v1, LX/4s9;->CHECK_SHARED_STRING_VALUES:LX/4s9;

    invoke-virtual {v1}, LX/4s9;->getMask()I

    move-result v1

    and-int/2addr v1, v3

    if-eqz v1, :cond_2

    .line 817366
    new-instance v0, LX/4pY;

    const-string v1, "Inconsistent settings: WRITE_HEADER disabled, but CHECK_SHARED_STRING_VALUES enabled; can not construct generator due to possible data loss (either enable WRITE_HEADER, or disable CHECK_SHARED_STRING_VALUES to resolve)"

    invoke-direct {v0, v1}, LX/4pY;-><init>(Ljava/lang/String;)V

    throw v0

    .line 817367
    :cond_2
    sget-object v1, LX/4s9;->ENCODE_BINARY_AS_7BIT:LX/4s9;

    invoke-virtual {v1}, LX/4s9;->getMask()I

    move-result v1

    and-int/2addr v1, v3

    if-nez v1, :cond_0

    .line 817368
    new-instance v0, LX/4pY;

    const-string v1, "Inconsistent settings: WRITE_HEADER disabled, but ENCODE_BINARY_AS_7BIT disabled; can not construct generator due to possible data loss (either enable WRITE_HEADER, or ENCODE_BINARY_AS_7BIT to resolve)"

    invoke-direct {v0, v1}, LX/4pY;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b(Ljava/io/File;)LX/4sE;
    .locals 2

    .prologue
    .line 817369
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/4 v1, 0x1

    invoke-static {p1, v1}, LX/0lp;->a(Ljava/lang/Object;Z)LX/12A;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/4s8;->b(Ljava/io/InputStream;LX/12A;)LX/4sE;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/io/InputStream;)LX/4sE;
    .locals 1

    .prologue
    .line 817370
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/0lp;->a(Ljava/lang/Object;Z)LX/12A;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/4s8;->b(Ljava/io/InputStream;LX/12A;)LX/4sE;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/io/InputStream;LX/12A;)LX/4sE;
    .locals 6

    .prologue
    .line 817371
    new-instance v0, LX/4sF;

    invoke-direct {v0, p2, p1}, LX/4sF;-><init>(LX/12A;Ljava/io/InputStream;)V

    iget v1, p0, LX/0lp;->_parserFeatures:I

    iget v2, p0, LX/4s8;->_smileParserFeatures:I

    sget-object v3, LX/0lq;->INTERN_FIELD_NAMES:LX/0lq;

    invoke-virtual {p0, v3}, LX/0lp;->a(LX/0lq;)Z

    move-result v3

    iget-object v4, p0, LX/0lp;->_objectCodec:LX/0lD;

    iget-object v5, p0, LX/0lp;->f:LX/0lv;

    invoke-virtual/range {v0 .. v5}, LX/4sF;->a(IIZLX/0lD;LX/0lv;)LX/4sE;

    move-result-object v0

    return-object v0
.end method

.method private b([B)LX/4sE;
    .locals 3

    .prologue
    .line 817372
    const/4 v0, 0x1

    invoke-static {p1, v0}, LX/0lp;->a(Ljava/lang/Object;Z)LX/12A;

    move-result-object v0

    .line 817373
    const/4 v1, 0x0

    array-length v2, p1

    invoke-direct {p0, p1, v1, v2, v0}, LX/4s8;->b([BIILX/12A;)LX/4sE;

    move-result-object v0

    return-object v0
.end method

.method private b([BIILX/12A;)LX/4sE;
    .locals 6

    .prologue
    .line 817374
    new-instance v0, LX/4sF;

    invoke-direct {v0, p4, p1, p2, p3}, LX/4sF;-><init>(LX/12A;[BII)V

    iget v1, p0, LX/0lp;->_parserFeatures:I

    iget v2, p0, LX/4s8;->_smileParserFeatures:I

    sget-object v3, LX/0lq;->INTERN_FIELD_NAMES:LX/0lq;

    invoke-virtual {p0, v3}, LX/0lp;->a(LX/0lq;)Z

    move-result v3

    iget-object v4, p0, LX/0lp;->_objectCodec:LX/0lD;

    iget-object v5, p0, LX/0lp;->f:LX/0lv;

    invoke-virtual/range {v0 .. v5}, LX/4sF;->a(IIZLX/0lD;LX/0lv;)LX/4sE;

    move-result-object v0

    return-object v0
.end method

.method private c(Ljava/io/OutputStream;)LX/4sB;
    .locals 1

    .prologue
    .line 817375
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/0lp;->a(Ljava/lang/Object;Z)LX/12A;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/4s8;->b(Ljava/io/OutputStream;LX/12A;)LX/4sB;

    move-result-object v0

    return-object v0
.end method

.method private d(Ljava/io/OutputStream;)LX/4sB;
    .locals 1

    .prologue
    .line 817376
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/0lp;->a(Ljava/lang/Object;Z)LX/12A;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/4s8;->b(Ljava/io/OutputStream;LX/12A;)LX/4sB;

    move-result-object v0

    return-object v0
.end method

.method private e(Ljava/io/OutputStream;)LX/4sB;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 817345
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/0lp;->a(Ljava/lang/Object;Z)LX/12A;

    move-result-object v0

    .line 817346
    invoke-direct {p0, p1, v0}, LX/4s8;->b(Ljava/io/OutputStream;LX/12A;)LX/4sB;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/io/OutputStream;)LX/0nX;
    .locals 1

    .prologue
    .line 817347
    invoke-direct {p0, p1}, LX/4s8;->d(Ljava/io/OutputStream;)LX/4sB;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/io/OutputStream;LX/12A;)LX/0nX;
    .locals 1

    .prologue
    .line 817344
    invoke-direct {p0, p1, p2}, LX/4s8;->b(Ljava/io/OutputStream;LX/12A;)LX/4sB;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/io/OutputStream;LX/1pL;)LX/0nX;
    .locals 1

    .prologue
    .line 817343
    invoke-direct {p0, p1}, LX/4s8;->c(Ljava/io/OutputStream;)LX/4sB;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/io/Writer;LX/12A;)LX/0nX;
    .locals 2

    .prologue
    .line 817340
    iget-boolean v0, p0, LX/4s8;->_cfgDelegateToTextual:Z

    if-eqz v0, :cond_0

    .line 817341
    invoke-super {p0, p1, p2}, LX/0lp;->a(Ljava/io/Writer;LX/12A;)LX/0nX;

    move-result-object v0

    return-object v0

    .line 817342
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Can not create generator for non-byte-based target"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic a(Ljava/io/File;)LX/15w;
    .locals 1

    .prologue
    .line 817339
    invoke-direct {p0, p1}, LX/4s8;->b(Ljava/io/File;)LX/4sE;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/io/InputStream;)LX/15w;
    .locals 1

    .prologue
    .line 817338
    invoke-direct {p0, p1}, LX/4s8;->b(Ljava/io/InputStream;)LX/4sE;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/io/InputStream;LX/12A;)LX/15w;
    .locals 1

    .prologue
    .line 817337
    invoke-direct {p0, p1, p2}, LX/4s8;->b(Ljava/io/InputStream;LX/12A;)LX/4sE;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/io/Reader;LX/12A;)LX/15w;
    .locals 2

    .prologue
    .line 817334
    iget-boolean v0, p0, LX/4s8;->_cfgDelegateToTextual:Z

    if-eqz v0, :cond_0

    .line 817335
    invoke-super {p0, p1, p2}, LX/0lp;->a(Ljava/io/Reader;LX/12A;)LX/15w;

    move-result-object v0

    return-object v0

    .line 817336
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Can not create generator for non-byte-based target"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic a([B)LX/15w;
    .locals 1

    .prologue
    .line 817333
    invoke-direct {p0, p1}, LX/4s8;->b([B)LX/4sE;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a([BIILX/12A;)LX/15w;
    .locals 1

    .prologue
    .line 817332
    invoke-direct {p0, p1, p2, p3, p4}, LX/4s8;->b([BIILX/12A;)LX/4sE;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/io/OutputStream;LX/1pL;LX/12A;)Ljava/io/Writer;
    .locals 2

    .prologue
    .line 817329
    iget-boolean v0, p0, LX/4s8;->_cfgDelegateToTextual:Z

    if-eqz v0, :cond_0

    .line 817330
    invoke-super {p0, p1, p2, p3}, LX/0lp;->a(Ljava/io/OutputStream;LX/1pL;LX/12A;)Ljava/io/Writer;

    move-result-object v0

    return-object v0

    .line 817331
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Can not create generator for non-byte-based target"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic b(Ljava/io/OutputStream;)LX/0nX;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 817328
    invoke-direct {p0, p1}, LX/4s8;->e(Ljava/io/OutputStream;)LX/4sB;

    move-result-object v0

    return-object v0
.end method

.method public readResolve()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 817327
    new-instance v0, LX/4s8;

    iget-object v1, p0, LX/0lp;->_objectCodec:LX/0lD;

    invoke-direct {v0, p0, v1}, LX/4s8;-><init>(LX/4s8;LX/0lD;)V

    return-object v0
.end method

.method public final version()LX/0ne;
    .locals 1

    .prologue
    .line 817326
    sget-object v0, LX/4s5;->VERSION:LX/0ne;

    return-object v0
.end method
