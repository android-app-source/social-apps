.class public LX/4bs;
.super Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile n:LX/4bs;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/0SG;

.field private final d:LX/0Sh;

.field private final e:Landroid/os/PowerManager;

.field private final f:LX/1r1;

.field private final g:Z

.field private final h:Ljava/util/concurrent/ScheduledExecutorService;

.field private final i:Ljava/lang/Runnable;

.field private final j:LX/1ql;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final k:LX/0Yd;

.field private final l:Ljava/lang/Object;

.field private m:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 794204
    const-class v0, LX/4bs;

    sput-object v0, LX/4bs;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;LX/0SG;LX/0Sh;Landroid/os/PowerManager;LX/1r1;Ljava/lang/Boolean;Ljava/util/concurrent/ScheduledExecutorService;LX/4cI;)V
    .locals 4
    .param p8    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/http/annotations/AllowFbClientConnManagerWakeLocks;
        .end annotation
    .end param
    .param p9    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 794187
    invoke-direct {p0, p2, p3}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 794188
    iput-object p1, p0, LX/4bs;->b:Landroid/content/Context;

    .line 794189
    iput-object p4, p0, LX/4bs;->c:LX/0SG;

    .line 794190
    iput-object p5, p0, LX/4bs;->d:LX/0Sh;

    .line 794191
    iput-object p6, p0, LX/4bs;->e:Landroid/os/PowerManager;

    .line 794192
    iput-object p7, p0, LX/4bs;->f:LX/1r1;

    .line 794193
    invoke-virtual {p8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/4bs;->g:Z

    .line 794194
    iput-object p9, p0, LX/4bs;->h:Ljava/util/concurrent/ScheduledExecutorService;

    .line 794195
    iget-object v0, p0, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;->connOperator:Lorg/apache/http/conn/ClientConnectionOperator;

    check-cast v0, LX/4bw;

    .line 794196
    iput-object p10, v0, LX/4bw;->b:LX/4cI;

    .line 794197
    new-instance v0, Lcom/facebook/http/common/executorimpl/apache/FbClientConnManager$CloseIdleConnectionsRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/http/common/executorimpl/apache/FbClientConnManager$CloseIdleConnectionsRunnable;-><init>(LX/4bs;)V

    iput-object v0, p0, LX/4bs;->i:Ljava/lang/Runnable;

    .line 794198
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/4bs;->l:Ljava/lang/Object;

    .line 794199
    iget-boolean v0, p0, LX/4bs;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4bs;->f:LX/1r1;

    const/4 v1, 0x1

    const-string v2, "FbClientConnManager"

    invoke-virtual {v0, v1, v2}, LX/1r1;->a(ILjava/lang/String;)LX/1ql;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/4bs;->j:LX/1ql;

    .line 794200
    new-instance v0, LX/0Yd;

    const-string v1, "android.intent.action.SCREEN_OFF"

    new-instance v2, LX/4br;

    invoke-direct {v2, p0}, LX/4br;-><init>(LX/4bs;)V

    invoke-static {v1, v2}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0Yd;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, LX/4bs;->k:LX/0Yd;

    .line 794201
    iget-object v0, p0, LX/4bs;->b:Landroid/content/Context;

    iget-object v1, p0, LX/4bs;->k:LX/0Yd;

    iget-object v2, p0, LX/4bs;->k:LX/0Yd;

    invoke-virtual {v2}, LX/0Yd;->a()Landroid/content/IntentFilter;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 794202
    return-void

    .line 794203
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/4bs;
    .locals 14

    .prologue
    .line 794168
    sget-object v0, LX/4bs;->n:LX/4bs;

    if-nez v0, :cond_1

    .line 794169
    const-class v1, LX/4bs;

    monitor-enter v1

    .line 794170
    :try_start_0
    sget-object v0, LX/4bs;->n:LX/4bs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 794171
    if-eqz v2, :cond_0

    .line 794172
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 794173
    new-instance v3, LX/4bs;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1i0;->b(LX/0QB;)Lorg/apache/http/params/HttpParams;

    move-result-object v5

    check-cast v5, Lorg/apache/http/params/HttpParams;

    .line 794174
    invoke-static {v0}, LX/4bc;->b(LX/0QB;)Lorg/apache/http/conn/scheme/SocketFactory;

    move-result-object v6

    check-cast v6, Lorg/apache/http/conn/scheme/SocketFactory;

    invoke-static {v6}, LX/14a;->a(Lorg/apache/http/conn/scheme/SocketFactory;)Lorg/apache/http/conn/scheme/SchemeRegistry;

    move-result-object v6

    move-object v6, v6

    .line 794175
    check-cast v6, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v8

    check-cast v8, LX/0Sh;

    invoke-static {v0}, LX/0kZ;->b(LX/0QB;)Landroid/os/PowerManager;

    move-result-object v9

    check-cast v9, Landroid/os/PowerManager;

    invoke-static {v0}, LX/1r1;->a(LX/0QB;)LX/1r1;

    move-result-object v10

    check-cast v10, LX/1r1;

    .line 794176
    invoke-static {}, LX/14a;->f()Ljava/lang/Boolean;

    move-result-object v11

    move-object v11, v11

    .line 794177
    check-cast v11, Ljava/lang/Boolean;

    invoke-static {v0}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v12

    check-cast v12, Ljava/util/concurrent/ScheduledExecutorService;

    .line 794178
    invoke-static {}, LX/14a;->a()LX/4cI;

    move-result-object v13

    move-object v13, v13

    .line 794179
    check-cast v13, LX/4cI;

    invoke-direct/range {v3 .. v13}, LX/4bs;-><init>(Landroid/content/Context;Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;LX/0SG;LX/0Sh;Landroid/os/PowerManager;LX/1r1;Ljava/lang/Boolean;Ljava/util/concurrent/ScheduledExecutorService;LX/4cI;)V

    .line 794180
    move-object v0, v3

    .line 794181
    sput-object v0, LX/4bs;->n:LX/4bs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 794182
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 794183
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 794184
    :cond_1
    sget-object v0, LX/4bs;->n:LX/4bs;

    return-object v0

    .line 794185
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 794186
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(LX/4bs;)V
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 794157
    iget-object v0, p0, LX/4bs;->e:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 794158
    :cond_0
    :goto_0
    return-void

    .line 794159
    :cond_1
    invoke-direct {p0}, LX/4bs;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 794160
    iget-object v1, p0, LX/4bs;->l:Ljava/lang/Object;

    monitor-enter v1

    .line 794161
    :try_start_0
    iget-object v0, p0, LX/4bs;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, p0, LX/4bs;->m:J

    sub-long/2addr v2, v4

    .line 794162
    const-wide/16 v4, 0x64

    cmp-long v0, v2, v4

    if-lez v0, :cond_3

    .line 794163
    iget-boolean v0, p0, LX/4bs;->g:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/4bs;->j:LX/1ql;

    if-eqz v0, :cond_2

    .line 794164
    iget-object v0, p0, LX/4bs;->j:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->c()V

    .line 794165
    :cond_2
    iget-object v0, p0, LX/4bs;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iput-wide v2, p0, LX/4bs;->m:J

    .line 794166
    iget-object v0, p0, LX/4bs;->h:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v2, p0, LX/4bs;->i:Ljava/lang/Runnable;

    const-wide/16 v4, 0x5dc

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v4, v5, v3}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 794167
    :cond_3
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private d()Z
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 794156
    invoke-virtual {p0}, LX/4bs;->getConnectionsInPool()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b()V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 794136
    iget-object v0, p0, LX/4bs;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 794137
    iget-object v1, p0, LX/4bs;->l:Ljava/lang/Object;

    monitor-enter v1

    .line 794138
    :try_start_0
    iget-object v0, p0, LX/4bs;->e:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 794139
    const-wide/16 v2, 0x514

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v2, v3, v0}, LX/4bs;->closeIdleConnections(JLjava/util/concurrent/TimeUnit;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 794140
    :cond_0
    :try_start_1
    iget-boolean v0, p0, LX/4bs;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/4bs;->j:LX/1ql;

    if-eqz v0, :cond_1

    .line 794141
    iget-object v0, p0, LX/4bs;->j:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->d()V

    .line 794142
    :cond_1
    monitor-exit v1

    return-void

    .line 794143
    :catchall_0
    move-exception v0

    iget-boolean v2, p0, LX/4bs;->g:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/4bs;->j:LX/1ql;

    if-eqz v2, :cond_2

    .line 794144
    iget-object v2, p0, LX/4bs;->j:LX/1ql;

    invoke-virtual {v2}, LX/1ql;->d()V

    :cond_2
    throw v0

    .line 794145
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0
.end method

.method public final createConnectionOperator(Lorg/apache/http/conn/scheme/SchemeRegistry;)Lorg/apache/http/conn/ClientConnectionOperator;
    .locals 1

    .prologue
    .line 794155
    new-instance v0, LX/4bw;

    invoke-direct {v0, p1}, LX/4bw;-><init>(Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    return-object v0
.end method

.method public final createConnectionPool(Lorg/apache/http/params/HttpParams;)Lorg/apache/http/impl/conn/tsccm/AbstractConnPool;
    .locals 1

    .prologue
    .line 794154
    invoke-super {p0, p1}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;->createConnectionPool(Lorg/apache/http/params/HttpParams;)Lorg/apache/http/impl/conn/tsccm/AbstractConnPool;

    move-result-object v0

    return-object v0
.end method

.method public final releaseConnection(Lorg/apache/http/conn/ManagedClientConnection;JLjava/util/concurrent/TimeUnit;)V
    .locals 2

    .prologue
    .line 794146
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gtz v0, :cond_0

    .line 794147
    const-wide/32 p2, 0x1d4c0

    .line 794148
    sget-object p4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 794149
    :cond_0
    :try_start_0
    invoke-super {p0, p1, p2, p3, p4}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;->releaseConnection(Lorg/apache/http/conn/ManagedClientConnection;JLjava/util/concurrent/TimeUnit;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 794150
    invoke-static {p0}, LX/4bs;->c(LX/4bs;)V

    .line 794151
    invoke-virtual {p0}, LX/4bs;->closeExpiredConnections()V

    .line 794152
    return-void

    .line 794153
    :catchall_0
    move-exception v0

    invoke-static {p0}, LX/4bs;->c(LX/4bs;)V

    throw v0
.end method
