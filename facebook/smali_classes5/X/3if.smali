.class public LX/3if;
.super Landroid/view/ViewGroup;
.source ""


# instance fields
.field public a:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/view/accessibility/AccessibilityManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public d:F

.field public e:F

.field private final f:F

.field public g:Landroid/view/View;

.field public h:Landroid/support/v7/widget/RecyclerView;

.field public i:LX/1P1;

.field public j:LX/3ij;

.field public k:LX/0wd;

.field public l:I

.field public m:F

.field public n:Z

.field public o:Z

.field public p:LX/3iq;

.field public q:LX/3ih;

.field private r:F

.field private s:F

.field private t:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    const/high16 v1, 0x3f000000    # 0.5f

    .line 630139
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 630140
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/3if;->c:Ljava/util/Set;

    .line 630141
    iput v1, p0, LX/3if;->d:F

    .line 630142
    iput v1, p0, LX/3if;->e:F

    .line 630143
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/3if;->f:F

    .line 630144
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3if;->n:Z

    .line 630145
    new-instance v0, LX/3ig;

    invoke-direct {v0, p0}, LX/3ig;-><init>(LX/3if;)V

    iput-object v0, p0, LX/3if;->q:LX/3ih;

    .line 630146
    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 630147
    const-class v2, LX/3if;

    invoke-static {v2, p0}, LX/3if;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 630148
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v3, -0x1000000

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v2}, LX/3if;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 630149
    invoke-virtual {p0}, LX/3if;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 630150
    invoke-virtual {p0}, LX/3if;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, LX/3if;->m:F

    .line 630151
    new-instance v2, LX/3ii;

    new-instance v3, LX/3ik;

    invoke-direct {v3, p0}, LX/3ik;-><init>(LX/3if;)V

    invoke-direct {v2, p0, v3}, LX/3ii;-><init>(LX/3if;LX/3il;)V

    iput-object v2, p0, LX/3if;->j:LX/3ij;

    .line 630152
    new-instance v2, LX/1P1;

    invoke-virtual {p0}, LX/3if;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/1P1;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, LX/3if;->i:LX/1P1;

    .line 630153
    new-instance v2, LX/3im;

    invoke-virtual {p0}, LX/3if;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, p0, v3}, LX/3im;-><init>(LX/3if;Landroid/content/Context;)V

    iput-object v2, p0, LX/3if;->h:Landroid/support/v7/widget/RecyclerView;

    .line 630154
    iget-object v2, p0, LX/3if;->h:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, p0, LX/3if;->i:LX/1P1;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 630155
    iget-object v2, p0, LX/3if;->h:Landroid/support/v7/widget/RecyclerView;

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v3, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 630156
    iget-object v2, p0, LX/3if;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0, v2}, LX/3if;->addView(Landroid/view/View;)V

    .line 630157
    new-instance v2, LX/3in;

    invoke-virtual {p0}, LX/3if;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, p0, v3}, LX/3in;-><init>(LX/3if;Landroid/content/Context;)V

    iput-object v2, p0, LX/3if;->g:Landroid/view/View;

    .line 630158
    iget-object v2, p0, LX/3if;->g:Landroid/view/View;

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v2, v3}, LX/3if;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 630159
    iget-object v2, p0, LX/3if;->a:LX/0wW;

    invoke-virtual {v2}, LX/0wW;->a()LX/0wd;

    move-result-object v2

    new-instance v3, LX/0wT;

    const-wide v4, 0x4085e00000000000L    # 700.0

    const-wide/high16 v6, 0x4048000000000000L    # 48.0

    invoke-direct {v3, v4, v5, v6, v7}, LX/0wT;-><init>(DD)V

    invoke-virtual {v2, v3}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v2

    new-instance v3, LX/3io;

    invoke-direct {v3, p0}, LX/3io;-><init>(LX/3if;)V

    invoke-virtual {v2, v3}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v2

    iput-object v2, p0, LX/3if;->k:LX/0wd;

    .line 630160
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/3if;

    invoke-static {p0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v1

    check-cast v1, LX/0wW;

    invoke-static {p0}, LX/0sY;->b(LX/0QB;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object p0

    check-cast p0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v1, p1, LX/3if;->a:LX/0wW;

    iput-object p0, p1, LX/3if;->b:Landroid/view/accessibility/AccessibilityManager;

    return-void
.end method

.method public static e$redex0(LX/3if;)V
    .locals 3

    .prologue
    .line 630131
    iget-object v0, p0, LX/3if;->i:LX/1P1;

    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v0

    if-eqz v0, :cond_1

    .line 630132
    :cond_0
    :goto_0
    return-void

    .line 630133
    :cond_1
    iget-object v0, p0, LX/3if;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getTop()I

    move-result v0

    iget-object v1, p0, LX/3if;->i:LX/1P1;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    add-int/2addr v0, v1

    .line 630134
    iget v1, p0, LX/3if;->l:I

    iget-object v2, p0, LX/3if;->g:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    .line 630135
    add-int/2addr v0, v1

    .line 630136
    invoke-virtual {p0}, LX/3if;->getHeight()I

    move-result v1

    mul-int/lit8 v1, v1, 0x3

    div-int/lit8 v1, v1, 0x4

    move v1, v1

    .line 630137
    if-lt v0, v1, :cond_0

    .line 630138
    iget-object v0, p0, LX/3if;->q:LX/3ih;

    invoke-interface {v0}, LX/3ih;->a()V

    goto :goto_0
.end method

.method private getDefaultShowRatio()F
    .locals 2

    .prologue
    .line 630125
    iget-object v0, p0, LX/3if;->b:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3if;->b:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 630126
    const/high16 v0, 0x3f800000    # 1.0f

    .line 630127
    :goto_0
    return v0

    .line 630128
    :cond_0
    invoke-virtual {p0}, LX/3if;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 630129
    iget v0, p0, LX/3if;->d:F

    goto :goto_0

    .line 630130
    :cond_1
    iget v0, p0, LX/3if;->e:F

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 630115
    iput-boolean v1, p0, LX/3if;->o:Z

    .line 630116
    iget-object v0, p0, LX/3if;->j:LX/3ij;

    invoke-virtual {v0}, LX/3ij;->b()V

    .line 630117
    iget-object v4, p0, LX/3if;->k:LX/0wd;

    const/high16 v5, 0x3f800000    # 1.0f

    iget-object v6, p0, LX/3if;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v6}, Landroid/support/v7/widget/RecyclerView;->getTop()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {p0}, LX/3if;->getBottom()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v6, v7

    sub-float/2addr v5, v6

    float-to-double v6, v5

    invoke-virtual {v4, v6, v7}, LX/0wd;->a(D)LX/0wd;

    .line 630118
    iget-object v4, p0, LX/3if;->k:LX/0wd;

    invoke-virtual {v4}, LX/0wd;->j()LX/0wd;

    .line 630119
    iget-object v0, p0, LX/3if;->k:LX/0wd;

    .line 630120
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 630121
    iget-object v0, p0, LX/3if;->k:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 630122
    iget-object v0, p0, LX/3if;->k:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3if;->p:LX/3iq;

    if-eqz v0, :cond_0

    .line 630123
    iget-object v0, p0, LX/3if;->p:LX/3iq;

    invoke-interface {v0}, LX/3iq;->a()V

    .line 630124
    :cond_0
    return-void
.end method

.method public final computeScroll()V
    .locals 8

    .prologue
    .line 630091
    iget-object v0, p0, LX/3if;->j:LX/3ij;

    const/4 v1, 0x1

    const/4 v7, 0x2

    const/4 v3, 0x0

    .line 630092
    iget v2, v0, LX/3ij;->a:I

    if-ne v2, v7, :cond_2

    .line 630093
    iget-object v2, v0, LX/3ij;->q:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v4

    .line 630094
    iget-object v2, v0, LX/3ij;->q:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrY()I

    move-result v2

    .line 630095
    iget-object v5, v0, LX/3ij;->t:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v5

    sub-int v5, v2, v5

    .line 630096
    if-lez v5, :cond_4

    .line 630097
    iget v5, v0, LX/3ij;->r:I

    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 630098
    :cond_0
    :goto_0
    iget-object v5, v0, LX/3ij;->t:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v5

    sub-int v5, v2, v5

    .line 630099
    if-eqz v5, :cond_1

    .line 630100
    iget-object v6, v0, LX/3ij;->t:Landroid/view/View;

    invoke-virtual {v6, v5}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 630101
    :cond_1
    if-eqz v4, :cond_7

    iget v5, v0, LX/3ij;->r:I

    if-ne v2, v5, :cond_7

    .line 630102
    iget-object v2, v0, LX/3ij;->q:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->abortAnimation()V

    .line 630103
    iget-object v2, v0, LX/3ij;->q:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->isFinished()Z

    move-result v2

    .line 630104
    :goto_1
    if-nez v2, :cond_2

    .line 630105
    if-eqz v1, :cond_5

    .line 630106
    iget-object v2, v0, LX/3ij;->v:Landroid/view/ViewGroup;

    iget-object v4, v0, LX/3ij;->w:Ljava/lang/Runnable;

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    .line 630107
    :cond_2
    :goto_2
    iget v2, v0, LX/3ij;->a:I

    if-ne v2, v7, :cond_6

    const/4 v2, 0x1

    :goto_3
    move v0, v2

    .line 630108
    if-eqz v0, :cond_3

    .line 630109
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    .line 630110
    :cond_3
    return-void

    .line 630111
    :cond_4
    if-gez v5, :cond_0

    .line 630112
    iget v5, v0, LX/3ij;->r:I

    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto :goto_0

    .line 630113
    :cond_5
    invoke-virtual {v0, v3}, LX/3ij;->b(I)V

    goto :goto_2

    :cond_6
    move v2, v3

    .line 630114
    goto :goto_3

    :cond_7
    move v2, v4

    goto :goto_1
.end method

.method public final onAttachedToWindow()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/16 v0, 0x2c

    const v1, -0x2cc3e47f

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 630083
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 630084
    iput-boolean v4, p0, LX/3if;->o:Z

    .line 630085
    iget-object v1, p0, LX/3if;->k:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, LX/0wd;->a(D)LX/0wd;

    .line 630086
    iget-object v1, p0, LX/3if;->k:LX/0wd;

    .line 630087
    iput-boolean v4, v1, LX/0wd;->c:Z

    .line 630088
    iget-object v1, p0, LX/3if;->k:LX/0wd;

    invoke-direct {p0}, LX/3if;->getDefaultShowRatio()F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 630089
    iget-object v1, p0, LX/3if;->i:LX/1P1;

    invoke-virtual {v1, v4, v4}, LX/1P1;->d(II)V

    .line 630090
    const/16 v1, 0x2d

    const v2, 0x56757430

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 630075
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 630076
    iget-boolean v0, p0, LX/3if;->o:Z

    if-eqz v0, :cond_0

    .line 630077
    :goto_0
    return-void

    .line 630078
    :cond_0
    iput v1, p0, LX/3if;->l:I

    .line 630079
    iget-object v0, p0, LX/3if;->k:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    .line 630080
    iget-object v0, p0, LX/3if;->k:LX/0wd;

    .line 630081
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 630082
    iget-object v0, p0, LX/3if;->k:LX/0wd;

    invoke-direct {p0}, LX/3if;->getDefaultShowRatio()F

    move-result v1

    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x6f982bf1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 630161
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 630162
    iget-object v1, p0, LX/3if;->k:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->j()LX/0wd;

    .line 630163
    const/16 v1, 0x2d

    const v2, -0x3de4c915

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 630042
    iget-boolean v0, p0, LX/3if;->o:Z

    if-eqz v0, :cond_0

    move v0, v2

    .line 630043
    :goto_0
    return v0

    .line 630044
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v5

    .line 630045
    packed-switch v5, :pswitch_data_0

    .line 630046
    :cond_1
    :goto_1
    if-nez v5, :cond_3

    move v4, v1

    .line 630047
    :goto_2
    if-nez v5, :cond_4

    move v0, v1

    .line 630048
    :goto_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    iput v5, p0, LX/3if;->r:F

    .line 630049
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iput v5, p0, LX/3if;->s:F

    .line 630050
    iget-object v5, p0, LX/3if;->c:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_5

    .line 630051
    iget-object v0, p0, LX/3if;->j:LX/3ij;

    invoke-virtual {v0}, LX/3ij;->b()V

    .line 630052
    iput-boolean v3, p0, LX/3if;->t:Z

    move v0, v3

    .line 630053
    goto :goto_0

    .line 630054
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v4, p0, LX/3if;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->getTop()I

    move-result v4

    int-to-float v4, v4

    cmpg-float v0, v0, v4

    if-gez v0, :cond_2

    .line 630055
    iget-object v0, p0, LX/3if;->q:LX/3ih;

    invoke-interface {v0}, LX/3ih;->a()V

    move v0, v2

    .line 630056
    goto :goto_0

    .line 630057
    :cond_2
    iget-object v0, p0, LX/3if;->k:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->i()Z

    move-result v0

    if-nez v0, :cond_1

    .line 630058
    iget-object v0, p0, LX/3if;->k:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    goto :goto_1

    .line 630059
    :pswitch_1
    iget-boolean v0, p0, LX/3if;->n:Z

    if-eqz v0, :cond_1

    .line 630060
    iget-object v0, p0, LX/3if;->q:LX/3ih;

    invoke-interface {v0}, LX/3ih;->a()V

    move v0, v2

    .line 630061
    goto :goto_0

    .line 630062
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v4, p0, LX/3if;->r:F

    sub-float/2addr v0, v4

    move v4, v0

    goto :goto_2

    .line 630063
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget v5, p0, LX/3if;->s:F

    sub-float/2addr v0, v5

    goto :goto_3

    .line 630064
    :cond_5
    iget-boolean v5, p0, LX/3if;->t:Z

    if-eqz v5, :cond_6

    .line 630065
    iput-boolean v3, p0, LX/3if;->t:Z

    move v0, v2

    .line 630066
    goto :goto_0

    .line 630067
    :cond_6
    iget-object v5, p0, LX/3if;->j:LX/3ij;

    invoke-virtual {v5, p1}, LX/3ij;->a(Landroid/view/MotionEvent;)Z

    move-result v5

    .line 630068
    if-eqz v5, :cond_8

    .line 630069
    cmpl-float v1, v4, v1

    if-eqz v1, :cond_7

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    div-float v0, v1, v0

    const v1, 0x3f333333    # 0.7f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_7

    .line 630070
    iput-boolean v2, p0, LX/3if;->t:Z

    .line 630071
    iget-object v0, p0, LX/3if;->j:LX/3ij;

    invoke-virtual {v0, p1}, LX/3ij;->b(Landroid/view/MotionEvent;)V

    move v0, v3

    .line 630072
    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 630073
    goto/16 :goto_0

    :cond_8
    move v0, v3

    .line 630074
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onLayout(ZIIII)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 630015
    iget-object v0, p0, LX/3if;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    .line 630016
    iget-object v1, p0, LX/3if;->g:Landroid/view/View;

    invoke-virtual {p0}, LX/3if;->getHeight()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {v1, v4, v0, v4, v2}, Landroid/view/View;->layout(IIII)V

    .line 630017
    iget-object v0, p0, LX/3if;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getTop()I

    move-result v0

    .line 630018
    iget-object v1, p0, LX/3if;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0}, LX/3if;->getWidth()I

    move-result v2

    invoke-virtual {p0}, LX/3if;->getHeight()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {v1, v4, v0, v2, v3}, Landroid/support/v7/widget/RecyclerView;->layout(IIII)V

    .line 630019
    return-void
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 630035
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 630036
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 630037
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/3if;->setMeasuredDimension(II)V

    .line 630038
    invoke-virtual {p0, p1, p2}, LX/3if;->measureChildren(II)V

    .line 630039
    return-void

    :cond_0
    move v0, v2

    .line 630040
    goto :goto_0

    :cond_1
    move v1, v2

    .line 630041
    goto :goto_1
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    const v0, 0x6e0a9fb7

    invoke-static {v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 630032
    iget-boolean v1, p0, LX/3if;->o:Z

    if-nez v1, :cond_0

    .line 630033
    iget-object v1, p0, LX/3if;->j:LX/3ij;

    invoke-virtual {v1, p1}, LX/3ij;->b(Landroid/view/MotionEvent;)V

    .line 630034
    :cond_0
    const v1, -0x5d0fa7d4

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v3
.end method

.method public final requestDisallowInterceptTouchEvent(Z)V
    .locals 0

    .prologue
    .line 630031
    return-void
.end method

.method public setAdapter(LX/1OM;)V
    .locals 1

    .prologue
    .line 630028
    iget-object v0, p0, LX/3if;->h:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 630029
    iget-object v0, p0, LX/3if;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 630030
    :cond_0
    return-void
.end method

.method public setLinearLayoutManager(LX/1P1;)V
    .locals 2

    .prologue
    .line 630022
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 630023
    iget-object v0, p0, LX/3if;->i:LX/1P1;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 630024
    :goto_0
    return-void

    .line 630025
    :cond_0
    iput-object p1, p0, LX/3if;->i:LX/1P1;

    .line 630026
    iget-object v0, p0, LX/3if;->h:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, LX/3if;->i:LX/1P1;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 630027
    invoke-virtual {p0}, LX/3if;->invalidate()V

    goto :goto_0
.end method

.method public setRecyclerViewBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 630020
    iget-object v0, p0, LX/3if;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 630021
    return-void
.end method
