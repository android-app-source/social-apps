.class public LX/52S;
.super LX/0SP;
.source ""

# interfaces
.implements Ljava/util/concurrent/RunnableFuture;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0SP",
        "<TV;>;",
        "Ljava/util/concurrent/RunnableFuture",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private a:Lcom/google/common/util/concurrent/TrustedListenableFutureTask$TrustedFutureInterruptibleTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/52S",
            "<TV;>.TrustedFutureInterruptibleTask;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/concurrent/Callable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 826080
    invoke-direct {p0}, LX/0SP;-><init>()V

    .line 826081
    new-instance v0, Lcom/google/common/util/concurrent/TrustedListenableFutureTask$TrustedFutureInterruptibleTask;

    invoke-direct {v0, p0, p1}, Lcom/google/common/util/concurrent/TrustedListenableFutureTask$TrustedFutureInterruptibleTask;-><init>(LX/52S;Ljava/util/concurrent/Callable;)V

    iput-object v0, p0, LX/52S;->a:Lcom/google/common/util/concurrent/TrustedListenableFutureTask$TrustedFutureInterruptibleTask;

    .line 826082
    return-void
.end method

.method public static a(Ljava/lang/Runnable;Ljava/lang/Object;)LX/52S;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Runnable;",
            "TV;)",
            "LX/52S",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 826083
    new-instance v0, LX/52S;

    invoke-static {p0, p1}, Ljava/util/concurrent/Executors;->callable(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Callable;

    move-result-object v1

    invoke-direct {v0, v1}, LX/52S;-><init>(Ljava/util/concurrent/Callable;)V

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Callable;)LX/52S;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TV;>;)",
            "LX/52S",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 826084
    new-instance v0, LX/52S;

    invoke-direct {v0, p0}, LX/52S;-><init>(Ljava/util/concurrent/Callable;)V

    return-object v0
.end method


# virtual methods
.method public final done()V
    .locals 1

    .prologue
    .line 826085
    invoke-super {p0}, LX/0SP;->done()V

    .line 826086
    const/4 v0, 0x0

    iput-object v0, p0, LX/52S;->a:Lcom/google/common/util/concurrent/TrustedListenableFutureTask$TrustedFutureInterruptibleTask;

    .line 826087
    return-void
.end method

.method public final interruptTask()V
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "Interruption not supported"
    .end annotation

    .prologue
    .line 826088
    iget-object v0, p0, LX/52S;->a:Lcom/google/common/util/concurrent/TrustedListenableFutureTask$TrustedFutureInterruptibleTask;

    .line 826089
    if-eqz v0, :cond_1

    .line 826090
    iget-object p0, v0, Lcom/google/common/util/concurrent/InterruptibleTask;->b:Ljava/lang/Thread;

    .line 826091
    if-eqz p0, :cond_0

    .line 826092
    invoke-virtual {p0}, Ljava/lang/Thread;->interrupt()V

    .line 826093
    :cond_0
    const/4 p0, 0x1

    iput-boolean p0, v0, Lcom/google/common/util/concurrent/InterruptibleTask;->c:Z

    .line 826094
    :cond_1
    return-void
.end method

.method public final run()V
    .locals 1

    .prologue
    .line 826095
    iget-object v0, p0, LX/52S;->a:Lcom/google/common/util/concurrent/TrustedListenableFutureTask$TrustedFutureInterruptibleTask;

    .line 826096
    if-eqz v0, :cond_0

    .line 826097
    invoke-virtual {v0}, Lcom/google/common/util/concurrent/InterruptibleTask;->run()V

    .line 826098
    :cond_0
    return-void
.end method
