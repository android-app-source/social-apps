.class public LX/4Lb;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 684189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 22

    .prologue
    .line 684190
    const/4 v15, 0x0

    .line 684191
    const/4 v14, 0x0

    .line 684192
    const/4 v13, 0x0

    .line 684193
    const/4 v12, 0x0

    .line 684194
    const-wide/16 v10, 0x0

    .line 684195
    const/4 v9, 0x0

    .line 684196
    const/4 v8, 0x0

    .line 684197
    const/4 v7, 0x0

    .line 684198
    const/4 v6, 0x0

    .line 684199
    const/4 v5, 0x0

    .line 684200
    const/4 v4, 0x0

    .line 684201
    const/4 v3, 0x0

    .line 684202
    const/4 v2, 0x0

    .line 684203
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_f

    .line 684204
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 684205
    const/4 v2, 0x0

    .line 684206
    :goto_0
    return v2

    .line 684207
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_c

    .line 684208
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 684209
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 684210
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v18

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 684211
    const-string v6, "cache_id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 684212
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto :goto_1

    .line 684213
    :cond_1
    const-string v6, "creativePymlItems"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 684214
    invoke-static/range {p0 .. p1}, LX/4Lc;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto :goto_1

    .line 684215
    :cond_2
    const-string v6, "creativePymlTitle"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 684216
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto :goto_1

    .line 684217
    :cond_3
    const-string v6, "debug_info"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 684218
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 684219
    :cond_4
    const-string v6, "fetchTimeMs"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 684220
    const/4 v2, 0x1

    .line 684221
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 684222
    :cond_5
    const-string v6, "local_last_negative_feedback_action_type"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 684223
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 684224
    :cond_6
    const-string v6, "local_story_visibility"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 684225
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 684226
    :cond_7
    const-string v6, "local_story_visible_height"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 684227
    const/4 v2, 0x1

    .line 684228
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move v12, v6

    goto/16 :goto_1

    .line 684229
    :cond_8
    const-string v6, "short_term_cache_key"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 684230
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 684231
    :cond_9
    const-string v6, "title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 684232
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 684233
    :cond_a
    const-string v6, "tracking"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 684234
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v9, v2

    goto/16 :goto_1

    .line 684235
    :cond_b
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 684236
    :cond_c
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 684237
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684238
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684239
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 684240
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 684241
    if-eqz v3, :cond_d

    .line 684242
    const/4 v3, 0x4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 684243
    :cond_d
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 684244
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 684245
    if-eqz v8, :cond_e

    .line 684246
    const/4 v2, 0x7

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12, v3}, LX/186;->a(III)V

    .line 684247
    :cond_e
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 684248
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 684249
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 684250
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_f
    move/from16 v16, v14

    move/from16 v17, v15

    move v14, v9

    move v15, v13

    move v13, v8

    move v9, v4

    move v8, v2

    move-wide/from16 v19, v10

    move v11, v6

    move v10, v5

    move-wide/from16 v4, v19

    move/from16 v21, v12

    move v12, v7

    move/from16 v7, v21

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 684251
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 684252
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 684253
    invoke-static {p0, v2}, LX/4Lb;->a(LX/15w;LX/186;)I

    move-result v1

    .line 684254
    if-eqz v0, :cond_0

    .line 684255
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 684256
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 684257
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 684258
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 684259
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 684260
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 684261
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 684262
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 684263
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684264
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 684265
    const-string v0, "name"

    const-string v1, "CreativePagesYouMayLikeFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 684266
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 684267
    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 684268
    if-eqz v0, :cond_0

    .line 684269
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684270
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684271
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 684272
    if-eqz v0, :cond_2

    .line 684273
    const-string v1, "creativePymlItems"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684274
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 684275
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 684276
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/4Lc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 684277
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 684278
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 684279
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 684280
    if-eqz v0, :cond_3

    .line 684281
    const-string v1, "creativePymlTitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684282
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 684283
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 684284
    if-eqz v0, :cond_4

    .line 684285
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684286
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684287
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 684288
    cmp-long v2, v0, v4

    if-eqz v2, :cond_5

    .line 684289
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684290
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 684291
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 684292
    if-eqz v0, :cond_6

    .line 684293
    const-string v1, "local_last_negative_feedback_action_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684294
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684295
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 684296
    if-eqz v0, :cond_7

    .line 684297
    const-string v1, "local_story_visibility"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684298
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684299
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 684300
    if-eqz v0, :cond_8

    .line 684301
    const-string v1, "local_story_visible_height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684302
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 684303
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 684304
    if-eqz v0, :cond_9

    .line 684305
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684306
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684307
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 684308
    if-eqz v0, :cond_a

    .line 684309
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684310
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 684311
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 684312
    if-eqz v0, :cond_b

    .line 684313
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684314
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684315
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 684316
    return-void
.end method
