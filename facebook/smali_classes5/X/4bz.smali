.class public LX/4bz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MQ;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile d:LX/4bz;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/4bv;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/1h1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 794415
    const-class v0, LX/4bz;

    sput-object v0, LX/4bz;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/1h1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/4bv;",
            ">;",
            "LX/1h1;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 794411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 794412
    iput-object p1, p0, LX/4bz;->b:LX/0Ot;

    .line 794413
    iput-object p2, p0, LX/4bz;->c:LX/1h1;

    .line 794414
    return-void
.end method

.method public static a(Lorg/apache/http/protocol/HttpContext;)LX/1iW;
    .locals 2

    .prologue
    .line 794409
    const-string v0, "fb_http_flow_statistics"

    invoke-interface {p0, v0}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1iW;

    .line 794410
    const-string v1, "HttpFlowStatistics not attached to context?"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1iW;

    return-object v0
.end method

.method public static a(LX/0QB;)LX/4bz;
    .locals 5

    .prologue
    .line 794416
    sget-object v0, LX/4bz;->d:LX/4bz;

    if-nez v0, :cond_1

    .line 794417
    const-class v1, LX/4bz;

    monitor-enter v1

    .line 794418
    :try_start_0
    sget-object v0, LX/4bz;->d:LX/4bz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 794419
    if-eqz v2, :cond_0

    .line 794420
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 794421
    new-instance v4, LX/4bz;

    const/16 v3, 0x24f8

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/1gz;->b(LX/0QB;)LX/1h1;

    move-result-object v3

    check-cast v3, LX/1h1;

    invoke-direct {v4, p0, v3}, LX/4bz;-><init>(LX/0Ot;LX/1h1;)V

    .line 794422
    move-object v0, v4

    .line 794423
    sput-object v0, LX/4bz;->d:LX/4bz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 794424
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 794425
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 794426
    :cond_1
    sget-object v0, LX/4bz;->d:LX/4bz;

    return-object v0

    .line 794427
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 794428
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(Lorg/apache/http/protocol/HttpContext;)Ljava/lang/String;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 794402
    const-string v0, "http.connection"

    invoke-interface {p0, v0}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 794403
    if-eqz v0, :cond_0

    instance-of v1, v0, Lorg/apache/http/impl/conn/AbstractClientConnAdapter;

    if-eqz v1, :cond_0

    .line 794404
    check-cast v0, Lorg/apache/http/impl/conn/AbstractClientConnAdapter;

    .line 794405
    invoke-virtual {v0}, Lorg/apache/http/impl/conn/AbstractClientConnAdapter;->getRemoteAddress()Ljava/net/InetAddress;

    move-result-object v0

    .line 794406
    if-eqz v0, :cond_0

    .line 794407
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    .line 794408
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lorg/apache/http/client/methods/HttpUriRequest;LX/15F;Lorg/apache/http/protocol/HttpContext;LX/1iW;)Lorg/apache/http/HttpResponse;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 794386
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/1iW;->a(Z)V

    .line 794387
    const-string v0, "fb_http_flow_statistics"

    invoke-interface {p3, v0, p4}, Lorg/apache/http/protocol/HttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    .line 794388
    iget-object v0, p0, LX/4bz;->c:LX/1h1;

    invoke-interface {v0, p1}, LX/1h1;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v2

    .line 794389
    :try_start_0
    const-string v0, "X-FB-HTTP-Engine"

    const-string v3, "Apache"

    invoke-interface {v2, v0, v3}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 794390
    iget-object v0, p0, LX/4bz;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4bv;

    invoke-interface {v0, v2, p3}, LX/4bv;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 794391
    :try_start_1
    invoke-static {p3}, LX/4bz;->b(Lorg/apache/http/protocol/HttpContext;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    .line 794392
    :goto_0
    if-eqz v1, :cond_0

    .line 794393
    iput-object v1, p4, LX/1iW;->e:Ljava/lang/String;

    .line 794394
    :cond_0
    invoke-virtual {p4}, LX/1iW;->h()V

    .line 794395
    return-object v0

    .line 794396
    :catchall_0
    move-exception v0

    .line 794397
    :try_start_2
    invoke-static {p3}, LX/4bz;->b(Lorg/apache/http/protocol/HttpContext;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v1

    .line 794398
    :goto_1
    if-eqz v1, :cond_1

    .line 794399
    iput-object v1, p4, LX/1iW;->e:Ljava/lang/String;

    .line 794400
    :cond_1
    invoke-virtual {p4}, LX/1iW;->h()V

    .line 794401
    throw v0

    :catch_0
    goto :goto_0

    :catch_1
    goto :goto_1
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 794383
    iget-object v0, p0, LX/4bz;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4bv;

    invoke-interface {v0}, LX/4bv;->a()Lorg/apache/http/client/CookieStore;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/client/CookieStore;->clear()V

    .line 794384
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 794385
    const-string v0, "HttpClient"

    return-object v0
.end method
