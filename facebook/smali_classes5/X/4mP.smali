.class public final LX/4mP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field public final synthetic a:LX/4mQ;


# direct methods
.method public constructor <init>(LX/4mQ;)V
    .locals 0

    .prologue
    .line 805702
    iput-object p1, p0, LX/4mP;->a:LX/4mQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getInterpolation(F)F
    .locals 13

    .prologue
    const-wide/16 v10, 0x0

    const/high16 v9, 0x42700000    # 60.0f

    const/4 v8, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 805703
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 805704
    iget-object v0, p0, LX/4mP;->a:LX/4mQ;

    iget-wide v4, v0, LX/4mQ;->n:J

    cmp-long v0, v4, v10

    if-nez v0, :cond_0

    .line 805705
    iget-object v0, p0, LX/4mP;->a:LX/4mQ;

    const-wide/16 v4, 0x10

    sub-long v4, v2, v4

    .line 805706
    iput-wide v4, v0, LX/4mQ;->n:J

    .line 805707
    :cond_0
    iget-object v0, p0, LX/4mP;->a:LX/4mQ;

    iget-wide v4, v0, LX/4mQ;->n:J

    sub-long/2addr v2, v4

    .line 805708
    long-to-float v0, v2

    const/high16 v2, 0x41800000    # 16.0f

    div-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 805709
    iget-object v0, p0, LX/4mP;->a:LX/4mQ;

    iget v0, v0, LX/4mQ;->o:I

    sub-int v3, v2, v0

    .line 805710
    iget-object v0, p0, LX/4mP;->a:LX/4mQ;

    iget-object v0, v0, LX/4mQ;->l:Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v4

    .line 805711
    iget-object v0, p0, LX/4mP;->a:LX/4mQ;

    iget-object v0, v0, LX/4mQ;->k:Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v5

    .line 805712
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    .line 805713
    iget-object v6, p0, LX/4mP;->a:LX/4mQ;

    iget v6, v6, LX/4mQ;->m:F

    sub-float v6, v4, v6

    iget-object v7, p0, LX/4mP;->a:LX/4mQ;

    iget v7, v7, LX/4mQ;->c:F

    mul-float/2addr v6, v7

    .line 805714
    iget-object v7, p0, LX/4mP;->a:LX/4mQ;

    mul-float/2addr v6, v9

    .line 805715
    iget v12, v7, LX/4mQ;->g:F

    add-float/2addr v12, v6

    iput v12, v7, LX/4mQ;->g:F

    .line 805716
    iget-object v6, p0, LX/4mP;->a:LX/4mQ;

    iget-object v7, p0, LX/4mP;->a:LX/4mQ;

    iget v7, v7, LX/4mQ;->d:F

    .line 805717
    iget v12, v6, LX/4mQ;->g:F

    mul-float/2addr v12, v7

    iput v12, v6, LX/4mQ;->g:F

    .line 805718
    iget-object v6, p0, LX/4mP;->a:LX/4mQ;

    iget-object v7, p0, LX/4mP;->a:LX/4mQ;

    iget v7, v7, LX/4mQ;->g:F

    div-float/2addr v7, v9

    .line 805719
    iget v12, v6, LX/4mQ;->m:F

    add-float/2addr v12, v7

    iput v12, v6, LX/4mQ;->m:F

    .line 805720
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 805721
    :cond_1
    iget-object v0, p0, LX/4mP;->a:LX/4mQ;

    .line 805722
    iput v2, v0, LX/4mQ;->o:I

    .line 805723
    sub-float v0, v4, v5

    .line 805724
    iget-object v2, p0, LX/4mP;->a:LX/4mQ;

    iget v2, v2, LX/4mQ;->m:F

    sub-float/2addr v2, v5

    .line 805725
    cmpl-float v3, v2, v8

    if-eqz v3, :cond_2

    cmpl-float v3, v0, v8

    if-nez v3, :cond_4

    :cond_2
    move v0, v1

    .line 805726
    :cond_3
    :goto_1
    sub-float v2, v1, v0

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 805727
    iget-object v3, p0, LX/4mP;->a:LX/4mQ;

    iget v3, v3, LX/4mQ;->g:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget-object v4, p0, LX/4mP;->a:LX/4mQ;

    iget v4, v4, LX/4mQ;->f:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_5

    iget-object v3, p0, LX/4mP;->a:LX/4mQ;

    iget v3, v3, LX/4mQ;->e:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_5

    .line 805728
    iget-object v0, p0, LX/4mP;->a:LX/4mQ;

    .line 805729
    iput v8, v0, LX/4mQ;->g:F

    .line 805730
    iget-object v0, p0, LX/4mP;->a:LX/4mQ;

    iget-object v0, v0, LX/4mQ;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 805731
    :goto_2
    return v1

    .line 805732
    :cond_4
    div-float v0, v2, v0

    .line 805733
    iget-object v2, p0, LX/4mP;->a:LX/4mQ;

    iget-boolean v2, v2, LX/4mQ;->q:Z

    if-eqz v2, :cond_3

    .line 805734
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_1

    .line 805735
    :cond_5
    iget-object v1, p0, LX/4mP;->a:LX/4mQ;

    iget-object v1, v1, LX/4mQ;->b:Landroid/animation/ObjectAnimator;

    const-wide/32 v2, 0x7fffffff

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move v1, v0

    .line 805736
    goto :goto_2
.end method
