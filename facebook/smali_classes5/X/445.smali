.class public LX/445;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<",
        "LEFT:Ljava/lang/Object;",
        "RIGHT:",
        "Ljava/lang/Object;",
        "KEY:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:LX/30b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/30b",
            "<T",
            "LEFT;",
            "TKEY;>;"
        }
    .end annotation
.end field

.field private b:LX/30b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/30b",
            "<TRIGHT;TKEY;>;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<TKEY;>;"
        }
    .end annotation
.end field

.field private d:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<T",
            "LEFT;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<TRIGHT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 669431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/444;
    .locals 6

    .prologue
    .line 669430
    new-instance v0, LX/444;

    iget-object v1, p0, LX/445;->c:Ljava/util/Comparator;

    iget-object v2, p0, LX/445;->d:Ljava/util/Iterator;

    iget-object v3, p0, LX/445;->e:Ljava/util/Iterator;

    iget-object v4, p0, LX/445;->a:LX/30b;

    iget-object v5, p0, LX/445;->b:LX/30b;

    invoke-direct/range {v0 .. v5}, LX/444;-><init>(Ljava/util/Comparator;Ljava/util/Iterator;Ljava/util/Iterator;LX/30b;LX/30b;)V

    return-object v0
.end method

.method public final a(LX/30b;)LX/445;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/30b",
            "<TRIGHT;TKEY;>;)",
            "LX/445;"
        }
    .end annotation

    .prologue
    .line 669420
    iput-object p1, p0, LX/445;->b:LX/30b;

    .line 669421
    return-object p0
.end method

.method public final a(Ljava/util/Comparator;)LX/445;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<TKEY;>;)",
            "LX/445;"
        }
    .end annotation

    .prologue
    .line 669428
    iput-object p1, p0, LX/445;->c:Ljava/util/Comparator;

    .line 669429
    return-object p0
.end method

.method public final a(Ljava/util/Iterator;)LX/445;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<T",
            "LEFT;",
            ">;)",
            "LX/445;"
        }
    .end annotation

    .prologue
    .line 669426
    iput-object p1, p0, LX/445;->d:Ljava/util/Iterator;

    .line 669427
    return-object p0
.end method

.method public final b(LX/30b;)LX/445;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/30b",
            "<T",
            "LEFT;",
            "TKEY;>;)",
            "LX/445;"
        }
    .end annotation

    .prologue
    .line 669424
    iput-object p1, p0, LX/445;->a:LX/30b;

    .line 669425
    return-object p0
.end method

.method public final b(Ljava/util/Iterator;)LX/445;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<TRIGHT;>;)",
            "LX/445;"
        }
    .end annotation

    .prologue
    .line 669422
    iput-object p1, p0, LX/445;->e:Ljava/util/Iterator;

    .line 669423
    return-object p0
.end method
