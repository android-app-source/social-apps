.class public LX/4LL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 682760
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 31

    .prologue
    .line 682761
    const-wide/16 v24, 0x0

    .line 682762
    const/16 v22, 0x0

    .line 682763
    const/16 v21, 0x0

    .line 682764
    const/16 v20, 0x0

    .line 682765
    const/16 v19, 0x0

    .line 682766
    const/16 v18, 0x0

    .line 682767
    const-wide/16 v16, 0x0

    .line 682768
    const-wide/16 v14, 0x0

    .line 682769
    const/4 v13, 0x0

    .line 682770
    const/4 v12, 0x0

    .line 682771
    const/4 v11, 0x0

    .line 682772
    const/4 v10, 0x0

    .line 682773
    const/4 v9, 0x0

    .line 682774
    const/4 v8, 0x0

    .line 682775
    const/4 v7, 0x0

    .line 682776
    const/4 v6, 0x0

    .line 682777
    const/4 v5, 0x0

    .line 682778
    const/4 v4, 0x0

    .line 682779
    const/4 v3, 0x0

    .line 682780
    const/4 v2, 0x0

    .line 682781
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v23

    sget-object v26, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_16

    .line 682782
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 682783
    const/4 v2, 0x0

    .line 682784
    :goto_0
    return v2

    .line 682785
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_10

    .line 682786
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 682787
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 682788
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 682789
    const-string v6, "creation_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 682790
    const/4 v2, 0x1

    .line 682791
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 682792
    :cond_1
    const-string v6, "document_owner"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 682793
    invoke-static/range {p0 .. p1}, LX/2aw;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v27, v2

    goto :goto_1

    .line 682794
    :cond_2
    const-string v6, "feedback"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 682795
    invoke-static/range {p0 .. p1}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v26, v2

    goto :goto_1

    .line 682796
    :cond_3
    const-string v6, "feedback_options"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 682797
    const/4 v2, 0x1

    .line 682798
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v6

    move v11, v2

    move-object/from16 v25, v6

    goto :goto_1

    .line 682799
    :cond_4
    const-string v6, "format_version"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 682800
    const/4 v2, 0x1

    .line 682801
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    move-result-object v6

    move v10, v2

    move-object/from16 v24, v6

    goto :goto_1

    .line 682802
    :cond_5
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 682803
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 682804
    :cond_6
    const-string v6, "modified_timestamp"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 682805
    const/4 v2, 0x1

    .line 682806
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v9, v2

    move-wide/from16 v22, v6

    goto/16 :goto_1

    .line 682807
    :cond_7
    const-string v6, "publish_timestamp"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 682808
    const/4 v2, 0x1

    .line 682809
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v8, v2

    move-wide/from16 v20, v6

    goto/16 :goto_1

    .line 682810
    :cond_8
    const-string v6, "url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 682811
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 682812
    :cond_9
    const-string v6, "copyright"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 682813
    invoke-static/range {p0 .. p1}, LX/4LK;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 682814
    :cond_a
    const-string v6, "credits"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 682815
    invoke-static/range {p0 .. p1}, LX/4LK;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 682816
    :cond_b
    const-string v6, "document_description"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 682817
    invoke-static/range {p0 .. p1}, LX/4LK;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 682818
    :cond_c
    const-string v6, "document_kicker"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 682819
    invoke-static/range {p0 .. p1}, LX/4LK;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 682820
    :cond_d
    const-string v6, "document_subtitle"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 682821
    invoke-static/range {p0 .. p1}, LX/4LK;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 682822
    :cond_e
    const-string v6, "document_title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 682823
    invoke-static/range {p0 .. p1}, LX/4LK;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 682824
    :cond_f
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 682825
    :cond_10
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 682826
    if-eqz v3, :cond_11

    .line 682827
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 682828
    :cond_11
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 682829
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 682830
    if-eqz v11, :cond_12

    .line 682831
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 682832
    :cond_12
    if-eqz v10, :cond_13

    .line 682833
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 682834
    :cond_13
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 682835
    if-eqz v9, :cond_14

    .line 682836
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v22

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 682837
    :cond_14
    if-eqz v8, :cond_15

    .line 682838
    const/16 v3, 0x8

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v20

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 682839
    :cond_15
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 682840
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 682841
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 682842
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 682843
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 682844
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 682845
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 682846
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_16
    move/from16 v26, v21

    move/from16 v27, v22

    move-wide/from16 v22, v16

    move/from16 v16, v11

    move/from16 v17, v12

    move v12, v7

    move v11, v5

    move/from16 v28, v9

    move v9, v3

    move v3, v6

    move/from16 v29, v8

    move v8, v2

    move/from16 v30, v10

    move v10, v4

    move-wide/from16 v4, v24

    move-object/from16 v24, v19

    move-object/from16 v25, v20

    move/from16 v19, v18

    move-wide/from16 v20, v14

    move/from16 v15, v30

    move/from16 v14, v28

    move/from16 v18, v13

    move/from16 v13, v29

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    .line 682847
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 682848
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 682849
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 682850
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682851
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 682852
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 682853
    if-eqz v0, :cond_1

    .line 682854
    const-string v1, "document_owner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682855
    invoke-static {p0, v0, p2, p3}, LX/2aw;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 682856
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 682857
    if-eqz v0, :cond_2

    .line 682858
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682859
    invoke-static {p0, v0, p2, p3}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 682860
    :cond_2
    invoke-virtual {p0, p1, v6, v3}, LX/15i;->a(IIS)S

    move-result v0

    .line 682861
    if-eqz v0, :cond_3

    .line 682862
    const-string v0, "feedback_options"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682863
    const-class v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    invoke-virtual {p0, p1, v6, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 682864
    :cond_3
    invoke-virtual {p0, p1, v7, v3}, LX/15i;->a(IIS)S

    move-result v0

    .line 682865
    if-eqz v0, :cond_4

    .line 682866
    const-string v0, "format_version"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682867
    const-class v0, Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    invoke-virtual {p0, p1, v7, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 682868
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 682869
    if-eqz v0, :cond_5

    .line 682870
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682871
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 682872
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 682873
    cmp-long v2, v0, v4

    if-eqz v2, :cond_6

    .line 682874
    const-string v2, "modified_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682875
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 682876
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 682877
    cmp-long v2, v0, v4

    if-eqz v2, :cond_7

    .line 682878
    const-string v2, "publish_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682879
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 682880
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 682881
    if-eqz v0, :cond_8

    .line 682882
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682883
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 682884
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 682885
    if-eqz v0, :cond_9

    .line 682886
    const-string v1, "copyright"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682887
    invoke-static {p0, v0, p2, p3}, LX/4LK;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 682888
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 682889
    if-eqz v0, :cond_a

    .line 682890
    const-string v1, "credits"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682891
    invoke-static {p0, v0, p2, p3}, LX/4LK;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 682892
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 682893
    if-eqz v0, :cond_b

    .line 682894
    const-string v1, "document_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682895
    invoke-static {p0, v0, p2, p3}, LX/4LK;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 682896
    :cond_b
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 682897
    if-eqz v0, :cond_c

    .line 682898
    const-string v1, "document_kicker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682899
    invoke-static {p0, v0, p2, p3}, LX/4LK;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 682900
    :cond_c
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 682901
    if-eqz v0, :cond_d

    .line 682902
    const-string v1, "document_subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682903
    invoke-static {p0, v0, p2, p3}, LX/4LK;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 682904
    :cond_d
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 682905
    if-eqz v0, :cond_e

    .line 682906
    const-string v1, "document_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682907
    invoke-static {p0, v0, p2, p3}, LX/4LK;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 682908
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 682909
    return-void
.end method
