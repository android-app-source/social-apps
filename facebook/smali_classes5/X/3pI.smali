.class public LX/3pI;
.super Landroid/widget/TabHost;
.source ""

# interfaces
.implements Landroid/widget/TabHost$OnTabChangeListener;


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/3pH;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/content/Context;

.field private c:LX/0gc;

.field private d:I

.field private e:Landroid/widget/TabHost$OnTabChangeListener;

.field private f:LX/3pH;

.field private g:Z


# direct methods
.method private a(Ljava/lang/String;LX/0hH;)LX/0hH;
    .locals 4

    .prologue
    .line 641445
    const/4 v1, 0x0

    .line 641446
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, LX/3pI;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 641447
    iget-object v0, p0, LX/3pI;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3pH;

    .line 641448
    iget-object v3, v0, LX/3pH;->a:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 641449
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 641450
    :cond_0
    if-nez v1, :cond_1

    .line 641451
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No tab known for tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 641452
    :cond_1
    iget-object v0, p0, LX/3pI;->f:LX/3pH;

    if-eq v0, v1, :cond_5

    .line 641453
    if-nez p2, :cond_2

    .line 641454
    iget-object v0, p0, LX/3pI;->c:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object p2

    .line 641455
    :cond_2
    iget-object v0, p0, LX/3pI;->f:LX/3pH;

    if-eqz v0, :cond_3

    .line 641456
    iget-object v0, p0, LX/3pI;->f:LX/3pH;

    iget-object v0, v0, LX/3pH;->d:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_3

    .line 641457
    iget-object v0, p0, LX/3pI;->f:LX/3pH;

    iget-object v0, v0, LX/3pH;->d:Landroid/support/v4/app/Fragment;

    invoke-virtual {p2, v0}, LX/0hH;->d(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 641458
    :cond_3
    if-eqz v1, :cond_4

    .line 641459
    iget-object v0, v1, LX/3pH;->d:Landroid/support/v4/app/Fragment;

    if-nez v0, :cond_6

    .line 641460
    iget-object v0, p0, LX/3pI;->b:Landroid/content/Context;

    iget-object v2, v1, LX/3pH;->b:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v1, LX/3pH;->c:Landroid/os/Bundle;

    invoke-static {v0, v2, v3}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 641461
    iput-object v0, v1, LX/3pH;->d:Landroid/support/v4/app/Fragment;

    .line 641462
    iget v0, p0, LX/3pI;->d:I

    iget-object v2, v1, LX/3pH;->d:Landroid/support/v4/app/Fragment;

    iget-object v3, v1, LX/3pH;->a:Ljava/lang/String;

    invoke-virtual {p2, v0, v2, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    .line 641463
    :cond_4
    :goto_2
    iput-object v1, p0, LX/3pI;->f:LX/3pH;

    .line 641464
    :cond_5
    return-object p2

    .line 641465
    :cond_6
    iget-object v0, v1, LX/3pH;->d:Landroid/support/v4/app/Fragment;

    invoke-virtual {p2, v0}, LX/0hH;->e(Landroid/support/v4/app/Fragment;)LX/0hH;

    goto :goto_2

    :cond_7
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2c

    const v2, 0x586bc007

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 641402
    invoke-super {p0}, Landroid/widget/TabHost;->onAttachedToWindow()V

    .line 641403
    invoke-virtual {p0}, LX/3pI;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v4

    .line 641404
    const/4 v1, 0x0

    .line 641405
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, LX/3pI;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 641406
    iget-object v0, p0, LX/3pI;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3pH;

    .line 641407
    iget-object v5, p0, LX/3pI;->c:LX/0gc;

    iget-object v6, v0, LX/3pH;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v5

    .line 641408
    iput-object v5, v0, LX/3pH;->d:Landroid/support/v4/app/Fragment;

    .line 641409
    iget-object v5, v0, LX/3pH;->d:Landroid/support/v4/app/Fragment;

    if-eqz v5, :cond_0

    iget-object v5, v0, LX/3pH;->d:Landroid/support/v4/app/Fragment;

    .line 641410
    iget-boolean v6, v5, Landroid/support/v4/app/Fragment;->mDetached:Z

    move v5, v6

    .line 641411
    if-nez v5, :cond_0

    .line 641412
    iget-object v5, v0, LX/3pH;->a:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 641413
    iput-object v0, p0, LX/3pI;->f:LX/3pH;

    .line 641414
    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 641415
    :cond_1
    if-nez v1, :cond_2

    .line 641416
    iget-object v1, p0, LX/3pI;->c:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 641417
    :cond_2
    iget-object v0, v0, LX/3pH;->d:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v0}, LX/0hH;->d(Landroid/support/v4/app/Fragment;)LX/0hH;

    goto :goto_1

    .line 641418
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3pI;->g:Z

    .line 641419
    invoke-direct {p0, v4, v1}, LX/3pI;->a(Ljava/lang/String;LX/0hH;)LX/0hH;

    move-result-object v0

    .line 641420
    if-eqz v0, :cond_4

    .line 641421
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 641422
    iget-object v0, p0, LX/3pI;->c:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 641423
    :cond_4
    const v0, 0x7556625c

    invoke-static {v0, v3}, LX/02F;->g(II)V

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x7ad4ffb7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 641424
    invoke-super {p0}, Landroid/widget/TabHost;->onDetachedFromWindow()V

    .line 641425
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/3pI;->g:Z

    .line 641426
    const/16 v1, 0x2d

    const v2, -0xb308e0d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 641427
    check-cast p1, Landroid/support/v4/app/FragmentTabHost$SavedState;

    .line 641428
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentTabHost$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/TabHost;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 641429
    iget-object v0, p1, Landroid/support/v4/app/FragmentTabHost$SavedState;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, LX/3pI;->setCurrentTabByTag(Ljava/lang/String;)V

    .line 641430
    return-void
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 641431
    invoke-super {p0}, Landroid/widget/TabHost;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 641432
    new-instance v1, Landroid/support/v4/app/FragmentTabHost$SavedState;

    invoke-direct {v1, v0}, Landroid/support/v4/app/FragmentTabHost$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 641433
    invoke-virtual {p0}, LX/3pI;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Landroid/support/v4/app/FragmentTabHost$SavedState;->a:Ljava/lang/String;

    .line 641434
    return-object v1
.end method

.method public final onTabChanged(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 641435
    iget-boolean v0, p0, LX/3pI;->g:Z

    if-eqz v0, :cond_0

    .line 641436
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3pI;->a(Ljava/lang/String;LX/0hH;)LX/0hH;

    move-result-object v0

    .line 641437
    if-eqz v0, :cond_0

    .line 641438
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 641439
    :cond_0
    iget-object v0, p0, LX/3pI;->e:Landroid/widget/TabHost$OnTabChangeListener;

    if-eqz v0, :cond_1

    .line 641440
    iget-object v0, p0, LX/3pI;->e:Landroid/widget/TabHost$OnTabChangeListener;

    invoke-interface {v0, p1}, Landroid/widget/TabHost$OnTabChangeListener;->onTabChanged(Ljava/lang/String;)V

    .line 641441
    :cond_1
    return-void
.end method

.method public setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V
    .locals 0

    .prologue
    .line 641442
    iput-object p1, p0, LX/3pI;->e:Landroid/widget/TabHost$OnTabChangeListener;

    .line 641443
    return-void
.end method

.method public final setup()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 641444
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must call setup() that takes a Context and FragmentManager"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
