.class public LX/4SL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 713942
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 713943
    const/16 v17, 0x0

    .line 713944
    const/16 v16, 0x0

    .line 713945
    const/4 v15, 0x0

    .line 713946
    const/4 v14, 0x0

    .line 713947
    const/4 v13, 0x0

    .line 713948
    const/4 v12, 0x0

    .line 713949
    const/4 v11, 0x0

    .line 713950
    const/4 v10, 0x0

    .line 713951
    const/4 v9, 0x0

    .line 713952
    const/4 v8, 0x0

    .line 713953
    const/4 v7, 0x0

    .line 713954
    const/4 v6, 0x0

    .line 713955
    const/4 v5, 0x0

    .line 713956
    const/4 v4, 0x0

    .line 713957
    const/4 v3, 0x0

    .line 713958
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_1

    .line 713959
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 713960
    const/4 v3, 0x0

    .line 713961
    :goto_0
    return v3

    .line 713962
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 713963
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_d

    .line 713964
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v18

    .line 713965
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 713966
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    if-eqz v18, :cond_1

    .line 713967
    const-string v19, "__type__"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_2

    const-string v19, "__typename"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 713968
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/String;)I

    move-result v17

    goto :goto_1

    .line 713969
    :cond_3
    const-string v19, "collapse_state"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 713970
    const/4 v6, 0x1

    .line 713971
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    move-result-object v16

    goto :goto_1

    .line 713972
    :cond_4
    const-string v19, "has_inner_borders"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 713973
    const/4 v5, 0x1

    .line 713974
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v15

    goto :goto_1

    .line 713975
    :cond_5
    const-string v19, "id"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 713976
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto :goto_1

    .line 713977
    :cond_6
    const-string v19, "page"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 713978
    invoke-static/range {p0 .. p1}, LX/2bc;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 713979
    :cond_7
    const-string v19, "unit_style"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 713980
    const/4 v4, 0x1

    .line 713981
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    move-result-object v12

    goto/16 :goto_1

    .line 713982
    :cond_8
    const-string v19, "unit_type_token"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 713983
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 713984
    :cond_9
    const-string v19, "welcome_note_message"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_a

    .line 713985
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 713986
    :cond_a
    const-string v19, "welcome_note_photo"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_b

    .line 713987
    invoke-static/range {p0 .. p1}, LX/2at;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 713988
    :cond_b
    const-string v19, "unit_score"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_c

    .line 713989
    const/4 v3, 0x1

    .line 713990
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v8

    goto/16 :goto_1

    .line 713991
    :cond_c
    const-string v19, "reaction_unit_components"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 713992
    invoke-static/range {p0 .. p1}, LX/4SK;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 713993
    :cond_d
    const/16 v18, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 713994
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 713995
    if-eqz v6, :cond_e

    .line 713996
    const/4 v6, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v6, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 713997
    :cond_e
    if-eqz v5, :cond_f

    .line 713998
    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v15}, LX/186;->a(IZ)V

    .line 713999
    :cond_f
    const/4 v5, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v14}, LX/186;->b(II)V

    .line 714000
    const/4 v5, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v13}, LX/186;->b(II)V

    .line 714001
    if-eqz v4, :cond_10

    .line 714002
    const/4 v4, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->a(ILjava/lang/Enum;)V

    .line 714003
    :cond_10
    const/4 v4, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 714004
    const/4 v4, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 714005
    const/16 v4, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 714006
    if-eqz v3, :cond_11

    .line 714007
    const/16 v3, 0x9

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8, v4}, LX/186;->a(III)V

    .line 714008
    :cond_11
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 714009
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x5

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 713895
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 713896
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 713897
    if-eqz v0, :cond_0

    .line 713898
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 713899
    invoke-static {p0, p1, v2, p2}, LX/2bt;->a(LX/15i;IILX/0nX;)V

    .line 713900
    :cond_0
    invoke-virtual {p0, p1, v1, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 713901
    if-eqz v0, :cond_1

    .line 713902
    const-string v0, "collapse_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 713903
    const-class v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 713904
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 713905
    if-eqz v0, :cond_2

    .line 713906
    const-string v1, "has_inner_borders"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 713907
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 713908
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 713909
    if-eqz v0, :cond_3

    .line 713910
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 713911
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 713912
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 713913
    if-eqz v0, :cond_4

    .line 713914
    const-string v1, "page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 713915
    invoke-static {p0, v0, p2, p3}, LX/2bc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 713916
    :cond_4
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 713917
    if-eqz v0, :cond_5

    .line 713918
    const-string v0, "unit_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 713919
    const-class v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 713920
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 713921
    if-eqz v0, :cond_6

    .line 713922
    const-string v1, "unit_type_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 713923
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 713924
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 713925
    if-eqz v0, :cond_7

    .line 713926
    const-string v1, "welcome_note_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 713927
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 713928
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 713929
    if-eqz v0, :cond_8

    .line 713930
    const-string v1, "welcome_note_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 713931
    invoke-static {p0, v0, p2, p3}, LX/2at;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 713932
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 713933
    if-eqz v0, :cond_9

    .line 713934
    const-string v1, "unit_score"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 713935
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 713936
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 713937
    if-eqz v0, :cond_a

    .line 713938
    const-string v1, "reaction_unit_components"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 713939
    invoke-static {p0, v0, p2, p3}, LX/4SK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 713940
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 713941
    return-void
.end method
