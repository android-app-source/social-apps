.class public LX/4NR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 692607
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 27

    .prologue
    .line 692608
    const/16 v19, 0x0

    .line 692609
    const-wide/16 v20, 0x0

    .line 692610
    const/16 v18, 0x0

    .line 692611
    const/16 v17, 0x0

    .line 692612
    const/16 v16, 0x0

    .line 692613
    const/4 v15, 0x0

    .line 692614
    const/4 v14, 0x0

    .line 692615
    const/4 v13, 0x0

    .line 692616
    const/4 v12, 0x0

    .line 692617
    const/4 v11, 0x0

    .line 692618
    const/4 v10, 0x0

    .line 692619
    const/4 v9, 0x0

    .line 692620
    const/4 v8, 0x0

    .line 692621
    const/4 v7, 0x0

    .line 692622
    const/4 v6, 0x0

    .line 692623
    const/4 v5, 0x0

    .line 692624
    const/4 v4, 0x0

    .line 692625
    const/4 v3, 0x0

    .line 692626
    const/4 v2, 0x0

    .line 692627
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v22

    sget-object v23, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_15

    .line 692628
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 692629
    const/4 v2, 0x0

    .line 692630
    :goto_0
    return v2

    .line 692631
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v7, :cond_13

    .line 692632
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 692633
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 692634
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v24, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v24

    if-eq v7, v0, :cond_0

    if-eqz v3, :cond_0

    .line 692635
    const-string v7, "data_points"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 692636
    invoke-static/range {p0 .. p1}, LX/4NV;->a(LX/15w;LX/186;)I

    move-result v3

    move v6, v3

    goto :goto_1

    .line 692637
    :cond_1
    const-string v7, "fetchTimeMs"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 692638
    const/4 v2, 0x1

    .line 692639
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto :goto_1

    .line 692640
    :cond_2
    const-string v7, "friend"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 692641
    invoke-static/range {p0 .. p1}, LX/2bO;->a(LX/15w;LX/186;)I

    move-result v3

    move/from16 v23, v3

    goto :goto_1

    .line 692642
    :cond_3
    const-string v7, "id"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 692643
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move/from16 v22, v3

    goto :goto_1

    .line 692644
    :cond_4
    const-string v7, "media_set"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 692645
    invoke-static/range {p0 .. p1}, LX/4PN;->a(LX/15w;LX/186;)I

    move-result v3

    move/from16 v21, v3

    goto :goto_1

    .line 692646
    :cond_5
    const-string v7, "messenger_share_preview_description"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 692647
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    move/from16 v20, v3

    goto :goto_1

    .line 692648
    :cond_6
    const-string v7, "messenger_share_preview_image"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 692649
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v3

    move/from16 v19, v3

    goto/16 :goto_1

    .line 692650
    :cond_7
    const-string v7, "photo_attachments"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 692651
    invoke-static/range {p0 .. p1}, LX/2as;->b(LX/15w;LX/186;)I

    move-result v3

    move/from16 v18, v3

    goto/16 :goto_1

    .line 692652
    :cond_8
    const-string v7, "share_message"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 692653
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    move/from16 v17, v3

    goto/16 :goto_1

    .line 692654
    :cond_9
    const-string v7, "share_preview_story_placeholder"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 692655
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    move/from16 v16, v3

    goto/16 :goto_1

    .line 692656
    :cond_a
    const-string v7, "share_preview_title"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 692657
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    move v15, v3

    goto/16 :goto_1

    .line 692658
    :cond_b
    const-string v7, "share_status"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 692659
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v14, v3

    goto/16 :goto_1

    .line 692660
    :cond_c
    const-string v7, "throwback_accent_image"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 692661
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v3

    move v13, v3

    goto/16 :goto_1

    .line 692662
    :cond_d
    const-string v7, "throwback_subtitle"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 692663
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    move v12, v3

    goto/16 :goto_1

    .line 692664
    :cond_e
    const-string v7, "throwback_title"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 692665
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    move v11, v3

    goto/16 :goto_1

    .line 692666
    :cond_f
    const-string v7, "url"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 692667
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v10, v3

    goto/16 :goto_1

    .line 692668
    :cond_10
    const-string v7, "video_campaign"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_11

    .line 692669
    invoke-static/range {p0 .. p1}, LX/4Nk;->a(LX/15w;LX/186;)I

    move-result v3

    move v9, v3

    goto/16 :goto_1

    .line 692670
    :cond_11
    const-string v7, "messenger_share_preview_title"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 692671
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 692672
    :cond_12
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 692673
    :cond_13
    const/16 v3, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 692674
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 692675
    if-eqz v2, :cond_14

    .line 692676
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 692677
    :cond_14
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 692678
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 692679
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 692680
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 692681
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 692682
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 692683
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 692684
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 692685
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 692686
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 692687
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 692688
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 692689
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 692690
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 692691
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 692692
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 692693
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_15
    move/from16 v22, v17

    move/from16 v23, v18

    move/from16 v17, v12

    move/from16 v18, v13

    move v12, v7

    move v13, v8

    move v8, v3

    move/from16 v25, v10

    move v10, v5

    move/from16 v26, v11

    move v11, v6

    move/from16 v6, v19

    move/from16 v19, v14

    move v14, v9

    move v9, v4

    move-wide/from16 v4, v20

    move/from16 v20, v15

    move/from16 v21, v16

    move/from16 v16, v26

    move/from16 v15, v25

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 692694
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 692695
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 692696
    invoke-static {p0, v2}, LX/4NR;->a(LX/15w;LX/186;)I

    move-result v1

    .line 692697
    if-eqz v0, :cond_0

    .line 692698
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 692699
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 692700
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 692701
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 692702
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 692703
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 692704
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 692705
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 692706
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692707
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 692708
    const-string v0, "name"

    const-string v1, "GoodwillFriendversaryCampaign"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 692709
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 692710
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692711
    if-eqz v0, :cond_0

    .line 692712
    const-string v1, "data_points"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692713
    invoke-static {p0, v0, p2, p3}, LX/4NV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 692714
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 692715
    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 692716
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692717
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 692718
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692719
    if-eqz v0, :cond_2

    .line 692720
    const-string v1, "friend"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692721
    invoke-static {p0, v0, p2, p3}, LX/2bO;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 692722
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 692723
    if-eqz v0, :cond_3

    .line 692724
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692725
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 692726
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692727
    if-eqz v0, :cond_4

    .line 692728
    const-string v1, "media_set"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692729
    invoke-static {p0, v0, p2, p3}, LX/4PN;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 692730
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692731
    if-eqz v0, :cond_5

    .line 692732
    const-string v1, "messenger_share_preview_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692733
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 692734
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692735
    if-eqz v0, :cond_6

    .line 692736
    const-string v1, "messenger_share_preview_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692737
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 692738
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692739
    if-eqz v0, :cond_7

    .line 692740
    const-string v1, "photo_attachments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692741
    invoke-static {p0, v0, p2, p3}, LX/2as;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 692742
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692743
    if-eqz v0, :cond_8

    .line 692744
    const-string v1, "share_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692745
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 692746
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692747
    if-eqz v0, :cond_9

    .line 692748
    const-string v1, "share_preview_story_placeholder"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692749
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 692750
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692751
    if-eqz v0, :cond_a

    .line 692752
    const-string v1, "share_preview_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692753
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 692754
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 692755
    if-eqz v0, :cond_b

    .line 692756
    const-string v1, "share_status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692757
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 692758
    :cond_b
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692759
    if-eqz v0, :cond_c

    .line 692760
    const-string v1, "throwback_accent_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692761
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 692762
    :cond_c
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692763
    if-eqz v0, :cond_d

    .line 692764
    const-string v1, "throwback_subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692765
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 692766
    :cond_d
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692767
    if-eqz v0, :cond_e

    .line 692768
    const-string v1, "throwback_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692769
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 692770
    :cond_e
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 692771
    if-eqz v0, :cond_f

    .line 692772
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692773
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 692774
    :cond_f
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692775
    if-eqz v0, :cond_10

    .line 692776
    const-string v1, "video_campaign"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692777
    invoke-static {p0, v0, p2, p3}, LX/4Nk;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 692778
    :cond_10
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692779
    if-eqz v0, :cond_11

    .line 692780
    const-string v1, "messenger_share_preview_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692781
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 692782
    :cond_11
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 692783
    return-void
.end method
