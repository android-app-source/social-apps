.class public final LX/4Uq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        "Ljava/util/List",
        "<TT;>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 741758
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 741759
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 741760
    if-nez p1, :cond_0

    .line 741761
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 741762
    :goto_0
    return-object v0

    .line 741763
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 741764
    invoke-virtual {p1}, Lcom/facebook/graphql/executor/GraphQLResult;->d()Ljava/util/Collection;

    move-result-object v1

    .line 741765
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 741766
    if-eqz v2, :cond_1

    .line 741767
    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 741768
    :cond_2
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
