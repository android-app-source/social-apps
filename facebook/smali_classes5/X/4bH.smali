.class public LX/4bH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/HttpResponse;


# instance fields
.field private final a:LX/4bG;

.field private final b:Lorg/apache/http/HttpResponse;

.field private c:Lorg/apache/http/HttpEntity;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private d:LX/4bF;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/4bG;Lorg/apache/http/HttpResponse;)V
    .locals 0
    .param p2    # Lorg/apache/http/HttpResponse;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 793545
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 793546
    iput-object p1, p0, LX/4bH;->a:LX/4bG;

    .line 793547
    iput-object p2, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    .line 793548
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()J
    .locals 2

    .prologue
    .line 793549
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4bH;->d:LX/4bF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/4bH;->d:LX/4bF;

    invoke-virtual {v0}, LX/4bF;->a()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final addHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 793550
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0, p1, p2}, Lorg/apache/http/HttpResponse;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 793551
    return-void
.end method

.method public final addHeader(Lorg/apache/http/Header;)V
    .locals 1

    .prologue
    .line 793552
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0, p1}, Lorg/apache/http/HttpResponse;->addHeader(Lorg/apache/http/Header;)V

    .line 793553
    return-void
.end method

.method public final containsHeader(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 793554
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0, p1}, Lorg/apache/http/HttpResponse;->containsHeader(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final getAllHeaders()[Lorg/apache/http/Header;
    .locals 1

    .prologue
    .line 793555
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized getEntity()Lorg/apache/http/HttpEntity;
    .locals 4

    .prologue
    .line 793556
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4bH;->c:Lorg/apache/http/HttpEntity;

    if-nez v0, :cond_0

    .line 793557
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 793558
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->isRepeatable()Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_0
    iput-object v0, p0, LX/4bH;->c:Lorg/apache/http/HttpEntity;

    .line 793559
    :cond_0
    iget-object v0, p0, LX/4bH;->c:Lorg/apache/http/HttpEntity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 793560
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/4bH;->a:LX/4bG;

    iget-object v1, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 793561
    new-instance v3, LX/4bF;

    const-class v2, LX/4bJ;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/4bJ;

    invoke-direct {v3, v2, v1}, LX/4bF;-><init>(LX/4bJ;Lorg/apache/http/HttpEntity;)V

    .line 793562
    move-object v0, v3

    .line 793563
    iput-object v0, p0, LX/4bH;->d:LX/4bF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 793564
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;
    .locals 1

    .prologue
    .line 793565
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0, p1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    return-object v0
.end method

.method public final getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;
    .locals 1

    .prologue
    .line 793566
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0, p1}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v0

    return-object v0
.end method

.method public final getLastHeader(Ljava/lang/String;)Lorg/apache/http/Header;
    .locals 1

    .prologue
    .line 793567
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0, p1}, Lorg/apache/http/HttpResponse;->getLastHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    return-object v0
.end method

.method public final getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 793568
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getLocale()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public final getParams()Lorg/apache/http/params/HttpParams;
    .locals 1

    .prologue
    .line 793569
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    return-object v0
.end method

.method public final getProtocolVersion()Lorg/apache/http/ProtocolVersion;
    .locals 1

    .prologue
    .line 793570
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getProtocolVersion()Lorg/apache/http/ProtocolVersion;

    move-result-object v0

    return-object v0
.end method

.method public final getStatusLine()Lorg/apache/http/StatusLine;
    .locals 1

    .prologue
    .line 793544
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    return-object v0
.end method

.method public final headerIterator()Lorg/apache/http/HeaderIterator;
    .locals 1

    .prologue
    .line 793571
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->headerIterator()Lorg/apache/http/HeaderIterator;

    move-result-object v0

    return-object v0
.end method

.method public final headerIterator(Ljava/lang/String;)Lorg/apache/http/HeaderIterator;
    .locals 1

    .prologue
    .line 793514
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0, p1}, Lorg/apache/http/HttpResponse;->headerIterator(Ljava/lang/String;)Lorg/apache/http/HeaderIterator;

    move-result-object v0

    return-object v0
.end method

.method public final removeHeader(Lorg/apache/http/Header;)V
    .locals 1

    .prologue
    .line 793515
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0, p1}, Lorg/apache/http/HttpResponse;->removeHeader(Lorg/apache/http/Header;)V

    .line 793516
    return-void
.end method

.method public final removeHeaders(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 793517
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0, p1}, Lorg/apache/http/HttpResponse;->removeHeaders(Ljava/lang/String;)V

    .line 793518
    return-void
.end method

.method public final declared-synchronized setEntity(Lorg/apache/http/HttpEntity;)V
    .locals 1

    .prologue
    .line 793519
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0, p1}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 793520
    const/4 v0, 0x0

    iput-object v0, p0, LX/4bH;->c:Lorg/apache/http/HttpEntity;

    .line 793521
    const/4 v0, 0x0

    iput-object v0, p0, LX/4bH;->d:LX/4bF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 793522
    monitor-exit p0

    return-void

    .line 793523
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final setHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 793524
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0, p1, p2}, Lorg/apache/http/HttpResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 793525
    return-void
.end method

.method public final setHeader(Lorg/apache/http/Header;)V
    .locals 1

    .prologue
    .line 793526
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0, p1}, Lorg/apache/http/HttpResponse;->setHeader(Lorg/apache/http/Header;)V

    .line 793527
    return-void
.end method

.method public final setHeaders([Lorg/apache/http/Header;)V
    .locals 1

    .prologue
    .line 793528
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0, p1}, Lorg/apache/http/HttpResponse;->setHeaders([Lorg/apache/http/Header;)V

    .line 793529
    return-void
.end method

.method public final setLocale(Ljava/util/Locale;)V
    .locals 1

    .prologue
    .line 793530
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0, p1}, Lorg/apache/http/HttpResponse;->setLocale(Ljava/util/Locale;)V

    .line 793531
    return-void
.end method

.method public final setParams(Lorg/apache/http/params/HttpParams;)V
    .locals 1

    .prologue
    .line 793532
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0, p1}, Lorg/apache/http/HttpResponse;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 793533
    return-void
.end method

.method public final setReasonPhrase(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 793534
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0, p1}, Lorg/apache/http/HttpResponse;->setReasonPhrase(Ljava/lang/String;)V

    .line 793535
    return-void
.end method

.method public final setStatusCode(I)V
    .locals 1

    .prologue
    .line 793536
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0, p1}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    .line 793537
    return-void
.end method

.method public final setStatusLine(Lorg/apache/http/ProtocolVersion;I)V
    .locals 1

    .prologue
    .line 793538
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0, p1, p2}, Lorg/apache/http/HttpResponse;->setStatusLine(Lorg/apache/http/ProtocolVersion;I)V

    .line 793539
    return-void
.end method

.method public final setStatusLine(Lorg/apache/http/ProtocolVersion;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 793540
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0, p1, p2, p3}, Lorg/apache/http/HttpResponse;->setStatusLine(Lorg/apache/http/ProtocolVersion;ILjava/lang/String;)V

    .line 793541
    return-void
.end method

.method public final setStatusLine(Lorg/apache/http/StatusLine;)V
    .locals 1

    .prologue
    .line 793542
    iget-object v0, p0, LX/4bH;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0, p1}, Lorg/apache/http/HttpResponse;->setStatusLine(Lorg/apache/http/StatusLine;)V

    .line 793543
    return-void
.end method
