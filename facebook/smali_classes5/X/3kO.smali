.class public LX/3kO;
.super LX/3Jj;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3Jj",
        "<",
        "LX/3Jc;",
        "LX/9Uq;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 632861
    invoke-direct {p0}, LX/3Jj;-><init>()V

    .line 632862
    return-void
.end method

.method private constructor <init>(Ljava/util/List;[[[F)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/3Jc;",
            ">;[[[F)V"
        }
    .end annotation

    .prologue
    .line 632863
    invoke-direct {p0, p1, p2}, LX/3Jj;-><init>(Ljava/util/List;[[[F)V

    .line 632864
    return-void
.end method

.method public static a(LX/3Jf;)LX/3kO;
    .locals 3

    .prologue
    .line 632865
    iget-object v0, p0, LX/3Jf;->b:LX/3JT;

    move-object v0, v0

    .line 632866
    sget-object v1, LX/3JT;->STROKE_WIDTH:LX/3JT;

    if-eq v0, v1, :cond_0

    .line 632867
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot create a KeyFramedStrokeWidth object from a non STROKE_WIDTH animation."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 632868
    :cond_0
    new-instance v0, LX/3kO;

    .line 632869
    iget-object v1, p0, LX/3Jf;->c:Ljava/util/List;

    move-object v1, v1

    .line 632870
    iget-object v2, p0, LX/3Jf;->d:[[[F

    move-object v2, v2

    .line 632871
    invoke-direct {v0, v1, v2}, LX/3kO;-><init>(Ljava/util/List;[[[F)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/3Jd;LX/3Jd;FLjava/lang/Object;)V
    .locals 3

    .prologue
    .line 632872
    check-cast p1, LX/3Jc;

    check-cast p2, LX/3Jc;

    check-cast p4, LX/9Uq;

    const/4 v2, 0x0

    .line 632873
    if-nez p2, :cond_0

    .line 632874
    iget-object v0, p1, LX/3Jc;->b:[F

    move-object v0, v0

    .line 632875
    aget v0, v0, v2

    .line 632876
    iput v0, p4, LX/9Uq;->a:F

    .line 632877
    :goto_0
    return-void

    .line 632878
    :cond_0
    iget-object v0, p1, LX/3Jc;->b:[F

    move-object v0, v0

    .line 632879
    aget v0, v0, v2

    .line 632880
    iget-object v1, p2, LX/3Jc;->b:[F

    move-object v1, v1

    .line 632881
    aget v1, v1, v2

    invoke-static {v0, v1, p3}, LX/3Jj;->a(FFF)F

    move-result v0

    .line 632882
    iput v0, p4, LX/9Uq;->a:F

    .line 632883
    goto :goto_0
.end method
