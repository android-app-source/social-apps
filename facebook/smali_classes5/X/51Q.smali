.class public final LX/51Q;
.super LX/1Ej;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1Ej",
        "<",
        "LX/50M",
        "<TK;>;TV;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "LX/50M",
            "<TK;>;TV;>;>;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/51S;


# direct methods
.method public constructor <init>(LX/51S;Ljava/lang/Iterable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "LX/51R",
            "<TK;TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 825042
    iput-object p1, p0, LX/51Q;->b:LX/51S;

    invoke-direct {p0}, LX/1Ej;-><init>()V

    .line 825043
    iput-object p2, p0, LX/51Q;->a:Ljava/lang/Iterable;

    .line 825044
    return-void
.end method


# virtual methods
.method public final containsKey(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 825045
    invoke-virtual {p0, p1}, LX/51Q;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "LX/50M",
            "<TK;>;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 825046
    iget-object v0, p0, LX/51Q;->a:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 825047
    instance-of v0, p1, LX/50M;

    if-eqz v0, :cond_0

    .line 825048
    check-cast p1, LX/50M;

    .line 825049
    iget-object v0, p0, LX/51Q;->b:LX/51S;

    iget-object v0, v0, LX/51S;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, LX/50M;->lowerBound:LX/4xM;

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/51R;

    .line 825050
    if-eqz v0, :cond_0

    .line 825051
    iget-object v1, v0, LX/51R;->a:LX/50M;

    move-object v1, v1

    .line 825052
    invoke-virtual {v1, p1}, LX/50M;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 825053
    invoke-virtual {v0}, LX/51R;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 825054
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 825055
    iget-object v0, p0, LX/51Q;->b:LX/51S;

    iget-object v0, v0, LX/51S;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->size()I

    move-result v0

    return v0
.end method
