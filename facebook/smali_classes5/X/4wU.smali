.class public final LX/4wU;
.super LX/0SY;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0SY",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public volatile a:J

.field public b:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public c:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public volatile d:J

.field public e:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public f:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILX/0R1;)V
    .locals 4
    .param p4    # LX/0R1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TK;>;TK;I",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    const-wide v2, 0x7fffffffffffffffL

    .line 820190
    invoke-direct {p0, p1, p2, p3, p4}, LX/0SY;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILX/0R1;)V

    .line 820191
    iput-wide v2, p0, LX/4wU;->a:J

    .line 820192
    sget-object v0, LX/0SZ;->INSTANCE:LX/0SZ;

    move-object v0, v0

    .line 820193
    iput-object v0, p0, LX/4wU;->b:LX/0R1;

    .line 820194
    sget-object v0, LX/0SZ;->INSTANCE:LX/0SZ;

    move-object v0, v0

    .line 820195
    iput-object v0, p0, LX/4wU;->c:LX/0R1;

    .line 820196
    iput-wide v2, p0, LX/4wU;->d:J

    .line 820197
    sget-object v0, LX/0SZ;->INSTANCE:LX/0SZ;

    move-object v0, v0

    .line 820198
    iput-object v0, p0, LX/4wU;->e:LX/0R1;

    .line 820199
    sget-object v0, LX/0SZ;->INSTANCE:LX/0SZ;

    move-object v0, v0

    .line 820200
    iput-object v0, p0, LX/4wU;->f:LX/0R1;

    .line 820201
    return-void
.end method


# virtual methods
.method public final getAccessTime()J
    .locals 2

    .prologue
    .line 820189
    iget-wide v0, p0, LX/4wU;->a:J

    return-wide v0
.end method

.method public final getNextInAccessQueue()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 820188
    iget-object v0, p0, LX/4wU;->b:LX/0R1;

    return-object v0
.end method

.method public final getNextInWriteQueue()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 820187
    iget-object v0, p0, LX/4wU;->e:LX/0R1;

    return-object v0
.end method

.method public final getPreviousInAccessQueue()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 820186
    iget-object v0, p0, LX/4wU;->c:LX/0R1;

    return-object v0
.end method

.method public final getPreviousInWriteQueue()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 820185
    iget-object v0, p0, LX/4wU;->f:LX/0R1;

    return-object v0
.end method

.method public final getWriteTime()J
    .locals 2

    .prologue
    .line 820172
    iget-wide v0, p0, LX/4wU;->d:J

    return-wide v0
.end method

.method public final setAccessTime(J)V
    .locals 1

    .prologue
    .line 820183
    iput-wide p1, p0, LX/4wU;->a:J

    .line 820184
    return-void
.end method

.method public final setNextInAccessQueue(LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 820181
    iput-object p1, p0, LX/4wU;->b:LX/0R1;

    .line 820182
    return-void
.end method

.method public final setNextInWriteQueue(LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 820179
    iput-object p1, p0, LX/4wU;->e:LX/0R1;

    .line 820180
    return-void
.end method

.method public final setPreviousInAccessQueue(LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 820177
    iput-object p1, p0, LX/4wU;->c:LX/0R1;

    .line 820178
    return-void
.end method

.method public final setPreviousInWriteQueue(LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 820175
    iput-object p1, p0, LX/4wU;->f:LX/0R1;

    .line 820176
    return-void
.end method

.method public final setWriteTime(J)V
    .locals 1

    .prologue
    .line 820173
    iput-wide p1, p0, LX/4wU;->d:J

    .line 820174
    return-void
.end method
