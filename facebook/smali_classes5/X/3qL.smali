.class public LX/3qL;
.super LX/3qK;
.source ""


# static fields
.field public static final a:LX/3qD;

.field public static final g:LX/3qG;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/CharSequence;

.field private final d:[Ljava/lang/CharSequence;

.field private final e:Z

.field private final f:Landroid/os/Bundle;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 642176
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_0

    .line 642177
    new-instance v0, LX/3qH;

    invoke-direct {v0}, LX/3qH;-><init>()V

    sput-object v0, LX/3qL;->g:LX/3qG;

    .line 642178
    :goto_0
    new-instance v0, LX/3qE;

    invoke-direct {v0}, LX/3qE;-><init>()V

    sput-object v0, LX/3qL;->a:LX/3qD;

    return-void

    .line 642179
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 642180
    new-instance v0, LX/3qJ;

    invoke-direct {v0}, LX/3qJ;-><init>()V

    sput-object v0, LX/3qL;->g:LX/3qG;

    goto :goto_0

    .line 642181
    :cond_1
    new-instance v0, LX/3qI;

    invoke-direct {v0}, LX/3qI;-><init>()V

    sput-object v0, LX/3qL;->g:LX/3qG;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/CharSequence;ZLandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 642169
    invoke-direct {p0}, LX/3qK;-><init>()V

    .line 642170
    iput-object p1, p0, LX/3qL;->b:Ljava/lang/String;

    .line 642171
    iput-object p2, p0, LX/3qL;->c:Ljava/lang/CharSequence;

    .line 642172
    iput-object p3, p0, LX/3qL;->d:[Ljava/lang/CharSequence;

    .line 642173
    iput-boolean p4, p0, LX/3qL;->e:Z

    .line 642174
    iput-object p5, p0, LX/3qL;->f:Landroid/os/Bundle;

    .line 642175
    return-void
.end method

.method public static a(Landroid/content/Intent;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 642163
    sget-object v0, LX/3qL;->g:LX/3qG;

    invoke-interface {v0, p0}, LX/3qG;->a(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 642168
    iget-object v0, p0, LX/3qL;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 642167
    iget-object v0, p0, LX/3qL;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final c()[Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 642166
    iget-object v0, p0, LX/3qL;->d:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 642165
    iget-boolean v0, p0, LX/3qL;->e:Z

    return v0
.end method

.method public final e()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 642164
    iget-object v0, p0, LX/3qL;->f:Landroid/os/Bundle;

    return-object v0
.end method
