.class public final enum LX/4dJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4dJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4dJ;

.field public static final enum BLEND_WITH_PREVIOUS:LX/4dJ;

.field public static final enum NO_BLEND:LX/4dJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 795730
    new-instance v0, LX/4dJ;

    const-string v1, "BLEND_WITH_PREVIOUS"

    invoke-direct {v0, v1, v2}, LX/4dJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4dJ;->BLEND_WITH_PREVIOUS:LX/4dJ;

    .line 795731
    new-instance v0, LX/4dJ;

    const-string v1, "NO_BLEND"

    invoke-direct {v0, v1, v3}, LX/4dJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4dJ;->NO_BLEND:LX/4dJ;

    .line 795732
    const/4 v0, 0x2

    new-array v0, v0, [LX/4dJ;

    sget-object v1, LX/4dJ;->BLEND_WITH_PREVIOUS:LX/4dJ;

    aput-object v1, v0, v2

    sget-object v1, LX/4dJ;->NO_BLEND:LX/4dJ;

    aput-object v1, v0, v3

    sput-object v0, LX/4dJ;->$VALUES:[LX/4dJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 795733
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4dJ;
    .locals 1

    .prologue
    .line 795729
    const-class v0, LX/4dJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4dJ;

    return-object v0
.end method

.method public static values()[LX/4dJ;
    .locals 1

    .prologue
    .line 795728
    sget-object v0, LX/4dJ;->$VALUES:[LX/4dJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4dJ;

    return-object v0
.end method
