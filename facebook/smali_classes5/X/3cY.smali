.class public final LX/3cY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/friends/model/FriendRequest;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V
    .locals 0

    .prologue
    .line 614692
    iput-object p1, p0, LX/3cY;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 614693
    iget-object v0, p0, LX/3cY;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v0, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->y:LX/0xB;

    sget-object v1, LX/12j;->FRIEND_REQUESTS:LX/12j;

    invoke-virtual {v0, v1}, LX/0xB;->a(LX/12j;)I

    move-result v0

    .line 614694
    const/4 v1, 0x3

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 614695
    const/4 v1, 0x6

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 614696
    iget-object v1, p0, LX/3cY;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v1, v1, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->s:LX/2dj;

    sget-object v2, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->i:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, LX/2dj;->a(ILcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
