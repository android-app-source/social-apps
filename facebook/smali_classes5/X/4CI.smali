.class public LX/4CI;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements Landroid/graphics/drawable/Animatable;
.implements LX/2dJ;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:LX/4CJ;


# instance fields
.field public c:LX/4C5;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:LX/4CN;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private volatile e:Z

.field public f:J

.field private g:J

.field public h:I

.field private volatile i:LX/4CJ;

.field private j:LX/1al;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 678689
    const-class v0, LX/4CI;

    sput-object v0, LX/4CI;->a:Ljava/lang/Class;

    .line 678690
    new-instance v0, LX/4CJ;

    invoke-direct {v0}, LX/4CJ;-><init>()V

    sput-object v0, LX/4CI;->b:LX/4CJ;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 678605
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/4CI;-><init>(LX/4C5;)V

    .line 678606
    return-void
.end method

.method public constructor <init>(LX/4C5;)V
    .locals 1
    .param p1    # LX/4C5;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 678679
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 678680
    sget-object v0, LX/4CI;->b:LX/4CJ;

    iput-object v0, p0, LX/4CI;->i:LX/4CJ;

    .line 678681
    new-instance v0, Lcom/facebook/fresco/animation/drawable/AnimatedDrawable2$1;

    invoke-direct {v0, p0}, Lcom/facebook/fresco/animation/drawable/AnimatedDrawable2$1;-><init>(LX/4CI;)V

    iput-object v0, p0, LX/4CI;->k:Ljava/lang/Runnable;

    .line 678682
    iput-object p1, p0, LX/4CI;->c:LX/4C5;

    .line 678683
    iget-object v0, p0, LX/4CI;->c:LX/4C5;

    .line 678684
    if-nez v0, :cond_0

    .line 678685
    const/4 p1, 0x0

    .line 678686
    :goto_0
    move-object v0, p1

    .line 678687
    iput-object v0, p0, LX/4CI;->d:LX/4CN;

    .line 678688
    return-void

    :cond_0
    new-instance p1, LX/4CN;

    invoke-direct {p1, v0}, LX/4CN;-><init>(LX/4C4;)V

    goto :goto_0
.end method

.method private static e()J
    .locals 2

    .prologue
    .line 678678
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 678675
    iget-object v0, p0, LX/4CI;->c:LX/4C5;

    if-eqz v0, :cond_0

    .line 678676
    iget-object v0, p0, LX/4CI;->c:LX/4C5;

    invoke-interface {v0}, LX/4C5;->c()V

    .line 678677
    :cond_0
    return-void
.end method

.method public final b()J
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 678666
    iget-object v1, p0, LX/4CI;->c:LX/4C5;

    if-nez v1, :cond_0

    .line 678667
    const-wide/16 v0, 0x0

    .line 678668
    :goto_0
    return-wide v0

    .line 678669
    :cond_0
    iget-object v1, p0, LX/4CI;->d:LX/4CN;

    if-eqz v1, :cond_1

    .line 678670
    iget-object v0, p0, LX/4CI;->d:LX/4CN;

    invoke-virtual {v0}, LX/4CN;->a()J

    move-result-wide v0

    goto :goto_0

    :cond_1
    move v1, v0

    .line 678671
    :goto_1
    iget-object v2, p0, LX/4CI;->c:LX/4C5;

    invoke-interface {v2}, LX/4C4;->d()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 678672
    iget-object v2, p0, LX/4CI;->c:LX/4C5;

    invoke-interface {v2, v0}, LX/4C4;->b(I)I

    move-result v2

    add-int/2addr v1, v2

    .line 678673
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 678674
    :cond_2
    int-to-long v0, v1

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    .line 678642
    iget-object v0, p0, LX/4CI;->c:LX/4C5;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4CI;->d:LX/4CN;

    if-nez v0, :cond_1

    .line 678643
    :cond_0
    :goto_0
    return-void

    .line 678644
    :cond_1
    invoke-static {}, LX/4CI;->e()J

    move-result-wide v0

    .line 678645
    iget-boolean v2, p0, LX/4CI;->e:Z

    if-eqz v2, :cond_4

    iget-wide v2, p0, LX/4CI;->f:J

    sub-long/2addr v0, v2

    .line 678646
    :goto_1
    iget-object v2, p0, LX/4CI;->d:LX/4CN;

    invoke-virtual {v2, v0, v1}, LX/4CN;->a(J)I

    move-result v2

    .line 678647
    iput-wide v0, p0, LX/4CI;->g:J

    .line 678648
    const/4 v0, -0x1

    if-ne v2, v0, :cond_5

    .line 678649
    iget-object v0, p0, LX/4CI;->c:LX/4C5;

    invoke-interface {v0}, LX/4C4;->d()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 678650
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/4CI;->e:Z

    .line 678651
    :goto_2
    iget-object v1, p0, LX/4CI;->c:LX/4C5;

    invoke-interface {v1, p0, p1, v0}, LX/4C5;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Canvas;I)Z

    move-result v0

    .line 678652
    if-nez v0, :cond_2

    .line 678653
    iget v1, p0, LX/4CI;->h:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/4CI;->h:I

    .line 678654
    const/4 v1, 0x2

    invoke-static {v1}, LX/03J;->a(I)Z

    .line 678655
    :cond_2
    iget-boolean v1, p0, LX/4CI;->e:Z

    if-eqz v1, :cond_0

    .line 678656
    invoke-static {}, LX/4CI;->e()J

    move-result-wide v2

    .line 678657
    const/4 v1, 0x2

    invoke-static {v1}, LX/03J;->a(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 678658
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 678659
    :cond_3
    iget-object v0, p0, LX/4CI;->d:LX/4CN;

    iget-wide v4, p0, LX/4CI;->f:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, LX/4CN;->b(J)J

    move-result-wide v0

    .line 678660
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 678661
    iget-object v6, p0, LX/4CI;->k:Ljava/lang/Runnable;

    iget-wide v8, p0, LX/4CI;->f:J

    add-long/2addr v8, v0

    invoke-virtual {p0, v6, v8, v9}, LX/4CI;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 678662
    goto :goto_0

    .line 678663
    :cond_4
    iget-wide v0, p0, LX/4CI;->g:J

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_1

    .line 678664
    :cond_5
    if-nez v2, :cond_6

    .line 678665
    :cond_6
    move v0, v2

    goto :goto_2
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 678639
    iget-object v0, p0, LX/4CI;->c:LX/4C5;

    if-nez v0, :cond_0

    .line 678640
    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 678641
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/4CI;->c:LX/4C5;

    invoke-interface {v0}, LX/4C5;->b()I

    move-result v0

    goto :goto_0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 678691
    iget-object v0, p0, LX/4CI;->c:LX/4C5;

    if-nez v0, :cond_0

    .line 678692
    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 678693
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/4CI;->c:LX/4C5;

    invoke-interface {v0}, LX/4C5;->a()I

    move-result v0

    goto :goto_0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 678638
    const/4 v0, -0x3

    return v0
.end method

.method public final isRunning()Z
    .locals 1

    .prologue
    .line 678637
    iget-boolean v0, p0, LX/4CI;->e:Z

    return v0
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 678633
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 678634
    iget-object v0, p0, LX/4CI;->c:LX/4C5;

    if-eqz v0, :cond_0

    .line 678635
    iget-object v0, p0, LX/4CI;->c:LX/4C5;

    invoke-interface {v0, p1}, LX/4C5;->a(Landroid/graphics/Rect;)V

    .line 678636
    :cond_0
    return-void
.end method

.method public final onLevelChange(I)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 678627
    iget-boolean v1, p0, LX/4CI;->e:Z

    if-eqz v1, :cond_1

    .line 678628
    :cond_0
    :goto_0
    return v0

    .line 678629
    :cond_1
    iget-wide v2, p0, LX/4CI;->g:J

    int-to-long v4, p1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 678630
    int-to-long v0, p1

    iput-wide v0, p0, LX/4CI;->g:J

    .line 678631
    invoke-virtual {p0}, LX/4CI;->invalidateSelf()V

    .line 678632
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 678620
    iget-object v0, p0, LX/4CI;->j:LX/1al;

    if-nez v0, :cond_0

    .line 678621
    new-instance v0, LX/1al;

    invoke-direct {v0}, LX/1al;-><init>()V

    iput-object v0, p0, LX/4CI;->j:LX/1al;

    .line 678622
    :cond_0
    iget-object v0, p0, LX/4CI;->j:LX/1al;

    .line 678623
    iput p1, v0, LX/1al;->a:I

    .line 678624
    iget-object v0, p0, LX/4CI;->c:LX/4C5;

    if-eqz v0, :cond_1

    .line 678625
    iget-object v0, p0, LX/4CI;->c:LX/4C5;

    invoke-interface {v0, p1}, LX/4C5;->a(I)V

    .line 678626
    :cond_1
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 678614
    iget-object v0, p0, LX/4CI;->j:LX/1al;

    if-nez v0, :cond_0

    .line 678615
    new-instance v0, LX/1al;

    invoke-direct {v0}, LX/1al;-><init>()V

    iput-object v0, p0, LX/4CI;->j:LX/1al;

    .line 678616
    :cond_0
    iget-object v0, p0, LX/4CI;->j:LX/1al;

    invoke-virtual {v0, p1}, LX/1al;->a(Landroid/graphics/ColorFilter;)V

    .line 678617
    iget-object v0, p0, LX/4CI;->c:LX/4C5;

    if-eqz v0, :cond_1

    .line 678618
    iget-object v0, p0, LX/4CI;->c:LX/4C5;

    invoke-interface {v0, p1}, LX/4C5;->a(Landroid/graphics/ColorFilter;)V

    .line 678619
    :cond_1
    return-void
.end method

.method public final start()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 678607
    iget-boolean v0, p0, LX/4CI;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/4CI;->c:LX/4C5;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4CI;->c:LX/4C5;

    invoke-interface {v0}, LX/4C4;->d()I

    move-result v0

    if-gt v0, v1, :cond_1

    .line 678608
    :cond_0
    :goto_0
    return-void

    .line 678609
    :cond_1
    iput-boolean v1, p0, LX/4CI;->e:Z

    .line 678610
    invoke-static {}, LX/4CI;->e()J

    move-result-wide v0

    iput-wide v0, p0, LX/4CI;->f:J

    .line 678611
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/4CI;->g:J

    .line 678612
    invoke-virtual {p0}, LX/4CI;->invalidateSelf()V

    .line 678613
    goto :goto_0
.end method

.method public final stop()V
    .locals 2

    .prologue
    .line 678598
    iget-boolean v0, p0, LX/4CI;->e:Z

    if-nez v0, :cond_0

    .line 678599
    :goto_0
    return-void

    .line 678600
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4CI;->e:Z

    .line 678601
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/4CI;->f:J

    .line 678602
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/4CI;->g:J

    .line 678603
    iget-object v0, p0, LX/4CI;->k:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, LX/4CI;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 678604
    goto :goto_0
.end method
