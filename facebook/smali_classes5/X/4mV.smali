.class public LX/4mV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/view/View;",
            "Lcom/facebook/ui/animations/ViewAnimator;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile b:LX/4mV;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 805989
    new-instance v0, Ljava/util/WeakHashMap;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/WeakHashMap;-><init>(I)V

    sput-object v0, LX/4mV;->a:Ljava/util/WeakHashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 805970
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 805971
    return-void
.end method

.method public static a(LX/0QB;)LX/4mV;
    .locals 3

    .prologue
    .line 805977
    sget-object v0, LX/4mV;->b:LX/4mV;

    if-nez v0, :cond_1

    .line 805978
    const-class v1, LX/4mV;

    monitor-enter v1

    .line 805979
    :try_start_0
    sget-object v0, LX/4mV;->b:LX/4mV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 805980
    if-eqz v2, :cond_0

    .line 805981
    :try_start_1
    new-instance v0, LX/4mV;

    invoke-direct {v0}, LX/4mV;-><init>()V

    .line 805982
    move-object v0, v0

    .line 805983
    sput-object v0, LX/4mV;->b:LX/4mV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 805984
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 805985
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 805986
    :cond_1
    sget-object v0, LX/4mV;->b:LX/4mV;

    return-object v0

    .line 805987
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 805988
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/View;)LX/4mU;
    .locals 2

    .prologue
    .line 805972
    sget-object v0, LX/4mV;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4mU;

    .line 805973
    if-nez v0, :cond_0

    .line 805974
    new-instance v0, LX/4mU;

    invoke-direct {v0, p1}, LX/4mU;-><init>(Landroid/view/View;)V

    .line 805975
    sget-object v1, LX/4mV;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 805976
    :cond_0
    return-object v0
.end method
