.class public LX/3ts;
.super Landroid/view/ViewGroup;
.source ""


# static fields
.field public static final a:LX/3to;


# instance fields
.field private b:I

.field private c:I

.field public d:Landroid/graphics/drawable/Drawable;

.field public e:Landroid/graphics/drawable/Drawable;

.field private final f:I

.field public g:Z

.field private h:Landroid/view/View;

.field public i:F

.field private j:F

.field private k:I

.field private l:Z

.field private m:I

.field private n:F

.field private o:F

.field private p:LX/3tm;

.field private final q:LX/3ty;

.field public r:Z

.field public s:Z

.field private final t:Landroid/graphics/Rect;

.field public final u:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/widget/SlidingPaneLayout$DisableLayerRunnable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 646733
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 646734
    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 646735
    new-instance v0, LX/3tr;

    invoke-direct {v0}, LX/3tr;-><init>()V

    sput-object v0, LX/3ts;->a:LX/3to;

    .line 646736
    :goto_0
    return-void

    .line 646737
    :cond_0
    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 646738
    new-instance v0, LX/3tq;

    invoke-direct {v0}, LX/3tq;-><init>()V

    sput-object v0, LX/3ts;->a:LX/3to;

    goto :goto_0

    .line 646739
    :cond_1
    new-instance v0, LX/3tp;

    invoke-direct {v0}, LX/3tp;-><init>()V

    sput-object v0, LX/3ts;->a:LX/3to;

    goto :goto_0
.end method

.method private static a(LX/3ts;Landroid/view/View;FI)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 646446
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3tl;

    .line 646447
    const/4 v1, 0x0

    cmpl-float v1, p2, v1

    if-lez v1, :cond_3

    if-eqz p3, :cond_3

    .line 646448
    const/high16 v1, -0x1000000

    and-int/2addr v1, p3

    ushr-int/lit8 v1, v1, 0x18

    .line 646449
    int-to-float v1, v1

    mul-float/2addr v1, p2

    float-to-int v1, v1

    .line 646450
    shl-int/lit8 v1, v1, 0x18

    const v2, 0xffffff

    and-int/2addr v2, p3

    or-int/2addr v1, v2

    .line 646451
    iget-object v2, v0, LX/3tl;->d:Landroid/graphics/Paint;

    if-nez v2, :cond_0

    .line 646452
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, v0, LX/3tl;->d:Landroid/graphics/Paint;

    .line 646453
    :cond_0
    iget-object v2, v0, LX/3tl;->d:Landroid/graphics/Paint;

    new-instance v3, Landroid/graphics/PorterDuffColorFilter;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v1, v4}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 646454
    invoke-static {p1}, LX/0vv;->g(Landroid/view/View;)I

    move-result v1

    if-eq v1, v5, :cond_1

    .line 646455
    iget-object v0, v0, LX/3tl;->d:Landroid/graphics/Paint;

    invoke-static {p1, v5, v0}, LX/0vv;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 646456
    :cond_1
    invoke-static {p0, p1}, LX/3ts;->c(LX/3ts;Landroid/view/View;)V

    .line 646457
    :cond_2
    :goto_0
    return-void

    .line 646458
    :cond_3
    invoke-static {p1}, LX/0vv;->g(Landroid/view/View;)I

    move-result v1

    if-eqz v1, :cond_2

    .line 646459
    iget-object v1, v0, LX/3tl;->d:Landroid/graphics/Paint;

    if-eqz v1, :cond_4

    .line 646460
    iget-object v0, v0, LX/3tl;->d:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 646461
    :cond_4
    new-instance v0, Landroid/support/v4/widget/SlidingPaneLayout$DisableLayerRunnable;

    invoke-direct {v0, p0, p1}, Landroid/support/v4/widget/SlidingPaneLayout$DisableLayerRunnable;-><init>(LX/3ts;Landroid/view/View;)V

    .line 646462
    iget-object v1, p0, LX/3ts;->u:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 646463
    invoke-static {p0, v0}, LX/0vv;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 17

    .prologue
    .line 646740
    invoke-static/range {p0 .. p0}, LX/3ts;->f(LX/3ts;)Z

    move-result v9

    .line 646741
    if-eqz v9, :cond_0

    invoke-virtual/range {p0 .. p0}, LX/3ts;->getWidth()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, LX/3ts;->getPaddingRight()I

    move-result v2

    sub-int v7, v1, v2

    .line 646742
    :goto_0
    if-eqz v9, :cond_1

    invoke-virtual/range {p0 .. p0}, LX/3ts;->getPaddingLeft()I

    move-result v1

    .line 646743
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/3ts;->getPaddingTop()I

    move-result v10

    .line 646744
    invoke-virtual/range {p0 .. p0}, LX/3ts;->getHeight()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, LX/3ts;->getPaddingBottom()I

    move-result v3

    sub-int v11, v2, v3

    .line 646745
    if-eqz p1, :cond_2

    invoke-static/range {p1 .. p1}, LX/3ts;->b(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 646746
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLeft()I

    move-result v5

    .line 646747
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getRight()I

    move-result v4

    .line 646748
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTop()I

    move-result v3

    .line 646749
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getBottom()I

    move-result v2

    .line 646750
    :goto_2
    const/4 v6, 0x0

    invoke-virtual/range {p0 .. p0}, LX/3ts;->getChildCount()I

    move-result v12

    move v8, v6

    :goto_3
    if-ge v8, v12, :cond_6

    .line 646751
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, LX/3ts;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    .line 646752
    move-object/from16 v0, p1

    if-eq v13, v0, :cond_6

    .line 646753
    if-eqz v9, :cond_3

    move v6, v1

    :goto_4
    invoke-virtual {v13}, Landroid/view/View;->getLeft()I

    move-result v14

    invoke-static {v6, v14}, Ljava/lang/Math;->max(II)I

    move-result v14

    .line 646754
    invoke-virtual {v13}, Landroid/view/View;->getTop()I

    move-result v6

    invoke-static {v10, v6}, Ljava/lang/Math;->max(II)I

    move-result v15

    .line 646755
    if-eqz v9, :cond_4

    move v6, v7

    :goto_5
    invoke-virtual {v13}, Landroid/view/View;->getRight()I

    move-result v16

    move/from16 v0, v16

    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 646756
    invoke-virtual {v13}, Landroid/view/View;->getBottom()I

    move-result v16

    move/from16 v0, v16

    invoke-static {v11, v0}, Ljava/lang/Math;->min(II)I

    move-result v16

    .line 646757
    if-lt v14, v5, :cond_5

    if-lt v15, v3, :cond_5

    if-gt v6, v4, :cond_5

    move/from16 v0, v16

    if-gt v0, v2, :cond_5

    .line 646758
    const/4 v6, 0x4

    .line 646759
    :goto_6
    invoke-virtual {v13, v6}, Landroid/view/View;->setVisibility(I)V

    .line 646760
    add-int/lit8 v6, v8, 0x1

    move v8, v6

    goto :goto_3

    .line 646761
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/3ts;->getPaddingLeft()I

    move-result v7

    goto :goto_0

    .line 646762
    :cond_1
    invoke-virtual/range {p0 .. p0}, LX/3ts;->getWidth()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, LX/3ts;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    goto/16 :goto_1

    .line 646763
    :cond_2
    const/4 v2, 0x0

    move v3, v2

    move v4, v2

    move v5, v2

    goto :goto_2

    :cond_3
    move v6, v7

    .line 646764
    goto :goto_4

    :cond_4
    move v6, v1

    .line 646765
    goto :goto_5

    .line 646766
    :cond_5
    const/4 v6, 0x0

    goto :goto_6

    .line 646767
    :cond_6
    return-void
.end method

.method public static a(LX/3ts;F)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 646768
    iget-boolean v0, p0, LX/3ts;->g:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 646769
    :goto_0
    return v0

    .line 646770
    :cond_0
    invoke-static {p0}, LX/3ts;->f(LX/3ts;)Z

    move-result v2

    .line 646771
    iget-object v0, p0, LX/3ts;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3tl;

    .line 646772
    if-eqz v2, :cond_3

    .line 646773
    invoke-virtual {p0}, LX/3ts;->getPaddingRight()I

    move-result v2

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v2

    .line 646774
    iget-object v2, p0, LX/3ts;->h:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    .line 646775
    invoke-virtual {p0}, LX/3ts;->getWidth()I

    move-result v3

    int-to-float v3, v3

    int-to-float v0, v0

    iget v4, p0, LX/3ts;->k:I

    int-to-float v4, v4

    mul-float/2addr v4, p1

    add-float/2addr v0, v4

    int-to-float v2, v2

    add-float/2addr v0, v2

    sub-float v0, v3, v0

    float-to-int v0, v0

    .line 646776
    :goto_1
    iget-object v2, p0, LX/3ts;->q:LX/3ty;

    iget-object v3, p0, LX/3ts;->h:Landroid/view/View;

    iget-object v4, p0, LX/3ts;->h:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {v2, v3, v0, v4}, LX/3ty;->a(Landroid/view/View;II)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 646777
    const/4 v1, 0x0

    .line 646778
    invoke-virtual {p0}, LX/3ts;->getChildCount()I

    move-result v2

    move v0, v1

    :goto_2
    if-ge v0, v2, :cond_2

    .line 646779
    invoke-virtual {p0, v0}, LX/3ts;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 646780
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/4 p1, 0x4

    if-ne v4, p1, :cond_1

    .line 646781
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 646782
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 646783
    :cond_2
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    .line 646784
    const/4 v0, 0x1

    goto :goto_0

    .line 646785
    :cond_3
    invoke-virtual {p0}, LX/3ts;->getPaddingLeft()I

    move-result v2

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v0, v2

    .line 646786
    int-to-float v0, v0

    iget v2, p0, LX/3ts;->k:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    add-float/2addr v0, v2

    float-to-int v0, v0

    goto :goto_1

    :cond_4
    move v0, v1

    .line 646787
    goto :goto_0
.end method

.method public static a(LX/3ts;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 646788
    iget-boolean v1, p0, LX/3ts;->s:Z

    if-nez v1, :cond_0

    const/4 v1, 0x0

    invoke-static {p0, v1}, LX/3ts;->a(LX/3ts;F)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 646789
    :cond_0
    iput-boolean v0, p0, LX/3ts;->r:Z

    .line 646790
    const/4 v0, 0x1

    .line 646791
    :cond_1
    return v0
.end method

.method private b(F)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    .line 646792
    invoke-static {p0}, LX/3ts;->f(LX/3ts;)Z

    move-result v3

    .line 646793
    iget-object v0, p0, LX/3ts;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3tl;

    .line 646794
    iget-boolean v2, v0, LX/3tl;->c:Z

    if-eqz v2, :cond_3

    if-eqz v3, :cond_2

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    :goto_0
    if-gtz v0, :cond_3

    const/4 v0, 0x1

    .line 646795
    :goto_1
    invoke-virtual {p0}, LX/3ts;->getChildCount()I

    move-result v4

    move v2, v1

    .line 646796
    :goto_2
    if-ge v2, v4, :cond_5

    .line 646797
    invoke-virtual {p0, v2}, LX/3ts;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 646798
    iget-object v1, p0, LX/3ts;->h:Landroid/view/View;

    if-eq v5, v1, :cond_1

    .line 646799
    iget v1, p0, LX/3ts;->j:F

    sub-float v1, v8, v1

    iget v6, p0, LX/3ts;->m:I

    int-to-float v6, v6

    mul-float/2addr v1, v6

    float-to-int v1, v1

    .line 646800
    iput p1, p0, LX/3ts;->j:F

    .line 646801
    sub-float v6, v8, p1

    iget v7, p0, LX/3ts;->m:I

    int-to-float v7, v7

    mul-float/2addr v6, v7

    float-to-int v6, v6

    .line 646802
    sub-int/2addr v1, v6

    .line 646803
    if-eqz v3, :cond_0

    neg-int v1, v1

    :cond_0
    invoke-virtual {v5, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 646804
    if-eqz v0, :cond_1

    .line 646805
    if-eqz v3, :cond_4

    iget v1, p0, LX/3ts;->j:F

    sub-float/2addr v1, v8

    :goto_3
    iget v6, p0, LX/3ts;->c:I

    invoke-static {p0, v5, v1, v6}, LX/3ts;->a(LX/3ts;Landroid/view/View;FI)V

    .line 646806
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 646807
    :cond_2
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 646808
    :cond_4
    iget v1, p0, LX/3ts;->j:F

    sub-float v1, v8, v1

    goto :goto_3

    .line 646809
    :cond_5
    return-void
.end method

.method private static b(Landroid/view/View;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 646810
    sget-object v2, LX/0vv;->a:LX/0w2;

    invoke-interface {v2, p0}, LX/0w2;->j(Landroid/view/View;)Z

    move-result v2

    move v2, v2

    .line 646811
    if-eqz v2, :cond_1

    .line 646812
    :cond_0
    :goto_0
    return v0

    .line 646813
    :cond_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-lt v2, v3, :cond_2

    move v0, v1

    goto :goto_0

    .line 646814
    :cond_2
    invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 646815
    if-eqz v2, :cond_3

    .line 646816
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 646817
    goto :goto_0
.end method

.method public static c(LX/3ts;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 646818
    sget-object v0, LX/3ts;->a:LX/3to;

    invoke-interface {v0, p0, p1}, LX/3to;->a(LX/3ts;Landroid/view/View;)V

    .line 646819
    return-void
.end method

.method private d(Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 646820
    if-nez p1, :cond_0

    move v0, v1

    .line 646821
    :goto_0
    return v0

    .line 646822
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3tl;

    .line 646823
    iget-boolean v2, p0, LX/3ts;->g:Z

    if-eqz v2, :cond_1

    iget-boolean v0, v0, LX/3tl;->c:Z

    if-eqz v0, :cond_1

    iget v0, p0, LX/3ts;->i:F

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private static f(LX/3ts;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 646875
    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 646824
    instance-of v0, p1, LX/3tl;

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final computeScroll()V
    .locals 2

    .prologue
    .line 646825
    iget-object v0, p0, LX/3ts;->q:LX/3ty;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/3ty;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 646826
    iget-boolean v0, p0, LX/3ts;->g:Z

    if-nez v0, :cond_1

    .line 646827
    iget-object v0, p0, LX/3ts;->q:LX/3ty;

    invoke-virtual {v0}, LX/3ty;->f()V

    .line 646828
    :cond_0
    :goto_0
    return-void

    .line 646829
    :cond_1
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 646830
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 646831
    invoke-static {p0}, LX/3ts;->f(LX/3ts;)Z

    move-result v0

    .line 646832
    if-eqz v0, :cond_1

    .line 646833
    iget-object v0, p0, LX/3ts;->e:Landroid/graphics/drawable/Drawable;

    .line 646834
    :goto_0
    invoke-virtual {p0}, LX/3ts;->getChildCount()I

    move-result v1

    if-le v1, v2, :cond_2

    invoke-virtual {p0, v2}, LX/3ts;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 646835
    :goto_1
    if-eqz v1, :cond_0

    if-nez v0, :cond_3

    .line 646836
    :cond_0
    :goto_2
    return-void

    .line 646837
    :cond_1
    iget-object v0, p0, LX/3ts;->d:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 646838
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 646839
    :cond_3
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    .line 646840
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v4

    .line 646841
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    .line 646842
    invoke-static {p0}, LX/3ts;->f(LX/3ts;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 646843
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v2

    .line 646844
    add-int v1, v2, v5

    .line 646845
    :goto_3
    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 646846
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_2

    .line 646847
    :cond_4
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 646848
    sub-int v2, v1, v5

    goto :goto_3
.end method

.method public final drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 646849
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3tl;

    .line 646850
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->save(I)I

    move-result v2

    .line 646851
    iget-boolean v3, p0, LX/3ts;->g:Z

    if-eqz v3, :cond_0

    iget-boolean v3, v0, LX/3tl;->b:Z

    if-nez v3, :cond_0

    iget-object v3, p0, LX/3ts;->h:Landroid/view/View;

    if-eqz v3, :cond_0

    .line 646852
    iget-object v3, p0, LX/3ts;->t:Landroid/graphics/Rect;

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    .line 646853
    invoke-static {p0}, LX/3ts;->f(LX/3ts;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 646854
    iget-object v3, p0, LX/3ts;->t:Landroid/graphics/Rect;

    iget-object v4, p0, LX/3ts;->t:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, LX/3ts;->h:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    iput v4, v3, Landroid/graphics/Rect;->left:I

    .line 646855
    :goto_0
    iget-object v3, p0, LX/3ts;->t:Landroid/graphics/Rect;

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 646856
    :cond_0
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-ge v3, v4, :cond_5

    .line 646857
    iget-boolean v3, v0, LX/3tl;->c:Z

    if-eqz v3, :cond_4

    iget v3, p0, LX/3ts;->i:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4

    .line 646858
    invoke-virtual {p2}, Landroid/view/View;->isDrawingCacheEnabled()Z

    move-result v3

    if-nez v3, :cond_1

    .line 646859
    const/4 v3, 0x1

    invoke-virtual {p2, v3}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 646860
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 646861
    if-eqz v3, :cond_3

    .line 646862
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v5

    int-to-float v5, v5

    iget-object v0, v0, LX/3tl;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4, v5, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    move v0, v1

    .line 646863
    :goto_1
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 646864
    return v0

    .line 646865
    :cond_2
    iget-object v3, p0, LX/3ts;->t:Landroid/graphics/Rect;

    iget-object v4, p0, LX/3ts;->t:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, LX/3ts;->h:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    iput v4, v3, Landroid/graphics/Rect;->right:I

    goto :goto_0

    .line 646866
    :cond_3
    const-string v0, "SlidingPaneLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "drawChild: child view "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " returned null drawing cache"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 646867
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    goto :goto_1

    .line 646868
    :cond_4
    invoke-virtual {p2}, Landroid/view/View;->isDrawingCacheEnabled()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 646869
    invoke-virtual {p2, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 646870
    :cond_5
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    goto :goto_1
.end method

.method public final generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 646871
    new-instance v0, LX/3tl;

    invoke-direct {v0}, LX/3tl;-><init>()V

    return-object v0
.end method

.method public final generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 646872
    new-instance v0, LX/3tl;

    invoke-virtual {p0}, LX/3ts;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/3tl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public final generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 646873
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_0

    new-instance v0, LX/3tl;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, LX/3tl;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/3tl;

    invoke-direct {v0, p1}, LX/3tl;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public getCoveredFadeColor()I
    .locals 1

    .prologue
    .line 646732
    iget v0, p0, LX/3ts;->c:I

    return v0
.end method

.method public getParallaxDistance()I
    .locals 1

    .prologue
    .line 646874
    iget v0, p0, LX/3ts;->m:I

    return v0
.end method

.method public getSliderFadeColor()I
    .locals 1

    .prologue
    .line 646445
    iget v0, p0, LX/3ts;->b:I

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x3a8de797

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 646464
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 646465
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/3ts;->s:Z

    .line 646466
    const/16 v1, 0x2d

    const v2, 0x53f60b0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2c

    const v2, -0x65d31951

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 646467
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 646468
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3ts;->s:Z

    .line 646469
    const/4 v0, 0x0

    iget-object v1, p0, LX/3ts;->u:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 646470
    iget-object v0, p0, LX/3ts;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SlidingPaneLayout$DisableLayerRunnable;

    .line 646471
    invoke-virtual {v0}, Landroid/support/v4/widget/SlidingPaneLayout$DisableLayerRunnable;->run()V

    .line 646472
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 646473
    :cond_0
    iget-object v0, p0, LX/3ts;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 646474
    const v0, 0x63460195

    invoke-static {v0, v2}, LX/02F;->g(II)V

    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 646475
    invoke-static {p1}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v3

    .line 646476
    iget-boolean v0, p0, LX/3ts;->g:Z

    if-nez v0, :cond_0

    if-nez v3, :cond_0

    invoke-virtual {p0}, LX/3ts;->getChildCount()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 646477
    invoke-virtual {p0, v1}, LX/3ts;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 646478
    if-eqz v0, :cond_0

    .line 646479
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-static {v0, v4, v5}, LX/3ty;->b(Landroid/view/View;II)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LX/3ts;->r:Z

    .line 646480
    :cond_0
    iget-boolean v0, p0, LX/3ts;->g:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/3ts;->l:Z

    if-eqz v0, :cond_4

    if-eqz v3, :cond_4

    .line 646481
    :cond_1
    iget-object v0, p0, LX/3ts;->q:LX/3ty;

    invoke-virtual {v0}, LX/3ty;->e()V

    .line 646482
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    .line 646483
    :cond_2
    :goto_1
    return v2

    :cond_3
    move v0, v2

    .line 646484
    goto :goto_0

    .line 646485
    :cond_4
    const/4 v0, 0x3

    if-eq v3, v0, :cond_5

    if-ne v3, v1, :cond_6

    .line 646486
    :cond_5
    iget-object v0, p0, LX/3ts;->q:LX/3ty;

    invoke-virtual {v0}, LX/3ty;->e()V

    goto :goto_1

    .line 646487
    :cond_6
    packed-switch v3, :pswitch_data_0

    :cond_7
    :pswitch_0
    move v0, v2

    .line 646488
    :goto_2
    iget-object v3, p0, LX/3ts;->q:LX/3ty;

    invoke-virtual {v3, p1}, LX/3ty;->a(Landroid/view/MotionEvent;)Z

    move-result v3

    .line 646489
    if-nez v3, :cond_8

    if-eqz v0, :cond_2

    :cond_8
    move v2, v1

    goto :goto_1

    .line 646490
    :pswitch_1
    iput-boolean v2, p0, LX/3ts;->l:Z

    .line 646491
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 646492
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 646493
    iput v0, p0, LX/3ts;->n:F

    .line 646494
    iput v3, p0, LX/3ts;->o:F

    .line 646495
    iget-object v4, p0, LX/3ts;->h:Landroid/view/View;

    float-to-int v0, v0

    float-to-int v3, v3

    invoke-static {v4, v0, v3}, LX/3ty;->b(Landroid/view/View;II)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/3ts;->h:Landroid/view/View;

    invoke-direct {p0, v0}, LX/3ts;->d(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    .line 646496
    goto :goto_2

    .line 646497
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 646498
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 646499
    iget v4, p0, LX/3ts;->n:F

    sub-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 646500
    iget v4, p0, LX/3ts;->o:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 646501
    iget-object v4, p0, LX/3ts;->q:LX/3ty;

    .line 646502
    iget v5, v4, LX/3ty;->b:I

    move v4, v5

    .line 646503
    int-to-float v4, v4

    cmpl-float v4, v0, v4

    if-lez v4, :cond_7

    cmpl-float v0, v3, v0

    if-lez v0, :cond_7

    .line 646504
    iget-object v0, p0, LX/3ts;->q:LX/3ty;

    invoke-virtual {v0}, LX/3ty;->e()V

    .line 646505
    iput-boolean v1, p0, LX/3ts;->l:Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final onLayout(ZIIII)V
    .locals 17

    .prologue
    .line 646506
    invoke-static/range {p0 .. p0}, LX/3ts;->f(LX/3ts;)Z

    move-result v9

    .line 646507
    if-eqz v9, :cond_1

    .line 646508
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3ts;->q:LX/3ty;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, LX/3ty;->a(I)V

    .line 646509
    :goto_0
    sub-int v10, p4, p2

    .line 646510
    if-eqz v9, :cond_2

    invoke-virtual/range {p0 .. p0}, LX/3ts;->getPaddingRight()I

    move-result v6

    .line 646511
    :goto_1
    if-eqz v9, :cond_3

    invoke-virtual/range {p0 .. p0}, LX/3ts;->getPaddingLeft()I

    move-result v1

    move v2, v1

    .line 646512
    :goto_2
    invoke-virtual/range {p0 .. p0}, LX/3ts;->getPaddingTop()I

    move-result v11

    .line 646513
    invoke-virtual/range {p0 .. p0}, LX/3ts;->getChildCount()I

    move-result v12

    .line 646514
    move-object/from16 v0, p0

    iget-boolean v1, v0, LX/3ts;->s:Z

    if-eqz v1, :cond_0

    .line 646515
    move-object/from16 v0, p0

    iget-boolean v1, v0, LX/3ts;->g:Z

    if-eqz v1, :cond_4

    move-object/from16 v0, p0

    iget-boolean v1, v0, LX/3ts;->r:Z

    if-eqz v1, :cond_4

    const/high16 v1, 0x3f800000    # 1.0f

    :goto_3
    move-object/from16 v0, p0

    iput v1, v0, LX/3ts;->i:F

    .line 646516
    :cond_0
    const/4 v1, 0x0

    move v8, v1

    move v7, v6

    :goto_4
    if-ge v8, v12, :cond_9

    .line 646517
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, LX/3ts;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    .line 646518
    invoke-virtual {v13}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v3, 0x8

    if-eq v1, v3, :cond_f

    .line 646519
    invoke-virtual {v13}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, LX/3tl;

    .line 646520
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    .line 646521
    const/4 v5, 0x0

    .line 646522
    iget-boolean v3, v1, LX/3tl;->b:Z

    if-eqz v3, :cond_7

    .line 646523
    iget v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v4, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v3, v4

    .line 646524
    sub-int v4, v10, v2

    move-object/from16 v0, p0

    iget v15, v0, LX/3ts;->f:I

    sub-int/2addr v4, v15

    invoke-static {v6, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    sub-int/2addr v4, v7

    sub-int v15, v4, v3

    .line 646525
    move-object/from16 v0, p0

    iput v15, v0, LX/3ts;->k:I

    .line 646526
    if-eqz v9, :cond_5

    iget v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 646527
    :goto_5
    add-int v4, v7, v3

    add-int/2addr v4, v15

    div-int/lit8 v16, v14, 0x2

    add-int v4, v4, v16

    sub-int v16, v10, v2

    move/from16 v0, v16

    if-le v4, v0, :cond_6

    const/4 v4, 0x1

    :goto_6
    iput-boolean v4, v1, LX/3tl;->c:Z

    .line 646528
    int-to-float v1, v15

    move-object/from16 v0, p0

    iget v4, v0, LX/3ts;->i:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    .line 646529
    add-int/2addr v3, v1

    add-int v4, v7, v3

    .line 646530
    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v3, v0, LX/3ts;->k:I

    int-to-float v3, v3

    div-float/2addr v1, v3

    move-object/from16 v0, p0

    iput v1, v0, LX/3ts;->i:F

    .line 646531
    :goto_7
    if-eqz v9, :cond_8

    .line 646532
    sub-int v1, v10, v4

    add-int v3, v1, v5

    .line 646533
    sub-int v1, v3, v14

    .line 646534
    :goto_8
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v11

    .line 646535
    invoke-virtual {v13, v1, v11, v3, v5}, Landroid/view/View;->layout(IIII)V

    .line 646536
    invoke-virtual {v13}, Landroid/view/View;->getWidth()I

    move-result v1

    add-int/2addr v1, v6

    move v3, v4

    .line 646537
    :goto_9
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    move v6, v1

    move v7, v3

    goto :goto_4

    .line 646538
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3ts;->q:LX/3ty;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/3ty;->a(I)V

    goto/16 :goto_0

    .line 646539
    :cond_2
    invoke-virtual/range {p0 .. p0}, LX/3ts;->getPaddingLeft()I

    move-result v6

    goto/16 :goto_1

    .line 646540
    :cond_3
    invoke-virtual/range {p0 .. p0}, LX/3ts;->getPaddingRight()I

    move-result v1

    move v2, v1

    goto/16 :goto_2

    .line 646541
    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 646542
    :cond_5
    iget v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto :goto_5

    .line 646543
    :cond_6
    const/4 v4, 0x0

    goto :goto_6

    .line 646544
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v1, v0, LX/3ts;->g:Z

    if-eqz v1, :cond_e

    move-object/from16 v0, p0

    iget v1, v0, LX/3ts;->m:I

    if-eqz v1, :cond_e

    .line 646545
    const/high16 v1, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v3, v0, LX/3ts;->i:F

    sub-float/2addr v1, v3

    move-object/from16 v0, p0

    iget v3, v0, LX/3ts;->m:I

    int-to-float v3, v3

    mul-float/2addr v1, v3

    float-to-int v1, v1

    :goto_a
    move v5, v1

    move v4, v6

    .line 646546
    goto :goto_7

    .line 646547
    :cond_8
    sub-int v1, v4, v5

    .line 646548
    add-int v3, v1, v14

    goto :goto_8

    .line 646549
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v1, v0, LX/3ts;->s:Z

    if-eqz v1, :cond_c

    .line 646550
    move-object/from16 v0, p0

    iget-boolean v1, v0, LX/3ts;->g:Z

    if-eqz v1, :cond_d

    .line 646551
    move-object/from16 v0, p0

    iget v1, v0, LX/3ts;->m:I

    if-eqz v1, :cond_a

    .line 646552
    move-object/from16 v0, p0

    iget v1, v0, LX/3ts;->i:F

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, LX/3ts;->b(F)V

    .line 646553
    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3ts;->h:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, LX/3tl;

    iget-boolean v1, v1, LX/3tl;->c:Z

    if-eqz v1, :cond_b

    .line 646554
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3ts;->h:Landroid/view/View;

    move-object/from16 v0, p0

    iget v2, v0, LX/3ts;->i:F

    move-object/from16 v0, p0

    iget v3, v0, LX/3ts;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v1, v2, v3}, LX/3ts;->a(LX/3ts;Landroid/view/View;FI)V

    .line 646555
    :cond_b
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3ts;->h:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, LX/3ts;->a(Landroid/view/View;)V

    .line 646556
    :cond_c
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, LX/3ts;->s:Z

    .line 646557
    return-void

    .line 646558
    :cond_d
    const/4 v1, 0x0

    :goto_b
    if-ge v1, v12, :cond_b

    .line 646559
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, LX/3ts;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, LX/3ts;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3, v4}, LX/3ts;->a(LX/3ts;Landroid/view/View;FI)V

    .line 646560
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_e
    move v1, v5

    goto :goto_a

    :cond_f
    move v1, v6

    move v3, v7

    goto/16 :goto_9
.end method

.method public final onMeasure(II)V
    .locals 17

    .prologue
    .line 646561
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    .line 646562
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 646563
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 646564
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 646565
    const/high16 v5, 0x40000000    # 2.0f

    if-eq v4, v5, :cond_2

    .line 646566
    invoke-virtual/range {p0 .. p0}, LX/3ts;->isInEditMode()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 646567
    const/high16 v5, -0x80000000

    if-eq v4, v5, :cond_1e

    .line 646568
    if-nez v4, :cond_1e

    .line 646569
    const/16 v3, 0x12c

    move v10, v2

    move v12, v3

    move v3, v1

    .line 646570
    :goto_0
    const/4 v2, 0x0

    .line 646571
    const/4 v1, -0x1

    .line 646572
    sparse-switch v10, :sswitch_data_0

    move/from16 v16, v1

    move v1, v2

    move/from16 v2, v16

    .line 646573
    :goto_1
    const/4 v4, 0x0

    .line 646574
    const/4 v7, 0x0

    .line 646575
    invoke-virtual/range {p0 .. p0}, LX/3ts;->getPaddingLeft()I

    move-result v3

    sub-int v3, v12, v3

    invoke-virtual/range {p0 .. p0}, LX/3ts;->getPaddingRight()I

    move-result v5

    sub-int v11, v3, v5

    .line 646576
    invoke-virtual/range {p0 .. p0}, LX/3ts;->getChildCount()I

    move-result v13

    .line 646577
    const/4 v3, 0x2

    if-le v13, v3, :cond_0

    .line 646578
    const-string v3, "SlidingPaneLayout"

    const-string v5, "onMeasure: More than two child views are not supported."

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 646579
    :cond_0
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, LX/3ts;->h:Landroid/view/View;

    .line 646580
    const/4 v3, 0x0

    move v9, v3

    move v6, v11

    move v8, v1

    move v3, v4

    :goto_2
    if-ge v9, v13, :cond_d

    .line 646581
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, LX/3ts;->getChildAt(I)Landroid/view/View;

    move-result-object v14

    .line 646582
    invoke-virtual {v14}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, LX/3tl;

    .line 646583
    invoke-virtual {v14}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-ne v4, v5, :cond_4

    .line 646584
    const/4 v4, 0x0

    iput-boolean v4, v1, LX/3tl;->c:Z

    move v1, v6

    move v4, v3

    move v5, v8

    move v3, v7

    .line 646585
    :goto_3
    add-int/lit8 v6, v9, 0x1

    move v9, v6

    move v7, v3

    move v8, v5

    move v3, v4

    move v6, v1

    goto :goto_2

    .line 646586
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Width must have an exact value or MATCH_PARENT"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 646587
    :cond_2
    if-nez v2, :cond_1e

    .line 646588
    invoke-virtual/range {p0 .. p0}, LX/3ts;->isInEditMode()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 646589
    if-nez v2, :cond_1e

    .line 646590
    const/high16 v2, -0x80000000

    .line 646591
    const/16 v1, 0x12c

    move v10, v2

    move v12, v3

    move v3, v1

    goto :goto_0

    .line 646592
    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Height must not be UNSPECIFIED"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 646593
    :sswitch_0
    invoke-virtual/range {p0 .. p0}, LX/3ts;->getPaddingTop()I

    move-result v1

    sub-int v1, v3, v1

    invoke-virtual/range {p0 .. p0}, LX/3ts;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    move v2, v1

    .line 646594
    goto :goto_1

    .line 646595
    :sswitch_1
    invoke-virtual/range {p0 .. p0}, LX/3ts;->getPaddingTop()I

    move-result v1

    sub-int v1, v3, v1

    invoke-virtual/range {p0 .. p0}, LX/3ts;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v1, v3

    move/from16 v16, v1

    move v1, v2

    move/from16 v2, v16

    goto/16 :goto_1

    .line 646596
    :cond_4
    iget v4, v1, LX/3tl;->a:F

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-lez v4, :cond_5

    .line 646597
    iget v4, v1, LX/3tl;->a:F

    add-float/2addr v3, v4

    .line 646598
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-eqz v4, :cond_1d

    .line 646599
    :cond_5
    iget v4, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v4, v5

    .line 646600
    iget v5, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v15, -0x2

    if-ne v5, v15, :cond_8

    .line 646601
    sub-int v4, v11, v4

    const/high16 v5, -0x80000000

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 646602
    :goto_4
    iget v5, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v15, -0x2

    if-ne v5, v15, :cond_a

    .line 646603
    const/high16 v5, -0x80000000

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 646604
    :goto_5
    invoke-virtual {v14, v4, v5}, Landroid/view/View;->measure(II)V

    .line 646605
    invoke-virtual {v14}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 646606
    invoke-virtual {v14}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    .line 646607
    const/high16 v15, -0x80000000

    if-ne v10, v15, :cond_6

    if-le v5, v8, :cond_6

    .line 646608
    invoke-static {v5, v2}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 646609
    :cond_6
    sub-int v5, v6, v4

    .line 646610
    if-gez v5, :cond_c

    const/4 v4, 0x1

    :goto_6
    iput-boolean v4, v1, LX/3tl;->b:Z

    or-int/2addr v4, v7

    .line 646611
    iget-boolean v1, v1, LX/3tl;->b:Z

    if-eqz v1, :cond_7

    .line 646612
    move-object/from16 v0, p0

    iput-object v14, v0, LX/3ts;->h:Landroid/view/View;

    :cond_7
    move v1, v5

    move v5, v8

    move/from16 v16, v3

    move v3, v4

    move/from16 v4, v16

    goto/16 :goto_3

    .line 646613
    :cond_8
    iget v5, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v15, -0x1

    if-ne v5, v15, :cond_9

    .line 646614
    sub-int v4, v11, v4

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    goto :goto_4

    .line 646615
    :cond_9
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    goto :goto_4

    .line 646616
    :cond_a
    iget v5, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v15, -0x1

    if-ne v5, v15, :cond_b

    .line 646617
    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    goto :goto_5

    .line 646618
    :cond_b
    iget v5, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/high16 v15, 0x40000000    # 2.0f

    invoke-static {v5, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    goto :goto_5

    .line 646619
    :cond_c
    const/4 v4, 0x0

    goto :goto_6

    .line 646620
    :cond_d
    if-nez v7, :cond_e

    const/4 v1, 0x0

    cmpl-float v1, v3, v1

    if-lez v1, :cond_1b

    .line 646621
    :cond_e
    move-object/from16 v0, p0

    iget v1, v0, LX/3ts;->f:I

    sub-int v14, v11, v1

    .line 646622
    const/4 v1, 0x0

    move v10, v1

    :goto_7
    if-ge v10, v13, :cond_1b

    .line 646623
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, LX/3ts;->getChildAt(I)Landroid/view/View;

    move-result-object v15

    .line 646624
    invoke-virtual {v15}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v4, 0x8

    if-eq v1, v4, :cond_10

    .line 646625
    invoke-virtual {v15}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, LX/3tl;

    .line 646626
    invoke-virtual {v15}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_10

    .line 646627
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-nez v4, :cond_11

    iget v4, v1, LX/3tl;->a:F

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-lez v4, :cond_11

    const/4 v4, 0x1

    move v9, v4

    .line 646628
    :goto_8
    if-eqz v9, :cond_12

    const/4 v4, 0x0

    move v5, v4

    .line 646629
    :goto_9
    if-eqz v7, :cond_16

    move-object/from16 v0, p0

    iget-object v4, v0, LX/3ts;->h:Landroid/view/View;

    if-eq v15, v4, :cond_16

    .line 646630
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-gez v4, :cond_10

    if-gt v5, v14, :cond_f

    iget v4, v1, LX/3tl;->a:F

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-lez v4, :cond_10

    .line 646631
    :cond_f
    if-eqz v9, :cond_15

    .line 646632
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v5, -0x2

    if-ne v4, v5, :cond_13

    .line 646633
    const/high16 v1, -0x80000000

    invoke-static {v2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 646634
    :goto_a
    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v14, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 646635
    invoke-virtual {v15, v4, v1}, Landroid/view/View;->measure(II)V

    .line 646636
    :cond_10
    :goto_b
    add-int/lit8 v1, v10, 0x1

    move v10, v1

    goto :goto_7

    .line 646637
    :cond_11
    const/4 v4, 0x0

    move v9, v4

    goto :goto_8

    .line 646638
    :cond_12
    invoke-virtual {v15}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    move v5, v4

    goto :goto_9

    .line 646639
    :cond_13
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_14

    .line 646640
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    goto :goto_a

    .line 646641
    :cond_14
    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    goto :goto_a

    .line 646642
    :cond_15
    invoke-virtual {v15}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    goto :goto_a

    .line 646643
    :cond_16
    iget v4, v1, LX/3tl;->a:F

    const/4 v9, 0x0

    cmpl-float v4, v4, v9

    if-lez v4, :cond_10

    .line 646644
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-nez v4, :cond_19

    .line 646645
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v9, -0x2

    if-ne v4, v9, :cond_17

    .line 646646
    const/high16 v4, -0x80000000

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 646647
    :goto_c
    if-eqz v7, :cond_1a

    .line 646648
    iget v9, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v1, v9

    .line 646649
    sub-int v1, v11, v1

    .line 646650
    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    .line 646651
    if-eq v5, v1, :cond_10

    .line 646652
    invoke-virtual {v15, v9, v4}, Landroid/view/View;->measure(II)V

    goto :goto_b

    .line 646653
    :cond_17
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v9, -0x1

    if-ne v4, v9, :cond_18

    .line 646654
    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    goto :goto_c

    .line 646655
    :cond_18
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v4, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    goto :goto_c

    .line 646656
    :cond_19
    invoke-virtual {v15}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v4, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    goto :goto_c

    .line 646657
    :cond_1a
    const/4 v9, 0x0

    invoke-static {v9, v6}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 646658
    iget v1, v1, LX/3tl;->a:F

    int-to-float v9, v9

    mul-float/2addr v1, v9

    div-float/2addr v1, v3

    float-to-int v1, v1

    .line 646659
    add-int/2addr v1, v5

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 646660
    invoke-virtual {v15, v1, v4}, Landroid/view/View;->measure(II)V

    goto/16 :goto_b

    .line 646661
    :cond_1b
    invoke-virtual/range {p0 .. p0}, LX/3ts;->getPaddingTop()I

    move-result v1

    add-int/2addr v1, v8

    invoke-virtual/range {p0 .. p0}, LX/3ts;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    .line 646662
    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v1}, LX/3ts;->setMeasuredDimension(II)V

    .line 646663
    move-object/from16 v0, p0

    iput-boolean v7, v0, LX/3ts;->g:Z

    .line 646664
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3ts;->q:LX/3ty;

    invoke-virtual {v1}, LX/3ty;->a()I

    move-result v1

    if-eqz v1, :cond_1c

    if-nez v7, :cond_1c

    .line 646665
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3ts;->q:LX/3ty;

    invoke-virtual {v1}, LX/3ty;->f()V

    .line 646666
    :cond_1c
    return-void

    :cond_1d
    move v1, v6

    move v4, v3

    move v5, v8

    move v3, v7

    goto/16 :goto_3

    :cond_1e
    move v10, v2

    move v12, v3

    move v3, v1

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    .prologue
    .line 646667
    check-cast p1, Landroid/support/v4/widget/SlidingPaneLayout$SavedState;

    .line 646668
    invoke-virtual {p1}, Landroid/support/v4/widget/SlidingPaneLayout$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 646669
    iget-boolean v0, p1, Landroid/support/v4/widget/SlidingPaneLayout$SavedState;->a:Z

    if-eqz v0, :cond_1

    .line 646670
    const/4 v1, 0x1

    .line 646671
    iget-boolean v2, p0, LX/3ts;->s:Z

    if-nez v2, :cond_0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {p0, v2}, LX/3ts;->a(LX/3ts;F)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 646672
    :cond_0
    iput-boolean v1, p0, LX/3ts;->r:Z

    .line 646673
    :goto_0
    iget-boolean v0, p1, Landroid/support/v4/widget/SlidingPaneLayout$SavedState;->a:Z

    iput-boolean v0, p0, LX/3ts;->r:Z

    .line 646674
    return-void

    .line 646675
    :cond_1
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/3ts;->a(LX/3ts;I)Z

    .line 646676
    goto :goto_0

    :cond_2
    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 646677
    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 646678
    new-instance v1, Landroid/support/v4/widget/SlidingPaneLayout$SavedState;

    invoke-direct {v1, v0}, Landroid/support/v4/widget/SlidingPaneLayout$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 646679
    iget-boolean v0, p0, LX/3ts;->g:Z

    move v0, v0

    .line 646680
    if-eqz v0, :cond_1

    .line 646681
    iget-boolean v0, p0, LX/3ts;->g:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/3ts;->i:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v2

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 646682
    :goto_1
    iput-boolean v0, v1, Landroid/support/v4/widget/SlidingPaneLayout$SavedState;->a:Z

    .line 646683
    return-object v1

    .line 646684
    :cond_1
    iget-boolean v0, p0, LX/3ts;->r:Z

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x749d3998

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 646685
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    .line 646686
    if-eq p1, p3, :cond_0

    .line 646687
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/3ts;->s:Z

    .line 646688
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x798adf42

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x2

    const v1, -0x6164ca52

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 646689
    iget-boolean v2, p0, LX/3ts;->g:Z

    if-nez v2, :cond_0

    .line 646690
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0x8fd9514

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 646691
    :goto_0
    return v0

    .line 646692
    :cond_0
    iget-object v2, p0, LX/3ts;->q:LX/3ty;

    invoke-virtual {v2, p1}, LX/3ty;->b(Landroid/view/MotionEvent;)V

    .line 646693
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 646694
    and-int/lit16 v2, v2, 0xff

    packed-switch v2, :pswitch_data_0

    .line 646695
    :cond_1
    :goto_1
    const v2, 0x28b187f5

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 646696
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 646697
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 646698
    iput v2, p0, LX/3ts;->n:F

    .line 646699
    iput v3, p0, LX/3ts;->o:F

    goto :goto_1

    .line 646700
    :pswitch_1
    iget-object v2, p0, LX/3ts;->h:Landroid/view/View;

    invoke-direct {p0, v2}, LX/3ts;->d(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 646701
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 646702
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 646703
    iget v4, p0, LX/3ts;->n:F

    sub-float v4, v2, v4

    .line 646704
    iget v5, p0, LX/3ts;->o:F

    sub-float v5, v3, v5

    .line 646705
    iget-object v6, p0, LX/3ts;->q:LX/3ty;

    .line 646706
    iget p1, v6, LX/3ty;->b:I

    move v6, p1

    .line 646707
    mul-float/2addr v4, v4

    mul-float/2addr v5, v5

    add-float/2addr v4, v5

    mul-int v5, v6, v6

    int-to-float v5, v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_1

    iget-object v4, p0, LX/3ts;->h:Landroid/view/View;

    float-to-int v2, v2

    float-to-int v3, v3

    invoke-static {v4, v2, v3}, LX/3ty;->b(Landroid/view/View;II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 646708
    const/4 v2, 0x0

    invoke-static {p0, v2}, LX/3ts;->a(LX/3ts;I)Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 646709
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 646710
    invoke-virtual {p0}, LX/3ts;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/3ts;->g:Z

    if-nez v0, :cond_0

    .line 646711
    iget-object v0, p0, LX/3ts;->h:Landroid/view/View;

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/3ts;->r:Z

    .line 646712
    :cond_0
    return-void

    .line 646713
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCoveredFadeColor(I)V
    .locals 0

    .prologue
    .line 646714
    iput p1, p0, LX/3ts;->c:I

    .line 646715
    return-void
.end method

.method public setPanelSlideListener(LX/3tm;)V
    .locals 0

    .prologue
    .line 646716
    iput-object p1, p0, LX/3ts;->p:LX/3tm;

    .line 646717
    return-void
.end method

.method public setParallaxDistance(I)V
    .locals 0

    .prologue
    .line 646718
    iput p1, p0, LX/3ts;->m:I

    .line 646719
    invoke-virtual {p0}, LX/3ts;->requestLayout()V

    .line 646720
    return-void
.end method

.method public setShadowResource(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 646721
    invoke-virtual {p0}, LX/3ts;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 646722
    iput-object v0, p0, LX/3ts;->d:Landroid/graphics/drawable/Drawable;

    .line 646723
    return-void
.end method

.method public setShadowResourceLeft(I)V
    .locals 1

    .prologue
    .line 646724
    invoke-virtual {p0}, LX/3ts;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 646725
    iput-object v0, p0, LX/3ts;->d:Landroid/graphics/drawable/Drawable;

    .line 646726
    return-void
.end method

.method public setShadowResourceRight(I)V
    .locals 1

    .prologue
    .line 646727
    invoke-virtual {p0}, LX/3ts;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 646728
    iput-object v0, p0, LX/3ts;->e:Landroid/graphics/drawable/Drawable;

    .line 646729
    return-void
.end method

.method public setSliderFadeColor(I)V
    .locals 0

    .prologue
    .line 646730
    iput p1, p0, LX/3ts;->b:I

    .line 646731
    return-void
.end method
