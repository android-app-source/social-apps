.class public LX/43H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/43C;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/2Qx;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 668545
    const-class v0, LX/43H;

    sput-object v0, LX/43H;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/2Qx;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 668594
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 668595
    iput-object p1, p0, LX/43H;->b:Landroid/content/Context;

    .line 668596
    iput-object p2, p0, LX/43H;->c:LX/2Qx;

    .line 668597
    return-void
.end method

.method public static b(LX/0QB;)LX/43H;
    .locals 3

    .prologue
    .line 668592
    new-instance v2, LX/43H;

    const-class v0, Landroid/content/Context;

    const-class v1, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/2Qx;->a(LX/0QB;)LX/2Qx;

    move-result-object v1

    check-cast v1, LX/2Qx;

    invoke-direct {v2, v0, v1}, LX/43H;-><init>(Landroid/content/Context;LX/2Qx;)V

    .line 668593
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;IILX/43G;Z)LX/43G;
    .locals 1

    .prologue
    .line 668591
    invoke-virtual {p0, p1, p2, p5}, LX/43H;->a(Ljava/lang/String;Ljava/lang/String;LX/43G;)LX/43G;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/43G;)LX/43G;
    .locals 7

    .prologue
    .line 668598
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 668599
    :try_start_0
    iget-object v0, p0, LX/43H;->c:LX/2Qx;

    iget-object v1, p0, LX/43H;->b:Landroid/content/Context;

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget v4, p3, LX/43G;->a:I

    iget v5, p3, LX/43G;->b:I

    iget v6, p3, LX/43G;->c:I

    invoke-virtual/range {v0 .. v6}, LX/2Qx;->a(Landroid/content/Context;Ljava/io/File;Ljava/io/File;III)Z
    :try_end_0
    .catch LX/42w; {:try_start_0 .. :try_end_0} :catch_0

    .line 668600
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 668601
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 668602
    invoke-static {p2, v0}, LX/436;->a(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 668603
    new-instance v1, LX/43G;

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iget v3, p3, LX/43G;->c:I

    invoke-direct {v1, v2, v0, v3}, LX/43G;-><init>(III)V

    return-object v1

    .line 668604
    :catch_0
    move-exception v0

    .line 668605
    new-instance v1, Lcom/facebook/bitmaps/ImageResizer$ImageResizingException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "J/scaleJpegFile "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Lcom/facebook/bitmaps/ImageResizer$ImageResizingException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Z)V

    throw v1
.end method

.method public final a(Ljava/lang/String;IIIZ)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    .line 668582
    :try_start_0
    iget-object v0, p0, LX/43H;->c:LX/2Qx;

    iget-object v1, p0, LX/43H;->b:Landroid/content/Context;

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, LX/2Qx;->a(Landroid/content/Context;Landroid/net/Uri;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch LX/42y; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/430; {:try_start_0 .. :try_end_0} :catch_1
    .catch LX/42x; {:try_start_0 .. :try_end_0} :catch_2
    .catch LX/42z; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    return-object v0

    .line 668583
    :catch_0
    move-exception v0

    .line 668584
    new-instance v1, LX/439;

    const-string v2, "J/scaleImage"

    invoke-direct {v1, v2, v0}, LX/439;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 668585
    :catch_1
    move-exception v0

    .line 668586
    new-instance v1, LX/43B;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "J/scaleImage "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/43B;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 668587
    :catch_2
    move-exception v0

    .line 668588
    new-instance v1, LX/438;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "J/scaleImage "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/438;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 668589
    :catch_3
    move-exception v0

    .line 668590
    new-instance v1, LX/43A;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "J/scaleImage "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/43A;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Ljava/lang/String;LX/431;)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 668549
    invoke-static {p1}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v3

    .line 668550
    iget v0, v3, LX/434;->b:I

    int-to-float v0, v0

    iget v2, v3, LX/434;->a:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 668551
    iget v2, p2, LX/431;->a:I

    const/16 v4, 0x5a

    if-eq v2, v4, :cond_0

    iget v2, p2, LX/431;->a:I

    const/16 v4, 0x10e

    if-ne v2, v4, :cond_1

    .line 668552
    :cond_0
    iget v0, v3, LX/434;->a:I

    int-to-float v0, v0

    iget v2, v3, LX/434;->b:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 668553
    :cond_1
    iget v2, p2, LX/431;->b:I

    int-to-float v2, v2

    iget v4, p2, LX/431;->c:I

    int-to-float v4, v4

    div-float/2addr v2, v4

    .line 668554
    sub-float/2addr v0, v2

    :try_start_0
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v2, 0x3d4ccccd    # 0.05f

    cmpg-float v0, v0, v2

    if-gez v0, :cond_3

    move v2, v1

    .line 668555
    :goto_0
    iget v0, p2, LX/431;->b:I

    iget v1, p2, LX/431;->c:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 668556
    if-eqz v2, :cond_4

    iget-object v1, p0, LX/43H;->c:LX/2Qx;

    iget-object v3, p0, LX/43H;->b:Landroid/content/Context;

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3, v4, v0}, LX/2Qx;->a(Landroid/content/Context;Ljava/io/File;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 668557
    :goto_1
    if-eqz v1, :cond_7

    iget v0, p2, LX/431;->a:I

    if-eqz v0, :cond_7

    .line 668558
    iget v0, p2, LX/431;->a:I

    const/4 v3, 0x1

    invoke-static {v1, v0, v3}, LX/2Qx;->a(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 668559
    if-eq v0, v1, :cond_2

    .line 668560
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 668561
    :cond_2
    :goto_2
    if-eqz v2, :cond_6

    .line 668562
    :goto_3
    return-object v0

    .line 668563
    :cond_3
    const/4 v0, 0x0

    move v2, v0

    goto :goto_0

    .line 668564
    :cond_4
    iget v1, v3, LX/434;->b:I

    iget v4, v3, LX/434;->a:I

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 668565
    const/4 v1, 0x1

    .line 668566
    :goto_4
    if-le v4, v0, :cond_5

    .line 668567
    add-int/lit8 v1, v1, 0x1

    .line 668568
    div-int/2addr v4, v1

    goto :goto_4

    .line 668569
    :cond_5
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 668570
    add-int/lit8 v1, v1, 0x1

    iput v1, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 668571
    invoke-static {p1, v4}, LX/436;->a(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    move-object v1, v1

    .line 668572
    goto :goto_1

    .line 668573
    :cond_6
    iget v1, p2, LX/431;->b:I

    iget v2, p2, LX/431;->c:I

    invoke-static {v0, v1, v2}, Landroid/media/ThumbnailUtils;->extractThumbnail(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    :try_end_0
    .catch LX/42y; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/430; {:try_start_0 .. :try_end_0} :catch_1
    .catch LX/42x; {:try_start_0 .. :try_end_0} :catch_2
    .catch LX/42z; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    goto :goto_3

    .line 668574
    :catch_0
    move-exception v0

    .line 668575
    new-instance v1, LX/439;

    const-string v2, "J/getThumbnail"

    invoke-direct {v1, v2, v0}, LX/439;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 668576
    :catch_1
    move-exception v0

    .line 668577
    new-instance v1, LX/43B;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "J/getThumbnail "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/43B;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 668578
    :catch_2
    move-exception v0

    .line 668579
    new-instance v1, LX/438;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "J/getThumbnail "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/438;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 668580
    :catch_3
    move-exception v0

    .line 668581
    new-instance v1, LX/43A;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "J/getThumbnail "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/43A;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_7
    move-object v0, v1

    goto :goto_2
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 668546
    iget-object v0, p0, LX/43H;->c:LX/2Qx;

    .line 668547
    iput-boolean p1, v0, LX/2Qx;->a:Z

    .line 668548
    return-void
.end method
