.class public final LX/50x;
.super LX/1q0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1q0",
        "<TR;",
        "Ljava/util/Map",
        "<TC;TV;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0kB;


# direct methods
.method public constructor <init>(LX/0kB;)V
    .locals 0

    .prologue
    .line 824196
    iput-object p1, p0, LX/50x;->a:LX/0kB;

    invoke-direct {p0}, LX/1q0;-><init>()V

    .line 824197
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TR;",
            "Ljava/util/Map",
            "<TC;TV;>;>;>;"
        }
    .end annotation

    .prologue
    .line 824198
    new-instance v0, LX/50w;

    invoke-direct {v0, p0}, LX/50w;-><init>(LX/50x;)V

    return-object v0
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 824199
    iget-object v0, p0, LX/50x;->a:LX/0kB;

    invoke-virtual {v0, p1}, LX/0kB;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 824200
    iget-object v0, p0, LX/50x;->a:LX/0kB;

    invoke-virtual {v0, p1}, LX/0kB;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/50x;->a:LX/0kB;

    invoke-virtual {v0, p1}, LX/0kB;->c(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 824201
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/50x;->a:LX/0kB;

    iget-object v0, v0, LX/0kB;->backingMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    goto :goto_0
.end method
