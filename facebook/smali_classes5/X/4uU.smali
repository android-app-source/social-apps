.class public final LX/4uU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2wY;


# instance fields
.field public final synthetic a:LX/4uW;


# direct methods
.method public constructor <init>(LX/4uW;)V
    .locals 0

    iput-object p1, p0, LX/4uU;->a:LX/4uW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(IZ)V
    .locals 2

    iget-object v0, p0, LX/4uU;->a:LX/4uW;

    iget-object v0, v0, LX/4uW;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, LX/4uU;->a:LX/4uW;

    iget-boolean v0, v0, LX/4uW;->l:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/4uU;->a:LX/4uW;

    iget-object v0, v0, LX/4uW;->k:Lcom/google/android/gms/common/ConnectionResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4uU;->a:LX/4uW;

    iget-object v0, v0, LX/4uW;->k:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {v0}, Lcom/google/android/gms/common/ConnectionResult;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/4uU;->a:LX/4uW;

    const/4 v1, 0x0

    iput-boolean v1, v0, LX/4uW;->l:Z

    iget-object v0, p0, LX/4uU;->a:LX/4uW;

    invoke-static {v0, p1, p2}, LX/4uW;->a$redex0(LX/4uW;IZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, LX/4uU;->a:LX/4uW;

    iget-object v0, v0, LX/4uW;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_0
    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, LX/4uU;->a:LX/4uW;

    const/4 v1, 0x1

    iput-boolean v1, v0, LX/4uW;->l:Z

    iget-object v0, p0, LX/4uU;->a:LX/4uW;

    iget-object v0, v0, LX/4uW;->e:LX/2wm;

    invoke-virtual {v0, p1}, LX/2wm;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, LX/4uU;->a:LX/4uW;

    iget-object v0, v0, LX/4uW;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/4uU;->a:LX/4uW;

    iget-object v1, v1, LX/4uW;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, LX/4uU;->a:LX/4uW;

    iget-object v0, v0, LX/4uW;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, LX/4uU;->a:LX/4uW;

    iget-object v1, v0, LX/4uW;->i:Landroid/os/Bundle;

    if-nez v1, :cond_1

    iput-object p1, v0, LX/4uW;->i:Landroid/os/Bundle;

    :cond_0
    :goto_0
    iget-object v0, p0, LX/4uU;->a:LX/4uW;

    sget-object v1, Lcom/google/android/gms/common/ConnectionResult;->a:Lcom/google/android/gms/common/ConnectionResult;

    iput-object v1, v0, LX/4uW;->j:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v0, p0, LX/4uU;->a:LX/4uW;

    invoke-static {v0}, LX/4uW;->j(LX/4uW;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, LX/4uU;->a:LX/4uW;

    iget-object v0, v0, LX/4uW;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/4uU;->a:LX/4uW;

    iget-object v1, v1, LX/4uW;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_1
    if-eqz p1, :cond_0

    iget-object v1, v0, LX/4uW;->i:Landroid/os/Bundle;

    invoke-virtual {v1, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, LX/4uU;->a:LX/4uW;

    iget-object v0, v0, LX/4uW;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, LX/4uU;->a:LX/4uW;

    iput-object p1, v0, LX/4uW;->j:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v0, p0, LX/4uU;->a:LX/4uW;

    invoke-static {v0}, LX/4uW;->j(LX/4uW;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, LX/4uU;->a:LX/4uW;

    iget-object v0, v0, LX/4uW;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/4uU;->a:LX/4uW;

    iget-object v1, v1, LX/4uW;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
