.class public LX/4zE;
.super Ljava/util/AbstractList;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractList",
        "<",
        "Ljava/util/List",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final b:I


# direct methods
.method public constructor <init>(Ljava/util/List;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;I)V"
        }
    .end annotation

    .prologue
    .line 822593
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 822594
    iput-object p1, p0, LX/4zE;->a:Ljava/util/List;

    .line 822595
    iput p2, p0, LX/4zE;->b:I

    .line 822596
    return-void
.end method


# virtual methods
.method public final get(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 822589
    invoke-virtual {p0}, LX/4zE;->size()I

    move-result v0

    invoke-static {p1, v0}, LX/0PB;->checkElementIndex(II)I

    .line 822590
    iget v0, p0, LX/4zE;->b:I

    mul-int/2addr v0, p1

    .line 822591
    iget v1, p0, LX/4zE;->b:I

    add-int/2addr v1, v0

    iget-object v2, p0, LX/4zE;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 822592
    iget-object v2, p0, LX/4zE;->a:Ljava/util/List;

    invoke-interface {v2, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 822588
    iget-object v0, p0, LX/4zE;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final size()I
    .locals 3

    .prologue
    .line 822587
    iget-object v0, p0, LX/4zE;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, LX/4zE;->b:I

    sget-object v2, Ljava/math/RoundingMode;->CEILING:Ljava/math/RoundingMode;

    invoke-static {v0, v1, v2}, LX/1IS;->a(IILjava/math/RoundingMode;)I

    move-result v0

    return v0
.end method
