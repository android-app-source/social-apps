.class public LX/4ps;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0lE;
.implements Ljava/io/Serializable;


# static fields
.field public static final a:LX/0lZ;

.field private static final serialVersionUID:J = -0x617d35a8b1013b0cL


# instance fields
.field public final _config:LX/0m2;

.field public final _jsonFactory:LX/0lp;

.field public final _prettyPrinter:LX/0lZ;

.field public final _rootSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final _rootType:LX/0lJ;

.field public final _schema:LX/4pX;

.field public final _serializerFactory:LX/0nS;

.field public final _serializerProvider:LX/0mx;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 812117
    new-instance v0, LX/4pk;

    invoke-direct {v0}, LX/4pk;-><init>()V

    sput-object v0, LX/4ps;->a:LX/0lZ;

    return-void
.end method

.method public constructor <init>(LX/0lC;LX/0m2;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 812107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 812108
    iput-object p2, p0, LX/4ps;->_config:LX/0m2;

    .line 812109
    iget-object v0, p1, LX/0lC;->_serializerProvider:LX/0mx;

    iput-object v0, p0, LX/4ps;->_serializerProvider:LX/0mx;

    .line 812110
    iget-object v0, p1, LX/0lC;->_serializerFactory:LX/0nS;

    iput-object v0, p0, LX/4ps;->_serializerFactory:LX/0nS;

    .line 812111
    iget-object v0, p1, LX/0lC;->_jsonFactory:LX/0lp;

    iput-object v0, p0, LX/4ps;->_jsonFactory:LX/0lp;

    .line 812112
    iput-object v1, p0, LX/4ps;->_rootType:LX/0lJ;

    .line 812113
    iput-object v1, p0, LX/4ps;->_rootSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 812114
    iput-object v1, p0, LX/4ps;->_prettyPrinter:LX/0lZ;

    .line 812115
    iput-object v1, p0, LX/4ps;->_schema:LX/4pX;

    .line 812116
    return-void
.end method

.method public constructor <init>(LX/0lC;LX/0m2;LX/0lJ;LX/0lZ;)V
    .locals 1

    .prologue
    .line 812095
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 812096
    iput-object p2, p0, LX/4ps;->_config:LX/0m2;

    .line 812097
    iget-object v0, p1, LX/0lC;->_serializerProvider:LX/0mx;

    iput-object v0, p0, LX/4ps;->_serializerProvider:LX/0mx;

    .line 812098
    iget-object v0, p1, LX/0lC;->_serializerFactory:LX/0nS;

    iput-object v0, p0, LX/4ps;->_serializerFactory:LX/0nS;

    .line 812099
    iget-object v0, p1, LX/0lC;->_jsonFactory:LX/0lp;

    iput-object v0, p0, LX/4ps;->_jsonFactory:LX/0lp;

    .line 812100
    if-eqz p3, :cond_0

    .line 812101
    invoke-virtual {p3}, LX/0lJ;->b()LX/0lJ;

    move-result-object p3

    .line 812102
    :cond_0
    iput-object p3, p0, LX/4ps;->_rootType:LX/0lJ;

    .line 812103
    iput-object p4, p0, LX/4ps;->_prettyPrinter:LX/0lZ;

    .line 812104
    const/4 v0, 0x0

    iput-object v0, p0, LX/4ps;->_schema:LX/4pX;

    .line 812105
    invoke-direct {p0, p2, p3}, LX/4ps;->a(LX/0m2;LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    iput-object v0, p0, LX/4ps;->_rootSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 812106
    return-void
.end method

.method private constructor <init>(LX/4ps;LX/0m2;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonSerializer;LX/0lZ;LX/4pX;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4ps;",
            "LX/0m2;",
            "LX/0lJ;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "LX/0lZ;",
            "LX/4pX;",
            ")V"
        }
    .end annotation

    .prologue
    .line 812085
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 812086
    iput-object p2, p0, LX/4ps;->_config:LX/0m2;

    .line 812087
    iget-object v0, p1, LX/4ps;->_serializerProvider:LX/0mx;

    iput-object v0, p0, LX/4ps;->_serializerProvider:LX/0mx;

    .line 812088
    iget-object v0, p1, LX/4ps;->_serializerFactory:LX/0nS;

    iput-object v0, p0, LX/4ps;->_serializerFactory:LX/0nS;

    .line 812089
    iget-object v0, p1, LX/4ps;->_jsonFactory:LX/0lp;

    iput-object v0, p0, LX/4ps;->_jsonFactory:LX/0lp;

    .line 812090
    iput-object p3, p0, LX/4ps;->_rootType:LX/0lJ;

    .line 812091
    iput-object p4, p0, LX/4ps;->_rootSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 812092
    iput-object p5, p0, LX/4ps;->_prettyPrinter:LX/0lZ;

    .line 812093
    iput-object p6, p0, LX/4ps;->_schema:LX/4pX;

    .line 812094
    return-void
.end method

.method private a(LX/0m2;)LX/0mx;
    .locals 2

    .prologue
    .line 812084
    iget-object v0, p0, LX/4ps;->_serializerProvider:LX/0mx;

    iget-object v1, p0, LX/4ps;->_serializerFactory:LX/0nS;

    invoke-virtual {v0, p1, v1}, LX/0mx;->a(LX/0m2;LX/0nS;)LX/0mx;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/0lZ;)LX/4ps;
    .locals 7

    .prologue
    .line 812079
    iget-object v0, p0, LX/4ps;->_prettyPrinter:LX/0lZ;

    if-ne p1, v0, :cond_0

    .line 812080
    :goto_0
    return-object p0

    .line 812081
    :cond_0
    if-nez p1, :cond_1

    .line 812082
    sget-object v5, LX/4ps;->a:LX/0lZ;

    .line 812083
    :goto_1
    new-instance v0, LX/4ps;

    iget-object v2, p0, LX/4ps;->_config:LX/0m2;

    iget-object v3, p0, LX/4ps;->_rootType:LX/0lJ;

    iget-object v4, p0, LX/4ps;->_rootSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    iget-object v6, p0, LX/4ps;->_schema:LX/4pX;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, LX/4ps;-><init>(LX/4ps;LX/0m2;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonSerializer;LX/0lZ;LX/4pX;)V

    move-object p0, v0

    goto :goto_0

    :cond_1
    move-object v5, p1

    goto :goto_1
.end method

.method private a(LX/0m2;LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m2;",
            "LX/0lJ;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 812026
    if-eqz p2, :cond_0

    iget-object v1, p0, LX/4ps;->_config:LX/0m2;

    sget-object v2, LX/0mt;->EAGER_SERIALIZER_FETCH:LX/0mt;

    invoke-virtual {v1, v2}, LX/0m2;->c(LX/0mt;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 812027
    :cond_0
    :goto_0
    return-object v0

    .line 812028
    :cond_1
    :try_start_0
    invoke-direct {p0, p1}, LX/4ps;->a(LX/0m2;)LX/0mx;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, p2, v2, v3}, LX/0my;->a(LX/0lJ;ZLX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 812029
    :catch_0
    goto :goto_0
.end method

.method private a(LX/0nX;)V
    .locals 2

    .prologue
    .line 812067
    iget-object v0, p0, LX/4ps;->_prettyPrinter:LX/0lZ;

    if-eqz v0, :cond_4

    .line 812068
    iget-object v0, p0, LX/4ps;->_prettyPrinter:LX/0lZ;

    .line 812069
    sget-object v1, LX/4ps;->a:LX/0lZ;

    if-ne v0, v1, :cond_2

    .line 812070
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/0nX;->a(LX/0lZ;)LX/0nX;

    .line 812071
    :cond_0
    :goto_0
    iget-object v0, p0, LX/4ps;->_schema:LX/4pX;

    if-eqz v0, :cond_1

    .line 812072
    iget-object v0, p0, LX/4ps;->_schema:LX/4pX;

    invoke-virtual {p1, v0}, LX/0nX;->a(LX/4pX;)V

    .line 812073
    :cond_1
    return-void

    .line 812074
    :cond_2
    instance-of v1, v0, LX/0la;

    if-eqz v1, :cond_3

    .line 812075
    check-cast v0, LX/0la;

    invoke-interface {v0}, LX/0la;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lZ;

    .line 812076
    :cond_3
    invoke-virtual {p1, v0}, LX/0nX;->a(LX/0lZ;)LX/0nX;

    goto :goto_0

    .line 812077
    :cond_4
    iget-object v0, p0, LX/4ps;->_config:LX/0m2;

    sget-object v1, LX/0mt;->INDENT_OUTPUT:LX/0mt;

    invoke-virtual {v0, v1}, LX/0m2;->c(LX/0mt;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 812078
    invoke-virtual {p1}, LX/0nX;->c()LX/0nX;

    goto :goto_0
.end method

.method private a(LX/0nX;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 812054
    invoke-direct {p0, p1}, LX/4ps;->a(LX/0nX;)V

    .line 812055
    iget-object v0, p0, LX/4ps;->_config:LX/0m2;

    sget-object v1, LX/0mt;->CLOSE_CLOSEABLE:LX/0mt;

    invoke-virtual {v0, v1}, LX/0m2;->c(LX/0mt;)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p2, Ljava/io/Closeable;

    if-eqz v0, :cond_0

    .line 812056
    iget-object v0, p0, LX/4ps;->_config:LX/0m2;

    invoke-direct {p0, p1, p2, v0}, LX/4ps;->a(LX/0nX;Ljava/lang/Object;LX/0m2;)V

    .line 812057
    :goto_0
    return-void

    .line 812058
    :cond_0
    const/4 v1, 0x0

    .line 812059
    :try_start_0
    iget-object v0, p0, LX/4ps;->_rootType:LX/0lJ;

    if-nez v0, :cond_2

    .line 812060
    iget-object v0, p0, LX/4ps;->_config:LX/0m2;

    invoke-direct {p0, v0}, LX/4ps;->a(LX/0m2;)LX/0mx;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LX/0mx;->a(LX/0nX;Ljava/lang/Object;)V

    .line 812061
    :goto_1
    const/4 v1, 0x1

    .line 812062
    invoke-virtual {p1}, LX/0nX;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 812063
    :catchall_0
    move-exception v0

    if-nez v1, :cond_1

    .line 812064
    :try_start_1
    invoke-virtual {p1}, LX/0nX;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 812065
    :cond_1
    :goto_2
    throw v0

    .line 812066
    :cond_2
    :try_start_2
    iget-object v0, p0, LX/4ps;->_config:LX/0m2;

    invoke-direct {p0, v0}, LX/4ps;->a(LX/0m2;)LX/0mx;

    move-result-object v0

    iget-object v2, p0, LX/4ps;->_rootType:LX/0lJ;

    iget-object v3, p0, LX/4ps;->_rootSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-virtual {v0, p1, p2, v2, v3}, LX/0mx;->a(LX/0nX;Ljava/lang/Object;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catch_0
    goto :goto_2
.end method

.method private final a(LX/0nX;Ljava/lang/Object;LX/0m2;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 812041
    move-object v0, p2

    check-cast v0, Ljava/io/Closeable;

    .line 812042
    :try_start_0
    iget-object v1, p0, LX/4ps;->_rootType:LX/0lJ;

    if-nez v1, :cond_0

    .line 812043
    invoke-direct {p0, p3}, LX/4ps;->a(LX/0m2;)LX/0mx;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, LX/0mx;->a(LX/0nX;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 812044
    :goto_0
    :try_start_1
    invoke-virtual {p1}, LX/0nX;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 812045
    :try_start_2
    invoke-interface {v0}, Ljava/io/Closeable;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 812046
    return-void

    .line 812047
    :cond_0
    :try_start_3
    invoke-direct {p0, p3}, LX/4ps;->a(LX/0m2;)LX/0mx;

    move-result-object v1

    iget-object v3, p0, LX/4ps;->_rootType:LX/0lJ;

    iget-object v4, p0, LX/4ps;->_rootSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-virtual {v1, p1, p2, v3, v4}, LX/0mx;->a(LX/0nX;Ljava/lang/Object;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 812048
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_1
    if-eqz p1, :cond_1

    .line 812049
    :try_start_4
    invoke-virtual {p1}, LX/0nX;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 812050
    :cond_1
    :goto_2
    if-eqz v1, :cond_2

    .line 812051
    :try_start_5
    invoke-interface {v1}, Ljava/io/Closeable;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 812052
    :cond_2
    :goto_3
    throw v0

    :catch_0
    goto :goto_2

    :catch_1
    goto :goto_3

    .line 812053
    :catchall_1
    move-exception v1

    move-object p1, v2

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    goto :goto_1

    :catchall_2
    move-exception v0

    move-object v1, v2

    move-object p1, v2

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/4ps;
    .locals 1

    .prologue
    .line 812040
    new-instance v0, LX/0lY;

    invoke-direct {v0}, LX/0lY;-><init>()V

    invoke-direct {p0, v0}, LX/4ps;->a(LX/0lZ;)LX/4ps;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 812033
    new-instance v0, LX/2Ae;

    invoke-static {}, LX/0lp;->b()LX/12B;

    move-result-object v1

    invoke-direct {v0, v1}, LX/2Ae;-><init>(LX/12B;)V

    .line 812034
    :try_start_0
    iget-object v1, p0, LX/4ps;->_jsonFactory:LX/0lp;

    invoke-virtual {v1, v0}, LX/0lp;->a(Ljava/io/Writer;)LX/0nX;

    move-result-object v1

    invoke-direct {p0, v1, p1}, LX/4ps;->a(LX/0nX;Ljava/lang/Object;)V
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 812035
    invoke-virtual {v0}, LX/2Ae;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 812036
    :catch_0
    move-exception v0

    .line 812037
    throw v0

    .line 812038
    :catch_1
    move-exception v0

    .line 812039
    invoke-static {v0}, LX/28E;->a(Ljava/io/IOException;)LX/28E;

    move-result-object v0

    throw v0
.end method

.method public final a(Ljava/io/File;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 812031
    iget-object v0, p0, LX/4ps;->_jsonFactory:LX/0lp;

    sget-object v1, LX/1pL;->UTF8:LX/1pL;

    invoke-virtual {v0, p1, v1}, LX/0lp;->a(Ljava/io/File;LX/1pL;)LX/0nX;

    move-result-object v0

    invoke-direct {p0, v0, p2}, LX/4ps;->a(LX/0nX;Ljava/lang/Object;)V

    .line 812032
    return-void
.end method

.method public final version()LX/0ne;
    .locals 1

    .prologue
    .line 812030
    sget-object v0, LX/4pz;->VERSION:LX/0ne;

    return-object v0
.end method
