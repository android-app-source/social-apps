.class public LX/4TI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 717666
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 28

    .prologue
    .line 717667
    const/16 v24, 0x0

    .line 717668
    const/16 v23, 0x0

    .line 717669
    const/16 v22, 0x0

    .line 717670
    const/16 v21, 0x0

    .line 717671
    const/16 v20, 0x0

    .line 717672
    const/16 v19, 0x0

    .line 717673
    const/16 v18, 0x0

    .line 717674
    const/16 v17, 0x0

    .line 717675
    const/16 v16, 0x0

    .line 717676
    const/4 v15, 0x0

    .line 717677
    const/4 v14, 0x0

    .line 717678
    const/4 v13, 0x0

    .line 717679
    const/4 v12, 0x0

    .line 717680
    const/4 v11, 0x0

    .line 717681
    const/4 v10, 0x0

    .line 717682
    const/4 v9, 0x0

    .line 717683
    const/4 v8, 0x0

    .line 717684
    const/4 v7, 0x0

    .line 717685
    const/4 v6, 0x0

    .line 717686
    const/4 v5, 0x0

    .line 717687
    const/4 v4, 0x0

    .line 717688
    const/4 v3, 0x0

    .line 717689
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v25

    sget-object v26, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_1

    .line 717690
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 717691
    const/4 v3, 0x0

    .line 717692
    :goto_0
    return v3

    .line 717693
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 717694
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v25

    sget-object v26, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_c

    .line 717695
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v25

    .line 717696
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 717697
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v26

    sget-object v27, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-eq v0, v1, :cond_1

    if-eqz v25, :cond_1

    .line 717698
    const-string v26, "best_post_reach"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_2

    .line 717699
    const/4 v13, 0x1

    .line 717700
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v24

    goto :goto_1

    .line 717701
    :cond_2
    const-string v26, "engaged_user_count"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_3

    .line 717702
    const/4 v12, 0x1

    .line 717703
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v23

    goto :goto_1

    .line 717704
    :cond_3
    const-string v26, "linkClicks"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_4

    .line 717705
    const/4 v11, 0x1

    .line 717706
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v22

    goto :goto_1

    .line 717707
    :cond_4
    const-string v26, "organic_reach"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_5

    .line 717708
    const/4 v10, 0x1

    .line 717709
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v21

    goto :goto_1

    .line 717710
    :cond_5
    const-string v26, "otherClicks"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_6

    .line 717711
    const/4 v9, 0x1

    .line 717712
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v20

    goto :goto_1

    .line 717713
    :cond_6
    const-string v26, "paid_reach"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_7

    .line 717714
    const/4 v8, 0x1

    .line 717715
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v19

    goto :goto_1

    .line 717716
    :cond_7
    const-string v26, "photoViews"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_8

    .line 717717
    const/4 v7, 0x1

    .line 717718
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v18

    goto/16 :goto_1

    .line 717719
    :cond_8
    const-string v26, "totalClicks"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_9

    .line 717720
    const/4 v6, 0x1

    .line 717721
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v17

    goto/16 :goto_1

    .line 717722
    :cond_9
    const-string v26, "total_reach"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_a

    .line 717723
    const/4 v5, 0x1

    .line 717724
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v16

    goto/16 :goto_1

    .line 717725
    :cond_a
    const-string v26, "videoPlays"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_b

    .line 717726
    const/4 v4, 0x1

    .line 717727
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v15

    goto/16 :goto_1

    .line 717728
    :cond_b
    const-string v26, "messaging_reply"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_0

    .line 717729
    const/4 v3, 0x1

    .line 717730
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    goto/16 :goto_1

    .line 717731
    :cond_c
    const/16 v25, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 717732
    if-eqz v13, :cond_d

    .line 717733
    const/4 v13, 0x0

    const/16 v25, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v13, v1, v2}, LX/186;->a(III)V

    .line 717734
    :cond_d
    if-eqz v12, :cond_e

    .line 717735
    const/4 v12, 0x1

    const/4 v13, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v12, v1, v13}, LX/186;->a(III)V

    .line 717736
    :cond_e
    if-eqz v11, :cond_f

    .line 717737
    const/4 v11, 0x2

    const/4 v12, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v11, v1, v12}, LX/186;->a(III)V

    .line 717738
    :cond_f
    if-eqz v10, :cond_10

    .line 717739
    const/4 v10, 0x3

    const/4 v11, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v10, v1, v11}, LX/186;->a(III)V

    .line 717740
    :cond_10
    if-eqz v9, :cond_11

    .line 717741
    const/4 v9, 0x4

    const/4 v10, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v9, v1, v10}, LX/186;->a(III)V

    .line 717742
    :cond_11
    if-eqz v8, :cond_12

    .line 717743
    const/4 v8, 0x5

    const/4 v9, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v8, v1, v9}, LX/186;->a(III)V

    .line 717744
    :cond_12
    if-eqz v7, :cond_13

    .line 717745
    const/4 v7, 0x6

    const/4 v8, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v7, v1, v8}, LX/186;->a(III)V

    .line 717746
    :cond_13
    if-eqz v6, :cond_14

    .line 717747
    const/4 v6, 0x7

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v6, v1, v7}, LX/186;->a(III)V

    .line 717748
    :cond_14
    if-eqz v5, :cond_15

    .line 717749
    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v5, v1, v6}, LX/186;->a(III)V

    .line 717750
    :cond_15
    if-eqz v4, :cond_16

    .line 717751
    const/16 v4, 0x9

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15, v5}, LX/186;->a(III)V

    .line 717752
    :cond_16
    if-eqz v3, :cond_17

    .line 717753
    const/16 v3, 0xa

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14, v4}, LX/186;->a(III)V

    .line 717754
    :cond_17
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 717755
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 717756
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 717757
    if-eqz v0, :cond_0

    .line 717758
    const-string v1, "best_post_reach"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717759
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 717760
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 717761
    if-eqz v0, :cond_1

    .line 717762
    const-string v1, "engaged_user_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717763
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 717764
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 717765
    if-eqz v0, :cond_2

    .line 717766
    const-string v1, "linkClicks"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717767
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 717768
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 717769
    if-eqz v0, :cond_3

    .line 717770
    const-string v1, "organic_reach"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717771
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 717772
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 717773
    if-eqz v0, :cond_4

    .line 717774
    const-string v1, "otherClicks"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717775
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 717776
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 717777
    if-eqz v0, :cond_5

    .line 717778
    const-string v1, "paid_reach"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717779
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 717780
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 717781
    if-eqz v0, :cond_6

    .line 717782
    const-string v1, "photoViews"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717783
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 717784
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 717785
    if-eqz v0, :cond_7

    .line 717786
    const-string v1, "totalClicks"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717787
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 717788
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 717789
    if-eqz v0, :cond_8

    .line 717790
    const-string v1, "total_reach"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717791
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 717792
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 717793
    if-eqz v0, :cond_9

    .line 717794
    const-string v1, "videoPlays"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717795
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 717796
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 717797
    if-eqz v0, :cond_a

    .line 717798
    const-string v1, "messaging_reply"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717799
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 717800
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 717801
    return-void
.end method
