.class public LX/446;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/1R7;

.field private b:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 669432
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 669433
    const/4 v0, 0x0

    iput v0, p0, LX/446;->b:I

    .line 669434
    invoke-static {p1}, LX/1R7;->a(I)LX/1R7;

    move-result-object v0

    iput-object v0, p0, LX/446;->a:LX/1R7;

    .line 669435
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 669436
    iget v0, p0, LX/446;->b:I

    if-nez v0, :cond_0

    .line 669437
    new-instance v0, Ljava/util/EmptyStackException;

    invoke-direct {v0}, Ljava/util/EmptyStackException;-><init>()V

    throw v0

    .line 669438
    :cond_0
    iget-object v0, p0, LX/446;->a:LX/1R7;

    iget v1, p0, LX/446;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LX/1R7;->c(I)I

    move-result v0

    .line 669439
    iget v1, p0, LX/446;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/446;->b:I

    .line 669440
    return v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 669441
    iget v0, p0, LX/446;->b:I

    iget-object v1, p0, LX/446;->a:LX/1R7;

    iget v1, v1, LX/1R7;->b:I

    if-ge v0, v1, :cond_0

    .line 669442
    iget-object v0, p0, LX/446;->a:LX/1R7;

    iget-object v0, v0, LX/1R7;->a:[I

    iget v1, p0, LX/446;->b:I

    aput p1, v0, v1

    .line 669443
    :goto_0
    iget v0, p0, LX/446;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/446;->b:I

    .line 669444
    return-void

    .line 669445
    :cond_0
    iget-object v0, p0, LX/446;->a:LX/1R7;

    invoke-virtual {v0, p1}, LX/1R7;->b(I)V

    goto :goto_0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 669446
    iget v0, p0, LX/446;->b:I

    if-nez v0, :cond_0

    .line 669447
    new-instance v0, Ljava/util/EmptyStackException;

    invoke-direct {v0}, Ljava/util/EmptyStackException;-><init>()V

    throw v0

    .line 669448
    :cond_0
    iget-object v0, p0, LX/446;->a:LX/1R7;

    iget v1, p0, LX/446;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LX/1R7;->c(I)I

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 669449
    const/4 v0, 0x0

    iput v0, p0, LX/446;->b:I

    .line 669450
    return-void
.end method
