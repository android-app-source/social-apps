.class public final LX/47J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/398;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/398;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method private constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 672063
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 672064
    iput-object p1, p0, LX/47J;->a:LX/0QB;

    .line 672065
    return-void
.end method

.method public static a(LX/0QB;)LX/0Or;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/398;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 672066
    new-instance v0, LX/47J;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1}, LX/47J;-><init>(LX/0QB;)V

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 672067
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/47J;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 672068
    packed-switch p2, :pswitch_data_0

    .line 672069
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 672070
    :pswitch_0
    invoke-static {p1}, LX/GCf;->a(LX/0QB;)LX/GCf;

    move-result-object v0

    .line 672071
    :goto_0
    return-object v0

    .line 672072
    :pswitch_1
    invoke-static {p1}, LX/GNV;->a(LX/0QB;)LX/GNV;

    move-result-object v0

    goto :goto_0

    .line 672073
    :pswitch_2
    invoke-static {p1}, LX/GNZ;->a(LX/0QB;)LX/GNZ;

    move-result-object v0

    goto :goto_0

    .line 672074
    :pswitch_3
    invoke-static {p1}, LX/ADR;->a(LX/0QB;)LX/ADR;

    move-result-object v0

    goto :goto_0

    .line 672075
    :pswitch_4
    invoke-static {p1}, LX/GRA;->a(LX/0QB;)LX/GRA;

    move-result-object v0

    goto :goto_0

    .line 672076
    :pswitch_5
    invoke-static {p1}, LX/GTg;->a(LX/0QB;)LX/GTg;

    move-result-object v0

    goto :goto_0

    .line 672077
    :pswitch_6
    invoke-static {p1}, LX/BbD;->a(LX/0QB;)LX/BbD;

    move-result-object v0

    goto :goto_0

    .line 672078
    :pswitch_7
    invoke-static {p1}, LX/Bby;->a(LX/0QB;)LX/Bby;

    move-result-object v0

    goto :goto_0

    .line 672079
    :pswitch_8
    invoke-static {p1}, LX/GWg;->a(LX/0QB;)LX/GWg;

    move-result-object v0

    goto :goto_0

    .line 672080
    :pswitch_9
    invoke-static {p1}, LX/GXP;->a(LX/0QB;)LX/GXP;

    move-result-object v0

    goto :goto_0

    .line 672081
    :pswitch_a
    invoke-static {p1}, LX/GZz;->a(LX/0QB;)LX/GZz;

    move-result-object v0

    goto :goto_0

    .line 672082
    :pswitch_b
    invoke-static {p1}, LX/GaV;->a(LX/0QB;)LX/GaV;

    move-result-object v0

    goto :goto_0

    .line 672083
    :pswitch_c
    invoke-static {p1}, LX/EjA;->a(LX/0QB;)LX/EjA;

    move-result-object v0

    goto :goto_0

    .line 672084
    :pswitch_d
    invoke-static {p1}, LX/Bgh;->a(LX/0QB;)LX/Bgh;

    move-result-object v0

    goto :goto_0

    .line 672085
    :pswitch_e
    invoke-static {p1}, LX/5N3;->a(LX/0QB;)LX/5N3;

    move-result-object v0

    goto :goto_0

    .line 672086
    :pswitch_f
    invoke-static {p1}, LX/I0B;->b(LX/0QB;)LX/I0B;

    move-result-object v0

    goto :goto_0

    .line 672087
    :pswitch_10
    invoke-static {p1}, LX/Erh;->a(LX/0QB;)LX/Erh;

    move-result-object v0

    goto :goto_0

    .line 672088
    :pswitch_11
    invoke-static {p1}, LX/IAq;->a(LX/0QB;)LX/IAq;

    move-result-object v0

    goto :goto_0

    .line 672089
    :pswitch_12
    invoke-static {p1}, LX/IBt;->a(LX/0QB;)LX/IBt;

    move-result-object v0

    goto :goto_0

    .line 672090
    :pswitch_13
    invoke-static {p1}, LX/IBv;->a(LX/0QB;)LX/IBv;

    move-result-object v0

    goto :goto_0

    .line 672091
    :pswitch_14
    invoke-static {p1}, LX/IBw;->a(LX/0QB;)LX/IBw;

    move-result-object v0

    goto :goto_0

    .line 672092
    :pswitch_15
    invoke-static {p1}, LX/IBy;->a(LX/0QB;)LX/IBy;

    move-result-object v0

    goto :goto_0

    .line 672093
    :pswitch_16
    invoke-static {p1}, LX/IC1;->a(LX/0QB;)LX/IC1;

    move-result-object v0

    goto :goto_0

    .line 672094
    :pswitch_17
    invoke-static {p1}, LX/Aiw;->a(LX/0QB;)LX/Aiw;

    move-result-object v0

    goto :goto_0

    .line 672095
    :pswitch_18
    invoke-static {p1}, LX/Erv;->a(LX/0QB;)LX/Erv;

    move-result-object v0

    goto :goto_0

    .line 672096
    :pswitch_19
    invoke-static {p1}, LX/Ami;->a(LX/0QB;)LX/Ami;

    move-result-object v0

    goto :goto_0

    .line 672097
    :pswitch_1a
    invoke-static {p1}, LX/AnM;->a(LX/0QB;)LX/AnM;

    move-result-object v0

    goto/16 :goto_0

    .line 672098
    :pswitch_1b
    invoke-static {p1}, LX/CAv;->a(LX/0QB;)LX/CAv;

    move-result-object v0

    goto/16 :goto_0

    .line 672099
    :pswitch_1c
    invoke-static {p1}, LX/EyO;->a(LX/0QB;)LX/EyO;

    move-result-object v0

    goto/16 :goto_0

    .line 672100
    :pswitch_1d
    new-instance v0, LX/Eyu;

    invoke-direct {v0}, LX/Eyu;-><init>()V

    .line 672101
    move-object v0, v0

    .line 672102
    move-object v0, v0

    .line 672103
    goto/16 :goto_0

    .line 672104
    :pswitch_1e
    invoke-static {p1}, LX/ID0;->a(LX/0QB;)LX/ID0;

    move-result-object v0

    goto/16 :goto_0

    .line 672105
    :pswitch_1f
    invoke-static {p1}, LX/IDk;->a(LX/0QB;)LX/IDk;

    move-result-object v0

    goto/16 :goto_0

    .line 672106
    :pswitch_20
    invoke-static {p1}, LX/IF7;->a(LX/0QB;)LX/IF7;

    move-result-object v0

    goto/16 :goto_0

    .line 672107
    :pswitch_21
    invoke-static {p1}, LX/IJt;->a(LX/0QB;)LX/IJt;

    move-result-object v0

    goto/16 :goto_0

    .line 672108
    :pswitch_22
    invoke-static {p1}, LX/JaP;->a(LX/0QB;)LX/JaP;

    move-result-object v0

    goto/16 :goto_0

    .line 672109
    :pswitch_23
    invoke-static {p1}, LX/IOj;->a(LX/0QB;)LX/IOj;

    move-result-object v0

    goto/16 :goto_0

    .line 672110
    :pswitch_24
    invoke-static {p1}, LX/IP2;->a(LX/0QB;)LX/IP2;

    move-result-object v0

    goto/16 :goto_0

    .line 672111
    :pswitch_25
    invoke-static {p1}, LX/F4p;->a(LX/0QB;)LX/F4p;

    move-result-object v0

    goto/16 :goto_0

    .line 672112
    :pswitch_26
    invoke-static {p1}, LX/JaR;->a(LX/0QB;)LX/JaR;

    move-result-object v0

    goto/16 :goto_0

    .line 672113
    :pswitch_27
    invoke-static {p1}, LX/IPO;->a(LX/0QB;)LX/IPO;

    move-result-object v0

    goto/16 :goto_0

    .line 672114
    :pswitch_28
    invoke-static {p1}, LX/IPQ;->a(LX/0QB;)LX/IPQ;

    move-result-object v0

    goto/16 :goto_0

    .line 672115
    :pswitch_29
    invoke-static {p1}, LX/IPR;->a(LX/0QB;)LX/IPR;

    move-result-object v0

    goto/16 :goto_0

    .line 672116
    :pswitch_2a
    invoke-static {p1}, LX/JaS;->a(LX/0QB;)LX/JaS;

    move-result-object v0

    goto/16 :goto_0

    .line 672117
    :pswitch_2b
    invoke-static {p1}, LX/IPS;->a(LX/0QB;)LX/IPS;

    move-result-object v0

    goto/16 :goto_0

    .line 672118
    :pswitch_2c
    invoke-static {p1}, LX/IPT;->a(LX/0QB;)LX/IPT;

    move-result-object v0

    goto/16 :goto_0

    .line 672119
    :pswitch_2d
    invoke-static {p1}, LX/IPU;->a(LX/0QB;)LX/IPU;

    move-result-object v0

    goto/16 :goto_0

    .line 672120
    :pswitch_2e
    invoke-static {p1}, LX/F5V;->a(LX/0QB;)LX/F5V;

    move-result-object v0

    goto/16 :goto_0

    .line 672121
    :pswitch_2f
    invoke-static {p1}, LX/IPw;->a(LX/0QB;)LX/IPw;

    move-result-object v0

    goto/16 :goto_0

    .line 672122
    :pswitch_30
    invoke-static {p1}, LX/IQ1;->a(LX/0QB;)LX/IQ1;

    move-result-object v0

    goto/16 :goto_0

    .line 672123
    :pswitch_31
    invoke-static {p1}, LX/IQ2;->a(LX/0QB;)LX/IQ2;

    move-result-object v0

    goto/16 :goto_0

    .line 672124
    :pswitch_32
    invoke-static {p1}, LX/DVa;->a(LX/0QB;)LX/DVa;

    move-result-object v0

    goto/16 :goto_0

    .line 672125
    :pswitch_33
    invoke-static {p1}, LX/DYX;->a(LX/0QB;)LX/DYX;

    move-result-object v0

    goto/16 :goto_0

    .line 672126
    :pswitch_34
    invoke-static {p1}, LX/F9l;->a(LX/0QB;)LX/F9l;

    move-result-object v0

    goto/16 :goto_0

    .line 672127
    :pswitch_35
    invoke-static {p1}, LX/88z;->a(LX/0QB;)LX/88z;

    move-result-object v0

    goto/16 :goto_0

    .line 672128
    :pswitch_36
    invoke-static {p1}, LX/DbB;->a(LX/0QB;)LX/DbB;

    move-result-object v0

    goto/16 :goto_0

    .line 672129
    :pswitch_37
    invoke-static {p1}, LX/IXG;->a(LX/0QB;)LX/IXG;

    move-result-object v0

    goto/16 :goto_0

    .line 672130
    :pswitch_38
    invoke-static {p1}, LX/Gn7;->a(LX/0QB;)LX/Gn7;

    move-result-object v0

    goto/16 :goto_0

    .line 672131
    :pswitch_39
    invoke-static {p1}, LX/FAr;->a(LX/0QB;)LX/FAr;

    move-result-object v0

    goto/16 :goto_0

    .line 672132
    :pswitch_3a
    invoke-static {p1}, LX/Gv6;->a(LX/0QB;)LX/Gv6;

    move-result-object v0

    goto/16 :goto_0

    .line 672133
    :pswitch_3b
    invoke-static {p1}, LX/Gv7;->a(LX/0QB;)LX/Gv7;

    move-result-object v0

    goto/16 :goto_0

    .line 672134
    :pswitch_3c
    invoke-static {p1}, LX/FAw;->a(LX/0QB;)LX/FAw;

    move-result-object v0

    goto/16 :goto_0

    .line 672135
    :pswitch_3d
    invoke-static {p1}, LX/FB6;->a(LX/0QB;)LX/FB6;

    move-result-object v0

    goto/16 :goto_0

    .line 672136
    :pswitch_3e
    invoke-static {p1}, LX/FBo;->b(LX/0QB;)LX/FBo;

    move-result-object v0

    goto/16 :goto_0

    .line 672137
    :pswitch_3f
    invoke-static {p1}, LX/2zR;->a(LX/0QB;)LX/2zR;

    move-result-object v0

    goto/16 :goto_0

    .line 672138
    :pswitch_40
    invoke-static {p1}, LX/2zQ;->a(LX/0QB;)LX/2zQ;

    move-result-object v0

    goto/16 :goto_0

    .line 672139
    :pswitch_41
    invoke-static {p1}, LX/2zU;->a(LX/0QB;)LX/2zU;

    move-result-object v0

    goto/16 :goto_0

    .line 672140
    :pswitch_42
    invoke-static {p1}, LX/2zT;->a(LX/0QB;)LX/2zT;

    move-result-object v0

    goto/16 :goto_0

    .line 672141
    :pswitch_43
    invoke-static {p1}, LX/2zS;->a(LX/0QB;)LX/2zS;

    move-result-object v0

    goto/16 :goto_0

    .line 672142
    :pswitch_44
    invoke-static {p1}, LX/2tF;->a(LX/0QB;)LX/2tF;

    move-result-object v0

    goto/16 :goto_0

    .line 672143
    :pswitch_45
    invoke-static {p1}, LX/2tC;->a(LX/0QB;)LX/2tC;

    move-result-object v0

    goto/16 :goto_0

    .line 672144
    :pswitch_46
    invoke-static {p1}, LX/2tB;->a(LX/0QB;)LX/2tB;

    move-result-object v0

    goto/16 :goto_0

    .line 672145
    :pswitch_47
    invoke-static {p1}, LX/FBq;->a(LX/0QB;)LX/FBq;

    move-result-object v0

    goto/16 :goto_0

    .line 672146
    :pswitch_48
    invoke-static {p1}, LX/FBr;->a(LX/0QB;)LX/FBr;

    move-result-object v0

    goto/16 :goto_0

    .line 672147
    :pswitch_49
    invoke-static {p1}, LX/FBt;->a(LX/0QB;)LX/FBt;

    move-result-object v0

    goto/16 :goto_0

    .line 672148
    :pswitch_4a
    invoke-static {p1}, LX/FBu;->a(LX/0QB;)LX/FBu;

    move-result-object v0

    goto/16 :goto_0

    .line 672149
    :pswitch_4b
    invoke-static {p1}, LX/IYg;->a(LX/0QB;)LX/IYg;

    move-result-object v0

    goto/16 :goto_0

    .line 672150
    :pswitch_4c
    invoke-static {p1}, LX/DbR;->a(LX/0QB;)LX/DbR;

    move-result-object v0

    goto/16 :goto_0

    .line 672151
    :pswitch_4d
    invoke-static {p1}, LX/Dbh;->a(LX/0QB;)LX/Dbh;

    move-result-object v0

    goto/16 :goto_0

    .line 672152
    :pswitch_4e
    invoke-static {p1}, LX/DcF;->a(LX/0QB;)LX/DcF;

    move-result-object v0

    goto/16 :goto_0

    .line 672153
    :pswitch_4f
    invoke-static {p1}, LX/DcT;->a(LX/0QB;)LX/DcT;

    move-result-object v0

    goto/16 :goto_0

    .line 672154
    :pswitch_50
    invoke-static {p1}, LX/Dcp;->a(LX/0QB;)LX/Dcp;

    move-result-object v0

    goto/16 :goto_0

    .line 672155
    :pswitch_51
    invoke-static {p1}, LX/6a4;->a(LX/0QB;)LX/6a4;

    move-result-object v0

    goto/16 :goto_0

    .line 672156
    :pswitch_52
    new-instance v1, LX/Ia9;

    const/16 v0, 0x15e7

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {p1}, LX/IZq;->b(LX/0QB;)LX/IZq;

    move-result-object v0

    check-cast v0, LX/IZq;

    invoke-direct {v1, p0, v0}, LX/Ia9;-><init>(LX/0Or;LX/IZq;)V

    .line 672157
    move-object v0, v1

    .line 672158
    goto/16 :goto_0

    .line 672159
    :pswitch_53
    new-instance v1, LX/IaB;

    const/16 v0, 0x15e7

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {p1}, LX/IZq;->b(LX/0QB;)LX/IZq;

    move-result-object v0

    check-cast v0, LX/IZq;

    invoke-direct {v1, p0, v0}, LX/IaB;-><init>(LX/0Or;LX/IZq;)V

    .line 672160
    move-object v0, v1

    .line 672161
    goto/16 :goto_0

    .line 672162
    :pswitch_54
    new-instance v0, LX/IcO;

    invoke-direct {v0}, LX/IcO;-><init>()V

    .line 672163
    move-object v0, v0

    .line 672164
    move-object v0, v0

    .line 672165
    goto/16 :goto_0

    .line 672166
    :pswitch_55
    invoke-static {p1}, LX/DnP;->a(LX/0QB;)LX/DnP;

    move-result-object v0

    goto/16 :goto_0

    .line 672167
    :pswitch_56
    new-instance v0, LX/H0u;

    invoke-direct {v0}, LX/H0u;-><init>()V

    .line 672168
    move-object v0, v0

    .line 672169
    move-object v0, v0

    .line 672170
    goto/16 :goto_0

    .line 672171
    :pswitch_57
    invoke-static {p1}, LX/Iuq;->a(LX/0QB;)LX/Iuq;

    move-result-object v0

    goto/16 :goto_0

    .line 672172
    :pswitch_58
    new-instance v1, LX/Jtl;

    invoke-static {p1}, LX/1Uk;->b(LX/0QB;)LX/1Uk;

    move-result-object v0

    check-cast v0, LX/1Uk;

    invoke-direct {v1, v0}, LX/Jtl;-><init>(LX/1Uk;)V

    .line 672173
    move-object v0, v1

    .line 672174
    goto/16 :goto_0

    .line 672175
    :pswitch_59
    invoke-static {p1}, LX/Ivz;->a(LX/0QB;)LX/Ivz;

    move-result-object v0

    goto/16 :goto_0

    .line 672176
    :pswitch_5a
    invoke-static {p1}, LX/Iwo;->a(LX/0QB;)LX/Iwo;

    move-result-object v0

    goto/16 :goto_0

    .line 672177
    :pswitch_5b
    invoke-static {p1}, LX/HCE;->a(LX/0QB;)LX/HCE;

    move-result-object v0

    goto/16 :goto_0

    .line 672178
    :pswitch_5c
    invoke-static {p1}, LX/Dsy;->a(LX/0QB;)LX/Dsy;

    move-result-object v0

    goto/16 :goto_0

    .line 672179
    :pswitch_5d
    invoke-static {p1}, LX/CSP;->a(LX/0QB;)LX/CSP;

    move-result-object v0

    goto/16 :goto_0

    .line 672180
    :pswitch_5e
    invoke-static {p1}, LX/FQc;->a(LX/0QB;)LX/FQc;

    move-result-object v0

    goto/16 :goto_0

    .line 672181
    :pswitch_5f
    invoke-static {p1}, LX/HSy;->a(LX/0QB;)LX/HSy;

    move-result-object v0

    goto/16 :goto_0

    .line 672182
    :pswitch_60
    invoke-static {p1}, LX/HT0;->a(LX/0QB;)LX/HT0;

    move-result-object v0

    goto/16 :goto_0

    .line 672183
    :pswitch_61
    invoke-static {p1}, LX/FQg;->a(LX/0QB;)LX/FQg;

    move-result-object v0

    goto/16 :goto_0

    .line 672184
    :pswitch_62
    invoke-static {p1}, LX/Ixy;->a(LX/0QB;)LX/Ixy;

    move-result-object v0

    goto/16 :goto_0

    .line 672185
    :pswitch_63
    invoke-static {p1}, LX/J3V;->b(LX/0QB;)LX/J3V;

    move-result-object v0

    goto/16 :goto_0

    .line 672186
    :pswitch_64
    invoke-static {p1}, LX/Cad;->a(LX/0QB;)LX/Cad;

    move-result-object v0

    goto/16 :goto_0

    .line 672187
    :pswitch_65
    invoke-static {p1}, LX/E1a;->a(LX/0QB;)LX/E1a;

    move-result-object v0

    goto/16 :goto_0

    .line 672188
    :pswitch_66
    invoke-static {p1}, LX/J3h;->a(LX/0QB;)LX/J3h;

    move-result-object v0

    goto/16 :goto_0

    .line 672189
    :pswitch_67
    invoke-static {p1}, LX/J78;->a(LX/0QB;)LX/J78;

    move-result-object v0

    goto/16 :goto_0

    .line 672190
    :pswitch_68
    invoke-static {p1}, LX/JyT;->a(LX/0QB;)LX/JyT;

    move-result-object v0

    goto/16 :goto_0

    .line 672191
    :pswitch_69
    invoke-static {p1}, LX/78E;->a(LX/0QB;)LX/78E;

    move-result-object v0

    goto/16 :goto_0

    .line 672192
    :pswitch_6a
    invoke-static {p1}, LX/HYQ;->a(LX/0QB;)LX/HYQ;

    move-result-object v0

    goto/16 :goto_0

    .line 672193
    :pswitch_6b
    invoke-static {p1}, LX/HaK;->a(LX/0QB;)LX/HaK;

    move-result-object v0

    goto/16 :goto_0

    .line 672194
    :pswitch_6c
    invoke-static {p1}, LX/EAd;->a(LX/0QB;)LX/EAd;

    move-result-object v0

    goto/16 :goto_0

    .line 672195
    :pswitch_6d
    invoke-static {p1}, LX/EAi;->a(LX/0QB;)LX/EAi;

    move-result-object v0

    goto/16 :goto_0

    .line 672196
    :pswitch_6e
    invoke-static {p1}, LX/2un;->a(LX/0QB;)LX/2un;

    move-result-object v0

    goto/16 :goto_0

    .line 672197
    :pswitch_6f
    invoke-static {p1}, LX/FjO;->a(LX/0QB;)LX/FjO;

    move-result-object v0

    goto/16 :goto_0

    .line 672198
    :pswitch_70
    invoke-static {p1}, LX/FjQ;->a(LX/0QB;)LX/FjQ;

    move-result-object v0

    goto/16 :goto_0

    .line 672199
    :pswitch_71
    invoke-static {p1}, LX/FjR;->a(LX/0QB;)LX/FjR;

    move-result-object v0

    goto/16 :goto_0

    .line 672200
    :pswitch_72
    invoke-static {p1}, LX/FkQ;->a(LX/0QB;)LX/FkQ;

    move-result-object v0

    goto/16 :goto_0

    .line 672201
    :pswitch_73
    invoke-static {p1}, LX/Fm5;->a(LX/0QB;)LX/Fm5;

    move-result-object v0

    goto/16 :goto_0

    .line 672202
    :pswitch_74
    invoke-static {p1}, LX/D1U;->a(LX/0QB;)LX/D1U;

    move-result-object v0

    goto/16 :goto_0

    .line 672203
    :pswitch_75
    new-instance v0, LX/K5W;

    invoke-direct {v0}, LX/K5W;-><init>()V

    .line 672204
    move-object v0, v0

    .line 672205
    move-object v0, v0

    .line 672206
    goto/16 :goto_0

    .line 672207
    :pswitch_76
    invoke-static {p1}, LX/D1p;->a(LX/0QB;)LX/D1p;

    move-result-object v0

    goto/16 :goto_0

    .line 672208
    :pswitch_77
    invoke-static {p1}, LX/J93;->a(LX/0QB;)LX/J93;

    move-result-object v0

    goto/16 :goto_0

    .line 672209
    :pswitch_78
    invoke-static {p1}, LX/D39;->a(LX/0QB;)LX/D39;

    move-result-object v0

    goto/16 :goto_0

    .line 672210
    :pswitch_79
    invoke-static {p1}, LX/A7v;->a(LX/0QB;)LX/A7v;

    move-result-object v0

    goto/16 :goto_0

    .line 672211
    :pswitch_7a
    invoke-static {p1}, LX/ERG;->a(LX/0QB;)LX/ERG;

    move-result-object v0

    goto/16 :goto_0

    .line 672212
    :pswitch_7b
    invoke-static {p1}, LX/D6k;->a(LX/0QB;)LX/D6k;

    move-result-object v0

    goto/16 :goto_0

    .line 672213
    :pswitch_7c
    invoke-static {p1}, LX/JE2;->a(LX/0QB;)LX/JE2;

    move-result-object v0

    goto/16 :goto_0

    .line 672214
    :pswitch_7d
    invoke-static {p1}, LX/Hh8;->a(LX/0QB;)LX/Hh8;

    move-result-object v0

    goto/16 :goto_0

    .line 672215
    :pswitch_7e
    invoke-static {p1}, LX/HhS;->a(LX/0QB;)LX/HhS;

    move-result-object v0

    goto/16 :goto_0

    .line 672216
    :pswitch_7f
    invoke-static {p1}, LX/JEX;->a(LX/0QB;)LX/JEX;

    move-result-object v0

    goto/16 :goto_0

    .line 672217
    :pswitch_80
    invoke-static {p1}, LX/JEY;->a(LX/0QB;)LX/JEY;

    move-result-object v0

    goto/16 :goto_0

    .line 672218
    :pswitch_81
    invoke-static {p1}, LX/JEo;->a(LX/0QB;)LX/JEo;

    move-result-object v0

    goto/16 :goto_0

    .line 672219
    :pswitch_82
    invoke-static {p1}, LX/7YA;->a(LX/0QB;)LX/7YA;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_42
        :pswitch_43
        :pswitch_44
        :pswitch_45
        :pswitch_46
        :pswitch_47
        :pswitch_48
        :pswitch_49
        :pswitch_4a
        :pswitch_4b
        :pswitch_4c
        :pswitch_4d
        :pswitch_4e
        :pswitch_4f
        :pswitch_50
        :pswitch_51
        :pswitch_52
        :pswitch_53
        :pswitch_54
        :pswitch_55
        :pswitch_56
        :pswitch_57
        :pswitch_58
        :pswitch_59
        :pswitch_5a
        :pswitch_5b
        :pswitch_5c
        :pswitch_5d
        :pswitch_5e
        :pswitch_5f
        :pswitch_60
        :pswitch_61
        :pswitch_62
        :pswitch_63
        :pswitch_64
        :pswitch_65
        :pswitch_66
        :pswitch_67
        :pswitch_68
        :pswitch_69
        :pswitch_6a
        :pswitch_6b
        :pswitch_6c
        :pswitch_6d
        :pswitch_6e
        :pswitch_6f
        :pswitch_70
        :pswitch_71
        :pswitch_72
        :pswitch_73
        :pswitch_74
        :pswitch_75
        :pswitch_76
        :pswitch_77
        :pswitch_78
        :pswitch_79
        :pswitch_7a
        :pswitch_7b
        :pswitch_7c
        :pswitch_7d
        :pswitch_7e
        :pswitch_7f
        :pswitch_80
        :pswitch_81
        :pswitch_82
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 672220
    const/16 v0, 0x83

    return v0
.end method
