.class public LX/4OD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 695802
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 695803
    const/4 v9, 0x0

    .line 695804
    const/4 v8, 0x0

    .line 695805
    const/4 v7, 0x0

    .line 695806
    const/4 v6, 0x0

    .line 695807
    const/4 v5, 0x0

    .line 695808
    const/4 v4, 0x0

    .line 695809
    const/4 v3, 0x0

    .line 695810
    const/4 v2, 0x0

    .line 695811
    const/4 v1, 0x0

    .line 695812
    const/4 v0, 0x0

    .line 695813
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 695814
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 695815
    const/4 v0, 0x0

    .line 695816
    :goto_0
    return v0

    .line 695817
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 695818
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_9

    .line 695819
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 695820
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 695821
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 695822
    const-string v11, "default_group_name"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 695823
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 695824
    :cond_2
    const-string v11, "default_visibility"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 695825
    const/4 v1, 0x1

    .line 695826
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v8

    goto :goto_1

    .line 695827
    :cond_3
    const-string v11, "suggested_members"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 695828
    invoke-static {p0, p1}, LX/2bO;->b(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 695829
    :cond_4
    const-string v11, "suggestion_cover_image"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 695830
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 695831
    :cond_5
    const-string v11, "suggestion_identifier"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 695832
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 695833
    :cond_6
    const-string v11, "suggestion_message"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 695834
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 695835
    :cond_7
    const-string v11, "suggestion_type"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 695836
    const/4 v0, 0x1

    .line 695837
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    move-result-object v3

    goto/16 :goto_1

    .line 695838
    :cond_8
    const-string v11, "extra_setting"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 695839
    invoke-static {p0, p1}, LX/4OE;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 695840
    :cond_9
    const/16 v10, 0x8

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 695841
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 695842
    if-eqz v1, :cond_a

    .line 695843
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v8}, LX/186;->a(ILjava/lang/Enum;)V

    .line 695844
    :cond_a
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 695845
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 695846
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 695847
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 695848
    if-eqz v0, :cond_b

    .line 695849
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->a(ILjava/lang/Enum;)V

    .line 695850
    :cond_b
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 695851
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/4 v4, 0x6

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 695852
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 695853
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 695854
    if-eqz v0, :cond_0

    .line 695855
    const-string v1, "default_group_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695856
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 695857
    :cond_0
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 695858
    if-eqz v0, :cond_1

    .line 695859
    const-string v0, "default_visibility"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695860
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 695861
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 695862
    if-eqz v0, :cond_2

    .line 695863
    const-string v1, "suggested_members"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695864
    invoke-static {p0, v0, p2, p3}, LX/2bO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 695865
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 695866
    if-eqz v0, :cond_3

    .line 695867
    const-string v1, "suggestion_cover_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695868
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 695869
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 695870
    if-eqz v0, :cond_4

    .line 695871
    const-string v1, "suggestion_identifier"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695872
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 695873
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 695874
    if-eqz v0, :cond_5

    .line 695875
    const-string v1, "suggestion_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695876
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 695877
    :cond_5
    invoke-virtual {p0, p1, v4, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 695878
    if-eqz v0, :cond_6

    .line 695879
    const-string v0, "suggestion_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695880
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 695881
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 695882
    if-eqz v0, :cond_7

    .line 695883
    const-string v1, "extra_setting"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695884
    invoke-static {p0, v0, p2, p3}, LX/4OE;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 695885
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 695886
    return-void
.end method
