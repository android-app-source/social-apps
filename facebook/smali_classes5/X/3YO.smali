.class public LX/3YO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TG;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/3YO;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 595861
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 595862
    return-void
.end method

.method public static a(LX/0QB;)LX/3YO;
    .locals 3

    .prologue
    .line 595849
    sget-object v0, LX/3YO;->a:LX/3YO;

    if-nez v0, :cond_1

    .line 595850
    const-class v1, LX/3YO;

    monitor-enter v1

    .line 595851
    :try_start_0
    sget-object v0, LX/3YO;->a:LX/3YO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 595852
    if-eqz v2, :cond_0

    .line 595853
    :try_start_1
    new-instance v0, LX/3YO;

    invoke-direct {v0}, LX/3YO;-><init>()V

    .line 595854
    move-object v0, v0

    .line 595855
    sput-object v0, LX/3YO;->a:LX/3YO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 595856
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 595857
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 595858
    :cond_1
    sget-object v0, LX/3YO;->a:LX/3YO;

    return-object v0

    .line 595859
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 595860
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ja;)V
    .locals 3

    .prologue
    .line 595866
    sget-object v0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;->a:LX/1Cz;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    sget-object v1, LX/0ja;->a:LX/3AL;

    sget-object v2, LX/0ja;->e:LX/3AM;

    invoke-virtual {p1, v0, v1, v2}, LX/0ja;->a(Ljava/lang/Class;LX/3AL;LX/3AM;)V

    .line 595867
    sget-object v0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;->b:LX/1Cz;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    sget-object v1, LX/0ja;->a:LX/3AL;

    sget-object v2, LX/0ja;->e:LX/3AM;

    invoke-virtual {p1, v0, v1, v2}, LX/0ja;->a(Ljava/lang/Class;LX/3AL;LX/3AM;)V

    .line 595868
    return-void
.end method

.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 595863
    sget-object v0, Lcom/facebook/feedplugins/multishare/MultiShareSingleTextAttachmentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 595864
    sget-object v0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentComponentPartDefinition;->d:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 595865
    return-void
.end method
