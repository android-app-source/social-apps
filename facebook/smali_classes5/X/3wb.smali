.class public LX/3wb;
.super LX/3ut;
.source ""

# interfaces
.implements LX/3rP;


# instance fields
.field public final g:LX/3wZ;

.field public h:I

.field public i:Landroid/view/View;

.field private j:Z

.field private k:Z

.field private l:I

.field private m:I

.field public n:I

.field public o:Z

.field private p:Z

.field private q:Z

.field public r:Z

.field private s:I

.field private final t:Landroid/util/SparseBooleanArray;

.field private u:Landroid/view/View;

.field public v:LX/3wY;

.field public w:LX/3wU;

.field public x:Landroid/support/v7/widget/ActionMenuPresenter$OpenOverflowRunnable;

.field private y:LX/3wV;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 655651
    const v0, 0x7f030004

    const v1, 0x7f030003

    invoke-direct {p0, p1, v0, v1}, LX/3ut;-><init>(Landroid/content/Context;II)V

    .line 655652
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, LX/3wb;->t:Landroid/util/SparseBooleanArray;

    .line 655653
    new-instance v0, LX/3wZ;

    invoke-direct {v0, p0}, LX/3wZ;-><init>(LX/3wb;)V

    iput-object v0, p0, LX/3wb;->g:LX/3wZ;

    .line 655654
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)LX/3ux;
    .locals 2

    .prologue
    .line 655602
    invoke-super {p0, p1}, LX/3ut;->a(Landroid/view/ViewGroup;)LX/3ux;

    move-result-object v1

    move-object v0, v1

    .line 655603
    check-cast v0, Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/ActionMenuView;->setPresenter(LX/3wb;)V

    .line 655604
    return-object v1
.end method

.method public final a(LX/3v3;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 655605
    invoke-virtual {p1}, LX/3v3;->getActionView()Landroid/view/View;

    move-result-object v0

    .line 655606
    if-eqz v0, :cond_0

    invoke-virtual {p1}, LX/3v3;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 655607
    :cond_0
    invoke-super {p0, p1, p2, p3}, LX/3ut;->a(LX/3v3;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 655608
    :cond_1
    invoke-virtual {p1}, LX/3v3;->isActionViewExpanded()Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 655609
    check-cast p3, Landroid/support/v7/widget/ActionMenuView;

    .line 655610
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 655611
    invoke-virtual {p3, v1}, Landroid/support/v7/widget/ActionMenuView;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 655612
    invoke-static {v1}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/view/ViewGroup$LayoutParams;)LX/3we;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 655613
    :cond_2
    return-object v0

    .line 655614
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(IZ)V
    .locals 1

    .prologue
    .line 655615
    iput p1, p0, LX/3wb;->l:I

    .line 655616
    iput-boolean p2, p0, LX/3wb;->p:Z

    .line 655617
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3wb;->q:Z

    .line 655618
    return-void
.end method

.method public final a(LX/3v0;Z)V
    .locals 0

    .prologue
    .line 655619
    invoke-virtual {p0}, LX/3wb;->f()Z

    .line 655620
    invoke-super {p0, p1, p2}, LX/3ut;->a(LX/3v0;Z)V

    .line 655621
    return-void
.end method

.method public final a(LX/3v3;LX/3uq;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 655655
    invoke-interface {p2, p1, v1}, LX/3uq;->a(LX/3v3;I)V

    .line 655656
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    check-cast v0, Landroid/support/v7/widget/ActionMenuView;

    .line 655657
    check-cast p2, Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    .line 655658
    iput-object v0, p2, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->d:LX/3uw;

    .line 655659
    iget-object v0, p0, LX/3wb;->y:LX/3wV;

    if-nez v0, :cond_0

    .line 655660
    new-instance v0, LX/3wV;

    invoke-direct {v0, p0}, LX/3wV;-><init>(LX/3wb;)V

    iput-object v0, p0, LX/3wb;->y:LX/3wV;

    .line 655661
    :cond_0
    iget-object v0, p0, LX/3wb;->y:LX/3wV;

    .line 655662
    iput-object v0, p2, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->f:LX/3uo;

    .line 655663
    return-void
.end method

.method public final a(Landroid/content/Context;LX/3v0;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 655622
    invoke-super {p0, p1, p2}, LX/3ut;->a(Landroid/content/Context;LX/3v0;)V

    .line 655623
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 655624
    invoke-static {p1}, LX/3ub;->a(Landroid/content/Context;)LX/3ub;

    move-result-object v0

    .line 655625
    iget-boolean v2, p0, LX/3wb;->k:Z

    if-nez v2, :cond_1

    .line 655626
    const/4 v2, 0x1

    .line 655627
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p1, 0x13

    if-lt v3, p1, :cond_6

    .line 655628
    :cond_0
    :goto_0
    move v2, v2

    .line 655629
    iput-boolean v2, p0, LX/3wb;->j:Z

    .line 655630
    :cond_1
    iget-boolean v2, p0, LX/3wb;->q:Z

    if-nez v2, :cond_2

    .line 655631
    iget-object v2, v0, LX/3ub;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v2, v2, 0x2

    move v2, v2

    .line 655632
    iput v2, p0, LX/3wb;->l:I

    .line 655633
    :cond_2
    iget-boolean v2, p0, LX/3wb;->o:Z

    if-nez v2, :cond_3

    .line 655634
    iget-object v2, v0, LX/3ub;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    move v0, v2

    .line 655635
    iput v0, p0, LX/3wb;->n:I

    .line 655636
    :cond_3
    iget v0, p0, LX/3wb;->l:I

    .line 655637
    iget-boolean v2, p0, LX/3wb;->j:Z

    if-eqz v2, :cond_5

    .line 655638
    iget-object v2, p0, LX/3wb;->i:Landroid/view/View;

    if-nez v2, :cond_4

    .line 655639
    new-instance v2, LX/3wX;

    iget-object v3, p0, LX/3ut;->a:Landroid/content/Context;

    invoke-direct {v2, p0, v3}, LX/3wX;-><init>(LX/3wb;Landroid/content/Context;)V

    iput-object v2, p0, LX/3wb;->i:Landroid/view/View;

    .line 655640
    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 655641
    iget-object v3, p0, LX/3wb;->i:Landroid/view/View;

    invoke-virtual {v3, v2, v2}, Landroid/view/View;->measure(II)V

    .line 655642
    :cond_4
    iget-object v2, p0, LX/3wb;->i:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v0, v2

    .line 655643
    :goto_1
    iput v0, p0, LX/3wb;->m:I

    .line 655644
    const/high16 v0, 0x42600000    # 56.0f

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, LX/3wb;->s:I

    .line 655645
    iput-object v5, p0, LX/3wb;->u:Landroid/view/View;

    .line 655646
    return-void

    .line 655647
    :cond_5
    iput-object v5, p0, LX/3wb;->i:Landroid/view/View;

    goto :goto_1

    :cond_6
    iget-object v3, v0, LX/3ub;->a:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v3

    invoke-static {v3}, LX/0iP;->b(Landroid/view/ViewConfiguration;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/support/v7/widget/ActionMenuView;)V
    .locals 1

    .prologue
    .line 655648
    iput-object p1, p0, LX/3wb;->f:LX/3ux;

    .line 655649
    iget-object v0, p0, LX/3ut;->c:LX/3v0;

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/ActionMenuView;->a(LX/3v0;)V

    .line 655650
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 655422
    if-eqz p1, :cond_0

    .line 655423
    const/4 v0, 0x0

    invoke-super {p0, v0}, LX/3ut;->a(LX/3vG;)Z

    .line 655424
    :goto_0
    return-void

    .line 655425
    :cond_0
    iget-object v0, p0, LX/3ut;->c:LX/3v0;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/3v0;->a(Z)V

    goto :goto_0
.end method

.method public final a(LX/3v3;)Z
    .locals 1

    .prologue
    .line 655575
    invoke-virtual {p1}, LX/3v3;->j()Z

    move-result v0

    return v0
.end method

.method public final a(LX/3vG;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 655576
    invoke-virtual {p1}, LX/3v0;->hasVisibleItems()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 655577
    :goto_0
    return v0

    :cond_0
    move-object v0, p1

    .line 655578
    :goto_1
    iget-object v2, v0, LX/3vG;->d:LX/3v0;

    move-object v2, v2

    .line 655579
    iget-object v3, p0, LX/3ut;->c:LX/3v0;

    if-eq v2, v3, :cond_1

    .line 655580
    iget-object v2, v0, LX/3vG;->d:LX/3v0;

    move-object v0, v2

    .line 655581
    check-cast v0, LX/3vG;

    goto :goto_1

    .line 655582
    :cond_1
    invoke-virtual {v0}, LX/3vG;->getItem()Landroid/view/MenuItem;

    move-result-object v0

    const/4 v5, 0x0

    .line 655583
    iget-object v2, p0, LX/3ut;->f:LX/3ux;

    check-cast v2, Landroid/view/ViewGroup;

    .line 655584
    if-nez v2, :cond_5

    move-object v4, v5

    .line 655585
    :cond_2
    :goto_2
    move-object v0, v4

    .line 655586
    if-nez v0, :cond_4

    .line 655587
    iget-object v0, p0, LX/3wb;->i:Landroid/view/View;

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    .line 655588
    :cond_3
    iget-object v0, p0, LX/3wb;->i:Landroid/view/View;

    .line 655589
    :cond_4
    invoke-virtual {p1}, LX/3vG;->getItem()Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    iput v1, p0, LX/3wb;->h:I

    .line 655590
    new-instance v1, LX/3wU;

    iget-object v2, p0, LX/3ut;->b:Landroid/content/Context;

    invoke-direct {v1, p0, v2, p1}, LX/3wU;-><init>(LX/3wb;Landroid/content/Context;LX/3vG;)V

    iput-object v1, p0, LX/3wb;->w:LX/3wU;

    .line 655591
    iget-object v1, p0, LX/3wb;->w:LX/3wU;

    .line 655592
    iput-object v0, v1, LX/3vD;->k:Landroid/view/View;

    .line 655593
    iget-object v0, p0, LX/3wb;->w:LX/3wU;

    invoke-virtual {v0}, LX/3vD;->a()V

    .line 655594
    invoke-super {p0, p1}, LX/3ut;->a(LX/3vG;)Z

    .line 655595
    const/4 v0, 0x1

    goto :goto_0

    .line 655596
    :cond_5
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v7

    .line 655597
    const/4 v3, 0x0

    move v6, v3

    :goto_3
    if-ge v6, v7, :cond_7

    .line 655598
    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 655599
    instance-of v3, v4, LX/3uq;

    if-eqz v3, :cond_6

    move-object v3, v4

    check-cast v3, LX/3uq;

    invoke-interface {v3}, LX/3uq;->getItemData()LX/3v3;

    move-result-object v3

    if-eq v3, v0, :cond_2

    .line 655600
    :cond_6
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_3

    :cond_7
    move-object v4, v5

    .line 655601
    goto :goto_2
.end method

.method public final a(Landroid/view/ViewGroup;I)Z
    .locals 2

    .prologue
    .line 655573
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, LX/3wb;->i:Landroid/view/View;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    .line 655574
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, LX/3ut;->a(Landroid/view/ViewGroup;I)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 655570
    iput p1, p0, LX/3wb;->n:I

    .line 655571
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3wb;->o:Z

    .line 655572
    return-void
.end method

.method public final b(Z)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 655537
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 655538
    invoke-super {p0, p1}, LX/3ut;->b(Z)V

    .line 655539
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 655540
    iget-object v0, p0, LX/3ut;->c:LX/3v0;

    if-eqz v0, :cond_1

    .line 655541
    iget-object v0, p0, LX/3ut;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->l()Ljava/util/ArrayList;

    move-result-object v4

    .line 655542
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v2

    .line 655543
    :goto_0
    if-ge v3, v5, :cond_1

    .line 655544
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    invoke-virtual {v0}, LX/3v3;->a()LX/3rR;

    move-result-object v0

    .line 655545
    if-eqz v0, :cond_0

    .line 655546
    iput-object p0, v0, LX/3rR;->b:LX/3rP;

    .line 655547
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 655548
    :cond_1
    iget-object v0, p0, LX/3ut;->c:LX/3v0;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/3ut;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->m()Ljava/util/ArrayList;

    move-result-object v0

    .line 655549
    :goto_1
    iget-boolean v3, p0, LX/3wb;->j:Z

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    .line 655550
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 655551
    if-ne v3, v1, :cond_8

    .line 655552
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    invoke-virtual {v0}, LX/3v3;->isActionViewExpanded()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_2
    move v2, v0

    .line 655553
    :cond_2
    :goto_3
    if-eqz v2, :cond_a

    .line 655554
    iget-object v0, p0, LX/3wb;->i:Landroid/view/View;

    if-nez v0, :cond_3

    .line 655555
    new-instance v0, LX/3wX;

    iget-object v1, p0, LX/3ut;->a:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, LX/3wX;-><init>(LX/3wb;Landroid/content/Context;)V

    iput-object v0, p0, LX/3wb;->i:Landroid/view/View;

    .line 655556
    :cond_3
    iget-object v0, p0, LX/3wb;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 655557
    iget-object v1, p0, LX/3ut;->f:LX/3ux;

    if-eq v0, v1, :cond_5

    .line 655558
    if-eqz v0, :cond_4

    .line 655559
    iget-object v1, p0, LX/3wb;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 655560
    :cond_4
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    check-cast v0, Landroid/support/v7/widget/ActionMenuView;

    .line 655561
    iget-object v1, p0, LX/3wb;->i:Landroid/view/View;

    invoke-static {}, Landroid/support/v7/widget/ActionMenuView;->b()LX/3we;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/ActionMenuView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 655562
    :cond_5
    :goto_4
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    check-cast v0, Landroid/support/v7/widget/ActionMenuView;

    iget-boolean v1, p0, LX/3wb;->j:Z

    .line 655563
    iput-boolean v1, v0, Landroid/support/v7/widget/ActionMenuView;->e:Z

    .line 655564
    return-void

    .line 655565
    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    :cond_7
    move v0, v2

    .line 655566
    goto :goto_2

    .line 655567
    :cond_8
    if-lez v3, :cond_9

    :goto_5
    move v2, v1

    goto :goto_3

    :cond_9
    move v1, v2

    goto :goto_5

    .line 655568
    :cond_a
    iget-object v0, p0, LX/3wb;->i:Landroid/view/View;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/3wb;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, LX/3ut;->f:LX/3ux;

    if-ne v0, v1, :cond_5

    .line 655569
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, LX/3wb;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_4
.end method

.method public final b()Z
    .locals 21

    .prologue
    .line 655452
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3ut;->c:LX/3v0;

    invoke-virtual {v2}, LX/3v0;->j()Ljava/util/ArrayList;

    move-result-object v13

    .line 655453
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v14

    .line 655454
    move-object/from16 v0, p0

    iget v7, v0, LX/3wb;->n:I

    .line 655455
    move-object/from16 v0, p0

    iget v9, v0, LX/3wb;->m:I

    .line 655456
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    .line 655457
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3ut;->f:LX/3ux;

    check-cast v2, Landroid/view/ViewGroup;

    .line 655458
    const/4 v6, 0x0

    .line 655459
    const/4 v5, 0x0

    .line 655460
    const/4 v8, 0x0

    .line 655461
    const/4 v4, 0x0

    .line 655462
    const/4 v3, 0x0

    move v10, v3

    :goto_0
    if-ge v10, v14, :cond_2

    .line 655463
    invoke-virtual {v13, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3v3;

    .line 655464
    invoke-virtual {v3}, LX/3v3;->l()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 655465
    add-int/lit8 v6, v6, 0x1

    .line 655466
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v11, v0, LX/3wb;->r:Z

    if-eqz v11, :cond_1e

    invoke-virtual {v3}, LX/3v3;->isActionViewExpanded()Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 655467
    const/4 v3, 0x0

    .line 655468
    :goto_2
    add-int/lit8 v7, v10, 0x1

    move v10, v7

    move v7, v3

    goto :goto_0

    .line 655469
    :cond_0
    invoke-virtual {v3}, LX/3v3;->k()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 655470
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 655471
    :cond_1
    const/4 v4, 0x1

    goto :goto_1

    .line 655472
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/3wb;->j:Z

    if-eqz v3, :cond_4

    if-nez v4, :cond_3

    add-int v3, v6, v5

    if-le v3, v7, :cond_4

    .line 655473
    :cond_3
    add-int/lit8 v7, v7, -0x1

    .line 655474
    :cond_4
    sub-int v10, v7, v6

    .line 655475
    move-object/from16 v0, p0

    iget-object v0, v0, LX/3wb;->t:Landroid/util/SparseBooleanArray;

    move-object/from16 v16, v0

    .line 655476
    invoke-virtual/range {v16 .. v16}, Landroid/util/SparseBooleanArray;->clear()V

    .line 655477
    const/4 v4, 0x0

    .line 655478
    const/4 v3, 0x0

    .line 655479
    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/3wb;->p:Z

    if-eqz v5, :cond_1d

    .line 655480
    move-object/from16 v0, p0

    iget v3, v0, LX/3wb;->s:I

    div-int v3, v9, v3

    .line 655481
    move-object/from16 v0, p0

    iget v4, v0, LX/3wb;->s:I

    rem-int v4, v9, v4

    .line 655482
    move-object/from16 v0, p0

    iget v5, v0, LX/3wb;->s:I

    div-int/2addr v4, v3

    add-int/2addr v4, v5

    move v5, v4

    .line 655483
    :goto_3
    const/4 v4, 0x0

    move v12, v4

    move v7, v8

    move v4, v3

    :goto_4
    if-ge v12, v14, :cond_17

    .line 655484
    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3v3;

    .line 655485
    invoke-virtual {v3}, LX/3v3;->l()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 655486
    move-object/from16 v0, p0

    iget-object v6, v0, LX/3wb;->u:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v6, v2}, LX/3ut;->a(LX/3v3;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 655487
    move-object/from16 v0, p0

    iget-object v8, v0, LX/3wb;->u:Landroid/view/View;

    if-nez v8, :cond_5

    .line 655488
    move-object/from16 v0, p0

    iput-object v6, v0, LX/3wb;->u:Landroid/view/View;

    .line 655489
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v8, v0, LX/3wb;->p:Z

    if-eqz v8, :cond_7

    .line 655490
    const/4 v8, 0x0

    invoke-static {v6, v5, v4, v15, v8}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/view/View;IIII)I

    move-result v8

    sub-int/2addr v4, v8

    .line 655491
    :goto_5
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    .line 655492
    sub-int v8, v9, v6

    .line 655493
    if-nez v7, :cond_1c

    .line 655494
    :goto_6
    invoke-virtual {v3}, LX/3v3;->getGroupId()I

    move-result v7

    .line 655495
    if-eqz v7, :cond_6

    .line 655496
    const/4 v9, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v7, v9}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 655497
    :cond_6
    const/4 v7, 0x1

    invoke-virtual {v3, v7}, LX/3v3;->d(Z)V

    move v3, v8

    move v7, v10

    .line 655498
    :goto_7
    add-int/lit8 v8, v12, 0x1

    move v12, v8

    move v9, v3

    move v10, v7

    move v7, v6

    goto :goto_4

    .line 655499
    :cond_7
    invoke-virtual {v6, v15, v15}, Landroid/view/View;->measure(II)V

    goto :goto_5

    .line 655500
    :cond_8
    invoke-virtual {v3}, LX/3v3;->k()Z

    move-result v6

    if-eqz v6, :cond_16

    .line 655501
    invoke-virtual {v3}, LX/3v3;->getGroupId()I

    move-result v17

    .line 655502
    invoke-virtual/range {v16 .. v17}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v18

    .line 655503
    if-gtz v10, :cond_9

    if-eqz v18, :cond_e

    :cond_9
    if-lez v9, :cond_e

    move-object/from16 v0, p0

    iget-boolean v6, v0, LX/3wb;->p:Z

    if-eqz v6, :cond_a

    if-lez v4, :cond_e

    :cond_a
    const/4 v6, 0x1

    .line 655504
    :goto_8
    if-eqz v6, :cond_1b

    .line 655505
    move-object/from16 v0, p0

    iget-object v8, v0, LX/3wb;->u:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v8, v2}, LX/3ut;->a(LX/3v3;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    .line 655506
    move-object/from16 v0, p0

    iget-object v8, v0, LX/3wb;->u:Landroid/view/View;

    if-nez v8, :cond_b

    .line 655507
    move-object/from16 v0, p0

    iput-object v11, v0, LX/3wb;->u:Landroid/view/View;

    .line 655508
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v8, v0, LX/3wb;->p:Z

    if-eqz v8, :cond_f

    .line 655509
    const/4 v8, 0x0

    invoke-static {v11, v5, v4, v15, v8}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/view/View;IIII)I

    move-result v19

    .line 655510
    sub-int v8, v4, v19

    .line 655511
    if-nez v19, :cond_1a

    .line 655512
    const/4 v4, 0x0

    :goto_9
    move v6, v8

    .line 655513
    :goto_a
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    .line 655514
    sub-int/2addr v9, v8

    .line 655515
    if-nez v7, :cond_c

    move v7, v8

    .line 655516
    :cond_c
    move-object/from16 v0, p0

    iget-boolean v8, v0, LX/3wb;->p:Z

    if-eqz v8, :cond_11

    .line 655517
    if-ltz v9, :cond_10

    const/4 v8, 0x1

    :goto_b
    and-int/2addr v4, v8

    move v11, v4

    move v8, v7

    move v7, v6

    .line 655518
    :goto_c
    if-eqz v11, :cond_13

    if-eqz v17, :cond_13

    .line 655519
    const/4 v4, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseBooleanArray;->put(IZ)V

    move v4, v10

    .line 655520
    :goto_d
    if-eqz v11, :cond_d

    add-int/lit8 v4, v4, -0x1

    .line 655521
    :cond_d
    invoke-virtual {v3, v11}, LX/3v3;->d(Z)V

    move v6, v8

    move v3, v9

    move/from16 v20, v7

    move v7, v4

    move/from16 v4, v20

    .line 655522
    goto :goto_7

    .line 655523
    :cond_e
    const/4 v6, 0x0

    goto :goto_8

    .line 655524
    :cond_f
    invoke-virtual {v11, v15, v15}, Landroid/view/View;->measure(II)V

    move/from16 v20, v6

    move v6, v4

    move/from16 v4, v20

    goto :goto_a

    .line 655525
    :cond_10
    const/4 v8, 0x0

    goto :goto_b

    .line 655526
    :cond_11
    add-int v8, v9, v7

    if-lez v8, :cond_12

    const/4 v8, 0x1

    :goto_e
    and-int/2addr v4, v8

    move v11, v4

    move v8, v7

    move v7, v6

    goto :goto_c

    :cond_12
    const/4 v8, 0x0

    goto :goto_e

    .line 655527
    :cond_13
    if-eqz v18, :cond_19

    .line 655528
    const/4 v4, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 655529
    const/4 v4, 0x0

    move v6, v10

    move v10, v4

    :goto_f
    if-ge v10, v12, :cond_18

    .line 655530
    invoke-virtual {v13, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3v3;

    .line 655531
    invoke-virtual {v4}, LX/3v3;->getGroupId()I

    move-result v18

    move/from16 v0, v18

    move/from16 v1, v17

    if-ne v0, v1, :cond_15

    .line 655532
    invoke-virtual {v4}, LX/3v3;->j()Z

    move-result v18

    if-eqz v18, :cond_14

    add-int/lit8 v6, v6, 0x1

    .line 655533
    :cond_14
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v4, v0}, LX/3v3;->d(Z)V

    .line 655534
    :cond_15
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    goto :goto_f

    .line 655535
    :cond_16
    const/4 v6, 0x0

    invoke-virtual {v3, v6}, LX/3v3;->d(Z)V

    move v6, v7

    move v3, v9

    move v7, v10

    goto/16 :goto_7

    .line 655536
    :cond_17
    const/4 v2, 0x1

    return v2

    :cond_18
    move v4, v6

    goto :goto_d

    :cond_19
    move v4, v10

    goto :goto_d

    :cond_1a
    move v4, v6

    goto/16 :goto_9

    :cond_1b
    move v11, v6

    move v8, v7

    move v7, v4

    goto :goto_c

    :cond_1c
    move v6, v7

    goto/16 :goto_6

    :cond_1d
    move v5, v4

    goto/16 :goto_3

    :cond_1e
    move v3, v7

    goto/16 :goto_2
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 655449
    iput-boolean p1, p0, LX/3wb;->j:Z

    .line 655450
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3wb;->k:Z

    .line 655451
    return-void
.end method

.method public final d()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 655443
    iget-boolean v0, p0, LX/3wb;->j:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/3wb;->h()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3ut;->c:LX/3v0;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3wb;->x:Landroid/support/v7/widget/ActionMenuPresenter$OpenOverflowRunnable;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3ut;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->m()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 655444
    new-instance v0, LX/3wY;

    iget-object v2, p0, LX/3ut;->b:Landroid/content/Context;

    iget-object v3, p0, LX/3ut;->c:LX/3v0;

    iget-object v4, p0, LX/3wb;->i:Landroid/view/View;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/3wY;-><init>(LX/3wb;Landroid/content/Context;LX/3v0;Landroid/view/View;Z)V

    .line 655445
    new-instance v1, Landroid/support/v7/widget/ActionMenuPresenter$OpenOverflowRunnable;

    invoke-direct {v1, p0, v0}, Landroid/support/v7/widget/ActionMenuPresenter$OpenOverflowRunnable;-><init>(LX/3wb;LX/3wY;)V

    iput-object v1, p0, LX/3wb;->x:Landroid/support/v7/widget/ActionMenuPresenter$OpenOverflowRunnable;

    .line 655446
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, LX/3wb;->x:Landroid/support/v7/widget/ActionMenuPresenter$OpenOverflowRunnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 655447
    const/4 v0, 0x0

    invoke-super {p0, v0}, LX/3ut;->a(LX/3vG;)Z

    .line 655448
    :goto_0
    return v5

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 655434
    iget-object v0, p0, LX/3wb;->x:Landroid/support/v7/widget/ActionMenuPresenter$OpenOverflowRunnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    if-eqz v0, :cond_0

    .line 655435
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    check-cast v0, Landroid/view/View;

    iget-object v2, p0, LX/3wb;->x:Landroid/support/v7/widget/ActionMenuPresenter$OpenOverflowRunnable;

    invoke-virtual {v0, v2}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 655436
    const/4 v0, 0x0

    iput-object v0, p0, LX/3wb;->x:Landroid/support/v7/widget/ActionMenuPresenter$OpenOverflowRunnable;

    move v0, v1

    .line 655437
    :goto_0
    return v0

    .line 655438
    :cond_0
    iget-object v0, p0, LX/3wb;->v:LX/3wY;

    .line 655439
    if-eqz v0, :cond_1

    .line 655440
    invoke-virtual {v0}, LX/3vD;->e()V

    move v0, v1

    .line 655441
    goto :goto_0

    .line 655442
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 655431
    invoke-virtual {p0}, LX/3wb;->e()Z

    move-result v0

    .line 655432
    invoke-virtual {p0}, LX/3wb;->g()Z

    move-result v1

    or-int/2addr v0, v1

    .line 655433
    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 655427
    iget-object v0, p0, LX/3wb;->w:LX/3wU;

    if-eqz v0, :cond_0

    .line 655428
    iget-object v0, p0, LX/3wb;->w:LX/3wU;

    invoke-virtual {v0}, LX/3vD;->e()V

    .line 655429
    const/4 v0, 0x1

    .line 655430
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 655426
    iget-object v0, p0, LX/3wb;->v:LX/3wY;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3wb;->v:LX/3wY;

    invoke-virtual {v0}, LX/3vD;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
