.class public final LX/3lb;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 634551
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/2cp;)Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;
    .locals 2

    .prologue
    .line 634540
    if-nez p0, :cond_0

    .line 634541
    const/4 v0, 0x0

    .line 634542
    :goto_0
    return-object v0

    .line 634543
    :cond_0
    new-instance v0, LX/4YG;

    invoke-direct {v0}, LX/4YG;-><init>()V

    .line 634544
    invoke-interface {p0}, LX/2cp;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    .line 634545
    iput-object v1, v0, LX/4YG;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 634546
    invoke-interface {p0}, LX/2cp;->c()Ljava/lang/String;

    move-result-object v1

    .line 634547
    iput-object v1, v0, LX/4YG;->b:Ljava/lang/String;

    .line 634548
    invoke-interface {p0}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v1

    .line 634549
    iput-object v1, v0, LX/4YG;->c:Ljava/lang/String;

    .line 634550
    invoke-virtual {v0}, LX/4YG;->a()Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/1oS;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 634456
    if-nez p0, :cond_0

    .line 634457
    const/4 v0, 0x0

    .line 634458
    :goto_0
    return-object v0

    .line 634459
    :cond_0
    new-instance v3, LX/4YH;

    invoke-direct {v3}, LX/4YH;-><init>()V

    .line 634460
    invoke-interface {p0}, LX/1oS;->e()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    move-result-object v0

    .line 634461
    iput-object v0, v3, LX/4YH;->b:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    .line 634462
    invoke-interface {p0}, LX/1oS;->x_()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 634463
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v1, v2

    .line 634464
    :goto_1
    invoke-interface {p0}, LX/1oS;->x_()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 634465
    invoke-interface {p0}, LX/1oS;->x_()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-static {v0}, LX/3lb;->a(LX/2cp;)Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 634466
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 634467
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 634468
    iput-object v0, v3, LX/4YH;->c:LX/0Px;

    .line 634469
    :cond_2
    invoke-interface {p0}, LX/1oS;->y_()Ljava/lang/String;

    move-result-object v0

    .line 634470
    iput-object v0, v3, LX/4YH;->d:Ljava/lang/String;

    .line 634471
    invoke-interface {p0}, LX/1oS;->b()LX/1Fd;

    move-result-object v0

    .line 634472
    if-nez v0, :cond_5

    .line 634473
    const/4 v1, 0x0

    .line 634474
    :goto_2
    move-object v0, v1

    .line 634475
    iput-object v0, v3, LX/4YH;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 634476
    invoke-interface {p0}, LX/1oS;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 634477
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 634478
    :goto_3
    invoke-interface {p0}, LX/1oS;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 634479
    invoke-interface {p0}, LX/1oS;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cp;

    invoke-static {v0}, LX/3lb;->a(LX/2cp;)Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 634480
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 634481
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 634482
    iput-object v0, v3, LX/4YH;->g:LX/0Px;

    .line 634483
    :cond_4
    invoke-interface {p0}, LX/1oS;->c()Ljava/lang/String;

    move-result-object v0

    .line 634484
    iput-object v0, v3, LX/4YH;->h:Ljava/lang/String;

    .line 634485
    invoke-interface {p0}, LX/1oS;->d()Ljava/lang/String;

    move-result-object v0

    .line 634486
    iput-object v0, v3, LX/4YH;->i:Ljava/lang/String;

    .line 634487
    invoke-interface {p0}, LX/1oS;->k()LX/2cq;

    move-result-object v0

    .line 634488
    if-nez v0, :cond_6

    .line 634489
    const/4 v1, 0x0

    .line 634490
    :goto_4
    move-object v0, v1

    .line 634491
    iput-object v0, v3, LX/4YH;->j:Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    .line 634492
    invoke-interface {p0}, LX/1oS;->l()LX/0Px;

    move-result-object v0

    .line 634493
    iput-object v0, v3, LX/4YH;->k:LX/0Px;

    .line 634494
    invoke-virtual {v3}, LX/4YH;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    goto/16 :goto_0

    .line 634495
    :cond_5
    new-instance v1, LX/2dc;

    invoke-direct {v1}, LX/2dc;-><init>()V

    .line 634496
    invoke-interface {v0}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v4

    .line 634497
    iput-object v4, v1, LX/2dc;->f:Ljava/lang/String;

    .line 634498
    invoke-virtual {v1}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    goto :goto_2

    .line 634499
    :cond_6
    new-instance v1, LX/4YK;

    invoke-direct {v1}, LX/4YK;-><init>()V

    .line 634500
    invoke-interface {v0}, LX/2cq;->a()LX/0Px;

    move-result-object v2

    .line 634501
    iput-object v2, v1, LX/4YK;->b:LX/0Px;

    .line 634502
    invoke-interface {v0}, LX/2cq;->b()Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    move-result-object v2

    .line 634503
    iput-object v2, v1, LX/4YK;->c:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 634504
    invoke-interface {v0}, LX/2cq;->c()LX/0Px;

    move-result-object v2

    .line 634505
    iput-object v2, v1, LX/4YK;->d:LX/0Px;

    .line 634506
    invoke-interface {v0}, LX/2cq;->d()Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    move-result-object v2

    .line 634507
    iput-object v2, v1, LX/4YK;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 634508
    invoke-virtual {v1}, LX/4YK;->a()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v1

    goto :goto_4
.end method

.method public static a(Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$FetchComposerPrivacyGuardrailInfoModel;)Lcom/facebook/graphql/model/GraphQLViewer;
    .locals 10
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGraphQLViewer"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 634509
    if-nez p0, :cond_0

    .line 634510
    const/4 v0, 0x0

    .line 634511
    :goto_0
    return-object v0

    .line 634512
    :cond_0
    new-instance v0, LX/2sK;

    invoke-direct {v0}, LX/2sK;-><init>()V

    .line 634513
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$FetchComposerPrivacyGuardrailInfoModel;->a()Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$FetchComposerPrivacyGuardrailInfoModel$AudienceInfoModel;

    move-result-object v1

    .line 634514
    if-nez v1, :cond_1

    .line 634515
    const/4 v2, 0x0

    .line 634516
    :goto_1
    move-object v1, v2

    .line 634517
    iput-object v1, v0, LX/2sK;->d:Lcom/facebook/graphql/model/GraphQLAudienceInfo;

    .line 634518
    invoke-virtual {v0}, LX/2sK;->a()Lcom/facebook/graphql/model/GraphQLViewer;

    move-result-object v0

    goto :goto_0

    .line 634519
    :cond_1
    new-instance v2, LX/3lc;

    invoke-direct {v2}, LX/3lc;-><init>()V

    .line 634520
    invoke-virtual {v1}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$FetchComposerPrivacyGuardrailInfoModel$AudienceInfoModel;->a()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 634521
    if-nez v3, :cond_2

    .line 634522
    const/4 v5, 0x0

    .line 634523
    :goto_2
    move-object v3, v5

    .line 634524
    iput-object v3, v2, LX/3lc;->b:Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;

    .line 634525
    new-instance v3, Lcom/facebook/graphql/model/GraphQLAudienceInfo;

    invoke-direct {v3, v2}, Lcom/facebook/graphql/model/GraphQLAudienceInfo;-><init>(LX/3lc;)V

    .line 634526
    move-object v2, v3

    .line 634527
    goto :goto_1

    .line 634528
    :cond_2
    new-instance v6, LX/3ld;

    invoke-direct {v6}, LX/3ld;-><init>()V

    .line 634529
    const/4 v5, 0x0

    const-class v7, Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyOptionFieldsForComposerModel;

    invoke-virtual {v4, v3, v5, v7}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v5

    check-cast v5, LX/1oS;

    invoke-static {v5}, LX/3lb;->a(LX/1oS;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v5

    .line 634530
    iput-object v5, v6, LX/3ld;->b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634531
    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, LX/15i;->h(II)Z

    move-result v5

    .line 634532
    iput-boolean v5, v6, LX/3ld;->c:Z

    .line 634533
    const/4 v5, 0x2

    invoke-virtual {v4, v3, v5}, LX/15i;->k(II)J

    move-result-wide v7

    .line 634534
    iput-wide v7, v6, LX/3ld;->d:J

    .line 634535
    const/4 v5, 0x3

    const-class v7, Lcom/facebook/privacy/protocol/options/PrivacyOptionsGraphQLModels$PrivacyOptionFieldsForComposerModel;

    invoke-virtual {v4, v3, v5, v7}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v5

    check-cast v5, LX/1oS;

    invoke-static {v5}, LX/3lb;->a(LX/1oS;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v5

    .line 634536
    iput-object v5, v6, LX/3ld;->e:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634537
    new-instance v5, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;

    invoke-direct {v5, v6}, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;-><init>(LX/3ld;)V

    .line 634538
    move-object v5, v5

    .line 634539
    goto :goto_2
.end method
