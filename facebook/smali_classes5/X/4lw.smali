.class public LX/4lw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static a:Ljava/lang/reflect/Field;

.field public static b:Z

.field private static volatile c:LX/4lw;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 805016
    const/4 v0, 0x0

    sput-boolean v0, LX/4lw;->b:Z

    .line 805017
    :try_start_0
    const-class v0, Ljava/net/Socket;

    const-string v1, "impl"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 805018
    sput-object v0, LX/4lw;->a:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 805019
    sget-object v0, LX/4lw;->a:Ljava/lang/reflect/Field;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 805020
    const/4 v0, 0x1

    sput-boolean v0, LX/4lw;->b:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 805021
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 805038
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 805039
    return-void
.end method

.method public static a(LX/0QB;)LX/4lw;
    .locals 3

    .prologue
    .line 805026
    sget-object v0, LX/4lw;->c:LX/4lw;

    if-nez v0, :cond_1

    .line 805027
    const-class v1, LX/4lw;

    monitor-enter v1

    .line 805028
    :try_start_0
    sget-object v0, LX/4lw;->c:LX/4lw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 805029
    if-eqz v2, :cond_0

    .line 805030
    :try_start_1
    new-instance v0, LX/4lw;

    invoke-direct {v0}, LX/4lw;-><init>()V

    .line 805031
    move-object v0, v0

    .line 805032
    sput-object v0, LX/4lw;->c:LX/4lw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 805033
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 805034
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 805035
    :cond_1
    sget-object v0, LX/4lw;->c:LX/4lw;

    return-object v0

    .line 805036
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 805037
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/net/Socket;[BLjava/lang/String;I)V
    .locals 2

    .prologue
    .line 805022
    :try_start_0
    sget-object v0, LX/4lw;->a:Ljava/lang/reflect/Field;

    new-instance v1, LX/4lt;

    invoke-direct {v1, p1, p2, p3}, LX/4lt;-><init>([BLjava/lang/String;I)V

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 805023
    return-void

    .line 805024
    :catch_0
    move-exception v0

    .line 805025
    new-instance v1, LX/4ll;

    invoke-direct {v1, v0}, LX/4ll;-><init>(Ljava/lang/Exception;)V

    throw v1
.end method
