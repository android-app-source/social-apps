.class public LX/3fc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/3MX;

.field private final b:LX/3fd;

.field public c:LX/00H;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3MX;LX/3fd;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 622765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 622766
    iput-object p1, p0, LX/3fc;->a:LX/3MX;

    .line 622767
    iput-object p2, p0, LX/3fc;->b:LX/3fd;

    .line 622768
    return-void
.end method

.method public static b(LX/0QB;)LX/3fc;
    .locals 3

    .prologue
    .line 622761
    new-instance v2, LX/3fc;

    invoke-static {p0}, LX/3MX;->a(LX/0QB;)LX/3MX;

    move-result-object v0

    check-cast v0, LX/3MX;

    invoke-static {p0}, LX/3fd;->a(LX/0QB;)LX/3fd;

    move-result-object v1

    check-cast v1, LX/3fd;

    invoke-direct {v2, v0, v1}, LX/3fc;-><init>(LX/3MX;LX/3fd;)V

    .line 622762
    const-class v0, LX/00H;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/00H;

    .line 622763
    iput-object v0, v2, LX/3fc;->c:LX/00H;

    .line 622764
    return-object v2
.end method


# virtual methods
.method public final a(LX/0gW;)V
    .locals 3

    .prologue
    .line 622757
    const-string v0, "small_img_size"

    iget-object v1, p0, LX/3fc;->a:LX/3MX;

    sget-object v2, LX/3MY;->SMALL:LX/3MY;

    invoke-virtual {v1, v2}, LX/3MX;->b(LX/3MY;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 622758
    const-string v0, "big_img_size"

    iget-object v1, p0, LX/3fc;->a:LX/3MX;

    sget-object v2, LX/3MY;->BIG:LX/3MY;

    invoke-virtual {v1, v2}, LX/3MX;->b(LX/3MY;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 622759
    const-string v0, "huge_img_size"

    iget-object v1, p0, LX/3fc;->a:LX/3MX;

    sget-object v2, LX/3MY;->HUGE:LX/3MY;

    invoke-virtual {v1, v2}, LX/3MX;->b(LX/3MY;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 622760
    return-void
.end method

.method public final b(LX/0gW;)V
    .locals 6

    .prologue
    .line 622740
    const-string v0, "profile_types"

    iget-object v1, p0, LX/3fc;->b:LX/3fd;

    .line 622741
    iget-object v2, v1, LX/3fd;->a:LX/0Px;

    if-nez v2, :cond_3

    .line 622742
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v3

    .line 622743
    iget-object v2, v1, LX/3fd;->c:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3gu;

    .line 622744
    invoke-interface {v2}, LX/3gu;->a()LX/0Rf;

    move-result-object v2

    invoke-virtual {v2}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2RU;

    .line 622745
    sget-object p0, LX/2RU;->UNMATCHED:LX/2RU;

    if-ne v2, p0, :cond_1

    .line 622746
    sget-object v2, LX/3fd;->b:Ljava/lang/Class;

    const-string p0, "Requesting UNMATCHED profile types not allowed"

    invoke-static {v2, p0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 622747
    :cond_1
    invoke-virtual {v2}, LX/2RU;->getGraphQlParamValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 622748
    :cond_2
    invoke-virtual {v3}, LX/0cA;->b()LX/0Rf;

    move-result-object v2

    invoke-virtual {v2}, LX/0Py;->asList()LX/0Px;

    move-result-object v2

    move-object v2, v2

    .line 622749
    iput-object v2, v1, LX/3fd;->a:LX/0Px;

    .line 622750
    :cond_3
    iget-object v2, v1, LX/3fd;->a:LX/0Px;

    move-object v1, v2

    .line 622751
    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    .line 622752
    return-void
.end method

.method public final c(LX/0gW;)V
    .locals 3

    .prologue
    .line 622753
    const-string v0, "is_for_messenger"

    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    iget-object v2, p0, LX/3fc;->c:LX/00H;

    .line 622754
    iget-object p0, v2, LX/00H;->j:LX/01T;

    move-object v2, p0

    .line 622755
    invoke-virtual {v1, v2}, LX/01T;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 622756
    return-void
.end method
