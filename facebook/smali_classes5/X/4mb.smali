.class public LX/4mb;
.super Landroid/app/AlertDialog$Builder;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/DialogInterface$OnClickListener;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 806037
    invoke-direct {p0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 806038
    iput-object p1, p0, LX/4mb;->c:Landroid/content/Context;

    .line 806039
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/4mb;->a:Ljava/util/ArrayList;

    .line 806040
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/4mb;->b:Ljava/util/ArrayList;

    .line 806041
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/4mb;
    .locals 1

    .prologue
    .line 806042
    iget-object v0, p0, LX/4mb;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 806043
    iget-object v0, p0, LX/4mb;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 806044
    return-object p0
.end method

.method public final show()Landroid/app/AlertDialog;
    .locals 2

    .prologue
    .line 806045
    iget-object v0, p0, LX/4mb;->a:Ljava/util/ArrayList;

    iget-object v1, p0, LX/4mb;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    new-instance v1, LX/4ma;

    invoke-direct {v1, p0}, LX/4ma;-><init>(LX/4mb;)V

    invoke-virtual {p0, v0, v1}, LX/4mb;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 806046
    const/4 v0, 0x0

    iput-object v0, p0, LX/4mb;->a:Ljava/util/ArrayList;

    .line 806047
    invoke-super {p0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    .line 806048
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 806049
    return-object v0
.end method
