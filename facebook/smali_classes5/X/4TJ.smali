.class public LX/4TJ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 717802
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 27

    .prologue
    .line 717803
    const/16 v23, 0x0

    .line 717804
    const/16 v22, 0x0

    .line 717805
    const/16 v21, 0x0

    .line 717806
    const/16 v20, 0x0

    .line 717807
    const/16 v19, 0x0

    .line 717808
    const/16 v18, 0x0

    .line 717809
    const-wide/16 v16, 0x0

    .line 717810
    const/4 v15, 0x0

    .line 717811
    const/4 v14, 0x0

    .line 717812
    const/4 v13, 0x0

    .line 717813
    const/4 v12, 0x0

    .line 717814
    const/4 v11, 0x0

    .line 717815
    const/4 v10, 0x0

    .line 717816
    const/4 v9, 0x0

    .line 717817
    const/4 v8, 0x0

    .line 717818
    const/4 v7, 0x0

    .line 717819
    const/4 v6, 0x0

    .line 717820
    const/4 v5, 0x0

    .line 717821
    const/4 v4, 0x0

    .line 717822
    const/4 v3, 0x0

    .line 717823
    const/4 v2, 0x0

    .line 717824
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_17

    .line 717825
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 717826
    const/4 v2, 0x0

    .line 717827
    :goto_0
    return v2

    .line 717828
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_14

    .line 717829
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 717830
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 717831
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v26, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v26

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 717832
    const-string v6, "action_links"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 717833
    invoke-static/range {p0 .. p1}, LX/2bb;->b(LX/15w;LX/186;)I

    move-result v2

    move/from16 v25, v2

    goto :goto_1

    .line 717834
    :cond_1
    const-string v6, "all_stories"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 717835
    invoke-static/range {p0 .. p1}, LX/4TL;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v24, v2

    goto :goto_1

    .line 717836
    :cond_2
    const-string v6, "cache_id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 717837
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v23, v2

    goto :goto_1

    .line 717838
    :cond_3
    const-string v6, "collection_type_list"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 717839
    const-class v2, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v2}, LX/2gu;->a(LX/15w;LX/186;Ljava/lang/Class;)I

    move-result v2

    move/from16 v22, v2

    goto :goto_1

    .line 717840
    :cond_4
    const-string v6, "debug_info"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 717841
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v21, v2

    goto :goto_1

    .line 717842
    :cond_5
    const-string v6, "feed_unit_pagination_identifier"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 717843
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v7, v2

    goto/16 :goto_1

    .line 717844
    :cond_6
    const-string v6, "fetchTimeMs"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 717845
    const/4 v2, 0x1

    .line 717846
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 717847
    :cond_7
    const-string v6, "hideable_token"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 717848
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 717849
    :cond_8
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 717850
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 717851
    :cond_9
    const-string v6, "local_last_negative_feedback_action_type"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 717852
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 717853
    :cond_a
    const-string v6, "local_story_visibility"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 717854
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 717855
    :cond_b
    const-string v6, "local_story_visible_height"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 717856
    const/4 v2, 0x1

    .line 717857
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move/from16 v16, v6

    goto/16 :goto_1

    .line 717858
    :cond_c
    const-string v6, "negative_feedback_actions"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 717859
    invoke-static/range {p0 .. p1}, LX/2bY;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 717860
    :cond_d
    const-string v6, "set_icon"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 717861
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 717862
    :cond_e
    const-string v6, "short_term_cache_key"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 717863
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 717864
    :cond_f
    const-string v6, "title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 717865
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 717866
    :cond_10
    const-string v6, "titleForSummary"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 717867
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 717868
    :cond_11
    const-string v6, "tracking"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 717869
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 717870
    :cond_12
    const-string v6, "url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 717871
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v9, v2

    goto/16 :goto_1

    .line 717872
    :cond_13
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 717873
    :cond_14
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 717874
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 717875
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 717876
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 717877
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 717878
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 717879
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 717880
    if-eqz v3, :cond_15

    .line 717881
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 717882
    :cond_15
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 717883
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 717884
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 717885
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 717886
    if-eqz v8, :cond_16

    .line 717887
    const/16 v2, 0xc

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 717888
    :cond_16
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 717889
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 717890
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 717891
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 717892
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 717893
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 717894
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 717895
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_17
    move/from16 v24, v22

    move/from16 v25, v23

    move/from16 v22, v20

    move/from16 v23, v21

    move/from16 v20, v15

    move/from16 v21, v19

    move v15, v10

    move/from16 v19, v14

    move v14, v9

    move v10, v5

    move v9, v4

    move-wide/from16 v4, v16

    move/from16 v16, v11

    move/from16 v17, v12

    move v12, v7

    move v11, v6

    move/from16 v7, v18

    move/from16 v18, v13

    move v13, v8

    move v8, v2

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 717896
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 717897
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 717898
    invoke-static {p0, v2}, LX/4TJ;->a(LX/15w;LX/186;)I

    move-result v1

    .line 717899
    if-eqz v0, :cond_0

    .line 717900
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 717901
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 717902
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 717903
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 717904
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 717905
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 717906
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x4

    .line 717907
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 717908
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717909
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 717910
    const-string v0, "name"

    const-string v1, "StorySet"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 717911
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 717912
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 717913
    if-eqz v0, :cond_0

    .line 717914
    const-string v1, "action_links"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717915
    invoke-static {p0, v0, p2, p3}, LX/2bb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 717916
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 717917
    if-eqz v0, :cond_1

    .line 717918
    const-string v1, "all_stories"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717919
    invoke-static {p0, v0, p2, p3}, LX/4TL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 717920
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 717921
    if-eqz v0, :cond_2

    .line 717922
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717923
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 717924
    :cond_2
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 717925
    if-eqz v0, :cond_3

    .line 717926
    const-string v0, "collection_type_list"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717927
    const-class v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->b(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->b(Ljava/util/Iterator;LX/0nX;)V

    .line 717928
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 717929
    if-eqz v0, :cond_4

    .line 717930
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717931
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 717932
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 717933
    if-eqz v0, :cond_5

    .line 717934
    const-string v1, "feed_unit_pagination_identifier"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717935
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 717936
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 717937
    cmp-long v2, v0, v4

    if-eqz v2, :cond_6

    .line 717938
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717939
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 717940
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 717941
    if-eqz v0, :cond_7

    .line 717942
    const-string v1, "hideable_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717943
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 717944
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 717945
    if-eqz v0, :cond_8

    .line 717946
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717947
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 717948
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 717949
    if-eqz v0, :cond_9

    .line 717950
    const-string v1, "local_last_negative_feedback_action_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717951
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 717952
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 717953
    if-eqz v0, :cond_a

    .line 717954
    const-string v1, "local_story_visibility"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717955
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 717956
    :cond_a
    const/16 v0, 0xc

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 717957
    if-eqz v0, :cond_b

    .line 717958
    const-string v1, "local_story_visible_height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717959
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 717960
    :cond_b
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 717961
    if-eqz v0, :cond_c

    .line 717962
    const-string v1, "negative_feedback_actions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717963
    invoke-static {p0, v0, p2, p3}, LX/2bY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 717964
    :cond_c
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 717965
    if-eqz v0, :cond_d

    .line 717966
    const-string v1, "set_icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717967
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 717968
    :cond_d
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 717969
    if-eqz v0, :cond_e

    .line 717970
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717971
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 717972
    :cond_e
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 717973
    if-eqz v0, :cond_f

    .line 717974
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717975
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 717976
    :cond_f
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 717977
    if-eqz v0, :cond_10

    .line 717978
    const-string v1, "titleForSummary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717979
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 717980
    :cond_10
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 717981
    if-eqz v0, :cond_11

    .line 717982
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717983
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 717984
    :cond_11
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 717985
    if-eqz v0, :cond_12

    .line 717986
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 717987
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 717988
    :cond_12
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 717989
    return-void
.end method
