.class public final LX/4Xp;
.super LX/0ur;
.source ""


# instance fields
.field public b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:J

.field public f:I

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:I

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 772489
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 772490
    const/4 v0, 0x0

    iput-object v0, p0, LX/4Xp;->q:LX/0x2;

    .line 772491
    instance-of v0, p0, LX/4Xp;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 772492
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)LX/4Xp;
    .locals 4

    .prologue
    .line 772493
    new-instance v1, LX/4Xp;

    invoke-direct {v1}, LX/4Xp;-><init>()V

    .line 772494
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 772495
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;

    move-result-object v0

    iput-object v0, v1, LX/4Xp;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;

    .line 772496
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xp;->c:Ljava/lang/String;

    .line 772497
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->E_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xp;->d:Ljava/lang/String;

    .line 772498
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->F_()J

    move-result-wide v2

    iput-wide v2, v1, LX/4Xp;->e:J

    .line 772499
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->s()I

    move-result v0

    iput v0, v1, LX/4Xp;->f:I

    .line 772500
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->C_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xp;->g:Ljava/lang/String;

    .line 772501
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->t()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xp;->h:Ljava/lang/String;

    .line 772502
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->A()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/4Xp;->i:LX/0Px;

    .line 772503
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->y()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xp;->j:Ljava/lang/String;

    .line 772504
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->z()I

    move-result v0

    iput v0, v1, LX/4Xp;->k:I

    .line 772505
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xp;->l:Ljava/lang/String;

    .line 772506
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/4Xp;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 772507
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/4Xp;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 772508
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xp;->o:Ljava/lang/String;

    .line 772509
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->x()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xp;->p:Ljava/lang/String;

    .line 772510
    invoke-static {v1, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 772511
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->L_()LX/0x2;

    move-result-object v0

    invoke-virtual {v0}, LX/0x2;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x2;

    iput-object v0, v1, LX/4Xp;->q:LX/0x2;

    .line 772512
    return-object v1
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;
    .locals 2

    .prologue
    .line 772513
    new-instance v0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;-><init>(LX/4Xp;)V

    .line 772514
    return-object v0
.end method
