.class public LX/4Te;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 719186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 719187
    const/4 v10, 0x0

    .line 719188
    const/4 v9, 0x0

    .line 719189
    const/4 v8, 0x0

    .line 719190
    const/4 v7, 0x0

    .line 719191
    const/4 v6, 0x0

    .line 719192
    const/4 v5, 0x0

    .line 719193
    const/4 v4, 0x0

    .line 719194
    const/4 v3, 0x0

    .line 719195
    const/4 v2, 0x0

    .line 719196
    const/4 v1, 0x0

    .line 719197
    const/4 v0, 0x0

    .line 719198
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 719199
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 719200
    const/4 v0, 0x0

    .line 719201
    :goto_0
    return v0

    .line 719202
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 719203
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_b

    .line 719204
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 719205
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 719206
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 719207
    const-string v12, "associated_places_info"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 719208
    invoke-static {p0, p1}, LX/4RL;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 719209
    :cond_2
    const-string v12, "cursor"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 719210
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 719211
    :cond_3
    const-string v12, "custom_icon_suggestions"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 719212
    invoke-static {p0, p1}, LX/3Ar;->b(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 719213
    :cond_4
    const-string v12, "display_name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 719214
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 719215
    :cond_5
    const-string v12, "icon"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 719216
    invoke-static {p0, p1}, LX/3Ar;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 719217
    :cond_6
    const-string v12, "iconImageLarge"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 719218
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 719219
    :cond_7
    const-string v12, "node"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 719220
    invoke-static {p0, p1}, LX/2aw;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 719221
    :cond_8
    const-string v12, "show_attachment_preview"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 719222
    const/4 v0, 0x1

    .line 719223
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    goto/16 :goto_1

    .line 719224
    :cond_9
    const-string v12, "subtext"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 719225
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 719226
    :cond_a
    const-string v12, "tracking"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 719227
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_1

    .line 719228
    :cond_b
    const/16 v11, 0xa

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 719229
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 719230
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 719231
    const/4 v9, 0x2

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 719232
    const/4 v8, 0x3

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 719233
    const/4 v7, 0x4

    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 719234
    const/4 v6, 0x5

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 719235
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 719236
    if-eqz v0, :cond_c

    .line 719237
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 719238
    :cond_c
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 719239
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 719240
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method
