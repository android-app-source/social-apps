.class public LX/4Mh;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 689085
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 689071
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 689072
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 689073
    :goto_0
    return v1

    .line 689074
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_2

    .line 689075
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 689076
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 689077
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_0

    if-eqz v7, :cond_0

    .line 689078
    const-string v8, "fetchTimeMs"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 689079
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v6

    goto :goto_1

    .line 689080
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 689081
    :cond_2
    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 689082
    if-eqz v0, :cond_3

    move-object v0, p1

    .line 689083
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 689084
    :cond_3
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move-wide v2, v4

    goto :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 689086
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 689087
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 689088
    invoke-static {p0, v2}, LX/4Mh;->a(LX/15w;LX/186;)I

    move-result v1

    .line 689089
    if-eqz v0, :cond_0

    .line 689090
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 689091
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 689092
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 689093
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 689094
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 689095
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 689096
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 689060
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 689061
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689062
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 689063
    const-string v0, "name"

    const-string v1, "FindPagesFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 689064
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 689065
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 689066
    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 689067
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689068
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 689069
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 689070
    return-void
.end method
