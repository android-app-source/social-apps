.class public LX/3fw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/3fb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3fx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation runtime Lcom/facebook/common/hardware/PhoneIsoCountryCode;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/3fc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 623862
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/0QB;)LX/3fw;
    .locals 5

    .prologue
    .line 623864
    new-instance v3, LX/3fw;

    invoke-direct {v3}, LX/3fw;-><init>()V

    .line 623865
    invoke-static {p0}, LX/3fb;->a(LX/0QB;)LX/3fb;

    move-result-object v0

    check-cast v0, LX/3fb;

    invoke-static {p0}, LX/3fx;->a(LX/0QB;)LX/3fx;

    move-result-object v1

    check-cast v1, LX/3fx;

    const/16 v2, 0x15ec

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/3fc;->b(LX/0QB;)LX/3fc;

    move-result-object v2

    check-cast v2, LX/3fc;

    .line 623866
    iput-object v0, v3, LX/3fw;->a:LX/3fb;

    iput-object v1, v3, LX/3fw;->b:LX/3fx;

    iput-object v4, v3, LX/3fw;->c:LX/0Or;

    iput-object v2, v3, LX/3fw;->d:LX/3fc;

    .line 623867
    return-object v3
.end method


# virtual methods
.method public final a(Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;)Lcom/facebook/contacts/graphql/Contact;
    .locals 1

    .prologue
    .line 623863
    iget-object v0, p0, LX/3fw;->a:LX/3fb;

    invoke-virtual {v0, p1}, LX/3fb;->a(Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;)LX/3hB;

    move-result-object v0

    invoke-virtual {v0}, LX/3hB;->O()Lcom/facebook/contacts/graphql/Contact;

    move-result-object v0

    return-object v0
.end method
