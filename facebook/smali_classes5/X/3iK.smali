.class public LX/3iK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final a:LX/1K9;

.field public final b:LX/0bH;

.field private final c:Ljava/util/concurrent/Executor;

.field public final d:LX/3iL;

.field public final e:LX/3H7;

.field private final f:LX/0ad;

.field private final g:LX/0if;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/3iM;

.field public final j:LX/1Ck;

.field public final k:LX/3iP;

.field private l:LX/0SI;

.field public final m:LX/3iQ;

.field public final n:LX/3id;

.field public final o:Z

.field private final p:Z

.field private final q:LX/0tn;


# direct methods
.method public constructor <init>(LX/1K9;LX/0bH;Ljava/util/concurrent/Executor;LX/3iL;LX/3H7;LX/0Ot;LX/3iM;LX/1Ck;LX/3iP;LX/0SI;LX/3iQ;LX/3id;LX/0ad;LX/0if;LX/0tn;Lcom/facebook/feedback/ui/FeedbackControllerParams;)V
    .locals 2
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p16    # Lcom/facebook/feedback/ui/FeedbackControllerParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1K9;",
            "LX/0bH;",
            "Ljava/util/concurrent/Executor;",
            "LX/3iL;",
            "LX/3H7;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;",
            "LX/3iM;",
            "LX/1Ck;",
            "LX/3iP;",
            "LX/0SI;",
            "LX/3iQ;",
            "LX/3id;",
            "LX/0ad;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "LX/0tn;",
            "Lcom/facebook/feedback/ui/FeedbackControllerParams;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 629179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 629180
    iput-object p1, p0, LX/3iK;->a:LX/1K9;

    .line 629181
    iput-object p2, p0, LX/3iK;->b:LX/0bH;

    .line 629182
    iput-object p6, p0, LX/3iK;->h:LX/0Ot;

    .line 629183
    iput-object p4, p0, LX/3iK;->d:LX/3iL;

    .line 629184
    iput-object p5, p0, LX/3iK;->e:LX/3H7;

    .line 629185
    iput-object p13, p0, LX/3iK;->f:LX/0ad;

    .line 629186
    move-object/from16 v0, p14

    iput-object v0, p0, LX/3iK;->g:LX/0if;

    .line 629187
    invoke-virtual/range {p16 .. p16}, Lcom/facebook/feedback/ui/FeedbackControllerParams;->a()Z

    move-result v1

    iput-boolean v1, p0, LX/3iK;->p:Z

    .line 629188
    iput-object p7, p0, LX/3iK;->i:LX/3iM;

    .line 629189
    iput-object p3, p0, LX/3iK;->c:Ljava/util/concurrent/Executor;

    .line 629190
    iput-object p8, p0, LX/3iK;->j:LX/1Ck;

    .line 629191
    iput-object p10, p0, LX/3iK;->l:LX/0SI;

    .line 629192
    iput-object p9, p0, LX/3iK;->k:LX/3iP;

    .line 629193
    iput-object p11, p0, LX/3iK;->m:LX/3iQ;

    .line 629194
    iput-object p12, p0, LX/3iK;->n:LX/3id;

    .line 629195
    invoke-virtual/range {p16 .. p16}, Lcom/facebook/feedback/ui/FeedbackControllerParams;->b()Z

    move-result v1

    iput-boolean v1, p0, LX/3iK;->o:Z

    .line 629196
    move-object/from16 v0, p15

    iput-object v0, p0, LX/3iK;->q:LX/0tn;

    .line 629197
    return-void
.end method

.method public static a(LX/3iK;Ljava/lang/String;Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;Lcom/facebook/graphql/model/GraphQLFeedback;)LX/1L9;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ")",
            "LX/1L9",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 629178
    new-instance v0, LX/9DQ;

    invoke-direct {v0, p0, p1, p3, p2}, LX/9DQ;-><init>(LX/3iK;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 629172
    iget-object v0, p0, LX/3iK;->j:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 629173
    iget-object v0, p0, LX/3iK;->m:LX/3iQ;

    .line 629174
    iget-object v1, v0, LX/3iQ;->d:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 629175
    iget-object v0, p0, LX/3iK;->n:LX/3id;

    .line 629176
    iget-object v1, v0, LX/3id;->b:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 629177
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V
    .locals 4

    .prologue
    .line 629156
    const/4 v0, 0x0

    .line 629157
    iget-object v1, p0, LX/3iK;->n:LX/3id;

    iget-boolean v2, p0, LX/3iK;->o:Z

    .line 629158
    new-instance v3, LX/9DR;

    invoke-direct {v3, p0, v0}, LX/9DR;-><init>(LX/3iK;LX/0Ve;)V

    move-object v3, v3

    .line 629159
    invoke-virtual {v1, p1, p2, v2, v3}, LX/3id;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZLX/1L9;)V

    .line 629160
    return-void
.end method

.method public final a(Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 629166
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 629167
    iget-object v0, p0, LX/3iK;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3RX;

    const-string v2, "post_comment"

    invoke-virtual {v0, v2}, LX/3RX;->a(Ljava/lang/String;)V

    .line 629168
    iget-object v0, p0, LX/3iK;->m:LX/3iQ;

    invoke-static {p0, v1, p1, p2}, LX/3iK;->a(LX/3iK;Ljava/lang/String;Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;Lcom/facebook/graphql/model/GraphQLFeedback;)LX/1L9;

    move-result-object v6

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v6}, LX/3iQ;->a(Ljava/lang/String;Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZLX/1L9;)V

    .line 629169
    iget-object v0, p0, LX/3iK;->f:LX/0ad;

    sget-short v1, LX/0wn;->aI:S

    invoke-interface {v0, v1, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3iK;->f:LX/0ad;

    sget-short v1, LX/0wn;->aH:S

    invoke-interface {v0, v1, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 629170
    :cond_0
    iget-object v0, p0, LX/3iK;->g:LX/0if;

    sget-object v1, LX/0ig;->r:LX/0ih;

    const-string v2, "Posting_Comments"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 629171
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/common/callercontext/CallerContext;ZLX/451;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Z",
            "LX/451",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 629161
    iget-object v0, p0, LX/3iK;->e:LX/3H7;

    sget-object v2, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    sget-object v3, LX/21y;->DEFAULT_ORDER:LX/21y;

    const/4 v4, 0x0

    move-object v1, p1

    move-object v6, v5

    move v7, p4

    move-object v8, p3

    move-object v9, p2

    invoke-virtual/range {v0 .. v9}, LX/3H7;->a(Ljava/lang/String;LX/0rS;LX/21y;ZLjava/lang/String;Ljava/lang/String;ZLcom/facebook/common/callercontext/CallerContext;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 629162
    invoke-virtual {p5}, LX/451;->a()V

    .line 629163
    if-eqz v0, :cond_0

    .line 629164
    iget-object v1, p0, LX/3iK;->j:LX/1Ck;

    const-string v2, "fetch_feedback_with_viewer_context"

    invoke-virtual {v1, v2, v0, p5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 629165
    :cond_0
    return-void
.end method
