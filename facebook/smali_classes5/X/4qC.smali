.class public final LX/4qC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final _isPrimitive:Z

.field private final _nullValue:Ljava/lang/Object;

.field private final _rawType:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0lJ;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 813178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 813179
    iput-object p2, p0, LX/4qC;->_nullValue:Ljava/lang/Object;

    .line 813180
    invoke-virtual {p1}, LX/0lJ;->j()Z

    move-result v0

    iput-boolean v0, p0, LX/4qC;->_isPrimitive:Z

    .line 813181
    iget-object v0, p1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 813182
    iput-object v0, p0, LX/4qC;->_rawType:Ljava/lang/Class;

    .line 813183
    return-void
.end method


# virtual methods
.method public final a(LX/0n3;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 813175
    iget-boolean v0, p0, LX/4qC;->_isPrimitive:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/0mv;->FAIL_ON_NULL_FOR_PRIMITIVES:LX/0mv;

    invoke-virtual {p1, v0}, LX/0n3;->a(LX/0mv;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 813176
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Can not map JSON null into type "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/4qC;->_rawType:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (set DeserializationConfig.DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES to \'false\' to allow)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0n3;->c(Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 813177
    :cond_0
    iget-object v0, p0, LX/4qC;->_nullValue:Ljava/lang/Object;

    return-object v0
.end method
