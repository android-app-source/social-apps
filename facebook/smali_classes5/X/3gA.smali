.class public LX/3gA;
.super LX/3gB;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/3gA;


# instance fields
.field public a:LX/0WJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3gD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:LX/3gC;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 624106
    invoke-direct {p0}, LX/3gB;-><init>()V

    .line 624107
    new-instance v0, LX/3gC;

    invoke-direct {v0, p0}, LX/3gC;-><init>(LX/3gA;)V

    iput-object v0, p0, LX/3gA;->c:LX/3gC;

    .line 624108
    return-void
.end method

.method public static a(LX/0QB;)LX/3gA;
    .locals 5

    .prologue
    .line 624114
    sget-object v0, LX/3gA;->d:LX/3gA;

    if-nez v0, :cond_1

    .line 624115
    const-class v1, LX/3gA;

    monitor-enter v1

    .line 624116
    :try_start_0
    sget-object v0, LX/3gA;->d:LX/3gA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 624117
    if-eqz v2, :cond_0

    .line 624118
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 624119
    new-instance p0, LX/3gA;

    invoke-direct {p0}, LX/3gA;-><init>()V

    .line 624120
    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v3

    check-cast v3, LX/0WJ;

    invoke-static {v0}, LX/3gD;->b(LX/0QB;)LX/3gD;

    move-result-object v4

    check-cast v4, LX/3gD;

    .line 624121
    iput-object v3, p0, LX/3gA;->a:LX/0WJ;

    iput-object v4, p0, LX/3gA;->b:LX/3gD;

    .line 624122
    move-object v0, p0

    .line 624123
    sput-object v0, LX/3gA;->d:LX/3gA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 624124
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 624125
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 624126
    :cond_1
    sget-object v0, LX/3gA;->d:LX/3gA;

    return-object v0

    .line 624127
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 624128
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()LX/2ZE;
    .locals 1

    .prologue
    .line 624113
    iget-object v0, p0, LX/3gA;->c:LX/3gC;

    return-object v0
.end method

.method public final dr_()J
    .locals 2

    .prologue
    .line 624109
    iget-object v0, p0, LX/3gA;->a:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v0

    .line 624110
    if-eqz v0, :cond_0

    .line 624111
    iget-boolean v1, v0, Lcom/facebook/user/model/User;->o:Z

    move v0, v1

    .line 624112
    if-nez v0, :cond_1

    :cond_0
    const-wide/32 v0, 0x240c8400

    :goto_0
    return-wide v0

    :cond_1
    const-wide/32 v0, 0x5265c00

    goto :goto_0
.end method
