.class public LX/4OJ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 696064
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 696047
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_5

    .line 696048
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 696049
    :goto_0
    return v1

    .line 696050
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_3

    .line 696051
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 696052
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 696053
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v5, :cond_0

    .line 696054
    const-string v6, "is_group_admin"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 696055
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 696056
    :cond_1
    const-string v6, "node"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 696057
    invoke-static {p0, p1}, LX/2bO;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 696058
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 696059
    :cond_3
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 696060
    if-eqz v0, :cond_4

    .line 696061
    invoke-virtual {p1, v1, v4}, LX/186;->a(IZ)V

    .line 696062
    :cond_4
    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 696063
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 696036
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 696037
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 696038
    if-eqz v0, :cond_0

    .line 696039
    const-string v1, "is_group_admin"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 696040
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 696041
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 696042
    if-eqz v0, :cond_1

    .line 696043
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 696044
    invoke-static {p0, v0, p2, p3}, LX/2bO;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 696045
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 696046
    return-void
.end method
