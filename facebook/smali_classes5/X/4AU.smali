.class public LX/4AU;
.super Landroid/graphics/drawable/Drawable;
.source ""


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private final b:Landroid/graphics/Path;

.field private final c:Landroid/graphics/RectF;

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field private h:I

.field public i:I

.field private j:Z

.field public k:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 676291
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 676292
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/4AU;->a:Landroid/graphics/Paint;

    .line 676293
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/4AU;->b:Landroid/graphics/Path;

    .line 676294
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/4AU;->c:Landroid/graphics/RectF;

    .line 676295
    const/high16 v0, -0x80000000

    iput v0, p0, LX/4AU;->d:I

    .line 676296
    const v0, -0x7fff7f01

    iput v0, p0, LX/4AU;->e:I

    .line 676297
    const/16 v0, 0xa

    iput v0, p0, LX/4AU;->f:I

    .line 676298
    const/16 v0, 0x14

    iput v0, p0, LX/4AU;->g:I

    .line 676299
    iput v2, p0, LX/4AU;->h:I

    .line 676300
    iput v2, p0, LX/4AU;->i:I

    .line 676301
    iput-boolean v2, p0, LX/4AU;->j:Z

    .line 676302
    iput-boolean v2, p0, LX/4AU;->k:Z

    return-void
.end method

.method private a(Landroid/graphics/Canvas;I)V
    .locals 5

    .prologue
    .line 676311
    iget-object v0, p0, LX/4AU;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 676312
    iget-object v0, p0, LX/4AU;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 676313
    iget-object v0, p0, LX/4AU;->b:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 676314
    iget-object v0, p0, LX/4AU;->b:Landroid/graphics/Path;

    sget-object v1, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 676315
    iget-object v0, p0, LX/4AU;->b:Landroid/graphics/Path;

    iget-object v1, p0, LX/4AU;->c:Landroid/graphics/RectF;

    iget v2, p0, LX/4AU;->i:I

    iget v3, p0, LX/4AU;->g:I

    div-int/lit8 v3, v3, 0x2

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, LX/4AU;->i:I

    iget v4, p0, LX/4AU;->g:I

    div-int/lit8 v4, v4, 0x2

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    int-to-float v3, v3

    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    .line 676316
    iget-object v0, p0, LX/4AU;->b:Landroid/graphics/Path;

    iget-object v1, p0, LX/4AU;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 676317
    return-void
.end method

.method private a(Landroid/graphics/Canvas;II)V
    .locals 6

    .prologue
    .line 676325
    invoke-virtual {p0}, LX/4AU;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 676326
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    iget v2, p0, LX/4AU;->f:I

    mul-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    mul-int/2addr v1, p2

    div-int/lit16 v1, v1, 0x2710

    .line 676327
    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v3, p0, LX/4AU;->f:I

    add-int/2addr v2, v3

    .line 676328
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, LX/4AU;->f:I

    sub-int/2addr v0, v3

    iget v3, p0, LX/4AU;->g:I

    sub-int/2addr v0, v3

    .line 676329
    iget-object v3, p0, LX/4AU;->c:Landroid/graphics/RectF;

    int-to-float v4, v2

    int-to-float v5, v0

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p0, LX/4AU;->g:I

    add-int/2addr v0, v2

    int-to-float v0, v0

    invoke-virtual {v3, v4, v5, v1, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 676330
    invoke-direct {p0, p1, p3}, LX/4AU;->a(Landroid/graphics/Canvas;I)V

    .line 676331
    return-void
.end method

.method private b(Landroid/graphics/Canvas;II)V
    .locals 7

    .prologue
    .line 676318
    invoke-virtual {p0}, LX/4AU;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 676319
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    iget v2, p0, LX/4AU;->f:I

    mul-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    mul-int/2addr v1, p2

    div-int/lit16 v1, v1, 0x2710

    .line 676320
    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v3, p0, LX/4AU;->f:I

    add-int/2addr v2, v3

    .line 676321
    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget v3, p0, LX/4AU;->f:I

    add-int/2addr v0, v3

    .line 676322
    iget-object v3, p0, LX/4AU;->c:Landroid/graphics/RectF;

    int-to-float v4, v2

    int-to-float v5, v0

    iget v6, p0, LX/4AU;->g:I

    add-int/2addr v2, v6

    int-to-float v2, v2

    add-int/2addr v0, v1

    int-to-float v0, v0

    invoke-virtual {v3, v4, v5, v2, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 676323
    invoke-direct {p0, p1, p3}, LX/4AU;->a(Landroid/graphics/Canvas;I)V

    .line 676324
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    const/16 v1, 0x2710

    .line 676303
    iget-boolean v0, p0, LX/4AU;->j:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/4AU;->h:I

    if-nez v0, :cond_0

    .line 676304
    :goto_0
    return-void

    .line 676305
    :cond_0
    iget-boolean v0, p0, LX/4AU;->k:Z

    if-eqz v0, :cond_1

    .line 676306
    iget v0, p0, LX/4AU;->d:I

    invoke-direct {p0, p1, v1, v0}, LX/4AU;->b(Landroid/graphics/Canvas;II)V

    .line 676307
    iget v0, p0, LX/4AU;->h:I

    iget v1, p0, LX/4AU;->e:I

    invoke-direct {p0, p1, v0, v1}, LX/4AU;->b(Landroid/graphics/Canvas;II)V

    goto :goto_0

    .line 676308
    :cond_1
    iget v0, p0, LX/4AU;->d:I

    invoke-direct {p0, p1, v1, v0}, LX/4AU;->a(Landroid/graphics/Canvas;II)V

    .line 676309
    iget v0, p0, LX/4AU;->h:I

    iget v1, p0, LX/4AU;->e:I

    invoke-direct {p0, p1, v0, v1}, LX/4AU;->a(Landroid/graphics/Canvas;II)V

    goto :goto_0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 676310
    iget-object v0, p0, LX/4AU;->a:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    invoke-static {v0}, LX/1am;->a(I)I

    move-result v0

    return v0
.end method

.method public final getPadding(Landroid/graphics/Rect;)Z
    .locals 4

    .prologue
    .line 676285
    iget v0, p0, LX/4AU;->f:I

    iget v1, p0, LX/4AU;->f:I

    iget v2, p0, LX/4AU;->f:I

    iget v3, p0, LX/4AU;->f:I

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 676286
    iget v0, p0, LX/4AU;->f:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onLevelChange(I)Z
    .locals 1

    .prologue
    .line 676282
    iput p1, p0, LX/4AU;->h:I

    .line 676283
    invoke-virtual {p0}, LX/4AU;->invalidateSelf()V

    .line 676284
    const/4 v0, 0x1

    return v0
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 676287
    iget-object v0, p0, LX/4AU;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 676288
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 676289
    iget-object v0, p0, LX/4AU;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 676290
    return-void
.end method
