.class public final LX/40S;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 663444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 663427
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 663428
    iget-object v1, p0, LX/40S;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 663429
    iget-object v3, p0, LX/40S;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 663430
    iget-object v5, p0, LX/40S;->c:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 663431
    iget-object v6, p0, LX/40S;->d:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v0, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 663432
    const/4 v7, 0x4

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 663433
    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 663434
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 663435
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 663436
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 663437
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 663438
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 663439
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 663440
    invoke-virtual {v1, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 663441
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 663442
    new-instance v1, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;

    invoke-direct {v1, v0}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;-><init>(LX/15i;)V

    .line 663443
    return-object v1
.end method
