.class public final enum LX/46b;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/46b;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/46b;

.field public static final enum CIRCLE:LX/46b;

.field public static final enum DEFAULT:LX/46b;

.field public static final enum ROUNDED:LX/46b;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 671309
    new-instance v0, LX/46b;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, LX/46b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/46b;->DEFAULT:LX/46b;

    new-instance v0, LX/46b;

    const-string v1, "ROUNDED"

    invoke-direct {v0, v1, v3}, LX/46b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/46b;->ROUNDED:LX/46b;

    new-instance v0, LX/46b;

    const-string v1, "CIRCLE"

    invoke-direct {v0, v1, v4}, LX/46b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/46b;->CIRCLE:LX/46b;

    const/4 v0, 0x3

    new-array v0, v0, [LX/46b;

    sget-object v1, LX/46b;->DEFAULT:LX/46b;

    aput-object v1, v0, v2

    sget-object v1, LX/46b;->ROUNDED:LX/46b;

    aput-object v1, v0, v3

    sget-object v1, LX/46b;->CIRCLE:LX/46b;

    aput-object v1, v0, v4

    sput-object v0, LX/46b;->$VALUES:[LX/46b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 671310
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/46b;
    .locals 1

    .prologue
    .line 671308
    const-class v0, LX/46b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/46b;

    return-object v0
.end method

.method public static values()[LX/46b;
    .locals 1

    .prologue
    .line 671307
    sget-object v0, LX/46b;->$VALUES:[LX/46b;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/46b;

    return-object v0
.end method
