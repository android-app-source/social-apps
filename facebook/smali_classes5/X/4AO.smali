.class public LX/4AO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Gd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1Gd",
        "<",
        "LX/1bk;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/1HI;

.field private final c:LX/4AP;

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1Ai;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/Context;LX/1FE;LX/4AM;)V
    .locals 1
    .param p3    # LX/4AM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 676138
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, LX/4AO;-><init>(Landroid/content/Context;LX/1FE;Ljava/util/Set;LX/4AM;)V

    .line 676139
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;LX/1FE;Ljava/util/Set;LX/4AM;)V
    .locals 8
    .param p4    # LX/4AM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/1FE;",
            "Ljava/util/Set",
            "<",
            "LX/1Ai;",
            ">;",
            "LX/4AM;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 676140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 676141
    iput-object p1, p0, LX/4AO;->a:Landroid/content/Context;

    .line 676142
    invoke-virtual {p2}, LX/1FE;->h()LX/1HI;

    move-result-object v0

    iput-object v0, p0, LX/4AO;->b:LX/1HI;

    .line 676143
    invoke-virtual {p2}, LX/1FE;->b()Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;

    move-result-object v0

    .line 676144
    if-eqz v0, :cond_2

    .line 676145
    invoke-virtual {v0, p1}, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->a(Landroid/content/Context;)LX/1c3;

    move-result-object v3

    .line 676146
    :goto_0
    new-instance v0, LX/4AP;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {}, LX/1br;->a()LX/1br;

    move-result-object v2

    invoke-static {}, LX/1by;->b()LX/1by;

    move-result-object v4

    iget-object v5, p0, LX/4AO;->b:LX/1HI;

    .line 676147
    iget-object v6, v5, LX/1HI;->e:LX/1Fh;

    move-object v5, v6

    .line 676148
    if-eqz p4, :cond_1

    .line 676149
    iget-object v6, p4, LX/4AM;->a:LX/1c9;

    move-object v6, v6

    .line 676150
    :goto_1
    if-eqz p4, :cond_0

    .line 676151
    iget-object v7, p4, LX/4AM;->b:LX/1Gd;

    move-object v7, v7

    .line 676152
    :cond_0
    invoke-direct/range {v0 .. v7}, LX/4AP;-><init>(Landroid/content/res/Resources;LX/1br;LX/1c3;Ljava/util/concurrent/Executor;LX/1Fh;LX/1c9;LX/1Gd;)V

    iput-object v0, p0, LX/4AO;->c:LX/4AP;

    .line 676153
    iput-object p3, p0, LX/4AO;->d:Ljava/util/Set;

    .line 676154
    return-void

    :cond_1
    move-object v6, v7

    .line 676155
    goto :goto_1

    :cond_2
    move-object v3, v7

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;LX/4AM;)V
    .locals 1
    .param p2    # LX/4AM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 676156
    invoke-static {}, LX/1FE;->a()LX/1FE;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, LX/4AO;-><init>(Landroid/content/Context;LX/1FE;LX/4AM;)V

    .line 676157
    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 676158
    invoke-virtual {p0}, LX/4AO;->b()LX/1bk;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/1bk;
    .locals 5

    .prologue
    .line 676159
    new-instance v0, LX/1bk;

    iget-object v1, p0, LX/4AO;->a:Landroid/content/Context;

    iget-object v2, p0, LX/4AO;->c:LX/4AP;

    iget-object v3, p0, LX/4AO;->b:LX/1HI;

    iget-object v4, p0, LX/4AO;->d:Ljava/util/Set;

    invoke-direct {v0, v1, v2, v3, v4}, LX/1bk;-><init>(Landroid/content/Context;LX/4AP;LX/1HI;Ljava/util/Set;)V

    return-object v0
.end method
