.class public LX/48V;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field private static final e:LX/2QP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2QP",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final g:LX/2QP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2QP",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final d:LX/03V;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 672914
    const-class v0, LX/48V;

    .line 672915
    sput-object v0, LX/48V;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "_disallowed_domain_load_event"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/48V;->b:Ljava/lang/String;

    .line 672916
    sget-object v0, LX/48V;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "_disallowed_scheme_load_event"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/48V;->c:Ljava/lang/String;

    .line 672917
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "http"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "https"

    aput-object v2, v0, v1

    invoke-static {v0}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v0

    sput-object v0, LX/48V;->e:LX/2QP;

    .line 672918
    new-array v0, v3, [Ljava/lang/Object;

    invoke-static {v0}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v0

    sput-object v0, LX/48V;->g:LX/2QP;

    return-void
.end method

.method public constructor <init>(LX/03V;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 672919
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 672920
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, LX/48V;->e:LX/2QP;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/48V;->f:Ljava/util/List;

    .line 672921
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, LX/48V;->g:LX/2QP;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/48V;->h:Ljava/util/List;

    .line 672922
    iput-object p1, p0, LX/48V;->d:LX/03V;

    .line 672923
    return-void
.end method

.method public static b(LX/0QB;)LX/48V;
    .locals 2

    .prologue
    .line 672924
    new-instance v1, LX/48V;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-direct {v1, v0}, LX/48V;-><init>(LX/03V;)V

    .line 672925
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 672926
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 672927
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 672928
    iget-object v3, p0, LX/48V;->f:Ljava/util/List;

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 672929
    sget-object v2, LX/48V;->a:Ljava/lang/Class;

    const-string v3, "Disallowed scheme: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v0

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 672930
    iget-object v1, p0, LX/48V;->d:LX/03V;

    sget-object v2, LX/48V;->c:Ljava/lang/String;

    const-string v3, "url: "

    invoke-virtual {v3, p2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 672931
    :goto_0
    move v0, v0

    .line 672932
    if-eqz v0, :cond_0

    .line 672933
    invoke-virtual {p1, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 672934
    :cond_0
    return-void

    .line 672935
    :cond_1
    invoke-static {v2}, LX/1H1;->c(Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v3, 0x0

    .line 672936
    if-eqz v2, :cond_2

    .line 672937
    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    .line 672938
    if-eqz v4, :cond_2

    iget-object v5, p0, LX/48V;->h:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x1

    .line 672939
    :cond_2
    move v2, v3

    .line 672940
    if-nez v2, :cond_3

    .line 672941
    sget-object v2, LX/48V;->a:Ljava/lang/Class;

    const-string v3, "Attempt to load a non allowed url: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v0

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 672942
    iget-object v1, p0, LX/48V;->d:LX/03V;

    sget-object v2, LX/48V;->b:Ljava/lang/String;

    const-string v3, "url: "

    invoke-virtual {v3, p2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 672943
    goto :goto_0
.end method

.method public final b(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 672944
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 672945
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "javascript"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 672946
    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    .line 672947
    :goto_0
    move-object v0, v0

    .line 672948
    if-eqz v0, :cond_0

    .line 672949
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_1

    .line 672950
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1, v0, v1}, Landroid/webkit/WebView;->evaluateJavascript(Ljava/lang/String;Landroid/webkit/ValueCallback;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 672951
    :cond_0
    :goto_1
    return-void

    .line 672952
    :catch_0
    move-exception v0

    .line 672953
    iget-object v1, p0, LX/48V;->d:LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/48V;->a:Ljava/lang/Class;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_loadJSURL"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Error loading JS on KK+ device"

    invoke-static {v2, v3}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    .line 672954
    iput-object v0, v2, LX/0VK;->c:Ljava/lang/Throwable;

    .line 672955
    move-object v0, v2

    .line 672956
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    .line 672957
    :cond_1
    invoke-virtual {p1, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
