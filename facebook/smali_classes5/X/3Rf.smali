.class public LX/3Rf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static o:LX/0Xm;


# instance fields
.field public a:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/1zC;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jjj;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/34G;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Og;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Jrr;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Ou;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/DdT;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field private j:I

.field private k:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private l:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private m:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private n:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 581390
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 581391
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 581392
    iput-object v0, p0, LX/3Rf;->c:LX/0Ot;

    .line 581393
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 581394
    iput-object v0, p0, LX/3Rf;->d:LX/0Ot;

    .line 581395
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 581396
    iput-object v0, p0, LX/3Rf;->i:LX/0Ot;

    .line 581397
    const/4 v0, -0x1

    iput v0, p0, LX/3Rf;->j:I

    .line 581398
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/3Rf;->k:LX/0am;

    .line 581399
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/3Rf;->l:LX/0am;

    .line 581400
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/3Rf;->m:LX/0am;

    .line 581401
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/3Rf;->n:LX/0am;

    .line 581402
    return-void
.end method

.method public static a(LX/0QB;)LX/3Rf;
    .locals 12

    .prologue
    .line 581403
    const-class v1, LX/3Rf;

    monitor-enter v1

    .line 581404
    :try_start_0
    sget-object v0, LX/3Rf;->o:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 581405
    sput-object v2, LX/3Rf;->o:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 581406
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 581407
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 581408
    new-instance v3, LX/3Rf;

    invoke-direct {v3}, LX/3Rf;-><init>()V

    .line 581409
    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1zC;->a(LX/0QB;)LX/1zC;

    move-result-object v5

    check-cast v5, LX/1zC;

    const/16 v6, 0x2774

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xd70

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xce8

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x2a07

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0xcee

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0x26ef

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const/16 p0, 0x1032

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 581410
    iput-object v4, v3, LX/3Rf;->a:Landroid/content/Context;

    iput-object v5, v3, LX/3Rf;->b:LX/1zC;

    iput-object v6, v3, LX/3Rf;->c:LX/0Ot;

    iput-object v7, v3, LX/3Rf;->d:LX/0Ot;

    iput-object v8, v3, LX/3Rf;->e:LX/0Or;

    iput-object v9, v3, LX/3Rf;->f:LX/0Or;

    iput-object v10, v3, LX/3Rf;->g:LX/0Or;

    iput-object v11, v3, LX/3Rf;->h:LX/0Or;

    iput-object p0, v3, LX/3Rf;->i:LX/0Ot;

    .line 581411
    move-object v0, v3

    .line 581412
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 581413
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3Rf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 581414
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 581415
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/ParticipantInfo;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 581416
    iget-object v0, p0, LX/3Rf;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Og;

    .line 581417
    invoke-static {v0, p2, p1}, LX/2Og;->c(LX/2Og;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/messages/ParticipantInfo;)Ljava/lang/String;

    move-result-object p0

    .line 581418
    if-eqz p0, :cond_0

    .line 581419
    :goto_0
    move-object v0, p0

    .line 581420
    return-object v0

    :cond_0
    iget-object p0, v0, LX/2Og;->b:LX/2Oh;

    invoke-virtual {p0, p1}, LX/2Oh;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method
