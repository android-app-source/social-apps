.class public LX/3Tx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile k:LX/3Tx;


# instance fields
.field private final a:LX/1Kf;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CIs;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/1vi;

.field private final e:LX/3Tz;

.field public final f:LX/3U3;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1vC;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/CfW;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/facebook/content/SecureContextHelper;

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Kf;LX/0Ot;LX/0Ot;LX/1vi;LX/3Tz;LX/3U3;LX/0Ot;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Kf;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CIs;",
            ">;",
            "LX/1vi;",
            "LX/3Tz;",
            "LX/3U3;",
            "LX/0Ot",
            "<",
            "LX/1vC;",
            ">;",
            "LX/0Or",
            "<",
            "LX/CfW;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 584979
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 584980
    iput-object p1, p0, LX/3Tx;->a:LX/1Kf;

    .line 584981
    iput-object p2, p0, LX/3Tx;->b:LX/0Ot;

    .line 584982
    iput-object p3, p0, LX/3Tx;->c:LX/0Ot;

    .line 584983
    iput-object p4, p0, LX/3Tx;->d:LX/1vi;

    .line 584984
    iput-object p5, p0, LX/3Tx;->e:LX/3Tz;

    .line 584985
    iput-object p6, p0, LX/3Tx;->f:LX/3U3;

    .line 584986
    iput-object p7, p0, LX/3Tx;->g:LX/0Ot;

    .line 584987
    iput-object p8, p0, LX/3Tx;->h:LX/0Or;

    .line 584988
    iput-object p9, p0, LX/3Tx;->i:Lcom/facebook/content/SecureContextHelper;

    .line 584989
    iput-object p10, p0, LX/3Tx;->j:LX/0Ot;

    .line 584990
    return-void
.end method

.method public static a(LX/0QB;)LX/3Tx;
    .locals 14

    .prologue
    .line 584991
    sget-object v0, LX/3Tx;->k:LX/3Tx;

    if-nez v0, :cond_1

    .line 584992
    const-class v1, LX/3Tx;

    monitor-enter v1

    .line 584993
    :try_start_0
    sget-object v0, LX/3Tx;->k:LX/3Tx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 584994
    if-eqz v2, :cond_0

    .line 584995
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 584996
    new-instance v3, LX/3Tx;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v4

    check-cast v4, LX/1Kf;

    const/16 v5, 0x259

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x25ea

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/1vi;->a(LX/0QB;)LX/1vi;

    move-result-object v7

    check-cast v7, LX/1vi;

    invoke-static {v0}, LX/3Tz;->b(LX/0QB;)LX/3Tz;

    move-result-object v8

    check-cast v8, LX/3Tz;

    invoke-static {v0}, LX/3U3;->a(LX/0QB;)LX/3U3;

    move-result-object v9

    check-cast v9, LX/3U3;

    const/16 v10, 0x1067

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x3096

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v12

    check-cast v12, Lcom/facebook/content/SecureContextHelper;

    const/16 v13, 0x12c4

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct/range {v3 .. v13}, LX/3Tx;-><init>(LX/1Kf;LX/0Ot;LX/0Ot;LX/1vi;LX/3Tz;LX/3U3;LX/0Ot;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/0Ot;)V

    .line 584997
    move-object v0, v3

    .line 584998
    sput-object v0, LX/3Tx;->k:LX/3Tx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 584999
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 585000
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 585001
    :cond_1
    sget-object v0, LX/3Tx;->k:LX/3Tx;

    return-object v0

    .line 585002
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 585003
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/Cfl;LX/0o8;Landroid/content/Context;)V
    .locals 11
    .param p3    # LX/0o8;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 584917
    if-nez p3, :cond_0

    .line 584918
    iget-object v0, p0, LX/3Tx;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "null cardContainer"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 584919
    :goto_0
    return-void

    .line 584920
    :cond_0
    iget-object v0, p2, LX/Cfl;->d:Landroid/content/Intent;

    if-nez v0, :cond_2

    .line 584921
    iget-object v0, p2, LX/Cfl;->c:LX/2jR;

    if-eqz v0, :cond_1

    .line 584922
    iget-object v0, p0, LX/3Tx;->d:LX/1vi;

    iget-object v1, p2, LX/Cfl;->c:LX/2jR;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 584923
    :cond_1
    goto :goto_0

    .line 584924
    :cond_2
    iget-object v0, p2, LX/Cfl;->d:Landroid/content/Intent;

    const-string v1, "launch_media_gallery"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 584925
    iget-object v0, p0, LX/3Tx;->e:LX/3Tz;

    const/4 v3, 0x0

    .line 584926
    invoke-static {p1, v3}, LX/9hF;->a(Ljava/lang/String;Ljava/util/List;)LX/9hE;

    move-result-object v1

    sget-object v2, LX/74S;->REACTION_SHOW_MORE_PHOTOS:LX/74S;

    invoke-virtual {v1, v2}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v1

    const/4 v2, 0x1

    .line 584927
    iput-boolean v2, v1, LX/9hD;->m:Z

    .line 584928
    move-object v1, v1

    .line 584929
    iget-object v2, v0, LX/3Tz;->a:LX/23R;

    invoke-virtual {v1}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v1

    invoke-interface {v2, p4, v1, v3}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 584930
    goto :goto_0

    .line 584931
    :cond_3
    iget-object v0, p2, LX/Cfl;->d:Landroid/content/Intent;

    const-string v1, "hide_ads_after_party_aymt_tip"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 584932
    iget-object v0, p2, LX/Cfl;->b:Ljava/lang/String;

    invoke-interface {p3, v0}, LX/0o8;->a(Ljava/lang/String;)Z

    .line 584933
    iget-object v0, p0, LX/3Tx;->f:LX/3U3;

    iget-object v1, p2, LX/Cfl;->b:Ljava/lang/String;

    .line 584934
    new-instance v2, LX/9ts;

    invoke-direct {v2}, LX/9ts;-><init>()V

    move-object p0, v2

    .line 584935
    invoke-interface {p3}, LX/0o8;->o()Ljava/lang/String;

    move-result-object v2

    .line 584936
    new-instance p2, LX/4J7;

    invoke-direct {p2}, LX/4J7;-><init>()V

    .line 584937
    const-string p3, "id"

    invoke-virtual {p2, p3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 584938
    move-object p2, p2

    .line 584939
    if-nez v2, :cond_b

    const/4 v2, 0x0

    .line 584940
    :goto_1
    const-string v1, "reaction_surface"

    invoke-virtual {p2, v1, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 584941
    move-object v2, p2

    .line 584942
    const-string p2, "input"

    invoke-virtual {p0, p2, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 584943
    invoke-static {p0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    .line 584944
    iget-object p0, v0, LX/3U3;->a:LX/0tX;

    invoke-virtual {p0, v2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 584945
    goto :goto_0

    .line 584946
    :cond_4
    iget-object v0, p2, LX/Cfl;->d:Landroid/content/Intent;

    const-string v1, "composer_configuration"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 584947
    iget-object v1, p0, LX/3Tx;->a:LX/1Kf;

    const/4 v2, 0x0

    iget-object v0, p2, LX/Cfl;->d:Landroid/content/Intent;

    const-string v3, "composer_configuration"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iget-object v3, p2, LX/Cfl;->a:LX/Cfc;

    invoke-virtual {v3}, LX/Cfc;->ordinal()I

    move-result v3

    invoke-interface {p3}, LX/0o8;->mZ_()Landroid/support/v4/app/Fragment;

    move-result-object v4

    invoke-interface {v1, v2, v0, v3, v4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/support/v4/app/Fragment;)V

    goto/16 :goto_0

    .line 584948
    :cond_5
    iget-object v0, p2, LX/Cfl;->d:Landroid/content/Intent;

    const-string v1, "launch_external_activity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 584949
    iget-object v0, p0, LX/3Tx;->i:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p2, LX/Cfl;->d:Landroid/content/Intent;

    invoke-interface {v0, v1, p4}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 584950
    :cond_6
    iget-object v0, p2, LX/Cfl;->d:Landroid/content/Intent;

    const-string v1, "launch_activity_for_result"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 584951
    iget-object v0, p2, LX/Cfl;->d:Landroid/content/Intent;

    const-string v1, "reaction_request_code"

    iget-object v2, p2, LX/Cfl;->a:LX/Cfc;

    invoke-virtual {v2}, LX/Cfc;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 584952
    iget-object v1, p0, LX/3Tx;->i:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p2, LX/Cfl;->d:Landroid/content/Intent;

    invoke-interface {p3}, LX/0o8;->mZ_()Landroid/support/v4/app/Fragment;

    move-result-object v3

    invoke-interface {v1, v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    goto/16 :goto_0

    .line 584953
    :cond_7
    iget-object v0, p2, LX/Cfl;->d:Landroid/content/Intent;

    const-string v1, "copy_to_clipboard"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 584954
    iget-object v0, p2, LX/Cfl;->b:Ljava/lang/String;

    invoke-static {p4, v0}, LX/47Y;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 584955
    iget-object v0, p0, LX/3Tx;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082239

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto/16 :goto_0

    .line 584956
    :cond_8
    iget-object v0, p2, LX/Cfl;->a:LX/Cfc;

    sget-object v1, LX/Cfc;->OPEN_LOCAL_SEARCH_TAP:LX/Cfc;

    if-ne v0, v1, :cond_a

    .line 584957
    iget-object v0, p2, LX/Cfl;->d:Landroid/content/Intent;

    const-string v1, "reaction_session_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 584958
    iget-object v1, p2, LX/Cfl;->d:Landroid/content/Intent;

    const-string v2, "reaction_surface"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 584959
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    if-eqz v1, :cond_a

    .line 584960
    iget-object v2, p0, LX/3Tx;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1vC;

    .line 584961
    const v3, 0x1e0002

    invoke-virtual {v2, v3, v0, v1}, LX/1vC;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 584962
    const v3, 0x1e000a

    invoke-virtual {v2, v3, v0, v1}, LX/1vC;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 584963
    iget-object v2, p2, LX/Cfl;->d:Landroid/content/Intent;

    .line 584964
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    if-eqz v5, :cond_9

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "place_id"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 584965
    :cond_9
    :goto_2
    iget-object v2, p2, LX/Cfl;->d:Landroid/content/Intent;

    .line 584966
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    .line 584967
    if-eqz v6, :cond_a

    const-string v5, "place_id"

    invoke-virtual {v6, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_a

    const-string v5, "query_title"

    invoke-virtual {v6, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 584968
    :cond_a
    :goto_3
    iget-object v0, p0, LX/3Tx;->i:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p2, LX/Cfl;->d:Landroid/content/Intent;

    invoke-interface {v0, v1, p4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 584969
    :cond_b
    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 584970
    :cond_c
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "place_id"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    .line 584971
    iget-object v5, p0, LX/3Tx;->h:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/CfW;

    new-instance v6, Lcom/facebook/reaction/ReactionQueryParams;

    invoke-direct {v6}, Lcom/facebook/reaction/ReactionQueryParams;-><init>()V

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 584972
    iput-object v9, v6, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    .line 584973
    move-object v6, v6

    .line 584974
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 584975
    iput-object v7, v6, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    .line 584976
    move-object v6, v6

    .line 584977
    invoke-virtual {v5, v0, v1, v6}, LX/CfW;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;

    goto :goto_2

    .line 584978
    :cond_d
    iget-object v5, p0, LX/3Tx;->c:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/CIs;

    const-string v7, "place_id"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v7, "query_title"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    sget-object v6, LX/8ci;->B:LX/8ci;

    invoke-virtual {v6}, LX/8ci;->toString()Ljava/lang/String;

    move-result-object v10

    move-object v6, v0

    move-object v7, v1

    invoke-virtual/range {v5 .. v10}, LX/CIs;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method
