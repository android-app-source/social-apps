.class public final enum LX/437;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/437;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/437;

.field public static final enum BEST_EFFORT_BOUND_FROM_ABOVE:LX/437;

.field public static final enum BEST_EFFORT_BOUND_FROM_BELOW:LX/437;

.field public static final enum EXACT:LX/437;

.field public static final enum FOR_CENTER_CROP:LX/437;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 668468
    new-instance v0, LX/437;

    const-string v1, "EXACT"

    invoke-direct {v0, v1, v2}, LX/437;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/437;->EXACT:LX/437;

    .line 668469
    new-instance v0, LX/437;

    const-string v1, "BEST_EFFORT_BOUND_FROM_BELOW"

    invoke-direct {v0, v1, v3}, LX/437;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/437;->BEST_EFFORT_BOUND_FROM_BELOW:LX/437;

    .line 668470
    new-instance v0, LX/437;

    const-string v1, "BEST_EFFORT_BOUND_FROM_ABOVE"

    invoke-direct {v0, v1, v4}, LX/437;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/437;->BEST_EFFORT_BOUND_FROM_ABOVE:LX/437;

    .line 668471
    new-instance v0, LX/437;

    const-string v1, "FOR_CENTER_CROP"

    invoke-direct {v0, v1, v5}, LX/437;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/437;->FOR_CENTER_CROP:LX/437;

    .line 668472
    const/4 v0, 0x4

    new-array v0, v0, [LX/437;

    sget-object v1, LX/437;->EXACT:LX/437;

    aput-object v1, v0, v2

    sget-object v1, LX/437;->BEST_EFFORT_BOUND_FROM_BELOW:LX/437;

    aput-object v1, v0, v3

    sget-object v1, LX/437;->BEST_EFFORT_BOUND_FROM_ABOVE:LX/437;

    aput-object v1, v0, v4

    sget-object v1, LX/437;->FOR_CENTER_CROP:LX/437;

    aput-object v1, v0, v5

    sput-object v0, LX/437;->$VALUES:[LX/437;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 668473
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/437;
    .locals 1

    .prologue
    .line 668474
    const-class v0, LX/437;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/437;

    return-object v0
.end method

.method public static values()[LX/437;
    .locals 1

    .prologue
    .line 668475
    sget-object v0, LX/437;->$VALUES:[LX/437;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/437;

    return-object v0
.end method
