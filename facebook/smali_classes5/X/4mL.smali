.class public LX/4mL;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0wT;


# instance fields
.field private final b:LX/0Sh;

.field private final c:LX/4lb;

.field private d:LX/0wT;

.field public e:LX/4mK;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 805640
    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/4mL;->a:LX/0wT;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/4lb;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 805641
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 805642
    sget-object v0, LX/4mL;->a:LX/0wT;

    iput-object v0, p0, LX/4mL;->d:LX/0wT;

    .line 805643
    sget-object v0, LX/4mK;->INACTIVE:LX/4mK;

    iput-object v0, p0, LX/4mL;->e:LX/4mK;

    .line 805644
    iput-object p1, p0, LX/4mL;->b:LX/0Sh;

    .line 805645
    iput-object p2, p0, LX/4mL;->c:LX/4lb;

    .line 805646
    return-void
.end method
