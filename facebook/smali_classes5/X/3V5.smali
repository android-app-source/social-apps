.class public LX/3V5;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements LX/3V1;


# static fields
.field public static final j:LX/1Cz;


# instance fields
.field public k:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final l:I

.field private final m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final n:Lcom/facebook/fbui/widget/text/TextLayoutView;

.field private final o:Lcom/facebook/fbui/widget/text/TextLayoutView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587540
    new-instance v0, LX/3V6;

    invoke-direct {v0}, LX/3V6;-><init>()V

    sput-object v0, LX/3V5;->j:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 587543
    const/4 v0, 0x0

    const v1, 0x7f030880

    invoke-direct {p0, p1, v0, v1}, LX/3V5;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 587544
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587541
    const v0, 0x7f030880

    invoke-direct {p0, p1, p2, v0}, LX/3V5;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 587542
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587509
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 587510
    const-class v0, LX/3V5;

    invoke-static {v0, p0}, LX/3V5;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 587511
    invoke-virtual {p0, p3}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 587512
    const v0, 0x7f0d160d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/3V5;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 587513
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 587514
    iget-object v0, p0, LX/3V5;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImportantForAccessibility(I)V

    .line 587515
    :cond_0
    const v0, 0x7f0d1616

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/TextLayoutView;

    iput-object v0, p0, LX/3V5;->n:Lcom/facebook/fbui/widget/text/TextLayoutView;

    .line 587516
    const v0, 0x7f0d1615

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/TextLayoutView;

    iput-object v0, p0, LX/3V5;->o:Lcom/facebook/fbui/widget/text/TextLayoutView;

    .line 587517
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a044e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v0, v0

    .line 587518
    iput v0, p0, LX/3V5;->l:I

    .line 587519
    iget-object v0, p0, LX/3V5;->n:Lcom/facebook/fbui/widget/text/TextLayoutView;

    iget v1, p0, LX/3V5;->l:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/TextLayoutView;->setHighlightColor(I)V

    .line 587520
    sget-object v0, LX/1vY;->HEADER:LX/1vY;

    invoke-static {p0, v0}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 587521
    iget-object v0, p0, LX/3V5;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, LX/1vY;->ACTOR_PHOTO:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 587522
    iget-object v0, p0, LX/3V5;->n:Lcom/facebook/fbui/widget/text/TextLayoutView;

    sget-object v1, LX/1vY;->TITLE:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 587523
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/3V5;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object p0

    check-cast p0, LX/03V;

    iput-object p0, p1, LX/3V5;->k:LX/03V;

    return-void
.end method

.method private a(Ljava/lang/StringIndexOutOfBoundsException;)V
    .locals 4

    .prologue
    .line 587545
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 587546
    new-instance v1, Landroid/util/StringBuilderPrinter;

    invoke-direct {v1, v0}, Landroid/util/StringBuilderPrinter;-><init>(Ljava/lang/StringBuilder;)V

    .line 587547
    iget-object v2, p0, LX/3V5;->n:Lcom/facebook/fbui/widget/text/TextLayoutView;

    invoke-virtual {v2}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    const-string v3, "title: "

    invoke-static {v2, v1, v3}, Landroid/text/TextUtils;->dumpSpans(Ljava/lang/CharSequence;Landroid/util/Printer;Ljava/lang/String;)V

    .line 587548
    iget-object v2, p0, LX/3V5;->o:Lcom/facebook/fbui/widget/text/TextLayoutView;

    invoke-virtual {v2}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    const-string v3, "subtitle: "

    invoke-static {v2, v1, v3}, Landroid/text/TextUtils;->dumpSpans(Ljava/lang/CharSequence;Landroid/util/Printer;Ljava/lang/String;)V

    .line 587549
    const-string v1, "HeaderViewWithTextLayout.dispatchDraw caused StringIndexOutOfBoundsException"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    .line 587550
    iput-object p1, v0, LX/0VK;->c:Ljava/lang/Throwable;

    .line 587551
    move-object v0, v0

    .line 587552
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    .line 587553
    iget-object v1, p0, LX/3V5;->k:LX/03V;

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    .line 587554
    return-void
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 587532
    :try_start_0
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->dispatchDraw(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch LX/2Fh; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    .line 587533
    :goto_0
    return-void

    .line 587534
    :catch_0
    move-exception v0

    .line 587535
    invoke-virtual {v0}, LX/2Fh;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/StringIndexOutOfBoundsException;

    if-eqz v1, :cond_0

    .line 587536
    invoke-virtual {v0}, LX/2Fh;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {p0, v0}, LX/3V5;->a(Ljava/lang/StringIndexOutOfBoundsException;)V

    goto :goto_0

    .line 587537
    :cond_0
    throw v0

    .line 587538
    :catch_1
    move-exception v0

    .line 587539
    invoke-direct {p0, v0}, LX/3V5;->a(Ljava/lang/StringIndexOutOfBoundsException;)V

    goto :goto_0
.end method

.method public getProfileImageView()Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 587531
    iget-object v0, p0, LX/3V5;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    return-object v0
.end method

.method public setProfileImageOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587529
    iget-object v0, p0, LX/3V5;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 587530
    return-void
.end method

.method public setSubtitleIcon(I)V
    .locals 2

    .prologue
    .line 587528
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not a simple text header, use text layout t6009510"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setSubtitleWithLayout(Landroid/text/Layout;)V
    .locals 2

    .prologue
    .line 587524
    iget-object v1, p0, LX/3V5;->o:Lcom/facebook/fbui/widget/text/TextLayoutView;

    if-nez p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->setVisibility(I)V

    .line 587525
    iget-object v0, p0, LX/3V5;->o:Lcom/facebook/fbui/widget/text/TextLayoutView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/TextLayoutView;->setTextLayout(Landroid/text/Layout;)V

    .line 587526
    return-void

    .line 587527
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
