.class public LX/4AT;
.super LX/1ah;
.source ""


# instance fields
.field public final a:Landroid/graphics/Matrix;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field private c:I

.field private final d:Landroid/graphics/Matrix;

.field private final e:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;I)V
    .locals 1

    .prologue
    .line 676251
    invoke-direct {p0, p1}, LX/1ah;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 676252
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/4AT;->d:Landroid/graphics/Matrix;

    .line 676253
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/4AT;->e:Landroid/graphics/RectF;

    .line 676254
    rem-int/lit8 v0, p2, 0x5a

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->a(Z)V

    .line 676255
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/4AT;->a:Landroid/graphics/Matrix;

    .line 676256
    iput p2, p0, LX/4AT;->c:I

    .line 676257
    return-void

    .line 676258
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/graphics/Matrix;)V
    .locals 1

    .prologue
    .line 676259
    invoke-virtual {p0, p1}, LX/1ah;->b(Landroid/graphics/Matrix;)V

    .line 676260
    iget-object v0, p0, LX/4AT;->a:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->isIdentity()Z

    move-result v0

    if-nez v0, :cond_0

    .line 676261
    iget-object v0, p0, LX/4AT;->a:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    .line 676262
    :cond_0
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 676263
    iget v0, p0, LX/4AT;->c:I

    if-gtz v0, :cond_0

    .line 676264
    invoke-super {p0, p1}, LX/1ah;->draw(Landroid/graphics/Canvas;)V

    .line 676265
    :goto_0
    return-void

    .line 676266
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 676267
    iget-object v1, p0, LX/4AT;->a:Landroid/graphics/Matrix;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 676268
    invoke-super {p0, p1}, LX/1ah;->draw(Landroid/graphics/Canvas;)V

    .line 676269
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 676270
    iget v0, p0, LX/4AT;->c:I

    rem-int/lit16 v0, v0, 0xb4

    if-nez v0, :cond_0

    invoke-super {p0}, LX/1ah;->getIntrinsicHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, LX/1ah;->getIntrinsicWidth()I

    move-result v0

    goto :goto_0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 676271
    iget v0, p0, LX/4AT;->c:I

    rem-int/lit16 v0, v0, 0xb4

    if-nez v0, :cond_0

    invoke-super {p0}, LX/1ah;->getIntrinsicWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, LX/1ah;->getIntrinsicHeight()I

    move-result v0

    goto :goto_0
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 5

    .prologue
    .line 676272
    invoke-virtual {p0}, LX/4AT;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 676273
    iget v1, p0, LX/4AT;->c:I

    if-lez v1, :cond_0

    .line 676274
    iget-object v1, p0, LX/4AT;->a:Landroid/graphics/Matrix;

    iget v2, p0, LX/4AT;->c:I

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 676275
    iget-object v1, p0, LX/4AT;->d:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 676276
    iget-object v1, p0, LX/4AT;->a:Landroid/graphics/Matrix;

    iget-object v2, p0, LX/4AT;->d:Landroid/graphics/Matrix;

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 676277
    iget-object v1, p0, LX/4AT;->e:Landroid/graphics/RectF;

    invoke-virtual {v1, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 676278
    iget-object v1, p0, LX/4AT;->d:Landroid/graphics/Matrix;

    iget-object v2, p0, LX/4AT;->e:Landroid/graphics/RectF;

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 676279
    iget-object v1, p0, LX/4AT;->e:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iget-object v2, p0, LX/4AT;->e:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    iget-object v3, p0, LX/4AT;->e:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    float-to-int v3, v3

    iget-object v4, p0, LX/4AT;->e:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    float-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 676280
    :goto_0
    return-void

    .line 676281
    :cond_0
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    goto :goto_0
.end method
