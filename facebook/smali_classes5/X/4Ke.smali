.class public LX/4Ke;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 680365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 33

    .prologue
    .line 680457
    const/16 v29, 0x0

    .line 680458
    const/16 v28, 0x0

    .line 680459
    const/16 v27, 0x0

    .line 680460
    const/16 v26, 0x0

    .line 680461
    const/16 v25, 0x0

    .line 680462
    const/16 v24, 0x0

    .line 680463
    const/16 v23, 0x0

    .line 680464
    const/16 v22, 0x0

    .line 680465
    const/16 v21, 0x0

    .line 680466
    const/16 v20, 0x0

    .line 680467
    const/16 v19, 0x0

    .line 680468
    const/16 v18, 0x0

    .line 680469
    const/16 v17, 0x0

    .line 680470
    const/16 v16, 0x0

    .line 680471
    const/4 v15, 0x0

    .line 680472
    const/4 v14, 0x0

    .line 680473
    const/4 v13, 0x0

    .line 680474
    const/4 v12, 0x0

    .line 680475
    const/4 v11, 0x0

    .line 680476
    const/4 v10, 0x0

    .line 680477
    const/4 v9, 0x0

    .line 680478
    const/4 v8, 0x0

    .line 680479
    const/4 v7, 0x0

    .line 680480
    const/4 v6, 0x0

    .line 680481
    const/4 v5, 0x0

    .line 680482
    const/4 v4, 0x0

    .line 680483
    const/4 v3, 0x0

    .line 680484
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v30

    sget-object v31, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    if-eq v0, v1, :cond_1

    .line 680485
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 680486
    const/4 v3, 0x0

    .line 680487
    :goto_0
    return v3

    .line 680488
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 680489
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v30

    sget-object v31, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    if-eq v0, v1, :cond_17

    .line 680490
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v30

    .line 680491
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 680492
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v31

    sget-object v32, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    if-eq v0, v1, :cond_1

    if-eqz v30, :cond_1

    .line 680493
    const-string v31, "account_info"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_2

    .line 680494
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    goto :goto_1

    .line 680495
    :cond_2
    const-string v31, "account_status"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_3

    .line 680496
    const/4 v7, 0x1

    .line 680497
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v28 .. v28}, Lcom/facebook/graphql/enums/GraphQLAdAccountStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAdAccountStatus;

    move-result-object v28

    goto :goto_1

    .line 680498
    :cond_3
    const-string v31, "ads_currency"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_4

    .line 680499
    invoke-static/range {p0 .. p1}, LX/4Lf;->a(LX/15w;LX/186;)I

    move-result v27

    goto :goto_1

    .line 680500
    :cond_4
    const-string v31, "balance"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_5

    .line 680501
    invoke-static/range {p0 .. p1}, LX/4Lf;->a(LX/15w;LX/186;)I

    move-result v26

    goto :goto_1

    .line 680502
    :cond_5
    const-string v31, "can_update_currency"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_6

    .line 680503
    const/4 v6, 0x1

    .line 680504
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v25

    goto :goto_1

    .line 680505
    :cond_6
    const-string v31, "creative_preview_url"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_7

    .line 680506
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    goto/16 :goto_1

    .line 680507
    :cond_7
    const-string v31, "currency"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_8

    .line 680508
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    goto/16 :goto_1

    .line 680509
    :cond_8
    const-string v31, "feed_unit_preview"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_9

    .line 680510
    invoke-static/range {p0 .. p1}, LX/2ao;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 680511
    :cond_9
    const-string v31, "has_funding_source"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_a

    .line 680512
    const/4 v5, 0x1

    .line 680513
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v21

    goto/16 :goto_1

    .line 680514
    :cond_a
    const-string v31, "id"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_b

    .line 680515
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    goto/16 :goto_1

    .line 680516
    :cond_b
    const-string v31, "legacy_account_id"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_c

    .line 680517
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    goto/16 :goto_1

    .line 680518
    :cond_c
    const-string v31, "max_daily_budget"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_d

    .line 680519
    invoke-static/range {p0 .. p1}, LX/4Lf;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 680520
    :cond_d
    const-string v31, "min_daily_budget"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_e

    .line 680521
    invoke-static/range {p0 .. p1}, LX/4Lf;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 680522
    :cond_e
    const-string v31, "name"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_f

    .line 680523
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto/16 :goto_1

    .line 680524
    :cond_f
    const-string v31, "payment_info"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_10

    .line 680525
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 680526
    :cond_10
    const-string v31, "prepay_account_balance"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_11

    .line 680527
    invoke-static/range {p0 .. p1}, LX/4Lf;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 680528
    :cond_11
    const-string v31, "stored_balance_status"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_12

    .line 680529
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto/16 :goto_1

    .line 680530
    :cond_12
    const-string v31, "timezone_info"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_13

    .line 680531
    invoke-static/range {p0 .. p1}, LX/4Tx;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 680532
    :cond_13
    const-string v31, "url"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_14

    .line 680533
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 680534
    :cond_14
    const-string v31, "checkout_available_balance"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_15

    .line 680535
    invoke-static/range {p0 .. p1}, LX/4Lf;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 680536
    :cond_15
    const-string v31, "show_checkout_experience"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_16

    .line 680537
    const/4 v4, 0x1

    .line 680538
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto/16 :goto_1

    .line 680539
    :cond_16
    const-string v31, "is_locked_into_checkout"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_0

    .line 680540
    const/4 v3, 0x1

    .line 680541
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto/16 :goto_1

    .line 680542
    :cond_17
    const/16 v30, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 680543
    const/16 v30, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v30

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 680544
    if-eqz v7, :cond_18

    .line 680545
    const/4 v7, 0x2

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v7, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 680546
    :cond_18
    const/4 v7, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 680547
    const/4 v7, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 680548
    if-eqz v6, :cond_19

    .line 680549
    const/4 v6, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 680550
    :cond_19
    const/4 v6, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 680551
    const/4 v6, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 680552
    const/16 v6, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 680553
    if-eqz v5, :cond_1a

    .line 680554
    const/16 v5, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 680555
    :cond_1a
    const/16 v5, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 680556
    const/16 v5, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 680557
    const/16 v5, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 680558
    const/16 v5, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 680559
    const/16 v5, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 680560
    const/16 v5, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v15}, LX/186;->b(II)V

    .line 680561
    const/16 v5, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v14}, LX/186;->b(II)V

    .line 680562
    const/16 v5, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v13}, LX/186;->b(II)V

    .line 680563
    const/16 v5, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v12}, LX/186;->b(II)V

    .line 680564
    const/16 v5, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v11}, LX/186;->b(II)V

    .line 680565
    const/16 v5, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v10}, LX/186;->b(II)V

    .line 680566
    if-eqz v4, :cond_1b

    .line 680567
    const/16 v4, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->a(IZ)V

    .line 680568
    :cond_1b
    if-eqz v3, :cond_1c

    .line 680569
    const/16 v3, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->a(IZ)V

    .line 680570
    :cond_1c
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 680366
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 680367
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 680368
    if-eqz v0, :cond_0

    .line 680369
    const-string v1, "account_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680370
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 680371
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->a(IIS)S

    move-result v0

    .line 680372
    if-eqz v0, :cond_1

    .line 680373
    const-string v0, "account_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680374
    const-class v0, Lcom/facebook/graphql/enums/GraphQLAdAccountStatus;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAdAccountStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLAdAccountStatus;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 680375
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 680376
    if-eqz v0, :cond_2

    .line 680377
    const-string v1, "ads_currency"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680378
    invoke-static {p0, v0, p2}, LX/4Lf;->a(LX/15i;ILX/0nX;)V

    .line 680379
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 680380
    if-eqz v0, :cond_3

    .line 680381
    const-string v1, "balance"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680382
    invoke-static {p0, v0, p2}, LX/4Lf;->a(LX/15i;ILX/0nX;)V

    .line 680383
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 680384
    if-eqz v0, :cond_4

    .line 680385
    const-string v1, "can_update_currency"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680386
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 680387
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 680388
    if-eqz v0, :cond_5

    .line 680389
    const-string v1, "creative_preview_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680390
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 680391
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 680392
    if-eqz v0, :cond_6

    .line 680393
    const-string v1, "currency"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680394
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 680395
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 680396
    if-eqz v0, :cond_7

    .line 680397
    const-string v1, "feed_unit_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680398
    invoke-static {p0, v0, p2, p3}, LX/2ao;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 680399
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 680400
    if-eqz v0, :cond_8

    .line 680401
    const-string v1, "has_funding_source"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680402
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 680403
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 680404
    if-eqz v0, :cond_9

    .line 680405
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680406
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 680407
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 680408
    if-eqz v0, :cond_a

    .line 680409
    const-string v1, "legacy_account_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680410
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 680411
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 680412
    if-eqz v0, :cond_b

    .line 680413
    const-string v1, "max_daily_budget"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680414
    invoke-static {p0, v0, p2}, LX/4Lf;->a(LX/15i;ILX/0nX;)V

    .line 680415
    :cond_b
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 680416
    if-eqz v0, :cond_c

    .line 680417
    const-string v1, "min_daily_budget"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680418
    invoke-static {p0, v0, p2}, LX/4Lf;->a(LX/15i;ILX/0nX;)V

    .line 680419
    :cond_c
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 680420
    if-eqz v0, :cond_d

    .line 680421
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680422
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 680423
    :cond_d
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 680424
    if-eqz v0, :cond_e

    .line 680425
    const-string v1, "payment_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680426
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 680427
    :cond_e
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 680428
    if-eqz v0, :cond_f

    .line 680429
    const-string v1, "prepay_account_balance"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680430
    invoke-static {p0, v0, p2}, LX/4Lf;->a(LX/15i;ILX/0nX;)V

    .line 680431
    :cond_f
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 680432
    if-eqz v0, :cond_10

    .line 680433
    const-string v1, "stored_balance_status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680434
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 680435
    :cond_10
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 680436
    if-eqz v0, :cond_11

    .line 680437
    const-string v1, "timezone_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680438
    invoke-static {p0, v0, p2}, LX/4Tx;->a(LX/15i;ILX/0nX;)V

    .line 680439
    :cond_11
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 680440
    if-eqz v0, :cond_12

    .line 680441
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680442
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 680443
    :cond_12
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 680444
    if-eqz v0, :cond_13

    .line 680445
    const-string v1, "checkout_available_balance"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680446
    invoke-static {p0, v0, p2}, LX/4Lf;->a(LX/15i;ILX/0nX;)V

    .line 680447
    :cond_13
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 680448
    if-eqz v0, :cond_14

    .line 680449
    const-string v1, "show_checkout_experience"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680450
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 680451
    :cond_14
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 680452
    if-eqz v0, :cond_15

    .line 680453
    const-string v1, "is_locked_into_checkout"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 680454
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 680455
    :cond_15
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 680456
    return-void
.end method
