.class public final LX/507;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/508;

.field private b:I

.field private c:I

.field private d:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<TE;>;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TE;>;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private g:Z


# direct methods
.method public constructor <init>(LX/508;)V
    .locals 1

    .prologue
    .line 823374
    iput-object p1, p0, LX/507;->a:LX/508;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 823375
    const/4 v0, -0x1

    iput v0, p0, LX/507;->b:I

    .line 823376
    iget-object v0, p0, LX/507;->a:LX/508;

    iget v0, v0, LX/508;->f:I

    iput v0, p0, LX/507;->c:I

    return-void
.end method

.method private a(I)I
    .locals 4

    .prologue
    .line 823365
    iget-object v0, p0, LX/507;->e:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 823366
    :goto_0
    iget-object v0, p0, LX/507;->a:LX/508;

    invoke-virtual {v0}, LX/508;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    iget-object v0, p0, LX/507;->e:Ljava/util/List;

    iget-object v1, p0, LX/507;->a:LX/508;

    invoke-virtual {v1, p1}, LX/508;->a(I)Ljava/lang/Object;

    move-result-object v1

    .line 823367
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 823368
    if-ne v3, v1, :cond_0

    .line 823369
    const/4 v2, 0x1

    .line 823370
    :goto_1
    move v0, v2

    .line 823371
    if-eqz v0, :cond_1

    .line 823372
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 823373
    :cond_1
    return p1

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private a()V
    .locals 2

    .prologue
    .line 823362
    iget-object v0, p0, LX/507;->a:LX/508;

    iget v0, v0, LX/508;->f:I

    iget v1, p0, LX/507;->c:I

    if-eq v0, v1, :cond_0

    .line 823363
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 823364
    :cond_0
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 2

    .prologue
    .line 823360
    invoke-direct {p0}, LX/507;->a()V

    .line 823361
    iget v0, p0, LX/507;->b:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, LX/507;->a(I)I

    move-result v0

    iget-object v1, p0, LX/507;->a:LX/508;

    invoke-virtual {v1}, LX/508;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget-object v0, p0, LX/507;->d:Ljava/util/Queue;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/507;->d:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 823346
    invoke-direct {p0}, LX/507;->a()V

    .line 823347
    iget v0, p0, LX/507;->b:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, LX/507;->a(I)I

    move-result v0

    .line 823348
    iget-object v1, p0, LX/507;->a:LX/508;

    invoke-virtual {v1}, LX/508;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 823349
    iput v0, p0, LX/507;->b:I

    .line 823350
    iput-boolean v2, p0, LX/507;->g:Z

    .line 823351
    iget-object v0, p0, LX/507;->a:LX/508;

    iget v1, p0, LX/507;->b:I

    invoke-virtual {v0, v1}, LX/508;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 823352
    :goto_0
    return-object v0

    .line 823353
    :cond_0
    iget-object v0, p0, LX/507;->d:Ljava/util/Queue;

    if-eqz v0, :cond_1

    .line 823354
    iget-object v0, p0, LX/507;->a:LX/508;

    invoke-virtual {v0}, LX/508;->size()I

    move-result v0

    iput v0, p0, LX/507;->b:I

    .line 823355
    iget-object v0, p0, LX/507;->d:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/507;->f:Ljava/lang/Object;

    .line 823356
    iget-object v0, p0, LX/507;->f:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 823357
    iput-boolean v2, p0, LX/507;->g:Z

    .line 823358
    iget-object v0, p0, LX/507;->f:Ljava/lang/Object;

    goto :goto_0

    .line 823359
    :cond_1
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "iterator moved past last element in queue."

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final remove()V
    .locals 4

    .prologue
    .line 823323
    iget-boolean v0, p0, LX/507;->g:Z

    invoke-static {v0}, LX/0P6;->a(Z)V

    .line 823324
    invoke-direct {p0}, LX/507;->a()V

    .line 823325
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/507;->g:Z

    .line 823326
    iget v0, p0, LX/507;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/507;->c:I

    .line 823327
    iget v0, p0, LX/507;->b:I

    iget-object v1, p0, LX/507;->a:LX/508;

    invoke-virtual {v1}, LX/508;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 823328
    iget-object v0, p0, LX/507;->a:LX/508;

    iget v1, p0, LX/507;->b:I

    invoke-virtual {v0, v1}, LX/508;->b(I)LX/506;

    move-result-object v0

    .line 823329
    if-eqz v0, :cond_1

    .line 823330
    iget-object v1, p0, LX/507;->d:Ljava/util/Queue;

    if-nez v1, :cond_0

    .line 823331
    new-instance v1, Ljava/util/ArrayDeque;

    invoke-direct {v1}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v1, p0, LX/507;->d:Ljava/util/Queue;

    .line 823332
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, LX/507;->e:Ljava/util/List;

    .line 823333
    :cond_0
    iget-object v1, p0, LX/507;->d:Ljava/util/Queue;

    iget-object v2, v0, LX/506;->a:Ljava/lang/Object;

    invoke-interface {v1, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 823334
    iget-object v1, p0, LX/507;->e:Ljava/util/List;

    iget-object v0, v0, LX/506;->b:Ljava/lang/Object;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 823335
    :cond_1
    iget v0, p0, LX/507;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/507;->b:I

    .line 823336
    :goto_0
    return-void

    .line 823337
    :cond_2
    iget-object v0, p0, LX/507;->f:Ljava/lang/Object;

    const/4 v2, 0x0

    .line 823338
    move v1, v2

    :goto_1
    iget-object v3, p0, LX/507;->a:LX/508;

    iget v3, v3, LX/508;->e:I

    if-ge v1, v3, :cond_3

    .line 823339
    iget-object v3, p0, LX/507;->a:LX/508;

    iget-object v3, v3, LX/508;->d:[Ljava/lang/Object;

    aget-object v3, v3, v1

    if-ne v3, v0, :cond_4

    .line 823340
    iget-object v2, p0, LX/507;->a:LX/508;

    invoke-virtual {v2, v1}, LX/508;->b(I)LX/506;

    .line 823341
    const/4 v2, 0x1

    .line 823342
    :cond_3
    move v0, v2

    .line 823343
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 823344
    const/4 v0, 0x0

    iput-object v0, p0, LX/507;->f:Ljava/lang/Object;

    goto :goto_0

    .line 823345
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
