.class public final LX/4tw;
.super LX/2EA;
.source ""


# instance fields
.field public i:J

.field public j:J


# direct methods
.method public constructor <init>()V
    .locals 2

    const-wide/16 v0, -0x1

    invoke-direct {p0}, LX/2EA;-><init>()V

    iput-wide v0, p0, LX/4tw;->i:J

    iput-wide v0, p0, LX/4tw;->j:J

    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4tw;->e:Z

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    const-wide/16 v4, -0x1

    invoke-super {p0}, LX/2EA;->a()V

    iget-wide v0, p0, LX/4tw;->i:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must call setPeriod(long) to establish an execution interval for this periodic task."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-wide v0, p0, LX/4tw;->i:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    iget-wide v2, p0, LX/4tw;->i:J

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x42

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Period set cannot be less than or equal to 0: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-wide v0, p0, LX/4tw;->j:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_3

    iget-wide v0, p0, LX/4tw;->i:J

    long-to-float v0, v0

    const v1, 0x3dcccccd    # 0.1f

    mul-float/2addr v0, v1

    float-to-long v0, v0

    iput-wide v0, p0, LX/4tw;->j:J

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-wide v0, p0, LX/4tw;->j:J

    iget-wide v2, p0, LX/4tw;->i:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    iget-wide v0, p0, LX/4tw;->i:J

    iput-wide v0, p0, LX/4tw;->j:J

    goto :goto_0
.end method

.method public final b(I)LX/2EA;
    .locals 1

    iput p1, p0, LX/4tw;->a:I

    return-object p0
.end method

.method public final b(Landroid/os/Bundle;)LX/2EA;
    .locals 1

    iput-object p1, p0, LX/4tw;->h:Landroid/os/Bundle;

    return-object p0
.end method

.method public final b(Ljava/lang/Class;)LX/2EA;
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/4tw;->b:Ljava/lang/String;

    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/2EA;
    .locals 1

    iput-object p1, p0, LX/4tw;->c:Ljava/lang/String;

    return-object p0
.end method

.method public final c()Lcom/google/android/gms/gcm/Task;
    .locals 2

    invoke-virtual {p0}, LX/4tw;->a()V

    new-instance v0, Lcom/google/android/gms/gcm/PeriodicTask;

    invoke-direct {v0, p0}, Lcom/google/android/gms/gcm/PeriodicTask;-><init>(LX/4tw;)V

    return-object v0
.end method

.method public final d(Z)LX/2EA;
    .locals 1

    iput-boolean p1, p0, LX/4tw;->d:Z

    return-object p0
.end method

.method public final e(Z)LX/2EA;
    .locals 1

    iput-boolean p1, p0, LX/4tw;->f:Z

    return-object p0
.end method
