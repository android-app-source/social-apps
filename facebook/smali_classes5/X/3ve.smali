.class public final LX/3ve;
.super Landroid/database/DataSetObserver;
.source ""


# instance fields
.field public final synthetic a:LX/3vM;

.field private b:Landroid/os/Parcelable;


# direct methods
.method public constructor <init>(LX/3vM;)V
    .locals 1

    .prologue
    .line 653397
    iput-object p1, p0, LX/3ve;->a:LX/3vM;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    .line 653398
    const/4 v0, 0x0

    iput-object v0, p0, LX/3ve;->b:Landroid/os/Parcelable;

    return-void
.end method


# virtual methods
.method public final onChanged()V
    .locals 8

    .prologue
    .line 653354
    iget-object v0, p0, LX/3ve;->a:LX/3vM;

    const/4 v1, 0x1

    iput-boolean v1, v0, LX/3vM;->u:Z

    .line 653355
    iget-object v0, p0, LX/3ve;->a:LX/3vM;

    iget-object v1, p0, LX/3ve;->a:LX/3vM;

    iget v1, v1, LX/3vM;->z:I

    iput v1, v0, LX/3vM;->A:I

    .line 653356
    iget-object v0, p0, LX/3ve;->a:LX/3vM;

    iget-object v1, p0, LX/3ve;->a:LX/3vM;

    invoke-virtual {v1}, LX/3vM;->getAdapter()Landroid/widget/Adapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/Adapter;->getCount()I

    move-result v1

    iput v1, v0, LX/3vM;->z:I

    .line 653357
    iget-object v0, p0, LX/3ve;->a:LX/3vM;

    invoke-virtual {v0}, LX/3vM;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/Adapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3ve;->b:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3ve;->a:LX/3vM;

    iget v0, v0, LX/3vM;->A:I

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3ve;->a:LX/3vM;

    iget v0, v0, LX/3vM;->z:I

    if-lez v0, :cond_0

    .line 653358
    iget-object v0, p0, LX/3ve;->a:LX/3vM;

    iget-object v1, p0, LX/3ve;->b:Landroid/os/Parcelable;

    invoke-static {v0, v1}, LX/3vM;->a(LX/3vM;Landroid/os/Parcelable;)V

    .line 653359
    const/4 v0, 0x0

    iput-object v0, p0, LX/3ve;->b:Landroid/os/Parcelable;

    .line 653360
    :goto_0
    iget-object v0, p0, LX/3ve;->a:LX/3vM;

    invoke-virtual {v0}, LX/3vM;->e()V

    .line 653361
    iget-object v0, p0, LX/3ve;->a:LX/3vM;

    invoke-virtual {v0}, LX/3vM;->requestLayout()V

    .line 653362
    return-void

    .line 653363
    :cond_0
    iget-object v0, p0, LX/3ve;->a:LX/3vM;

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 653364
    invoke-virtual {v0}, LX/3vM;->getChildCount()I

    move-result v2

    if-lez v2, :cond_2

    .line 653365
    iput-boolean v7, v0, LX/3vM;->o:Z

    .line 653366
    iget v2, v0, LX/3vM;->a:I

    int-to-long v2, v2

    iput-wide v2, v0, LX/3vM;->n:J

    .line 653367
    iget v2, v0, LX/3vM;->x:I

    if-ltz v2, :cond_3

    .line 653368
    iget v2, v0, LX/3vM;->x:I

    iget v3, v0, LX/3vM;->j:I

    sub-int/2addr v2, v3

    invoke-virtual {v0, v2}, LX/3vM;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 653369
    iget-wide v4, v0, LX/3vM;->w:J

    iput-wide v4, v0, LX/3vM;->m:J

    .line 653370
    iget v3, v0, LX/3vM;->v:I

    iput v3, v0, LX/3vM;->l:I

    .line 653371
    if-eqz v2, :cond_1

    .line 653372
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    iput v2, v0, LX/3vM;->k:I

    .line 653373
    :cond_1
    iput v6, v0, LX/3vM;->p:I

    .line 653374
    :cond_2
    :goto_1
    goto :goto_0

    .line 653375
    :cond_3
    invoke-virtual {v0, v6}, LX/3vM;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 653376
    invoke-virtual {v0}, LX/3vM;->getAdapter()Landroid/widget/Adapter;

    move-result-object v3

    .line 653377
    iget v4, v0, LX/3vM;->j:I

    if-ltz v4, :cond_5

    iget v4, v0, LX/3vM;->j:I

    invoke-interface {v3}, Landroid/widget/Adapter;->getCount()I

    move-result v5

    if-ge v4, v5, :cond_5

    .line 653378
    iget v4, v0, LX/3vM;->j:I

    invoke-interface {v3, v4}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v4

    iput-wide v4, v0, LX/3vM;->m:J

    .line 653379
    :goto_2
    iget v3, v0, LX/3vM;->j:I

    iput v3, v0, LX/3vM;->l:I

    .line 653380
    if-eqz v2, :cond_4

    .line 653381
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    iput v2, v0, LX/3vM;->k:I

    .line 653382
    :cond_4
    iput v7, v0, LX/3vM;->p:I

    goto :goto_1

    .line 653383
    :cond_5
    const-wide/16 v4, -0x1

    iput-wide v4, v0, LX/3vM;->m:J

    goto :goto_2
.end method

.method public final onInvalidated()V
    .locals 6

    .prologue
    const-wide/high16 v4, -0x8000000000000000L

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 653384
    iget-object v0, p0, LX/3ve;->a:LX/3vM;

    const/4 v1, 0x1

    iput-boolean v1, v0, LX/3vM;->u:Z

    .line 653385
    iget-object v0, p0, LX/3ve;->a:LX/3vM;

    invoke-virtual {v0}, LX/3vM;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/Adapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 653386
    iget-object v0, p0, LX/3ve;->a:LX/3vM;

    invoke-static {v0}, LX/3vM;->a(LX/3vM;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, LX/3ve;->b:Landroid/os/Parcelable;

    .line 653387
    :cond_0
    iget-object v0, p0, LX/3ve;->a:LX/3vM;

    iget-object v1, p0, LX/3ve;->a:LX/3vM;

    iget v1, v1, LX/3vM;->z:I

    iput v1, v0, LX/3vM;->A:I

    .line 653388
    iget-object v0, p0, LX/3ve;->a:LX/3vM;

    iput v3, v0, LX/3vM;->z:I

    .line 653389
    iget-object v0, p0, LX/3ve;->a:LX/3vM;

    iput v2, v0, LX/3vM;->x:I

    .line 653390
    iget-object v0, p0, LX/3ve;->a:LX/3vM;

    iput-wide v4, v0, LX/3vM;->y:J

    .line 653391
    iget-object v0, p0, LX/3ve;->a:LX/3vM;

    iput v2, v0, LX/3vM;->v:I

    .line 653392
    iget-object v0, p0, LX/3ve;->a:LX/3vM;

    iput-wide v4, v0, LX/3vM;->w:J

    .line 653393
    iget-object v0, p0, LX/3ve;->a:LX/3vM;

    iput-boolean v3, v0, LX/3vM;->o:Z

    .line 653394
    iget-object v0, p0, LX/3ve;->a:LX/3vM;

    invoke-virtual {v0}, LX/3vM;->e()V

    .line 653395
    iget-object v0, p0, LX/3ve;->a:LX/3vM;

    invoke-virtual {v0}, LX/3vM;->requestLayout()V

    .line 653396
    return-void
.end method
