.class public LX/3kD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 632769
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/util/JsonReader;)LX/3kJ;
    .locals 4

    .prologue
    .line 632770
    invoke-virtual {p0}, Landroid/util/JsonReader;->beginObject()V

    .line 632771
    new-instance v1, LX/3kE;

    invoke-direct {v1}, LX/3kE;-><init>()V

    .line 632772
    :goto_0
    invoke-virtual {p0}, Landroid/util/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 632773
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v2

    .line 632774
    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 632775
    invoke-virtual {p0}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_0

    .line 632776
    :sswitch_0
    const-string v3, "key_values"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v3, "timing_curves"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    .line 632777
    :pswitch_0
    sget-object v0, LX/3kF;->a:LX/3JO;

    invoke-virtual {v0, p0}, LX/3JO;->a(Landroid/util/JsonReader;)Ljava/util/List;

    move-result-object v0

    iput-object v0, v1, LX/3kE;->a:Ljava/util/List;

    goto :goto_0

    .line 632778
    :pswitch_1
    invoke-static {p0}, LX/3JX;->b(Landroid/util/JsonReader;)[[[F

    move-result-object v0

    iput-object v0, v1, LX/3kE;->b:[[[F

    goto :goto_0

    .line 632779
    :cond_1
    invoke-virtual {p0}, Landroid/util/JsonReader;->endObject()V

    .line 632780
    new-instance v0, LX/3kJ;

    iget-object v2, v1, LX/3kE;->a:Ljava/util/List;

    iget-object v3, v1, LX/3kE;->b:[[[F

    invoke-direct {v0, v2, v3}, LX/3kJ;-><init>(Ljava/util/List;[[[F)V

    move-object v0, v0

    .line 632781
    return-object v0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5b3ddd07 -> :sswitch_1
        0x2375e302 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
