.class public final LX/4qV;
.super LX/3CB;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 814494
    const-class v0, Ljava/lang/Boolean;

    invoke-direct {p0, v0}, LX/3CB;-><init>(Ljava/lang/Class;)V

    return-void
.end method

.method private c(Ljava/lang/String;LX/0n3;)Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 814495
    const-string v0, "true"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 814496
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 814497
    :goto_0
    return-object v0

    .line 814498
    :cond_0
    const-string v0, "false"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 814499
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 814500
    :cond_1
    iget-object v0, p0, LX/3CB;->_keyClass:Ljava/lang/Class;

    const-string v1, "value not \'true\' or \'false\'"

    invoke-virtual {p2, v0, p1, v1}, LX/0n3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public final synthetic b(Ljava/lang/String;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 814501
    invoke-direct {p0, p1, p2}, LX/4qV;->c(Ljava/lang/String;LX/0n3;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
