.class public LX/50E;
.super LX/4wx;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/4wx",
        "<TE;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field private final count:I

.field private final element:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;I)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;I)V"
        }
    .end annotation

    .prologue
    .line 823527
    invoke-direct {p0}, LX/4wx;-><init>()V

    .line 823528
    iput-object p1, p0, LX/50E;->element:Ljava/lang/Object;

    .line 823529
    iput p2, p0, LX/50E;->count:I

    .line 823530
    const-string v0, "count"

    invoke-static {p2, v0}, LX/0P6;->a(ILjava/lang/String;)I

    .line 823531
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 823532
    iget-object v0, p0, LX/50E;->element:Ljava/lang/Object;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 823533
    iget v0, p0, LX/50E;->count:I

    return v0
.end method

.method public c()LX/50E;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/50E",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 823534
    const/4 v0, 0x0

    return-object v0
.end method
