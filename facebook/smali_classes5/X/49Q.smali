.class public LX/49Q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;


# instance fields
.field private final e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 674460
    sget-object v0, LX/0Tm;->e:LX/0Tn;

    const-string v1, "data_usage/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 674461
    sput-object v0, LX/49Q;->a:LX/0Tn;

    const-string v1, "limited"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/49Q;->b:LX/0Tn;

    .line 674462
    sget-object v0, LX/49Q;->a:LX/0Tn;

    const-string v1, "hourly_limit_kb"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/49Q;->c:LX/0Tn;

    .line 674463
    sget-object v0, LX/49Q;->a:LX/0Tn;

    const-string v1, "alarm_limit_kb"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/49Q;->d:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 674464
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 674465
    const-class v0, LX/49Q;

    iput-object v0, p0, LX/49Q;->e:Ljava/lang/Class;

    .line 674466
    iput-object p1, p0, LX/49Q;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 674467
    return-void
.end method
