.class public final LX/4Yy;
.super LX/0ur;
.source ""


# instance fields
.field public b:I

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 784947
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 784948
    instance-of v0, p0, LX/4Yy;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 784949
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;)LX/4Yy;
    .locals 2

    .prologue
    .line 784950
    new-instance v0, LX/4Yy;

    invoke-direct {v0}, LX/4Yy;-><init>()V

    .line 784951
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 784952
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;->a()I

    move-result v1

    iput v1, v0, LX/4Yy;->b:I

    .line 784953
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;->j()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Yy;->c:LX/0Px;

    .line 784954
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    iput-object v1, v0, LX/4Yy;->d:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 784955
    invoke-static {v0, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 784956
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;
    .locals 2

    .prologue
    .line 784957
    new-instance v0, Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;-><init>(LX/4Yy;)V

    .line 784958
    return-object v0
.end method
