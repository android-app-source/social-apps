.class public LX/3RS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/customthreads/annotations/CanViewThreadCustomization;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 580433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 580434
    iput-object p1, p0, LX/3RS;->a:LX/0Or;

    .line 580435
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 1

    .prologue
    .line 580436
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->v:LX/0P1;

    .line 580437
    iget-object p1, p0, LX/3RS;->a:LX/0Or;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_2

    .line 580438
    const/4 p1, 0x0

    .line 580439
    :cond_0
    :goto_0
    move-object p1, p1

    .line 580440
    if-eqz p1, :cond_1

    const/4 p1, 0x1

    :goto_1
    move v0, p1

    .line 580441
    return v0

    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    .line 580442
    :cond_2
    const-string p1, "hot_emoji_size"

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 580443
    if-nez p1, :cond_0

    .line 580444
    const-string p1, "emoji_like"

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    goto :goto_0
.end method
