.class public LX/4xj;
.super LX/0Xt;
.source ""

# interfaces
.implements LX/4xi;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Xt",
        "<TK;TV;>;",
        "LX/4xi",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public final a:LX/0Xu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Xu",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final b:LX/0Rl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rl",
            "<-",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Xu;LX/0Rl;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Xu",
            "<TK;TV;>;",
            "LX/0Rl",
            "<-",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 821273
    invoke-direct {p0}, LX/0Xt;-><init>()V

    .line 821274
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xu;

    iput-object v0, p0, LX/4xj;->a:LX/0Xu;

    .line 821275
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Rl;

    iput-object v0, p0, LX/4xj;->b:LX/0Rl;

    .line 821276
    return-void
.end method

.method public static a(Ljava/util/Collection;LX/0Rl;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<TE;>;",
            "LX/0Rl",
            "<-TE;>;)",
            "Ljava/util/Collection",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 821270
    instance-of v0, p0, Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 821271
    check-cast p0, Ljava/util/Set;

    invoke-static {p0, p1}, LX/0RA;->a(Ljava/util/Set;LX/0Rl;)Ljava/util/Set;

    move-result-object v0

    .line 821272
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, LX/0PN;->a(Ljava/util/Collection;LX/0Rl;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(LX/4xj;Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)Z"
        }
    .end annotation

    .prologue
    .line 821269
    iget-object v0, p0, LX/4xj;->b:LX/0Rl;

    invoke-static {p1, p2}, LX/0PM;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Rl;->apply(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()LX/0Xu;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Xu",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 821268
    iget-object v0, p0, LX/4xj;->a:LX/0Xu;

    return-object v0
.end method

.method public final a(LX/0Rl;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rl",
            "<-",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;>;)Z"
        }
    .end annotation

    .prologue
    .line 821255
    iget-object v0, p0, LX/4xj;->a:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 821256
    const/4 v0, 0x0

    move v2, v0

    .line 821257
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 821258
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 821259
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    .line 821260
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    new-instance v5, LX/4xh;

    invoke-direct {v5, p0, v4}, LX/4xh;-><init>(LX/4xj;Ljava/lang/Object;)V

    invoke-static {v1, v5}, LX/4xj;->a(Ljava/util/Collection;LX/0Rl;)Ljava/util/Collection;

    move-result-object v1

    .line 821261
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-static {v4, v1}, LX/0PM;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v4

    invoke-interface {p1, v4}, LX/0Rl;->apply(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 821262
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    if-ne v2, v0, :cond_0

    .line 821263
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 821264
    :goto_1
    const/4 v2, 0x1

    move v0, v2

    :goto_2
    move v2, v0

    .line 821265
    goto :goto_0

    .line 821266
    :cond_0
    invoke-interface {v1}, Ljava/util/Collection;->clear()V

    goto :goto_1

    .line 821267
    :cond_1
    return v2

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method public final c()LX/0Rl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rl",
            "<-",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 821254
    iget-object v0, p0, LX/4xj;->b:LX/0Rl;

    return-object v0
.end method

.method public c(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 821253
    iget-object v0, p0, LX/4xj;->a:LX/0Xu;

    invoke-interface {v0, p1}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    new-instance v1, LX/4xh;

    invoke-direct {v1, p0, p1}, LX/4xh;-><init>(LX/4xj;Ljava/lang/Object;)V

    invoke-static {v0, v1}, LX/4xj;->a(Ljava/util/Collection;LX/0Rl;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 821249
    invoke-virtual {p0}, LX/0Xt;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 821250
    iget-object v1, p0, LX/4xj;->a:LX/0Xu;

    instance-of v1, v1, LX/0vX;

    if-eqz v1, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    :goto_0
    move-object v1, v1

    .line 821251
    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 821277
    invoke-virtual {p0}, LX/0Xt;->k()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    return v0
.end method

.method public final f(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 821252
    invoke-virtual {p0}, LX/0Xt;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 821247
    invoke-virtual {p0}, LX/0Xt;->k()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 821248
    return-void
.end method

.method public final l()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 821246
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should never be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public final m()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 821245
    new-instance v0, LX/4xd;

    invoke-direct {v0, p0}, LX/4xd;-><init>(LX/4xj;)V

    return-object v0
.end method

.method public o()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 821244
    iget-object v0, p0, LX/4xj;->a:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->k()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, LX/4xj;->b:LX/0Rl;

    invoke-static {v0, v1}, LX/4xj;->a(Ljava/util/Collection;LX/0Rl;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final p()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 821243
    invoke-virtual {p0}, LX/0Xt;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final r()LX/1M1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1M1",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 821242
    new-instance v0, LX/4xg;

    invoke-direct {v0, p0}, LX/4xg;-><init>(LX/4xj;)V

    return-object v0
.end method

.method public final s()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 821241
    new-instance v0, LX/4xl;

    invoke-direct {v0, p0}, LX/4xl;-><init>(LX/4xi;)V

    return-object v0
.end method
