.class public final enum LX/4mK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4mK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4mK;

.field public static final enum ACTIVE:LX/4mK;

.field public static final enum COMPLETE:LX/4mK;

.field public static final enum INACTIVE:LX/4mK;

.field public static final enum SCHEDULED:LX/4mK;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 805632
    new-instance v0, LX/4mK;

    const-string v1, "INACTIVE"

    invoke-direct {v0, v1, v2}, LX/4mK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4mK;->INACTIVE:LX/4mK;

    .line 805633
    new-instance v0, LX/4mK;

    const-string v1, "SCHEDULED"

    invoke-direct {v0, v1, v3}, LX/4mK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4mK;->SCHEDULED:LX/4mK;

    .line 805634
    new-instance v0, LX/4mK;

    const-string v1, "ACTIVE"

    invoke-direct {v0, v1, v4}, LX/4mK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4mK;->ACTIVE:LX/4mK;

    .line 805635
    new-instance v0, LX/4mK;

    const-string v1, "COMPLETE"

    invoke-direct {v0, v1, v5}, LX/4mK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4mK;->COMPLETE:LX/4mK;

    .line 805636
    const/4 v0, 0x4

    new-array v0, v0, [LX/4mK;

    sget-object v1, LX/4mK;->INACTIVE:LX/4mK;

    aput-object v1, v0, v2

    sget-object v1, LX/4mK;->SCHEDULED:LX/4mK;

    aput-object v1, v0, v3

    sget-object v1, LX/4mK;->ACTIVE:LX/4mK;

    aput-object v1, v0, v4

    sget-object v1, LX/4mK;->COMPLETE:LX/4mK;

    aput-object v1, v0, v5

    sput-object v0, LX/4mK;->$VALUES:[LX/4mK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 805637
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4mK;
    .locals 1

    .prologue
    .line 805638
    const-class v0, LX/4mK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4mK;

    return-object v0
.end method

.method public static values()[LX/4mK;
    .locals 1

    .prologue
    .line 805639
    sget-object v0, LX/4mK;->$VALUES:[LX/4mK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4mK;

    return-object v0
.end method
