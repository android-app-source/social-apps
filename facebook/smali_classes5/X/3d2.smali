.class public LX/3d2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final b:LX/3d4;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3d4",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;LX/3d4;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;",
            "LX/3d4",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 615964
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 615965
    iput-object p1, p0, LX/3d2;->a:Ljava/lang/Class;

    .line 615966
    iput-object p2, p0, LX/3d2;->b:LX/3d4;

    .line 615967
    return-void
.end method

.method public static c(LX/3d2;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U:",
            "Ljava/lang/Object;",
            ">(TU;)TU;"
        }
    .end annotation

    .prologue
    .line 615958
    instance-of v0, p1, LX/0jT;

    if-nez v0, :cond_0

    .line 615959
    :goto_0
    return-object p1

    .line 615960
    :cond_0
    new-instance v1, LX/3d5;

    invoke-direct {v1, p0}, LX/3d5;-><init>(LX/3d2;)V

    .line 615961
    iget-object v0, p0, LX/3d2;->a:Ljava/lang/Class;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 615962
    iget-object v0, p0, LX/3d2;->b:LX/3d4;

    invoke-interface {v0, p1}, LX/3d4;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 615963
    :goto_1
    if-nez v0, :cond_1

    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    check-cast v0, LX/0jT;

    invoke-interface {v0, v1}, LX/0jT;->a(LX/1jy;)LX/0jT;

    move-result-object p1

    goto :goto_0

    :cond_2
    move-object v0, p1

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U:",
            "Ljava/lang/Object;",
            ">(TU;)TU;"
        }
    .end annotation

    .prologue
    .line 615920
    instance-of v0, p1, LX/0Px;

    if-eqz v0, :cond_1

    .line 615921
    check-cast p1, LX/0Px;

    const/4 v0, 0x0

    .line 615922
    if-nez p1, :cond_6

    .line 615923
    :cond_0
    :goto_0
    move-object v0, p1

    .line 615924
    :goto_1
    return-object v0

    .line 615925
    :cond_1
    instance-of v0, p1, Ljava/util/List;

    if-eqz v0, :cond_2

    .line 615926
    check-cast p1, Ljava/util/List;

    .line 615927
    if-nez p1, :cond_b

    .line 615928
    :goto_2
    move-object v0, p1

    .line 615929
    goto :goto_1

    .line 615930
    :cond_2
    instance-of v0, p1, Ljava/util/Map;

    if-eqz v0, :cond_5

    .line 615931
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 615932
    check-cast p1, Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 615933
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/3d2;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 615934
    if-eqz v3, :cond_3

    .line 615935
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_4
    move-object v0, v1

    .line 615936
    goto :goto_1

    .line 615937
    :cond_5
    invoke-static {p0, p1}, LX/3d2;->c(LX/3d2;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1

    .line 615938
    :cond_6
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    invoke-static {v1}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v4

    .line 615939
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    move v3, v0

    move v2, v0

    :goto_4
    if-ge v3, v5, :cond_a

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 615940
    instance-of v0, v1, LX/0jT;

    if-eqz v0, :cond_8

    move-object v0, v1

    .line 615941
    check-cast v0, LX/0jT;

    invoke-static {p0, v0}, LX/3d2;->c(LX/3d2;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 615942
    if-eqz v0, :cond_7

    .line 615943
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 615944
    :cond_7
    if-eq v0, v1, :cond_9

    .line 615945
    const/4 v0, 0x1

    .line 615946
    :goto_5
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v0

    goto :goto_4

    .line 615947
    :cond_8
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_9
    move v0, v2

    goto :goto_5

    .line 615948
    :cond_a
    if-eqz v2, :cond_0

    .line 615949
    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object p1

    goto :goto_0

    .line 615950
    :cond_b
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 615951
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_c
    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 615952
    instance-of v3, v0, LX/0jT;

    if-eqz v3, :cond_d

    .line 615953
    check-cast v0, LX/0jT;

    invoke-static {p0, v0}, LX/3d2;->c(LX/3d2;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 615954
    if-eqz v0, :cond_c

    .line 615955
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 615956
    :cond_d
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_e
    move-object p1, v1

    .line 615957
    goto/16 :goto_2
.end method
