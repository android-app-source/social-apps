.class public final LX/4yW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K::",
        "Ljava/lang/Comparable",
        "<*>;V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/4x5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4x5",
            "<TK;>;"
        }
    .end annotation
.end field

.field private final b:LX/4yY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4yY",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 821835
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 821836
    new-instance v0, LX/51U;

    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    invoke-direct {v0, v1}, LX/51U;-><init>(Ljava/util/NavigableMap;)V

    move-object v0, v0

    .line 821837
    iput-object v0, p0, LX/4yW;->a:LX/4x5;

    .line 821838
    new-instance v0, LX/51S;

    invoke-direct {v0}, LX/51S;-><init>()V

    move-object v0, v0

    .line 821839
    iput-object v0, p0, LX/4yW;->b:LX/4yY;

    .line 821840
    return-void
.end method


# virtual methods
.method public final a(LX/50M;Ljava/lang/Object;)LX/4yW;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/50M",
            "<TK;>;TV;)",
            "LX/4yW",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 821841
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 821842
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 821843
    invoke-virtual {p1}, LX/50M;->i()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Range must not be empty, but was %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v3, v1}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 821844
    iget-object v0, p0, LX/4yW;->a:LX/4x5;

    invoke-interface {v0}, LX/4x5;->b()LX/4x5;

    move-result-object v0

    invoke-interface {v0, p1}, LX/4x5;->c(LX/50M;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 821845
    iget-object v0, p0, LX/4yW;->b:LX/4yY;

    invoke-interface {v0}, LX/4yY;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 821846
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/50M;

    .line 821847
    invoke-virtual {v1, p1}, LX/50M;->b(LX/50M;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1, p1}, LX/50M;->c(LX/50M;)LX/50M;

    move-result-object v1

    invoke-virtual {v1}, LX/50M;->i()Z

    move-result v1

    if-nez v1, :cond_0

    .line 821848
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Overlapping ranges: range "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " overlaps with entry "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    move v0, v2

    .line 821849
    goto :goto_0

    .line 821850
    :cond_2
    iget-object v0, p0, LX/4yW;->a:LX/4x5;

    invoke-interface {v0, p1}, LX/4x5;->a(LX/50M;)V

    .line 821851
    iget-object v0, p0, LX/4yW;->b:LX/4yY;

    invoke-interface {v0, p1, p2}, LX/4yY;->a(LX/50M;Ljava/lang/Object;)V

    .line 821852
    return-object p0
.end method

.method public final a()LX/4yZ;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4yZ",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 821853
    iget-object v0, p0, LX/4yW;->b:LX/4yY;

    invoke-interface {v0}, LX/4yY;->b()Ljava/util/Map;

    move-result-object v0

    .line 821854
    new-instance v1, LX/0Pz;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, LX/0Pz;-><init>(I)V

    .line 821855
    new-instance v2, LX/0Pz;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v3

    invoke-direct {v2, v3}, LX/0Pz;-><init>(I)V

    .line 821856
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 821857
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 821858
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 821859
    :cond_0
    new-instance v0, LX/4yZ;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/4yZ;-><init>(LX/0Px;LX/0Px;)V

    return-object v0
.end method
