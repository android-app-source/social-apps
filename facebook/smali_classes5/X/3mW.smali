.class public abstract LX/3mW;
.super LX/3mX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PageProp:",
        "Ljava/lang/Object;",
        "E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/3mX",
        "<TPageProp;TE;>;"
    }
.end annotation


# instance fields
.field private final c:LX/3mU;

.field private final d:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feed/rows/core/props/FeedProps;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/3mU;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;LX/25M;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<TPageProp;>;",
            "LX/3mU;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps;",
            "LX/25M;",
            ")V"
        }
    .end annotation

    .prologue
    .line 636444
    invoke-direct {p0, p1, p2, p4, p6}, LX/3mX;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 636445
    iput-object p3, p0, LX/3mW;->c:LX/3mU;

    .line 636446
    iput-object p4, p0, LX/3mW;->d:LX/1Pq;

    .line 636447
    iput-object p5, p0, LX/3mW;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 636448
    return-void
.end method


# virtual methods
.method public a(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V
    .locals 1

    .prologue
    .line 636449
    invoke-super {p0, p1}, LX/3mX;->a(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    .line 636450
    iget-object v0, p0, LX/3mW;->c:LX/3mU;

    .line 636451
    iput-object p0, v0, LX/3mU;->a:LX/3mW;

    .line 636452
    return-void
.end method

.method public b(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V
    .locals 2

    .prologue
    .line 636453
    invoke-super {p0, p1}, LX/3mX;->b(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    .line 636454
    iget-object v0, p0, LX/3mW;->c:LX/3mU;

    const/4 v1, 0x0

    .line 636455
    iput-object v1, v0, LX/3mU;->a:LX/3mW;

    .line 636456
    return-void
.end method

.method public synthetic e(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 636457
    check-cast p1, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {p0, p1}, LX/3mW;->a(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    return-void
.end method

.method public synthetic f(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 636458
    check-cast p1, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {p0, p1}, LX/3mW;->b(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    return-void
.end method

.method public final g(I)V
    .locals 1

    .prologue
    .line 636459
    invoke-super {p0, p1}, LX/3mX;->g(I)V

    .line 636460
    iget-boolean v0, p0, LX/3mW;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3mW;->c:LX/3mU;

    invoke-virtual {v0, p1}, LX/3mU;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 636461
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3mW;->f:Z

    .line 636462
    iget-object v0, p0, LX/3mW;->c:LX/3mU;

    invoke-virtual {v0}, LX/3mU;->a()V

    .line 636463
    :cond_0
    return-void
.end method
