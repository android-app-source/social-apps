.class public abstract LX/3vJ;
.super Landroid/view/ViewGroup;
.source ""


# static fields
.field public static final j:Landroid/view/animation/Interpolator;


# instance fields
.field public final a:LX/3vI;

.field public final b:Landroid/content/Context;

.field public c:Landroid/support/v7/widget/ActionMenuView;

.field public d:LX/3wb;

.field public e:Landroid/view/ViewGroup;

.field public f:Z

.field public g:Z

.field public h:I

.field public i:LX/3sU;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 651139
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, LX/3vJ;->j:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 651137
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3vJ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 651138
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 651135
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3vJ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 651136
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 651128
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 651129
    new-instance v0, LX/3vI;

    invoke-direct {v0, p0}, LX/3vI;-><init>(LX/3vJ;)V

    iput-object v0, p0, LX/3vJ;->a:LX/3vI;

    .line 651130
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 651131
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f010010

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, v0, Landroid/util/TypedValue;->resourceId:I

    if-eqz v1, :cond_0

    .line 651132
    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-direct {v1, p1, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, LX/3vJ;->b:Landroid/content/Context;

    .line 651133
    :goto_0
    return-void

    .line 651134
    :cond_0
    iput-object p1, p0, LX/3vJ;->b:Landroid/content/Context;

    goto :goto_0
.end method

.method public static a(IIZ)I
    .locals 1

    .prologue
    .line 651091
    if-eqz p2, :cond_0

    sub-int v0, p0, p1

    :goto_0
    return v0

    :cond_0
    add-int v0, p0, p1

    goto :goto_0
.end method

.method public static a(Landroid/view/View;III)I
    .locals 2

    .prologue
    .line 651124
    const/high16 v0, -0x80000000

    invoke-static {p1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p0, v0, p2}, Landroid/view/View;->measure(II)V

    .line 651125
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    sub-int v0, p1, v0

    .line 651126
    sub-int/2addr v0, p3

    .line 651127
    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/View;IIIZ)I
    .locals 4

    .prologue
    .line 651117
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 651118
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 651119
    sub-int v2, p3, v1

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, p2

    .line 651120
    if-eqz p4, :cond_1

    .line 651121
    sub-int v3, p1, v0

    add-int/2addr v1, v2

    invoke-virtual {p0, v3, v2, p1, v1}, Landroid/view/View;->layout(IIII)V

    .line 651122
    :goto_0
    if-eqz p4, :cond_0

    neg-int v0, v0

    :cond_0
    return v0

    .line 651123
    :cond_1
    add-int v3, p1, v0

    add-int/2addr v1, v2

    invoke-virtual {p0, p1, v2, v3, v1}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method


# virtual methods
.method public getAnimatedVisibility()I
    .locals 1

    .prologue
    .line 651114
    iget-object v0, p0, LX/3vJ;->i:LX/3sU;

    if-eqz v0, :cond_0

    .line 651115
    iget-object v0, p0, LX/3vJ;->a:LX/3vI;

    iget v0, v0, LX/3vI;->a:I

    .line 651116
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/3vJ;->getVisibility()I

    move-result v0

    goto :goto_0
.end method

.method public getContentHeight()I
    .locals 1

    .prologue
    .line 651113
    iget v0, p0, LX/3vJ;->h:I

    return v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 651101
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    .line 651102
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 651103
    :cond_0
    invoke-virtual {p0}, LX/3vJ;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    sget-object v2, LX/03r;->ActionBar:[I

    const v3, 0x7f010011

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 651104
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v1

    invoke-virtual {p0, v1}, LX/3vJ;->setContentHeight(I)V

    .line 651105
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 651106
    iget-object v0, p0, LX/3vJ;->d:LX/3wb;

    if-eqz v0, :cond_2

    .line 651107
    iget-object v0, p0, LX/3vJ;->d:LX/3wb;

    .line 651108
    iget-boolean v1, v0, LX/3wb;->o:Z

    if-nez v1, :cond_1

    .line 651109
    iget-object v1, v0, LX/3ut;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, v0, LX/3wb;->n:I

    .line 651110
    :cond_1
    iget-object v1, v0, LX/3ut;->c:LX/3v0;

    if-eqz v1, :cond_2

    .line 651111
    iget-object v1, v0, LX/3ut;->c:LX/3v0;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/3v0;->b(Z)V

    .line 651112
    :cond_2
    return-void
.end method

.method public setContentHeight(I)V
    .locals 0

    .prologue
    .line 651098
    iput p1, p0, LX/3vJ;->h:I

    .line 651099
    invoke-virtual {p0}, LX/3vJ;->requestLayout()V

    .line 651100
    return-void
.end method

.method public setSplitToolbar(Z)V
    .locals 0

    .prologue
    .line 651096
    iput-boolean p1, p0, LX/3vJ;->f:Z

    .line 651097
    return-void
.end method

.method public setSplitView(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 651094
    iput-object p1, p0, LX/3vJ;->e:Landroid/view/ViewGroup;

    .line 651095
    return-void
.end method

.method public setSplitWhenNarrow(Z)V
    .locals 0

    .prologue
    .line 651092
    iput-boolean p1, p0, LX/3vJ;->g:Z

    .line 651093
    return-void
.end method
