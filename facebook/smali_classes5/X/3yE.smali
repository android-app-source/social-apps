.class public LX/3yE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0pZ;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/2WT;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 660805
    const-class v0, LX/3yE;

    sput-object v0, LX/3yE;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 660806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 660807
    new-instance v0, LX/2WT;

    const-string v1, "local_default_group"

    .line 660808
    sget-object v2, LX/0Rg;->a:LX/0Rg;

    move-object v2, v2

    .line 660809
    invoke-direct {v0, v3, v3, v1, v2}, LX/2WT;-><init>(ZZLjava/lang/String;LX/0P1;)V

    iput-object v0, p0, LX/3yE;->b:LX/2WT;

    .line 660810
    return-void
.end method

.method private static d(LX/2Ff;)LX/2Fe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<CONFIG:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2Ff",
            "<TCONFIG;>;)",
            "LX/2Fe",
            "<TCONFIG;>;"
        }
    .end annotation

    .prologue
    .line 660811
    instance-of v0, p0, LX/2Fe;

    const-string v1, "QuickExperimentControllerLite can only handle the new QE API"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 660812
    check-cast p0, LX/2Fe;

    return-object p0
.end method


# virtual methods
.method public final a(LX/2Ff;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<CONFIG:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2Ff",
            "<TCONFIG;>;)TCONFIG;"
        }
    .end annotation

    .prologue
    .line 660813
    invoke-static {p1}, LX/3yE;->d(LX/2Ff;)LX/2Fe;

    move-result-object v0

    iget-object v1, p0, LX/3yE;->b:LX/2WT;

    invoke-interface {v0, v1}, LX/2Fe;->a(LX/2WT;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/2Ff;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<CONFIG:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2Ff",
            "<TCONFIG;>;)V"
        }
    .end annotation

    .prologue
    .line 660814
    invoke-static {p1}, LX/3yE;->d(LX/2Ff;)LX/2Fe;

    .line 660815
    return-void
.end method

.method public final c(LX/2Ff;)LX/3yG;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<CONFIG:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2Ff",
            "<TCONFIG;>;)",
            "LX/3yG;"
        }
    .end annotation

    .prologue
    .line 660816
    new-instance v0, LX/3yG;

    const-string v1, "local_default_group"

    invoke-direct {v0, v1}, LX/3yG;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
