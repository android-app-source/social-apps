.class public LX/4R7;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 707683
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 707684
    const/4 v12, 0x0

    .line 707685
    const/4 v9, 0x0

    .line 707686
    const-wide/16 v10, 0x0

    .line 707687
    const/4 v8, 0x0

    .line 707688
    const/4 v7, 0x0

    .line 707689
    const/4 v6, 0x0

    .line 707690
    const/4 v5, 0x0

    .line 707691
    const/4 v4, 0x0

    .line 707692
    const/4 v3, 0x0

    .line 707693
    const/4 v2, 0x0

    .line 707694
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_c

    .line 707695
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 707696
    const/4 v2, 0x0

    .line 707697
    :goto_0
    return v2

    .line 707698
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v14, :cond_a

    .line 707699
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 707700
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 707701
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_0

    if-eqz v3, :cond_0

    .line 707702
    const-string v14, "cache_id"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 707703
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v7, v3

    goto :goto_1

    .line 707704
    :cond_1
    const-string v14, "debug_info"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 707705
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v6, v3

    goto :goto_1

    .line 707706
    :cond_2
    const-string v14, "fetchTimeMs"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 707707
    const/4 v2, 0x1

    .line 707708
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto :goto_1

    .line 707709
    :cond_3
    const-string v14, "id"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 707710
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v13, v3

    goto :goto_1

    .line 707711
    :cond_4
    const-string v14, "people_to_follow_at_work_items"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 707712
    invoke-static/range {p0 .. p1}, LX/4R5;->a(LX/15w;LX/186;)I

    move-result v3

    move v12, v3

    goto :goto_1

    .line 707713
    :cond_5
    const-string v14, "short_term_cache_key"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 707714
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v11, v3

    goto/16 :goto_1

    .line 707715
    :cond_6
    const-string v14, "title"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 707716
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    move v10, v3

    goto/16 :goto_1

    .line 707717
    :cond_7
    const-string v14, "titleForSummary"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 707718
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    move v9, v3

    goto/16 :goto_1

    .line 707719
    :cond_8
    const-string v14, "url"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 707720
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 707721
    :cond_9
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 707722
    :cond_a
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 707723
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 707724
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 707725
    if-eqz v2, :cond_b

    .line 707726
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 707727
    :cond_b
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 707728
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 707729
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 707730
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 707731
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 707732
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 707733
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_c
    move v13, v8

    move v8, v3

    move/from16 v16, v5

    move/from16 v17, v6

    move v6, v9

    move v9, v4

    move-wide v4, v10

    move/from16 v10, v16

    move/from16 v11, v17

    move/from16 v18, v7

    move v7, v12

    move/from16 v12, v18

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 707734
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 707735
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 707736
    invoke-static {p0, v2}, LX/4R7;->a(LX/15w;LX/186;)I

    move-result v1

    .line 707737
    if-eqz v0, :cond_0

    .line 707738
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 707739
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 707740
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 707741
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 707742
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 707743
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 707744
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 707745
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 707746
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707747
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 707748
    const-string v0, "name"

    const-string v1, "PeopleYouShouldFollowAtWorkFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 707749
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 707750
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 707751
    if-eqz v0, :cond_0

    .line 707752
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707753
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 707754
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 707755
    if-eqz v0, :cond_1

    .line 707756
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707757
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 707758
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 707759
    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    .line 707760
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707761
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 707762
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 707763
    if-eqz v0, :cond_3

    .line 707764
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707765
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 707766
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 707767
    if-eqz v0, :cond_4

    .line 707768
    const-string v1, "people_to_follow_at_work_items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707769
    invoke-static {p0, v0, p2, p3}, LX/4R5;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 707770
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 707771
    if-eqz v0, :cond_5

    .line 707772
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707773
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 707774
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 707775
    if-eqz v0, :cond_6

    .line 707776
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707777
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 707778
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 707779
    if-eqz v0, :cond_7

    .line 707780
    const-string v1, "titleForSummary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707781
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 707782
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 707783
    if-eqz v0, :cond_8

    .line 707784
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707785
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 707786
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 707787
    return-void
.end method
