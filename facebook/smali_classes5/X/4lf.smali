.class public LX/4lf;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Ljavax/net/ssl/HostnameVerifier;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Ljavax/net/ssl/HostnameVerifier;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 804611
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Ljavax/net/ssl/HostnameVerifier;
    .locals 3

    .prologue
    .line 804612
    sget-object v0, LX/4lf;->a:Ljavax/net/ssl/HostnameVerifier;

    if-nez v0, :cond_1

    .line 804613
    const-class v1, LX/4lf;

    monitor-enter v1

    .line 804614
    :try_start_0
    sget-object v0, LX/4lf;->a:Ljavax/net/ssl/HostnameVerifier;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 804615
    if-eqz v2, :cond_0

    .line 804616
    :try_start_1
    invoke-static {}, LX/4lg;->b()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v0

    sput-object v0, LX/4lf;->a:Ljavax/net/ssl/HostnameVerifier;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 804617
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 804618
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 804619
    :cond_1
    sget-object v0, LX/4lf;->a:Ljavax/net/ssl/HostnameVerifier;

    return-object v0

    .line 804620
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 804621
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 804622
    invoke-static {}, LX/4lg;->b()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v0

    return-object v0
.end method
