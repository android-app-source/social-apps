.class public LX/3W2;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/2yW;
.implements LX/2yX;


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field private final b:Landroid/widget/TextView;

.field private final c:Lcom/facebook/resources/ui/EllipsizingTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 588963
    new-instance v0, LX/3W3;

    invoke-direct {v0}, LX/3W3;-><init>()V

    sput-object v0, LX/3W2;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 588964
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 588965
    const v0, 0x7f031544

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 588966
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/3W2;->setOrientation(I)V

    .line 588967
    const v0, 0x7f0d052c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3W2;->b:Landroid/widget/TextView;

    .line 588968
    const v0, 0x7f0d052d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/EllipsizingTextView;

    iput-object v0, p0, LX/3W2;->c:Lcom/facebook/resources/ui/EllipsizingTextView;

    .line 588969
    return-void
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 588970
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 588971
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 588972
    return-void

    .line 588973
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 588974
    return-void
.end method

.method public setContextText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 588975
    iget-object v0, p0, LX/3W2;->c:Lcom/facebook/resources/ui/EllipsizingTextView;

    invoke-static {v0, p1}, LX/3W2;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 588976
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 588977
    iget-object v0, p0, LX/3W2;->b:Landroid/widget/TextView;

    invoke-static {v0, p1}, LX/3W2;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 588978
    return-void
.end method
