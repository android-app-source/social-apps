.class public LX/4S7;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 711723
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 18

    .prologue
    .line 711724
    const/4 v14, 0x0

    .line 711725
    const/4 v11, 0x0

    .line 711726
    const-wide/16 v12, 0x0

    .line 711727
    const/4 v10, 0x0

    .line 711728
    const/4 v9, 0x0

    .line 711729
    const/4 v8, 0x0

    .line 711730
    const/4 v7, 0x0

    .line 711731
    const/4 v6, 0x0

    .line 711732
    const/4 v5, 0x0

    .line 711733
    const/4 v4, 0x0

    .line 711734
    const/4 v3, 0x0

    .line 711735
    const/4 v2, 0x0

    .line 711736
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_e

    .line 711737
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 711738
    const/4 v2, 0x0

    .line 711739
    :goto_0
    return v2

    .line 711740
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_b

    .line 711741
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 711742
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 711743
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v17, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 711744
    const-string v6, "cache_id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 711745
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v16, v2

    goto :goto_1

    .line 711746
    :cond_1
    const-string v6, "debug_info"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 711747
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 711748
    :cond_2
    const-string v6, "fetchTimeMs"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 711749
    const/4 v2, 0x1

    .line 711750
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 711751
    :cond_3
    const-string v6, "native_template_view"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 711752
    invoke-static/range {p0 .. p1}, LX/4Pn;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto :goto_1

    .line 711753
    :cond_4
    const-string v6, "quick_promotion_items"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 711754
    invoke-static/range {p0 .. p1}, LX/4S6;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto :goto_1

    .line 711755
    :cond_5
    const-string v6, "short_term_cache_key"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 711756
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 711757
    :cond_6
    const-string v6, "tracking"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 711758
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 711759
    :cond_7
    const-string v6, "local_last_negative_feedback_action_type"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 711760
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 711761
    :cond_8
    const-string v6, "local_story_visibility"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 711762
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 711763
    :cond_9
    const-string v6, "local_story_visible_height"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 711764
    const/4 v2, 0x1

    .line 711765
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move v9, v6

    goto/16 :goto_1

    .line 711766
    :cond_a
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 711767
    :cond_b
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 711768
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 711769
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 711770
    if-eqz v3, :cond_c

    .line 711771
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 711772
    :cond_c
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 711773
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 711774
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 711775
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 711776
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 711777
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 711778
    if-eqz v8, :cond_d

    .line 711779
    const/16 v2, 0x9

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9, v3}, LX/186;->a(III)V

    .line 711780
    :cond_d
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_e
    move v15, v10

    move/from16 v16, v14

    move v10, v5

    move v14, v9

    move v9, v4

    move-wide v4, v12

    move v13, v8

    move v12, v7

    move v7, v11

    move v8, v2

    move v11, v6

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 711781
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 711782
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 711783
    invoke-static {p0, v2}, LX/4S7;->a(LX/15w;LX/186;)I

    move-result v1

    .line 711784
    if-eqz v0, :cond_0

    .line 711785
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 711786
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 711787
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 711788
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 711789
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 711790
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 711791
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 711792
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 711793
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711794
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 711795
    const-string v0, "name"

    const-string v1, "QuickPromotionNativeTemplateFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 711796
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 711797
    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 711798
    if-eqz v0, :cond_0

    .line 711799
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711800
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 711801
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 711802
    if-eqz v0, :cond_1

    .line 711803
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711804
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 711805
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 711806
    cmp-long v2, v0, v4

    if-eqz v2, :cond_2

    .line 711807
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711808
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 711809
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 711810
    if-eqz v0, :cond_3

    .line 711811
    const-string v1, "native_template_view"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711812
    invoke-static {p0, v0, p2, p3}, LX/4Pn;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 711813
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 711814
    if-eqz v0, :cond_4

    .line 711815
    const-string v1, "quick_promotion_items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711816
    invoke-static {p0, v0, p2, p3}, LX/4S6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 711817
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 711818
    if-eqz v0, :cond_5

    .line 711819
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711820
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 711821
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 711822
    if-eqz v0, :cond_6

    .line 711823
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711824
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 711825
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 711826
    if-eqz v0, :cond_7

    .line 711827
    const-string v1, "local_last_negative_feedback_action_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711828
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 711829
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 711830
    if-eqz v0, :cond_8

    .line 711831
    const-string v1, "local_story_visibility"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711832
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 711833
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 711834
    if-eqz v0, :cond_9

    .line 711835
    const-string v1, "local_story_visible_height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711836
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 711837
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 711838
    return-void
.end method
