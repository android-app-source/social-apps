.class public LX/4Rq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 710764
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 710765
    const/4 v14, 0x0

    .line 710766
    const-wide/16 v12, 0x0

    .line 710767
    const/4 v11, 0x0

    .line 710768
    const/4 v10, 0x0

    .line 710769
    const/4 v9, 0x0

    .line 710770
    const/4 v8, 0x0

    .line 710771
    const/4 v7, 0x0

    .line 710772
    const/4 v6, 0x0

    .line 710773
    const/4 v5, 0x0

    .line 710774
    const/4 v4, 0x0

    .line 710775
    const/4 v3, 0x0

    .line 710776
    const/4 v2, 0x0

    .line 710777
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_e

    .line 710778
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 710779
    const/4 v2, 0x0

    .line 710780
    :goto_0
    return v2

    .line 710781
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_b

    .line 710782
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 710783
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 710784
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v17, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 710785
    const-string v6, "application"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 710786
    invoke-static/range {p0 .. p1}, LX/2uk;->a(LX/15w;LX/186;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 710787
    :cond_1
    const-string v6, "default_expiration_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 710788
    const/4 v2, 0x1

    .line 710789
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 710790
    :cond_2
    const-string v6, "description"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 710791
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto :goto_1

    .line 710792
    :cond_3
    const-string v6, "mask"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 710793
    invoke-static/range {p0 .. p1}, LX/4Rr;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto :goto_1

    .line 710794
    :cond_4
    const-string v6, "not_installed_description"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 710795
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto :goto_1

    .line 710796
    :cond_5
    const-string v6, "not_installed_title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 710797
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v13, v2

    goto :goto_1

    .line 710798
    :cond_6
    const-string v6, "profile"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 710799
    invoke-static/range {p0 .. p1}, LX/2aw;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 710800
    :cond_7
    const-string v6, "title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 710801
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 710802
    :cond_8
    const-string v6, "url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 710803
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 710804
    :cond_9
    const-string v6, "link_type"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 710805
    const/4 v2, 0x1

    .line 710806
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v6

    move v8, v2

    move-object v9, v6

    goto/16 :goto_1

    .line 710807
    :cond_a
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 710808
    :cond_b
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 710809
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 710810
    if-eqz v3, :cond_c

    .line 710811
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 710812
    :cond_c
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 710813
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 710814
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 710815
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 710816
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 710817
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 710818
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 710819
    if-eqz v8, :cond_d

    .line 710820
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->a(ILjava/lang/Enum;)V

    .line 710821
    :cond_d
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_e
    move v15, v10

    move/from16 v16, v11

    move v10, v5

    move v11, v6

    move/from16 v18, v7

    move v7, v14

    move v14, v9

    move-object v9, v4

    move-wide v4, v12

    move v13, v8

    move/from16 v12, v18

    move v8, v2

    goto/16 :goto_1
.end method
