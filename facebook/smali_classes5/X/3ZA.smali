.class public LX/3ZA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/3ZA;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 598618
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 598619
    return-void
.end method

.method public static a(LX/0QB;)LX/3ZA;
    .locals 3

    .prologue
    .line 598620
    sget-object v0, LX/3ZA;->a:LX/3ZA;

    if-nez v0, :cond_1

    .line 598621
    const-class v1, LX/3ZA;

    monitor-enter v1

    .line 598622
    :try_start_0
    sget-object v0, LX/3ZA;->a:LX/3ZA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 598623
    if-eqz v2, :cond_0

    .line 598624
    :try_start_1
    new-instance v0, LX/3ZA;

    invoke-direct {v0}, LX/3ZA;-><init>()V

    .line 598625
    move-object v0, v0

    .line 598626
    sput-object v0, LX/3ZA;->a:LX/3ZA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 598627
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 598628
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 598629
    :cond_1
    sget-object v0, LX/3ZA;->a:LX/3ZA;

    return-object v0

    .line 598630
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 598631
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 7

    .prologue
    .line 598632
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/ComposerUnitComponentPartDefinition;->b:LX/1Cz;

    sget-object v1, Lcom/facebook/components/feed/ComponentPartDefinition;->a:LX/1Cz;

    sget-object v2, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;->a:LX/1Cz;

    sget-object v3, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;->a:LX/1Cz;

    sget-object v4, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;->a:LX/1Cz;

    sget-object v5, Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadTextPartDefinition;->a:LX/1Cz;

    sget-object v6, Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;->a:LX/1Cz;

    invoke-static/range {v0 .. v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 598633
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cz;

    .line 598634
    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 598635
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 598636
    :cond_0
    return-void
.end method
