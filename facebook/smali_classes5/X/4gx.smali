.class public final enum LX/4gx;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4gx;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4gx;

.field public static final enum ANDROID_ACCOUNT_RECOVERY:LX/4gx;

.field public static final enum ANDROID_CLIFF_CONFIRMATION:LX/4gx;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 800305
    new-instance v0, LX/4gx;

    const-string v1, "ANDROID_CLIFF_CONFIRMATION"

    const-string v2, "android_cliff_confirmation"

    invoke-direct {v0, v1, v3, v2}, LX/4gx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4gx;->ANDROID_CLIFF_CONFIRMATION:LX/4gx;

    .line 800306
    new-instance v0, LX/4gx;

    const-string v1, "ANDROID_ACCOUNT_RECOVERY"

    const-string v2, "android_account_recovery"

    invoke-direct {v0, v1, v4, v2}, LX/4gx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4gx;->ANDROID_ACCOUNT_RECOVERY:LX/4gx;

    .line 800307
    const/4 v0, 0x2

    new-array v0, v0, [LX/4gx;

    sget-object v1, LX/4gx;->ANDROID_CLIFF_CONFIRMATION:LX/4gx;

    aput-object v1, v0, v3

    sget-object v1, LX/4gx;->ANDROID_ACCOUNT_RECOVERY:LX/4gx;

    aput-object v1, v0, v4

    sput-object v0, LX/4gx;->$VALUES:[LX/4gx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 800308
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 800309
    iput-object p3, p0, LX/4gx;->name:Ljava/lang/String;

    .line 800310
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4gx;
    .locals 1

    .prologue
    .line 800311
    const-class v0, LX/4gx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4gx;

    return-object v0
.end method

.method public static values()[LX/4gx;
    .locals 1

    .prologue
    .line 800312
    sget-object v0, LX/4gx;->$VALUES:[LX/4gx;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4gx;

    return-object v0
.end method
