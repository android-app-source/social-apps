.class public abstract LX/3pF;
.super LX/0gG;
.source ""


# instance fields
.field private final a:LX/0gc;

.field private b:LX/0hH;

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/app/Fragment$SavedState;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/support/v4/app/Fragment;


# direct methods
.method public constructor <init>(LX/0gc;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 641382
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 641383
    iput-object v1, p0, LX/3pF;->b:LX/0hH;

    .line 641384
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3pF;->c:Ljava/util/ArrayList;

    .line 641385
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3pF;->d:Ljava/util/ArrayList;

    .line 641386
    iput-object v1, p0, LX/3pF;->e:Landroid/support/v4/app/Fragment;

    .line 641387
    iput-object p1, p0, LX/3pF;->a:LX/0gc;

    .line 641388
    return-void
.end method


# virtual methods
.method public abstract a(I)Landroid/support/v4/app/Fragment;
.end method

.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 641364
    iget-object v0, p0, LX/3pF;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p2, :cond_0

    .line 641365
    iget-object v0, p0, LX/3pF;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 641366
    if-eqz v0, :cond_0

    .line 641367
    :goto_0
    return-object v0

    .line 641368
    :cond_0
    iget-object v0, p0, LX/3pF;->b:LX/0hH;

    if-nez v0, :cond_1

    .line 641369
    iget-object v0, p0, LX/3pF;->a:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iput-object v0, p0, LX/3pF;->b:LX/0hH;

    .line 641370
    :cond_1
    invoke-virtual {p0, p2}, LX/3pF;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 641371
    iget-object v0, p0, LX/3pF;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p2, :cond_2

    .line 641372
    iget-object v0, p0, LX/3pF;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment$SavedState;

    .line 641373
    if-eqz v0, :cond_2

    .line 641374
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setInitialSavedState(Landroid/support/v4/app/Fragment$SavedState;)V

    .line 641375
    :cond_2
    :goto_1
    iget-object v0, p0, LX/3pF;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, p2, :cond_3

    .line 641376
    iget-object v0, p0, LX/3pF;->d:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 641377
    :cond_3
    invoke-virtual {v1, v3}, Landroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    .line 641378
    invoke-virtual {v1, v3}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 641379
    iget-object v0, p0, LX/3pF;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 641380
    iget-object v0, p0, LX/3pF;->b:LX/0hH;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v2

    invoke-virtual {v0, v2, v1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-object v0, v1

    .line 641381
    goto :goto_0
.end method

.method public a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 641342
    if-eqz p1, :cond_4

    .line 641343
    check-cast p1, Landroid/os/Bundle;

    .line 641344
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 641345
    const-string v0, "states"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v3

    .line 641346
    iget-object v0, p0, LX/3pF;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 641347
    iget-object v0, p0, LX/3pF;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 641348
    if-eqz v3, :cond_0

    move v1, v2

    .line 641349
    :goto_0
    array-length v0, v3

    if-ge v1, v0, :cond_0

    .line 641350
    iget-object v4, p0, LX/3pF;->c:Ljava/util/ArrayList;

    aget-object v0, v3, v1

    check-cast v0, Landroid/support/v4/app/Fragment$SavedState;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 641351
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 641352
    :cond_0
    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 641353
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 641354
    const-string v3, "f"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 641355
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 641356
    iget-object v4, p0, LX/3pF;->a:LX/0gc;

    invoke-virtual {v4, p1, v0}, LX/0gc;->a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    .line 641357
    if-eqz v4, :cond_3

    .line 641358
    :goto_2
    iget-object v0, p0, LX/3pF;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, v3, :cond_2

    .line 641359
    iget-object v0, p0, LX/3pF;->d:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 641360
    :cond_2
    invoke-virtual {v4, v2}, Landroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    .line 641361
    iget-object v0, p0, LX/3pF;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v3, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 641362
    :cond_3
    const-string v3, "FragmentStatePagerAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Bad fragment at key "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 641363
    :cond_4
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 641341
    return-void
.end method

.method public a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 641299
    check-cast p3, Landroid/support/v4/app/Fragment;

    .line 641300
    iget-object v0, p0, LX/3pF;->b:LX/0hH;

    if-nez v0, :cond_0

    .line 641301
    iget-object v0, p0, LX/3pF;->a:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iput-object v0, p0, LX/3pF;->b:LX/0hH;

    .line 641302
    :cond_0
    :goto_0
    iget-object v0, p0, LX/3pF;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, p2, :cond_1

    .line 641303
    iget-object v0, p0, LX/3pF;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 641304
    :cond_1
    iget-object v0, p0, LX/3pF;->c:Ljava/util/ArrayList;

    iget-object v1, p0, LX/3pF;->a:LX/0gc;

    invoke-virtual {v1, p3}, LX/0gc;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/Fragment$SavedState;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 641305
    iget-object v0, p0, LX/3pF;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 641306
    iget-object v0, p0, LX/3pF;->b:LX/0hH;

    invoke-virtual {v0, p3}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 641307
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 641338
    check-cast p2, Landroid/support/v4/app/Fragment;

    .line 641339
    iget-object v0, p2, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 641340
    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 641333
    iget-object v0, p0, LX/3pF;->b:LX/0hH;

    if-eqz v0, :cond_0

    .line 641334
    iget-object v0, p0, LX/3pF;->b:LX/0hH;

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 641335
    const/4 v0, 0x0

    iput-object v0, p0, LX/3pF;->b:LX/0hH;

    .line 641336
    iget-object v0, p0, LX/3pF;->a:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 641337
    :cond_0
    return-void
.end method

.method public b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 641323
    check-cast p3, Landroid/support/v4/app/Fragment;

    .line 641324
    iget-object v0, p0, LX/3pF;->e:Landroid/support/v4/app/Fragment;

    if-eq p3, v0, :cond_2

    .line 641325
    iget-object v0, p0, LX/3pF;->e:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    .line 641326
    iget-object v0, p0, LX/3pF;->e:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    .line 641327
    iget-object v0, p0, LX/3pF;->e:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 641328
    :cond_0
    if-eqz p3, :cond_1

    .line 641329
    invoke-virtual {p3, v2}, Landroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    .line 641330
    invoke-virtual {p3, v2}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 641331
    :cond_1
    iput-object p3, p0, LX/3pF;->e:Landroid/support/v4/app/Fragment;

    .line 641332
    :cond_2
    return-void
.end method

.method public lf_()Landroid/os/Parcelable;
    .locals 5

    .prologue
    .line 641308
    const/4 v0, 0x0

    .line 641309
    iget-object v1, p0, LX/3pF;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 641310
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 641311
    iget-object v1, p0, LX/3pF;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Landroid/support/v4/app/Fragment$SavedState;

    .line 641312
    iget-object v2, p0, LX/3pF;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 641313
    const-string v2, "states"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 641314
    :cond_0
    const/4 v1, 0x0

    move-object v2, v0

    :goto_0
    iget-object v0, p0, LX/3pF;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 641315
    iget-object v0, p0, LX/3pF;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 641316
    if-eqz v0, :cond_2

    .line 641317
    if-nez v2, :cond_1

    .line 641318
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 641319
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "f"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 641320
    iget-object v4, p0, LX/3pF;->a:LX/0gc;

    invoke-virtual {v4, v2, v3, v0}, LX/0gc;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 641321
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 641322
    :cond_3
    return-object v2
.end method
