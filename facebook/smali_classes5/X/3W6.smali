.class public LX/3W6;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/35o;
.implements LX/35q;
.implements LX/3Vw;
.implements LX/3Vx;


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field private final b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final c:Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 589054
    new-instance v0, LX/3W7;

    invoke-direct {v0}, LX/3W7;-><init>()V

    sput-object v0, LX/3W6;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 589042
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 589043
    const v0, 0x7f03139c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 589044
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/3W6;->setOrientation(I)V

    .line 589045
    const v0, 0x7f0d2d50

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;

    iput-object v0, p0, LX/3W6;->c:Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;

    .line 589046
    const v0, 0x7f0d2d4f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/3W6;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 589047
    iget-object v0, p0, LX/3W6;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v1, 0x3ff745d1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 589048
    invoke-virtual {p0}, LX/3W6;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 589049
    new-instance v1, LX/1Uo;

    invoke-direct {v1, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    const v2, 0x7f0a045d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 589050
    iput-object v0, v1, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 589051
    move-object v0, v1

    .line 589052
    iget-object v1, p0, LX/3W6;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 589053
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 589040
    iget-object v0, p0, LX/3W6;->c:Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;

    invoke-virtual {v0}, LX/3Vv;->a()V

    .line 589041
    return-void
.end method

.method public getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;
    .locals 1

    .prologue
    .line 589039
    iget-object v0, p0, LX/3W6;->c:Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v0

    return-object v0
.end method

.method public setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 589037
    iget-object v0, p0, LX/3W6;->c:Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 589038
    return-void
.end method

.method public setContextText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 589035
    iget-object v0, p0, LX/3W6;->c:Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;

    invoke-virtual {v0, p1}, LX/3Vv;->setContextText(Ljava/lang/CharSequence;)V

    .line 589036
    return-void
.end method

.method public setLargeImageAspectRatio(F)V
    .locals 1

    .prologue
    .line 589033
    iget-object v0, p0, LX/3W6;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 589034
    return-void
.end method

.method public setLargeImageController(LX/1aZ;)V
    .locals 2
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 589029
    iget-object v1, p0, LX/3W6;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 589030
    iget-object v0, p0, LX/3W6;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 589031
    return-void

    .line 589032
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setNumberOfStars(I)V
    .locals 1

    .prologue
    .line 589017
    iget-object v0, p0, LX/3W6;->c:Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;

    invoke-virtual {v0, p1}, LX/3Vv;->setNumberOfStars(I)V

    .line 589018
    return-void
.end method

.method public setRating(F)V
    .locals 1

    .prologue
    .line 589027
    iget-object v0, p0, LX/3W6;->c:Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;

    invoke-virtual {v0, p1}, LX/3Vv;->setRating(F)V

    .line 589028
    return-void
.end method

.method public setShowRating(Z)V
    .locals 1

    .prologue
    .line 589025
    iget-object v0, p0, LX/3W6;->c:Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;

    invoke-virtual {v0, p1}, LX/3Vv;->setShowRating(Z)V

    .line 589026
    return-void
.end method

.method public setSideImageController(LX/1aZ;)V
    .locals 1
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 589023
    iget-object v0, p0, LX/3W6;->c:Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;

    invoke-virtual {v0, p1}, LX/3Vv;->setSideImageController(LX/1aZ;)V

    .line 589024
    return-void
.end method

.method public setSubcontextText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 589021
    iget-object v0, p0, LX/3W6;->c:Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;

    invoke-virtual {v0, p1}, LX/3Vv;->setSubcontextText(Ljava/lang/CharSequence;)V

    .line 589022
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 589019
    iget-object v0, p0, LX/3W6;->c:Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;

    invoke-virtual {v0, p1}, LX/3Vv;->setTitle(Ljava/lang/CharSequence;)V

    .line 589020
    return-void
.end method
