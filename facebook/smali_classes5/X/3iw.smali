.class public final LX/3iw;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialoguePinnedStoryQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 630670
    const-class v1, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialoguePinnedStoryQueryModel;

    const v0, 0x542d17d8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "DailyDialoguePinnedStoryQuery"

    const-string v6, "3f9c09b304352b01cb6cc722a5a218dd"

    const-string v7, "viewer"

    const-string v8, "10155255658631729"

    const-string v9, "10155259086671729"

    .line 630671
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 630672
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 630673
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 630674
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 630675
    sparse-switch v0, :sswitch_data_0

    .line 630676
    :goto_0
    return-object p1

    .line 630677
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 630678
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 630679
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 630680
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 630681
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 630682
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 630683
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 630684
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 630685
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 630686
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 630687
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 630688
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 630689
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 630690
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 630691
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 630692
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 630693
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6a24640d -> :sswitch_e
        -0x680de62a -> :sswitch_5
        -0x6326fdb3 -> :sswitch_4
        -0x55d45394 -> :sswitch_9
        -0x4496acc9 -> :sswitch_6
        -0x25a646c8 -> :sswitch_1
        -0x1b87b280 -> :sswitch_3
        -0x14283bca -> :sswitch_c
        -0x12efdeb3 -> :sswitch_7
        -0x2c40f79 -> :sswitch_10
        0x683094a -> :sswitch_d
        0x6be2dc6 -> :sswitch_f
        0x83009af -> :sswitch_a
        0xa1fa812 -> :sswitch_0
        0x214100e0 -> :sswitch_8
        0x44725d44 -> :sswitch_2
        0x73a026b5 -> :sswitch_b
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/16 v3, 0x800

    .line 630694
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 630695
    :goto_1
    return v0

    .line 630696
    :sswitch_0
    const-string v2, "1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "3"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "5"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "7"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v2, "11"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    :sswitch_5
    const-string v2, "14"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    .line 630697
    :pswitch_0
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 630698
    :pswitch_1
    const-string v0, "%s"

    invoke-static {p2, v3, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 630699
    :pswitch_2
    const-string v0, "%s"

    invoke-static {p2, v3, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 630700
    :pswitch_3
    const-string v0, "%s"

    invoke-static {p2, v3, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 630701
    :pswitch_4
    const-string v0, "image/jpeg"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 630702
    :pswitch_5
    const-string v0, "contain-fit"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_0
        0x33 -> :sswitch_1
        0x35 -> :sswitch_2
        0x37 -> :sswitch_3
        0x620 -> :sswitch_4
        0x623 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
