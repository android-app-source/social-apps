.class public LX/4nz;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation


# instance fields
.field public a:D

.field private final b:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 808058
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/4nz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 808059
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 808060
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/4nz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 808061
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 808062
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 808063
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, LX/4nz;->a:D

    .line 808064
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/4nz;->b:Landroid/graphics/Rect;

    .line 808065
    sget-object v0, LX/03r;->ExpandingFixedAspectRatioFrameLayout:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 808066
    const/16 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    float-to-double v2, v1

    iput-wide v2, p0, LX/4nz;->a:D

    .line 808067
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 808068
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 13
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/high16 v8, -0x80000000

    const/high16 v12, 0x40000000    # 2.0f

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 808069
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 808070
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    .line 808071
    iget-object v0, p0, LX/4nz;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, LX/4nz;->b:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v5, v0

    .line 808072
    iget-object v0, p0, LX/4nz;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, LX/4nz;->b:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v6, v0

    .line 808073
    if-nez v3, :cond_0

    if-eqz v4, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    const-string v7, "ExpandingFixedAspectRatioFrameLayout must have a bounded size in at least one direction"

    invoke-static {v0, v7}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 808074
    if-eq v4, v12, :cond_3

    move v0, v2

    :goto_1
    const-string v7, "ExpandingFixedAspectRatioFrameLayout does not support exact height dimensions"

    invoke-static {v0, v7}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 808075
    if-eq v3, v8, :cond_1

    if-ne v3, v12, :cond_7

    .line 808076
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    sub-int v3, v0, v5

    .line 808077
    if-ne v4, v8, :cond_5

    .line 808078
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    sub-int/2addr v0, v6

    .line 808079
    iget-wide v8, p0, LX/4nz;->a:D

    int-to-double v10, v0

    mul-double/2addr v8, v10

    double-to-int v1, v8

    if-le v3, v1, :cond_4

    .line 808080
    iget-wide v2, p0, LX/4nz;->a:D

    int-to-double v8, v0

    mul-double/2addr v2, v8

    double-to-int v1, v2

    :goto_2
    move v3, v1

    .line 808081
    :goto_3
    add-int v1, v3, v5

    invoke-static {v1, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    add-int/2addr v0, v6

    invoke-static {v0, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, v1, v0}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 808082
    return-void

    :cond_2
    move v0, v1

    .line 808083
    goto :goto_0

    :cond_3
    move v0, v1

    .line 808084
    goto :goto_1

    .line 808085
    :cond_4
    int-to-double v0, v3

    iget-wide v8, p0, LX/4nz;->a:D

    div-double/2addr v0, v8

    double-to-int v0, v0

    .line 808086
    goto :goto_3

    .line 808087
    :cond_5
    if-nez v4, :cond_6

    :goto_4
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 808088
    int-to-double v0, v3

    iget-wide v8, p0, LX/4nz;->a:D

    div-double/2addr v0, v8

    double-to-int v0, v0

    move v1, v3

    goto :goto_2

    :cond_6
    move v2, v1

    .line 808089
    goto :goto_4

    .line 808090
    :cond_7
    if-nez v3, :cond_8

    if-ne v4, v8, :cond_8

    :goto_5
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 808091
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    sub-int/2addr v0, v6

    .line 808092
    iget-wide v2, p0, LX/4nz;->a:D

    int-to-double v8, v0

    mul-double/2addr v2, v8

    double-to-int v3, v2

    goto :goto_3

    :cond_8
    move v2, v1

    .line 808093
    goto :goto_5
.end method

.method public setAspectRatio(D)V
    .locals 1

    .prologue
    .line 808094
    iput-wide p1, p0, LX/4nz;->a:D

    .line 808095
    return-void
.end method

.method public setPadding(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 808096
    iget-object v0, p0, LX/4nz;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 808097
    return-void
.end method
