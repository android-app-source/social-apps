.class public LX/4oe;
.super LX/2da;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/0wO;",
        ">",
        "LX/2da;"
    }
.end annotation


# instance fields
.field private b:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 810265
    invoke-direct {p0, p1}, LX/2da;-><init>(Landroid/view/View;)V

    .line 810266
    iput-object p1, p0, LX/4oe;->b:Landroid/view/View;

    .line 810267
    return-void
.end method


# virtual methods
.method public final c(I)Landroid/text/style/ClickableSpan;
    .locals 2

    .prologue
    .line 810256
    invoke-virtual {p0}, LX/4oe;->f()[Landroid/text/style/ClickableSpan;

    move-result-object v0

    .line 810257
    if-eqz v0, :cond_0

    array-length v1, v0

    if-gt v1, p1, :cond_1

    .line 810258
    :cond_0
    const/4 v0, 0x0

    .line 810259
    :goto_0
    return-object v0

    :cond_1
    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public final f()[Landroid/text/style/ClickableSpan;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 810260
    iget-object v0, p0, LX/4oe;->b:Landroid/view/View;

    check-cast v0, LX/0wO;

    invoke-interface {v0}, LX/0wO;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    instance-of v0, v0, Landroid/text/Spanned;

    if-nez v0, :cond_0

    .line 810261
    new-array v0, v3, [Landroid/text/style/ClickableSpan;

    .line 810262
    :goto_0
    return-object v0

    .line 810263
    :cond_0
    iget-object v0, p0, LX/4oe;->b:Landroid/view/View;

    check-cast v0, LX/0wO;

    invoke-interface {v0}, LX/0wO;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spanned;

    .line 810264
    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v1

    const-class v2, Landroid/text/style/ClickableSpan;

    invoke-interface {v0, v3, v1, v2}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/ClickableSpan;

    goto :goto_0
.end method
