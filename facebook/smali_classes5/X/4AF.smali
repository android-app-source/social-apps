.class public abstract LX/4AF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public b:I

.field public c:LX/4AI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4AI",
            "<TK;>;"
        }
    .end annotation
.end field

.field public d:LX/4AI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4AI",
            "<TK;>;"
        }
    .end annotation
.end field

.field public e:I

.field public final synthetic f:LX/4AJ;


# direct methods
.method public constructor <init>(LX/4AJ;)V
    .locals 4

    .prologue
    .line 675742
    iput-object p1, p0, LX/4AF;->f:LX/4AJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 675743
    iget-object v0, p0, LX/4AF;->f:LX/4AJ;

    iget-object v0, v0, LX/4AJ;->d:LX/4AI;

    iput-object v0, p0, LX/4AF;->c:LX/4AI;

    .line 675744
    iget-object v0, p0, LX/4AF;->f:LX/4AJ;

    iget v0, v0, LX/4AJ;->f:I

    iput v0, p0, LX/4AF;->e:I

    .line 675745
    iget-object v0, p0, LX/4AF;->c:LX/4AI;

    if-nez v0, :cond_1

    .line 675746
    iget-object v1, p1, LX/4AJ;->c:[LX/4AI;

    .line 675747
    const/4 v0, 0x0

    .line 675748
    :goto_0
    if-nez v0, :cond_0

    iget v2, p0, LX/4AF;->b:I

    array-length v3, v1

    if-ge v2, v3, :cond_0

    .line 675749
    iget v0, p0, LX/4AF;->b:I

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, LX/4AF;->b:I

    aget-object v0, v1, v0

    goto :goto_0

    .line 675750
    :cond_0
    iput-object v0, p0, LX/4AF;->c:LX/4AI;

    .line 675751
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()LX/4AI;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4AI",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 675726
    iget-object v0, p0, LX/4AF;->f:LX/4AJ;

    iget v0, v0, LX/4AJ;->f:I

    iget v1, p0, LX/4AF;->e:I

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 675727
    :cond_0
    iget-object v0, p0, LX/4AF;->c:LX/4AI;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 675728
    :cond_1
    iget-object v1, p0, LX/4AF;->c:LX/4AI;

    .line 675729
    iget-object v0, p0, LX/4AF;->f:LX/4AJ;

    iget-object v2, v0, LX/4AJ;->c:[LX/4AI;

    .line 675730
    iget-object v0, v1, LX/4AI;->f:LX/4AI;

    .line 675731
    :goto_0
    if-nez v0, :cond_2

    iget v3, p0, LX/4AF;->b:I

    array-length v4, v2

    if-ge v3, v4, :cond_2

    .line 675732
    iget v0, p0, LX/4AF;->b:I

    add-int/lit8 v3, v0, 0x1

    iput v3, p0, LX/4AF;->b:I

    aget-object v0, v2, v0

    goto :goto_0

    .line 675733
    :cond_2
    iput-object v0, p0, LX/4AF;->c:LX/4AI;

    .line 675734
    iput-object v1, p0, LX/4AF;->d:LX/4AI;

    return-object v1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 675736
    iget-object v0, p0, LX/4AF;->d:LX/4AI;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 675737
    :cond_0
    iget-object v0, p0, LX/4AF;->f:LX/4AJ;

    iget v0, v0, LX/4AJ;->f:I

    iget v1, p0, LX/4AF;->e:I

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 675738
    :cond_1
    iget-object v0, p0, LX/4AF;->f:LX/4AJ;

    iget-object v1, p0, LX/4AF;->d:LX/4AI;

    iget-object v1, v1, LX/4AI;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LX/4AB;->c(Ljava/lang/Object;)LX/1vs;

    .line 675739
    const/4 v0, 0x0

    iput-object v0, p0, LX/4AF;->d:LX/4AI;

    .line 675740
    iget-object v0, p0, LX/4AF;->f:LX/4AJ;

    iget v0, v0, LX/4AJ;->f:I

    iput v0, p0, LX/4AF;->e:I

    .line 675741
    return-void
.end method

.method public final hasNext()Z
    .locals 1

    .prologue
    .line 675735
    iget-object v0, p0, LX/4AF;->c:LX/4AI;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
