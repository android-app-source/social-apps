.class public LX/3ym;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "LX/0Px",
        "<",
        "Ljava/lang/String;",
        ">;",
        "LX/0Px",
        "<",
        "Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationEdgeModel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 661711
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 661712
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 661717
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 661718
    const-class v0, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ViewerConfigsModel$ConfigsModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ViewerConfigsModel$ConfigsModel;

    .line 661719
    if-nez v0, :cond_0

    .line 661720
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 661721
    :goto_0
    return-object v0

    .line 661722
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ViewerConfigsModel$ConfigsModel;->a()LX/0Px;

    move-result-object v2

    .line 661723
    const/4 v0, 0x0

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    :goto_1
    if-ge v0, v3, :cond_1

    .line 661724
    invoke-virtual {v2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 661725
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 661726
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 661716
    const/4 v0, 0x2

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 2

    .prologue
    .line 661713
    check-cast p1, LX/0Px;

    .line 661714
    new-instance v0, LX/3yd;

    invoke-direct {v0}, LX/3yd;-><init>()V

    move-object v0, v0

    .line 661715
    const-string v1, "config_names"

    invoke-virtual {v0, v1, p1}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v0

    return-object v0
.end method
