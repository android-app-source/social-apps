.class public LX/43J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/43C;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/2Qx;

.field private final c:Lcom/facebook/bitmaps/NativeImageProcessor;

.field private final d:LX/43H;

.field private final e:LX/11i;

.field private final f:LX/435;

.field private g:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/11o",
            "<",
            "LX/43R;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2Qx;Lcom/facebook/bitmaps/NativeImageProcessor;LX/43H;LX/11i;LX/435;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 668716
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 668717
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/43J;->g:LX/0am;

    .line 668718
    iput-object p1, p0, LX/43J;->a:Landroid/content/Context;

    .line 668719
    iput-object p2, p0, LX/43J;->b:LX/2Qx;

    .line 668720
    iput-object p3, p0, LX/43J;->c:Lcom/facebook/bitmaps/NativeImageProcessor;

    .line 668721
    iput-object p4, p0, LX/43J;->d:LX/43H;

    .line 668722
    iput-object p5, p0, LX/43J;->e:LX/11i;

    .line 668723
    iput-object p6, p0, LX/43J;->f:LX/435;

    .line 668724
    return-void
.end method

.method private a(LX/43Q;)V
    .locals 5

    .prologue
    .line 668725
    iget-object v0, p0, LX/43J;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 668726
    iget-object v0, p0, LX/43J;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11o;

    const-string v1, "GetThumbnail"

    const/4 v2, 0x0

    const-string v3, "GenerateThumbnailMethod"

    invoke-virtual {p1}, LX/43Q;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v3

    const v4, -0x448b107a

    invoke-static {v0, v1, v2, v3, v4}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 668727
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;IILX/43G;Z)LX/43G;
    .locals 15

    .prologue
    .line 668728
    new-instance v1, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 668729
    new-instance v1, Lcom/facebook/bitmaps/ImageResizer$ImageResizingInputFileException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "N/missing file: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/facebook/bitmaps/ImageResizer$ImageResizingInputFileException;-><init>(Ljava/lang/String;Z)V

    throw v1

    .line 668730
    :cond_0
    if-lez p3, :cond_1

    if-lez p4, :cond_1

    invoke-virtual/range {p5 .. p5}, LX/43G;->a()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual/range {p5 .. p5}, LX/43G;->b()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual/range {p5 .. p5}, LX/43G;->c()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual/range {p5 .. p5}, LX/43G;->c()I

    move-result v1

    const/16 v2, 0x64

    if-le v1, v2, :cond_2

    .line 668731
    :cond_1
    new-instance v1, Lcom/facebook/bitmaps/ImageResizer$ImageResizingBadParamException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "N/input dims: ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "], target dims: ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p5 .. p5}, LX/43G;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p5 .. p5}, LX/43G;->b()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "], q: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p5 .. p5}, LX/43G;->c()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", path: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/bitmaps/ImageResizer$ImageResizingBadParamException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 668732
    :cond_2
    const-string v2, "N/scaleImageAndWriteToFile"

    .line 668733
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 668734
    :try_start_0
    invoke-static/range {p1 .. p1}, LX/1la;->a(Ljava/lang/String;)LX/1lW;

    move-result-object v14

    .line 668735
    sget-object v1, LX/1ld;->a:LX/1lW;

    if-eq v14, v1, :cond_4

    .line 668736
    const-string v8, "J/scaleJpegFile"
    :try_end_0
    .catch LX/42y; {:try_start_0 .. :try_end_0} :catch_1
    .catch LX/42w; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 668737
    :try_start_1
    iget-object v1, p0, LX/43J;->b:LX/2Qx;

    iget-object v2, p0, LX/43J;->a:Landroid/content/Context;

    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p5

    iget v5, v0, LX/43G;->a:I

    move-object/from16 v0, p5

    iget v6, v0, LX/43G;->b:I

    move-object/from16 v0, p5

    iget v7, v0, LX/43G;->c:I

    invoke-virtual/range {v1 .. v7}, LX/2Qx;->a(Landroid/content/Context;Ljava/io/File;Ljava/io/File;III)Z
    :try_end_1
    .catch LX/42y; {:try_start_1 .. :try_end_1} :catch_5
    .catch LX/42w; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v2, v8

    .line 668738
    :goto_0
    const/4 v4, 0x0

    .line 668739
    :try_start_2
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 668740
    const/4 v3, 0x1

    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 668741
    move-object/from16 v0, p2

    invoke-static {v0, v1}, LX/436;->a(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 668742
    sget-object v3, LX/1ld;->a:LX/1lW;

    if-ne v14, v3, :cond_3

    .line 668743
    invoke-static/range {p1 .. p2}, LX/2Qx;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 668744
    :cond_3
    new-instance v3, LX/43G;

    iget v5, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v1, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    move-object/from16 v0, p5

    iget v6, v0, LX/43G;->c:I

    invoke-direct {v3, v5, v1, v6}, LX/43G;-><init>(III)V

    .line 668745
    return-object v3

    .line 668746
    :cond_4
    const-string v2, "N/transcodeJpeg"

    .line 668747
    iget-object v5, p0, LX/43J;->c:Lcom/facebook/bitmaps/NativeImageProcessor;

    const/4 v7, 0x0

    move-object/from16 v0, p5

    iget v8, v0, LX/43G;->a:I

    move-object/from16 v0, p5

    iget v9, v0, LX/43G;->b:I

    move-object/from16 v0, p5

    iget-boolean v1, v0, LX/43G;->d:Z

    if-eqz v1, :cond_6

    sget-object v10, LX/437;->EXACT:LX/437;

    :goto_1
    sget-object v11, LX/43I;->NO_ROTATION:LX/43I;

    move-object/from16 v0, p5

    iget v12, v0, LX/43G;->c:I

    move-object/from16 v6, p1

    move/from16 v13, p6

    invoke-virtual/range {v5 .. v13}, Lcom/facebook/bitmaps/NativeImageProcessor;->a(Ljava/lang/String;IIILX/437;LX/43I;IZ)[B
    :try_end_2
    .catch LX/42y; {:try_start_2 .. :try_end_2} :catch_1
    .catch LX/42w; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    .line 668748
    :try_start_3
    invoke-static {v1, v4}, LX/1t3;->a([BLjava/io/File;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch LX/42y; {:try_start_3 .. :try_end_3} :catch_1
    .catch LX/42w; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 668749
    :catch_0
    move-exception v1

    .line 668750
    :try_start_4
    new-instance v3, Lcom/facebook/bitmaps/ImageResizer$ImageResizingOutputFileException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "N/writing "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v3, v5, v1, v6}, Lcom/facebook/bitmaps/ImageResizer$ImageResizingOutputFileException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Z)V

    throw v3
    :try_end_4
    .catch LX/42y; {:try_start_4 .. :try_end_4} :catch_1
    .catch LX/42w; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 668751
    :catch_1
    move-exception v1

    .line 668752
    :goto_2
    :try_start_5
    new-instance v3, LX/439;

    invoke-direct {v3, v2, v1}, LX/439;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 668753
    :catchall_0
    move-exception v1

    if-eqz v4, :cond_5

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 668754
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    :cond_5
    throw v1

    .line 668755
    :cond_6
    :try_start_6
    sget-object v10, LX/437;->BEST_EFFORT_BOUND_FROM_BELOW:LX/437;

    goto :goto_1

    .line 668756
    :catch_2
    move-exception v1

    .line 668757
    new-instance v3, Lcom/facebook/bitmaps/ImageResizer$ImageResizingOutputFileException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "N/writing "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-direct {v3, v5, v1, v6}, Lcom/facebook/bitmaps/ImageResizer$ImageResizingOutputFileException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Z)V

    throw v3
    :try_end_6
    .catch LX/42y; {:try_start_6 .. :try_end_6} :catch_1
    .catch LX/42w; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 668758
    :catch_3
    move-exception v1

    .line 668759
    :goto_3
    :try_start_7
    new-instance v3, Lcom/facebook/bitmaps/ImageResizer$ImageResizingException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    invoke-direct {v3, v2, v1, v5}, Lcom/facebook/bitmaps/ImageResizer$ImageResizingException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Z)V

    throw v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 668760
    :catch_4
    move-exception v1

    move-object v2, v8

    goto :goto_3

    .line 668761
    :catch_5
    move-exception v1

    move-object v2, v8

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/43G;)LX/43G;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 668762
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 668763
    iput-boolean v6, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 668764
    invoke-static {p1, v0}, LX/436;->a(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 668765
    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v6}, LX/43J;->a(Ljava/lang/String;Ljava/lang/String;IILX/43G;Z)LX/43G;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;IIIZ)Landroid/graphics/Bitmap;
    .locals 9

    .prologue
    .line 668766
    invoke-static {p1}, LX/1la;->a(Ljava/lang/String;)LX/1lW;

    move-result-object v0

    .line 668767
    sget-object v1, LX/1ld;->a:LX/1lW;

    if-eq v0, v1, :cond_0

    .line 668768
    :try_start_0
    iget-object v0, p0, LX/43J;->b:LX/2Qx;

    iget-object v1, p0, LX/43J;->a:Landroid/content/Context;

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, LX/2Qx;->a(Landroid/content/Context;Landroid/net/Uri;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch LX/42y; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/430; {:try_start_0 .. :try_end_0} :catch_1
    .catch LX/42x; {:try_start_0 .. :try_end_0} :catch_2
    .catch LX/42z; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    .line 668769
    :goto_0
    return-object v0

    .line 668770
    :catch_0
    move-exception v0

    .line 668771
    new-instance v1, LX/439;

    const-string v2, "N/scaleImage"

    invoke-direct {v1, v2, v0}, LX/439;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 668772
    :catch_1
    move-exception v0

    .line 668773
    new-instance v1, LX/43B;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "N/scaleImage "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/43B;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 668774
    :catch_2
    move-exception v0

    .line 668775
    new-instance v1, LX/438;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "N/scaleImage "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/438;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 668776
    :catch_3
    move-exception v0

    .line 668777
    new-instance v1, LX/43A;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "N/scaleImage "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/43A;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 668778
    :cond_0
    iget-object v0, p0, LX/43J;->c:Lcom/facebook/bitmaps/NativeImageProcessor;

    sget-object v5, LX/437;->BEST_EFFORT_BOUND_FROM_BELOW:LX/437;

    if-eqz p5, :cond_1

    sget-object v6, LX/43I;->ROTATE_TO_0:LX/43I;

    :goto_1
    const/16 v7, 0x55

    const/4 v8, 0x1

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v8}, Lcom/facebook/bitmaps/NativeImageProcessor;->a(Ljava/lang/String;IIILX/437;LX/43I;IZ)[B

    move-result-object v0

    .line 668779
    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {v0, v1, v2}, LX/436;->a([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 668780
    :cond_1
    sget-object v6, LX/43I;->NO_ROTATION:LX/43I;

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;LX/431;)Landroid/graphics/Bitmap;
    .locals 9

    .prologue
    .line 668781
    iget-object v0, p0, LX/43J;->e:LX/11i;

    sget-object v1, LX/43S;->a:LX/43R;

    invoke-interface {v0, v1, p1}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/43J;->g:LX/0am;

    .line 668782
    iget-object v0, p0, LX/43J;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 668783
    iget-object v0, p0, LX/43J;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11o;

    const-string v1, "GetThumbnail"

    const v2, -0xede1b7d

    invoke-static {v0, v1, v2}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 668784
    :cond_0
    invoke-static {p1}, LX/1la;->a(Ljava/lang/String;)LX/1lW;

    move-result-object v0

    .line 668785
    sget-object v1, LX/1ld;->a:LX/1lW;

    if-ne v0, v1, :cond_3

    .line 668786
    iget-boolean v0, p2, LX/431;->d:Z

    move v0, v0

    .line 668787
    if-eqz v0, :cond_2

    .line 668788
    iget-object v0, p0, LX/43J;->c:Lcom/facebook/bitmaps/NativeImageProcessor;

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 668789
    :try_start_0
    new-instance v2, Landroid/media/ExifInterface;

    invoke-direct {v2, p1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 668790
    invoke-virtual {v2}, Landroid/media/ExifInterface;->hasThumbnail()Z

    move-result v3

    if-nez v3, :cond_4

    .line 668791
    :cond_1
    :goto_0
    move-object v0, v1

    .line 668792
    if-eqz v0, :cond_2

    .line 668793
    sget-object v1, LX/43Q;->EXIF:LX/43Q;

    invoke-direct {p0, v1}, LX/43J;->a(LX/43Q;)V

    .line 668794
    :goto_1
    return-object v0

    .line 668795
    :cond_2
    iget-object v0, p0, LX/43J;->c:Lcom/facebook/bitmaps/NativeImageProcessor;

    iget v2, p2, LX/431;->a:I

    iget v3, p2, LX/431;->b:I

    iget v4, p2, LX/431;->c:I

    sget-object v5, LX/437;->FOR_CENTER_CROP:LX/437;

    sget-object v6, LX/43I;->ROTATE_TO_0:LX/43I;

    const/16 v7, 0x55

    const/4 v8, 0x1

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, Lcom/facebook/bitmaps/NativeImageProcessor;->a(Ljava/lang/String;IIILX/437;LX/43I;IZ)[B

    move-result-object v0

    .line 668796
    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {v0, v1, v2}, LX/436;->a([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 668797
    sget-object v1, LX/43Q;->NATIVE_RESIZER:LX/43Q;

    invoke-direct {p0, v1}, LX/43J;->a(LX/43Q;)V

    goto :goto_1

    .line 668798
    :cond_3
    iget-object v0, p0, LX/43J;->d:LX/43H;

    invoke-virtual {v0, p1, p2}, LX/43H;->a(Ljava/lang/String;LX/431;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 668799
    sget-object v1, LX/43Q;->JAVA_RESIZER:LX/43Q;

    invoke-direct {p0, v1}, LX/43J;->a(LX/43Q;)V

    goto :goto_1

    .line 668800
    :catch_0
    move-exception v2

    .line 668801
    sget-object v3, LX/435;->a:Ljava/lang/Class;

    const-string v4, "Error getting the exif"

    invoke-static {v3, v4, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 668802
    :cond_4
    invoke-virtual {v2}, Landroid/media/ExifInterface;->getThumbnail()[B

    move-result-object v2

    .line 668803
    if-eqz v2, :cond_1

    .line 668804
    iget v3, p2, LX/431;->a:I

    if-nez v3, :cond_5

    .line 668805
    array-length v1, v2

    invoke-static {v2, v4, v1}, LX/436;->a([BII)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0

    .line 668806
    :cond_5
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 668807
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 668808
    :try_start_1
    iget v4, p2, LX/431;->a:I

    invoke-virtual {v0, v3, v2, v4}, Lcom/facebook/bitmaps/NativeImageProcessor;->a(Ljava/io/InputStream;Ljava/io/OutputStream;I)V

    .line 668809
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    .line 668810
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 668811
    const/4 v2, 0x0

    array-length v4, v3

    invoke-static {v3, v2, v4}, LX/436;->a([BII)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    goto :goto_0

    .line 668812
    :catch_1
    move-exception v2

    .line 668813
    sget-object v3, LX/435;->a:Ljava/lang/Class;

    const-string v4, "Error resizing the exif thumbnail: "

    invoke-static {v3, v4, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 668814
    iget-object v0, p0, LX/43J;->b:LX/2Qx;

    .line 668815
    iput-boolean p1, v0, LX/2Qx;->a:Z

    .line 668816
    iget-object v0, p0, LX/43J;->d:LX/43H;

    invoke-virtual {v0, p1}, LX/43H;->a(Z)V

    .line 668817
    return-void
.end method
