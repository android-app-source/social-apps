.class public LX/3om;
.super LX/3oi;
.source ""


# instance fields
.field public final a:Landroid/animation/ValueAnimator;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 640981
    invoke-direct {p0}, LX/3oi;-><init>()V

    .line 640982
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, LX/3om;->a:Landroid/animation/ValueAnimator;

    .line 640983
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 640979
    iget-object v0, p0, LX/3om;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 640980
    return-void
.end method

.method public final a(FF)V
    .locals 3

    .prologue
    .line 640977
    iget-object v0, p0, LX/3om;->a:Landroid/animation/ValueAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 640978
    return-void
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 640975
    iget-object v0, p0, LX/3om;->a:Landroid/animation/ValueAnimator;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 640976
    return-void
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 640973
    iget-object v0, p0, LX/3om;->a:Landroid/animation/ValueAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 640974
    return-void
.end method

.method public final a(LX/3og;)V
    .locals 2

    .prologue
    .line 640984
    iget-object v0, p0, LX/3om;->a:Landroid/animation/ValueAnimator;

    new-instance v1, LX/3ol;

    invoke-direct {v1, p0, p1}, LX/3ol;-><init>(LX/3om;LX/3og;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 640985
    return-void
.end method

.method public final a(Landroid/view/animation/Interpolator;)V
    .locals 1

    .prologue
    .line 640971
    iget-object v0, p0, LX/3om;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 640972
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 640970
    iget-object v0, p0, LX/3om;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 640969
    iget-object v0, p0, LX/3om;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final d()F
    .locals 1

    .prologue
    .line 640966
    iget-object v0, p0, LX/3om;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 640967
    iget-object v0, p0, LX/3om;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 640968
    return-void
.end method
