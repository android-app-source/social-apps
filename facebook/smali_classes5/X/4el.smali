.class public LX/4el;
.super LX/04L;
.source ""


# static fields
.field private static b:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 797436
    invoke-direct {p0}, LX/04L;-><init>()V

    return-void
.end method

.method private a(LX/1FJ;I[BLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;I[B",
            "Landroid/graphics/BitmapFactory$Options;",
            ")",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 797466
    :try_start_0
    invoke-static {p1, p2, p3}, LX/4el;->a(LX/1FJ;I[B)Landroid/os/MemoryFile;

    move-result-object v1

    .line 797467
    invoke-direct {p0, v1}, LX/4el;->a(Landroid/os/MemoryFile;)Ljava/io/FileDescriptor;

    move-result-object v0

    .line 797468
    sget-object v2, LX/1cG;->d:Lcom/facebook/webpsupport/WebpBitmapFactoryImpl;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, p4}, Lcom/facebook/webpsupport/WebpBitmapFactoryImpl;->a(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 797469
    const-string v2, "BitmapFactory returned null"

    invoke-static {v0, v2}, LX/03g;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 797470
    if-eqz v1, :cond_0

    .line 797471
    invoke-virtual {v1}, Landroid/os/MemoryFile;->close()V

    :cond_0
    return-object v0

    .line 797472
    :catch_0
    move-exception v0

    .line 797473
    :try_start_1
    invoke-static {v0}, LX/0V9;->b(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 797474
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 797475
    invoke-virtual {v1}, Landroid/os/MemoryFile;->close()V

    :cond_1
    throw v0
.end method

.method private static a(LX/1FJ;I[B)Landroid/os/MemoryFile;
    .locals 7
    .param p2    # [B
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;I[B)",
            "Landroid/os/MemoryFile;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 797447
    if-nez p2, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, p1

    .line 797448
    new-instance v5, Landroid/os/MemoryFile;

    invoke-direct {v5, v2, v0}, Landroid/os/MemoryFile;-><init>(Ljava/lang/String;I)V

    .line 797449
    invoke-virtual {v5, v1}, Landroid/os/MemoryFile;->allowPurging(Z)Z

    .line 797450
    :try_start_0
    new-instance v4, LX/1lZ;

    invoke-virtual {p0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FK;

    invoke-direct {v4, v0}, LX/1lZ;-><init>(LX/1FK;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 797451
    :try_start_1
    new-instance v3, LX/46c;

    invoke-direct {v3, v4, p1}, LX/46c;-><init>(Ljava/io/InputStream;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 797452
    :try_start_2
    invoke-virtual {v5}, Landroid/os/MemoryFile;->getOutputStream()Ljava/io/OutputStream;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v1

    .line 797453
    :try_start_3
    invoke-static {v3, v1}, LX/0aP;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 797454
    if-eqz p2, :cond_0

    .line 797455
    const/4 v0, 0x0

    array-length v2, p2

    invoke-virtual {v5, p2, v0, p1, v2}, Landroid/os/MemoryFile;->writeBytes([BIII)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 797456
    :cond_0
    invoke-static {p0}, LX/1FJ;->c(LX/1FJ;)V

    .line 797457
    invoke-static {v4}, LX/1vz;->a(Ljava/io/InputStream;)V

    .line 797458
    invoke-static {v3}, LX/1vz;->a(Ljava/io/InputStream;)V

    .line 797459
    invoke-static {v1, v6}, LX/1vz;->a(Ljava/io/Closeable;Z)V

    return-object v5

    .line 797460
    :cond_1
    array-length v0, p2

    goto :goto_0

    .line 797461
    :catchall_0
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    :goto_1
    invoke-static {p0}, LX/1FJ;->c(LX/1FJ;)V

    .line 797462
    invoke-static {v3}, LX/1vz;->a(Ljava/io/InputStream;)V

    .line 797463
    invoke-static {v2}, LX/1vz;->a(Ljava/io/InputStream;)V

    .line 797464
    invoke-static {v1, v6}, LX/1vz;->a(Ljava/io/Closeable;Z)V

    throw v0

    .line 797465
    :catchall_1
    move-exception v0

    move-object v1, v2

    move-object v3, v4

    goto :goto_1

    :catchall_2
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    move-object v3, v4

    goto :goto_1

    :catchall_3
    move-exception v0

    move-object v2, v3

    move-object v3, v4

    goto :goto_1
.end method

.method private a(Landroid/os/MemoryFile;)Ljava/io/FileDescriptor;
    .locals 2

    .prologue
    .line 797476
    :try_start_0
    invoke-static {p0}, LX/4el;->a(LX/4el;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 797477
    check-cast v0, Ljava/io/FileDescriptor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 797478
    :catch_0
    move-exception v0

    .line 797479
    invoke-static {v0}, LX/0V9;->b(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method private static declared-synchronized a(LX/4el;)Ljava/lang/reflect/Method;
    .locals 3

    .prologue
    .line 797441
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/4el;->b:Ljava/lang/reflect/Method;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 797442
    :try_start_1
    const-class v0, Landroid/os/MemoryFile;

    const-string v1, "getFileDescriptor"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, LX/4el;->b:Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 797443
    :cond_0
    :try_start_2
    sget-object v0, LX/4el;->b:Ljava/lang/reflect/Method;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    .line 797444
    :catch_0
    move-exception v0

    .line 797445
    :try_start_3
    invoke-static {v0}, LX/0V9;->b(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 797446
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(LX/1FJ;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;I",
            "Landroid/graphics/BitmapFactory$Options;",
            ")",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 797438
    invoke-static {p1, p2}, LX/04L;->a(LX/1FJ;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 797439
    :goto_0
    invoke-direct {p0, p1, p2, v0, p3}, LX/4el;->a(LX/1FJ;I[BLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    .line 797440
    :cond_0
    sget-object v0, LX/04L;->a:[B

    goto :goto_0
.end method

.method public final a(LX/1FJ;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;",
            "Landroid/graphics/BitmapFactory$Options;",
            ")",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 797437
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FK;

    invoke-virtual {v0}, LX/1FK;->a()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, LX/4el;->a(LX/1FJ;I[BLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
