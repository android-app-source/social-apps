.class public LX/3YF;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field public final b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field public final c:Landroid/widget/ImageView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 595711
    new-instance v0, LX/3YG;

    invoke-direct {v0}, LX/3YG;-><init>()V

    sput-object v0, LX/3YF;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 595712
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 595713
    const v0, 0x7f030950

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 595714
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/3YF;->setOrientation(I)V

    .line 595715
    const v0, 0x7f0d15bc

    invoke-virtual {p0, v0}, LX/3YF;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, LX/3YF;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 595716
    iget-object v0, p0, LX/3YF;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 595717
    const v0, 0x7f0d0503

    invoke-virtual {p0, v0}, LX/3YF;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/3YF;->c:Landroid/widget/ImageView;

    .line 595718
    return-void
.end method


# virtual methods
.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 595719
    iget-object v0, p0, LX/3YF;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 595720
    return-void
.end method
