.class public LX/45I;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/text/BreakIterator;

.field public final b:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 670265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 670266
    if-nez p1, :cond_0

    .line 670267
    const-string p1, ""

    .line 670268
    :cond_0
    iput-object p1, p0, LX/45I;->b:Ljava/lang/CharSequence;

    .line 670269
    invoke-static {}, Ljava/text/BreakIterator;->getWordInstance()Ljava/text/BreakIterator;

    move-result-object v0

    iput-object v0, p0, LX/45I;->a:Ljava/text/BreakIterator;

    .line 670270
    iget-object v0, p0, LX/45I;->a:Ljava/text/BreakIterator;

    iget-object v1, p0, LX/45I;->b:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/BreakIterator;->setText(Ljava/lang/String;)V

    .line 670271
    iget-object v0, p0, LX/45I;->a:Ljava/text/BreakIterator;

    invoke-virtual {v0}, Ljava/text/BreakIterator;->last()I

    .line 670272
    return-void
.end method

.method public static a(LX/45I;I)Z
    .locals 1

    .prologue
    .line 670273
    iget-object v0, p0, LX/45I;->b:Ljava/lang/CharSequence;

    invoke-interface {v0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
