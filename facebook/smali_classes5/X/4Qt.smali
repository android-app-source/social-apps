.class public LX/4Qt;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 706713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 66

    .prologue
    .line 706714
    const/16 v55, 0x0

    .line 706715
    const-wide/16 v56, 0x0

    .line 706716
    const/16 v54, 0x0

    .line 706717
    const/16 v53, 0x0

    .line 706718
    const/16 v52, 0x0

    .line 706719
    const/16 v51, 0x0

    .line 706720
    const/16 v50, 0x0

    .line 706721
    const-wide/16 v48, 0x0

    .line 706722
    const/16 v47, 0x0

    .line 706723
    const/16 v46, 0x0

    .line 706724
    const-wide/16 v44, 0x0

    .line 706725
    const/16 v43, 0x0

    .line 706726
    const/16 v42, 0x0

    .line 706727
    const/16 v41, 0x0

    .line 706728
    const/16 v40, 0x0

    .line 706729
    const-wide/16 v38, 0x0

    .line 706730
    const/16 v35, 0x0

    .line 706731
    const-wide/16 v36, 0x0

    .line 706732
    const/16 v34, 0x0

    .line 706733
    const-wide/16 v32, 0x0

    .line 706734
    const/16 v31, 0x0

    .line 706735
    const/16 v30, 0x0

    .line 706736
    const-wide/16 v28, 0x0

    .line 706737
    const-wide/16 v26, 0x0

    .line 706738
    const/16 v19, 0x0

    .line 706739
    const-wide/16 v24, 0x0

    .line 706740
    const-wide/16 v22, 0x0

    .line 706741
    const-wide/16 v20, 0x0

    .line 706742
    const/16 v18, 0x0

    .line 706743
    const/16 v17, 0x0

    .line 706744
    const/16 v16, 0x0

    .line 706745
    const/4 v15, 0x0

    .line 706746
    const/4 v14, 0x0

    .line 706747
    const/4 v13, 0x0

    .line 706748
    const/4 v12, 0x0

    .line 706749
    const/4 v11, 0x0

    .line 706750
    const/4 v10, 0x0

    .line 706751
    const/4 v9, 0x0

    .line 706752
    const/4 v8, 0x0

    .line 706753
    const/4 v7, 0x0

    .line 706754
    const/4 v6, 0x0

    .line 706755
    const/4 v5, 0x0

    .line 706756
    const/4 v4, 0x0

    .line 706757
    const/4 v3, 0x0

    .line 706758
    const/4 v2, 0x0

    .line 706759
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v58

    sget-object v59, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v58

    move-object/from16 v1, v59

    if-eq v0, v1, :cond_2f

    .line 706760
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 706761
    const/4 v2, 0x0

    .line 706762
    :goto_0
    return v2

    .line 706763
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_1f

    .line 706764
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 706765
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 706766
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 706767
    const-string v6, "animation_assets"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 706768
    invoke-static/range {p0 .. p1}, LX/4Qu;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v60, v2

    goto :goto_1

    .line 706769
    :cond_1
    const-string v6, "attraction_force_strength"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 706770
    const/4 v2, 0x1

    .line 706771
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 706772
    :cond_2
    const-string v6, "emitter_assets"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 706773
    invoke-static/range {p0 .. p1}, LX/4Qv;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v59, v2

    goto :goto_1

    .line 706774
    :cond_3
    const-string v6, "extra_config"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 706775
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v58, v2

    goto :goto_1

    .line 706776
    :cond_4
    const-string v6, "gravity"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 706777
    invoke-static/range {p0 .. p1}, LX/4Qx;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v57, v2

    goto :goto_1

    .line 706778
    :cond_5
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 706779
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v56, v2

    goto :goto_1

    .line 706780
    :cond_6
    const-string v6, "init_max_position"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 706781
    invoke-static/range {p0 .. p1}, LX/4Qx;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v53, v2

    goto/16 :goto_1

    .line 706782
    :cond_7
    const-string v6, "init_max_rotation"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 706783
    const/4 v2, 0x1

    .line 706784
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move/from16 v21, v2

    move-wide/from16 v54, v6

    goto/16 :goto_1

    .line 706785
    :cond_8
    const-string v6, "init_max_velocity"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 706786
    invoke-static/range {p0 .. p1}, LX/4Qx;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v52, v2

    goto/16 :goto_1

    .line 706787
    :cond_9
    const-string v6, "init_min_position"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 706788
    invoke-static/range {p0 .. p1}, LX/4Qx;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v49, v2

    goto/16 :goto_1

    .line 706789
    :cond_a
    const-string v6, "init_min_rotation"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 706790
    const/4 v2, 0x1

    .line 706791
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move/from16 v20, v2

    move-wide/from16 v50, v6

    goto/16 :goto_1

    .line 706792
    :cond_b
    const-string v6, "init_min_velocity"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 706793
    invoke-static/range {p0 .. p1}, LX/4Qx;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v48, v2

    goto/16 :goto_1

    .line 706794
    :cond_c
    const-string v6, "init_particles"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 706795
    const/4 v2, 0x1

    .line 706796
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move/from16 v19, v2

    move/from16 v47, v6

    goto/16 :goto_1

    .line 706797
    :cond_d
    const-string v6, "max_color_hsva"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 706798
    invoke-static/range {p0 .. p1}, LX/4Qr;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v46, v2

    goto/16 :goto_1

    .line 706799
    :cond_e
    const-string v6, "max_lifetime_millis"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 706800
    const/4 v2, 0x1

    .line 706801
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move/from16 v18, v2

    move/from16 v43, v6

    goto/16 :goto_1

    .line 706802
    :cond_f
    const-string v6, "max_linear_dampening"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 706803
    const/4 v2, 0x1

    .line 706804
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move/from16 v17, v2

    move-wide/from16 v44, v6

    goto/16 :goto_1

    .line 706805
    :cond_10
    const-string v6, "max_particles"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 706806
    const/4 v2, 0x1

    .line 706807
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move/from16 v16, v2

    move/from16 v42, v6

    goto/16 :goto_1

    .line 706808
    :cond_11
    const-string v6, "max_pixel_size"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 706809
    const/4 v2, 0x1

    .line 706810
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v15, v2

    move-wide/from16 v40, v6

    goto/16 :goto_1

    .line 706811
    :cond_12
    const-string v6, "max_position"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 706812
    invoke-static/range {p0 .. p1}, LX/4Qx;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v37, v2

    goto/16 :goto_1

    .line 706813
    :cond_13
    const-string v6, "max_rotation_velocity"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 706814
    const/4 v2, 0x1

    .line 706815
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v14, v2

    move-wide/from16 v38, v6

    goto/16 :goto_1

    .line 706816
    :cond_14
    const-string v6, "min_color_hsva"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 706817
    invoke-static/range {p0 .. p1}, LX/4Qr;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v36, v2

    goto/16 :goto_1

    .line 706818
    :cond_15
    const-string v6, "min_lifetime_millis"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_16

    .line 706819
    const/4 v2, 0x1

    .line 706820
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v13, v2

    move/from16 v31, v6

    goto/16 :goto_1

    .line 706821
    :cond_16
    const-string v6, "min_linear_dampening"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 706822
    const/4 v2, 0x1

    .line 706823
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v12, v2

    move-wide/from16 v34, v6

    goto/16 :goto_1

    .line 706824
    :cond_17
    const-string v6, "min_pixel_size"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_18

    .line 706825
    const/4 v2, 0x1

    .line 706826
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v11, v2

    move-wide/from16 v32, v6

    goto/16 :goto_1

    .line 706827
    :cond_18
    const-string v6, "min_position"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_19

    .line 706828
    invoke-static/range {p0 .. p1}, LX/4Qx;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v30, v2

    goto/16 :goto_1

    .line 706829
    :cond_19
    const-string v6, "min_rotation_velocity"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1a

    .line 706830
    const/4 v2, 0x1

    .line 706831
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v10, v2

    move-wide/from16 v28, v6

    goto/16 :goto_1

    .line 706832
    :cond_1a
    const-string v6, "rotational_dampening"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1b

    .line 706833
    const/4 v2, 0x1

    .line 706834
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v9, v2

    move-wide/from16 v26, v6

    goto/16 :goto_1

    .line 706835
    :cond_1b
    const-string v6, "spawn_rate"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 706836
    const/4 v2, 0x1

    .line 706837
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v8, v2

    move-wide/from16 v24, v6

    goto/16 :goto_1

    .line 706838
    :cond_1c
    const-string v6, "url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1d

    .line 706839
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 706840
    :cond_1d
    const-string v6, "velocity_scalar"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 706841
    invoke-static/range {p0 .. p1}, LX/4Qw;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 706842
    :cond_1e
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 706843
    :cond_1f
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 706844
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v60

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 706845
    if-eqz v3, :cond_20

    .line 706846
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 706847
    :cond_20
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v59

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 706848
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v58

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 706849
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 706850
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v56

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 706851
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 706852
    if-eqz v21, :cond_21

    .line 706853
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v54

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 706854
    :cond_21
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 706855
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 706856
    if-eqz v20, :cond_22

    .line 706857
    const/16 v3, 0xa

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v50

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 706858
    :cond_22
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 706859
    if-eqz v19, :cond_23

    .line 706860
    const/16 v2, 0xc

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 706861
    :cond_23
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 706862
    if-eqz v18, :cond_24

    .line 706863
    const/16 v2, 0xe

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 706864
    :cond_24
    if-eqz v17, :cond_25

    .line 706865
    const/16 v3, 0xf

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v44

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 706866
    :cond_25
    if-eqz v16, :cond_26

    .line 706867
    const/16 v2, 0x10

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 706868
    :cond_26
    if-eqz v15, :cond_27

    .line 706869
    const/16 v3, 0x11

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v40

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 706870
    :cond_27
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 706871
    if-eqz v14, :cond_28

    .line 706872
    const/16 v3, 0x13

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v38

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 706873
    :cond_28
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 706874
    if-eqz v13, :cond_29

    .line 706875
    const/16 v2, 0x15

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 706876
    :cond_29
    if-eqz v12, :cond_2a

    .line 706877
    const/16 v3, 0x16

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v34

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 706878
    :cond_2a
    if-eqz v11, :cond_2b

    .line 706879
    const/16 v3, 0x17

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v32

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 706880
    :cond_2b
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 706881
    if-eqz v10, :cond_2c

    .line 706882
    const/16 v3, 0x19

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v28

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 706883
    :cond_2c
    if-eqz v9, :cond_2d

    .line 706884
    const/16 v3, 0x1a

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v26

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 706885
    :cond_2d
    if-eqz v8, :cond_2e

    .line 706886
    const/16 v3, 0x1b

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v24

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 706887
    :cond_2e
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 706888
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 706889
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_2f
    move/from16 v58, v53

    move/from16 v59, v54

    move/from16 v60, v55

    move-wide/from16 v54, v48

    move/from16 v53, v50

    move/from16 v48, v43

    move/from16 v49, v46

    move/from16 v43, v40

    move/from16 v46, v41

    move-wide/from16 v40, v36

    move/from16 v36, v31

    move/from16 v37, v34

    move/from16 v31, v30

    move/from16 v30, v19

    move/from16 v19, v13

    move v13, v7

    move/from16 v61, v18

    move/from16 v18, v12

    move v12, v6

    move-wide/from16 v62, v22

    move/from16 v23, v61

    move/from16 v22, v17

    move/from16 v17, v11

    move v11, v5

    move-wide/from16 v64, v28

    move-wide/from16 v28, v24

    move-wide/from16 v24, v20

    move/from16 v21, v15

    move/from16 v20, v14

    move v15, v9

    move v14, v8

    move v8, v2

    move v9, v3

    move/from16 v3, v16

    move/from16 v16, v10

    move v10, v4

    move-wide/from16 v4, v56

    move/from16 v56, v51

    move/from16 v57, v52

    move/from16 v52, v47

    move-wide/from16 v50, v44

    move-wide/from16 v44, v38

    move/from16 v47, v42

    move/from16 v42, v35

    move-wide/from16 v38, v32

    move-wide/from16 v34, v64

    move-wide/from16 v32, v26

    move-wide/from16 v26, v62

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    .line 706890
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 706891
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 706892
    if-eqz v0, :cond_0

    .line 706893
    const-string v1, "animation_assets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706894
    invoke-static {p0, v0, p2, p3}, LX/4Qu;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 706895
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 706896
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 706897
    const-string v2, "attraction_force_strength"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706898
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 706899
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 706900
    if-eqz v0, :cond_2

    .line 706901
    const-string v1, "emitter_assets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706902
    invoke-static {p0, v0, p2, p3}, LX/4Qv;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 706903
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 706904
    if-eqz v0, :cond_3

    .line 706905
    const-string v1, "extra_config"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706906
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 706907
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 706908
    if-eqz v0, :cond_4

    .line 706909
    const-string v1, "gravity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706910
    invoke-static {p0, v0, p2}, LX/4Qx;->a(LX/15i;ILX/0nX;)V

    .line 706911
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 706912
    if-eqz v0, :cond_5

    .line 706913
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706914
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 706915
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 706916
    if-eqz v0, :cond_6

    .line 706917
    const-string v1, "init_max_position"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706918
    invoke-static {p0, v0, p2}, LX/4Qx;->a(LX/15i;ILX/0nX;)V

    .line 706919
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 706920
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_7

    .line 706921
    const-string v2, "init_max_rotation"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706922
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 706923
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 706924
    if-eqz v0, :cond_8

    .line 706925
    const-string v1, "init_max_velocity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706926
    invoke-static {p0, v0, p2}, LX/4Qx;->a(LX/15i;ILX/0nX;)V

    .line 706927
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 706928
    if-eqz v0, :cond_9

    .line 706929
    const-string v1, "init_min_position"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706930
    invoke-static {p0, v0, p2}, LX/4Qx;->a(LX/15i;ILX/0nX;)V

    .line 706931
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 706932
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_a

    .line 706933
    const-string v2, "init_min_rotation"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706934
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 706935
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 706936
    if-eqz v0, :cond_b

    .line 706937
    const-string v1, "init_min_velocity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706938
    invoke-static {p0, v0, p2}, LX/4Qx;->a(LX/15i;ILX/0nX;)V

    .line 706939
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 706940
    if-eqz v0, :cond_c

    .line 706941
    const-string v1, "init_particles"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706942
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 706943
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 706944
    if-eqz v0, :cond_d

    .line 706945
    const-string v1, "max_color_hsva"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706946
    invoke-static {p0, v0, p2}, LX/4Qr;->a(LX/15i;ILX/0nX;)V

    .line 706947
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 706948
    if-eqz v0, :cond_e

    .line 706949
    const-string v1, "max_lifetime_millis"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706950
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 706951
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 706952
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_f

    .line 706953
    const-string v2, "max_linear_dampening"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706954
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 706955
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 706956
    if-eqz v0, :cond_10

    .line 706957
    const-string v1, "max_particles"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706958
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 706959
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 706960
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_11

    .line 706961
    const-string v2, "max_pixel_size"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706962
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 706963
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 706964
    if-eqz v0, :cond_12

    .line 706965
    const-string v1, "max_position"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706966
    invoke-static {p0, v0, p2}, LX/4Qx;->a(LX/15i;ILX/0nX;)V

    .line 706967
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 706968
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_13

    .line 706969
    const-string v2, "max_rotation_velocity"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706970
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 706971
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 706972
    if-eqz v0, :cond_14

    .line 706973
    const-string v1, "min_color_hsva"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706974
    invoke-static {p0, v0, p2}, LX/4Qr;->a(LX/15i;ILX/0nX;)V

    .line 706975
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 706976
    if-eqz v0, :cond_15

    .line 706977
    const-string v1, "min_lifetime_millis"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706978
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 706979
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 706980
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_16

    .line 706981
    const-string v2, "min_linear_dampening"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706982
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 706983
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 706984
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_17

    .line 706985
    const-string v2, "min_pixel_size"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706986
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 706987
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 706988
    if-eqz v0, :cond_18

    .line 706989
    const-string v1, "min_position"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706990
    invoke-static {p0, v0, p2}, LX/4Qx;->a(LX/15i;ILX/0nX;)V

    .line 706991
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 706992
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_19

    .line 706993
    const-string v2, "min_rotation_velocity"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706994
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 706995
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 706996
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1a

    .line 706997
    const-string v2, "rotational_dampening"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706998
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 706999
    :cond_1a
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 707000
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1b

    .line 707001
    const-string v2, "spawn_rate"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707002
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 707003
    :cond_1b
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 707004
    if-eqz v0, :cond_1c

    .line 707005
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707006
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 707007
    :cond_1c
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 707008
    if-eqz v0, :cond_1d

    .line 707009
    const-string v1, "velocity_scalar"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707010
    invoke-static {p0, v0, p2}, LX/4Qw;->a(LX/15i;ILX/0nX;)V

    .line 707011
    :cond_1d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 707012
    return-void
.end method
