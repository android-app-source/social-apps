.class public final LX/4qa;
.super LX/1Xt;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final _delegate:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;"
        }
    .end annotation
.end field

.field public final _keyClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 814520
    invoke-direct {p0}, LX/1Xt;-><init>()V

    .line 814521
    iput-object p1, p0, LX/4qa;->_keyClass:Ljava/lang/Class;

    .line 814522
    iput-object p2, p0, LX/4qa;->_delegate:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 814523
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 814524
    if-nez p1, :cond_1

    .line 814525
    const/4 v0, 0x0

    .line 814526
    :cond_0
    return-object v0

    .line 814527
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/4qa;->_delegate:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 814528
    iget-object v1, p2, LX/0n3;->a:LX/15w;

    move-object v1, v1

    .line 814529
    invoke-virtual {v0, v1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 814530
    if-nez v0, :cond_0

    .line 814531
    iget-object v0, p0, LX/4qa;->_keyClass:Ljava/lang/Class;

    const-string v1, "not a valid representation"

    invoke-virtual {p2, v0, p1, v1}, LX/0n3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 814532
    :catch_0
    move-exception v0

    .line 814533
    iget-object v1, p0, LX/4qa;->_keyClass:Ljava/lang/Class;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "not a valid representation: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v1, p1, v0}, LX/0n3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0
.end method
