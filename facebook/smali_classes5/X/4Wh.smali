.class public final LX/4Wh;
.super LX/0ur;
.source ""


# instance fields
.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLGraphSearchConnectedFriendsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGraphSearchHighlightSnippet;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;",
            ">;"
        }
    .end annotation
.end field

.field public k:Z

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 758608
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 758609
    instance-of v0, p0, LX/4Wh;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 758610
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;
    .locals 2

    .prologue
    .line 758611
    new-instance v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;-><init>(LX/4Wh;)V

    .line 758612
    return-object v0
.end method
