.class public final LX/50t;
.super LX/1Ej;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1Ej",
        "<TC;TV;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TR;"
        }
    .end annotation
.end field

.field public b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TC;TV;>;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/0kB;


# direct methods
.method public constructor <init>(LX/0kB;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    .prologue
    .line 824176
    iput-object p1, p0, LX/50t;->c:LX/0kB;

    invoke-direct {p0}, LX/1Ej;-><init>()V

    .line 824177
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/50t;->a:Ljava/lang/Object;

    .line 824178
    return-void
.end method

.method private b()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TC;TV;>;"
        }
    .end annotation

    .prologue
    .line 824173
    iget-object v0, p0, LX/50t;->b:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/50t;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/50t;->c:LX/0kB;

    iget-object v0, v0, LX/0kB;->backingMap:Ljava/util/Map;

    iget-object v1, p0, LX/50t;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 824174
    :cond_0
    iget-object v0, p0, LX/50t;->c:LX/0kB;

    iget-object v0, v0, LX/0kB;->backingMap:Ljava/util/Map;

    iget-object v1, p0, LX/50t;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    move-object v0, v0

    .line 824175
    iput-object v0, p0, LX/50t;->b:Ljava/util/Map;

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/50t;->b:Ljava/util/Map;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 824169
    invoke-direct {p0}, LX/50t;->b()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/50t;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 824170
    iget-object v0, p0, LX/50t;->c:LX/0kB;

    iget-object v0, v0, LX/0kB;->backingMap:Ljava/util/Map;

    iget-object v1, p0, LX/50t;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 824171
    const/4 v0, 0x0

    iput-object v0, p0, LX/50t;->b:Ljava/util/Map;

    .line 824172
    :cond_0
    return-void
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 824164
    invoke-direct {p0}, LX/50t;->b()Ljava/util/Map;

    move-result-object v0

    .line 824165
    if-eqz v0, :cond_0

    .line 824166
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 824167
    :cond_0
    invoke-virtual {p0}, LX/50t;->a()V

    .line 824168
    return-void
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 824162
    invoke-direct {p0}, LX/50t;->b()Ljava/util/Map;

    move-result-object v0

    .line 824163
    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    invoke-static {v0, p1}, LX/0PM;->b(Ljava/util/Map;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TC;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 824156
    invoke-direct {p0}, LX/50t;->b()Ljava/util/Map;

    move-result-object v0

    .line 824157
    if-nez v0, :cond_0

    .line 824158
    sget-object v0, LX/0RZ;->b:Ljava/util/Iterator;

    move-object v0, v0

    .line 824159
    :goto_0
    return-object v0

    .line 824160
    :cond_0
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 824161
    new-instance v0, LX/50s;

    invoke-direct {v0, p0, v1}, LX/50s;-><init>(LX/50t;Ljava/util/Iterator;)V

    goto :goto_0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 824141
    invoke-direct {p0}, LX/50t;->b()Ljava/util/Map;

    move-result-object v0

    .line 824142
    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    invoke-static {v0, p1}, LX/0PM;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 824151
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 824152
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 824153
    iget-object v0, p0, LX/50t;->b:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/50t;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 824154
    iget-object v0, p0, LX/50t;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 824155
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/50t;->c:LX/0kB;

    iget-object v1, p0, LX/50t;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1, p2}, LX/0kB;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 824145
    invoke-direct {p0}, LX/50t;->b()Ljava/util/Map;

    move-result-object v0

    .line 824146
    if-nez v0, :cond_0

    .line 824147
    const/4 v0, 0x0

    .line 824148
    :goto_0
    return-object v0

    .line 824149
    :cond_0
    invoke-static {v0, p1}, LX/0PM;->c(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 824150
    invoke-virtual {p0}, LX/50t;->a()V

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 824143
    invoke-direct {p0}, LX/50t;->b()Ljava/util/Map;

    move-result-object v0

    .line 824144
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    goto :goto_0
.end method
