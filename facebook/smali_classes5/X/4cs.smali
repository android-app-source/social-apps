.class public LX/4cs;
.super Ljava/io/FilterOutputStream;
.source ""


# instance fields
.field private final a:LX/4cr;

.field private b:J


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;LX/4cr;)V
    .locals 2

    .prologue
    .line 795351
    invoke-direct {p0, p1}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 795352
    iput-object p2, p0, LX/4cs;->a:LX/4cr;

    .line 795353
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/4cs;->b:J

    .line 795354
    return-void
.end method


# virtual methods
.method public final write(I)V
    .locals 4

    .prologue
    .line 795355
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 795356
    iget-wide v0, p0, LX/4cs;->b:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/4cs;->b:J

    .line 795357
    iget-object v0, p0, LX/4cs;->a:LX/4cr;

    iget-wide v2, p0, LX/4cs;->b:J

    invoke-interface {v0, v2, v3}, LX/4cr;->a(J)V

    .line 795358
    return-void
.end method

.method public final write([BII)V
    .locals 4

    .prologue
    .line 795359
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 795360
    iget-wide v0, p0, LX/4cs;->b:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/4cs;->b:J

    .line 795361
    iget-object v0, p0, LX/4cs;->a:LX/4cr;

    iget-wide v2, p0, LX/4cs;->b:J

    invoke-interface {v0, v2, v3}, LX/4cr;->a(J)V

    .line 795362
    return-void
.end method
