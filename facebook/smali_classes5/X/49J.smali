.class public final LX/49J;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/debug/fieldusage/FieldTrackable$AccessTracker;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:J

.field public final g:LX/0So;

.field public h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 674234
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, LX/49J;->a:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;LX/0So;Z)V
    .locals 2

    .prologue
    .line 674235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 674236
    iput-object p1, p0, LX/49J;->d:Ljava/lang/String;

    .line 674237
    iput-object p2, p0, LX/49J;->e:Ljava/lang/String;

    .line 674238
    invoke-interface {p3}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/49J;->f:J

    .line 674239
    iput-object p3, p0, LX/49J;->g:LX/0So;

    .line 674240
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, LX/49J;->c:Ljava/util/Stack;

    .line 674241
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/49J;->b:Ljava/util/List;

    .line 674242
    iput-boolean p4, p0, LX/49J;->h:Z

    .line 674243
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 674244
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/49J;->h:Z

    .line 674245
    iget-object v0, p0, LX/49J;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 674246
    iget-object v0, p0, LX/49J;->c:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 674247
    if-eqz p1, :cond_0

    .line 674248
    iget-object v0, p0, LX/49J;->c:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 674249
    :cond_0
    return-void
.end method
