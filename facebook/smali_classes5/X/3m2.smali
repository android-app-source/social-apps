.class public LX/3m2;
.super LX/0Tr;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/3m2;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/3m3;Ljava/lang/String;)V
    .locals 1
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/browser/prefetch/db/BrowserDatabaseName;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 635306
    invoke-static {p3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0, p4}, LX/0Tr;-><init>(Landroid/content/Context;LX/0Tt;LX/0Px;Ljava/lang/String;)V

    .line 635307
    return-void
.end method

.method public static a(LX/0QB;)LX/3m2;
    .locals 7

    .prologue
    .line 635308
    sget-object v0, LX/3m2;->a:LX/3m2;

    if-nez v0, :cond_1

    .line 635309
    const-class v1, LX/3m2;

    monitor-enter v1

    .line 635310
    :try_start_0
    sget-object v0, LX/3m2;->a:LX/3m2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 635311
    if-eqz v2, :cond_0

    .line 635312
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 635313
    new-instance p0, LX/3m2;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Ts;->a(LX/0QB;)LX/0Ts;

    move-result-object v4

    check-cast v4, LX/0Tt;

    invoke-static {v0}, LX/3m3;->a(LX/0QB;)LX/3m3;

    move-result-object v5

    check-cast v5, LX/3m3;

    .line 635314
    const-string v6, "inapp_browser_db"

    move-object v6, v6

    .line 635315
    move-object v6, v6

    .line 635316
    check-cast v6, Ljava/lang/String;

    invoke-direct {p0, v3, v4, v5, v6}, LX/3m2;-><init>(Landroid/content/Context;LX/0Tt;LX/3m3;Ljava/lang/String;)V

    .line 635317
    move-object v0, p0

    .line 635318
    sput-object v0, LX/3m2;->a:LX/3m2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 635319
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 635320
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 635321
    :cond_1
    sget-object v0, LX/3m2;->a:LX/3m2;

    return-object v0

    .line 635322
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 635323
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final d()I
    .locals 1

    .prologue
    .line 635324
    const v0, 0xc800

    return v0
.end method
