.class public final enum LX/49w;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/49w;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/49w;

.field public static final enum FACEWEB:LX/49w;

.field public static final enum FEATURE_TAG:LX/49w;

.field public static final enum IMAGE_SIZE:LX/49w;

.field public static final enum URI:LX/49w;


# instance fields
.field public mType:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 675188
    new-instance v0, LX/49w;

    const-string v1, "URI"

    const-string v2, "uri"

    invoke-direct {v0, v1, v3, v2}, LX/49w;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/49w;->URI:LX/49w;

    .line 675189
    new-instance v0, LX/49w;

    const-string v1, "FEATURE_TAG"

    const-string v2, "feature_tag"

    invoke-direct {v0, v1, v4, v2}, LX/49w;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/49w;->FEATURE_TAG:LX/49w;

    .line 675190
    new-instance v0, LX/49w;

    const-string v1, "FACEWEB"

    const-string v2, "faceweb"

    invoke-direct {v0, v1, v5, v2}, LX/49w;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/49w;->FACEWEB:LX/49w;

    .line 675191
    new-instance v0, LX/49w;

    const-string v1, "IMAGE_SIZE"

    const-string v2, "image_size"

    invoke-direct {v0, v1, v6, v2}, LX/49w;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/49w;->IMAGE_SIZE:LX/49w;

    .line 675192
    const/4 v0, 0x4

    new-array v0, v0, [LX/49w;

    sget-object v1, LX/49w;->URI:LX/49w;

    aput-object v1, v0, v3

    sget-object v1, LX/49w;->FEATURE_TAG:LX/49w;

    aput-object v1, v0, v4

    sget-object v1, LX/49w;->FACEWEB:LX/49w;

    aput-object v1, v0, v5

    sget-object v1, LX/49w;->IMAGE_SIZE:LX/49w;

    aput-object v1, v0, v6

    sput-object v0, LX/49w;->$VALUES:[LX/49w;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 675193
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 675194
    iput-object p3, p0, LX/49w;->mType:Ljava/lang/String;

    .line 675195
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/49w;
    .locals 1

    .prologue
    .line 675196
    const-class v0, LX/49w;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/49w;

    return-object v0
.end method

.method public static values()[LX/49w;
    .locals 1

    .prologue
    .line 675197
    sget-object v0, LX/49w;->$VALUES:[LX/49w;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/49w;

    return-object v0
.end method


# virtual methods
.method public final getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 675198
    iget-object v0, p0, LX/49w;->mType:Ljava/lang/String;

    return-object v0
.end method
