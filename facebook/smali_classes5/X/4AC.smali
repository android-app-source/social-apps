.class public final LX/4AC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2sN;


# instance fields
.field public final synthetic a:LX/4AD;

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(LX/4AD;)V
    .locals 1

    .prologue
    .line 675562
    iput-object p1, p0, LX/4AC;->a:LX/4AD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 675563
    iget-object v0, p0, LX/4AC;->a:LX/4AD;

    iget v0, v0, LX/4AD;->b:I

    iput v0, p0, LX/4AC;->b:I

    .line 675564
    const/4 v0, -0x1

    iput v0, p0, LX/4AC;->c:I

    .line 675565
    iget-object v0, p0, LX/4AC;->a:LX/4AD;

    iget v0, v0, LX/4A8;->a:I

    iput v0, p0, LX/4AC;->d:I

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 675561
    iget v0, p0, LX/4AC;->b:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/1vs;
    .locals 5

    .prologue
    .line 675545
    iget-object v0, p0, LX/4AC;->a:LX/4AD;

    .line 675546
    iget v1, p0, LX/4AC;->b:I

    .line 675547
    iget v2, v0, LX/4A8;->a:I

    iget v3, p0, LX/4AC;->d:I

    if-eq v2, v3, :cond_0

    .line 675548
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 675549
    :cond_0
    if-nez v1, :cond_1

    .line 675550
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 675551
    :cond_1
    add-int/lit8 v2, v1, -0x1

    iput v2, p0, LX/4AC;->b:I

    .line 675552
    iget v2, v0, LX/4AD;->b:I

    sub-int v1, v2, v1

    iput v1, p0, LX/4AC;->c:I

    .line 675553
    iget-object v0, v0, LX/4AD;->c:LX/3Sd;

    .line 675554
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 675555
    :try_start_0
    invoke-virtual {v0, v1}, LX/3Sd;->b(I)LX/15i;

    move-result-object v3

    .line 675556
    invoke-virtual {v0, v1}, LX/3Sd;->c(I)I

    move-result v4

    .line 675557
    invoke-virtual {v0, v1}, LX/3Sd;->d(I)I

    move-result v0

    .line 675558
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 675559
    invoke-static {v3, v4, v0}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    return-object v0

    .line 675560
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
