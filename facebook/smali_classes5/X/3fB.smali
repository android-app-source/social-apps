.class public LX/3fB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3fB;


# instance fields
.field public final a:LX/11i;


# direct methods
.method public constructor <init>(LX/11i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 621572
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 621573
    iput-object p1, p0, LX/3fB;->a:LX/11i;

    .line 621574
    return-void
.end method

.method public static a(LX/0QB;)LX/3fB;
    .locals 4

    .prologue
    .line 621575
    sget-object v0, LX/3fB;->b:LX/3fB;

    if-nez v0, :cond_1

    .line 621576
    const-class v1, LX/3fB;

    monitor-enter v1

    .line 621577
    :try_start_0
    sget-object v0, LX/3fB;->b:LX/3fB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 621578
    if-eqz v2, :cond_0

    .line 621579
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 621580
    new-instance p0, LX/3fB;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-direct {p0, v3}, LX/3fB;-><init>(LX/11i;)V

    .line 621581
    move-object v0, p0

    .line 621582
    sput-object v0, LX/3fB;->b:LX/3fB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 621583
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 621584
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 621585
    :cond_1
    sget-object v0, LX/3fB;->b:LX/3fB;

    return-object v0

    .line 621586
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 621587
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/3fB;LX/0Px;Ljava/lang/String;)LX/3fB;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "LX/0Pq;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/3fB;"
        }
    .end annotation

    .prologue
    .line 621588
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pq;

    .line 621589
    iget-object v3, p0, LX/3fB;->a:LX/11i;

    invoke-interface {v3, v0}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 621590
    if-eqz v0, :cond_0

    .line 621591
    const v3, -0x4a6d0403

    invoke-static {v0, p2, v3}, LX/096;->d(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 621592
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 621593
    :cond_1
    return-object p0
.end method

.method public static a(LX/3fB;LX/0Px;Ljava/lang/String;Ljava/lang/String;LX/0P1;)LX/3fB;
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "LX/0Pq;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/3fB;"
        }
    .end annotation

    .prologue
    .line 621594
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pq;

    .line 621595
    iget-object v3, p0, LX/3fB;->a:LX/11i;

    invoke-interface {v3, v0}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 621596
    if-eqz v0, :cond_0

    .line 621597
    const v3, -0x2ed83d12

    invoke-static {v0, p2, p3, p4, v3}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 621598
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 621599
    :cond_1
    return-object p0
.end method

.method public static b(LX/3fB;LX/0Px;Ljava/lang/String;)LX/3fB;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "LX/0Pq;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/3fB;"
        }
    .end annotation

    .prologue
    .line 621533
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pq;

    .line 621534
    iget-object v3, p0, LX/3fB;->a:LX/11i;

    invoke-interface {v3, v0}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 621535
    if-eqz v0, :cond_0

    .line 621536
    const v3, 0x9088eb2

    invoke-static {v0, p2, v3}, LX/096;->c(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 621537
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 621538
    :cond_1
    return-object p0
.end method

.method public static b(LX/3fB;LX/0Px;Ljava/lang/String;Ljava/lang/String;LX/0P1;)LX/3fB;
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "LX/0Pq;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/3fB;"
        }
    .end annotation

    .prologue
    .line 621566
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pq;

    .line 621567
    iget-object v3, p0, LX/3fB;->a:LX/11i;

    invoke-interface {v3, v0}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 621568
    if-eqz v0, :cond_0

    .line 621569
    const v3, 0x6c9c65ac

    invoke-static {v0, p2, p3, p4, v3}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 621570
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 621571
    :cond_1
    return-object p0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/3fB;
    .locals 2

    .prologue
    .line 621561
    const/4 v1, 0x0

    .line 621562
    sget-object v0, LX/8EG;->d:LX/0Px;

    invoke-static {p0, v0, p1, v1, v1}, LX/3fB;->a(LX/3fB;LX/0Px;Ljava/lang/String;Ljava/lang/String;LX/0P1;)LX/3fB;

    .line 621563
    const/4 v1, 0x0

    .line 621564
    sget-object v0, LX/8EG;->e:LX/0Px;

    invoke-static {p0, v0, p1, v1, v1}, LX/3fB;->a(LX/3fB;LX/0Px;Ljava/lang/String;Ljava/lang/String;LX/0P1;)LX/3fB;

    .line 621565
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/3fB;
    .locals 2

    .prologue
    .line 621556
    const/4 v1, 0x0

    .line 621557
    sget-object v0, LX/8EG;->d:LX/0Px;

    invoke-static {p0, v0, p1, v1, v1}, LX/3fB;->b(LX/3fB;LX/0Px;Ljava/lang/String;Ljava/lang/String;LX/0P1;)LX/3fB;

    .line 621558
    const/4 v1, 0x0

    .line 621559
    sget-object v0, LX/8EG;->e:LX/0Px;

    invoke-static {p0, v0, p1, v1, v1}, LX/3fB;->b(LX/3fB;LX/0Px;Ljava/lang/String;Ljava/lang/String;LX/0P1;)LX/3fB;

    .line 621560
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/3fB;
    .locals 7

    .prologue
    .line 621542
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 621543
    sget-object v4, LX/8EG;->d:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_2

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pq;

    .line 621544
    iget-object v6, p0, LX/3fB;->a:LX/11i;

    invoke-interface {v6, v0}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 621545
    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 621546
    :goto_1
    move v0, v0

    .line 621547
    if-nez v0, :cond_0

    .line 621548
    :goto_2
    return-object p0

    :cond_0
    invoke-virtual {p0, p1}, LX/3fB;->b(Ljava/lang/String;)LX/3fB;

    move-result-object p0

    goto :goto_2

    .line 621549
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 621550
    :cond_2
    sget-object v4, LX/8EG;->e:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_3
    if-ge v3, v5, :cond_4

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pq;

    .line 621551
    iget-object v6, p0, LX/3fB;->a:LX/11i;

    invoke-interface {v6, v0}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 621552
    if-eqz v0, :cond_3

    invoke-interface {v0, p1}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 621553
    goto :goto_1

    .line 621554
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_4
    move v0, v2

    .line 621555
    goto :goto_1
.end method

.method public final e(Ljava/lang/String;)LX/3fB;
    .locals 1

    .prologue
    .line 621539
    sget-object v0, LX/8EG;->d:LX/0Px;

    invoke-static {p0, v0, p1}, LX/3fB;->a(LX/3fB;LX/0Px;Ljava/lang/String;)LX/3fB;

    .line 621540
    sget-object v0, LX/8EG;->e:LX/0Px;

    invoke-static {p0, v0, p1}, LX/3fB;->a(LX/3fB;LX/0Px;Ljava/lang/String;)LX/3fB;

    .line 621541
    return-object p0
.end method
