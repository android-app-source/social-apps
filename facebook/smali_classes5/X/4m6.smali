.class public LX/4m6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Landroid/net/Uri;

.field public f:Landroid/net/Uri;

.field public g:Landroid/net/Uri;

.field public h:Landroid/net/Uri;

.field public i:I

.field public j:J

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public r:Lcom/facebook/stickers/model/StickerCapabilities;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 805394
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)LX/4m6;
    .locals 0

    .prologue
    .line 805392
    iput p1, p0, LX/4m6;->i:I

    .line 805393
    return-object p0
.end method

.method public final a(J)LX/4m6;
    .locals 1

    .prologue
    .line 805390
    iput-wide p1, p0, LX/4m6;->j:J

    .line 805391
    return-object p0
.end method

.method public final a(Landroid/net/Uri;)LX/4m6;
    .locals 0

    .prologue
    .line 805388
    iput-object p1, p0, LX/4m6;->e:Landroid/net/Uri;

    .line 805389
    return-object p0
.end method

.method public final a(Lcom/facebook/stickers/model/StickerCapabilities;)LX/4m6;
    .locals 0

    .prologue
    .line 805386
    iput-object p1, p0, LX/4m6;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    .line 805387
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/4m6;
    .locals 0

    .prologue
    .line 805384
    iput-object p1, p0, LX/4m6;->a:Ljava/lang/String;

    .line 805385
    return-object p0
.end method

.method public final a(Ljava/util/List;)LX/4m6;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/4m6;"
        }
    .end annotation

    .prologue
    .line 805382
    iput-object p1, p0, LX/4m6;->p:Ljava/util/List;

    .line 805383
    return-object p0
.end method

.method public final a(Z)LX/4m6;
    .locals 0

    .prologue
    .line 805380
    iput-boolean p1, p0, LX/4m6;->k:Z

    .line 805381
    return-object p0
.end method

.method public final b(Landroid/net/Uri;)LX/4m6;
    .locals 0

    .prologue
    .line 805378
    iput-object p1, p0, LX/4m6;->f:Landroid/net/Uri;

    .line 805379
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/4m6;
    .locals 0

    .prologue
    .line 805359
    iput-object p1, p0, LX/4m6;->b:Ljava/lang/String;

    .line 805360
    return-object p0
.end method

.method public final b(Ljava/util/List;)LX/4m6;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/4m6;"
        }
    .end annotation

    .prologue
    .line 805395
    iput-object p1, p0, LX/4m6;->q:Ljava/util/List;

    .line 805396
    return-object p0
.end method

.method public final b(Z)LX/4m6;
    .locals 0

    .prologue
    .line 805376
    iput-boolean p1, p0, LX/4m6;->l:Z

    .line 805377
    return-object p0
.end method

.method public final c(Landroid/net/Uri;)LX/4m6;
    .locals 0

    .prologue
    .line 805374
    iput-object p1, p0, LX/4m6;->g:Landroid/net/Uri;

    .line 805375
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/4m6;
    .locals 0

    .prologue
    .line 805372
    iput-object p1, p0, LX/4m6;->c:Ljava/lang/String;

    .line 805373
    return-object p0
.end method

.method public final c(Z)LX/4m6;
    .locals 0

    .prologue
    .line 805370
    iput-boolean p1, p0, LX/4m6;->m:Z

    .line 805371
    return-object p0
.end method

.method public final d(Landroid/net/Uri;)LX/4m6;
    .locals 0

    .prologue
    .line 805368
    iput-object p1, p0, LX/4m6;->h:Landroid/net/Uri;

    .line 805369
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/4m6;
    .locals 0

    .prologue
    .line 805366
    iput-object p1, p0, LX/4m6;->d:Ljava/lang/String;

    .line 805367
    return-object p0
.end method

.method public final d(Z)LX/4m6;
    .locals 0

    .prologue
    .line 805364
    iput-boolean p1, p0, LX/4m6;->n:Z

    .line 805365
    return-object p0
.end method

.method public final e(Z)LX/4m6;
    .locals 0

    .prologue
    .line 805362
    iput-boolean p1, p0, LX/4m6;->o:Z

    .line 805363
    return-object p0
.end method

.method public final s()Lcom/facebook/stickers/model/StickerPack;
    .locals 1

    .prologue
    .line 805361
    new-instance v0, Lcom/facebook/stickers/model/StickerPack;

    invoke-direct {v0, p0}, Lcom/facebook/stickers/model/StickerPack;-><init>(LX/4m6;)V

    return-object v0
.end method
