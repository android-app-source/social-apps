.class public final LX/4KQ;
.super LX/2vO;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 679873
    invoke-direct {p0}, LX/2vO;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Integer;)LX/4KQ;
    .locals 1

    .prologue
    .line 679874
    const-string v0, "latest_time_sent"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 679875
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/4KQ;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/NotifReadnessUpdateTypeValue;
        .end annotation
    .end param

    .prologue
    .line 679876
    const-string v0, "update_type"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679877
    return-object p0
.end method

.method public final a(Ljava/util/List;)LX/4KQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/4KQ;"
        }
    .end annotation

    .prologue
    .line 679878
    const-string v0, "story_ids"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 679879
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/4KQ;
    .locals 1

    .prologue
    .line 679880
    const-string v0, "source"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679881
    return-object p0
.end method
