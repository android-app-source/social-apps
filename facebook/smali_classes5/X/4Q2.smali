.class public LX/4Q2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 702488
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 28

    .prologue
    .line 702489
    const/16 v23, 0x0

    .line 702490
    const/16 v22, 0x0

    .line 702491
    const/16 v21, 0x0

    .line 702492
    const/16 v20, 0x0

    .line 702493
    const/16 v19, 0x0

    .line 702494
    const/16 v18, 0x0

    .line 702495
    const/16 v17, 0x0

    .line 702496
    const/16 v16, 0x0

    .line 702497
    const/4 v13, 0x0

    .line 702498
    const-wide/16 v14, 0x0

    .line 702499
    const/4 v12, 0x0

    .line 702500
    const/4 v11, 0x0

    .line 702501
    const/4 v10, 0x0

    .line 702502
    const/4 v9, 0x0

    .line 702503
    const/4 v8, 0x0

    .line 702504
    const/4 v7, 0x0

    .line 702505
    const/4 v6, 0x0

    .line 702506
    const/4 v5, 0x0

    .line 702507
    const/4 v4, 0x0

    .line 702508
    const/4 v3, 0x0

    .line 702509
    const/4 v2, 0x0

    .line 702510
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_17

    .line 702511
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 702512
    const/4 v2, 0x0

    .line 702513
    :goto_0
    return v2

    .line 702514
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v25, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v25

    if-eq v2, v0, :cond_12

    .line 702515
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 702516
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 702517
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v25

    sget-object v26, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 702518
    const-string v25, "aggregated_claim_count"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_1

    .line 702519
    const/4 v2, 0x1

    .line 702520
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    move/from16 v24, v7

    move v7, v2

    goto :goto_1

    .line 702521
    :cond_1
    const-string v25, "availability_location"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_2

    .line 702522
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v23, v2

    goto :goto_1

    .line 702523
    :cond_2
    const-string v25, "block_resharing"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_3

    .line 702524
    const/4 v2, 0x1

    .line 702525
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v22, v6

    move v6, v2

    goto :goto_1

    .line 702526
    :cond_3
    const-string v25, "claim_type"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_4

    .line 702527
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v21, v2

    goto :goto_1

    .line 702528
    :cond_4
    const-string v25, "description"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_5

    .line 702529
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 702530
    :cond_5
    const-string v25, "destination_link"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_6

    .line 702531
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 702532
    :cond_6
    const-string v25, "discount_barcode_image"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_7

    .line 702533
    invoke-static/range {p0 .. p1}, LX/2sY;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 702534
    :cond_7
    const-string v25, "discount_barcode_type"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_8

    .line 702535
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 702536
    :cond_8
    const-string v25, "discount_barcode_value"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_9

    .line 702537
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 702538
    :cond_9
    const-string v25, "expiration"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_a

    .line 702539
    const/4 v2, 0x1

    .line 702540
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 702541
    :cond_a
    const-string v25, "id"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_b

    .line 702542
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 702543
    :cond_b
    const-string v25, "offer_title"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_c

    .line 702544
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 702545
    :cond_c
    const-string v25, "owner"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_d

    .line 702546
    invoke-static/range {p0 .. p1}, LX/2bM;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 702547
    :cond_d
    const-string v25, "terms_and_conditions"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_e

    .line 702548
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 702549
    :cond_e
    const-string v25, "url"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_f

    .line 702550
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 702551
    :cond_f
    const-string v25, "published_offer_views"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_10

    .line 702552
    invoke-static/range {p0 .. p1}, LX/4Q3;->b(LX/15w;LX/186;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 702553
    :cond_10
    const-string v25, "is_messenger"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 702554
    const/4 v2, 0x1

    .line 702555
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    move v9, v8

    move v8, v2

    goto/16 :goto_1

    .line 702556
    :cond_11
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 702557
    :cond_12
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 702558
    if-eqz v7, :cond_13

    .line 702559
    const/4 v2, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1, v7}, LX/186;->a(III)V

    .line 702560
    :cond_13
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 702561
    if-eqz v6, :cond_14

    .line 702562
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 702563
    :cond_14
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 702564
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 702565
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 702566
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 702567
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 702568
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 702569
    if-eqz v3, :cond_15

    .line 702570
    const/16 v3, 0x9

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 702571
    :cond_15
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 702572
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 702573
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 702574
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 702575
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 702576
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 702577
    if-eqz v8, :cond_16

    .line 702578
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->a(IZ)V

    .line 702579
    :cond_16
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_17
    move/from16 v24, v23

    move/from16 v23, v22

    move/from16 v22, v21

    move/from16 v21, v20

    move/from16 v20, v19

    move/from16 v19, v18

    move/from16 v18, v17

    move/from16 v17, v16

    move/from16 v16, v13

    move v13, v10

    move v10, v7

    move v7, v5

    move/from16 v27, v12

    move v12, v9

    move v9, v6

    move v6, v4

    move-wide v4, v14

    move/from16 v15, v27

    move v14, v11

    move v11, v8

    move v8, v2

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 702580
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 702581
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 702582
    if-eqz v0, :cond_0

    .line 702583
    const-string v1, "aggregated_claim_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702584
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 702585
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 702586
    if-eqz v0, :cond_1

    .line 702587
    const-string v1, "availability_location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702588
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 702589
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 702590
    if-eqz v0, :cond_2

    .line 702591
    const-string v1, "block_resharing"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702592
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 702593
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 702594
    if-eqz v0, :cond_3

    .line 702595
    const-string v1, "claim_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702596
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 702597
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 702598
    if-eqz v0, :cond_4

    .line 702599
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702600
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 702601
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 702602
    if-eqz v0, :cond_5

    .line 702603
    const-string v1, "destination_link"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702604
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 702605
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 702606
    if-eqz v0, :cond_6

    .line 702607
    const-string v1, "discount_barcode_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702608
    invoke-static {p0, v0, p2, p3}, LX/2sY;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 702609
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 702610
    if-eqz v0, :cond_7

    .line 702611
    const-string v1, "discount_barcode_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702612
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 702613
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 702614
    if-eqz v0, :cond_8

    .line 702615
    const-string v1, "discount_barcode_value"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702616
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 702617
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 702618
    cmp-long v2, v0, v2

    if-eqz v2, :cond_9

    .line 702619
    const-string v2, "expiration"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702620
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 702621
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 702622
    if-eqz v0, :cond_a

    .line 702623
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702624
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 702625
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 702626
    if-eqz v0, :cond_b

    .line 702627
    const-string v1, "offer_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702628
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 702629
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 702630
    if-eqz v0, :cond_c

    .line 702631
    const-string v1, "owner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702632
    invoke-static {p0, v0, p2, p3}, LX/2bM;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 702633
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 702634
    if-eqz v0, :cond_d

    .line 702635
    const-string v1, "terms_and_conditions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702636
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 702637
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 702638
    if-eqz v0, :cond_e

    .line 702639
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702640
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 702641
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 702642
    if-eqz v0, :cond_10

    .line 702643
    const-string v1, "published_offer_views"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702644
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 702645
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_f

    .line 702646
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/4Q3;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 702647
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 702648
    :cond_f
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 702649
    :cond_10
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 702650
    if-eqz v0, :cond_11

    .line 702651
    const-string v1, "is_messenger"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702652
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 702653
    :cond_11
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 702654
    return-void
.end method
