.class public LX/4OY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 697133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 34

    .prologue
    .line 697134
    const/16 v30, 0x0

    .line 697135
    const/16 v29, 0x0

    .line 697136
    const/16 v28, 0x0

    .line 697137
    const/16 v27, 0x0

    .line 697138
    const/16 v26, 0x0

    .line 697139
    const/16 v25, 0x0

    .line 697140
    const/16 v24, 0x0

    .line 697141
    const/16 v23, 0x0

    .line 697142
    const/16 v22, 0x0

    .line 697143
    const/16 v21, 0x0

    .line 697144
    const/16 v20, 0x0

    .line 697145
    const/16 v19, 0x0

    .line 697146
    const/16 v18, 0x0

    .line 697147
    const/16 v17, 0x0

    .line 697148
    const/16 v16, 0x0

    .line 697149
    const/4 v15, 0x0

    .line 697150
    const/4 v14, 0x0

    .line 697151
    const/4 v13, 0x0

    .line 697152
    const/4 v12, 0x0

    .line 697153
    const/4 v11, 0x0

    .line 697154
    const/4 v10, 0x0

    .line 697155
    const/4 v9, 0x0

    .line 697156
    const/4 v8, 0x0

    .line 697157
    const/4 v7, 0x0

    .line 697158
    const/4 v6, 0x0

    .line 697159
    const/4 v5, 0x0

    .line 697160
    const/4 v4, 0x0

    .line 697161
    const/4 v3, 0x0

    .line 697162
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v31

    sget-object v32, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    if-eq v0, v1, :cond_1

    .line 697163
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 697164
    const/4 v3, 0x0

    .line 697165
    :goto_0
    return v3

    .line 697166
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 697167
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v31

    sget-object v32, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    if-eq v0, v1, :cond_1c

    .line 697168
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v31

    .line 697169
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 697170
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v32

    sget-object v33, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    if-eq v0, v1, :cond_1

    if-eqz v31, :cond_1

    .line 697171
    const-string v32, "android_urls"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_2

    .line 697172
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v30

    goto :goto_1

    .line 697173
    :cond_2
    const-string v32, "feedAwesomizerProfilePicture"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_3

    .line 697174
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v29

    goto :goto_1

    .line 697175
    :cond_3
    const-string v32, "id"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_4

    .line 697176
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    goto :goto_1

    .line 697177
    :cond_4
    const-string v32, "image"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_5

    .line 697178
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v27

    goto :goto_1

    .line 697179
    :cond_5
    const-string v32, "imageHighOrig"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_6

    .line 697180
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v26

    goto :goto_1

    .line 697181
    :cond_6
    const-string v32, "name"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_7

    .line 697182
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    goto :goto_1

    .line 697183
    :cond_7
    const-string v32, "name_search_tokens"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_8

    .line 697184
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 697185
    :cond_8
    const-string v32, "profileImageLarge"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_9

    .line 697186
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 697187
    :cond_9
    const-string v32, "profileImageSmall"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_a

    .line 697188
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 697189
    :cond_a
    const-string v32, "profilePicture50"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_b

    .line 697190
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 697191
    :cond_b
    const-string v32, "profilePictureHighRes"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_c

    .line 697192
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 697193
    :cond_c
    const-string v32, "profilePictureLarge"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_d

    .line 697194
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 697195
    :cond_d
    const-string v32, "profile_photo"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_e

    .line 697196
    invoke-static/range {p0 .. p1}, LX/2sY;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 697197
    :cond_e
    const-string v32, "profile_picture"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_f

    .line 697198
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 697199
    :cond_f
    const-string v32, "profile_picture_is_silhouette"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_10

    .line 697200
    const/4 v3, 0x1

    .line 697201
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v16

    goto/16 :goto_1

    .line 697202
    :cond_10
    const-string v32, "related_article_title"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_11

    .line 697203
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 697204
    :cond_11
    const-string v32, "social_context"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_12

    .line 697205
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 697206
    :cond_12
    const-string v32, "streaming_profile_picture"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_13

    .line 697207
    invoke-static/range {p0 .. p1}, LX/4TN;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 697208
    :cond_13
    const-string v32, "tag"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_14

    .line 697209
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 697210
    :cond_14
    const-string v32, "taggable_object_profile_picture"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_15

    .line 697211
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 697212
    :cond_15
    const-string v32, "top_headline_object"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_16

    .line 697213
    invoke-static/range {p0 .. p1}, LX/2bR;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 697214
    :cond_16
    const-string v32, "topic_image"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_17

    .line 697215
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 697216
    :cond_17
    const-string v32, "trending_topic_data"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_18

    .line 697217
    invoke-static/range {p0 .. p1}, LX/4U4;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 697218
    :cond_18
    const-string v32, "trending_topic_name"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_19

    .line 697219
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 697220
    :cond_19
    const-string v32, "url"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_1a

    .line 697221
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 697222
    :cond_1a
    const-string v32, "profilePicture180"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_1b

    .line 697223
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 697224
    :cond_1b
    const-string v32, "publisher_profile_image"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_0

    .line 697225
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 697226
    :cond_1c
    const/16 v31, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 697227
    const/16 v31, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v31

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 697228
    const/16 v30, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v30

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 697229
    const/16 v29, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v29

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 697230
    const/16 v28, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v28

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 697231
    const/16 v27, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 697232
    const/16 v26, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v26

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 697233
    const/16 v25, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 697234
    const/16 v24, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 697235
    const/16 v23, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 697236
    const/16 v22, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 697237
    const/16 v21, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 697238
    const/16 v20, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 697239
    const/16 v19, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 697240
    const/16 v18, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 697241
    if-eqz v3, :cond_1d

    .line 697242
    const/16 v3, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 697243
    :cond_1d
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 697244
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 697245
    const/16 v3, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 697246
    const/16 v3, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 697247
    const/16 v3, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 697248
    const/16 v3, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 697249
    const/16 v3, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 697250
    const/16 v3, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 697251
    const/16 v3, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 697252
    const/16 v3, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 697253
    const/16 v3, 0x1b

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 697254
    const/16 v3, 0x1c

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 697255
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
