.class public LX/3ll;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;


# instance fields
.field public final traceInfo:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 634978
    new-instance v0, LX/1sv;

    const-string v1, "MqttThriftHeader"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/3ll;->b:LX/1sv;

    .line 634979
    new-instance v0, LX/1sw;

    const-string v1, "traceInfo"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/3ll;->c:LX/1sw;

    .line 634980
    sput-boolean v3, LX/3ll;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 634975
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 634976
    iput-object p1, p0, LX/3ll;->traceInfo:Ljava/lang/String;

    .line 634977
    return-void
.end method

.method public static b(LX/1su;)LX/3ll;
    .locals 4

    .prologue
    .line 634963
    const/4 v0, 0x0

    .line 634964
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    .line 634965
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v1

    .line 634966
    iget-byte v2, v1, LX/1sw;->b:B

    if-eqz v2, :cond_1

    .line 634967
    iget-short v2, v1, LX/1sw;->c:S

    packed-switch v2, :pswitch_data_0

    .line 634968
    iget-byte v1, v1, LX/1sw;->b:B

    invoke-static {p0, v1}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 634969
    :pswitch_0
    iget-byte v2, v1, LX/1sw;->b:B

    const/16 v3, 0xb

    if-ne v2, v3, :cond_0

    .line 634970
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 634971
    :cond_0
    iget-byte v1, v1, LX/1sw;->b:B

    invoke-static {p0, v1}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 634972
    :cond_1
    invoke-virtual {p0}, LX/1su;->e()V

    .line 634973
    new-instance v1, LX/3ll;

    invoke-direct {v1, v0}, LX/3ll;-><init>(Ljava/lang/String;)V

    .line 634974
    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 634942
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 634943
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v1, v0

    .line 634944
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    .line 634945
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MqttThriftHeader"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 634946
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 634947
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 634948
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 634949
    iget-object v4, p0, LX/3ll;->traceInfo:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 634950
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 634951
    const-string v4, "traceInfo"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 634952
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 634953
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 634954
    iget-object v0, p0, LX/3ll;->traceInfo:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 634955
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 634956
    :cond_0
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 634957
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 634958
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 634959
    :cond_1
    const-string v0, ""

    move-object v2, v0

    goto :goto_0

    .line 634960
    :cond_2
    const-string v0, ""

    move-object v1, v0

    goto :goto_1

    .line 634961
    :cond_3
    const-string v0, ""

    goto :goto_2

    .line 634962
    :cond_4
    iget-object v0, p0, LX/3ll;->traceInfo:Ljava/lang/String;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 634934
    invoke-virtual {p1}, LX/1su;->a()V

    .line 634935
    iget-object v0, p0, LX/3ll;->traceInfo:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 634936
    iget-object v0, p0, LX/3ll;->traceInfo:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 634937
    sget-object v0, LX/3ll;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 634938
    iget-object v0, p0, LX/3ll;->traceInfo:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 634939
    :cond_0
    invoke-virtual {p1}, LX/1su;->c()V

    .line 634940
    invoke-virtual {p1}, LX/1su;->b()V

    .line 634941
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 634919
    if-nez p1, :cond_1

    .line 634920
    :cond_0
    :goto_0
    return v0

    .line 634921
    :cond_1
    instance-of v1, p1, LX/3ll;

    if-eqz v1, :cond_0

    .line 634922
    check-cast p1, LX/3ll;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 634923
    if-nez p1, :cond_3

    .line 634924
    :cond_2
    :goto_1
    move v0, v2

    .line 634925
    goto :goto_0

    .line 634926
    :cond_3
    iget-object v0, p0, LX/3ll;->traceInfo:Ljava/lang/String;

    if-eqz v0, :cond_6

    move v0, v1

    .line 634927
    :goto_2
    iget-object v3, p1, LX/3ll;->traceInfo:Ljava/lang/String;

    if-eqz v3, :cond_7

    move v3, v1

    .line 634928
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 634929
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 634930
    iget-object v0, p0, LX/3ll;->traceInfo:Ljava/lang/String;

    iget-object v3, p1, LX/3ll;->traceInfo:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_5
    move v2, v1

    .line 634931
    goto :goto_1

    :cond_6
    move v0, v2

    .line 634932
    goto :goto_2

    :cond_7
    move v3, v2

    .line 634933
    goto :goto_3
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 634918
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 634915
    sget-boolean v0, LX/3ll;->a:Z

    .line 634916
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/3ll;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 634917
    return-object v0
.end method
