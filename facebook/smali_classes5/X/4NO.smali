.class public LX/4NO;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 692282
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v0, 0x0

    .line 692283
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_c

    .line 692284
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 692285
    :goto_0
    return v0

    .line 692286
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 692287
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_b

    .line 692288
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 692289
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 692290
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 692291
    const-string v11, "campaign_owner"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 692292
    invoke-static {p0, p1}, LX/2aw;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 692293
    :cond_2
    const-string v11, "confirmation_accent_image"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 692294
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 692295
    :cond_3
    const-string v11, "confirmation_title"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 692296
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 692297
    :cond_4
    const-string v11, "id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 692298
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 692299
    :cond_5
    const-string v11, "image_overlays"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 692300
    invoke-static {p0, p1}, LX/4Ob;->b(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 692301
    :cond_6
    const-string v11, "media_attachments"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 692302
    invoke-static {p0, p1}, LX/2as;->b(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 692303
    :cond_7
    const-string v11, "posting_actors"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 692304
    invoke-static {p0, p1}, LX/4NP;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 692305
    :cond_8
    const-string v11, "social_context"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 692306
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 692307
    :cond_9
    const-string v11, "url"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 692308
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_1

    .line 692309
    :cond_a
    const-string v11, "video_campaign"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 692310
    invoke-static {p0, p1}, LX/4Nk;->a(LX/15w;LX/186;)I

    move-result v0

    goto/16 :goto_1

    .line 692311
    :cond_b
    const/16 v10, 0xb

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 692312
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 692313
    const/4 v9, 0x2

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 692314
    const/4 v8, 0x3

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 692315
    const/4 v7, 0x4

    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 692316
    const/4 v6, 0x5

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 692317
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 692318
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 692319
    const/16 v3, 0x8

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 692320
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 692321
    const/16 v1, 0xa

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 692322
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_c
    move v1, v0

    move v2, v0

    move v3, v0

    move v4, v0

    move v5, v0

    move v6, v0

    move v7, v0

    move v8, v0

    move v9, v0

    goto/16 :goto_1
.end method
