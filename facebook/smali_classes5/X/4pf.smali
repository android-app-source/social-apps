.class public final LX/4pf;
.super Ljava/io/Writer;
.source ""


# instance fields
.field private final a:LX/12A;

.field private b:Ljava/io/OutputStream;

.field private c:[B

.field private final d:I

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(LX/12A;Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 811761
    invoke-direct {p0}, Ljava/io/Writer;-><init>()V

    .line 811762
    iput v1, p0, LX/4pf;->f:I

    .line 811763
    iput-object p1, p0, LX/4pf;->a:LX/12A;

    .line 811764
    iput-object p2, p0, LX/4pf;->b:Ljava/io/OutputStream;

    .line 811765
    invoke-virtual {p1}, LX/12A;->f()[B

    move-result-object v0

    iput-object v0, p0, LX/4pf;->c:[B

    .line 811766
    iget-object v0, p0, LX/4pf;->c:[B

    array-length v0, v0

    add-int/lit8 v0, v0, -0x4

    iput v0, p0, LX/4pf;->d:I

    .line 811767
    iput v1, p0, LX/4pf;->e:I

    .line 811768
    return-void
.end method

.method public static a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 811753
    const v0, 0x10ffff

    if-le p0, v0, :cond_0

    .line 811754
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Illegal character point (0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") to output; max is 0x10FFFF as per RFC 4627"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 811755
    :goto_0
    return-object v0

    .line 811756
    :cond_0
    const v0, 0xd800

    if-lt p0, v0, :cond_2

    .line 811757
    const v0, 0xdbff

    if-gt p0, v0, :cond_1

    .line 811758
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unmatched first part of surrogate pair (0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 811759
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unmatched second part of surrogate pair (0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 811760
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Illegal character point (0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") to output"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b(I)I
    .locals 4

    .prologue
    const v3, 0xdc00

    .line 811748
    iget v0, p0, LX/4pf;->f:I

    .line 811749
    const/4 v1, 0x0

    iput v1, p0, LX/4pf;->f:I

    .line 811750
    if-lt p1, v3, :cond_0

    const v1, 0xdfff

    if-le p1, v1, :cond_1

    .line 811751
    :cond_0
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Broken surrogate pair: first char 0x"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", second 0x"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; illegal combination"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 811752
    :cond_1
    const/high16 v1, 0x10000

    const v2, 0xd800

    sub-int/2addr v0, v2

    shl-int/lit8 v0, v0, 0xa

    add-int/2addr v0, v1

    sub-int v1, p1, v3

    add-int/2addr v0, v1

    return v0
.end method

.method private static c(I)V
    .locals 2

    .prologue
    .line 811747
    new-instance v0, Ljava/io/IOException;

    invoke-static {p0}, LX/4pf;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final append(C)Ljava/io/Writer;
    .locals 0

    .prologue
    .line 811745
    invoke-virtual {p0, p1}, LX/4pf;->write(I)V

    .line 811746
    return-object p0
.end method

.method public final bridge synthetic append(C)Ljava/lang/Appendable;
    .locals 1

    .prologue
    .line 811744
    invoke-virtual {p0, p1}, LX/4pf;->append(C)Ljava/io/Writer;

    move-result-object v0

    return-object v0
.end method

.method public final close()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 811728
    iget-object v0, p0, LX/4pf;->b:Ljava/io/OutputStream;

    if-eqz v0, :cond_2

    .line 811729
    iget v0, p0, LX/4pf;->e:I

    if-lez v0, :cond_0

    .line 811730
    iget-object v0, p0, LX/4pf;->b:Ljava/io/OutputStream;

    iget-object v1, p0, LX/4pf;->c:[B

    iget v2, p0, LX/4pf;->e:I

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 811731
    iput v3, p0, LX/4pf;->e:I

    .line 811732
    :cond_0
    iget-object v0, p0, LX/4pf;->b:Ljava/io/OutputStream;

    .line 811733
    iput-object v4, p0, LX/4pf;->b:Ljava/io/OutputStream;

    .line 811734
    iget-object v1, p0, LX/4pf;->c:[B

    .line 811735
    if-eqz v1, :cond_1

    .line 811736
    iput-object v4, p0, LX/4pf;->c:[B

    .line 811737
    iget-object v2, p0, LX/4pf;->a:LX/12A;

    invoke-virtual {v2, v1}, LX/12A;->b([B)V

    .line 811738
    :cond_1
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 811739
    iget v0, p0, LX/4pf;->f:I

    .line 811740
    iput v3, p0, LX/4pf;->f:I

    .line 811741
    if-lez v0, :cond_2

    .line 811742
    invoke-static {v0}, LX/4pf;->c(I)V

    .line 811743
    :cond_2
    return-void
.end method

.method public final flush()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 811722
    iget-object v0, p0, LX/4pf;->b:Ljava/io/OutputStream;

    if-eqz v0, :cond_1

    .line 811723
    iget v0, p0, LX/4pf;->e:I

    if-lez v0, :cond_0

    .line 811724
    iget-object v0, p0, LX/4pf;->b:Ljava/io/OutputStream;

    iget-object v1, p0, LX/4pf;->c:[B

    iget v2, p0, LX/4pf;->e:I

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 811725
    iput v3, p0, LX/4pf;->e:I

    .line 811726
    :cond_0
    iget-object v0, p0, LX/4pf;->b:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 811727
    :cond_1
    return-void
.end method

.method public final write(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 811695
    iget v0, p0, LX/4pf;->f:I

    if-lez v0, :cond_2

    .line 811696
    invoke-direct {p0, p1}, LX/4pf;->b(I)I

    move-result p1

    .line 811697
    :cond_0
    iget v0, p0, LX/4pf;->e:I

    iget v1, p0, LX/4pf;->d:I

    if-lt v0, v1, :cond_1

    .line 811698
    iget-object v0, p0, LX/4pf;->b:Ljava/io/OutputStream;

    iget-object v1, p0, LX/4pf;->c:[B

    iget v2, p0, LX/4pf;->e:I

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 811699
    iput v3, p0, LX/4pf;->e:I

    .line 811700
    :cond_1
    const/16 v0, 0x80

    if-ge p1, v0, :cond_4

    .line 811701
    iget-object v0, p0, LX/4pf;->c:[B

    iget v1, p0, LX/4pf;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4pf;->e:I

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 811702
    :goto_0
    return-void

    .line 811703
    :cond_2
    const v0, 0xd800

    if-lt p1, v0, :cond_0

    const v0, 0xdfff

    if-gt p1, v0, :cond_0

    .line 811704
    const v0, 0xdbff

    if-le p1, v0, :cond_3

    .line 811705
    invoke-static {p1}, LX/4pf;->c(I)V

    .line 811706
    :cond_3
    iput p1, p0, LX/4pf;->f:I

    goto :goto_0

    .line 811707
    :cond_4
    iget v0, p0, LX/4pf;->e:I

    .line 811708
    const/16 v1, 0x800

    if-ge p1, v1, :cond_5

    .line 811709
    iget-object v1, p0, LX/4pf;->c:[B

    add-int/lit8 v2, v0, 0x1

    shr-int/lit8 v3, p1, 0x6

    or-int/lit16 v3, v3, 0xc0

    int-to-byte v3, v3

    aput-byte v3, v1, v0

    .line 811710
    iget-object v1, p0, LX/4pf;->c:[B

    add-int/lit8 v0, v2, 0x1

    and-int/lit8 v3, p1, 0x3f

    or-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 811711
    :goto_1
    iput v0, p0, LX/4pf;->e:I

    goto :goto_0

    .line 811712
    :cond_5
    const v1, 0xffff

    if-gt p1, v1, :cond_6

    .line 811713
    iget-object v1, p0, LX/4pf;->c:[B

    add-int/lit8 v2, v0, 0x1

    shr-int/lit8 v3, p1, 0xc

    or-int/lit16 v3, v3, 0xe0

    int-to-byte v3, v3

    aput-byte v3, v1, v0

    .line 811714
    iget-object v0, p0, LX/4pf;->c:[B

    add-int/lit8 v1, v2, 0x1

    shr-int/lit8 v3, p1, 0x6

    and-int/lit8 v3, v3, 0x3f

    or-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    aput-byte v3, v0, v2

    .line 811715
    iget-object v2, p0, LX/4pf;->c:[B

    add-int/lit8 v0, v1, 0x1

    and-int/lit8 v3, p1, 0x3f

    or-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    goto :goto_1

    .line 811716
    :cond_6
    const v1, 0x10ffff

    if-le p1, v1, :cond_7

    .line 811717
    invoke-static {p1}, LX/4pf;->c(I)V

    .line 811718
    :cond_7
    iget-object v1, p0, LX/4pf;->c:[B

    add-int/lit8 v2, v0, 0x1

    shr-int/lit8 v3, p1, 0x12

    or-int/lit16 v3, v3, 0xf0

    int-to-byte v3, v3

    aput-byte v3, v1, v0

    .line 811719
    iget-object v0, p0, LX/4pf;->c:[B

    add-int/lit8 v1, v2, 0x1

    shr-int/lit8 v3, p1, 0xc

    and-int/lit8 v3, v3, 0x3f

    or-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    aput-byte v3, v0, v2

    .line 811720
    iget-object v0, p0, LX/4pf;->c:[B

    add-int/lit8 v2, v1, 0x1

    shr-int/lit8 v3, p1, 0x6

    and-int/lit8 v3, v3, 0x3f

    or-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    aput-byte v3, v0, v1

    .line 811721
    iget-object v1, p0, LX/4pf;->c:[B

    add-int/lit8 v0, v2, 0x1

    and-int/lit8 v3, p1, 0x3f

    or-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    goto :goto_1
.end method

.method public final write(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 811595
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, LX/4pf;->write(Ljava/lang/String;II)V

    .line 811596
    return-void
.end method

.method public final write(Ljava/lang/String;II)V
    .locals 11

    .prologue
    const/16 v9, 0x80

    const/4 v1, 0x0

    .line 811647
    const/4 v0, 0x2

    if-ge p3, v0, :cond_1

    .line 811648
    const/4 v0, 0x1

    if-ne p3, v0, :cond_0

    .line 811649
    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {p0, v0}, LX/4pf;->write(I)V

    .line 811650
    :cond_0
    :goto_0
    return-void

    .line 811651
    :cond_1
    iget v0, p0, LX/4pf;->f:I

    if-lez v0, :cond_2

    .line 811652
    add-int/lit8 v0, p2, 0x1

    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 811653
    add-int/lit8 p3, p3, -0x1

    .line 811654
    invoke-direct {p0, v2}, LX/4pf;->b(I)I

    move-result v2

    invoke-virtual {p0, v2}, LX/4pf;->write(I)V

    move p2, v0

    .line 811655
    :cond_2
    iget v0, p0, LX/4pf;->e:I

    .line 811656
    iget-object v5, p0, LX/4pf;->c:[B

    .line 811657
    iget v6, p0, LX/4pf;->d:I

    .line 811658
    add-int v7, p3, p2

    .line 811659
    :goto_1
    if-ge p2, v7, :cond_b

    .line 811660
    if-lt v0, v6, :cond_3

    .line 811661
    iget-object v2, p0, LX/4pf;->b:Ljava/io/OutputStream;

    invoke-virtual {v2, v5, v1, v0}, Ljava/io/OutputStream;->write([BII)V

    move v0, v1

    .line 811662
    :cond_3
    add-int/lit8 v3, p2, 0x1

    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 811663
    if-ge v2, v9, :cond_5

    .line 811664
    add-int/lit8 v4, v0, 0x1

    int-to-byte v2, v2

    aput-byte v2, v5, v0

    .line 811665
    sub-int v2, v7, v3

    .line 811666
    sub-int v0, v6, v4

    .line 811667
    if-le v2, v0, :cond_d

    .line 811668
    :goto_2
    add-int v8, v0, v3

    move v2, v4

    move v0, v3

    .line 811669
    :goto_3
    if-ge v0, v8, :cond_c

    .line 811670
    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 811671
    if-ge v0, v9, :cond_4

    .line 811672
    add-int/lit8 v4, v2, 0x1

    int-to-byte v0, v0

    aput-byte v0, v5, v2

    move v2, v4

    move v0, v3

    goto :goto_3

    :cond_4
    move v10, v0

    move v0, v2

    move v2, v10

    .line 811673
    :cond_5
    const/16 v4, 0x800

    if-ge v2, v4, :cond_6

    .line 811674
    add-int/lit8 v4, v0, 0x1

    shr-int/lit8 v8, v2, 0x6

    or-int/lit16 v8, v8, 0xc0

    int-to-byte v8, v8

    aput-byte v8, v5, v0

    .line 811675
    add-int/lit8 v0, v4, 0x1

    and-int/lit8 v2, v2, 0x3f

    or-int/lit16 v2, v2, 0x80

    int-to-byte v2, v2

    aput-byte v2, v5, v4

    move p2, v3

    goto :goto_1

    .line 811676
    :cond_6
    const v4, 0xd800

    if-lt v2, v4, :cond_7

    const v4, 0xdfff

    if-le v2, v4, :cond_8

    .line 811677
    :cond_7
    add-int/lit8 v4, v0, 0x1

    shr-int/lit8 v8, v2, 0xc

    or-int/lit16 v8, v8, 0xe0

    int-to-byte v8, v8

    aput-byte v8, v5, v0

    .line 811678
    add-int/lit8 v8, v4, 0x1

    shr-int/lit8 v0, v2, 0x6

    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    aput-byte v0, v5, v4

    .line 811679
    add-int/lit8 v0, v8, 0x1

    and-int/lit8 v2, v2, 0x3f

    or-int/lit16 v2, v2, 0x80

    int-to-byte v2, v2

    aput-byte v2, v5, v8

    move p2, v3

    .line 811680
    goto :goto_1

    .line 811681
    :cond_8
    const v4, 0xdbff

    if-le v2, v4, :cond_9

    .line 811682
    iput v0, p0, LX/4pf;->e:I

    .line 811683
    invoke-static {v2}, LX/4pf;->c(I)V

    .line 811684
    :cond_9
    iput v2, p0, LX/4pf;->f:I

    .line 811685
    if-ge v3, v7, :cond_b

    .line 811686
    add-int/lit8 p2, v3, 0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-direct {p0, v2}, LX/4pf;->b(I)I

    move-result v2

    .line 811687
    const v3, 0x10ffff

    if-le v2, v3, :cond_a

    .line 811688
    iput v0, p0, LX/4pf;->e:I

    .line 811689
    invoke-static {v2}, LX/4pf;->c(I)V

    .line 811690
    :cond_a
    add-int/lit8 v3, v0, 0x1

    shr-int/lit8 v4, v2, 0x12

    or-int/lit16 v4, v4, 0xf0

    int-to-byte v4, v4

    aput-byte v4, v5, v0

    .line 811691
    add-int/lit8 v0, v3, 0x1

    shr-int/lit8 v4, v2, 0xc

    and-int/lit8 v4, v4, 0x3f

    or-int/lit16 v4, v4, 0x80

    int-to-byte v4, v4

    aput-byte v4, v5, v3

    .line 811692
    add-int/lit8 v3, v0, 0x1

    shr-int/lit8 v4, v2, 0x6

    and-int/lit8 v4, v4, 0x3f

    or-int/lit16 v4, v4, 0x80

    int-to-byte v4, v4

    aput-byte v4, v5, v0

    .line 811693
    add-int/lit8 v0, v3, 0x1

    and-int/lit8 v2, v2, 0x3f

    or-int/lit16 v2, v2, 0x80

    int-to-byte v2, v2

    aput-byte v2, v5, v3

    goto/16 :goto_1

    .line 811694
    :cond_b
    iput v0, p0, LX/4pf;->e:I

    goto/16 :goto_0

    :cond_c
    move p2, v0

    move v0, v2

    goto/16 :goto_1

    :cond_d
    move v0, v2

    goto/16 :goto_2
.end method

.method public final write([C)V
    .locals 2

    .prologue
    .line 811645
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, LX/4pf;->write([CII)V

    .line 811646
    return-void
.end method

.method public final write([CII)V
    .locals 11

    .prologue
    const/16 v9, 0x80

    const/4 v1, 0x0

    .line 811597
    const/4 v0, 0x2

    if-ge p3, v0, :cond_1

    .line 811598
    const/4 v0, 0x1

    if-ne p3, v0, :cond_0

    .line 811599
    aget-char v0, p1, p2

    invoke-virtual {p0, v0}, LX/4pf;->write(I)V

    .line 811600
    :cond_0
    :goto_0
    return-void

    .line 811601
    :cond_1
    iget v0, p0, LX/4pf;->f:I

    if-lez v0, :cond_2

    .line 811602
    add-int/lit8 v0, p2, 0x1

    aget-char v2, p1, p2

    .line 811603
    add-int/lit8 p3, p3, -0x1

    .line 811604
    invoke-direct {p0, v2}, LX/4pf;->b(I)I

    move-result v2

    invoke-virtual {p0, v2}, LX/4pf;->write(I)V

    move p2, v0

    .line 811605
    :cond_2
    iget v0, p0, LX/4pf;->e:I

    .line 811606
    iget-object v5, p0, LX/4pf;->c:[B

    .line 811607
    iget v6, p0, LX/4pf;->d:I

    .line 811608
    add-int v7, p3, p2

    .line 811609
    :goto_1
    if-ge p2, v7, :cond_b

    .line 811610
    if-lt v0, v6, :cond_3

    .line 811611
    iget-object v2, p0, LX/4pf;->b:Ljava/io/OutputStream;

    invoke-virtual {v2, v5, v1, v0}, Ljava/io/OutputStream;->write([BII)V

    move v0, v1

    .line 811612
    :cond_3
    add-int/lit8 v3, p2, 0x1

    aget-char v2, p1, p2

    .line 811613
    if-ge v2, v9, :cond_5

    .line 811614
    add-int/lit8 v4, v0, 0x1

    int-to-byte v2, v2

    aput-byte v2, v5, v0

    .line 811615
    sub-int v2, v7, v3

    .line 811616
    sub-int v0, v6, v4

    .line 811617
    if-le v2, v0, :cond_d

    .line 811618
    :goto_2
    add-int v8, v0, v3

    move v2, v4

    move v0, v3

    .line 811619
    :goto_3
    if-ge v0, v8, :cond_c

    .line 811620
    add-int/lit8 v3, v0, 0x1

    aget-char v0, p1, v0

    .line 811621
    if-ge v0, v9, :cond_4

    .line 811622
    add-int/lit8 v4, v2, 0x1

    int-to-byte v0, v0

    aput-byte v0, v5, v2

    move v2, v4

    move v0, v3

    goto :goto_3

    :cond_4
    move v10, v0

    move v0, v2

    move v2, v10

    .line 811623
    :cond_5
    const/16 v4, 0x800

    if-ge v2, v4, :cond_6

    .line 811624
    add-int/lit8 v4, v0, 0x1

    shr-int/lit8 v8, v2, 0x6

    or-int/lit16 v8, v8, 0xc0

    int-to-byte v8, v8

    aput-byte v8, v5, v0

    .line 811625
    add-int/lit8 v0, v4, 0x1

    and-int/lit8 v2, v2, 0x3f

    or-int/lit16 v2, v2, 0x80

    int-to-byte v2, v2

    aput-byte v2, v5, v4

    move p2, v3

    goto :goto_1

    .line 811626
    :cond_6
    const v4, 0xd800

    if-lt v2, v4, :cond_7

    const v4, 0xdfff

    if-le v2, v4, :cond_8

    .line 811627
    :cond_7
    add-int/lit8 v4, v0, 0x1

    shr-int/lit8 v8, v2, 0xc

    or-int/lit16 v8, v8, 0xe0

    int-to-byte v8, v8

    aput-byte v8, v5, v0

    .line 811628
    add-int/lit8 v8, v4, 0x1

    shr-int/lit8 v0, v2, 0x6

    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    aput-byte v0, v5, v4

    .line 811629
    add-int/lit8 v0, v8, 0x1

    and-int/lit8 v2, v2, 0x3f

    or-int/lit16 v2, v2, 0x80

    int-to-byte v2, v2

    aput-byte v2, v5, v8

    move p2, v3

    .line 811630
    goto :goto_1

    .line 811631
    :cond_8
    const v4, 0xdbff

    if-le v2, v4, :cond_9

    .line 811632
    iput v0, p0, LX/4pf;->e:I

    .line 811633
    invoke-static {v2}, LX/4pf;->c(I)V

    .line 811634
    :cond_9
    iput v2, p0, LX/4pf;->f:I

    .line 811635
    if-ge v3, v7, :cond_b

    .line 811636
    add-int/lit8 p2, v3, 0x1

    aget-char v2, p1, v3

    invoke-direct {p0, v2}, LX/4pf;->b(I)I

    move-result v2

    .line 811637
    const v3, 0x10ffff

    if-le v2, v3, :cond_a

    .line 811638
    iput v0, p0, LX/4pf;->e:I

    .line 811639
    invoke-static {v2}, LX/4pf;->c(I)V

    .line 811640
    :cond_a
    add-int/lit8 v3, v0, 0x1

    shr-int/lit8 v4, v2, 0x12

    or-int/lit16 v4, v4, 0xf0

    int-to-byte v4, v4

    aput-byte v4, v5, v0

    .line 811641
    add-int/lit8 v0, v3, 0x1

    shr-int/lit8 v4, v2, 0xc

    and-int/lit8 v4, v4, 0x3f

    or-int/lit16 v4, v4, 0x80

    int-to-byte v4, v4

    aput-byte v4, v5, v3

    .line 811642
    add-int/lit8 v3, v0, 0x1

    shr-int/lit8 v4, v2, 0x6

    and-int/lit8 v4, v4, 0x3f

    or-int/lit16 v4, v4, 0x80

    int-to-byte v4, v4

    aput-byte v4, v5, v0

    .line 811643
    add-int/lit8 v0, v3, 0x1

    and-int/lit8 v2, v2, 0x3f

    or-int/lit16 v2, v2, 0x80

    int-to-byte v2, v2

    aput-byte v2, v5, v3

    goto/16 :goto_1

    .line 811644
    :cond_b
    iput v0, p0, LX/4pf;->e:I

    goto/16 :goto_0

    :cond_c
    move p2, v0

    move v0, v2

    goto/16 :goto_1

    :cond_d
    move v0, v2

    goto/16 :goto_2
.end method
