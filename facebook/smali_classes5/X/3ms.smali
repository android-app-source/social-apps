.class public final LX/3ms;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/3mZ;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/3mr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3mZ",
            "<TE;>.GroupsYouShouldJoinPageComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/3mZ;

.field private c:[Ljava/lang/String;

.field private d:I

.field public e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/3mZ;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 637373
    iput-object p1, p0, LX/3ms;->b:LX/3mZ;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 637374
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "gysjProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/3ms;->c:[Ljava/lang/String;

    .line 637375
    iput v3, p0, LX/3ms;->d:I

    .line 637376
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/3ms;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/3ms;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/3ms;LX/1De;IILX/3mr;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/3mZ",
            "<TE;>.GroupsYouShouldJoinPageComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 637369
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 637370
    iput-object p4, p0, LX/3ms;->a:LX/3mr;

    .line 637371
    iget-object v0, p0, LX/3ms;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 637372
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 637365
    invoke-super {p0}, LX/1X5;->a()V

    .line 637366
    const/4 v0, 0x0

    iput-object v0, p0, LX/3ms;->a:LX/3mr;

    .line 637367
    iget-object v0, p0, LX/3ms;->b:LX/3mZ;

    iget-object v0, v0, LX/3mZ;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 637368
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/3mZ;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 637355
    iget-object v1, p0, LX/3ms;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/3ms;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/3ms;->d:I

    if-ge v1, v2, :cond_2

    .line 637356
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 637357
    :goto_0
    iget v2, p0, LX/3ms;->d:I

    if-ge v0, v2, :cond_1

    .line 637358
    iget-object v2, p0, LX/3ms;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 637359
    iget-object v2, p0, LX/3ms;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 637360
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 637361
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 637362
    :cond_2
    iget-object v0, p0, LX/3ms;->a:LX/3mr;

    .line 637363
    invoke-virtual {p0}, LX/3ms;->a()V

    .line 637364
    return-object v0
.end method
