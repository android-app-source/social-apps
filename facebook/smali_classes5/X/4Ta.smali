.class public LX/4Ta;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 718946
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 28

    .prologue
    .line 718756
    const/16 v24, 0x0

    .line 718757
    const/16 v21, 0x0

    .line 718758
    const-wide/16 v22, 0x0

    .line 718759
    const/16 v20, 0x0

    .line 718760
    const/16 v19, 0x0

    .line 718761
    const/16 v18, 0x0

    .line 718762
    const/16 v17, 0x0

    .line 718763
    const/16 v16, 0x0

    .line 718764
    const/4 v15, 0x0

    .line 718765
    const/4 v14, 0x0

    .line 718766
    const/4 v13, 0x0

    .line 718767
    const/4 v12, 0x0

    .line 718768
    const/4 v11, 0x0

    .line 718769
    const/4 v10, 0x0

    .line 718770
    const/4 v9, 0x0

    .line 718771
    const/4 v8, 0x0

    .line 718772
    const/4 v7, 0x0

    .line 718773
    const/4 v6, 0x0

    .line 718774
    const/4 v5, 0x0

    .line 718775
    const/4 v4, 0x0

    .line 718776
    const/4 v3, 0x0

    .line 718777
    const/4 v2, 0x0

    .line 718778
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v25

    sget-object v26, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_18

    .line 718779
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 718780
    const/4 v2, 0x0

    .line 718781
    :goto_0
    return v2

    .line 718782
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_14

    .line 718783
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 718784
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 718785
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v27, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v27

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 718786
    const-string v6, "cache_id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 718787
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v26, v2

    goto :goto_1

    .line 718788
    :cond_1
    const-string v6, "debug_info"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 718789
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 718790
    :cond_2
    const-string v6, "fetchTimeMs"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 718791
    const/4 v2, 0x1

    .line 718792
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 718793
    :cond_3
    const-string v6, "hideable_token"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 718794
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v25, v2

    goto :goto_1

    .line 718795
    :cond_4
    const-string v6, "local_last_negative_feedback_action_type"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 718796
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v24, v2

    goto :goto_1

    .line 718797
    :cond_5
    const-string v6, "local_story_visibility"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 718798
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 718799
    :cond_6
    const-string v6, "local_story_visible_height"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 718800
    const/4 v2, 0x1

    .line 718801
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v9, v2

    move/from16 v22, v6

    goto/16 :goto_1

    .line 718802
    :cond_7
    const-string v6, "short_term_cache_key"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 718803
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 718804
    :cond_8
    const-string v6, "sponsored_data"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 718805
    invoke-static/range {p0 .. p1}, LX/2uY;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 718806
    :cond_9
    const-string v6, "surveyActor"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 718807
    invoke-static/range {p0 .. p1}, LX/2bM;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 718808
    :cond_a
    const-string v6, "surveyContent"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 718809
    invoke-static/range {p0 .. p1}, LX/4TP;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 718810
    :cond_b
    const-string v6, "surveyHideableToken"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 718811
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 718812
    :cond_c
    const-string v6, "surveyResponse"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 718813
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 718814
    :cond_d
    const-string v6, "surveySponsoredData"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 718815
    invoke-static/range {p0 .. p1}, LX/2uY;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 718816
    :cond_e
    const-string v6, "surveyTitle"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 718817
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 718818
    :cond_f
    const-string v6, "surveyTracking"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 718819
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 718820
    :cond_10
    const-string v6, "title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 718821
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 718822
    :cond_11
    const-string v6, "tracking"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 718823
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 718824
    :cond_12
    const-string v6, "local_is_completed"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 718825
    const/4 v2, 0x1

    .line 718826
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v8, v2

    move v10, v6

    goto/16 :goto_1

    .line 718827
    :cond_13
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 718828
    :cond_14
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 718829
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 718830
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 718831
    if-eqz v3, :cond_15

    .line 718832
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 718833
    :cond_15
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 718834
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 718835
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 718836
    if-eqz v9, :cond_16

    .line 718837
    const/4 v2, 0x6

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 718838
    :cond_16
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 718839
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 718840
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 718841
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 718842
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 718843
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 718844
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 718845
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 718846
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 718847
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 718848
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 718849
    if-eqz v8, :cond_17

    .line 718850
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->a(IZ)V

    .line 718851
    :cond_17
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_18
    move/from16 v25, v20

    move/from16 v26, v24

    move/from16 v20, v15

    move/from16 v24, v19

    move v15, v10

    move/from16 v19, v14

    move v14, v9

    move v10, v5

    move v9, v3

    move v3, v4

    move-wide/from16 v4, v22

    move/from16 v22, v17

    move/from16 v23, v18

    move/from16 v18, v13

    move/from16 v17, v12

    move v12, v7

    move v13, v8

    move v8, v2

    move/from16 v7, v21

    move/from16 v21, v16

    move/from16 v16, v11

    move v11, v6

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 718852
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 718853
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 718854
    invoke-static {p0, v2}, LX/4Ta;->a(LX/15w;LX/186;)I

    move-result v1

    .line 718855
    if-eqz v0, :cond_0

    .line 718856
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 718857
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 718858
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 718859
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 718860
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 718861
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 718862
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 718863
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 718864
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718865
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 718866
    const-string v0, "name"

    const-string v1, "SurveyFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 718867
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 718868
    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 718869
    if-eqz v0, :cond_0

    .line 718870
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718871
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 718872
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 718873
    if-eqz v0, :cond_1

    .line 718874
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718875
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 718876
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 718877
    cmp-long v2, v0, v4

    if-eqz v2, :cond_2

    .line 718878
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718879
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 718880
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 718881
    if-eqz v0, :cond_3

    .line 718882
    const-string v1, "hideable_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718883
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 718884
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 718885
    if-eqz v0, :cond_4

    .line 718886
    const-string v1, "local_last_negative_feedback_action_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718887
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 718888
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 718889
    if-eqz v0, :cond_5

    .line 718890
    const-string v1, "local_story_visibility"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718891
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 718892
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 718893
    if-eqz v0, :cond_6

    .line 718894
    const-string v1, "local_story_visible_height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718895
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 718896
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 718897
    if-eqz v0, :cond_7

    .line 718898
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718899
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 718900
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 718901
    if-eqz v0, :cond_8

    .line 718902
    const-string v1, "sponsored_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718903
    invoke-static {p0, v0, p2, p3}, LX/2uY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 718904
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 718905
    if-eqz v0, :cond_9

    .line 718906
    const-string v1, "surveyActor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718907
    invoke-static {p0, v0, p2, p3}, LX/2bM;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 718908
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 718909
    if-eqz v0, :cond_a

    .line 718910
    const-string v1, "surveyContent"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718911
    invoke-static {p0, v0, p2, p3}, LX/4TP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 718912
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 718913
    if-eqz v0, :cond_b

    .line 718914
    const-string v1, "surveyHideableToken"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718915
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 718916
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 718917
    if-eqz v0, :cond_c

    .line 718918
    const-string v1, "surveyResponse"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718919
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 718920
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 718921
    if-eqz v0, :cond_d

    .line 718922
    const-string v1, "surveySponsoredData"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718923
    invoke-static {p0, v0, p2, p3}, LX/2uY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 718924
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 718925
    if-eqz v0, :cond_e

    .line 718926
    const-string v1, "surveyTitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718927
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 718928
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 718929
    if-eqz v0, :cond_f

    .line 718930
    const-string v1, "surveyTracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718931
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 718932
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 718933
    if-eqz v0, :cond_10

    .line 718934
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718935
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 718936
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 718937
    if-eqz v0, :cond_11

    .line 718938
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718939
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 718940
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 718941
    if-eqz v0, :cond_12

    .line 718942
    const-string v1, "local_is_completed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718943
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 718944
    :cond_12
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 718945
    return-void
.end method
