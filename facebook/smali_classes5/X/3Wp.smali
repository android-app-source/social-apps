.class public LX/3Wp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/3Wp;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 591413
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 591414
    return-void
.end method

.method public static a(LX/0QB;)LX/3Wp;
    .locals 3

    .prologue
    .line 591401
    sget-object v0, LX/3Wp;->a:LX/3Wp;

    if-nez v0, :cond_1

    .line 591402
    const-class v1, LX/3Wp;

    monitor-enter v1

    .line 591403
    :try_start_0
    sget-object v0, LX/3Wp;->a:LX/3Wp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 591404
    if-eqz v2, :cond_0

    .line 591405
    :try_start_1
    new-instance v0, LX/3Wp;

    invoke-direct {v0}, LX/3Wp;-><init>()V

    .line 591406
    move-object v0, v0

    .line 591407
    sput-object v0, LX/3Wp;->a:LX/3Wp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 591408
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 591409
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 591410
    :cond_1
    sget-object v0, LX/3Wp;->a:LX/3Wp;

    return-object v0

    .line 591411
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 591412
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 16

    .prologue
    .line 591396
    sget-object v1, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->p:LX/1Cz;

    sget-object v2, Lcom/facebook/feedback/ui/rows/CommentOfflineStatusPartDefinition;->a:LX/1Cz;

    sget-object v3, Lcom/facebook/feedback/ui/rows/CommentActionsPartDefinition;->a:LX/1Cz;

    sget-object v4, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->a:LX/1Cz;

    sget-object v5, Lcom/facebook/feedback/ui/rows/OriginalPostButtonPartDefinition;->a:LX/1Cz;

    sget-object v6, Lcom/facebook/feedback/ui/rows/TypingIndicatorPartDefinition;->a:LX/1Cz;

    sget-object v7, Lcom/facebook/feedback/ui/rows/CommentPhotoAttachmentPartDefinition;->a:LX/1Cz;

    sget-object v8, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;->a:LX/1Cz;

    sget-object v9, Lcom/facebook/feedback/ui/rows/CommentShareAttachmentPartDefinition;->a:LX/1Cz;

    sget-object v10, Lcom/facebook/feedback/ui/rows/CommentStickerAttachmentPartDefinition;->a:LX/1Cz;

    sget-object v11, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;->a:LX/1Cz;

    sget-object v12, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;->a:LX/1Cz;

    const/16 v13, 0xa

    new-array v13, v13, [LX/1Cz;

    const/4 v14, 0x0

    sget-object v15, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x1

    sget-object v15, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x2

    sget-object v15, Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x3

    sget-object v15, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x4

    sget-object v15, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x5

    sget-object v15, Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x6

    sget-object v15, Lcom/facebook/feedback/ui/rows/InlineReplyComposerPartDefinition;->b:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x7

    sget-object v15, Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x8

    sget-object v15, Lcom/facebook/feedback/ui/rows/InlineReplyDraftPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x9

    sget-object v15, Lcom/facebook/feedback/ui/rows/EscapeHatchPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    invoke-static/range {v1 .. v13}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 591397
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Cz;

    .line 591398
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, LX/1KB;->a(LX/1Cz;)V

    .line 591399
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 591400
    :cond_0
    return-void
.end method
