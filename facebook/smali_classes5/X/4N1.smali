.class public LX/4N1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 690042
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 23

    .prologue
    .line 690043
    const/4 v15, 0x0

    .line 690044
    const-wide/16 v16, 0x0

    .line 690045
    const/4 v14, 0x0

    .line 690046
    const/4 v13, 0x0

    .line 690047
    const/4 v12, 0x0

    .line 690048
    const-wide/16 v10, 0x0

    .line 690049
    const/4 v9, 0x0

    .line 690050
    const/4 v8, 0x0

    .line 690051
    const/4 v7, 0x0

    .line 690052
    const/4 v6, 0x0

    .line 690053
    const/4 v5, 0x0

    .line 690054
    const/4 v4, 0x0

    .line 690055
    const/4 v3, 0x0

    .line 690056
    const/4 v2, 0x0

    .line 690057
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_10

    .line 690058
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 690059
    const/4 v2, 0x0

    .line 690060
    :goto_0
    return v2

    .line 690061
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_c

    .line 690062
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 690063
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 690064
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 690065
    const-string v6, "cache_id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 690066
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto :goto_1

    .line 690067
    :cond_1
    const-string v6, "creation_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 690068
    const/4 v2, 0x1

    .line 690069
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 690070
    :cond_2
    const-string v6, "debug_info"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 690071
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto :goto_1

    .line 690072
    :cond_3
    const-string v6, "description"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 690073
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 690074
    :cond_4
    const-string v6, "feed_type"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 690075
    const/4 v2, 0x1

    .line 690076
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    move-result-object v6

    move v9, v2

    move-object v15, v6

    goto :goto_1

    .line 690077
    :cond_5
    const-string v6, "fetchTimeMs"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 690078
    const/4 v2, 0x1

    .line 690079
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v8, v2

    move-wide/from16 v16, v6

    goto/16 :goto_1

    .line 690080
    :cond_6
    const-string v6, "footer"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 690081
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 690082
    :cond_7
    const-string v6, "friendsLocationsItems"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 690083
    invoke-static/range {p0 .. p1}, LX/4Mw;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 690084
    :cond_8
    const-string v6, "short_term_cache_key"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 690085
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 690086
    :cond_9
    const-string v6, "title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 690087
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 690088
    :cond_a
    const-string v6, "tracking"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 690089
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 690090
    :cond_b
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 690091
    :cond_c
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 690092
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 690093
    if-eqz v3, :cond_d

    .line 690094
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 690095
    :cond_d
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 690096
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 690097
    if-eqz v9, :cond_e

    .line 690098
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->a(ILjava/lang/Enum;)V

    .line 690099
    :cond_e
    if-eqz v8, :cond_f

    .line 690100
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 690101
    :cond_f
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 690102
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 690103
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 690104
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 690105
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 690106
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_10
    move/from16 v18, v13

    move/from16 v19, v14

    move/from16 v20, v15

    move v13, v8

    move v14, v9

    move-object v15, v12

    move v12, v7

    move v8, v2

    move v9, v3

    move v3, v4

    move-wide/from16 v21, v10

    move v11, v6

    move v10, v5

    move-wide/from16 v4, v16

    move-wide/from16 v16, v21

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 690107
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 690108
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 690109
    invoke-static {p0, v2}, LX/4N1;->a(LX/15w;LX/186;)I

    move-result v1

    .line 690110
    if-eqz v0, :cond_0

    .line 690111
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 690112
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 690113
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 690114
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 690115
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 690116
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 690117
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    .line 690118
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 690119
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690120
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 690121
    const-string v0, "name"

    const-string v1, "FriendsLocationsFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 690122
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 690123
    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 690124
    if-eqz v0, :cond_0

    .line 690125
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690126
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 690127
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 690128
    cmp-long v2, v0, v4

    if-eqz v2, :cond_1

    .line 690129
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690130
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 690131
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 690132
    if-eqz v0, :cond_2

    .line 690133
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690134
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 690135
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 690136
    if-eqz v0, :cond_3

    .line 690137
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690138
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 690139
    :cond_3
    invoke-virtual {p0, p1, v6, v3}, LX/15i;->a(IIS)S

    move-result v0

    .line 690140
    if-eqz v0, :cond_4

    .line 690141
    const-string v0, "feed_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690142
    const-class v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    invoke-virtual {p0, p1, v6, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 690143
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 690144
    cmp-long v2, v0, v4

    if-eqz v2, :cond_5

    .line 690145
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690146
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 690147
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 690148
    if-eqz v0, :cond_6

    .line 690149
    const-string v1, "footer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690150
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 690151
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 690152
    if-eqz v0, :cond_8

    .line 690153
    const-string v1, "friendsLocationsItems"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690154
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 690155
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_7

    .line 690156
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/4Mw;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 690157
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 690158
    :cond_7
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 690159
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 690160
    if-eqz v0, :cond_9

    .line 690161
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690162
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 690163
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 690164
    if-eqz v0, :cond_a

    .line 690165
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690166
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 690167
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 690168
    if-eqz v0, :cond_b

    .line 690169
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690170
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 690171
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 690172
    return-void
.end method
