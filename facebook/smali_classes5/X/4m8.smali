.class public LX/4m8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Z

.field public e:I

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 805429
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 805430
    iput-object v0, p0, LX/4m8;->a:Ljava/lang/String;

    .line 805431
    iput-object v0, p0, LX/4m8;->b:Ljava/lang/String;

    .line 805432
    const-string v0, ""

    iput-object v0, p0, LX/4m8;->c:Ljava/lang/String;

    .line 805433
    iput-boolean v1, p0, LX/4m8;->d:Z

    .line 805434
    iput v1, p0, LX/4m8;->e:I

    .line 805435
    const-string v0, ""

    iput-object v0, p0, LX/4m8;->f:Ljava/lang/String;

    .line 805436
    return-void
.end method

.method public static newBuilder()LX/4m8;
    .locals 1

    .prologue
    .line 805437
    new-instance v0, LX/4m8;

    invoke-direct {v0}, LX/4m8;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/stickers/model/StickerTag;
    .locals 7

    .prologue
    .line 805428
    new-instance v0, Lcom/facebook/stickers/model/StickerTag;

    iget-object v1, p0, LX/4m8;->a:Ljava/lang/String;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, LX/4m8;->b:Ljava/lang/String;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, LX/4m8;->c:Ljava/lang/String;

    iget-boolean v4, p0, LX/4m8;->d:Z

    iget v5, p0, LX/4m8;->e:I

    iget-object v6, p0, LX/4m8;->f:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/stickers/model/StickerTag;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;)V

    return-object v0
.end method
