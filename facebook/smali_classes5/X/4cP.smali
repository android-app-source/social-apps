.class public LX/4cP;
.super LX/4cO;
.source ""


# instance fields
.field private final b:LX/0nA;

.field private final c:Ljava/nio/charset/Charset;


# direct methods
.method public constructor <init>(LX/0nA;Ljava/lang/String;Ljava/nio/charset/Charset;)V
    .locals 0

    .prologue
    .line 794961
    invoke-direct {p0, p2}, LX/4cO;-><init>(Ljava/lang/String;)V

    .line 794962
    iput-object p1, p0, LX/4cP;->b:LX/0nA;

    .line 794963
    iput-object p3, p0, LX/4cP;->c:Ljava/nio/charset/Charset;

    .line 794964
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 794965
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 794966
    new-instance v0, Ljava/io/OutputStreamWriter;

    invoke-direct {v0, p1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    .line 794967
    iget-object v1, p0, LX/4cP;->b:LX/0nA;

    invoke-virtual {v1, v0}, LX/0nA;->a(Ljava/io/Writer;)V

    .line 794968
    invoke-virtual {v0}, Ljava/io/OutputStreamWriter;->flush()V

    .line 794969
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 794970
    iget-object v0, p0, LX/4cP;->c:Ljava/nio/charset/Charset;

    invoke-virtual {v0}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 794971
    const-string v0, "8bit"

    return-object v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 794972
    const-wide/16 v0, -0x1

    return-wide v0
.end method
