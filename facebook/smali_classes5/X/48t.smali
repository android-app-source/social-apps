.class public LX/48t;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I

.field private static final d:I

.field private static final e:I

.field private static final f:I

.field private static final g:[I

.field private static final h:[I

.field private static final i:[I

.field private static final j:[I

.field private static final k:[I

.field private static final l:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 673932
    sget-object v0, Lcom/facebook/csslayout/YogaFlexDirection;->COLUMN:Lcom/facebook/csslayout/YogaFlexDirection;

    invoke-virtual {v0}, Lcom/facebook/csslayout/YogaFlexDirection;->ordinal()I

    move-result v0

    sput v0, LX/48t;->a:I

    .line 673933
    sget-object v0, Lcom/facebook/csslayout/YogaFlexDirection;->COLUMN_REVERSE:Lcom/facebook/csslayout/YogaFlexDirection;

    invoke-virtual {v0}, Lcom/facebook/csslayout/YogaFlexDirection;->ordinal()I

    move-result v0

    sput v0, LX/48t;->b:I

    .line 673934
    sget-object v0, Lcom/facebook/csslayout/YogaFlexDirection;->ROW:Lcom/facebook/csslayout/YogaFlexDirection;

    invoke-virtual {v0}, Lcom/facebook/csslayout/YogaFlexDirection;->ordinal()I

    move-result v0

    sput v0, LX/48t;->c:I

    .line 673935
    sget-object v0, Lcom/facebook/csslayout/YogaFlexDirection;->ROW_REVERSE:Lcom/facebook/csslayout/YogaFlexDirection;

    invoke-virtual {v0}, Lcom/facebook/csslayout/YogaFlexDirection;->ordinal()I

    move-result v0

    sput v0, LX/48t;->d:I

    .line 673936
    sget-object v0, Lcom/facebook/csslayout/YogaPositionType;->RELATIVE:Lcom/facebook/csslayout/YogaPositionType;

    invoke-virtual {v0}, Lcom/facebook/csslayout/YogaPositionType;->ordinal()I

    move-result v0

    sput v0, LX/48t;->e:I

    .line 673937
    sget-object v0, Lcom/facebook/csslayout/YogaPositionType;->ABSOLUTE:Lcom/facebook/csslayout/YogaPositionType;

    invoke-virtual {v0}, Lcom/facebook/csslayout/YogaPositionType;->ordinal()I

    move-result v0

    sput v0, LX/48t;->f:I

    .line 673938
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/48t;->g:[I

    .line 673939
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, LX/48t;->h:[I

    .line 673940
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, LX/48t;->i:[I

    .line 673941
    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, LX/48t;->j:[I

    .line 673942
    new-array v0, v1, [I

    fill-array-data v0, :array_4

    sput-object v0, LX/48t;->k:[I

    .line 673943
    new-array v0, v1, [I

    fill-array-data v0, :array_5

    sput-object v0, LX/48t;->l:[I

    return-void

    .line 673944
    :array_0
    .array-data 4
        0x1
        0x3
        0x0
        0x2
    .end array-data

    .line 673945
    :array_1
    .array-data 4
        0x3
        0x1
        0x2
        0x0
    .end array-data

    .line 673946
    :array_2
    .array-data 4
        0x1
        0x3
        0x0
        0x2
    .end array-data

    .line 673947
    :array_3
    .array-data 4
        0x1
        0x1
        0x0
        0x0
    .end array-data

    .line 673948
    :array_4
    .array-data 4
        0x1
        0x3
        0x4
        0x4
    .end array-data

    .line 673949
    :array_5
    .array-data 4
        0x3
        0x1
        0x5
        0x5
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 673950
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/48q;I)F
    .locals 3

    .prologue
    .line 673951
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->p:LX/1mz;

    sget-object v1, LX/48t;->k:[I

    aget v1, v1, p1

    sget-object v2, LX/48t;->g:[I

    aget v2, v2, p1

    invoke-virtual {v0, v1, v2}, LX/1mz;->a(II)F

    move-result v0

    .line 673952
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_0

    .line 673953
    :goto_0
    return v0

    .line 673954
    :cond_0
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->p:LX/1mz;

    sget-object v1, LX/48t;->l:[I

    aget v1, v1, p1

    sget-object v2, LX/48t;->h:[I

    aget v2, v2, p1

    invoke-virtual {v0, v1, v2}, LX/1mz;->a(II)F

    move-result v0

    .line 673955
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    neg-float v0, v0

    goto :goto_0
.end method

.method private static a(LX/48q;IF)F
    .locals 6

    .prologue
    const/high16 v0, 0x7fc00000    # NaNf

    const-wide/16 v4, 0x0

    .line 673460
    sget v1, LX/48t;->a:I

    if-eq p1, v1, :cond_0

    sget v1, LX/48t;->b:I

    if-ne p1, v1, :cond_3

    .line 673461
    :cond_0
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget v1, v0, LX/48r;->s:F

    .line 673462
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget v0, v0, LX/48r;->u:F

    .line 673463
    :goto_0
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_1

    float-to-double v2, v0

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_1

    cmpl-float v2, p2, v0

    if-lez v2, :cond_1

    move p2, v0

    .line 673464
    :cond_1
    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_2

    float-to-double v2, v1

    cmpl-double v0, v2, v4

    if-ltz v0, :cond_2

    cmpg-float v0, p2, v1

    if-gez v0, :cond_2

    move p2, v1

    .line 673465
    :cond_2
    return p2

    .line 673466
    :cond_3
    sget v1, LX/48t;->c:I

    if-eq p1, v1, :cond_4

    sget v1, LX/48t;->d:I

    if-ne p1, v1, :cond_5

    .line 673467
    :cond_4
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget v1, v0, LX/48r;->r:F

    .line 673468
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget v0, v0, LX/48r;->t:F

    goto :goto_0

    :cond_5
    move v1, v0

    goto :goto_0
.end method

.method private static a(ILcom/facebook/csslayout/YogaDirection;)I
    .locals 1

    .prologue
    .line 673990
    sget-object v0, Lcom/facebook/csslayout/YogaDirection;->RTL:Lcom/facebook/csslayout/YogaDirection;

    if-ne p1, v0, :cond_0

    .line 673991
    sget v0, LX/48t;->c:I

    if-ne p0, v0, :cond_1

    .line 673992
    sget p0, LX/48t;->d:I

    .line 673993
    :cond_0
    :goto_0
    return p0

    .line 673994
    :cond_1
    sget v0, LX/48t;->d:I

    if-ne p0, v0, :cond_0

    .line 673995
    sget p0, LX/48t;->c:I

    goto :goto_0
.end method

.method private static a(LX/48q;LX/48q;)Lcom/facebook/csslayout/YogaAlign;
    .locals 2

    .prologue
    .line 673956
    iget-object v0, p1, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->f:Lcom/facebook/csslayout/YogaAlign;

    sget-object v1, Lcom/facebook/csslayout/YogaAlign;->AUTO:Lcom/facebook/csslayout/YogaAlign;

    if-eq v0, v1, :cond_0

    .line 673957
    iget-object v0, p1, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->f:Lcom/facebook/csslayout/YogaAlign;

    .line 673958
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->e:Lcom/facebook/csslayout/YogaAlign;

    goto :goto_0
.end method

.method public static a(LX/1Dq;LX/48q;FFLcom/facebook/csslayout/YogaDirection;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v7, 0x1

    const-wide/16 v8, 0x0

    .line 673959
    iget v0, p0, LX/1Dq;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1Dq;->b:I

    .line 673960
    sget-object v5, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    .line 673961
    sget-object v6, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    .line 673962
    invoke-static {p2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_1

    .line 673963
    sget-object v5, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    move v2, p2

    .line 673964
    :goto_0
    invoke-static {p3}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_3

    .line 673965
    sget-object v6, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    move v3, p3

    :goto_1
    move-object v0, p0

    move-object v1, p1

    move-object v4, p4

    .line 673966
    invoke-static/range {v0 .. v7}, LX/48t;->a(LX/1Dq;LX/48q;FFLcom/facebook/csslayout/YogaDirection;Lcom/facebook/csslayout/YogaMeasureMode;Lcom/facebook/csslayout/YogaMeasureMode;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 673967
    iget-object v0, p1, LX/48q;->b:LX/48o;

    iget-object v0, v0, LX/48o;->c:Lcom/facebook/csslayout/YogaDirection;

    invoke-static {p1, v0}, LX/48t;->a(LX/48q;Lcom/facebook/csslayout/YogaDirection;)V

    .line 673968
    :cond_0
    return-void

    .line 673969
    :cond_1
    iget-object v0, p1, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->q:[F

    aget v0, v0, v10

    float-to-double v0, v0

    cmpl-double v0, v0, v8

    if-ltz v0, :cond_2

    .line 673970
    iget-object v0, p1, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->m:LX/1mz;

    sget-object v1, LX/48t;->k:[I

    sget v2, LX/48t;->c:I

    aget v1, v1, v2

    sget-object v2, LX/48t;->g:[I

    sget v3, LX/48t;->c:I

    aget v2, v2, v3

    invoke-virtual {v0, v1, v2}, LX/1mz;->a(II)F

    move-result v0

    iget-object v1, p1, LX/48q;->a:LX/48r;

    iget-object v1, v1, LX/48r;->m:LX/1mz;

    sget-object v2, LX/48t;->l:[I

    sget v3, LX/48t;->c:I

    aget v2, v2, v3

    sget-object v3, LX/48t;->h:[I

    sget v4, LX/48t;->c:I

    aget v3, v3, v4

    invoke-virtual {v1, v2, v3}, LX/1mz;->a(II)F

    move-result v1

    add-float/2addr v0, v1

    .line 673971
    iget-object v1, p1, LX/48q;->a:LX/48r;

    iget-object v1, v1, LX/48r;->q:[F

    aget v1, v1, v10

    add-float p2, v1, v0

    .line 673972
    sget-object v5, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    move v2, p2

    .line 673973
    goto :goto_0

    :cond_2
    iget-object v0, p1, LX/48q;->a:LX/48r;

    iget v0, v0, LX/48r;->t:F

    float-to-double v0, v0

    cmpl-double v0, v0, v8

    if-ltz v0, :cond_6

    .line 673974
    iget-object v0, p1, LX/48q;->a:LX/48r;

    iget p2, v0, LX/48r;->t:F

    .line 673975
    sget-object v5, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    move v2, p2

    goto :goto_0

    .line 673976
    :cond_3
    iget-object v0, p1, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->q:[F

    aget v0, v0, v7

    float-to-double v0, v0

    cmpl-double v0, v0, v8

    if-ltz v0, :cond_4

    .line 673977
    iget-object v0, p1, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->m:LX/1mz;

    sget-object v1, LX/48t;->k:[I

    sget v3, LX/48t;->a:I

    aget v1, v1, v3

    sget-object v3, LX/48t;->g:[I

    sget v4, LX/48t;->a:I

    aget v3, v3, v4

    invoke-virtual {v0, v1, v3}, LX/1mz;->a(II)F

    move-result v0

    iget-object v1, p1, LX/48q;->a:LX/48r;

    iget-object v1, v1, LX/48r;->m:LX/1mz;

    sget-object v3, LX/48t;->l:[I

    sget v4, LX/48t;->a:I

    aget v3, v3, v4

    sget-object v4, LX/48t;->h:[I

    sget v6, LX/48t;->a:I

    aget v4, v4, v6

    invoke-virtual {v1, v3, v4}, LX/1mz;->a(II)F

    move-result v1

    add-float/2addr v0, v1

    .line 673978
    iget-object v1, p1, LX/48q;->a:LX/48r;

    iget-object v1, v1, LX/48r;->q:[F

    aget v1, v1, v7

    add-float p3, v1, v0

    .line 673979
    sget-object v6, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    move v3, p3

    .line 673980
    goto/16 :goto_1

    :cond_4
    iget-object v0, p1, LX/48q;->a:LX/48r;

    iget v0, v0, LX/48r;->u:F

    float-to-double v0, v0

    cmpl-double v0, v0, v8

    if-ltz v0, :cond_5

    .line 673981
    iget-object v0, p1, LX/48q;->a:LX/48r;

    iget p3, v0, LX/48r;->u:F

    .line 673982
    sget-object v6, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    move v3, p3

    goto/16 :goto_1

    :cond_5
    move v3, p3

    goto/16 :goto_1

    :cond_6
    move v2, p2

    goto/16 :goto_0
.end method

.method private static a(LX/48q;Lcom/facebook/csslayout/YogaDirection;)V
    .locals 7

    .prologue
    .line 673983
    invoke-static {p0}, LX/48t;->d(LX/48q;)I

    move-result v0

    invoke-static {v0, p1}, LX/48t;->a(ILcom/facebook/csslayout/YogaDirection;)I

    move-result v0

    .line 673984
    invoke-static {v0, p1}, LX/48t;->b(ILcom/facebook/csslayout/YogaDirection;)I

    move-result v1

    .line 673985
    iget-object v2, p0, LX/48q;->b:LX/48o;

    iget-object v2, v2, LX/48o;->a:[F

    sget-object v3, LX/48t;->g:[I

    aget v3, v3, v0

    iget-object v4, p0, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->m:LX/1mz;

    sget-object v5, LX/48t;->k:[I

    aget v5, v5, v0

    sget-object v6, LX/48t;->g:[I

    aget v6, v6, v0

    invoke-virtual {v4, v5, v6}, LX/1mz;->a(II)F

    move-result v4

    invoke-static {p0, v0}, LX/48t;->a(LX/48q;I)F

    move-result v5

    add-float/2addr v4, v5

    aput v4, v2, v3

    .line 673986
    iget-object v2, p0, LX/48q;->b:LX/48o;

    iget-object v2, v2, LX/48o;->a:[F

    sget-object v3, LX/48t;->h:[I

    aget v3, v3, v0

    iget-object v4, p0, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->m:LX/1mz;

    sget-object v5, LX/48t;->l:[I

    aget v5, v5, v0

    sget-object v6, LX/48t;->h:[I

    aget v6, v6, v0

    invoke-virtual {v4, v5, v6}, LX/1mz;->a(II)F

    move-result v4

    invoke-static {p0, v0}, LX/48t;->a(LX/48q;I)F

    move-result v0

    add-float/2addr v0, v4

    aput v0, v2, v3

    .line 673987
    iget-object v0, p0, LX/48q;->b:LX/48o;

    iget-object v0, v0, LX/48o;->a:[F

    sget-object v2, LX/48t;->g:[I

    aget v2, v2, v1

    iget-object v3, p0, LX/48q;->a:LX/48r;

    iget-object v3, v3, LX/48r;->m:LX/1mz;

    sget-object v4, LX/48t;->k:[I

    aget v4, v4, v1

    sget-object v5, LX/48t;->g:[I

    aget v5, v5, v1

    invoke-virtual {v3, v4, v5}, LX/1mz;->a(II)F

    move-result v3

    invoke-static {p0, v1}, LX/48t;->a(LX/48q;I)F

    move-result v4

    add-float/2addr v3, v4

    aput v3, v0, v2

    .line 673988
    iget-object v0, p0, LX/48q;->b:LX/48o;

    iget-object v0, v0, LX/48o;->a:[F

    sget-object v2, LX/48t;->h:[I

    aget v2, v2, v1

    iget-object v3, p0, LX/48q;->a:LX/48r;

    iget-object v3, v3, LX/48r;->m:LX/1mz;

    sget-object v4, LX/48t;->l:[I

    aget v4, v4, v1

    sget-object v5, LX/48t;->h:[I

    aget v5, v5, v1

    invoke-virtual {v3, v4, v5}, LX/1mz;->a(II)F

    move-result v3

    invoke-static {p0, v1}, LX/48t;->a(LX/48q;I)F

    move-result v1

    add-float/2addr v1, v3

    aput v1, v0, v2

    .line 673989
    return-void
.end method

.method private static a(FFFFLcom/facebook/csslayout/YogaMeasureMode;Lcom/facebook/csslayout/YogaMeasureMode;LX/48n;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 673864
    iget-object v0, p6, LX/48n;->d:Lcom/facebook/csslayout/YogaMeasureMode;

    sget-object v3, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    if-ne v0, v3, :cond_0

    sget-object v0, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    if-eq p5, v0, :cond_1

    :cond_0
    iget-object v0, p6, LX/48n;->d:Lcom/facebook/csslayout/YogaMeasureMode;

    if-ne v0, p5, :cond_5

    iget v0, p6, LX/48n;->b:F

    invoke-static {v0, p1}, LX/3Fp;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    move v0, v2

    .line 673865
    :goto_0
    iget-object v3, p6, LX/48n;->c:Lcom/facebook/csslayout/YogaMeasureMode;

    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    if-ne v3, v4, :cond_2

    sget-object v3, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    if-eq p4, v3, :cond_3

    :cond_2
    iget-object v3, p6, LX/48n;->c:Lcom/facebook/csslayout/YogaMeasureMode;

    if-ne v3, p4, :cond_6

    iget v3, p6, LX/48n;->a:F

    invoke-static {v3, p0}, LX/3Fp;->a(FF)Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_3
    move v3, v2

    .line 673866
    :goto_1
    if-eqz v0, :cond_7

    if-eqz v3, :cond_7

    .line 673867
    :cond_4
    :goto_2
    return v2

    :cond_5
    move v0, v1

    .line 673868
    goto :goto_0

    :cond_6
    move v3, v1

    .line 673869
    goto :goto_1

    .line 673870
    :cond_7
    iget-object v4, p6, LX/48n;->d:Lcom/facebook/csslayout/YogaMeasureMode;

    sget-object v5, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    if-ne v4, v5, :cond_8

    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    if-ne p5, v4, :cond_8

    iget v4, p6, LX/48n;->f:F

    sub-float v5, p1, p3

    cmpg-float v4, v4, v5

    if-lez v4, :cond_9

    :cond_8
    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    if-ne p5, v4, :cond_f

    iget v4, p6, LX/48n;->f:F

    sub-float v5, p1, p3

    invoke-static {v4, v5}, LX/3Fp;->a(FF)Z

    move-result v4

    if-eqz v4, :cond_f

    :cond_9
    move v4, v2

    .line 673871
    :goto_3
    if-eqz v3, :cond_a

    if-nez v4, :cond_4

    .line 673872
    :cond_a
    iget-object v3, p6, LX/48n;->c:Lcom/facebook/csslayout/YogaMeasureMode;

    sget-object v5, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    if-ne v3, v5, :cond_b

    sget-object v3, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    if-ne p4, v3, :cond_b

    iget v3, p6, LX/48n;->e:F

    sub-float v5, p0, p2

    cmpg-float v3, v3, v5

    if-lez v3, :cond_c

    :cond_b
    sget-object v3, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    if-ne p4, v3, :cond_10

    iget v3, p6, LX/48n;->e:F

    sub-float v5, p0, p2

    invoke-static {v3, v5}, LX/3Fp;->a(FF)Z

    move-result v3

    if-eqz v3, :cond_10

    :cond_c
    move v3, v2

    .line 673873
    :goto_4
    if-eqz v0, :cond_d

    if-nez v3, :cond_4

    .line 673874
    :cond_d
    if-eqz v4, :cond_e

    if-nez v3, :cond_4

    :cond_e
    move v2, v1

    .line 673875
    goto :goto_2

    :cond_f
    move v4, v1

    .line 673876
    goto :goto_3

    :cond_10
    move v3, v1

    .line 673877
    goto :goto_4
.end method

.method private static a(LX/1Dq;LX/48q;FFLcom/facebook/csslayout/YogaDirection;Lcom/facebook/csslayout/YogaMeasureMode;Lcom/facebook/csslayout/YogaMeasureMode;Z)Z
    .locals 12

    .prologue
    .line 673878
    iget-object v11, p1, LX/48q;->b:LX/48o;

    .line 673879
    invoke-virtual {p1}, LX/48q;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, v11, LX/48o;->e:I

    iget v2, p0, LX/1Dq;->b:I

    if-ne v1, v2, :cond_1

    :cond_0
    iget-object v1, v11, LX/48o;->f:Lcom/facebook/csslayout/YogaDirection;

    move-object/from16 v0, p4

    if-eq v1, v0, :cond_6

    :cond_1
    const/4 v1, 0x1

    move v10, v1

    .line 673880
    :goto_0
    if-eqz v10, :cond_2

    .line 673881
    const/4 v1, 0x0

    iput v1, v11, LX/48o;->g:I

    .line 673882
    iget-object v1, v11, LX/48o;->j:LX/48n;

    const/4 v2, 0x0

    iput-object v2, v1, LX/48n;->c:Lcom/facebook/csslayout/YogaMeasureMode;

    .line 673883
    iget-object v1, v11, LX/48o;->j:LX/48n;

    const/4 v2, 0x0

    iput-object v2, v1, LX/48n;->d:Lcom/facebook/csslayout/YogaMeasureMode;

    .line 673884
    :cond_2
    const/4 v9, 0x0

    .line 673885
    invoke-static {p1}, LX/48t;->e(LX/48q;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 673886
    iget-object v1, p1, LX/48q;->a:LX/48r;

    iget-object v1, v1, LX/48r;->m:LX/1mz;

    sget-object v2, LX/48t;->k:[I

    sget v3, LX/48t;->c:I

    aget v2, v2, v3

    sget-object v3, LX/48t;->g:[I

    sget v4, LX/48t;->c:I

    aget v3, v3, v4

    invoke-virtual {v1, v2, v3}, LX/1mz;->a(II)F

    move-result v1

    iget-object v2, p1, LX/48q;->a:LX/48r;

    iget-object v2, v2, LX/48r;->m:LX/1mz;

    sget-object v3, LX/48t;->l:[I

    sget v4, LX/48t;->c:I

    aget v3, v3, v4

    sget-object v4, LX/48t;->h:[I

    sget v5, LX/48t;->c:I

    aget v4, v4, v5

    invoke-virtual {v2, v3, v4}, LX/1mz;->a(II)F

    move-result v2

    add-float v3, v1, v2

    .line 673887
    iget-object v1, p1, LX/48q;->a:LX/48r;

    iget-object v1, v1, LX/48r;->m:LX/1mz;

    sget-object v2, LX/48t;->k:[I

    sget v4, LX/48t;->a:I

    aget v2, v2, v4

    sget-object v4, LX/48t;->g:[I

    sget v5, LX/48t;->a:I

    aget v4, v4, v5

    invoke-virtual {v1, v2, v4}, LX/1mz;->a(II)F

    move-result v1

    iget-object v2, p1, LX/48q;->a:LX/48r;

    iget-object v2, v2, LX/48r;->m:LX/1mz;

    sget-object v4, LX/48t;->l:[I

    sget v5, LX/48t;->a:I

    aget v4, v4, v5

    sget-object v5, LX/48t;->h:[I

    sget v6, LX/48t;->a:I

    aget v5, v5, v6

    invoke-virtual {v2, v4, v5}, LX/1mz;->a(II)F

    move-result v2

    add-float v4, v1, v2

    .line 673888
    iget-object v7, v11, LX/48o;->j:LX/48n;

    move v1, p2

    move v2, p3

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-static/range {v1 .. v7}, LX/48t;->a(FFFFLcom/facebook/csslayout/YogaMeasureMode;Lcom/facebook/csslayout/YogaMeasureMode;LX/48n;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 673889
    iget-object v1, v11, LX/48o;->j:LX/48n;

    move-object v2, v1

    .line 673890
    :goto_1
    if-nez v10, :cond_d

    if-eqz v2, :cond_d

    .line 673891
    iget-object v1, v11, LX/48o;->i:[F

    const/4 v3, 0x0

    iget v4, v2, LX/48n;->e:F

    aput v4, v1, v3

    .line 673892
    iget-object v1, v11, LX/48o;->i:[F

    const/4 v3, 0x1

    iget v4, v2, LX/48n;->f:F

    aput v4, v1, v3

    .line 673893
    :cond_3
    :goto_2
    if-eqz p7, :cond_4

    .line 673894
    iget-object v1, p1, LX/48q;->b:LX/48o;

    iget-object v1, v1, LX/48o;->b:[F

    const/4 v3, 0x0

    iget-object v4, p1, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    aput v4, v1, v3

    .line 673895
    iget-object v1, p1, LX/48q;->b:LX/48o;

    iget-object v1, v1, LX/48o;->b:[F

    const/4 v3, 0x1

    iget-object v4, p1, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    aput v4, v1, v3

    .line 673896
    invoke-virtual {p1}, LX/48q;->p()V

    .line 673897
    :cond_4
    iget v1, p0, LX/1Dq;->b:I

    iput v1, v11, LX/48o;->e:I

    .line 673898
    if-nez v10, :cond_5

    if-nez v2, :cond_11

    :cond_5
    const/4 v1, 0x1

    :goto_3
    return v1

    .line 673899
    :cond_6
    const/4 v1, 0x0

    move v10, v1

    goto/16 :goto_0

    .line 673900
    :cond_7
    const/4 v1, 0x0

    move v8, v1

    :goto_4
    iget v1, v11, LX/48o;->g:I

    if-ge v8, v1, :cond_9

    .line 673901
    iget-object v1, v11, LX/48o;->h:[LX/48n;

    aget-object v7, v1, v8

    move v1, p2

    move v2, p3

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-static/range {v1 .. v7}, LX/48t;->a(FFFFLcom/facebook/csslayout/YogaMeasureMode;Lcom/facebook/csslayout/YogaMeasureMode;LX/48n;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 673902
    iget-object v1, v11, LX/48o;->h:[LX/48n;

    aget-object v1, v1, v8

    move-object v2, v1

    .line 673903
    goto :goto_1

    .line 673904
    :cond_8
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto :goto_4

    :cond_9
    move-object v2, v9

    .line 673905
    goto :goto_1

    :cond_a
    if-eqz p7, :cond_b

    .line 673906
    iget-object v1, v11, LX/48o;->j:LX/48n;

    iget v1, v1, LX/48n;->a:F

    invoke-static {v1, p2}, LX/3Fp;->a(FF)Z

    move-result v1

    if-eqz v1, :cond_12

    iget-object v1, v11, LX/48o;->j:LX/48n;

    iget v1, v1, LX/48n;->b:F

    invoke-static {v1, p3}, LX/3Fp;->a(FF)Z

    move-result v1

    if-eqz v1, :cond_12

    iget-object v1, v11, LX/48o;->j:LX/48n;

    iget-object v1, v1, LX/48n;->c:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p5

    if-ne v1, v0, :cond_12

    iget-object v1, v11, LX/48o;->j:LX/48n;

    iget-object v1, v1, LX/48n;->d:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p6

    if-ne v1, v0, :cond_12

    .line 673907
    iget-object v1, v11, LX/48o;->j:LX/48n;

    move-object v2, v1

    goto/16 :goto_1

    .line 673908
    :cond_b
    const/4 v1, 0x0

    :goto_5
    iget v2, v11, LX/48o;->g:I

    if-ge v1, v2, :cond_12

    .line 673909
    iget-object v2, v11, LX/48o;->h:[LX/48n;

    aget-object v2, v2, v1

    iget v2, v2, LX/48n;->a:F

    invoke-static {v2, p2}, LX/3Fp;->a(FF)Z

    move-result v2

    if-eqz v2, :cond_c

    iget-object v2, v11, LX/48o;->h:[LX/48n;

    aget-object v2, v2, v1

    iget v2, v2, LX/48n;->b:F

    invoke-static {v2, p3}, LX/3Fp;->a(FF)Z

    move-result v2

    if-eqz v2, :cond_c

    iget-object v2, v11, LX/48o;->h:[LX/48n;

    aget-object v2, v2, v1

    iget-object v2, v2, LX/48n;->c:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p5

    if-ne v2, v0, :cond_c

    iget-object v2, v11, LX/48o;->h:[LX/48n;

    aget-object v2, v2, v1

    iget-object v2, v2, LX/48n;->d:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p6

    if-ne v2, v0, :cond_c

    .line 673910
    iget-object v2, v11, LX/48o;->h:[LX/48n;

    aget-object v1, v2, v1

    move-object v2, v1

    .line 673911
    goto/16 :goto_1

    .line 673912
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 673913
    :cond_d
    invoke-static/range {p0 .. p7}, LX/48t;->b(LX/1Dq;LX/48q;FFLcom/facebook/csslayout/YogaDirection;Lcom/facebook/csslayout/YogaMeasureMode;Lcom/facebook/csslayout/YogaMeasureMode;Z)V

    .line 673914
    move-object/from16 v0, p4

    iput-object v0, v11, LX/48o;->f:Lcom/facebook/csslayout/YogaDirection;

    .line 673915
    if-nez v2, :cond_3

    .line 673916
    iget v1, v11, LX/48o;->g:I

    const/16 v3, 0x10

    if-ne v1, v3, :cond_e

    .line 673917
    const/4 v1, 0x0

    iput v1, v11, LX/48o;->g:I

    .line 673918
    :cond_e
    if-eqz p7, :cond_f

    .line 673919
    iget-object v1, v11, LX/48o;->j:LX/48n;

    .line 673920
    :goto_6
    iput p2, v1, LX/48n;->a:F

    .line 673921
    iput p3, v1, LX/48n;->b:F

    .line 673922
    move-object/from16 v0, p5

    iput-object v0, v1, LX/48n;->c:Lcom/facebook/csslayout/YogaMeasureMode;

    .line 673923
    move-object/from16 v0, p6

    iput-object v0, v1, LX/48n;->d:Lcom/facebook/csslayout/YogaMeasureMode;

    .line 673924
    iget-object v3, v11, LX/48o;->i:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    iput v3, v1, LX/48n;->e:F

    .line 673925
    iget-object v3, v11, LX/48o;->i:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    iput v3, v1, LX/48n;->f:F

    goto/16 :goto_2

    .line 673926
    :cond_f
    iget-object v1, v11, LX/48o;->h:[LX/48n;

    iget v3, v11, LX/48o;->g:I

    aget-object v1, v1, v3

    .line 673927
    if-nez v1, :cond_10

    .line 673928
    new-instance v1, LX/48n;

    invoke-direct {v1}, LX/48n;-><init>()V

    .line 673929
    iget-object v3, v11, LX/48o;->h:[LX/48n;

    iget v4, v11, LX/48o;->g:I

    aput-object v1, v3, v4

    .line 673930
    :cond_10
    iget v3, v11, LX/48o;->g:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v11, LX/48o;->g:I

    goto :goto_6

    .line 673931
    :cond_11
    const/4 v1, 0x0

    goto/16 :goto_3

    :cond_12
    move-object v2, v9

    goto/16 :goto_1
.end method

.method private static a(LX/48q;)Z
    .locals 1

    .prologue
    .line 673863
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget v0, v0, LX/48r;->l:F

    invoke-static {v0}, LX/1mo;->a(F)Z

    move-result v0

    return v0
.end method

.method private static b(LX/48q;)F
    .locals 1

    .prologue
    .line 673862
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget v0, v0, LX/48r;->j:F

    return v0
.end method

.method private static b(LX/48q;IF)F
    .locals 4

    .prologue
    .line 673860
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->n:LX/1mz;

    sget-object v1, LX/48t;->k:[I

    aget v1, v1, p1

    sget-object v2, LX/48t;->g:[I

    aget v2, v2, p1

    invoke-virtual {v0, v1, v2}, LX/1mz;->a(II)F

    move-result v0

    iget-object v1, p0, LX/48q;->a:LX/48r;

    iget-object v1, v1, LX/48r;->o:LX/1mz;

    sget-object v2, LX/48t;->k:[I

    aget v2, v2, p1

    sget-object v3, LX/48t;->g:[I

    aget v3, v3, p1

    invoke-virtual {v1, v2, v3}, LX/1mz;->a(II)F

    move-result v1

    add-float/2addr v0, v1

    iget-object v1, p0, LX/48q;->a:LX/48r;

    iget-object v1, v1, LX/48r;->n:LX/1mz;

    sget-object v2, LX/48t;->l:[I

    aget v2, v2, p1

    sget-object v3, LX/48t;->h:[I

    aget v3, v3, p1

    invoke-virtual {v1, v2, v3}, LX/1mz;->a(II)F

    move-result v1

    add-float/2addr v0, v1

    iget-object v1, p0, LX/48q;->a:LX/48r;

    iget-object v1, v1, LX/48r;->o:LX/1mz;

    sget-object v2, LX/48t;->l:[I

    aget v2, v2, p1

    sget-object v3, LX/48t;->h:[I

    aget v3, v3, p1

    invoke-virtual {v1, v2, v3}, LX/1mz;->a(II)F

    move-result v1

    add-float/2addr v0, v1

    .line 673861
    invoke-static {p0, p1, p2}, LX/48t;->a(LX/48q;IF)F

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method private static b(ILcom/facebook/csslayout/YogaDirection;)I
    .locals 1

    .prologue
    .line 673857
    sget v0, LX/48t;->a:I

    if-eq p0, v0, :cond_0

    sget v0, LX/48t;->b:I

    if-ne p0, v0, :cond_1

    .line 673858
    :cond_0
    sget v0, LX/48t;->c:I

    invoke-static {v0, p1}, LX/48t;->a(ILcom/facebook/csslayout/YogaDirection;)I

    move-result v0

    .line 673859
    :goto_0
    return v0

    :cond_1
    sget v0, LX/48t;->a:I

    goto :goto_0
.end method

.method private static b(LX/48q;Lcom/facebook/csslayout/YogaDirection;)Lcom/facebook/csslayout/YogaDirection;
    .locals 2

    .prologue
    .line 673853
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->a:Lcom/facebook/csslayout/YogaDirection;

    .line 673854
    sget-object v1, Lcom/facebook/csslayout/YogaDirection;->INHERIT:Lcom/facebook/csslayout/YogaDirection;

    if-ne v0, v1, :cond_1

    .line 673855
    if-nez p1, :cond_0

    sget-object p1, Lcom/facebook/csslayout/YogaDirection;->LTR:Lcom/facebook/csslayout/YogaDirection;

    .line 673856
    :cond_0
    :goto_0
    return-object p1

    :cond_1
    move-object p1, v0

    goto :goto_0
.end method

.method private static b(LX/1Dq;LX/48q;FFLcom/facebook/csslayout/YogaDirection;Lcom/facebook/csslayout/YogaMeasureMode;Lcom/facebook/csslayout/YogaMeasureMode;Z)V
    .locals 51

    .prologue
    .line 673469
    invoke-static/range {p2 .. p2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p5

    if-eq v0, v4, :cond_2

    const/4 v4, 0x0

    :goto_0
    const-string v5, "availableWidth is indefinite so widthMeasureMode must be YogaMeasureMode.UNDEFINED"

    invoke-static {v4, v5}, LX/0nE;->a(ZLjava/lang/String;)V

    .line 673470
    invoke-static/range {p3 .. p3}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-eqz v4, :cond_3

    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p6

    if-eq v0, v4, :cond_3

    const/4 v4, 0x0

    :goto_1
    const-string v5, "availableHeight is indefinite so heightMeasureMode must be YogaMeasureMode.UNDEFINED"

    invoke-static {v4, v5}, LX/0nE;->a(ZLjava/lang/String;)V

    .line 673471
    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->n:LX/1mz;

    sget-object v5, LX/48t;->k:[I

    sget v6, LX/48t;->c:I

    aget v5, v5, v6

    sget-object v6, LX/48t;->g:[I

    sget v7, LX/48t;->c:I

    aget v6, v6, v7

    invoke-virtual {v4, v5, v6}, LX/1mz;->a(II)F

    move-result v4

    move-object/from16 v0, p1

    iget-object v5, v0, LX/48q;->a:LX/48r;

    iget-object v5, v5, LX/48r;->o:LX/1mz;

    sget-object v6, LX/48t;->k:[I

    sget v7, LX/48t;->c:I

    aget v6, v6, v7

    sget-object v7, LX/48t;->g:[I

    sget v8, LX/48t;->c:I

    aget v7, v7, v8

    invoke-virtual {v5, v6, v7}, LX/1mz;->a(II)F

    move-result v5

    add-float/2addr v4, v5

    move-object/from16 v0, p1

    iget-object v5, v0, LX/48q;->a:LX/48r;

    iget-object v5, v5, LX/48r;->n:LX/1mz;

    sget-object v6, LX/48t;->l:[I

    sget v7, LX/48t;->c:I

    aget v6, v6, v7

    sget-object v7, LX/48t;->h:[I

    sget v8, LX/48t;->c:I

    aget v7, v7, v8

    invoke-virtual {v5, v6, v7}, LX/1mz;->a(II)F

    move-result v5

    move-object/from16 v0, p1

    iget-object v6, v0, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->o:LX/1mz;

    sget-object v7, LX/48t;->l:[I

    sget v8, LX/48t;->c:I

    aget v7, v7, v8

    sget-object v8, LX/48t;->h:[I

    sget v9, LX/48t;->c:I

    aget v8, v8, v9

    invoke-virtual {v6, v7, v8}, LX/1mz;->a(II)F

    move-result v6

    add-float/2addr v5, v6

    add-float/2addr v4, v5

    .line 673472
    move-object/from16 v0, p1

    iget-object v5, v0, LX/48q;->a:LX/48r;

    iget-object v5, v5, LX/48r;->n:LX/1mz;

    sget-object v6, LX/48t;->k:[I

    sget v7, LX/48t;->a:I

    aget v6, v6, v7

    sget-object v7, LX/48t;->g:[I

    sget v8, LX/48t;->a:I

    aget v7, v7, v8

    invoke-virtual {v5, v6, v7}, LX/1mz;->a(II)F

    move-result v5

    move-object/from16 v0, p1

    iget-object v6, v0, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->o:LX/1mz;

    sget-object v7, LX/48t;->k:[I

    sget v8, LX/48t;->a:I

    aget v7, v7, v8

    sget-object v8, LX/48t;->g:[I

    sget v9, LX/48t;->a:I

    aget v8, v8, v9

    invoke-virtual {v6, v7, v8}, LX/1mz;->a(II)F

    move-result v6

    add-float/2addr v5, v6

    move-object/from16 v0, p1

    iget-object v6, v0, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->n:LX/1mz;

    sget-object v7, LX/48t;->l:[I

    sget v8, LX/48t;->a:I

    aget v7, v7, v8

    sget-object v8, LX/48t;->h:[I

    sget v9, LX/48t;->a:I

    aget v8, v8, v9

    invoke-virtual {v6, v7, v8}, LX/1mz;->a(II)F

    move-result v6

    move-object/from16 v0, p1

    iget-object v7, v0, LX/48q;->a:LX/48r;

    iget-object v7, v7, LX/48r;->o:LX/1mz;

    sget-object v8, LX/48t;->l:[I

    sget v9, LX/48t;->a:I

    aget v8, v8, v9

    sget-object v9, LX/48t;->h:[I

    sget v10, LX/48t;->a:I

    aget v9, v9, v10

    invoke-virtual {v7, v8, v9}, LX/1mz;->a(II)F

    move-result v7

    add-float/2addr v6, v7

    add-float v7, v5, v6

    .line 673473
    move-object/from16 v0, p1

    iget-object v5, v0, LX/48q;->a:LX/48r;

    iget-object v5, v5, LX/48r;->m:LX/1mz;

    sget-object v6, LX/48t;->k:[I

    sget v8, LX/48t;->c:I

    aget v6, v6, v8

    sget-object v8, LX/48t;->g:[I

    sget v9, LX/48t;->c:I

    aget v8, v8, v9

    invoke-virtual {v5, v6, v8}, LX/1mz;->a(II)F

    move-result v5

    move-object/from16 v0, p1

    iget-object v6, v0, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->m:LX/1mz;

    sget-object v8, LX/48t;->l:[I

    sget v9, LX/48t;->c:I

    aget v8, v8, v9

    sget-object v9, LX/48t;->h:[I

    sget v10, LX/48t;->c:I

    aget v9, v9, v10

    invoke-virtual {v6, v8, v9}, LX/1mz;->a(II)F

    move-result v6

    add-float v32, v5, v6

    .line 673474
    move-object/from16 v0, p1

    iget-object v5, v0, LX/48q;->a:LX/48r;

    iget-object v5, v5, LX/48r;->m:LX/1mz;

    sget-object v6, LX/48t;->k:[I

    sget v8, LX/48t;->a:I

    aget v6, v6, v8

    sget-object v8, LX/48t;->g:[I

    sget v9, LX/48t;->a:I

    aget v8, v8, v9

    invoke-virtual {v5, v6, v8}, LX/1mz;->a(II)F

    move-result v5

    move-object/from16 v0, p1

    iget-object v6, v0, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->m:LX/1mz;

    sget-object v8, LX/48t;->l:[I

    sget v9, LX/48t;->a:I

    aget v8, v8, v9

    sget-object v9, LX/48t;->h:[I

    sget v10, LX/48t;->a:I

    aget v9, v9, v10

    invoke-virtual {v6, v8, v9}, LX/1mz;->a(II)F

    move-result v6

    add-float v33, v5, v6

    .line 673475
    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-static {v0, v1}, LX/48t;->b(LX/48q;Lcom/facebook/csslayout/YogaDirection;)Lcom/facebook/csslayout/YogaDirection;

    move-result-object v8

    .line 673476
    move-object/from16 v0, p1

    iget-object v5, v0, LX/48q;->b:LX/48o;

    iput-object v8, v5, LX/48o;->c:Lcom/facebook/csslayout/YogaDirection;

    .line 673477
    invoke-static/range {p1 .. p1}, LX/48t;->e(LX/48q;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 673478
    sub-float v5, p2, v32

    sub-float/2addr v5, v4

    .line 673479
    sub-float v6, p3, v33

    sub-float/2addr v6, v7

    .line 673480
    sget-object v8, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p5

    if-ne v0, v8, :cond_4

    sget-object v8, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p6

    if-ne v0, v8, :cond_4

    .line 673481
    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    const/4 v5, 0x0

    sget v6, LX/48t;->c:I

    sub-float v7, p2, v32

    move-object/from16 v0, p1

    invoke-static {v0, v6, v7}, LX/48t;->b(LX/48q;IF)F

    move-result v6

    aput v6, v4, v5

    .line 673482
    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->b:LX/48o;

    iget-object v6, v4, LX/48o;->i:[F

    const/4 v5, 0x1

    sget v4, LX/48t;->a:I

    .line 673483
    :cond_0
    sub-float v7, p3, v33

    move/from16 v50, v7

    move-object v7, v6

    move v6, v5

    move v5, v4

    move/from16 v4, v50

    :goto_2
    move-object/from16 v0, p1

    invoke-static {v0, v5, v4}, LX/48t;->b(LX/48q;IF)F

    move-result v4

    aput v4, v7, v6

    .line 673484
    :cond_1
    :goto_3
    return-void

    .line 673485
    :cond_2
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 673486
    :cond_3
    const/4 v4, 0x1

    goto/16 :goto_1

    .line 673487
    :cond_4
    const/4 v8, 0x0

    cmpg-float v8, v5, v8

    if-lez v8, :cond_5

    const/4 v8, 0x0

    cmpg-float v8, v6, v8

    if-gtz v8, :cond_6

    .line 673488
    :cond_5
    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    const/4 v5, 0x0

    sget v6, LX/48t;->c:I

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v6, v7}, LX/48t;->b(LX/48q;IF)F

    move-result v6

    aput v6, v4, v5

    .line 673489
    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->b:LX/48o;

    iget-object v7, v4, LX/48o;->i:[F

    const/4 v6, 0x1

    sget v5, LX/48t;->a:I

    const/4 v4, 0x0

    goto :goto_2

    .line 673490
    :cond_6
    move-object/from16 v0, p1

    move-object/from16 v1, p5

    move-object/from16 v2, p6

    invoke-virtual {v0, v5, v1, v6, v2}, LX/48q;->a(FLcom/facebook/csslayout/YogaMeasureMode;FLcom/facebook/csslayout/YogaMeasureMode;)J

    move-result-wide v8

    .line 673491
    invoke-static {v8, v9}, LX/1Dr;->a(J)I

    move-result v5

    .line 673492
    invoke-static {v8, v9}, LX/1Dr;->b(J)I

    move-result v8

    .line 673493
    move-object/from16 v0, p1

    iget-object v6, v0, LX/48q;->b:LX/48o;

    iget-object v6, v6, LX/48o;->i:[F

    const/4 v9, 0x0

    sget v10, LX/48t;->c:I

    sget-object v11, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p5

    if-eq v0, v11, :cond_7

    sget-object v11, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p5

    if-ne v0, v11, :cond_9

    :cond_7
    int-to-float v5, v5

    add-float/2addr v4, v5

    :goto_4
    move-object/from16 v0, p1

    invoke-static {v0, v10, v4}, LX/48t;->b(LX/48q;IF)F

    move-result v4

    aput v4, v6, v9

    .line 673494
    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->b:LX/48o;

    iget-object v6, v4, LX/48o;->i:[F

    const/4 v5, 0x1

    sget v4, LX/48t;->a:I

    sget-object v9, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p6

    if-eq v0, v9, :cond_8

    sget-object v9, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p6

    if-ne v0, v9, :cond_0

    :cond_8
    int-to-float v8, v8

    add-float/2addr v7, v8

    move/from16 v50, v7

    move-object v7, v6

    move v6, v5

    move v5, v4

    move/from16 v4, v50

    goto/16 :goto_2

    .line 673495
    :cond_9
    sub-float v4, p2, v32

    goto :goto_4

    .line 673496
    :cond_a
    invoke-virtual/range {p1 .. p1}, LX/48q;->b()I

    move-result v34

    .line 673497
    if-nez v34, :cond_f

    .line 673498
    move-object/from16 v0, p1

    iget-object v5, v0, LX/48q;->b:LX/48o;

    iget-object v5, v5, LX/48o;->i:[F

    const/4 v6, 0x0

    sget v8, LX/48t;->c:I

    sget-object v9, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p5

    if-eq v0, v9, :cond_b

    sget-object v9, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p5

    if-ne v0, v9, :cond_d

    :cond_b
    :goto_5
    move-object/from16 v0, p1

    invoke-static {v0, v8, v4}, LX/48t;->b(LX/48q;IF)F

    move-result v4

    aput v4, v5, v6

    .line 673499
    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->b:LX/48o;

    iget-object v5, v4, LX/48o;->i:[F

    const/4 v6, 0x1

    sget v8, LX/48t;->a:I

    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p6

    if-eq v0, v4, :cond_c

    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p6

    if-ne v0, v4, :cond_e

    :cond_c
    move v4, v7

    :goto_6
    move-object/from16 v0, p1

    invoke-static {v0, v8, v4}, LX/48t;->b(LX/48q;IF)F

    move-result v4

    aput v4, v5, v6

    goto/16 :goto_3

    .line 673500
    :cond_d
    sub-float v4, p2, v32

    goto :goto_5

    .line 673501
    :cond_e
    sub-float v4, p3, v33

    goto :goto_6

    .line 673502
    :cond_f
    if-nez p7, :cond_15

    .line 673503
    sget-object v5, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p5

    if-ne v0, v5, :cond_10

    const/4 v5, 0x0

    cmpg-float v5, p2, v5

    if-gtz v5, :cond_10

    sget-object v5, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p6

    if-ne v0, v5, :cond_10

    const/4 v5, 0x0

    cmpg-float v5, p3, v5

    if-gtz v5, :cond_10

    .line 673504
    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    const/4 v5, 0x0

    sget v6, LX/48t;->c:I

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v6, v7}, LX/48t;->b(LX/48q;IF)F

    move-result v6

    aput v6, v4, v5

    .line 673505
    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    const/4 v5, 0x1

    sget v6, LX/48t;->a:I

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v6, v7}, LX/48t;->b(LX/48q;IF)F

    move-result v6

    aput v6, v4, v5

    goto/16 :goto_3

    .line 673506
    :cond_10
    sget-object v5, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p5

    if-ne v0, v5, :cond_12

    const/4 v5, 0x0

    cmpg-float v5, p2, v5

    if-gtz v5, :cond_12

    .line 673507
    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    const/4 v5, 0x0

    sget v6, LX/48t;->c:I

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v6, v7}, LX/48t;->b(LX/48q;IF)F

    move-result v6

    aput v6, v4, v5

    .line 673508
    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->b:LX/48o;

    iget-object v5, v4, LX/48o;->i:[F

    const/4 v6, 0x1

    sget v7, LX/48t;->a:I

    invoke-static/range {p3 .. p3}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-eqz v4, :cond_11

    const/4 v4, 0x0

    :goto_7
    move-object/from16 v0, p1

    invoke-static {v0, v7, v4}, LX/48t;->b(LX/48q;IF)F

    move-result v4

    aput v4, v5, v6

    goto/16 :goto_3

    :cond_11
    sub-float v4, p3, v33

    goto :goto_7

    .line 673509
    :cond_12
    sget-object v5, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p6

    if-ne v0, v5, :cond_14

    const/4 v5, 0x0

    cmpg-float v5, p3, v5

    if-gtz v5, :cond_14

    .line 673510
    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->b:LX/48o;

    iget-object v5, v4, LX/48o;->i:[F

    const/4 v6, 0x0

    sget v7, LX/48t;->c:I

    invoke-static/range {p2 .. p2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-eqz v4, :cond_13

    const/4 v4, 0x0

    :goto_8
    move-object/from16 v0, p1

    invoke-static {v0, v7, v4}, LX/48t;->b(LX/48q;IF)F

    move-result v4

    aput v4, v5, v6

    .line 673511
    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    const/4 v5, 0x1

    sget v6, LX/48t;->a:I

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v6, v7}, LX/48t;->b(LX/48q;IF)F

    move-result v6

    aput v6, v4, v5

    goto/16 :goto_3

    .line 673512
    :cond_13
    sub-float v4, p2, v32

    goto :goto_8

    .line 673513
    :cond_14
    sget-object v5, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p5

    if-ne v0, v5, :cond_15

    sget-object v5, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p6

    if-ne v0, v5, :cond_15

    .line 673514
    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    const/4 v5, 0x0

    sget v6, LX/48t;->c:I

    sub-float v7, p2, v32

    move-object/from16 v0, p1

    invoke-static {v0, v6, v7}, LX/48t;->b(LX/48q;IF)F

    move-result v6

    aput v6, v4, v5

    .line 673515
    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    const/4 v5, 0x1

    sget v6, LX/48t;->a:I

    sub-float v7, p3, v33

    move-object/from16 v0, p1

    invoke-static {v0, v6, v7}, LX/48t;->b(LX/48q;IF)F

    move-result v6

    aput v6, v4, v5

    goto/16 :goto_3

    .line 673516
    :cond_15
    invoke-static/range {p1 .. p1}, LX/48t;->d(LX/48q;)I

    move-result v5

    invoke-static {v5, v8}, LX/48t;->a(ILcom/facebook/csslayout/YogaDirection;)I

    move-result v35

    .line 673517
    move/from16 v0, v35

    invoke-static {v0, v8}, LX/48t;->b(ILcom/facebook/csslayout/YogaDirection;)I

    move-result v36

    .line 673518
    sget v5, LX/48t;->c:I

    move/from16 v0, v35

    if-eq v0, v5, :cond_16

    sget v5, LX/48t;->d:I

    move/from16 v0, v35

    if-ne v0, v5, :cond_19

    :cond_16
    const/4 v5, 0x1

    move v12, v5

    .line 673519
    :goto_9
    move-object/from16 v0, p1

    iget-object v5, v0, LX/48q;->a:LX/48r;

    iget-object v0, v5, LX/48r;->c:Lcom/facebook/csslayout/YogaJustify;

    move-object/from16 v37, v0

    .line 673520
    move-object/from16 v0, p1

    iget-object v5, v0, LX/48q;->a:LX/48r;

    iget-object v5, v5, LX/48r;->h:Lcom/facebook/csslayout/YogaWrap;

    sget-object v6, Lcom/facebook/csslayout/YogaWrap;->WRAP:Lcom/facebook/csslayout/YogaWrap;

    if-ne v5, v6, :cond_1a

    const/4 v5, 0x1

    move v13, v5

    .line 673521
    :goto_a
    const/16 v18, 0x0

    .line 673522
    const/16 v17, 0x0

    .line 673523
    move-object/from16 v0, p1

    iget-object v5, v0, LX/48q;->a:LX/48r;

    iget-object v5, v5, LX/48r;->n:LX/1mz;

    sget-object v6, LX/48t;->k:[I

    aget v6, v6, v35

    sget-object v9, LX/48t;->g:[I

    aget v9, v9, v35

    invoke-virtual {v5, v6, v9}, LX/1mz;->a(II)F

    move-result v5

    move-object/from16 v0, p1

    iget-object v6, v0, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->o:LX/1mz;

    sget-object v9, LX/48t;->k:[I

    aget v9, v9, v35

    sget-object v10, LX/48t;->g:[I

    aget v10, v10, v35

    invoke-virtual {v6, v9, v10}, LX/1mz;->a(II)F

    move-result v6

    add-float v38, v5, v6

    .line 673524
    move-object/from16 v0, p1

    iget-object v5, v0, LX/48q;->a:LX/48r;

    iget-object v5, v5, LX/48r;->n:LX/1mz;

    sget-object v6, LX/48t;->l:[I

    aget v6, v6, v35

    sget-object v9, LX/48t;->h:[I

    aget v9, v9, v35

    invoke-virtual {v5, v6, v9}, LX/1mz;->a(II)F

    move-result v5

    move-object/from16 v0, p1

    iget-object v6, v0, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->o:LX/1mz;

    sget-object v9, LX/48t;->l:[I

    aget v9, v9, v35

    sget-object v10, LX/48t;->h:[I

    aget v10, v10, v35

    invoke-virtual {v6, v9, v10}, LX/1mz;->a(II)F

    move-result v6

    add-float v39, v5, v6

    .line 673525
    move-object/from16 v0, p1

    iget-object v5, v0, LX/48q;->a:LX/48r;

    iget-object v5, v5, LX/48r;->n:LX/1mz;

    sget-object v6, LX/48t;->k:[I

    aget v6, v6, v36

    sget-object v9, LX/48t;->g:[I

    aget v9, v9, v36

    invoke-virtual {v5, v6, v9}, LX/1mz;->a(II)F

    move-result v5

    move-object/from16 v0, p1

    iget-object v6, v0, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->o:LX/1mz;

    sget-object v9, LX/48t;->k:[I

    aget v9, v9, v36

    sget-object v10, LX/48t;->g:[I

    aget v10, v10, v36

    invoke-virtual {v6, v9, v10}, LX/1mz;->a(II)F

    move-result v6

    add-float v21, v5, v6

    .line 673526
    move-object/from16 v0, p1

    iget-object v5, v0, LX/48q;->a:LX/48r;

    iget-object v5, v5, LX/48r;->n:LX/1mz;

    sget-object v6, LX/48t;->k:[I

    aget v6, v6, v35

    sget-object v9, LX/48t;->g:[I

    aget v9, v9, v35

    invoke-virtual {v5, v6, v9}, LX/1mz;->a(II)F

    move-result v5

    move-object/from16 v0, p1

    iget-object v6, v0, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->o:LX/1mz;

    sget-object v9, LX/48t;->k:[I

    aget v9, v9, v35

    sget-object v10, LX/48t;->g:[I

    aget v10, v10, v35

    invoke-virtual {v6, v9, v10}, LX/1mz;->a(II)F

    move-result v6

    add-float/2addr v5, v6

    move-object/from16 v0, p1

    iget-object v6, v0, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->n:LX/1mz;

    sget-object v9, LX/48t;->l:[I

    aget v9, v9, v35

    sget-object v10, LX/48t;->h:[I

    aget v10, v10, v35

    invoke-virtual {v6, v9, v10}, LX/1mz;->a(II)F

    move-result v6

    move-object/from16 v0, p1

    iget-object v9, v0, LX/48q;->a:LX/48r;

    iget-object v9, v9, LX/48r;->o:LX/1mz;

    sget-object v10, LX/48t;->l:[I

    aget v10, v10, v35

    sget-object v11, LX/48t;->h:[I

    aget v11, v11, v35

    invoke-virtual {v9, v10, v11}, LX/1mz;->a(II)F

    move-result v9

    add-float/2addr v6, v9

    add-float v40, v5, v6

    .line 673527
    move-object/from16 v0, p1

    iget-object v5, v0, LX/48q;->a:LX/48r;

    iget-object v5, v5, LX/48r;->n:LX/1mz;

    sget-object v6, LX/48t;->k:[I

    aget v6, v6, v36

    sget-object v9, LX/48t;->g:[I

    aget v9, v9, v36

    invoke-virtual {v5, v6, v9}, LX/1mz;->a(II)F

    move-result v5

    move-object/from16 v0, p1

    iget-object v6, v0, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->o:LX/1mz;

    sget-object v9, LX/48t;->k:[I

    aget v9, v9, v36

    sget-object v10, LX/48t;->g:[I

    aget v10, v10, v36

    invoke-virtual {v6, v9, v10}, LX/1mz;->a(II)F

    move-result v6

    add-float/2addr v5, v6

    move-object/from16 v0, p1

    iget-object v6, v0, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->n:LX/1mz;

    sget-object v9, LX/48t;->l:[I

    aget v9, v9, v36

    sget-object v10, LX/48t;->h:[I

    aget v10, v10, v36

    invoke-virtual {v6, v9, v10}, LX/1mz;->a(II)F

    move-result v6

    move-object/from16 v0, p1

    iget-object v9, v0, LX/48q;->a:LX/48r;

    iget-object v9, v9, LX/48r;->o:LX/1mz;

    sget-object v10, LX/48t;->l:[I

    aget v10, v10, v36

    sget-object v11, LX/48t;->h:[I

    aget v11, v11, v36

    invoke-virtual {v9, v10, v11}, LX/1mz;->a(II)F

    move-result v9

    add-float/2addr v6, v9

    add-float v41, v5, v6

    .line 673528
    if-eqz v12, :cond_1b

    move-object/from16 v31, p5

    .line 673529
    :goto_b
    if-eqz v12, :cond_1c

    move-object/from16 v30, p6

    .line 673530
    :goto_c
    sub-float v5, p2, v32

    sub-float v16, v5, v4

    .line 673531
    sub-float v4, p3, v33

    sub-float v15, v4, v7

    .line 673532
    if-eqz v12, :cond_1d

    move/from16 v29, v16

    .line 673533
    :goto_d
    if-eqz v12, :cond_1e

    move v14, v15

    .line 673534
    :goto_e
    const/4 v4, 0x0

    move/from16 v19, v4

    :goto_f
    move/from16 v0, v19

    move/from16 v1, v34

    if-ge v0, v1, :cond_2e

    .line 673535
    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, LX/48q;->a(I)LX/48q;

    move-result-object v5

    .line 673536
    if-eqz p7, :cond_17

    .line 673537
    invoke-static {v5, v8}, LX/48t;->b(LX/48q;Lcom/facebook/csslayout/YogaDirection;)Lcom/facebook/csslayout/YogaDirection;

    move-result-object v4

    .line 673538
    invoke-static {v5, v4}, LX/48t;->a(LX/48q;Lcom/facebook/csslayout/YogaDirection;)V

    .line 673539
    :cond_17
    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->g:Lcom/facebook/csslayout/YogaPositionType;

    sget-object v6, Lcom/facebook/csslayout/YogaPositionType;->ABSOLUTE:Lcom/facebook/csslayout/YogaPositionType;

    if-ne v4, v6, :cond_1f

    .line 673540
    if-nez v18, :cond_8f

    move-object v4, v5

    .line 673541
    :goto_10
    if-eqz v17, :cond_18

    .line 673542
    move-object/from16 v0, v17

    iput-object v5, v0, LX/48q;->e:LX/48q;

    .line 673543
    :cond_18
    const/4 v6, 0x0

    iput-object v6, v5, LX/48q;->e:LX/48q;

    .line 673544
    :goto_11
    add-int/lit8 v6, v19, 0x1

    move/from16 v19, v6

    move-object/from16 v17, v5

    move-object/from16 v18, v4

    goto :goto_f

    .line 673545
    :cond_19
    const/4 v5, 0x0

    move v12, v5

    goto/16 :goto_9

    .line 673546
    :cond_1a
    const/4 v5, 0x0

    move v13, v5

    goto/16 :goto_a

    :cond_1b
    move-object/from16 v31, p6

    .line 673547
    goto :goto_b

    :cond_1c
    move-object/from16 v30, p5

    .line 673548
    goto :goto_c

    :cond_1d
    move/from16 v29, v15

    .line 673549
    goto :goto_d

    :cond_1e
    move/from16 v14, v16

    .line 673550
    goto :goto_e

    .line 673551
    :cond_1f
    if-eqz v12, :cond_20

    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->q:[F

    sget-object v6, LX/48t;->j:[I

    sget v7, LX/48t;->c:I

    aget v6, v6, v7

    aget v4, v4, v6

    float-to-double v6, v4

    const-wide/16 v10, 0x0

    cmpl-double v4, v6, v10

    if-ltz v4, :cond_20

    .line 673552
    iget-object v4, v5, LX/48q;->b:LX/48o;

    iget-object v6, v5, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->q:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    iget-object v7, v5, LX/48q;->a:LX/48r;

    iget-object v7, v7, LX/48r;->n:LX/1mz;

    sget-object v9, LX/48t;->k:[I

    sget v10, LX/48t;->c:I

    aget v9, v9, v10

    sget-object v10, LX/48t;->g:[I

    sget v11, LX/48t;->c:I

    aget v10, v10, v11

    invoke-virtual {v7, v9, v10}, LX/1mz;->a(II)F

    move-result v7

    iget-object v9, v5, LX/48q;->a:LX/48r;

    iget-object v9, v9, LX/48r;->o:LX/1mz;

    sget-object v10, LX/48t;->k:[I

    sget v11, LX/48t;->c:I

    aget v10, v10, v11

    sget-object v11, LX/48t;->g:[I

    sget v20, LX/48t;->c:I

    aget v11, v11, v20

    invoke-virtual {v9, v10, v11}, LX/1mz;->a(II)F

    move-result v9

    add-float/2addr v7, v9

    iget-object v9, v5, LX/48q;->a:LX/48r;

    iget-object v9, v9, LX/48r;->n:LX/1mz;

    sget-object v10, LX/48t;->l:[I

    sget v11, LX/48t;->c:I

    aget v10, v10, v11

    sget-object v11, LX/48t;->h:[I

    sget v20, LX/48t;->c:I

    aget v11, v11, v20

    invoke-virtual {v9, v10, v11}, LX/1mz;->a(II)F

    move-result v9

    iget-object v5, v5, LX/48q;->a:LX/48r;

    iget-object v5, v5, LX/48r;->o:LX/1mz;

    sget-object v10, LX/48t;->l:[I

    sget v11, LX/48t;->c:I

    aget v10, v10, v11

    sget-object v11, LX/48t;->h:[I

    sget v20, LX/48t;->c:I

    aget v11, v11, v20

    invoke-virtual {v5, v10, v11}, LX/1mz;->a(II)F

    move-result v5

    add-float/2addr v5, v9

    add-float/2addr v5, v7

    invoke-static {v6, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    iput v5, v4, LX/48o;->d:F

    move-object/from16 v5, v17

    move-object/from16 v4, v18

    goto/16 :goto_11

    .line 673553
    :cond_20
    if-nez v12, :cond_21

    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->q:[F

    sget-object v6, LX/48t;->j:[I

    sget v7, LX/48t;->a:I

    aget v6, v6, v7

    aget v4, v4, v6

    float-to-double v6, v4

    const-wide/16 v10, 0x0

    cmpl-double v4, v6, v10

    if-ltz v4, :cond_21

    .line 673554
    iget-object v4, v5, LX/48q;->b:LX/48o;

    iget-object v6, v5, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->q:[F

    const/4 v7, 0x1

    aget v6, v6, v7

    iget-object v7, v5, LX/48q;->a:LX/48r;

    iget-object v7, v7, LX/48r;->n:LX/1mz;

    sget-object v9, LX/48t;->k:[I

    sget v10, LX/48t;->a:I

    aget v9, v9, v10

    sget-object v10, LX/48t;->g:[I

    sget v11, LX/48t;->a:I

    aget v10, v10, v11

    invoke-virtual {v7, v9, v10}, LX/1mz;->a(II)F

    move-result v7

    iget-object v9, v5, LX/48q;->a:LX/48r;

    iget-object v9, v9, LX/48r;->o:LX/1mz;

    sget-object v10, LX/48t;->k:[I

    sget v11, LX/48t;->a:I

    aget v10, v10, v11

    sget-object v11, LX/48t;->g:[I

    sget v20, LX/48t;->a:I

    aget v11, v11, v20

    invoke-virtual {v9, v10, v11}, LX/1mz;->a(II)F

    move-result v9

    add-float/2addr v7, v9

    iget-object v9, v5, LX/48q;->a:LX/48r;

    iget-object v9, v9, LX/48r;->n:LX/1mz;

    sget-object v10, LX/48t;->l:[I

    sget v11, LX/48t;->a:I

    aget v10, v10, v11

    sget-object v11, LX/48t;->h:[I

    sget v20, LX/48t;->a:I

    aget v11, v11, v20

    invoke-virtual {v9, v10, v11}, LX/1mz;->a(II)F

    move-result v9

    iget-object v5, v5, LX/48q;->a:LX/48r;

    iget-object v5, v5, LX/48r;->o:LX/1mz;

    sget-object v10, LX/48t;->l:[I

    sget v11, LX/48t;->a:I

    aget v10, v10, v11

    sget-object v11, LX/48t;->h:[I

    sget v20, LX/48t;->a:I

    aget v11, v11, v20

    invoke-virtual {v5, v10, v11}, LX/1mz;->a(II)F

    move-result v5

    add-float/2addr v5, v9

    add-float/2addr v5, v7

    invoke-static {v6, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    iput v5, v4, LX/48o;->d:F

    move-object/from16 v5, v17

    move-object/from16 v4, v18

    goto/16 :goto_11

    .line 673555
    :cond_21
    invoke-static {v5}, LX/48t;->a(LX/48q;)Z

    move-result v4

    if-nez v4, :cond_23

    invoke-static/range {v29 .. v29}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-nez v4, :cond_23

    .line 673556
    iget-object v4, v5, LX/48q;->b:LX/48o;

    iget v4, v4, LX/48o;->d:F

    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-eqz v4, :cond_22

    .line 673557
    iget-object v6, v5, LX/48q;->b:LX/48o;

    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget v4, v4, LX/48r;->l:F

    .line 673558
    :goto_12
    iget-object v7, v5, LX/48q;->a:LX/48r;

    iget-object v7, v7, LX/48r;->n:LX/1mz;

    sget-object v9, LX/48t;->k:[I

    aget v9, v9, v35

    sget-object v10, LX/48t;->g:[I

    aget v10, v10, v35

    invoke-virtual {v7, v9, v10}, LX/1mz;->a(II)F

    move-result v7

    iget-object v9, v5, LX/48q;->a:LX/48r;

    iget-object v9, v9, LX/48r;->o:LX/1mz;

    sget-object v10, LX/48t;->k:[I

    aget v10, v10, v35

    sget-object v11, LX/48t;->g:[I

    aget v11, v11, v35

    invoke-virtual {v9, v10, v11}, LX/1mz;->a(II)F

    move-result v9

    add-float/2addr v7, v9

    iget-object v9, v5, LX/48q;->a:LX/48r;

    iget-object v9, v9, LX/48r;->n:LX/1mz;

    sget-object v10, LX/48t;->l:[I

    aget v10, v10, v35

    sget-object v11, LX/48t;->h:[I

    aget v11, v11, v35

    invoke-virtual {v9, v10, v11}, LX/1mz;->a(II)F

    move-result v9

    iget-object v5, v5, LX/48q;->a:LX/48r;

    iget-object v5, v5, LX/48r;->o:LX/1mz;

    sget-object v10, LX/48t;->l:[I

    aget v10, v10, v35

    sget-object v11, LX/48t;->h:[I

    aget v11, v11, v35

    invoke-virtual {v5, v10, v11}, LX/1mz;->a(II)F

    move-result v5

    add-float/2addr v5, v9

    add-float/2addr v5, v7

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    iput v4, v6, LX/48o;->d:F

    :cond_22
    move-object/from16 v5, v17

    move-object/from16 v4, v18

    goto/16 :goto_11

    .line 673559
    :cond_23
    const/high16 v6, 0x7fc00000    # NaNf

    .line 673560
    const/high16 v7, 0x7fc00000    # NaNf

    .line 673561
    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    .line 673562
    sget-object v10, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    .line 673563
    iget-object v9, v5, LX/48q;->a:LX/48r;

    iget-object v9, v9, LX/48r;->q:[F

    sget-object v11, LX/48t;->j:[I

    sget v20, LX/48t;->c:I

    aget v11, v11, v20

    aget v9, v9, v11

    float-to-double v0, v9

    move-wide/from16 v22, v0

    const-wide/16 v24, 0x0

    cmpl-double v9, v22, v24

    if-ltz v9, :cond_24

    .line 673564
    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->q:[F

    const/4 v6, 0x0

    aget v4, v4, v6

    iget-object v6, v5, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->m:LX/1mz;

    sget-object v9, LX/48t;->k:[I

    sget v11, LX/48t;->c:I

    aget v9, v9, v11

    sget-object v11, LX/48t;->g:[I

    sget v20, LX/48t;->c:I

    aget v11, v11, v20

    invoke-virtual {v6, v9, v11}, LX/1mz;->a(II)F

    move-result v6

    iget-object v9, v5, LX/48q;->a:LX/48r;

    iget-object v9, v9, LX/48r;->m:LX/1mz;

    sget-object v11, LX/48t;->l:[I

    sget v20, LX/48t;->c:I

    aget v11, v11, v20

    sget-object v20, LX/48t;->h:[I

    sget v22, LX/48t;->c:I

    aget v20, v20, v22

    move/from16 v0, v20

    invoke-virtual {v9, v11, v0}, LX/1mz;->a(II)F

    move-result v9

    add-float/2addr v6, v9

    add-float/2addr v6, v4

    .line 673565
    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    .line 673566
    :cond_24
    iget-object v9, v5, LX/48q;->a:LX/48r;

    iget-object v9, v9, LX/48r;->q:[F

    sget-object v11, LX/48t;->j:[I

    sget v20, LX/48t;->a:I

    aget v11, v11, v20

    aget v9, v9, v11

    float-to-double v0, v9

    move-wide/from16 v22, v0

    const-wide/16 v24, 0x0

    cmpl-double v9, v22, v24

    if-ltz v9, :cond_25

    .line 673567
    iget-object v7, v5, LX/48q;->a:LX/48r;

    iget-object v7, v7, LX/48r;->q:[F

    const/4 v9, 0x1

    aget v7, v7, v9

    iget-object v9, v5, LX/48q;->a:LX/48r;

    iget-object v9, v9, LX/48r;->m:LX/1mz;

    sget-object v10, LX/48t;->k:[I

    sget v11, LX/48t;->a:I

    aget v10, v10, v11

    sget-object v11, LX/48t;->g:[I

    sget v20, LX/48t;->a:I

    aget v11, v11, v20

    invoke-virtual {v9, v10, v11}, LX/1mz;->a(II)F

    move-result v9

    iget-object v10, v5, LX/48q;->a:LX/48r;

    iget-object v10, v10, LX/48r;->m:LX/1mz;

    sget-object v11, LX/48t;->l:[I

    sget v20, LX/48t;->a:I

    aget v11, v11, v20

    sget-object v20, LX/48t;->h:[I

    sget v22, LX/48t;->a:I

    aget v20, v20, v22

    move/from16 v0, v20

    invoke-virtual {v10, v11, v0}, LX/1mz;->a(II)F

    move-result v10

    add-float/2addr v9, v10

    add-float/2addr v7, v9

    .line 673568
    sget-object v10, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    .line 673569
    :cond_25
    if-nez v12, :cond_26

    move-object/from16 v0, p1

    iget-object v9, v0, LX/48q;->a:LX/48r;

    iget-object v9, v9, LX/48r;->i:Lcom/facebook/csslayout/YogaOverflow;

    sget-object v11, Lcom/facebook/csslayout/YogaOverflow;->SCROLL:Lcom/facebook/csslayout/YogaOverflow;

    if-eq v9, v11, :cond_27

    :cond_26
    move-object/from16 v0, p1

    iget-object v9, v0, LX/48q;->a:LX/48r;

    iget-object v9, v9, LX/48r;->i:Lcom/facebook/csslayout/YogaOverflow;

    sget-object v11, Lcom/facebook/csslayout/YogaOverflow;->SCROLL:Lcom/facebook/csslayout/YogaOverflow;

    if-eq v9, v11, :cond_8e

    .line 673570
    :cond_27
    invoke-static {v6}, Ljava/lang/Float;->isNaN(F)Z

    move-result v9

    if-eqz v9, :cond_8e

    invoke-static/range {v16 .. v16}, Ljava/lang/Float;->isNaN(F)Z

    move-result v9

    if-nez v9, :cond_8e

    .line 673571
    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object v9, v4

    move/from16 v6, v16

    .line 673572
    :goto_13
    if-eqz v12, :cond_28

    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->i:Lcom/facebook/csslayout/YogaOverflow;

    sget-object v11, Lcom/facebook/csslayout/YogaOverflow;->SCROLL:Lcom/facebook/csslayout/YogaOverflow;

    if-eq v4, v11, :cond_29

    :cond_28
    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->i:Lcom/facebook/csslayout/YogaOverflow;

    sget-object v11, Lcom/facebook/csslayout/YogaOverflow;->SCROLL:Lcom/facebook/csslayout/YogaOverflow;

    if-eq v4, v11, :cond_2a

    .line 673573
    :cond_29
    invoke-static {v7}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-eqz v4, :cond_2a

    invoke-static {v15}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-nez v4, :cond_2a

    .line 673574
    sget-object v10, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    move v7, v15

    .line 673575
    :cond_2a
    if-nez v12, :cond_2b

    invoke-static/range {v16 .. v16}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-nez v4, :cond_2b

    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->q:[F

    sget-object v11, LX/48t;->j:[I

    sget v20, LX/48t;->c:I

    aget v11, v11, v20

    aget v4, v4, v11

    float-to-double v0, v4

    move-wide/from16 v22, v0

    const-wide/16 v24, 0x0

    cmpl-double v4, v22, v24

    if-gez v4, :cond_2b

    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p5

    if-ne v0, v4, :cond_2b

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/48t;->a(LX/48q;LX/48q;)Lcom/facebook/csslayout/YogaAlign;

    move-result-object v4

    sget-object v11, Lcom/facebook/csslayout/YogaAlign;->STRETCH:Lcom/facebook/csslayout/YogaAlign;

    if-ne v4, v11, :cond_2b

    .line 673576
    sget-object v9, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    move/from16 v6, v16

    .line 673577
    :cond_2b
    if-eqz v12, :cond_2c

    invoke-static {v15}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-nez v4, :cond_2c

    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->q:[F

    sget-object v11, LX/48t;->j:[I

    sget v20, LX/48t;->a:I

    aget v11, v11, v20

    aget v4, v4, v11

    float-to-double v0, v4

    move-wide/from16 v22, v0

    const-wide/16 v24, 0x0

    cmpl-double v4, v22, v24

    if-gez v4, :cond_2c

    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p6

    if-ne v0, v4, :cond_2c

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/48t;->a(LX/48q;LX/48q;)Lcom/facebook/csslayout/YogaAlign;

    move-result-object v4

    sget-object v11, Lcom/facebook/csslayout/YogaAlign;->STRETCH:Lcom/facebook/csslayout/YogaAlign;

    if-ne v4, v11, :cond_2c

    .line 673578
    sget-object v10, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    move v7, v15

    .line 673579
    :cond_2c
    const/4 v11, 0x0

    move-object/from16 v4, p0

    invoke-static/range {v4 .. v11}, LX/48t;->a(LX/1Dq;LX/48q;FFLcom/facebook/csslayout/YogaDirection;Lcom/facebook/csslayout/YogaMeasureMode;Lcom/facebook/csslayout/YogaMeasureMode;Z)Z

    .line 673580
    iget-object v6, v5, LX/48q;->b:LX/48o;

    if-eqz v12, :cond_2d

    iget-object v4, v5, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    const/4 v7, 0x0

    aget v4, v4, v7

    goto/16 :goto_12

    :cond_2d
    iget-object v4, v5, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    const/4 v7, 0x1

    aget v4, v4, v7

    goto/16 :goto_12

    .line 673581
    :cond_2e
    const/16 v22, 0x0

    .line 673582
    const/16 v25, 0x0

    .line 673583
    const/4 v6, 0x0

    .line 673584
    const/4 v5, 0x0

    .line 673585
    const/4 v4, 0x0

    move/from16 v26, v4

    move/from16 v27, v5

    move/from16 v28, v6

    move/from16 v4, v25

    .line 673586
    :goto_14
    move/from16 v0, v34

    if-ge v4, v0, :cond_5d

    .line 673587
    const/4 v15, 0x0

    .line 673588
    const/4 v10, 0x0

    .line 673589
    const/4 v9, 0x0

    .line 673590
    const/4 v7, 0x0

    .line 673591
    const/4 v6, 0x0

    .line 673592
    const/4 v5, 0x0

    move/from16 v25, v4

    move/from16 v17, v22

    .line 673593
    :goto_15
    move/from16 v0, v17

    move/from16 v1, v34

    if-ge v0, v1, :cond_34

    .line 673594
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, LX/48q;->a(I)LX/48q;

    move-result-object v4

    .line 673595
    move/from16 v0, v28

    iput v0, v4, LX/48q;->d:I

    .line 673596
    iget-object v11, v4, LX/48q;->a:LX/48r;

    iget-object v11, v11, LX/48r;->g:Lcom/facebook/csslayout/YogaPositionType;

    sget-object v19, Lcom/facebook/csslayout/YogaPositionType;->ABSOLUTE:Lcom/facebook/csslayout/YogaPositionType;

    move-object/from16 v0, v19

    if-eq v11, v0, :cond_8d

    .line 673597
    iget-object v11, v4, LX/48q;->b:LX/48o;

    iget v11, v11, LX/48o;->d:F

    iget-object v0, v4, LX/48q;->a:LX/48r;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, LX/48r;->m:LX/1mz;

    move-object/from16 v19, v0

    sget-object v20, LX/48t;->k:[I

    aget v20, v20, v35

    sget-object v23, LX/48t;->g:[I

    aget v23, v23, v35

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/1mz;->a(II)F

    move-result v19

    iget-object v0, v4, LX/48q;->a:LX/48r;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, LX/48r;->m:LX/1mz;

    move-object/from16 v20, v0

    sget-object v23, LX/48t;->l:[I

    aget v23, v23, v35

    sget-object v24, LX/48t;->h:[I

    aget v24, v24, v35

    move-object/from16 v0, v20

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, LX/1mz;->a(II)F

    move-result v20

    add-float v19, v19, v20

    add-float v11, v11, v19

    .line 673598
    add-float v19, v10, v11

    cmpl-float v19, v19, v29

    if-lez v19, :cond_2f

    if-eqz v13, :cond_2f

    if-gtz v15, :cond_34

    .line 673599
    :cond_2f
    add-float/2addr v10, v11

    .line 673600
    add-int/lit8 v11, v15, 0x1

    .line 673601
    iget-object v15, v4, LX/48q;->a:LX/48r;

    iget-object v15, v15, LX/48r;->g:Lcom/facebook/csslayout/YogaPositionType;

    sget-object v19, Lcom/facebook/csslayout/YogaPositionType;->RELATIVE:Lcom/facebook/csslayout/YogaPositionType;

    move-object/from16 v0, v19

    if-ne v15, v0, :cond_31

    iget-object v15, v4, LX/48q;->a:LX/48r;

    iget v15, v15, LX/48r;->j:F

    const/16 v19, 0x0

    cmpl-float v15, v15, v19

    if-nez v15, :cond_30

    iget-object v15, v4, LX/48q;->a:LX/48r;

    iget v15, v15, LX/48r;->k:F

    const/16 v19, 0x0

    cmpl-float v15, v15, v19

    if-eqz v15, :cond_31

    .line 673602
    :cond_30
    invoke-static {v4}, LX/48t;->b(LX/48q;)F

    move-result v15

    add-float/2addr v9, v15

    .line 673603
    invoke-static {v4}, LX/48t;->c(LX/48q;)F

    move-result v15

    iget-object v0, v4, LX/48q;->b:LX/48o;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, LX/48o;->d:F

    move/from16 v19, v0

    mul-float v15, v15, v19

    add-float/2addr v7, v15

    .line 673604
    :cond_31
    if-nez v6, :cond_32

    move-object v6, v4

    .line 673605
    :cond_32
    if-eqz v5, :cond_33

    .line 673606
    iput-object v4, v5, LX/48q;->e:LX/48q;

    .line 673607
    :cond_33
    const/4 v5, 0x0

    iput-object v5, v4, LX/48q;->e:LX/48q;

    move-object v5, v6

    move v6, v7

    move v7, v9

    move v9, v10

    move v10, v11

    .line 673608
    :goto_16
    add-int/lit8 v15, v17, 0x1

    .line 673609
    add-int/lit8 v11, v25, 0x1

    move/from16 v25, v11

    move/from16 v17, v15

    move v15, v10

    move v10, v9

    move v9, v7

    move v7, v6

    move-object v6, v5

    move-object v5, v4

    goto/16 :goto_15

    .line 673610
    :cond_34
    if-nez p7, :cond_36

    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, v30

    if-ne v0, v4, :cond_36

    const/4 v4, 0x1

    move/from16 v17, v4

    .line 673611
    :goto_17
    const/16 v20, 0x0

    .line 673612
    const/16 v19, 0x0

    .line 673613
    const/4 v4, 0x0

    .line 673614
    invoke-static/range {v29 .. v29}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-nez v5, :cond_37

    .line 673615
    sub-float v4, v29, v10

    move/from16 v24, v4

    .line 673616
    :goto_18
    const/4 v4, 0x0

    .line 673617
    if-nez v17, :cond_44

    .line 673618
    const/4 v10, 0x0

    .line 673619
    const/4 v5, 0x0

    move-object v11, v6

    move/from16 v50, v10

    move v10, v4

    move v4, v5

    move/from16 v5, v50

    .line 673620
    :goto_19
    if-eqz v11, :cond_39

    .line 673621
    iget-object v0, v11, LX/48q;->b:LX/48o;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, LX/48o;->d:F

    move/from16 v23, v0

    .line 673622
    const/16 v42, 0x0

    cmpg-float v42, v24, v42

    if-gez v42, :cond_38

    .line 673623
    invoke-static {v11}, LX/48t;->c(LX/48q;)F

    move-result v42

    mul-float v42, v42, v23

    .line 673624
    const/16 v43, 0x0

    cmpl-float v43, v42, v43

    if-eqz v43, :cond_35

    .line 673625
    div-float v43, v24, v7

    mul-float v43, v43, v42

    add-float v43, v43, v23

    .line 673626
    move/from16 v0, v35

    move/from16 v1, v43

    invoke-static {v11, v0, v1}, LX/48t;->b(LX/48q;IF)F

    move-result v44

    .line 673627
    cmpl-float v43, v43, v44

    if-eqz v43, :cond_35

    .line 673628
    sub-float v23, v44, v23

    sub-float v10, v10, v23

    .line 673629
    sub-float v5, v5, v42

    .line 673630
    :cond_35
    :goto_1a
    iget-object v11, v11, LX/48q;->e:LX/48q;

    goto :goto_19

    .line 673631
    :cond_36
    const/4 v4, 0x0

    move/from16 v17, v4

    goto :goto_17

    .line 673632
    :cond_37
    const/4 v5, 0x0

    cmpg-float v5, v10, v5

    if-gez v5, :cond_8c

    .line 673633
    neg-float v4, v10

    move/from16 v24, v4

    goto :goto_18

    .line 673634
    :cond_38
    const/16 v42, 0x0

    cmpl-float v42, v24, v42

    if-lez v42, :cond_35

    .line 673635
    invoke-static {v11}, LX/48t;->b(LX/48q;)F

    move-result v42

    .line 673636
    const/16 v43, 0x0

    cmpl-float v43, v42, v43

    if-eqz v43, :cond_35

    .line 673637
    div-float v43, v24, v9

    mul-float v43, v43, v42

    add-float v43, v43, v23

    .line 673638
    move/from16 v0, v35

    move/from16 v1, v43

    invoke-static {v11, v0, v1}, LX/48t;->b(LX/48q;IF)F

    move-result v44

    .line 673639
    cmpl-float v43, v43, v44

    if-eqz v43, :cond_35

    .line 673640
    sub-float v23, v44, v23

    sub-float v10, v10, v23

    .line 673641
    sub-float v4, v4, v42

    goto :goto_1a

    .line 673642
    :cond_39
    add-float v42, v7, v5

    .line 673643
    add-float v43, v9, v4

    .line 673644
    add-float v44, v24, v10

    .line 673645
    const/4 v4, 0x0

    move-object v5, v6

    .line 673646
    :goto_1b
    if-eqz v5, :cond_44

    .line 673647
    iget-object v6, v5, LX/48q;->b:LX/48o;

    iget v7, v6, LX/48o;->d:F

    .line 673648
    const/4 v6, 0x0

    cmpg-float v6, v44, v6

    if-gez v6, :cond_3a

    .line 673649
    invoke-static {v5}, LX/48t;->c(LX/48q;)F

    move-result v6

    mul-float/2addr v6, v7

    .line 673650
    const/4 v9, 0x0

    cmpl-float v9, v6, v9

    if-eqz v9, :cond_8b

    .line 673651
    div-float v9, v44, v42

    mul-float/2addr v6, v9

    add-float/2addr v6, v7

    move/from16 v0, v35

    invoke-static {v5, v0, v6}, LX/48t;->b(LX/48q;IF)F

    move-result v6

    .line 673652
    :goto_1c
    sub-float v7, v6, v7

    sub-float v23, v4, v7

    .line 673653
    if-eqz v12, :cond_3e

    .line 673654
    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->m:LX/1mz;

    sget-object v7, LX/48t;->k:[I

    sget v9, LX/48t;->c:I

    aget v7, v7, v9

    sget-object v9, LX/48t;->g:[I

    sget v10, LX/48t;->c:I

    aget v9, v9, v10

    invoke-virtual {v4, v7, v9}, LX/1mz;->a(II)F

    move-result v4

    iget-object v7, v5, LX/48q;->a:LX/48r;

    iget-object v7, v7, LX/48r;->m:LX/1mz;

    sget-object v9, LX/48t;->l:[I

    sget v10, LX/48t;->c:I

    aget v9, v9, v10

    sget-object v10, LX/48t;->h:[I

    sget v11, LX/48t;->c:I

    aget v10, v10, v11

    invoke-virtual {v7, v9, v10}, LX/1mz;->a(II)F

    move-result v7

    add-float/2addr v4, v7

    add-float/2addr v6, v4

    .line 673655
    sget-object v9, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    .line 673656
    invoke-static {v14}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-nez v4, :cond_3b

    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->q:[F

    sget-object v7, LX/48t;->j:[I

    sget v10, LX/48t;->a:I

    aget v7, v7, v10

    aget v4, v4, v7

    float-to-double v10, v4

    const-wide/16 v46, 0x0

    cmpl-double v4, v10, v46

    if-gez v4, :cond_3b

    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p6

    if-ne v0, v4, :cond_3b

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/48t;->a(LX/48q;LX/48q;)Lcom/facebook/csslayout/YogaAlign;

    move-result-object v4

    sget-object v7, Lcom/facebook/csslayout/YogaAlign;->STRETCH:Lcom/facebook/csslayout/YogaAlign;

    if-ne v4, v7, :cond_3b

    .line 673657
    sget-object v10, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    move v7, v14

    .line 673658
    :goto_1d
    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->q:[F

    sget-object v11, LX/48t;->j:[I

    aget v11, v11, v36

    aget v4, v4, v11

    float-to-double v0, v4

    move-wide/from16 v46, v0

    const-wide/16 v48, 0x0

    cmpl-double v4, v46, v48

    if-gez v4, :cond_42

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/48t;->a(LX/48q;LX/48q;)Lcom/facebook/csslayout/YogaAlign;

    move-result-object v4

    sget-object v11, Lcom/facebook/csslayout/YogaAlign;->STRETCH:Lcom/facebook/csslayout/YogaAlign;

    if-ne v4, v11, :cond_42

    const/4 v4, 0x1

    .line 673659
    :goto_1e
    if-eqz p7, :cond_43

    if-nez v4, :cond_43

    const/4 v11, 0x1

    :goto_1f
    move-object/from16 v4, p0

    invoke-static/range {v4 .. v11}, LX/48t;->a(LX/1Dq;LX/48q;FFLcom/facebook/csslayout/YogaDirection;Lcom/facebook/csslayout/YogaMeasureMode;Lcom/facebook/csslayout/YogaMeasureMode;Z)Z

    .line 673660
    iget-object v5, v5, LX/48q;->e:LX/48q;

    move/from16 v4, v23

    .line 673661
    goto/16 :goto_1b

    .line 673662
    :cond_3a
    const/4 v6, 0x0

    cmpl-float v6, v44, v6

    if-lez v6, :cond_8b

    .line 673663
    invoke-static {v5}, LX/48t;->b(LX/48q;)F

    move-result v6

    .line 673664
    const/4 v9, 0x0

    cmpl-float v9, v6, v9

    if-eqz v9, :cond_8b

    .line 673665
    div-float v9, v44, v43

    mul-float/2addr v6, v9

    add-float/2addr v6, v7

    move/from16 v0, v35

    invoke-static {v5, v0, v6}, LX/48t;->b(LX/48q;IF)F

    move-result v6

    goto/16 :goto_1c

    .line 673666
    :cond_3b
    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->q:[F

    sget-object v7, LX/48t;->j:[I

    sget v10, LX/48t;->a:I

    aget v7, v7, v10

    aget v4, v4, v7

    float-to-double v10, v4

    const-wide/16 v46, 0x0

    cmpl-double v4, v10, v46

    if-gez v4, :cond_3d

    .line 673667
    invoke-static {v14}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-eqz v4, :cond_3c

    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    :goto_20
    move-object v10, v4

    move v7, v14

    goto :goto_1d

    :cond_3c
    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    goto :goto_20

    .line 673668
    :cond_3d
    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->q:[F

    const/4 v7, 0x1

    aget v4, v4, v7

    iget-object v7, v5, LX/48q;->a:LX/48r;

    iget-object v7, v7, LX/48r;->m:LX/1mz;

    sget-object v10, LX/48t;->k:[I

    sget v11, LX/48t;->a:I

    aget v10, v10, v11

    sget-object v11, LX/48t;->g:[I

    sget v45, LX/48t;->a:I

    aget v11, v11, v45

    invoke-virtual {v7, v10, v11}, LX/1mz;->a(II)F

    move-result v7

    iget-object v10, v5, LX/48q;->a:LX/48r;

    iget-object v10, v10, LX/48r;->m:LX/1mz;

    sget-object v11, LX/48t;->l:[I

    sget v45, LX/48t;->a:I

    aget v11, v11, v45

    sget-object v45, LX/48t;->h:[I

    sget v46, LX/48t;->a:I

    aget v45, v45, v46

    move/from16 v0, v45

    invoke-virtual {v10, v11, v0}, LX/1mz;->a(II)F

    move-result v10

    add-float/2addr v7, v10

    add-float/2addr v7, v4

    .line 673669
    sget-object v10, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    goto/16 :goto_1d

    .line 673670
    :cond_3e
    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->m:LX/1mz;

    sget-object v7, LX/48t;->k:[I

    sget v9, LX/48t;->a:I

    aget v7, v7, v9

    sget-object v9, LX/48t;->g:[I

    sget v10, LX/48t;->a:I

    aget v9, v9, v10

    invoke-virtual {v4, v7, v9}, LX/1mz;->a(II)F

    move-result v4

    iget-object v7, v5, LX/48q;->a:LX/48r;

    iget-object v7, v7, LX/48r;->m:LX/1mz;

    sget-object v9, LX/48t;->l:[I

    sget v10, LX/48t;->a:I

    aget v9, v9, v10

    sget-object v10, LX/48t;->h:[I

    sget v11, LX/48t;->a:I

    aget v10, v10, v11

    invoke-virtual {v7, v9, v10}, LX/1mz;->a(II)F

    move-result v7

    add-float/2addr v4, v7

    add-float v7, v6, v4

    .line 673671
    sget-object v10, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    .line 673672
    invoke-static {v14}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-nez v4, :cond_3f

    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->q:[F

    sget-object v6, LX/48t;->j:[I

    sget v9, LX/48t;->c:I

    aget v6, v6, v9

    aget v4, v4, v6

    float-to-double v0, v4

    move-wide/from16 v46, v0

    const-wide/16 v48, 0x0

    cmpl-double v4, v46, v48

    if-gez v4, :cond_3f

    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, p5

    if-ne v0, v4, :cond_3f

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/48t;->a(LX/48q;LX/48q;)Lcom/facebook/csslayout/YogaAlign;

    move-result-object v4

    sget-object v6, Lcom/facebook/csslayout/YogaAlign;->STRETCH:Lcom/facebook/csslayout/YogaAlign;

    if-ne v4, v6, :cond_3f

    .line 673673
    sget-object v9, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    move v6, v14

    goto/16 :goto_1d

    .line 673674
    :cond_3f
    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->q:[F

    sget-object v6, LX/48t;->j:[I

    sget v9, LX/48t;->c:I

    aget v6, v6, v9

    aget v4, v4, v6

    float-to-double v0, v4

    move-wide/from16 v46, v0

    const-wide/16 v48, 0x0

    cmpl-double v4, v46, v48

    if-gez v4, :cond_41

    .line 673675
    invoke-static {v14}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-eqz v4, :cond_40

    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    :goto_21
    move-object v9, v4

    move v6, v14

    goto/16 :goto_1d

    :cond_40
    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    goto :goto_21

    .line 673676
    :cond_41
    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->q:[F

    const/4 v6, 0x0

    aget v4, v4, v6

    iget-object v6, v5, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->m:LX/1mz;

    sget-object v9, LX/48t;->k:[I

    sget v11, LX/48t;->c:I

    aget v9, v9, v11

    sget-object v11, LX/48t;->g:[I

    sget v45, LX/48t;->c:I

    aget v11, v11, v45

    invoke-virtual {v6, v9, v11}, LX/1mz;->a(II)F

    move-result v6

    iget-object v9, v5, LX/48q;->a:LX/48r;

    iget-object v9, v9, LX/48r;->m:LX/1mz;

    sget-object v11, LX/48t;->l:[I

    sget v45, LX/48t;->c:I

    aget v11, v11, v45

    sget-object v45, LX/48t;->h:[I

    sget v46, LX/48t;->c:I

    aget v45, v45, v46

    move/from16 v0, v45

    invoke-virtual {v9, v11, v0}, LX/1mz;->a(II)F

    move-result v9

    add-float/2addr v6, v9

    add-float/2addr v6, v4

    .line 673677
    sget-object v9, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    goto/16 :goto_1d

    .line 673678
    :cond_42
    const/4 v4, 0x0

    goto/16 :goto_1e

    .line 673679
    :cond_43
    const/4 v11, 0x0

    goto/16 :goto_1f

    .line 673680
    :cond_44
    add-float v4, v4, v24

    .line 673681
    sget-object v5, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, v31

    if-ne v0, v5, :cond_45

    .line 673682
    const/4 v4, 0x0

    .line 673683
    :cond_45
    sget-object v5, Lcom/facebook/csslayout/YogaJustify;->FLEX_START:Lcom/facebook/csslayout/YogaJustify;

    move-object/from16 v0, v37

    if-eq v0, v5, :cond_8a

    .line 673684
    sget-object v5, Lcom/facebook/csslayout/YogaJustify;->CENTER:Lcom/facebook/csslayout/YogaJustify;

    move-object/from16 v0, v37

    if-ne v0, v5, :cond_46

    .line 673685
    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    move v5, v4

    move/from16 v4, v19

    .line 673686
    :goto_22
    add-float v7, v38, v5

    .line 673687
    const/4 v6, 0x0

    move/from16 v9, v22

    .line 673688
    :goto_23
    move/from16 v0, v25

    if-ge v9, v0, :cond_4e

    .line 673689
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/48q;->a(I)LX/48q;

    move-result-object v10

    .line 673690
    iget-object v5, v10, LX/48q;->a:LX/48r;

    iget-object v5, v5, LX/48r;->g:Lcom/facebook/csslayout/YogaPositionType;

    sget-object v11, Lcom/facebook/csslayout/YogaPositionType;->ABSOLUTE:Lcom/facebook/csslayout/YogaPositionType;

    if-ne v5, v11, :cond_4b

    iget-object v5, v10, LX/48q;->a:LX/48r;

    iget-object v5, v5, LX/48r;->p:LX/1mz;

    sget-object v11, LX/48t;->k:[I

    aget v11, v11, v35

    sget-object v15, LX/48t;->g:[I

    aget v15, v15, v35

    invoke-virtual {v5, v11, v15}, LX/1mz;->a(II)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-nez v5, :cond_4b

    .line 673691
    if-eqz p7, :cond_89

    .line 673692
    iget-object v5, v10, LX/48q;->b:LX/48o;

    iget-object v11, v5, LX/48o;->a:[F

    sget-object v5, LX/48t;->i:[I

    aget v15, v5, v35

    iget-object v5, v10, LX/48q;->a:LX/48r;

    iget-object v5, v5, LX/48r;->p:LX/1mz;

    sget-object v19, LX/48t;->k:[I

    aget v19, v19, v35

    sget-object v20, LX/48t;->g:[I

    aget v20, v20, v35

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v5, v0, v1}, LX/1mz;->a(II)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-eqz v5, :cond_4a

    const/4 v5, 0x0

    :goto_24
    move-object/from16 v0, p1

    iget-object v0, v0, LX/48q;->a:LX/48r;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, LX/48r;->o:LX/1mz;

    move-object/from16 v19, v0

    sget-object v20, LX/48t;->k:[I

    aget v20, v20, v35

    sget-object v23, LX/48t;->g:[I

    aget v23, v23, v35

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/1mz;->a(II)F

    move-result v19

    add-float v5, v5, v19

    iget-object v10, v10, LX/48q;->a:LX/48r;

    iget-object v10, v10, LX/48r;->m:LX/1mz;

    sget-object v19, LX/48t;->k:[I

    aget v19, v19, v35

    sget-object v20, LX/48t;->g:[I

    aget v20, v20, v35

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v10, v0, v1}, LX/1mz;->a(II)F

    move-result v10

    add-float/2addr v5, v10

    aput v5, v11, v15

    move v5, v6

    move v6, v7

    .line 673693
    :goto_25
    add-int/lit8 v7, v9, 0x1

    move v9, v7

    move v7, v6

    move v6, v5

    goto/16 :goto_23

    .line 673694
    :cond_46
    sget-object v5, Lcom/facebook/csslayout/YogaJustify;->FLEX_END:Lcom/facebook/csslayout/YogaJustify;

    move-object/from16 v0, v37

    if-ne v0, v5, :cond_47

    move v5, v4

    move/from16 v4, v19

    .line 673695
    goto/16 :goto_22

    .line 673696
    :cond_47
    sget-object v5, Lcom/facebook/csslayout/YogaJustify;->SPACE_BETWEEN:Lcom/facebook/csslayout/YogaJustify;

    move-object/from16 v0, v37

    if-ne v0, v5, :cond_49

    .line 673697
    const/4 v5, 0x0

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 673698
    const/4 v5, 0x1

    if-le v15, v5, :cond_48

    .line 673699
    add-int/lit8 v5, v15, -0x1

    int-to-float v5, v5

    div-float/2addr v4, v5

    move/from16 v5, v20

    goto/16 :goto_22

    .line 673700
    :cond_48
    const/4 v4, 0x0

    move/from16 v5, v20

    goto/16 :goto_22

    .line 673701
    :cond_49
    sget-object v5, Lcom/facebook/csslayout/YogaJustify;->SPACE_AROUND:Lcom/facebook/csslayout/YogaJustify;

    move-object/from16 v0, v37

    if-ne v0, v5, :cond_8a

    .line 673702
    int-to-float v5, v15

    div-float/2addr v4, v5

    .line 673703
    const/high16 v5, 0x40000000    # 2.0f

    div-float v5, v4, v5

    goto/16 :goto_22

    .line 673704
    :cond_4a
    iget-object v5, v10, LX/48q;->a:LX/48r;

    iget-object v5, v5, LX/48r;->p:LX/1mz;

    sget-object v19, LX/48t;->k:[I

    aget v19, v19, v35

    sget-object v20, LX/48t;->g:[I

    aget v20, v20, v35

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v5, v0, v1}, LX/1mz;->a(II)F

    move-result v5

    goto/16 :goto_24

    .line 673705
    :cond_4b
    if-eqz p7, :cond_4c

    .line 673706
    iget-object v5, v10, LX/48q;->b:LX/48o;

    iget-object v5, v5, LX/48o;->a:[F

    sget-object v11, LX/48t;->i:[I

    aget v11, v11, v35

    aget v15, v5, v11

    add-float/2addr v15, v7

    aput v15, v5, v11

    .line 673707
    :cond_4c
    iget-object v5, v10, LX/48q;->a:LX/48r;

    iget-object v5, v5, LX/48r;->g:Lcom/facebook/csslayout/YogaPositionType;

    sget-object v11, Lcom/facebook/csslayout/YogaPositionType;->RELATIVE:Lcom/facebook/csslayout/YogaPositionType;

    if-ne v5, v11, :cond_89

    .line 673708
    if-eqz v17, :cond_4d

    .line 673709
    iget-object v5, v10, LX/48q;->a:LX/48r;

    iget-object v5, v5, LX/48r;->m:LX/1mz;

    sget-object v6, LX/48t;->k:[I

    aget v6, v6, v35

    sget-object v11, LX/48t;->g:[I

    aget v11, v11, v35

    invoke-virtual {v5, v6, v11}, LX/1mz;->a(II)F

    move-result v5

    iget-object v6, v10, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->m:LX/1mz;

    sget-object v11, LX/48t;->l:[I

    aget v11, v11, v35

    sget-object v15, LX/48t;->h:[I

    aget v15, v15, v35

    invoke-virtual {v6, v11, v15}, LX/1mz;->a(II)F

    move-result v6

    add-float/2addr v5, v6

    add-float/2addr v5, v4

    iget-object v6, v10, LX/48q;->b:LX/48o;

    iget v6, v6, LX/48o;->d:F

    add-float/2addr v5, v6

    add-float/2addr v5, v7

    move v6, v5

    move v5, v14

    .line 673710
    goto/16 :goto_25

    .line 673711
    :cond_4d
    iget-object v5, v10, LX/48q;->b:LX/48o;

    iget-object v5, v5, LX/48o;->i:[F

    sget-object v11, LX/48t;->j:[I

    aget v11, v11, v35

    aget v5, v5, v11

    iget-object v11, v10, LX/48q;->a:LX/48r;

    iget-object v11, v11, LX/48r;->m:LX/1mz;

    sget-object v15, LX/48t;->k:[I

    aget v15, v15, v35

    sget-object v19, LX/48t;->g:[I

    aget v19, v19, v35

    move/from16 v0, v19

    invoke-virtual {v11, v15, v0}, LX/1mz;->a(II)F

    move-result v11

    add-float/2addr v5, v11

    iget-object v11, v10, LX/48q;->a:LX/48r;

    iget-object v11, v11, LX/48r;->m:LX/1mz;

    sget-object v15, LX/48t;->l:[I

    aget v15, v15, v35

    sget-object v19, LX/48t;->h:[I

    aget v19, v19, v35

    move/from16 v0, v19

    invoke-virtual {v11, v15, v0}, LX/1mz;->a(II)F

    move-result v11

    add-float/2addr v5, v11

    add-float/2addr v5, v4

    add-float/2addr v7, v5

    .line 673712
    iget-object v5, v10, LX/48q;->b:LX/48o;

    iget-object v5, v5, LX/48o;->i:[F

    sget-object v11, LX/48t;->j:[I

    aget v11, v11, v36

    aget v5, v5, v11

    iget-object v11, v10, LX/48q;->a:LX/48r;

    iget-object v11, v11, LX/48r;->m:LX/1mz;

    sget-object v15, LX/48t;->k:[I

    aget v15, v15, v36

    sget-object v19, LX/48t;->g:[I

    aget v19, v19, v36

    move/from16 v0, v19

    invoke-virtual {v11, v15, v0}, LX/1mz;->a(II)F

    move-result v11

    add-float/2addr v5, v11

    iget-object v10, v10, LX/48q;->a:LX/48r;

    iget-object v10, v10, LX/48r;->m:LX/1mz;

    sget-object v11, LX/48t;->l:[I

    aget v11, v11, v36

    sget-object v15, LX/48t;->h:[I

    aget v15, v15, v36

    invoke-virtual {v10, v11, v15}, LX/1mz;->a(II)F

    move-result v10

    add-float/2addr v5, v10

    invoke-static {v6, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    move v6, v7

    goto/16 :goto_25

    .line 673713
    :cond_4e
    add-float v20, v7, v39

    .line 673714
    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, v30

    if-eq v0, v4, :cond_4f

    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, v30

    if-ne v0, v4, :cond_88

    .line 673715
    :cond_4f
    add-float v4, v6, v41

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-static {v0, v1, v4}, LX/48t;->b(LX/48q;IF)F

    move-result v4

    sub-float v4, v4, v41

    .line 673716
    sget-object v5, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, v30

    if-ne v0, v5, :cond_87

    .line 673717
    invoke-static {v4, v14}, Ljava/lang/Math;->min(FF)F

    move-result v4

    move/from16 v19, v4

    .line 673718
    :goto_26
    if-nez v13, :cond_50

    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, v30

    if-ne v0, v4, :cond_50

    move v6, v14

    .line 673719
    :cond_50
    add-float v4, v6, v41

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-static {v0, v1, v4}, LX/48t;->b(LX/48q;IF)F

    move-result v4

    sub-float v15, v4, v41

    .line 673720
    if-eqz p7, :cond_5c

    move/from16 v17, v22

    .line 673721
    :goto_27
    move/from16 v0, v17

    move/from16 v1, v25

    if-ge v0, v1, :cond_5c

    .line 673722
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, LX/48q;->a(I)LX/48q;

    move-result-object v5

    .line 673723
    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->g:Lcom/facebook/csslayout/YogaPositionType;

    sget-object v6, Lcom/facebook/csslayout/YogaPositionType;->ABSOLUTE:Lcom/facebook/csslayout/YogaPositionType;

    if-ne v4, v6, :cond_53

    .line 673724
    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->p:LX/1mz;

    sget-object v6, LX/48t;->k:[I

    aget v6, v6, v36

    sget-object v7, LX/48t;->g:[I

    aget v7, v7, v36

    invoke-virtual {v4, v6, v7}, LX/1mz;->a(II)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-nez v4, :cond_52

    .line 673725
    iget-object v4, v5, LX/48q;->b:LX/48o;

    iget-object v6, v4, LX/48o;->a:[F

    sget-object v4, LX/48t;->i:[I

    aget v7, v4, v36

    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->p:LX/1mz;

    sget-object v9, LX/48t;->k:[I

    aget v9, v9, v36

    sget-object v10, LX/48t;->g:[I

    aget v10, v10, v36

    invoke-virtual {v4, v9, v10}, LX/1mz;->a(II)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-eqz v4, :cond_51

    const/4 v4, 0x0

    :goto_28
    move-object/from16 v0, p1

    iget-object v9, v0, LX/48q;->a:LX/48r;

    iget-object v9, v9, LX/48r;->o:LX/1mz;

    sget-object v10, LX/48t;->k:[I

    aget v10, v10, v36

    sget-object v11, LX/48t;->g:[I

    aget v11, v11, v36

    invoke-virtual {v9, v10, v11}, LX/1mz;->a(II)F

    move-result v9

    add-float/2addr v4, v9

    iget-object v5, v5, LX/48q;->a:LX/48r;

    iget-object v5, v5, LX/48r;->m:LX/1mz;

    sget-object v9, LX/48t;->k:[I

    aget v9, v9, v36

    sget-object v10, LX/48t;->g:[I

    aget v10, v10, v36

    invoke-virtual {v5, v9, v10}, LX/1mz;->a(II)F

    move-result v5

    add-float/2addr v4, v5

    aput v4, v6, v7

    .line 673726
    :goto_29
    add-int/lit8 v4, v17, 0x1

    move/from16 v17, v4

    goto :goto_27

    .line 673727
    :cond_51
    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->p:LX/1mz;

    sget-object v9, LX/48t;->k:[I

    aget v9, v9, v36

    sget-object v10, LX/48t;->g:[I

    aget v10, v10, v36

    invoke-virtual {v4, v9, v10}, LX/1mz;->a(II)F

    move-result v4

    goto :goto_28

    .line 673728
    :cond_52
    iget-object v4, v5, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->a:[F

    sget-object v6, LX/48t;->i:[I

    aget v6, v6, v36

    iget-object v5, v5, LX/48q;->a:LX/48r;

    iget-object v5, v5, LX/48r;->m:LX/1mz;

    sget-object v7, LX/48t;->k:[I

    aget v7, v7, v36

    sget-object v9, LX/48t;->g:[I

    aget v9, v9, v36

    invoke-virtual {v5, v7, v9}, LX/1mz;->a(II)F

    move-result v5

    add-float v5, v5, v21

    aput v5, v4, v6

    goto :goto_29

    .line 673729
    :cond_53
    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/48t;->a(LX/48q;LX/48q;)Lcom/facebook/csslayout/YogaAlign;

    move-result-object v4

    .line 673730
    sget-object v6, Lcom/facebook/csslayout/YogaAlign;->STRETCH:Lcom/facebook/csslayout/YogaAlign;

    if-ne v4, v6, :cond_5a

    .line 673731
    iget-object v4, v5, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    const/4 v6, 0x0

    aget v4, v4, v6

    iget-object v6, v5, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->m:LX/1mz;

    sget-object v7, LX/48t;->k:[I

    sget v9, LX/48t;->c:I

    aget v7, v7, v9

    sget-object v9, LX/48t;->g:[I

    sget v10, LX/48t;->c:I

    aget v9, v9, v10

    invoke-virtual {v6, v7, v9}, LX/1mz;->a(II)F

    move-result v6

    iget-object v7, v5, LX/48q;->a:LX/48r;

    iget-object v7, v7, LX/48r;->m:LX/1mz;

    sget-object v9, LX/48t;->l:[I

    sget v10, LX/48t;->c:I

    aget v9, v9, v10

    sget-object v10, LX/48t;->h:[I

    sget v11, LX/48t;->c:I

    aget v10, v10, v11

    invoke-virtual {v7, v9, v10}, LX/1mz;->a(II)F

    move-result v7

    add-float/2addr v6, v7

    add-float/2addr v6, v4

    .line 673732
    iget-object v4, v5, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    const/4 v7, 0x1

    aget v4, v4, v7

    iget-object v7, v5, LX/48q;->a:LX/48r;

    iget-object v7, v7, LX/48r;->m:LX/1mz;

    sget-object v9, LX/48t;->k:[I

    sget v10, LX/48t;->a:I

    aget v9, v9, v10

    sget-object v10, LX/48t;->g:[I

    sget v11, LX/48t;->a:I

    aget v10, v10, v11

    invoke-virtual {v7, v9, v10}, LX/1mz;->a(II)F

    move-result v7

    iget-object v9, v5, LX/48q;->a:LX/48r;

    iget-object v9, v9, LX/48r;->m:LX/1mz;

    sget-object v10, LX/48t;->l:[I

    sget v11, LX/48t;->a:I

    aget v10, v10, v11

    sget-object v11, LX/48t;->h:[I

    sget v22, LX/48t;->a:I

    aget v11, v11, v22

    invoke-virtual {v9, v10, v11}, LX/1mz;->a(II)F

    move-result v9

    add-float/2addr v7, v9

    add-float/2addr v7, v4

    .line 673733
    if-eqz v12, :cond_56

    .line 673734
    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->q:[F

    sget-object v7, LX/48t;->j:[I

    sget v9, LX/48t;->a:I

    aget v7, v7, v9

    aget v4, v4, v7

    float-to-double v10, v4

    const-wide/16 v22, 0x0

    cmpl-double v4, v10, v22

    if-ltz v4, :cond_55

    const/4 v4, 0x1

    :goto_2a
    move v7, v15

    .line 673735
    :goto_2b
    if-nez v4, :cond_54

    .line 673736
    invoke-static {v6}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-eqz v4, :cond_58

    sget-object v9, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    .line 673737
    :goto_2c
    invoke-static {v7}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-eqz v4, :cond_59

    sget-object v10, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    .line 673738
    :goto_2d
    const/4 v11, 0x1

    move-object/from16 v4, p0

    invoke-static/range {v4 .. v11}, LX/48t;->a(LX/1Dq;LX/48q;FFLcom/facebook/csslayout/YogaDirection;Lcom/facebook/csslayout/YogaMeasureMode;Lcom/facebook/csslayout/YogaMeasureMode;Z)Z

    :cond_54
    move/from16 v4, v21

    .line 673739
    :goto_2e
    iget-object v5, v5, LX/48q;->b:LX/48o;

    iget-object v5, v5, LX/48o;->a:[F

    sget-object v6, LX/48t;->i:[I

    aget v6, v6, v36

    aget v7, v5, v6

    add-float v4, v4, v27

    add-float/2addr v4, v7

    aput v4, v5, v6

    goto/16 :goto_29

    .line 673740
    :cond_55
    const/4 v4, 0x0

    goto :goto_2a

    .line 673741
    :cond_56
    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->q:[F

    sget-object v6, LX/48t;->j:[I

    sget v9, LX/48t;->c:I

    aget v6, v6, v9

    aget v4, v4, v6

    float-to-double v10, v4

    const-wide/16 v22, 0x0

    cmpl-double v4, v10, v22

    if-ltz v4, :cond_57

    const/4 v4, 0x1

    :goto_2f
    move v6, v15

    .line 673742
    goto :goto_2b

    .line 673743
    :cond_57
    const/4 v4, 0x0

    goto :goto_2f

    .line 673744
    :cond_58
    sget-object v9, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    goto :goto_2c

    .line 673745
    :cond_59
    sget-object v10, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    goto :goto_2d

    .line 673746
    :cond_5a
    sget-object v6, Lcom/facebook/csslayout/YogaAlign;->FLEX_START:Lcom/facebook/csslayout/YogaAlign;

    if-eq v4, v6, :cond_86

    .line 673747
    iget-object v6, v5, LX/48q;->b:LX/48o;

    iget-object v6, v6, LX/48o;->i:[F

    sget-object v7, LX/48t;->j:[I

    aget v7, v7, v36

    aget v6, v6, v7

    iget-object v7, v5, LX/48q;->a:LX/48r;

    iget-object v7, v7, LX/48r;->m:LX/1mz;

    sget-object v9, LX/48t;->k:[I

    aget v9, v9, v36

    sget-object v10, LX/48t;->g:[I

    aget v10, v10, v36

    invoke-virtual {v7, v9, v10}, LX/1mz;->a(II)F

    move-result v7

    add-float/2addr v6, v7

    iget-object v7, v5, LX/48q;->a:LX/48r;

    iget-object v7, v7, LX/48r;->m:LX/1mz;

    sget-object v9, LX/48t;->l:[I

    aget v9, v9, v36

    sget-object v10, LX/48t;->h:[I

    aget v10, v10, v36

    invoke-virtual {v7, v9, v10}, LX/1mz;->a(II)F

    move-result v7

    add-float/2addr v6, v7

    sub-float v6, v19, v6

    .line 673748
    sget-object v7, Lcom/facebook/csslayout/YogaAlign;->CENTER:Lcom/facebook/csslayout/YogaAlign;

    if-ne v4, v7, :cond_5b

    .line 673749
    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v6, v4

    add-float v4, v4, v21

    goto :goto_2e

    .line 673750
    :cond_5b
    add-float v4, v21, v6

    goto :goto_2e

    .line 673751
    :cond_5c
    add-float v5, v27, v15

    .line 673752
    move/from16 v0, v26

    move/from16 v1, v20

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 673753
    add-int/lit8 v6, v28, 0x1

    move/from16 v26, v4

    move/from16 v27, v5

    move/from16 v28, v6

    move/from16 v22, v25

    move/from16 v4, v25

    .line 673754
    goto/16 :goto_14

    .line 673755
    :cond_5d
    const/4 v4, 0x1

    move/from16 v0, v28

    if-le v0, v4, :cond_68

    if-eqz p7, :cond_68

    invoke-static {v14}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-nez v4, :cond_68

    .line 673756
    sub-float v5, v14, v27

    .line 673757
    const/4 v4, 0x0

    .line 673758
    move-object/from16 v0, p1

    iget-object v6, v0, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->d:Lcom/facebook/csslayout/YogaAlign;

    .line 673759
    sget-object v7, Lcom/facebook/csslayout/YogaAlign;->FLEX_END:Lcom/facebook/csslayout/YogaAlign;

    if-ne v6, v7, :cond_60

    .line 673760
    add-float v21, v21, v5

    .line 673761
    :cond_5e
    :goto_30
    const/4 v6, 0x0

    .line 673762
    const/4 v5, 0x0

    move v9, v5

    :goto_31
    move/from16 v0, v28

    if-ge v9, v0, :cond_68

    .line 673763
    const/4 v5, 0x0

    move v7, v6

    .line 673764
    :goto_32
    move/from16 v0, v34

    if-ge v7, v0, :cond_62

    .line 673765
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/48q;->a(I)LX/48q;

    move-result-object v10

    .line 673766
    iget-object v11, v10, LX/48q;->a:LX/48r;

    iget-object v11, v11, LX/48r;->g:Lcom/facebook/csslayout/YogaPositionType;

    sget-object v13, Lcom/facebook/csslayout/YogaPositionType;->RELATIVE:Lcom/facebook/csslayout/YogaPositionType;

    if-ne v11, v13, :cond_5f

    .line 673767
    iget v11, v10, LX/48q;->d:I

    if-ne v11, v9, :cond_62

    .line 673768
    iget-object v11, v10, LX/48q;->b:LX/48o;

    iget-object v11, v11, LX/48o;->i:[F

    sget-object v13, LX/48t;->j:[I

    aget v13, v13, v36

    aget v11, v11, v13

    float-to-double v0, v11

    move-wide/from16 v22, v0

    const-wide/16 v24, 0x0

    cmpl-double v11, v22, v24

    if-ltz v11, :cond_5f

    .line 673769
    iget-object v11, v10, LX/48q;->b:LX/48o;

    iget-object v11, v11, LX/48o;->i:[F

    sget-object v13, LX/48t;->j:[I

    aget v13, v13, v36

    aget v11, v11, v13

    iget-object v13, v10, LX/48q;->a:LX/48r;

    iget-object v13, v13, LX/48r;->m:LX/1mz;

    sget-object v15, LX/48t;->k:[I

    aget v15, v15, v36

    sget-object v17, LX/48t;->g:[I

    aget v17, v17, v36

    move/from16 v0, v17

    invoke-virtual {v13, v15, v0}, LX/1mz;->a(II)F

    move-result v13

    iget-object v10, v10, LX/48q;->a:LX/48r;

    iget-object v10, v10, LX/48r;->m:LX/1mz;

    sget-object v15, LX/48t;->l:[I

    aget v15, v15, v36

    sget-object v17, LX/48t;->h:[I

    aget v17, v17, v36

    move/from16 v0, v17

    invoke-virtual {v10, v15, v0}, LX/1mz;->a(II)F

    move-result v10

    add-float/2addr v10, v13

    add-float/2addr v10, v11

    invoke-static {v5, v10}, Ljava/lang/Math;->max(FF)F

    move-result v5

    .line 673770
    :cond_5f
    add-int/lit8 v7, v7, 0x1

    goto :goto_32

    .line 673771
    :cond_60
    sget-object v7, Lcom/facebook/csslayout/YogaAlign;->CENTER:Lcom/facebook/csslayout/YogaAlign;

    if-ne v6, v7, :cond_61

    .line 673772
    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    add-float v21, v21, v5

    goto :goto_30

    .line 673773
    :cond_61
    sget-object v7, Lcom/facebook/csslayout/YogaAlign;->STRETCH:Lcom/facebook/csslayout/YogaAlign;

    if-ne v6, v7, :cond_5e

    .line 673774
    cmpl-float v6, v14, v27

    if-lez v6, :cond_5e

    .line 673775
    move/from16 v0, v28

    int-to-float v4, v0

    div-float v4, v5, v4

    goto :goto_30

    .line 673776
    :cond_62
    add-float v10, v5, v4

    .line 673777
    if-eqz p7, :cond_67

    move v5, v6

    .line 673778
    :goto_33
    if-ge v5, v7, :cond_67

    .line 673779
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/48q;->a(I)LX/48q;

    move-result-object v6

    .line 673780
    iget-object v11, v6, LX/48q;->a:LX/48r;

    iget-object v11, v11, LX/48r;->g:Lcom/facebook/csslayout/YogaPositionType;

    sget-object v13, Lcom/facebook/csslayout/YogaPositionType;->RELATIVE:Lcom/facebook/csslayout/YogaPositionType;

    if-ne v11, v13, :cond_63

    .line 673781
    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/48t;->a(LX/48q;LX/48q;)Lcom/facebook/csslayout/YogaAlign;

    move-result-object v11

    .line 673782
    sget-object v13, Lcom/facebook/csslayout/YogaAlign;->FLEX_START:Lcom/facebook/csslayout/YogaAlign;

    if-ne v11, v13, :cond_64

    .line 673783
    iget-object v11, v6, LX/48q;->b:LX/48o;

    iget-object v11, v11, LX/48o;->a:[F

    sget-object v13, LX/48t;->i:[I

    aget v13, v13, v36

    iget-object v6, v6, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->m:LX/1mz;

    sget-object v15, LX/48t;->k:[I

    aget v15, v15, v36

    sget-object v17, LX/48t;->g:[I

    aget v17, v17, v36

    move/from16 v0, v17

    invoke-virtual {v6, v15, v0}, LX/1mz;->a(II)F

    move-result v6

    add-float v6, v6, v21

    aput v6, v11, v13

    .line 673784
    :cond_63
    :goto_34
    add-int/lit8 v5, v5, 0x1

    goto :goto_33

    .line 673785
    :cond_64
    sget-object v13, Lcom/facebook/csslayout/YogaAlign;->FLEX_END:Lcom/facebook/csslayout/YogaAlign;

    if-ne v11, v13, :cond_65

    .line 673786
    iget-object v11, v6, LX/48q;->b:LX/48o;

    iget-object v11, v11, LX/48o;->a:[F

    sget-object v13, LX/48t;->i:[I

    aget v13, v13, v36

    add-float v15, v21, v10

    iget-object v0, v6, LX/48q;->a:LX/48r;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, LX/48r;->m:LX/1mz;

    move-object/from16 v17, v0

    sget-object v19, LX/48t;->l:[I

    aget v19, v19, v36

    sget-object v20, LX/48t;->h:[I

    aget v20, v20, v36

    move-object/from16 v0, v17

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/1mz;->a(II)F

    move-result v17

    sub-float v15, v15, v17

    iget-object v6, v6, LX/48q;->b:LX/48o;

    iget-object v6, v6, LX/48o;->i:[F

    sget-object v17, LX/48t;->j:[I

    aget v17, v17, v36

    aget v6, v6, v17

    sub-float v6, v15, v6

    aput v6, v11, v13

    goto :goto_34

    .line 673787
    :cond_65
    sget-object v13, Lcom/facebook/csslayout/YogaAlign;->CENTER:Lcom/facebook/csslayout/YogaAlign;

    if-ne v11, v13, :cond_66

    .line 673788
    iget-object v11, v6, LX/48q;->b:LX/48o;

    iget-object v11, v11, LX/48o;->i:[F

    sget-object v13, LX/48t;->j:[I

    aget v13, v13, v36

    aget v11, v11, v13

    .line 673789
    iget-object v6, v6, LX/48q;->b:LX/48o;

    iget-object v6, v6, LX/48o;->a:[F

    sget-object v13, LX/48t;->i:[I

    aget v13, v13, v36

    sub-float v11, v10, v11

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v11, v15

    add-float v11, v11, v21

    aput v11, v6, v13

    goto :goto_34

    .line 673790
    :cond_66
    sget-object v13, Lcom/facebook/csslayout/YogaAlign;->STRETCH:Lcom/facebook/csslayout/YogaAlign;

    if-ne v11, v13, :cond_63

    .line 673791
    iget-object v11, v6, LX/48q;->b:LX/48o;

    iget-object v11, v11, LX/48o;->a:[F

    sget-object v13, LX/48t;->i:[I

    aget v13, v13, v36

    iget-object v6, v6, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->m:LX/1mz;

    sget-object v15, LX/48t;->k:[I

    aget v15, v15, v36

    sget-object v17, LX/48t;->g:[I

    aget v17, v17, v36

    move/from16 v0, v17

    invoke-virtual {v6, v15, v0}, LX/1mz;->a(II)F

    move-result v6

    add-float v6, v6, v21

    aput v6, v11, v13

    goto :goto_34

    .line 673792
    :cond_67
    add-float v21, v21, v10

    .line 673793
    add-int/lit8 v5, v9, 0x1

    move v6, v7

    move v9, v5

    goto/16 :goto_31

    .line 673794
    :cond_68
    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    const/4 v5, 0x0

    sget v6, LX/48t;->c:I

    sub-float v7, p2, v32

    move-object/from16 v0, p1

    invoke-static {v0, v6, v7}, LX/48t;->b(LX/48q;IF)F

    move-result v6

    aput v6, v4, v5

    .line 673795
    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    const/4 v5, 0x1

    sget v6, LX/48t;->a:I

    sub-float v7, p3, v33

    move-object/from16 v0, p1

    invoke-static {v0, v6, v7}, LX/48t;->b(LX/48q;IF)F

    move-result v6

    aput v6, v4, v5

    .line 673796
    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, v31

    if-ne v0, v4, :cond_70

    .line 673797
    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    sget-object v5, LX/48t;->j:[I

    aget v5, v5, v35

    move-object/from16 v0, p1

    move/from16 v1, v35

    move/from16 v2, v26

    invoke-static {v0, v1, v2}, LX/48t;->b(LX/48q;IF)F

    move-result v6

    aput v6, v4, v5

    .line 673798
    :cond_69
    :goto_35
    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, v30

    if-ne v0, v4, :cond_71

    .line 673799
    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    sget-object v5, LX/48t;->j:[I

    aget v5, v5, v36

    add-float v6, v27, v41

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-static {v0, v1, v6}, LX/48t;->b(LX/48q;IF)F

    move-result v6

    aput v6, v4, v5

    :cond_6a
    :goto_36
    move-object/from16 v5, v18

    .line 673800
    :goto_37
    if-eqz v5, :cond_7c

    .line 673801
    if-eqz p7, :cond_6f

    .line 673802
    const/high16 v4, 0x7fc00000    # NaNf

    .line 673803
    const/high16 v7, 0x7fc00000    # NaNf

    .line 673804
    iget-object v6, v5, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->q:[F

    sget-object v9, LX/48t;->j:[I

    sget v10, LX/48t;->c:I

    aget v9, v9, v10

    aget v6, v6, v9

    float-to-double v10, v6

    const-wide/16 v14, 0x0

    cmpl-double v6, v10, v14

    if-ltz v6, :cond_72

    .line 673805
    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->q:[F

    const/4 v6, 0x0

    aget v4, v4, v6

    iget-object v6, v5, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->m:LX/1mz;

    sget-object v9, LX/48t;->k:[I

    sget v10, LX/48t;->c:I

    aget v9, v9, v10

    sget-object v10, LX/48t;->g:[I

    sget v11, LX/48t;->c:I

    aget v10, v10, v11

    invoke-virtual {v6, v9, v10}, LX/1mz;->a(II)F

    move-result v6

    iget-object v9, v5, LX/48q;->a:LX/48r;

    iget-object v9, v9, LX/48r;->m:LX/1mz;

    sget-object v10, LX/48t;->l:[I

    sget v11, LX/48t;->c:I

    aget v10, v10, v11

    sget-object v11, LX/48t;->h:[I

    sget v13, LX/48t;->c:I

    aget v11, v11, v13

    invoke-virtual {v9, v10, v11}, LX/1mz;->a(II)F

    move-result v9

    add-float/2addr v6, v9

    add-float/2addr v4, v6

    .line 673806
    :cond_6b
    :goto_38
    iget-object v6, v5, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->q:[F

    sget-object v9, LX/48t;->j:[I

    sget v10, LX/48t;->a:I

    aget v9, v9, v10

    aget v6, v6, v9

    float-to-double v10, v6

    const-wide/16 v14, 0x0

    cmpl-double v6, v10, v14

    if-ltz v6, :cond_75

    .line 673807
    iget-object v6, v5, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->q:[F

    const/4 v7, 0x1

    aget v6, v6, v7

    iget-object v7, v5, LX/48q;->a:LX/48r;

    iget-object v7, v7, LX/48r;->m:LX/1mz;

    sget-object v9, LX/48t;->k:[I

    sget v10, LX/48t;->a:I

    aget v9, v9, v10

    sget-object v10, LX/48t;->g:[I

    sget v11, LX/48t;->a:I

    aget v10, v10, v11

    invoke-virtual {v7, v9, v10}, LX/1mz;->a(II)F

    move-result v7

    iget-object v9, v5, LX/48q;->a:LX/48r;

    iget-object v9, v9, LX/48r;->m:LX/1mz;

    sget-object v10, LX/48t;->l:[I

    sget v11, LX/48t;->a:I

    aget v10, v10, v11

    sget-object v11, LX/48t;->h:[I

    sget v13, LX/48t;->a:I

    aget v11, v11, v13

    invoke-virtual {v9, v10, v11}, LX/1mz;->a(II)F

    move-result v9

    add-float/2addr v7, v9

    add-float/2addr v7, v6

    .line 673808
    :cond_6c
    :goto_39
    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v6

    if-nez v6, :cond_6d

    invoke-static {v7}, Ljava/lang/Float;->isNaN(F)Z

    move-result v6

    if-eqz v6, :cond_85

    .line 673809
    :cond_6d
    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v6

    if-eqz v6, :cond_78

    sget-object v6, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    .line 673810
    :goto_3a
    invoke-static {v7}, Ljava/lang/Float;->isNaN(F)Z

    move-result v9

    if-eqz v9, :cond_79

    sget-object v10, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    .line 673811
    :goto_3b
    if-nez v12, :cond_84

    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v9

    if-eqz v9, :cond_84

    invoke-static/range {v16 .. v16}, Ljava/lang/Float;->isNaN(F)Z

    move-result v9

    if-nez v9, :cond_84

    .line 673812
    sget-object v6, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object v9, v6

    move/from16 v6, v16

    .line 673813
    :goto_3c
    const/4 v11, 0x0

    move-object/from16 v4, p0

    invoke-static/range {v4 .. v11}, LX/48t;->a(LX/1Dq;LX/48q;FFLcom/facebook/csslayout/YogaDirection;Lcom/facebook/csslayout/YogaMeasureMode;Lcom/facebook/csslayout/YogaMeasureMode;Z)Z

    .line 673814
    iget-object v4, v5, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    const/4 v6, 0x0

    aget v4, v4, v6

    iget-object v6, v5, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->m:LX/1mz;

    sget-object v7, LX/48t;->k:[I

    sget v9, LX/48t;->c:I

    aget v7, v7, v9

    sget-object v9, LX/48t;->g:[I

    sget v10, LX/48t;->c:I

    aget v9, v9, v10

    invoke-virtual {v6, v7, v9}, LX/1mz;->a(II)F

    move-result v6

    iget-object v7, v5, LX/48q;->a:LX/48r;

    iget-object v7, v7, LX/48r;->m:LX/1mz;

    sget-object v9, LX/48t;->l:[I

    sget v10, LX/48t;->c:I

    aget v9, v9, v10

    sget-object v10, LX/48t;->h:[I

    sget v11, LX/48t;->c:I

    aget v10, v10, v11

    invoke-virtual {v7, v9, v10}, LX/1mz;->a(II)F

    move-result v7

    add-float/2addr v6, v7

    add-float/2addr v6, v4

    .line 673815
    iget-object v4, v5, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    const/4 v7, 0x1

    aget v4, v4, v7

    iget-object v7, v5, LX/48q;->a:LX/48r;

    iget-object v7, v7, LX/48r;->m:LX/1mz;

    sget-object v9, LX/48t;->k:[I

    sget v10, LX/48t;->a:I

    aget v9, v9, v10

    sget-object v10, LX/48t;->g:[I

    sget v11, LX/48t;->a:I

    aget v10, v10, v11

    invoke-virtual {v7, v9, v10}, LX/1mz;->a(II)F

    move-result v7

    iget-object v9, v5, LX/48q;->a:LX/48r;

    iget-object v9, v9, LX/48r;->m:LX/1mz;

    sget-object v10, LX/48t;->l:[I

    sget v11, LX/48t;->a:I

    aget v10, v10, v11

    sget-object v11, LX/48t;->h:[I

    sget v13, LX/48t;->a:I

    aget v11, v11, v13

    invoke-virtual {v9, v10, v11}, LX/1mz;->a(II)F

    move-result v9

    add-float/2addr v7, v9

    add-float/2addr v7, v4

    .line 673816
    :goto_3d
    sget-object v9, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    sget-object v10, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    const/4 v11, 0x1

    move-object/from16 v4, p0

    invoke-static/range {v4 .. v11}, LX/48t;->a(LX/1Dq;LX/48q;FFLcom/facebook/csslayout/YogaDirection;Lcom/facebook/csslayout/YogaMeasureMode;Lcom/facebook/csslayout/YogaMeasureMode;Z)Z

    .line 673817
    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->p:LX/1mz;

    sget-object v6, LX/48t;->l:[I

    aget v6, v6, v35

    sget-object v7, LX/48t;->h:[I

    aget v7, v7, v35

    invoke-virtual {v4, v6, v7}, LX/1mz;->a(II)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-nez v4, :cond_6e

    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->p:LX/1mz;

    sget-object v6, LX/48t;->k:[I

    aget v6, v6, v35

    sget-object v7, LX/48t;->g:[I

    aget v7, v7, v35

    invoke-virtual {v4, v6, v7}, LX/1mz;->a(II)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-eqz v4, :cond_6e

    .line 673818
    iget-object v4, v5, LX/48q;->b:LX/48o;

    iget-object v6, v4, LX/48o;->a:[F

    sget-object v4, LX/48t;->g:[I

    aget v7, v4, v35

    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    sget-object v9, LX/48t;->j:[I

    aget v9, v9, v35

    aget v4, v4, v9

    iget-object v9, v5, LX/48q;->b:LX/48o;

    iget-object v9, v9, LX/48o;->i:[F

    sget-object v10, LX/48t;->j:[I

    aget v10, v10, v35

    aget v9, v9, v10

    sub-float v9, v4, v9

    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->p:LX/1mz;

    sget-object v10, LX/48t;->l:[I

    aget v10, v10, v35

    sget-object v11, LX/48t;->h:[I

    aget v11, v11, v35

    invoke-virtual {v4, v10, v11}, LX/1mz;->a(II)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-eqz v4, :cond_7a

    const/4 v4, 0x0

    :goto_3e
    sub-float v4, v9, v4

    aput v4, v6, v7

    .line 673819
    :cond_6e
    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->p:LX/1mz;

    sget-object v6, LX/48t;->l:[I

    aget v6, v6, v36

    sget-object v7, LX/48t;->h:[I

    aget v7, v7, v36

    invoke-virtual {v4, v6, v7}, LX/1mz;->a(II)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-nez v4, :cond_6f

    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->p:LX/1mz;

    sget-object v6, LX/48t;->k:[I

    aget v6, v6, v36

    sget-object v7, LX/48t;->g:[I

    aget v7, v7, v36

    invoke-virtual {v4, v6, v7}, LX/1mz;->a(II)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-eqz v4, :cond_6f

    .line 673820
    iget-object v4, v5, LX/48q;->b:LX/48o;

    iget-object v6, v4, LX/48o;->a:[F

    sget-object v4, LX/48t;->g:[I

    aget v7, v4, v36

    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    sget-object v9, LX/48t;->j:[I

    aget v9, v9, v36

    aget v4, v4, v9

    iget-object v9, v5, LX/48q;->b:LX/48o;

    iget-object v9, v9, LX/48o;->i:[F

    sget-object v10, LX/48t;->j:[I

    aget v10, v10, v36

    aget v9, v9, v10

    sub-float v9, v4, v9

    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->p:LX/1mz;

    sget-object v10, LX/48t;->l:[I

    aget v10, v10, v36

    sget-object v11, LX/48t;->h:[I

    aget v11, v11, v36

    invoke-virtual {v4, v10, v11}, LX/1mz;->a(II)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-eqz v4, :cond_7b

    const/4 v4, 0x0

    :goto_3f
    sub-float v4, v9, v4

    aput v4, v6, v7

    .line 673821
    :cond_6f
    iget-object v5, v5, LX/48q;->e:LX/48q;

    goto/16 :goto_37

    .line 673822
    :cond_70
    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, v31

    if-ne v0, v4, :cond_69

    .line 673823
    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    sget-object v5, LX/48t;->j:[I

    aget v5, v5, v35

    add-float v6, v29, v40

    move-object/from16 v0, p1

    move/from16 v1, v35

    move/from16 v2, v26

    invoke-static {v0, v1, v2}, LX/48t;->a(LX/48q;IF)F

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    move/from16 v0, v40

    invoke-static {v6, v0}, Ljava/lang/Math;->max(FF)F

    move-result v6

    aput v6, v4, v5

    goto/16 :goto_35

    .line 673824
    :cond_71
    sget-object v4, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    move-object/from16 v0, v30

    if-ne v0, v4, :cond_6a

    .line 673825
    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    sget-object v5, LX/48t;->j:[I

    aget v5, v5, v36

    add-float v6, v14, v41

    add-float v7, v27, v41

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-static {v0, v1, v7}, LX/48t;->a(LX/48q;IF)F

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    move/from16 v0, v41

    invoke-static {v6, v0}, Ljava/lang/Math;->max(FF)F

    move-result v6

    aput v6, v4, v5

    goto/16 :goto_36

    .line 673826
    :cond_72
    iget-object v6, v5, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->p:LX/1mz;

    sget-object v9, LX/48t;->k:[I

    sget v10, LX/48t;->c:I

    aget v9, v9, v10

    sget-object v10, LX/48t;->g:[I

    sget v11, LX/48t;->c:I

    aget v10, v10, v11

    invoke-virtual {v6, v9, v10}, LX/1mz;->a(II)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->isNaN(F)Z

    move-result v6

    if-nez v6, :cond_6b

    iget-object v6, v5, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->p:LX/1mz;

    sget-object v9, LX/48t;->l:[I

    sget v10, LX/48t;->c:I

    aget v9, v9, v10

    sget-object v10, LX/48t;->h:[I

    sget v11, LX/48t;->c:I

    aget v10, v10, v11

    invoke-virtual {v6, v9, v10}, LX/1mz;->a(II)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->isNaN(F)Z

    move-result v6

    if-nez v6, :cond_6b

    .line 673827
    move-object/from16 v0, p1

    iget-object v4, v0, LX/48q;->b:LX/48o;

    iget-object v4, v4, LX/48o;->i:[F

    const/4 v6, 0x0

    aget v4, v4, v6

    move-object/from16 v0, p1

    iget-object v6, v0, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->o:LX/1mz;

    sget-object v9, LX/48t;->k:[I

    sget v10, LX/48t;->c:I

    aget v9, v9, v10

    sget-object v10, LX/48t;->g:[I

    sget v11, LX/48t;->c:I

    aget v10, v10, v11

    invoke-virtual {v6, v9, v10}, LX/1mz;->a(II)F

    move-result v6

    move-object/from16 v0, p1

    iget-object v9, v0, LX/48q;->a:LX/48r;

    iget-object v9, v9, LX/48r;->o:LX/1mz;

    sget-object v10, LX/48t;->l:[I

    sget v11, LX/48t;->c:I

    aget v10, v10, v11

    sget-object v11, LX/48t;->h:[I

    sget v13, LX/48t;->c:I

    aget v11, v11, v13

    invoke-virtual {v9, v10, v11}, LX/1mz;->a(II)F

    move-result v9

    add-float/2addr v6, v9

    sub-float v9, v4, v6

    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->p:LX/1mz;

    sget-object v6, LX/48t;->k:[I

    sget v10, LX/48t;->c:I

    aget v6, v6, v10

    sget-object v10, LX/48t;->g:[I

    sget v11, LX/48t;->c:I

    aget v10, v10, v11

    invoke-virtual {v4, v6, v10}, LX/1mz;->a(II)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-eqz v4, :cond_73

    const/4 v4, 0x0

    :goto_40
    iget-object v6, v5, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->p:LX/1mz;

    sget-object v10, LX/48t;->l:[I

    sget v11, LX/48t;->c:I

    aget v10, v10, v11

    sget-object v11, LX/48t;->h:[I

    sget v13, LX/48t;->c:I

    aget v11, v11, v13

    invoke-virtual {v6, v10, v11}, LX/1mz;->a(II)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->isNaN(F)Z

    move-result v6

    if-eqz v6, :cond_74

    const/4 v6, 0x0

    :goto_41
    add-float/2addr v4, v6

    sub-float v4, v9, v4

    .line 673828
    sget v6, LX/48t;->c:I

    invoke-static {v5, v6, v4}, LX/48t;->b(LX/48q;IF)F

    move-result v4

    goto/16 :goto_38

    .line 673829
    :cond_73
    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->p:LX/1mz;

    sget-object v6, LX/48t;->k:[I

    sget v10, LX/48t;->c:I

    aget v6, v6, v10

    sget-object v10, LX/48t;->g:[I

    sget v11, LX/48t;->c:I

    aget v10, v10, v11

    invoke-virtual {v4, v6, v10}, LX/1mz;->a(II)F

    move-result v4

    goto :goto_40

    :cond_74
    iget-object v6, v5, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->p:LX/1mz;

    sget-object v10, LX/48t;->l:[I

    sget v11, LX/48t;->c:I

    aget v10, v10, v11

    sget-object v11, LX/48t;->h:[I

    sget v13, LX/48t;->c:I

    aget v11, v11, v13

    invoke-virtual {v6, v10, v11}, LX/1mz;->a(II)F

    move-result v6

    goto :goto_41

    .line 673830
    :cond_75
    iget-object v6, v5, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->p:LX/1mz;

    sget-object v9, LX/48t;->k:[I

    sget v10, LX/48t;->a:I

    aget v9, v9, v10

    sget-object v10, LX/48t;->g:[I

    sget v11, LX/48t;->a:I

    aget v10, v10, v11

    invoke-virtual {v6, v9, v10}, LX/1mz;->a(II)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->isNaN(F)Z

    move-result v6

    if-nez v6, :cond_6c

    iget-object v6, v5, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->p:LX/1mz;

    sget-object v9, LX/48t;->l:[I

    sget v10, LX/48t;->a:I

    aget v9, v9, v10

    sget-object v10, LX/48t;->h:[I

    sget v11, LX/48t;->a:I

    aget v10, v10, v11

    invoke-virtual {v6, v9, v10}, LX/1mz;->a(II)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->isNaN(F)Z

    move-result v6

    if-nez v6, :cond_6c

    .line 673831
    move-object/from16 v0, p1

    iget-object v6, v0, LX/48q;->b:LX/48o;

    iget-object v6, v6, LX/48o;->i:[F

    const/4 v7, 0x1

    aget v6, v6, v7

    move-object/from16 v0, p1

    iget-object v7, v0, LX/48q;->a:LX/48r;

    iget-object v7, v7, LX/48r;->o:LX/1mz;

    sget-object v9, LX/48t;->k:[I

    sget v10, LX/48t;->a:I

    aget v9, v9, v10

    sget-object v10, LX/48t;->g:[I

    sget v11, LX/48t;->a:I

    aget v10, v10, v11

    invoke-virtual {v7, v9, v10}, LX/1mz;->a(II)F

    move-result v7

    move-object/from16 v0, p1

    iget-object v9, v0, LX/48q;->a:LX/48r;

    iget-object v9, v9, LX/48r;->o:LX/1mz;

    sget-object v10, LX/48t;->l:[I

    sget v11, LX/48t;->a:I

    aget v10, v10, v11

    sget-object v11, LX/48t;->h:[I

    sget v13, LX/48t;->a:I

    aget v11, v11, v13

    invoke-virtual {v9, v10, v11}, LX/1mz;->a(II)F

    move-result v9

    add-float/2addr v7, v9

    sub-float v9, v6, v7

    iget-object v6, v5, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->p:LX/1mz;

    sget-object v7, LX/48t;->k:[I

    sget v10, LX/48t;->a:I

    aget v7, v7, v10

    sget-object v10, LX/48t;->g:[I

    sget v11, LX/48t;->a:I

    aget v10, v10, v11

    invoke-virtual {v6, v7, v10}, LX/1mz;->a(II)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->isNaN(F)Z

    move-result v6

    if-eqz v6, :cond_76

    const/4 v6, 0x0

    :goto_42
    iget-object v7, v5, LX/48q;->a:LX/48r;

    iget-object v7, v7, LX/48r;->p:LX/1mz;

    sget-object v10, LX/48t;->l:[I

    sget v11, LX/48t;->a:I

    aget v10, v10, v11

    sget-object v11, LX/48t;->h:[I

    sget v13, LX/48t;->a:I

    aget v11, v11, v13

    invoke-virtual {v7, v10, v11}, LX/1mz;->a(II)F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->isNaN(F)Z

    move-result v7

    if-eqz v7, :cond_77

    const/4 v7, 0x0

    :goto_43
    add-float/2addr v6, v7

    sub-float v6, v9, v6

    .line 673832
    sget v7, LX/48t;->a:I

    invoke-static {v5, v7, v6}, LX/48t;->b(LX/48q;IF)F

    move-result v7

    goto/16 :goto_39

    .line 673833
    :cond_76
    iget-object v6, v5, LX/48q;->a:LX/48r;

    iget-object v6, v6, LX/48r;->p:LX/1mz;

    sget-object v7, LX/48t;->k:[I

    sget v10, LX/48t;->a:I

    aget v7, v7, v10

    sget-object v10, LX/48t;->g:[I

    sget v11, LX/48t;->a:I

    aget v10, v10, v11

    invoke-virtual {v6, v7, v10}, LX/1mz;->a(II)F

    move-result v6

    goto :goto_42

    :cond_77
    iget-object v7, v5, LX/48q;->a:LX/48r;

    iget-object v7, v7, LX/48r;->p:LX/1mz;

    sget-object v10, LX/48t;->l:[I

    sget v11, LX/48t;->a:I

    aget v10, v10, v11

    sget-object v11, LX/48t;->h:[I

    sget v13, LX/48t;->a:I

    aget v11, v11, v13

    invoke-virtual {v7, v10, v11}, LX/1mz;->a(II)F

    move-result v7

    goto :goto_43

    .line 673834
    :cond_78
    sget-object v6, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    goto/16 :goto_3a

    .line 673835
    :cond_79
    sget-object v10, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    goto/16 :goto_3b

    .line 673836
    :cond_7a
    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->p:LX/1mz;

    sget-object v10, LX/48t;->l:[I

    aget v10, v10, v35

    sget-object v11, LX/48t;->h:[I

    aget v11, v11, v35

    invoke-virtual {v4, v10, v11}, LX/1mz;->a(II)F

    move-result v4

    goto/16 :goto_3e

    .line 673837
    :cond_7b
    iget-object v4, v5, LX/48q;->a:LX/48r;

    iget-object v4, v4, LX/48r;->p:LX/1mz;

    sget-object v10, LX/48t;->l:[I

    aget v10, v10, v36

    sget-object v11, LX/48t;->h:[I

    aget v11, v11, v36

    invoke-virtual {v4, v10, v11}, LX/1mz;->a(II)F

    move-result v4

    goto/16 :goto_3f

    .line 673838
    :cond_7c
    if-eqz p7, :cond_1

    .line 673839
    const/4 v4, 0x0

    .line 673840
    const/4 v5, 0x0

    .line 673841
    sget v6, LX/48t;->d:I

    move/from16 v0, v35

    if-eq v0, v6, :cond_7d

    sget v6, LX/48t;->b:I

    move/from16 v0, v35

    if-ne v0, v6, :cond_7e

    .line 673842
    :cond_7d
    const/4 v4, 0x1

    .line 673843
    :cond_7e
    sget v6, LX/48t;->d:I

    move/from16 v0, v36

    if-eq v0, v6, :cond_7f

    sget v6, LX/48t;->b:I

    move/from16 v0, v36

    if-ne v0, v6, :cond_83

    .line 673844
    :cond_7f
    const/4 v5, 0x1

    move v6, v5

    .line 673845
    :goto_44
    if-nez v4, :cond_80

    if-eqz v6, :cond_1

    .line 673846
    :cond_80
    const/4 v5, 0x0

    :goto_45
    move/from16 v0, v34

    if-ge v5, v0, :cond_1

    .line 673847
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/48q;->a(I)LX/48q;

    move-result-object v7

    .line 673848
    if-eqz v4, :cond_81

    .line 673849
    iget-object v8, v7, LX/48q;->b:LX/48o;

    iget-object v8, v8, LX/48o;->a:[F

    sget-object v9, LX/48t;->h:[I

    aget v9, v9, v35

    move-object/from16 v0, p1

    iget-object v10, v0, LX/48q;->b:LX/48o;

    iget-object v10, v10, LX/48o;->i:[F

    sget-object v11, LX/48t;->j:[I

    aget v11, v11, v35

    aget v10, v10, v11

    iget-object v11, v7, LX/48q;->b:LX/48o;

    iget-object v11, v11, LX/48o;->i:[F

    sget-object v12, LX/48t;->j:[I

    aget v12, v12, v35

    aget v11, v11, v12

    sub-float/2addr v10, v11

    iget-object v11, v7, LX/48q;->b:LX/48o;

    iget-object v11, v11, LX/48o;->a:[F

    sget-object v12, LX/48t;->i:[I

    aget v12, v12, v35

    aget v11, v11, v12

    sub-float/2addr v10, v11

    aput v10, v8, v9

    .line 673850
    :cond_81
    if-eqz v6, :cond_82

    .line 673851
    iget-object v8, v7, LX/48q;->b:LX/48o;

    iget-object v8, v8, LX/48o;->a:[F

    sget-object v9, LX/48t;->h:[I

    aget v9, v9, v36

    move-object/from16 v0, p1

    iget-object v10, v0, LX/48q;->b:LX/48o;

    iget-object v10, v10, LX/48o;->i:[F

    sget-object v11, LX/48t;->j:[I

    aget v11, v11, v36

    aget v10, v10, v11

    iget-object v11, v7, LX/48q;->b:LX/48o;

    iget-object v11, v11, LX/48o;->i:[F

    sget-object v12, LX/48t;->j:[I

    aget v12, v12, v36

    aget v11, v11, v12

    sub-float/2addr v10, v11

    iget-object v7, v7, LX/48q;->b:LX/48o;

    iget-object v7, v7, LX/48o;->a:[F

    sget-object v11, LX/48t;->i:[I

    aget v11, v11, v36

    aget v7, v7, v11

    sub-float v7, v10, v7

    aput v7, v8, v9

    .line 673852
    :cond_82
    add-int/lit8 v5, v5, 0x1

    goto :goto_45

    :cond_83
    move v6, v5

    goto :goto_44

    :cond_84
    move-object v9, v6

    move v6, v4

    goto/16 :goto_3c

    :cond_85
    move v6, v4

    goto/16 :goto_3d

    :cond_86
    move/from16 v4, v21

    goto/16 :goto_2e

    :cond_87
    move/from16 v19, v4

    goto/16 :goto_26

    :cond_88
    move/from16 v19, v14

    goto/16 :goto_26

    :cond_89
    move v5, v6

    move v6, v7

    goto/16 :goto_25

    :cond_8a
    move/from16 v4, v19

    move/from16 v5, v20

    goto/16 :goto_22

    :cond_8b
    move v6, v7

    goto/16 :goto_1c

    :cond_8c
    move/from16 v24, v4

    goto/16 :goto_18

    :cond_8d
    move-object v4, v5

    move-object v5, v6

    move v6, v7

    move v7, v9

    move v9, v10

    move v10, v15

    goto/16 :goto_16

    :cond_8e
    move-object v9, v4

    goto/16 :goto_13

    :cond_8f
    move-object/from16 v4, v18

    goto/16 :goto_10
.end method

.method private static c(LX/48q;)F
    .locals 1

    .prologue
    .line 673459
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget v0, v0, LX/48r;->k:F

    return v0
.end method

.method private static d(LX/48q;)I
    .locals 1

    .prologue
    .line 673458
    iget-object v0, p0, LX/48q;->a:LX/48r;

    iget-object v0, v0, LX/48r;->b:Lcom/facebook/csslayout/YogaFlexDirection;

    invoke-virtual {v0}, Lcom/facebook/csslayout/YogaFlexDirection;->ordinal()I

    move-result v0

    return v0
.end method

.method private static e(LX/48q;)Z
    .locals 1

    .prologue
    .line 673457
    invoke-virtual {p0}, LX/48q;->e()Z

    move-result v0

    return v0
.end method
