.class public Lgifdrawable/pl/droidsonroids/gif/GifDrawable;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements Landroid/graphics/drawable/Animatable;
.implements Landroid/widget/MediaController$MediaPlayerControl;


# static fields
.field private static final b:Landroid/os/Handler;


# instance fields
.field public final a:Landroid/graphics/Paint;

.field public volatile c:I

.field private volatile d:Z

.field private final e:[I

.field private f:F

.field private g:F

.field private h:Z

.field private final i:Landroid/graphics/Rect;

.field public j:[I

.field private final k:Ljava/lang/Runnable;

.field private final l:Ljava/lang/Runnable;

.field private final m:Ljava/lang/Runnable;

.field private final n:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 826327
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->b:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(LX/52j;)V
    .locals 4

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 826328
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 826329
    iput-boolean v3, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->d:Z

    .line 826330
    iput v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->f:F

    .line 826331
    iput v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->g:F

    .line 826332
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->i:Landroid/graphics/Rect;

    .line 826333
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->a:Landroid/graphics/Paint;

    .line 826334
    new-instance v0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable$1;

    invoke-direct {v0, p0}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable$1;-><init>(Lgifdrawable/pl/droidsonroids/gif/GifDrawable;)V

    iput-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->k:Ljava/lang/Runnable;

    .line 826335
    new-instance v0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable$2;

    invoke-direct {v0, p0}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable$2;-><init>(Lgifdrawable/pl/droidsonroids/gif/GifDrawable;)V

    iput-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->l:Ljava/lang/Runnable;

    .line 826336
    new-instance v0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable$3;

    invoke-direct {v0, p0}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable$3;-><init>(Lgifdrawable/pl/droidsonroids/gif/GifDrawable;)V

    iput-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->m:Ljava/lang/Runnable;

    .line 826337
    new-instance v0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable$4;

    invoke-direct {v0, p0}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable$4;-><init>(Lgifdrawable/pl/droidsonroids/gif/GifDrawable;)V

    iput-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->n:Ljava/lang/Runnable;

    .line 826338
    iget-object v0, p1, LX/52j;->b:[I

    move-object v0, v0

    .line 826339
    iput-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->e:[I

    .line 826340
    iget v0, p1, LX/52j;->a:I

    move v0, v0

    .line 826341
    iput v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->c:I

    .line 826342
    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->e:[I

    aget v0, v0, v3

    iget-object v1, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->e:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    mul-int/2addr v0, v1

    new-array v0, v0, [I

    iput-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->j:[I

    .line 826343
    invoke-direct {p0, v3}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->d(I)V

    .line 826344
    return-void
.end method

.method public static a(Ljava/io/InputStream;)LX/52j;
    .locals 3

    .prologue
    .line 826345
    if-nez p0, :cond_0

    .line 826346
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Source is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 826347
    :cond_0
    invoke-virtual {p0}, Ljava/io/InputStream;->markSupported()Z

    move-result v0

    if-nez v0, :cond_1

    .line 826348
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "InputStream does not support marking"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 826349
    :cond_1
    const/4 v0, 0x5

    new-array v0, v0, [I

    .line 826350
    invoke-static {v0, p0}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->openStream([ILjava/io/InputStream;)I

    move-result v1

    .line 826351
    new-instance v2, LX/52j;

    invoke-direct {v2, v1, v0}, LX/52j;-><init>(I[I)V

    return-object v2
.end method

.method private a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 826352
    iput-boolean v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->d:Z

    .line 826353
    iput v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->c:I

    .line 826354
    const/4 v0, 0x0

    iput-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->j:[I

    .line 826355
    return-void
.end method

.method public static a(LX/52j;)V
    .locals 1

    .prologue
    .line 826356
    iget v0, p0, LX/52j;->a:I

    move v0, v0

    .line 826357
    invoke-static {v0}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->free(I)V

    .line 826358
    return-void
.end method

.method private static a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 826359
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    sget-object v1, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->b:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 826360
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    .line 826361
    :goto_0
    return-void

    .line 826362
    :cond_0
    sget-object v0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->b:Landroid/os/Handler;

    const v1, -0x72cee3cd

    invoke-static {v0, p0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public static synthetic a(I)Z
    .locals 1

    .prologue
    .line 826363
    invoke-static {p0}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->reset(I)Z

    move-result v0

    return v0
.end method

.method private b()I
    .locals 2

    .prologue
    .line 826364
    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->e:[I

    const/4 v1, 0x2

    aget v0, v0, v1

    return v0
.end method

.method private d(I)V
    .locals 2

    .prologue
    .line 826365
    if-gez p1, :cond_0

    .line 826366
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "frameIndex is not positive"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 826367
    :cond_0
    new-instance v0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable$6;

    invoke-direct {v0, p0, p1}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable$6;-><init>(Lgifdrawable/pl/droidsonroids/gif/GifDrawable;I)V

    invoke-static {v0}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->a(Ljava/lang/Runnable;)V

    .line 826368
    return-void
.end method

.method private static native free(I)V
.end method

.method private static native getAllocationByteCount(I)J
.end method

.method private static native getComment(I)Ljava/lang/String;
.end method

.method private static native getCurrentPosition(I)I
.end method

.method private static native getDuration(I)I
.end method

.method private static native getLoopCount(I)I
.end method

.method private static native openByteArray([I[B)I
.end method

.method private static native openDirectByteBuffer([ILjava/nio/ByteBuffer;)I
.end method

.method private static native openFd([ILjava/io/FileDescriptor;J)I
.end method

.method private static native openFile([ILjava/lang/String;)I
.end method

.method private static native openStream([ILjava/io/InputStream;)I
.end method

.method private static native renderFrame([II[I)V
.end method

.method private static native reset(I)Z
.end method

.method public static native restoreRemainder(I)V
.end method

.method public static native saveRemainder(I)V
.end method

.method public static native seekToFrame(II[I)V
.end method

.method public static native seekToTime(II[I)V
.end method

.method private static native setSpeedFactor(IF)V
.end method


# virtual methods
.method public final canPause()Z
    .locals 1

    .prologue
    .line 826266
    const/4 v0, 0x1

    return v0
.end method

.method public final canSeekBackward()Z
    .locals 1

    .prologue
    .line 826296
    const/4 v0, 0x0

    return v0
.end method

.method public final canSeekForward()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 826295
    invoke-direct {p0}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->b()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/4 v10, 0x4

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 826278
    iget-boolean v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->h:Z

    if-eqz v0, :cond_0

    .line 826279
    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->i:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 826280
    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->i:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->e:[I

    aget v1, v1, v2

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->f:F

    .line 826281
    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->i:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->e:[I

    aget v1, v1, v8

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->g:F

    .line 826282
    iput-boolean v2, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->h:Z

    .line 826283
    :cond_0
    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->a:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getShader()Landroid/graphics/Shader;

    move-result-object v0

    if-nez v0, :cond_4

    .line 826284
    iget-boolean v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->d:Z

    if-eqz v0, :cond_3

    .line 826285
    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->j:[I

    iget v1, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->c:I

    iget-object v3, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->e:[I

    invoke-static {v0, v1, v3}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->renderFrame([II[I)V

    .line 826286
    :goto_0
    iget v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->f:F

    iget v1, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->g:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 826287
    iget-object v1, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->j:[I

    .line 826288
    if-eqz v1, :cond_1

    .line 826289
    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->e:[I

    aget v3, v0, v2

    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->e:[I

    aget v6, v0, v2

    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->e:[I

    aget v7, v0, v8

    iget-object v9, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->a:Landroid/graphics/Paint;

    move-object v0, p1

    move v5, v4

    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIFFIIZLandroid/graphics/Paint;)V

    .line 826290
    :cond_1
    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->e:[I

    aget v0, v0, v10

    if-ltz v0, :cond_2

    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->e:[I

    const/4 v1, 0x2

    aget v0, v0, v1

    if-le v0, v8, :cond_2

    .line 826291
    sget-object v0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->b:Landroid/os/Handler;

    iget-object v1, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->n:Ljava/lang/Runnable;

    iget-object v2, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->e:[I

    aget v2, v2, v10

    int-to-long v2, v2

    const v4, 0x537f5b7f

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 826292
    :cond_2
    :goto_1
    return-void

    .line 826293
    :cond_3
    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->e:[I

    const/4 v1, -0x1

    aput v1, v0, v10

    goto :goto_0

    .line 826294
    :cond_4
    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->i:Landroid/graphics/Rect;

    iget-object v1, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_1
.end method

.method public final finalize()V
    .locals 1

    .prologue
    .line 826274
    :try_start_0
    invoke-direct {p0}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 826275
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 826276
    return-void

    .line 826277
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public final getAlpha()I
    .locals 1

    .prologue
    .line 826273
    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->a:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    return v0
.end method

.method public final getAudioSessionId()I
    .locals 1

    .prologue
    .line 826272
    const/4 v0, 0x0

    return v0
.end method

.method public final getBufferPercentage()I
    .locals 1

    .prologue
    .line 826271
    const/16 v0, 0x64

    return v0
.end method

.method public final getCurrentPosition()I
    .locals 1

    .prologue
    .line 826270
    iget v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->c:I

    invoke-static {v0}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->getCurrentPosition(I)I

    move-result v0

    return v0
.end method

.method public final getDuration()I
    .locals 1

    .prologue
    .line 826269
    iget v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->c:I

    invoke-static {v0}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->getDuration(I)I

    move-result v0

    return v0
.end method

.method public final getIntrinsicHeight()I
    .locals 2

    .prologue
    .line 826268
    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->e:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 2

    .prologue
    .line 826267
    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->e:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public final getMinimumHeight()I
    .locals 2

    .prologue
    .line 826265
    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->e:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    return v0
.end method

.method public final getMinimumWidth()I
    .locals 2

    .prologue
    .line 826298
    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->e:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 826299
    const/4 v0, -0x2

    return v0
.end method

.method public final isPlaying()Z
    .locals 1

    .prologue
    .line 826300
    iget-boolean v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->d:Z

    return v0
.end method

.method public final isRunning()Z
    .locals 1

    .prologue
    .line 826301
    iget-boolean v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->d:Z

    return v0
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 826302
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 826303
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->h:Z

    .line 826304
    return-void
.end method

.method public final pause()V
    .locals 0

    .prologue
    .line 826305
    invoke-virtual {p0}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->stop()V

    .line 826306
    return-void
.end method

.method public final seekTo(I)V
    .locals 2

    .prologue
    .line 826307
    if-gez p1, :cond_0

    .line 826308
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Position is not positive"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 826309
    :cond_0
    new-instance v0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable$5;

    invoke-direct {v0, p0, p1}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable$5;-><init>(Lgifdrawable/pl/droidsonroids/gif/GifDrawable;I)V

    invoke-static {v0}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->a(Ljava/lang/Runnable;)V

    .line 826310
    return-void
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 826311
    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 826312
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 826313
    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 826314
    return-void
.end method

.method public final setDither(Z)V
    .locals 1

    .prologue
    .line 826315
    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 826316
    invoke-virtual {p0}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->invalidateSelf()V

    .line 826317
    return-void
.end method

.method public final setFilterBitmap(Z)V
    .locals 1

    .prologue
    .line 826318
    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 826319
    invoke-virtual {p0}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->invalidateSelf()V

    .line 826320
    return-void
.end method

.method public final start()V
    .locals 1

    .prologue
    .line 826321
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->d:Z

    .line 826322
    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->l:Ljava/lang/Runnable;

    invoke-static {v0}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->a(Ljava/lang/Runnable;)V

    .line 826323
    return-void
.end method

.method public final stop()V
    .locals 1

    .prologue
    .line 826324
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->d:Z

    .line 826325
    iget-object v0, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->m:Ljava/lang/Runnable;

    invoke-static {v0}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->a(Ljava/lang/Runnable;)V

    .line 826326
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 826297
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "Size: %dx%d, %d frames, error: %d"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->e:[I

    aget v3, v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget-object v3, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->e:[I

    aget v3, v3, v5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    iget-object v3, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->e:[I

    aget v3, v3, v6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    iget-object v3, p0, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;->e:[I

    aget v3, v3, v7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
