.class public final Landroid/support/design/widget/HeaderBehavior$FlingRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/support/design/widget/HeaderBehavior;

.field private final b:Landroid/support/design/widget/CoordinatorLayout;

.field private final c:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/support/design/widget/HeaderBehavior;Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/design/widget/CoordinatorLayout;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 640004
    iput-object p1, p0, Landroid/support/design/widget/HeaderBehavior$FlingRunnable;->a:Landroid/support/design/widget/HeaderBehavior;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 640005
    iput-object p2, p0, Landroid/support/design/widget/HeaderBehavior$FlingRunnable;->b:Landroid/support/design/widget/CoordinatorLayout;

    .line 640006
    iput-object p3, p0, Landroid/support/design/widget/HeaderBehavior$FlingRunnable;->c:Landroid/view/View;

    .line 640007
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 640008
    iget-object v0, p0, Landroid/support/design/widget/HeaderBehavior$FlingRunnable;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/design/widget/HeaderBehavior$FlingRunnable;->a:Landroid/support/design/widget/HeaderBehavior;

    iget-object v0, v0, Landroid/support/design/widget/HeaderBehavior;->b:LX/1Og;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/design/widget/HeaderBehavior$FlingRunnable;->a:Landroid/support/design/widget/HeaderBehavior;

    iget-object v0, v0, Landroid/support/design/widget/HeaderBehavior;->b:LX/1Og;

    invoke-virtual {v0}, LX/1Og;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 640009
    iget-object v0, p0, Landroid/support/design/widget/HeaderBehavior$FlingRunnable;->a:Landroid/support/design/widget/HeaderBehavior;

    iget-object v1, p0, Landroid/support/design/widget/HeaderBehavior$FlingRunnable;->b:Landroid/support/design/widget/CoordinatorLayout;

    iget-object v2, p0, Landroid/support/design/widget/HeaderBehavior$FlingRunnable;->c:Landroid/view/View;

    iget-object v3, p0, Landroid/support/design/widget/HeaderBehavior$FlingRunnable;->a:Landroid/support/design/widget/HeaderBehavior;

    iget-object v3, v3, Landroid/support/design/widget/HeaderBehavior;->b:LX/1Og;

    invoke-virtual {v3}, LX/1Og;->c()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/design/widget/HeaderBehavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)I

    .line 640010
    iget-object v0, p0, Landroid/support/design/widget/HeaderBehavior$FlingRunnable;->c:Landroid/view/View;

    invoke-static {v0, p0}, LX/0vv;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 640011
    :cond_0
    return-void
.end method
