.class public Landroid/support/design/widget/SwipeDismissBehavior;
.super Landroid/support/design/widget/CoordinatorLayout$Behavior;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Landroid/support/design/widget/CoordinatorLayout$Behavior",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public a:LX/3ty;

.field public b:LX/3oK;

.field private c:Z

.field public d:F

.field public e:Z

.field public f:I

.field public g:F

.field public h:F

.field public i:F

.field public final j:LX/3nx;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f000000    # 0.5f

    const/4 v1, 0x0

    .line 640125
    invoke-direct {p0}, Landroid/support/design/widget/CoordinatorLayout$Behavior;-><init>()V

    .line 640126
    iput v1, p0, Landroid/support/design/widget/SwipeDismissBehavior;->d:F

    .line 640127
    const/4 v0, 0x2

    iput v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->f:I

    .line 640128
    iput v2, p0, Landroid/support/design/widget/SwipeDismissBehavior;->g:F

    .line 640129
    iput v1, p0, Landroid/support/design/widget/SwipeDismissBehavior;->h:F

    .line 640130
    iput v2, p0, Landroid/support/design/widget/SwipeDismissBehavior;->i:F

    .line 640131
    new-instance v0, LX/3oa;

    invoke-direct {v0, p0}, LX/3oa;-><init>(Landroid/support/design/widget/SwipeDismissBehavior;)V

    iput-object v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->j:LX/3nx;

    .line 640132
    return-void
.end method

.method public static c(FFF)F
    .locals 1

    .prologue
    .line 640133
    invoke-static {p0, p1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {v0, p2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Landroid/view/View;)Z
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 640134
    const/4 v0, 0x1

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/design/widget/CoordinatorLayout;",
            "TV;",
            "Landroid/view/MotionEvent;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 640135
    invoke-static {p3}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 640136
    :pswitch_0
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1, p2, v0, v2}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;II)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->c:Z

    .line 640137
    :cond_0
    iget-boolean v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->c:Z

    if-eqz v0, :cond_2

    .line 640138
    :goto_1
    return v1

    .line 640139
    :pswitch_1
    iget-boolean v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->c:Z

    if-eqz v0, :cond_0

    .line 640140
    iput-boolean v1, p0, Landroid/support/design/widget/SwipeDismissBehavior;->c:Z

    goto :goto_1

    :cond_1
    move v0, v1

    .line 640141
    goto :goto_0

    .line 640142
    :cond_2
    iget-object v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->a:LX/3ty;

    if-nez v0, :cond_3

    .line 640143
    iget-boolean v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->e:Z

    if-eqz v0, :cond_4

    iget v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->d:F

    iget-object v1, p0, Landroid/support/design/widget/SwipeDismissBehavior;->j:LX/3nx;

    invoke-static {p1, v0, v1}, LX/3ty;->a(Landroid/view/ViewGroup;FLX/3nx;)LX/3ty;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->a:LX/3ty;

    .line 640144
    :cond_3
    iget-object v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->a:LX/3ty;

    invoke-virtual {v0, p3}, LX/3ty;->a(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_1

    .line 640145
    :cond_4
    iget-object v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->j:LX/3nx;

    invoke-static {p1, v0}, LX/3ty;->a(Landroid/view/ViewGroup;LX/3nx;)LX/3ty;

    move-result-object v0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onTouchEvent(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/design/widget/CoordinatorLayout;",
            "TV;",
            "Landroid/view/MotionEvent;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 640146
    iget-object v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->a:LX/3ty;

    if-eqz v0, :cond_0

    .line 640147
    iget-object v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->a:LX/3ty;

    invoke-virtual {v0, p3}, LX/3ty;->b(Landroid/view/MotionEvent;)V

    .line 640148
    const/4 v0, 0x1

    .line 640149
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
