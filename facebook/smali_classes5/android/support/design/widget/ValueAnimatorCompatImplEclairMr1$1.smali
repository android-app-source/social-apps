.class public final Landroid/support/design/widget/ValueAnimatorCompatImplEclairMr1$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/3ok;


# direct methods
.method public constructor <init>(LX/3ok;)V
    .locals 0

    .prologue
    .line 640914
    iput-object p1, p0, Landroid/support/design/widget/ValueAnimatorCompatImplEclairMr1$1;->a:LX/3ok;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 640915
    iget-object v0, p0, Landroid/support/design/widget/ValueAnimatorCompatImplEclairMr1$1;->a:LX/3ok;

    .line 640916
    iget-boolean v1, v0, LX/3ok;->c:Z

    if-eqz v1, :cond_2

    .line 640917
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iget-wide v3, v0, LX/3ok;->b:J

    sub-long/2addr v1, v3

    .line 640918
    long-to-float v1, v1

    iget v2, v0, LX/3ok;->f:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 640919
    iget-object v2, v0, LX/3ok;->g:Landroid/view/animation/Interpolator;

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/3ok;->g:Landroid/view/animation/Interpolator;

    invoke-interface {v2, v1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v1

    :cond_0
    iput v1, v0, LX/3ok;->i:F

    .line 640920
    iget-object v1, v0, LX/3ok;->h:LX/3og;

    if-eqz v1, :cond_1

    .line 640921
    iget-object v1, v0, LX/3ok;->h:LX/3og;

    invoke-interface {v1}, LX/3og;->a()V

    .line 640922
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iget-wide v3, v0, LX/3ok;->b:J

    iget v5, v0, LX/3ok;->f:I

    int-to-long v5, v5

    add-long/2addr v3, v5

    cmp-long v1, v1, v3

    if-ltz v1, :cond_2

    .line 640923
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/3ok;->c:Z

    .line 640924
    :cond_2
    iget-boolean v1, v0, LX/3ok;->c:Z

    if-eqz v1, :cond_3

    .line 640925
    sget-object v1, LX/3ok;->a:Landroid/os/Handler;

    iget-object v2, v0, LX/3ok;->j:Ljava/lang/Runnable;

    const-wide/16 v3, 0xa

    const v5, 0x4d9dffb0

    invoke-static {v1, v2, v3, v4, v5}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 640926
    :cond_3
    return-void
.end method
