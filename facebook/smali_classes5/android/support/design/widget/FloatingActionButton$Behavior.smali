.class public final Landroid/support/design/widget/FloatingActionButton$Behavior;
.super Landroid/support/design/widget/CoordinatorLayout$Behavior;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/design/widget/CoordinatorLayout$Behavior",
        "<",
        "LX/3oC;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Z


# instance fields
.field public b:LX/3oj;

.field public c:F

.field private d:Landroid/graphics/Rect;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 639878
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Landroid/support/design/widget/FloatingActionButton$Behavior;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 639879
    invoke-direct {p0}, Landroid/support/design/widget/CoordinatorLayout$Behavior;-><init>()V

    return-void
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;LX/3oC;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 639880
    invoke-virtual {p3}, LX/3oC;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1uK;

    .line 639881
    iget v2, v0, LX/1uK;->f:I

    move v0, v2

    .line 639882
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getId()I

    move-result v2

    if-eq v0, v2, :cond_0

    move v0, v1

    .line 639883
    :goto_0
    return v0

    .line 639884
    :cond_0
    iget v0, p3, LX/3oB;->a:I

    move v0, v0

    .line 639885
    if-eqz v0, :cond_1

    move v0, v1

    .line 639886
    goto :goto_0

    .line 639887
    :cond_1
    iget-object v0, p0, Landroid/support/design/widget/FloatingActionButton$Behavior;->d:Landroid/graphics/Rect;

    if-nez v0, :cond_2

    .line 639888
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/FloatingActionButton$Behavior;->d:Landroid/graphics/Rect;

    .line 639889
    :cond_2
    iget-object v0, p0, Landroid/support/design/widget/FloatingActionButton$Behavior;->d:Landroid/graphics/Rect;

    .line 639890
    invoke-static {p1, p2, v0}, LX/1ui;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V

    .line 639891
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getMinimumHeightForVisibleOverlappingContent()I

    move-result v2

    if-gt v0, v2, :cond_3

    .line 639892
    iget-object v0, p3, LX/3oC;->f:LX/3oD;

    invoke-static {p3, v3}, LX/3oC;->a(LX/3oC;LX/3oA;)LX/3o7;

    invoke-virtual {v0}, LX/3oD;->f()V

    .line 639893
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 639894
    :cond_3
    iget-object v0, p3, LX/3oC;->f:LX/3oD;

    invoke-static {p3, v3}, LX/3oC;->a(LX/3oC;LX/3oA;)LX/3o7;

    invoke-virtual {v0}, LX/3oD;->g()V

    .line 639895
    goto :goto_1
.end method


# virtual methods
.method public final layoutDependsOn(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 639896
    sget-boolean v0, Landroid/support/design/widget/FloatingActionButton$Behavior;->a:Z

    if-eqz v0, :cond_0

    instance-of v0, p3, Landroid/support/design/widget/Snackbar$SnackbarLayout;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDependentViewChanged(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z
    .locals 5

    .prologue
    .line 639897
    check-cast p2, LX/3oC;

    .line 639898
    instance-of v0, p3, Landroid/support/design/widget/Snackbar$SnackbarLayout;

    if-eqz v0, :cond_1

    .line 639899
    invoke-virtual {p2}, LX/3oC;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    .line 639900
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 639901
    :cond_1
    instance-of v0, p3, Landroid/support/design/widget/AppBarLayout;

    if-eqz v0, :cond_0

    .line 639902
    check-cast p3, Landroid/support/design/widget/AppBarLayout;

    invoke-direct {p0, p1, p3, p2}, Landroid/support/design/widget/FloatingActionButton$Behavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;LX/3oC;)Z

    goto :goto_0

    .line 639903
    :cond_2
    const/4 v1, 0x0

    .line 639904
    invoke-virtual {p1, p2}, Landroid/support/design/widget/CoordinatorLayout;->c(Landroid/view/View;)Ljava/util/List;

    move-result-object v3

    .line 639905
    const/4 v0, 0x0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_3

    .line 639906
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 639907
    instance-of p3, v0, Landroid/support/design/widget/Snackbar$SnackbarLayout;

    if-eqz p3, :cond_7

    invoke-virtual {p1, p2, v0}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;Landroid/view/View;)Z

    move-result p3

    if-eqz p3, :cond_7

    .line 639908
    invoke-static {v0}, LX/0vv;->s(Landroid/view/View;)F

    move-result p3

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, p3, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 639909
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 639910
    :cond_3
    move v0, v1

    .line 639911
    iget v1, p0, Landroid/support/design/widget/FloatingActionButton$Behavior;->c:F

    cmpl-float v1, v1, v0

    if-eqz v1, :cond_0

    .line 639912
    invoke-static {p2}, LX/0vv;->s(Landroid/view/View;)F

    move-result v1

    .line 639913
    iget-object v2, p0, Landroid/support/design/widget/FloatingActionButton$Behavior;->b:LX/3oj;

    if-eqz v2, :cond_4

    iget-object v2, p0, Landroid/support/design/widget/FloatingActionButton$Behavior;->b:LX/3oj;

    invoke-virtual {v2}, LX/3oj;->b()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 639914
    iget-object v2, p0, Landroid/support/design/widget/FloatingActionButton$Behavior;->b:LX/3oj;

    invoke-virtual {v2}, LX/3oj;->e()V

    .line 639915
    :cond_4
    sub-float v2, v1, v0

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-virtual {p2}, LX/3oC;->getHeight()I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3f2ac083    # 0.667f

    mul-float/2addr v3, v4

    cmpl-float v2, v2, v3

    if-lez v2, :cond_6

    .line 639916
    iget-object v2, p0, Landroid/support/design/widget/FloatingActionButton$Behavior;->b:LX/3oj;

    if-nez v2, :cond_5

    .line 639917
    invoke-static {}, LX/1uL;->a()LX/3oj;

    move-result-object v2

    iput-object v2, p0, Landroid/support/design/widget/FloatingActionButton$Behavior;->b:LX/3oj;

    .line 639918
    iget-object v2, p0, Landroid/support/design/widget/FloatingActionButton$Behavior;->b:LX/3oj;

    sget-object v3, LX/3ns;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v2, v3}, LX/3oj;->a(Landroid/view/animation/Interpolator;)V

    .line 639919
    iget-object v2, p0, Landroid/support/design/widget/FloatingActionButton$Behavior;->b:LX/3oj;

    new-instance v3, LX/3o9;

    invoke-direct {v3, p0, p2}, LX/3o9;-><init>(Landroid/support/design/widget/FloatingActionButton$Behavior;LX/3oC;)V

    invoke-virtual {v2, v3}, LX/3oj;->a(LX/3nt;)V

    .line 639920
    :cond_5
    iget-object v2, p0, Landroid/support/design/widget/FloatingActionButton$Behavior;->b:LX/3oj;

    invoke-virtual {v2, v1, v0}, LX/3oj;->a(FF)V

    .line 639921
    iget-object v1, p0, Landroid/support/design/widget/FloatingActionButton$Behavior;->b:LX/3oj;

    invoke-virtual {v1}, LX/3oj;->a()V

    .line 639922
    :goto_3
    iput v0, p0, Landroid/support/design/widget/FloatingActionButton$Behavior;->c:F

    goto/16 :goto_0

    .line 639923
    :cond_6
    invoke-static {p2, v0}, LX/0vv;->b(Landroid/view/View;F)V

    goto :goto_3

    :cond_7
    move v0, v1

    goto :goto_2
.end method

.method public final onLayoutChild(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z
    .locals 5

    .prologue
    .line 639924
    check-cast p2, LX/3oC;

    .line 639925
    invoke-virtual {p1, p2}, Landroid/support/design/widget/CoordinatorLayout;->c(Landroid/view/View;)Ljava/util/List;

    move-result-object v2

    .line 639926
    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 639927
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 639928
    instance-of v4, v0, Landroid/support/design/widget/AppBarLayout;

    if-eqz v4, :cond_0

    check-cast v0, Landroid/support/design/widget/AppBarLayout;

    invoke-direct {p0, p1, v0, p2}, Landroid/support/design/widget/FloatingActionButton$Behavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;LX/3oC;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 639929
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 639930
    :cond_1
    invoke-virtual {p1, p2, p3}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;I)V

    .line 639931
    const/4 v2, 0x0

    .line 639932
    iget-object v3, p2, LX/3oC;->e:Landroid/graphics/Rect;

    .line 639933
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    if-lez v0, :cond_3

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    if-lez v0, :cond_3

    .line 639934
    invoke-virtual {p2}, LX/3oC;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1uK;

    .line 639935
    invoke-virtual {p2}, LX/3oC;->getRight()I

    move-result v1

    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->getWidth()I

    move-result v4

    iget p0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v4, p0

    if-lt v1, v4, :cond_4

    .line 639936
    iget v1, v3, Landroid/graphics/Rect;->right:I

    .line 639937
    :goto_1
    invoke-virtual {p2}, LX/3oC;->getBottom()I

    move-result v4

    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->getBottom()I

    move-result p0

    iget p3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int/2addr p0, p3

    if-lt v4, p0, :cond_5

    .line 639938
    iget v2, v3, Landroid/graphics/Rect;->bottom:I

    .line 639939
    :cond_2
    :goto_2
    invoke-virtual {p2, v2}, LX/3oC;->offsetTopAndBottom(I)V

    .line 639940
    invoke-virtual {p2, v1}, LX/3oC;->offsetLeftAndRight(I)V

    .line 639941
    :cond_3
    const/4 v0, 0x1

    return v0

    .line 639942
    :cond_4
    invoke-virtual {p2}, LX/3oC;->getLeft()I

    move-result v1

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    if-gt v1, v4, :cond_6

    .line 639943
    iget v1, v3, Landroid/graphics/Rect;->left:I

    neg-int v1, v1

    goto :goto_1

    .line 639944
    :cond_5
    invoke-virtual {p2}, LX/3oC;->getTop()I

    move-result v4

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-gt v4, v0, :cond_2

    .line 639945
    iget v0, v3, Landroid/graphics/Rect;->top:I

    neg-int v2, v0

    goto :goto_2

    :cond_6
    move v1, v2

    goto :goto_1
.end method
