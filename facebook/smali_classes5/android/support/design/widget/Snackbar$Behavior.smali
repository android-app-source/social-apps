.class public final Landroid/support/design/widget/Snackbar$Behavior;
.super Landroid/support/design/widget/SwipeDismissBehavior;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/design/widget/SwipeDismissBehavior",
        "<",
        "Landroid/support/design/widget/Snackbar$SnackbarLayout;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3oW;


# direct methods
.method public constructor <init>(LX/3oW;)V
    .locals 0

    .prologue
    .line 640150
    iput-object p1, p0, Landroid/support/design/widget/Snackbar$Behavior;->a:LX/3oW;

    invoke-direct {p0}, Landroid/support/design/widget/SwipeDismissBehavior;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 640151
    instance-of v0, p1, Landroid/support/design/widget/Snackbar$SnackbarLayout;

    return v0
.end method

.method public final onInterceptTouchEvent(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 640152
    check-cast p2, Landroid/support/design/widget/Snackbar$SnackbarLayout;

    .line 640153
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1, p2, v0, v1}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 640154
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 640155
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/design/widget/SwipeDismissBehavior;->onInterceptTouchEvent(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 640156
    :pswitch_1
    invoke-static {}, LX/3oZ;->a()LX/3oZ;

    move-result-object v0

    iget-object v1, p0, Landroid/support/design/widget/Snackbar$Behavior;->a:LX/3oW;

    iget-object v1, v1, LX/3oW;->g:LX/3oI;

    invoke-virtual {v0, v1}, LX/3oZ;->c(LX/3oI;)V

    goto :goto_0

    .line 640157
    :pswitch_2
    invoke-static {}, LX/3oZ;->a()LX/3oZ;

    move-result-object v0

    iget-object v1, p0, Landroid/support/design/widget/Snackbar$Behavior;->a:LX/3oW;

    iget-object v1, v1, LX/3oW;->g:LX/3oI;

    invoke-virtual {v0, v1}, LX/3oZ;->d(LX/3oI;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
