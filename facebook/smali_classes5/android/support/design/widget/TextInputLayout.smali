.class public Landroid/support/design/widget/TextInputLayout;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field public a:Landroid/widget/EditText;

.field private b:Ljava/lang/CharSequence;

.field private c:Landroid/graphics/Paint;

.field private d:Landroid/widget/LinearLayout;

.field private e:Z

.field public f:Landroid/widget/TextView;

.field private g:I

.field private h:Z

.field public i:Z

.field private j:Landroid/widget/TextView;

.field private k:I

.field private l:I

.field private m:I

.field private n:Z

.field private o:Landroid/content/res/ColorStateList;

.field private p:Landroid/content/res/ColorStateList;

.field public final q:LX/3o0;

.field public r:Z

.field private s:LX/3oj;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 640605
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/design/widget/TextInputLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 640606
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 640555
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/design/widget/TextInputLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 640556
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 640615
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 640616
    new-instance v0, LX/3o0;

    invoke-direct {v0, p0}, LX/3o0;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    .line 640617
    invoke-static {p1}, LX/1uI;->a(Landroid/content/Context;)V

    .line 640618
    invoke-virtual {p0, v5}, Landroid/support/design/widget/TextInputLayout;->setOrientation(I)V

    .line 640619
    invoke-virtual {p0, v4}, Landroid/support/design/widget/TextInputLayout;->setWillNotDraw(Z)V

    .line 640620
    invoke-virtual {p0, v5}, Landroid/support/design/widget/TextInputLayout;->setAddStatesFromChildren(Z)V

    .line 640621
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    sget-object v1, LX/3ns;->b:Landroid/view/animation/Interpolator;

    .line 640622
    iput-object v1, v0, LX/3o0;->K:Landroid/view/animation/Interpolator;

    .line 640623
    invoke-virtual {v0}, LX/3o0;->g()V

    .line 640624
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    .line 640625
    iput-object v1, v0, LX/3o0;->J:Landroid/view/animation/Interpolator;

    .line 640626
    invoke-virtual {v0}, LX/3o0;->g()V

    .line 640627
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    const v1, 0x800033

    invoke-virtual {v0, v1}, LX/3o0;->d(I)V

    .line 640628
    sget-object v0, LX/03r;->TextInputLayout:[I

    const v1, 0x7f0e0272

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 640629
    const/16 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/design/widget/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    .line 640630
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/design/widget/TextInputLayout;->r:Z

    .line 640631
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 640632
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Landroid/support/design/widget/TextInputLayout;->p:Landroid/content/res/ColorStateList;

    iput-object v1, p0, Landroid/support/design/widget/TextInputLayout;->o:Landroid/content/res/ColorStateList;

    .line 640633
    :cond_0
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 640634
    if-eq v1, v6, :cond_1

    .line 640635
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/design/widget/TextInputLayout;->setHintTextAppearance(I)V

    .line 640636
    :cond_1
    const/16 v1, 0x4

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/TextInputLayout;->g:I

    .line 640637
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 640638
    const/16 v2, 0x5

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    .line 640639
    const/16 v3, 0x6

    invoke-virtual {v0, v3, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    invoke-virtual {p0, v3}, Landroid/support/design/widget/TextInputLayout;->setCounterMaxLength(I)V

    .line 640640
    const/16 v3, 0x7

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, p0, Landroid/support/design/widget/TextInputLayout;->l:I

    .line 640641
    const/16 v3, 0x8

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, p0, Landroid/support/design/widget/TextInputLayout;->m:I

    .line 640642
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 640643
    invoke-virtual {p0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 640644
    invoke-virtual {p0, v2}, Landroid/support/design/widget/TextInputLayout;->setCounterEnabled(Z)V

    .line 640645
    invoke-static {p0}, LX/0vv;->e(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_2

    .line 640646
    invoke-static {p0, v5}, LX/0vv;->d(Landroid/view/View;I)V

    .line 640647
    :cond_2
    new-instance v0, LX/3of;

    invoke-direct {v0, p0}, LX/3of;-><init>(Landroid/support/design/widget/TextInputLayout;)V

    invoke-static {p0, v0}, LX/0vv;->a(Landroid/view/View;LX/0vn;)V

    .line 640648
    return-void
.end method

.method private a(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;
    .locals 3

    .prologue
    .line 640649
    instance-of v0, p1, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v0, :cond_1

    check-cast p1, Landroid/widget/LinearLayout$LayoutParams;

    .line 640650
    :goto_0
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->c:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 640651
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/TextInputLayout;->c:Landroid/graphics/Paint;

    .line 640652
    :cond_0
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->c:Landroid/graphics/Paint;

    iget-object v1, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    invoke-virtual {v1}, LX/3o0;->c()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 640653
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->c:Landroid/graphics/Paint;

    iget-object v1, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    .line 640654
    iget v2, v1, LX/3o0;->l:F

    move v1, v2

    .line 640655
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 640656
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->c:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->ascent()F

    move-result v0

    neg-float v0, v0

    float-to-int v0, v0

    iput v0, p1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 640657
    return-object p1

    .line 640658
    :cond_1
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    move-object p1, v0

    goto :goto_0
.end method

.method private a()V
    .locals 5

    .prologue
    .line 640659
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->d:Landroid/widget/LinearLayout;

    iget-object v1, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    invoke-static {v1}, LX/0vv;->n(Landroid/view/View;)I

    move-result v1

    const/4 v2, 0x0

    iget-object v3, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    invoke-static {v3}, LX/0vv;->o(Landroid/view/View;)I

    move-result v3

    iget-object v4, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getPaddingBottom()I

    move-result v4

    invoke-static {v0, v1, v2, v3, v4}, LX/0vv;->b(Landroid/view/View;IIII)V

    .line 640660
    return-void
.end method

.method private a(F)V
    .locals 3

    .prologue
    .line 640661
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    .line 640662
    iget v1, v0, LX/3o0;->e:F

    move v0, v1

    .line 640663
    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    .line 640664
    :goto_0
    return-void

    .line 640665
    :cond_0
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->s:LX/3oj;

    if-nez v0, :cond_1

    .line 640666
    invoke-static {}, LX/1uL;->a()LX/3oj;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/widget/TextInputLayout;->s:LX/3oj;

    .line 640667
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->s:LX/3oj;

    sget-object v1, LX/3ns;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, LX/3oj;->a(Landroid/view/animation/Interpolator;)V

    .line 640668
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->s:LX/3oj;

    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, LX/3oj;->a(I)V

    .line 640669
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->s:LX/3oj;

    new-instance v1, LX/3oe;

    invoke-direct {v1, p0}, LX/3oe;-><init>(Landroid/support/design/widget/TextInputLayout;)V

    invoke-virtual {v0, v1}, LX/3oj;->a(LX/3nt;)V

    .line 640670
    :cond_1
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->s:LX/3oj;

    iget-object v1, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    .line 640671
    iget v2, v1, LX/3o0;->e:F

    move v1, v2

    .line 640672
    invoke-virtual {v0, v1, p1}, LX/3oj;->a(FF)V

    .line 640673
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->s:LX/3oj;

    invoke-virtual {v0}, LX/3oj;->a()V

    goto :goto_0
.end method

.method private a(Landroid/widget/TextView;)V
    .locals 2

    .prologue
    .line 640674
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->d:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 640675
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 640676
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 640677
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->d:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 640678
    :cond_0
    return-void
.end method

.method private a(Landroid/widget/TextView;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 640679
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->d:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    .line 640680
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Landroid/support/design/widget/TextInputLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/design/widget/TextInputLayout;->d:Landroid/widget/LinearLayout;

    .line 640681
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 640682
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->d:Landroid/widget/LinearLayout;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-virtual {p0, v0, v1, v2}, Landroid/support/design/widget/TextInputLayout;->addView(Landroid/view/View;II)V

    .line 640683
    new-instance v0, LX/3tt;

    invoke-virtual {p0}, Landroid/support/design/widget/TextInputLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3tt;-><init>(Landroid/content/Context;)V

    .line 640684
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v1, v3, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 640685
    iget-object v2, p0, Landroid/support/design/widget/TextInputLayout;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 640686
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 640687
    invoke-direct {p0}, Landroid/support/design/widget/TextInputLayout;->a()V

    .line 640688
    :cond_0
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 640689
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 640690
    return-void
.end method

.method private static a([II)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 640882
    array-length v2, p0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget v3, p0, v1

    .line 640883
    if-ne v3, p1, :cond_1

    .line 640884
    const/4 v0, 0x1

    .line 640885
    :cond_0
    return v0

    .line 640886
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static a$redex0(Landroid/support/design/widget/TextInputLayout;I)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 640691
    iget-boolean v3, p0, Landroid/support/design/widget/TextInputLayout;->n:Z

    .line 640692
    iget v0, p0, Landroid/support/design/widget/TextInputLayout;->k:I

    const/4 v4, -0x1

    if-ne v0, v4, :cond_1

    .line 640693
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->j:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 640694
    iput-boolean v2, p0, Landroid/support/design/widget/TextInputLayout;->n:Z

    .line 640695
    :goto_0
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/support/design/widget/TextInputLayout;->n:Z

    if-eq v3, v0, :cond_0

    .line 640696
    invoke-static {p0, v2}, Landroid/support/design/widget/TextInputLayout;->a$redex0(Landroid/support/design/widget/TextInputLayout;Z)V

    .line 640697
    invoke-direct {p0}, Landroid/support/design/widget/TextInputLayout;->b()V

    .line 640698
    :cond_0
    return-void

    .line 640699
    :cond_1
    iget v0, p0, Landroid/support/design/widget/TextInputLayout;->k:I

    if-le p1, v0, :cond_3

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Landroid/support/design/widget/TextInputLayout;->n:Z

    .line 640700
    iget-boolean v0, p0, Landroid/support/design/widget/TextInputLayout;->n:Z

    if-eq v3, v0, :cond_2

    .line 640701
    iget-object v4, p0, Landroid/support/design/widget/TextInputLayout;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/design/widget/TextInputLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-boolean v0, p0, Landroid/support/design/widget/TextInputLayout;->n:Z

    if-eqz v0, :cond_4

    iget v0, p0, Landroid/support/design/widget/TextInputLayout;->m:I

    :goto_2
    invoke-virtual {v4, v5, v0}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 640702
    :cond_2
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/design/widget/TextInputLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f080151

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    iget v7, p0, Landroid/support/design/widget/TextInputLayout;->k:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 640703
    goto :goto_1

    .line 640704
    :cond_4
    iget v0, p0, Landroid/support/design/widget/TextInputLayout;->l:I

    goto :goto_2
.end method

.method public static a$redex0(Landroid/support/design/widget/TextInputLayout;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 640705
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 640706
    :goto_0
    invoke-virtual {p0}, Landroid/support/design/widget/TextInputLayout;->getDrawableState()[I

    move-result-object v3

    const v4, 0x101009c

    invoke-static {v3, v4}, Landroid/support/design/widget/TextInputLayout;->a([II)Z

    move-result v3

    .line 640707
    invoke-virtual {p0}, Landroid/support/design/widget/TextInputLayout;->getError()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 640708
    :goto_1
    iget-object v2, p0, Landroid/support/design/widget/TextInputLayout;->o:Landroid/content/res/ColorStateList;

    if-eqz v2, :cond_0

    .line 640709
    iget-object v2, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    iget-object v4, p0, Landroid/support/design/widget/TextInputLayout;->o:Landroid/content/res/ColorStateList;

    invoke-virtual {v4}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v4

    invoke-virtual {v2, v4}, LX/3o0;->b(I)V

    .line 640710
    :cond_0
    iget-boolean v2, p0, Landroid/support/design/widget/TextInputLayout;->n:Z

    if-eqz v2, :cond_5

    iget-object v2, p0, Landroid/support/design/widget/TextInputLayout;->j:Landroid/widget/TextView;

    if-eqz v2, :cond_5

    .line 640711
    iget-object v2, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    iget-object v4, p0, Landroid/support/design/widget/TextInputLayout;->j:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v4

    invoke-virtual {v2, v4}, LX/3o0;->a(I)V

    .line 640712
    :cond_1
    :goto_2
    if-nez v0, :cond_2

    if-nez v3, :cond_2

    if-eqz v1, :cond_8

    .line 640713
    :cond_2
    invoke-direct {p0, p1}, Landroid/support/design/widget/TextInputLayout;->b(Z)V

    .line 640714
    :goto_3
    return-void

    :cond_3
    move v0, v2

    .line 640715
    goto :goto_0

    :cond_4
    move v1, v2

    .line 640716
    goto :goto_1

    .line 640717
    :cond_5
    if-eqz v1, :cond_6

    iget-object v2, p0, Landroid/support/design/widget/TextInputLayout;->f:Landroid/widget/TextView;

    if-eqz v2, :cond_6

    .line 640718
    iget-object v2, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    iget-object v4, p0, Landroid/support/design/widget/TextInputLayout;->f:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v4

    invoke-virtual {v2, v4}, LX/3o0;->a(I)V

    goto :goto_2

    .line 640719
    :cond_6
    if-eqz v3, :cond_7

    iget-object v2, p0, Landroid/support/design/widget/TextInputLayout;->p:Landroid/content/res/ColorStateList;

    if-eqz v2, :cond_7

    .line 640720
    iget-object v2, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    iget-object v4, p0, Landroid/support/design/widget/TextInputLayout;->p:Landroid/content/res/ColorStateList;

    invoke-virtual {v4}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v4

    invoke-virtual {v2, v4}, LX/3o0;->a(I)V

    goto :goto_2

    .line 640721
    :cond_7
    iget-object v2, p0, Landroid/support/design/widget/TextInputLayout;->o:Landroid/content/res/ColorStateList;

    if-eqz v2, :cond_1

    .line 640722
    iget-object v2, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    iget-object v4, p0, Landroid/support/design/widget/TextInputLayout;->o:Landroid/content/res/ColorStateList;

    invoke-virtual {v4}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v4

    invoke-virtual {v2, v4}, LX/3o0;->a(I)V

    goto :goto_2

    .line 640723
    :cond_8
    invoke-direct {p0, p1}, Landroid/support/design/widget/TextInputLayout;->c(Z)V

    goto :goto_3
.end method

.method private b()V
    .locals 13

    .prologue
    .line 640724
    iget-boolean v0, p0, Landroid/support/design/widget/TextInputLayout;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 640725
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    iget-object v1, p0, Landroid/support/design/widget/TextInputLayout;->f:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v1

    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-static {v0, v1}, LX/0vv;->a(Landroid/view/View;Landroid/content/res/ColorStateList;)V

    .line 640726
    :goto_0
    return-void

    .line 640727
    :cond_0
    iget-boolean v0, p0, Landroid/support/design/widget/TextInputLayout;->n:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->j:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 640728
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    iget-object v1, p0, Landroid/support/design/widget/TextInputLayout;->j:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v1

    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-static {v0, v1}, LX/0vv;->a(Landroid/view/View;Landroid/content/res/ColorStateList;)V

    goto :goto_0

    .line 640729
    :cond_1
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    .line 640730
    sget-object v1, LX/3wi;->b:LX/3wi;

    if-nez v1, :cond_2

    .line 640731
    new-instance v1, LX/3wi;

    invoke-direct {v1}, LX/3wi;-><init>()V

    sput-object v1, LX/3wi;->b:LX/3wi;

    .line 640732
    :cond_2
    sget-object v1, LX/3wi;->b:LX/3wi;

    move-object v1, v1

    .line 640733
    invoke-virtual {p0}, Landroid/support/design/widget/TextInputLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f020013

    .line 640734
    const/4 v5, 0x0

    .line 640735
    iget-object v4, v1, LX/3wi;->j:Ljava/util/WeakHashMap;

    if-eqz v4, :cond_12

    .line 640736
    iget-object v4, v1, LX/3wi;->j:Ljava/util/WeakHashMap;

    invoke-virtual {v4, v2}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/util/SparseArray;

    .line 640737
    if-eqz v4, :cond_11

    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/res/ColorStateList;

    .line 640738
    :goto_1
    move-object v4, v4

    .line 640739
    if-nez v4, :cond_6

    .line 640740
    const v5, 0x7f020013

    if-ne v3, v5, :cond_7

    .line 640741
    const/4 v5, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 640742
    new-array v4, v5, [[I

    .line 640743
    new-array v5, v5, [I

    .line 640744
    sget-object v6, LX/3xW;->a:[I

    aput-object v6, v4, v7

    .line 640745
    const v6, 0x7f010054

    invoke-static {v2, v6}, LX/3xW;->c(Landroid/content/Context;I)I

    move-result v6

    aput v6, v5, v7

    .line 640746
    sget-object v6, LX/3xW;->g:[I

    aput-object v6, v4, v8

    .line 640747
    const v6, 0x7f010054

    invoke-static {v2, v6}, LX/3xW;->a(Landroid/content/Context;I)I

    move-result v6

    aput v6, v5, v8

    .line 640748
    sget-object v6, LX/3xW;->h:[I

    aput-object v6, v4, v9

    .line 640749
    const v6, 0x7f010055

    invoke-static {v2, v6}, LX/3xW;->a(Landroid/content/Context;I)I

    move-result v6

    aput v6, v5, v9

    .line 640750
    new-instance v6, Landroid/content/res/ColorStateList;

    invoke-direct {v6, v4, v5}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    move-object v4, v6

    .line 640751
    :cond_3
    :goto_2
    if-eqz v4, :cond_6

    .line 640752
    iget-object v5, v1, LX/3wi;->j:Ljava/util/WeakHashMap;

    if-nez v5, :cond_4

    .line 640753
    new-instance v5, Ljava/util/WeakHashMap;

    invoke-direct {v5}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v5, v1, LX/3wi;->j:Ljava/util/WeakHashMap;

    .line 640754
    :cond_4
    iget-object v5, v1, LX/3wi;->j:Ljava/util/WeakHashMap;

    invoke-virtual {v5, v2}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/util/SparseArray;

    .line 640755
    if-nez v5, :cond_5

    .line 640756
    new-instance v5, Landroid/util/SparseArray;

    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    .line 640757
    iget-object v6, v1, LX/3wi;->j:Ljava/util/WeakHashMap;

    invoke-virtual {v6, v2, v5}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 640758
    :cond_5
    invoke-virtual {v5, v3, v4}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 640759
    :cond_6
    move-object v1, v4

    .line 640760
    invoke-static {v0, v1}, LX/0vv;->a(Landroid/view/View;Landroid/content/res/ColorStateList;)V

    goto/16 :goto_0

    .line 640761
    :cond_7
    const v5, 0x7f020033

    if-ne v3, v5, :cond_8

    .line 640762
    const/4 v5, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    const v8, 0x3e99999a    # 0.3f

    .line 640763
    new-array v4, v5, [[I

    .line 640764
    new-array v5, v5, [I

    .line 640765
    sget-object v6, LX/3xW;->a:[I

    aput-object v6, v4, v9

    .line 640766
    const v6, 0x1010030

    const v7, 0x3dcccccd    # 0.1f

    invoke-static {v2, v6, v7}, LX/3xW;->a(Landroid/content/Context;IF)I

    move-result v6

    aput v6, v5, v9

    .line 640767
    sget-object v6, LX/3xW;->e:[I

    aput-object v6, v4, v10

    .line 640768
    const v6, 0x7f010055

    invoke-static {v2, v6, v8}, LX/3xW;->a(Landroid/content/Context;IF)I

    move-result v6

    aput v6, v5, v10

    .line 640769
    sget-object v6, LX/3xW;->h:[I

    aput-object v6, v4, v11

    .line 640770
    const v6, 0x1010030

    invoke-static {v2, v6, v8}, LX/3xW;->a(Landroid/content/Context;IF)I

    move-result v6

    aput v6, v5, v11

    .line 640771
    new-instance v6, Landroid/content/res/ColorStateList;

    invoke-direct {v6, v4, v5}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    move-object v4, v6

    .line 640772
    goto :goto_2

    .line 640773
    :cond_8
    const v5, 0x7f020032

    if-ne v3, v5, :cond_9

    .line 640774
    const/4 v5, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 640775
    new-array v4, v5, [[I

    .line 640776
    new-array v5, v5, [I

    .line 640777
    const v6, 0x7f010058

    invoke-static {v2, v6}, LX/3xW;->b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v6

    .line 640778
    if-eqz v6, :cond_13

    invoke-virtual {v6}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v7

    if-eqz v7, :cond_13

    .line 640779
    sget-object v7, LX/3xW;->a:[I

    aput-object v7, v4, v8

    .line 640780
    aget-object v7, v4, v8

    invoke-virtual {v6, v7, v8}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v7

    aput v7, v5, v8

    .line 640781
    sget-object v7, LX/3xW;->e:[I

    aput-object v7, v4, v9

    .line 640782
    const v7, 0x7f010055

    invoke-static {v2, v7}, LX/3xW;->a(Landroid/content/Context;I)I

    move-result v7

    aput v7, v5, v9

    .line 640783
    sget-object v7, LX/3xW;->h:[I

    aput-object v7, v4, v10

    .line 640784
    invoke-virtual {v6}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v6

    aput v6, v5, v10

    .line 640785
    :goto_3
    new-instance v6, Landroid/content/res/ColorStateList;

    invoke-direct {v6, v4, v5}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    move-object v4, v6

    .line 640786
    goto/16 :goto_2

    .line 640787
    :cond_9
    const v5, 0x7f020008

    if-eq v3, v5, :cond_a

    const v5, 0x7f020003

    if-ne v3, v5, :cond_b

    .line 640788
    :cond_a
    const v4, 0x7f010057

    invoke-static {v2, v4}, LX/3wi;->c(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v4

    move-object v4, v4

    .line 640789
    goto/16 :goto_2

    .line 640790
    :cond_b
    const v5, 0x7f020007

    if-ne v3, v5, :cond_c

    .line 640791
    const v4, 0x7f010053

    invoke-static {v2, v4}, LX/3wi;->c(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v4

    move-object v4, v4

    .line 640792
    goto/16 :goto_2

    .line 640793
    :cond_c
    const v5, 0x7f020030

    if-eq v3, v5, :cond_d

    const v5, 0x7f020031

    if-ne v3, v5, :cond_e

    .line 640794
    :cond_d
    const/4 v5, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 640795
    new-array v4, v5, [[I

    .line 640796
    new-array v5, v5, [I

    .line 640797
    sget-object v6, LX/3xW;->a:[I

    aput-object v6, v4, v7

    .line 640798
    const v6, 0x7f010054

    invoke-static {v2, v6}, LX/3xW;->c(Landroid/content/Context;I)I

    move-result v6

    aput v6, v5, v7

    .line 640799
    sget-object v6, LX/3xW;->g:[I

    aput-object v6, v4, v8

    .line 640800
    const v6, 0x7f010054

    invoke-static {v2, v6}, LX/3xW;->a(Landroid/content/Context;I)I

    move-result v6

    aput v6, v5, v8

    .line 640801
    sget-object v6, LX/3xW;->h:[I

    aput-object v6, v4, v9

    .line 640802
    const v6, 0x7f010055

    invoke-static {v2, v6}, LX/3xW;->a(Landroid/content/Context;I)I

    move-result v6

    aput v6, v5, v9

    .line 640803
    new-instance v6, Landroid/content/res/ColorStateList;

    invoke-direct {v6, v4, v5}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    move-object v4, v6

    .line 640804
    goto/16 :goto_2

    .line 640805
    :cond_e
    sget-object v5, LX/3wi;->e:[I

    invoke-static {v5, v3}, LX/3wi;->a([II)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 640806
    const v4, 0x7f010054

    invoke-static {v2, v4}, LX/3xW;->b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v4

    goto/16 :goto_2

    .line 640807
    :cond_f
    sget-object v5, LX/3wi;->h:[I

    invoke-static {v5, v3}, LX/3wi;->a([II)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 640808
    const/4 p0, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 640809
    const v4, 0x7f010054

    invoke-static {v2, v4}, LX/3xW;->a(Landroid/content/Context;I)I

    move-result v4

    .line 640810
    const v5, 0x7f010055

    invoke-static {v2, v5}, LX/3xW;->a(Landroid/content/Context;I)I

    move-result v5

    .line 640811
    const/4 v6, 0x7

    new-array v6, v6, [[I

    .line 640812
    const/4 v7, 0x7

    new-array v7, v7, [I

    .line 640813
    sget-object v8, LX/3xW;->a:[I

    aput-object v8, v6, v9

    .line 640814
    const v8, 0x7f010054

    invoke-static {v2, v8}, LX/3xW;->c(Landroid/content/Context;I)I

    move-result v8

    aput v8, v7, v9

    .line 640815
    sget-object v8, LX/3xW;->b:[I

    aput-object v8, v6, v10

    .line 640816
    aput v5, v7, v10

    .line 640817
    sget-object v8, LX/3xW;->c:[I

    aput-object v8, v6, v11

    .line 640818
    aput v5, v7, v11

    .line 640819
    sget-object v8, LX/3xW;->d:[I

    aput-object v8, v6, v12

    .line 640820
    aput v5, v7, v12

    .line 640821
    sget-object v8, LX/3xW;->e:[I

    aput-object v8, v6, p0

    .line 640822
    aput v5, v7, p0

    .line 640823
    const/4 v8, 0x5

    sget-object v9, LX/3xW;->f:[I

    aput-object v9, v6, v8

    .line 640824
    const/4 v8, 0x5

    aput v5, v7, v8

    .line 640825
    const/4 v5, 0x6

    sget-object v8, LX/3xW;->h:[I

    aput-object v8, v6, v5

    .line 640826
    const/4 v5, 0x6

    aput v4, v7, v5

    .line 640827
    new-instance v4, Landroid/content/res/ColorStateList;

    invoke-direct {v4, v6, v7}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    move-object v4, v4

    .line 640828
    goto/16 :goto_2

    .line 640829
    :cond_10
    sget-object v5, LX/3wi;->i:[I

    invoke-static {v5, v3}, LX/3wi;->a([II)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 640830
    const/4 v5, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 640831
    new-array v4, v5, [[I

    .line 640832
    new-array v5, v5, [I

    .line 640833
    sget-object v6, LX/3xW;->a:[I

    aput-object v6, v4, v7

    .line 640834
    const v6, 0x7f010054

    invoke-static {v2, v6}, LX/3xW;->c(Landroid/content/Context;I)I

    move-result v6

    aput v6, v5, v7

    .line 640835
    sget-object v6, LX/3xW;->e:[I

    aput-object v6, v4, v8

    .line 640836
    const v6, 0x7f010055

    invoke-static {v2, v6}, LX/3xW;->a(Landroid/content/Context;I)I

    move-result v6

    aput v6, v5, v8

    .line 640837
    sget-object v6, LX/3xW;->h:[I

    aput-object v6, v4, v9

    .line 640838
    const v6, 0x7f010054

    invoke-static {v2, v6}, LX/3xW;->a(Landroid/content/Context;I)I

    move-result v6

    aput v6, v5, v9

    .line 640839
    new-instance v6, Landroid/content/res/ColorStateList;

    invoke-direct {v6, v4, v5}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    move-object v4, v6

    .line 640840
    goto/16 :goto_2

    :cond_11
    move-object v4, v5

    .line 640841
    goto/16 :goto_1

    :cond_12
    move-object v4, v5

    .line 640842
    goto/16 :goto_1

    .line 640843
    :cond_13
    sget-object v6, LX/3xW;->a:[I

    aput-object v6, v4, v8

    .line 640844
    const v6, 0x7f010058

    invoke-static {v2, v6}, LX/3xW;->c(Landroid/content/Context;I)I

    move-result v6

    aput v6, v5, v8

    .line 640845
    sget-object v6, LX/3xW;->e:[I

    aput-object v6, v4, v9

    .line 640846
    const v6, 0x7f010055

    invoke-static {v2, v6}, LX/3xW;->a(Landroid/content/Context;I)I

    move-result v6

    aput v6, v5, v9

    .line 640847
    sget-object v6, LX/3xW;->h:[I

    aput-object v6, v4, v10

    .line 640848
    const v6, 0x7f010058

    invoke-static {v2, v6}, LX/3xW;->a(Landroid/content/Context;I)I

    move-result v6

    aput v6, v5, v10

    goto/16 :goto_3
.end method

.method private b(Z)V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 640849
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->s:LX/3oj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->s:LX/3oj;

    invoke-virtual {v0}, LX/3oj;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 640850
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->s:LX/3oj;

    invoke-virtual {v0}, LX/3oj;->e()V

    .line 640851
    :cond_0
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Landroid/support/design/widget/TextInputLayout;->r:Z

    if-eqz v0, :cond_1

    .line 640852
    invoke-direct {p0, v1}, Landroid/support/design/widget/TextInputLayout;->a(F)V

    .line 640853
    :goto_0
    return-void

    .line 640854
    :cond_1
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    invoke-virtual {v0, v1}, LX/3o0;->b(F)V

    goto :goto_0
.end method

.method private c(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 640855
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->s:LX/3oj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->s:LX/3oj;

    invoke-virtual {v0}, LX/3oj;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 640856
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->s:LX/3oj;

    invoke-virtual {v0}, LX/3oj;->e()V

    .line 640857
    :cond_0
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Landroid/support/design/widget/TextInputLayout;->r:Z

    if-eqz v0, :cond_1

    .line 640858
    invoke-direct {p0, v1}, Landroid/support/design/widget/TextInputLayout;->a(F)V

    .line 640859
    :goto_0
    return-void

    .line 640860
    :cond_1
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    invoke-virtual {v0, v1}, LX/3o0;->b(F)V

    goto :goto_0
.end method

.method private setEditText(Landroid/widget/EditText;)V
    .locals 2

    .prologue
    .line 640861
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 640862
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "We already have an EditText, can only have one"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 640863
    :cond_0
    iput-object p1, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    .line 640864
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    iget-object v1, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3o0;->c(Landroid/graphics/Typeface;)V

    .line 640865
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    iget-object v1, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getTextSize()F

    move-result v1

    .line 640866
    iget p1, v0, LX/3o0;->k:F

    cmpl-float p1, p1, v1

    if-eqz p1, :cond_1

    .line 640867
    iput v1, v0, LX/3o0;->k:F

    .line 640868
    invoke-virtual {v0}, LX/3o0;->g()V

    .line 640869
    :cond_1
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    iget-object v1, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getGravity()I

    move-result v1

    invoke-virtual {v0, v1}, LX/3o0;->c(I)V

    .line 640870
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    new-instance v1, LX/3ob;

    invoke-direct {v1, p0}, LX/3ob;-><init>(Landroid/support/design/widget/TextInputLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 640871
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->o:Landroid/content/res/ColorStateList;

    if-nez v0, :cond_2

    .line 640872
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getHintTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/widget/TextInputLayout;->o:Landroid/content/res/ColorStateList;

    .line 640873
    :cond_2
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->b:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 640874
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/design/widget/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    .line 640875
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 640876
    :cond_3
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->j:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 640877
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-static {p0, v0}, Landroid/support/design/widget/TextInputLayout;->a$redex0(Landroid/support/design/widget/TextInputLayout;I)V

    .line 640878
    :cond_4
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->d:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_5

    .line 640879
    invoke-direct {p0}, Landroid/support/design/widget/TextInputLayout;->a()V

    .line 640880
    :cond_5
    const/4 v0, 0x0

    invoke-static {p0, v0}, Landroid/support/design/widget/TextInputLayout;->a$redex0(Landroid/support/design/widget/TextInputLayout;Z)V

    .line 640881
    return-void
.end method


# virtual methods
.method public final addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 640607
    instance-of v0, p1, Landroid/widget/EditText;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 640608
    check-cast v0, Landroid/widget/EditText;

    invoke-direct {p0, v0}, Landroid/support/design/widget/TextInputLayout;->setEditText(Landroid/widget/EditText;)V

    .line 640609
    const/4 v0, 0x0

    invoke-direct {p0, p3}, Landroid/support/design/widget/TextInputLayout;->a(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v1

    invoke-super {p0, p1, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 640610
    :goto_0
    return-void

    .line 640611
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 640612
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->draw(Landroid/graphics/Canvas;)V

    .line 640613
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    invoke-virtual {v0, p1}, LX/3o0;->a(Landroid/graphics/Canvas;)V

    .line 640614
    return-void
.end method

.method public getCounterMaxLength()I
    .locals 1

    .prologue
    .line 640523
    iget v0, p0, Landroid/support/design/widget/TextInputLayout;->k:I

    return v0
.end method

.method public getEditText()Landroid/widget/EditText;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 640524
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    return-object v0
.end method

.method public getError()Ljava/lang/CharSequence;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 640525
    iget-boolean v0, p0, Landroid/support/design/widget/TextInputLayout;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 640526
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 640527
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHint()Ljava/lang/CharSequence;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 640528
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTypeface()Landroid/graphics/Typeface;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 640529
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    invoke-virtual {v0}, LX/3o0;->c()Landroid/graphics/Typeface;

    move-result-object v0

    return-object v0
.end method

.method public final onLayout(ZIIII)V
    .locals 6

    .prologue
    .line 640530
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 640531
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 640532
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getLeft()I

    move-result v0

    iget-object v1, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getCompoundPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    .line 640533
    iget-object v1, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getRight()I

    move-result v1

    iget-object v2, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getCompoundPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 640534
    iget-object v2, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    iget-object v3, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getTop()I

    move-result v3

    iget-object v4, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getCompoundPaddingTop()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getBottom()I

    move-result v4

    iget-object v5, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getCompoundPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v2, v0, v3, v1, v4}, LX/3o0;->a(IIII)V

    .line 640535
    iget-object v2, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    invoke-virtual {p0}, Landroid/support/design/widget/TextInputLayout;->getPaddingTop()I

    move-result v3

    sub-int v4, p5, p3

    invoke-virtual {p0}, Landroid/support/design/widget/TextInputLayout;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v2, v0, v3, v1, v4}, LX/3o0;->b(IIII)V

    .line 640536
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    invoke-virtual {v0}, LX/3o0;->g()V

    .line 640537
    :cond_0
    return-void
.end method

.method public final refreshDrawableState()V
    .locals 1

    .prologue
    .line 640538
    invoke-super {p0}, Landroid/widget/LinearLayout;->refreshDrawableState()V

    .line 640539
    invoke-static {p0}, LX/0vv;->E(Landroid/view/View;)Z

    move-result v0

    invoke-static {p0, v0}, Landroid/support/design/widget/TextInputLayout;->a$redex0(Landroid/support/design/widget/TextInputLayout;Z)V

    .line 640540
    return-void
.end method

.method public setCounterEnabled(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 640541
    iget-boolean v0, p0, Landroid/support/design/widget/TextInputLayout;->i:Z

    if-eq v0, p1, :cond_0

    .line 640542
    if-eqz p1, :cond_2

    .line 640543
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/design/widget/TextInputLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/design/widget/TextInputLayout;->j:Landroid/widget/TextView;

    .line 640544
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 640545
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/design/widget/TextInputLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Landroid/support/design/widget/TextInputLayout;->l:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 640546
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->j:Landroid/widget/TextView;

    invoke-static {v0, v3}, LX/0vv;->f(Landroid/view/View;I)V

    .line 640547
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->j:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Landroid/support/design/widget/TextInputLayout;->a(Landroid/widget/TextView;I)V

    .line 640548
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    if-nez v0, :cond_1

    .line 640549
    const/4 v0, 0x0

    invoke-static {p0, v0}, Landroid/support/design/widget/TextInputLayout;->a$redex0(Landroid/support/design/widget/TextInputLayout;I)V

    .line 640550
    :goto_0
    iput-boolean p1, p0, Landroid/support/design/widget/TextInputLayout;->i:Z

    .line 640551
    :cond_0
    return-void

    .line 640552
    :cond_1
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-static {p0, v0}, Landroid/support/design/widget/TextInputLayout;->a$redex0(Landroid/support/design/widget/TextInputLayout;I)V

    goto :goto_0

    .line 640553
    :cond_2
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->j:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Landroid/support/design/widget/TextInputLayout;->a(Landroid/widget/TextView;)V

    .line 640554
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/design/widget/TextInputLayout;->j:Landroid/widget/TextView;

    goto :goto_0
.end method

.method public setCounterMaxLength(I)V
    .locals 1

    .prologue
    .line 640515
    iget v0, p0, Landroid/support/design/widget/TextInputLayout;->k:I

    if-eq v0, p1, :cond_0

    .line 640516
    if-lez p1, :cond_1

    .line 640517
    iput p1, p0, Landroid/support/design/widget/TextInputLayout;->k:I

    .line 640518
    :goto_0
    iget-boolean v0, p0, Landroid/support/design/widget/TextInputLayout;->i:Z

    if-eqz v0, :cond_0

    .line 640519
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-static {p0, v0}, Landroid/support/design/widget/TextInputLayout;->a$redex0(Landroid/support/design/widget/TextInputLayout;I)V

    .line 640520
    :cond_0
    return-void

    .line 640521
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/design/widget/TextInputLayout;->k:I

    goto :goto_0

    .line 640522
    :cond_2
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    goto :goto_1
.end method

.method public setError(Ljava/lang/CharSequence;)V
    .locals 6
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/16 v4, 0xc8

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 640557
    iget-boolean v0, p0, Landroid/support/design/widget/TextInputLayout;->e:Z

    if-nez v0, :cond_2

    .line 640558
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 640559
    :cond_0
    :goto_0
    return-void

    .line 640560
    :cond_1
    invoke-virtual {p0, v2}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 640561
    :cond_2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 640562
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->f:Landroid/widget/TextView;

    invoke-static {v0, v1}, LX/0vv;->c(Landroid/view/View;F)V

    .line 640563
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 640564
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->f:Landroid/widget/TextView;

    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, LX/3sU;->a(F)LX/3sU;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, LX/3sU;->a(J)LX/3sU;

    move-result-object v0

    sget-object v1, LX/3ns;->d:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, LX/3sU;->a(Landroid/view/animation/Interpolator;)LX/3sU;

    move-result-object v0

    new-instance v1, LX/3oc;

    invoke-direct {v1, p0}, LX/3oc;-><init>(Landroid/support/design/widget/TextInputLayout;)V

    invoke-virtual {v0, v1}, LX/3sU;->a(LX/3oQ;)LX/3sU;

    move-result-object v0

    invoke-virtual {v0}, LX/3sU;->b()V

    .line 640565
    iput-boolean v2, p0, Landroid/support/design/widget/TextInputLayout;->h:Z

    .line 640566
    invoke-direct {p0}, Landroid/support/design/widget/TextInputLayout;->b()V

    .line 640567
    invoke-static {p0, v2}, Landroid/support/design/widget/TextInputLayout;->a$redex0(Landroid/support/design/widget/TextInputLayout;Z)V

    goto :goto_0

    .line 640568
    :cond_3
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 640569
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->f:Landroid/widget/TextView;

    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/3sU;->a(F)LX/3sU;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, LX/3sU;->a(J)LX/3sU;

    move-result-object v0

    sget-object v1, LX/3ns;->c:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, LX/3sU;->a(Landroid/view/animation/Interpolator;)LX/3sU;

    move-result-object v0

    new-instance v1, LX/3od;

    invoke-direct {v1, p0}, LX/3od;-><init>(Landroid/support/design/widget/TextInputLayout;)V

    invoke-virtual {v0, v1}, LX/3sU;->a(LX/3oQ;)LX/3sU;

    move-result-object v0

    invoke-virtual {v0}, LX/3sU;->b()V

    .line 640570
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/design/widget/TextInputLayout;->h:Z

    .line 640571
    invoke-direct {p0}, Landroid/support/design/widget/TextInputLayout;->b()V

    goto :goto_0
.end method

.method public setErrorEnabled(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 640572
    iget-boolean v0, p0, Landroid/support/design/widget/TextInputLayout;->e:Z

    if-eq v0, p1, :cond_1

    .line 640573
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 640574
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->f:Landroid/widget/TextView;

    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v0

    invoke-virtual {v0}, LX/3sU;->a()V

    .line 640575
    :cond_0
    if-eqz p1, :cond_2

    .line 640576
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/design/widget/TextInputLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/design/widget/TextInputLayout;->f:Landroid/widget/TextView;

    .line 640577
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/design/widget/TextInputLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Landroid/support/design/widget/TextInputLayout;->g:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 640578
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->f:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 640579
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->f:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/0vv;->f(Landroid/view/View;I)V

    .line 640580
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->f:Landroid/widget/TextView;

    invoke-direct {p0, v0, v3}, Landroid/support/design/widget/TextInputLayout;->a(Landroid/widget/TextView;I)V

    .line 640581
    :goto_0
    iput-boolean p1, p0, Landroid/support/design/widget/TextInputLayout;->e:Z

    .line 640582
    :cond_1
    return-void

    .line 640583
    :cond_2
    iput-boolean v3, p0, Landroid/support/design/widget/TextInputLayout;->h:Z

    .line 640584
    invoke-direct {p0}, Landroid/support/design/widget/TextInputLayout;->b()V

    .line 640585
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->f:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Landroid/support/design/widget/TextInputLayout;->a(Landroid/widget/TextView;)V

    .line 640586
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/design/widget/TextInputLayout;->f:Landroid/widget/TextView;

    goto :goto_0
.end method

.method public setHint(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 640587
    iput-object p1, p0, Landroid/support/design/widget/TextInputLayout;->b:Ljava/lang/CharSequence;

    .line 640588
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    invoke-virtual {v0, p1}, LX/3o0;->a(Ljava/lang/CharSequence;)V

    .line 640589
    const/16 v0, 0x800

    invoke-virtual {p0, v0}, Landroid/support/design/widget/TextInputLayout;->sendAccessibilityEvent(I)V

    .line 640590
    return-void
.end method

.method public setHintAnimationEnabled(Z)V
    .locals 0

    .prologue
    .line 640591
    iput-boolean p1, p0, Landroid/support/design/widget/TextInputLayout;->r:Z

    .line 640592
    return-void
.end method

.method public setHintTextAppearance(I)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param

    .prologue
    .line 640593
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    invoke-virtual {v0, p1}, LX/3o0;->e(I)V

    .line 640594
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    .line 640595
    iget v1, v0, LX/3o0;->n:I

    move v0, v1

    .line 640596
    invoke-static {v0}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/widget/TextInputLayout;->p:Landroid/content/res/ColorStateList;

    .line 640597
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 640598
    const/4 v0, 0x0

    invoke-static {p0, v0}, Landroid/support/design/widget/TextInputLayout;->a$redex0(Landroid/support/design/widget/TextInputLayout;Z)V

    .line 640599
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/design/widget/TextInputLayout;->a(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    .line 640600
    iget-object v1, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 640601
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestLayout()V

    .line 640602
    :cond_0
    return-void
.end method

.method public setTypeface(Landroid/graphics/Typeface;)V
    .locals 1
    .param p1    # Landroid/graphics/Typeface;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 640603
    iget-object v0, p0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    invoke-virtual {v0, p1}, LX/3o0;->c(Landroid/graphics/Typeface;)V

    .line 640604
    return-void
.end method
