.class public Landroid/support/design/widget/BottomSheetBehavior;
.super Landroid/support/design/widget/CoordinatorLayout$Behavior;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Landroid/support/design/widget/CoordinatorLayout$Behavior",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field private d:I

.field public e:LX/3ty;

.field private f:Z

.field private g:I

.field public h:I

.field private i:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final j:LX/3nx;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 639366
    invoke-direct {p0}, Landroid/support/design/widget/CoordinatorLayout$Behavior;-><init>()V

    .line 639367
    const/4 v0, 0x4

    iput v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    .line 639368
    new-instance v0, LX/3ny;

    invoke-direct {v0, p0}, LX/3ny;-><init>(Landroid/support/design/widget/BottomSheetBehavior;)V

    iput-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->j:LX/3nx;

    .line 639369
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 639294
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/CoordinatorLayout$Behavior;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 639295
    const/4 v0, 0x4

    iput v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    .line 639296
    new-instance v0, LX/3ny;

    invoke-direct {v0, p0}, LX/3ny;-><init>(Landroid/support/design/widget/BottomSheetBehavior;)V

    iput-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->j:LX/3nx;

    .line 639297
    sget-object v0, LX/03r;->BottomSheetBehavior_Params:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 639298
    const/16 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 639299
    const/4 v2, 0x0

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->a:I

    .line 639300
    iget v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->h:I

    sub-int/2addr v2, v1

    iput v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->c:I

    .line 639301
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 639302
    return-void
.end method

.method public static b(Landroid/support/design/widget/BottomSheetBehavior;I)V
    .locals 1

    .prologue
    .line 639342
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    if-ne v0, p1, :cond_0

    .line 639343
    :goto_0
    return-void

    .line 639344
    :cond_0
    iput p1, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    goto :goto_0
.end method


# virtual methods
.method public final onInterceptTouchEvent(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/design/widget/CoordinatorLayout;",
            "TV;",
            "Landroid/view/MotionEvent;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 639336
    invoke-static {p3}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v0

    .line 639337
    packed-switch v0, :pswitch_data_0

    .line 639338
    :cond_0
    :goto_0
    :pswitch_0
    iget-boolean v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->e:LX/3ty;

    invoke-virtual {v0, p3}, LX/3ty;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v2, v1

    :cond_1
    :goto_1
    return v2

    .line 639339
    :pswitch_1
    iget-boolean v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:Z

    if-eqz v0, :cond_0

    .line 639340
    iput-boolean v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:Z

    goto :goto_1

    .line 639341
    :pswitch_2
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1, p2, v0, v3}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;II)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:Z

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onLayoutChild(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/design/widget/CoordinatorLayout;",
            "TV;I)Z"
        }
    .end annotation

    .prologue
    .line 639324
    invoke-virtual {p1, p2, p3}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;I)V

    .line 639325
    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->getHeight()I

    move-result v0

    iput v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->h:I

    .line 639326
    const/4 v0, 0x0

    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->h:I

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->b:I

    .line 639327
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->h:I

    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->a:I

    sub-int/2addr v0, v1

    iput v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->c:I

    .line 639328
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 639329
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->b:I

    invoke-static {p2, v0}, LX/0vv;->g(Landroid/view/View;I)V

    .line 639330
    :goto_0
    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->e:LX/3ty;

    if-nez v0, :cond_0

    .line 639331
    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->j:LX/3nx;

    invoke-static {p1, v0}, LX/3ty;->a(Landroid/view/ViewGroup;LX/3nx;)LX/3ty;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->e:LX/3ty;

    .line 639332
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->i:Ljava/lang/ref/WeakReference;

    .line 639333
    const/4 v0, 0x1

    return v0

    .line 639334
    :cond_1
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->c:I

    invoke-static {p2, v0}, LX/0vv;->g(Landroid/view/View;I)V

    .line 639335
    const/4 v0, 0x4

    iput v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    goto :goto_0
.end method

.method public final onNestedPreScroll(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;II[I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/design/widget/CoordinatorLayout;",
            "TV;",
            "Landroid/view/View;",
            "II[I)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 639345
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v0

    .line 639346
    sub-int v1, v0, p5

    .line 639347
    if-lez p5, :cond_2

    .line 639348
    iget v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->b:I

    if-ge v1, v2, :cond_1

    .line 639349
    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->b:I

    sub-int/2addr v0, v1

    aput v0, p6, v3

    .line 639350
    aget v0, p6, v3

    neg-int v0, v0

    invoke-virtual {p2, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 639351
    const/4 v0, 0x3

    invoke-static {p0, v0}, Landroid/support/design/widget/BottomSheetBehavior;->b(Landroid/support/design/widget/BottomSheetBehavior;I)V

    .line 639352
    :cond_0
    :goto_0
    iput p5, p0, Landroid/support/design/widget/BottomSheetBehavior;->g:I

    .line 639353
    return-void

    .line 639354
    :cond_1
    aput p5, p6, v3

    .line 639355
    neg-int v0, p5

    invoke-virtual {p2, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 639356
    invoke-static {p0, v3}, Landroid/support/design/widget/BottomSheetBehavior;->b(Landroid/support/design/widget/BottomSheetBehavior;I)V

    goto :goto_0

    .line 639357
    :cond_2
    if-gez p5, :cond_0

    .line 639358
    const/4 v2, -0x1

    invoke-static {p3, v2}, LX/0vv;->b(Landroid/view/View;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 639359
    iget v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->c:I

    if-le v1, v2, :cond_3

    .line 639360
    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->c:I

    sub-int/2addr v0, v1

    aput v0, p6, v3

    .line 639361
    aget v0, p6, v3

    neg-int v0, v0

    invoke-virtual {p2, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 639362
    const/4 v0, 0x4

    invoke-static {p0, v0}, Landroid/support/design/widget/BottomSheetBehavior;->b(Landroid/support/design/widget/BottomSheetBehavior;I)V

    goto :goto_0

    .line 639363
    :cond_3
    aput p5, p6, v3

    .line 639364
    neg-int v0, p5

    invoke-virtual {p2, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 639365
    invoke-static {p0, v3}, Landroid/support/design/widget/BottomSheetBehavior;->b(Landroid/support/design/widget/BottomSheetBehavior;I)V

    goto :goto_0
.end method

.method public final onRestoreInstanceState(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/os/Parcelable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/design/widget/CoordinatorLayout;",
            "TV;",
            "Landroid/os/Parcelable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 639318
    check-cast p3, Landroid/support/design/widget/BottomSheetBehavior$SavedState;

    .line 639319
    invoke-virtual {p3}, Landroid/support/design/widget/BottomSheetBehavior$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, p1, p2, v0}, Landroid/support/design/widget/CoordinatorLayout$Behavior;->onRestoreInstanceState(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/os/Parcelable;)V

    .line 639320
    iget v0, p3, Landroid/support/design/widget/BottomSheetBehavior$SavedState;->a:I

    iput v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    .line 639321
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 639322
    :cond_0
    const/4 v0, 0x4

    iput v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    .line 639323
    :cond_1
    return-void
.end method

.method public final onSaveInstanceState(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Landroid/os/Parcelable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/design/widget/CoordinatorLayout;",
            "TV;)",
            "Landroid/os/Parcelable;"
        }
    .end annotation

    .prologue
    .line 639317
    new-instance v0, Landroid/support/design/widget/BottomSheetBehavior$SavedState;

    invoke-super {p0, p1, p2}, Landroid/support/design/widget/CoordinatorLayout$Behavior;->onSaveInstanceState(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Landroid/os/Parcelable;

    move-result-object v1

    iget v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    invoke-direct {v0, v1, v2}, Landroid/support/design/widget/BottomSheetBehavior$SavedState;-><init>(Landroid/os/Parcelable;I)V

    return-object v0
.end method

.method public final onStartNestedScroll(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;Landroid/view/View;I)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/design/widget/CoordinatorLayout;",
            "TV;",
            "Landroid/view/View;",
            "Landroid/view/View;",
            "I)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 639315
    iput v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->g:I

    .line 639316
    and-int/lit8 v1, p5, 0x2

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final onStopNestedScroll(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/design/widget/CoordinatorLayout;",
            "TV;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 639305
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->g:I

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v0

    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->b:I

    if-ne v0, v1, :cond_1

    .line 639306
    :cond_0
    :goto_0
    return-void

    .line 639307
    :cond_1
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->g:I

    if-lez v0, :cond_2

    .line 639308
    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->b:I

    .line 639309
    const/4 v0, 0x3

    .line 639310
    :goto_1
    const/4 v2, 0x2

    invoke-static {p0, v2}, Landroid/support/design/widget/BottomSheetBehavior;->b(Landroid/support/design/widget/BottomSheetBehavior;I)V

    .line 639311
    iget-object v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->e:LX/3ty;

    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-virtual {v2, p2, v3, v1}, LX/3ty;->a(Landroid/view/View;II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 639312
    new-instance v1, Landroid/support/design/widget/BottomSheetBehavior$SettleRunnable;

    invoke-direct {v1, p0, p2, v0}, Landroid/support/design/widget/BottomSheetBehavior$SettleRunnable;-><init>(Landroid/support/design/widget/BottomSheetBehavior;Landroid/view/View;I)V

    invoke-static {p2, v1}, LX/0vv;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 639313
    :cond_2
    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->c:I

    .line 639314
    const/4 v0, 0x4

    goto :goto_1
.end method

.method public final onTouchEvent(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/design/widget/CoordinatorLayout;",
            "TV;",
            "Landroid/view/MotionEvent;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 639303
    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->e:LX/3ty;

    invoke-virtual {v0, p3}, LX/3ty;->b(Landroid/view/MotionEvent;)V

    .line 639304
    const/4 v0, 0x1

    return v0
.end method
