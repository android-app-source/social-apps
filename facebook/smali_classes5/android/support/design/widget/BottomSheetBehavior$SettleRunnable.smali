.class public final Landroid/support/design/widget/BottomSheetBehavior$SettleRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/support/design/widget/BottomSheetBehavior;

.field private final b:Landroid/view/View;

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/support/design/widget/BottomSheetBehavior;Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 639286
    iput-object p1, p0, Landroid/support/design/widget/BottomSheetBehavior$SettleRunnable;->a:Landroid/support/design/widget/BottomSheetBehavior;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 639287
    iput-object p2, p0, Landroid/support/design/widget/BottomSheetBehavior$SettleRunnable;->b:Landroid/view/View;

    .line 639288
    iput p3, p0, Landroid/support/design/widget/BottomSheetBehavior$SettleRunnable;->c:I

    .line 639289
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 639290
    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior$SettleRunnable;->a:Landroid/support/design/widget/BottomSheetBehavior;

    iget-object v0, v0, Landroid/support/design/widget/BottomSheetBehavior;->e:LX/3ty;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior$SettleRunnable;->a:Landroid/support/design/widget/BottomSheetBehavior;

    iget-object v0, v0, Landroid/support/design/widget/BottomSheetBehavior;->e:LX/3ty;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/3ty;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 639291
    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior$SettleRunnable;->b:Landroid/view/View;

    invoke-static {v0, p0}, LX/0vv;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 639292
    :goto_0
    return-void

    .line 639293
    :cond_0
    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior$SettleRunnable;->a:Landroid/support/design/widget/BottomSheetBehavior;

    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior$SettleRunnable;->c:I

    invoke-static {v0, v1}, Landroid/support/design/widget/BottomSheetBehavior;->b(Landroid/support/design/widget/BottomSheetBehavior;I)V

    goto :goto_0
.end method
