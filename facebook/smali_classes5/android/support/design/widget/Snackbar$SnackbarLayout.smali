.class public final Landroid/support/design/widget/Snackbar$SnackbarLayout;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field public a:Landroid/widget/TextView;

.field public b:Landroid/widget/Button;

.field private c:I

.field private d:I

.field public e:LX/3oO;

.field public f:LX/3oM;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 640161
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 640162
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 640223
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 640224
    sget-object v0, LX/03r;->SnackbarLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 640225
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->c:I

    .line 640226
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->d:I

    .line 640227
    const/16 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 640228
    const/16 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    int-to-float v1, v1

    invoke-static {p0, v1}, LX/0vv;->f(Landroid/view/View;F)V

    .line 640229
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 640230
    invoke-virtual {p0, v3}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->setClickable(Z)V

    .line 640231
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030401

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 640232
    invoke-static {p0, v3}, LX/0vv;->f(Landroid/view/View;I)V

    .line 640233
    return-void
.end method

.method private static a(Landroid/view/View;II)V
    .locals 2

    .prologue
    .line 640218
    sget-object v0, LX/0vv;->a:LX/0w2;

    invoke-interface {v0, p0}, LX/0w2;->D(Landroid/view/View;)Z

    move-result v0

    move v0, v0

    .line 640219
    if-eqz v0, :cond_0

    .line 640220
    invoke-static {p0}, LX/0vv;->n(Landroid/view/View;)I

    move-result v0

    invoke-static {p0}, LX/0vv;->o(Landroid/view/View;)I

    move-result v1

    invoke-static {p0, v0, p1, v1, p2}, LX/0vv;->b(Landroid/view/View;IIII)V

    .line 640221
    :goto_0
    return-void

    .line 640222
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    invoke-virtual {p0, v0, p1, v1, p2}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0
.end method

.method private a(III)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 640212
    const/4 v0, 0x0

    .line 640213
    invoke-virtual {p0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getOrientation()I

    move-result v2

    if-eq p1, v2, :cond_0

    .line 640214
    invoke-virtual {p0, p1}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->setOrientation(I)V

    move v0, v1

    .line 640215
    :cond_0
    iget-object v2, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->a:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v2

    if-ne v2, p2, :cond_1

    iget-object v2, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->a:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v2

    if-eq v2, p3, :cond_2

    .line 640216
    :cond_1
    iget-object v0, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->a:Landroid/widget/TextView;

    invoke-static {v0, p2, p3}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->a(Landroid/view/View;II)V

    move v0, v1

    .line 640217
    :cond_2
    return v0
.end method


# virtual methods
.method public final a(II)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 640206
    iget-object v0, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->a:Landroid/widget/TextView;

    invoke-static {v0, v1}, LX/0vv;->c(Landroid/view/View;F)V

    .line 640207
    iget-object v0, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->a:Landroid/widget/TextView;

    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/3sU;->a(F)LX/3sU;

    move-result-object v0

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, LX/3sU;->a(J)LX/3sU;

    move-result-object v0

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, LX/3sU;->b(J)LX/3sU;

    move-result-object v0

    invoke-virtual {v0}, LX/3sU;->b()V

    .line 640208
    iget-object v0, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->b:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 640209
    iget-object v0, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->b:Landroid/widget/Button;

    invoke-static {v0, v1}, LX/0vv;->c(Landroid/view/View;F)V

    .line 640210
    iget-object v0, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->b:Landroid/widget/Button;

    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/3sU;->a(F)LX/3sU;

    move-result-object v0

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, LX/3sU;->a(J)LX/3sU;

    move-result-object v0

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, LX/3sU;->b(J)LX/3sU;

    move-result-object v0

    invoke-virtual {v0}, LX/3sU;->b()V

    .line 640211
    :cond_0
    return-void
.end method

.method public final b(II)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 640200
    iget-object v0, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->a:Landroid/widget/TextView;

    invoke-static {v0, v4}, LX/0vv;->c(Landroid/view/View;F)V

    .line 640201
    iget-object v0, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->a:Landroid/widget/TextView;

    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/3sU;->a(F)LX/3sU;

    move-result-object v0

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, LX/3sU;->a(J)LX/3sU;

    move-result-object v0

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, LX/3sU;->b(J)LX/3sU;

    move-result-object v0

    invoke-virtual {v0}, LX/3sU;->b()V

    .line 640202
    iget-object v0, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->b:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 640203
    iget-object v0, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->b:Landroid/widget/Button;

    invoke-static {v0, v4}, LX/0vv;->c(Landroid/view/View;F)V

    .line 640204
    iget-object v0, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->b:Landroid/widget/Button;

    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/3sU;->a(F)LX/3sU;

    move-result-object v0

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, LX/3sU;->a(J)LX/3sU;

    move-result-object v0

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, LX/3sU;->b(J)LX/3sU;

    move-result-object v0

    invoke-virtual {v0}, LX/3sU;->b()V

    .line 640205
    :cond_0
    return-void
.end method

.method public getActionView()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 640199
    iget-object v0, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->b:Landroid/widget/Button;

    return-object v0
.end method

.method public getMessageView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 640198
    iget-object v0, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->a:Landroid/widget/TextView;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x630710dd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 640196
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 640197
    const/16 v1, 0x2d

    const v2, 0x60bbb7a1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x1098cbfd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 640192
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 640193
    iget-object v1, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->f:LX/3oM;

    if-eqz v1, :cond_0

    .line 640194
    iget-object v1, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->f:LX/3oM;

    invoke-interface {v1}, LX/3oM;->a()V

    .line 640195
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x7f981c08

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x2debe1c2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 640188
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 640189
    const v0, 0x7f0d0c51

    invoke-virtual {p0, v0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->a:Landroid/widget/TextView;

    .line 640190
    const v0, 0x7f0d0b17

    invoke-virtual {p0, v0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->b:Landroid/widget/Button;

    .line 640191
    const/16 v0, 0x2d

    const v2, -0x4ec6cea9

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 640184
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 640185
    if-eqz p1, :cond_0

    iget-object v0, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->e:LX/3oO;

    if-eqz v0, :cond_0

    .line 640186
    iget-object v0, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->e:LX/3oO;

    invoke-interface {v0}, LX/3oO;->a()V

    .line 640187
    :cond_0
    return-void
.end method

.method public final onMeasure(II)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 640167
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 640168
    iget v0, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->c:I

    if-lez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->c:I

    if-le v0, v1, :cond_0

    .line 640169
    iget v0, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->c:I

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 640170
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 640171
    :cond_0
    invoke-virtual {p0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0139

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 640172
    invoke-virtual {p0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0b0138

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 640173
    iget-object v4, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->a:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v4

    invoke-virtual {v4}, Landroid/text/Layout;->getLineCount()I

    move-result v4

    if-le v4, v2, :cond_2

    move v4, v2

    .line 640174
    :goto_0
    if-eqz v4, :cond_3

    iget v5, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->d:I

    if-lez v5, :cond_3

    iget-object v5, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->b:Landroid/widget/Button;

    invoke-virtual {v5}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v5

    iget v6, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->d:I

    if-le v5, v6, :cond_3

    .line 640175
    sub-int v1, v0, v1

    invoke-direct {p0, v2, v0, v1}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->a(III)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v2

    .line 640176
    :goto_1
    if-eqz v0, :cond_1

    .line 640177
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 640178
    :cond_1
    return-void

    :cond_2
    move v4, v3

    .line 640179
    goto :goto_0

    .line 640180
    :cond_3
    if-eqz v4, :cond_4

    .line 640181
    :goto_2
    invoke-direct {p0, v3, v0, v0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->a(III)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v2

    .line 640182
    goto :goto_1

    :cond_4
    move v0, v1

    .line 640183
    goto :goto_2

    :cond_5
    move v0, v3

    goto :goto_1
.end method

.method public setOnAttachStateChangeListener(LX/3oM;)V
    .locals 0

    .prologue
    .line 640165
    iput-object p1, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->f:LX/3oM;

    .line 640166
    return-void
.end method

.method public setOnLayoutChangeListener(LX/3oO;)V
    .locals 0

    .prologue
    .line 640163
    iput-object p1, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->e:LX/3oO;

    .line 640164
    return-void
.end method
