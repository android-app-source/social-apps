.class public final Landroid/support/v4/animation/DonutAnimatorCompatProvider$DonutFloatValueAnimator$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/3ox;


# direct methods
.method public constructor <init>(LX/3ox;)V
    .locals 0

    .prologue
    .line 641018
    iput-object p1, p0, Landroid/support/v4/animation/DonutAnimatorCompatProvider$DonutFloatValueAnimator$1;->a:LX/3ox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 641005
    iget-object v0, p0, Landroid/support/v4/animation/DonutAnimatorCompatProvider$DonutFloatValueAnimator$1;->a:LX/3ox;

    invoke-static {v0}, LX/3ox;->e(LX/3ox;)J

    move-result-wide v2

    iget-object v0, p0, Landroid/support/v4/animation/DonutAnimatorCompatProvider$DonutFloatValueAnimator$1;->a:LX/3ox;

    iget-wide v4, v0, LX/3ox;->d:J

    sub-long/2addr v2, v4

    .line 641006
    long-to-float v0, v2

    mul-float/2addr v0, v1

    iget-object v2, p0, Landroid/support/v4/animation/DonutAnimatorCompatProvider$DonutFloatValueAnimator$1;->a:LX/3ox;

    iget-wide v2, v2, LX/3ox;->e:J

    long-to-float v2, v2

    div-float/2addr v0, v2

    .line 641007
    cmpl-float v2, v0, v1

    if-gtz v2, :cond_0

    iget-object v2, p0, Landroid/support/v4/animation/DonutAnimatorCompatProvider$DonutFloatValueAnimator$1;->a:LX/3ox;

    iget-object v2, v2, LX/3ox;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 641008
    :cond_1
    iget-object v2, p0, Landroid/support/v4/animation/DonutAnimatorCompatProvider$DonutFloatValueAnimator$1;->a:LX/3ox;

    .line 641009
    iput v0, v2, LX/3ox;->f:F

    .line 641010
    iget-object v0, p0, Landroid/support/v4/animation/DonutAnimatorCompatProvider$DonutFloatValueAnimator$1;->a:LX/3ox;

    .line 641011
    iget-object v2, v0, LX/3ox;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v3, v2

    :goto_0
    if-ltz v3, :cond_2

    .line 641012
    iget-object v2, v0, LX/3ox;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3ov;

    invoke-interface {v2, v0}, LX/3ov;->a(LX/3ow;)V

    .line 641013
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    goto :goto_0

    .line 641014
    :cond_2
    iget-object v0, p0, Landroid/support/v4/animation/DonutAnimatorCompatProvider$DonutFloatValueAnimator$1;->a:LX/3ox;

    iget v0, v0, LX/3ox;->f:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_3

    .line 641015
    iget-object v0, p0, Landroid/support/v4/animation/DonutAnimatorCompatProvider$DonutFloatValueAnimator$1;->a:LX/3ox;

    invoke-static {v0}, LX/3ox;->g(LX/3ox;)V

    .line 641016
    :goto_1
    return-void

    .line 641017
    :cond_3
    iget-object v0, p0, Landroid/support/v4/animation/DonutAnimatorCompatProvider$DonutFloatValueAnimator$1;->a:LX/3ox;

    iget-object v0, v0, LX/3ox;->c:Landroid/view/View;

    iget-object v1, p0, Landroid/support/v4/animation/DonutAnimatorCompatProvider$DonutFloatValueAnimator$1;->a:LX/3ox;

    iget-object v1, v1, LX/3ox;->i:Ljava/lang/Runnable;

    const-wide/16 v2, 0x10

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method
