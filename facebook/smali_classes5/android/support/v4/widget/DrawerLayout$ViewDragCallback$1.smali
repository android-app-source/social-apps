.class public final Landroid/support/v4/widget/DrawerLayout$ViewDragCallback$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/3tU;


# direct methods
.method public constructor <init>(LX/3tU;)V
    .locals 0

    .prologue
    .line 644917
    iput-object p1, p0, Landroid/support/v4/widget/DrawerLayout$ViewDragCallback$1;->a:LX/3tU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 15

    .prologue
    .line 644918
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout$ViewDragCallback$1;->a:LX/3tU;

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 644919
    iget-object v2, v0, LX/3tU;->c:LX/3ty;

    .line 644920
    iget v3, v2, LX/3ty;->o:I

    move v3, v3

    .line 644921
    iget v2, v0, LX/3tU;->b:I

    if-ne v2, v6, :cond_5

    move v4, v5

    .line 644922
    :goto_0
    if-eqz v4, :cond_6

    .line 644923
    iget-object v2, v0, LX/3tU;->a:LX/3tW;

    invoke-virtual {v2, v6}, LX/3tW;->b(I)Landroid/view/View;

    move-result-object v2

    .line 644924
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v1

    neg-int v1, v1

    :cond_0
    add-int/2addr v1, v3

    move-object v3, v2

    move v2, v1

    .line 644925
    :goto_1
    if-eqz v3, :cond_4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v1

    if-lt v1, v2, :cond_2

    :cond_1
    if-nez v4, :cond_4

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v1

    if-le v1, v2, :cond_4

    :cond_2
    iget-object v1, v0, LX/3tU;->a:LX/3tW;

    invoke-virtual {v1, v3}, LX/3tW;->a(Landroid/view/View;)I

    move-result v1

    if-nez v1, :cond_4

    .line 644926
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, LX/3tS;

    .line 644927
    iget-object v4, v0, LX/3tU;->c:LX/3ty;

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v6

    invoke-virtual {v4, v3, v2, v6}, LX/3ty;->a(Landroid/view/View;II)Z

    .line 644928
    iput-boolean v5, v1, LX/3tS;->c:Z

    .line 644929
    iget-object v1, v0, LX/3tU;->a:LX/3tW;

    invoke-virtual {v1}, LX/3tW;->invalidate()V

    .line 644930
    invoke-static {v0}, LX/3tU;->e(LX/3tU;)V

    .line 644931
    iget-object v1, v0, LX/3tU;->a:LX/3tW;

    const/4 v14, 0x0

    const/4 v12, 0x0

    .line 644932
    iget-boolean v7, v1, LX/3tW;->s:Z

    if-nez v7, :cond_4

    .line 644933
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v7

    .line 644934
    const/4 v11, 0x3

    move-wide v9, v7

    move v13, v12

    invoke-static/range {v7 .. v14}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v7

    .line 644935
    invoke-virtual {v1}, LX/3tW;->getChildCount()I

    move-result v8

    .line 644936
    :goto_2
    if-ge v14, v8, :cond_3

    .line 644937
    invoke-virtual {v1, v14}, LX/3tW;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 644938
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    .line 644939
    :cond_3
    invoke-virtual {v7}, Landroid/view/MotionEvent;->recycle()V

    .line 644940
    const/4 v7, 0x1

    iput-boolean v7, v1, LX/3tW;->s:Z

    .line 644941
    :cond_4
    return-void

    :cond_5
    move v4, v1

    .line 644942
    goto :goto_0

    .line 644943
    :cond_6
    iget-object v1, v0, LX/3tU;->a:LX/3tW;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, LX/3tW;->b(I)Landroid/view/View;

    move-result-object v2

    .line 644944
    iget-object v1, v0, LX/3tU;->a:LX/3tW;

    invoke-virtual {v1}, LX/3tW;->getWidth()I

    move-result v1

    sub-int/2addr v1, v3

    move-object v3, v2

    move v2, v1

    goto :goto_1
.end method
