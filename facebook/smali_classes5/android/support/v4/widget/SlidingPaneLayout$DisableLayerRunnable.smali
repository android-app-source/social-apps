.class public final Landroid/support/v4/widget/SlidingPaneLayout$DisableLayerRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final a:Landroid/view/View;

.field public final synthetic b:LX/3ts;


# direct methods
.method public constructor <init>(LX/3ts;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 646383
    iput-object p1, p0, Landroid/support/v4/widget/SlidingPaneLayout$DisableLayerRunnable;->b:LX/3ts;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 646384
    iput-object p2, p0, Landroid/support/v4/widget/SlidingPaneLayout$DisableLayerRunnable;->a:Landroid/view/View;

    .line 646385
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 646386
    iget-object v0, p0, Landroid/support/v4/widget/SlidingPaneLayout$DisableLayerRunnable;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/widget/SlidingPaneLayout$DisableLayerRunnable;->b:LX/3ts;

    if-ne v0, v1, :cond_0

    .line 646387
    iget-object v0, p0, Landroid/support/v4/widget/SlidingPaneLayout$DisableLayerRunnable;->a:Landroid/view/View;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/0vv;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 646388
    iget-object v0, p0, Landroid/support/v4/widget/SlidingPaneLayout$DisableLayerRunnable;->b:LX/3ts;

    iget-object v1, p0, Landroid/support/v4/widget/SlidingPaneLayout$DisableLayerRunnable;->a:Landroid/view/View;

    invoke-static {v0, v1}, LX/3ts;->c(LX/3ts;Landroid/view/View;)V

    .line 646389
    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/SlidingPaneLayout$DisableLayerRunnable;->b:LX/3ts;

    iget-object v0, v0, LX/3ts;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 646390
    return-void
.end method
