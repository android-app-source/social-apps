.class public final Landroid/support/v4/widget/AutoScrollHelper$ScrollAnimationRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/3tG;


# direct methods
.method public constructor <init>(LX/3tG;)V
    .locals 0

    .prologue
    .line 644509
    iput-object p1, p0, Landroid/support/v4/widget/AutoScrollHelper$ScrollAnimationRunnable;->a:LX/3tG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 644510
    iget-object v0, p0, Landroid/support/v4/widget/AutoScrollHelper$ScrollAnimationRunnable;->a:LX/3tG;

    iget-boolean v0, v0, LX/3tG;->o:Z

    if-nez v0, :cond_0

    .line 644511
    :goto_0
    return-void

    .line 644512
    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/AutoScrollHelper$ScrollAnimationRunnable;->a:LX/3tG;

    iget-boolean v0, v0, LX/3tG;->m:Z

    if-eqz v0, :cond_1

    .line 644513
    iget-object v0, p0, Landroid/support/v4/widget/AutoScrollHelper$ScrollAnimationRunnable;->a:LX/3tG;

    .line 644514
    iput-boolean v2, v0, LX/3tG;->m:Z

    .line 644515
    iget-object v0, p0, Landroid/support/v4/widget/AutoScrollHelper$ScrollAnimationRunnable;->a:LX/3tG;

    iget-object v0, v0, LX/3tG;->a:LX/3tF;

    const/4 v5, 0x0

    .line 644516
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v3

    iput-wide v3, v0, LX/3tF;->e:J

    .line 644517
    const-wide/16 v3, -0x1

    iput-wide v3, v0, LX/3tF;->i:J

    .line 644518
    iget-wide v3, v0, LX/3tF;->e:J

    iput-wide v3, v0, LX/3tF;->f:J

    .line 644519
    const/high16 v3, 0x3f000000    # 0.5f

    iput v3, v0, LX/3tF;->j:F

    .line 644520
    iput v5, v0, LX/3tF;->g:I

    .line 644521
    iput v5, v0, LX/3tF;->h:I

    .line 644522
    :cond_1
    iget-object v0, p0, Landroid/support/v4/widget/AutoScrollHelper$ScrollAnimationRunnable;->a:LX/3tG;

    iget-object v0, v0, LX/3tG;->a:LX/3tF;

    .line 644523
    iget-wide v3, v0, LX/3tF;->i:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-lez v3, :cond_5

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v3

    iget-wide v5, v0, LX/3tF;->i:J

    iget v7, v0, LX/3tF;->k:I

    int-to-long v7, v7

    add-long/2addr v5, v7

    cmp-long v3, v3, v5

    if-lez v3, :cond_5

    const/4 v3, 0x1

    :goto_1
    move v1, v3

    .line 644524
    if-nez v1, :cond_2

    iget-object v1, p0, Landroid/support/v4/widget/AutoScrollHelper$ScrollAnimationRunnable;->a:LX/3tG;

    invoke-static {v1}, LX/3tG;->b$redex0(LX/3tG;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 644525
    :cond_2
    iget-object v0, p0, Landroid/support/v4/widget/AutoScrollHelper$ScrollAnimationRunnable;->a:LX/3tG;

    .line 644526
    iput-boolean v2, v0, LX/3tG;->o:Z

    .line 644527
    goto :goto_0

    .line 644528
    :cond_3
    iget-object v1, p0, Landroid/support/v4/widget/AutoScrollHelper$ScrollAnimationRunnable;->a:LX/3tG;

    iget-boolean v1, v1, LX/3tG;->n:Z

    if-eqz v1, :cond_4

    .line 644529
    iget-object v1, p0, Landroid/support/v4/widget/AutoScrollHelper$ScrollAnimationRunnable;->a:LX/3tG;

    .line 644530
    iput-boolean v2, v1, LX/3tG;->n:Z

    .line 644531
    iget-object v1, p0, Landroid/support/v4/widget/AutoScrollHelper$ScrollAnimationRunnable;->a:LX/3tG;

    const/4 v8, 0x0

    .line 644532
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    .line 644533
    const/4 v7, 0x3

    const/4 v10, 0x0

    move-wide v5, v3

    move v9, v8

    invoke-static/range {v3 .. v10}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v3

    .line 644534
    iget-object v4, v1, LX/3tG;->c:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 644535
    invoke-virtual {v3}, Landroid/view/MotionEvent;->recycle()V

    .line 644536
    :cond_4
    invoke-virtual {v0}, LX/3tF;->d()V

    .line 644537
    iget v1, v0, LX/3tF;->h:I

    move v0, v1

    .line 644538
    iget-object v1, p0, Landroid/support/v4/widget/AutoScrollHelper$ScrollAnimationRunnable;->a:LX/3tG;

    invoke-virtual {v1, v0}, LX/3tG;->a(I)V

    .line 644539
    iget-object v0, p0, Landroid/support/v4/widget/AutoScrollHelper$ScrollAnimationRunnable;->a:LX/3tG;

    iget-object v0, v0, LX/3tG;->c:Landroid/view/View;

    invoke-static {v0, p0}, LX/0vv;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    :cond_5
    const/4 v3, 0x0

    goto :goto_1
.end method
