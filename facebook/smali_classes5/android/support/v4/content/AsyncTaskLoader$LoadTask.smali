.class public final Landroid/support/v4/content/AsyncTaskLoader$LoadTask;
.super LX/3cc;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3cc",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "TD;>;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TD;"
        }
    .end annotation
.end field

.field public b:Z

.field public final synthetic c:LX/25A;

.field private e:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method public constructor <init>(LX/25A;)V
    .locals 2

    .prologue
    .line 614718
    iput-object p1, p0, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;->c:LX/25A;

    invoke-direct {p0}, LX/3cc;-><init>()V

    .line 614719
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;->e:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 614720
    iget-object v0, p0, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;->c:LX/25A;

    .line 614721
    invoke-virtual {v0}, LX/25A;->d()Ljava/lang/Object;

    move-result-object p1

    move-object v0, p1

    .line 614722
    iput-object v0, p0, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;->a:Ljava/lang/Object;

    .line 614723
    iget-object v0, p0, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 614724
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;->c:LX/25A;

    iget-object v1, p0, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;->a:Ljava/lang/Object;

    invoke-virtual {v0, p0, v1}, LX/25A;->a(Landroid/support/v4/content/AsyncTaskLoader$LoadTask;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 614725
    iget-object v0, p0, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 614726
    return-void

    .line 614727
    :catchall_0
    move-exception v0

    iget-object v1, p0, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;)V"
        }
    .end annotation

    .prologue
    .line 614728
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;->c:LX/25A;

    .line 614729
    iget-object v2, v0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    if-eq v2, p0, :cond_0

    .line 614730
    invoke-virtual {v0, p0, p1}, LX/25A;->a(Landroid/support/v4/content/AsyncTaskLoader$LoadTask;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 614731
    :goto_0
    iget-object v0, p0, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 614732
    return-void

    .line 614733
    :catchall_0
    move-exception v0

    iget-object v1, p0, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0

    .line 614734
    :cond_0
    iget-boolean v2, v0, LX/0k9;->q:Z

    move v2, v2

    .line 614735
    if-eqz v2, :cond_1

    .line 614736
    invoke-virtual {v0, p1}, LX/25A;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 614737
    :cond_1
    invoke-virtual {v0}, LX/0k9;->u()V

    .line 614738
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, LX/25A;->d:J

    .line 614739
    const/4 v2, 0x0

    iput-object v2, v0, LX/25A;->a:Landroid/support/v4/content/AsyncTaskLoader$LoadTask;

    .line 614740
    invoke-virtual {v0, p1}, LX/0k9;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final run()V
    .locals 1

    .prologue
    .line 614741
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;->b:Z

    .line 614742
    iget-object v0, p0, Landroid/support/v4/content/AsyncTaskLoader$LoadTask;->c:LX/25A;

    invoke-virtual {v0}, LX/25A;->c()V

    .line 614743
    return-void
.end method
