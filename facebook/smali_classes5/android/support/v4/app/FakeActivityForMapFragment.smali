.class public Landroid/support/v4/app/FakeActivityForMapFragment;
.super Landroid/support/v4/app/FragmentActivity;
.source ""


# instance fields
.field private final p:Landroid/support/v4/app/Fragment;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/Fragment;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 641249
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 641250
    iput-object p1, p0, Landroid/support/v4/app/FakeActivityForMapFragment;->p:Landroid/support/v4/app/Fragment;

    .line 641251
    invoke-virtual {p0, p2}, Landroid/support/v4/app/FakeActivityForMapFragment;->attachBaseContext(Landroid/content/Context;)V

    .line 641252
    return-void
.end method


# virtual methods
.method public final getComponentName()Landroid/content/ComponentName;
    .locals 2

    .prologue
    .line 641253
    invoke-virtual {p0}, Landroid/support/v4/app/FakeActivityForMapFragment;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/FakeActivityForMapFragment;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 641254
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getWindow()Landroid/view/Window;
    .locals 2

    .prologue
    .line 641255
    iget-object v0, p0, Landroid/support/v4/app/FakeActivityForMapFragment;->p:Landroid/support/v4/app/Fragment;

    .line 641256
    :goto_0
    if-eqz v0, :cond_1

    .line 641257
    instance-of v1, v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v1, :cond_0

    .line 641258
    check-cast v0, Landroid/support/v4/app/DialogFragment;

    iget-object v0, v0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 641259
    :goto_1
    return-object v0

    .line 641260
    :cond_0
    iget-object v0, v0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    iget-object v0, v0, LX/0jz;->p:Landroid/support/v4/app/Fragment;

    goto :goto_0

    .line 641261
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
