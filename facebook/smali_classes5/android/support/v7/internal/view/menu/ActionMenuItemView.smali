.class public Landroid/support/v7/internal/view/menu/ActionMenuItemView;
.super LX/3up;
.source ""

# interfaces
.implements LX/3uq;
.implements LX/3ur;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field public a:LX/3v3;

.field private b:Ljava/lang/CharSequence;

.field private c:Landroid/graphics/drawable/Drawable;

.field public d:LX/3uw;

.field private e:LX/3um;

.field public f:LX/3uo;

.field private g:Z

.field private h:Z

.field private i:I

.field private j:I

.field private k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 649668
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 649669
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 649666
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 649667
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 649654
    invoke-direct {p0, p1, p2, p3}, LX/3up;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 649655
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 649656
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->g:Z

    .line 649657
    sget-object v1, LX/03r;->ActionMenuItemView:[I

    invoke-virtual {p1, p2, v1, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 649658
    const/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->i:I

    .line 649659
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 649660
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 649661
    const/high16 v1, 0x42000000    # 32.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->k:I

    .line 649662
    invoke-virtual {p0, p0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 649663
    invoke-virtual {p0, p0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 649664
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->j:I

    .line 649665
    return-void
.end method

.method private e()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 649646
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->b:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 649647
    :goto_0
    iget-object v3, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_0

    iget-object v3, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->a:LX/3v3;

    .line 649648
    iget v4, v3, LX/3v3;->q:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_4

    const/4 v4, 0x1

    :goto_1
    move v3, v4

    .line 649649
    if-eqz v3, :cond_1

    iget-boolean v3, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->g:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->h:Z

    if-eqz v3, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    and-int/2addr v0, v2

    .line 649650
    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->b:Ljava/lang/CharSequence;

    :goto_2
    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->setText(Ljava/lang/CharSequence;)V

    .line 649651
    return-void

    :cond_2
    move v0, v2

    .line 649652
    goto :goto_0

    .line 649653
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    const/4 v4, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/3v3;I)V
    .locals 1

    .prologue
    .line 649635
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->a:LX/3v3;

    .line 649636
    invoke-virtual {p1}, LX/3v3;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 649637
    invoke-virtual {p1, p0}, LX/3v3;->a(LX/3uq;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->setTitle(Ljava/lang/CharSequence;)V

    .line 649638
    invoke-virtual {p1}, LX/3v3;->getItemId()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->setId(I)V

    .line 649639
    invoke-virtual {p1}, LX/3v3;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->setVisibility(I)V

    .line 649640
    invoke-virtual {p1}, LX/3v3;->isEnabled()Z

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->setEnabled(Z)V

    .line 649641
    invoke-virtual {p1}, LX/3v3;->hasSubMenu()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 649642
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->e:LX/3um;

    if-nez v0, :cond_0

    .line 649643
    new-instance v0, LX/3un;

    invoke-direct {v0, p0}, LX/3un;-><init>(Landroid/support/v7/internal/view/menu/ActionMenuItemView;)V

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->e:LX/3um;

    .line 649644
    :cond_0
    return-void

    .line 649645
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 649634
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 649633
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 649632
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->a:LX/3v3;

    invoke-virtual {v0}, LX/3v3;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 649631
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->b()Z

    move-result v0

    return v0
.end method

.method public getItemData()LX/3v3;
    .locals 1

    .prologue
    .line 649630
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->a:LX/3v3;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x748b908a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 649548
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->d:LX/3uw;

    if-eqz v1, :cond_0

    .line 649549
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->d:LX/3uw;

    iget-object v2, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->a:LX/3v3;

    invoke-interface {v1, v2}, LX/3uw;->a(LX/3v3;)Z

    .line 649550
    :cond_0
    const v1, -0x6aeda6a8

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 649625
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    .line 649626
    invoke-super {p0, p1}, LX/3up;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 649627
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->g:Z

    .line 649628
    invoke-direct {p0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->e()V

    .line 649629
    return-void
.end method

.method public final onLongClick(Landroid/view/View;)Z
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 649605
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 649606
    :goto_0
    return v0

    .line 649607
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 649608
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 649609
    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->getLocationOnScreen([I)V

    .line 649610
    invoke-virtual {p0, v3}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 649611
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 649612
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->getWidth()I

    move-result v5

    .line 649613
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->getHeight()I

    move-result v6

    .line 649614
    aget v7, v0, v2

    div-int/lit8 v8, v6, 0x2

    add-int/2addr v7, v8

    .line 649615
    aget v0, v0, v1

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v0, v5

    .line 649616
    invoke-static {p1}, LX/0vv;->h(Landroid/view/View;)I

    move-result v5

    if-nez v5, :cond_1

    .line 649617
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 649618
    sub-int v0, v5, v0

    .line 649619
    :cond_1
    iget-object v5, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->a:LX/3v3;

    invoke-virtual {v5}, LX/3v3;->getTitle()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v4, v5, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    .line 649620
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    if-ge v7, v3, :cond_2

    .line 649621
    const v1, 0x800035

    invoke-virtual {v4, v1, v0, v6}, Landroid/widget/Toast;->setGravity(III)V

    .line 649622
    :goto_1
    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    move v0, v2

    .line 649623
    goto :goto_0

    .line 649624
    :cond_2
    const/16 v0, 0x51

    invoke-virtual {v4, v0, v1, v6}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_1
.end method

.method public final onMeasure(II)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v0, 0x2

    const/16 v1, 0x2c

    const v2, 0x29a4dc77

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 649589
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->b()Z

    move-result v2

    .line 649590
    if-eqz v2, :cond_0

    iget v0, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->j:I

    if-ltz v0, :cond_0

    .line 649591
    iget v0, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->j:I

    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->getPaddingTop()I

    move-result v3

    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->getPaddingRight()I

    move-result v4

    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->getPaddingBottom()I

    move-result v5

    invoke-super {p0, v0, v3, v4, v5}, LX/3up;->setPadding(IIII)V

    .line 649592
    :cond_0
    invoke-super {p0, p1, p2}, LX/3up;->onMeasure(II)V

    .line 649593
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 649594
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 649595
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->getMeasuredWidth()I

    move-result v4

    .line 649596
    const/high16 v5, -0x80000000

    if-ne v3, v5, :cond_3

    iget v5, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->i:I

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 649597
    :goto_0
    if-eq v3, v6, :cond_1

    iget v3, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->i:I

    if-lez v3, :cond_1

    if-ge v4, v0, :cond_1

    .line 649598
    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, v0, p2}, LX/3up;->onMeasure(II)V

    .line 649599
    :cond_1
    if-nez v2, :cond_2

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    .line 649600
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->getMeasuredWidth()I

    move-result v0

    .line 649601
    iget-object v2, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 649602
    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->getPaddingRight()I

    move-result v3

    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->getPaddingBottom()I

    move-result v4

    invoke-super {p0, v0, v2, v3, v4}, LX/3up;->setPadding(IIII)V

    .line 649603
    :cond_2
    const v0, 0xd6ae233

    invoke-static {v0, v1}, LX/02F;->g(II)V

    return-void

    .line 649604
    :cond_3
    iget v0, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->i:I

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x2

    const v1, -0x3e4e506a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 649586
    iget-object v2, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->a:LX/3v3;

    invoke-virtual {v2}, LX/3v3;->hasSubMenu()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->e:LX/3um;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->e:LX/3um;

    invoke-virtual {v2, p0, p1}, LX/3um;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 649587
    const v2, 0x135028db

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 649588
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/3up;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, -0x371485

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setCheckable(Z)V
    .locals 0

    .prologue
    .line 649585
    return-void
.end method

.method public setChecked(Z)V
    .locals 0

    .prologue
    .line 649584
    return-void
.end method

.method public setExpandedFormat(Z)V
    .locals 1

    .prologue
    .line 649578
    iget-boolean v0, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->h:Z

    if-eq v0, p1, :cond_0

    .line 649579
    iput-boolean p1, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->h:Z

    .line 649580
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->a:LX/3v3;

    if-eqz v0, :cond_0

    .line 649581
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->a:LX/3v3;

    .line 649582
    iget-object p0, v0, LX/3v3;->l:LX/3v0;

    invoke-virtual {p0}, LX/3v0;->i()V

    .line 649583
    :cond_0
    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 649562
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->c:Landroid/graphics/drawable/Drawable;

    .line 649563
    if-eqz p1, :cond_2

    .line 649564
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 649565
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 649566
    iget v2, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->k:I

    if-le v1, v2, :cond_0

    .line 649567
    iget v2, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->k:I

    int-to-float v2, v2

    int-to-float v1, v1

    div-float/2addr v2, v1

    .line 649568
    iget v1, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->k:I

    .line 649569
    int-to-float v0, v0

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 649570
    :cond_0
    iget v2, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->k:I

    if-le v0, v2, :cond_1

    .line 649571
    iget v2, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->k:I

    int-to-float v2, v2

    int-to-float v0, v0

    div-float/2addr v2, v0

    .line 649572
    iget v0, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->k:I

    .line 649573
    int-to-float v1, v1

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 649574
    :cond_1
    invoke-virtual {p1, v4, v4, v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 649575
    :cond_2
    invoke-virtual {p0, p1, v3, v3, v3}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 649576
    invoke-direct {p0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->e()V

    .line 649577
    return-void
.end method

.method public setItemInvoker(LX/3uw;)V
    .locals 0

    .prologue
    .line 649560
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->d:LX/3uw;

    .line 649561
    return-void
.end method

.method public final setPadding(IIII)V
    .locals 0

    .prologue
    .line 649557
    iput p1, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->j:I

    .line 649558
    invoke-super {p0, p1, p2, p3, p4}, LX/3up;->setPadding(IIII)V

    .line 649559
    return-void
.end method

.method public setPopupCallback(LX/3uo;)V
    .locals 0

    .prologue
    .line 649555
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->f:LX/3uo;

    .line 649556
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 649551
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->b:Ljava/lang/CharSequence;

    .line 649552
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->b:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 649553
    invoke-direct {p0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->e()V

    .line 649554
    return-void
.end method
