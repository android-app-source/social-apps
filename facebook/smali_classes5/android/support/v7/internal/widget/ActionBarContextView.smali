.class public Landroid/support/v7/internal/widget/ActionBarContextView;
.super LX/3vJ;
.source ""

# interfaces
.implements LX/3oQ;


# instance fields
.field public j:Ljava/lang/CharSequence;

.field public k:Ljava/lang/CharSequence;

.field private l:Landroid/view/View;

.field private m:Landroid/view/View;

.field private n:Landroid/widget/LinearLayout;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/widget/TextView;

.field private q:I

.field private r:I

.field private s:Landroid/graphics/drawable/Drawable;

.field public t:Z

.field private u:I

.field private v:LX/3uk;

.field private w:Z

.field private x:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 651808
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 651809
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 651810
    const v0, 0x7f01001a

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 651811
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 651812
    invoke-direct {p0, p1, p2, p3}, LX/3vJ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 651813
    sget-object v0, LX/03r;->ActionMode:[I

    invoke-static {p1, p2, v0, p3, v2}, LX/3wC;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)LX/3wC;

    move-result-object v0

    .line 651814
    const/16 v1, 0x3

    invoke-virtual {v0, v1}, LX/3wC;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/v7/internal/widget/ActionBarContextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 651815
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, LX/3wC;->f(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->q:I

    .line 651816
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v2}, LX/3wC;->f(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->r:I

    .line 651817
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, LX/3wC;->e(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->h:I

    .line 651818
    const/16 v1, 0x4

    invoke-virtual {v0, v1}, LX/3wC;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->s:Landroid/graphics/drawable/Drawable;

    .line 651819
    const/16 v1, 0x5

    const v2, 0x7f030006

    invoke-virtual {v0, v1, v2}, LX/3wC;->f(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->u:I

    .line 651820
    invoke-virtual {v0}, LX/3wC;->b()V

    .line 651821
    return-void
.end method

.method private e()V
    .locals 6

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 651822
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->n:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    .line 651823
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 651824
    const/high16 v3, 0x7f030000

    invoke-virtual {v0, v3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 651825
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->n:Landroid/widget/LinearLayout;

    .line 651826
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->n:Landroid/widget/LinearLayout;

    const v3, 0x7f0d033b

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->o:Landroid/widget/TextView;

    .line 651827
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->n:Landroid/widget/LinearLayout;

    const v3, 0x7f0d033c

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->p:Landroid/widget/TextView;

    .line 651828
    iget v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->q:I

    if-eqz v0, :cond_0

    .line 651829
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->o:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget v5, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->q:I

    invoke-virtual {v0, v3, v5}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 651830
    :cond_0
    iget v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->r:I

    if-eqz v0, :cond_1

    .line 651831
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->p:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget v5, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->r:I

    invoke-virtual {v0, v3, v5}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 651832
    :cond_1
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->o:Landroid/widget/TextView;

    iget-object v3, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->j:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 651833
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->p:Landroid/widget/TextView;

    iget-object v3, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->k:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 651834
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->j:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 651835
    :goto_0
    iget-object v3, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->k:Ljava/lang/CharSequence;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 651836
    :goto_1
    iget-object v5, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->p:Landroid/widget/TextView;

    if-eqz v1, :cond_7

    move v3, v2

    :goto_2
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 651837
    iget-object v3, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->n:Landroid/widget/LinearLayout;

    if-nez v0, :cond_2

    if-eqz v1, :cond_3

    :cond_2
    move v4, v2

    :cond_3
    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 651838
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_4

    .line 651839
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->addView(Landroid/view/View;)V

    .line 651840
    :cond_4
    return-void

    :cond_5
    move v0, v2

    .line 651841
    goto :goto_0

    :cond_6
    move v1, v2

    .line 651842
    goto :goto_1

    :cond_7
    move v3, v4

    .line 651843
    goto :goto_2
.end method

.method private f()V
    .locals 2

    .prologue
    .line 651844
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->v:LX/3uk;

    .line 651845
    if-eqz v0, :cond_0

    .line 651846
    const/4 v1, 0x0

    iput-object v1, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->v:LX/3uk;

    .line 651847
    invoke-virtual {v0}, LX/3uk;->b()V

    .line 651848
    :cond_0
    return-void
.end method

.method private g()LX/3uk;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 651849
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->l:Landroid/view/View;

    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    neg-int v2, v0

    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v0, v2, v0

    int-to-float v0, v0

    invoke-static {v1, v0}, LX/0vv;->a(Landroid/view/View;F)V

    .line 651850
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->l:Landroid/view/View;

    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/3sU;->b(F)LX/3sU;

    move-result-object v0

    .line 651851
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, LX/3sU;->a(J)LX/3sU;

    .line 651852
    invoke-virtual {v0, p0}, LX/3sU;->a(LX/3oQ;)LX/3sU;

    .line 651853
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, LX/3sU;->a(Landroid/view/animation/Interpolator;)LX/3sU;

    .line 651854
    new-instance v1, LX/3uk;

    invoke-direct {v1}, LX/3uk;-><init>()V

    .line 651855
    invoke-virtual {v1, v0}, LX/3uk;->a(LX/3sU;)LX/3uk;

    .line 651856
    iget-object v0, p0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    if-eqz v0, :cond_0

    .line 651857
    iget-object v0, p0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->getChildCount()I

    move-result v0

    .line 651858
    if-lez v0, :cond_0

    .line 651859
    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 651860
    iget-object v2, p0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 651861
    invoke-static {v2, v6}, LX/0vv;->e(Landroid/view/View;F)V

    .line 651862
    invoke-static {v2}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, LX/3sU;->e(F)LX/3sU;

    move-result-object v2

    .line 651863
    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v4, v5}, LX/3sU;->a(J)LX/3sU;

    .line 651864
    invoke-virtual {v1, v2}, LX/3uk;->a(LX/3sU;)LX/3uk;

    .line 651865
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 651866
    :cond_0
    return-object v1
.end method

.method private h()LX/3uk;
    .locals 4

    .prologue
    .line 651867
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->l:Landroid/view/View;

    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v1

    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    neg-int v2, v0

    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v0, v2, v0

    int-to-float v0, v0

    invoke-virtual {v1, v0}, LX/3sU;->b(F)LX/3sU;

    move-result-object v0

    .line 651868
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, LX/3sU;->a(J)LX/3sU;

    .line 651869
    invoke-virtual {v0, p0}, LX/3sU;->a(LX/3oQ;)LX/3sU;

    .line 651870
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, LX/3sU;->a(Landroid/view/animation/Interpolator;)LX/3sU;

    .line 651871
    new-instance v1, LX/3uk;

    invoke-direct {v1}, LX/3uk;-><init>()V

    .line 651872
    invoke-virtual {v1, v0}, LX/3uk;->a(LX/3sU;)LX/3uk;

    .line 651873
    iget-object v0, p0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    if-eqz v0, :cond_0

    .line 651874
    iget-object v0, p0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->getChildCount()I

    .line 651875
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final a(LX/3uV;)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x1

    .line 651876
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->l:Landroid/view/View;

    if-nez v0, :cond_2

    .line 651877
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 651878
    iget v1, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->u:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->l:Landroid/view/View;

    .line 651879
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->l:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->addView(Landroid/view/View;)V

    .line 651880
    :cond_0
    :goto_0
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->l:Landroid/view/View;

    const v1, 0x7f0d033d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 651881
    new-instance v1, LX/3vQ;

    invoke-direct {v1, p0, p1}, LX/3vQ;-><init>(Landroid/support/v7/internal/widget/ActionBarContextView;LX/3uV;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 651882
    invoke-virtual {p1}, LX/3uV;->b()Landroid/view/Menu;

    move-result-object v0

    check-cast v0, LX/3v0;

    .line 651883
    iget-object v1, p0, LX/3vJ;->d:LX/3wb;

    if-eqz v1, :cond_1

    .line 651884
    iget-object v1, p0, LX/3vJ;->d:LX/3wb;

    invoke-virtual {v1}, LX/3wb;->f()Z

    .line 651885
    :cond_1
    new-instance v1, LX/3wb;

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/3wb;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->d:LX/3wb;

    .line 651886
    iget-object v1, p0, LX/3vJ;->d:LX/3wb;

    invoke-virtual {v1, v4}, LX/3wb;->c(Z)V

    .line 651887
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v2, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 651888
    iget-boolean v2, p0, LX/3vJ;->f:Z

    if-nez v2, :cond_3

    .line 651889
    iget-object v2, p0, LX/3vJ;->d:LX/3wb;

    iget-object v3, p0, LX/3vJ;->b:Landroid/content/Context;

    invoke-virtual {v0, v2, v3}, LX/3v0;->a(LX/3us;Landroid/content/Context;)V

    .line 651890
    iget-object v0, p0, LX/3vJ;->d:LX/3wb;

    invoke-virtual {v0, p0}, LX/3ut;->a(Landroid/view/ViewGroup;)LX/3ux;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ActionMenuView;

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->c:Landroid/support/v7/widget/ActionMenuView;

    .line 651891
    iget-object v0, p0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ActionMenuView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 651892
    iget-object v0, p0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/internal/widget/ActionBarContextView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 651893
    :goto_1
    iput-boolean v4, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->w:Z

    .line 651894
    return-void

    .line 651895
    :cond_2
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    .line 651896
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->l:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 651897
    :cond_3
    iget-object v2, p0, LX/3vJ;->d:LX/3wb;

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v2, v3, v4}, LX/3wb;->a(IZ)V

    .line 651898
    iget-object v2, p0, LX/3vJ;->d:LX/3wb;

    const v3, 0x7fffffff

    invoke-virtual {v2, v3}, LX/3wb;->b(I)V

    .line 651899
    iput v5, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 651900
    iget v2, p0, LX/3vJ;->h:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 651901
    iget-object v2, p0, LX/3vJ;->d:LX/3wb;

    iget-object v3, p0, LX/3vJ;->b:Landroid/content/Context;

    invoke-virtual {v0, v2, v3}, LX/3v0;->a(LX/3us;Landroid/content/Context;)V

    .line 651902
    iget-object v0, p0, LX/3vJ;->d:LX/3wb;

    invoke-virtual {v0, p0}, LX/3ut;->a(Landroid/view/ViewGroup;)LX/3ux;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ActionMenuView;

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->c:Landroid/support/v7/widget/ActionMenuView;

    .line 651903
    iget-object v0, p0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    iget-object v2, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ActionMenuView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 651904
    iget-object v0, p0, LX/3vJ;->e:Landroid/view/ViewGroup;

    iget-object v2, p0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v0, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1
.end method

.method public final a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 651905
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 651906
    iget-object v0, p0, LX/3vJ;->d:LX/3wb;

    if-eqz v0, :cond_0

    .line 651907
    iget-object v0, p0, LX/3vJ;->d:LX/3wb;

    invoke-virtual {v0}, LX/3wb;->d()Z

    move-result v0

    .line 651908
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 651909
    iget v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->x:I

    if-ne v0, v1, :cond_0

    .line 651910
    :goto_0
    return-void

    .line 651911
    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->l:Landroid/view/View;

    if-nez v0, :cond_1

    .line 651912
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->c()V

    goto :goto_0

    .line 651913
    :cond_1
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->f()V

    .line 651914
    iput v1, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->x:I

    .line 651915
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->h()LX/3uk;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->v:LX/3uk;

    .line 651916
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->v:LX/3uk;

    invoke-virtual {v0}, LX/3uk;->a()V

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 651917
    iget v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->x:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 651918
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->c()V

    .line 651919
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->x:I

    .line 651920
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 651800
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->f()V

    .line 651801
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->removeAllViews()V

    .line 651802
    iget-object v0, p0, LX/3vJ;->e:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 651803
    iget-object v0, p0, LX/3vJ;->e:Landroid/view/ViewGroup;

    iget-object v1, p0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 651804
    :cond_0
    iput-object v2, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->m:Landroid/view/View;

    .line 651805
    iput-object v2, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->c:Landroid/support/v7/widget/ActionMenuView;

    .line 651806
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->w:Z

    .line 651807
    return-void
.end method

.method public final c(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 651661
    return-void
.end method

.method public final generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    .prologue
    .line 651921
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    return-object v0
.end method

.method public final generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 651660
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public getSubtitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 651662
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->k:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 651663
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->j:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x63574ab1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 651664
    invoke-super {p0}, LX/3vJ;->onDetachedFromWindow()V

    .line 651665
    iget-object v1, p0, LX/3vJ;->d:LX/3wb;

    if-eqz v1, :cond_0

    .line 651666
    iget-object v1, p0, LX/3vJ;->d:LX/3wb;

    invoke-virtual {v1}, LX/3wb;->e()Z

    .line 651667
    iget-object v1, p0, LX/3vJ;->d:LX/3wb;

    invoke-virtual {v1}, LX/3wb;->g()Z

    .line 651668
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x11b2afeb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    .prologue
    .line 651669
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 651670
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_1

    .line 651671
    invoke-virtual {p1, p0}, Landroid/view/accessibility/AccessibilityEvent;->setSource(Landroid/view/View;)V

    .line 651672
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 651673
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    .line 651674
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->j:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 651675
    :cond_0
    :goto_0
    return-void

    .line 651676
    :cond_1
    invoke-super {p0, p1}, LX/3vJ;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 651677
    invoke-static {p0}, LX/3wJ;->a(Landroid/view/View;)Z

    move-result v5

    .line 651678
    if-eqz v5, :cond_4

    sub-int v0, p4, p2

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    move v1, v0

    .line 651679
    :goto_0
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getPaddingTop()I

    move-result v6

    .line 651680
    sub-int v0, p5, p3

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getPaddingTop()I

    move-result v4

    sub-int/2addr v0, v4

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getPaddingBottom()I

    move-result v4

    sub-int v7, v0, v4

    .line 651681
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->l:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v8, :cond_0

    .line 651682
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 651683
    if-eqz v5, :cond_5

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 651684
    :goto_1
    if-eqz v5, :cond_6

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 651685
    :goto_2
    invoke-static {v1, v4, v5}, LX/3vJ;->a(IIZ)I

    move-result v1

    .line 651686
    iget-object v4, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->l:Landroid/view/View;

    invoke-static {v4, v1, v6, v7, v5}, LX/3vJ;->a(Landroid/view/View;IIIZ)I

    move-result v4

    add-int/2addr v1, v4

    .line 651687
    invoke-static {v1, v0, v5}, LX/3vJ;->a(IIZ)I

    move-result v1

    .line 651688
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->w:Z

    if-eqz v0, :cond_0

    .line 651689
    iput v2, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->x:I

    .line 651690
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->g()LX/3uk;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->v:LX/3uk;

    .line 651691
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->v:LX/3uk;

    invoke-virtual {v0}, LX/3uk;->a()V

    .line 651692
    iput-boolean v3, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->w:Z

    .line 651693
    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->n:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->m:Landroid/view/View;

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-eq v0, v8, :cond_1

    .line 651694
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->n:Landroid/widget/LinearLayout;

    invoke-static {v0, v1, v6, v7, v5}, LX/3vJ;->a(Landroid/view/View;IIIZ)I

    move-result v0

    add-int/2addr v1, v0

    .line 651695
    :cond_1
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->m:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 651696
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->m:Landroid/view/View;

    invoke-static {v0, v1, v6, v7, v5}, LX/3vJ;->a(Landroid/view/View;IIIZ)I

    .line 651697
    :cond_2
    if-eqz v5, :cond_7

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getPaddingLeft()I

    move-result v0

    .line 651698
    :goto_3
    iget-object v1, p0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    if-eqz v1, :cond_3

    .line 651699
    iget-object v4, p0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    if-nez v5, :cond_8

    move v1, v2

    :goto_4
    invoke-static {v4, v0, v6, v7, v1}, LX/3vJ;->a(Landroid/view/View;IIIZ)I

    .line 651700
    :cond_3
    return-void

    .line 651701
    :cond_4
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getPaddingLeft()I

    move-result v0

    move v1, v0

    goto/16 :goto_0

    .line 651702
    :cond_5
    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto :goto_1

    .line 651703
    :cond_6
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto :goto_2

    .line 651704
    :cond_7
    sub-int v0, p4, p2

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_3

    :cond_8
    move v1, v3

    .line 651705
    goto :goto_4
.end method

.method public final onMeasure(II)V
    .locals 12

    .prologue
    const/4 v11, -0x2

    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v5, -0x80000000

    const/4 v3, 0x0

    .line 651706
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 651707
    if-eq v0, v4, :cond_0

    .line 651708
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " can only be used with android:layout_width=\"match_parent\" (or fill_parent)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 651709
    :cond_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 651710
    if-nez v0, :cond_1

    .line 651711
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " can only be used with android:layout_height=\"wrap_content\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 651712
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    .line 651713
    iget v0, p0, LX/3vJ;->h:I

    if-lez v0, :cond_8

    iget v0, p0, LX/3vJ;->h:I

    move v1, v0

    .line 651714
    :goto_0
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getPaddingBottom()I

    move-result v2

    add-int v8, v0, v2

    .line 651715
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getPaddingLeft()I

    move-result v0

    sub-int v0, v7, v0

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v0, v2

    .line 651716
    sub-int v6, v1, v8

    .line 651717
    invoke-static {v6, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 651718
    iget-object v9, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->l:Landroid/view/View;

    if-eqz v9, :cond_2

    .line 651719
    iget-object v9, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->l:Landroid/view/View;

    invoke-static {v9, v0, v2, v3}, LX/3vJ;->a(Landroid/view/View;III)I

    move-result v9

    .line 651720
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 651721
    iget v10, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v10

    sub-int v0, v9, v0

    .line 651722
    :cond_2
    iget-object v9, p0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    if-eqz v9, :cond_3

    iget-object v9, p0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v9}, Landroid/support/v7/widget/ActionMenuView;->getParent()Landroid/view/ViewParent;

    move-result-object v9

    if-ne v9, p0, :cond_3

    .line 651723
    iget-object v9, p0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    invoke-static {v9, v0, v2, v3}, LX/3vJ;->a(Landroid/view/View;III)I

    move-result v0

    .line 651724
    :cond_3
    iget-object v9, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->n:Landroid/widget/LinearLayout;

    if-eqz v9, :cond_5

    iget-object v9, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->m:Landroid/view/View;

    if-nez v9, :cond_5

    .line 651725
    iget-boolean v9, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->t:Z

    if-eqz v9, :cond_b

    .line 651726
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    .line 651727
    iget-object v10, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v10, v9, v2}, Landroid/widget/LinearLayout;->measure(II)V

    .line 651728
    iget-object v2, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v9

    .line 651729
    if-gt v9, v0, :cond_9

    const/4 v2, 0x1

    .line 651730
    :goto_1
    if-eqz v2, :cond_4

    .line 651731
    sub-int/2addr v0, v9

    .line 651732
    :cond_4
    iget-object v9, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->n:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_a

    move v2, v3

    :goto_2
    invoke-virtual {v9, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 651733
    :cond_5
    :goto_3
    iget-object v2, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->m:Landroid/view/View;

    if-eqz v2, :cond_7

    .line 651734
    iget-object v2, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->m:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    .line 651735
    iget v2, v9, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-eq v2, v11, :cond_c

    move v2, v4

    .line 651736
    :goto_4
    iget v10, v9, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ltz v10, :cond_6

    iget v10, v9, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v10, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 651737
    :cond_6
    iget v10, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v10, v11, :cond_d

    .line 651738
    :goto_5
    iget v5, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ltz v5, :cond_e

    iget v5, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 651739
    :goto_6
    iget-object v6, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->m:Landroid/view/View;

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v5, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v6, v0, v2}, Landroid/view/View;->measure(II)V

    .line 651740
    :cond_7
    iget v0, p0, LX/3vJ;->h:I

    if-gtz v0, :cond_10

    .line 651741
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getChildCount()I

    move-result v2

    move v1, v3

    .line 651742
    :goto_7
    if-ge v3, v2, :cond_f

    .line 651743
    invoke-virtual {p0, v3}, Landroid/support/v7/internal/widget/ActionBarContextView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 651744
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v8

    .line 651745
    if-le v0, v1, :cond_11

    .line 651746
    :goto_8
    add-int/lit8 v3, v3, 0x1

    move v1, v0

    goto :goto_7

    .line 651747
    :cond_8
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    move v1, v0

    goto/16 :goto_0

    :cond_9
    move v2, v3

    .line 651748
    goto :goto_1

    .line 651749
    :cond_a
    const/16 v2, 0x8

    goto :goto_2

    .line 651750
    :cond_b
    iget-object v9, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->n:Landroid/widget/LinearLayout;

    invoke-static {v9, v0, v2, v3}, LX/3vJ;->a(Landroid/view/View;III)I

    move-result v0

    goto :goto_3

    :cond_c
    move v2, v5

    .line 651751
    goto :goto_4

    :cond_d
    move v4, v5

    .line 651752
    goto :goto_5

    :cond_e
    move v5, v6

    .line 651753
    goto :goto_6

    .line 651754
    :cond_f
    invoke-virtual {p0, v7, v1}, Landroid/support/v7/internal/widget/ActionBarContextView;->setMeasuredDimension(II)V

    .line 651755
    :goto_9
    return-void

    .line 651756
    :cond_10
    invoke-virtual {p0, v7, v1}, Landroid/support/v7/internal/widget/ActionBarContextView;->setMeasuredDimension(II)V

    goto :goto_9

    :cond_11
    move v0, v1

    goto :goto_8
.end method

.method public setContentHeight(I)V
    .locals 0

    .prologue
    .line 651757
    iput p1, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->h:I

    .line 651758
    return-void
.end method

.method public setCustomView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 651759
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->m:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 651760
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->m:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->removeView(Landroid/view/View;)V

    .line 651761
    :cond_0
    iput-object p1, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->m:Landroid/view/View;

    .line 651762
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->n:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 651763
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->removeView(Landroid/view/View;)V

    .line 651764
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->n:Landroid/widget/LinearLayout;

    .line 651765
    :cond_1
    if-eqz p1, :cond_2

    .line 651766
    invoke-virtual {p0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->addView(Landroid/view/View;)V

    .line 651767
    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->requestLayout()V

    .line 651768
    return-void
.end method

.method public setSplitToolbar(Z)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 651769
    iget-boolean v0, p0, LX/3vJ;->f:Z

    if-eq v0, p1, :cond_2

    .line 651770
    iget-object v0, p0, LX/3vJ;->d:LX/3wb;

    if-eqz v0, :cond_1

    .line 651771
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v0, -0x2

    invoke-direct {v1, v0, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 651772
    if-nez p1, :cond_3

    .line 651773
    iget-object v0, p0, LX/3vJ;->d:LX/3wb;

    invoke-virtual {v0, p0}, LX/3ut;->a(Landroid/view/ViewGroup;)LX/3ux;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ActionMenuView;

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->c:Landroid/support/v7/widget/ActionMenuView;

    .line 651774
    iget-object v0, p0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ActionMenuView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 651775
    iget-object v0, p0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 651776
    if-eqz v0, :cond_0

    iget-object v2, p0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 651777
    :cond_0
    iget-object v0, p0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/internal/widget/ActionBarContextView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 651778
    :cond_1
    :goto_0
    invoke-super {p0, p1}, LX/3vJ;->setSplitToolbar(Z)V

    .line 651779
    :cond_2
    return-void

    .line 651780
    :cond_3
    iget-object v0, p0, LX/3vJ;->d:LX/3wb;

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, LX/3wb;->a(IZ)V

    .line 651781
    iget-object v0, p0, LX/3vJ;->d:LX/3wb;

    const v2, 0x7fffffff

    invoke-virtual {v0, v2}, LX/3wb;->b(I)V

    .line 651782
    iput v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 651783
    iget v0, p0, LX/3vJ;->h:I

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 651784
    iget-object v0, p0, LX/3vJ;->d:LX/3wb;

    invoke-virtual {v0, p0}, LX/3ut;->a(Landroid/view/ViewGroup;)LX/3ux;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ActionMenuView;

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->c:Landroid/support/v7/widget/ActionMenuView;

    .line 651785
    iget-object v0, p0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    iget-object v2, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ActionMenuView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 651786
    iget-object v0, p0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 651787
    if-eqz v0, :cond_4

    iget-object v2, p0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 651788
    :cond_4
    iget-object v0, p0, LX/3vJ;->e:Landroid/view/ViewGroup;

    iget-object v2, p0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v0, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 651789
    iput-object p1, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->k:Ljava/lang/CharSequence;

    .line 651790
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->e()V

    .line 651791
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 651792
    iput-object p1, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->j:Ljava/lang/CharSequence;

    .line 651793
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->e()V

    .line 651794
    return-void
.end method

.method public setTitleOptional(Z)V
    .locals 1

    .prologue
    .line 651795
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->t:Z

    if-eq p1, v0, :cond_0

    .line 651796
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActionBarContextView;->requestLayout()V

    .line 651797
    :cond_0
    iput-boolean p1, p0, Landroid/support/v7/internal/widget/ActionBarContextView;->t:Z

    .line 651798
    return-void
.end method

.method public final shouldDelayChildPressedState()Z
    .locals 1

    .prologue
    .line 651799
    const/4 v0, 0x0

    return v0
.end method
