.class public final Landroid/support/v7/internal/widget/ViewStubCompat;
.super Landroid/view/View;
.source ""


# instance fields
.field private a:LX/3wH;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private b:I

.field private c:I

.field private d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public e:Landroid/view/LayoutInflater;

.field private f:LX/3wI;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 654931
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/internal/widget/ViewStubCompat;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 654932
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 654933
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 654934
    iput v2, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->b:I

    .line 654935
    sget-object v0, LX/03r;->ViewStubCompat:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 654936
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->c:I

    .line 654937
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->b:I

    .line 654938
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/v7/internal/widget/ViewStubCompat;->setId(I)V

    .line 654939
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 654940
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ViewStubCompat;->setVisibility(I)V

    .line 654941
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ViewStubCompat;->setWillNotDraw(Z)V

    .line 654942
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 4

    .prologue
    .line 654943
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ViewStubCompat;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 654944
    if-eqz v0, :cond_6

    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_6

    .line 654945
    iget v1, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->b:I

    if-nez v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->a:LX/3wH;

    if-eqz v1, :cond_5

    .line 654946
    :cond_0
    check-cast v0, Landroid/view/ViewGroup;

    .line 654947
    iget-object v1, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->e:Landroid/view/LayoutInflater;

    if-eqz v1, :cond_2

    .line 654948
    iget-object v1, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->e:Landroid/view/LayoutInflater;

    .line 654949
    :goto_0
    iget v2, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->b:I

    if-eqz v2, :cond_3

    iget v2, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->b:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 654950
    :goto_1
    iget v2, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->c:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 654951
    iget v2, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->c:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    .line 654952
    :cond_1
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v2

    .line 654953
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    .line 654954
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ViewStubCompat;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 654955
    if-eqz v3, :cond_4

    .line 654956
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 654957
    :goto_2
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->d:Ljava/lang/ref/WeakReference;

    .line 654958
    return-object v1

    .line 654959
    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ViewStubCompat;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    goto :goto_0

    .line 654960
    :cond_3
    iget-object v1, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->a:LX/3wH;

    invoke-interface {v1}, LX/3wH;->a()Landroid/view/View;

    move-result-object v1

    goto :goto_1

    .line 654961
    :cond_4
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    goto :goto_2

    .line 654962
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ViewStub must have a valid layoutResource"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 654963
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ViewStub must have a non-null ViewGroup viewParent"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 654964
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 654965
    return-void
.end method

.method public final getInflatedId()I
    .locals 1

    .prologue
    .line 654966
    iget v0, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->c:I

    return v0
.end method

.method public final getLayoutInflater()Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 654929
    iget-object v0, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->e:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method public final getLayoutResource()I
    .locals 1

    .prologue
    .line 654930
    iget-object v0, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->a:LX/3wH;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->a:LX/3wH;

    invoke-interface {v0}, LX/3wH;->b()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->b:I

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 654927
    invoke-virtual {p0, v0, v0}, Landroid/support/v7/internal/widget/ViewStubCompat;->setMeasuredDimension(II)V

    .line 654928
    return-void
.end method

.method public final setAlternateInflater(LX/3wH;)V
    .locals 1

    .prologue
    .line 654924
    iput-object p1, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->a:LX/3wH;

    .line 654925
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->b:I

    .line 654926
    return-void
.end method

.method public final setInflatedId(I)V
    .locals 0

    .prologue
    .line 654922
    iput p1, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->c:I

    .line 654923
    return-void
.end method

.method public final setLayoutInflater(Landroid/view/LayoutInflater;)V
    .locals 0

    .prologue
    .line 654920
    iput-object p1, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->e:Landroid/view/LayoutInflater;

    .line 654921
    return-void
.end method

.method public final setLayoutResource(I)V
    .locals 1

    .prologue
    .line 654917
    iput p1, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->b:I

    .line 654918
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->a:LX/3wH;

    .line 654919
    return-void
.end method

.method public final setOnInflateListener(LX/3wI;)V
    .locals 0

    .prologue
    .line 654915
    iput-object p1, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->f:LX/3wI;

    .line 654916
    return-void
.end method

.method public final setVisibility(I)V
    .locals 2

    .prologue
    .line 654906
    iget-object v0, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->d:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    .line 654907
    iget-object v0, p0, Landroid/support/v7/internal/widget/ViewStubCompat;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 654908
    if-eqz v0, :cond_1

    .line 654909
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 654910
    :cond_0
    :goto_0
    return-void

    .line 654911
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setVisibility called on un-referenced view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 654912
    :cond_2
    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 654913
    if-eqz p1, :cond_3

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 654914
    :cond_3
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ViewStubCompat;->a()Landroid/view/View;

    goto :goto_0
.end method
