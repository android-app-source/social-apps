.class public Landroid/support/v7/internal/widget/FitWindowsLinearLayout;
.super Landroid/widget/LinearLayout;
.source ""

# interfaces
.implements LX/3vl;


# instance fields
.field private a:LX/3uC;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 653455
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 653456
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 653462
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 653463
    return-void
.end method


# virtual methods
.method public final fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 1

    .prologue
    .line 653459
    iget-object v0, p0, Landroid/support/v7/internal/widget/FitWindowsLinearLayout;->a:LX/3uC;

    if-eqz v0, :cond_0

    .line 653460
    iget-object v0, p0, Landroid/support/v7/internal/widget/FitWindowsLinearLayout;->a:LX/3uC;

    invoke-interface {v0, p1}, LX/3uC;->a(Landroid/graphics/Rect;)V

    .line 653461
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->fitSystemWindows(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public setOnFitSystemWindowsListener(LX/3uC;)V
    .locals 0

    .prologue
    .line 653457
    iput-object p1, p0, Landroid/support/v7/internal/widget/FitWindowsLinearLayout;->a:LX/3uC;

    .line 653458
    return-void
.end method
