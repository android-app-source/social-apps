.class public Landroid/support/v7/internal/widget/FitWindowsFrameLayout;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements LX/3vl;


# instance fields
.field private a:LX/3uC;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 653446
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 653447
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 653448
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 653449
    return-void
.end method


# virtual methods
.method public final fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 1

    .prologue
    .line 653450
    iget-object v0, p0, Landroid/support/v7/internal/widget/FitWindowsFrameLayout;->a:LX/3uC;

    if-eqz v0, :cond_0

    .line 653451
    iget-object v0, p0, Landroid/support/v7/internal/widget/FitWindowsFrameLayout;->a:LX/3uC;

    invoke-interface {v0, p1}, LX/3uC;->a(Landroid/graphics/Rect;)V

    .line 653452
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->fitSystemWindows(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public setOnFitSystemWindowsListener(LX/3uC;)V
    .locals 0

    .prologue
    .line 653453
    iput-object p1, p0, Landroid/support/v7/internal/widget/FitWindowsFrameLayout;->a:LX/3uC;

    .line 653454
    return-void
.end method
