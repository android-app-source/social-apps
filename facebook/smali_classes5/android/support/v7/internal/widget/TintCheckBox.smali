.class public Landroid/support/v7/internal/widget/TintCheckBox;
.super Landroid/widget/CheckBox;
.source ""


# static fields
.field private static final a:[I


# instance fields
.field private b:LX/3wA;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 654409
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x1010107

    aput v2, v0, v1

    sput-object v0, Landroid/support/v7/internal/widget/TintCheckBox;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 654410
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/internal/widget/TintCheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 654411
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 654412
    const v0, 0x101006c

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/internal/widget/TintCheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 654413
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 654414
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 654415
    sget-boolean v0, LX/3wA;->a:Z

    if-eqz v0, :cond_0

    .line 654416
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/TintCheckBox;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Landroid/support/v7/internal/widget/TintCheckBox;->a:[I

    invoke-static {v0, p2, v1, p3, v2}, LX/3wC;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)LX/3wC;

    move-result-object v0

    .line 654417
    invoke-virtual {v0, v2}, LX/3wC;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/v7/internal/widget/TintCheckBox;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 654418
    invoke-virtual {v0}, LX/3wC;->b()V

    .line 654419
    invoke-virtual {v0}, LX/3wC;->c()LX/3wA;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/widget/TintCheckBox;->b:LX/3wA;

    .line 654420
    :cond_0
    return-void
.end method


# virtual methods
.method public setButtonDrawable(I)V
    .locals 1

    .prologue
    .line 654421
    iget-object v0, p0, Landroid/support/v7/internal/widget/TintCheckBox;->b:LX/3wA;

    if-eqz v0, :cond_0

    .line 654422
    iget-object v0, p0, Landroid/support/v7/internal/widget/TintCheckBox;->b:LX/3wA;

    invoke-virtual {v0, p1}, LX/3wA;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/TintCheckBox;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 654423
    :goto_0
    return-void

    .line 654424
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/CheckBox;->setButtonDrawable(I)V

    goto :goto_0
.end method
