.class public final Landroid/support/v7/internal/widget/ActionBarOverlayLayout$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;


# direct methods
.method public constructor <init>(Landroid/support/v7/internal/widget/ActionBarOverlayLayout;)V
    .locals 0

    .prologue
    .line 651952
    iput-object p1, p0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout$4;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 651953
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout$4;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {v0}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->k(Landroid/support/v7/internal/widget/ActionBarOverlayLayout;)V

    .line 651954
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout$4;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout$4;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v1, v1, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->f:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v1}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout$4;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v2, v2, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->f:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, LX/3sU;->c(F)LX/3sU;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout$4;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v2, v2, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->A:LX/3oQ;

    invoke-virtual {v1, v2}, LX/3sU;->a(LX/3oQ;)LX/3sU;

    move-result-object v1

    .line 651955
    iput-object v1, v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->y:LX/3sU;

    .line 651956
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout$4;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v0, v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout$4;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v0, v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 651957
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout$4;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout$4;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v1, v1, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v1}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout$4;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v2, v2, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, LX/3sU;->c(F)LX/3sU;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout$4;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v2, v2, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->B:LX/3oQ;

    invoke-virtual {v1, v2}, LX/3sU;->a(LX/3oQ;)LX/3sU;

    move-result-object v1

    .line 651958
    iput-object v1, v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->z:LX/3sU;

    .line 651959
    :cond_0
    return-void
.end method
