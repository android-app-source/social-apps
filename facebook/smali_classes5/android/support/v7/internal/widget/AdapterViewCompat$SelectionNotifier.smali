.class public final Landroid/support/v7/internal/widget/AdapterViewCompat$SelectionNotifier;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/3vM;


# direct methods
.method public constructor <init>(LX/3vM;)V
    .locals 0

    .prologue
    .line 653399
    iput-object p1, p0, Landroid/support/v7/internal/widget/AdapterViewCompat$SelectionNotifier;->a:LX/3vM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 1

    .prologue
    .line 653400
    iget-object v0, p0, Landroid/support/v7/internal/widget/AdapterViewCompat$SelectionNotifier;->a:LX/3vM;

    iget-boolean v0, v0, LX/3vM;->u:Z

    if-eqz v0, :cond_1

    .line 653401
    iget-object v0, p0, Landroid/support/v7/internal/widget/AdapterViewCompat$SelectionNotifier;->a:LX/3vM;

    invoke-virtual {v0}, LX/3vM;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 653402
    iget-object v0, p0, Landroid/support/v7/internal/widget/AdapterViewCompat$SelectionNotifier;->a:LX/3vM;

    invoke-virtual {v0, p0}, LX/3vM;->post(Ljava/lang/Runnable;)Z

    .line 653403
    :cond_0
    :goto_0
    return-void

    .line 653404
    :cond_1
    iget-object v0, p0, Landroid/support/v7/internal/widget/AdapterViewCompat$SelectionNotifier;->a:LX/3vM;

    .line 653405
    invoke-static {v0}, LX/3vM;->c(LX/3vM;)V

    .line 653406
    goto :goto_0
.end method
