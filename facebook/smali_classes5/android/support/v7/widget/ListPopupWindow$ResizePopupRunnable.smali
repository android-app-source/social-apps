.class public final Landroid/support/v7/widget/ListPopupWindow$ResizePopupRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/3w1;


# direct methods
.method public constructor <init>(LX/3w1;)V
    .locals 0

    .prologue
    .line 656693
    iput-object p1, p0, Landroid/support/v7/widget/ListPopupWindow$ResizePopupRunnable;->a:LX/3w1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 656694
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow$ResizePopupRunnable;->a:LX/3w1;

    iget-object v0, v0, LX/3w1;->f:LX/3wy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow$ResizePopupRunnable;->a:LX/3w1;

    iget-object v0, v0, LX/3w1;->f:LX/3wy;

    invoke-virtual {v0}, LX/3wy;->getCount()I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow$ResizePopupRunnable;->a:LX/3w1;

    iget-object v1, v1, LX/3w1;->f:LX/3wy;

    invoke-virtual {v1}, LX/3wy;->getChildCount()I

    move-result v1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow$ResizePopupRunnable;->a:LX/3w1;

    iget-object v0, v0, LX/3w1;->f:LX/3wy;

    invoke-virtual {v0}, LX/3wy;->getChildCount()I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow$ResizePopupRunnable;->a:LX/3w1;

    iget v1, v1, LX/3w1;->b:I

    if-gt v0, v1, :cond_0

    .line 656695
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow$ResizePopupRunnable;->a:LX/3w1;

    iget-object v0, v0, LX/3w1;->d:Landroid/widget/PopupWindow;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 656696
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow$ResizePopupRunnable;->a:LX/3w1;

    invoke-virtual {v0}, LX/3w1;->c()V

    .line 656697
    :cond_0
    return-void
.end method
