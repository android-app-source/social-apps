.class public Landroid/support/v7/widget/ActionMenuView;
.super Landroid/support/v7/widget/LinearLayoutCompat;
.source ""

# interfaces
.implements LX/3uw;
.implements LX/3ux;


# instance fields
.field public a:LX/3v0;

.field private b:Landroid/content/Context;

.field private c:Landroid/content/Context;

.field private d:I

.field public e:Z

.field private f:LX/3wb;

.field private g:LX/3uE;

.field public h:LX/3u7;

.field private i:Z

.field private j:I

.field private k:I

.field private l:I

.field public m:LX/3wg;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 656000
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/ActionMenuView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 656001
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 655961
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/LinearLayoutCompat;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 655962
    iput-object p1, p0, Landroid/support/v7/widget/ActionMenuView;->b:Landroid/content/Context;

    .line 655963
    iput-boolean v2, p0, Landroid/support/v7/widget/LinearLayoutCompat;->mBaselineAligned:Z

    .line 655964
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 655965
    const/high16 v1, 0x42600000    # 56.0f

    mul-float/2addr v1, v0

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v7/widget/ActionMenuView;->k:I

    .line 655966
    const/high16 v1, 0x40800000    # 4.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/ActionMenuView;->l:I

    .line 655967
    iput-object p1, p0, Landroid/support/v7/widget/ActionMenuView;->c:Landroid/content/Context;

    .line 655968
    iput v2, p0, Landroid/support/v7/widget/ActionMenuView;->d:I

    .line 655969
    return-void
.end method

.method public static a(Landroid/view/View;IIII)I
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 655971
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3we;

    .line 655972
    invoke-static {p3}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    sub-int/2addr v1, p4

    .line 655973
    invoke-static {p3}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .line 655974
    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 655975
    instance-of v1, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    if-eqz v1, :cond_4

    move-object v1, p0

    check-cast v1, Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    .line 655976
    :goto_0
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    move v5, v4

    .line 655977
    :goto_1
    if-lez p2, :cond_6

    if-eqz v5, :cond_0

    if-lt p2, v3, :cond_6

    .line 655978
    :cond_0
    mul-int v1, p1, p2

    const/high16 v7, -0x80000000

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 655979
    invoke-virtual {p0, v1, v6}, Landroid/view/View;->measure(II)V

    .line 655980
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    .line 655981
    div-int v1, v7, p1

    .line 655982
    rem-int/2addr v7, p1

    if-eqz v7, :cond_1

    add-int/lit8 v1, v1, 0x1

    .line 655983
    :cond_1
    if-eqz v5, :cond_2

    if-ge v1, v3, :cond_2

    move v1, v3

    .line 655984
    :cond_2
    :goto_2
    iget-boolean v3, v0, LX/3we;->a:Z

    if-nez v3, :cond_3

    if-eqz v5, :cond_3

    move v2, v4

    .line 655985
    :cond_3
    iput-boolean v2, v0, LX/3we;->d:Z

    .line 655986
    iput v1, v0, LX/3we;->b:I

    .line 655987
    mul-int v0, v1, p1

    .line 655988
    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p0, v0, v6}, Landroid/view/View;->measure(II)V

    .line 655989
    return v1

    .line 655990
    :cond_4
    const/4 v1, 0x0

    goto :goto_0

    :cond_5
    move v5, v2

    .line 655991
    goto :goto_1

    :cond_6
    move v1, v2

    goto :goto_2
.end method

.method private a(Landroid/util/AttributeSet;)LX/3we;
    .locals 2

    .prologue
    .line 655992
    new-instance v0, LX/3we;

    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/3we;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public static final a(Landroid/view/ViewGroup$LayoutParams;)LX/3we;
    .locals 2

    .prologue
    .line 655993
    if-eqz p0, :cond_2

    .line 655994
    instance-of v0, p0, LX/3we;

    if-eqz v0, :cond_1

    new-instance v0, LX/3we;

    check-cast p0, LX/3we;

    invoke-direct {v0, p0}, LX/3we;-><init>(LX/3we;)V

    .line 655995
    :goto_0
    iget v1, v0, LX/3wd;->h:I

    if-gtz v1, :cond_0

    .line 655996
    const/16 v1, 0x10

    iput v1, v0, LX/3we;->h:I

    .line 655997
    :cond_0
    :goto_1
    return-object v0

    .line 655998
    :cond_1
    new-instance v0, LX/3we;

    invoke-direct {v0, p0}, LX/3we;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 655999
    :cond_2
    invoke-static {}, Landroid/support/v7/widget/ActionMenuView;->i()LX/3we;

    move-result-object v0

    goto :goto_1
.end method

.method private a(II)V
    .locals 34

    .prologue
    .line 655837
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v23

    .line 655838
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 655839
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v17

    .line 655840
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingLeft()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingRight()I

    move-result v8

    add-int/2addr v7, v8

    .line 655841
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingTop()I

    move-result v8

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingBottom()I

    move-result v9

    add-int v19, v8, v9

    .line 655842
    const/4 v8, -0x2

    move/from16 v0, p2

    move/from16 v1, v19

    invoke-static {v0, v1, v8}, Landroid/support/v7/widget/ActionMenuView;->getChildMeasureSpec(III)I

    move-result v24

    .line 655843
    sub-int v25, v6, v7

    .line 655844
    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/ActionMenuView;->k:I

    div-int v9, v25, v6

    .line 655845
    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/ActionMenuView;->k:I

    rem-int v6, v25, v6

    .line 655846
    if-nez v9, :cond_0

    .line 655847
    const/4 v6, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1, v6}, Landroid/support/v7/widget/ActionMenuView;->setMeasuredDimension(II)V

    .line 655848
    :goto_0
    return-void

    .line 655849
    :cond_0
    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/v7/widget/ActionMenuView;->k:I

    div-int/2addr v6, v9

    add-int v26, v7, v6

    .line 655850
    const/16 v16, 0x0

    .line 655851
    const/4 v15, 0x0

    .line 655852
    const/4 v10, 0x0

    .line 655853
    const/4 v7, 0x0

    .line 655854
    const/4 v11, 0x0

    .line 655855
    const-wide/16 v12, 0x0

    .line 655856
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getChildCount()I

    move-result v27

    .line 655857
    const/4 v6, 0x0

    move/from16 v18, v6

    :goto_1
    move/from16 v0, v18

    move/from16 v1, v27

    if-ge v0, v1, :cond_4

    .line 655858
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 655859
    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v6

    const/16 v14, 0x8

    if-eq v6, v14, :cond_20

    .line 655860
    instance-of v0, v8, Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    move/from16 v20, v0

    .line 655861
    add-int/lit8 v14, v7, 0x1

    .line 655862
    if-eqz v20, :cond_1

    .line 655863
    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/ActionMenuView;->l:I

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v7/widget/ActionMenuView;->l:I

    move/from16 v21, v0

    const/16 v22, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v8, v6, v7, v0, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 655864
    :cond_1
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, LX/3we;

    .line 655865
    const/4 v7, 0x0

    iput-boolean v7, v6, LX/3we;->f:Z

    .line 655866
    const/4 v7, 0x0

    iput v7, v6, LX/3we;->c:I

    .line 655867
    const/4 v7, 0x0

    iput v7, v6, LX/3we;->b:I

    .line 655868
    const/4 v7, 0x0

    iput-boolean v7, v6, LX/3we;->d:Z

    .line 655869
    const/4 v7, 0x0

    iput v7, v6, LX/3we;->leftMargin:I

    .line 655870
    const/4 v7, 0x0

    iput v7, v6, LX/3we;->rightMargin:I

    .line 655871
    if-eqz v20, :cond_2

    move-object v7, v8

    check-cast v7, Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    invoke-virtual {v7}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->b()Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v7, 0x1

    :goto_2
    iput-boolean v7, v6, LX/3we;->e:Z

    .line 655872
    iget-boolean v7, v6, LX/3we;->a:Z

    if-eqz v7, :cond_3

    const/4 v7, 0x1

    .line 655873
    :goto_3
    move/from16 v0, v26

    move/from16 v1, v24

    move/from16 v2, v19

    invoke-static {v8, v0, v7, v1, v2}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/view/View;IIII)I

    move-result v20

    .line 655874
    move/from16 v0, v20

    invoke-static {v15, v0}, Ljava/lang/Math;->max(II)I

    move-result v15

    .line 655875
    iget-boolean v7, v6, LX/3we;->d:Z

    if-eqz v7, :cond_1f

    add-int/lit8 v7, v10, 0x1

    .line 655876
    :goto_4
    iget-boolean v6, v6, LX/3we;->a:Z

    if-eqz v6, :cond_1e

    const/4 v6, 0x1

    .line 655877
    :goto_5
    sub-int v11, v9, v20

    .line 655878
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    move/from16 v0, v16

    invoke-static {v0, v8}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 655879
    const/4 v8, 0x1

    move/from16 v0, v20

    if-ne v0, v8, :cond_1d

    const/4 v8, 0x1

    shl-int v8, v8, v18

    int-to-long v8, v8

    or-long/2addr v8, v12

    move v12, v10

    move v13, v11

    move v10, v7

    move v11, v6

    move-wide v6, v8

    move v9, v15

    move v8, v14

    .line 655880
    :goto_6
    add-int/lit8 v14, v18, 0x1

    move/from16 v18, v14

    move v15, v9

    move/from16 v16, v12

    move v9, v13

    move-wide v12, v6

    move v7, v8

    goto/16 :goto_1

    .line 655881
    :cond_2
    const/4 v7, 0x0

    goto :goto_2

    :cond_3
    move v7, v9

    .line 655882
    goto :goto_3

    .line 655883
    :cond_4
    if-eqz v11, :cond_5

    const/4 v6, 0x2

    if-ne v7, v6, :cond_5

    const/4 v6, 0x1

    move v8, v6

    .line 655884
    :goto_7
    const/16 v18, 0x0

    move-wide/from16 v20, v12

    move/from16 v19, v9

    .line 655885
    :goto_8
    if-lez v10, :cond_b

    if-lez v19, :cond_b

    .line 655886
    const v14, 0x7fffffff

    .line 655887
    const-wide/16 v12, 0x0

    .line 655888
    const/4 v9, 0x0

    .line 655889
    const/4 v6, 0x0

    move/from16 v22, v6

    :goto_9
    move/from16 v0, v22

    move/from16 v1, v27

    if-ge v0, v1, :cond_7

    .line 655890
    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 655891
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, LX/3we;

    .line 655892
    iget-boolean v0, v6, LX/3we;->d:Z

    move/from16 v28, v0

    if-eqz v28, :cond_1c

    .line 655893
    iget v0, v6, LX/3we;->b:I

    move/from16 v28, v0

    move/from16 v0, v28

    if-ge v0, v14, :cond_6

    .line 655894
    iget v9, v6, LX/3we;->b:I

    .line 655895
    const/4 v6, 0x1

    shl-int v6, v6, v22

    int-to-long v12, v6

    .line 655896
    const/4 v6, 0x1

    .line 655897
    :goto_a
    add-int/lit8 v14, v22, 0x1

    move/from16 v22, v14

    move v14, v9

    move v9, v6

    goto :goto_9

    .line 655898
    :cond_5
    const/4 v6, 0x0

    move v8, v6

    goto :goto_7

    .line 655899
    :cond_6
    iget v6, v6, LX/3we;->b:I

    if-ne v6, v14, :cond_1c

    .line 655900
    const/4 v6, 0x1

    shl-int v6, v6, v22

    int-to-long v0, v6

    move-wide/from16 v28, v0

    or-long v12, v12, v28

    .line 655901
    add-int/lit8 v6, v9, 0x1

    move v9, v14

    goto :goto_a

    .line 655902
    :cond_7
    or-long v20, v20, v12

    .line 655903
    move/from16 v0, v19

    if-gt v9, v0, :cond_b

    .line 655904
    add-int/lit8 v22, v14, 0x1

    .line 655905
    const/4 v6, 0x0

    move v14, v6

    move/from16 v9, v19

    move-wide/from16 v18, v20

    :goto_b
    move/from16 v0, v27

    if-ge v14, v0, :cond_a

    .line 655906
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v20

    .line 655907
    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, LX/3we;

    .line 655908
    const/16 v21, 0x1

    shl-int v21, v21, v14

    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v28, v0

    and-long v28, v28, v12

    const-wide/16 v30, 0x0

    cmp-long v21, v28, v30

    if-nez v21, :cond_8

    .line 655909
    iget v6, v6, LX/3we;->b:I

    move/from16 v0, v22

    if-ne v6, v0, :cond_1b

    const/4 v6, 0x1

    shl-int/2addr v6, v14

    int-to-long v0, v6

    move-wide/from16 v20, v0

    or-long v18, v18, v20

    move v6, v9

    .line 655910
    :goto_c
    add-int/lit8 v9, v14, 0x1

    move v14, v9

    move v9, v6

    goto :goto_b

    .line 655911
    :cond_8
    if-eqz v8, :cond_9

    iget-boolean v0, v6, LX/3we;->e:Z

    move/from16 v21, v0

    if-eqz v21, :cond_9

    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v9, v0, :cond_9

    .line 655912
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v7/widget/ActionMenuView;->l:I

    move/from16 v21, v0

    add-int v21, v21, v26

    const/16 v28, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v7/widget/ActionMenuView;->l:I

    move/from16 v29, v0

    const/16 v30, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v28

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 655913
    :cond_9
    iget v0, v6, LX/3we;->b:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    iput v0, v6, LX/3we;->b:I

    .line 655914
    const/16 v20, 0x1

    move/from16 v0, v20

    iput-boolean v0, v6, LX/3we;->f:Z

    .line 655915
    add-int/lit8 v6, v9, -0x1

    goto :goto_c

    .line 655916
    :cond_a
    const/4 v6, 0x1

    move-wide/from16 v20, v18

    move/from16 v18, v6

    move/from16 v19, v9

    .line 655917
    goto/16 :goto_8

    :cond_b
    move-wide/from16 v12, v20

    .line 655918
    if-nez v11, :cond_f

    const/4 v6, 0x1

    if-ne v7, v6, :cond_f

    const/4 v6, 0x1

    .line 655919
    :goto_d
    if-lez v19, :cond_15

    const-wide/16 v8, 0x0

    cmp-long v8, v12, v8

    if-eqz v8, :cond_15

    add-int/lit8 v7, v7, -0x1

    move/from16 v0, v19

    if-lt v0, v7, :cond_c

    if-nez v6, :cond_c

    const/4 v7, 0x1

    if-le v15, v7, :cond_15

    .line 655920
    :cond_c
    invoke-static {v12, v13}, Ljava/lang/Long;->bitCount(J)I

    move-result v7

    int-to-float v7, v7

    .line 655921
    if-nez v6, :cond_1a

    .line 655922
    const-wide/16 v8, 0x1

    and-long/2addr v8, v12

    const-wide/16 v10, 0x0

    cmp-long v6, v8, v10

    if-eqz v6, :cond_d

    .line 655923
    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, LX/3we;

    .line 655924
    iget-boolean v6, v6, LX/3we;->e:Z

    if-nez v6, :cond_d

    const/high16 v6, 0x3f000000    # 0.5f

    sub-float/2addr v7, v6

    .line 655925
    :cond_d
    const/4 v6, 0x1

    add-int/lit8 v8, v27, -0x1

    shl-int/2addr v6, v8

    int-to-long v8, v6

    and-long/2addr v8, v12

    const-wide/16 v10, 0x0

    cmp-long v6, v8, v10

    if-eqz v6, :cond_1a

    .line 655926
    add-int/lit8 v6, v27, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, LX/3we;

    .line 655927
    iget-boolean v6, v6, LX/3we;->e:Z

    if-nez v6, :cond_1a

    const/high16 v6, 0x3f000000    # 0.5f

    sub-float v6, v7, v6

    .line 655928
    :goto_e
    const/4 v7, 0x0

    cmpl-float v7, v6, v7

    if-lez v7, :cond_10

    mul-int v7, v19, v26

    int-to-float v7, v7

    div-float v6, v7, v6

    float-to-int v6, v6

    move v7, v6

    .line 655929
    :goto_f
    const/4 v6, 0x0

    move v9, v6

    move/from16 v8, v18

    :goto_10
    move/from16 v0, v27

    if-ge v9, v0, :cond_16

    .line 655930
    const/4 v6, 0x1

    shl-int/2addr v6, v9

    int-to-long v10, v6

    and-long/2addr v10, v12

    const-wide/16 v14, 0x0

    cmp-long v6, v10, v14

    if-eqz v6, :cond_14

    .line 655931
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 655932
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, LX/3we;

    .line 655933
    instance-of v10, v10, Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    if-eqz v10, :cond_11

    .line 655934
    iput v7, v6, LX/3we;->c:I

    .line 655935
    const/4 v8, 0x1

    iput-boolean v8, v6, LX/3we;->f:Z

    .line 655936
    if-nez v9, :cond_e

    iget-boolean v8, v6, LX/3we;->e:Z

    if-nez v8, :cond_e

    .line 655937
    neg-int v8, v7

    div-int/lit8 v8, v8, 0x2

    iput v8, v6, LX/3we;->leftMargin:I

    .line 655938
    :cond_e
    const/4 v6, 0x1

    .line 655939
    :goto_11
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    move v8, v6

    goto :goto_10

    .line 655940
    :cond_f
    const/4 v6, 0x0

    goto/16 :goto_d

    .line 655941
    :cond_10
    const/4 v6, 0x0

    move v7, v6

    goto :goto_f

    .line 655942
    :cond_11
    iget-boolean v10, v6, LX/3we;->a:Z

    if-eqz v10, :cond_12

    .line 655943
    iput v7, v6, LX/3we;->c:I

    .line 655944
    const/4 v8, 0x1

    iput-boolean v8, v6, LX/3we;->f:Z

    .line 655945
    neg-int v8, v7

    div-int/lit8 v8, v8, 0x2

    iput v8, v6, LX/3we;->rightMargin:I

    .line 655946
    const/4 v6, 0x1

    goto :goto_11

    .line 655947
    :cond_12
    if-eqz v9, :cond_13

    .line 655948
    div-int/lit8 v10, v7, 0x2

    iput v10, v6, LX/3we;->leftMargin:I

    .line 655949
    :cond_13
    add-int/lit8 v10, v27, -0x1

    if-eq v9, v10, :cond_14

    .line 655950
    div-int/lit8 v10, v7, 0x2

    iput v10, v6, LX/3we;->rightMargin:I

    :cond_14
    move v6, v8

    goto :goto_11

    :cond_15
    move/from16 v8, v18

    .line 655951
    :cond_16
    if-eqz v8, :cond_18

    .line 655952
    const/4 v6, 0x0

    move v7, v6

    :goto_12
    move/from16 v0, v27

    if-ge v7, v0, :cond_18

    .line 655953
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 655954
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, LX/3we;

    .line 655955
    iget-boolean v9, v6, LX/3we;->f:Z

    if-eqz v9, :cond_17

    .line 655956
    iget v9, v6, LX/3we;->b:I

    mul-int v9, v9, v26

    iget v6, v6, LX/3we;->c:I

    add-int/2addr v6, v9

    .line 655957
    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v6, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    move/from16 v0, v24

    invoke-virtual {v8, v6, v0}, Landroid/view/View;->measure(II)V

    .line 655958
    :cond_17
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_12

    .line 655959
    :cond_18
    const/high16 v6, 0x40000000    # 2.0f

    move/from16 v0, v23

    if-eq v0, v6, :cond_19

    .line 655960
    :goto_13
    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/ActionMenuView;->setMeasuredDimension(II)V

    goto/16 :goto_0

    :cond_19
    move/from16 v16, v17

    goto :goto_13

    :cond_1a
    move v6, v7

    goto/16 :goto_e

    :cond_1b
    move v6, v9

    goto/16 :goto_c

    :cond_1c
    move v6, v9

    move v9, v14

    goto/16 :goto_a

    :cond_1d
    move v8, v14

    move v9, v15

    move-wide/from16 v32, v12

    move v12, v10

    move v13, v11

    move v11, v6

    move v10, v7

    move-wide/from16 v6, v32

    goto/16 :goto_6

    :cond_1e
    move v6, v11

    goto/16 :goto_5

    :cond_1f
    move v7, v10

    goto/16 :goto_4

    :cond_20
    move v8, v7

    move-wide v6, v12

    move/from16 v12, v16

    move v13, v9

    move v9, v15

    goto/16 :goto_6
.end method

.method private a(I)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 656002
    if-nez p1, :cond_0

    move v0, v2

    .line 656003
    :goto_0
    return v0

    .line 656004
    :cond_0
    add-int/lit8 v0, p1, -0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 656005
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 656006
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getChildCount()I

    move-result v3

    if-ge p1, v3, :cond_1

    instance-of v3, v0, LX/3ur;

    if-eqz v3, :cond_1

    .line 656007
    check-cast v0, LX/3ur;

    invoke-interface {v0}, LX/3ur;->d()Z

    move-result v0

    or-int/lit8 v2, v0, 0x0

    .line 656008
    :cond_1
    if-lez p1, :cond_2

    instance-of v0, v1, LX/3ur;

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 656009
    check-cast v0, LX/3ur;

    invoke-interface {v0}, LX/3ur;->c()Z

    move-result v0

    or-int/2addr v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public static final b()LX/3we;
    .locals 2

    .prologue
    .line 656010
    invoke-static {}, Landroid/support/v7/widget/ActionMenuView;->i()LX/3we;

    move-result-object v0

    .line 656011
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/3we;->a:Z

    .line 656012
    return-object v0
.end method

.method private static i()LX/3we;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 656013
    new-instance v0, LX/3we;

    invoke-direct {v0, v1, v1}, LX/3we;-><init>(II)V

    .line 656014
    const/16 v1, 0x10

    iput v1, v0, LX/3we;->h:I

    .line 656015
    return-object v0
.end method


# virtual methods
.method public final a(LX/3uE;LX/3u7;)V
    .locals 0

    .prologue
    .line 656025
    iput-object p1, p0, Landroid/support/v7/widget/ActionMenuView;->g:LX/3uE;

    .line 656026
    iput-object p2, p0, Landroid/support/v7/widget/ActionMenuView;->h:LX/3u7;

    .line 656027
    return-void
.end method

.method public final a(LX/3v0;)V
    .locals 0

    .prologue
    .line 656016
    iput-object p1, p0, Landroid/support/v7/widget/ActionMenuView;->a:LX/3v0;

    .line 656017
    return-void
.end method

.method public final a(LX/3v3;)Z
    .locals 2

    .prologue
    .line 656029
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->a:LX/3v0;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/3v0;->a(Landroid/view/MenuItem;I)Z

    move-result v0

    return v0
.end method

.method public final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 656024
    if-eqz p1, :cond_0

    instance-of v0, p1, LX/3we;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 656028
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->f:LX/3wb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->f:LX/3wb;

    invoke-virtual {v0}, LX/3wb;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .prologue
    .line 656023
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 656022
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->f:LX/3wb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->f:LX/3wb;

    invoke-virtual {v0}, LX/3wb;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 656021
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->f:LX/3wb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->f:LX/3wb;

    invoke-virtual {v0}, LX/3wb;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 656018
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->f:LX/3wb;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->f:LX/3wb;

    .line 656019
    iget-object p0, v0, LX/3wb;->x:Landroid/support/v7/widget/ActionMenuPresenter$OpenOverflowRunnable;

    if-nez p0, :cond_0

    invoke-virtual {v0}, LX/3wb;->h()Z

    move-result p0

    if-eqz p0, :cond_2

    :cond_0
    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 656020
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final synthetic generateDefaultLayoutParams()LX/3wd;
    .locals 1

    .prologue
    .line 655970
    invoke-static {}, Landroid/support/v7/widget/ActionMenuView;->i()LX/3we;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 655698
    invoke-static {}, Landroid/support/v7/widget/ActionMenuView;->i()LX/3we;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/util/AttributeSet;)LX/3wd;
    .locals 1

    .prologue
    .line 655699
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/util/AttributeSet;)LX/3we;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)LX/3wd;
    .locals 1

    .prologue
    .line 655700
    invoke-static {p1}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/view/ViewGroup$LayoutParams;)LX/3we;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 655701
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/util/AttributeSet;)LX/3we;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 655702
    invoke-static {p1}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/view/ViewGroup$LayoutParams;)LX/3we;

    move-result-object v0

    return-object v0
.end method

.method public getMenu()Landroid/view/Menu;
    .locals 4

    .prologue
    .line 655703
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->a:LX/3v0;

    if-nez v0, :cond_0

    .line 655704
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 655705
    new-instance v1, LX/3v0;

    invoke-direct {v1, v0}, LX/3v0;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Landroid/support/v7/widget/ActionMenuView;->a:LX/3v0;

    .line 655706
    iget-object v1, p0, Landroid/support/v7/widget/ActionMenuView;->a:LX/3v0;

    new-instance v2, LX/3wf;

    invoke-direct {v2, p0}, LX/3wf;-><init>(Landroid/support/v7/widget/ActionMenuView;)V

    invoke-virtual {v1, v2}, LX/3v0;->a(LX/3u7;)V

    .line 655707
    new-instance v1, LX/3wb;

    invoke-direct {v1, v0}, LX/3wb;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Landroid/support/v7/widget/ActionMenuView;->f:LX/3wb;

    .line 655708
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->f:LX/3wb;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/3wb;->c(Z)V

    .line 655709
    iget-object v1, p0, Landroid/support/v7/widget/ActionMenuView;->f:LX/3wb;

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->g:LX/3uE;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->g:LX/3uE;

    .line 655710
    :goto_0
    iput-object v0, v1, LX/3ut;->g:LX/3uE;

    .line 655711
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->a:LX/3v0;

    iget-object v1, p0, Landroid/support/v7/widget/ActionMenuView;->f:LX/3wb;

    iget-object v2, p0, Landroid/support/v7/widget/ActionMenuView;->c:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, LX/3v0;->a(LX/3us;Landroid/content/Context;)V

    .line 655712
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->f:LX/3wb;

    invoke-virtual {v0, p0}, LX/3wb;->a(Landroid/support/v7/widget/ActionMenuView;)V

    .line 655713
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->a:LX/3v0;

    return-object v0

    .line 655714
    :cond_1
    new-instance v0, LX/3wc;

    invoke-direct {v0, p0}, LX/3wc;-><init>(Landroid/support/v7/widget/ActionMenuView;)V

    goto :goto_0
.end method

.method public getPopupTheme()I
    .locals 1

    .prologue
    .line 655715
    iget v0, p0, Landroid/support/v7/widget/ActionMenuView;->d:I

    return v0
.end method

.method public getWindowAnimations()I
    .locals 1

    .prologue
    .line 655716
    const/4 v0, 0x0

    return v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 655717
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->f:LX/3wb;

    if-eqz v0, :cond_0

    .line 655718
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->f:LX/3wb;

    invoke-virtual {v0}, LX/3wb;->f()Z

    .line 655719
    :cond_0
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 655720
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    .line 655721
    invoke-super {p0, p1}, Landroid/support/v7/widget/LinearLayoutCompat;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 655722
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->f:LX/3wb;

    if-eqz v0, :cond_1

    .line 655723
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->f:LX/3wb;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/3ut;->b(Z)V

    .line 655724
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->f:LX/3wb;

    invoke-virtual {v0}, LX/3wb;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 655725
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->f:LX/3wb;

    invoke-virtual {v0}, LX/3wb;->e()Z

    .line 655726
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->f:LX/3wb;

    invoke-virtual {v0}, LX/3wb;->d()Z

    .line 655727
    :cond_1
    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x2bf3e843

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 655728
    invoke-super {p0}, Landroid/support/v7/widget/LinearLayoutCompat;->onDetachedFromWindow()V

    .line 655729
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->h()V

    .line 655730
    const/16 v1, 0x2d

    const v2, 0x6276ae5f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 14

    .prologue
    .line 655731
    iget-boolean v0, p0, Landroid/support/v7/widget/ActionMenuView;->i:Z

    if-nez v0, :cond_1

    .line 655732
    invoke-super/range {p0 .. p5}, Landroid/support/v7/widget/LinearLayoutCompat;->onLayout(ZIIII)V

    .line 655733
    :cond_0
    :goto_0
    return-void

    .line 655734
    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getChildCount()I

    move-result v6

    .line 655735
    sub-int v0, p5, p3

    div-int/lit8 v7, v0, 0x2

    .line 655736
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutCompat;->getDividerWidth()I

    move-result v8

    .line 655737
    const/4 v4, 0x0

    .line 655738
    sub-int v0, p4, p2

    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingLeft()I

    move-result v1

    sub-int v3, v0, v1

    .line 655739
    const/4 v1, 0x0

    .line 655740
    invoke-static {p0}, LX/3wJ;->a(Landroid/view/View;)Z

    move-result v9

    .line 655741
    const/4 v0, 0x0

    move v5, v0

    :goto_1
    if-ge v5, v6, :cond_5

    .line 655742
    invoke-virtual {p0, v5}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 655743
    invoke-virtual {v10}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_c

    .line 655744
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3we;

    .line 655745
    iget-boolean v2, v0, LX/3we;->a:Z

    if-eqz v2, :cond_4

    .line 655746
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 655747
    invoke-direct {p0, v5}, Landroid/support/v7/widget/ActionMenuView;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 655748
    add-int/2addr v1, v8

    .line 655749
    :cond_2
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    .line 655750
    if-eqz v9, :cond_3

    .line 655751
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingLeft()I

    move-result v2

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v2, v0

    .line 655752
    add-int v0, v2, v1

    .line 655753
    :goto_2
    div-int/lit8 v12, v11, 0x2

    sub-int v12, v7, v12

    .line 655754
    add-int/2addr v11, v12

    .line 655755
    invoke-virtual {v10, v2, v12, v0, v11}, Landroid/view/View;->layout(IIII)V

    .line 655756
    sub-int v1, v3, v1

    .line 655757
    const/4 v0, 0x1

    move v2, v4

    .line 655758
    :goto_3
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v4, v2

    move v3, v1

    move v1, v0

    goto :goto_1

    .line 655759
    :cond_3
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingRight()I

    move-result v12

    sub-int/2addr v2, v12

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v0, v2, v0

    .line 655760
    sub-int v2, v0, v1

    goto :goto_2

    .line 655761
    :cond_4
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    iget v10, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v2, v10

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v2

    .line 655762
    sub-int v0, v3, v0

    .line 655763
    invoke-direct {p0, v5}, Landroid/support/v7/widget/ActionMenuView;->a(I)Z

    .line 655764
    add-int/lit8 v2, v4, 0x1

    move v13, v1

    move v1, v0

    move v0, v13

    goto :goto_3

    .line 655765
    :cond_5
    const/4 v0, 0x1

    if-ne v6, v0, :cond_6

    if-nez v1, :cond_6

    .line 655766
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 655767
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 655768
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 655769
    sub-int v3, p4, p2

    div-int/lit8 v3, v3, 0x2

    .line 655770
    div-int/lit8 v4, v1, 0x2

    sub-int/2addr v3, v4

    .line 655771
    div-int/lit8 v4, v2, 0x2

    sub-int v4, v7, v4

    .line 655772
    add-int/2addr v1, v3

    add-int/2addr v2, v4

    invoke-virtual {v0, v3, v4, v1, v2}, Landroid/view/View;->layout(IIII)V

    goto/16 :goto_0

    .line 655773
    :cond_6
    if-eqz v1, :cond_7

    const/4 v0, 0x0

    :goto_4
    sub-int v0, v4, v0

    .line 655774
    const/4 v1, 0x0

    if-lez v0, :cond_8

    div-int v0, v3, v0

    :goto_5
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 655775
    if-eqz v9, :cond_9

    .line 655776
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingRight()I

    move-result v1

    sub-int v1, v0, v1

    .line 655777
    const/4 v0, 0x0

    move v2, v0

    :goto_6
    if-ge v2, v6, :cond_0

    .line 655778
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 655779
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3we;

    .line 655780
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v8, 0x8

    if-eq v5, v8, :cond_b

    iget-boolean v5, v0, LX/3we;->a:Z

    if-nez v5, :cond_b

    .line 655781
    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v1, v5

    .line 655782
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 655783
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    .line 655784
    div-int/lit8 v9, v8, 0x2

    sub-int v9, v7, v9

    .line 655785
    sub-int v10, v1, v5

    add-int/2addr v8, v9

    invoke-virtual {v4, v10, v9, v1, v8}, Landroid/view/View;->layout(IIII)V

    .line 655786
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v0, v5

    add-int/2addr v0, v3

    sub-int v0, v1, v0

    .line 655787
    :goto_7
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_6

    .line 655788
    :cond_7
    const/4 v0, 0x1

    goto :goto_4

    .line 655789
    :cond_8
    const/4 v0, 0x0

    goto :goto_5

    .line 655790
    :cond_9
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingLeft()I

    move-result v1

    .line 655791
    const/4 v0, 0x0

    move v2, v0

    :goto_8
    if-ge v2, v6, :cond_0

    .line 655792
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 655793
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3we;

    .line 655794
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v8, 0x8

    if-eq v5, v8, :cond_a

    iget-boolean v5, v0, LX/3we;->a:Z

    if-nez v5, :cond_a

    .line 655795
    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v1, v5

    .line 655796
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 655797
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    .line 655798
    div-int/lit8 v9, v8, 0x2

    sub-int v9, v7, v9

    .line 655799
    add-int v10, v1, v5

    add-int/2addr v8, v9

    invoke-virtual {v4, v1, v9, v10, v8}, Landroid/view/View;->layout(IIII)V

    .line 655800
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v5

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    .line 655801
    :goto_9
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_8

    :cond_a
    move v0, v1

    goto :goto_9

    :cond_b
    move v0, v1

    goto :goto_7

    :cond_c
    move v0, v1

    move v2, v4

    move v1, v3

    goto/16 :goto_3
.end method

.method public final onMeasure(II)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 655802
    iget-boolean v3, p0, Landroid/support/v7/widget/ActionMenuView;->i:Z

    .line 655803
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v4, 0x40000000    # 2.0f

    if-ne v0, v4, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Landroid/support/v7/widget/ActionMenuView;->i:Z

    .line 655804
    iget-boolean v0, p0, Landroid/support/v7/widget/ActionMenuView;->i:Z

    if-eq v3, v0, :cond_0

    .line 655805
    iput v2, p0, Landroid/support/v7/widget/ActionMenuView;->j:I

    .line 655806
    :cond_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 655807
    iget-boolean v3, p0, Landroid/support/v7/widget/ActionMenuView;->i:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Landroid/support/v7/widget/ActionMenuView;->a:LX/3v0;

    if-eqz v3, :cond_1

    iget v3, p0, Landroid/support/v7/widget/ActionMenuView;->j:I

    if-eq v0, v3, :cond_1

    .line 655808
    iput v0, p0, Landroid/support/v7/widget/ActionMenuView;->j:I

    .line 655809
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->a:LX/3v0;

    invoke-virtual {v0, v1}, LX/3v0;->b(Z)V

    .line 655810
    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getChildCount()I

    move-result v3

    .line 655811
    iget-boolean v0, p0, Landroid/support/v7/widget/ActionMenuView;->i:Z

    if-eqz v0, :cond_3

    if-lez v3, :cond_3

    .line 655812
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/ActionMenuView;->a(II)V

    .line 655813
    :goto_1
    return-void

    :cond_2
    move v0, v2

    .line 655814
    goto :goto_0

    :cond_3
    move v1, v2

    .line 655815
    :goto_2
    if-ge v1, v3, :cond_4

    .line 655816
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 655817
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3we;

    .line 655818
    iput v2, v0, LX/3we;->rightMargin:I

    iput v2, v0, LX/3we;->leftMargin:I

    .line 655819
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 655820
    :cond_4
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/LinearLayoutCompat;->onMeasure(II)V

    goto :goto_1
.end method

.method public setExpandedActionViewsExclusive(Z)V
    .locals 1

    .prologue
    .line 655821
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->f:LX/3wb;

    .line 655822
    iput-boolean p1, v0, LX/3wb;->r:Z

    .line 655823
    return-void
.end method

.method public setOnMenuItemClickListener(LX/3wg;)V
    .locals 0

    .prologue
    .line 655824
    iput-object p1, p0, Landroid/support/v7/widget/ActionMenuView;->m:LX/3wg;

    .line 655825
    return-void
.end method

.method public setOverflowReserved(Z)V
    .locals 0

    .prologue
    .line 655826
    iput-boolean p1, p0, Landroid/support/v7/widget/ActionMenuView;->e:Z

    .line 655827
    return-void
.end method

.method public setPopupTheme(I)V
    .locals 2

    .prologue
    .line 655828
    iget v0, p0, Landroid/support/v7/widget/ActionMenuView;->d:I

    if-eq v0, p1, :cond_0

    .line 655829
    iput p1, p0, Landroid/support/v7/widget/ActionMenuView;->d:I

    .line 655830
    if-nez p1, :cond_1

    .line 655831
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->b:Landroid/content/Context;

    iput-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->c:Landroid/content/Context;

    .line 655832
    :cond_0
    :goto_0
    return-void

    .line 655833
    :cond_1
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Landroid/support/v7/widget/ActionMenuView;->b:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->c:Landroid/content/Context;

    goto :goto_0
.end method

.method public setPresenter(LX/3wb;)V
    .locals 1

    .prologue
    .line 655834
    iput-object p1, p0, Landroid/support/v7/widget/ActionMenuView;->f:LX/3wb;

    .line 655835
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->f:LX/3wb;

    invoke-virtual {v0, p0}, LX/3wb;->a(Landroid/support/v7/widget/ActionMenuView;)V

    .line 655836
    return-void
.end method
