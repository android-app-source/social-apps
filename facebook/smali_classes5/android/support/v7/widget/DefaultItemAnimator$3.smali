.class public final Landroid/support/v7/widget/DefaultItemAnimator$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/ArrayList;

.field public final synthetic b:LX/1Oe;


# direct methods
.method public constructor <init>(LX/1Oe;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 656092
    iput-object p1, p0, Landroid/support/v7/widget/DefaultItemAnimator$3;->b:LX/1Oe;

    iput-object p2, p0, Landroid/support/v7/widget/DefaultItemAnimator$3;->a:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 656093
    iget-object v0, p0, Landroid/support/v7/widget/DefaultItemAnimator$3;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 656094
    iget-object v2, p0, Landroid/support/v7/widget/DefaultItemAnimator$3;->b:LX/1Oe;

    .line 656095
    iget-object v3, v0, LX/1a1;->a:Landroid/view/View;

    .line 656096
    invoke-static {v3}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v3

    .line 656097
    iget-object v4, v2, LX/1Oe;->h:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 656098
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, LX/3sU;->a(F)LX/3sU;

    move-result-object v4

    .line 656099
    iget-wide v7, v2, LX/1Of;->c:J

    move-wide v5, v7

    .line 656100
    invoke-virtual {v4, v5, v6}, LX/3sU;->a(J)LX/3sU;

    move-result-object v4

    new-instance v5, LX/3wl;

    invoke-direct {v5, v2, v0, v3}, LX/3wl;-><init>(LX/1Oe;LX/1a1;LX/3sU;)V

    invoke-virtual {v4, v5}, LX/3sU;->a(LX/3oQ;)LX/3sU;

    move-result-object v3

    invoke-virtual {v3}, LX/3sU;->b()V

    .line 656101
    goto :goto_0

    .line 656102
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/DefaultItemAnimator$3;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 656103
    iget-object v0, p0, Landroid/support/v7/widget/DefaultItemAnimator$3;->b:LX/1Oe;

    iget-object v0, v0, LX/1Oe;->e:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v7/widget/DefaultItemAnimator$3;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 656104
    return-void
.end method
