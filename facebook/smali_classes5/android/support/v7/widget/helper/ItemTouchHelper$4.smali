.class public final Landroid/support/v7/widget/helper/ItemTouchHelper$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/3xe;

.field public final synthetic b:I

.field public final synthetic c:LX/3xm;


# direct methods
.method public constructor <init>(LX/3xm;LX/3xe;I)V
    .locals 0

    .prologue
    .line 659779
    iput-object p1, p0, Landroid/support/v7/widget/helper/ItemTouchHelper$4;->c:LX/3xm;

    iput-object p2, p0, Landroid/support/v7/widget/helper/ItemTouchHelper$4;->a:LX/3xe;

    iput p3, p0, Landroid/support/v7/widget/helper/ItemTouchHelper$4;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 659780
    iget-object v0, p0, Landroid/support/v7/widget/helper/ItemTouchHelper$4;->c:LX/3xm;

    iget-object v0, v0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/helper/ItemTouchHelper$4;->c:LX/3xm;

    iget-object v0, v0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/helper/ItemTouchHelper$4;->a:LX/3xe;

    iget-boolean v0, v0, LX/3xe;->m:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/helper/ItemTouchHelper$4;->a:LX/3xe;

    iget-object v0, v0, LX/3xe;->h:LX/1a1;

    invoke-virtual {v0}, LX/1a1;->e()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 659781
    iget-object v0, p0, Landroid/support/v7/widget/helper/ItemTouchHelper$4;->c:LX/3xm;

    iget-object v0, v0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    .line 659782
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    move-object v0, v1

    .line 659783
    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1Of;->a(LX/3x5;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/helper/ItemTouchHelper$4;->c:LX/3xm;

    const/4 v2, 0x0

    .line 659784
    iget-object v1, v0, LX/3xm;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    .line 659785
    :goto_0
    if-ge v3, v4, :cond_4

    .line 659786
    iget-object v1, v0, LX/3xm;->m:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3xe;

    iget-boolean v1, v1, LX/3xe;->c:Z

    if-nez v1, :cond_3

    .line 659787
    const/4 v1, 0x1

    .line 659788
    :goto_1
    move v0, v1

    .line 659789
    if-nez v0, :cond_2

    .line 659790
    iget-object v0, p0, Landroid/support/v7/widget/helper/ItemTouchHelper$4;->c:LX/3xm;

    iget-object v0, v0, LX/3xm;->j:LX/3xj;

    invoke-virtual {v0}, LX/3xj;->f()V

    .line 659791
    :cond_1
    :goto_2
    return-void

    .line 659792
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/helper/ItemTouchHelper$4;->c:LX/3xm;

    iget-object v0, v0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView;->post(Ljava/lang/Runnable;)Z

    goto :goto_2

    .line 659793
    :cond_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_4
    move v1, v2

    .line 659794
    goto :goto_1
.end method
