.class public final Landroid/support/v7/widget/DefaultItemAnimator$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/ArrayList;

.field public final synthetic b:LX/1Oe;


# direct methods
.method public constructor <init>(LX/1Oe;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 656066
    iput-object p1, p0, Landroid/support/v7/widget/DefaultItemAnimator$2;->b:LX/1Oe;

    iput-object p2, p0, Landroid/support/v7/widget/DefaultItemAnimator$2;->a:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    .line 656067
    iget-object v0, p0, Landroid/support/v7/widget/DefaultItemAnimator$2;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3wp;

    .line 656068
    iget-object v2, p0, Landroid/support/v7/widget/DefaultItemAnimator$2;->b:LX/1Oe;

    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 656069
    iget-object v3, v0, LX/3wp;->a:LX/1a1;

    .line 656070
    if-nez v3, :cond_4

    move-object v3, v4

    .line 656071
    :goto_1
    iget-object v5, v0, LX/3wp;->b:LX/1a1;

    .line 656072
    if-eqz v5, :cond_0

    iget-object v4, v5, LX/1a1;->a:Landroid/view/View;

    .line 656073
    :cond_0
    if-eqz v3, :cond_1

    .line 656074
    invoke-static {v3}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v3

    .line 656075
    iget-wide v9, v2, LX/1Of;->f:J

    move-wide v5, v9

    .line 656076
    invoke-virtual {v3, v5, v6}, LX/3sU;->a(J)LX/3sU;

    move-result-object v3

    .line 656077
    iget-object v5, v2, LX/1Oe;->k:Ljava/util/ArrayList;

    iget-object v6, v0, LX/3wp;->a:LX/1a1;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 656078
    iget v5, v0, LX/3wp;->e:I

    iget v6, v0, LX/3wp;->c:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {v3, v5}, LX/3sU;->b(F)LX/3sU;

    .line 656079
    iget v5, v0, LX/3wp;->f:I

    iget v6, v0, LX/3wp;->d:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {v3, v5}, LX/3sU;->c(F)LX/3sU;

    .line 656080
    invoke-virtual {v3, v7}, LX/3sU;->a(F)LX/3sU;

    move-result-object v5

    new-instance v6, LX/3wn;

    invoke-direct {v6, v2, v0, v3}, LX/3wn;-><init>(LX/1Oe;LX/3wp;LX/3sU;)V

    invoke-virtual {v5, v6}, LX/3sU;->a(LX/3oQ;)LX/3sU;

    move-result-object v3

    invoke-virtual {v3}, LX/3sU;->b()V

    .line 656081
    :cond_1
    if-eqz v4, :cond_2

    .line 656082
    invoke-static {v4}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v3

    .line 656083
    iget-object v5, v2, LX/1Oe;->k:Ljava/util/ArrayList;

    iget-object v6, v0, LX/3wp;->b:LX/1a1;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 656084
    invoke-virtual {v3, v7}, LX/3sU;->b(F)LX/3sU;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/3sU;->c(F)LX/3sU;

    move-result-object v5

    .line 656085
    iget-wide v9, v2, LX/1Of;->f:J

    move-wide v7, v9

    .line 656086
    invoke-virtual {v5, v7, v8}, LX/3sU;->a(J)LX/3sU;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v5, v6}, LX/3sU;->a(F)LX/3sU;

    move-result-object v5

    new-instance v6, LX/3wo;

    invoke-direct {v6, v2, v0, v3, v4}, LX/3wo;-><init>(LX/1Oe;LX/3wp;LX/3sU;Landroid/view/View;)V

    invoke-virtual {v5, v6}, LX/3sU;->a(LX/3oQ;)LX/3sU;

    move-result-object v3

    invoke-virtual {v3}, LX/3sU;->b()V

    .line 656087
    :cond_2
    goto :goto_0

    .line 656088
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/DefaultItemAnimator$2;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 656089
    iget-object v0, p0, Landroid/support/v7/widget/DefaultItemAnimator$2;->b:LX/1Oe;

    iget-object v0, v0, LX/1Oe;->g:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v7/widget/DefaultItemAnimator$2;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 656090
    return-void

    .line 656091
    :cond_4
    iget-object v3, v3, LX/1a1;->a:Landroid/view/View;

    goto :goto_1
.end method
