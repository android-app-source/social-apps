.class public final Landroid/support/v7/widget/ListPopupWindow$ForwardingListener$TriggerLongPress;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/3um;


# direct methods
.method public constructor <init>(LX/3um;)V
    .locals 0

    .prologue
    .line 656655
    iput-object p1, p0, Landroid/support/v7/widget/ListPopupWindow$ForwardingListener$TriggerLongPress;->a:LX/3um;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    .line 656656
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow$ForwardingListener$TriggerLongPress;->a:LX/3um;

    const/4 v6, 0x0

    const/4 v10, 0x1

    .line 656657
    invoke-static {v0}, LX/3um;->d(LX/3um;)V

    .line 656658
    iget-object v9, v0, LX/3um;->d:Landroid/view/View;

    .line 656659
    invoke-virtual {v9}, Landroid/view/View;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v9}, Landroid/view/View;->isLongClickable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 656660
    :cond_0
    :goto_0
    return-void

    .line 656661
    :cond_1
    invoke-virtual {v0}, LX/3um;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 656662
    invoke-virtual {v9}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v10}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 656663
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 656664
    const/4 v5, 0x3

    const/4 v8, 0x0

    move-wide v3, v1

    move v7, v6

    invoke-static/range {v1 .. v8}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v1

    .line 656665
    invoke-virtual {v9, v1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 656666
    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    .line 656667
    iput-boolean v10, v0, LX/3um;->g:Z

    .line 656668
    iput-boolean v10, v0, LX/3um;->h:Z

    goto :goto_0
.end method
