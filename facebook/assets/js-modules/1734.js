__d(function(r,t,n,e){"use strict";function u(r){return g<=r&&r<=v}function i(r){return h.test(r)}function o(r,t){return 1+u(r.charCodeAt(t))}function f(r){if(!i(r))return r.length;for(var t=0,n=0;n<r.length;n+=o(r,n))t++;return t}function s(r,t,n){if(t=t||0,n=void 0===n?1/0:n||0,!i(r))return r.substr(t,n);var e=r.length;if(e<=0||t>e||n<=0)return"";var u=0;if(t>0){for(;t>0&&u<e;t--)u+=o(r,u);if(u>=e)return""}else if(t<0){for(u=e;t<0&&0<u;t++)u-=o(r,u-1);u<0&&(u=0)}var f=e;if(n<e)for(f=u;n>0&&f<e;n--)f+=o(r,f);return r.substring(u,f)}function a(r,t,n){t=t||0,n=void 0===n?1/0:n||0,t<0&&(t=0),n<0&&(n=0);var e=Math.abs(n-t);return t=t<n?t:n,s(r,t,e)}function c(r){for(var n=[],e=0;e<r.length;e+=o(r,e)){var u=t(1735).codePointAt.call(r,e);n.push(u)}return n}var g=55296,v=57343,h=/[\uD800-\uDFFF]/,l={isCodeUnitInSurrogateRange:u,getUTF16Length:o,strlen:f,substr:s,substring:a,getCodePoints:c};n.exports=l},1734);