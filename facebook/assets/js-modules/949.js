__d(function(t,o,n,r){"use strict";function e(t,o,n){for(var r=arguments.length>3&&void 0!==arguments[3]?arguments[3]:0,e=2*Math.PI/n,i=[],s=void 0,a=0;a<n;a++)s=a*e+r,i.push({x:t.x+o*Math.cos(s),y:t.y+o*Math.sin(s)});return i}n.exports={createPolygon:e}},949);