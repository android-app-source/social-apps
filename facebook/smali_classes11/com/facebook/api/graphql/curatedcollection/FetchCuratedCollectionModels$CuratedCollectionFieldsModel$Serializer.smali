.class public final Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2350729
    const-class v0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;

    new-instance v1, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2350730
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2350731
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2350732
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2350733
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2350734
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2350735
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2350736
    if-eqz v2, :cond_0

    .line 2350737
    const-string p0, "collection_stories"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2350738
    invoke-static {v1, v2, p1, p2}, LX/GQr;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2350739
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2350740
    if-eqz v2, :cond_1

    .line 2350741
    const-string p0, "collection_title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2350742
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2350743
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2350744
    if-eqz v2, :cond_2

    .line 2350745
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2350746
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2350747
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2350748
    if-eqz v2, :cond_3

    .line 2350749
    const-string p0, "owner"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2350750
    invoke-static {v1, v2, p1, p2}, LX/57q;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2350751
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2350752
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2350753
    check-cast p1, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$Serializer;->a(Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
