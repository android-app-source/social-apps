.class public final Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x174b420c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2350784
    const-class v0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2350783
    const-class v0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2350781
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2350782
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2350775
    iget-object v0, p0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->f:Ljava/lang/String;

    .line 2350776
    iget-object v0, p0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2350779
    iget-object v0, p0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->g:Ljava/lang/String;

    .line 2350780
    iget-object v0, p0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2350777
    iget-object v0, p0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->h:Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->h:Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsModel;

    .line 2350778
    iget-object v0, p0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->h:Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2350785
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2350786
    invoke-virtual {p0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->j()Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2350787
    invoke-direct {p0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2350788
    invoke-direct {p0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2350789
    invoke-direct {p0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->m()Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2350790
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2350791
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2350792
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2350793
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2350794
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2350795
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2350796
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2350758
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2350759
    invoke-virtual {p0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->j()Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2350760
    invoke-virtual {p0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->j()Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;

    .line 2350761
    invoke-virtual {p0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->j()Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2350762
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;

    .line 2350763
    iput-object v0, v1, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->e:Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;

    .line 2350764
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->m()Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2350765
    invoke-direct {p0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->m()Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsModel;

    .line 2350766
    invoke-direct {p0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->m()Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2350767
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;

    .line 2350768
    iput-object v0, v1, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->h:Lcom/facebook/api/graphql/actor/NewsFeedActorGraphQLModels$DefaultActorFieldsModel;

    .line 2350769
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2350770
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2350754
    invoke-direct {p0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2350755
    new-instance v0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;-><init>()V

    .line 2350756
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2350757
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2350771
    const v0, 0x64bb695d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2350772
    const v0, 0x21b0c670

    return v0
.end method

.method public final j()Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2350773
    iget-object v0, p0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->e:Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->e:Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;

    .line 2350774
    iget-object v0, p0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel;->e:Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;

    return-object v0
.end method
