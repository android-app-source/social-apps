.class public final Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x789077ba
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel$PageInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2350693
    const-class v0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2350692
    const-class v0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2350690
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2350691
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2350682
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2350683
    invoke-virtual {p0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2350684
    invoke-virtual {p0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;->j()Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel$PageInfoModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2350685
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2350686
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2350687
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2350688
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2350689
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2350680
    iget-object v0, p0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;->e:Ljava/util/List;

    .line 2350681
    iget-object v0, p0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2350667
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2350668
    invoke-virtual {p0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2350669
    invoke-virtual {p0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2350670
    if-eqz v1, :cond_2

    .line 2350671
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;

    .line 2350672
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 2350673
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;->j()Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel$PageInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2350674
    invoke-virtual {p0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;->j()Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel$PageInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel$PageInfoModel;

    .line 2350675
    invoke-virtual {p0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;->j()Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel$PageInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2350676
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;

    .line 2350677
    iput-object v0, v1, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;->f:Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel$PageInfoModel;

    .line 2350678
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2350679
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2350664
    new-instance v0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;-><init>()V

    .line 2350665
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2350666
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2350663
    const v0, 0x18193287

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2350662
    const v0, 0x393fd701

    return v0
.end method

.method public final j()Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel$PageInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2350660
    iget-object v0, p0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;->f:Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel$PageInfoModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel$PageInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel$PageInfoModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;->f:Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel$PageInfoModel;

    .line 2350661
    iget-object v0, p0, Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel;->f:Lcom/facebook/api/graphql/curatedcollection/FetchCuratedCollectionModels$CuratedCollectionFieldsModel$CollectionStoriesModel$PageInfoModel;

    return-object v0
.end method
