.class public final Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x12c8f374
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2358547
    const-class v0, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2358546
    const-class v0, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2358544
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2358545
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2358538
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2358539
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, -0x66e57885

    invoke-static {v1, v0, v2}, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2358540
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2358541
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2358542
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2358543
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2358528
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2358529
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2358530
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x66e57885

    invoke-static {v2, v0, v3}, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2358531
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2358532
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;

    .line 2358533
    iput v3, v0, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;->e:I

    .line 2358534
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2358535
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 2358536
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 2358537
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getLocationSharing"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2358518
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2358519
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2358525
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2358526
    const/4 v0, 0x0

    const v1, -0x66e57885

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;->e:I

    .line 2358527
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2358522
    new-instance v0, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;

    invoke-direct {v0}, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;-><init>()V

    .line 2358523
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2358524
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2358521
    const v0, -0x35fa8a9a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2358520
    const v0, -0x6747e1ce

    return v0
.end method
