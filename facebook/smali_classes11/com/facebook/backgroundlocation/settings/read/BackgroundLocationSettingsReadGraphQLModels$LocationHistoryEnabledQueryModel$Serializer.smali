.class public final Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2358492
    const-class v0, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;

    new-instance v1, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2358493
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2358494
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2358495
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2358496
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2358497
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2358498
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2358499
    if-eqz v2, :cond_3

    .line 2358500
    const-string p0, "location_sharing"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2358501
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2358502
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 2358503
    if-eqz p0, :cond_0

    .line 2358504
    const-string v0, "is_sharing_enabled"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2358505
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 2358506
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v2, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 2358507
    if-eqz p0, :cond_1

    .line 2358508
    const-string v0, "is_tracking_enabled"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2358509
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 2358510
    :cond_1
    const/4 p0, 0x2

    invoke-virtual {v1, v2, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 2358511
    if-eqz p0, :cond_2

    .line 2358512
    const-string v0, "show_nux"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2358513
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 2358514
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2358515
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2358516
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2358517
    check-cast p1, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel$Serializer;->a(Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
