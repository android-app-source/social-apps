.class public final Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2358422
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2358423
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2358424
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2358425
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 2358426
    if-nez p1, :cond_0

    .line 2358427
    :goto_0
    return v0

    .line 2358428
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 2358429
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2358430
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v1

    .line 2358431
    invoke-virtual {p0, p1, v5}, LX/15i;->b(II)Z

    move-result v2

    .line 2358432
    invoke-virtual {p0, p1, v6}, LX/15i;->b(II)Z

    move-result v3

    .line 2358433
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2358434
    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 2358435
    invoke-virtual {p3, v5, v2}, LX/186;->a(IZ)V

    .line 2358436
    invoke-virtual {p3, v6, v3}, LX/186;->a(IZ)V

    .line 2358437
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x66e57885
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2358441
    new-instance v0, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2358438
    packed-switch p0, :pswitch_data_0

    .line 2358439
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2358440
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch -0x66e57885
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2358442
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2358415
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$DraculaImplementation;->b(I)V

    .line 2358416
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2358417
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2358418
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2358419
    :cond_0
    iput-object p1, p0, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2358420
    iput p2, p0, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$DraculaImplementation;->b:I

    .line 2358421
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2358414
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2358413
    new-instance v0, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2358410
    iget v0, p0, LX/1vt;->c:I

    .line 2358411
    move v0, v0

    .line 2358412
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2358407
    iget v0, p0, LX/1vt;->c:I

    .line 2358408
    move v0, v0

    .line 2358409
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2358404
    iget v0, p0, LX/1vt;->b:I

    .line 2358405
    move v0, v0

    .line 2358406
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2358401
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2358402
    move-object v0, v0

    .line 2358403
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2358392
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2358393
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2358394
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2358395
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2358396
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2358397
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2358398
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2358399
    invoke-static {v3, v9, v2}, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2358400
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2358389
    iget v0, p0, LX/1vt;->c:I

    .line 2358390
    move v0, v0

    .line 2358391
    return v0
.end method
