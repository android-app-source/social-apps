.class public final Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2358443
    const-class v0, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;

    new-instance v1, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2358444
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2358491
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2358445
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2358446
    const/4 v2, 0x0

    .line 2358447
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 2358448
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2358449
    :goto_0
    move v1, v2

    .line 2358450
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2358451
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2358452
    new-instance v1, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;

    invoke-direct {v1}, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;-><init>()V

    .line 2358453
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2358454
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2358455
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2358456
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2358457
    :cond_0
    return-object v1

    .line 2358458
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2358459
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2358460
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2358461
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2358462
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 2358463
    const-string v4, "location_sharing"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2358464
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2358465
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_d

    .line 2358466
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2358467
    :goto_2
    move v1, v3

    .line 2358468
    goto :goto_1

    .line 2358469
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2358470
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2358471
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 2358472
    :cond_5
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_9

    .line 2358473
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2358474
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2358475
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_5

    if-eqz v10, :cond_5

    .line 2358476
    const-string p0, "is_sharing_enabled"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 2358477
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v6

    move v9, v6

    move v6, v4

    goto :goto_3

    .line 2358478
    :cond_6
    const-string p0, "is_tracking_enabled"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 2358479
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v5

    move v8, v5

    move v5, v4

    goto :goto_3

    .line 2358480
    :cond_7
    const-string p0, "show_nux"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 2358481
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v7, v1

    move v1, v4

    goto :goto_3

    .line 2358482
    :cond_8
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 2358483
    :cond_9
    const/4 v10, 0x3

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 2358484
    if-eqz v6, :cond_a

    .line 2358485
    invoke-virtual {v0, v3, v9}, LX/186;->a(IZ)V

    .line 2358486
    :cond_a
    if-eqz v5, :cond_b

    .line 2358487
    invoke-virtual {v0, v4, v8}, LX/186;->a(IZ)V

    .line 2358488
    :cond_b
    if-eqz v1, :cond_c

    .line 2358489
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v7}, LX/186;->a(IZ)V

    .line 2358490
    :cond_c
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_d
    move v1, v3

    move v5, v3

    move v6, v3

    move v7, v3

    move v8, v3

    move v9, v3

    goto :goto_3
.end method
