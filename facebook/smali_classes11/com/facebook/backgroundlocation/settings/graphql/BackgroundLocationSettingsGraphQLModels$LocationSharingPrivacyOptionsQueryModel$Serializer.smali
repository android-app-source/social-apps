.class public final Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2358192
    const-class v0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;

    new-instance v1, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2358193
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2358194
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2358195
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2358196
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2358197
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2358198
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2358199
    if-eqz v2, :cond_0

    .line 2358200
    const-string p0, "location_sharing"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2358201
    invoke-static {v1, v2, p1, p2}, LX/GUt;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2358202
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2358203
    if-eqz v2, :cond_1

    .line 2358204
    const-string p0, "privacy_settings"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2358205
    invoke-static {v1, v2, p1, p2}, LX/GUu;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2358206
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2358207
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2358208
    check-cast p1, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$Serializer;->a(Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
