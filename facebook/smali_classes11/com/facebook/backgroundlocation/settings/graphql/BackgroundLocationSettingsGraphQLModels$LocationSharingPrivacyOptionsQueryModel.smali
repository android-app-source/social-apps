.class public final Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x33505d33
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$PrivacySettingsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2358236
    const-class v0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2358235
    const-class v0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2358233
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2358234
    return-void
.end method

.method private j()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2358231
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;->e:Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;->e:Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;

    .line 2358232
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;->e:Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;

    return-object v0
.end method

.method private k()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$PrivacySettingsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2358229
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;->f:Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$PrivacySettingsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$PrivacySettingsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$PrivacySettingsModel;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;->f:Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$PrivacySettingsModel;

    .line 2358230
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;->f:Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$PrivacySettingsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2358237
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2358238
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;->j()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2358239
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;->k()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$PrivacySettingsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2358240
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2358241
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2358242
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2358243
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2358244
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2358216
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2358217
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;->j()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2358218
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;->j()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;

    .line 2358219
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;->j()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2358220
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;

    .line 2358221
    iput-object v0, v1, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;->e:Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;

    .line 2358222
    :cond_0
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;->k()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$PrivacySettingsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2358223
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;->k()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$PrivacySettingsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$PrivacySettingsModel;

    .line 2358224
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;->k()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$PrivacySettingsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2358225
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;

    .line 2358226
    iput-object v0, v1, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;->f:Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$PrivacySettingsModel;

    .line 2358227
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2358228
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2358215
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;->j()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$PrivacySettingsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2358214
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;->k()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$PrivacySettingsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2358211
    new-instance v0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;

    invoke-direct {v0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;-><init>()V

    .line 2358212
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2358213
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2358210
    const v0, 0x11343b71

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2358209
    const v0, -0x6747e1ce

    return v0
.end method
