.class public final Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x29560c70
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Z

.field private g:J

.field private h:Z

.field private i:Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2358140
    const-class v0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2358143
    const-class v0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2358141
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2358142
    return-void
.end method

.method private j()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2358102
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->i:Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->i:Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;

    .line 2358103
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->i:Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 2358130
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2358131
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->j()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2358132
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 2358133
    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->e:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2358134
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->f:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2358135
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->g:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2358136
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2358137
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2358138
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2358139
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2358122
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2358123
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->j()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2358124
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->j()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;

    .line 2358125
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->j()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2358126
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;

    .line 2358127
    iput-object v0, v1, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->i:Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;

    .line 2358128
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2358129
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 2358116
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2358117
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->e:Z

    .line 2358118
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->f:Z

    .line 2358119
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->g:J

    .line 2358120
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->h:Z

    .line 2358121
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2358144
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2358145
    iget-boolean v0, p0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2358113
    new-instance v0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;

    invoke-direct {v0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;-><init>()V

    .line 2358114
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2358115
    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 2358111
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2358112
    iget-boolean v0, p0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->f:Z

    return v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 2358109
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2358110
    iget-wide v0, p0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->g:J

    return-wide v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 2358107
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2358108
    iget-boolean v0, p0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->h:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2358106
    const v0, 0x49e59635

    return v0
.end method

.method public final synthetic e()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2358105
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->j()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2358104
    const v0, 0x37207b47

    return v0
.end method
