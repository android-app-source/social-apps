.class public final Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4cc77bbb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel$FriendsSharingLocationConnectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2358101
    const-class v0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2358100
    const-class v0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2358098
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2358099
    return-void
.end method

.method private j()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel$FriendsSharingLocationConnectionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2358096
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;->e:Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel$FriendsSharingLocationConnectionModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel$FriendsSharingLocationConnectionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel$FriendsSharingLocationConnectionModel;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;->e:Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel$FriendsSharingLocationConnectionModel;

    .line 2358097
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;->e:Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel$FriendsSharingLocationConnectionModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2358076
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2358077
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;->j()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel$FriendsSharingLocationConnectionModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2358078
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2358079
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2358080
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2358081
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2358088
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2358089
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;->j()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel$FriendsSharingLocationConnectionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2358090
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;->j()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel$FriendsSharingLocationConnectionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel$FriendsSharingLocationConnectionModel;

    .line 2358091
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;->j()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel$FriendsSharingLocationConnectionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2358092
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;

    .line 2358093
    iput-object v0, v1, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;->e:Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel$FriendsSharingLocationConnectionModel;

    .line 2358094
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2358095
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel$FriendsSharingLocationConnectionModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2358087
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;->j()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel$FriendsSharingLocationConnectionModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2358084
    new-instance v0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;

    invoke-direct {v0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;-><init>()V

    .line 2358085
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2358086
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2358083
    const v0, -0x379294ff

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2358082
    const v0, -0x511a8984

    return v0
.end method
