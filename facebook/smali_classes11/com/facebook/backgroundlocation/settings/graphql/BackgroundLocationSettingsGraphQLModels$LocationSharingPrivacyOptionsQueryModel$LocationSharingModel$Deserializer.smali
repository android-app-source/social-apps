.class public final Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2357996
    const-class v0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;

    new-instance v1, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2357997
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2357995
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2357985
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2357986
    invoke-static {p1, v0}, LX/GUt;->a(LX/15w;LX/186;)I

    move-result v1

    .line 2357987
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2357988
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2357989
    new-instance v1, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;

    invoke-direct {v1}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;-><init>()V

    .line 2357990
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2357991
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2357992
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2357993
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2357994
    :cond_0
    return-object v1
.end method
