.class public Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# static fields
.field public static final p:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field private B:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field private C:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field private D:Landroid/view/View;

.field private E:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;

.field private F:Landroid/view/View;

.field private G:Landroid/view/View;

.field private H:Landroid/view/View;

.field private I:Landroid/widget/TextView;

.field private J:Landroid/widget/TextView;

.field public K:LX/0kL;

.field public L:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

.field public M:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

.field private N:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

.field private O:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

.field public P:Z

.field public Q:LX/GUB;

.field private R:Lcom/facebook/content/SecureContextHelper;

.field public S:LX/03V;

.field private T:LX/0y3;

.field public U:LX/6G2;

.field public V:LX/0Uh;

.field public W:LX/GUi;

.field public X:LX/GUl;

.field private Y:LX/0i4;

.field private Z:LX/0i5;

.field private aa:LX/6Zb;

.field private ab:LX/6Ze;

.field private ac:LX/0ad;

.field private ad:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;"
        }
    .end annotation
.end field

.field private ae:LX/0wM;

.field public af:LX/GUm;

.field private ag:Z

.field public ah:LX/03R;

.field public ai:Z

.field public aj:LX/0yG;

.field public ak:Z

.field private final al:Landroid/view/View$OnClickListener;

.field private final am:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private final an:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private final ao:LX/GUa;

.field private q:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/GUh;",
            ">;"
        }
    .end annotation
.end field

.field public r:LX/0tX;

.field public s:LX/0aG;

.field private t:Lcom/facebook/location/ui/LocationSettingsOffView;

.field private u:Lcom/facebook/widget/SwitchCompat;

.field private v:Lcom/facebook/fbui/popover/PopoverSpinner;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/widget/TextView;

.field private y:Landroid/widget/TextView;

.field private z:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2357680
    const-class v0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2357681
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2357682
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ah:LX/03R;

    .line 2357683
    new-instance v0, LX/GUS;

    invoke-direct {v0, p0}, LX/GUS;-><init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->al:Landroid/view/View$OnClickListener;

    .line 2357684
    new-instance v0, LX/GUY;

    invoke-direct {v0, p0}, LX/GUY;-><init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->am:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 2357685
    new-instance v0, LX/GUZ;

    invoke-direct {v0, p0}, LX/GUZ;-><init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->an:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 2357686
    new-instance v0, LX/GUb;

    invoke-direct {v0, p0}, LX/GUb;-><init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ao:LX/GUa;

    .line 2357687
    return-void
.end method

.method private A()V
    .locals 2

    .prologue
    .line 2357688
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->z:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2357689
    return-void
.end method

.method public static B(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V
    .locals 6

    .prologue
    .line 2357690
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ad:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Af;

    .line 2357691
    new-instance v1, LX/7TY;

    invoke-direct {v1, p0}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 2357692
    const v2, 0x7f083122

    invoke-virtual {v1, v2}, LX/7TY;->f(I)V

    .line 2357693
    iget-object v2, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ae:LX/0wM;

    const v3, 0x7f0207fd

    const v4, 0x7f0a00fc

    invoke-static {p0, v4}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2357694
    iget-object v3, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ae:LX/0wM;

    const v4, 0x7f020987

    const v5, 0x7f0a00fc

    invoke-static {p0, v5}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 2357695
    const v4, 0x7f083123

    invoke-virtual {v1, v4}, LX/34c;->e(I)LX/3Ai;

    move-result-object v4

    .line 2357696
    invoke-virtual {v4, v2}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 2357697
    new-instance v5, LX/GUM;

    invoke-direct {v5, p0}, LX/GUM;-><init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    invoke-virtual {v4, v5}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2357698
    const v4, 0x7f083124

    invoke-virtual {v1, v4}, LX/34c;->e(I)LX/3Ai;

    move-result-object v4

    .line 2357699
    invoke-virtual {v4, v2}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 2357700
    new-instance v5, LX/GUN;

    invoke-direct {v5, p0}, LX/GUN;-><init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    invoke-virtual {v4, v5}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2357701
    const v4, 0x7f083125

    invoke-virtual {v1, v4}, LX/34c;->e(I)LX/3Ai;

    move-result-object v4

    .line 2357702
    invoke-virtual {v4, v2}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 2357703
    new-instance v2, LX/GUO;

    invoke-direct {v2, p0}, LX/GUO;-><init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    invoke-virtual {v4, v2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2357704
    const v2, 0x7f083126

    invoke-direct {p0, v2}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->f(I)Landroid/text/SpannableString;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v2

    .line 2357705
    invoke-virtual {v2, v3}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 2357706
    new-instance v3, LX/GUP;

    invoke-direct {v3, p0}, LX/GUP;-><init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    invoke-virtual {v2, v3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2357707
    invoke-virtual {v0, v1}, LX/3Af;->a(LX/1OM;)V

    .line 2357708
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/3Af;->a(I)V

    .line 2357709
    new-instance v1, LX/GUQ;

    invoke-direct {v1, p0}, LX/GUQ;-><init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    invoke-virtual {v0, v1}, LX/3Af;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 2357710
    invoke-virtual {v0}, LX/3Af;->show()V

    .line 2357711
    return-void
.end method

.method public static C(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V
    .locals 3

    .prologue
    .line 2357712
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->M:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    .line 2357713
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->d(Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2357714
    :cond_0
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->N:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    .line 2357715
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->a(ZLjava/lang/String;)Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->a(Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)V

    .line 2357716
    return-void
.end method

.method public static D(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V
    .locals 2

    .prologue
    .line 2357717
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->O:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->a(Ljava/lang/String;)Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->O:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    invoke-direct {p0, v0, v1}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->a(Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)V

    .line 2357718
    return-void
.end method

.method public static E(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V
    .locals 2

    .prologue
    .line 2357719
    const-wide/16 v0, 0x0

    invoke-static {p0, v0, v1}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->a$redex0(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;J)V

    .line 2357720
    return-void
.end method

.method private F()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2357601
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->I()V

    .line 2357602
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->F:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2357603
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->H:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2357604
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->G:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2357605
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->D:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2357606
    return-void
.end method

.method private G()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2357721
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->I()V

    .line 2357722
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->F:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2357723
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->H:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2357724
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->G:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2357725
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->D:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2357726
    return-void
.end method

.method private H()V
    .locals 3

    .prologue
    .line 2357727
    new-instance v0, LX/0ju;

    invoke-direct {v0, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v1, 0x7f08311d

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080016

    new-instance v2, LX/GUV;

    invoke-direct {v2, p0}, LX/GUV;-><init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2357728
    return-void
.end method

.method private I()V
    .locals 2

    .prologue
    .line 2357729
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->L()Lcom/facebook/widget/CustomLinearLayout;

    move-result-object v0

    new-instance v1, LX/GUW;

    invoke-direct {v1, p0}, LX/GUW;-><init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->a(LX/10U;)V

    .line 2357730
    return-void
.end method

.method private J()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2357731
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->H:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2357732
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->G:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2357733
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->D:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2357734
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->F:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2357735
    return-void
.end method

.method public static K(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V
    .locals 2

    .prologue
    .line 2357736
    const v0, 0x7f080039

    invoke-virtual {p0, v0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f080040

    invoke-virtual {p0, v1}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2357737
    return-void
.end method

.method private L()Lcom/facebook/widget/CustomLinearLayout;
    .locals 1

    .prologue
    .line 2357738
    const v0, 0x7f0d0674

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    return-object v0
.end method

.method private a(ZZZ)I
    .locals 2

    .prologue
    .line 2357817
    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    .line 2357818
    const v0, 0x7f083110

    .line 2357819
    :goto_0
    return v0

    .line 2357820
    :cond_0
    if-nez p3, :cond_4

    .line 2357821
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->aj:LX/0yG;

    invoke-static {v0}, LX/1rv;->b(LX/0yG;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2357822
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->aj:LX/0yG;

    sget-object v1, LX/0yG;->LOCATION_DISABLED:LX/0yG;

    if-ne v0, v1, :cond_2

    .line 2357823
    if-eqz p1, :cond_1

    .line 2357824
    const v0, 0x7f083111

    goto :goto_0

    .line 2357825
    :cond_1
    const v0, 0x7f083114

    goto :goto_0

    .line 2357826
    :cond_2
    if-eqz p1, :cond_3

    .line 2357827
    const v0, 0x7f083115

    goto :goto_0

    .line 2357828
    :cond_3
    const v0, 0x7f083116

    goto :goto_0

    .line 2357829
    :cond_4
    if-eqz p2, :cond_5

    .line 2357830
    const v0, 0x7f083113

    goto :goto_0

    .line 2357831
    :cond_5
    const v0, 0x7f083112

    goto :goto_0
.end method

.method private a(ILX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/0Px",
            "<+",
            "Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLInterfaces$BackgroundLocationUpsellProfile$;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2357739
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->E:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;

    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ao:LX/GUa;

    invoke-virtual {v0, p1, p2, v1}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;->a(ILX/0Px;LX/GUa;)V

    .line 2357740
    return-void
.end method

.method private a(LX/0tX;LX/1Ck;LX/0aG;Lcom/facebook/content/SecureContextHelper;LX/0kL;LX/03V;LX/0y3;LX/6G2;LX/0Uh;LX/GUi;LX/GUl;LX/0i4;LX/6Zb;LX/6Ze;LX/0ad;LX/0Or;LX/0wM;LX/GUm;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "LX/1Ck;",
            "LX/0aG;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0kL;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0y3;",
            "LX/6G2;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/GUi;",
            "LX/GUl;",
            "LX/0i4;",
            "LX/6Zb;",
            "LX/6Ze;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;",
            "LX/0wM;",
            "LX/GUm;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2357798
    iput-object p9, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->V:LX/0Uh;

    .line 2357799
    iput-object p2, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->q:LX/1Ck;

    .line 2357800
    iput-object p1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->r:LX/0tX;

    .line 2357801
    iput-object p3, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->s:LX/0aG;

    .line 2357802
    iput-object p4, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->R:Lcom/facebook/content/SecureContextHelper;

    .line 2357803
    iput-object p5, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->K:LX/0kL;

    .line 2357804
    iput-object p6, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->S:LX/03V;

    .line 2357805
    iput-object p7, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->T:LX/0y3;

    .line 2357806
    iput-object p8, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->U:LX/6G2;

    .line 2357807
    iput-object p10, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->W:LX/GUi;

    .line 2357808
    iput-object p11, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->X:LX/GUl;

    .line 2357809
    iput-object p12, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->Y:LX/0i4;

    .line 2357810
    iput-object p13, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->aa:LX/6Zb;

    .line 2357811
    iput-object p14, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ab:LX/6Ze;

    .line 2357812
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ac:LX/0ad;

    .line 2357813
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ad:LX/0Or;

    .line 2357814
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ae:LX/0wM;

    .line 2357815
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->af:LX/GUm;

    .line 2357816
    return-void
.end method

.method private a(Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 2357794
    const v0, 0x7f080024

    const/4 v1, 0x0

    invoke-static {v0, v2, v1, v2}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    .line 2357795
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    const-string v2, "write_privacy_setting_progress"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2357796
    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->q:LX/1Ck;

    sget-object v2, LX/GUh;->LOCATION_SHARING_PRIVACY_WRITING:LX/GUh;

    new-instance v3, LX/GUR;

    invoke-direct {v3, p0, p1}, LX/GUR;-><init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;)V

    new-instance v4, LX/GUT;

    invoke-direct {v4, p0, p1, p2, v0}, LX/GUT;-><init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;Landroid/support/v4/app/DialogFragment;)V

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2357797
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 21

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v20

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    invoke-static/range {v20 .. v20}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static/range {v20 .. v20}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static/range {v20 .. v20}, LX/0aF;->getInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v5

    check-cast v5, LX/0aG;

    invoke-static/range {v20 .. v20}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v20 .. v20}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v7

    check-cast v7, LX/0kL;

    invoke-static/range {v20 .. v20}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static/range {v20 .. v20}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v9

    check-cast v9, LX/0y3;

    invoke-static/range {v20 .. v20}, LX/6G2;->a(LX/0QB;)LX/6G2;

    move-result-object v10

    check-cast v10, LX/6G2;

    invoke-static/range {v20 .. v20}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    invoke-static/range {v20 .. v20}, LX/GUi;->a(LX/0QB;)LX/GUi;

    move-result-object v12

    check-cast v12, LX/GUi;

    invoke-static/range {v20 .. v20}, LX/GUl;->a(LX/0QB;)LX/GUl;

    move-result-object v13

    check-cast v13, LX/GUl;

    const-class v14, LX/0i4;

    move-object/from16 v0, v20

    invoke-interface {v0, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/0i4;

    invoke-static/range {v20 .. v20}, LX/6Zb;->a(LX/0QB;)LX/6Zb;

    move-result-object v15

    check-cast v15, LX/6Zb;

    invoke-static/range {v20 .. v20}, LX/6Ze;->a(LX/0QB;)LX/6Ze;

    move-result-object v16

    check-cast v16, LX/6Ze;

    invoke-static/range {v20 .. v20}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v17

    check-cast v17, LX/0ad;

    const/16 v18, 0x1399

    move-object/from16 v0, v20

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v18

    invoke-static/range {v20 .. v20}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v19

    check-cast v19, LX/0wM;

    invoke-static/range {v20 .. v20}, LX/GUm;->a(LX/0QB;)LX/GUm;

    move-result-object v20

    check-cast v20, LX/GUm;

    invoke-direct/range {v2 .. v20}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->a(LX/0tX;LX/1Ck;LX/0aG;Lcom/facebook/content/SecureContextHelper;LX/0kL;LX/03V;LX/0y3;LX/6G2;LX/0Uh;LX/GUi;LX/GUl;LX/0i4;LX/6Zb;LX/6Ze;LX/0ad;LX/0Or;LX/0wM;LX/GUm;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2357786
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->G:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2357787
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->D:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2357788
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->F:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2357789
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->H:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2357790
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->I:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2357791
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->J:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2357792
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->H:Landroid/view/View;

    new-instance v1, LX/GUX;

    invoke-direct {v1, p0}, LX/GUX;-><init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2357793
    return-void
.end method

.method public static a$redex0(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;J)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 2357777
    const v0, 0x7f080024

    const/4 v1, 0x0

    invoke-static {v0, v2, v1, v2}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    .line 2357778
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    const-string v2, "pause_progress"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2357779
    invoke-static {}, LX/BaX;->a()LX/BaW;

    move-result-object v1

    .line 2357780
    new-instance v2, LX/4Gn;

    invoke-direct {v2}, LX/4Gn;-><init>()V

    .line 2357781
    const-wide/16 v4, 0x3e8

    div-long v4, p1, v4

    long-to-int v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4Gn;->a(Ljava/lang/Integer;)LX/4Gn;

    .line 2357782
    const-string v3, "input"

    invoke-virtual {v1, v3, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2357783
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 2357784
    iget-object v2, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->q:LX/1Ck;

    sget-object v3, LX/GUh;->LOCATION_SHARING_PAUSE:LX/GUh;

    iget-object v4, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->r:LX/0tX;

    sget-object v5, LX/3Fz;->b:LX/3Fz;

    invoke-virtual {v4, v1, v5}, LX/0tX;->a(LX/399;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v4, LX/GUU;

    invoke-direct {v4, p0, v0}, LX/GUU;-><init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;Landroid/support/v4/app/DialogFragment;)V

    invoke-virtual {v2, v3, v1, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2357785
    return-void
.end method

.method public static a$redex0(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2357832
    invoke-virtual {p1}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;->a()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ag:Z

    .line 2357833
    invoke-virtual {p1}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;->a()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->c()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ai:Z

    .line 2357834
    invoke-virtual {p1}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;->a()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->P:Z

    .line 2357835
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2357836
    invoke-virtual {p1}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;->b()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$PrivacySettingsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$PrivacySettingsModel;->a()Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;->a()LX/0Px;

    move-result-object v5

    .line 2357837
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_5

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionEdgeModel;

    .line 2357838
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionEdgeModel;->c()Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    move-result-object v7

    .line 2357839
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionEdgeModel;->a()Z

    move-result v8

    .line 2357840
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionEdgeModel;->b()Z

    move-result v0

    .line 2357841
    if-eqz v8, :cond_0

    .line 2357842
    iput-object v7, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->L:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    .line 2357843
    :cond_0
    if-eqz v0, :cond_1

    .line 2357844
    iput-object v7, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->M:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    .line 2357845
    :cond_1
    invoke-virtual {v7}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 2357846
    const-string v8, "{\"value\":\"ALL_FRIENDS\"}"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2357847
    iput-object v7, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->N:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    .line 2357848
    :cond_2
    invoke-static {v7}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->d(Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2357849
    iput-object v7, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->O:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    .line 2357850
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_3
    move v0, v2

    .line 2357851
    goto :goto_0

    .line 2357852
    :cond_4
    invoke-virtual {v4, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 2357853
    :cond_5
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->N:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->O:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->L:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    if-nez v0, :cond_8

    .line 2357854
    :cond_6
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->S:LX/03V;

    const-string v1, "background_location_settings"

    const-string v2, "Must have Friends, Only Me, and current selection in the privacy setting options"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2357855
    invoke-static {p0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->K(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    .line 2357856
    :cond_7
    :goto_3
    return-void

    .line 2357857
    :cond_8
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->y()Z

    move-result v3

    .line 2357858
    if-nez v3, :cond_9

    invoke-virtual {p1}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;->a()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->d()Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    .line 2357859
    :goto_4
    iget-object v5, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ah:LX/03R;

    sget-object v6, LX/03R;->UNSET:LX/03R;

    if-eq v5, v6, :cond_a

    .line 2357860
    :goto_5
    if-eqz v0, :cond_b

    .line 2357861
    invoke-virtual {p1}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;->a()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel;->e()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel;->a()Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel$FriendsSharingLocationConnectionModel;

    move-result-object v0

    .line 2357862
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel$FriendsSharingLocationConnectionModel;->a()I

    move-result v2

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel$LocationSharingModel$UpsellModel$FriendsSharingLocationConnectionModel;->b()LX/0Px;

    move-result-object v0

    invoke-direct {p0, v2, v0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->a(ILX/0Px;)V

    .line 2357863
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->G()V

    .line 2357864
    :goto_6
    if-nez v1, :cond_7

    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->w()Z

    move-result v0

    if-nez v0, :cond_7

    if-eqz v3, :cond_7

    .line 2357865
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->H()V

    goto :goto_3

    :cond_9
    move v0, v2

    .line 2357866
    goto :goto_4

    :cond_a
    move v1, v2

    .line 2357867
    goto :goto_5

    .line 2357868
    :cond_b
    new-instance v0, LX/GUB;

    invoke-direct {v0}, LX/GUB;-><init>()V

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->Q:LX/GUB;

    .line 2357869
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->Q:LX/GUB;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/GUB;->a(LX/0Px;)V

    .line 2357870
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->v:Lcom/facebook/fbui/popover/PopoverSpinner;

    iget-object v2, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->Q:LX/GUB;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/popover/PopoverSpinner;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2357871
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->L:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    invoke-static {p0, v0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->b(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)V

    .line 2357872
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->F()V

    goto :goto_6
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 2357774
    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 2357775
    :cond_0
    invoke-static {p0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->x(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    .line 2357776
    :cond_1
    return-void
.end method

.method public static b(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2357750
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->w()Z

    move-result v3

    .line 2357751
    iget-boolean v4, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->P:Z

    .line 2357752
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->y()Z

    move-result v5

    .line 2357753
    if-eqz v5, :cond_0

    if-eqz v3, :cond_0

    move v0, v1

    .line 2357754
    :goto_0
    invoke-direct {p0, v5}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->h(Z)V

    .line 2357755
    iget-object v6, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->u:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v6, v3}, Lcom/facebook/widget/SwitchCompat;->setEnabled(Z)V

    .line 2357756
    invoke-static {v5}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v6

    iput-object v6, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ah:LX/03R;

    .line 2357757
    invoke-direct {p0, v5}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->b(Z)V

    .line 2357758
    invoke-direct {p0, v3}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->c(Z)V

    .line 2357759
    iget-object v6, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->Q:LX/GUB;

    .line 2357760
    iget-object v7, v6, LX/GUB;->a:LX/0Px;

    invoke-virtual {v7, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v7

    move v6, v7

    .line 2357761
    invoke-direct {p0, v6}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->g(I)V

    .line 2357762
    iget-boolean v6, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ai:Z

    invoke-direct {p0, v5, v6, v3}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->a(ZZZ)I

    move-result v6

    invoke-direct {p0, v6}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->d(I)V

    .line 2357763
    invoke-direct {p0, v3}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->d(Z)V

    .line 2357764
    if-nez v3, :cond_1

    :goto_1
    invoke-direct {p0, v1}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->e(Z)V

    .line 2357765
    invoke-direct {p0, v0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->f(Z)V

    .line 2357766
    if-nez v3, :cond_2

    if-nez v5, :cond_2

    .line 2357767
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->A()V

    .line 2357768
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 2357769
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2357770
    goto :goto_1

    .line 2357771
    :cond_2
    if-eqz v4, :cond_3

    .line 2357772
    const v0, 0x7f083118

    invoke-direct {p0, v0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->e(I)V

    goto :goto_2

    .line 2357773
    :cond_3
    const v0, 0x7f083119

    invoke-direct {p0, v0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->e(I)V

    goto :goto_2
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 2357747
    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->v:Lcom/facebook/fbui/popover/PopoverSpinner;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/popover/PopoverSpinner;->setVisibility(I)V

    .line 2357748
    return-void

    .line 2357749
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static c(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)V
    .locals 1

    .prologue
    .line 2357745
    invoke-virtual {p1}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->a(Ljava/lang/String;)Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->a(Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)V

    .line 2357746
    return-void
.end method

.method private c(Z)V
    .locals 1

    .prologue
    .line 2357743
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->v:Lcom/facebook/fbui/popover/PopoverSpinner;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/popover/PopoverSpinner;->setEnabled(Z)V

    .line 2357744
    return-void
.end method

.method private d(I)V
    .locals 1

    .prologue
    .line 2357741
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->w:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 2357742
    return-void
.end method

.method private d(Z)V
    .locals 2

    .prologue
    .line 2357676
    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->x:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2357677
    return-void

    .line 2357678
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static d(Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)Z
    .locals 2

    .prologue
    .line 2357679
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "{\"value\":\"SELF\"}"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private e(I)V
    .locals 2

    .prologue
    .line 2357514
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->z:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2357515
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->z:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 2357516
    return-void
.end method

.method private e(Z)V
    .locals 2

    .prologue
    .line 2357517
    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->y:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2357518
    if-nez p1, :cond_1

    .line 2357519
    :goto_1
    return-void

    .line 2357520
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 2357521
    :cond_1
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->aj:LX/0yG;

    invoke-static {v0}, LX/1rv;->b(LX/0yG;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2357522
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->aj:LX/0yG;

    sget-object v1, LX/0yG;->LOCATION_DISABLED:LX/0yG;

    if-ne v0, v1, :cond_2

    const v0, 0x7f083117

    .line 2357523
    :goto_2
    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->y:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 2357524
    :cond_2
    const v0, 0x7f083121

    goto :goto_2
.end method

.method private f(I)Landroid/text/SpannableString;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2357525
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2357526
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2357527
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    const v3, 0x7f0a00d3

    invoke-static {p0, v3}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v1, v2, v4, v0, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2357528
    return-object v1
.end method

.method private f(Z)V
    .locals 2

    .prologue
    .line 2357529
    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->C:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setVisibility(I)V

    .line 2357530
    return-void

    .line 2357531
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private g(I)V
    .locals 1

    .prologue
    .line 2357532
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->v:Lcom/facebook/fbui/popover/PopoverSpinner;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/popover/PopoverSpinner;->setSelection(I)V

    .line 2357533
    return-void
.end method

.method public static g(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;Z)V
    .locals 2

    .prologue
    .line 2357534
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->u:Lcom/facebook/widget/SwitchCompat;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2357535
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->u:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 2357536
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->u:Lcom/facebook/widget/SwitchCompat;

    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->am:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2357537
    return-void
.end method

.method private h(Z)V
    .locals 2

    .prologue
    .line 2357538
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->u:Lcom/facebook/widget/SwitchCompat;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2357539
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->u:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/SwitchCompat;->setCheckedNoAnimation(Z)V

    .line 2357540
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->u:Lcom/facebook/widget/SwitchCompat;

    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->am:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2357541
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 2357542
    const v0, 0x7f0d0665

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/ui/LocationSettingsOffView;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->t:Lcom/facebook/location/ui/LocationSettingsOffView;

    .line 2357543
    const v0, 0x7f0d066d

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->A:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2357544
    const v0, 0x7f0d066c

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->z:Landroid/widget/TextView;

    .line 2357545
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->t:Lcom/facebook/location/ui/LocationSettingsOffView;

    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->al:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/location/ui/LocationSettingsOffView;->setButtonListener(Landroid/view/View$OnClickListener;)V

    .line 2357546
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->A:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    new-instance v1, LX/GUd;

    invoke-direct {v1, p0}, LX/GUd;-><init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2357547
    return-void
.end method

.method private m()V
    .locals 2

    .prologue
    .line 2357548
    const v0, 0x7f0d0667

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/SwitchCompat;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->u:Lcom/facebook/widget/SwitchCompat;

    .line 2357549
    const v0, 0x7f0d0668

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/popover/PopoverSpinner;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->v:Lcom/facebook/fbui/popover/PopoverSpinner;

    .line 2357550
    const v0, 0x7f0d0669

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->w:Landroid/widget/TextView;

    .line 2357551
    const v0, 0x7f0d066a

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->x:Landroid/widget/TextView;

    .line 2357552
    const v0, 0x7f0d066b

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->y:Landroid/widget/TextView;

    .line 2357553
    const v0, 0x7f0d066c

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->z:Landroid/widget/TextView;

    .line 2357554
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->v:Lcom/facebook/fbui/popover/PopoverSpinner;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/popover/PopoverSpinner;->setPopoverDimAmount(F)V

    .line 2357555
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->v:Lcom/facebook/fbui/popover/PopoverSpinner;

    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->an:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/popover/PopoverSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 2357556
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->x:Landroid/widget/TextView;

    new-instance v1, LX/GUe;

    invoke-direct {v1, p0}, LX/GUe;-><init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2357557
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->y:Landroid/widget/TextView;

    new-instance v1, LX/GUf;

    invoke-direct {v1, p0}, LX/GUf;-><init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2357558
    return-void
.end method

.method public static n$redex0(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V
    .locals 2

    .prologue
    .line 2357559
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2357560
    sget-object v1, LX/0ax;->dZ:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2357561
    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->R:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2357562
    return-void
.end method

.method private o()V
    .locals 2

    .prologue
    .line 2357563
    const v0, 0x7f0d066e

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->C:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2357564
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->C:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    new-instance v1, LX/GUg;

    invoke-direct {v1, p0}, LX/GUg;-><init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2357565
    return-void
.end method

.method private p()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2357566
    const v0, 0x7f0d066f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->B:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2357567
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->V:LX/0Uh;

    const/16 v1, 0x2ed

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2357568
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->B:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setVisibility(I)V

    .line 2357569
    :goto_0
    return-void

    .line 2357570
    :cond_0
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->B:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setVisibility(I)V

    .line 2357571
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->B:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    new-instance v1, LX/GUI;

    invoke-direct {v1, p0}, LX/GUI;-><init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public static q(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V
    .locals 3

    .prologue
    .line 2357572
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    sget-object v1, LX/0ax;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "faceweb"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "f"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "href"

    const-string v2, "/tour/locationsharing/learnmore"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 2357573
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2357574
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->R:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2357575
    return-void
.end method

.method public static r(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V
    .locals 2

    .prologue
    .line 2357576
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2357577
    sget-object v1, LX/0ax;->eg:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2357578
    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->R:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2357579
    return-void
.end method

.method public static s(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V
    .locals 2

    .prologue
    .line 2357580
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2357581
    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->R:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2357582
    return-void
.end method

.method public static t(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V
    .locals 3

    .prologue
    .line 2357583
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->Z:LX/0i5;

    sget-object v1, LX/3KE;->a:[Ljava/lang/String;

    new-instance v2, LX/GUJ;

    invoke-direct {v2, p0}, LX/GUJ;-><init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 2357584
    return-void
.end method

.method public static u$redex0(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V
    .locals 4

    .prologue
    .line 2357585
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->aa:LX/6Zb;

    new-instance v1, LX/2si;

    invoke-direct {v1}, LX/2si;-><init>()V

    const-string v2, "surface_nearby_friends_settings"

    const-string v3, "mechanism_turn_on_button"

    invoke-virtual {v0, v1, v2, v3}, LX/6Zb;->a(LX/2si;Ljava/lang/String;Ljava/lang/String;)V

    .line 2357586
    return-void
.end method

.method public static v(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V
    .locals 3

    .prologue
    .line 2357587
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2357588
    const-string v1, "source"

    const-string v2, "settings_upsell"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2357589
    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->R:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2357590
    return-void
.end method

.method private w()Z
    .locals 2

    .prologue
    .line 2357591
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->T:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->a()LX/0yG;

    move-result-object v0

    sget-object v1, LX/0yG;->OKAY:LX/0yG;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static x(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V
    .locals 4

    .prologue
    .line 2357592
    new-instance v0, LX/GUn;

    invoke-direct {v0}, LX/GUn;-><init>()V

    move-object v0, v0

    .line 2357593
    const-string v1, "n_upsell_results"

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2357594
    const-string v1, "image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2357595
    const-string v1, "image_size"

    const/16 v2, 0x40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2357596
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->J()V

    .line 2357597
    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->q:LX/1Ck;

    sget-object v2, LX/GUh;->LOCATION_SHARING_PRIVACY_READING:LX/GUh;

    new-instance v3, LX/GUK;

    invoke-direct {v3, p0, v0}, LX/GUK;-><init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;LX/GUn;)V

    new-instance v0, LX/GUL;

    invoke-direct {v0, p0}, LX/GUL;-><init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    invoke-virtual {v1, v2, v3, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2357598
    return-void
.end method

.method private y()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2357599
    iget-boolean v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ai:Z

    if-eqz v1, :cond_1

    .line 2357600
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ah:LX/03R;

    sget-object v2, LX/03R;->UNSET:LX/03R;

    if-ne v1, v2, :cond_2

    iget-boolean v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ag:Z

    goto :goto_0

    :cond_2
    iget-boolean v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->P:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->L:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    invoke-static {v1}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->d(Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private z()V
    .locals 3

    .prologue
    .line 2357607
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->aj:LX/0yG;

    invoke-static {v0}, LX/1rv;->b(LX/0yG;)Z

    move-result v1

    .line 2357608
    iget-object v2, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->t:Lcom/facebook/location/ui/LocationSettingsOffView;

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/location/ui/LocationSettingsOffView;->setVisibility(I)V

    .line 2357609
    if-nez v1, :cond_1

    .line 2357610
    :goto_1
    return-void

    .line 2357611
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 2357612
    :cond_1
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->aj:LX/0yG;

    sget-object v1, LX/0yG;->LOCATION_DISABLED:LX/0yG;

    if-ne v0, v1, :cond_2

    const v0, 0x7f08311e

    .line 2357613
    :goto_2
    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->aj:LX/0yG;

    sget-object v2, LX/0yG;->LOCATION_DISABLED:LX/0yG;

    if-ne v1, v2, :cond_3

    const v1, 0x7f083120

    .line 2357614
    :goto_3
    iget-object v2, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->t:Lcom/facebook/location/ui/LocationSettingsOffView;

    invoke-virtual {v2, v0}, Lcom/facebook/location/ui/LocationSettingsOffView;->setDescriptionText(I)V

    .line 2357615
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->t:Lcom/facebook/location/ui/LocationSettingsOffView;

    invoke-virtual {v0, v1}, Lcom/facebook/location/ui/LocationSettingsOffView;->setButtonText(I)V

    goto :goto_1

    .line 2357616
    :cond_2
    const v0, 0x7f08311f

    goto :goto_2

    .line 2357617
    :cond_3
    const v1, 0x7f083121

    goto :goto_3
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2357618
    const-string v0, "friends_nearby_settings"

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2357619
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2357620
    invoke-static {p0, p0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2357621
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->Y:LX/0i4;

    invoke-virtual {v0, p0}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->Z:LX/0i5;

    .line 2357622
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->aa:LX/6Zb;

    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ab:LX/6Ze;

    invoke-virtual {v0, p0, v1}, LX/6Zb;->a(Lcom/facebook/base/activity/FbFragmentActivity;LX/6ZZ;)V

    .line 2357623
    const v0, 0x7f030165

    invoke-virtual {p0, v0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->setContentView(I)V

    .line 2357624
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2357625
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->c(I)LX/0am;

    move-result-object v0

    .line 2357626
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2357627
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2357628
    const v1, 0x7f08310f

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 2357629
    new-instance v1, LX/GUc;

    invoke-direct {v1, p0}, LX/GUc;-><init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2357630
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ak:Z

    .line 2357631
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->X:LX/GUl;

    sget-object v1, LX/GUk;->OVERALL_TTI:LX/GUk;

    invoke-virtual {v0, v1}, LX/GUl;->a(LX/GUk;)V

    .line 2357632
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->X:LX/GUl;

    sget-object v1, LX/GUk;->INIT:LX/GUk;

    invoke-virtual {v0, v1}, LX/GUl;->a(LX/GUk;)V

    .line 2357633
    const v0, 0x7f0d0673

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->F:Landroid/view/View;

    .line 2357634
    const v0, 0x7f0d0664

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->G:Landroid/view/View;

    .line 2357635
    const v0, 0x7f0d0675

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->D:Landroid/view/View;

    .line 2357636
    const v0, 0x7f0d0676

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->E:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;

    .line 2357637
    const v0, 0x7f0d0670

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->H:Landroid/view/View;

    .line 2357638
    const v0, 0x7f0d0671

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->I:Landroid/widget/TextView;

    .line 2357639
    const v0, 0x7f0d0672

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->J:Landroid/widget/TextView;

    .line 2357640
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->m()V

    .line 2357641
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->l()V

    .line 2357642
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->o()V

    .line 2357643
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->p()V

    .line 2357644
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2357645
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2357646
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 2357647
    invoke-direct {p0, p2}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->b(I)V

    .line 2357648
    :cond_0
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x22

    const v2, -0x22fa85d1

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2357649
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2357650
    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->aa:LX/6Zb;

    if-nez v1, :cond_0

    .line 2357651
    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->S:LX/03V;

    const-string v2, "background_location_settings_destroy_fail"

    const-string v3, "gms upsell dialog controllder is empty"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2357652
    :goto_0
    const v1, 0x18e6bb6b

    invoke-static {v1, v0}, LX/02F;->c(II)V

    return-void

    .line 2357653
    :cond_0
    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->aa:LX/6Zb;

    invoke-virtual {v1}, LX/6Zb;->a()V

    goto :goto_0
.end method

.method public final onPause()V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x2d513bba

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2357654
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2357655
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ak:Z

    .line 2357656
    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->q:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2357657
    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->X:LX/GUl;

    .line 2357658
    invoke-static {}, LX/GUk;->values()[LX/GUk;

    move-result-object v4

    array-length v5, v4

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    .line 2357659
    iget-object v7, v1, LX/GUl;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    iget v8, v6, LX/GUk;->eventId:I

    iget-object p0, v6, LX/GUk;->markerName:Ljava/lang/String;

    invoke-interface {v7, v8, p0}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    .line 2357660
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2357661
    :cond_0
    const/16 v1, 0x23

    const v2, -0x2f2c541e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x2

    const/16 v3, 0x22

    const v4, -0x1abd3553

    invoke-static {v0, v3, v4}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2357662
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2357663
    iget-boolean v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ak:Z

    if-nez v0, :cond_0

    .line 2357664
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->X:LX/GUl;

    sget-object v4, LX/GUk;->INIT:LX/GUk;

    invoke-virtual {v0, v4}, LX/GUl;->b(LX/GUk;)V

    .line 2357665
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->X:LX/GUl;

    sget-object v4, LX/GUk;->FETCH_DATA:LX/GUk;

    invoke-virtual {v0, v4}, LX/GUl;->a(LX/GUk;)V

    .line 2357666
    :cond_0
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->aj:LX/0yG;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->aj:LX/0yG;

    sget-object v4, LX/0yG;->OKAY:LX/0yG;

    if-ne v0, v4, :cond_3

    :cond_1
    move v0, v2

    .line 2357667
    :goto_0
    iget-object v4, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->T:LX/0y3;

    invoke-virtual {v4}, LX/0y3;->a()LX/0yG;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->aj:LX/0yG;

    .line 2357668
    iget-object v4, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->aj:LX/0yG;

    sget-object v5, LX/0yG;->OKAY:LX/0yG;

    if-ne v4, v5, :cond_4

    .line 2357669
    :goto_1
    iget-object v1, p0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ah:LX/03R;

    sget-object v4, LX/03R;->YES:LX/03R;

    if-ne v1, v4, :cond_2

    if-eqz v0, :cond_2

    if-nez v2, :cond_2

    .line 2357670
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->H()V

    .line 2357671
    :cond_2
    invoke-static {p0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->x(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    .line 2357672
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->z()V

    .line 2357673
    const v0, -0x3da8a1ee

    invoke-static {v0, v3}, LX/02F;->c(II)V

    return-void

    :cond_3
    move v0, v1

    .line 2357674
    goto :goto_0

    :cond_4
    move v2, v1

    .line 2357675
    goto :goto_1
.end method
