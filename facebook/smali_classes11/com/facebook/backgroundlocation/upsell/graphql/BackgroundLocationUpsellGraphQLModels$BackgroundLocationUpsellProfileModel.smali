.class public final Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x25a67f62
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2359178
    const-class v0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2359177
    const-class v0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2359175
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2359176
    return-void
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2359173
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    .line 2359174
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2359161
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2359162
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2359163
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->d()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x505bc130

    invoke-static {v2, v1, v3}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2359164
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2359165
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2359166
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2359167
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2359168
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2359169
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2359170
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2359171
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2359172
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2359146
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2359147
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->d()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2359148
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->d()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x505bc130

    invoke-static {v2, v0, v3}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2359149
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->d()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2359150
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;

    .line 2359151
    iput v3, v0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->f:I

    move-object v1, v0

    .line 2359152
    :cond_0
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2359153
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    .line 2359154
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2359155
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;

    .line 2359156
    iput-object v0, v1, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    .line 2359157
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2359158
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 2359159
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 2359160
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2359145
    new-instance v0, LX/GVN;

    invoke-direct {v0, p1}, LX/GVN;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2359144
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2359127
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2359128
    const/4 v0, 0x1

    const v1, -0x505bc130

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->f:I

    .line 2359129
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2359142
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2359143
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2359141
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2359138
    new-instance v0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;

    invoke-direct {v0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;-><init>()V

    .line 2359139
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2359140
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2359136
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->e:Ljava/lang/String;

    .line 2359137
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2359134
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->g:Ljava/lang/String;

    .line 2359135
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2359132
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2359133
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2359131
    const v0, -0x4b89f65c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2359130
    const v0, 0x285feb

    return v0
.end method
