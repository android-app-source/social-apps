.class public Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/6ZZ;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation

.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# static fields
.field public static final A:Ljava/lang/String;


# instance fields
.field private B:Landroid/view/View;

.field private C:Landroid/view/View;

.field private D:Landroid/view/View;

.field public E:Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;

.field public F:Landroid/widget/TextView;

.field private G:Landroid/widget/Button;

.field private H:Landroid/widget/Button;

.field private I:Landroid/widget/TextView;

.field private J:Landroid/view/View;

.field public p:LX/GVF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0y3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/6Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/2bj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/GTt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2358675
    sget-object v0, LX/0ax;->dp:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "/tour/locationsharing/learnmore"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->A:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2358676
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2358677
    return-void
.end method

.method private static a(Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;LX/GVF;LX/0tX;LX/1Ck;LX/0y3;LX/0i4;LX/6Zb;LX/17W;LX/03V;Lcom/facebook/content/SecureContextHelper;LX/2bj;LX/GTt;)V
    .locals 0

    .prologue
    .line 2358678
    iput-object p1, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->p:LX/GVF;

    iput-object p2, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->q:LX/0tX;

    iput-object p3, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->r:LX/1Ck;

    iput-object p4, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->s:LX/0y3;

    iput-object p5, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->t:LX/0i4;

    iput-object p6, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->u:LX/6Zb;

    iput-object p7, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->v:LX/17W;

    iput-object p8, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->w:LX/03V;

    iput-object p9, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->x:Lcom/facebook/content/SecureContextHelper;

    iput-object p10, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->y:LX/2bj;

    iput-object p11, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->z:LX/GTt;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 12

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v11

    move-object v0, p0

    check-cast v0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;

    invoke-static {v11}, LX/GVF;->a(LX/0QB;)LX/GVF;

    move-result-object v1

    check-cast v1, LX/GVF;

    invoke-static {v11}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {v11}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v11}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v4

    check-cast v4, LX/0y3;

    const-class v5, LX/0i4;

    invoke-interface {v11, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/0i4;

    invoke-static {v11}, LX/6Zb;->b(LX/0QB;)LX/6Zb;

    move-result-object v6

    check-cast v6, LX/6Zb;

    invoke-static {v11}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v7

    check-cast v7, LX/17W;

    invoke-static {v11}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {v11}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v11}, LX/2bj;->a(LX/0QB;)LX/2bj;

    move-result-object v10

    check-cast v10, LX/2bj;

    invoke-static {v11}, LX/GTt;->a(LX/0QB;)LX/GTt;

    move-result-object v11

    check-cast v11, LX/GTt;

    invoke-static/range {v0 .. v11}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->a(Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;LX/GVF;LX/0tX;LX/1Ck;LX/0y3;LX/0i4;LX/6Zb;LX/17W;LX/03V;Lcom/facebook/content/SecureContextHelper;LX/2bj;LX/GTt;)V

    return-void
.end method

.method public static b(LX/0Px;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLInterfaces$BackgroundLocationUpsellProfile$;",
            ">;)",
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2358686
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2358687
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_3

    invoke-virtual {p0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;

    .line 2358688
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->d()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->d()LX/1vs;

    move-result-object v1

    iget-object v6, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v6, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_0

    .line 2358689
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->d()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2358690
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v1, v2

    .line 2358691
    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_1

    .line 2358692
    :cond_3
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static b$redex0(Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;)V
    .locals 2

    .prologue
    .line 2358679
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2358680
    sget-object v1, LX/0ax;->eb:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2358681
    iget-object v1, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->x:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2358682
    return-void
.end method

.method public static l(Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;)V
    .locals 4

    .prologue
    .line 2358683
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->s:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->a()LX/0yG;

    move-result-object v0

    .line 2358684
    iget-object v1, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->t:LX/0i4;

    invoke-virtual {v1, p0}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v1

    sget-object v2, LX/3KE;->a:[Ljava/lang/String;

    new-instance v3, LX/GV5;

    invoke-direct {v3, p0, v0}, LX/GV5;-><init>(Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;LX/0yG;)V

    invoke-virtual {v1, v2, v3}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 2358685
    return-void
.end method

.method private m()V
    .locals 2

    .prologue
    .line 2358672
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2358673
    iget-object v1, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->x:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2358674
    return-void
.end method

.method private n()V
    .locals 2

    .prologue
    .line 2358614
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->B:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2358615
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->J:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2358616
    return-void
.end method

.method public static o(Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;)V
    .locals 2

    .prologue
    .line 2358669
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->B:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2358670
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->J:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2358671
    return-void
.end method

.method private p()V
    .locals 4

    .prologue
    .line 2358662
    new-instance v0, LX/GVK;

    invoke-direct {v0}, LX/GVK;-><init>()V

    move-object v0, v0

    .line 2358663
    const-string v1, "image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2358664
    const-string v1, "image_size"

    const/16 v2, 0x40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2358665
    const-string v1, "n_upsell_results"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2358666
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2358667
    iget-object v1, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->r:LX/1Ck;

    const-string v2, "fetch_upsell_data"

    iget-object v3, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->q:LX/0tX;

    invoke-virtual {v3, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v3, LX/GV6;

    invoke-direct {v3, p0}, LX/GV6;-><init>(Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2358668
    return-void
.end method


# virtual methods
.method public final a(LX/6ZY;)V
    .locals 2

    .prologue
    .line 2358652
    sget-object v0, LX/GV7;->a:[I

    invoke-virtual {p1}, LX/6ZY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2358653
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Illegal location upsell dialog result."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2358654
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->z:LX/GTt;

    invoke-virtual {v0}, LX/GTt;->b()V

    .line 2358655
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->finish()V

    .line 2358656
    :goto_0
    return-void

    .line 2358657
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->z:LX/GTt;

    invoke-virtual {v0}, LX/GTt;->c()V

    .line 2358658
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->finish()V

    goto :goto_0

    .line 2358659
    :pswitch_2
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->finish()V

    goto :goto_0

    .line 2358660
    :pswitch_3
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->m()V

    .line 2358661
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v1, 0x400

    .line 2358627
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2358628
    invoke-static {p0, p0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2358629
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 2358630
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->u:LX/6Zb;

    invoke-virtual {v0, p0, p0}, LX/6Zb;->a(Lcom/facebook/base/activity/FbFragmentActivity;LX/6ZZ;)V

    .line 2358631
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->y:LX/2bj;

    .line 2358632
    iget-object v3, v0, LX/2bj;->b:Lcom/facebook/common/time/RealtimeSinceBootClock;

    invoke-virtual {v3}, Lcom/facebook/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v3

    iput-wide v3, v0, LX/2bj;->c:J

    .line 2358633
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->z:LX/GTt;

    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "source"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "resurrection"

    invoke-virtual {v0, v1, v2}, LX/GTt;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2358634
    const v0, 0x7f03015e

    invoke-virtual {p0, v0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->setContentView(I)V

    .line 2358635
    const v0, 0x7f0d0655

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->B:Landroid/view/View;

    .line 2358636
    const v0, 0x7f0d0656

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->C:Landroid/view/View;

    .line 2358637
    const v0, 0x7f0d0657

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->D:Landroid/view/View;

    .line 2358638
    const v0, 0x7f0d0658

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->E:Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;

    .line 2358639
    const v0, 0x7f0d0659

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->F:Landroid/widget/TextView;

    .line 2358640
    const v0, 0x7f0d065a

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->G:Landroid/widget/Button;

    .line 2358641
    const v0, 0x7f0d065b

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->H:Landroid/widget/Button;

    .line 2358642
    const v0, 0x7f0d065c

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->I:Landroid/widget/TextView;

    .line 2358643
    const v0, 0x7f0d065d

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->J:Landroid/view/View;

    .line 2358644
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->C:Landroid/view/View;

    new-instance v1, LX/GV0;

    invoke-direct {v1, p0}, LX/GV0;-><init>(Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2358645
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->D:Landroid/view/View;

    new-instance v1, LX/GV1;

    invoke-direct {v1, p0}, LX/GV1;-><init>(Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2358646
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->G:Landroid/widget/Button;

    new-instance v1, LX/GV2;

    invoke-direct {v1, p0}, LX/GV2;-><init>(Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2358647
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->H:Landroid/widget/Button;

    new-instance v1, LX/GV3;

    invoke-direct {v1, p0}, LX/GV3;-><init>(Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2358648
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->I:Landroid/widget/TextView;

    new-instance v1, LX/GV4;

    invoke-direct {v1, p0}, LX/GV4;-><init>(Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2358649
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->n()V

    .line 2358650
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->p()V

    .line 2358651
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x2d9d9b99

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2358621
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2358622
    iget-object v1, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->u:LX/6Zb;

    if-eqz v1, :cond_0

    .line 2358623
    iget-object v1, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->u:LX/6Zb;

    invoke-virtual {v1}, LX/6Zb;->a()V

    .line 2358624
    :cond_0
    iget-object v1, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->r:LX/1Ck;

    if-eqz v1, :cond_1

    .line 2358625
    iget-object v1, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->r:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2358626
    :cond_1
    const/16 v1, 0x23

    const v2, 0x55d36339

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x5265f7c4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2358617
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2358618
    iget-object v1, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->s:LX/0y3;

    invoke-virtual {v1}, LX/0y3;->a()LX/0yG;

    move-result-object v1

    sget-object v2, LX/0yG;->OKAY:LX/0yG;

    if-ne v1, v2, :cond_0

    .line 2358619
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->finish()V

    .line 2358620
    :cond_0
    const/16 v1, 0x23

    const v2, 0x348d577f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
