.class public Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:I

.field public b:LX/GUa;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2358810
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2358811
    new-instance v0, LX/GVG;

    invoke-direct {v0, p0}, LX/GVG;-><init>(Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;)V

    iput-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;->c:Landroid/view/View$OnClickListener;

    .line 2358812
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2358807
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2358808
    new-instance v0, LX/GVG;

    invoke-direct {v0, p0}, LX/GVG;-><init>(Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;)V

    iput-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;->c:Landroid/view/View$OnClickListener;

    .line 2358809
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2358804
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2358805
    new-instance v0, LX/GVG;

    invoke-direct {v0, p0}, LX/GVG;-><init>(Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;)V

    iput-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;->c:Landroid/view/View$OnClickListener;

    .line 2358806
    return-void
.end method

.method private getFriendsSharingChild()LX/GVC;
    .locals 2

    .prologue
    .line 2358797
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2358798
    instance-of v1, v0, LX/GVC;

    if-eqz v1, :cond_0

    .line 2358799
    check-cast v0, LX/GVC;

    .line 2358800
    :goto_0
    return-object v0

    .line 2358801
    :cond_0
    new-instance v0, LX/GVC;

    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/GVC;-><init>(Landroid/content/Context;)V

    .line 2358802
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;->removeAllViews()V

    .line 2358803
    invoke-virtual {p0, v0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private getNoFriendsSharingChild()LX/GVD;
    .locals 2

    .prologue
    .line 2358790
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2358791
    instance-of v1, v0, LX/GVD;

    if-eqz v1, :cond_0

    .line 2358792
    check-cast v0, LX/GVD;

    .line 2358793
    :goto_0
    return-object v0

    .line 2358794
    :cond_0
    new-instance v0, LX/GVD;

    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/GVD;-><init>(Landroid/content/Context;)V

    .line 2358795
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;->removeAllViews()V

    .line 2358796
    invoke-virtual {p0, v0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;->addView(Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(ILX/0Px;LX/GUa;)V
    .locals 2
    .param p3    # LX/GUa;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "setParams"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/0Px",
            "<+",
            "Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLInterfaces$BackgroundLocationUpsellProfile$;",
            ">;",
            "LX/GUa;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2358771
    iput p1, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;->a:I

    .line 2358772
    iput-object p3, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;->b:LX/GUa;

    .line 2358773
    if-nez p3, :cond_0

    const/4 v0, 0x0

    .line 2358774
    :goto_0
    const/4 v1, 0x1

    if-gt p1, v1, :cond_1

    .line 2358775
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;->getNoFriendsSharingChild()LX/GVD;

    move-result-object v1

    .line 2358776
    iget-object p1, v1, LX/GVD;->a:Landroid/widget/Button;

    if-nez v0, :cond_2

    const/16 p0, 0x8

    :goto_1
    invoke-virtual {p1, p0}, Landroid/widget/Button;->setVisibility(I)V

    .line 2358777
    iget-object p0, v1, LX/GVD;->a:Landroid/widget/Button;

    invoke-virtual {p0, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2358778
    :goto_2
    return-void

    .line 2358779
    :cond_0
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;->c:Landroid/view/View$OnClickListener;

    goto :goto_0

    .line 2358780
    :cond_1
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;->getFriendsSharingChild()LX/GVC;

    move-result-object v1

    .line 2358781
    iget-object p0, v1, LX/GVC;->b:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-static {p0, p2}, LX/GVB;->a(Lcom/facebook/fbui/facepile/FacepileView;LX/0Px;)V

    .line 2358782
    iget-object p0, v1, LX/GVC;->c:Landroid/widget/TextView;

    iget-object p3, v1, LX/GVC;->a:LX/GVF;

    invoke-virtual {p3, p1, p2}, LX/GVF;->a(ILX/0Px;)Landroid/text/SpannableString;

    move-result-object p3

    invoke-virtual {p0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2358783
    iget-object p3, v1, LX/GVC;->d:Landroid/widget/Button;

    if-nez v0, :cond_3

    const/16 p0, 0x8

    :goto_3
    invoke-virtual {p3, p0}, Landroid/widget/Button;->setVisibility(I)V

    .line 2358784
    iget-object p0, v1, LX/GVC;->d:Landroid/widget/Button;

    invoke-virtual {p0, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2358785
    goto :goto_2

    .line 2358786
    :cond_2
    const/4 p0, 0x0

    goto :goto_1

    .line 2358787
    :cond_3
    const/4 p0, 0x0

    goto :goto_3
.end method

.method public getDesignName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2358789
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/GVA;

    invoke-interface {v0}, LX/GV9;->getDesignName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTotalFriendsSharing()I
    .locals 1

    .prologue
    .line 2358788
    iget v0, p0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationUpsellView;->a:I

    return v0
.end method
