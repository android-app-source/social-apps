.class public Lcom/facebook/backgroundlocation/upsell/UpsellContainerActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/6ZZ;


# instance fields
.field public p:LX/6Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/2bj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2358963
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 2358953
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/UpsellContainerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2358954
    if-nez v0, :cond_0

    .line 2358955
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/UpsellContainerActivity;->finish()V

    .line 2358956
    :goto_0
    return-void

    .line 2358957
    :cond_0
    const-string v1, "gms_dialog_surface"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2358958
    const-string v2, "gms_dialog_mechanism"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2358959
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2358960
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/UpsellContainerActivity;->finish()V

    goto :goto_0

    .line 2358961
    :cond_2
    iget-object v2, p0, Lcom/facebook/backgroundlocation/upsell/UpsellContainerActivity;->p:LX/6Zb;

    new-instance v3, LX/2si;

    invoke-direct {v3}, LX/2si;-><init>()V

    invoke-virtual {v2, v3, v1, v0}, LX/6Zb;->a(LX/2si;Ljava/lang/String;Ljava/lang/String;)V

    .line 2358962
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/UpsellContainerActivity;->q:LX/2bj;

    const-string v1, "location_opt_in_google_play_location_dialog_triggered"

    invoke-virtual {v0, v1}, LX/2bj;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/backgroundlocation/upsell/UpsellContainerActivity;LX/6Zb;LX/2bj;)V
    .locals 0

    .prologue
    .line 2358952
    iput-object p1, p0, Lcom/facebook/backgroundlocation/upsell/UpsellContainerActivity;->p:LX/6Zb;

    iput-object p2, p0, Lcom/facebook/backgroundlocation/upsell/UpsellContainerActivity;->q:LX/2bj;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/backgroundlocation/upsell/UpsellContainerActivity;

    invoke-static {v1}, LX/6Zb;->b(LX/0QB;)LX/6Zb;

    move-result-object v0

    check-cast v0, LX/6Zb;

    invoke-static {v1}, LX/2bj;->a(LX/0QB;)LX/2bj;

    move-result-object v1

    check-cast v1, LX/2bj;

    invoke-static {p0, v0, v1}, Lcom/facebook/backgroundlocation/upsell/UpsellContainerActivity;->a(Lcom/facebook/backgroundlocation/upsell/UpsellContainerActivity;LX/6Zb;LX/2bj;)V

    return-void
.end method


# virtual methods
.method public final a(LX/6ZY;)V
    .locals 3

    .prologue
    .line 2358944
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/UpsellContainerActivity;->q:LX/2bj;

    .line 2358945
    sget-object v1, LX/GV8;->b:[I

    invoke-virtual {p1}, LX/6ZY;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2358946
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/UpsellContainerActivity;->finish()V

    .line 2358947
    return-void

    .line 2358948
    :pswitch_0
    const-string v1, "location_opt_in_google_play_location_success"

    invoke-virtual {v0, v1}, LX/2bj;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2358949
    :pswitch_1
    const-string v1, "location_opt_in_google_play_location_failed"

    invoke-virtual {v0, v1}, LX/2bj;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2358950
    :pswitch_2
    const-string v1, "location_opt_in_google_play_location_not_needed"

    invoke-virtual {v0, v1}, LX/2bj;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2358951
    :pswitch_3
    const-string v1, "location_opt_in_google_play_location_not_possible"

    invoke-virtual {v0, v1}, LX/2bj;->a(Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2358938
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2358939
    invoke-static {p0, p0}, Lcom/facebook/backgroundlocation/upsell/UpsellContainerActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2358940
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/UpsellContainerActivity;->p:LX/6Zb;

    invoke-virtual {v0, p0, p0}, LX/6Zb;->a(Lcom/facebook/base/activity/FbFragmentActivity;LX/6ZZ;)V

    .line 2358941
    if-nez p1, :cond_0

    .line 2358942
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/upsell/UpsellContainerActivity;->a()V

    .line 2358943
    :cond_0
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x6747ae2e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2358934
    iget-object v1, p0, Lcom/facebook/backgroundlocation/upsell/UpsellContainerActivity;->p:LX/6Zb;

    if-eqz v1, :cond_0

    .line 2358935
    iget-object v1, p0, Lcom/facebook/backgroundlocation/upsell/UpsellContainerActivity;->p:LX/6Zb;

    invoke-virtual {v1}, LX/6Zb;->a()V

    .line 2358936
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2358937
    const/16 v1, 0x23

    const v2, -0x431e6f67

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
