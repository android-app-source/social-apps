.class public Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:[D

.field private static final c:[F

.field private static final d:[J

.field private static final e:[J


# instance fields
.field private f:Landroid/content/res/Resources;

.field private g:I

.field private h:I

.field private i:I

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/animation/AnimatorSet;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 2358918
    const-class v0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;

    const-string v1, "background_location"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2358919
    new-array v0, v2, [D

    fill-array-data v0, :array_0

    sput-object v0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->b:[D

    .line 2358920
    new-array v0, v2, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->c:[F

    .line 2358921
    new-array v0, v2, [J

    fill-array-data v0, :array_2

    sput-object v0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->d:[J

    .line 2358922
    new-array v0, v2, [J

    fill-array-data v0, :array_3

    sput-object v0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->e:[J

    return-void

    nop

    :array_0
    .array-data 8
        0x3fdccccccccccccdL    # 0.45
        0x3fd0000000000000L    # 0.25
        0x3fe6666666666666L    # 0.7
        0x3fee666666666666L    # 0.95
        0x3fe0f5c28f5c28f6L    # 0.53
        0x3fc3333333333333L    # 0.15
        0x3fe4cccccccccccdL    # 0.65
        0x3fdccccccccccccdL    # 0.45
        0x3fd3333333333333L    # 0.3
        0x3fe3333333333333L    # 0.6
    .end array-data

    .line 2358923
    :array_1
    .array-data 4
        0x3f666666    # 0.9f
        0x3f000000    # 0.5f
        0x3e4ccccd    # 0.2f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    .line 2358924
    :array_2
    .array-data 8
        0x0
        0x0
        0x0
        0x190
        0x3e8
        0x640
        0x7d0
        0x960
        0xd48
        0xe74
    .end array-data

    .line 2358925
    :array_3
    .array-data 8
        0x0
        0x4b0
        0x640
        0x640
        0x640
        0x640
        0x640
        0x640
        0x640
        0x640
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2358915
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2358916
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->a()V

    .line 2358917
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2358873
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2358874
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->a()V

    .line 2358875
    return-void
.end method

.method private a(Landroid/view/View;FJJ)Landroid/animation/AnimatorSet;
    .locals 7

    .prologue
    .line 2358890
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 2358891
    :goto_0
    sget-object v2, Landroid/view/View;->ROTATION:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v3, v1, [F

    const/4 v1, 0x0

    const/4 v4, 0x0

    aput v4, v3, v1

    const/4 v4, 0x1

    if-eqz v0, :cond_1

    const/high16 v1, -0x3e600000    # -20.0f

    :goto_1
    aput v1, v3, v4

    invoke-static {p1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 2358892
    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 2358893
    sget-object v3, Landroid/view/View;->ROTATION:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v4, v1, [F

    const/4 v5, 0x0

    if-eqz v0, :cond_2

    const/high16 v1, -0x3e600000    # -20.0f

    :goto_2
    aput v1, v4, v5

    const/4 v1, 0x1

    if-eqz v0, :cond_3

    const/high16 v0, 0x41a00000    # 20.0f

    :goto_3
    aput v0, v4, v1

    invoke-static {p1, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 2358894
    const-wide/16 v4, 0x7d0

    invoke-virtual {v0, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 2358895
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    .line 2358896
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 2358897
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 2358898
    const/4 v3, 0x2

    new-array v3, v3, [Landroid/animation/Animator;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    const/4 v2, 0x1

    aput-object v0, v3, v2

    invoke-virtual {v1, v3}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 2358899
    sget-object v0, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    iget v4, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->g:I

    neg-int v4, v4

    int-to-float v4, v4

    mul-float/2addr v4, p2

    aput v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->g:I

    int-to-float v4, v4

    mul-float/2addr v4, p2

    iget v5, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->i:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    iget v5, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->g:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    neg-float v4, v4

    aput v4, v2, v3

    invoke-static {p1, v0, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 2358900
    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 2358901
    sget-object v2, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    const/4 v5, 0x0

    aput v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->i:I

    iget v6, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->g:I

    add-int/2addr v5, v6

    neg-int v5, v5

    int-to-float v5, v5

    aput v5, v3, v4

    invoke-static {p1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 2358902
    const-wide/16 v4, 0x2710

    invoke-virtual {v2, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 2358903
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    .line 2358904
    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 2358905
    invoke-virtual {v2, p5, p6}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 2358906
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 2358907
    const/4 v4, 0x2

    new-array v4, v4, [Landroid/animation/Animator;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v2, v4, v0

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 2358908
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 2358909
    const/4 v2, 0x2

    new-array v2, v2, [Landroid/animation/Animator;

    const/4 v4, 0x0

    aput-object v1, v2, v4

    const/4 v1, 0x1

    aput-object v3, v2, v1

    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 2358910
    invoke-virtual {v0, p3, p4}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 2358911
    return-object v0

    .line 2358912
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2358913
    :cond_1
    const/high16 v1, 0x41a00000    # 20.0f

    goto/16 :goto_1

    .line 2358914
    :cond_2
    const/high16 v1, 0x41a00000    # 20.0f

    goto/16 :goto_2

    :cond_3
    const/high16 v0, -0x3e600000    # -20.0f

    goto/16 :goto_3
.end method

.method private a(D)Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 2358876
    new-instance v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 2358877
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    iget v2, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->g:I

    iget v3, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->g:I

    const/16 v4, 0x50

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 2358878
    iget v2, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->h:I

    int-to-double v2, v2

    mul-double/2addr v2, p1

    double-to-int v2, v2

    iget v3, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->g:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    .line 2358879
    iget v3, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->g:I

    neg-int v3, v3

    invoke-virtual {v1, v2, v5, v5, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 2358880
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2358881
    iget-object v1, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->f:Landroid/content/res/Resources;

    const v3, 0x7f0a00d5

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 2358882
    iget-object v3, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->f:Landroid/content/res/Resources;

    const v4, 0x7f0b0b51

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2358883
    new-instance v4, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v4, v5}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v5

    int-to-float v3, v3

    invoke-virtual {v5, v1, v3}, LX/4Ab;->a(IF)LX/4Ab;

    move-result-object v1

    .line 2358884
    iput-object v1, v4, LX/1Uo;->u:LX/4Ab;

    .line 2358885
    move-object v1, v4

    .line 2358886
    const v3, 0x7f0203b2

    invoke-virtual {v1, v3}, LX/1Uo;->b(I)LX/1Uo;

    move-result-object v1

    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    .line 2358887
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2358888
    iget v1, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->i:I

    iget v3, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->g:I

    add-int/2addr v3, v2

    iget v4, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->i:I

    iget v5, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->g:I

    add-int/2addr v4, v5

    invoke-virtual {v0, v2, v1, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->layout(IIII)V

    .line 2358889
    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2358926
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->removeAllViews()V

    .line 2358927
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->f:Landroid/content/res/Resources;

    .line 2358928
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0b50

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->g:I

    .line 2358929
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2358930
    iput-object v0, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->j:Ljava/util/List;

    .line 2358931
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->k:Ljava/util/List;

    .line 2358932
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->l:Ljava/util/List;

    .line 2358933
    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    .line 2358857
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 2358858
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    .line 2358859
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->k:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->j:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    sget-object v4, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2358860
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->l:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 2358861
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2358862
    :cond_0
    return-void
.end method

.method private e()V
    .locals 9

    .prologue
    .line 2358847
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->getHeight()I

    move-result v0

    if-nez v0, :cond_1

    .line 2358848
    :cond_0
    return-void

    .line 2358849
    :cond_1
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2358850
    iget-object v0, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2358851
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    .line 2358852
    sget-object v1, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->b:[D

    aget-wide v2, v1, v0

    invoke-direct {p0, v2, v3}, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->a(D)Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v2

    .line 2358853
    invoke-virtual {p0, v2}, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->addView(Landroid/view/View;)V

    .line 2358854
    iget-object v1, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->k:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2358855
    iget-object v8, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->l:Ljava/util/List;

    sget-object v1, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->c:[F

    aget v3, v1, v0

    sget-object v1, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->d:[J

    aget-wide v4, v1, v0

    sget-object v1, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->e:[J

    aget-wide v6, v1, v0

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->a(Landroid/view/View;FJJ)Landroid/animation/AnimatorSet;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2358856
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 2358863
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomFrameLayout;->onLayout(ZIIII)V

    .line 2358864
    if-eqz p1, :cond_0

    .line 2358865
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->h:I

    .line 2358866
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->i:I

    .line 2358867
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->e()V

    .line 2358868
    :cond_0
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->b()V

    .line 2358869
    return-void
.end method

.method public setProfileImages(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2358870
    iput-object p1, p0, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->j:Ljava/util/List;

    .line 2358871
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->requestLayout()V

    .line 2358872
    return-void
.end method
