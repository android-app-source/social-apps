.class public final Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2356663
    const-class v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;

    new-instance v1, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2356664
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2356635
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2356637
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2356638
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2356639
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2356640
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2356641
    if-eqz v2, :cond_0

    .line 2356642
    const-string p0, "actor"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2356643
    invoke-static {v1, v2, p1, p2}, LX/GU5;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2356644
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2356645
    if-eqz v2, :cond_1

    .line 2356646
    const-string p0, "current_location_page"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2356647
    invoke-static {v1, v2, p1, p2}, LX/GU6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2356648
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2356649
    if-eqz v2, :cond_2

    .line 2356650
    const-string p0, "location_sharing"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2356651
    invoke-static {v1, v2, p1, p2}, LX/GU7;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2356652
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2356653
    if-eqz v2, :cond_4

    .line 2356654
    const-string p0, "privacy_settings"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2356655
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2356656
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 2356657
    if-eqz p0, :cond_3

    .line 2356658
    const-string v0, "location_privacy_options"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2356659
    invoke-static {v1, p0, p1, p2}, LX/GUG;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2356660
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2356661
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2356662
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2356636
    check-cast p1, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$Serializer;->a(Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
