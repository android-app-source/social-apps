.class public final Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1a185a67
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private h:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2356631
    const-class v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2356630
    const-class v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2356628
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2356629
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2356618
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2356619
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->j()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2356620
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->k()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2356621
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2356622
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2356623
    const/4 v0, 0x1

    iget-boolean v2, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->f:Z

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 2356624
    const/4 v0, 0x2

    iget-boolean v2, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->g:Z

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 2356625
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2356626
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2356627
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2356595
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2356596
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->j()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2356597
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->j()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    .line 2356598
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->j()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2356599
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;

    .line 2356600
    iput-object v0, v1, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->e:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    .line 2356601
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->k()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2356602
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->k()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    .line 2356603
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->k()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2356604
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;

    .line 2356605
    iput-object v0, v1, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->h:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    .line 2356606
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2356607
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2356617
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->j()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2356613
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2356614
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->f:Z

    .line 2356615
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->g:Z

    .line 2356616
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2356632
    new-instance v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;

    invoke-direct {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;-><init>()V

    .line 2356633
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2356634
    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 2356611
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2356612
    iget-boolean v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->f:Z

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 2356609
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2356610
    iget-boolean v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->g:Z

    return v0
.end method

.method public final synthetic d()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2356608
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->k()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2356594
    const v0, -0x28773aa1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2356593
    const v0, 0x37207b47

    return v0
.end method

.method public final j()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2356591
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->e:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->e:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    .line 2356592
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->e:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2356589
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->h:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->h:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    .line 2356590
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->h:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    return-object v0
.end method
