.class public final Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2356520
    const-class v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;

    new-instance v1, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2356521
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2356522
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2356523
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2356524
    const/4 v2, 0x0

    .line 2356525
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_7

    .line 2356526
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2356527
    :goto_0
    move v1, v2

    .line 2356528
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2356529
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2356530
    new-instance v1, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;

    invoke-direct {v1}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;-><init>()V

    .line 2356531
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2356532
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2356533
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2356534
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2356535
    :cond_0
    return-object v1

    .line 2356536
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2356537
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 2356538
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2356539
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2356540
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, p0, :cond_2

    if-eqz v6, :cond_2

    .line 2356541
    const-string v7, "actor"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2356542
    invoke-static {p1, v0}, LX/GU5;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2356543
    :cond_3
    const-string v7, "current_location_page"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2356544
    invoke-static {p1, v0}, LX/GU6;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2356545
    :cond_4
    const-string v7, "location_sharing"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2356546
    invoke-static {p1, v0}, LX/GU7;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2356547
    :cond_5
    const-string v7, "privacy_settings"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2356548
    const/4 v6, 0x0

    .line 2356549
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v7, :cond_b

    .line 2356550
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2356551
    :goto_2
    move v1, v6

    .line 2356552
    goto :goto_1

    .line 2356553
    :cond_6
    const/4 v6, 0x4

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 2356554
    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 2356555
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 2356556
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2356557
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2356558
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_7
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    goto :goto_1

    .line 2356559
    :cond_8
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2356560
    :cond_9
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_a

    .line 2356561
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2356562
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2356563
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_9

    if-eqz v7, :cond_9

    .line 2356564
    const-string p0, "location_privacy_options"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 2356565
    invoke-static {p1, v0}, LX/GUG;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_3

    .line 2356566
    :cond_a
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 2356567
    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 2356568
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto :goto_2

    :cond_b
    move v1, v6

    goto :goto_3
.end method
