.class public final Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4d01df6d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$CurrentLocationPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2356704
    const-class v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2356705
    const-class v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2356720
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2356721
    return-void
.end method

.method private l()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$CurrentLocationPageModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCurrentLocationPage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2356706
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->f:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$CurrentLocationPageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$CurrentLocationPageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$CurrentLocationPageModel;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->f:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$CurrentLocationPageModel;

    .line 2356707
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->f:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$CurrentLocationPageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 2356708
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2356709
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->a()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2356710
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->l()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$CurrentLocationPageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2356711
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->j()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2356712
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->k()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x5f4941de

    invoke-static {v4, v3, v5}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2356713
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2356714
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2356715
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2356716
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2356717
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2356718
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2356719
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2356677
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2356678
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->a()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2356679
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->a()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel;

    .line 2356680
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->a()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2356681
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;

    .line 2356682
    iput-object v0, v1, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->e:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel;

    .line 2356683
    :cond_0
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->l()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$CurrentLocationPageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2356684
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->l()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$CurrentLocationPageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$CurrentLocationPageModel;

    .line 2356685
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->l()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$CurrentLocationPageModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2356686
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;

    .line 2356687
    iput-object v0, v1, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->f:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$CurrentLocationPageModel;

    .line 2356688
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->j()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2356689
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->j()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;

    .line 2356690
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->j()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2356691
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;

    .line 2356692
    iput-object v0, v1, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->g:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;

    .line 2356693
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 2356694
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x5f4941de

    invoke-static {v2, v0, v3}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2356695
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2356696
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;

    .line 2356697
    iput v3, v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->h:I

    move-object v1, v0

    .line 2356698
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2356699
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    .line 2356700
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_4
    move-object p0, v1

    .line 2356701
    goto :goto_0
.end method

.method public final a()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2356702
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->e:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->e:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel;

    .line 2356703
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->e:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2356674
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2356675
    const/4 v0, 0x3

    const v1, 0x5f4941de

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->h:I

    .line 2356676
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2356671
    new-instance v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;

    invoke-direct {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;-><init>()V

    .line 2356672
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2356673
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2356670
    const v0, 0x62e6e953

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2356669
    const v0, -0x6747e1ce

    return v0
.end method

.method public final j()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2356667
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->g:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->g:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;

    .line 2356668
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->g:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;

    return-object v0
.end method

.method public final k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPrivacySettings"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2356665
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2356666
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
