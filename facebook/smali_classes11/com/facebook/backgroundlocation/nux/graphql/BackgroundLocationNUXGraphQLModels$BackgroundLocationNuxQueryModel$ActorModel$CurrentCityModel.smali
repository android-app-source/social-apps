.class public final Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel$CurrentCityModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1cca35f3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel$CurrentCityModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel$CurrentCityModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2356386
    const-class v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel$CurrentCityModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2356385
    const-class v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel$CurrentCityModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2356383
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2356384
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2356381
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel$CurrentCityModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel$CurrentCityModel;->e:Ljava/lang/String;

    .line 2356382
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel$CurrentCityModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2356375
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2356376
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel$CurrentCityModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2356377
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2356378
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2356379
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2356380
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2356387
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2356388
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2356389
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2356366
    new-instance v0, LX/GTy;

    invoke-direct {v0, p1}, LX/GTy;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2356373
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2356374
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2356367
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2356368
    new-instance v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel$CurrentCityModel;

    invoke-direct {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel$CurrentCityModel;-><init>()V

    .line 2356369
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2356370
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2356371
    const v0, -0x74ccd12c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2356372
    const v0, 0x25d6af

    return v0
.end method
