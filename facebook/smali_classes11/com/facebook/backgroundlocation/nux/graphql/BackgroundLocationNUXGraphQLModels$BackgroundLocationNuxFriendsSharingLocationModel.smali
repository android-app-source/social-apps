.class public final Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x39c4e71a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2356343
    const-class v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2356318
    const-class v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2356341
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2356342
    return-void
.end method

.method private j()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2356339
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;->e:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;->e:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;

    .line 2356340
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;->e:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2356333
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2356334
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;->j()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2356335
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2356336
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2356337
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2356338
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2356325
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2356326
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;->j()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2356327
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;->j()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;

    .line 2356328
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;->j()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2356329
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    .line 2356330
    iput-object v0, v1, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;->e:Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;

    .line 2356331
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2356332
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2356324
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;->j()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2356321
    new-instance v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    invoke-direct {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;-><init>()V

    .line 2356322
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2356323
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2356320
    const v0, 0x56c41c67

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2356319
    const v0, -0x511a8984

    return v0
.end method
