.class public Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final B:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final C:Lcom/facebook/common/callercontext/CallerContext;

.field private static final D:Ljava/lang/String;


# instance fields
.field public A:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private E:Landroid/view/View;

.field private F:Landroid/view/View;

.field private G:Lcom/facebook/fbui/glyph/GlyphView;

.field private H:Landroid/widget/TextView;

.field public I:Lcom/facebook/fbui/facepile/FacepileView;

.field public J:Landroid/view/View;

.field public K:Lcom/facebook/fbui/popover/PopoverSpinner;

.field private L:Landroid/widget/TextView;

.field private M:Landroid/widget/Button;

.field private N:Landroid/widget/Button;

.field public O:Landroid/widget/ImageView;

.field public P:Landroid/widget/TextView;

.field public Q:Landroid/view/View;

.field public R:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public S:Landroid/widget/TextView;

.field public T:Landroid/view/View;

.field public U:Ljava/lang/String;

.field public V:Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;

.field private W:LX/1Mv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Mv",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public X:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

.field public Y:LX/GUB;

.field public final Z:Landroid/widget/AdapterView$OnItemSelectedListener;

.field public p:LX/GTZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/GVF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/GTe;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/GTt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0aU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2356104
    const-class v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    sput-object v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->B:Ljava/lang/Class;

    .line 2356105
    const-class v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->C:Lcom/facebook/common/callercontext/CallerContext;

    .line 2356106
    sget-object v0, LX/0ax;->dp:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "/tour/locationsharing/learnmore"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->D:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2356088
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2356089
    const-string v0, "nearby_friends_undecided"

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->U:Ljava/lang/String;

    .line 2356090
    new-instance v0, LX/GTi;

    invoke-direct {v0, p0}, LX/GTi;-><init>(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)V

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->Z:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 2356091
    return-void
.end method

.method private a(LX/0Px;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLInterfaces$BackgroundLocationUpsellProfile$;",
            ">;)",
            "LX/0Px",
            "<+",
            "Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLInterfaces$BackgroundLocationUpsellProfile$;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2356092
    const-string v0, "invite_notification"

    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->U:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2356093
    :goto_0
    return-object p1

    .line 2356094
    :cond_0
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->V:Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 2356095
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2356096
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->V:Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2356097
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;

    .line 2356098
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2356099
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2356100
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2356101
    :cond_2
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object p1

    goto :goto_0
.end method

.method public static synthetic a(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;LX/0Px;)LX/0Px;
    .locals 1

    .prologue
    .line 2356102
    invoke-direct {p0, p1}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;LX/GTZ;LX/GVF;LX/GTe;LX/GTt;LX/0aG;Ljava/util/concurrent/ExecutorService;LX/0aU;LX/03V;LX/17W;LX/0kL;Lcom/facebook/content/SecureContextHelper;LX/1Ck;)V
    .locals 0

    .prologue
    .line 2356103
    iput-object p1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->p:LX/GTZ;

    iput-object p2, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->q:LX/GVF;

    iput-object p3, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->r:LX/GTe;

    iput-object p4, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->s:LX/GTt;

    iput-object p5, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->t:LX/0aG;

    iput-object p6, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->u:Ljava/util/concurrent/ExecutorService;

    iput-object p7, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->v:LX/0aU;

    iput-object p8, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->w:LX/03V;

    iput-object p9, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->x:LX/17W;

    iput-object p10, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->y:LX/0kL;

    iput-object p11, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->z:Lcom/facebook/content/SecureContextHelper;

    iput-object p12, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->A:LX/1Ck;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 13

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v12

    move-object v0, p0

    check-cast v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-static {v12}, LX/GTZ;->a(LX/0QB;)LX/GTZ;

    move-result-object v1

    check-cast v1, LX/GTZ;

    invoke-static {v12}, LX/GVF;->a(LX/0QB;)LX/GVF;

    move-result-object v2

    check-cast v2, LX/GVF;

    invoke-static {v12}, LX/GTe;->a(LX/0QB;)LX/GTe;

    move-result-object v3

    check-cast v3, LX/GTe;

    invoke-static {v12}, LX/GTt;->a(LX/0QB;)LX/GTt;

    move-result-object v4

    check-cast v4, LX/GTt;

    invoke-static {v12}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v5

    check-cast v5, LX/0aG;

    invoke-static {v12}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {v12}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v7

    check-cast v7, LX/0aU;

    invoke-static {v12}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {v12}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v9

    check-cast v9, LX/17W;

    invoke-static {v12}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v10

    check-cast v10, LX/0kL;

    invoke-static {v12}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v11

    check-cast v11, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v12}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v12

    check-cast v12, LX/1Ck;

    invoke-static/range {v0 .. v12}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->a(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;LX/GTZ;LX/GVF;LX/GTe;LX/GTt;LX/0aG;Ljava/util/concurrent/ExecutorService;LX/0aU;LX/03V;LX/17W;LX/0kL;Lcom/facebook/content/SecureContextHelper;LX/1Ck;)V

    return-void
.end method

.method private a(ZLcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)V
    .locals 4
    .param p2    # Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 2356107
    const v0, 0x7f080024

    invoke-static {v0, v1, v3, v1}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v1

    .line 2356108
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v2, "save_nux_decision"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2356109
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->W:LX/1Mv;

    if-eqz v0, :cond_0

    .line 2356110
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->W:LX/1Mv;

    invoke-virtual {v0, v3}, LX/1Mv;->a(Z)V

    .line 2356111
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->W:LX/1Mv;

    .line 2356112
    :cond_0
    const-string v0, "informational"

    iget-object v2, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->U:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2356113
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2356114
    invoke-direct {p0, p2}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->b(Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2356115
    :goto_0
    new-instance v2, LX/GTq;

    invoke-direct {v2, p0, v1, p1}, LX/GTq;-><init>(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;Landroid/support/v4/app/DialogFragment;Z)V

    .line 2356116
    invoke-static {v0, v2}, LX/1Mv;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)LX/1Mv;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->W:LX/1Mv;

    .line 2356117
    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->u:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v2, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2356118
    return-void

    .line 2356119
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->b(ZLcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)Z
    .locals 2
    .param p0    # Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2356120
    if-nez p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    if-eqz p0, :cond_2

    if-nez p1, :cond_2

    .line 2356121
    :cond_1
    const/4 v0, 0x0

    .line 2356122
    :goto_0
    return v0

    .line 2356123
    :cond_2
    if-ne p0, p1, :cond_3

    .line 2356124
    const/4 v0, 0x1

    goto :goto_0

    .line 2356125
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)V
    .locals 1

    .prologue
    .line 2356126
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->a(ZLcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)V

    .line 2356127
    return-void
.end method

.method public static b(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;I)Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2356128
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->K:Lcom/facebook/fbui/popover/PopoverSpinner;

    invoke-virtual {v0}, Lcom/facebook/fbui/popover/PopoverSpinner;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->Y:LX/GUB;

    invoke-virtual {v0}, LX/GUB;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 2356129
    :cond_0
    const/4 v0, 0x0

    .line 2356130
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->Y:LX/GUB;

    invoke-virtual {v0, p1}, LX/GUB;->a(I)Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLInterfaces$BackgroundLocationPrivacyPickerOption;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2356131
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->X:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    invoke-static {p1, v0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->a(Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2356132
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2356133
    :goto_0
    return-object v0

    .line 2356134
    :cond_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2356135
    invoke-virtual {p1}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 2356136
    const-string v1, "BackgroundLocationUpdateSettingsParams"

    invoke-static {v0}, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->a(Ljava/lang/String;)Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2356137
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->t:LX/0aG;

    const-string v1, "background_location_update_settings"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->C:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x6ccdabf9

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2356138
    new-instance v1, LX/GTh;

    invoke-direct {v1, p0}, LX/GTh;-><init>(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method private b(ZLcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p2    # Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLInterfaces$BackgroundLocationPrivacyPickerOption;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2356139
    sget-object v0, LX/GTf;->SHARING_UPSELL:LX/GTf;

    .line 2356140
    if-eqz p1, :cond_0

    .line 2356141
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2356142
    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->r:LX/GTe;

    iget-object v2, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->U:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 2356143
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2356144
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2356145
    const-string p0, "nux_type"

    const-string p1, "privacy"

    invoke-static {p0, v2, p1, v3}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object p0

    .line 2356146
    sget-object p1, LX/8DK;->COMPLETE:LX/8DK;

    invoke-static {v1, v0, p1, p0}, LX/GTe;->a(LX/GTe;LX/GTf;LX/8DK;LX/0P1;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p0

    move-object v0, p0

    .line 2356147
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->r:LX/GTe;

    iget-object v2, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->U:Ljava/lang/String;

    .line 2356148
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2356149
    const-string v3, "nux_type"

    invoke-static {v3, v2}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v3

    .line 2356150
    sget-object p0, LX/8DK;->SKIPPED:LX/8DK;

    invoke-static {v1, v0, p0, v3}, LX/GTe;->a(LX/GTe;LX/GTf;LX/8DK;LX/0P1;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 2356151
    goto :goto_0
.end method

.method private b(Z)V
    .locals 6

    .prologue
    .line 2356152
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->A:LX/1Ck;

    const-string v1, "fetch_upsell_data"

    iget-object v2, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->p:LX/GTZ;

    .line 2356153
    iget-object v3, v2, LX/GTZ;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2356154
    iget-object v3, v2, LX/GTZ;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v4, LX/GTY;

    invoke-direct {v4, v2}, LX/GTY;-><init>(LX/GTZ;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v2, v3

    .line 2356155
    new-instance v3, LX/GTo;

    invoke-direct {v3, p0, p1}, LX/GTo;-><init>(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;Z)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2356156
    return-void
.end method

.method public static b$redex0(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2356157
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->A:LX/1Ck;

    const-string v1, "fetch_inviter_proifile"

    iget-object v2, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->p:LX/GTZ;

    .line 2356158
    new-instance v3, LX/GTu;

    invoke-direct {v3}, LX/GTu;-><init>()V

    move-object v3, v3

    .line 2356159
    const-string v4, "id"

    invoke-virtual {v3, v4, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2356160
    const-string v4, "image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2356161
    const-string v4, "image_size"

    const/16 v5, 0x40

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2356162
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    .line 2356163
    iget-object v4, v2, LX/GTZ;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    iput-object v3, v2, LX/GTZ;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2356164
    iget-object v3, v2, LX/GTZ;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v2, v3

    .line 2356165
    new-instance v3, LX/GTn;

    invoke-direct {v3, p0}, LX/GTn;-><init>(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2356166
    return-void
.end method

.method public static c$redex0(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2356167
    if-nez p1, :cond_0

    .line 2356168
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->O:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2356169
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->P:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2356170
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->I:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 2356171
    :cond_0
    return-void
.end method

.method public static d(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;Z)V
    .locals 3

    .prologue
    .line 2356075
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->v:LX/0aU;

    const-string v2, "BACKGROUND_LOCATION_REPORTING_SETTINGS_REQUEST_REFRESH_ACTION"

    invoke-virtual {v1, v2}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2356076
    const-string v1, "expected_location_history_setting"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2356077
    invoke-virtual {p0, v0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 2356078
    return-void
.end method

.method public static e(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;Z)V
    .locals 3

    .prologue
    .line 2356079
    if-eqz p1, :cond_1

    .line 2356080
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2356081
    sget-object v1, LX/0ax;->dX:Ljava/lang/String;

    const-string v2, "one_page_nux"

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2356082
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->v()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2356083
    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->z:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2356084
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->setResult(ILandroid/content/Intent;)V

    .line 2356085
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->finish()V

    .line 2356086
    return-void

    .line 2356087
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->setResult(I)V

    goto :goto_0
.end method

.method public static l(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2355945
    const-string v0, "invite_notification"

    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->U:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "source"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v4

    .line 2355946
    :goto_0
    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->s:LX/GTt;

    iget-object v2, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->U:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/GTt;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2355947
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->s:LX/GTt;

    invoke-virtual {v0}, LX/GTt;->a()V

    .line 2355948
    const-string v0, "traveling"

    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->U:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2355949
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->F:Landroid/view/View;

    const v1, 0x7f02010d

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2355950
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->G:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f02078a

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2355951
    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->H:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->q(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f083799

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 2355952
    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->H:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0b0054

    :goto_2
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v4, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2355953
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->p()V

    .line 2355954
    :goto_3
    const-string v0, "traveling"

    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->U:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->b(Z)V

    .line 2355955
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->s()V

    .line 2355956
    return-void

    .line 2355957
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "source"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2355958
    :cond_1
    const v0, 0x7f08379d

    goto :goto_1

    .line 2355959
    :cond_2
    const v0, 0x7f0b0058

    goto :goto_2

    .line 2355960
    :cond_3
    const-string v0, "close_friends"

    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->U:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2355961
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->F:Landroid/view/View;

    const v1, 0x7f0200fe

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2355962
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->G:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f02093c

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2355963
    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->H:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->q(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f083799

    :goto_4
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 2355964
    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->H:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->r()Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f0b0054

    :goto_5
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v4, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_3

    .line 2355965
    :cond_4
    const v0, 0x7f0837a1

    goto :goto_4

    .line 2355966
    :cond_5
    const v0, 0x7f0b0058

    goto :goto_5

    .line 2355967
    :cond_6
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->F:Landroid/view/View;

    const v1, 0x7f0200ff

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2355968
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->G:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f02093c

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2355969
    const-string v0, "invite_notification"

    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->U:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->V:Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0837a0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->V:Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;

    invoke-virtual {v3}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2355970
    :goto_6
    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->H:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 2355971
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083799

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_6
.end method

.method private m()V
    .locals 2

    .prologue
    .line 2355972
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->E:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2355973
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->F:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2355974
    return-void
.end method

.method public static n(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)V
    .locals 2

    .prologue
    .line 2355975
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->E:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2355976
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->F:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2355977
    return-void
.end method

.method private o()V
    .locals 7

    .prologue
    .line 2355978
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->A:LX/1Ck;

    const-string v1, "fetch_show_nux_type"

    iget-object v2, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->p:LX/GTZ;

    iget-object v3, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->U:Ljava/lang/String;

    .line 2355979
    iget-object v4, v2, LX/GTZ;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2355980
    iget-object v4, v2, LX/GTZ;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v5, LX/GTV;

    invoke-direct {v5, v2, v3}, LX/GTV;-><init>(LX/GTZ;Ljava/lang/String;)V

    .line 2355981
    sget-object v6, LX/131;->INSTANCE:LX/131;

    move-object v6, v6

    .line 2355982
    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v2, v4

    .line 2355983
    new-instance v3, LX/GTl;

    invoke-direct {v3, p0}, LX/GTl;-><init>(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2355984
    return-void
.end method

.method private p()V
    .locals 6

    .prologue
    .line 2355985
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->A:LX/1Ck;

    const-string v1, "fetch_actor_info"

    iget-object v2, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->p:LX/GTZ;

    .line 2355986
    iget-object v3, v2, LX/GTZ;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2355987
    iget-object v3, v2, LX/GTZ;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v4, LX/GTW;

    invoke-direct {v4, v2}, LX/GTW;-><init>(LX/GTZ;)V

    .line 2355988
    sget-object v5, LX/131;->INSTANCE:LX/131;

    move-object v5, v5

    .line 2355989
    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v2, v3

    .line 2355990
    new-instance v3, LX/GTm;

    invoke-direct {v3, p0}, LX/GTm;-><init>(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2355991
    return-void
.end method

.method public static q(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)Z
    .locals 2

    .prologue
    .line 2355992
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 2355993
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v1, v1

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    div-float v0, v1, v0

    const/high16 v1, 0x43fa0000    # 500.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private r()Z
    .locals 3

    .prologue
    .line 2355994
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 2355995
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v1, v1

    iget v2, v0, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v1, v2

    const/high16 v2, 0x43fa0000    # 500.0f

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v1, v1

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    div-float v0, v1, v0

    const/high16 v1, 0x44160000    # 600.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private s()V
    .locals 6

    .prologue
    .line 2355996
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->A:LX/1Ck;

    const-string v1, "fetch_privacy_options"

    iget-object v2, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->p:LX/GTZ;

    .line 2355997
    iget-object v3, v2, LX/GTZ;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2355998
    iget-object v3, v2, LX/GTZ;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v4, LX/GTX;

    invoke-direct {v4, v2}, LX/GTX;-><init>(LX/GTZ;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v2, v3

    .line 2355999
    new-instance v3, LX/GTp;

    invoke-direct {v3, p0}, LX/GTp;-><init>(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2356000
    return-void
.end method

.method public static t(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)V
    .locals 2

    .prologue
    .line 2356001
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->a(ZLcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)V

    .line 2356002
    return-void
.end method

.method private u()V
    .locals 9

    .prologue
    .line 2356003
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08379b

    const/4 v2, 0x1

    new-array v2, v2, [LX/47s;

    const/4 v3, 0x0

    new-instance v4, LX/47s;

    const v5, 0x7f08379c

    new-instance v6, LX/GTr;

    sget-object v7, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->D:Ljava/lang/String;

    sget-object v8, LX/GTs;->LEARN_MORE:LX/GTs;

    invoke-direct {v6, p0, v7, v8}, LX/GTr;-><init>(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;Ljava/lang/String;LX/GTs;)V

    const/16 v7, 0x21

    invoke-direct {v4, v5, v6, v7}, LX/47s;-><init>(ILjava/lang/Object;I)V

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/47r;->a(Landroid/content/res/Resources;I[LX/47s;)Landroid/text/SpannableString;

    move-result-object v0

    .line 2356004
    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->L:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2356005
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->L:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2356006
    return-void
.end method

.method private v()Z
    .locals 3

    .prologue
    .line 2356007
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "redirect_after_accept"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v1, 0x400

    const/4 v3, 0x0

    .line 2356008
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2356009
    invoke-static {p0, p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2356010
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 2356011
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "nux_type"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->U:Ljava/lang/String;

    .line 2356012
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->p:LX/GTZ;

    .line 2356013
    iget-object v4, v0, LX/GTZ;->b:LX/0y2;

    invoke-virtual {v4}, LX/0y2;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v4

    .line 2356014
    new-instance v5, LX/GTv;

    invoke-direct {v5}, LX/GTv;-><init>()V

    move-object v5, v5

    .line 2356015
    const-string v6, "image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2356016
    const-string v6, "image_size"

    const/16 v7, 0x40

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2356017
    const-string v6, "n_upsell_results"

    const/16 v7, 0xb

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2356018
    if-eqz v4, :cond_0

    .line 2356019
    const-string v6, "viewer_latitude"

    invoke-virtual {v4}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2356020
    const-string v6, "viewer_longitude"

    invoke-virtual {v4}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2356021
    :cond_0
    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    .line 2356022
    iget-object v5, v0, LX/GTZ;->a:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    iput-object v4, v0, LX/GTZ;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2356023
    const v0, 0x7f03015a

    invoke-virtual {p0, v0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->setContentView(I)V

    .line 2356024
    const v0, 0x7f0d064e

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->E:Landroid/view/View;

    .line 2356025
    const v0, 0x7f0d0647

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->F:Landroid/view/View;

    .line 2356026
    const v0, 0x7f0d0651

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->G:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2356027
    const v0, 0x7f0d0652

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->H:Landroid/widget/TextView;

    .line 2356028
    const v0, 0x7f0d0648

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->O:Landroid/widget/ImageView;

    .line 2356029
    const v0, 0x7f0d0649

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->P:Landroid/widget/TextView;

    .line 2356030
    const v0, 0x7f0d064a

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->Q:Landroid/view/View;

    .line 2356031
    const v0, 0x7f0d064b

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->R:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2356032
    const v0, 0x7f0d064c

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->S:Landroid/widget/TextView;

    .line 2356033
    const v0, 0x7f0d064d

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->T:Landroid/view/View;

    .line 2356034
    const v0, 0x7f0d0650

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->I:Lcom/facebook/fbui/facepile/FacepileView;

    .line 2356035
    const v0, 0x7f0d0642

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->J:Landroid/view/View;

    .line 2356036
    const v0, 0x7f0d0643

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/popover/PopoverSpinner;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->K:Lcom/facebook/fbui/popover/PopoverSpinner;

    .line 2356037
    const v0, 0x7f0d0644

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->L:Landroid/widget/TextView;

    .line 2356038
    const v0, 0x7f0d0646

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->M:Landroid/widget/Button;

    .line 2356039
    const v0, 0x7f0d0645

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->N:Landroid/widget/Button;

    .line 2356040
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->u()V

    .line 2356041
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->M:Landroid/widget/Button;

    new-instance v1, LX/GTj;

    invoke-direct {v1, p0}, LX/GTj;-><init>(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2356042
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->N:Landroid/widget/Button;

    new-instance v1, LX/GTk;

    invoke-direct {v1, p0}, LX/GTk;-><init>(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2356043
    const-string v0, "informational"

    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->U:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2356044
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->N:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 2356045
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v3, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 2356046
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b238e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2356047
    invoke-virtual {v0, v1, v3, v1, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2356048
    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->M:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2356049
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->M:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080016

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2356050
    invoke-static {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->l(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)V

    .line 2356051
    :goto_0
    return-void

    .line 2356052
    :cond_1
    const-string v0, "close_friends"

    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->U:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2356053
    invoke-static {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->l(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)V

    goto :goto_0

    .line 2356054
    :cond_2
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->m()V

    .line 2356055
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->o()V

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 2356056
    iget-object v0, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->s:LX/GTt;

    invoke-virtual {v0}, LX/GTt;->d()V

    .line 2356057
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2356058
    return-void
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x12d20232

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2356059
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2356060
    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->A:LX/1Ck;

    if-eqz v1, :cond_0

    .line 2356061
    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->A:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2356062
    :cond_0
    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->p:LX/GTZ;

    if-eqz v1, :cond_1

    .line 2356063
    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->p:LX/GTZ;

    const/4 p0, 0x0

    const/4 v4, 0x0

    .line 2356064
    iget-object v2, v1, LX/GTZ;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v2, v4}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2356065
    iput-object p0, v1, LX/GTZ;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2356066
    iget-object v2, v1, LX/GTZ;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v2, :cond_1

    .line 2356067
    iget-object v2, v1, LX/GTZ;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v2, v4}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2356068
    iput-object p0, v1, LX/GTZ;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2356069
    :cond_1
    const/16 v1, 0x23

    const v2, 0x5f145b29

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x69383a1f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2356070
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStop()V

    .line 2356071
    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->W:LX/1Mv;

    if-eqz v1, :cond_0

    .line 2356072
    iget-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->W:LX/1Mv;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/1Mv;->a(Z)V

    .line 2356073
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->W:LX/1Mv;

    .line 2356074
    :cond_0
    const/16 v1, 0x23

    const v2, -0x50debbe6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
