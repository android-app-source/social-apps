.class public Lcom/facebook/caspian/ui/pagerindicator/CaspianTabbedViewPagerIndicatorBadgeTextView;
.super Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2359840
    invoke-direct {p0, p1}, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;-><init>(Landroid/content/Context;)V

    .line 2359841
    invoke-direct {p0}, Lcom/facebook/caspian/ui/pagerindicator/CaspianTabbedViewPagerIndicatorBadgeTextView;->a()V

    .line 2359842
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2359843
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2359844
    invoke-direct {p0}, Lcom/facebook/caspian/ui/pagerindicator/CaspianTabbedViewPagerIndicatorBadgeTextView;->a()V

    .line 2359845
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2359830
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2359831
    invoke-direct {p0}, Lcom/facebook/caspian/ui/pagerindicator/CaspianTabbedViewPagerIndicatorBadgeTextView;->a()V

    .line 2359832
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2359833
    sget-object v0, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v1, LX/0xr;->MEDIUM:LX/0xr;

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/pagerindicator/CaspianTabbedViewPagerIndicatorBadgeTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 2359834
    invoke-virtual {p0}, Lcom/facebook/caspian/ui/pagerindicator/CaspianTabbedViewPagerIndicatorBadgeTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2359835
    const v1, 0x7f0a00d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 2359836
    iput v1, p0, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;->b:I

    .line 2359837
    const v1, 0x7f0a0156

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2359838
    iput v0, p0, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;->c:I

    .line 2359839
    return-void
.end method
