.class public Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;
.super LX/1ZN;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final k:Ljava/lang/String;

.field private static final l:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/11H;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0ka;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0VP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/2Kx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/GiW;

.field public g:LX/Gib;

.field public h:LX/GiY;

.field public i:LX/Gid;

.field public j:Landroid/os/Handler;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2386290
    const-class v0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->k:Ljava/lang/String;

    .line 2386291
    const-class v0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;

    const-string v1, "infrastructure"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->l:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2386284
    const-string v0, "GlobalLibraryCollectorService"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 2386285
    new-instance v0, LX/GiW;

    invoke-direct {v0}, LX/GiW;-><init>()V

    iput-object v0, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->f:LX/GiW;

    .line 2386286
    new-instance v0, LX/Gib;

    invoke-direct {v0}, LX/Gib;-><init>()V

    iput-object v0, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->g:LX/Gib;

    .line 2386287
    new-instance v0, LX/GiY;

    invoke-direct {v0}, LX/GiY;-><init>()V

    iput-object v0, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->h:LX/GiY;

    .line 2386288
    new-instance v0, LX/Gid;

    invoke-direct {v0}, LX/Gid;-><init>()V

    iput-object v0, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->i:LX/Gid;

    .line 2386289
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2386268
    iget-object v0, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->e:LX/2Kx;

    .line 2386269
    iget-object v1, v0, LX/2Kx;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2Kx;->c:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2386270
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->e:LX/2Kx;

    invoke-virtual {v0}, LX/2Kx;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2386271
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->a:LX/11H;

    iget-object v3, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->f:LX/GiW;

    const/4 v4, 0x0

    sget-object v5, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v3, v4, v5}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GiV;

    .line 2386272
    if-nez v0, :cond_1

    move-object v0, v2

    .line 2386273
    :goto_0
    return-object v0

    .line 2386274
    :cond_1
    invoke-static {}, LX/2Kx;->b()Ljava/lang/String;

    move-result-object v3

    .line 2386275
    iget-object v4, v0, LX/GiV;->mHash:Ljava/lang/String;

    move-object v4, v4

    .line 2386276
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2386277
    iget-object v1, v0, LX/GiV;->mFbid:Ljava/lang/String;

    move-object v0, v1

    .line 2386278
    iget-object v1, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->e:LX/2Kx;

    invoke-virtual {v1, v0}, LX/2Kx;->a(Ljava/lang/String;)V

    .line 2386279
    iget-object v1, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->e:LX/2Kx;

    invoke-virtual {v1}, LX/2Kx;->c()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2386280
    :catch_0
    move-exception v0

    .line 2386281
    sget-object v1, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->k:Ljava/lang/String;

    const-string v3, "Error querying device info from server"

    invoke-static {v1, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v2

    .line 2386282
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 2386283
    goto :goto_0
.end method

.method private a(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/GiZ;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 2386262
    const/4 v1, 0x0

    .line 2386263
    :try_start_0
    iget-object v0, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->a:LX/11H;

    iget-object v2, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->g:LX/Gib;

    new-instance v3, LX/Gia;

    invoke-static {}, LX/2Kx;->b()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, p1}, LX/Gia;-><init>(Ljava/lang/String;Ljava/util/ArrayList;)V

    sget-object v4, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v3, v4}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2386264
    :goto_0
    if-eqz v0, :cond_0

    .line 2386265
    iget-object v1, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->e:LX/2Kx;

    invoke-virtual {v1, v0}, LX/2Kx;->a(Ljava/lang/String;)V

    .line 2386266
    iget-object v1, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->e:LX/2Kx;

    invoke-virtual {v1}, LX/2Kx;->c()V

    .line 2386267
    :cond_0
    return-object v0

    :catch_0
    move-object v0, v1

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "LX/GiZ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2386257
    const/4 v1, 0x0

    .line 2386258
    :try_start_0
    iget-object v0, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->a:LX/11H;

    iget-object v2, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->h:LX/GiY;

    new-instance v3, LX/GiX;

    invoke-direct {v3, p1}, LX/GiX;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v3, v4}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2386259
    :goto_0
    return-object v0

    .line 2386260
    :catch_0
    move-exception v0

    .line 2386261
    sget-object v2, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->k:Ljava/lang/String;

    const-string v3, "Failed to obtain pending library list from server"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_0
.end method

.method private a(LX/GiV;)V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2386227
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v3, "device_lib_hashes.dat"

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2386228
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2386229
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 2386230
    :cond_0
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2386231
    :try_start_1
    new-instance v3, Landroid/util/JsonWriter;

    new-instance v0, Ljava/io/OutputStreamWriter;

    invoke-direct {v0, v1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v3, v0}, Landroid/util/JsonWriter;-><init>(Ljava/io/Writer;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2386232
    :try_start_2
    invoke-virtual {v3}, Landroid/util/JsonWriter;->beginObject()Landroid/util/JsonWriter;

    .line 2386233
    const-string v0, "hash"

    invoke-virtual {v3, v0}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v0

    iget-object v2, p1, LX/GiV;->mHash:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 2386234
    iget-object v0, p1, LX/GiV;->mLibs:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 2386235
    const-string v0, "libs"

    invoke-virtual {v3, v0}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 2386236
    invoke-virtual {v3}, Landroid/util/JsonWriter;->beginArray()Landroid/util/JsonWriter;

    .line 2386237
    iget-object v0, p1, LX/GiV;->mLibs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    iget-object v0, p1, LX/GiV;->mLibs:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GiZ;

    .line 2386238
    invoke-virtual {v3}, Landroid/util/JsonWriter;->beginObject()Landroid/util/JsonWriter;

    .line 2386239
    const-string v5, "file_name"

    invoke-virtual {v3, v5}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v5

    iget-object p0, v0, LX/GiZ;->mFileName:Ljava/lang/String;

    invoke-virtual {v5, p0}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 2386240
    const-string v5, "file_path"

    invoke-virtual {v3, v5}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v5

    iget-object p0, v0, LX/GiZ;->mFilePath:Ljava/lang/String;

    invoke-virtual {v5, p0}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 2386241
    const-string v5, "hash"

    invoke-virtual {v3, v5}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v5

    iget-object p0, v0, LX/GiZ;->mHash:Ljava/lang/String;

    invoke-virtual {v5, p0}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 2386242
    invoke-virtual {v3}, Landroid/util/JsonWriter;->endObject()Landroid/util/JsonWriter;

    .line 2386243
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2386244
    :cond_1
    invoke-virtual {v3}, Landroid/util/JsonWriter;->endArray()Landroid/util/JsonWriter;

    .line 2386245
    :cond_2
    invoke-virtual {v3}, Landroid/util/JsonWriter;->endObject()Landroid/util/JsonWriter;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2386246
    invoke-static {v3}, LX/2Kx;->a(Ljava/io/Closeable;)V

    .line 2386247
    invoke-static {v1}, LX/2Kx;->a(Ljava/io/Closeable;)V

    .line 2386248
    :goto_1
    return-void

    .line 2386249
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 2386250
    :goto_2
    :try_start_3
    sget-object v3, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->k:Ljava/lang/String;

    const-string v4, "Error updating device library has file"

    invoke-static {v3, v4, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2386251
    invoke-static {v2}, LX/2Kx;->a(Ljava/io/Closeable;)V

    .line 2386252
    invoke-static {v1}, LX/2Kx;->a(Ljava/io/Closeable;)V

    goto :goto_1

    .line 2386253
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    invoke-static {v2}, LX/2Kx;->a(Ljava/io/Closeable;)V

    .line 2386254
    invoke-static {v1}, LX/2Kx;->a(Ljava/io/Closeable;)V

    throw v0

    .line 2386255
    :catchall_1
    move-exception v0

    goto :goto_3

    :catchall_2
    move-exception v0

    move-object v2, v3

    goto :goto_3

    .line 2386256
    :catch_1
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v2, v3

    goto :goto_2
.end method

.method private static a(Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;LX/11H;LX/0ka;LX/0VP;LX/03V;LX/2Kx;)V
    .locals 0

    .prologue
    .line 2386226
    iput-object p1, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->a:LX/11H;

    iput-object p2, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->b:LX/0ka;

    iput-object p3, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->c:LX/0VP;

    iput-object p4, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->d:LX/03V;

    iput-object p5, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->e:LX/2Kx;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;

    invoke-static {v5}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v1

    check-cast v1, LX/11H;

    invoke-static {v5}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v2

    check-cast v2, LX/0ka;

    invoke-static {v5}, LX/0VP;->a(LX/0QB;)LX/0VP;

    move-result-object v3

    check-cast v3, LX/0VP;

    invoke-static {v5}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v5}, LX/2Kx;->a(LX/0QB;)LX/2Kx;

    move-result-object v5

    check-cast v5, LX/2Kx;

    invoke-static/range {v0 .. v5}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->a(Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;LX/11H;LX/0ka;LX/0VP;LX/03V;LX/2Kx;)V

    return-void
.end method

.method private a(LX/GiZ;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2386199
    iget-object v0, p1, LX/GiZ;->mFileName:Ljava/lang/String;

    move-object v0, v0

    .line 2386200
    iget-object v2, p1, LX/GiZ;->mFilePath:Ljava/lang/String;

    move-object v2, v2

    .line 2386201
    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 2386202
    :goto_0
    return v0

    .line 2386203
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2386204
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 2386205
    goto :goto_0

    .line 2386206
    :cond_2
    const/4 v3, 0x0

    .line 2386207
    :try_start_0
    const-string v2, "%s.gz"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->getCacheDir()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2386208
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2386209
    :try_start_1
    invoke-static {v0, v2}, LX/0VP;->a(Ljava/io/File;Ljava/io/File;)V

    .line 2386210
    iget-object v0, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->a:LX/11H;

    iget-object v3, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->i:LX/Gid;

    new-instance v4, LX/Gic;

    invoke-direct {v4, v2, p2}, LX/Gic;-><init>(Ljava/io/File;Ljava/lang/String;)V

    sget-object v5, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v3, v4, v5}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    .line 2386211
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 2386212
    :catch_0
    move-exception v0

    move-object v2, v3

    .line 2386213
    :goto_1
    :try_start_2
    sget-object v3, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->k:Ljava/lang/String;

    const-string v4, "Error when preparing library for upload"

    invoke-static {v3, v4, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2386214
    if-eqz v2, :cond_3

    .line 2386215
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    :cond_3
    :goto_2
    move v0, v1

    .line 2386216
    goto :goto_0

    .line 2386217
    :catch_1
    move-exception v0

    .line 2386218
    :goto_3
    :try_start_3
    sget-object v2, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->k:Ljava/lang/String;

    const-string v4, "Error when uploading library"

    invoke-static {v2, v4, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2386219
    if-eqz v3, :cond_3

    .line 2386220
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    goto :goto_2

    .line 2386221
    :catchall_0
    move-exception v0

    :goto_4
    if-eqz v3, :cond_4

    .line 2386222
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    :cond_4
    throw v0

    .line 2386223
    :catchall_1
    move-exception v0

    move-object v3, v2

    goto :goto_4

    .line 2386224
    :catch_2
    move-exception v0

    move-object v3, v2

    goto :goto_3

    .line 2386225
    :catch_3
    move-exception v0

    goto :goto_1
.end method

.method private b()Ljava/util/ArrayList;
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "LX/GiZ;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2386167
    new-instance v4, Ljava/io/File;

    invoke-virtual {p0}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->getFilesDir()Ljava/io/File;

    move-result-object v0

    const-string v2, "device_lib_hashes.dat"

    invoke-direct {v4, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2386168
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2386169
    invoke-static {}, LX/2Kx;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2386170
    new-instance v2, LX/GiV;

    invoke-static {}, LX/2Kx;->b()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1, v0}, LX/GiV;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2386171
    invoke-direct {p0, v2}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->a(LX/GiV;)V

    .line 2386172
    :goto_0
    return-object v0

    .line 2386173
    :cond_0
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2386174
    :try_start_1
    new-instance v2, Landroid/util/JsonReader;

    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v0}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2386175
    :try_start_2
    invoke-static {v2}, LX/GiV;->a(Landroid/util/JsonReader;)LX/GiV;

    move-result-object v0

    .line 2386176
    if-nez v0, :cond_1

    .line 2386177
    invoke-virtual {v4}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2386178
    invoke-static {v2}, LX/2Kx;->a(Ljava/io/Closeable;)V

    .line 2386179
    invoke-static {v3}, LX/2Kx;->a(Ljava/io/Closeable;)V

    move-object v0, v1

    goto :goto_0

    .line 2386180
    :cond_1
    :try_start_3
    invoke-static {}, LX/2Kx;->b()Ljava/lang/String;

    move-result-object v5

    .line 2386181
    iget-object v6, v0, LX/GiV;->mHash:Ljava/lang/String;

    move-object v6, v6

    .line 2386182
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 2386183
    invoke-static {}, LX/2Kx;->a()Ljava/util/ArrayList;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v0

    .line 2386184
    :try_start_4
    new-instance v1, LX/GiV;

    invoke-static {}, LX/2Kx;->b()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v1, v5, v6, v0}, LX/GiV;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2386185
    invoke-direct {p0, v1}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->a(LX/GiV;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 2386186
    :goto_1
    invoke-static {v2}, LX/2Kx;->a(Ljava/io/Closeable;)V

    .line 2386187
    invoke-static {v3}, LX/2Kx;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 2386188
    :cond_2
    :try_start_5
    iget-object v5, v0, LX/GiV;->mLibs:Ljava/util/ArrayList;

    move-object v0, v5
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2386189
    goto :goto_1

    .line 2386190
    :catch_0
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    .line 2386191
    :goto_2
    :try_start_6
    sget-object v5, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->k:Ljava/lang/String;

    const-string v6, "Error when reading object from device hash file"

    invoke-static {v5, v6, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2386192
    invoke-virtual {v4}, Ljava/io/File;->delete()Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 2386193
    invoke-static {v2}, LX/2Kx;->a(Ljava/io/Closeable;)V

    .line 2386194
    invoke-static {v3}, LX/2Kx;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 2386195
    :catchall_0
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    :goto_3
    invoke-static {v2}, LX/2Kx;->a(Ljava/io/Closeable;)V

    .line 2386196
    invoke-static {v3}, LX/2Kx;->a(Ljava/io/Closeable;)V

    throw v0

    .line 2386197
    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_3

    :catchall_2
    move-exception v0

    goto :goto_3

    .line 2386198
    :catch_1
    move-exception v0

    move-object v2, v1

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_2
.end method

.method private b(Ljava/lang/String;)Z
    .locals 12

    .prologue
    const/4 v4, 0x0

    .line 2386125
    :try_start_0
    invoke-direct {p0}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->b()Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 2386126
    if-nez v9, :cond_0

    .line 2386127
    iget-object v0, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->j:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService$DisplayToastRunnable;

    const-string v2, "Local device hash file corrupted. Please try again"

    invoke-direct {v1, p0, v2}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService$DisplayToastRunnable;-><init>(Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;Ljava/lang/String;)V

    const v2, -0x412c57ff

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    move v0, v4

    .line 2386128
    :goto_0
    return v0

    .line 2386129
    :catch_0
    move-exception v0

    .line 2386130
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2386131
    :cond_0
    if-nez p1, :cond_1

    .line 2386132
    :try_start_1
    invoke-direct {p0, v9}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->a(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object p1

    .line 2386133
    if-nez p1, :cond_1

    .line 2386134
    iget-object v0, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->j:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService$DisplayToastRunnable;

    const-string v2, "Failed to upload device library hash info"

    invoke-direct {v1, p0, v2}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService$DisplayToastRunnable;-><init>(Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;Ljava/lang/String;)V

    const v2, -0x6efce61

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    move v0, v4

    .line 2386135
    goto :goto_0

    .line 2386136
    :catch_1
    move-exception v0

    .line 2386137
    iget-object v1, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->j:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService$DisplayToastRunnable;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Error when uploading: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, p0, v0}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService$DisplayToastRunnable;-><init>(Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;Ljava/lang/String;)V

    const v0, 0x5b813e4e

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    move v0, v4

    .line 2386138
    goto :goto_0

    .line 2386139
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2386140
    iget-object v1, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->j:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService$DisplayToastRunnable;

    const-string v3, "Beginning to upload pending libraries"

    invoke-direct {v2, p0, v3}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService$DisplayToastRunnable;-><init>(Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;Ljava/lang/String;)V

    const v3, 0x65821744

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    move v3, v4

    move v7, v4

    move-object v8, v0

    move v0, v4

    .line 2386141
    :goto_1
    if-eqz v8, :cond_6

    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 2386142
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int v6, v0, v1

    .line 2386143
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v10

    move v5, v4

    move v2, v4

    :goto_2
    if-ge v5, v10, :cond_4

    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GiZ;

    .line 2386144
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 2386145
    const/4 v11, -0x1

    if-ne v1, v11, :cond_3

    .line 2386146
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2386147
    const-string v11, "Mismatched hash encountered for GLC library: "

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2386148
    iget-object v11, v0, LX/GiZ;->mFileName:Ljava/lang/String;

    move-object v0, v11

    .line 2386149
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". Device ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2386150
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v1, v3

    .line 2386151
    :goto_3
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v3, v1

    move v2, v0

    goto :goto_2

    .line 2386152
    :cond_3
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/GiZ;

    .line 2386153
    iget-object v11, v0, LX/GiZ;->mFbid:Ljava/lang/String;

    move-object v0, v11

    .line 2386154
    invoke-direct {p0, v1, v0}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->a(LX/GiZ;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2386155
    add-int/lit8 v0, v3, 0x1

    move v1, v0

    move v0, v2

    goto :goto_3

    .line 2386156
    :cond_4
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v2, v0, :cond_5

    .line 2386157
    iget-object v0, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->j:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService$DisplayToastRunnable;

    const-string v2, "Unable to upload any pending libraries"

    invoke-direct {v1, p0, v2}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService$DisplayToastRunnable;-><init>(Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;Ljava/lang/String;)V

    const v2, 0x78ed726b

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    move v0, v4

    .line 2386158
    goto/16 :goto_0

    .line 2386159
    :cond_5
    add-int v0, v7, v2

    .line 2386160
    invoke-direct {p0, p1}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    move v7, v0

    move-object v8, v1

    move v0, v6

    .line 2386161
    goto :goto_1

    .line 2386162
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2386163
    const-string v2, "Total Libs: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\nPending Libs: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\nUpload Success: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\nUpload Errors: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2386164
    iget-object v0, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->j:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService$DisplayToastRunnable;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, p0, v1}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService$DisplayToastRunnable;-><init>(Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;Ljava/lang/String;)V

    const v1, 0x7a0b1068

    invoke-static {v0, v2, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2386165
    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    .line 2386166
    iget-object v1, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->e:LX/2Kx;

    invoke-virtual {v1, v0}, LX/2Kx;->a(Z)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, 0x20e7fee9

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2386084
    iget-object v0, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->b:LX/0ka;

    invoke-virtual {v0, v5}, LX/0ka;->a(Z)Z

    move-result v0

    .line 2386085
    :try_start_0
    invoke-direct {p0}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->a()Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2386086
    const-string v3, "upload_all_libs"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2386087
    if-eqz v0, :cond_0

    .line 2386088
    invoke-direct {p0, v1}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->b(Ljava/lang/String;)Z

    .line 2386089
    :goto_0
    const v0, 0x2479b262

    invoke-static {v0, v2}, LX/02F;->d(II)V

    .line 2386090
    :goto_1
    return-void

    .line 2386091
    :catch_0
    move-exception v0

    .line 2386092
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    const v0, -0x28d8c385

    invoke-static {v0, v2}, LX/02F;->d(II)V

    throw v1

    .line 2386093
    :cond_0
    iget-object v0, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->j:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService$DisplayToastRunnable;

    const-string v3, "Can\'t upload libraries: Make sure you have WiFi available before uploading"

    invoke-direct {v1, p0, v3}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService$DisplayToastRunnable;-><init>(Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;Ljava/lang/String;)V

    const v3, 0x653fefa0

    invoke-static {v0, v1, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0

    .line 2386094
    :cond_1
    if-eqz v1, :cond_6

    .line 2386095
    if-eqz v0, :cond_4

    .line 2386096
    invoke-direct {p0, v1}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2386097
    if-eqz v0, :cond_4

    .line 2386098
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    .line 2386099
    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    .line 2386100
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GiZ;

    .line 2386101
    :try_start_1
    invoke-direct {p0}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->b()Ljava/util/ArrayList;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 2386102
    if-nez v3, :cond_2

    .line 2386103
    const v0, -0x2559bdd4

    invoke-static {v0, v2}, LX/02F;->d(II)V

    goto :goto_1

    .line 2386104
    :catch_1
    move-exception v0

    .line 2386105
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    const v0, -0x560d0d48

    invoke-static {v0, v2}, LX/02F;->d(II)V

    throw v1

    .line 2386106
    :cond_2
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v4

    .line 2386107
    const/4 v5, -0x1

    if-ne v4, v5, :cond_3

    .line 2386108
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 2386109
    const-string v4, "Mismatched hash encountered for GLC library: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 2386110
    iget-object v5, v0, LX/GiZ;->mFileName:Ljava/lang/String;

    move-object v0, v5

    .line 2386111
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ". Device ID: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2386112
    iget-object v0, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->d:LX/03V;

    sget-object v1, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->k:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2386113
    const v0, -0x1087c897

    invoke-static {v0, v2}, LX/02F;->d(II)V

    goto/16 :goto_1

    .line 2386114
    :cond_3
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/GiZ;

    .line 2386115
    iget-object v3, v0, LX/GiZ;->mFbid:Ljava/lang/String;

    move-object v0, v3

    .line 2386116
    invoke-direct {p0, v1, v0}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->a(LX/GiZ;Ljava/lang/String;)Z

    .line 2386117
    :cond_4
    :goto_2
    const v0, 0x6ebf6cd9

    invoke-static {v0, v2}, LX/02F;->d(II)V

    goto/16 :goto_1

    .line 2386118
    :cond_5
    iget-object v0, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->e:LX/2Kx;

    invoke-virtual {v0, v5}, LX/2Kx;->a(Z)V

    goto :goto_2

    .line 2386119
    :cond_6
    :try_start_2
    invoke-direct {p0}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->b()Ljava/util/ArrayList;
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    .line 2386120
    if-nez v0, :cond_7

    .line 2386121
    const v0, 0xb6230ab

    invoke-static {v0, v2}, LX/02F;->d(II)V

    goto/16 :goto_1

    .line 2386122
    :cond_7
    :try_start_3
    invoke-direct {p0, v0}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->a(Ljava/util/ArrayList;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    .line 2386123
    :catch_2
    move-exception v0

    .line 2386124
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    const v0, -0x660dbd31

    invoke-static {v0, v2}, LX/02F;->d(II)V

    throw v1
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x32a23dc1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2386080
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 2386081
    invoke-static {p0, p0}, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2386082
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;->j:Landroid/os/Handler;

    .line 2386083
    const/16 v1, 0x25

    const v2, -0x406f1c45

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
