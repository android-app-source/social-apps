.class public final Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorScheduler$LibraryServiceStarterRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field public final synthetic a:LX/2Kw;


# direct methods
.method public constructor <init>(LX/2Kw;)V
    .locals 0

    .prologue
    .line 2386062
    iput-object p1, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorScheduler$LibraryServiceStarterRunnable;->a:LX/2Kw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2386063
    :try_start_0
    iget-object v0, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorScheduler$LibraryServiceStarterRunnable;->a:LX/2Kw;

    iget-object v0, v0, LX/2Kw;->e:LX/2Kx;

    invoke-virtual {v0}, LX/2Kx;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2386064
    iget-object v0, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorScheduler$LibraryServiceStarterRunnable;->a:LX/2Kw;

    iget-object v0, v0, LX/2Kw;->g:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2386065
    :goto_0
    return-void

    .line 2386066
    :catch_0
    move-exception v0

    .line 2386067
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2386068
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorScheduler$LibraryServiceStarterRunnable;->a:LX/2Kw;

    iget-object v1, v1, LX/2Kw;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2386069
    iget-object v1, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorScheduler$LibraryServiceStarterRunnable;->a:LX/2Kw;

    iget-object v1, v1, LX/2Kw;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorScheduler$LibraryServiceStarterRunnable;->a:LX/2Kw;

    iget-object v2, v2, LX/2Kw;->b:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->c(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;

    .line 2386070
    iget-object v0, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorScheduler$LibraryServiceStarterRunnable;->a:LX/2Kw;

    iget-object v1, p0, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorScheduler$LibraryServiceStarterRunnable;->a:LX/2Kw;

    iget-object v1, v1, LX/2Kw;->h:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    .line 2386071
    iget-object v1, v0, LX/2Kw;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 2386072
    sget-object v4, LX/2Kw;->a:LX/0Tn;

    invoke-interface {v1, v4, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 2386073
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2386074
    goto :goto_0
.end method
