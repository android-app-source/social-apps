.class public final Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3d369c4a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2409244
    const-class v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2409221
    const-class v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2409242
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2409243
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2409240
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2409241
    iget v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2409234
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2409235
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 2409236
    iget v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 2409237
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;->f:D

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2409238
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2409239
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2409231
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2409232
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2409233
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2409227
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2409228
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;->e:I

    .line 2409229
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;->f:D

    .line 2409230
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2409224
    new-instance v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;

    invoke-direct {v0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;-><init>()V

    .line 2409225
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2409226
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2409223
    const v0, 0x63d303f8

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2409222
    const v0, -0x6e856243

    return v0
.end method

.method public final j()D
    .locals 2

    .prologue
    .line 2409219
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2409220
    iget-wide v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;->f:D

    return-wide v0
.end method
