.class public Lcom/facebook/local/surface/LocalSurfaceFragmentActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# static fields
.field private static final p:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final q:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2409937
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/facebook/local/surface/LocalSurfaceFragmentActivity;->p:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 2409938
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-ge v0, v1, :cond_0

    invoke-static {}, Lcom/facebook/local/surface/LocalSurfaceFragmentActivity;->a()I

    move-result v0

    :goto_0
    sput v0, Lcom/facebook/local/surface/LocalSurfaceFragmentActivity;->q:I

    return-void

    :cond_0
    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2409939
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a()I
    .locals 3

    .prologue
    .line 2409940
    :cond_0
    sget-object v0, Lcom/facebook/local/surface/LocalSurfaceFragmentActivity;->p:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    .line 2409941
    add-int/lit8 v0, v1, 0x1

    .line 2409942
    const v2, 0xffffff

    if-le v0, v2, :cond_1

    const/4 v0, 0x1

    .line 2409943
    :cond_1
    sget-object v2, Lcom/facebook/local/surface/LocalSurfaceFragmentActivity;->p:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2, v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2409944
    return v1
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, -0x1

    .line 2409945
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2409946
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2409947
    sget v1, Lcom/facebook/local/surface/LocalSurfaceFragmentActivity;->q:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setId(I)V

    .line 2409948
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/facebook/local/surface/LocalSurfaceFragmentActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2409949
    if-nez p1, :cond_0

    .line 2409950
    new-instance v0, Lcom/facebook/local/surface/LocalSurfaceFragment;

    invoke-direct {v0}, Lcom/facebook/local/surface/LocalSurfaceFragment;-><init>()V

    .line 2409951
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    sget v2, Lcom/facebook/local/surface/LocalSurfaceFragmentActivity;->q:I

    invoke-virtual {v1, v2, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2409952
    :cond_0
    return-void
.end method
