.class public final Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x29adad02
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel$ItemsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2408998
    const-class v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2408978
    const-class v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2408996
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2408997
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2408994
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel;->e:Ljava/lang/String;

    .line 2408995
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel$ItemsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2408992
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel$ItemsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel;->f:Ljava/util/List;

    .line 2408993
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2408999
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2409000
    invoke-direct {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2409001
    invoke-direct {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 2409002
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2409003
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2409004
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2409005
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2409006
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2408984
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2408985
    invoke-direct {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2408986
    invoke-direct {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2408987
    if-eqz v1, :cond_0

    .line 2408988
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel;

    .line 2408989
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel;->f:Ljava/util/List;

    .line 2408990
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2408991
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2408981
    new-instance v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel;

    invoke-direct {v0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel;-><init>()V

    .line 2408982
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2408983
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2408980
    const v0, 0x5f812c9d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2408979
    const v0, -0x3ebdf4dd

    return v0
.end method
