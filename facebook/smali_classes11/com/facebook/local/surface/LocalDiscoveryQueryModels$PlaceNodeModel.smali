.class public final Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x43cd0147
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$AddressModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$LocationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$PlaceOpenStatusModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2409317
    const-class v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2409318
    const-class v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2409319
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2409320
    return-void
.end method

.method private u()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2409321
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->p:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->p:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2409322
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->p:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 13

    .prologue
    .line 2409323
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2409324
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->j()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$AddressModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2409325
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2409326
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->l()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$LocationModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2409327
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2409328
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->n()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2409329
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->o()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$PlaceOpenStatusModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2409330
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->p()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 2409331
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->q()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2409332
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2409333
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->s()LX/0Px;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/util/List;)I

    move-result v9

    .line 2409334
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->t()Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    .line 2409335
    invoke-direct {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->u()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v11

    invoke-virtual {p1, v11}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v11

    .line 2409336
    const/16 v12, 0xc

    invoke-virtual {p1, v12}, LX/186;->c(I)V

    .line 2409337
    const/4 v12, 0x0

    invoke-virtual {p1, v12, v0}, LX/186;->b(II)V

    .line 2409338
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2409339
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2409340
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2409341
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2409342
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2409343
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2409344
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2409345
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2409346
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 2409347
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 2409348
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 2409349
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2409350
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2409351
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2409352
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->j()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$AddressModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2409353
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->j()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$AddressModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$AddressModel;

    .line 2409354
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->j()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$AddressModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2409355
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;

    .line 2409356
    iput-object v0, v1, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->e:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$AddressModel;

    .line 2409357
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->l()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$LocationModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2409358
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->l()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$LocationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$LocationModel;

    .line 2409359
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->l()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$LocationModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2409360
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;

    .line 2409361
    iput-object v0, v1, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->g:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$LocationModel;

    .line 2409362
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->n()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2409363
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->n()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;

    .line 2409364
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->n()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2409365
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;

    .line 2409366
    iput-object v0, v1, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->i:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;

    .line 2409367
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->o()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$PlaceOpenStatusModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2409368
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->o()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$PlaceOpenStatusModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$PlaceOpenStatusModel;

    .line 2409369
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->o()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$PlaceOpenStatusModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2409370
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;

    .line 2409371
    iput-object v0, v1, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->j:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$PlaceOpenStatusModel;

    .line 2409372
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2409373
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2409374
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 2409375
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;

    .line 2409376
    iput-object v0, v1, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->m:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2409377
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2409378
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2409379
    new-instance v0, LX/Gy5;

    invoke-direct {v0, p1}, LX/Gy5;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2409380
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2409292
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2409293
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2409381
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2409382
    new-instance v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;

    invoke-direct {v0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;-><init>()V

    .line 2409383
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2409384
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2409316
    const v0, -0xfecc545

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2409385
    const v0, 0x25d6af

    return v0
.end method

.method public final j()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$AddressModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2409314
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->e:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$AddressModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$AddressModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$AddressModel;

    iput-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->e:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$AddressModel;

    .line 2409315
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->e:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$AddressModel;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2409312
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->f:Ljava/lang/String;

    .line 2409313
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$LocationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2409310
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->g:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$LocationModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$LocationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$LocationModel;

    iput-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->g:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$LocationModel;

    .line 2409311
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->g:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$LocationModel;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2409308
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->h:Ljava/lang/String;

    .line 2409309
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2409306
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->i:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;

    iput-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->i:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;

    .line 2409307
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->i:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;

    return-object v0
.end method

.method public final o()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$PlaceOpenStatusModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2409304
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->j:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$PlaceOpenStatusModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$PlaceOpenStatusModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$PlaceOpenStatusModel;

    iput-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->j:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$PlaceOpenStatusModel;

    .line 2409305
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->j:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$PlaceOpenStatusModel;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2409302
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->k:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    iput-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->k:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 2409303
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->k:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2409300
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->l:Ljava/lang/String;

    .line 2409301
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2409298
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->m:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->m:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2409299
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->m:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method public final s()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2409296
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->n:Ljava/util/List;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->n:Ljava/util/List;

    .line 2409297
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->n:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2409294
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->o:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    iput-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->o:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 2409295
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->o:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    return-object v0
.end method
