.class public final Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x502e8fd1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2409070
    const-class v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2409069
    const-class v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2409067
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2409068
    return-void
.end method

.method private k()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2409065
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->e:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel;

    iput-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->e:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel;

    .line 2409066
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->e:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel;

    return-object v0
.end method

.method private l()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2409063
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$FiltersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->f:Ljava/util/List;

    .line 2409064
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 2409050
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2409051
    invoke-direct {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->k()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2409052
    invoke-direct {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->l()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 2409053
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->a()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 2409054
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->j()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x4e363642    # 7.6425229E8f

    invoke-static {v4, v3, v5}, Lcom/facebook/controller/connectioncontroller/pageinfo/ConnectionControllerPageInfoGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/controller/connectioncontroller/pageinfo/ConnectionControllerPageInfoGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2409055
    const/4 v4, 0x5

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2409056
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2409057
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2409058
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2409059
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2409060
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2409061
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2409062
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2409071
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->h:Ljava/util/List;

    .line 2409072
    iget-object v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2409025
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2409026
    invoke-direct {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->k()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2409027
    invoke-direct {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->k()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel;

    .line 2409028
    invoke-direct {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->k()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2409029
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;

    .line 2409030
    iput-object v0, v1, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->e:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel;

    .line 2409031
    :cond_0
    invoke-direct {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->l()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2409032
    invoke-direct {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->l()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 2409033
    if-eqz v2, :cond_1

    .line 2409034
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;

    .line 2409035
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->f:Ljava/util/List;

    move-object v1, v0

    .line 2409036
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2409037
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 2409038
    if-eqz v2, :cond_2

    .line 2409039
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;

    .line 2409040
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->h:Ljava/util/List;

    move-object v1, v0

    .line 2409041
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 2409042
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x4e363642    # 7.6425229E8f

    invoke-static {v2, v0, v3}, Lcom/facebook/controller/connectioncontroller/pageinfo/ConnectionControllerPageInfoGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/controller/connectioncontroller/pageinfo/ConnectionControllerPageInfoGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2409043
    invoke-virtual {p0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2409044
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;

    .line 2409045
    iput v3, v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->i:I

    move-object v1, v0

    .line 2409046
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2409047
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    .line 2409048
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_4
    move-object p0, v1

    .line 2409049
    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2409021
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2409022
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->g:Z

    .line 2409023
    const/4 v0, 0x4

    const v1, 0x4e363642    # 7.6425229E8f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->i:I

    .line 2409024
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2409018
    new-instance v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;

    invoke-direct {v0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;-><init>()V

    .line 2409019
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2409020
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2409017
    const v0, -0x4bc1b274

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2409016
    const v0, -0x7c73ffc3

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2409014
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2409015
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
