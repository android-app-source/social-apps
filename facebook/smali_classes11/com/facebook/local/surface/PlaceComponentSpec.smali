.class public Lcom/facebook/local/surface/PlaceComponentSpec;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/GyN;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nG;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2410147
    const-class v0, Lcom/facebook/local/surface/PlaceComponentSpec;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "local_surface"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/local/surface/PlaceComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/GyN;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/GyN;",
            "LX/0Ot",
            "<",
            "LX/1nG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2410130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2410131
    iput-object p1, p0, Lcom/facebook/local/surface/PlaceComponentSpec;->b:LX/0Or;

    .line 2410132
    iput-object p2, p0, Lcom/facebook/local/surface/PlaceComponentSpec;->c:LX/GyN;

    .line 2410133
    iput-object p3, p0, Lcom/facebook/local/surface/PlaceComponentSpec;->d:LX/0Ot;

    .line 2410134
    iput-object p4, p0, Lcom/facebook/local/surface/PlaceComponentSpec;->e:LX/0Ot;

    .line 2410135
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/local/surface/PlaceComponentSpec;
    .locals 7

    .prologue
    .line 2410136
    const-class v1, Lcom/facebook/local/surface/PlaceComponentSpec;

    monitor-enter v1

    .line 2410137
    :try_start_0
    sget-object v0, Lcom/facebook/local/surface/PlaceComponentSpec;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2410138
    sput-object v2, Lcom/facebook/local/surface/PlaceComponentSpec;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2410139
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2410140
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2410141
    new-instance v4, Lcom/facebook/local/surface/PlaceComponentSpec;

    const/16 v3, 0x509

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/GyN;->a(LX/0QB;)LX/GyN;

    move-result-object v3

    check-cast v3, LX/GyN;

    const/16 v6, 0xb19

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x2eb

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v5, v3, v6, p0}, Lcom/facebook/local/surface/PlaceComponentSpec;-><init>(LX/0Or;LX/GyN;LX/0Ot;LX/0Ot;)V

    .line 2410142
    move-object v0, v4

    .line 2410143
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2410144
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/local/surface/PlaceComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2410145
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2410146
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
