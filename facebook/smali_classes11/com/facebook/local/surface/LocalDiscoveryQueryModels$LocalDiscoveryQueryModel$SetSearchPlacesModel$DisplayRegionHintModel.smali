.class public final Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x40239636
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel$Serializer;
.end annotation


# instance fields
.field private e:D

.field private f:D

.field private g:D

.field private h:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2408909
    const-class v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2408908
    const-class v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2408884
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2408885
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 2408900
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2408901
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 2408902
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel;->e:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2408903
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel;->f:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2408904
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel;->g:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2408905
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel;->h:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2408906
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2408907
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2408897
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2408898
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2408899
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2408891
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2408892
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel;->e:D

    .line 2408893
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel;->f:D

    .line 2408894
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel;->g:D

    .line 2408895
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel;->h:D

    .line 2408896
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2408888
    new-instance v0, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel;

    invoke-direct {v0}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$LocalDiscoveryQueryModel$SetSearchPlacesModel$DisplayRegionHintModel;-><init>()V

    .line 2408889
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2408890
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2408887
    const v0, 0x3576e300

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2408886
    const v0, -0x7960c2c2

    return v0
.end method
