.class public Lcom/facebook/local/surface/LocalSurfaceFragment;
.super Lcom/facebook/components/list/fb/fragment/ListComponentFragment;
.source ""


# instance fields
.field private a:LX/2kW;

.field public b:LX/1rq;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/GyJ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/GyL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2409936
    invoke-direct {p0}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/local/surface/LocalSurfaceFragment;

    const-class v1, LX/1rq;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/1rq;

    invoke-static {p0}, LX/GyJ;->a(LX/0QB;)LX/GyJ;

    move-result-object v2

    check-cast v2, LX/GyJ;

    new-instance v0, LX/GyL;

    invoke-direct {v0}, LX/GyL;-><init>()V

    move-object v0, v0

    move-object p0, v0

    check-cast p0, LX/GyL;

    iput-object v1, p1, Lcom/facebook/local/surface/LocalSurfaceFragment;->b:LX/1rq;

    iput-object v2, p1, Lcom/facebook/local/surface/LocalSurfaceFragment;->c:LX/GyJ;

    iput-object p0, p1, Lcom/facebook/local/surface/LocalSurfaceFragment;->d:LX/GyL;

    return-void
.end method


# virtual methods
.method public final a(LX/BcP;LX/BcQ;)LX/BcO;
    .locals 3

    .prologue
    .line 2409921
    iget-object v0, p0, Lcom/facebook/local/surface/LocalSurfaceFragment;->c:LX/GyJ;

    .line 2409922
    new-instance v1, LX/GyI;

    invoke-direct {v1, v0}, LX/GyI;-><init>(LX/GyJ;)V

    .line 2409923
    iget-object v2, v0, LX/GyJ;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/GyH;

    .line 2409924
    if-nez v2, :cond_0

    .line 2409925
    new-instance v2, LX/GyH;

    invoke-direct {v2, v0}, LX/GyH;-><init>(LX/GyJ;)V

    .line 2409926
    :cond_0
    iput-object v1, v2, LX/BcN;->a:LX/BcO;

    .line 2409927
    iput-object v1, v2, LX/GyH;->a:LX/GyI;

    .line 2409928
    iget-object v0, v2, LX/GyH;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2409929
    move-object v1, v2

    .line 2409930
    move-object v0, v1

    .line 2409931
    iget-object v1, p0, Lcom/facebook/local/surface/LocalSurfaceFragment;->a:LX/2kW;

    .line 2409932
    iget-object v2, v0, LX/GyH;->a:LX/GyI;

    iput-object v1, v2, LX/GyI;->b:LX/2kW;

    .line 2409933
    iget-object v2, v0, LX/GyH;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 2409934
    move-object v0, v0

    .line 2409935
    invoke-virtual {v0, p2}, LX/GyH;->b(LX/BcQ;)LX/GyH;

    move-result-object v0

    invoke-virtual {v0}, LX/GyH;->b()LX/BcO;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2409908
    invoke-super {p0, p1}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->a(Landroid/os/Bundle;)V

    .line 2409909
    const-class v0, Lcom/facebook/local/surface/LocalSurfaceFragment;

    invoke-static {v0, p0}, Lcom/facebook/local/surface/LocalSurfaceFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2409910
    iget-object v0, p0, Lcom/facebook/local/surface/LocalSurfaceFragment;->d:LX/GyL;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 2409911
    iput v1, v0, LX/GyL;->a:F

    .line 2409912
    iget-object v0, p0, Lcom/facebook/local/surface/LocalSurfaceFragment;->b:LX/1rq;

    const-string v1, "/local_discovery_surface/"

    iget-object v2, p0, Lcom/facebook/local/surface/LocalSurfaceFragment;->d:LX/GyL;

    invoke-virtual {v0, v1, v2}, LX/1rq;->a(Ljava/lang/String;LX/1rs;)LX/2jj;

    move-result-object v0

    const/4 v1, 0x1

    .line 2409913
    iput-boolean v1, v0, LX/2jj;->k:Z

    .line 2409914
    move-object v0, v0

    .line 2409915
    const-wide/16 v2, 0x0

    .line 2409916
    iput-wide v2, v0, LX/2jj;->j:J

    .line 2409917
    move-object v0, v0

    .line 2409918
    invoke-virtual {v0}, LX/2jj;->a()LX/2kW;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/local/surface/LocalSurfaceFragment;->a:LX/2kW;

    .line 2409919
    return-void
.end method

.method public final d()LX/BcG;
    .locals 1

    .prologue
    .line 2409920
    const/4 v0, 0x0

    return-object v0
.end method
