.class public Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:[Ljava/lang/String;

.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final c:Lcom/facebook/common/callercontext/CallerContext;

.field public static final d:Lcom/facebook/location/FbLocationOperationParams;


# instance fields
.field private e:LX/0i4;

.field public final f:LX/03V;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/GEJ;

.field public final i:LX/1Ck;

.field public j:LX/GDS;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 2374425
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.ACCESS_COARSE_LOCATION"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->a:[Ljava/lang/String;

    .line 2374426
    const-class v0, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;

    sput-object v0, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->b:Ljava/lang/Class;

    .line 2374427
    const-class v0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    const-string v1, "location_fetcher"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 2374428
    sget-object v0, LX/0yF;->HIGH_ACCURACY:LX/0yF;

    invoke-static {v0}, Lcom/facebook/location/FbLocationOperationParams;->a(LX/0yF;)LX/1S7;

    move-result-object v0

    const-wide/32 v2, 0xdbba0

    invoke-virtual {v0, v2, v3}, LX/1S7;->c(J)LX/1S7;

    move-result-object v0

    const-wide/32 v2, 0x2bf20

    .line 2374429
    iput-wide v2, v0, LX/1S7;->b:J

    .line 2374430
    move-object v0, v0

    .line 2374431
    const/high16 v1, 0x43fa0000    # 500.0f

    .line 2374432
    iput v1, v0, LX/1S7;->c:F

    .line 2374433
    move-object v0, v0

    .line 2374434
    const-wide/16 v2, 0x1388

    .line 2374435
    iput-wide v2, v0, LX/1S7;->d:J

    .line 2374436
    move-object v0, v0

    .line 2374437
    invoke-virtual {v0}, LX/1S7;->a()Lcom/facebook/location/FbLocationOperationParams;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->d:Lcom/facebook/location/FbLocationOperationParams;

    return-void
.end method

.method public constructor <init>(LX/0i4;LX/03V;LX/1Ck;LX/0Or;LX/GEJ;LX/GDS;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0i4;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1Ck;",
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;",
            "LX/GEJ;",
            "LX/GDS;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2374438
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2374439
    iput-object p1, p0, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->e:LX/0i4;

    .line 2374440
    iput-object p2, p0, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->f:LX/03V;

    .line 2374441
    iput-object p4, p0, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->g:LX/0Or;

    .line 2374442
    iput-object p5, p0, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->h:LX/GEJ;

    .line 2374443
    iput-object p3, p0, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->i:LX/1Ck;

    .line 2374444
    iput-object p6, p0, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->j:LX/GDS;

    .line 2374445
    return-void
.end method

.method public static a$redex0(Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;DDLX/GDA;)V
    .locals 7

    .prologue
    .line 2374446
    iget-object v0, p0, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->i:LX/1Ck;

    sget-object v1, LX/Gdh;->FETCH_RADIUS:LX/Gdh;

    iget-object v2, p0, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->h:LX/GEJ;

    .line 2374447
    new-instance v3, LX/2vb;

    invoke-direct {v3}, LX/2vb;-><init>()V

    .line 2374448
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/2vb;->a(Ljava/lang/Double;)LX/2vb;

    .line 2374449
    invoke-static {p3, p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/2vb;->b(Ljava/lang/Double;)LX/2vb;

    .line 2374450
    new-instance v4, LX/ABq;

    invoke-direct {v4}, LX/ABq;-><init>()V

    move-object v4, v4

    .line 2374451
    const-string v5, "coordinates"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v3

    move-object v3, v3

    .line 2374452
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    move-object v3, v3

    .line 2374453
    iget-object v4, v2, LX/GEJ;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    move-object v2, v3

    .line 2374454
    new-instance v3, LX/Gdg;

    invoke-direct {v3, p0, p5}, LX/Gdg;-><init>(Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;LX/GDA;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2374455
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;
    .locals 8

    .prologue
    .line 2374456
    new-instance v0, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;

    const-class v1, LX/0i4;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/0i4;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    const/16 v4, 0xc81

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    .line 2374457
    new-instance v7, LX/GEJ;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {p0}, LX/2U4;->a(LX/0QB;)LX/2U4;

    move-result-object v6

    check-cast v6, LX/2U4;

    invoke-direct {v7, v5, v6}, LX/GEJ;-><init>(LX/0tX;LX/2U4;)V

    .line 2374458
    move-object v5, v7

    .line 2374459
    check-cast v5, LX/GEJ;

    invoke-static {p0}, LX/GDS;->b(LX/0QB;)LX/GDS;

    move-result-object v6

    check-cast v6, LX/GDS;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;-><init>(LX/0i4;LX/03V;LX/1Ck;LX/0Or;LX/GEJ;LX/GDS;)V

    .line 2374460
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2374461
    iget-object v0, p0, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->i:LX/1Ck;

    sget-object v1, LX/Gdh;->FETCH_LOCATION:LX/Gdh;

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2374462
    iget-object v0, p0, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->i:LX/1Ck;

    sget-object v1, LX/Gdh;->FETCH_RADIUS:LX/Gdh;

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2374463
    return-void
.end method

.method public final a(LX/GDA;Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 2374464
    iget-object v0, p0, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->e:LX/0i4;

    invoke-virtual {v0, p2}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    .line 2374465
    sget-object v1, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->a:[Ljava/lang/String;

    new-instance v2, LX/Gde;

    invoke-direct {v2, p0, p1}, LX/Gde;-><init>(Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;LX/GDA;)V

    invoke-virtual {v0, v1, v2}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 2374466
    return-void
.end method
