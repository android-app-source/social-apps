.class public Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;
.super LX/Gdm;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:LX/2eR;

.field private static q:LX/0Xm;


# instance fields
.field public final c:Landroid/content/Context;

.field public final d:LX/0hB;

.field private final e:LX/1qa;

.field public final f:LX/03V;

.field public final g:I

.field public final h:I

.field public i:I

.field public j:I

.field private final k:LX/1nA;

.field private final l:LX/17Q;

.field public final m:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final n:LX/AnL;

.field private final o:LX/Gdi;

.field public final p:LX/1Ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2374823
    const-class v0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;

    const-string v1, "photos_feed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2374824
    new-instance v0, LX/Gdu;

    invoke-direct {v0}, LX/Gdu;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->b:LX/2eR;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0hB;LX/1qa;LX/1nA;LX/17Q;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/03V;LX/AnL;LX/Gdi;LX/1Ad;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2374808
    invoke-direct {p0}, LX/Gdm;-><init>()V

    .line 2374809
    iput-object p1, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->c:Landroid/content/Context;

    .line 2374810
    iput-object p2, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->d:LX/0hB;

    .line 2374811
    iput-object p3, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->e:LX/1qa;

    .line 2374812
    iput-object p7, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->f:LX/03V;

    .line 2374813
    iget-object v0, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2374814
    const v1, 0x7f0b00bd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->h:I

    .line 2374815
    const v1, 0x7f0b0943

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget v1, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->h:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->g:I

    .line 2374816
    iput-object p4, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->k:LX/1nA;

    .line 2374817
    iput-object p5, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->l:LX/17Q;

    .line 2374818
    iput-object p6, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->m:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 2374819
    iput-object p8, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->n:LX/AnL;

    .line 2374820
    iput-object p9, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->o:LX/Gdi;

    .line 2374821
    iput-object p10, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->p:LX/1Ad;

    .line 2374822
    return-void
.end method

.method public static a(Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;)Landroid/view/View$OnClickListener;
    .locals 2

    .prologue
    .line 2374804
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLPageBrowserCategoryInfo;

    move-result-object v0

    .line 2374805
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageBrowserCategoryInfo;->a()Ljava/lang/String;

    move-result-object v0

    .line 2374806
    :goto_0
    new-instance v1, LX/Gdv;

    invoke-direct {v1, p0, v0}, LX/Gdv;-><init>(Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;Ljava/lang/String;)V

    return-object v1

    .line 2374807
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;
    .locals 14

    .prologue
    .line 2374793
    const-class v1, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;

    monitor-enter v1

    .line 2374794
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->q:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2374795
    sput-object v2, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->q:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2374796
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2374797
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2374798
    new-instance v3, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v5

    check-cast v5, LX/0hB;

    invoke-static {v0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v6

    check-cast v6, LX/1qa;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v7

    check-cast v7, LX/1nA;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v8

    check-cast v8, LX/17Q;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v9

    check-cast v9, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-static {v0}, LX/AnL;->b(LX/0QB;)LX/AnL;

    move-result-object v11

    check-cast v11, LX/AnL;

    invoke-static {v0}, LX/Gdi;->b(LX/0QB;)LX/Gdi;

    move-result-object v12

    check-cast v12, LX/Gdi;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v13

    check-cast v13, LX/1Ad;

    invoke-direct/range {v3 .. v13}, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;-><init>(Landroid/content/Context;LX/0hB;LX/1qa;LX/1nA;LX/17Q;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/03V;LX/AnL;LX/Gdi;LX/1Ad;)V

    .line 2374799
    move-object v0, v3

    .line 2374800
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2374801
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2374802
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2374803
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;LX/GgF;Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;LX/25E;LX/AnC;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 2374786
    iget-object v0, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->n:LX/AnL;

    iget-object v1, p1, LX/GgF;->f:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p2, p3, v1, p4}, LX/AnL;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;LX/25E;Lcom/facebook/fbui/glyph/GlyphView;LX/AnC;)V

    .line 2374787
    invoke-interface {p3}, LX/25E;->x()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {p3, p2}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v1

    invoke-static {v0, v1}, LX/17Q;->a(ZLX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    move-object v3, v0

    .line 2374788
    iget-object v7, p1, LX/GgF;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->k:LX/1nA;

    iget-object v1, p1, LX/GgF;->c:Landroid/widget/TextView;

    invoke-interface {p3}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-static {v2}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPage;)LX/1y5;

    move-result-object v2

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v6}, LX/1nA;->b(Landroid/view/View;LX/1y5;Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2374789
    iget-object v7, p1, LX/GgF;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->k:LX/1nA;

    iget-object v1, p1, LX/GgF;->d:Landroid/widget/TextView;

    invoke-interface {p3}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-static {v2}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPage;)LX/1y5;

    move-result-object v2

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v6}, LX/1nA;->b(Landroid/view/View;LX/1y5;Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2374790
    iget-object v7, p1, LX/GgF;->e:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->k:LX/1nA;

    iget-object v1, p1, LX/GgF;->e:Landroid/widget/TextView;

    invoke-interface {p3}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-static {v2}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPage;)LX/1y5;

    move-result-object v2

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v6}, LX/1nA;->b(Landroid/view/View;LX/1y5;Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2374791
    iget-object v7, p1, LX/GgF;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v0, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->k:LX/1nA;

    iget-object v1, p1, LX/GgF;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-interface {p3}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-static {v2}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPage;)LX/1y5;

    move-result-object v2

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v6}, LX/1nA;->b(Landroid/view/View;LX/1y5;Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2374792
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/99X;)F
    .locals 2

    .prologue
    .line 2374782
    sget-object v0, LX/99X;->FIRST:LX/99X;

    if-ne p1, v0, :cond_0

    .line 2374783
    iget v0, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->g:I

    iget v1, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->j:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->h:I

    add-int/2addr v0, v1

    .line 2374784
    int-to-float v0, v0

    iget v1, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->i:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 2374785
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public final a()LX/2eR;
    .locals 1

    .prologue
    .line 2374781
    sget-object v0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->b:LX/2eR;

    return-object v0
.end method

.method public final a(LX/Gdp;Landroid/support/v4/view/ViewPager;)V
    .locals 2

    .prologue
    .line 2374697
    iget-boolean v0, p1, LX/99W;->b:Z

    move v0, v0

    .line 2374698
    if-eqz v0, :cond_0

    .line 2374699
    invoke-virtual {p2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 2374700
    invoke-virtual {p1}, LX/0gG;->b()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2374701
    invoke-virtual {p2, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2374702
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Landroid/view/View;Ljava/lang/Object;LX/99X;LX/AnC;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2374728
    move-object v0, p2

    check-cast v0, LX/GgG;

    .line 2374729
    iget-object v1, v0, LX/GgG;->a:LX/GgF;

    move-object v1, v1

    .line 2374730
    check-cast p3, LX/25E;

    .line 2374731
    new-instance v2, LX/18u;

    invoke-direct {v2}, LX/18u;-><init>()V

    invoke-virtual {p2, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2374732
    sget-object v2, LX/99X;->FIRST:LX/99X;

    if-ne p4, v2, :cond_1

    .line 2374733
    iget v2, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->h:I

    iget v3, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->j:I

    invoke-virtual {v0, v2, v4, v3, v4}, LX/GgG;->setPadding(IIII)V

    .line 2374734
    :goto_0
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    .line 2374735
    invoke-interface {p3}, LX/25E;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2374736
    iget-object v0, v1, LX/GgF;->h:Landroid/view/View;

    if-nez v0, :cond_0

    .line 2374737
    iget-object v0, v1, LX/GgF;->g:Landroid/view/ViewStub;

    const v2, 0x7f0310b5

    invoke-virtual {v0, v2}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2374738
    iget-object v0, v1, LX/GgF;->g:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, LX/GgF;->h:Landroid/view/View;

    .line 2374739
    iget-object v0, v1, LX/GgF;->h:Landroid/view/View;

    const v2, 0x7f0d0b44

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {p0, p1}, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->a(Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2374740
    iget-object v0, v1, LX/GgF;->h:Landroid/view/View;

    const v2, 0x7f0d0b43

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {p0, p1}, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->a(Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2374741
    :cond_0
    iget-object v0, v1, LX/GgF;->a:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2374742
    iget-object v0, v1, LX/GgF;->h:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2374743
    :goto_1
    return-void

    .line 2374744
    :cond_1
    iget v2, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->j:I

    iget v3, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->j:I

    invoke-virtual {v0, v2, v4, v3, v4}, LX/GgG;->setPadding(IIII)V

    goto :goto_0

    .line 2374745
    :cond_2
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 2374746
    iget-object v0, v1, LX/GgF;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2374747
    iget-object v0, v1, LX/GgF;->h:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 2374748
    iget-object v0, v1, LX/GgF;->h:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2374749
    :cond_3
    invoke-interface {p3}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-static {v0}, LX/16z;->d(Lcom/facebook/graphql/model/GraphQLPage;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2374750
    invoke-interface {p3}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 2374751
    :goto_2
    move-object v2, v0

    .line 2374752
    if-eqz v2, :cond_7

    .line 2374753
    iget-object v0, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->p:LX/1Ad;

    sget-object p2, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object p2, v1, LX/GgF;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p2}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object p2

    invoke-virtual {v0, p2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-static {v2}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/1bf;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    .line 2374754
    iget-object v0, v1, LX/GgF;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    const p2, 0x7f0213f3

    invoke-virtual {v0, p2}, LX/1af;->b(I)V

    .line 2374755
    iget-object v0, v1, LX/GgF;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 p2, 0x0

    invoke-virtual {v0, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2374756
    iget-object v0, v1, LX/GgF;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p3}, LX/Gdi;->d(LX/25E;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2374757
    iget-object v0, v1, LX/GgF;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2374758
    :goto_3
    invoke-static {p3}, LX/Gdi;->d(LX/25E;)Ljava/lang/String;

    move-result-object v0

    .line 2374759
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2374760
    iget-object v2, v1, LX/GgF;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2374761
    iget-object v2, v1, LX/GgF;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2374762
    :goto_4
    invoke-interface {p3}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->r()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-interface {p3}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 2374763
    const-string v0, "/"

    invoke-interface {p3}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->r()LX/0Px;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 2374764
    :goto_5
    move-object v0, v0

    .line 2374765
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 2374766
    iget-object v2, v1, LX/GgF;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2374767
    iget-object v2, v1, LX/GgF;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2374768
    :goto_6
    invoke-interface {p3}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-static {v0}, LX/16z;->c(Lcom/facebook/graphql/model/GraphQLPage;)I

    move-result v0

    .line 2374769
    if-lez v0, :cond_a

    .line 2374770
    iget-object v2, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p2, 0x7f0810d0

    const p4, 0x7f0810d1

    invoke-static {v2, p2, p4, v0}, LX/1z0;->a(Landroid/content/res/Resources;III)Ljava/lang/String;

    move-result-object v0

    .line 2374771
    :goto_7
    move-object v0, v0

    .line 2374772
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 2374773
    iget-object v2, v1, LX/GgF;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2374774
    iget-object v2, v1, LX/GgF;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2374775
    :goto_8
    invoke-static {p0, v1, p1, p3, p5}, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->b(Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;LX/GgF;Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;LX/25E;LX/AnC;)V

    .line 2374776
    goto/16 :goto_1

    .line 2374777
    :cond_4
    iget-object v0, v1, LX/GgF;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    .line 2374778
    :cond_5
    iget-object v0, v1, LX/GgF;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_6

    .line 2374779
    :cond_6
    iget-object v0, v1, LX/GgF;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_8

    .line 2374780
    :cond_7
    iget-object v0, v1, LX/GgF;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_9
    const/4 v0, 0x0

    goto :goto_5

    :cond_a
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Landroid/widget/TextView;Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2374714
    invoke-interface {p1}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->p()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 2374715
    invoke-interface {p1}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->p()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/25E;

    .line 2374716
    invoke-interface {p1}, Lcom/facebook/graphql/model/ItemListFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Lcom/facebook/graphql/model/ItemListFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2374717
    invoke-interface {p1}, Lcom/facebook/graphql/model/ItemListFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    .line 2374718
    :goto_0
    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    .line 2374719
    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2374720
    const/16 v0, 0x8

    invoke-virtual {p3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2374721
    :goto_1
    return-void

    .line 2374722
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0f0066

    invoke-virtual {v1, v3, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 2374723
    :cond_1
    iget-object v2, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->o:LX/Gdi;

    invoke-virtual {v2, p1, v0}, LX/Gdi;->b(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;LX/25E;)Landroid/text/Spannable;

    move-result-object v0

    .line 2374724
    if-eqz v0, :cond_2

    .line 2374725
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2374726
    :goto_2
    invoke-virtual {p3, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 2374727
    :cond_2
    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method public final a(Lcom/facebook/widget/CustomViewPager;Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 2374709
    iget-object v0, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->d:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->i:I

    .line 2374710
    iget v0, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->i:I

    iget p2, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->g:I

    sub-int/2addr v0, p2

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->j:I

    .line 2374711
    iget v0, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->h:I

    iget p2, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->j:I

    mul-int/lit8 p2, p2, 0x2

    add-int/2addr v0, p2

    .line 2374712
    mul-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 2374713
    return-void
.end method

.method public final a(Ljava/util/List;Lcom/facebook/widget/CustomViewPager;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2374706
    const/high16 v0, 0x43660000    # 230.0f

    iget-object v1, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v2, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 2374707
    invoke-virtual {p2, v0, v2}, Lcom/facebook/widget/CustomViewPager;->b(IZ)V

    .line 2374708
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 2374705
    iget v0, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->i:I

    iget v1, p0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->g:I

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final c()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2374704
    const-class v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2374703
    const/4 v0, 0x0

    return v0
.end method
