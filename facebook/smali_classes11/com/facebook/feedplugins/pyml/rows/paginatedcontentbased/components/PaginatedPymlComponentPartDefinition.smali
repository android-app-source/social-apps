.class public Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/0Uh;

.field private final e:LX/Gft;

.field private final f:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Gft;LX/0Uh;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2378175
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2378176
    iput-object p2, p0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlComponentPartDefinition;->e:LX/Gft;

    .line 2378177
    iput-object p3, p0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlComponentPartDefinition;->d:LX/0Uh;

    .line 2378178
    iput-object p4, p0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlComponentPartDefinition;->f:LX/1V0;

    .line 2378179
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2378208
    new-instance v0, LX/1X6;

    sget-object v1, LX/2eF;->a:LX/1Ua;

    invoke-direct {v0, p2, v1}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 2378209
    iget-object v1, p0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlComponentPartDefinition;->e:LX/Gft;

    const/4 v2, 0x0

    .line 2378210
    new-instance v3, LX/Gfs;

    invoke-direct {v3, v1}, LX/Gfs;-><init>(LX/Gft;)V

    .line 2378211
    iget-object v4, v1, LX/Gft;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Gfr;

    .line 2378212
    if-nez v4, :cond_0

    .line 2378213
    new-instance v4, LX/Gfr;

    invoke-direct {v4, v1}, LX/Gfr;-><init>(LX/Gft;)V

    .line 2378214
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/Gfr;->a$redex0(LX/Gfr;LX/1De;IILX/Gfs;)V

    .line 2378215
    move-object v3, v4

    .line 2378216
    move-object v2, v3

    .line 2378217
    move-object v1, v2

    .line 2378218
    iget-object v2, v1, LX/Gfr;->a:LX/Gfs;

    iput-object p2, v2, LX/Gfs;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2378219
    iget-object v2, v1, LX/Gfr;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2378220
    move-object v1, v1

    .line 2378221
    iget-object v2, v1, LX/Gfr;->a:LX/Gfs;

    iput-object p3, v2, LX/Gfs;->b:LX/1Pn;

    .line 2378222
    iget-object v2, v1, LX/Gfr;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2378223
    move-object v1, v1

    .line 2378224
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 2378225
    iget-object v2, p0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    .line 2378226
    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlComponentPartDefinition;
    .locals 7

    .prologue
    .line 2378197
    const-class v1, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlComponentPartDefinition;

    monitor-enter v1

    .line 2378198
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2378199
    sput-object v2, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2378200
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2378201
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2378202
    new-instance p0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/Gft;->a(LX/0QB;)LX/Gft;

    move-result-object v4

    check-cast v4, LX/Gft;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlComponentPartDefinition;-><init>(Landroid/content/Context;LX/Gft;LX/0Uh;LX/1V0;)V

    .line 2378203
    move-object v0, p0

    .line 2378204
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2378205
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2378206
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2378207
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2378196
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2378195
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 2378194
    check-cast p1, LX/1Pn;

    invoke-virtual {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1Pn;)Z

    move-result v0

    return v0
.end method

.method public final a(LX/1Pn;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 2378193
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 2378182
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2378183
    iget-object v0, p0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlComponentPartDefinition;->d:LX/0Uh;

    const/16 v3, 0x5bd

    invoke-virtual {v0, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2378184
    :goto_0
    return v0

    .line 2378185
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2378186
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

    .line 2378187
    if-nez v0, :cond_1

    move-object v0, v2

    .line 2378188
    :goto_1
    if-nez v0, :cond_2

    move-object v0, v2

    .line 2378189
    :goto_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    .line 2378190
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;

    move-result-object v0

    goto :goto_1

    .line 2378191
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;->a()LX/0Px;

    move-result-object v0

    goto :goto_2

    :cond_3
    move v0, v1

    .line 2378192
    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2378180
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2378181
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
