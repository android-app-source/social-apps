.class public Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field private final b:LX/1nu;

.field public final c:LX/Gfl;

.field public final d:LX/0hy;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2378758
    const-class v0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;

    const-string v1, "native_newsfeed"

    const-string v2, "share_photo"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/Gfl;LX/0hy;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2378759
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2378760
    iput-object p1, p0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;->b:LX/1nu;

    .line 2378761
    iput-object p2, p0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;->c:LX/Gfl;

    .line 2378762
    iput-object p3, p0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;->d:LX/0hy;

    .line 2378763
    return-void
.end method

.method public static a(LX/1De;)LX/1Di;
    .locals 2

    .prologue
    .line 2378764
    invoke-static {p0}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/25Q;->h(I)LX/25Q;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const v1, 0x7f0b22fc

    invoke-interface {v0, v1}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, LX/1Di;->b(F)LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;LX/1De;Lcom/facebook/graphql/model/GraphQLNode;LX/1Pp;)LX/1Di;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            "TE;)",
            "LX/1Di;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x2

    const/high16 v5, 0x3f800000    # 1.0f

    .line 2378765
    invoke-static {p2}, LX/Gfl;->c(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 2378766
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2378767
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    .line 2378768
    :goto_0
    move-object v0, v0

    .line 2378769
    invoke-static {p2}, LX/Gfl;->c(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 2378770
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v2

    :goto_1
    move-object v2, v2

    .line 2378771
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    .line 2378772
    const v4, -0x67704e08

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p2, v7, v8

    invoke-static {p1, v4, v7}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2378773
    invoke-interface {v3, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b22fc

    invoke-interface {v3, v4}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v3

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_2
    invoke-interface {v3, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    if-nez v2, :cond_1

    :goto_3
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v4, p0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;->b:LX/1nu;

    invoke-virtual {v4, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v0

    sget-object v4, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v4}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v0

    sget-object v4, LX/1Up;->g:LX/1Up;

    invoke-virtual {v0, v4}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v0

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/1nw;->a(Z)LX/1nw;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1nw;->a(LX/1Pp;)LX/1nw;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, v5}, LX/1Di;->b(F)LX/1Di;

    move-result-object v0

    goto :goto_2

    :cond_1
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const v2, 0x7f0a010c

    invoke-virtual {v1, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0b0052

    invoke-virtual {v1, v2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v1

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v1, v6}, LX/1Di;->b(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1, v5}, LX/1Di;->b(F)LX/1Di;

    move-result-object v1

    const/4 v2, 0x4

    const v3, 0x7f0b2300

    invoke-interface {v1, v2, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    goto :goto_3

    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_1
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;
    .locals 6

    .prologue
    .line 2378774
    const-class v1, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;

    monitor-enter v1

    .line 2378775
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2378776
    sput-object v2, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2378777
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2378778
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2378779
    new-instance p0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/Gfl;->a(LX/0QB;)LX/Gfl;

    move-result-object v4

    check-cast v4, LX/Gfl;

    invoke-static {v0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v5

    check-cast v5, LX/0hy;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;-><init>(LX/1nu;LX/Gfl;LX/0hy;)V

    .line 2378780
    move-object v0, p0

    .line 2378781
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2378782
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2378783
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2378784
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
