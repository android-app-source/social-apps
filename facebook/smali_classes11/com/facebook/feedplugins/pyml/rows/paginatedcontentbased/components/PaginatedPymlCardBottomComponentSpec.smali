.class public Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlCardBottomComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/1vg;

.field public final c:LX/Gfl;

.field public final d:LX/1nu;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2378057
    const-class v0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlCardBottomComponentSpec;

    const-string v1, "native_newsfeed"

    const-string v2, "actor_photo"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlCardBottomComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1vg;LX/Gfl;LX/1nu;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2378058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2378059
    iput-object p1, p0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlCardBottomComponentSpec;->b:LX/1vg;

    .line 2378060
    iput-object p2, p0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlCardBottomComponentSpec;->c:LX/Gfl;

    .line 2378061
    iput-object p3, p0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlCardBottomComponentSpec;->d:LX/1nu;

    .line 2378062
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlCardBottomComponentSpec;
    .locals 6

    .prologue
    .line 2378063
    const-class v1, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlCardBottomComponentSpec;

    monitor-enter v1

    .line 2378064
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlCardBottomComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2378065
    sput-object v2, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlCardBottomComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2378066
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2378067
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2378068
    new-instance p0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlCardBottomComponentSpec;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v3

    check-cast v3, LX/1vg;

    invoke-static {v0}, LX/Gfl;->a(LX/0QB;)LX/Gfl;

    move-result-object v4

    check-cast v4, LX/Gfl;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v5

    check-cast v5, LX/1nu;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlCardBottomComponentSpec;-><init>(LX/1vg;LX/Gfl;LX/1nu;)V

    .line 2378069
    move-object v0, p0

    .line 2378070
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2378071
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlCardBottomComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2378072
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2378073
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
