.class public Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field private final b:LX/1nu;

.field public final c:LX/Gfl;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2378652
    const-class v0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;

    const-string v1, "native_newsfeed"

    const-string v2, "photo_grid"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/Gfl;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2378653
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2378654
    iput-object p1, p0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;->b:LX/1nu;

    .line 2378655
    iput-object p2, p0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;->c:LX/Gfl;

    .line 2378656
    return-void
.end method

.method public static a(Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;LX/1De;Landroid/net/Uri;Landroid/net/Uri;LX/1Pp;)LX/1Di;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "TE;)",
            "LX/1Di;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x2

    const/4 v6, 0x1

    const/high16 v5, 0x3f000000    # 0.5f

    .line 2378657
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    if-nez p2, :cond_0

    move-object v0, v1

    :goto_0
    invoke-interface {v2, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    if-nez p3, :cond_1

    :goto_1
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;->b:LX/1nu;

    invoke-virtual {v0, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v0

    sget-object v3, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v3}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v0

    sget-object v3, LX/1Up;->g:LX/1Up;

    invoke-virtual {v0, v3}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/1nw;->a(Z)LX/1nw;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/1nw;->a(LX/1Pp;)LX/1nw;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, v5}, LX/1Di;->b(F)LX/1Di;

    move-result-object v0

    const v3, 0x7f0b22fc

    invoke-interface {v0, v3}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    const v3, 0x7f0b22fd

    invoke-interface {v0, v4, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    const/4 v3, 0x5

    const v4, 0x7f0b22fd

    invoke-interface {v0, v3, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;->b:LX/1nu;

    invoke-virtual {v1, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v1

    sget-object v2, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v1

    sget-object v2, LX/1Up;->g:LX/1Up;

    invoke-virtual {v1, v2}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v1

    invoke-virtual {v1, v6}, LX/1nw;->a(Z)LX/1nw;

    move-result-object v1

    invoke-virtual {v1, p4}, LX/1nw;->a(LX/1Pp;)LX/1nw;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v1, v5}, LX/1Di;->b(F)LX/1Di;

    move-result-object v1

    const v2, 0x7f0b22fc

    invoke-interface {v1, v2}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    const/4 v2, 0x0

    const v3, 0x7f0b22fd

    invoke-interface {v1, v2, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    const/4 v2, 0x4

    const v3, 0x7f0b22fd

    invoke-interface {v1, v2, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    goto :goto_1
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;
    .locals 5

    .prologue
    .line 2378658
    const-class v1, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;

    monitor-enter v1

    .line 2378659
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2378660
    sput-object v2, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2378661
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2378662
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2378663
    new-instance p0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/Gfl;->a(LX/0QB;)LX/Gfl;

    move-result-object v4

    check-cast v4, LX/Gfl;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;-><init>(LX/1nu;LX/Gfl;)V

    .line 2378664
    move-object v0, p0

    .line 2378665
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2378666
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2378667
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2378668
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
