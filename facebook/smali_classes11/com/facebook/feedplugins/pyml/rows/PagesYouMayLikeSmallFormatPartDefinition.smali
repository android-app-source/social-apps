.class public Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikeSmallFormatPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2375449
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2375450
    iput-object p1, p0, Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikeSmallFormatPartDefinition;->a:Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;

    .line 2375451
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikeSmallFormatPartDefinition;
    .locals 4

    .prologue
    .line 2375438
    const-class v1, Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikeSmallFormatPartDefinition;

    monitor-enter v1

    .line 2375439
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikeSmallFormatPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2375440
    sput-object v2, Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikeSmallFormatPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2375441
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2375442
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2375443
    new-instance p0, Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikeSmallFormatPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikeSmallFormatPartDefinition;-><init>(Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;)V

    .line 2375444
    move-object v0, p0

    .line 2375445
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2375446
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikeSmallFormatPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2375447
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2375448
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2375434
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2375435
    iget-object v0, p0, Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikeSmallFormatPartDefinition;->a:Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2375436
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2375437
    const/4 v0, 0x1

    return v0
.end method
