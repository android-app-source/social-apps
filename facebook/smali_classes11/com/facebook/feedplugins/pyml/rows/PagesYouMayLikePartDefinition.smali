.class public Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikePartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pj;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;",
            "TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2375408
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2375409
    iput-object p2, p0, Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikePartDefinition;->b:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;

    .line 2375410
    iput-object p1, p0, Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikePartDefinition;->a:Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;

    .line 2375411
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikePartDefinition;
    .locals 5

    .prologue
    .line 2375412
    const-class v1, Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikePartDefinition;

    monitor-enter v1

    .line 2375413
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikePartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2375414
    sput-object v2, Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikePartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2375415
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2375416
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2375417
    new-instance p0, Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikePartDefinition;-><init>(Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;)V

    .line 2375418
    move-object v0, p0

    .line 2375419
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2375420
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2375421
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2375422
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2375423
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    .line 2375424
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2375425
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    .line 2375426
    invoke-static {v0}, LX/4ZZ;->a(Lcom/facebook/graphql/model/ItemListFeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object v1, v2

    .line 2375427
    :goto_0
    new-instance v3, LX/99i;

    invoke-direct {v3, p2, v1}, LX/99i;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/8yy;)V

    .line 2375428
    iget-object v1, p0, Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikePartDefinition;->a:Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;

    invoke-virtual {p1, v1, v3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2375429
    invoke-static {v0}, LX/4ZZ;->a(Lcom/facebook/graphql/model/ItemListFeedUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2375430
    iget-object v0, p0, Lcom/facebook/feedplugins/pyml/rows/PagesYouMayLikePartDefinition;->b:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;

    invoke-virtual {p1, v0, v3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2375431
    :cond_0
    return-object v2

    .line 2375432
    :cond_1
    invoke-static {}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;->b()LX/99l;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2375433
    const/4 v0, 0x1

    return v0
.end method
