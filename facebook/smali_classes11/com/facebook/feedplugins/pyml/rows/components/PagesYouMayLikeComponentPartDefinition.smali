.class public Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pj;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/99i",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final d:LX/1DR;

.field private final e:LX/GfA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/GfA",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/1V0;

.field private final g:LX/0pf;

.field private h:LX/1Ua;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1DR;LX/GfA;LX/1V0;LX/1V7;LX/0pf;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2377059
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2377060
    iput-object p2, p0, Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;->d:LX/1DR;

    .line 2377061
    iput-object p3, p0, Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;->e:LX/GfA;

    .line 2377062
    iput-object p4, p0, Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;->f:LX/1V0;

    .line 2377063
    invoke-static {}, LX/1UY;->c()LX/1UY;

    move-result-object v0

    invoke-virtual {p5}, LX/1V7;->c()F

    move-result v1

    neg-float v1, v1

    invoke-virtual {p5}, LX/1V7;->d()F

    move-result v2

    sub-float/2addr v1, v2

    .line 2377064
    iput v1, v0, LX/1UY;->b:F

    .line 2377065
    move-object v0, v0

    .line 2377066
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;->h:LX/1Ua;

    .line 2377067
    iput-object p6, p0, Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;->g:LX/0pf;

    .line 2377068
    return-void
.end method

.method private a(LX/1De;LX/99i;LX/1Pn;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/99i",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2377026
    new-instance v1, LX/1X6;

    iget-object v0, p2, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;->h:LX/1Ua;

    sget-object v3, LX/1X9;->TOP:LX/1X9;

    invoke-direct {v1, v0, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    .line 2377027
    iget-object v2, p0, Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;->g:LX/0pf;

    iget-object v0, p2, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2377028
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 2377029
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v2, v0}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v0

    .line 2377030
    iget-boolean v2, v0, LX/1g0;->o:Z

    move v2, v2

    .line 2377031
    iget-object v0, p0, Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;->e:LX/GfA;

    const/4 v3, 0x0

    .line 2377032
    new-instance v4, LX/Gf9;

    invoke-direct {v4, v0}, LX/Gf9;-><init>(LX/GfA;)V

    .line 2377033
    iget-object v5, v0, LX/GfA;->b:LX/0Zi;

    invoke-virtual {v5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/Gf8;

    .line 2377034
    if-nez v5, :cond_0

    .line 2377035
    new-instance v5, LX/Gf8;

    invoke-direct {v5, v0}, LX/Gf8;-><init>(LX/GfA;)V

    .line 2377036
    :cond_0
    invoke-static {v5, p1, v3, v3, v4}, LX/Gf8;->a$redex0(LX/Gf8;LX/1De;IILX/Gf9;)V

    .line 2377037
    move-object v4, v5

    .line 2377038
    move-object v3, v4

    .line 2377039
    move-object v0, v3

    .line 2377040
    iget-object v3, v0, LX/Gf8;->a:LX/Gf9;

    iput-object p2, v3, LX/Gf9;->b:LX/99i;

    .line 2377041
    iget-object v3, v0, LX/Gf8;->e:Ljava/util/BitSet;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2377042
    move-object v0, v0

    .line 2377043
    iget-object v3, p0, Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;->d:LX/1DR;

    invoke-virtual {v3}, LX/1DR;->a()I

    move-result v3

    .line 2377044
    iget-object v4, v0, LX/Gf8;->a:LX/Gf9;

    iput v3, v4, LX/Gf9;->c:I

    .line 2377045
    iget-object v4, v0, LX/Gf8;->e:Ljava/util/BitSet;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 2377046
    move-object v3, v0

    .line 2377047
    move-object v0, p3

    check-cast v0, LX/1Pp;

    .line 2377048
    iget-object v4, v3, LX/Gf8;->a:LX/Gf9;

    iput-object v0, v4, LX/Gf9;->a:LX/1Pp;

    .line 2377049
    iget-object v4, v3, LX/Gf8;->e:Ljava/util/BitSet;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 2377050
    move-object v3, v3

    .line 2377051
    if-nez v2, :cond_1

    const/4 v0, 0x1

    .line 2377052
    :goto_0
    iget-object v2, v3, LX/Gf8;->a:LX/Gf9;

    iput-boolean v0, v2, LX/Gf9;->d:Z

    .line 2377053
    iget-object v2, v3, LX/Gf8;->e:Ljava/util/BitSet;

    const/4 v4, 0x3

    invoke-virtual {v2, v4}, Ljava/util/BitSet;->set(I)V

    .line 2377054
    move-object v0, v3

    .line 2377055
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2377056
    iget-object v2, p0, Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    .line 2377057
    return-object v0

    .line 2377058
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;
    .locals 10

    .prologue
    .line 2377015
    const-class v1, Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;

    monitor-enter v1

    .line 2377016
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2377017
    sput-object v2, Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2377018
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2377019
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2377020
    new-instance v3, Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v5

    check-cast v5, LX/1DR;

    invoke-static {v0}, LX/GfA;->a(LX/0QB;)LX/GfA;

    move-result-object v6

    check-cast v6, LX/GfA;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v7

    check-cast v7, LX/1V0;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v8

    check-cast v8, LX/1V7;

    invoke-static {v0}, LX/0pf;->a(LX/0QB;)LX/0pf;

    move-result-object v9

    check-cast v9, LX/0pf;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;-><init>(Landroid/content/Context;LX/1DR;LX/GfA;LX/1V0;LX/1V7;LX/0pf;)V

    .line 2377021
    move-object v0, v3

    .line 2377022
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2377023
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2377024
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2377025
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2377014
    check-cast p2, LX/99i;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;->a(LX/1De;LX/99i;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2377013
    check-cast p2, LX/99i;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pyml/rows/components/PagesYouMayLikeComponentPartDefinition;->a(LX/1De;LX/99i;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 2377007
    check-cast p1, LX/1Pn;

    invoke-virtual {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1Pn;)Z

    move-result v0

    return v0
.end method

.method public final a(LX/1Pn;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 2377012
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2377011
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2377009
    check-cast p1, LX/99i;

    .line 2377010
    iget-object v0, p1, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2377008
    const/4 v0, 0x0

    return v0
.end method
