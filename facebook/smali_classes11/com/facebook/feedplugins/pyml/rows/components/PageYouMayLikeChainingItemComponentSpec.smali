.class public Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static i:LX/0Xm;


# instance fields
.field public final b:LX/1vg;

.field public final c:LX/1nu;

.field public final d:LX/359;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/359",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nA;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Q;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/GeG;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2376529
    const-class v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/1vg;LX/359;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p7    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nu;",
            "LX/1vg;",
            "LX/359;",
            "LX/0Ot",
            "<",
            "LX/1nA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17Q;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/GeG;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2376530
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2376531
    iput-object p1, p0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;->c:LX/1nu;

    .line 2376532
    iput-object p2, p0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;->b:LX/1vg;

    .line 2376533
    iput-object p3, p0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;->d:LX/359;

    .line 2376534
    iput-object p4, p0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;->e:LX/0Ot;

    .line 2376535
    iput-object p5, p0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;->f:LX/0Ot;

    .line 2376536
    iput-object p6, p0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;->g:LX/0Ot;

    .line 2376537
    iput-object p7, p0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;->h:LX/0Ot;

    .line 2376538
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;
    .locals 11

    .prologue
    .line 2376539
    const-class v1, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;

    monitor-enter v1

    .line 2376540
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2376541
    sput-object v2, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2376542
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2376543
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2376544
    new-instance v3, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v4

    check-cast v4, LX/1nu;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v5

    check-cast v5, LX/1vg;

    invoke-static {v0}, LX/359;->a(LX/0QB;)LX/359;

    move-result-object v6

    check-cast v6, LX/359;

    const/16 v7, 0x6b9

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x5d3

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x20f7

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x1430

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;-><init>(LX/1nu;LX/1vg;LX/359;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2376545
    move-object v0, v3

    .line 2376546
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2376547
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2376548
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2376549
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;Lcom/facebook/feed/rows/core/props/FeedProps;LX/3mj;LX/1Pr;)LX/1Dg;
    .locals 8
    .param p2    # Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/3mj;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # LX/1Pr;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/3mj;",
            "TE;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2376550
    if-nez p2, :cond_0

    .line 2376551
    const/4 v0, 0x0

    .line 2376552
    :goto_0
    return-object v0

    .line 2376553
    :cond_0
    new-instance v1, LX/GfH;

    invoke-direct {v1, p2}, LX/GfH;-><init>(Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;)V

    .line 2376554
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2376555
    check-cast v0, LX/0jW;

    invoke-interface {p5, v1, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GfI;

    .line 2376556
    iget-boolean v1, v0, LX/GfI;->b:Z

    move v1, v1

    .line 2376557
    if-eqz v1, :cond_1

    .line 2376558
    iget-object v3, p0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;->h:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v4, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec$1;

    invoke-direct {v4, p0, p4}, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec$1;-><init>(Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;LX/3mj;)V

    const-wide/16 v5, 0x1f4

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v4, v5, v6, v7}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 2376559
    iput-boolean v2, v0, LX/GfI;->b:Z

    .line 2376560
    :cond_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v0

    const v1, 0x7f020a3d

    invoke-interface {v0, v1}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v0

    const v1, 0x7f0b0942

    invoke-interface {v0, v1}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x6

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v0

    .line 2376561
    invoke-static {p2}, LX/Gdi;->c(LX/25E;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 2376562
    iget-object v2, p0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;->c:LX/1nu;

    invoke-virtual {v2, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v2

    if-nez v1, :cond_3

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v2, v1}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v1

    sget-object v2, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v1

    const v2, 0x7f0213f3

    invoke-virtual {v1, v2}, LX/1nw;->h(I)LX/1nw;

    move-result-object v1

    sget-object v2, LX/1Up;->a:LX/1Up;

    invoke-virtual {v1, v2}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    move-object v1, v1

    .line 2376563
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    .line 2376564
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v5, 0x1

    .line 2376565
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-static {p2}, LX/Gdi;->d(LX/25E;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b0050

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f0a0427

    invoke-virtual {v3, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/1ne;->t(I)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-static {p2}, LX/Gdi;->b(LX/25E;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b004b

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f0a015d

    invoke-virtual {v3, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-static {p1, p2}, LX/Gdi;->a(Landroid/content/Context;LX/25E;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b004b

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f0a043b

    invoke-virtual {v3, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/1ne;->t(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x6

    const v4, 0x7f0b0917

    invoke-interface {v2, v3, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v2, v3}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v2

    move-object v2, v2

    .line 2376566
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    .line 2376567
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v2

    const v3, 0x7f0a015a

    invoke-virtual {v2, v3}, LX/25Q;->i(I)LX/25Q;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v3, 0x7f0b0033

    invoke-interface {v2, v3}, LX/1Di;->i(I)LX/1Di;

    move-result-object v2

    const/4 v3, 0x4

    invoke-interface {v2, v3}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    move-object v2, v2

    .line 2376568
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    .line 2376569
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v3

    .line 2376570
    new-instance v4, LX/GfH;

    invoke-direct {v4, p2}, LX/GfH;-><init>(Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;)V

    .line 2376571
    iget-object v2, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2376572
    check-cast v2, LX/0jW;

    invoke-interface {p5, v4, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/GfI;

    .line 2376573
    iget-object v4, v2, LX/GfI;->a:Ljava/lang/Boolean;

    move-object v4, v4

    .line 2376574
    if-nez v4, :cond_2

    .line 2376575
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 2376576
    iput-object v3, v2, LX/GfI;->a:Ljava/lang/Boolean;

    .line 2376577
    :cond_2
    iget-object v3, p0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;->b:LX/1vg;

    invoke-virtual {v3, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v3

    const v4, 0x7f020abc

    invoke-virtual {v3, v4}, LX/2xv;->h(I)LX/2xv;

    move-result-object v3

    .line 2376578
    iget-object v4, v2, LX/GfI;->a:Ljava/lang/Boolean;

    move-object v2, v4

    .line 2376579
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_4

    const v2, 0x7f0a00d2

    :goto_2
    invoke-virtual {v3, v2}, LX/2xv;->j(I)LX/2xv;

    move-result-object v2

    invoke-virtual {v2}, LX/1n6;->b()LX/1dc;

    move-result-object v2

    .line 2376580
    iget-object v3, p0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;->d:LX/359;

    invoke-virtual {v3, p1}, LX/359;->c(LX/1De;)LX/35B;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/35B;->a(LX/1dc;)LX/35B;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/35B;->b(Ljava/lang/String;)LX/35B;

    move-result-object v3

    .line 2376581
    iget-object v2, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2376582
    check-cast v2, LX/0jW;

    invoke-virtual {v3, v2}, LX/35B;->a(LX/0jW;)LX/35B;

    move-result-object v2

    invoke-virtual {v2, p5}, LX/35B;->a(LX/1Pr;)LX/35B;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 v3, 0x6

    const v4, 0x7f0b0964

    invoke-interface {v2, v3, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x7

    const v4, 0x7f0b0965

    invoke-interface {v2, v3, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    .line 2376583
    const v3, 0x1552f40b

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2376584
    invoke-interface {v2, v3}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    move-object v2, v2

    .line 2376585
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x7

    const v3, 0x7f0b0966

    invoke-interface {v1, v2, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    move-object v1, v1

    .line 2376586
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    .line 2376587
    const v1, 0x5259f580

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v1

    move-object v1, v1

    .line 2376588
    invoke-interface {v0, v1}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    goto/16 :goto_1

    .line 2376589
    :cond_4
    const v2, 0x7f0a00e7

    goto :goto_2
.end method
