.class public Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final i:Lcom/facebook/common/callercontext/CallerContext;

.field private static j:LX/0Xm;


# instance fields
.field private final b:LX/GeH;

.field public final c:LX/1vg;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/GeG;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/1nu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1nu",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final g:I

.field private final h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2376383
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;->a:Landroid/util/SparseArray;

    .line 2376384
    const-class v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;

    const-string v1, "native_newsfeed"

    const-string v2, "cover_photo"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;->i:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/GeH;LX/1vg;LX/0Ot;LX/1nu;LX/0Ot;)V
    .locals 1
    .param p5    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/GeH;",
            "LX/1vg;",
            "LX/0Ot",
            "<",
            "LX/GeG;",
            ">;",
            "LX/1nu;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2376385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2376386
    const/4 v0, 0x6

    iput v0, p0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;->g:I

    .line 2376387
    const/16 v0, 0x54

    iput v0, p0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;->h:I

    .line 2376388
    iput-object p1, p0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;->b:LX/GeH;

    .line 2376389
    iput-object p2, p0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;->c:LX/1vg;

    .line 2376390
    iput-object p3, p0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;->d:LX/0Ot;

    .line 2376391
    iput-object p4, p0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;->f:LX/1nu;

    .line 2376392
    iput-object p5, p0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;->e:LX/0Ot;

    .line 2376393
    return-void
.end method

.method public static a(Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;LX/1De;LX/25E;LX/1f9;LX/1Pp;)LX/1Di;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/25E;",
            "LX/1f9;",
            "TE;)",
            "LX/1Di;"
        }
    .end annotation

    .prologue
    const/high16 v1, 0x3f000000    # 0.5f

    .line 2376394
    const/high16 v0, 0x42a80000    # 84.0f

    invoke-static {p1, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    invoke-interface {p2, v0}, LX/25E;->a(I)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 2376395
    if-eqz v0, :cond_0

    .line 2376396
    :goto_0
    invoke-interface {p2}, LX/25E;->n()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v3

    .line 2376397
    iget-object v2, p0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;->f:LX/1nu;

    invoke-virtual {v2, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v2

    invoke-virtual {v2, p4}, LX/1nw;->a(LX/1Pp;)LX/1nw;

    move-result-object v2

    invoke-virtual {v2, p3}, LX/1nw;->a(LX/1f9;)LX/1nw;

    move-result-object v2

    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v0

    sget-object v2, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;->i:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LX/1nw;->a(Z)LX/1nw;

    move-result-object v0

    const v2, 0x402d70a4    # 2.71f

    invoke-virtual {v0, v2}, LX/1nw;->c(F)LX/1nw;

    move-result-object v4

    new-instance v5, Landroid/graphics/PointF;

    if-nez v3, :cond_1

    move v2, v1

    :goto_1
    if-nez v3, :cond_2

    move v0, v1

    :goto_2
    invoke-direct {v5, v2, v0}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v4, v5}, LX/1nw;->a(Landroid/graphics/PointF;)LX/1nw;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/16 v1, 0x54

    invoke-interface {v0, v1}, LX/1Di;->j(I)LX/1Di;

    move-result-object v0

    const/16 v1, 0x8

    const/4 v2, 0x6

    invoke-interface {v0, v1, v2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v0

    return-object v0

    .line 2376398
    :cond_0
    invoke-interface {p2}, LX/25E;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0

    .line 2376399
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVect2;->a()D

    move-result-wide v6

    double-to-float v0, v6

    move v2, v0

    goto :goto_1

    :cond_2
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVect2;->b()D

    move-result-wide v0

    double-to-float v0, v0

    goto :goto_2
.end method

.method public static a(Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;LX/1De;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;LX/25E;Z)LX/1Di;
    .locals 5

    .prologue
    .line 2376400
    iget-object v0, p0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;->b:LX/GeH;

    invoke-virtual {v0, p2, p3}, LX/GeH;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;LX/25E;)Landroid/text/Spannable;

    move-result-object v0

    .line 2376401
    invoke-static {p3}, LX/Gdi;->b(LX/25E;)Ljava/lang/String;

    move-result-object v2

    .line 2376402
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2376403
    if-eqz v0, :cond_0

    .line 2376404
    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2376405
    new-instance v3, Landroid/text/style/TextAppearanceSpan;

    const v4, 0x7f0e05e1

    invoke-direct {v3, p1, v4}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    const/4 v4, 0x0

    invoke-interface {v0}, Landroid/text/Spannable;->length()I

    move-result p0

    const/16 p2, 0x11

    invoke-virtual {v1, v3, v4, p0, p2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2376406
    :cond_0
    if-eqz v2, :cond_2

    .line 2376407
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 2376408
    const/16 v3, 0xa

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 2376409
    :cond_1
    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2376410
    :cond_2
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_5

    :goto_0
    move-object v0, v1

    .line 2376411
    sget-object v1, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;->a:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-nez v1, :cond_3

    .line 2376412
    sget-object v1, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;->a:Landroid/util/SparseArray;

    const v2, 0x7f0d0081

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2376413
    :cond_3
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0b0050

    invoke-virtual {v1, v2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v1

    const v2, 0x7f0b1081

    invoke-virtual {v1, v2}, LX/1ne;->s(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    if-eqz p4, :cond_4

    sget-object v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;->a:Landroid/util/SparseArray;

    :goto_1
    invoke-interface {v1, v0}, LX/1Di;->a(Landroid/util/SparseArray;)LX/1Di;

    move-result-object v0

    return-object v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;
    .locals 9

    .prologue
    .line 2376414
    const-class v1, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;

    monitor-enter v1

    .line 2376415
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2376416
    sput-object v2, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2376417
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2376418
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2376419
    new-instance v3, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;

    invoke-static {v0}, LX/GeH;->b(LX/0QB;)LX/GeH;

    move-result-object v4

    check-cast v4, LX/GeH;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v5

    check-cast v5, LX/1vg;

    const/16 v6, 0x20f7

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v7

    check-cast v7, LX/1nu;

    const/16 v8, 0x1430

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;-><init>(LX/GeH;LX/1vg;LX/0Ot;LX/1nu;LX/0Ot;)V

    .line 2376420
    move-object v0, v3

    .line 2376421
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2376422
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2376423
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2376424
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
