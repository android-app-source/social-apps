.class public Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeBodyHeaderComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/GeH;

.field public final c:LX/CE0;

.field public final d:LX/CE9;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2376232
    const-class v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeBodyHeaderComponentSpec;

    const-string v1, "native_newsfeed"

    const-string v2, "actor_photo"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeBodyHeaderComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/GeH;LX/CE0;LX/CE9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2376233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2376234
    iput-object p1, p0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeBodyHeaderComponentSpec;->b:LX/GeH;

    .line 2376235
    iput-object p2, p0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeBodyHeaderComponentSpec;->c:LX/CE0;

    .line 2376236
    iput-object p3, p0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeBodyHeaderComponentSpec;->d:LX/CE9;

    .line 2376237
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeBodyHeaderComponentSpec;
    .locals 6

    .prologue
    .line 2376238
    const-class v1, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeBodyHeaderComponentSpec;

    monitor-enter v1

    .line 2376239
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeBodyHeaderComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2376240
    sput-object v2, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeBodyHeaderComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2376241
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2376242
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2376243
    new-instance p0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeBodyHeaderComponentSpec;

    invoke-static {v0}, LX/GeH;->b(LX/0QB;)LX/GeH;

    move-result-object v3

    check-cast v3, LX/GeH;

    invoke-static {v0}, LX/CE0;->a(LX/0QB;)LX/CE0;

    move-result-object v4

    check-cast v4, LX/CE0;

    invoke-static {v0}, LX/CE9;->a(LX/0QB;)LX/CE9;

    move-result-object v5

    check-cast v5, LX/CE9;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeBodyHeaderComponentSpec;-><init>(LX/GeH;LX/CE0;LX/CE9;)V

    .line 2376244
    move-object v0, p0

    .line 2376245
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2376246
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeBodyHeaderComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2376247
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2376248
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
