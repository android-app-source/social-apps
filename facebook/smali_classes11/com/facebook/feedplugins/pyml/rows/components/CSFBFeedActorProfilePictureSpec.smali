.class public Lcom/facebook/feedplugins/pyml/rows/components/CSFBFeedActorProfilePictureSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/CE9;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2375523
    const-class v0, Lcom/facebook/feedplugins/pyml/rows/components/CSFBFeedActorProfilePictureSpec;

    const-string v1, "native_newsfeed"

    const-string v2, "actor_photo"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/pyml/rows/components/CSFBFeedActorProfilePictureSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/CE9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2375524
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2375525
    iput-object p1, p0, Lcom/facebook/feedplugins/pyml/rows/components/CSFBFeedActorProfilePictureSpec;->b:LX/CE9;

    .line 2375526
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pyml/rows/components/CSFBFeedActorProfilePictureSpec;
    .locals 4

    .prologue
    .line 2375527
    const-class v1, Lcom/facebook/feedplugins/pyml/rows/components/CSFBFeedActorProfilePictureSpec;

    monitor-enter v1

    .line 2375528
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pyml/rows/components/CSFBFeedActorProfilePictureSpec;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2375529
    sput-object v2, Lcom/facebook/feedplugins/pyml/rows/components/CSFBFeedActorProfilePictureSpec;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2375530
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2375531
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2375532
    new-instance p0, Lcom/facebook/feedplugins/pyml/rows/components/CSFBFeedActorProfilePictureSpec;

    invoke-static {v0}, LX/CE9;->a(LX/0QB;)LX/CE9;

    move-result-object v3

    check-cast v3, LX/CE9;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/pyml/rows/components/CSFBFeedActorProfilePictureSpec;-><init>(LX/CE9;)V

    .line 2375533
    move-object v0, p0

    .line 2375534
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2375535
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/components/CSFBFeedActorProfilePictureSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2375536
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2375537
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
