.class public Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final d:LX/1DR;

.field private final e:LX/GfP;

.field private final f:LX/1V0;

.field private final g:LX/0pf;

.field private h:LX/1Ua;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1DR;LX/GfP;LX/1V0;LX/0pf;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2377419
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2377420
    iput-object p2, p0, Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;->d:LX/1DR;

    .line 2377421
    iput-object p3, p0, Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;->e:LX/GfP;

    .line 2377422
    iput-object p4, p0, Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;->f:LX/1V0;

    .line 2377423
    invoke-static {}, LX/1UY;->c()LX/1UY;

    move-result-object v0

    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;->h:LX/1Ua;

    .line 2377424
    iput-object p5, p0, Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;->g:LX/0pf;

    .line 2377425
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2377387
    new-instance v1, LX/1X6;

    iget-object v0, p0, Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;->h:LX/1Ua;

    invoke-direct {v1, p2, v0}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 2377388
    iget-object v2, p0, Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;->g:LX/0pf;

    .line 2377389
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2377390
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v2, v0}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v0

    .line 2377391
    iget-boolean v2, v0, LX/1g0;->o:Z

    move v0, v2

    .line 2377392
    iget-object v2, p0, Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;->d:LX/1DR;

    const/high16 v3, 0x41000000    # 8.0f

    invoke-virtual {v2, p1, v3}, LX/1DR;->a(Landroid/content/Context;F)I

    move-result v2

    .line 2377393
    iget-object v3, p0, Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;->e:LX/GfP;

    const/4 v4, 0x0

    .line 2377394
    new-instance v5, LX/GfO;

    invoke-direct {v5, v3}, LX/GfO;-><init>(LX/GfP;)V

    .line 2377395
    iget-object v6, v3, LX/GfP;->b:LX/0Zi;

    invoke-virtual {v6}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/GfN;

    .line 2377396
    if-nez v6, :cond_0

    .line 2377397
    new-instance v6, LX/GfN;

    invoke-direct {v6, v3}, LX/GfN;-><init>(LX/GfP;)V

    .line 2377398
    :cond_0
    invoke-static {v6, p1, v4, v4, v5}, LX/GfN;->a$redex0(LX/GfN;LX/1De;IILX/GfO;)V

    .line 2377399
    move-object v5, v6

    .line 2377400
    move-object v4, v5

    .line 2377401
    move-object v3, v4

    .line 2377402
    iget-object v4, v3, LX/GfN;->a:LX/GfO;

    iput-object p2, v4, LX/GfO;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2377403
    iget-object v4, v3, LX/GfN;->e:Ljava/util/BitSet;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 2377404
    move-object v3, v3

    .line 2377405
    iget-object v4, v3, LX/GfN;->a:LX/GfO;

    iput v2, v4, LX/GfO;->c:I

    .line 2377406
    iget-object v4, v3, LX/GfN;->e:Ljava/util/BitSet;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 2377407
    move-object v2, v3

    .line 2377408
    iget-object v3, v2, LX/GfN;->a:LX/GfO;

    iput-object p3, v3, LX/GfO;->a:LX/1Pn;

    .line 2377409
    iget-object v3, v2, LX/GfN;->e:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2377410
    move-object v2, v2

    .line 2377411
    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 2377412
    :goto_0
    iget-object v3, v2, LX/GfN;->a:LX/GfO;

    iput-boolean v0, v3, LX/GfO;->d:Z

    .line 2377413
    iget-object v3, v2, LX/GfN;->e:Ljava/util/BitSet;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2377414
    move-object v0, v2

    .line 2377415
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2377416
    iget-object v2, p0, Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    .line 2377417
    return-object v0

    .line 2377418
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;
    .locals 9

    .prologue
    .line 2377376
    const-class v1, Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;

    monitor-enter v1

    .line 2377377
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2377378
    sput-object v2, Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2377379
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2377380
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2377381
    new-instance v3, Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v5

    check-cast v5, LX/1DR;

    invoke-static {v0}, LX/GfP;->a(LX/0QB;)LX/GfP;

    move-result-object v6

    check-cast v6, LX/GfP;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v7

    check-cast v7, LX/1V0;

    invoke-static {v0}, LX/0pf;->a(LX/0QB;)LX/0pf;

    move-result-object v8

    check-cast v8, LX/0pf;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;-><init>(Landroid/content/Context;LX/1DR;LX/GfP;LX/1V0;LX/0pf;)V

    .line 2377382
    move-object v0, v3

    .line 2377383
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2377384
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2377385
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2377386
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2377426
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2377375
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pyml/rows/components/small/PagesYouMayLikeSmallFormatComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 2377374
    check-cast p1, LX/1Pn;

    invoke-virtual {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1Pn;)Z

    move-result v0

    return v0
.end method

.method public final a(LX/1Pn;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 2377373
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2377372
    const/4 v0, 0x1

    return v0
.end method
