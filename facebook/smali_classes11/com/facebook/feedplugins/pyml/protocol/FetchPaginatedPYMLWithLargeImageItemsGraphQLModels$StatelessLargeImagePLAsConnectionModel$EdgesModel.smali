.class public final Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xc954245
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2375237
    const-class v0, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2375236
    const-class v0, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2375234
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2375235
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2375228
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2375229
    invoke-virtual {p0}, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$EdgesModel;->a()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2375230
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2375231
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2375232
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2375233
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2375220
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2375221
    invoke-virtual {p0}, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$EdgesModel;->a()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2375222
    invoke-virtual {p0}, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$EdgesModel;->a()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    .line 2375223
    invoke-virtual {p0}, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$EdgesModel;->a()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2375224
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$EdgesModel;

    .line 2375225
    iput-object v0, v1, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$EdgesModel;->e:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    .line 2375226
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2375227
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2375218
    iget-object v0, p0, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$EdgesModel;->e:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    iput-object v0, p0, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$EdgesModel;->e:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    .line 2375219
    iget-object v0, p0, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$EdgesModel;->e:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2375215
    new-instance v0, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$EdgesModel;-><init>()V

    .line 2375216
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2375217
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2375213
    const v0, -0x18010147

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2375214
    const v0, -0x1df403cb

    return v0
.end method
