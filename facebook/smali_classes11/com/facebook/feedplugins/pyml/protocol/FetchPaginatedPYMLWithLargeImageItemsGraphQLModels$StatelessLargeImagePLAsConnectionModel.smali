.class public final Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x99ef49d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2375268
    const-class v0, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2375267
    const-class v0, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2375245
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2375246
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2375259
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2375260
    invoke-virtual {p0}, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2375261
    invoke-virtual {p0}, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel;->b()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x7926f1db

    invoke-static {v2, v1, v3}, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2375262
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2375263
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2375264
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2375265
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2375266
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2375257
    iget-object v0, p0, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel;->e:Ljava/util/List;

    .line 2375258
    iget-object v0, p0, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2375269
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2375270
    invoke-virtual {p0}, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2375271
    invoke-virtual {p0}, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2375272
    if-eqz v1, :cond_2

    .line 2375273
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel;

    .line 2375274
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 2375275
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel;->b()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2375276
    invoke-virtual {p0}, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x7926f1db

    invoke-static {v2, v0, v3}, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2375277
    invoke-virtual {p0}, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel;->b()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2375278
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel;

    .line 2375279
    iput v3, v0, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel;->f:I

    move-object v1, v0

    .line 2375280
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2375281
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    .line 2375282
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move-object p0, v1

    .line 2375283
    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2375254
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2375255
    const/4 v0, 0x1

    const v1, -0x7926f1db

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel;->f:I

    .line 2375256
    return-void
.end method

.method public final b()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2375252
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2375253
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2375249
    new-instance v0, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel;-><init>()V

    .line 2375250
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2375251
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2375248
    const v0, -0x5974b7a5

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2375247
    const v0, 0x5f6a5356

    return v0
.end method
