.class public Lcom/facebook/location/ui/LocationSettingsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->LOCATION_SETTINGS_FRAGMENT:LX/0cQ;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:Landroid/widget/TextView;

.field private B:Landroid/view/View;

.field public C:Landroid/view/ViewGroup;

.field private D:Lcom/facebook/location/ui/LocationSettingsOffView;

.field public E:Landroid/view/ViewGroup;

.field private F:Landroid/view/View;

.field private G:Landroid/view/View;

.field private H:Landroid/view/View;

.field private I:Landroid/view/View;

.field public J:Z

.field public K:LX/03R;

.field public L:LX/03R;

.field public M:LX/1rv;

.field public N:Z

.field private final O:Landroid/view/View$OnClickListener;

.field private final P:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private c:LX/0SG;

.field private d:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/Gyn;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0tX;

.field private f:LX/0aG;

.field private g:LX/0y3;

.field private h:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public i:LX/0kL;

.field public j:Lcom/facebook/content/SecureContextHelper;

.field private k:Z

.field public l:Z

.field private m:Z

.field private n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/GyR;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/GyW;

.field public p:LX/Gyq;

.field public q:LX/17Y;

.field private r:LX/0i4;

.field public s:LX/0i5;

.field public t:LX/6Zb;

.field private u:LX/6Ze;

.field public v:Landroid/view/ViewGroup;

.field public w:Landroid/view/ViewGroup;

.field private x:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field public y:Lcom/facebook/widget/BetterSwitch;

.field private z:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2410557
    const-class v0, Lcom/facebook/location/ui/LocationSettingsFragment;

    sput-object v0, Lcom/facebook/location/ui/LocationSettingsFragment;->a:Ljava/lang/Class;

    .line 2410558
    const-class v0, Lcom/facebook/location/ui/LocationSettingsFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/location/ui/LocationSettingsFragment;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2410552
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2410553
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->K:LX/03R;

    .line 2410554
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->L:LX/03R;

    .line 2410555
    new-instance v0, LX/Gyd;

    invoke-direct {v0, p0}, LX/Gyd;-><init>(Lcom/facebook/location/ui/LocationSettingsFragment;)V

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->O:Landroid/view/View$OnClickListener;

    .line 2410556
    new-instance v0, LX/Gye;

    invoke-direct {v0, p0}, LX/Gye;-><init>(Lcom/facebook/location/ui/LocationSettingsFragment;)V

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->P:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    return-void
.end method

.method private static a(Lcom/facebook/location/ui/LocationSettingsFragment;LX/0SG;LX/1Ck;LX/0tX;LX/0aG;LX/0y3;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0kL;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/0Or;LX/0Or;LX/GyU;LX/Gyt;LX/GyS;LX/GyW;LX/Gyq;LX/17Y;LX/0i4;LX/6Zb;LX/6Ze;)V
    .locals 3
    .param p8    # Lcom/facebook/content/SecureContextHelper;
        .annotation runtime Lcom/facebook/location/ui/IsLocationHistoryFeatureAvailable;
        .end annotation
    .end param
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/placetips/settings/IsPlaceTipsSettingsEnabled;
        .end annotation
    .end param
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/location/ui/IsNearbyFriendsFeatureAvailable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/1Ck;",
            "LX/0tX;",
            "LX/0aG;",
            "LX/0y3;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0kL;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/GyU;",
            "LX/Gyt;",
            "LX/GyS;",
            "LX/GyW;",
            "LX/Gyq;",
            "LX/17Y;",
            "LX/0i4;",
            "LX/6Zb;",
            "LX/6Ze;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2410533
    iput-object p2, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->d:LX/1Ck;

    .line 2410534
    iput-object p3, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->e:LX/0tX;

    .line 2410535
    iput-object p4, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->f:LX/0aG;

    .line 2410536
    iput-object p5, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->g:LX/0y3;

    .line 2410537
    iput-object p6, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2410538
    iput-object p7, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->i:LX/0kL;

    .line 2410539
    iput-object p8, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->j:Lcom/facebook/content/SecureContextHelper;

    .line 2410540
    invoke-interface {p9}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03R;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/03R;->asBoolean(Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->k:Z

    .line 2410541
    invoke-interface {p10}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->l:Z

    .line 2410542
    invoke-interface {p11}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03R;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/03R;->asBoolean(Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->m:Z

    .line 2410543
    invoke-static/range {p12 .. p14}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->n:LX/0Px;

    .line 2410544
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->o:LX/GyW;

    .line 2410545
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->p:LX/Gyq;

    .line 2410546
    iput-object p1, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->c:LX/0SG;

    .line 2410547
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->q:LX/17Y;

    .line 2410548
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->r:LX/0i4;

    .line 2410549
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->t:LX/6Zb;

    .line 2410550
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->u:LX/6Ze;

    .line 2410551
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 23

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v22

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/location/ui/LocationSettingsFragment;

    invoke-static/range {v22 .. v22}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static/range {v22 .. v22}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static/range {v22 .. v22}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static/range {v22 .. v22}, LX/0aF;->getInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v6

    check-cast v6, LX/0aG;

    invoke-static/range {v22 .. v22}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v7

    check-cast v7, LX/0y3;

    invoke-static/range {v22 .. v22}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v22 .. v22}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v9

    check-cast v9, LX/0kL;

    invoke-static/range {v22 .. v22}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v10

    check-cast v10, Lcom/facebook/content/SecureContextHelper;

    const/16 v11, 0x332

    move-object/from16 v0, v22

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const/16 v12, 0x1558

    move-object/from16 v0, v22

    invoke-static {v0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    const/16 v13, 0x333

    move-object/from16 v0, v22

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static/range {v22 .. v22}, LX/GyU;->a(LX/0QB;)LX/GyU;

    move-result-object v14

    check-cast v14, LX/GyU;

    invoke-static/range {v22 .. v22}, LX/Gyt;->a(LX/0QB;)LX/Gyt;

    move-result-object v15

    check-cast v15, LX/Gyt;

    invoke-static/range {v22 .. v22}, LX/GyS;->a(LX/0QB;)LX/GyS;

    move-result-object v16

    check-cast v16, LX/GyS;

    invoke-static/range {v22 .. v22}, LX/GyW;->a(LX/0QB;)LX/GyW;

    move-result-object v17

    check-cast v17, LX/GyW;

    invoke-static/range {v22 .. v22}, LX/Gyq;->a(LX/0QB;)LX/Gyq;

    move-result-object v18

    check-cast v18, LX/Gyq;

    invoke-static/range {v22 .. v22}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v19

    check-cast v19, LX/17Y;

    const-class v20, LX/0i4;

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v20

    check-cast v20, LX/0i4;

    invoke-static/range {v22 .. v22}, LX/6Zb;->a(LX/0QB;)LX/6Zb;

    move-result-object v21

    check-cast v21, LX/6Zb;

    invoke-static/range {v22 .. v22}, LX/6Ze;->a(LX/0QB;)LX/6Ze;

    move-result-object v22

    check-cast v22, LX/6Ze;

    invoke-static/range {v2 .. v22}, Lcom/facebook/location/ui/LocationSettingsFragment;->a(Lcom/facebook/location/ui/LocationSettingsFragment;LX/0SG;LX/1Ck;LX/0tX;LX/0aG;LX/0y3;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0kL;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/0Or;LX/0Or;LX/GyU;LX/Gyt;LX/GyS;LX/GyW;LX/Gyq;LX/17Y;LX/0i4;LX/6Zb;LX/6Ze;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/location/ui/LocationSettingsFragment;LX/Gym;)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2410526
    iget-object v3, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->v:Landroid/view/ViewGroup;

    sget-object v0, LX/Gym;->CONTENT:LX/Gym;

    if-ne p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2410527
    iget-object v3, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->F:Landroid/view/View;

    sget-object v0, LX/Gym;->LOADING:LX/Gym;

    if-ne p1, v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2410528
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->G:Landroid/view/View;

    sget-object v3, LX/Gym;->ERROR:LX/Gym;

    if-ne p1, v3, :cond_2

    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2410529
    return-void

    :cond_0
    move v0, v2

    .line 2410530
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2410531
    goto :goto_1

    :cond_2
    move v1, v2

    .line 2410532
    goto :goto_2
.end method

.method public static a$redex0(Lcom/facebook/location/ui/LocationSettingsFragment;Z)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    .line 2410520
    const v0, 0x7f080024

    const/4 v1, 0x0

    invoke-static {v0, v2, v1, v2}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v6

    .line 2410521
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "save_setting_progress"

    invoke-virtual {v6, v0, v1}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2410522
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2410523
    const-string v0, "BackgroundLocationUpdateSettingsParams"

    const-string v1, "{\"value\":\"SELF\"}"

    invoke-static {p1, v1}, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->a(ZLjava/lang/String;)Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2410524
    iget-object v7, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->d:LX/1Ck;

    sget-object v8, LX/Gyn;->SAVE_HISTORY_SETTING:LX/Gyn;

    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->f:LX/0aG;

    const-string v1, "background_location_update_settings"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/location/ui/LocationSettingsFragment;->b:Lcom/facebook/common/callercontext/CallerContext;

    const v5, 0x4fc15a8d

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    new-instance v1, LX/Gyk;

    invoke-direct {v1, p0, p1, v6}, LX/Gyk;-><init>(Lcom/facebook/location/ui/LocationSettingsFragment;ZLandroid/support/v4/app/DialogFragment;)V

    invoke-virtual {v7, v8, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2410525
    return-void
.end method

.method public static a$redex0(Lcom/facebook/location/ui/LocationSettingsFragment;ZZ)V
    .locals 2

    .prologue
    .line 2410510
    iget-object v1, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->z:Landroid/widget/TextView;

    if-eqz p2, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2410511
    if-eqz p2, :cond_1

    .line 2410512
    :goto_1
    return-void

    .line 2410513
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2410514
    :cond_1
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->M:LX/1rv;

    iget-object v0, v0, LX/1rv;->a:LX/0yG;

    invoke-static {v0}, LX/1rv;->b(LX/0yG;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2410515
    if-eqz p1, :cond_3

    .line 2410516
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->M:LX/1rv;

    iget-object v0, v0, LX/1rv;->a:LX/0yG;

    sget-object v1, LX/0yG;->LOCATION_DISABLED:LX/0yG;

    if-ne v0, v1, :cond_2

    const v0, 0x7f0837ac

    .line 2410517
    :goto_2
    iget-object v1, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->z:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 2410518
    :cond_2
    const v0, 0x7f0837b2

    goto :goto_2

    .line 2410519
    :cond_3
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->M:LX/1rv;

    iget-object v0, v0, LX/1rv;->a:LX/0yG;

    sget-object v1, LX/0yG;->LOCATION_DISABLED:LX/0yG;

    if-ne v0, v1, :cond_4

    const v0, 0x7f0837ad

    goto :goto_2

    :cond_4
    const v0, 0x7f0837b3

    goto :goto_2
.end method

.method public static b(Lcom/facebook/location/ui/LocationSettingsFragment;Z)V
    .locals 2

    .prologue
    .line 2410374
    iget-object v1, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->x:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    if-eqz p1, :cond_0

    const v0, 0x7f020e61

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailResource(I)V

    .line 2410375
    return-void

    .line 2410376
    :cond_0
    const v0, 0x7f020e60

    goto :goto_0
.end method

.method public static c$redex0(Lcom/facebook/location/ui/LocationSettingsFragment;Z)V
    .locals 2

    .prologue
    .line 2410506
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->y:Lcom/facebook/widget/BetterSwitch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/BetterSwitch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2410507
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->y:Lcom/facebook/widget/BetterSwitch;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/BetterSwitch;->setChecked(Z)V

    .line 2410508
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->y:Lcom/facebook/widget/BetterSwitch;

    iget-object v1, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->P:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/BetterSwitch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2410509
    return-void
.end method

.method public static d(Lcom/facebook/location/ui/LocationSettingsFragment;)V
    .locals 4

    .prologue
    .line 2410500
    iget-boolean v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->k:Z

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2410501
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->L:LX/03R;

    .line 2410502
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->d:LX/1Ck;

    sget-object v1, LX/Gyn;->LOAD_HISTORY_SETTING:LX/Gyn;

    iget-object v2, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->e:LX/0tX;

    .line 2410503
    new-instance v3, LX/GUw;

    invoke-direct {v3}, LX/GUw;-><init>()V

    move-object v3, v3

    .line 2410504
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    new-instance v3, LX/Gyj;

    invoke-direct {v3, p0}, LX/Gyj;-><init>(Lcom/facebook/location/ui/LocationSettingsFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2410505
    return-void
.end method

.method public static e$redex0(Lcom/facebook/location/ui/LocationSettingsFragment;)V
    .locals 8

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 2410478
    iget-boolean v2, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->m:Z

    .line 2410479
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 2410480
    iget-boolean v5, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->l:Z

    if-nez v5, :cond_8

    .line 2410481
    :cond_0
    :goto_0
    move v4, v0

    .line 2410482
    iget-boolean v5, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->k:Z

    .line 2410483
    if-nez v2, :cond_1

    if-nez v4, :cond_1

    if-eqz v5, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 2410484
    :goto_1
    iget-object v6, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->H:Landroid/view/View;

    if-eqz v2, :cond_3

    move v2, v1

    :goto_2
    invoke-virtual {v6, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2410485
    iget-object v6, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->I:Landroid/view/View;

    if-eqz v4, :cond_4

    move v2, v1

    :goto_3
    invoke-virtual {v6, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2410486
    iget-object v4, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->x:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    if-eqz v5, :cond_5

    move v2, v1

    :goto_4
    invoke-virtual {v4, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setVisibility(I)V

    .line 2410487
    iget-object v4, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->B:Landroid/view/View;

    if-eqz v5, :cond_6

    move v2, v1

    :goto_5
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2410488
    iget-object v2, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->w:Landroid/view/ViewGroup;

    if-eqz v0, :cond_7

    :goto_6
    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2410489
    return-void

    :cond_2
    move v0, v1

    .line 2410490
    goto :goto_1

    :cond_3
    move v2, v3

    .line 2410491
    goto :goto_2

    :cond_4
    move v2, v3

    .line 2410492
    goto :goto_3

    :cond_5
    move v2, v3

    .line 2410493
    goto :goto_4

    :cond_6
    move v2, v3

    .line 2410494
    goto :goto_5

    :cond_7
    move v1, v3

    .line 2410495
    goto :goto_6

    .line 2410496
    :cond_8
    iget-object v5, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v5, v5

    .line 2410497
    if-nez v5, :cond_9

    move v0, v4

    .line 2410498
    goto :goto_0

    .line 2410499
    :cond_9
    sget-object v6, LX/0ax;->eh:Ljava/lang/String;

    const-string v7, "extra_from_uri"

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    move v0, v4

    goto :goto_0
.end method

.method public static k(Lcom/facebook/location/ui/LocationSettingsFragment;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2410559
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->E:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2410560
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->n:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    move v1, v2

    :goto_0
    if-ge v3, v4, :cond_0

    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->n:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GyR;

    .line 2410561
    invoke-interface {v0}, LX/GyR;->a()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2410562
    new-instance v1, LX/GyV;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v1, v5}, LX/GyV;-><init>(Landroid/content/Context;)V

    .line 2410563
    iget-object v5, v1, LX/GyV;->a:Landroid/widget/TextView;

    invoke-interface {v0}, LX/GyR;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2410564
    iget-object v5, v1, LX/GyV;->b:Landroid/widget/TextView;

    invoke-interface {v0}, LX/GyR;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2410565
    new-instance v5, LX/Gyl;

    invoke-direct {v5, p0, v0}, LX/Gyl;-><init>(Lcom/facebook/location/ui/LocationSettingsFragment;LX/GyR;)V

    invoke-virtual {v1, v5}, LX/GyV;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2410566
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->E:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2410567
    const/4 v0, 0x1

    .line 2410568
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    .line 2410569
    :cond_0
    iget-object v3, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->C:Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    move v0, v2

    :goto_2
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2410570
    return-void

    .line 2410571
    :cond_1
    const/16 v0, 0x8

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public static m(Lcom/facebook/location/ui/LocationSettingsFragment;)Z
    .locals 2

    .prologue
    .line 2410373
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->M:LX/1rv;

    iget-object v0, v0, LX/1rv;->a:LX/0yG;

    sget-object v1, LX/0yG;->OKAY:LX/0yG;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static n$redex0(Lcom/facebook/location/ui/LocationSettingsFragment;)V
    .locals 3

    .prologue
    .line 2410377
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->M:LX/1rv;

    iget-object v0, v0, LX/1rv;->a:LX/0yG;

    invoke-static {v0}, LX/1rv;->b(LX/0yG;)Z

    move-result v1

    .line 2410378
    iget-object v2, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->D:Lcom/facebook/location/ui/LocationSettingsOffView;

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/location/ui/LocationSettingsOffView;->setVisibility(I)V

    .line 2410379
    if-nez v1, :cond_1

    .line 2410380
    :goto_1
    return-void

    .line 2410381
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 2410382
    :cond_1
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->M:LX/1rv;

    iget-object v0, v0, LX/1rv;->a:LX/0yG;

    sget-object v1, LX/0yG;->LOCATION_DISABLED:LX/0yG;

    if-ne v0, v1, :cond_2

    const v0, 0x7f0837ae

    .line 2410383
    :goto_2
    iget-object v1, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->M:LX/1rv;

    iget-object v1, v1, LX/1rv;->a:LX/0yG;

    sget-object v2, LX/0yG;->LOCATION_DISABLED:LX/0yG;

    if-ne v1, v2, :cond_3

    const v1, 0x7f0837b0

    .line 2410384
    :goto_3
    iget-object v2, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->D:Lcom/facebook/location/ui/LocationSettingsOffView;

    invoke-virtual {v2, v0}, Lcom/facebook/location/ui/LocationSettingsOffView;->setDescriptionText(I)V

    .line 2410385
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->D:Lcom/facebook/location/ui/LocationSettingsOffView;

    invoke-virtual {v0, v1}, Lcom/facebook/location/ui/LocationSettingsOffView;->setButtonText(I)V

    goto :goto_1

    .line 2410386
    :cond_2
    const v0, 0x7f0837af

    goto :goto_2

    .line 2410387
    :cond_3
    const v1, 0x7f0837b1

    goto :goto_3
.end method

.method public static s$redex0(Lcom/facebook/location/ui/LocationSettingsFragment;)V
    .locals 3

    .prologue
    .line 2410388
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0837ab

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080016

    new-instance v2, LX/Gyb;

    invoke-direct {v2, p0}, LX/Gyb;-><init>(Lcom/facebook/location/ui/LocationSettingsFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2410389
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2410390
    const-string v0, "location_settings"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2410391
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2410392
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/location/ui/LocationSettingsFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2410393
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->r:LX/0i4;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->s:LX/0i5;

    .line 2410394
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->t:LX/6Zb;

    iget-object v1, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->u:LX/6Ze;

    invoke-virtual {v0, p0, v1}, LX/6Zb;->a(Lcom/facebook/base/fragment/FbFragment;LX/6ZZ;)V

    .line 2410395
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->N:Z

    .line 2410396
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->p:LX/Gyq;

    sget-object v1, LX/Gyp;->OVERALL_TTI:LX/Gyp;

    invoke-virtual {v0, v1}, LX/Gyq;->a(LX/Gyp;)V

    .line 2410397
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->p:LX/Gyq;

    sget-object v1, LX/Gyp;->INIT:LX/Gyp;

    invoke-virtual {v0, v1}, LX/Gyq;->a(LX/Gyp;)V

    .line 2410398
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xf2ba32d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2410399
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2410400
    iget-object v1, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->D:Lcom/facebook/location/ui/LocationSettingsOffView;

    iget-object v2, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->O:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/facebook/location/ui/LocationSettingsOffView;->setButtonListener(Landroid/view/View$OnClickListener;)V

    .line 2410401
    iget-object v1, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->A:Landroid/widget/TextView;

    new-instance v2, LX/Gyf;

    invoke-direct {v2, p0}, LX/Gyf;-><init>(Lcom/facebook/location/ui/LocationSettingsFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2410402
    iget-object v1, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->G:Landroid/view/View;

    new-instance v2, LX/Gyg;

    invoke-direct {v2, p0}, LX/Gyg;-><init>(Lcom/facebook/location/ui/LocationSettingsFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2410403
    iget-object v1, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->H:Landroid/view/View;

    new-instance v2, LX/Gyh;

    invoke-direct {v2, p0}, LX/Gyh;-><init>(Lcom/facebook/location/ui/LocationSettingsFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2410404
    iget-object v1, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->I:Landroid/view/View;

    new-instance v2, LX/Gyi;

    invoke-direct {v2, p0}, LX/Gyi;-><init>(Lcom/facebook/location/ui/LocationSettingsFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2410405
    iget-boolean v1, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->N:Z

    if-nez v1, :cond_0

    .line 2410406
    iget-object v1, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->p:LX/Gyq;

    sget-object v2, LX/Gyp;->INIT:LX/Gyp;

    invoke-virtual {v1, v2}, LX/Gyq;->b(LX/Gyp;)V

    .line 2410407
    iget-object v1, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->p:LX/Gyq;

    sget-object v2, LX/Gyp;->FETCH_DATA:LX/Gyp;

    invoke-virtual {v1, v2}, LX/Gyq;->a(LX/Gyp;)V

    .line 2410408
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x301dd0ac

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x74c3d899

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2410409
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 2410410
    const v1, 0x7f030a4c

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x204e76b7

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xf9487f6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2410411
    iget-object v1, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->t:LX/6Zb;

    invoke-virtual {v1}, LX/6Zb;->a()V

    .line 2410412
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2410413
    const/16 v1, 0x2b

    const v2, -0x7f57b4e1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 10

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5d86d55c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2410414
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2410415
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->N:Z

    .line 2410416
    iget-object v1, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->p:LX/Gyq;

    .line 2410417
    invoke-static {}, LX/Gyp;->values()[LX/Gyp;

    move-result-object v4

    array-length v5, v4

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    .line 2410418
    iget-object v7, v1, LX/Gyq;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    iget v8, v6, LX/Gyp;->markerId:I

    iget-object v9, v6, LX/Gyp;->markerName:Ljava/lang/String;

    invoke-interface {v7, v8, v9}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    .line 2410419
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2410420
    :cond_0
    iget-object v1, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->d:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2410421
    const/16 v1, 0x2b

    const v2, -0x3743c2e1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x2

    const/16 v3, 0x2a

    const v4, -0x4f536130

    invoke-static {v0, v3, v4}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2410422
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2410423
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2410424
    if-nez v0, :cond_7

    .line 2410425
    :goto_0
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->M:LX/1rv;

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/facebook/location/ui/LocationSettingsFragment;->m(Lcom/facebook/location/ui/LocationSettingsFragment;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    move v0, v2

    .line 2410426
    :goto_1
    iget-object v4, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->g:LX/0y3;

    invoke-virtual {v4}, LX/0y3;->b()LX/1rv;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->M:LX/1rv;

    .line 2410427
    invoke-static {p0}, Lcom/facebook/location/ui/LocationSettingsFragment;->m(Lcom/facebook/location/ui/LocationSettingsFragment;)Z

    move-result v4

    .line 2410428
    iget-object v5, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->K:LX/03R;

    sget-object v6, LX/03R;->YES:LX/03R;

    if-ne v5, v6, :cond_1

    if-eqz v0, :cond_1

    if-nez v4, :cond_1

    .line 2410429
    invoke-static {p0}, Lcom/facebook/location/ui/LocationSettingsFragment;->s$redex0(Lcom/facebook/location/ui/LocationSettingsFragment;)V

    .line 2410430
    :cond_1
    const/4 v5, 0x0

    .line 2410431
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->v:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2410432
    invoke-static {p0}, Lcom/facebook/location/ui/LocationSettingsFragment;->m(Lcom/facebook/location/ui/LocationSettingsFragment;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2410433
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->v:Landroid/view/ViewGroup;

    iget-object v6, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->w:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2410434
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->v:Landroid/view/ViewGroup;

    iget-object v6, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->C:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2410435
    invoke-static {p0}, Lcom/facebook/location/ui/LocationSettingsFragment;->n$redex0(Lcom/facebook/location/ui/LocationSettingsFragment;)V

    .line 2410436
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->E:Landroid/view/ViewGroup;

    .line 2410437
    :goto_2
    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2410438
    iget-object v5, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->y:Lcom/facebook/widget/BetterSwitch;

    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->K:LX/03R;

    sget-object v6, LX/03R;->YES:LX/03R;

    if-eq v0, v6, :cond_2

    if-eqz v4, :cond_5

    :cond_2
    move v0, v2

    :goto_3
    invoke-virtual {v5, v0}, Lcom/facebook/widget/BetterSwitch;->setEnabled(Z)V

    .line 2410439
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->K:LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_6

    if-eqz v4, :cond_6

    :goto_4
    invoke-static {p0, v2}, Lcom/facebook/location/ui/LocationSettingsFragment;->b(Lcom/facebook/location/ui/LocationSettingsFragment;Z)V

    .line 2410440
    sget-object v0, LX/Gym;->CONTENT:LX/Gym;

    .line 2410441
    iget-boolean v1, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->k:Z

    if-eqz v1, :cond_3

    .line 2410442
    sget-object v0, LX/Gym;->LOADING:LX/Gym;

    .line 2410443
    invoke-static {p0}, Lcom/facebook/location/ui/LocationSettingsFragment;->d(Lcom/facebook/location/ui/LocationSettingsFragment;)V

    .line 2410444
    :cond_3
    invoke-static {p0}, Lcom/facebook/location/ui/LocationSettingsFragment;->k(Lcom/facebook/location/ui/LocationSettingsFragment;)V

    .line 2410445
    invoke-static {p0, v0}, Lcom/facebook/location/ui/LocationSettingsFragment;->a$redex0(Lcom/facebook/location/ui/LocationSettingsFragment;LX/Gym;)V

    .line 2410446
    const v0, -0xad76f4d

    invoke-static {v0, v3}, LX/02F;->f(II)V

    return-void

    :cond_4
    move v0, v1

    .line 2410447
    goto :goto_1

    :cond_5
    move v0, v1

    .line 2410448
    goto :goto_3

    :cond_6
    move v2, v1

    .line 2410449
    goto :goto_4

    .line 2410450
    :cond_7
    const v4, 0x7f0837a2

    invoke-interface {v0, v4}, LX/1ZF;->x_(I)V

    goto/16 :goto_0

    .line 2410451
    :cond_8
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->v:Landroid/view/ViewGroup;

    iget-object v6, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->C:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2410452
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->v:Landroid/view/ViewGroup;

    iget-object v6, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->w:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2410453
    invoke-static {p0}, Lcom/facebook/location/ui/LocationSettingsFragment;->n$redex0(Lcom/facebook/location/ui/LocationSettingsFragment;)V

    .line 2410454
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->M:LX/1rv;

    iget-object v0, v0, LX/1rv;->a:LX/0yG;

    invoke-static {v0}, LX/1rv;->b(LX/0yG;)Z

    move-result v0

    if-nez v0, :cond_9

    const/4 v0, 0x1

    .line 2410455
    :goto_5
    iget-object v6, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->E:Landroid/view/ViewGroup;

    if-eqz v0, :cond_a

    move-object v0, v6

    goto :goto_2

    :cond_9
    move v0, v5

    .line 2410456
    goto :goto_5

    .line 2410457
    :cond_a
    const/16 v5, 0x8

    move-object v0, v6

    goto :goto_2
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2410458
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2410459
    const v0, 0x7f0d19f9

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->v:Landroid/view/ViewGroup;

    .line 2410460
    const v0, 0x7f0d19fa

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->w:Landroid/view/ViewGroup;

    .line 2410461
    const v0, 0x7f0d19fb

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/header/SectionHeaderView;

    .line 2410462
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0837a3

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5}, LX/5MF;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2410463
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/header/SectionHeaderView;->setTitleText(Ljava/lang/String;)V

    .line 2410464
    const v0, 0x7f0d1a00

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->x:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2410465
    const v0, 0x7f0d1a01

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/BetterSwitch;

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->y:Lcom/facebook/widget/BetterSwitch;

    .line 2410466
    const v0, 0x7f0d1a03

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->z:Landroid/widget/TextView;

    .line 2410467
    const v0, 0x7f0d1a05

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->A:Landroid/widget/TextView;

    .line 2410468
    const v0, 0x7f0d1a02

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->B:Landroid/view/View;

    .line 2410469
    const v0, 0x7f0d1a06

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->C:Landroid/view/ViewGroup;

    .line 2410470
    const v0, 0x7f0d1a08

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/ui/LocationSettingsOffView;

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->D:Lcom/facebook/location/ui/LocationSettingsOffView;

    .line 2410471
    const v0, 0x7f0d1a09

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->E:Landroid/view/ViewGroup;

    .line 2410472
    const v0, 0x7f0d1a0a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->F:Landroid/view/View;

    .line 2410473
    const v0, 0x7f0d1a0b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->G:Landroid/view/View;

    .line 2410474
    const v0, 0x7f0d19fc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->H:Landroid/view/View;

    .line 2410475
    const v0, 0x7f0d19fe

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsFragment;->I:Landroid/view/View;

    .line 2410476
    invoke-static {p0}, Lcom/facebook/location/ui/LocationSettingsFragment;->e$redex0(Lcom/facebook/location/ui/LocationSettingsFragment;)V

    .line 2410477
    return-void
.end method
