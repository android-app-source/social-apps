.class public Lcom/facebook/location/ui/LocationSettingsOffView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2410580
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2410581
    invoke-direct {p0}, Lcom/facebook/location/ui/LocationSettingsOffView;->a()V

    .line 2410582
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2410583
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2410584
    invoke-direct {p0}, Lcom/facebook/location/ui/LocationSettingsOffView;->a()V

    .line 2410585
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2410586
    const v0, 0x7f030a4d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2410587
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/location/ui/LocationSettingsOffView;->setOrientation(I)V

    .line 2410588
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/location/ui/LocationSettingsOffView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0048

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/facebook/location/ui/LocationSettingsOffView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2410589
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/location/ui/LocationSettingsOffView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0116

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2410590
    invoke-virtual {p0}, Lcom/facebook/location/ui/LocationSettingsOffView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0036

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 2410591
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 2410592
    const v0, 0x7f0d1a0c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsOffView;->a:Landroid/widget/TextView;

    .line 2410593
    const v0, 0x7f0d1a0d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/location/ui/LocationSettingsOffView;->b:Landroid/widget/Button;

    .line 2410594
    return-void
.end method


# virtual methods
.method public setButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2410595
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsOffView;->b:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2410596
    return-void
.end method

.method public setButtonText(I)V
    .locals 1

    .prologue
    .line 2410597
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsOffView;->b:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 2410598
    return-void
.end method

.method public setButtonText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2410599
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsOffView;->b:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2410600
    return-void
.end method

.method public setDescriptionText(I)V
    .locals 1

    .prologue
    .line 2410601
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsOffView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 2410602
    return-void
.end method

.method public setDescriptionText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2410603
    iget-object v0, p0, Lcom/facebook/location/ui/LocationSettingsOffView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2410604
    return-void
.end method
