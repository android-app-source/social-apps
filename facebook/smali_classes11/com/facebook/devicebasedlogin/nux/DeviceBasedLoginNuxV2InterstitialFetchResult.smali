.class public Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResultDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final targetedNux:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "targeted_nux"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2369174
    const-class v0, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResultDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2369175
    const-class v0, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResultSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2369176
    new-instance v0, LX/Gb9;

    invoke-direct {v0}, LX/Gb9;-><init>()V

    sput-object v0, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2369177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2369178
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResult;->targetedNux:Ljava/lang/String;

    .line 2369179
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2369180
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2369181
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResult;->targetedNux:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2369182
    return-void
.end method
