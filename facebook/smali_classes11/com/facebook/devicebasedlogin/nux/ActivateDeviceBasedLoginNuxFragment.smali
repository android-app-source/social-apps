.class public Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements LX/0l6;
.implements LX/Gb4;
.implements LX/2JL;


# static fields
.field private static final d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/GbG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/10M;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/10N;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Gb3;

.field public g:Landroid/view/View;

.field public h:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

.field public i:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

.field private j:Lcom/facebook/resources/ui/FbTextView;

.field private k:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2369023
    const-class v0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;

    sput-object v0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->d:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2369022
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static a(ZLX/GbF;)Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;
    .locals 2

    .prologue
    .line 2369016
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2369017
    const-string v1, "arg_show_passcode_cta"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2369018
    const-string v1, "arg_nux_type"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2369019
    new-instance v1, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;

    invoke-direct {v1}, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;-><init>()V

    .line 2369020
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2369021
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2369013
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2369014
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;

    invoke-static {p1}, LX/10N;->b(LX/0QB;)LX/10N;

    move-result-object v2

    check-cast v2, LX/10N;

    const/16 v3, 0x12cb

    invoke-static {p1, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    invoke-static {p1}, LX/GbG;->b(LX/0QB;)LX/GbG;

    move-result-object v3

    check-cast v3, LX/GbG;

    invoke-static {p1}, LX/10M;->b(LX/0QB;)LX/10M;

    move-result-object p1

    check-cast p1, LX/10M;

    iput-object v2, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->e:LX/10N;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->a:LX/0Or;

    iput-object v3, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->b:LX/GbG;

    iput-object p1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->c:LX/10M;

    .line 2369015
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2369012
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 2369011
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 2368892
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x2b3080e3

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2369000
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 2369001
    const v2, 0x7f0d03a7

    if-ne v1, v2, :cond_1

    .line 2369002
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->f:LX/Gb3;

    if-eqz v1, :cond_0

    .line 2369003
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->f:LX/Gb3;

    invoke-interface {v1}, LX/Gb3;->a()V

    .line 2369004
    :cond_0
    :goto_0
    const v1, 0x594dce43

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2369005
    :cond_1
    const v2, 0x7f0d03a6

    if-ne v1, v2, :cond_2

    .line 2369006
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->f:LX/Gb3;

    if-eqz v1, :cond_0

    .line 2369007
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->f:LX/Gb3;

    invoke-interface {v1}, LX/Gb3;->b()V

    goto :goto_0

    .line 2369008
    :cond_2
    const v2, 0x7f0d039b

    if-ne v1, v2, :cond_0

    .line 2369009
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->f:LX/Gb3;

    if-eqz v1, :cond_0

    .line 2369010
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->f:LX/Gb3;

    invoke-interface {v1}, LX/Gb3;->lR_()V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x42a6f5fa

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2368998
    const v1, 0x7f03002e

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->g:Landroid/view/View;

    .line 2368999
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->g:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, -0x57b9be4d

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onResume()V
    .locals 14

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x287e10ae

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2368990
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2368991
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 2368992
    iget-object v4, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->g:Landroid/view/View;

    const v7, 0x7f0d039c

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    .line 2368993
    new-instance v4, Landroid/view/animation/TranslateAnimation;

    const/high16 v10, 0x3f800000    # 1.0f

    move v7, v5

    move v8, v6

    move v9, v5

    move v11, v5

    move v12, v6

    invoke-direct/range {v4 .. v12}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 2368994
    const-wide/16 v6, 0x1f4

    invoke-virtual {v4, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2368995
    invoke-virtual {v4, v5}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 2368996
    invoke-virtual {v13, v4}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2368997
    const/16 v1, 0x2b

    const v2, -0x7509249c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6c27cf86

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2368988
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2368989
    const/16 v1, 0x2b

    const v2, -0x6c84a23c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v5, 0x0

    const/16 v9, 0x8

    const/4 v4, 0x1

    .line 2368893
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2368894
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->e:LX/10N;

    invoke-virtual {v0}, LX/10N;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->g:Landroid/view/View;

    const v1, 0x7f0d03a2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2368895
    :goto_0
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 2368896
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v6, v0

    .line 2368897
    const-string v0, "arg_nux_type"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/GbF;

    .line 2368898
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->g:Landroid/view/View;

    const v2, 0x7f0d03a8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    iput-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->h:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    .line 2368899
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->h:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->setImage(Ljava/lang/String;)V

    .line 2368900
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->g:Landroid/view/View;

    const v2, 0x7f0d03a9

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2368901
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->e:LX/10N;

    invoke-virtual {v2}, LX/10N;->i()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    .line 2368902
    iget-object v3, v2, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v2, v3

    .line 2368903
    invoke-virtual {v2}, Lcom/facebook/user/model/Name;->f()Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2368904
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->g:Landroid/view/View;

    const v2, 0x7f0d03ab

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2368905
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->g:Landroid/view/View;

    const v3, 0x7f0d03aa

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    iput-object v2, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->i:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    .line 2368906
    sget-object v2, LX/GbF;->SHARED_DEVICE:LX/GbF;

    if-ne v0, v2, :cond_b

    const/4 v2, 0x1

    :goto_2
    move v2, v2

    .line 2368907
    if-eqz v2, :cond_a

    .line 2368908
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->c:LX/10M;

    invoke-virtual {v2}, LX/10M;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 2368909
    iget-object v8, v2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->a:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/user/model/User;

    .line 2368910
    iget-object p1, v3, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, p1

    .line 2368911
    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2368912
    iget-object v3, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->i:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    iget-object v7, v2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mPicUrl:Ljava/lang/String;

    invoke-virtual {v3, v7}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->setImage(Ljava/lang/String;)V

    .line 2368913
    iget-object v3, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->e:LX/10N;

    invoke-virtual {v3}, LX/10N;->i()Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, v2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mFullName:Ljava/lang/String;

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2368914
    iget-object v2, v2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mFullName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    move v2, v4

    .line 2368915
    :goto_4
    if-nez v2, :cond_1

    .line 2368916
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->i:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    invoke-virtual {v2, v9}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->setVisibility(I)V

    .line 2368917
    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2368918
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->e:LX/10N;

    invoke-virtual {v1}, LX/10N;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2368919
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->g:Landroid/view/View;

    const v2, 0x7f0d03ac

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    .line 2368920
    :cond_1
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->g:Landroid/view/View;

    const v2, 0x7f0d039b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iput-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->j:Lcom/facebook/resources/ui/FbTextView;

    .line 2368921
    const-string v1, "arg_show_passcode_cta"

    invoke-virtual {v6, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->e:LX/10N;

    invoke-virtual {v1}, LX/10N;->i()Z

    move-result v1

    if-nez v1, :cond_8

    move v1, v4

    :goto_5
    iput-boolean v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->k:Z

    .line 2368922
    iget-boolean v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->k:Z

    if-eqz v1, :cond_9

    .line 2368923
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, p0}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2368924
    :goto_6
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->g:Landroid/view/View;

    const v2, 0x7f0d02c4

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 2368925
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->g:Landroid/view/View;

    const v3, 0x7f0d02c3

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    .line 2368926
    iget-object v3, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->e:LX/10N;

    invoke-virtual {v3}, LX/10N;->c()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->e:LX/10N;

    invoke-virtual {v3}, LX/10N;->j()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    sget-object v3, LX/GbF;->DEFAULT:LX/GbF;

    if-ne v0, v3, :cond_3

    .line 2368927
    sget-object v0, LX/GbF;->AUTOSAVE:LX/GbF;

    .line 2368928
    :cond_3
    iget-object v3, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->b:LX/GbG;

    .line 2368929
    iget-object v4, v3, LX/GbG;->b:LX/10N;

    invoke-virtual {v4}, LX/10N;->o()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 2368930
    iget-object v4, v3, LX/GbG;->a:Landroid/content/Context;

    const v5, 0x7f08345e

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2368931
    :goto_7
    move-object v3, v4

    .line 2368932
    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2368933
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->b:LX/GbG;

    .line 2368934
    iget-object v3, v1, LX/GbG;->b:LX/10N;

    invoke-virtual {v3}, LX/10N;->o()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 2368935
    iget-object v3, v1, LX/GbG;->a:Landroid/content/Context;

    const v4, 0x7f083460

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2368936
    :goto_8
    move-object v0, v3

    .line 2368937
    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2368938
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->g:Landroid/view/View;

    const v1, 0x7f0d03a7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 2368939
    invoke-virtual {v0, p0}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2368940
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->b:LX/GbG;

    .line 2368941
    iget-object v2, v1, LX/GbG;->b:LX/10N;

    invoke-virtual {v2}, LX/10N;->o()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 2368942
    iget-object v2, v1, LX/GbG;->a:Landroid/content/Context;

    const v3, 0x7f083466

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2368943
    :goto_9
    move-object v1, v2

    .line 2368944
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2368945
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->g:Landroid/view/View;

    const v1, 0x7f0d03a6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 2368946
    invoke-virtual {v0, p0}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2368947
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->g:Landroid/view/View;

    const v1, 0x7f0d039c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 2368948
    const/16 v5, 0x8

    const v4, 0x3f333333    # 0.7f

    .line 2368949
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 2368950
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 2368951
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v1

    .line 2368952
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v2

    .line 2368953
    const/16 v0, 0x12c

    if-gt v1, v0, :cond_4

    .line 2368954
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->g:Landroid/view/View;

    const v3, 0x7f0d039d

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/nux/canvas/MobileChromeView;

    .line 2368955
    invoke-virtual {v0, v4}, Lcom/facebook/fbui/nux/canvas/MobileChromeView;->setScaleX(F)V

    .line 2368956
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->g:Landroid/view/View;

    const v3, 0x7f0d03a9

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2368957
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->g:Landroid/view/View;

    const v3, 0x7f0d03ab

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2368958
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->h:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    invoke-virtual {v0, v4}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->setScaleX(F)V

    .line 2368959
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->h:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    invoke-virtual {v0, v4}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->setScaleY(F)V

    .line 2368960
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->i:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    invoke-virtual {v0, v4}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->setScaleX(F)V

    .line 2368961
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->i:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    invoke-virtual {v0, v4}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->setScaleY(F)V

    .line 2368962
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->g:Landroid/view/View;

    const v3, 0x7f0d039f

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setScaleX(F)V

    .line 2368963
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->g:Landroid/view/View;

    const v3, 0x7f0d039f

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setScaleY(F)V

    .line 2368964
    :cond_4
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->g:Landroid/view/View;

    const v3, 0x7f0d039c

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 2368965
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-direct {v3, v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 2368966
    div-int/lit8 v4, v1, 0xc

    div-int/lit8 v2, v2, 0x14

    div-int/lit8 v1, v1, 0xc

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v2, v1, v5}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2368967
    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2368968
    return-void

    .line 2368969
    :cond_5
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->g:Landroid/view/View;

    const v1, 0x7f0d03a0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    goto/16 :goto_0

    .line 2368970
    :cond_6
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    .line 2368971
    iget-object v3, v2, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v2, v3

    .line 2368972
    invoke-virtual {v2}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 2368973
    :cond_7
    iget-object v2, v2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_8
    move v1, v5

    .line 2368974
    goto/16 :goto_5

    .line 2368975
    :cond_9
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v10}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_6

    :cond_a
    move v2, v5

    goto/16 :goto_4

    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 2368976
    :cond_c
    sget-object v4, LX/GbE;->a:[I

    invoke-virtual {v0}, LX/GbF;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2368977
    iget-object v4, v3, LX/GbG;->a:Landroid/content/Context;

    const v5, 0x7f08345f

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_7

    .line 2368978
    :pswitch_0
    iget-object v4, v3, LX/GbG;->a:Landroid/content/Context;

    const v5, 0x7f083462

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_7

    .line 2368979
    :pswitch_1
    iget-object v4, v3, LX/GbG;->a:Landroid/content/Context;

    const v5, 0x7f0834a9

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_7

    .line 2368980
    :pswitch_2
    iget-object v4, v3, LX/GbG;->a:Landroid/content/Context;

    const v5, 0x7f0834a7

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_7

    .line 2368981
    :pswitch_3
    iget-object v4, v3, LX/GbG;->a:Landroid/content/Context;

    const v5, 0x7f0834ab

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_7

    .line 2368982
    :cond_d
    sget-object v3, LX/GbE;->a:[I

    invoke-virtual {v0}, LX/GbF;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    .line 2368983
    iget-object v3, v1, LX/GbG;->a:Landroid/content/Context;

    const v4, 0x7f083461

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_8

    .line 2368984
    :pswitch_4
    iget-object v3, v1, LX/GbG;->a:Landroid/content/Context;

    const v4, 0x7f083463

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_8

    .line 2368985
    :pswitch_5
    iget-object v3, v1, LX/GbG;->a:Landroid/content/Context;

    const v4, 0x7f0834aa

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_8

    .line 2368986
    :pswitch_6
    iget-object v3, v1, LX/GbG;->a:Landroid/content/Context;

    const v4, 0x7f0834a8

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_8

    .line 2368987
    :pswitch_7
    iget-object v3, v1, LX/GbG;->a:Landroid/content/Context;

    const v4, 0x7f0834aa

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_8

    :cond_e
    iget-object v2, v1, LX/GbG;->a:Landroid/content/Context;

    const v3, 0x7f083465

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_9

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
