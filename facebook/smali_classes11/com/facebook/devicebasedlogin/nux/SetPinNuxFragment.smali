.class public Lcom/facebook/devicebasedlogin/nux/SetPinNuxFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/Gb4;
.implements LX/GbD;


# instance fields
.field public a:LX/Gb3;

.field public b:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

.field private final c:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2369265
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2369266
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/nux/SetPinNuxFragment;->c:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2369244
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/SetPinNuxFragment;->b:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    invoke-virtual {v0}, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->b()V

    .line 2369245
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2369246
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/SetPinNuxFragment;->a:LX/Gb3;

    if-eqz v0, :cond_0

    .line 2369247
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2369248
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/SetPinNuxFragment;->a:LX/Gb3;

    invoke-interface {v0, p1}, LX/Gb3;->d_(Ljava/lang/String;)V

    .line 2369249
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 2369250
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 2369251
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/16 v0, 0x2a

    const v1, -0x3c1aba8f

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2369252
    const v0, 0x7f03040d

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2369253
    const v0, 0x7f0d02d5

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/nux/SetPinNuxFragment;->b:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    .line 2369254
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/SetPinNuxFragment;->b:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    .line 2369255
    iput-object p0, v0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->f:LX/GbD;

    .line 2369256
    const v0, 0x7f0d02c4

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2369257
    const v3, 0x7f08346c

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 2369258
    const v0, 0x7f0d0c62

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2369259
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2369260
    const/16 v0, 0x2b

    const v3, -0x262e9871

    invoke-static {v5, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onResume()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x2a

    const v1, -0x762e1e1f

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2369261
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2369262
    new-instance v1, Lcom/facebook/devicebasedlogin/nux/SetPinNuxFragment$1;

    invoke-direct {v1, p0}, Lcom/facebook/devicebasedlogin/nux/SetPinNuxFragment$1;-><init>(Lcom/facebook/devicebasedlogin/nux/SetPinNuxFragment;)V

    .line 2369263
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/nux/SetPinNuxFragment;->c:Landroid/os/Handler;

    const-wide/16 v4, 0x1f4

    const v3, -0x4d9b02cf

    invoke-static {v2, v1, v4, v5, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2369264
    const/16 v1, 0x2b

    const v2, 0x40e80a6e

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
