.class public Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResultSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2369108
    const-class v0, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;

    new-instance v1, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResultSerializer;

    invoke-direct {v1}, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResultSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2369109
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2369110
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2369111
    if-nez p0, :cond_0

    .line 2369112
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2369113
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2369114
    invoke-static {p0, p1, p2}, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResultSerializer;->b(Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;LX/0nX;LX/0my;)V

    .line 2369115
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2369116
    return-void
.end method

.method private static b(Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2369117
    const-string v0, "triggerGeneration"

    iget v1, p0, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;->triggerGeneration:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2369118
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2369119
    check-cast p1, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;

    invoke-static {p1, p2, p3}, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResultSerializer;->a(Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;LX/0nX;LX/0my;)V

    return-void
.end method
