.class public Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResultDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final triggerGeneration:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "triggerGeneration"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2369083
    const-class v0, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResultDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2369075
    const-class v0, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResultSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2369082
    new-instance v0, LX/Gb7;

    invoke-direct {v0}, LX/Gb7;-><init>()V

    sput-object v0, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2369084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2369085
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;->triggerGeneration:I

    .line 2369086
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2369079
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2369080
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;->triggerGeneration:I

    .line 2369081
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2369078
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2369076
    iget v0, p0, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;->triggerGeneration:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2369077
    return-void
.end method
