.class public Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResultSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResult;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2369204
    const-class v0, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResult;

    new-instance v1, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResultSerializer;

    invoke-direct {v1}, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResultSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2369205
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2369215
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResult;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2369209
    if-nez p0, :cond_0

    .line 2369210
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2369211
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2369212
    invoke-static {p0, p1, p2}, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResultSerializer;->b(Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResult;LX/0nX;LX/0my;)V

    .line 2369213
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2369214
    return-void
.end method

.method private static b(Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResult;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2369207
    const-string v0, "targeted_nux"

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResult;->targetedNux:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2369208
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2369206
    check-cast p1, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResult;

    invoke-static {p1, p2, p3}, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResultSerializer;->a(Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxV2InterstitialFetchResult;LX/0nX;LX/0my;)V

    return-void
.end method
