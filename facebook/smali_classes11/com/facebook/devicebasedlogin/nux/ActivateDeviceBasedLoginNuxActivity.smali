.class public Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0l6;
.implements LX/Gb3;
.implements LX/2JL;


# instance fields
.field private A:LX/Gb5;

.field private B:Ljava/lang/String;

.field public p:LX/10N;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:LX/10O;

.field private r:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private s:LX/10M;

.field private t:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

.field private u:LX/0ad;

.field private v:LX/0SG;

.field private w:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private x:I

.field private y:LX/GbG;

.field private z:LX/GbF;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2368891
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a(LX/0Or;LX/10O;LX/10M;LX/2J4;LX/0ad;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/GbG;LX/Gb5;)V
    .locals 2
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/10O;",
            "LX/10M;",
            "LX/2J4;",
            "LX/0ad;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0SG;",
            "LX/GbG;",
            "LX/Gb5;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2368881
    iput-object p1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->r:LX/0Or;

    .line 2368882
    iput-object p2, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->q:LX/10O;

    .line 2368883
    iput-object p3, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->s:LX/10M;

    .line 2368884
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->s:LX/10M;

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->q:LX/10O;

    invoke-virtual {p4, v0, v1}, LX/2J4;->a(LX/10M;LX/10O;)Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->t:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    .line 2368885
    iput-object p5, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->u:LX/0ad;

    .line 2368886
    iput-object p6, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->w:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2368887
    iput-object p7, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->v:LX/0SG;

    .line 2368888
    iput-object p8, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->y:LX/GbG;

    .line 2368889
    iput-object p9, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->A:LX/Gb5;

    .line 2368890
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 11

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v10

    move-object v0, p0

    check-cast v0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;

    const/16 v1, 0x12cb

    invoke-static {v10, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v10}, LX/10O;->b(LX/0QB;)LX/10O;

    move-result-object v2

    check-cast v2, LX/10O;

    invoke-static {v10}, LX/10M;->b(LX/0QB;)LX/10M;

    move-result-object v3

    check-cast v3, LX/10M;

    const-class v4, LX/2J4;

    invoke-interface {v10, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/2J4;

    invoke-static {v10}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v10}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v10}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static {v10}, LX/GbG;->b(LX/0QB;)LX/GbG;

    move-result-object v8

    check-cast v8, LX/GbG;

    invoke-static {v10}, LX/Gb5;->b(LX/0QB;)LX/Gb5;

    move-result-object v9

    check-cast v9, LX/Gb5;

    invoke-direct/range {v0 .. v9}, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->a(LX/0Or;LX/10O;LX/10M;LX/2J4;LX/0ad;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/GbG;LX/Gb5;)V

    invoke-static {v10}, LX/10N;->b(LX/0QB;)LX/10N;

    move-result-object v1

    check-cast v1, LX/10N;

    iput-object v1, v0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->p:LX/10N;

    return-void
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 2368879
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2368880
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method private l()V
    .locals 12

    .prologue
    .line 2368859
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->r:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2368860
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "targeted_nux"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2368861
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2368862
    if-nez v0, :cond_0

    .line 2368863
    :goto_0
    return-void

    .line 2368864
    :cond_0
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2368865
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->w:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v0, LX/26p;->l:LX/0Tn;

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget v3, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->x:I

    invoke-interface {v2, v0, v3}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2368866
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->w:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v0, LX/26p;->m:LX/0Tn;

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->v:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    invoke-interface {v2, v0, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0

    .line 2368867
    :cond_1
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->A:LX/Gb5;

    .line 2368868
    iget-object v6, v0, LX/Gb5;->b:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 2368869
    if-nez v6, :cond_3

    .line 2368870
    :cond_2
    :goto_1
    goto :goto_0

    .line 2368871
    :cond_3
    iget-object v7, v0, LX/Gb5;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v8

    sget-object v7, LX/26p;->n:LX/0Tn;

    invoke-virtual {v7, v6}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v7

    check-cast v7, LX/0Tn;

    invoke-interface {v8, v7, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v7

    invoke-interface {v7}, LX/0hN;->commit()V

    .line 2368872
    iget-object v7, v0, LX/Gb5;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v8

    sget-object v7, LX/26p;->m:LX/0Tn;

    invoke-virtual {v7, v6}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v7

    check-cast v7, LX/0Tn;

    iget-object v9, v0, LX/Gb5;->c:LX/0SG;

    invoke-interface {v9}, LX/0SG;->a()J

    move-result-wide v10

    invoke-interface {v8, v7, v10, v11}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v7

    invoke-interface {v7}, LX/0hN;->commit()V

    .line 2368873
    iget-object v8, v0, LX/Gb5;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/26p;->o:LX/0Tn;

    invoke-virtual {v7, v6}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v7

    check-cast v7, LX/0Tn;

    const/4 v9, 0x0

    invoke-interface {v8, v7, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v7

    .line 2368874
    if-nez v7, :cond_4

    .line 2368875
    const/4 v7, 0x1

    .line 2368876
    :goto_2
    const/16 v8, 0x40

    if-gt v7, v8, :cond_2

    .line 2368877
    iget-object v8, v0, LX/Gb5;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v8

    sget-object v9, LX/26p;->o:LX/0Tn;

    invoke-virtual {v9, v6}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v6

    check-cast v6, LX/0Tn;

    invoke-interface {v8, v6, v7}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v6

    invoke-interface {v6}, LX/0hN;->commit()V

    goto :goto_1

    .line 2368878
    :cond_4
    mul-int/lit8 v7, v7, 0x2

    goto :goto_2
.end method

.method private m()V
    .locals 2

    .prologue
    .line 2368855
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->setResult(I)V

    .line 2368856
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->finish()V

    .line 2368857
    const/4 v0, 0x0

    const v1, 0x7f040040

    invoke-virtual {p0, v0, v1}, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->overridePendingTransition(II)V

    .line 2368858
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2368845
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->t:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    const-string v1, ""

    iget-object v3, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->z:LX/GbF;

    invoke-virtual {v3}, LX/GbF;->getTypeString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    move-object v3, v2

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a(Ljava/lang/String;Lcom/facebook/auth/credentials/DBLFacebookCredentials;LX/0TF;Ljava/lang/String;Z)V

    .line 2368846
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->p:LX/10N;

    invoke-virtual {v0}, LX/10N;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->p:LX/10N;

    invoke-virtual {v0}, LX/10N;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2368847
    :cond_0
    const v0, 0x7f083469

    invoke-direct {p0, v0}, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->b(I)V

    .line 2368848
    :goto_0
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->m()V

    .line 2368849
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2368850
    const-string v1, "source"

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->z:LX/GbF;

    invoke-virtual {v2}, LX/GbF;->getTypeString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2368851
    const-string v1, "nux_version"

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->B:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2368852
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->q:LX/10O;

    const-string v2, "dbl_nux_dismiss_forward"

    invoke-virtual {v1, v2, v0}, LX/10O;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2368853
    return-void

    .line 2368854
    :cond_1
    const v0, 0x7f083468

    invoke-direct {p0, v0}, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->b(I)V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2368839
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2368840
    const-string v1, "source"

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->z:LX/GbF;

    invoke-virtual {v2}, LX/GbF;->getTypeString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2368841
    const-string v1, "nux_version"

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->B:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2368842
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->q:LX/10O;

    const-string v2, "dbl_nux_dismiss_backward"

    invoke-virtual {v1, v2, v0}, LX/10O;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2368843
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->m()V

    .line 2368844
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2368815
    invoke-static {p0, p0}, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2368816
    const v0, 0x7f0303e0

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->setContentView(I)V

    .line 2368817
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->r:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2368818
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->finish()V

    .line 2368819
    :goto_0
    return-void

    .line 2368820
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2368821
    const-string v1, "generation"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->x:I

    .line 2368822
    const-string v1, "targeted_nux"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2368823
    const-string v1, "v2"

    iput-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->B:Ljava/lang/String;

    .line 2368824
    :goto_1
    const-string v1, "targeted_nux"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2368825
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2368826
    sget-object v1, LX/GbF;->DEFAULT:LX/GbF;

    .line 2368827
    :goto_2
    move-object v0, v1

    .line 2368828
    iput-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->z:LX/GbF;

    .line 2368829
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->z:LX/GbF;

    invoke-static {v0, v1}, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->a(ZLX/GbF;)Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;

    move-result-object v0

    .line 2368830
    iput-object p0, v0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->f:LX/Gb3;

    .line 2368831
    instance-of v1, v0, LX/Gb4;

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2368832
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    .line 2368833
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    goto :goto_0

    .line 2368834
    :cond_1
    const-string v1, "v1"

    iput-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->B:Ljava/lang/String;

    goto :goto_1

    .line 2368835
    :cond_2
    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2368836
    const-string v1, "\\+"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 2368837
    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v1}, LX/GbF;->fromString(Ljava/lang/String;)LX/GbF;

    move-result-object v1

    goto :goto_2

    .line 2368838
    :cond_3
    sget-object v1, LX/GbF;->DEFAULT:LX/GbF;

    goto :goto_2
.end method

.method public final d_(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2368809
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->t:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->z:LX/GbF;

    invoke-virtual {v1}, LX/GbF;->getTypeString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    move-object v1, p1

    move-object v3, v2

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a(Ljava/lang/String;Lcom/facebook/auth/credentials/DBLFacebookCredentials;LX/0TF;Ljava/lang/String;Z)V

    .line 2368810
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->p:LX/10N;

    invoke-virtual {v0}, LX/10N;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->p:LX/10N;

    invoke-virtual {v0}, LX/10N;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2368811
    :cond_0
    const v0, 0x7f083469

    invoke-direct {p0, v0}, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->b(I)V

    .line 2368812
    :goto_0
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->m()V

    .line 2368813
    return-void

    .line 2368814
    :cond_1
    const v0, 0x7f083468

    invoke-direct {p0, v0}, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->b(I)V

    goto :goto_0
.end method

.method public final lR_()V
    .locals 6

    .prologue
    .line 2368795
    new-instance v0, Lcom/facebook/devicebasedlogin/nux/SetPinNuxFragment;

    invoke-direct {v0}, Lcom/facebook/devicebasedlogin/nux/SetPinNuxFragment;-><init>()V

    .line 2368796
    iput-object p0, v0, Lcom/facebook/devicebasedlogin/nux/SetPinNuxFragment;->a:LX/Gb3;

    .line 2368797
    instance-of v1, v0, LX/Gb4;

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2368798
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    .line 2368799
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0400d8

    const v3, 0x7f0400dc

    const v4, 0x7f0400e9

    const v5, 0x7f0400ec

    invoke-virtual {v1, v2, v3, v4, v5}, LX/0hH;->a(IIII)LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2368800
    return-void
.end method

.method public final onBackPressed()V
    .locals 3

    .prologue
    .line 2368804
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->f()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 2368805
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->q:LX/10O;

    const-string v1, "dbl_nux_dismiss_backward"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/10O;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2368806
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->m()V

    .line 2368807
    :goto_0
    return-void

    .line 2368808
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->d()V

    goto :goto_0
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x5fe74a4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2368801
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStart()V

    .line 2368802
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;->l()V

    .line 2368803
    const/16 v1, 0x23

    const v2, -0x5b347810

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
