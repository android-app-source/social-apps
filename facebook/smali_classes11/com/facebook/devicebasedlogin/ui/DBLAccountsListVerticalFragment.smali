.class public Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;
.super Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;
.source ""


# static fields
.field public static final s:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:Landroid/view/ViewGroup;

.field public B:Landroid/view/View;

.field public C:Landroid/view/ViewGroup;

.field private D:Landroid/view/ViewGroup;

.field public final E:Landroid/os/Handler;

.field private F:Z

.field public G:Z

.field public H:LX/GcD;

.field public I:Ljava/lang/String;

.field public l:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/10N;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/H5o;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/8D3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:Landroid/view/View;

.field public u:Landroid/view/View;

.field public v:Landroid/widget/ImageView;

.field private w:Lcom/facebook/resources/ui/FbTextView;

.field public x:Landroid/view/View;

.field public y:Landroid/widget/ScrollView;

.field private z:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2371475
    const-class v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->s:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2371476
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;-><init>()V

    .line 2371477
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->E:Landroid/os/Handler;

    .line 2371478
    iput-boolean v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->F:Z

    .line 2371479
    iput-boolean v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->G:Z

    .line 2371480
    sget-object v0, LX/GcD;->UNSET:LX/GcD;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->H:LX/GcD;

    return-void
.end method

.method public static D(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;)Z
    .locals 1

    .prologue
    .line 2371481
    :try_start_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2371482
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 2371483
    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;LX/3x3;Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V
    .locals 4

    .prologue
    .line 2371484
    invoke-static {p2}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->b(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)Ljava/lang/String;

    move-result-object v0

    .line 2371485
    iget-object v1, p1, LX/3x3;->b:LX/3v0;

    move-object v1, v1

    .line 2371486
    const v2, 0x7f0d3203

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 2371487
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->m:LX/10N;

    .line 2371488
    iget-object v3, v2, LX/10N;->c:LX/0Uh;

    const/16 p1, 0x25

    const/4 p2, 0x0

    invoke-virtual {v3, p1, p2}, LX/0Uh;->a(IZ)Z

    move-result v3

    move v2, v3

    .line 2371489
    if-nez v2, :cond_1

    .line 2371490
    :cond_0
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2371491
    :goto_0
    return-void

    .line 2371492
    :cond_1
    invoke-static {p0, v0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->c(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0834b3

    :goto_1
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0

    :cond_2
    const v0, 0x7f0834b2

    goto :goto_1
.end method

.method public static b(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2371493
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2371494
    invoke-virtual {p0}, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->a()Ljava/lang/String;

    move-result-object v0

    .line 2371495
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2371496
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v4, v2

    .line 2371497
    const v2, 0x7f0d0c13

    invoke-virtual {p0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 2371498
    if-eqz v2, :cond_1

    .line 2371499
    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v5

    .line 2371500
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int/2addr v3, v5

    .line 2371501
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v2

    add-int/2addr v2, v5

    .line 2371502
    :goto_0
    if-lt v4, v3, :cond_0

    if-gt v4, v2, :cond_0

    :goto_1
    return v0

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    move v2, v1

    move v3, v0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 2371532
    invoke-static {p1}, LX/0hM;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 2371533
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public static y(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;)Z
    .locals 2

    .prologue
    .line 2371503
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->H:LX/GcD;

    sget-object v1, LX/GcD;->SMALL_AND_SCROLL:LX/GcD;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private z()V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 2371504
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->D:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2371505
    iget-object v3, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->u:Landroid/view/View;

    invoke-static {p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->y(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2371506
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->v:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2371507
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->w:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2371508
    iget-object v3, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->x:Landroid/view/View;

    invoke-static {p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->y(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2371509
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->y:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2371510
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->z:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2371511
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->A:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2371512
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->B:Landroid/view/View;

    invoke-static {p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->y(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;)Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2371513
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->C:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2371514
    return-void

    :cond_0
    move v0, v2

    .line 2371515
    goto :goto_0

    :cond_1
    move v0, v1

    .line 2371516
    goto :goto_1

    :cond_2
    move v1, v2

    .line 2371517
    goto :goto_2
.end method


# virtual methods
.method public final a(ILandroid/view/View;Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 2371518
    invoke-static {p2, p3}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->b(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2371519
    const v0, 0x7f0203aa

    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2371520
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 2371521
    invoke-super {p0, p1}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a(Landroid/os/Bundle;)V

    .line 2371522
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v3

    check-cast v3, LX/23P;

    invoke-static {v0}, LX/10N;->b(LX/0QB;)LX/10N;

    move-result-object v4

    check-cast v4, LX/10N;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/H5o;->b(LX/0QB;)LX/H5o;

    move-result-object v6

    check-cast v6, LX/H5o;

    invoke-static {v0}, LX/8D3;->a(LX/0QB;)LX/8D3;

    move-result-object v7

    check-cast v7, LX/8D3;

    const/16 p1, 0xb83

    invoke-static {v0, p1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    iput-object v3, v2, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->l:LX/23P;

    iput-object v4, v2, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->m:LX/10N;

    iput-object v5, v2, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->n:Ljava/util/concurrent/ExecutorService;

    iput-object v6, v2, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->o:LX/H5o;

    iput-object v7, v2, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->p:LX/8D3;

    iput-object p1, v2, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->q:LX/0Or;

    iput-object v0, v2, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->r:Ljava/util/concurrent/ExecutorService;

    .line 2371523
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2371524
    if-eqz v0, :cond_0

    .line 2371525
    const-string v1, "user_id_to_log_in"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->I:Ljava/lang/String;

    .line 2371526
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2371527
    const v0, 0x7f0d0c13

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2371528
    if-eqz v0, :cond_0

    .line 2371529
    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->j:Landroid/view/View;

    .line 2371530
    :goto_0
    return-void

    .line 2371531
    :cond_0
    iput-object p1, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->j:Landroid/view/View;

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 2371456
    invoke-static {p1, p2}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->b(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2371457
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->F:Z

    .line 2371458
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;Landroid/view/View;I[I)V
    .locals 2

    .prologue
    .line 2371459
    iget-boolean v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->F:Z

    if-eqz v0, :cond_1

    .line 2371460
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->f:I

    .line 2371461
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->F:Z

    .line 2371462
    const v0, 0x7f0d0c13

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2371463
    if-eqz v0, :cond_0

    .line 2371464
    new-instance v1, LX/3x3;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {v1, p2, v0}, LX/3x3;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 2371465
    iget-object p2, v1, LX/3x3;->b:LX/3v0;

    move-object p2, p2

    .line 2371466
    invoke-interface {p2}, Landroid/view/Menu;->clear()V

    .line 2371467
    invoke-virtual {v1}, LX/3x3;->b()Landroid/view/MenuInflater;

    move-result-object p3

    const p4, 0x7f110005

    invoke-virtual {p3, p4, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2371468
    invoke-static {p0, v1, p1}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->a(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;LX/3x3;Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V

    .line 2371469
    new-instance p2, LX/GcB;

    invoke-direct {p2, p0, p1}, LX/GcB;-><init>(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V

    .line 2371470
    iput-object p2, v1, LX/3x3;->e:LX/3x2;

    .line 2371471
    invoke-virtual {v1}, LX/3x3;->c()V

    .line 2371472
    :cond_0
    :goto_0
    return-void

    .line 2371473
    :cond_1
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->d:LX/2Ad;

    if-eqz v0, :cond_0

    .line 2371474
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->d:LX/2Ad;

    invoke-interface {v0, p1}, LX/2Ad;->a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)Z
    .locals 2

    .prologue
    .line 2371455
    const-string v0, "password_account"

    iget-object v1, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mNonce:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(ILandroid/view/View;)V
    .locals 1

    .prologue
    .line 2371453
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2371454
    return-void
.end method

.method public final c(ILandroid/view/View;)V
    .locals 1

    .prologue
    .line 2371451
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2371452
    return-void
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2371450
    const v0, 0x7f0303d6

    return v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 2371449
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->H:LX/GcD;

    sget-object v1, LX/GcD;->ALL_LARGE:LX/GcD;

    if-ne v0, v1, :cond_0

    const v0, 0x7f0303d9

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0303db

    goto :goto_0
.end method

.method public final l()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2371438
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->D:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2371439
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->u:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2371440
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->v:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2371441
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->w:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2371442
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->x:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2371443
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->y:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2371444
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->z:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2371445
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->A:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2371446
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->B:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2371447
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->C:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2371448
    return-void
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 2371433
    iget-boolean v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->G:Z

    if-eqz v0, :cond_0

    .line 2371434
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->G:Z

    .line 2371435
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a:LX/Gbz;

    invoke-virtual {v0}, LX/Gbz;->a()Z

    .line 2371436
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->z()V

    .line 2371437
    :cond_0
    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 2371430
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->G:Z

    .line 2371431
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->z()V

    .line 2371432
    return-void
.end method

.method public final o()V
    .locals 11

    .prologue
    .line 2371359
    invoke-static {p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->D(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2371360
    sget-object v0, LX/GcD;->SMALL_AND_SCROLL:LX/GcD;

    .line 2371361
    :goto_0
    move-object v0, v0

    .line 2371362
    const/4 v1, 0x1

    .line 2371363
    sget-object v2, LX/GcD;->UNSET:LX/GcD;

    if-ne v0, v2, :cond_6

    .line 2371364
    :goto_1
    return-void

    .line 2371365
    :cond_0
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->t:Landroid/view/View;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 2371366
    :goto_2
    if-gtz v0, :cond_1

    .line 2371367
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 2371368
    :cond_1
    const/4 v1, 0x1

    const/high16 v2, 0x41c00000    # 24.0f

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    .line 2371369
    int-to-float v0, v0

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 2371370
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b22ad

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2371371
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b22ae

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2371372
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b22b5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2371373
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b22b6

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2371374
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b22b9

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 2371375
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b22ba

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 2371376
    iget-object v7, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a:LX/Gbz;

    invoke-virtual {v7}, LX/Gbz;->getCount()I

    move-result v7

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b22be

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    mul-int/2addr v7, v8

    .line 2371377
    iget-object v8, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a:LX/Gbz;

    invoke-virtual {v8}, LX/Gbz;->getCount()I

    move-result v8

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0b22bf

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    mul-int/2addr v8, v9

    .line 2371378
    add-int/2addr v3, v4

    .line 2371379
    sub-int/2addr v0, v3

    .line 2371380
    add-int v3, v1, v5

    add-int/2addr v3, v7

    if-ge v3, v0, :cond_3

    .line 2371381
    sget-object v0, LX/GcD;->ALL_LARGE:LX/GcD;

    goto/16 :goto_0

    .line 2371382
    :cond_2
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->t:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    goto/16 :goto_2

    .line 2371383
    :cond_3
    add-int/2addr v1, v5

    add-int/2addr v1, v8

    if-ge v1, v0, :cond_4

    .line 2371384
    sget-object v0, LX/GcD;->SMALL_LIST:LX/GcD;

    goto/16 :goto_0

    .line 2371385
    :cond_4
    add-int v1, v2, v6

    add-int/2addr v1, v8

    if-ge v1, v0, :cond_5

    .line 2371386
    sget-object v0, LX/GcD;->ALL_SMALL:LX/GcD;

    goto/16 :goto_0

    .line 2371387
    :cond_5
    sget-object v0, LX/GcD;->SMALL_AND_SCROLL:LX/GcD;

    goto/16 :goto_0

    .line 2371388
    :cond_6
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->H:LX/GcD;

    .line 2371389
    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->H:LX/GcD;

    .line 2371390
    sget-object v3, LX/GcD;->ALL_LARGE:LX/GcD;

    if-ne v2, v3, :cond_7

    sget-object v3, LX/GcD;->ALL_LARGE:LX/GcD;

    if-ne v0, v3, :cond_8

    :cond_7
    sget-object v3, LX/GcD;->ALL_LARGE:LX/GcD;

    if-eq v2, v3, :cond_9

    sget-object v2, LX/GcD;->ALL_LARGE:LX/GcD;

    if-ne v0, v2, :cond_9

    .line 2371391
    :cond_8
    iput-boolean v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->h:Z

    .line 2371392
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a:LX/Gbz;

    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->k()I

    move-result v3

    .line 2371393
    iput v3, v2, LX/Gbz;->d:I

    .line 2371394
    :cond_9
    sget-object v2, LX/GcD;->ALL_LARGE:LX/GcD;

    if-eq v0, v2, :cond_a

    sget-object v2, LX/GcD;->SMALL_LIST:LX/GcD;

    if-ne v0, v2, :cond_b

    .line 2371395
    :cond_a
    :goto_3
    invoke-static {p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->D(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 2371396
    :goto_4
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 2371397
    invoke-static {p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->y(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;)Z

    move-result v5

    .line 2371398
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->y:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 2371399
    if-eqz v5, :cond_13

    const/high16 v2, 0x3f800000    # 1.0f

    :goto_5
    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2371400
    if-eqz v5, :cond_14

    move v2, v3

    :goto_6
    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2371401
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->y:Landroid/widget/ScrollView;

    invoke-virtual {v2, v1}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2371402
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->u:Landroid/view/View;

    if-eqz v5, :cond_15

    move v1, v4

    :goto_7
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2371403
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->B:Landroid/view/View;

    if-eqz v5, :cond_16

    move v1, v4

    :goto_8
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2371404
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->x:Landroid/view/View;

    if-eqz v5, :cond_17

    :goto_9
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2371405
    goto/16 :goto_1

    .line 2371406
    :cond_b
    const/4 v1, 0x0

    goto :goto_3

    .line 2371407
    :cond_c
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->v:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 2371408
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    if-eqz v1, :cond_d

    const v3, 0x7f0b22af

    :goto_a
    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 2371409
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    if-eqz v1, :cond_e

    const v3, 0x7f0b22ad

    :goto_b
    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2371410
    iget-object v3, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->v:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2371411
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    if-eqz v1, :cond_f

    const v2, 0x7f0b22b1

    :goto_c
    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2371412
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    if-eqz v1, :cond_10

    const v2, 0x7f0b22b2

    :goto_d
    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2371413
    iget-object v4, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->v:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->v:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getPaddingLeft()I

    move-result v5

    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->v:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getPaddingRight()I

    move-result v0

    invoke-virtual {v4, v5, v3, v0, v2}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 2371414
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->C:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 2371415
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    if-eqz v1, :cond_11

    const v3, 0x7f0b22b9

    :goto_e
    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2371416
    iget-object v3, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->C:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2371417
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    if-eqz v1, :cond_12

    const v2, 0x7f0b22bc

    :goto_f
    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2371418
    iget-object v3, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->C:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->C:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->C:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v5

    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->C:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v0

    invoke-virtual {v3, v4, v2, v5, v0}, Landroid/view/ViewGroup;->setPadding(IIII)V

    goto/16 :goto_4

    .line 2371419
    :cond_d
    const v3, 0x7f0b22b0

    goto/16 :goto_a

    .line 2371420
    :cond_e
    const v3, 0x7f0b22ae

    goto :goto_b

    .line 2371421
    :cond_f
    const v2, 0x7f0b22b3

    goto :goto_c

    .line 2371422
    :cond_10
    const v2, 0x7f0b22b4

    goto :goto_d

    .line 2371423
    :cond_11
    const v3, 0x7f0b22ba

    goto :goto_e

    .line 2371424
    :cond_12
    const v2, 0x7f0b22bd

    goto :goto_f

    .line 2371425
    :cond_13
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 2371426
    :cond_14
    const/4 v2, -0x2

    goto/16 :goto_6

    :cond_15
    move v1, v3

    .line 2371427
    goto/16 :goto_7

    :cond_16
    move v1, v3

    .line 2371428
    goto/16 :goto_8

    :cond_17
    move v3, v4

    .line 2371429
    goto/16 :goto_9
.end method

.method public final onResume()V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x63eb74a0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2371345
    invoke-super {p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->onResume()V

    .line 2371346
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->I:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2371347
    const/4 v1, 0x0

    .line 2371348
    :goto_0
    move v1, v1

    .line 2371349
    if-eqz v1, :cond_0

    .line 2371350
    const/16 v1, 0x2b

    const v2, 0x6d5938ad

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2371351
    :goto_1
    return-void

    .line 2371352
    :cond_0
    iget-object v4, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->j:Landroid/view/View;

    if-eqz v4, :cond_1

    iget-boolean v4, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->g:Z

    if-eqz v4, :cond_3

    .line 2371353
    :cond_1
    :goto_2
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->n:Ljava/util/concurrent/ExecutorService;

    new-instance v2, LX/GcC;

    invoke-direct {v2, p0}, LX/GcC;-><init>(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;)V

    const v3, -0x3dbbf463

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Callable;I)Ljava/util/concurrent/Future;

    .line 2371354
    const v1, 0x41e0010d    # 28.000513f

    invoke-static {v1, v0}, LX/02F;->f(II)V

    goto :goto_1

    .line 2371355
    :cond_2
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->d:LX/2Ad;

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->I:Ljava/lang/String;

    invoke-interface {v1, v2}, LX/2Ad;->a(Ljava/lang/String;)V

    .line 2371356
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->I:Ljava/lang/String;

    .line 2371357
    const/4 v1, 0x1

    goto :goto_0

    .line 2371358
    :cond_3
    iget-object v4, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->E:Landroid/os/Handler;

    new-instance v5, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment$1;

    invoke-direct {v5, p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment$1;-><init>(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;)V

    const-wide/16 v6, 0x3e8

    const v8, 0x10c06828

    invoke-static {v4, v5, v6, v7, v8}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_2
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2371327
    invoke-super {p0, p1, p2}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2371328
    const v0, 0x7f0d0bfe

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->t:Landroid/view/View;

    .line 2371329
    const v0, 0x7f0d0c03

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->i:Landroid/view/ViewGroup;

    .line 2371330
    const v0, 0x7f0d0bff

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->u:Landroid/view/View;

    .line 2371331
    const v0, 0x7f0d039f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->v:Landroid/widget/ImageView;

    .line 2371332
    const v0, 0x7f0d0c00

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->w:Lcom/facebook/resources/ui/FbTextView;

    .line 2371333
    const v0, 0x7f0d0c01

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->x:Landroid/view/View;

    .line 2371334
    const v0, 0x7f0d0c02

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->y:Landroid/widget/ScrollView;

    .line 2371335
    const v0, 0x7f0d0c04

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->z:Landroid/view/View;

    .line 2371336
    const v0, 0x7f0d0c05

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->A:Landroid/view/ViewGroup;

    .line 2371337
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->A:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->t()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2371338
    const v0, 0x7f0d0c06

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->B:Landroid/view/View;

    .line 2371339
    const v0, 0x7f0d0c07

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->C:Landroid/view/ViewGroup;

    .line 2371340
    const v0, 0x7f0d0c08

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 2371341
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->l:LX/23P;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2371342
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->u()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2371343
    const v0, 0x7f0d04de

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->D:Landroid/view/ViewGroup;

    .line 2371344
    return-void
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 2371323
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;->F:Z

    .line 2371324
    return-void
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 2371326
    const/4 v0, 0x1

    return v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 2371325
    const/4 v0, 0x1

    return v0
.end method
