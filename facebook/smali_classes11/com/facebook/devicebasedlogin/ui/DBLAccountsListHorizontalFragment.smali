.class public Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;
.super Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;
.source ""


# instance fields
.field public l:LX/10N;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private m:Landroid/widget/ProgressBar;

.field private n:Lcom/facebook/fbui/glyph/GlyphView;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/widget/TextView;

.field private q:Lcom/facebook/resources/ui/FbTextView;

.field private r:Landroid/widget/LinearLayout;

.field private s:LX/8YM;

.field public final t:Landroid/os/Handler;

.field private u:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/8YK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2371270
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;-><init>()V

    .line 2371271
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->t:Landroid/os/Handler;

    return-void
.end method

.method private a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;Landroid/view/View;I[IZ)V
    .locals 11

    .prologue
    .line 2371222
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 2371223
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 2371224
    iget v0, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2371225
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    const/4 v3, 0x0

    neg-int v4, v0

    int-to-float v4, v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2371226
    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 2371227
    invoke-virtual {v2}, Landroid/view/animation/TranslateAnimation;->reset()V

    .line 2371228
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 2371229
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    const/4 v4, 0x0

    int-to-float v0, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v3, v4, v0, v5, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2371230
    const-wide/16 v4, 0x1f4

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 2371231
    invoke-virtual {v3}, Landroid/view/animation/TranslateAnimation;->reset()V

    .line 2371232
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 2371233
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_0

    .line 2371234
    iget-object v4, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->i:Landroid/view/ViewGroup;

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2371235
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2371236
    :cond_0
    add-int/lit8 v0, p3, 0x1

    :goto_1
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a:LX/Gbz;

    invoke-virtual {v2}, LX/Gbz;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 2371237
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->i:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2371238
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2371239
    :cond_1
    iget v0, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v0, v0, 0x2

    .line 2371240
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    .line 2371241
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a:LX/Gbz;

    invoke-virtual {v2}, LX/Gbz;->getCount()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 2371242
    const/4 v0, 0x0

    aget v0, p4, v0

    .line 2371243
    :cond_2
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getTop()I

    move-result v2

    .line 2371244
    iget-object v3, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    .line 2371245
    if-nez p5, :cond_4

    .line 2371246
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->d:LX/2Ad;

    if-eqz v0, :cond_3

    .line 2371247
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->d:LX/2Ad;

    invoke-interface {v0, p1}, LX/2Ad;->a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V

    .line 2371248
    :cond_3
    :goto_2
    return-void

    .line 2371249
    :cond_4
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    .line 2371250
    const/high16 v4, 0x43c50000    # 394.0f

    mul-float/2addr v3, v4

    const/high16 v4, 0x3f000000    # 0.5f

    add-float/2addr v3, v4

    float-to-int v3, v3

    .line 2371251
    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    .line 2371252
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0d0c0c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    .line 2371253
    const-string v4, "translationY"

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v6, 0x0

    sub-int/2addr v1, v3

    int-to-float v1, v1

    aput v1, v5, v6

    invoke-static {p2, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 2371254
    const-string v3, "translationX"

    const/4 v4, 0x1

    new-array v4, v4, [F

    const/4 v5, 0x0

    const/4 v6, 0x0

    aget v6, p4, v6

    sub-int/2addr v0, v6

    int-to-float v0, v0

    aput v0, v4, v5

    invoke-static {p2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 2371255
    const v3, 0x7f0d0bf2

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const-string v4, "alpha"

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v6, 0x0

    const/4 v7, 0x0

    aput v7, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 2371256
    iget-object v4, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->r:Landroid/widget/LinearLayout;

    const-string v5, "translationY"

    const/4 v6, 0x1

    new-array v6, v6, [F

    const/4 v7, 0x0

    neg-int v2, v2

    int-to-float v2, v2

    aput v2, v6, v7

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 2371257
    iget-object v4, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->n:Lcom/facebook/fbui/glyph/GlyphView;

    const-string v5, "translationY"

    const/4 v6, 0x1

    new-array v6, v6, [F

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->n:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v8}, Lcom/facebook/fbui/glyph/GlyphView;->getTop()I

    move-result v8

    int-to-float v8, v8

    aput v8, v6, v7

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 2371258
    iget-object v5, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->o:Landroid/widget/TextView;

    const-string v6, "translationY"

    const/4 v7, 0x1

    new-array v7, v7, [F

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->o:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getTop()I

    move-result v9

    int-to-float v9, v9

    aput v9, v7, v8

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 2371259
    iget-object v6, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->p:Landroid/widget/TextView;

    const-string v7, "translationY"

    const/4 v8, 0x1

    new-array v8, v8, [F

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->p:Landroid/widget/TextView;

    invoke-virtual {v10}, Landroid/widget/TextView;->getTop()I

    move-result v10

    int-to-float v10, v10

    aput v10, v8, v9

    invoke-static {v6, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 2371260
    new-instance v7, Landroid/animation/AnimatorSet;

    invoke-direct {v7}, Landroid/animation/AnimatorSet;-><init>()V

    .line 2371261
    invoke-virtual {v7, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 2371262
    invoke-virtual {v7, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 2371263
    invoke-virtual {v7, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 2371264
    invoke-virtual {v7, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 2371265
    invoke-virtual {v7, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 2371266
    invoke-virtual {v7, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 2371267
    new-instance v0, LX/Gc7;

    invoke-direct {v0, p0, p1}, LX/Gc7;-><init>(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V

    invoke-virtual {v7, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2371268
    const-wide/16 v0, 0x1f4

    invoke-virtual {v7, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 2371269
    invoke-virtual {v7}, Landroid/animation/AnimatorSet;->start()V

    goto/16 :goto_2
.end method

.method public static a$redex0(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;Landroid/view/View;I[I)V
    .locals 16

    .prologue
    .line 2371179
    new-instance v12, Landroid/util/DisplayMetrics;

    invoke-direct {v12}, Landroid/util/DisplayMetrics;-><init>()V

    .line 2371180
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v12}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 2371181
    iget v13, v12, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2371182
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    const/4 v3, 0x0

    neg-int v4, v13

    int-to-float v4, v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-direct/range {v2 .. v10}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 2371183
    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 2371184
    invoke-virtual {v2}, Landroid/view/animation/TranslateAnimation;->reset()V

    .line 2371185
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 2371186
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    const/4 v4, 0x0

    int-to-float v5, v13

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-direct/range {v3 .. v11}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 2371187
    const-wide/16 v4, 0x1f4

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 2371188
    invoke-virtual {v3}, Landroid/view/animation/TranslateAnimation;->reset()V

    .line 2371189
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 2371190
    new-instance v4, LX/Gc8;

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v4, v0, v1}, LX/Gc8;-><init>(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;I)V

    invoke-virtual {v3, v4}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2371191
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getTop()I

    move-result v4

    .line 2371192
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v5

    add-int v14, v4, v5

    .line 2371193
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    .line 2371194
    const/high16 v5, 0x43c50000    # 394.0f

    mul-float/2addr v4, v5

    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v4, v5

    float-to-int v4, v4

    .line 2371195
    iget v5, v12, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int v4, v5, v4

    div-int/lit8 v4, v4, 0x2

    .line 2371196
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v5

    const v6, 0x7f0d0c0c

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v5

    .line 2371197
    sub-int v10, v5, v4

    .line 2371198
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    sub-int v4, v13, v4

    div-int/lit8 v15, v4, 0x2

    .line 2371199
    new-instance v4, Landroid/view/animation/TranslateAnimation;

    const/4 v5, 0x0

    const/4 v6, 0x0

    aget v6, p3, v6

    sub-int v6, v15, v6

    int-to-float v6, v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    aget v8, p3, v8

    sub-int v8, v15, v8

    int-to-float v8, v8

    const/4 v9, 0x0

    neg-int v10, v10

    int-to-float v10, v10

    const/4 v11, 0x1

    const/4 v12, 0x0

    invoke-direct/range {v4 .. v12}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 2371200
    const-wide/16 v6, 0x1f4

    invoke-virtual {v4, v6, v7}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 2371201
    invoke-virtual {v4}, Landroid/view/animation/TranslateAnimation;->reset()V

    .line 2371202
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 2371203
    new-instance v5, Landroid/view/animation/AlphaAnimation;

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-direct {v5, v6, v7}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 2371204
    const-wide/16 v6, 0x1f4

    invoke-virtual {v5, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 2371205
    invoke-virtual {v5}, Landroid/view/animation/AlphaAnimation;->reset()V

    .line 2371206
    const v6, 0x7f0d0bf2

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2371207
    new-instance v5, Landroid/view/animation/TranslateAnimation;

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x2

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x1

    const/4 v13, 0x0

    invoke-direct/range {v5 .. v13}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 2371208
    const-wide/16 v6, 0x1f4

    invoke-virtual {v5, v6, v7}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 2371209
    invoke-virtual {v5}, Landroid/view/animation/TranslateAnimation;->reset()V

    .line 2371210
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 2371211
    new-instance v6, Landroid/view/animation/TranslateAnimation;

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    neg-int v12, v14

    int-to-float v12, v12

    const/4 v13, 0x1

    const/4 v14, 0x0

    invoke-direct/range {v6 .. v14}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 2371212
    const-wide/16 v8, 0x1f4

    invoke-virtual {v6, v8, v9}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 2371213
    invoke-virtual {v6}, Landroid/view/animation/TranslateAnimation;->reset()V

    .line 2371214
    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 2371215
    new-instance v7, LX/Gc9;

    move-object/from16 v8, p0

    move/from16 v9, p2

    move-object v10, v2

    move-object v11, v3

    move v12, v15

    move-object/from16 v13, p3

    move-object/from16 v14, p1

    invoke-direct/range {v7 .. v14}, LX/Gc9;-><init>(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;ILandroid/view/animation/TranslateAnimation;Landroid/view/animation/TranslateAnimation;I[ILandroid/view/View;)V

    invoke-virtual {v6, v7}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2371216
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2371217
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2371218
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->n:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v2, v5}, Lcom/facebook/fbui/glyph/GlyphView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2371219
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->o:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2371220
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->p:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2371221
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/View;)V
    .locals 6

    .prologue
    .line 2371173
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->s:LX/8YM;

    invoke-virtual {v0}, LX/8YH;->a()LX/8YK;

    move-result-object v0

    .line 2371174
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->u:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2371175
    const-wide/high16 v2, 0x4044000000000000L    # 40.0

    const-wide/high16 v4, 0x4028000000000000L    # 12.0

    invoke-static {v2, v3, v4, v5}, LX/8YL;->a(DD)LX/8YL;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8YK;->a(LX/8YL;)LX/8YK;

    .line 2371176
    new-instance v1, LX/Gc6;

    invoke-direct {v1, p0, p2}, LX/Gc6;-><init>(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;Landroid/view/View;)V

    move-object v1, v1

    .line 2371177
    invoke-virtual {v0, v1}, LX/8YK;->a(LX/7hQ;)LX/8YK;

    .line 2371178
    return-void
.end method

.method public final a(ILandroid/view/View;Landroid/view/MotionEvent;)V
    .locals 4

    .prologue
    .line 2371171
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->u:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8YK;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/8YK;->b(D)LX/8YK;

    .line 2371172
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2371168
    invoke-super {p0, p1}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a(Landroid/os/Bundle;)V

    .line 2371169
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;

    invoke-static {v0}, LX/10N;->b(LX/0QB;)LX/10N;

    move-result-object v0

    check-cast v0, LX/10N;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->l:LX/10N;

    .line 2371170
    return-void
.end method

.method public final a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;Landroid/view/View;I[I)V
    .locals 6

    .prologue
    .line 2371166
    iget-object v0, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mIsPinSet:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;Landroid/view/View;I[IZ)V

    .line 2371167
    return-void
.end method

.method public final a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)Z
    .locals 2

    .prologue
    .line 2371272
    const-string v0, "password_account"

    iget-object v1, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mNonce:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2371273
    iget-object v0, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->b(Ljava/lang/String;)Z

    move-result v0

    .line 2371274
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(ILandroid/view/View;)V
    .locals 4

    .prologue
    .line 2371104
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->u:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8YK;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/8YK;->b(D)LX/8YK;

    .line 2371105
    return-void
.end method

.method public final c(ILandroid/view/View;)V
    .locals 4

    .prologue
    .line 2371164
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->u:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8YK;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/8YK;->b(D)LX/8YK;

    .line 2371165
    return-void
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2371106
    const v0, 0x7f0303d7

    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 2371107
    const v0, 0x7f0303d8

    return v0
.end method

.method public final l()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2371108
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->m:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2371109
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2371110
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->n:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2371111
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2371112
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2371113
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2371114
    return-void
.end method

.method public final n()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2371115
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->m:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2371116
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2371117
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->n:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2371118
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2371119
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2371120
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2371121
    return-void
.end method

.method public final onResume()V
    .locals 10

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x5c1c3f46

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2371122
    invoke-super {p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->onResume()V

    .line 2371123
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->e:Ljava/lang/String;

    const-string v2, "login_state_passcode_entry"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2371124
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2371125
    const-string v2, "dbl_account_details"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 2371126
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->e:Ljava/lang/String;

    .line 2371127
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 2371128
    if-eqz v2, :cond_0

    .line 2371129
    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    .line 2371130
    if-eqz v3, :cond_0

    .line 2371131
    new-instance v4, LX/Gc5;

    invoke-direct {v4, p0, v2, v0}, LX/Gc5;-><init>(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;Landroid/view/View;Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V

    invoke-virtual {v3, v4}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2371132
    :cond_0
    :goto_0
    const v0, -0xd08aa2a

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2371133
    :cond_1
    iget-object v5, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->j:Landroid/view/View;

    if-eqz v5, :cond_2

    iget-boolean v5, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->g:Z

    if-eqz v5, :cond_3

    .line 2371134
    :cond_2
    :goto_1
    goto :goto_0

    .line 2371135
    :cond_3
    iget-object v5, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->t:Landroid/os/Handler;

    new-instance v6, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment$6;

    invoke-direct {v6, p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment$6;-><init>(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;)V

    const-wide/16 v7, 0x3e8

    const v9, -0x50edeb77

    invoke-static {v5, v6, v7, v8, v9}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2371136
    invoke-super {p0, p1, p2}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2371137
    const v0, 0x7f0d0c0c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    .line 2371138
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 2371139
    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 2371140
    const/high16 v2, 0x41200000    # 10.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 2371141
    invoke-virtual {v0, v1, v3, v1, v3}, Landroid/widget/HorizontalScrollView;->setPadding(IIII)V

    .line 2371142
    const v0, 0x7f0d0c03

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->i:Landroid/view/ViewGroup;

    .line 2371143
    const v0, 0x7f0d04de

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->m:Landroid/widget/ProgressBar;

    .line 2371144
    const v0, 0x7f0d0c0a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->r:Landroid/widget/LinearLayout;

    .line 2371145
    const v0, 0x7f0d0c0d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->n:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2371146
    const v0, 0x7f0d0c0e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->o:Landroid/widget/TextView;

    .line 2371147
    const v0, 0x7f0d0c0f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->p:Landroid/widget/TextView;

    .line 2371148
    const v0, 0x7f0d0c0b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->q:Lcom/facebook/resources/ui/FbTextView;

    .line 2371149
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->l:LX/10N;

    .line 2371150
    iget-object v1, v0, LX/10N;->c:LX/0Uh;

    const/16 v2, 0x14

    const/4 p1, 0x0

    invoke-virtual {v1, v2, p1}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 2371151
    if-nez v0, :cond_0

    .line 2371152
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->q:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2371153
    :goto_0
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->o:Landroid/widget/TextView;

    const v1, 0x7f08346f

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2371154
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->p:Landroid/widget/TextView;

    const v1, 0x7f0800f0

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2371155
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->n:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2371156
    new-instance v1, LX/Gc2;

    invoke-direct {v1, p0}, LX/Gc2;-><init>(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;)V

    move-object v1, v1

    .line 2371157
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2371158
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->o:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->t()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2371159
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->p:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->u()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2371160
    invoke-static {}, LX/8YM;->b()LX/8YM;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->s:LX/8YM;

    .line 2371161
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->u:Ljava/util/Map;

    .line 2371162
    return-void

    .line 2371163
    :cond_0
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->q:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method
