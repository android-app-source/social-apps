.class public Lcom/facebook/devicebasedlogin/ui/NumPadView;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field public k:Landroid/widget/ImageView;

.field private l:Landroid/view/View;

.field private m:Landroid/view/View;

.field private n:Landroid/view/View;

.field private o:Landroid/view/View;

.field private p:Landroid/widget/LinearLayout;

.field private q:Landroid/widget/LinearLayout;

.field public r:Ljava/lang/StringBuilder;

.field public s:LX/Gci;

.field private t:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2372461
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2372462
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->r:Ljava/lang/StringBuilder;

    .line 2372463
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->t:Landroid/view/View$OnClickListener;

    .line 2372464
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 2372465
    const v1, 0x7f030c3f

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2372466
    invoke-virtual {p0, v2}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2372467
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->p:Landroid/widget/LinearLayout;

    .line 2372468
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->l:Landroid/view/View;

    .line 2372469
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->m:Landroid/view/View;

    .line 2372470
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->n:Landroid/view/View;

    .line 2372471
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->o:Landroid/view/View;

    .line 2372472
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->q:Landroid/widget/LinearLayout;

    .line 2372473
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 2372474
    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->a:Landroid/widget/TextView;

    .line 2372475
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->d:Landroid/widget/TextView;

    .line 2372476
    invoke-virtual {v0, v4}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->g:Landroid/widget/TextView;

    .line 2372477
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 2372478
    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->b:Landroid/widget/TextView;

    .line 2372479
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->e:Landroid/widget/TextView;

    .line 2372480
    invoke-virtual {v0, v4}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->h:Landroid/widget/TextView;

    .line 2372481
    invoke-virtual {v0, v5}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->j:Landroid/widget/TextView;

    .line 2372482
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 2372483
    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->c:Landroid/widget/TextView;

    .line 2372484
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->f:Landroid/widget/TextView;

    .line 2372485
    invoke-virtual {v0, v4}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->i:Landroid/widget/TextView;

    .line 2372486
    invoke-virtual {v0, v5}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->k:Landroid/widget/ImageView;

    .line 2372487
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->a:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->getOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2372488
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->getOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2372489
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->getOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2372490
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->getOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2372491
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->getOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2372492
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->getOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2372493
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->g:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->getOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2372494
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->h:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->getOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2372495
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->getOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2372496
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->getOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2372497
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->k:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->getOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2372498
    return-void
.end method

.method private a(ILandroid/view/View;)V
    .locals 4

    .prologue
    .line 2372499
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->r:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    .line 2372500
    invoke-virtual {p2}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    .line 2372501
    if-le v1, p1, :cond_0

    .line 2372502
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 2372503
    :goto_0
    return-void

    .line 2372504
    :cond_0
    const/16 v1, 0x4a

    const/16 v2, 0x6d

    const/16 v3, 0xa7

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    goto :goto_0
.end method

.method private d()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2372505
    new-instance v0, LX/Gcl;

    invoke-direct {v0, p0}, LX/Gcl;-><init>(Lcom/facebook/devicebasedlogin/ui/NumPadView;)V

    return-object v0
.end method

.method public static e(Lcom/facebook/devicebasedlogin/ui/NumPadView;)V
    .locals 2

    .prologue
    .line 2372506
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->l:Landroid/view/View;

    invoke-direct {p0, v0, v1}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->a(ILandroid/view/View;)V

    .line 2372507
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->m:Landroid/view/View;

    invoke-direct {p0, v0, v1}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->a(ILandroid/view/View;)V

    .line 2372508
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->n:Landroid/view/View;

    invoke-direct {p0, v0, v1}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->a(ILandroid/view/View;)V

    .line 2372509
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->o:Landroid/view/View;

    invoke-direct {p0, v0, v1}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->a(ILandroid/view/View;)V

    .line 2372510
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->r:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 2372511
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->s:LX/Gci;

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->r:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/Gci;->b(Ljava/lang/String;)V

    .line 2372512
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2372513
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->r:Ljava/lang/StringBuilder;

    .line 2372514
    invoke-static {p0}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->e(Lcom/facebook/devicebasedlogin/ui/NumPadView;)V

    .line 2372515
    return-void
.end method

.method public final a(IIII)V
    .locals 2

    .prologue
    .line 2372516
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2372517
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2372518
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2372519
    return-void
.end method

.method public final b()V
    .locals 9

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2372520
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    move v3, v1

    move v4, v2

    move v5, v1

    move v7, v1

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 2372521
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 2372522
    iget-object v3, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2372523
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v2, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 2372524
    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2372525
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 2372526
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2372527
    return-void
.end method

.method public final c()V
    .locals 9

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2372528
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    move v3, v1

    move v4, v2

    move v5, v1

    move v6, v2

    move v7, v1

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 2372529
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 2372530
    iget-object v3, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2372531
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v8, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 2372532
    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2372533
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 2372534
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2372535
    return-void
.end method

.method public getOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2372536
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->t:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 2372537
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->t:Landroid/view/View$OnClickListener;

    .line 2372538
    :goto_0
    return-object v0

    .line 2372539
    :cond_0
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->d()Landroid/view/View$OnClickListener;

    move-result-object v0

    .line 2372540
    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->t:Landroid/view/View$OnClickListener;

    goto :goto_0
.end method

.method public setNumPadViewListener(LX/Gci;)V
    .locals 0

    .prologue
    .line 2372541
    iput-object p1, p0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->s:LX/Gci;

    .line 2372542
    return-void
.end method
