.class public Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;
.super Landroid/view/View;
.source ""


# instance fields
.field private a:Landroid/graphics/Paint;

.field private b:LX/7yR;

.field private c:I

.field private d:I

.field private e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2372394
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2372395
    invoke-direct {p0, p1}, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->a(Landroid/content/Context;)V

    .line 2372396
    iput-object p1, p0, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->e:Landroid/content/Context;

    .line 2372397
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2372385
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->a:Landroid/graphics/Paint;

    .line 2372386
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2372387
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 2372388
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->a:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2372389
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->a:Landroid/graphics/Paint;

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2372390
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2372391
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->a:Landroid/graphics/Paint;

    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2372392
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->a:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/DashPathEffect;

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 2372393
    return-void

    nop

    :array_0
    .array-data 4
        0x41a00000    # 20.0f
        0x41a00000    # 20.0f
    .end array-data
.end method

.method private a(Landroid/graphics/Canvas;LX/7yR;)V
    .locals 6

    .prologue
    .line 2372398
    invoke-virtual {p2}, LX/7yR;->a()F

    move-result v0

    iget v1, p0, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->c:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    .line 2372399
    invoke-virtual {p2}, LX/7yR;->b()F

    move-result v1

    iget v2, p0, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->d:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    .line 2372400
    invoke-virtual {p2}, LX/7yR;->c()F

    move-result v2

    iget v3, p0, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->c:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    .line 2372401
    invoke-virtual {p2}, LX/7yR;->d()F

    move-result v3

    iget v4, p0, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->d:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    .line 2372402
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2372403
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 2372404
    const/high16 v1, -0x40800000    # -1.0f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 2372405
    iget v1, p0, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->c:I

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 2372406
    invoke-virtual {v0, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 2372407
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 2372408
    const v1, 0x3f8ccccd    # 1.1f

    const v2, 0x3fb33333    # 1.4f

    invoke-virtual {v4}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    invoke-virtual {v4}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    invoke-virtual {v0, v1, v2, v3, v5}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 2372409
    invoke-virtual {v0, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 2372410
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v0}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 2372411
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2372383
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->a:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2372384
    return-void
.end method

.method public final a(LX/7yR;II)V
    .locals 0

    .prologue
    .line 2372378
    iput-object p1, p0, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->b:LX/7yR;

    .line 2372379
    iput p2, p0, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->c:I

    .line 2372380
    iput p3, p0, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->d:I

    .line 2372381
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->postInvalidate()V

    .line 2372382
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 2372373
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2372374
    monitor-enter p0

    .line 2372375
    :try_start_0
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->b:LX/7yR;

    if-eqz v0, :cond_0

    .line 2372376
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->b:LX/7yR;

    invoke-direct {p0, p1, v0}, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->a(Landroid/graphics/Canvas;LX/7yR;)V

    .line 2372377
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
