.class public final Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment$6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;)V
    .locals 0

    .prologue
    .line 2371077
    iput-object p1, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment$6;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 2371078
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment$6;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;

    new-instance v1, LX/0hs;

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment$6;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;

    invoke-virtual {v2}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v1, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->k:LX/0hs;

    .line 2371079
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment$6;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->k:LX/0hs;

    const/4 v1, -0x1

    .line 2371080
    iput v1, v0, LX/0hs;->t:I

    .line 2371081
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment$6;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->k:LX/0hs;

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment$6;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;

    iget-object v1, v1, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0ht;->c(Landroid/view/View;)V

    .line 2371082
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment$6;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;

    const/4 v2, 0x1

    .line 2371083
    new-instance v3, Landroid/text/SpannableStringBuilder;

    const v1, 0x7f08349c

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v4, "[[settings_icon]]"

    invoke-static {v1, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2371084
    const-string v1, "[[settings_icon]]"

    .line 2371085
    invoke-static {v1}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    .line 2371086
    invoke-virtual {v4, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 2371087
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2371088
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->start()I

    move-result v4

    .line 2371089
    :goto_0
    move v4, v4

    .line 2371090
    const/4 v1, -0x1

    if-eq v4, v1, :cond_0

    move v1, v2

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2371091
    new-instance v1, Landroid/text/style/ImageSpan;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f02198e

    invoke-direct {v1, v5, v6, v2}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;II)V

    .line 2371092
    add-int/lit8 v2, v4, 0x11

    const/16 v5, 0x21

    invoke-virtual {v3, v1, v4, v2, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2371093
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2371094
    iget-object v2, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->k:LX/0hs;

    invoke-virtual {v2, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 2371095
    iget-object v1, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->k:LX/0hs;

    new-instance v2, LX/GcA;

    invoke-direct {v2, v0}, LX/GcA;-><init>(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;)V

    invoke-virtual {v1, v2}, LX/0hs;->a(LX/5Od;)V

    .line 2371096
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment$6;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->k:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->d()V

    .line 2371097
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment$6;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->g:Z

    .line 2371098
    return-void

    .line 2371099
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    const/4 v4, -0x1

    goto :goto_0
.end method
