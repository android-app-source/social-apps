.class public abstract Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0l6;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/Gb4;
.implements LX/2JL;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# static fields
.field private static final l:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/Gbz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/2Ad;

.field public e:Ljava/lang/String;

.field public f:I

.field public g:Z

.field public h:Z

.field public i:Landroid/view/ViewGroup;

.field public j:Landroid/view/View;

.field public k:LX/0hs;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2370998
    const-class v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    sput-object v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->l:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2370999
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2371000
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->f:I

    .line 2371001
    iput-boolean v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->g:Z

    .line 2371002
    iput-boolean v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->h:Z

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    new-instance v0, LX/Gbz;

    const-class v1, Landroid/content/Context;

    invoke-interface {v3, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {v3}, LX/10M;->b(LX/0QB;)LX/10M;

    move-result-object v2

    check-cast v2, LX/10M;

    invoke-static {v3}, LX/10N;->b(LX/0QB;)LX/10N;

    move-result-object v4

    check-cast v4, LX/10N;

    invoke-static {v3}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object p0

    check-cast p0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v0, v1, v2, v4, p0}, LX/Gbz;-><init>(Landroid/content/Context;LX/10M;LX/10N;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    move-object v1, v0

    check-cast v1, LX/Gbz;

    invoke-static {v3}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v3}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    iput-object v1, p1, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a:LX/Gbz;

    iput-object v2, p1, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v3, p1, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->c:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method


# virtual methods
.method public a(ILandroid/view/View;)V
    .locals 0

    .prologue
    .line 2371003
    return-void
.end method

.method public a(ILandroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 2371004
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2371005
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2371006
    const-class v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    invoke-static {v0, p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2371007
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2371008
    if-eqz v0, :cond_0

    .line 2371009
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2371010
    const-string v1, "previous_login_state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->e:Ljava/lang/String;

    .line 2371011
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2371012
    iput-object p1, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->j:Landroid/view/View;

    .line 2371013
    return-void
.end method

.method public a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 2371014
    return-void
.end method

.method public abstract a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;Landroid/view/View;I[I)V
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2371015
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->f:I

    .line 2371016
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->n()V

    .line 2371017
    return-void
.end method

.method public abstract a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)Z
.end method

.method public b(ILandroid/view/View;)V
    .locals 0

    .prologue
    .line 2371018
    return-void
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2371019
    invoke-static {p1}, LX/2ss;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    .line 2371020
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2371021
    const/4 v0, 0x0

    .line 2371022
    :goto_0
    return v0

    .line 2371023
    :cond_0
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    invoke-interface {v2, v1, v0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    goto :goto_0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 2371024
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->l()V

    .line 2371025
    return-void
.end method

.method public c(ILandroid/view/View;)V
    .locals 0

    .prologue
    .line 2371026
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2370995
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->f:I

    .line 2370996
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->m()V

    .line 2370997
    return-void
.end method

.method public abstract e()I
.end method

.method public abstract k()I
.end method

.method public abstract l()V
.end method

.method public m()V
    .locals 0

    .prologue
    .line 2370972
    return-void
.end method

.method public abstract n()V
.end method

.method public o()V
    .locals 0

    .prologue
    .line 2370973
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x39675982

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2370974
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2370975
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a:LX/Gbz;

    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->k()I

    move-result v2

    .line 2370976
    iput v2, v1, LX/Gbz;->d:I

    .line 2370977
    const/16 v1, 0x2b

    const v2, -0x189f045a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x1474e162

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2370978
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->e()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2370979
    const/16 v2, 0x2b

    const v3, 0x15bd83dc

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x29164bec

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2370980
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2370981
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a:LX/Gbz;

    invoke-virtual {v1}, LX/Gbz;->a()Z

    move-result v1

    .line 2370982
    if-nez v1, :cond_0

    .line 2370983
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->d:LX/2Ad;

    if-eqz v1, :cond_0

    .line 2370984
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->d:LX/2Ad;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/2Ad;->l_(Z)V

    .line 2370985
    :cond_0
    const/16 v1, 0x2b

    const v2, 0xf4ff33c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2370986
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a:LX/Gbz;

    .line 2370987
    new-instance v1, LX/Gc1;

    invoke-direct {v1, p0}, LX/Gc1;-><init>(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;)V

    move-object v1, v1

    .line 2370988
    invoke-virtual {v0, v1}, LX/Gbz;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2370989
    return-void
.end method

.method public p()V
    .locals 0

    .prologue
    .line 2370990
    return-void
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 2370991
    const/4 v0, 0x0

    return v0
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 2370992
    const/4 v0, 0x0

    return v0
.end method

.method public final t()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2370993
    new-instance v0, LX/Gc3;

    invoke-direct {v0, p0}, LX/Gc3;-><init>(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;)V

    return-object v0
.end method

.method public final u()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2370994
    new-instance v0, LX/Gc4;

    invoke-direct {v0, p0}, LX/Gc4;-><init>(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;)V

    return-object v0
.end method
