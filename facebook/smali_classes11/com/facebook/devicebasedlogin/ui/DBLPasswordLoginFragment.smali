.class public Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements LX/0l6;
.implements LX/Gb4;
.implements LX/FAz;
.implements LX/2JL;


# instance fields
.field public a:LX/0dC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/2Lx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/10N;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/widget/EditText;

.field public e:LX/2Ad;

.field public f:Landroid/widget/Button;

.field private g:Landroid/widget/ProgressBar;

.field private h:I

.field public i:Landroid/widget/TextView;

.field public j:Landroid/widget/TextView;

.field private k:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

.field public l:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

.field public m:Landroid/view/View;

.field public final n:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2371627
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2371628
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->n:Landroid/os/Handler;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v1

    check-cast v1, LX/0dC;

    invoke-static {p0}, LX/2Lx;->b(LX/0QB;)LX/2Lx;

    move-result-object v2

    check-cast v2, LX/2Lx;

    invoke-static {p0}, LX/10N;->b(LX/0QB;)LX/10N;

    move-result-object p0

    check-cast p0, LX/10N;

    iput-object v1, p1, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->a:LX/0dC;

    iput-object v2, p1, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->b:LX/2Lx;

    iput-object p0, p1, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->c:LX/10N;

    return-void
.end method

.method public static l(Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2371705
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->f:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 2371706
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2371707
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->m:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2371708
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2371709
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->l:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    invoke-virtual {v0, v1}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->setVisibility(I)V

    .line 2371710
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->d:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 2371711
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 2371700
    iput p1, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->h:I

    .line 2371701
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2371702
    if-eqz v0, :cond_0

    .line 2371703
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->j:Landroid/widget/TextView;

    iget v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->h:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2371704
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2371695
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2371696
    const-class v0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;

    invoke-static {v0, p0}, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2371697
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2371698
    const-string v1, "dbl_account_details"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->k:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 2371699
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2371691
    invoke-static {p0}, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->l(Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;)V

    .line 2371692
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->d:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2371693
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->g:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2371694
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2371682
    const/16 v1, 0x8

    .line 2371683
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->f:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 2371684
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2371685
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->m:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2371686
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2371687
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->l:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    invoke-virtual {v0, v1}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->setVisibility(I)V

    .line 2371688
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->d:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 2371689
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->g:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2371690
    return-void
.end method

.method public final c_(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2371678
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2371679
    const-string v1, "dbl_flag"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 2371680
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->e:LX/2Ad;

    new-instance v2, Lcom/facebook/auth/credentials/PasswordCredentials;

    iget-object v3, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->k:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v3, v3, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    sget-object v4, LX/28K;->DBL_PASSWORD:LX/28K;

    invoke-direct {v2, v3, p1, v4}, Lcom/facebook/auth/credentials/PasswordCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;LX/28K;)V

    iget-object v3, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->k:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    invoke-interface {v1, v2, v3, v0}, LX/2Ad;->a(Lcom/facebook/auth/credentials/PasswordCredentials;Lcom/facebook/auth/credentials/DBLFacebookCredentials;I)V

    .line 2371681
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 2371677
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x64e8442f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2371660
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 2371661
    const v2, 0x7f0d0c1e

    if-ne v1, v2, :cond_2

    .line 2371662
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->d:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2371663
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2371664
    const v1, 0xae25900

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2371665
    :goto_0
    return-void

    .line 2371666
    :cond_0
    invoke-virtual {p0, v1}, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->c_(Ljava/lang/String;)V

    .line 2371667
    :cond_1
    :goto_1
    const v1, -0x3cfd5dc0

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0

    .line 2371668
    :cond_2
    const v2, 0x7f0d0c1f

    if-ne v1, v2, :cond_1

    .line 2371669
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->c:LX/10N;

    invoke-virtual {v1}, LX/10N;->i()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2371670
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2371671
    const-string v2, "dbl_flag"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 2371672
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->e:LX/2Ad;

    iget-object v3, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->k:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    invoke-interface {v2, v3, v1}, LX/2Ad;->a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;I)V

    goto :goto_1

    .line 2371673
    :cond_3
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->b:LX/2Lx;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "login/identify?ctx=recover&allow_token_login=1"

    invoke-virtual {v1, v2, v3}, LX/2Lx;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2371674
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "device_id"

    iget-object p1, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->a:LX/0dC;

    invoke-virtual {p1}, LX/0dC;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, v3, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 2371675
    move-object v1, v2

    .line 2371676
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->e:LX/2Ad;

    invoke-interface {v2, v1}, LX/2Ad;->a(Landroid/net/Uri;)V

    goto :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/16 v0, 0x2a

    const v1, -0x257ed45c

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2371640
    const v0, 0x7f0303e5

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2371641
    const v0, 0x7f0d0945

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->j:Landroid/widget/TextView;

    .line 2371642
    iget v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->h:I

    if-eqz v0, :cond_0

    .line 2371643
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->j:Landroid/widget/TextView;

    iget v3, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->h:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 2371644
    :cond_0
    const v0, 0x7f0d0575

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->l:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    .line 2371645
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->l:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    iget-object v3, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->k:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v3, v3, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mPicUrl:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->setImage(Ljava/lang/String;)V

    .line 2371646
    const v0, 0x7f0d0c15

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->d:Landroid/widget/EditText;

    .line 2371647
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->d:Landroid/widget/EditText;

    .line 2371648
    new-instance v3, LX/GcH;

    invoke-direct {v3, p0}, LX/GcH;-><init>(Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;)V

    move-object v3, v3

    .line 2371649
    invoke-virtual {v0, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2371650
    const v0, 0x7f0d0c1d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->m:Landroid/view/View;

    .line 2371651
    new-instance v0, LX/FAy;

    invoke-direct {v0}, LX/FAy;-><init>()V

    .line 2371652
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->d:Landroid/widget/EditText;

    const v5, 0x7f083492

    invoke-virtual {p0, v5}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, p0, v3, v4, v5}, LX/FAy;->a(LX/FAz;Landroid/content/Context;Landroid/widget/EditText;Ljava/lang/String;)V

    .line 2371653
    const v0, 0x7f0d0c1f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->i:Landroid/widget/TextView;

    .line 2371654
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2371655
    const v0, 0x7f0d04de

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->g:Landroid/widget/ProgressBar;

    .line 2371656
    const v0, 0x7f0d0c1e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->f:Landroid/widget/Button;

    .line 2371657
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->f:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2371658
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->f:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2371659
    const/16 v0, 0x2b

    const v3, 0x5f9f8e14

    invoke-static {v7, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onResume()V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5814f9c9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2371636
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2371637
    new-instance v4, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment$2;

    invoke-direct {v4, p0}, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment$2;-><init>(Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;)V

    .line 2371638
    iget-object v5, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->n:Landroid/os/Handler;

    const-wide/16 v6, 0x1f4

    const v8, -0x47c894a5

    invoke-static {v5, v4, v6, v7, v8}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2371639
    const/16 v1, 0x2b

    const v2, 0xdcceea8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x68b519d7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2371632
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2371633
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->d:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 2371634
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->d:Landroid/widget/EditText;

    invoke-static {v1, v2}, LX/2Na;->b(Landroid/content/Context;Landroid/view/View;)V

    .line 2371635
    const/16 v1, 0x2b

    const v2, -0x7a21ddb4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5a8143e1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2371629
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2371630
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2371631
    const/16 v1, 0x2b

    const v2, 0x2f170c38

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
