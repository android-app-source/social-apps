.class public Lcom/facebook/devicebasedlogin/ui/PinCodeView;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/resources/ui/FbEditText;

.field public b:Lcom/facebook/resources/ui/FbEditText;

.field public c:Lcom/facebook/resources/ui/FbEditText;

.field public d:Lcom/facebook/resources/ui/FbEditText;

.field public e:Landroid/widget/EditText;

.field public f:LX/GbD;

.field public g:Landroid/text/method/PasswordTransformationMethod;

.field public h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2372563
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2372564
    iput-boolean v3, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->h:Z

    .line 2372565
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 2372566
    const v1, 0x7f030f5f

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2372567
    invoke-virtual {p0, v3}, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2372568
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbEditText;

    iput-object v1, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->a:Lcom/facebook/resources/ui/FbEditText;

    .line 2372569
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbEditText;

    iput-object v1, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->b:Lcom/facebook/resources/ui/FbEditText;

    .line 2372570
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbEditText;

    iput-object v1, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->c:Lcom/facebook/resources/ui/FbEditText;

    .line 2372571
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->d:Lcom/facebook/resources/ui/FbEditText;

    .line 2372572
    invoke-virtual {p0, v2}, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->e:Landroid/widget/EditText;

    .line 2372573
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->e:Landroid/widget/EditText;

    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->e()Landroid/text/TextWatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2372574
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbEditText;->setActivated(Z)V

    .line 2372575
    new-instance v0, Landroid/text/method/PasswordTransformationMethod;

    invoke-direct {v0}, Landroid/text/method/PasswordTransformationMethod;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->g:Landroid/text/method/PasswordTransformationMethod;

    .line 2372576
    return-void
.end method

.method private e()Landroid/text/TextWatcher;
    .locals 1

    .prologue
    .line 2372577
    new-instance v0, LX/Gcm;

    invoke-direct {v0, p0}, LX/Gcm;-><init>(Lcom/facebook/devicebasedlogin/ui/PinCodeView;)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2372578
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->h:Z

    .line 2372579
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2372580
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->a:Lcom/facebook/resources/ui/FbEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2372581
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->b:Lcom/facebook/resources/ui/FbEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2372582
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->c:Lcom/facebook/resources/ui/FbEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2372583
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->d:Lcom/facebook/resources/ui/FbEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2372584
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->e:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2372585
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 2372586
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setVisibility(I)V

    .line 2372587
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->b:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setVisibility(I)V

    .line 2372588
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setVisibility(I)V

    .line 2372589
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setVisibility(I)V

    .line 2372590
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2372591
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setVisibility(I)V

    .line 2372592
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->b:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setVisibility(I)V

    .line 2372593
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setVisibility(I)V

    .line 2372594
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setVisibility(I)V

    .line 2372595
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->a:Lcom/facebook/resources/ui/FbEditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setActivated(Z)V

    .line 2372596
    return-void
.end method

.method public getEditText()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 2372597
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->e:Landroid/widget/EditText;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2372598
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setPasscodeViewListener(LX/GbD;)V
    .locals 0

    .prologue
    .line 2372599
    iput-object p1, p0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->f:LX/GbD;

    .line 2372600
    return-void
.end method
