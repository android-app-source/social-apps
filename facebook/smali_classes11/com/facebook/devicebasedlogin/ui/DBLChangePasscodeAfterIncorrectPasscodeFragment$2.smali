.class public final Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;)V
    .locals 0

    .prologue
    .line 2371545
    iput-object p1, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment$2;->a:Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2371546
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 2371547
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2371548
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment$2;->a:Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;

    iget-object v1, v1, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2371549
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment$2;->a:Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;

    iget-object v1, v1, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->g:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2371550
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment$2;->a:Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;

    iget-object v1, v1, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->b:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    invoke-virtual {v1, v0}, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2371551
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment$2;->a:Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;

    iget-object v1, v1, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2371552
    new-instance v1, LX/GcF;

    invoke-direct {v1, p0}, LX/GcF;-><init>(Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment$2;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2371553
    return-void
.end method
