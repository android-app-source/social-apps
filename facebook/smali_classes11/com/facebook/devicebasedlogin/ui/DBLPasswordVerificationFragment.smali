.class public Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/Gb4;
.implements LX/FAz;


# instance fields
.field public a:Landroid/widget/EditText;

.field public b:LX/Gbb;

.field private c:Landroid/widget/Button;

.field private d:Landroid/widget/ProgressBar;

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2371725
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 2371766
    iput p1, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->e:I

    .line 2371767
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2371768
    if-eqz v0, :cond_0

    .line 2371769
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2371770
    const v1, 0x7f0d0541

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2371771
    iget v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->e:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2371772
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2371762
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->a:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2371763
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->c:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 2371764
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->d:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2371765
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2371759
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->c:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 2371760
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->d:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2371761
    return-void
.end method

.method public final c_(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2371757
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->b:LX/Gbb;

    invoke-interface {v0, p1}, LX/Gbb;->c(Ljava/lang/String;)V

    .line 2371758
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2371773
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->d:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2371774
    return-void
.end method

.method public final d(I)V
    .locals 2

    .prologue
    .line 2371750
    iput p1, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->f:I

    .line 2371751
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2371752
    if-eqz v0, :cond_0

    .line 2371753
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2371754
    const v1, 0x7f0d0945

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2371755
    iget v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->f:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2371756
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x2a

    const v1, -0x34de720b    # -1.0587637E7f

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2371733
    const v0, 0x7f0303dd

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2371734
    iget v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->e:I

    if-eqz v0, :cond_0

    .line 2371735
    const v0, 0x7f0d0541

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2371736
    iget v3, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->e:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 2371737
    :cond_0
    iget v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->f:I

    if-eqz v0, :cond_1

    .line 2371738
    const v0, 0x7f0d0945

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2371739
    iget v3, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->f:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 2371740
    :cond_1
    const v0, 0x7f0d0c15

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->a:Landroid/widget/EditText;

    .line 2371741
    new-instance v0, LX/FAy;

    invoke-direct {v0}, LX/FAy;-><init>()V

    .line 2371742
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->a:Landroid/widget/EditText;

    const v5, 0x7f083492

    invoke-virtual {p0, v5}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, p0, v3, v4, v5}, LX/FAy;->a(LX/FAz;Landroid/content/Context;Landroid/widget/EditText;Ljava/lang/String;)V

    .line 2371743
    const v0, 0x7f0d04de

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->d:Landroid/widget/ProgressBar;

    .line 2371744
    const v0, 0x7f0d0c16

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->c:Landroid/widget/Button;

    .line 2371745
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->c:Landroid/widget/Button;

    .line 2371746
    new-instance v3, LX/GcJ;

    invoke-direct {v3, p0}, LX/GcJ;-><init>(Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;)V

    move-object v3, v3

    .line 2371747
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2371748
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2371749
    const/16 v0, 0x2b

    const v3, 0x12749301

    invoke-static {v6, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7b8d269

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2371729
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2371730
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 2371731
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->a:Landroid/widget/EditText;

    invoke-static {v1, v2}, LX/2Na;->b(Landroid/content/Context;Landroid/view/View;)V

    .line 2371732
    const/16 v1, 0x2b

    const v2, -0x512a26b6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1b1fb895

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2371726
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2371727
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2371728
    const/16 v1, 0x2b

    const v2, -0x48787b39

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
