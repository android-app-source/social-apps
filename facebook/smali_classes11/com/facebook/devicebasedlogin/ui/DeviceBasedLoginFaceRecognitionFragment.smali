.class public Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/Gb4;


# static fields
.field public static final d:Ljava/lang/String;


# instance fields
.field public a:LX/Gch;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

.field public f:LX/2Ad;

.field private g:Landroid/widget/ProgressBar;

.field private h:Lcom/facebook/resources/ui/FbButton;

.field private i:Lcom/facebook/resources/ui/FbButton;

.field public j:Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;

.field public k:LX/Gcg;

.field public volatile l:Z

.field private m:Lcom/facebook/auth/credentials/DBLFacebookCredentials;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2372112
    const-class v0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2372110
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2372111
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->l:Z

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;

    const-class v1, LX/Gch;

    invoke-interface {v3, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/Gch;

    const-class v2, LX/0i4;

    invoke-interface {v3, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/0i4;

    const/16 p0, 0x259

    invoke-static {v3, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    iput-object v1, p1, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->a:LX/Gch;

    iput-object v2, p1, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->b:LX/0i4;

    iput-object v3, p1, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->c:LX/0Ot;

    return-void
.end method

.method public static m(Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;)V
    .locals 6

    .prologue
    .line 2372108
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->f:LX/2Ad;

    new-instance v1, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->m:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v2, v2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->m:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v3, v3, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mNonce:Ljava/lang/String;

    const-string v4, ""

    sget-object v5, LX/41B;->DEVICE_BASED_LOGIN_TYPE:LX/41B;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/41B;)V

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->m:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    invoke-interface {v0, v1, v2}, LX/2Ad;->a(Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V

    .line 2372109
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 2372066
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2372067
    const-class v0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;

    invoke-static {v0, p0}, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2372068
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2372069
    const-string v1, "dbl_account_details"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->m:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 2372070
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->a:LX/Gch;

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->m:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v1, v1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/Gch;->a(Ljava/lang/String;)LX/Gcg;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->k:LX/Gcg;

    .line 2372071
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->k:LX/Gcg;

    .line 2372072
    iput-object p0, v0, LX/Gcg;->y:Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;

    .line 2372073
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->k:LX/Gcg;

    const/4 v11, 0x0

    .line 2372074
    invoke-static {}, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->newBuilder()LX/ANc;

    move-result-object v2

    sget-object v3, Lcom/facebook/cameracore/ui/CaptureType;->PHOTO:Lcom/facebook/cameracore/ui/CaptureType;

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 2372075
    iput-object v3, v2, LX/ANc;->d:LX/0Px;

    .line 2372076
    move-object v2, v2

    .line 2372077
    const/4 v3, 0x0

    .line 2372078
    iput-boolean v3, v2, LX/ANc;->h:Z

    .line 2372079
    move-object v2, v2

    .line 2372080
    const-string v3, "DeviceBasedLogin"

    .line 2372081
    iput-object v3, v2, LX/ANc;->f:Ljava/lang/String;

    .line 2372082
    move-object v2, v2

    .line 2372083
    iget-object v3, v0, LX/Gcg;->c:Ljava/lang/String;

    .line 2372084
    iput-object v3, v2, LX/ANc;->g:Ljava/lang/String;

    .line 2372085
    move-object v2, v2

    .line 2372086
    sget-object v3, LX/6JF;->FRONT:LX/6JF;

    .line 2372087
    iput-object v3, v2, LX/ANc;->b:LX/6JF;

    .line 2372088
    move-object v2, v2

    .line 2372089
    invoke-virtual {v2}, LX/ANc;->a()Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

    move-result-object v2

    iput-object v2, v0, LX/Gcg;->q:Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

    .line 2372090
    iget-object v2, v0, LX/Gcg;->h:LX/6KO;

    iget-object v3, v0, LX/Gcg;->q:Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

    .line 2372091
    iget-object v4, v3, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->e:Ljava/lang/String;

    move-object v3, v4

    .line 2372092
    iget-object v4, v0, LX/Gcg;->q:Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

    .line 2372093
    iget-object v5, v4, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->f:Ljava/lang/String;

    move-object v4, v5

    .line 2372094
    invoke-virtual {v2, v3, v4}, LX/6KO;->a(Ljava/lang/String;Ljava/lang/String;)LX/6KN;

    move-result-object v2

    iput-object v2, v0, LX/Gcg;->n:LX/6KN;

    .line 2372095
    iget-object v2, v0, LX/Gcg;->i:LX/6KT;

    invoke-static {v2}, LX/6KD;->a(LX/6KT;)LX/6KD;

    move-result-object v2

    invoke-virtual {v2}, LX/6KD;->a()LX/6KB;

    move-result-object v2

    iput-object v2, v0, LX/Gcg;->r:LX/6KB;

    .line 2372096
    iget-object v2, v0, LX/Gcg;->q:Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

    .line 2372097
    iget-object v3, v2, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->a:LX/6JF;

    move-object v2, v3

    .line 2372098
    iput-object v2, v0, LX/Gcg;->p:LX/6JF;

    .line 2372099
    sget-object v2, LX/Gcg;->b:LX/6Jt;

    if-nez v2, :cond_0

    .line 2372100
    new-instance v2, LX/6Jt;

    iget-object v3, v0, LX/Gcg;->d:LX/0Zr;

    iget-object v4, v0, LX/Gcg;->e:Landroid/content/res/Resources;

    iget-object v5, v0, LX/Gcg;->g:Landroid/os/Handler;

    iget-object v6, v0, LX/Gcg;->f:Ljava/util/concurrent/ExecutorService;

    iget-object v7, v0, LX/Gcg;->n:LX/6KN;

    iget-object v8, v0, LX/Gcg;->r:LX/6KB;

    invoke-static {v0}, LX/Gcg;->e(LX/Gcg;)I

    move-result v9

    .line 2372101
    new-instance v10, LX/Gcd;

    invoke-direct {v10, v0}, LX/Gcd;-><init>(LX/Gcg;)V

    move-object v10, v10

    .line 2372102
    invoke-direct/range {v2 .. v10}, LX/6Jt;-><init>(LX/0Zr;Landroid/content/res/Resources;Landroid/os/Handler;Ljava/util/concurrent/ExecutorService;LX/6KN;LX/6KB;ILX/6Jp;)V

    sput-object v2, LX/Gcg;->b:LX/6Jt;

    .line 2372103
    :cond_0
    iget-object v2, v0, LX/Gcg;->j:Landroid/content/Context;

    iget-object v3, v0, LX/Gcg;->p:LX/6JF;

    iget-object v4, v0, LX/Gcg;->n:LX/6KN;

    invoke-static {v0}, LX/Gcg;->g(LX/Gcg;)LX/6J9;

    move-result-object v5

    iget-object v6, v0, LX/Gcg;->r:LX/6KB;

    .line 2372104
    iget-object v7, v6, LX/6KB;->a:LX/6KF;

    move-object v6, v7

    .line 2372105
    invoke-static {v2, v3, v4, v5, v6}, LX/6J7;->a(Landroid/content/Context;LX/6JF;LX/6KN;LX/6J9;LX/6KF;)LX/6Ia;

    move-result-object v2

    iput-object v2, v0, LX/Gcg;->o:LX/6Ia;

    .line 2372106
    new-instance v2, LX/6JC;

    invoke-direct {v2, v11, v11}, LX/6JC;-><init>(LX/6JN;LX/6JO;)V

    iput-object v2, v0, LX/Gcg;->u:LX/6JC;

    .line 2372107
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2372062
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->g:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2372063
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->h:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2372064
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->i:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2372065
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2372056
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->g:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2372057
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->e:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    invoke-virtual {v0, v2}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->setVisibility(I)V

    .line 2372058
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->h:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2372059
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->i:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2372060
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->j:Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;

    invoke-virtual {v0, v2}, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->setVisibility(I)V

    .line 2372061
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 2372055
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x393471d3

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2372044
    const v0, 0x7f0303de

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2372045
    const v0, 0x7f0d0681

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->e:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    .line 2372046
    const v0, 0x7f0d04de

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->g:Landroid/widget/ProgressBar;

    .line 2372047
    const v0, 0x7f0d0c17

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->h:Lcom/facebook/resources/ui/FbButton;

    .line 2372048
    const v0, 0x7f0d0c18

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->i:Lcom/facebook/resources/ui/FbButton;

    .line 2372049
    const v0, 0x7f0d0c19

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->j:Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;

    .line 2372050
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->h:Lcom/facebook/resources/ui/FbButton;

    new-instance v3, LX/GcW;

    invoke-direct {v3, p0}, LX/GcW;-><init>(Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2372051
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->i:Lcom/facebook/resources/ui/FbButton;

    new-instance v3, LX/GcZ;

    invoke-direct {v3, p0, p1}, LX/GcZ;-><init>(Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;Landroid/view/LayoutInflater;)V

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2372052
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->b:LX/0i4;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    .line 2372053
    const-string v3, "android.permission.CAMERA"

    new-instance p1, LX/Gca;

    invoke-direct {p1, p0}, LX/Gca;-><init>(Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;)V

    invoke-virtual {v0, v3, p1}, LX/0i5;->a(Ljava/lang/String;LX/6Zx;)V

    .line 2372054
    const/16 v0, 0x2b

    const v3, 0x4338657c

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x630eb258

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2372041
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2372042
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->k:LX/Gcg;

    invoke-virtual {v1}, LX/Gcg;->b()V

    .line 2372043
    const/16 v1, 0x2b

    const v2, 0x8273e66

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
