.class public Lcom/facebook/devicebasedlogin/ui/LoginApprovalsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements LX/Gb4;
.implements LX/FAz;


# instance fields
.field private a:Landroid/widget/EditText;

.field public b:LX/2Ad;

.field private c:Landroid/widget/Button;

.field private d:Landroid/widget/ProgressBar;

.field private e:Lcom/facebook/auth/credentials/DBLFacebookCredentials;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2372412
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2372413
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2372414
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2372415
    const-string v1, "dbl_account_details"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/LoginApprovalsFragment;->e:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 2372416
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2372417
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/LoginApprovalsFragment;->c:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 2372418
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/LoginApprovalsFragment;->a:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2372419
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/LoginApprovalsFragment;->a:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 2372420
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/LoginApprovalsFragment;->d:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2372421
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2372422
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/LoginApprovalsFragment;->c:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 2372423
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/LoginApprovalsFragment;->a:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 2372424
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/LoginApprovalsFragment;->d:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2372425
    return-void
.end method

.method public final c_(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2372426
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2372427
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2372428
    const-string v1, "dbl_flag"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 2372429
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/LoginApprovalsFragment;->b:LX/2Ad;

    new-instance v2, Lcom/facebook/auth/credentials/PasswordCredentials;

    iget-object v3, p0, Lcom/facebook/devicebasedlogin/ui/LoginApprovalsFragment;->e:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v3, v3, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    sget-object v4, LX/28K;->PASSWORD:LX/28K;

    invoke-direct {v2, v3, p1, v4}, Lcom/facebook/auth/credentials/PasswordCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;LX/28K;)V

    iget-object v3, p0, Lcom/facebook/devicebasedlogin/ui/LoginApprovalsFragment;->e:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    invoke-interface {v1, v2, v3, v0}, LX/2Ad;->a(Lcom/facebook/auth/credentials/PasswordCredentials;Lcom/facebook/auth/credentials/DBLFacebookCredentials;I)V

    .line 2372430
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2372431
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/LoginApprovalsFragment;->d:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2372432
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x4dedca8f    # 4.98684384E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2372433
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 2372434
    const v2, 0x7f0d10af

    if-ne v1, v2, :cond_1

    .line 2372435
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/LoginApprovalsFragment;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2372436
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2372437
    const v1, 0x3e52f9a0

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2372438
    :goto_0
    return-void

    .line 2372439
    :cond_0
    invoke-virtual {p0, v1}, Lcom/facebook/devicebasedlogin/ui/LoginApprovalsFragment;->c_(Ljava/lang/String;)V

    .line 2372440
    :cond_1
    const v1, -0x2b763aa5

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x2a

    const v1, -0x31416072

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2372441
    const v0, 0x7f030a57

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2372442
    const v0, 0x7f0d0955

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/LoginApprovalsFragment;->a:Landroid/widget/EditText;

    .line 2372443
    new-instance v0, LX/FAy;

    invoke-direct {v0}, LX/FAy;-><init>()V

    .line 2372444
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/devicebasedlogin/ui/LoginApprovalsFragment;->a:Landroid/widget/EditText;

    const v5, 0x7f083491

    invoke-virtual {p0, v5}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, p0, v3, v4, v5}, LX/FAy;->a(LX/FAz;Landroid/content/Context;Landroid/widget/EditText;Ljava/lang/String;)V

    .line 2372445
    const v0, 0x7f0d04de

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/LoginApprovalsFragment;->d:Landroid/widget/ProgressBar;

    .line 2372446
    const v0, 0x7f0d10af

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/LoginApprovalsFragment;->c:Landroid/widget/Button;

    .line 2372447
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/LoginApprovalsFragment;->c:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2372448
    const/16 v0, 0x2b

    const v3, -0x5e2e2cb6

    invoke-static {v6, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method
