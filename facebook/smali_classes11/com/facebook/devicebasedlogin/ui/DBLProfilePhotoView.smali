.class public Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:Lcom/facebook/drawee/view/DraweeView;

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 2371884
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2371885
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "dbl"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 2371886
    invoke-direct {p0, p1, p2}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2371887
    return-void
.end method

.method private final a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2371888
    const-class v0, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    invoke-static {v0, p0}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2371889
    const v0, 0x7f0303e6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2371890
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->setOrientation(I)V

    .line 2371891
    invoke-static {p1, p2, v4}, LX/4oN;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)LX/4oN;

    move-result-object v1

    .line 2371892
    const v0, 0x7f0d0c20

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/DraweeView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->a:Lcom/facebook/drawee/view/DraweeView;

    .line 2371893
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02151a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sget-object v3, LX/1Up;->a:LX/1Up;

    invoke-virtual {v0, v2, v3}, LX/1Uo;->a(Landroid/graphics/drawable/Drawable;LX/1Up;)LX/1Uo;

    move-result-object v0

    .line 2371894
    iget v2, v1, LX/4oN;->f:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 2371895
    iget v1, v1, LX/4oN;->f:F

    invoke-static {v1}, LX/4Ab;->b(F)LX/4Ab;

    move-result-object v1

    .line 2371896
    iput-object v1, v0, LX/1Uo;->u:LX/4Ab;

    .line 2371897
    :cond_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2371898
    sget-object v1, LX/03r;->DBLOverlayParams:[I

    const/4 v2, 0x0

    invoke-virtual {p1, p2, v1, v4, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 2371899
    new-instance v2, LX/GcG;

    const/16 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-direct {v2, v3}, LX/GcG;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 2371900
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 2371901
    move-object v1, v2

    .line 2371902
    iget-object v2, v1, LX/GcG;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_1

    .line 2371903
    iget-object v1, v1, LX/GcG;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, LX/1Uo;->f(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    .line 2371904
    :cond_1
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->a:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2371905
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    const/16 v1, 0x509

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->b:LX/0Or;

    return-void
.end method


# virtual methods
.method public setImage(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2371906
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2371907
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->a:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2371908
    return-void
.end method

.method public setImageRequest(LX/1bf;)V
    .locals 2

    .prologue
    .line 2371909
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2371910
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->a:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2371911
    return-void
.end method
