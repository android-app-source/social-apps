.class public Lcom/facebook/devicebasedlogin/ui/DBLSetNewPinFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/Gb4;
.implements LX/GbD;


# instance fields
.field public a:LX/Gbb;

.field private b:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

.field private c:Landroid/widget/ProgressBar;

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2371957
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 2371912
    iput p1, p0, Lcom/facebook/devicebasedlogin/ui/DBLSetNewPinFragment;->d:I

    .line 2371913
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2371914
    if-eqz v0, :cond_0

    .line 2371915
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2371916
    const v1, 0x7f0d0541

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2371917
    iget v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLSetNewPinFragment;->d:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2371918
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2371954
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLSetNewPinFragment;->c:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2371955
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLSetNewPinFragment;->b:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    invoke-virtual {v0}, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->b()V

    .line 2371956
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2371951
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLSetNewPinFragment;->a:LX/Gbb;

    if-eqz v0, :cond_0

    .line 2371952
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLSetNewPinFragment;->a:LX/Gbb;

    invoke-interface {v0, p1}, LX/Gbb;->a(Ljava/lang/String;)V

    .line 2371953
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2371949
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLSetNewPinFragment;->c:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2371950
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2371958
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLSetNewPinFragment;->c:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2371959
    return-void
.end method

.method public final d(I)V
    .locals 2

    .prologue
    .line 2371942
    iput p1, p0, Lcom/facebook/devicebasedlogin/ui/DBLSetNewPinFragment;->e:I

    .line 2371943
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2371944
    if-eqz v0, :cond_0

    .line 2371945
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2371946
    const v1, 0x7f0d0945

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2371947
    iget v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLSetNewPinFragment;->e:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2371948
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x14844c93

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2371930
    const v0, 0x7f0303dc

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2371931
    iget v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLSetNewPinFragment;->d:I

    if-eqz v0, :cond_0

    .line 2371932
    const v0, 0x7f0d0541

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2371933
    iget v3, p0, Lcom/facebook/devicebasedlogin/ui/DBLSetNewPinFragment;->d:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 2371934
    :cond_0
    iget v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLSetNewPinFragment;->e:I

    if-eqz v0, :cond_1

    .line 2371935
    const v0, 0x7f0d0945

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2371936
    iget v3, p0, Lcom/facebook/devicebasedlogin/ui/DBLSetNewPinFragment;->e:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 2371937
    :cond_1
    const v0, 0x7f0d02d5

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLSetNewPinFragment;->b:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    .line 2371938
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLSetNewPinFragment;->b:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    .line 2371939
    iput-object p0, v0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->f:LX/GbD;

    .line 2371940
    const v0, 0x7f0d04de

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLSetNewPinFragment;->c:Landroid/widget/ProgressBar;

    .line 2371941
    const/16 v0, 0x2b

    const v3, -0x7e341c80

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x112871d2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2371922
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2371923
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLSetNewPinFragment;->b:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    .line 2371924
    iget-object v2, v1, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->e:Landroid/widget/EditText;

    move-object v1, v2

    .line 2371925
    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 2371926
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DBLSetNewPinFragment;->b:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    .line 2371927
    iget-object p0, v2, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->e:Landroid/widget/EditText;

    move-object v2, p0

    .line 2371928
    invoke-static {v1, v2}, LX/2Na;->b(Landroid/content/Context;Landroid/view/View;)V

    .line 2371929
    const/16 v1, 0x2b

    const v2, 0x55c4007e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x67bdec96

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2371919
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2371920
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2371921
    const/16 v1, 0x2b

    const v2, -0x7c8336d8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
