.class public final Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/nio/ByteBuffer;

.field public final synthetic b:LX/Gcg;


# direct methods
.method public constructor <init>(LX/Gcg;Ljava/nio/ByteBuffer;)V
    .locals 0

    .prologue
    .line 2372142
    iput-object p1, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;->b:LX/Gcg;

    iput-object p2, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;->a:Ljava/nio/ByteBuffer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 2372143
    :try_start_0
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;->b:LX/Gcg;

    iget-object v0, v0, LX/Gcg;->l:Lcom/facebook/facedetection/detector/MacerFaceDetector;

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;->a:Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;->b:LX/Gcg;

    iget-object v2, v2, LX/Gcg;->t:LX/6JR;

    iget v2, v2, LX/6JR;->a:I

    iget-object v3, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;->b:LX/Gcg;

    iget-object v3, v3, LX/Gcg;->t:LX/6JR;

    iget v3, v3, LX/6JR;->b:I

    iget-object v4, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;->b:LX/Gcg;

    const/4 v7, -0x1

    .line 2372144
    const/16 v5, 0x10e

    .line 2372145
    iget v6, v4, LX/Gcg;->C:I

    if-ne v6, v7, :cond_0

    iget v6, v4, LX/Gcg;->D:I

    if-eq v6, v7, :cond_1

    .line 2372146
    :cond_0
    iget v5, v4, LX/Gcg;->D:I

    iget v6, v4, LX/Gcg;->C:I

    sub-int/2addr v5, v6

    add-int/lit16 v5, v5, 0x168

    rem-int/lit16 v5, v5, 0x168

    .line 2372147
    :cond_1
    move v4, v5

    .line 2372148
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/facebook/facedetection/detector/MacerFaceDetector;->a(Ljava/nio/ByteBuffer;III)[B

    move-result-object v0

    .line 2372149
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 2372150
    invoke-static {v0}, LX/7yS;->a(Ljava/nio/ByteBuffer;)LX/7yS;

    move-result-object v0

    .line 2372151
    invoke-virtual {v0}, LX/7yS;->a()I

    move-result v1

    if-lez v1, :cond_5

    .line 2372152
    new-instance v2, LX/7yR;

    invoke-direct {v2}, LX/7yR;-><init>()V

    .line 2372153
    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, LX/7yS;->a(LX/7yR;I)LX/7yR;

    .line 2372154
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;->b:LX/Gcg;

    iget-object v0, v0, LX/Gcg;->y:Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;

    if-eqz v0, :cond_2

    .line 2372155
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;->b:LX/Gcg;

    iget-object v0, v0, LX/Gcg;->y:Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;

    .line 2372156
    iget-object v1, v0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->j:Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;

    iget-object v3, v0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->e:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    invoke-virtual {v3}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->getWidth()I

    move-result v3

    iget-object v4, v0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->e:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    invoke-virtual {v4}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->getHeight()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->a(LX/7yR;II)V

    .line 2372157
    :cond_2
    new-instance v7, LX/Gck;

    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;->b:LX/Gcg;

    iget-object v0, v0, LX/Gcg;->v:LX/7yL;

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;->b:LX/Gcg;

    iget-object v3, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;->b:LX/Gcg;

    iget-object v3, v3, LX/Gcg;->x:LX/1FZ;

    invoke-direct {v7, v0, v1, v3}, LX/Gck;-><init>(LX/7yL;LX/Gcg;LX/1FZ;)V

    .line 2372158
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;->b:LX/Gcg;

    iget-object v0, v0, LX/Gcg;->t:LX/6JR;

    iget v0, v0, LX/6JR;->a:I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;->b:LX/Gcg;

    iget-object v0, v0, LX/Gcg;->t:LX/6JR;

    iget v0, v0, LX/6JR;->b:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_4

    .line 2372159
    :cond_3
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;->b:LX/Gcg;

    iget-object v0, v0, LX/Gcg;->B:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 2372160
    :goto_0
    return-void

    .line 2372161
    :cond_4
    :try_start_1
    new-instance v0, LX/Gcj;

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;->b:LX/Gcg;

    iget-object v1, v1, LX/Gcg;->z:Ljava/nio/ByteBuffer;

    iget-object v3, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;->b:LX/Gcg;

    iget v3, v3, LX/Gcg;->D:I

    iget-object v4, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;->b:LX/Gcg;

    iget-object v4, v4, LX/Gcg;->t:LX/6JR;

    iget v4, v4, LX/6JR;->a:I

    iget-object v5, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;->b:LX/Gcg;

    iget-object v5, v5, LX/Gcg;->t:LX/6JR;

    iget v5, v5, LX/6JR;->b:I

    iget-object v6, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;->b:LX/Gcg;

    iget-object v6, v6, LX/Gcg;->l:Lcom/facebook/facedetection/detector/MacerFaceDetector;

    .line 2372162
    iget-object v8, v6, Lcom/facebook/facedetection/detector/MacerFaceDetector;->a:LX/7yM;

    move-object v6, v8

    .line 2372163
    invoke-virtual {v6}, LX/7yM;->c()LX/7yP;

    move-result-object v6

    invoke-virtual {v6}, LX/7yP;->c()LX/7yQ;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/Gcj;-><init>(Ljava/nio/ByteBuffer;LX/7yR;IIILX/7yQ;)V

    .line 2372164
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;->b:LX/Gcg;

    iget-object v1, v1, LX/Gcg;->j:Landroid/content/Context;

    const/4 v2, 0x1

    new-array v2, v2, [LX/Gcj;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v7, v1, v2}, LX/3nE;->a(Landroid/content/Context;[Ljava/lang/Object;)LX/3nE;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2372165
    :cond_5
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;->b:LX/Gcg;

    iget-object v0, v0, LX/Gcg;->B:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 2372166
    :catch_0
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;->b:LX/Gcg;

    iget-object v0, v0, LX/Gcg;->B:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;->b:LX/Gcg;

    iget-object v1, v1, LX/Gcg;->B:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
