.class public Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0l6;
.implements LX/Gb4;
.implements LX/GbD;
.implements LX/2JL;


# instance fields
.field public a:LX/Gbb;

.field public b:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

.field private c:Landroid/widget/ProgressBar;

.field public d:I

.field public e:I

.field public f:Landroid/widget/TextView;

.field public g:Landroid/view/View;

.field public final h:Landroid/os/Handler;

.field public i:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2371591
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2371592
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->h:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2371586
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->c:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2371587
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2371588
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2371589
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->b:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    invoke-virtual {v0}, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->b()V

    .line 2371590
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2371584
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->a:LX/Gbb;

    invoke-interface {v0, p1}, LX/Gbb;->a(Ljava/lang/String;)V

    .line 2371585
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2371593
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->c:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2371594
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2371595
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2371596
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 2371583
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x4e9272cf

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2371564
    const v0, 0x7f0303e1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2371565
    const v0, 0x7f0d02c3

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->i:Landroid/widget/TextView;

    .line 2371566
    iget v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->d:I

    if-eqz v0, :cond_0

    .line 2371567
    const v0, 0x7f0d0541

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2371568
    iget v3, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->d:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 2371569
    :cond_0
    iget v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->e:I

    if-eqz v0, :cond_1

    .line 2371570
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->i:Landroid/widget/TextView;

    iget v3, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->e:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 2371571
    :cond_1
    const v0, 0x7f0d0c1b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->b:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    .line 2371572
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->b:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    .line 2371573
    iput-object p0, v0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->f:LX/GbD;

    .line 2371574
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->b:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    invoke-virtual {v0}, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->c()V

    .line 2371575
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->b:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    invoke-virtual {v0}, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->b()V

    .line 2371576
    const v0, 0x7f0d04de

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->c:Landroid/widget/ProgressBar;

    .line 2371577
    const v0, 0x7f0d07fd

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->g:Landroid/view/View;

    .line 2371578
    const v0, 0x7f0d0c1c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->f:Landroid/widget/TextView;

    .line 2371579
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->f:Landroid/widget/TextView;

    .line 2371580
    new-instance v3, LX/GcE;

    invoke-direct {v3, p0}, LX/GcE;-><init>(Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;)V

    move-object v3, v3

    .line 2371581
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2371582
    const/16 v0, 0x2b

    const v3, -0x24e44dd3

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onResume()V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7fe705fa

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2371560
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2371561
    new-instance v4, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment$2;

    invoke-direct {v4, p0}, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment$2;-><init>(Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;)V

    .line 2371562
    iget-object v5, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->h:Landroid/os/Handler;

    const-wide/16 v6, 0x1f4

    const v8, 0x26be81be

    invoke-static {v5, v4, v6, v7, v8}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2371563
    const/16 v1, 0x2b

    const v2, -0x25167139

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5fc698b9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2371554
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2371555
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->b:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    .line 2371556
    iget-object v2, v1, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->e:Landroid/widget/EditText;

    move-object v1, v2

    .line 2371557
    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 2371558
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->b:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    invoke-static {v1, v2}, LX/2Na;->b(Landroid/content/Context;Landroid/view/View;)V

    .line 2371559
    const/16 v1, 0x2b

    const v2, 0x1429333f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
