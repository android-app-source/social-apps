.class public Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/Gb4;
.implements LX/GbD;


# instance fields
.field public a:Landroid/widget/TextView;

.field public b:LX/Gbb;

.field private c:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

.field private d:Landroid/widget/ProgressBar;

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2372009
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 2371964
    iput p1, p0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->e:I

    .line 2371965
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2371966
    if-eqz v0, :cond_0

    .line 2371967
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2371968
    const v1, 0x7f0d0541

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2371969
    iget v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->e:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2371970
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2372006
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->d:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2372007
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->c:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    invoke-virtual {v0}, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->b()V

    .line 2372008
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2372004
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->b:LX/Gbb;

    invoke-interface {v0, p1}, LX/Gbb;->b(Ljava/lang/String;)V

    .line 2372005
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2372002
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->d:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2372003
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2372000
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->d:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2372001
    return-void
.end method

.method public final d(I)V
    .locals 2

    .prologue
    .line 2371996
    iput p1, p0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->f:I

    .line 2371997
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->a:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 2371998
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->a:Landroid/widget/TextView;

    iget v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->f:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2371999
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x7b1313d0

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2371982
    const v0, 0x7f0303dc

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2371983
    const v0, 0x7f0d0945

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->a:Landroid/widget/TextView;

    .line 2371984
    iget v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->e:I

    if-eqz v0, :cond_0

    .line 2371985
    const v0, 0x7f0d0541

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2371986
    iget v3, p0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->e:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 2371987
    :cond_0
    iget v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->f:I

    if-eqz v0, :cond_1

    .line 2371988
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->a:Landroid/widget/TextView;

    iget v3, p0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->f:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 2371989
    :cond_1
    const v0, 0x7f0d02d5

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->c:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    .line 2371990
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->c:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    .line 2371991
    iput-object p0, v0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->f:LX/GbD;

    .line 2371992
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->c:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    invoke-virtual {v0}, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->a()V

    .line 2371993
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->c:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    invoke-virtual {v0}, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->b()V

    .line 2371994
    const v0, 0x7f0d04de

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->d:Landroid/widget/ProgressBar;

    .line 2371995
    const/16 v0, 0x2b

    const v3, 0x55ca9c2c

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x53c73edf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2371974
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2371975
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->c:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    .line 2371976
    iget-object v2, v1, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->e:Landroid/widget/EditText;

    move-object v1, v2

    .line 2371977
    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 2371978
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->c:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    .line 2371979
    iget-object p0, v2, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->e:Landroid/widget/EditText;

    move-object v2, p0

    .line 2371980
    invoke-static {v1, v2}, LX/2Na;->b(Landroid/content/Context;Landroid/view/View;)V

    .line 2371981
    const/16 v1, 0x2b

    const v2, -0x49d7cc89

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3ff877d6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2371971
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2371972
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2371973
    const/16 v1, 0x2b

    const v2, -0x24a47dfc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
