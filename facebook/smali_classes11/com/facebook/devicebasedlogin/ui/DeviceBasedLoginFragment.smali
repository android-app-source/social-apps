.class public Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0l6;
.implements LX/Gb4;
.implements LX/Gci;
.implements LX/2JL;


# instance fields
.field public a:LX/HeS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

.field public c:LX/2Ad;

.field public d:Lcom/facebook/devicebasedlogin/ui/NumPadView;

.field private e:Landroid/widget/ProgressBar;

.field public f:Landroid/view/View;

.field public g:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

.field public h:I

.field private i:LX/HeV;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2372327
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2372328
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->h:I

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;

    new-instance v0, LX/HeS;

    invoke-static {v1}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object p0

    check-cast p0, LX/16I;

    invoke-direct {v0, p0}, LX/HeS;-><init>(LX/16I;)V

    move-object v1, v0

    check-cast v1, LX/HeS;

    iput-object v1, p1, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->a:LX/HeS;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2372322
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2372323
    const-class v0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;

    invoke-static {v0, p0}, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2372324
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2372325
    const-string v1, "dbl_account_details"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->b:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 2372326
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2372315
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->d:Lcom/facebook/devicebasedlogin/ui/NumPadView;

    invoke-virtual {v0}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->a()V

    .line 2372316
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->g:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    invoke-virtual {v0, v2}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->setVisibility(I)V

    .line 2372317
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2372318
    const v1, 0x7f0d02c4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2372319
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->e:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2372320
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->d:Lcom/facebook/devicebasedlogin/ui/NumPadView;

    invoke-virtual {v0, v2}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->setVisibility(I)V

    .line 2372321
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2372312
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->c:LX/2Ad;

    if-eqz v0, :cond_0

    .line 2372313
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->c:LX/2Ad;

    new-instance v1, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->b:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v2, v2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->b:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v3, v3, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mNonce:Ljava/lang/String;

    sget-object v4, LX/41B;->DEVICE_BASED_LOGIN_TYPE:LX/41B;

    invoke-direct {v1, v2, v3, p1, v4}, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/41B;)V

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->b:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    invoke-interface {v0, v1, v2}, LX/2Ad;->a(Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V

    .line 2372314
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 2372306
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->g:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    invoke-virtual {v0, v2}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->setVisibility(I)V

    .line 2372307
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2372308
    const v1, 0x7f0d02c4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2372309
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->e:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2372310
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->d:Lcom/facebook/devicebasedlogin/ui/NumPadView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->setVisibility(I)V

    .line 2372311
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 2372305
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/16 v0, 0x2a

    const v1, 0x499bae20    # 1275332.0f

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2372286
    const v0, 0x7f03040c

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->f:Landroid/view/View;

    .line 2372287
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->f:Landroid/view/View;

    const v2, 0x7f0d0c61

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/devicebasedlogin/ui/NumPadView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->d:Lcom/facebook/devicebasedlogin/ui/NumPadView;

    .line 2372288
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->d:Lcom/facebook/devicebasedlogin/ui/NumPadView;

    .line 2372289
    iput-object p0, v0, Lcom/facebook/devicebasedlogin/ui/NumPadView;->s:LX/Gci;

    .line 2372290
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->f:Landroid/view/View;

    const v2, 0x7f0d04de

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->e:Landroid/widget/ProgressBar;

    .line 2372291
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->f:Landroid/view/View;

    const v2, 0x7f0d0575

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->g:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    .line 2372292
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->g:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->b:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v2, v2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mPicUrl:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->setImage(Ljava/lang/String;)V

    .line 2372293
    iget v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->h:I

    if-eqz v0, :cond_0

    .line 2372294
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->f:Landroid/view/View;

    const v2, 0x7f0d02c4

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2372295
    iget v2, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->h:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 2372296
    :cond_0
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->f:Landroid/view/View;

    const v2, 0x7f0d0c60

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/errors/ErrorBannerViewStub;

    .line 2372297
    invoke-virtual {v0}, LX/4nv;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/HeV;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->i:LX/HeV;

    .line 2372298
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->a:LX/HeS;

    const/4 v2, 0x1

    new-array v2, v2, [LX/46O;

    iget-object v3, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->i:LX/HeV;

    aput-object v3, v2, v4

    .line 2372299
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, v0, LX/HeS;->b:Ljava/util/List;

    .line 2372300
    iput-object v2, v0, LX/HeS;->c:[LX/46O;

    .line 2372301
    iget-object v3, v0, LX/HeS;->b:Ljava/util/List;

    iget-object v4, v0, LX/HeS;->a:LX/16I;

    sget-object p1, LX/1Ed;->CONNECTED:LX/1Ed;

    new-instance p2, Lcom/facebook/ui/errors/ConnectivityBannerController$1;

    invoke-direct {p2, v0}, Lcom/facebook/ui/errors/ConnectivityBannerController$1;-><init>(LX/HeS;)V

    invoke-virtual {v4, p1, p2}, LX/16I;->a(LX/1Ed;Ljava/lang/Runnable;)LX/0Yb;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2372302
    iget-object v3, v0, LX/HeS;->b:Ljava/util/List;

    iget-object v4, v0, LX/HeS;->a:LX/16I;

    sget-object p1, LX/1Ed;->NO_INTERNET:LX/1Ed;

    new-instance p2, Lcom/facebook/ui/errors/ConnectivityBannerController$2;

    invoke-direct {p2, v0}, Lcom/facebook/ui/errors/ConnectivityBannerController$2;-><init>(LX/HeS;)V

    invoke-virtual {v4, p1, p2}, LX/16I;->a(LX/1Ed;Ljava/lang/Runnable;)LX/0Yb;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2372303
    invoke-virtual {v0}, LX/HeS;->b()V

    .line 2372304
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->f:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, 0x640f9bc6

    invoke-static {v5, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onDestroy()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x53e9c36

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2372253
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->a:LX/HeS;

    if-eqz v1, :cond_2

    .line 2372254
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->a:LX/HeS;

    const/4 v5, 0x0

    .line 2372255
    iget-object v2, v1, LX/HeS;->b:Ljava/util/List;

    if-eqz v2, :cond_1

    .line 2372256
    const/4 v2, 0x0

    move v4, v2

    :goto_0
    iget-object v2, v1, LX/HeS;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v4, v2, :cond_0

    .line 2372257
    iget-object v2, v1, LX/HeS;->b:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Yb;

    invoke-virtual {v2}, LX/0Yb;->c()V

    .line 2372258
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 2372259
    :cond_0
    iget-object v2, v1, LX/HeS;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2372260
    iput-object v5, v1, LX/HeS;->b:Ljava/util/List;

    .line 2372261
    :cond_1
    iput-object v5, v1, LX/HeS;->c:[LX/46O;

    .line 2372262
    :cond_2
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2372263
    const/16 v1, 0x2b

    const v2, 0x3b478480

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7dd1c7c8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2372278
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2372279
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->a:LX/HeS;

    invoke-virtual {v1}, LX/HeS;->b()V

    .line 2372280
    new-instance v4, Landroid/view/animation/AlphaAnimation;

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-direct {v4, v5, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 2372281
    const-wide/16 v6, 0x258

    invoke-virtual {v4, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2372282
    iget-object v5, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->f:Landroid/view/View;

    const v6, 0x7f0d02c4

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2372283
    iget-object v4, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->d:Lcom/facebook/devicebasedlogin/ui/NumPadView;

    invoke-virtual {v4}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->b()V

    .line 2372284
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->d:Lcom/facebook/devicebasedlogin/ui/NumPadView;

    invoke-virtual {v1}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->a()V

    .line 2372285
    const/16 v1, 0x2b

    const v2, -0x771f03d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x38bd1d5c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2372264
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2372265
    const/high16 v5, 0x3f000000    # 0.5f

    .line 2372266
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 2372267
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 2372268
    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 2372269
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    .line 2372270
    const/high16 v4, 0x43c50000    # 394.0f

    mul-float/2addr v4, v2

    add-float/2addr v4, v5

    float-to-int v4, v4

    .line 2372271
    sub-int v4, v1, v4

    .line 2372272
    const/high16 v1, 0x41a00000    # 20.0f

    mul-float/2addr v1, v2

    add-float/2addr v1, v5

    float-to-int v2, v1

    .line 2372273
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->g:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    invoke-virtual {v1}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2372274
    div-int/lit8 v5, v4, 0x2

    const/4 v6, 0x0

    invoke-virtual {v1, v2, v5, v2, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2372275
    iget-object v5, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->g:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    invoke-virtual {v5, v1}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2372276
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->d:Lcom/facebook/devicebasedlogin/ui/NumPadView;

    div-int/lit8 v5, v2, 0x2

    div-int/lit8 v6, v4, 0x4

    div-int/lit8 v2, v2, 0x2

    div-int/lit8 v4, v4, 0x4

    invoke-virtual {v1, v5, v6, v2, v4}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->a(IIII)V

    .line 2372277
    const/16 v1, 0x2b

    const v2, 0x2821cb4c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
