.class public final Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;)V
    .locals 0

    .prologue
    .line 2371617
    iput-object p1, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment$2;->a:Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2371618
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 2371619
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2371620
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment$2;->a:Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;

    iget-object v1, v1, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2371621
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment$2;->a:Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;

    iget-object v1, v1, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->f:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2371622
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment$2;->a:Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;

    iget-object v1, v1, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->m:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2371623
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment$2;->a:Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;

    iget-object v1, v1, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->l:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    invoke-virtual {v1, v0}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2371624
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment$2;->a:Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;

    iget-object v1, v1, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->j:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2371625
    new-instance v1, LX/GcI;

    invoke-direct {v1, p0}, LX/GcI;-><init>(Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment$2;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2371626
    return-void
.end method
