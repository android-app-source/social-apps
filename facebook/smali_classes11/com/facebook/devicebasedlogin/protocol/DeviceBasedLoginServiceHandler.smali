.class public Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static k:LX/0Xm;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/2Xr;

.field public final d:LX/42B;

.field public final e:LX/423;

.field public final f:LX/427;

.field public final g:LX/429;

.field public final h:LX/425;

.field public final i:LX/GbH;

.field public final j:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2369482
    const-class v0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/2Xr;LX/42B;LX/423;LX/427;LX/429;LX/425;LX/GbH;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/2Xr;",
            "LX/42B;",
            "LX/423;",
            "LX/427;",
            "LX/429;",
            "LX/425;",
            "LX/GbH;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2369483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2369484
    iput-object p1, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->b:LX/0Or;

    .line 2369485
    iput-object p2, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->c:LX/2Xr;

    .line 2369486
    iput-object p3, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->d:LX/42B;

    .line 2369487
    iput-object p4, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->e:LX/423;

    .line 2369488
    iput-object p5, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->f:LX/427;

    .line 2369489
    iput-object p6, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->g:LX/429;

    .line 2369490
    iput-object p7, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->h:LX/425;

    .line 2369491
    iput-object p8, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->i:LX/GbH;

    .line 2369492
    iput-object p9, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2369493
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;
    .locals 15

    .prologue
    .line 2369494
    const-class v1, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;

    monitor-enter v1

    .line 2369495
    :try_start_0
    sget-object v0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2369496
    sput-object v2, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2369497
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2369498
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2369499
    new-instance v3, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;

    const/16 v4, 0xb83

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/2Xr;->b(LX/0QB;)LX/2Xr;

    move-result-object v5

    check-cast v5, LX/2Xr;

    .line 2369500
    new-instance v7, LX/42B;

    const-class v6, LX/00I;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/00I;

    invoke-direct {v7, v6}, LX/42B;-><init>(LX/00I;)V

    .line 2369501
    move-object v6, v7

    .line 2369502
    check-cast v6, LX/42B;

    .line 2369503
    new-instance v11, LX/423;

    const-class v7, LX/00I;

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/00I;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v9

    check-cast v9, LX/0lC;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-direct {v11, v7, v8, v9, v10}, LX/423;-><init>(LX/00I;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lC;LX/03V;)V

    .line 2369504
    move-object v7, v11

    .line 2369505
    check-cast v7, LX/423;

    .line 2369506
    new-instance v9, LX/427;

    const-class v8, LX/00I;

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/00I;

    invoke-direct {v9, v8}, LX/427;-><init>(LX/00I;)V

    .line 2369507
    move-object v8, v9

    .line 2369508
    check-cast v8, LX/427;

    .line 2369509
    new-instance v10, LX/429;

    const-class v9, LX/00I;

    invoke-interface {v0, v9}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/00I;

    invoke-direct {v10, v9}, LX/429;-><init>(LX/00I;)V

    .line 2369510
    move-object v9, v10

    .line 2369511
    check-cast v9, LX/429;

    .line 2369512
    new-instance v14, LX/425;

    const-class v10, LX/00I;

    invoke-interface {v0, v10}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/00I;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v11

    check-cast v11, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v12

    check-cast v12, LX/0lC;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v13

    check-cast v13, LX/03V;

    invoke-direct {v14, v10, v11, v12, v13}, LX/425;-><init>(LX/00I;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lC;LX/03V;)V

    .line 2369513
    move-object v10, v14

    .line 2369514
    check-cast v10, LX/425;

    .line 2369515
    new-instance v11, LX/GbH;

    invoke-direct {v11}, LX/GbH;-><init>()V

    .line 2369516
    move-object v11, v11

    .line 2369517
    move-object v11, v11

    .line 2369518
    check-cast v11, LX/GbH;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v12

    check-cast v12, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;-><init>(LX/0Or;LX/2Xr;LX/42B;LX/423;LX/427;LX/429;LX/425;LX/GbH;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 2369519
    move-object v0, v3

    .line 2369520
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2369521
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2369522
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2369523
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 11

    .prologue
    .line 2369524
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2369525
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 2369526
    const-string v2, "set_nonce"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2369527
    const-string v0, "machine_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "pin"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "nonce_to_keep"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x0

    .line 2369528
    if-nez v0, :cond_0

    .line 2369529
    iget-object v3, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/26p;->f:LX/0Tn;

    invoke-interface {v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2369530
    :cond_0
    new-instance v4, LX/42Q;

    invoke-direct {v4, v0, v2, v1, v5}, LX/42Q;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2369531
    iget-object v3, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/11H;

    iget-object v5, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->c:LX/2Xr;

    sget-object p1, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v5, v4, p1}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 2369532
    invoke-static {v3}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v3

    move-object v0, v3

    .line 2369533
    :goto_0
    return-object v0

    .line 2369534
    :cond_1
    const-string v2, "remove_nonce"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2369535
    const-string v0, "account_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "nonce"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "end_persisted"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 2369536
    iget-object v3, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/26p;->f:LX/0Tn;

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2369537
    new-instance v4, LX/42A;

    invoke-direct {v4, v3, v0, v2, v1}, LX/42A;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2369538
    iget-object v3, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/11H;

    iget-object v5, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->d:LX/42B;

    sget-object p1, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v5, v4, p1}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    .line 2369539
    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v3

    move-object v0, v3

    .line 2369540
    goto :goto_0

    .line 2369541
    :cond_2
    const-string v2, "change_nonce"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2369542
    const-string v0, "account_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "nonce"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "old_pin"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "new_pin"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2369543
    iget-object v5, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/26p;->f:LX/0Tn;

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2369544
    new-instance v5, LX/422;

    move-object v6, v0

    move-object v8, v2

    move-object v9, v3

    move-object v10, v1

    invoke-direct/range {v5 .. v10}, LX/422;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2369545
    iget-object v6, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->b:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/11H;

    iget-object v7, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->e:LX/423;

    sget-object v8, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, v7, v5, v8}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 2369546
    invoke-static {v5}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v5

    move-object v0, v5

    .line 2369547
    goto/16 :goto_0

    .line 2369548
    :cond_3
    const-string v2, "check_nonce"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2369549
    const-string v0, "account_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "nonce"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "pin"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2369550
    iget-object v3, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/26p;->f:LX/0Tn;

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2369551
    new-instance v4, LX/426;

    invoke-direct {v4, v0, v3, v2, v1}, LX/426;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2369552
    iget-object v3, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/11H;

    iget-object v5, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->f:LX/427;

    sget-object v6, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v5, v4, v6}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    .line 2369553
    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v3

    move-object v0, v3

    .line 2369554
    goto/16 :goto_0

    .line 2369555
    :cond_4
    const-string v1, "check_password"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2369556
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2369557
    const-string v1, "account_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2369558
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 2369559
    const-string v2, "password"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2369560
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/26p;->f:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2369561
    new-instance v3, LX/428;

    invoke-direct {v3, v0, v2, v1}, LX/428;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2369562
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/11H;

    iget-object v4, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->g:LX/429;

    sget-object v5, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v4, v3, v5}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    .line 2369563
    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    move-object v0, v2

    .line 2369564
    goto/16 :goto_0

    .line 2369565
    :cond_5
    const-string v1, "change_nonce_using_password"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2369566
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2369567
    const-string v1, "account_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2369568
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 2369569
    const-string v2, "password"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2369570
    iget-object v2, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v2, v2

    .line 2369571
    const-string v3, "new_pin"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2369572
    iget-object v3, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/26p;->f:LX/0Tn;

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2369573
    new-instance v4, LX/424;

    invoke-direct {v4, v0, v3, v1, v2}, LX/424;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2369574
    iget-object v3, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/11H;

    iget-object v5, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->h:LX/425;

    sget-object v6, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v5, v4, v6}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 2369575
    invoke-static {v3}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v3

    move-object v0, v3

    .line 2369576
    goto/16 :goto_0

    .line 2369577
    :cond_6
    const-string v1, "dbl_face_rec"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2369578
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2369579
    const-string v1, "account_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2369580
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 2369581
    const-string v2, "face_crop_data"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    .line 2369582
    new-instance v3, Lcom/facebook/devicebasedlogin/protocol/DBLFaceRecParams;

    invoke-direct {v3, v0, v1}, Lcom/facebook/devicebasedlogin/protocol/DBLFaceRecParams;-><init>(Ljava/lang/String;[B)V

    .line 2369583
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/11H;

    iget-object v4, p0, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->i:LX/GbH;

    sget-object v5, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v4, v3, v5}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    .line 2369584
    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    move-object v0, v2

    .line 2369585
    goto/16 :goto_0

    .line 2369586
    :cond_7
    sget-object v0, LX/1nY;->ORCA_SERVICE_UNKNOWN_OPERATION:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0
.end method
