.class public Lcom/facebook/devicebasedlogin/protocol/DBLFaceRecParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/devicebasedlogin/protocol/DBLFaceRecParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public final b:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2369326
    new-instance v0, LX/GbI;

    invoke-direct {v0}, LX/GbI;-><init>()V

    sput-object v0, Lcom/facebook/devicebasedlogin/protocol/DBLFaceRecParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2369327
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2369328
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/protocol/DBLFaceRecParams;->a:Ljava/lang/String;

    .line 2369329
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/protocol/DBLFaceRecParams;->b:[B

    .line 2369330
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[B)V
    .locals 0

    .prologue
    .line 2369331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2369332
    iput-object p1, p0, Lcom/facebook/devicebasedlogin/protocol/DBLFaceRecParams;->a:Ljava/lang/String;

    .line 2369333
    iput-object p2, p0, Lcom/facebook/devicebasedlogin/protocol/DBLFaceRecParams;->b:[B

    .line 2369334
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2369335
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2369336
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/protocol/DBLFaceRecParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2369337
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/protocol/DBLFaceRecParams;->b:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 2369338
    return-void
.end method
