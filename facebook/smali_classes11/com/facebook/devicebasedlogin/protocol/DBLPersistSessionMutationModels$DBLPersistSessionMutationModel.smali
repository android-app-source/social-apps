.class public final Lcom/facebook/devicebasedlogin/protocol/DBLPersistSessionMutationModels$DBLPersistSessionMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xe72ee5e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/devicebasedlogin/protocol/DBLPersistSessionMutationModels$DBLPersistSessionMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/devicebasedlogin/protocol/DBLPersistSessionMutationModels$DBLPersistSessionMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2369414
    const-class v0, Lcom/facebook/devicebasedlogin/protocol/DBLPersistSessionMutationModels$DBLPersistSessionMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2369413
    const-class v0, Lcom/facebook/devicebasedlogin/protocol/DBLPersistSessionMutationModels$DBLPersistSessionMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2369411
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2369412
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2369409
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/protocol/DBLPersistSessionMutationModels$DBLPersistSessionMutationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/protocol/DBLPersistSessionMutationModels$DBLPersistSessionMutationModel;->e:Ljava/lang/String;

    .line 2369410
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/protocol/DBLPersistSessionMutationModels$DBLPersistSessionMutationModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2369415
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2369416
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/protocol/DBLPersistSessionMutationModels$DBLPersistSessionMutationModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2369417
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2369418
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2369419
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/facebook/devicebasedlogin/protocol/DBLPersistSessionMutationModels$DBLPersistSessionMutationModel;->f:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2369420
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2369421
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2369406
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2369407
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2369408
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2369403
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2369404
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/devicebasedlogin/protocol/DBLPersistSessionMutationModels$DBLPersistSessionMutationModel;->f:Z

    .line 2369405
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 2369401
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2369402
    iget-boolean v0, p0, Lcom/facebook/devicebasedlogin/protocol/DBLPersistSessionMutationModels$DBLPersistSessionMutationModel;->f:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2369398
    new-instance v0, Lcom/facebook/devicebasedlogin/protocol/DBLPersistSessionMutationModels$DBLPersistSessionMutationModel;

    invoke-direct {v0}, Lcom/facebook/devicebasedlogin/protocol/DBLPersistSessionMutationModels$DBLPersistSessionMutationModel;-><init>()V

    .line 2369399
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2369400
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2369397
    const v0, -0x7bf423b3

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2369396
    const v0, -0x9bf73a1

    return v0
.end method
