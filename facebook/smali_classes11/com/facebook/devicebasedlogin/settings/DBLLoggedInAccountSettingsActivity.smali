.class public Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/Gb3;


# instance fields
.field private p:LX/10M;

.field private q:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

.field private r:LX/10O;

.field public s:LX/GbV;

.field private t:Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;

.field private u:Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsProgressFragment;

.field public final v:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2369754
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2369755
    new-instance v0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity$1;

    invoke-direct {v0, p0}, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity$1;-><init>(Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;)V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->v:Ljava/lang/Runnable;

    return-void
.end method

.method private a(LX/10M;LX/2J4;LX/10O;LX/GbV;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2369749
    iput-object p1, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->p:LX/10M;

    .line 2369750
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->p:LX/10M;

    invoke-virtual {p2, v0, p3}, LX/2J4;->a(LX/10M;LX/10O;)Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->q:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    .line 2369751
    iput-object p3, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->r:LX/10O;

    .line 2369752
    iput-object p4, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->s:LX/GbV;

    .line 2369753
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;

    invoke-static {v3}, LX/10M;->b(LX/0QB;)LX/10M;

    move-result-object v0

    check-cast v0, LX/10M;

    const-class v1, LX/2J4;

    invoke-interface {v3, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/2J4;

    invoke-static {v3}, LX/10O;->b(LX/0QB;)LX/10O;

    move-result-object v2

    check-cast v2, LX/10O;

    invoke-static {v3}, LX/GbV;->a(LX/0QB;)LX/GbV;

    move-result-object v3

    check-cast v3, LX/GbV;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->a(LX/10M;LX/2J4;LX/10O;LX/GbV;)V

    return-void
.end method

.method private l()V
    .locals 3

    .prologue
    .line 2369741
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->s:LX/GbV;

    .line 2369742
    iget-object v2, v0, LX/GbV;->a:LX/10M;

    iget-object v1, v0, LX/GbV;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 2369743
    iget-object v0, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v0

    .line 2369744
    invoke-virtual {v2, v1}, LX/10M;->c(Ljava/lang/String;)Z

    move-result v1

    move v0, v1

    .line 2369745
    if-eqz v0, :cond_0

    .line 2369746
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->s:LX/GbV;

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->v:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, p0}, LX/GbV;->a(Ljava/lang/Runnable;Landroid/content/Context;)V

    .line 2369747
    :goto_0
    return-void

    .line 2369748
    :cond_0
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->m()V

    goto :goto_0
.end method

.method private m()V
    .locals 3

    .prologue
    .line 2369735
    const/4 v0, 0x0

    sget-object v1, LX/GbF;->DEFAULT:LX/GbF;

    invoke-static {v0, v1}, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->a(ZLX/GbF;)Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;

    move-result-object v0

    .line 2369736
    iput-object p0, v0, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxFragment;->f:LX/Gb3;

    .line 2369737
    instance-of v1, v0, LX/Gb4;

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2369738
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    .line 2369739
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2369740
    return-void
.end method

.method public static n(Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;)V
    .locals 3

    .prologue
    .line 2369730
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->p()V

    .line 2369731
    new-instance v0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;

    invoke-direct {v0}, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->t:Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;

    .line 2369732
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    .line 2369733
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->t:Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2369734
    return-void
.end method

.method private o()V
    .locals 5

    .prologue
    .line 2369726
    new-instance v0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsProgressFragment;

    invoke-direct {v0}, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsProgressFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->u:Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsProgressFragment;

    .line 2369727
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    .line 2369728
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0400d8

    const v2, 0x7f0400dc

    const v3, 0x7f0400e9

    const v4, 0x7f0400ec

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0hH;->a(IIII)LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->u:Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsProgressFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2369729
    return-void
.end method

.method private p()V
    .locals 3

    .prologue
    .line 2369756
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2369757
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2369758
    new-instance v1, LX/GbR;

    invoke-direct {v1, p0}, LX/GbR;-><init>(Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2369759
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08349d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2369760
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2369722
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->q:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    const-string v1, ""

    new-instance v3, LX/GbQ;

    invoke-direct {v3, p0}, LX/GbQ;-><init>(Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;)V

    const-string v4, "logged_in_settings"

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a(Ljava/lang/String;Lcom/facebook/auth/credentials/DBLFacebookCredentials;LX/0TF;Ljava/lang/String;Z)V

    .line 2369723
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->r:LX/10O;

    const-string v1, "dbl_nux_dismiss_forward"

    invoke-virtual {v0, v1, v2}, LX/10O;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2369724
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->o()V

    .line 2369725
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2369719
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->r:LX/10O;

    const-string v1, "dbl_nux_dismiss_backward"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/10O;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2369720
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->onBackPressed()V

    .line 2369721
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2369714
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2369715
    invoke-static {p0, p0}, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2369716
    const v0, 0x7f0303e9

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->setContentView(I)V

    .line 2369717
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->l()V

    .line 2369718
    return-void
.end method

.method public final d_(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2369713
    return-void
.end method

.method public final lR_()V
    .locals 0

    .prologue
    .line 2369712
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2369707
    const/16 v0, 0xc

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->t:Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;

    if-eqz v0, :cond_0

    .line 2369708
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsActivity;->t:Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;

    .line 2369709
    iget-object p0, v0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;->b:LX/Gbj;

    iget-object p1, v0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;->a:LX/GbV;

    invoke-virtual {p1}, LX/GbV;->c()Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, LX/Gbj;->a(Ljava/util/List;Landroid/content/Context;)LX/Gbi;

    move-result-object p0

    iput-object p0, v0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;->d:LX/Gbi;

    .line 2369710
    iget-object p0, v0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object p1, v0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;->d:LX/Gbi;

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2369711
    :cond_0
    return-void
.end method
