.class public final Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$FetchDBLNonceInfoQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$FetchDBLNonceInfoQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2370708
    const-class v0, Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$FetchDBLNonceInfoQueryModel;

    new-instance v1, Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$FetchDBLNonceInfoQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$FetchDBLNonceInfoQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2370709
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2370710
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$FetchDBLNonceInfoQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2370690
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2370691
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2370692
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2370693
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2370694
    if-eqz v2, :cond_2

    .line 2370695
    const-string p0, "device_based_login_nonce_infos"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2370696
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2370697
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 2370698
    if-eqz p0, :cond_1

    .line 2370699
    const-string v0, "nodes"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2370700
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2370701
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, p0}, LX/15i;->c(I)I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2370702
    invoke-virtual {v1, p0, v0}, LX/15i;->q(II)I

    move-result v2

    invoke-static {v1, v2, p1, p2}, LX/Gbw;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2370703
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2370704
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2370705
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2370706
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2370707
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2370689
    check-cast p1, Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$FetchDBLNonceInfoQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$FetchDBLNonceInfoQueryModel$Serializer;->a(Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$FetchDBLNonceInfoQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
