.class public Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsAccountsListFragment;
.super Lcom/facebook/base/fragment/FbListFragment;
.source ""

# interfaces
.implements LX/0l6;
.implements LX/2JL;


# static fields
.field private static final k:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public i:LX/Gbg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/GbX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2369941
    const-class v0, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsAccountsListFragment;

    sput-object v0, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsAccountsListFragment;->k:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2369940
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbListFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 2369931
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->a:Landroid/widget/ListAdapter;

    move-object v0, v0

    .line 2369932
    invoke-interface {v0, p2}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 2369933
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsAccountsListFragment;->j:LX/GbX;

    invoke-interface {v1, v0}, LX/GbX;->a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V

    .line 2369934
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x545fa2e2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2369935
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2369936
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsAccountsListFragment;->i:LX/Gbg;

    invoke-virtual {p0, v1}, Landroid/support/v4/app/ListFragment;->a(Landroid/widget/ListAdapter;)V

    .line 2369937
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsAccountsListFragment;->i:LX/Gbg;

    const v2, 0x7f0303e3

    .line 2369938
    iput v2, v1, LX/Gbg;->d:I

    .line 2369939
    const/16 v1, 0x2b

    const v2, -0x11401889

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x105e320f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2369928
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2369929
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v4

    check-cast p0, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsAccountsListFragment;

    new-instance v2, LX/Gbg;

    const-class p1, Landroid/content/Context;

    invoke-interface {v4, p1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    invoke-static {v4}, LX/10M;->b(LX/0QB;)LX/10M;

    move-result-object v1

    check-cast v1, LX/10M;

    invoke-direct {v2, p1, v1}, LX/Gbg;-><init>(Landroid/content/Context;LX/10M;)V

    move-object v4, v2

    check-cast v4, LX/Gbg;

    iput-object v4, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsAccountsListFragment;->i:LX/Gbg;

    .line 2369930
    const/16 v1, 0x2b

    const v2, 0x616afa2b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x7b83bafe

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2369913
    const v0, 0x7f0303e2

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2369914
    const v0, 0x7f0d0541

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2369915
    const v3, 0x7f08346e

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 2369916
    const/16 v0, 0x2b

    const v3, -0x7cfaac9e

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3a0dd117

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2369917
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onResume()V

    .line 2369918
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsAccountsListFragment;->i:LX/Gbg;

    .line 2369919
    iget-object v2, v1, LX/Gbg;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2369920
    iget-object v2, v1, LX/Gbg;->c:LX/10M;

    invoke-virtual {v2}, LX/10M;->f()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2369921
    iget-object v2, v1, LX/Gbg;->b:Ljava/util/List;

    iget-object v4, v1, LX/Gbg;->c:LX/10M;

    invoke-virtual {v4}, LX/10M;->a()Ljava/util/List;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2369922
    const v2, 0x729ec6fb

    invoke-static {v1, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2369923
    const/4 v2, 0x1

    .line 2369924
    :goto_0
    move v1, v2

    .line 2369925
    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsAccountsListFragment;->j:LX/GbX;

    if-eqz v1, :cond_0

    .line 2369926
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsAccountsListFragment;->j:LX/GbX;

    invoke-interface {v1}, LX/GbX;->a()V

    .line 2369927
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x4eb5686f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
