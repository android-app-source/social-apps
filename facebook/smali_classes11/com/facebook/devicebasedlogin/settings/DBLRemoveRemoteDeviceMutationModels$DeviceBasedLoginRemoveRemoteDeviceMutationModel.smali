.class public final Lcom/facebook/devicebasedlogin/settings/DBLRemoveRemoteDeviceMutationModels$DeviceBasedLoginRemoveRemoteDeviceMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x75753d2f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/devicebasedlogin/settings/DBLRemoveRemoteDeviceMutationModels$DeviceBasedLoginRemoveRemoteDeviceMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/devicebasedlogin/settings/DBLRemoveRemoteDeviceMutationModels$DeviceBasedLoginRemoveRemoteDeviceMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2370328
    const-class v0, Lcom/facebook/devicebasedlogin/settings/DBLRemoveRemoteDeviceMutationModels$DeviceBasedLoginRemoveRemoteDeviceMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2370327
    const-class v0, Lcom/facebook/devicebasedlogin/settings/DBLRemoveRemoteDeviceMutationModels$DeviceBasedLoginRemoveRemoteDeviceMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2370325
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2370326
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2370315
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLRemoveRemoteDeviceMutationModels$DeviceBasedLoginRemoveRemoteDeviceMutationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLRemoveRemoteDeviceMutationModels$DeviceBasedLoginRemoveRemoteDeviceMutationModel;->e:Ljava/lang/String;

    .line 2370316
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLRemoveRemoteDeviceMutationModels$DeviceBasedLoginRemoveRemoteDeviceMutationModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2370329
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2370330
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/settings/DBLRemoveRemoteDeviceMutationModels$DeviceBasedLoginRemoveRemoteDeviceMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2370331
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2370332
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2370333
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2370334
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2370322
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2370323
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2370324
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2370319
    new-instance v0, Lcom/facebook/devicebasedlogin/settings/DBLRemoveRemoteDeviceMutationModels$DeviceBasedLoginRemoveRemoteDeviceMutationModel;

    invoke-direct {v0}, Lcom/facebook/devicebasedlogin/settings/DBLRemoveRemoteDeviceMutationModels$DeviceBasedLoginRemoveRemoteDeviceMutationModel;-><init>()V

    .line 2370320
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2370321
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2370318
    const v0, -0x105f5fd6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2370317
    const v0, -0x1d68041f

    return v0
.end method
