.class public Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0l6;
.implements LX/GbX;
.implements LX/2JL;


# instance fields
.field public p:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation runtime Lcom/facebook/katana/annotations/DBLLoginSettingsFragmentName;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/10O;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsAccountsListFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2369942
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsActivity;Lcom/facebook/content/SecureContextHelper;Ljava/lang/String;LX/10O;)V
    .locals 0

    .prologue
    .line 2369943
    iput-object p1, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsActivity;->p:Lcom/facebook/content/SecureContextHelper;

    iput-object p2, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsActivity;->q:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsActivity;->r:LX/10O;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsActivity;

    invoke-static {v2}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {}, LX/Gbp;->a()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v2}, LX/10O;->b(LX/0QB;)LX/10O;

    move-result-object v2

    check-cast v2, LX/10O;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsActivity;->a(Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsActivity;Lcom/facebook/content/SecureContextHelper;Ljava/lang/String;LX/10O;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2369944
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsActivity;->finish()V

    .line 2369945
    return-void
.end method

.method public final a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V
    .locals 3

    .prologue
    .line 2369946
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2369947
    const-string v1, "fbid"

    iget-object v2, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2369948
    iget-object v1, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mNonce:Ljava/lang/String;

    const-string v2, "password_account"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2369949
    const-string v1, "account_type"

    const-string v2, "password_account"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2369950
    :goto_0
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsActivity;->r:LX/10O;

    const-string v2, "dbl_settings_displayed"

    invoke-virtual {v1, v2, v0}, LX/10O;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2369951
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2369952
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2369953
    const-string v2, "dbl_account_details"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2369954
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2369955
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsActivity;->p:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2369956
    const v0, 0x7f04003b

    const v1, 0x7f040041

    invoke-virtual {p0, v0, v1}, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsActivity;->overridePendingTransition(II)V

    .line 2369957
    return-void

    .line 2369958
    :cond_0
    iget-object v1, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mIsPinSet:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2369959
    const-string v1, "view"

    const-string v2, "pin"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2369960
    :cond_1
    const-string v1, "view"

    const-string v2, "no_pin"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2369961
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2369962
    invoke-static {p0, p0}, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2369963
    const v0, 0x7f0303e0

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsActivity;->setContentView(I)V

    .line 2369964
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    .line 2369965
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsActivity;->q:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    move-object v0, v1

    .line 2369966
    check-cast v0, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsAccountsListFragment;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsActivity;->s:Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsAccountsListFragment;

    .line 2369967
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsActivity;->s:Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsAccountsListFragment;

    .line 2369968
    iput-object p0, v0, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsAccountsListFragment;->j:LX/GbX;

    .line 2369969
    invoke-virtual {v2}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v2, 0x7f0d002f

    invoke-virtual {v0, v2, v1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2369970
    return-void
.end method
