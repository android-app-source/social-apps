.class public final Lcom/facebook/devicebasedlogin/settings/DBLRemoveRemoteDeviceMutationModels$DeviceBasedLoginRemoveRemoteDeviceMutationModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2370300
    const-class v0, Lcom/facebook/devicebasedlogin/settings/DBLRemoveRemoteDeviceMutationModels$DeviceBasedLoginRemoveRemoteDeviceMutationModel;

    new-instance v1, Lcom/facebook/devicebasedlogin/settings/DBLRemoveRemoteDeviceMutationModels$DeviceBasedLoginRemoveRemoteDeviceMutationModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/devicebasedlogin/settings/DBLRemoveRemoteDeviceMutationModels$DeviceBasedLoginRemoveRemoteDeviceMutationModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2370301
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2370276
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2370277
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2370278
    const/4 v2, 0x0

    .line 2370279
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 2370280
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2370281
    :goto_0
    move v1, v2

    .line 2370282
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2370283
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2370284
    new-instance v1, Lcom/facebook/devicebasedlogin/settings/DBLRemoveRemoteDeviceMutationModels$DeviceBasedLoginRemoveRemoteDeviceMutationModel;

    invoke-direct {v1}, Lcom/facebook/devicebasedlogin/settings/DBLRemoveRemoteDeviceMutationModels$DeviceBasedLoginRemoveRemoteDeviceMutationModel;-><init>()V

    .line 2370285
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2370286
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2370287
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2370288
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2370289
    :cond_0
    return-object v1

    .line 2370290
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2370291
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, p0, :cond_3

    .line 2370292
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2370293
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2370294
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v3, :cond_2

    .line 2370295
    const-string p0, "datr"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2370296
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    .line 2370297
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2370298
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2370299
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1
.end method
