.class public Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements LX/0l6;
.implements LX/2JL;


# instance fields
.field private A:Landroid/view/View;

.field private B:Z

.field public p:Landroid/content/ComponentName;
    .annotation runtime Lcom/facebook/katana/login/LoginActivityComponent;
    .end annotation
.end field

.field private q:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

.field private r:LX/10M;

.field private s:Lcom/facebook/content/SecureContextHelper;

.field private t:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

.field private u:Landroid/view/View;

.field private v:Landroid/widget/TextView;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/view/View;

.field private y:Landroid/view/View;

.field private z:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2369685
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2369686
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->B:Z

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2369660
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2369661
    const-string v1, "dbl_account_details"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->q:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 2369662
    const v0, 0x7f0d0575

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    .line 2369663
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->q:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v1, v1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mPicUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->setImage(Ljava/lang/String;)V

    .line 2369664
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->q:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v0, v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mNonce:Ljava/lang/String;

    const-string v1, "password_account"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2369665
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->B:Z

    .line 2369666
    :cond_0
    const v0, 0x7f0d0bf2

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2369667
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->q:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v1, v1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2369668
    const v0, 0x7f0d0bf3

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->u:Landroid/view/View;

    .line 2369669
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->u:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2369670
    const v0, 0x7f0d0bf5

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->v:Landroid/widget/TextView;

    .line 2369671
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2369672
    const v0, 0x7f0d0bf6

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->w:Landroid/widget/TextView;

    .line 2369673
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->w:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2369674
    const v0, 0x7f0d0541

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2369675
    const v1, 0x7f083486

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2369676
    const v0, 0x7f0d0bf7

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->x:Landroid/view/View;

    .line 2369677
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->x:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2369678
    const v0, 0x7f0d0bfa

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->y:Landroid/view/View;

    .line 2369679
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->y:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2369680
    const v0, 0x7f0d0bf9

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->A:Landroid/view/View;

    .line 2369681
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->A:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2369682
    const v0, 0x7f0d0bfb

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->z:Landroid/view/View;

    .line 2369683
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->z:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2369684
    return-void
.end method

.method private a(LX/10M;LX/10O;Lcom/facebook/content/SecureContextHelper;Landroid/content/ComponentName;LX/2J4;)V
    .locals 1
    .param p4    # Landroid/content/ComponentName;
        .annotation runtime Lcom/facebook/katana/login/LoginActivityComponent;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2369655
    iput-object p1, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->r:LX/10M;

    .line 2369656
    iput-object p3, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->s:Lcom/facebook/content/SecureContextHelper;

    .line 2369657
    iput-object p4, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->p:Landroid/content/ComponentName;

    .line 2369658
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->r:LX/10M;

    invoke-virtual {p5, v0, p2}, LX/2J4;->a(LX/10M;LX/10O;)Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->t:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    .line 2369659
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;

    invoke-static {v5}, LX/10M;->b(LX/0QB;)LX/10M;

    move-result-object v1

    check-cast v1, LX/10M;

    invoke-static {v5}, LX/10O;->b(LX/0QB;)LX/10O;

    move-result-object v2

    check-cast v2, LX/10O;

    invoke-static {v5}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v5}, LX/27F;->b(LX/0QB;)Landroid/content/ComponentName;

    move-result-object v4

    check-cast v4, Landroid/content/ComponentName;

    const-class v6, LX/2J4;

    invoke-interface {v5, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/2J4;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->a(LX/10M;LX/10O;Lcom/facebook/content/SecureContextHelper;Landroid/content/ComponentName;LX/2J4;)V

    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 2369587
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->r:LX/10M;

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->q:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v1, v1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/10M;->b(Ljava/lang/String;)Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->q:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 2369588
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->q:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    if-nez v0, :cond_1

    .line 2369589
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->finish()V

    .line 2369590
    :cond_0
    :goto_0
    return-void

    .line 2369591
    :cond_1
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->q:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v0, v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mNonce:Ljava/lang/String;

    const-string v1, "password_account"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2369592
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->B:Z

    .line 2369593
    :goto_1
    iget-boolean v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->B:Z

    if-eqz v0, :cond_4

    .line 2369594
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->u:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2369595
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2369596
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2369597
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->x:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2369598
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->y:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2369599
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->A:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2369600
    :cond_2
    :goto_2
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2369601
    const v0, 0x7f0d0bfc

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0834a5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2369602
    const v0, 0x7f0d0bfd

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0834a6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2369603
    const v0, 0x7f0d0bf4

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2369604
    :cond_3
    iput-boolean v3, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->B:Z

    goto :goto_1

    .line 2369605
    :cond_4
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->y:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2369606
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->A:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2369607
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->q:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v0, v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mIsPinSet:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2369608
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->u:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2369609
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2369610
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2369611
    :goto_3
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2369612
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->x:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 2369613
    :cond_5
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->u:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2369614
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2369615
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3
.end method

.method private l()Z
    .locals 2

    .prologue
    .line 2369654
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->r:LX/10M;

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->q:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v1, v1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/10M;->g(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private m()V
    .locals 4

    .prologue
    .line 2369687
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->t:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->q:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    const-string v2, "logged_out_settings"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;Ljava/lang/String;Z)V

    .line 2369688
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08349a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2369689
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->r:LX/10M;

    invoke-virtual {v0}, LX/10M;->f()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2369690
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->finish()V

    .line 2369691
    :goto_0
    return-void

    .line 2369692
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->p:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 2369693
    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2369694
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->s:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method private n()V
    .locals 3

    .prologue
    .line 2369650
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->t:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->q:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    const-string v2, "logged_out_settings"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;Ljava/lang/String;)V

    .line 2369651
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f083499

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2369652
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->b()V

    .line 2369653
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2369645
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2369646
    invoke-static {p0, p0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2369647
    const v0, 0x7f0303d5

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->setContentView(I)V

    .line 2369648
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->a()V

    .line 2369649
    return-void
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 2369641
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2369642
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->r:LX/10M;

    invoke-virtual {v0}, LX/10M;->c()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 2369643
    const v0, 0x7f04003a

    const v1, 0x7f040040

    invoke-virtual {p0, v0, v1}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->overridePendingTransition(II)V

    .line 2369644
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x9a047c1

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2369619
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 2369620
    const v2, 0x7f0d0bfb

    if-ne v1, v2, :cond_0

    .line 2369621
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->m()V

    .line 2369622
    :goto_0
    const v1, 0x53ad9db0

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2369623
    :cond_0
    const v2, 0x7f0d0bf7

    if-ne v1, v2, :cond_1

    .line 2369624
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->n()V

    goto :goto_0

    .line 2369625
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2369626
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2369627
    const-string v4, "dbl_account_details"

    iget-object v5, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->q:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2369628
    const v4, 0x7f0d0bf3

    if-ne v1, v4, :cond_3

    .line 2369629
    const-string v1, "operation_type"

    const-string v4, "add_pin"

    invoke-virtual {v3, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2369630
    :cond_2
    :goto_1
    invoke-virtual {v2, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2369631
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->s:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2369632
    const v1, 0x7f04003a

    const v2, 0x7f040040

    invoke-virtual {p0, v1, v2}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->overridePendingTransition(II)V

    goto :goto_0

    .line 2369633
    :cond_3
    const v4, 0x7f0d0bf5

    if-ne v1, v4, :cond_4

    .line 2369634
    const-string v1, "operation_type"

    const-string v4, "change_pin"

    invoke-virtual {v3, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2369635
    :cond_4
    const v4, 0x7f0d0bf6

    if-ne v1, v4, :cond_5

    .line 2369636
    const-string v1, "operation_type"

    const-string v4, "remove_pin"

    invoke-virtual {v3, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2369637
    :cond_5
    const v4, 0x7f0d0bfa

    if-ne v1, v4, :cond_6

    .line 2369638
    const-string v1, "operation_type"

    const-string v4, "switch_to_dbl_with_pin"

    invoke-virtual {v3, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2369639
    :cond_6
    const v4, 0x7f0d0bf9

    if-ne v1, v4, :cond_2

    .line 2369640
    const-string v1, "operation_type"

    const-string v4, "switch_to_dbl"

    invoke-virtual {v3, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x789b4e8d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2369616
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2369617
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;->b()V

    .line 2369618
    const/16 v1, 0x23

    const v2, 0x3d4c74c6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
