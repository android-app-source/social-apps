.class public Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/GbV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Gbj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public d:LX/Gbi;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2369894
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2369891
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2369892
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;

    invoke-static {p1}, LX/GbV;->a(LX/0QB;)LX/GbV;

    move-result-object v2

    check-cast v2, LX/GbV;

    const-class v0, LX/Gbj;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/Gbj;

    iput-object v2, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;->a:LX/GbV;

    iput-object p1, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;->b:LX/Gbj;

    .line 2369893
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x264a4a66

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2369878
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2369879
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;->b:LX/Gbj;

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;->a:LX/GbV;

    invoke-virtual {v2}, LX/GbV;->c()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/Gbj;->a(Ljava/util/List;Landroid/content/Context;)LX/Gbi;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;->d:LX/Gbi;

    .line 2369880
    new-instance v1, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1P1;-><init>(Landroid/content/Context;)V

    .line 2369881
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;->d:LX/Gbi;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2369882
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2369883
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v2, LX/Gbr;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, LX/Gbr;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2369884
    const/16 v1, 0x2b

    const v2, 0xb3d265d

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/16 v0, 0x2a

    const v1, 0x34e831dd

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2369885
    const v0, 0x7f0303eb

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2369886
    const v0, 0x7f0d0c26

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2369887
    const/4 v0, 0x1

    const/high16 v3, 0x41800000    # 16.0f

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v0, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 2369888
    iget-object v3, p0, Lcom/facebook/devicebasedlogin/settings/DBLLoggedInAccountSettingsFragment;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v3, v0, v5, v0, v5}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setPadding(IIII)V

    .line 2369889
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00d5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v0, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v2, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2369890
    const/16 v0, 0x2b

    const v3, -0x6a2bf139

    invoke-static {v6, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method
