.class public final Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2370641
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2370642
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2370639
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2370640
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 8

    .prologue
    .line 2370597
    if-nez p1, :cond_0

    .line 2370598
    const/4 v0, 0x0

    .line 2370599
    :goto_0
    return v0

    .line 2370600
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2370601
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2370602
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2370603
    const v1, -0x4f997970

    const/4 v3, 0x0

    .line 2370604
    if-nez v0, :cond_1

    move v2, v3

    .line 2370605
    :goto_1
    move v0, v2

    .line 2370606
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2370607
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2370608
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2370609
    :sswitch_1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2370610
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2370611
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v1

    .line 2370612
    const v2, -0x349c2fe0    # -1.4929952E7f

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2370613
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v4

    .line 2370614
    const/4 v2, 0x3

    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v5

    .line 2370615
    const/4 v2, 0x4

    const-wide/16 v6, 0x0

    invoke-virtual {p0, p1, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 2370616
    const/4 v6, 0x5

    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v6

    .line 2370617
    invoke-virtual {p3, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2370618
    const/4 v7, 0x6

    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 2370619
    const/4 v7, 0x0

    invoke-virtual {p3, v7, v0}, LX/186;->b(II)V

    .line 2370620
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2370621
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v4}, LX/186;->a(IZ)V

    .line 2370622
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v5}, LX/186;->a(IZ)V

    .line 2370623
    const/4 v1, 0x4

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2370624
    const/4 v0, 0x5

    invoke-virtual {p3, v0, v6}, LX/186;->b(II)V

    .line 2370625
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2370626
    :sswitch_2
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2370627
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2370628
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2370629
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2370630
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2370631
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    .line 2370632
    if-nez v4, :cond_2

    const/4 v2, 0x0

    .line 2370633
    :goto_2
    if-ge v3, v4, :cond_3

    .line 2370634
    invoke-virtual {p0, v0, v3}, LX/15i;->q(II)I

    move-result v5

    .line 2370635
    invoke-static {p0, v5, v1, p3}, Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v5

    aput v5, v2, v3

    .line 2370636
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 2370637
    :cond_2
    new-array v2, v4, [I

    goto :goto_2

    .line 2370638
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {p3, v2, v3}, LX/186;->a([IZ)I

    move-result v2

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x4f997970 -> :sswitch_1
        -0x349c2fe0 -> :sswitch_2
        0x794b6d13 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2370596
    new-instance v0, Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 2370583
    sparse-switch p2, :sswitch_data_0

    .line 2370584
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2370585
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2370586
    const v1, -0x4f997970

    .line 2370587
    if-eqz v0, :cond_0

    .line 2370588
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 2370589
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 2370590
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2370591
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2370592
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2370593
    :cond_0
    :goto_1
    :sswitch_1
    return-void

    .line 2370594
    :sswitch_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2370595
    const v1, -0x349c2fe0    # -1.4929952E7f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x4f997970 -> :sswitch_2
        -0x349c2fe0 -> :sswitch_1
        0x794b6d13 -> :sswitch_0
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2370577
    if-eqz p1, :cond_0

    .line 2370578
    invoke-static {p0, p1, p2}, Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$DraculaImplementation;

    move-result-object v1

    .line 2370579
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$DraculaImplementation;

    .line 2370580
    if-eq v0, v1, :cond_0

    .line 2370581
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2370582
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2370576
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2370574
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2370575
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2370569
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2370570
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2370571
    :cond_0
    iput-object p1, p0, Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2370572
    iput p2, p0, Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$DraculaImplementation;->b:I

    .line 2370573
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2370543
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2370568
    new-instance v0, Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2370565
    iget v0, p0, LX/1vt;->c:I

    .line 2370566
    move v0, v0

    .line 2370567
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2370562
    iget v0, p0, LX/1vt;->c:I

    .line 2370563
    move v0, v0

    .line 2370564
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2370559
    iget v0, p0, LX/1vt;->b:I

    .line 2370560
    move v0, v0

    .line 2370561
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2370556
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2370557
    move-object v0, v0

    .line 2370558
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2370547
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2370548
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2370549
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2370550
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2370551
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2370552
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2370553
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2370554
    invoke-static {v3, v9, v2}, Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2370555
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2370544
    iget v0, p0, LX/1vt;->c:I

    .line 2370545
    move v0, v0

    .line 2370546
    return v0
.end method
