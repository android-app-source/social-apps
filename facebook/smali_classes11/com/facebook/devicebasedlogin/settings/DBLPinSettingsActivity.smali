.class public Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0l6;
.implements LX/Gbb;
.implements LX/2JL;


# static fields
.field public static final A:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public B:LX/GcK;

.field public C:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

.field private D:Ljava/lang/String;

.field public E:Z

.field private F:I

.field public p:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/10M;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/10O;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/2J4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0fW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/2Di;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private w:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

.field public x:I

.field public y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2370195
    const-class v0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    sput-object v0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->A:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2370196
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2370197
    iput v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->x:I

    .line 2370198
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->y:Ljava/lang/String;

    .line 2370199
    iput v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->F:I

    return-void
.end method

.method private a(Landroid/os/Bundle;Ljava/lang/String;LX/0TF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            "LX/0TF",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2370200
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->p:LX/0aG;

    const v1, -0x31513895

    invoke-static {v0, p2, p1, v1}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    .line 2370201
    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2370202
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->q:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, p3, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2370203
    return-void
.end method

.method private static a(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;LX/0aG;Ljava/util/concurrent/ExecutorService;LX/10M;LX/10O;LX/2J4;LX/0fW;LX/2Di;)V
    .locals 0

    .prologue
    .line 2370204
    iput-object p1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->p:LX/0aG;

    iput-object p2, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->q:Ljava/util/concurrent/ExecutorService;

    iput-object p3, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->r:LX/10M;

    iput-object p4, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->s:LX/10O;

    iput-object p5, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->t:LX/2J4;

    iput-object p6, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->u:LX/0fW;

    iput-object p7, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->v:LX/2Di;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    invoke-static {v7}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v1

    check-cast v1, LX/0aG;

    invoke-static {v7}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {v7}, LX/10M;->b(LX/0QB;)LX/10M;

    move-result-object v3

    check-cast v3, LX/10M;

    invoke-static {v7}, LX/10O;->b(LX/0QB;)LX/10O;

    move-result-object v4

    check-cast v4, LX/10O;

    const-class v5, LX/2J4;

    invoke-interface {v7, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/2J4;

    invoke-static {v7}, LX/0fW;->a(LX/0QB;)LX/0fW;

    move-result-object v6

    check-cast v6, LX/0fW;

    invoke-static {v7}, LX/2Di;->b(LX/0QB;)LX/2Di;

    move-result-object v7

    check-cast v7, LX/2Di;

    invoke-static/range {v0 .. v7}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->a(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;LX/0aG;Ljava/util/concurrent/ExecutorService;LX/10M;LX/10O;LX/2J4;LX/0fW;LX/2Di;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2370205
    invoke-static {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->t(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2370206
    invoke-static {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->q(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)LX/Gb4;

    move-result-object v0

    invoke-interface {v0}, LX/Gb4;->c()V

    .line 2370207
    :cond_0
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->C:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    invoke-interface {v0, p1, p2, v1}, LX/GcK;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/auth/credentials/DBLFacebookCredentials;)Landroid/os/Bundle;

    move-result-object v0

    .line 2370208
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    invoke-interface {v1}, LX/GcK;->a()Ljava/lang/String;

    move-result-object v1

    .line 2370209
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->l()LX/0TF;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->a(Landroid/os/Bundle;Ljava/lang/String;LX/0TF;)V

    .line 2370210
    invoke-direct {p0, p1, p2}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2370211
    return-void
.end method

.method private b(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 2370263
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    .line 2370264
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1, p1}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 2370265
    return-void
.end method

.method public static b(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2370212
    const v0, 0x7f08348f

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2370213
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v0, :cond_0

    .line 2370214
    check-cast p1, Lcom/facebook/fbservice/service/ServiceException;

    .line 2370215
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2370216
    iget-object v2, v0, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    move-object v0, v2

    .line 2370217
    sget-object v2, LX/1nY;->API_ERROR:LX/1nY;

    if-ne v0, v2, :cond_3

    .line 2370218
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2370219
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultData()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "result"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/protocol/ApiErrorResult;

    .line 2370220
    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v0

    .line 2370221
    sparse-switch v0, :sswitch_data_0

    move-object v0, v1

    :goto_0
    move-object v1, v0

    .line 2370222
    :cond_0
    :goto_1
    invoke-static {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->t(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2370223
    invoke-static {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->q(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)LX/Gb4;

    move-result-object v0

    invoke-interface {v0, v1}, LX/Gb4;->a(Ljava/lang/String;)V

    .line 2370224
    :cond_1
    if-eqz v1, :cond_2

    .line 2370225
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2370226
    :cond_2
    return-void

    .line 2370227
    :sswitch_0
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->s()V

    goto :goto_1

    .line 2370228
    :sswitch_1
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->s()V

    goto :goto_1

    .line 2370229
    :sswitch_2
    const v0, 0x7f083494

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 2370230
    :sswitch_3
    const v0, 0x7f083473

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2370231
    :cond_3
    sget-object v2, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-ne v0, v2, :cond_0

    .line 2370232
    const v0, 0x7f08003f

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x170 -> :sswitch_2
        0x191 -> :sswitch_3
        0x17d4 -> :sswitch_1
        0x17d5 -> :sswitch_0
    .end sparse-switch
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2370233
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2370234
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->v:LX/2Di;

    invoke-virtual {v0}, LX/2Di;->c()V

    .line 2370235
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->u:LX/0fW;

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->C:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v1, v1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0fW;->c(Ljava/lang/String;)V

    .line 2370236
    :cond_0
    return-void
.end method

.method public static b$redex0(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;Z)V
    .locals 3

    .prologue
    .line 2370237
    const-string v0, ""

    .line 2370238
    iget v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->F:I

    const v2, 0x7f08346c

    if-ne v1, v2, :cond_2

    .line 2370239
    const v0, 0x7f083495

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2370240
    :cond_0
    :goto_0
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2370241
    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2370242
    :cond_1
    return-void

    .line 2370243
    :cond_2
    iget v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->F:I

    const v2, 0x7f08347b

    if-ne v1, v2, :cond_3

    .line 2370244
    const v0, 0x7f083496

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2370245
    :cond_3
    iget v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->F:I

    const v2, 0x7f083479

    if-ne v1, v2, :cond_5

    .line 2370246
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    invoke-interface {v0}, LX/GcK;->b()LX/GcT;

    move-result-object v0

    sget-object v1, LX/GcT;->CHANGE_PASSCODE_FROM_LOGIN_FLOW:LX/GcT;

    if-ne v0, v1, :cond_4

    if-nez p1, :cond_4

    .line 2370247
    const v0, 0x7f083496

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2370248
    :cond_4
    const v0, 0x7f083497

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2370249
    :cond_5
    iget v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->F:I

    const v2, 0x7f083484

    if-ne v1, v2, :cond_0

    .line 2370250
    if-eqz p1, :cond_6

    .line 2370251
    const v0, 0x7f083495

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2370252
    :cond_6
    const v0, 0x7f083498

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;Z)V
    .locals 3

    .prologue
    .line 2370253
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2370254
    const-string v1, "fbid"

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->C:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v2, v2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2370255
    const-string v1, "has_pin"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2370256
    const-string v1, "switch_to"

    const-string v2, "dbl"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2370257
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->z:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2370258
    const-string v1, "source"

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->z:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2370259
    :cond_0
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->s:LX/10O;

    const-string v2, "switch_account"

    invoke-virtual {v1, v2, v0}, LX/10O;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2370260
    return-void
.end method

.method public static synthetic h(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)I
    .locals 2

    .prologue
    .line 2370261
    iget v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->x:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->x:I

    return v0
.end method

.method private l()LX/0TF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0TF",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2370262
    new-instance v0, LX/GbY;

    invoke-direct {v0, p0}, LX/GbY;-><init>(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)V

    return-object v0
.end method

.method private m()LX/0TF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0TF",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2370190
    new-instance v0, LX/GbZ;

    invoke-direct {v0, p0}, LX/GbZ;-><init>(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)V

    return-object v0
.end method

.method public static n(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)V
    .locals 3

    .prologue
    .line 2370191
    invoke-static {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->o(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)V

    .line 2370192
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    iget v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->F:I

    const/4 v2, 0x0

    invoke-interface {v0, p0, v1, v2}, LX/GcK;->a(LX/Gbb;II)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2370193
    invoke-direct {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->b(Landroid/support/v4/app/Fragment;)V

    .line 2370194
    return-void
.end method

.method public static o(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 2370039
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    invoke-interface {v0}, LX/GcK;->b()LX/GcT;

    move-result-object v0

    .line 2370040
    sget-object v1, LX/GcT;->REMOVE_PIN:LX/GcT;

    if-ne v0, v1, :cond_2

    .line 2370041
    iget v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->x:I

    if-ge v0, v2, :cond_1

    .line 2370042
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->y:Ljava/lang/String;

    const-string v1, ""

    invoke-direct {p0, v0, v1}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2370043
    :cond_0
    :goto_0
    return-void

    .line 2370044
    :cond_1
    new-instance v0, LX/GcR;

    invoke-direct {v0}, LX/GcR;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    goto :goto_0

    .line 2370045
    :cond_2
    sget-object v1, LX/GcT;->CHANGE_PIN:LX/GcT;

    if-ne v0, v1, :cond_4

    .line 2370046
    iget v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->x:I

    if-lt v0, v2, :cond_3

    .line 2370047
    new-instance v0, LX/GcP;

    invoke-direct {v0}, LX/GcP;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    goto :goto_0

    .line 2370048
    :cond_3
    new-instance v0, LX/GcL;

    invoke-direct {v0}, LX/GcL;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    .line 2370049
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    iget v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->F:I

    const v2, 0x7f08348a

    invoke-interface {v0, p0, v1, v2}, LX/GcK;->a(LX/Gbb;II)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2370050
    invoke-direct {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->b(Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 2370051
    :cond_4
    sget-object v1, LX/GcT;->REMOVE_PIN_USING_PASSWORD:LX/GcT;

    if-ne v0, v1, :cond_5

    .line 2370052
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->y:Ljava/lang/String;

    const-string v1, ""

    invoke-direct {p0, v0, v1}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2370053
    :cond_5
    sget-object v1, LX/GcT;->CHANGE_PIN_USING_PASSWORD:LX/GcT;

    if-ne v0, v1, :cond_6

    .line 2370054
    new-instance v0, LX/GcM;

    invoke-direct {v0}, LX/GcM;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    .line 2370055
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    iget v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->F:I

    const v2, 0x7f08348a

    invoke-interface {v0, p0, v1, v2}, LX/GcK;->a(LX/Gbb;II)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2370056
    invoke-direct {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->b(Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 2370057
    :cond_6
    sget-object v1, LX/GcT;->SWITCH_TO_DBL:LX/GcT;

    if-ne v0, v1, :cond_0

    .line 2370058
    new-instance v0, LX/GcL;

    invoke-direct {v0}, LX/GcL;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    .line 2370059
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    iget v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->F:I

    const v2, 0x7f08348a

    invoke-interface {v0, p0, v1, v2}, LX/GcK;->a(LX/Gbb;II)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2370060
    invoke-direct {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->b(Landroid/support/v4/app/Fragment;)V

    goto :goto_0
.end method

.method private p()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2370061
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    invoke-interface {v0}, LX/GcK;->b()LX/GcT;

    move-result-object v0

    .line 2370062
    sget-object v1, LX/GcT;->CHANGE_PIN_USING_PASSWORD:LX/GcT;

    if-ne v0, v1, :cond_1

    .line 2370063
    iget-boolean v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->E:Z

    if-eqz v0, :cond_0

    .line 2370064
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->a()V

    .line 2370065
    :goto_0
    return-void

    .line 2370066
    :cond_0
    iput v2, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->x:I

    .line 2370067
    new-instance v0, LX/GcO;

    invoke-direct {v0}, LX/GcO;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    .line 2370068
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    iget v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->F:I

    invoke-interface {v0, p0, v1, v2}, LX/GcK;->a(LX/Gbb;II)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2370069
    invoke-direct {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->b(Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 2370070
    :cond_1
    sget-object v1, LX/GcT;->REMOVE_PIN_USING_PASSWORD:LX/GcT;

    if-ne v0, v1, :cond_3

    .line 2370071
    iget-boolean v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->E:Z

    if-eqz v0, :cond_2

    .line 2370072
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->a()V

    goto :goto_0

    .line 2370073
    :cond_2
    iput v2, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->x:I

    .line 2370074
    new-instance v0, LX/GcQ;

    invoke-direct {v0}, LX/GcQ;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    .line 2370075
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    iget v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->F:I

    invoke-interface {v0, p0, v1, v2}, LX/GcK;->a(LX/Gbb;II)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2370076
    invoke-direct {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->b(Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 2370077
    :cond_3
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->f()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_4

    .line 2370078
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->a()V

    goto :goto_0

    .line 2370079
    :cond_4
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->d()V

    goto :goto_0
.end method

.method public static q(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)LX/Gb4;
    .locals 2

    .prologue
    .line 2370080
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, LX/Gb4;

    return-object v0
.end method

.method public static r(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)V
    .locals 3

    .prologue
    .line 2370081
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2370082
    const-string v1, "fbid"

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->C:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v2, v2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2370083
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->z:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2370084
    const-string v1, "source"

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->z:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2370085
    :cond_0
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->s:LX/10O;

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->D:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/10O;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2370086
    return-void
.end method

.method private s()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2370087
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    invoke-interface {v0}, LX/GcK;->b()LX/GcT;

    move-result-object v0

    .line 2370088
    sget-object v1, LX/GcT;->REMOVE_PIN:LX/GcT;

    if-ne v0, v1, :cond_1

    .line 2370089
    new-instance v0, LX/GcR;

    invoke-direct {v0}, LX/GcR;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    .line 2370090
    iput-boolean v2, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->E:Z

    .line 2370091
    :goto_0
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    iget v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->F:I

    const v2, 0x7f083493

    invoke-interface {v0, p0, v1, v2}, LX/GcK;->a(LX/Gbb;II)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2370092
    invoke-direct {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->b(Landroid/support/v4/app/Fragment;)V

    .line 2370093
    :cond_0
    return-void

    .line 2370094
    :cond_1
    sget-object v1, LX/GcT;->ADD_PIN:LX/GcT;

    if-ne v0, v1, :cond_2

    .line 2370095
    new-instance v0, LX/GcP;

    invoke-direct {v0}, LX/GcP;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    .line 2370096
    iput-boolean v2, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->E:Z

    goto :goto_0

    .line 2370097
    :cond_2
    sget-object v1, LX/GcT;->CHANGE_PIN:LX/GcT;

    if-ne v0, v1, :cond_0

    .line 2370098
    new-instance v0, LX/GcP;

    invoke-direct {v0}, LX/GcP;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    .line 2370099
    iput-boolean v2, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->E:Z

    goto :goto_0
.end method

.method public static t(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)Z
    .locals 1

    .prologue
    .line 2370100
    invoke-static {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->q(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)LX/Gb4;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private u()V
    .locals 5

    .prologue
    .line 2370101
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2370102
    new-instance v1, Lcom/facebook/auth/credentials/PasswordCredentials;

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->C:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v2, v2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->y:Ljava/lang/String;

    sget-object v4, LX/28K;->PASSWORD:LX/28K;

    invoke-direct {v1, v2, v3, v4}, Lcom/facebook/auth/credentials/PasswordCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;LX/28K;)V

    .line 2370103
    const-string v2, "passwordCredentials"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2370104
    const-string v1, "error_detail_type_param"

    const-string v2, "button_with_disabled"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2370105
    const-string v1, "logged_out_set_nonce"

    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->v()LX/0TF;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->a(Landroid/os/Bundle;Ljava/lang/String;LX/0TF;)V

    .line 2370106
    return-void
.end method

.method private v()LX/0TF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0TF",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2370107
    new-instance v0, LX/Gba;

    invoke-direct {v0, p0}, LX/Gba;-><init>(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2370108
    invoke-static {p0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2370109
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->finish()V

    .line 2370110
    const v0, 0x7f04003a

    const v1, 0x7f040040

    invoke-virtual {p0, v0, v1}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->overridePendingTransition(II)V

    .line 2370111
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2370112
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->y:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2370113
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2370114
    invoke-static {p0, p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2370115
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 2370116
    const-string v0, "dbl_account_details"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->C:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 2370117
    const-string v0, "operation_type"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2370118
    const-string v3, "source"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->z:Ljava/lang/String;

    .line 2370119
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->C:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    if-nez v2, :cond_1

    .line 2370120
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->finish()V

    .line 2370121
    :goto_0
    return-void

    .line 2370122
    :cond_1
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->t:LX/2J4;

    iget-object v3, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->r:LX/10M;

    iget-object v4, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->s:LX/10O;

    invoke-virtual {v2, v3, v4}, LX/2J4;->a(LX/10M;LX/10O;)Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->w:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    .line 2370123
    iput-boolean v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->E:Z

    .line 2370124
    iget-object v2, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->C:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v2, v2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mNonce:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2370125
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->E:Z

    .line 2370126
    :cond_2
    const-string v2, "change_passcode_from_login_flow"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2370127
    const v0, 0x7f083479

    iput v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->F:I

    .line 2370128
    new-instance v0, LX/GcN;

    invoke-direct {v0}, LX/GcN;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    .line 2370129
    :cond_3
    :goto_1
    const v0, 0x7f0303e0

    invoke-virtual {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->setContentView(I)V

    .line 2370130
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    iget v2, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->F:I

    invoke-interface {v0, p0, v2, v1}, LX/GcK;->a(LX/Gbb;II)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2370131
    invoke-direct {p0, v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->b(Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 2370132
    :cond_4
    const-string v2, "add_pin"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2370133
    const v0, 0x7f08346c

    iput v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->F:I

    .line 2370134
    iget-boolean v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->E:Z

    if-eqz v0, :cond_5

    .line 2370135
    new-instance v0, LX/GcP;

    invoke-direct {v0}, LX/GcP;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    .line 2370136
    const v0, 0x7f083493

    .line 2370137
    :goto_2
    const-string v1, "dbl_settings_passcode_add"

    iput-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->D:Ljava/lang/String;

    move v1, v0

    goto :goto_1

    .line 2370138
    :cond_5
    new-instance v0, LX/GcL;

    invoke-direct {v0}, LX/GcL;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    move v0, v1

    goto :goto_2

    .line 2370139
    :cond_6
    const-string v2, "remove_pin"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2370140
    const v0, 0x7f08347b

    iput v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->F:I

    .line 2370141
    iget-boolean v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->E:Z

    if-eqz v0, :cond_7

    .line 2370142
    new-instance v0, LX/GcR;

    invoke-direct {v0}, LX/GcR;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    .line 2370143
    const v1, 0x7f083493

    .line 2370144
    :goto_3
    const-string v0, "dbl_settings_passcode_remove"

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->D:Ljava/lang/String;

    goto :goto_1

    .line 2370145
    :cond_7
    new-instance v0, LX/GcQ;

    invoke-direct {v0}, LX/GcQ;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    goto :goto_3

    .line 2370146
    :cond_8
    const-string v2, "change_pin"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2370147
    const v0, 0x7f083479

    iput v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->F:I

    .line 2370148
    iget-boolean v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->E:Z

    if-eqz v0, :cond_9

    .line 2370149
    new-instance v0, LX/GcP;

    invoke-direct {v0}, LX/GcP;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    .line 2370150
    const v1, 0x7f083493

    .line 2370151
    :goto_4
    const-string v0, "dbl_settings_passcode_change"

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->D:Ljava/lang/String;

    goto/16 :goto_1

    .line 2370152
    :cond_9
    new-instance v0, LX/GcO;

    invoke-direct {v0}, LX/GcO;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    goto :goto_4

    .line 2370153
    :cond_a
    const-string v2, "switch_to_dbl"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    const-string v2, "switch_to_dbl_with_pin"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2370154
    :cond_b
    const v0, 0x7f083484

    iput v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->F:I

    .line 2370155
    new-instance v0, LX/GcS;

    invoke-direct {v0}, LX/GcS;-><init>()V

    iput-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    .line 2370156
    const v1, 0x7f083485

    goto/16 :goto_1
.end method

.method public final b(Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 2370157
    invoke-static {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->t(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2370158
    invoke-static {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->q(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)LX/Gb4;

    move-result-object v0

    invoke-interface {v0}, LX/Gb4;->c()V

    .line 2370159
    :cond_0
    iput-object p1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->y:Ljava/lang/String;

    .line 2370160
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->w:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->C:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->m()LX/0TF;

    move-result-object v2

    .line 2370161
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2370162
    const-string v3, "account_id"

    iget-object v4, v1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2370163
    const-string v3, "nonce"

    iget-object v4, v1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mNonce:Ljava/lang/String;

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2370164
    const-string v3, "pin"

    invoke-virtual {v5, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2370165
    iget-object v3, v0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->f:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0aG;

    const-string v4, "check_nonce"

    sget-object v6, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v7, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->e:Lcom/facebook/common/callercontext/CallerContext;

    const v8, 0x33d629f

    invoke-static/range {v3 .. v8}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v3

    .line 2370166
    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    move-result-object v3

    .line 2370167
    if-eqz v2, :cond_1

    .line 2370168
    iget-object v4, v0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->g:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v2, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2370169
    :cond_1
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 2370170
    invoke-static {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->t(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2370171
    invoke-static {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->q(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)LX/Gb4;

    move-result-object v0

    invoke-interface {v0}, LX/Gb4;->c()V

    .line 2370172
    :cond_0
    iput-object p1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->y:Ljava/lang/String;

    .line 2370173
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    invoke-interface {v0}, LX/GcK;->b()LX/GcT;

    move-result-object v0

    sget-object v1, LX/GcT;->SWITCH_TO_DBL:LX/GcT;

    if-ne v0, v1, :cond_1

    .line 2370174
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->u()V

    .line 2370175
    :goto_0
    return-void

    .line 2370176
    :cond_1
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->w:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->C:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->m()LX/0TF;

    move-result-object v2

    .line 2370177
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2370178
    const-string v3, "account_id"

    iget-object v4, v1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2370179
    const-string v3, "password"

    invoke-virtual {v5, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2370180
    iget-object v3, v0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->f:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0aG;

    const-string v4, "check_password"

    sget-object v6, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v7, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->e:Lcom/facebook/common/callercontext/CallerContext;

    const v8, -0x2cb45b5a

    invoke-static/range {v3 .. v8}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v3

    .line 2370181
    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    move-result-object v3

    .line 2370182
    if-eqz v2, :cond_2

    .line 2370183
    iget-object v4, v0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->g:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v2, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2370184
    :cond_2
    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 0

    .prologue
    .line 2370185
    invoke-direct {p0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->p()V

    .line 2370186
    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x73a11fcd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2370187
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStop()V

    .line 2370188
    invoke-static {p0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2370189
    const/16 v1, 0x23

    const v2, 0x229e9a3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
