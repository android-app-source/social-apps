.class public Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field public p:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0dz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/Gxx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/27g;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private t:Lcom/facebook/widget/listview/BetterListView;

.field private u:LX/0h5;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2408371
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2408372
    iget-object v0, p0, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0eC;->b:LX/0Tn;

    const-string v2, "device"

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2408373
    invoke-direct {p0}, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0dz;LX/Gxx;LX/27g;)V
    .locals 0

    .prologue
    .line 2408374
    iput-object p1, p0, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p2, p0, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->q:LX/0dz;

    iput-object p3, p0, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->r:LX/Gxx;

    iput-object p4, p0, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->s:LX/27g;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;

    invoke-static {v3}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v3}, LX/0dz;->a(LX/0QB;)LX/0dz;

    move-result-object v1

    check-cast v1, LX/0dz;

    invoke-static {v3}, LX/Gxx;->b(LX/0QB;)LX/Gxx;

    move-result-object v2

    check-cast v2, LX/Gxx;

    invoke-static {v3}, LX/27g;->b(LX/0QB;)LX/27g;

    move-result-object v3

    check-cast v3, LX/27g;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->a(Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0dz;LX/Gxx;LX/27g;)V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2408375
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2408376
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->u:LX/0h5;

    .line 2408377
    iget-object v0, p0, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->u:LX/0h5;

    new-instance v1, LX/Gxe;

    invoke-direct {v1, p0}, LX/Gxe;-><init>(Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2408378
    iget-object v0, p0, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->u:LX/0h5;

    invoke-virtual {p0}, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0835c4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2408379
    return-void
.end method

.method private l()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 2408380
    const v0, 0x7f0d0846

    invoke-virtual {p0, v0}, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->t:Lcom/facebook/widget/listview/BetterListView;

    .line 2408381
    iget-object v0, p0, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->t:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v6}, Lcom/facebook/widget/listview/BetterListView;->setChoiceMode(I)V

    .line 2408382
    iget-object v0, p0, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->s:LX/27g;

    invoke-virtual {v0}, LX/27g;->a()LX/27h;

    move-result-object v0

    .line 2408383
    invoke-virtual {v0}, LX/27h;->a()[Ljava/lang/String;

    move-result-object v1

    .line 2408384
    invoke-virtual {v0}, LX/27h;->b()[Ljava/lang/String;

    move-result-object v2

    .line 2408385
    iget v3, v0, LX/27h;->b:I

    move v0, v3

    .line 2408386
    new-instance v3, Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->t:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v4}, Lcom/facebook/widget/listview/BetterListView;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f030a41

    invoke-direct {v3, v4, v5, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 2408387
    iget-object v2, p0, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->t:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2408388
    iget-object v2, p0, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->t:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v2, v0, v6}, Lcom/facebook/widget/listview/BetterListView;->setItemChecked(IZ)V

    .line 2408389
    iget-object v0, p0, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->t:Lcom/facebook/widget/listview/BetterListView;

    new-instance v2, LX/Gxf;

    invoke-direct {v2, p0, v1}, LX/Gxf;-><init>(Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;[Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2408390
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2408391
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2408392
    invoke-static {p0, p0}, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2408393
    const v0, 0x7f0309b4

    invoke-virtual {p0, v0}, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->setContentView(I)V

    .line 2408394
    invoke-direct {p0}, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->b()V

    .line 2408395
    invoke-direct {p0}, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->l()V

    .line 2408396
    return-void
.end method
