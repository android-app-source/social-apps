.class public Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field private static final g:I

.field private static final h:I

.field private static final i:I


# instance fields
.field public a:LX/0dz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Gxv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/GvB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/27g;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final f:Ljava/lang/String;

.field private j:Lcom/facebook/resources/ui/FbButton;

.field private k:Lcom/facebook/resources/ui/FbTextView;

.field public l:Landroid/view/View;

.field public m:Landroid/view/View;

.field private n:Landroid/widget/RadioGroup;

.field private o:Lcom/facebook/resources/ui/FbTextView;

.field public p:I

.field public q:I

.field public r:Z

.field public s:I

.field public t:I

.field public u:I

.field public v:Z

.field public w:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private x:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/RadioButton;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2408650
    const v0, 0x7f0835c8

    sput v0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->g:I

    .line 2408651
    const v0, 0x7f0835c9

    sput v0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->h:I

    .line 2408652
    const v0, 0x7f0835c6

    sput v0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->i:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2408653
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2408654
    const-string v0, "en"

    iput-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->f:Ljava/lang/String;

    .line 2408655
    iput v1, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->p:I

    .line 2408656
    iput v1, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->q:I

    .line 2408657
    iput-boolean v1, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->r:Z

    .line 2408658
    iput v1, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->s:I

    .line 2408659
    iput v1, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->t:I

    .line 2408660
    iput-object v2, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->w:Ljava/util/List;

    .line 2408661
    iput-object v2, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->x:Ljava/util/List;

    .line 2408662
    return-void
.end method

.method private declared-synchronized a(Landroid/view/LayoutInflater;Ljava/util/Locale;)Landroid/widget/RadioButton;
    .locals 4

    .prologue
    .line 2408643
    monitor-enter p0

    :try_start_0
    const v0, 0x7f0309b7

    iget-object v1, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->n:Landroid/widget/RadioGroup;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 2408644
    invoke-static {p2}, LX/0e9;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 2408645
    iget-object v1, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->x:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2408646
    iget-object v1, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->n:Landroid/widget/RadioGroup;

    invoke-virtual {v1, v0}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;)V

    .line 2408647
    const v1, 0x7f0309b5

    iget-object v2, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->n:Landroid/widget/RadioGroup;

    const/4 v3, 0x1

    invoke-virtual {p1, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2408648
    monitor-exit p0

    return-object v0

    .line 2408649
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Landroid/widget/TextView;Ljava/lang/String;I)V
    .locals 7

    .prologue
    .line 2408628
    monitor-enter p0

    :try_start_0
    iget v2, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->p:I

    .line 2408629
    iget-boolean v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->v:Z

    if-nez v0, :cond_0

    .line 2408630
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2408631
    :goto_0
    monitor-exit p0

    return-void

    .line 2408632
    :cond_0
    :try_start_1
    iget v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->u:I

    const v1, 0xffffff

    and-int/2addr v0, v1

    const/high16 v1, 0x54000000

    or-int/2addr v0, v1

    .line 2408633
    const-string v1, "textColor"

    const/4 v3, 0x2

    new-array v3, v3, [I

    const/4 v4, 0x0

    iget v5, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->u:I

    aput v5, v3, v4

    const/4 v4, 0x1

    aput v0, v3, v4

    invoke-static {p1, v1, v3}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 2408634
    const-wide/16 v4, 0xfa

    invoke-virtual {v6, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 2408635
    int-to-long v4, p3

    invoke-virtual {v6, v4, v5}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 2408636
    new-instance v1, Landroid/animation/ArgbEvaluator;

    invoke-direct {v1}, Landroid/animation/ArgbEvaluator;-><init>()V

    invoke-virtual {v6, v1}, Landroid/animation/ObjectAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 2408637
    const-string v1, "textColor"

    const/4 v3, 0x2

    new-array v3, v3, [I

    const/4 v4, 0x0

    aput v0, v3, v4

    const/4 v0, 0x1

    iget v4, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->u:I

    aput v4, v3, v0

    invoke-static {p1, v1, v3}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 2408638
    const-wide/16 v0, 0xfa

    invoke-virtual {v5, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 2408639
    new-instance v0, Landroid/animation/ArgbEvaluator;

    invoke-direct {v0}, Landroid/animation/ArgbEvaluator;-><init>()V

    invoke-virtual {v5, v0}, Landroid/animation/ObjectAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 2408640
    new-instance v0, LX/Gxn;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, LX/Gxn;-><init>(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;ILandroid/widget/TextView;Ljava/lang/String;Landroid/animation/ObjectAnimator;)V

    invoke-virtual {v6, v0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2408641
    invoke-virtual {v6}, Landroid/animation/ObjectAnimator;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2408642
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static a(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;Landroid/widget/TextView;II)V
    .locals 1

    .prologue
    .line 2408626
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->a(Landroid/widget/TextView;Ljava/lang/String;I)V

    .line 2408627
    return-void
.end method

.method public static declared-synchronized a$redex0(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;Landroid/view/LayoutInflater;Ljava/util/Locale;I)Landroid/widget/RadioButton;
    .locals 3

    .prologue
    .line 2408620
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->w:Ljava/util/List;

    invoke-virtual {p2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2408621
    invoke-direct {p0, p1, p2}, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->a(Landroid/view/LayoutInflater;Ljava/util/Locale;)Landroid/widget/RadioButton;

    move-result-object v0

    .line 2408622
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 2408623
    invoke-virtual {v0}, Landroid/widget/RadioButton;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, LX/Gxq;

    invoke-direct {v2, p0, v0, p3}, LX/Gxq;-><init>(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;Landroid/widget/RadioButton;I)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2408624
    monitor-exit p0

    return-object v0

    .line 2408625
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a$redex0(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;I)V
    .locals 2

    .prologue
    .line 2408610
    monitor-enter p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2408611
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->x:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->getId()I

    move-result v0

    if-ne v0, p1, :cond_2

    .line 2408612
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->w:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2408613
    iget-boolean p1, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->v:Z

    if-eqz p1, :cond_0

    .line 2408614
    iget p1, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->t:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->t:I

    .line 2408615
    :cond_0
    iget-object p1, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->a:LX/0dz;

    invoke-virtual {p1, v0}, LX/0dz;->a(Ljava/lang/String;)V

    .line 2408616
    invoke-static {p0}, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->b(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;)V

    .line 2408617
    iput v1, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->q:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2408618
    :cond_1
    monitor-exit p0

    return-void

    .line 2408619
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized b(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;)V
    .locals 3

    .prologue
    .line 2408663
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->p:I

    .line 2408664
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->o:Lcom/facebook/resources/ui/FbTextView;

    sget v1, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->i:I

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->a(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;Landroid/widget/TextView;II)V

    .line 2408665
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->j:Lcom/facebook/resources/ui/FbButton;

    sget v1, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->g:I

    const/16 v2, 0x7d

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->a(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;Landroid/widget/TextView;II)V

    .line 2408666
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->k:Lcom/facebook/resources/ui/FbTextView;

    sget v1, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->h:I

    const/16 v2, 0xfa

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->a(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;Landroid/widget/TextView;II)V

    .line 2408667
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->v:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2408668
    monitor-exit p0

    return-void

    .line 2408669
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized b(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;Landroid/view/LayoutInflater;Ljava/util/Locale;)V
    .locals 4

    .prologue
    .line 2408522
    monitor-enter p0

    :try_start_0
    invoke-static {p2}, LX/0e9;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 2408523
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 2408524
    invoke-virtual {v0}, Landroid/widget/RadioButton;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2408525
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2408526
    :goto_0
    monitor-exit p0

    return-void

    .line 2408527
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x4

    if-ge v0, v2, :cond_2

    .line 2408528
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->a$redex0(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;Landroid/view/LayoutInflater;Ljava/util/Locale;I)Landroid/widget/RadioButton;

    move-result-object v0

    .line 2408529
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2408530
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2408531
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v2, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->x:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 2408532
    iget-object v2, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->w:Ljava/util/List;

    invoke-virtual {p2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2408533
    iget-object v2, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->x:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 2408534
    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->a(Landroid/widget/TextView;Ljava/lang/String;I)V

    .line 2408535
    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2408536
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 2408537
    :cond_3
    invoke-virtual {v0}, Landroid/widget/RadioButton;->getId()I

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->a$redex0(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;)V
    .locals 5

    .prologue
    .line 2408538
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2408539
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2408540
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2408541
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 2408542
    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-static {v3}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v3

    .line 2408543
    new-instance v4, LX/Gxr;

    invoke-direct {v4, p0, v1, v2, v0}, LX/Gxr;-><init>(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;III)V

    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2408544
    const-wide/16 v0, 0xfa

    invoke-virtual {v3, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2408545
    new-instance v0, LX/Gxs;

    invoke-direct {v0, p0}, LX/Gxs;-><init>(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;)V

    invoke-virtual {v3, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2408546
    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->start()V

    .line 2408547
    return-void

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2408548
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2408549
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    invoke-static {v0}, LX/0dz;->a(LX/0QB;)LX/0dz;

    move-result-object v3

    check-cast v3, LX/0dz;

    new-instance p1, LX/Gxv;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/0e7;->b(LX/0QB;)Landroid/telephony/TelephonyManager;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    invoke-direct {p1, v4, v5}, LX/Gxv;-><init>(LX/0Zb;Landroid/telephony/TelephonyManager;)V

    move-object v4, p1

    check-cast v4, LX/Gxv;

    invoke-static {v0}, LX/GvB;->b(LX/0QB;)LX/GvB;

    move-result-object v5

    check-cast v5, LX/GvB;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p1

    check-cast p1, LX/1Ck;

    invoke-static {v0}, LX/27g;->b(LX/0QB;)LX/27g;

    move-result-object v0

    check-cast v0, LX/27g;

    iput-object v3, v2, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->a:LX/0dz;

    iput-object v4, v2, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->b:LX/Gxv;

    iput-object v5, v2, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->c:LX/GvB;

    iput-object p1, v2, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->d:LX/1Ck;

    iput-object v0, v2, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->e:LX/27g;

    .line 2408550
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setRetainInstance(Z)V

    .line 2408551
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->b:LX/Gxv;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "calling_intent"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2408552
    iget-object v2, v0, LX/Gxv;->a:LX/0Zb;

    sget-object v3, LX/Gxu;->STARTED:LX/Gxu;

    invoke-static {v3}, LX/Gxv;->a(LX/Gxu;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "source"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2408553
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x5e413966

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2408554
    const v0, 0x7f0309b6

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2408555
    const v0, 0x7f0d18ba

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->j:Lcom/facebook/resources/ui/FbButton;

    .line 2408556
    const v0, 0x7f0d18bb

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->k:Lcom/facebook/resources/ui/FbTextView;

    .line 2408557
    const v0, 0x7f0d18b9

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->l:Landroid/view/View;

    .line 2408558
    const v0, 0x7f0d18b8

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->m:Landroid/view/View;

    .line 2408559
    const v0, 0x7f0d18b7

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->n:Landroid/widget/RadioGroup;

    .line 2408560
    const v0, 0x7f0d18b6

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->o:Lcom/facebook/resources/ui/FbTextView;

    .line 2408561
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    iput v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->u:I

    .line 2408562
    iput-boolean v3, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->v:Z

    .line 2408563
    if-eqz p3, :cond_0

    .line 2408564
    const-string v0, "display_locales"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->w:Ljava/util/List;

    .line 2408565
    const-string v0, "checked_index"

    invoke-virtual {p3, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->q:I

    .line 2408566
    const-string v0, "fetched_suggestions"

    invoke-virtual {p3, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->r:Z

    .line 2408567
    const-string v0, "num_manual_selected"

    invoke-virtual {p3, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->s:I

    .line 2408568
    const-string v0, "num_selected"

    invoke-virtual {p3, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->t:I

    .line 2408569
    :cond_0
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->w:Ljava/util/List;

    if-nez v0, :cond_3

    .line 2408570
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->w:Ljava/util/List;

    .line 2408571
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->a:LX/0dz;

    invoke-virtual {v0}, LX/0dz;->a()Ljava/lang/String;

    move-result-object v0

    .line 2408572
    iget-object v3, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->a:LX/0dz;

    .line 2408573
    iget-object v4, v3, LX/0dz;->l:LX/0e8;

    .line 2408574
    invoke-virtual {v4}, LX/0e8;->c()LX/0Py;

    move-result-object v5

    invoke-static {}, LX/0W9;->e()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, LX/0e8;->a(LX/0Py;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v4, v5

    .line 2408575
    move-object v3, v4

    .line 2408576
    if-eqz v0, :cond_1

    .line 2408577
    iget-object v4, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->w:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2408578
    :cond_1
    if-eqz v3, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2408579
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->w:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2408580
    :cond_2
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2408581
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->w:Ljava/util/List;

    const-string v3, "en"

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2408582
    :cond_3
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->x:Ljava/util/List;

    .line 2408583
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2408584
    invoke-static {v0}, LX/0e9;->a(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->a(Landroid/view/LayoutInflater;Ljava/util/Locale;)Landroid/widget/RadioButton;

    goto :goto_0

    .line 2408585
    :cond_4
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->n:Landroid/widget/RadioGroup;

    new-instance v3, LX/Gxg;

    invoke-direct {v3, p0}, LX/Gxg;-><init>(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 2408586
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->k:Lcom/facebook/resources/ui/FbTextView;

    new-instance v3, LX/Gxj;

    invoke-direct {v3, p0, p1}, LX/Gxj;-><init>(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;Landroid/view/LayoutInflater;)V

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2408587
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->j:Lcom/facebook/resources/ui/FbButton;

    new-instance v3, LX/Gxk;

    invoke-direct {v3, p0}, LX/Gxk;-><init>(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2408588
    monitor-enter p0

    .line 2408589
    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->r:Z

    if-nez v0, :cond_5

    .line 2408590
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->m:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2408591
    :cond_5
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2408592
    iget-boolean v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->r:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->d:LX/1Ck;

    sget-object v3, LX/Gxt;->SUGGESTIONS:LX/Gxt;

    invoke-virtual {v3}, LX/Gxt;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2408593
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->d:LX/1Ck;

    sget-object v3, LX/Gxt;->SUGGESTIONS:LX/Gxt;

    invoke-virtual {v3}, LX/Gxt;->name()Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/Gxl;

    invoke-direct {v4, p0}, LX/Gxl;-><init>(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;)V

    new-instance v5, LX/Gxm;

    invoke-direct {v5, p0, p1}, LX/Gxm;-><init>(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;Landroid/view/LayoutInflater;)V

    invoke-virtual {v0, v3, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2408594
    :cond_6
    const v0, -0x1228edde

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-object v2

    .line 2408595
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const v2, -0x6c300a61

    invoke-static {v2, v1}, LX/02F;->f(II)V

    throw v0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x688fd551

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2408596
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2408597
    iget-object v1, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->d:LX/1Ck;

    sget-object v2, LX/Gxt;->SUGGESTIONS:LX/Gxt;

    invoke-virtual {v2}, LX/Gxt;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2408598
    const/16 v1, 0x2b

    const v2, -0x2441d301

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x35ff0198    # 1.8999444E-6f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2408606
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2408607
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->x:Ljava/util/List;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->q:I

    iget-object v2, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->x:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2408608
    iget-object v0, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->x:Ljava/util/List;

    iget v2, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->q:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 2408609
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x1cfa2b34

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2408599
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2408600
    const-string v0, "checked_index"

    iget v1, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->q:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2408601
    const-string v0, "fetched_suggestions"

    iget-boolean v1, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->r:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2408602
    const-string v0, "num_manual_selected"

    iget v1, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->s:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2408603
    const-string v0, "num_selected"

    iget v1, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->t:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2408604
    const-string v0, "display_locales"

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->w:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2408605
    return-void
.end method
