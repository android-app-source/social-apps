.class public Lcom/facebook/languages/switcher/LanguageSwitchPromotionActivity;
.super Landroid/app/Activity;
.source ""


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Gxx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2408338
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/languages/switcher/LanguageSwitchPromotionActivity;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/Gxx;)V
    .locals 0

    .prologue
    .line 2408339
    iput-object p1, p0, Lcom/facebook/languages/switcher/LanguageSwitchPromotionActivity;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p2, p0, Lcom/facebook/languages/switcher/LanguageSwitchPromotionActivity;->b:LX/Gxx;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/languages/switcher/LanguageSwitchPromotionActivity;

    invoke-static {v1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v1}, LX/Gxx;->b(LX/0QB;)LX/Gxx;

    move-result-object v1

    check-cast v1, LX/Gxx;

    invoke-static {p0, v0, v1}, Lcom/facebook/languages/switcher/LanguageSwitchPromotionActivity;->a(Lcom/facebook/languages/switcher/LanguageSwitchPromotionActivity;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/Gxx;)V

    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x22

    const v1, -0x2a1923a3

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2408340
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 2408341
    invoke-static {p0, p0}, Lcom/facebook/languages/switcher/LanguageSwitchPromotionActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2408342
    invoke-virtual {p0}, Lcom/facebook/languages/switcher/LanguageSwitchPromotionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2408343
    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 2408344
    const-string v2, "locale"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2408345
    invoke-static {v1}, LX/0e9;->a(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v1

    .line 2408346
    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2408347
    iget-object v2, p0, Lcom/facebook/languages/switcher/LanguageSwitchPromotionActivity;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0eC;->b:LX/0Tn;

    const-string v4, "device"

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2408348
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2408349
    iget-object v3, p0, Lcom/facebook/languages/switcher/LanguageSwitchPromotionActivity;->b:LX/Gxx;

    .line 2408350
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v6, LX/Gxw;->SWITCH_PROMO:LX/Gxw;

    invoke-virtual {v6}, LX/Gxw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v6, "language_switcher"

    .line 2408351
    iput-object v6, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2408352
    move-object v4, v4

    .line 2408353
    const-string v6, "current_app_locale"

    invoke-virtual {v4, v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v6, "new_app_locale"

    invoke-virtual {v4, v6, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v6, "current_fb_locale"

    invoke-static {v3, v2}, LX/Gxx;->b(LX/Gxx;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, v6, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v6, "new_fb_locale"

    invoke-static {v3, v1}, LX/Gxx;->b(LX/Gxx;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, v6, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v6, "system_locale"

    invoke-static {}, LX/0W9;->e()Ljava/util/Locale;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, v6, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2408354
    iget-object v6, v3, LX/Gxx;->a:LX/0Zb;

    invoke-interface {v6, v4}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2408355
    iget-object v2, p0, Lcom/facebook/languages/switcher/LanguageSwitchPromotionActivity;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/0eC;->b:LX/0Tn;

    invoke-interface {v2, v3, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2408356
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/languages/switcher/LanguageSwitchPromotionActivity;->finish()V

    .line 2408357
    const/16 v1, 0x23

    const v2, -0x1c4ccced

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
