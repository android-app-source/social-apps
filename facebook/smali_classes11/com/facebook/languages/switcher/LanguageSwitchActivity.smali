.class public Lcom/facebook/languages/switcher/LanguageSwitchActivity;
.super Landroid/app/Activity;
.source ""


# static fields
.field public static final e:Ljava/lang/String;


# instance fields
.field public a:Ljava/util/concurrent/ScheduledExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0dz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0W9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final f:Landroid/os/Handler;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2408336
    const-class v0, Lcom/facebook/languages/switcher/LanguageSwitchActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/languages/switcher/LanguageSwitchActivity;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2408333
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 2408334
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/languages/switcher/LanguageSwitchActivity;->f:Landroid/os/Handler;

    .line 2408335
    return-void
.end method

.method private static a(Lcom/facebook/languages/switcher/LanguageSwitchActivity;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;LX/0dz;LX/0W9;)V
    .locals 0

    .prologue
    .line 2408337
    iput-object p1, p0, Lcom/facebook/languages/switcher/LanguageSwitchActivity;->a:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object p2, p0, Lcom/facebook/languages/switcher/LanguageSwitchActivity;->b:LX/03V;

    iput-object p3, p0, Lcom/facebook/languages/switcher/LanguageSwitchActivity;->c:LX/0dz;

    iput-object p4, p0, Lcom/facebook/languages/switcher/LanguageSwitchActivity;->d:LX/0W9;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/languages/switcher/LanguageSwitchActivity;

    invoke-static {v3}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v3}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {v3}, LX/0dz;->a(LX/0QB;)LX/0dz;

    move-result-object v2

    check-cast v2, LX/0dz;

    invoke-static {v3}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v3

    check-cast v3, LX/0W9;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/languages/switcher/LanguageSwitchActivity;->a(Lcom/facebook/languages/switcher/LanguageSwitchActivity;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;LX/0dz;LX/0W9;)V

    return-void
.end method

.method public static b(Lcom/facebook/languages/switcher/LanguageSwitchActivity;)V
    .locals 5

    .prologue
    .line 2408331
    iget-object v0, p0, Lcom/facebook/languages/switcher/LanguageSwitchActivity;->f:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/languages/switcher/LanguageSwitchActivity$RestartRunnable;

    invoke-direct {v1}, Lcom/facebook/languages/switcher/LanguageSwitchActivity$RestartRunnable;-><init>()V

    const-wide/16 v2, 0x1388

    const v4, -0x68067319

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2408332
    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/16 v0, 0x22

    const v1, -0x3e4f0ad4

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2408319
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 2408320
    invoke-static {p0, p0}, Lcom/facebook/languages/switcher/LanguageSwitchActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2408321
    const v0, 0x7f0309b2

    invoke-virtual {p0, v0}, Lcom/facebook/languages/switcher/LanguageSwitchActivity;->setContentView(I)V

    .line 2408322
    iget-object v0, p0, Lcom/facebook/languages/switcher/LanguageSwitchActivity;->d:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->b()Ljava/util/Locale;

    move-result-object v0

    .line 2408323
    invoke-static {v0}, LX/0e9;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 2408324
    invoke-virtual {p0}, Lcom/facebook/languages/switcher/LanguageSwitchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 2408325
    const v0, 0x7f0d18b5

    invoke-virtual {p0, v0}, Lcom/facebook/languages/switcher/LanguageSwitchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2408326
    const v4, 0x7f0835c7

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2408327
    iget-object v0, p0, Lcom/facebook/languages/switcher/LanguageSwitchActivity;->c:LX/0dz;

    .line 2408328
    iget-object v2, v0, LX/0dz;->q:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v0, v2

    .line 2408329
    new-instance v2, LX/Gxc;

    invoke-direct {v2, p0}, LX/Gxc;-><init>(Lcom/facebook/languages/switcher/LanguageSwitchActivity;)V

    iget-object v3, p0, Lcom/facebook/languages/switcher/LanguageSwitchActivity;->a:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2408330
    const/16 v0, 0x23

    const v2, -0x47b73084

    invoke-static {v7, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
