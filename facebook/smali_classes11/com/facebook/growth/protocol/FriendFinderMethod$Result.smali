.class public final Lcom/facebook/growth/protocol/FriendFinderMethod$Result;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/growth/protocol/FriendFinderMethod_ResultDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/growth/protocol/FriendFinderMethod$Result;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mContacts:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "friendable"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;",
            ">;"
        }
    .end annotation
.end field

.field private final mCursor:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "cursor"
    .end annotation
.end field

.field private final mInvites:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "invitable"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/growth/protocol/FriendFinderMethod$Result$Invites;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2205645
    const-class v0, Lcom/facebook/growth/protocol/FriendFinderMethod_ResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2205623
    new-instance v0, LX/F9c;

    invoke-direct {v0}, LX/F9c;-><init>()V

    sput-object v0, Lcom/facebook/growth/protocol/FriendFinderMethod$Result;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2205638
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2205639
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2205640
    iput-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Result;->mContacts:Ljava/util/List;

    .line 2205641
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2205642
    iput-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Result;->mInvites:Ljava/util/List;

    .line 2205643
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Result;->mCursor:I

    .line 2205644
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2205631
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2205632
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Result;->mContacts:Ljava/util/List;

    .line 2205633
    iget-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Result;->mContacts:Ljava/util/List;

    sget-object v1, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 2205634
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Result;->mInvites:Ljava/util/List;

    .line 2205635
    iget-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Result;->mInvites:Ljava/util/List;

    sget-object v1, Lcom/facebook/growth/protocol/FriendFinderMethod$Result$Invites;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 2205636
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Result;->mCursor:I

    .line 2205637
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2205646
    iget-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Result;->mContacts:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/growth/protocol/FriendFinderMethod$Result$Invites;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2205630
    iget-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Result;->mInvites:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2205629
    iget v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Result;->mCursor:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2205628
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2205624
    iget-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Result;->mContacts:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2205625
    iget-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Result;->mInvites:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2205626
    iget v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Result;->mCursor:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2205627
    return-void
.end method
