.class public final Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Result;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/growth/protocol/FriendFinderPYMKMethod_ResultDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Result;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mPymk:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "friendable"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2205751
    const-class v0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod_ResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2205752
    new-instance v0, LX/F9g;

    invoke-direct {v0}, LX/F9g;-><init>()V

    sput-object v0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Result;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2205753
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2205754
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2205755
    iput-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Result;->mPymk:Ljava/util/List;

    .line 2205756
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2205757
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2205758
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Result;->mPymk:Ljava/util/List;

    .line 2205759
    iget-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Result;->mPymk:Ljava/util/List;

    sget-object v1, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 2205760
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2205761
    iget-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Result;->mPymk:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2205762
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2205763
    iget-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Result;->mPymk:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2205764
    return-void
.end method
