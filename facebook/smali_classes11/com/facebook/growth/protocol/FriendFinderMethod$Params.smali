.class public final Lcom/facebook/growth/protocol/FriendFinderMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/growth/protocol/FriendFinderMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/model/FacebookPhonebookContact;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:LX/89v;

.field public final e:Ljava/lang/String;

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2205570
    new-instance v0, LX/F9b;

    invoke-direct {v0}, LX/F9b;-><init>()V

    sput-object v0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2205571
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2205572
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->a:Ljava/util/List;

    .line 2205573
    iget-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->a:Ljava/util/List;

    sget-object v1, Lcom/facebook/ipc/model/FacebookPhonebookContact;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 2205574
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->b:Ljava/lang/String;

    .line 2205575
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->c:I

    .line 2205576
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/89v;

    iput-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->d:LX/89v;

    .line 2205577
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->e:Ljava/lang/String;

    .line 2205578
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->f:I

    .line 2205579
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->g:I

    .line 2205580
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->h:I

    .line 2205581
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->i:I

    .line 2205582
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/String;ILX/89v;Ljava/lang/String;IIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/model/FacebookPhonebookContact;",
            ">;",
            "Ljava/lang/String;",
            "I",
            "LX/89v;",
            "Ljava/lang/String;",
            "IIII)V"
        }
    .end annotation

    .prologue
    .line 2205583
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2205584
    iput-object p1, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->a:Ljava/util/List;

    .line 2205585
    iput-object p2, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->b:Ljava/lang/String;

    .line 2205586
    iput p3, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->c:I

    .line 2205587
    iput-object p4, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->d:LX/89v;

    .line 2205588
    iput-object p5, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->e:Ljava/lang/String;

    .line 2205589
    iput p6, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->f:I

    .line 2205590
    iput p7, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->g:I

    .line 2205591
    iput p8, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->h:I

    .line 2205592
    iput p9, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->i:I

    .line 2205593
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2205594
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2205595
    iget-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->a:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2205596
    iget-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2205597
    iget v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2205598
    iget-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->d:LX/89v;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2205599
    iget-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2205600
    iget v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2205601
    iget v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2205602
    iget v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2205603
    iget v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2205604
    return-void
.end method
