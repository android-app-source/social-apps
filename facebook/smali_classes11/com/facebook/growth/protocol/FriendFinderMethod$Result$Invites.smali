.class public final Lcom/facebook/growth/protocol/FriendFinderMethod$Result$Invites;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/growth/protocol/FriendFinderMethod_Result_InvitesDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/growth/protocol/FriendFinderMethod$Result$Invites;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mRecordId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "record_id"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2205611
    const-class v0, Lcom/facebook/growth/protocol/FriendFinderMethod_Result_InvitesDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2205612
    new-instance v0, LX/F9d;

    invoke-direct {v0}, LX/F9d;-><init>()V

    sput-object v0, Lcom/facebook/growth/protocol/FriendFinderMethod$Result$Invites;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2205613
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2205614
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Result$Invites;->mRecordId:J

    .line 2205615
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2205616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2205617
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Result$Invites;->mRecordId:J

    .line 2205618
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 2205619
    iget-wide v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Result$Invites;->mRecordId:J

    return-wide v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2205620
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2205621
    iget-wide v0, p0, Lcom/facebook/growth/protocol/FriendFinderMethod$Result$Invites;->mRecordId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2205622
    return-void
.end method
