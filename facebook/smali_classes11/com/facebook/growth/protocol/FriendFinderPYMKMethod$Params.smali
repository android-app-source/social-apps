.class public final Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field public final c:LX/89v;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2205740
    new-instance v0, LX/F9f;

    invoke-direct {v0}, LX/F9f;-><init>()V

    sput-object v0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;LX/89v;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "LX/89v;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2205733
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2205734
    iput p1, p0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;->a:I

    .line 2205735
    iput-object p2, p0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;->b:Ljava/lang/String;

    .line 2205736
    iput-object p3, p0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;->c:LX/89v;

    .line 2205737
    iput-object p4, p0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;->d:Ljava/util/List;

    .line 2205738
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2205741
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2205742
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;->a:I

    .line 2205743
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;->b:Ljava/lang/String;

    .line 2205744
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/89v;

    iput-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;->c:LX/89v;

    .line 2205745
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;->d:Ljava/util/List;

    .line 2205746
    iget-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;->d:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 2205747
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2205739
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2205728
    iget v0, p0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2205729
    iget-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2205730
    iget-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;->c:LX/89v;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2205731
    iget-object v0, p0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;->d:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 2205732
    return-void
.end method
