.class public Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/30Y;

.field private final c:Landroid/content/Context;

.field private final d:LX/94q;

.field private final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final f:LX/0aG;

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/30I;

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ml;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0Tn;

.field private final m:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2202528
    const-class v0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/30Y;Landroid/content/Context;LX/94q;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0aG;LX/0Or;LX/0Or;LX/0Ot;LX/30I;LX/0Ot;)V
    .locals 2
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/contacts/upload/annotation/IsContactsUploadBackgroundTaskEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/30Y;",
            "Landroid/content/Context;",
            "LX/94q;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0aG;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/30I;",
            "LX/0Ot",
            "<",
            "LX/1Ml;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2202529
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2202530
    iput-object p1, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->b:LX/30Y;

    .line 2202531
    iput-object p2, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->c:Landroid/content/Context;

    .line 2202532
    iput-object p4, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2202533
    iput-object p3, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->d:LX/94q;

    .line 2202534
    iput-object p5, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->f:LX/0aG;

    .line 2202535
    iput-object p6, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->g:LX/0Or;

    .line 2202536
    iput-object p7, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->h:LX/0Or;

    .line 2202537
    iput-object p8, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->i:LX/0Ot;

    .line 2202538
    iput-object p9, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->j:LX/30I;

    .line 2202539
    iput-object p10, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->k:LX/0Ot;

    .line 2202540
    invoke-interface {p6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/1nR;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->l:LX/0Tn;

    .line 2202541
    invoke-interface {p6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0, v1}, LX/1nR;->a(Ljava/lang/String;Lcom/facebook/prefs/shared/FbSharedPreferences;)LX/0Tn;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->m:LX/0Tn;

    .line 2202542
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;
    .locals 11

    .prologue
    .line 2202543
    new-instance v0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    invoke-static {p0}, LX/30Y;->b(LX/0QB;)LX/30Y;

    move-result-object v1

    check-cast v1, LX/30Y;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/94q;->a(LX/0QB;)LX/94q;

    move-result-object v3

    check-cast v3, LX/94q;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v5

    check-cast v5, LX/0aG;

    const/16 v6, 0x15e7

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x30d

    invoke-static {p0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x12c4

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {p0}, LX/30I;->b(LX/0QB;)LX/30I;

    move-result-object v9

    check-cast v9, LX/30I;

    const/16 v10, 0x1127

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v0 .. v10}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;-><init>(LX/30Y;Landroid/content/Context;LX/94q;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0aG;LX/0Or;LX/0Or;LX/0Ot;LX/30I;LX/0Ot;)V

    .line 2202544
    return-object v0
.end method

.method private c(Z)V
    .locals 2

    .prologue
    .line 2202545
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->m:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2202546
    return-void
.end method


# virtual methods
.method public final a(Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2202547
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->a()Z

    move-result v0

    if-ne v0, p1, :cond_0

    .line 2202548
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2202549
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2202550
    :goto_0
    return-object v0

    .line 2202551
    :cond_0
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->l:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2202552
    if-eqz p1, :cond_2

    .line 2202553
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->b:LX/30Y;

    invoke-virtual {v0}, LX/30Y;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2202554
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->d:LX/94q;

    sget-object v1, Lcom/facebook/contacts/upload/ContactsUploadVisibility;->SHOW:Lcom/facebook/contacts/upload/ContactsUploadVisibility;

    invoke-virtual {v0, v1}, LX/94q;->a(Lcom/facebook/contacts/upload/ContactsUploadVisibility;)LX/1ML;

    .line 2202555
    :goto_1
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/1nR;->b(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2202556
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2202557
    const-string v1, "growthSetContinuousContactsUploadParamsKey"

    if-eqz p1, :cond_3

    sget-object v0, LX/F9i;->ON:LX/F9i;

    :goto_2
    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2202558
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->f:LX/0aG;

    const-string v1, "growth_set_continuous_contacts_upload"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v5, 0x216e5c71

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    goto :goto_0

    .line 2202559
    :cond_1
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->j:LX/30I;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/30I;->a(Z)V

    goto :goto_1

    .line 2202560
    :cond_2
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->j:LX/30I;

    invoke-virtual {v0, v2}, LX/30I;->a(Z)V

    .line 2202561
    invoke-direct {p0, v2}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->c(Z)V

    goto :goto_1

    .line 2202562
    :cond_3
    sget-object v0, LX/F9i;->OFF:LX/F9i;

    goto :goto_2
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 2202563
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->l:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/app/Activity;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2202564
    instance-of v0, p1, LX/8DG;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/1nR;->b(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    invoke-interface {v2, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 2202565
    invoke-direct {p0, v2}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->c(Z)V

    .line 2202566
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2202567
    invoke-virtual {p0, v2}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->a(Z)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2202568
    if-eqz p1, :cond_0

    .line 2202569
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->c:Landroid/content/Context;

    const v4, 0x7f0833b7

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->c:Landroid/content/Context;

    const v4, 0x7f0833b8

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2202570
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2202571
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2202572
    :goto_0
    return v0

    .line 2202573
    :cond_0
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ml;

    const-string v3, "android.permission.READ_CONTACTS"

    invoke-virtual {v0, v3}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 2202574
    goto :goto_0

    .line 2202575
    :cond_1
    iget-object v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/1nR;->b(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    invoke-interface {v3, v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    .line 2202576
    iget-object v4, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/1nR;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    invoke-interface {v4, v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 2202577
    if-nez v3, :cond_2

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method
