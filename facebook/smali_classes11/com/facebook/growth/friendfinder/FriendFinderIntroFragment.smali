.class public Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field public static final h:[Ljava/lang/String;


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/9Tk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/83b;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0i5;

.field public j:Lcom/facebook/friending/center/FriendsCenterHomeFragment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/89v;

.field public l:Ljava/lang/String;

.field public m:Z

.field private n:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2202328
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.READ_CONTACTS"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.WRITE_CONTACTS"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->h:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2202325
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2202326
    iput-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->m:Z

    .line 2202327
    iput-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->n:Z

    return-void
.end method

.method public static a(LX/89v;Ljava/lang/String;)Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2202321
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->a(LX/89v;Ljava/lang/String;Z)Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    move-result-object v0

    .line 2202322
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2202323
    const-string v2, "FINISH_CONTAINING_ACTIVITY"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2202324
    return-object v0
.end method

.method public static a(LX/89v;Ljava/lang/String;Z)Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2202320
    sget-object v0, LX/03R;->UNSET:LX/03R;

    invoke-static {p0, p1, p2, v0}, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->a(LX/89v;Ljava/lang/String;ZLX/03R;)Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/89v;Ljava/lang/String;ZLX/03R;)Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2202280
    new-instance v0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    invoke-direct {v0}, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;-><init>()V

    .line 2202281
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2202282
    const-string v2, "ci_flow"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2202283
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2202284
    const-string p1, "UNKNOWN"

    .line 2202285
    :cond_0
    const-string v2, "ccu_ref"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2202286
    const-string v2, "configurable_ci_enabled"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2202287
    const-string v2, "should_skip_term_in_fc_gk"

    invoke-virtual {p3}, LX/03R;->getDbValue()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2202288
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2202289
    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 2202312
    iget-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->n:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->m:Z

    if-eqz v0, :cond_1

    .line 2202313
    :cond_0
    :goto_0
    return-void

    .line 2202314
    :cond_1
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2202315
    if-eqz v0, :cond_0

    .line 2202316
    const v1, 0x7f080faa

    invoke-interface {v0, v1}, LX/1ZF;->x_(I)V

    .line 2202317
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    .line 2202318
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->k:LX/89v;

    sget-object v2, LX/89v;->NEW_ACCOUNT_NUX:LX/89v;

    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/facebook/growth/nux/CILegalNuxActivity;

    if-nez v1, :cond_0

    .line 2202319
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 2202294
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2202295
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p1}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v4

    check-cast v4, LX/17W;

    invoke-static {p1}, LX/9Tk;->a(LX/0QB;)LX/9Tk;

    move-result-object v5

    check-cast v5, LX/9Tk;

    invoke-static {p1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {p1}, LX/83b;->a(LX/0QB;)LX/83b;

    move-result-object v7

    check-cast v7, LX/83b;

    invoke-static {p1}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->b(LX/0QB;)Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    move-result-object v8

    check-cast v8, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    const-class v0, LX/0i4;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/0i4;

    iput-object v3, v2, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->a:Lcom/facebook/content/SecureContextHelper;

    iput-object v4, v2, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->b:LX/17W;

    iput-object v5, v2, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->c:LX/9Tk;

    iput-object v6, v2, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->d:LX/1Ck;

    iput-object v7, v2, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->e:LX/83b;

    iput-object v8, v2, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->f:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    iput-object p1, v2, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->g:LX/0i4;

    .line 2202296
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->g:LX/0i4;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->i:LX/0i5;

    .line 2202297
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2202298
    const-string v1, "ci_flow"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    invoke-static {v0}, LX/89v;->fromSerializable(Ljava/io/Serializable;)LX/89v;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->k:LX/89v;

    .line 2202299
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2202300
    const-string v1, "ccu_ref"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->l:Ljava/lang/String;

    .line 2202301
    const-string v0, "UNKNOWN"

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2202302
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->k:LX/89v;

    iget-object v0, v0, LX/89v;->value:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->l:Ljava/lang/String;

    .line 2202303
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2202304
    const-string v1, "configurable_ci_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->m:Z

    .line 2202305
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->c:LX/9Tk;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->k:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->l:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->f:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    invoke-virtual {v3}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->a()Z

    move-result v3

    iget-object v4, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->f:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    invoke-virtual {v4}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->c()Z

    move-result v4

    .line 2202306
    iget-object v5, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v5, v5

    .line 2202307
    const-string v6, "should_skip_term_in_fc_gk"

    const/4 v7, -0x1

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v5

    invoke-virtual {v5}, LX/03R;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/9Tk;->a(Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)V

    .line 2202308
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 2202309
    instance-of v1, v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    if-eqz v1, :cond_1

    .line 2202310
    check-cast v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->j:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    .line 2202311
    :cond_1
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2202329
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2202330
    iget-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->m:Z

    if-eqz v0, :cond_1

    .line 2202331
    :cond_0
    :goto_0
    return-void

    .line 2202332
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 2202333
    instance-of v1, v0, LX/8DG;

    if-eqz v1, :cond_0

    if-nez p1, :cond_0

    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    .line 2202334
    const-string v1, "contact_importer"

    .line 2202335
    check-cast v0, LX/8DG;

    invoke-interface {v0, v1}, LX/8DG;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x3bd9fcb1

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2202293
    const v1, 0x7f0306d2

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x29a9a7f5

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xae50fa9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2202290
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->d:LX/1Ck;

    const-string v2, "DAILY_DIALOG_TASK_KEY"

    invoke-virtual {v1, v2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2202291
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2202292
    const/16 v1, 0x2b

    const v2, -0x2956299

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6ad71b2c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2202277
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->n:Z

    .line 2202278
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2202279
    const/16 v1, 0x2b

    const v2, -0xe4b5268

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5210327c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2202273
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2202274
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->n:Z

    .line 2202275
    invoke-direct {p0}, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->e()V

    .line 2202276
    const/16 v1, 0x2b

    const v2, -0x220ab07

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 2202248
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2202249
    const v0, 0x7f0d126c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 2202250
    const v1, 0x7f0d126d

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;

    .line 2202251
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2202252
    new-instance v2, LX/F7b;

    invoke-direct {v2, p0}, LX/F7b;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;)V

    move-object v2, v2

    .line 2202253
    iput-object v2, v1, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->c:LX/F7a;

    .line 2202254
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    .line 2202255
    instance-of v3, v2, Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    move v2, v3

    .line 2202256
    if-eqz v2, :cond_0

    .line 2202257
    sget-object v2, LX/F7k;->EVENT_INVITE_GUEST_STYLE:LX/F7k;

    .line 2202258
    :goto_0
    move-object v2, v2

    .line 2202259
    invoke-virtual {v1, v2}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->setIntroViewTheme(LX/F7k;)V

    .line 2202260
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->setVisibility(I)V

    .line 2202261
    iget-object v2, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->d:LX/1Ck;

    const-string v3, "DAILY_DIALOG_TASK_KEY"

    iget-object v4, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->e:LX/83b;

    const/4 v5, 0x5

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b08ae

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sget-object v7, LX/83a;->MUTUAL_IMPORTANCE:LX/83a;

    .line 2202262
    new-instance v8, LX/84i;

    invoke-direct {v8}, LX/84i;-><init>()V

    move-object v8, v8

    .line 2202263
    const-string v9, "image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v9

    const-string v10, "first"

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v9

    const-string v10, "order_by"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/String;

    const/4 p1, 0x0

    iget-object p2, v7, LX/83a;->value:Ljava/lang/String;

    aput-object p2, v11, p1

    invoke-static {v11}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v9

    const-string v10, "picture_size"

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v9

    const-string v10, "media_type"

    iget-object v11, v4, LX/83b;->b:LX/0rq;

    invoke-virtual {v11}, LX/0rq;->b()LX/0wF;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2202264
    invoke-static {v8}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v8

    .line 2202265
    sget-object v9, LX/0zS;->a:LX/0zS;

    invoke-virtual {v8, v9}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 2202266
    iget-object v9, v4, LX/83b;->a:LX/0tX;

    invoke-virtual {v9, v8}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v8

    new-instance v9, LX/83Z;

    invoke-direct {v9, v4}, LX/83Z;-><init>(LX/83b;)V

    iget-object v10, v4, LX/83b;->c:LX/0TD;

    invoke-static {v8, v9, v10}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    move-object v4, v8

    .line 2202267
    new-instance v5, LX/F7Z;

    invoke-direct {v5, p0, v1, v0}, LX/F7Z;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;Lcom/facebook/growth/friendfinder/FriendFinderIntroView;Landroid/widget/ProgressBar;)V

    invoke-virtual {v2, v3, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2202268
    return-void

    .line 2202269
    :cond_0
    iget-object v2, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->f:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->a(Landroid/app/Activity;)Z

    move-result v2

    .line 2202270
    if-nez v2, :cond_1

    .line 2202271
    sget-object v2, LX/F7k;->DAILY_DIALOGUE_STYLE:LX/F7k;

    goto/16 :goto_0

    .line 2202272
    :cond_1
    sget-object v2, LX/F7k;->REJECT_REG_TERMS_DD_STYLE:LX/F7k;

    goto/16 :goto_0
.end method

.method public final setUserVisibleHint(Z)V
    .locals 0

    .prologue
    .line 2202245
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 2202246
    invoke-direct {p0}, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->e()V

    .line 2202247
    return-void
.end method
