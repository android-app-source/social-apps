.class public Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/F7o;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Tf;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation
.end field

.field public f:J

.field private final g:LX/0fN;

.field private final h:LX/F7s;

.field private i:LX/0gc;

.field private j:Z

.field public k:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2202720
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2202721
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->f:J

    .line 2202722
    new-instance v0, LX/F7r;

    invoke-direct {v0, p0}, LX/F7r;-><init>(Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;)V

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->g:LX/0fN;

    .line 2202723
    new-instance v0, LX/F7s;

    invoke-direct {v0, p0}, LX/F7s;-><init>(Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;)V

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->h:LX/F7s;

    .line 2202724
    iput-boolean v2, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->j:Z

    .line 2202725
    iput-boolean v2, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->k:Z

    return-void
.end method

.method private a(Landroid/support/v4/app/Fragment;)V
    .locals 4

    .prologue
    .line 2202726
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->b:LX/F7o;

    invoke-virtual {v0}, LX/F7o;->c()LX/F7v;

    move-result-object v0

    sget-object v1, LX/F7v;->FRIENDABLE_CONTACTS:LX/F7v;

    if-ne v0, v1, :cond_1

    .line 2202727
    iget-wide v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->f:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2202728
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->f:J

    .line 2202729
    :cond_0
    check-cast p1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->h:LX/F7s;

    .line 2202730
    iput-object v0, p1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->w:LX/F7s;

    .line 2202731
    :cond_1
    return-void
.end method

.method public static a$redex0(Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;LX/1ZF;)V
    .locals 2

    .prologue
    .line 2202738
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->b:LX/F7o;

    invoke-virtual {v0}, LX/F7o;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f080029

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2202739
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    .line 2202740
    iput-object v0, v1, LX/108;->g:Ljava/lang/String;

    .line 2202741
    move-object v1, v1

    .line 2202742
    iput-object v0, v1, LX/108;->j:Ljava/lang/String;

    .line 2202743
    move-object v0, v1

    .line 2202744
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 2202745
    invoke-interface {p1, v0}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2202746
    new-instance v0, LX/F7t;

    invoke-direct {v0, p0}, LX/F7t;-><init>(Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;)V

    invoke-interface {p1, v0}, LX/1ZF;->a(LX/63W;)V

    .line 2202747
    return-void

    .line 2202748
    :cond_0
    const v0, 0x7f08001e

    goto :goto_0
.end method

.method public static c(Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;)V
    .locals 2

    .prologue
    .line 2202732
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->b:LX/F7o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->i:LX/0gc;

    if-nez v0, :cond_1

    .line 2202733
    :cond_0
    :goto_0
    return-void

    .line 2202734
    :cond_1
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->b:LX/F7o;

    invoke-virtual {v0}, LX/F7o;->d()Ljava/lang/String;

    move-result-object v0

    .line 2202735
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->i:LX/0gc;

    invoke-virtual {v1, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2202736
    if-eqz v0, :cond_0

    .line 2202737
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    goto :goto_0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 2202749
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->b:LX/F7o;

    invoke-virtual {v0}, LX/F7o;->d()Ljava/lang/String;

    move-result-object v1

    .line 2202750
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->i:LX/0gc;

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2202751
    if-nez v0, :cond_0

    .line 2202752
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->b:LX/F7o;

    .line 2202753
    iget-object v2, v0, LX/F7o;->b:LX/0Px;

    iget v3, v0, LX/F7o;->c:I

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/F7v;

    sget-object v3, LX/89v;->NEW_ACCOUNT_NUX:LX/89v;

    invoke-static {v2, v3}, LX/F7q;->a(LX/F7v;LX/89v;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    move-object v0, v2

    .line 2202754
    :cond_0
    invoke-direct {p0, v0}, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->a(Landroid/support/v4/app/Fragment;)V

    .line 2202755
    iget-object v2, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->i:LX/0gc;

    invoke-virtual {v2}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    const v3, 0x7f0d1275

    invoke-virtual {v2, v3, v0, v1}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2202756
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->i:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 2202757
    return-void
.end method

.method public static e(Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;)V
    .locals 15

    .prologue
    const/4 v6, 0x0

    .line 2202684
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->j:Z

    if-nez v0, :cond_1

    .line 2202685
    :cond_0
    :goto_0
    return-void

    .line 2202686
    :cond_1
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2202687
    if-eqz v0, :cond_0

    .line 2202688
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->b:LX/F7o;

    invoke-virtual {v1}, LX/F7o;->c()LX/F7v;

    move-result-object v1

    .line 2202689
    sget-object v2, LX/F7v;->LEGAL_SCREEN:LX/F7v;

    if-ne v1, v2, :cond_5

    .line 2202690
    const v2, 0x7f080faa

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2202691
    :goto_1
    invoke-interface {v0, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2202692
    const/4 v2, 0x1

    invoke-interface {v0, v2}, LX/1ZF;->k_(Z)V

    .line 2202693
    sget-object v2, LX/F7v;->LEGAL_SCREEN:LX/F7v;

    if-eq v1, v2, :cond_0

    .line 2202694
    sget-object v2, LX/F7v;->FRIENDABLE_CONTACTS:LX/F7v;

    if-ne v1, v2, :cond_4

    .line 2202695
    const-wide/16 v9, 0x0

    .line 2202696
    iget-object v7, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->c:LX/0ad;

    sget v8, LX/F94;->b:I

    const/4 v11, 0x0

    invoke-interface {v7, v8, v11}, LX/0ad;->a(II)I

    move-result v7

    .line 2202697
    if-ltz v7, :cond_8

    .line 2202698
    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v11, v7

    invoke-virtual {v8, v11, v12}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v7

    iget-object v11, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->a:LX/0SG;

    invoke-interface {v11}, LX/0SG;->a()J

    move-result-wide v11

    iget-wide v13, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->f:J

    sub-long/2addr v11, v13

    sub-long/2addr v7, v11

    .line 2202699
    :goto_2
    cmp-long v11, v7, v9

    if-lez v11, :cond_7

    :goto_3
    move-wide v2, v7

    .line 2202700
    iget-boolean v1, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->k:Z

    if-nez v1, :cond_2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gtz v1, :cond_3

    .line 2202701
    :cond_2
    invoke-static {p0, v0}, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->a$redex0(Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;LX/1ZF;)V

    goto :goto_0

    .line 2202702
    :cond_3
    invoke-interface {v0, v6}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2202703
    invoke-interface {v0, v6}, LX/1ZF;->a(LX/63W;)V

    .line 2202704
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v1, :cond_0

    .line 2202705
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->d:LX/0Tf;

    new-instance v4, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment$3;

    invoke-direct {v4, p0, v0}, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment$3;-><init>(Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;LX/1ZF;)V

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v4, v2, v3, v0}, LX/0Tf;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2202706
    :cond_4
    invoke-static {p0, v0}, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->a$redex0(Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;LX/1ZF;)V

    goto :goto_0

    .line 2202707
    :cond_5
    sget-object v2, LX/F7v;->FRIENDABLE_CONTACTS:LX/F7v;

    if-ne v1, v2, :cond_6

    .line 2202708
    const v2, 0x7f08339e

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 2202709
    :cond_6
    const v2, 0x7f0833a3

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_7
    move-wide v7, v9

    goto :goto_3

    :cond_8
    move-wide v7, v9

    goto :goto_2
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2202711
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2202712
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-static {v0}, LX/F7o;->a(LX/0QB;)LX/F7o;

    move-result-object v3

    check-cast v3, LX/F7o;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p1

    check-cast p1, LX/0ad;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, LX/0Tf;

    iput-object v2, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->a:LX/0SG;

    iput-object v3, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->b:LX/F7o;

    iput-object p1, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->c:LX/0ad;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->d:LX/0Tf;

    .line 2202713
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2202714
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->b:LX/F7o;

    invoke-virtual {v0}, LX/F7o;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2202715
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, LX/F7u;

    invoke-interface {v0}, LX/F7u;->a()V

    .line 2202716
    :goto_0
    return-void

    .line 2202717
    :cond_0
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->b:LX/F7o;

    .line 2202718
    iget v1, v0, LX/F7o;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/F7o;->c:I

    .line 2202719
    invoke-direct {p0}, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->d()V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x46eac2f1

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2202710
    const v1, 0x7f0306d7

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x947e8a5

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x11bcacb7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2202678
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->i:LX/0gc;

    iget-object v2, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->g:LX/0fN;

    invoke-virtual {v1, v2}, LX/0gc;->b(LX/0fN;)V

    .line 2202679
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    .line 2202680
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2202681
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2202682
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2202683
    const/16 v1, 0x2b

    const v2, -0x1a14aa97

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1fe7b4b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2202675
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->j:Z

    .line 2202676
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2202677
    const/16 v1, 0x2b

    const v2, -0x262936c0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x51582710

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2202671
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2202672
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->j:Z

    .line 2202673
    invoke-static {p0}, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->e(Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;)V

    .line 2202674
    const/16 v1, 0x2b

    const v2, 0x37d8b409

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2202664
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->i:LX/0gc;

    if-nez v0, :cond_0

    .line 2202665
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->i:LX/0gc;

    .line 2202666
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->b:LX/F7o;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->i:LX/0gc;

    .line 2202667
    iput-object v1, v0, LX/F7o;->a:LX/0gc;

    .line 2202668
    :cond_0
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->i:LX/0gc;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->g:LX/0fN;

    invoke-virtual {v0, v1}, LX/0gc;->a(LX/0fN;)V

    .line 2202669
    invoke-direct {p0}, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->d()V

    .line 2202670
    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 0

    .prologue
    .line 2202660
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 2202661
    invoke-static {p0}, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->c(Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;)V

    .line 2202662
    invoke-static {p0}, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->e(Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;)V

    .line 2202663
    return-void
.end method
