.class public Lcom/facebook/growth/friendfinder/FriendFinderHostingActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/1ZF;


# instance fields
.field private p:LX/0h5;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2202164
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/63W;)V
    .locals 1

    .prologue
    .line 2202162
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderHostingActivity;->p:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2202163
    return-void
.end method

.method public final a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 3

    .prologue
    .line 2202160
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderHostingActivity;->p:LX/0h5;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2202161
    return-void
.end method

.method public final a_(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2202158
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderHostingActivity;->p:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2202159
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 2202155
    invoke-virtual {p0, p1}, Lcom/facebook/growth/friendfinder/FriendFinderHostingActivity;->setResult(I)V

    .line 2202156
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/FriendFinderHostingActivity;->finish()V

    .line 2202157
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2202142
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2202143
    const v0, 0x7f040056

    const v1, 0x7f0400a6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/growth/friendfinder/FriendFinderHostingActivity;->overridePendingTransition(II)V

    .line 2202144
    const v0, 0x7f0306cf

    invoke-virtual {p0, v0}, Lcom/facebook/growth/friendfinder/FriendFinderHostingActivity;->setContentView(I)V

    .line 2202145
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2202146
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/growth/friendfinder/FriendFinderHostingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderHostingActivity;->p:LX/0h5;

    .line 2202147
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderHostingActivity;->p:LX/0h5;

    new-instance v1, LX/F7Y;

    invoke-direct {v1, p0}, LX/F7Y;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderHostingActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2202148
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/FriendFinderHostingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ci_flow"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/89v;

    .line 2202149
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    .line 2202150
    const v2, 0x7f0d1263

    invoke-virtual {v1, v2}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 2202151
    if-nez v2, :cond_0

    .line 2202152
    invoke-static {v0}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->a(LX/89v;)Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;

    move-result-object v0

    .line 2202153
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d1263

    invoke-virtual {v1, v2, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2202154
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 0

    .prologue
    .line 2202165
    return-void
.end method

.method public final f()Landroid/view/View;
    .locals 1

    .prologue
    .line 2202130
    const/4 v0, 0x0

    return-object v0
.end method

.method public final finish()V
    .locals 2

    .prologue
    .line 2202139
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 2202140
    const v0, 0x7f0400a1

    const v1, 0x7f040093

    invoke-virtual {p0, v0, v1}, Lcom/facebook/growth/friendfinder/FriendFinderHostingActivity;->overridePendingTransition(II)V

    .line 2202141
    return-void
.end method

.method public final k_(Z)V
    .locals 0

    .prologue
    .line 2202138
    return-void
.end method

.method public final lH_()V
    .locals 0

    .prologue
    .line 2202137
    return-void
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 2202134
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2202135
    const v0, 0x7f0400a1

    const v1, 0x7f040093

    invoke-virtual {p0, v0, v1}, Lcom/facebook/growth/friendfinder/FriendFinderHostingActivity;->overridePendingTransition(II)V

    .line 2202136
    return-void
.end method

.method public setCustomTitle(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2202133
    return-void
.end method

.method public final x_(I)V
    .locals 1

    .prologue
    .line 2202131
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderHostingActivity;->p:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setTitle(I)V

    .line 2202132
    return-void
.end method
