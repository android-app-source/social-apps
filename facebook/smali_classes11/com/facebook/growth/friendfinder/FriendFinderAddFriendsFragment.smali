.class public Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final p:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;",
            ">;"
        }
    .end annotation
.end field

.field private static final q:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/F7L;",
            ">;"
        }
    .end annotation
.end field

.field private B:LX/0Vq;

.field private C:LX/F7J;

.field private D:Landroid/text/TextWatcher;

.field public E:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/model/FacebookPhonebookContact;",
            ">;"
        }
    .end annotation
.end field

.field private F:Ljava/lang/String;

.field private G:I

.field public H:LX/89v;

.field public I:Ljava/lang/String;

.field public J:J

.field public K:Z

.field public L:I

.field private M:Z

.field public N:Z

.field private O:Z

.field public P:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/facebook/ipc/model/FacebookPhonebookContact;",
            ">;"
        }
    .end annotation
.end field

.field public Q:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/facebook/ipc/model/FacebookPhonebookContact;",
            ">;"
        }
    .end annotation
.end field

.field public a:LX/F74;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/F7X;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/F9r;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/9Tk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Sy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Lcom/facebook/performancelogger/PerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Or;
    .annotation runtime Lcom/facebook/common/hardware/StrictPhoneIsoCountryCode;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/2Ku;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Landroid/view/View;

.field private s:Landroid/widget/ProgressBar;

.field private t:Landroid/widget/TextView;

.field public u:Landroid/view/View;

.field public v:Lcom/facebook/ui/search/SearchEditText;

.field private w:Landroid/widget/TextView;

.field private x:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public y:LX/F73;

.field public z:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2201587
    const-class v0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;

    .line 2201588
    sput-object v0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->p:Ljava/lang/Class;

    const-string v1, "friend_finder_add_friends_fragment"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->q:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2201589
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2201590
    iput-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->M:Z

    .line 2201591
    iput-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->N:Z

    .line 2201592
    iput-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->O:Z

    .line 2201593
    return-void
.end method

.method public static a(LX/89v;)Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;
    .locals 3

    .prologue
    .line 2201594
    new-instance v0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;

    invoke-direct {v0}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;-><init>()V

    .line 2201595
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2201596
    const-string v2, "ci_flow"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2201597
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2201598
    return-object v0
.end method

.method private a(I)V
    .locals 4

    .prologue
    .line 2201599
    if-gtz p1, :cond_0

    .line 2201600
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->b()V

    .line 2201601
    :goto_0
    return-void

    .line 2201602
    :cond_0
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->f:LX/0Sh;

    new-instance v1, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment$7;

    invoke-direct {v1, p0}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment$7;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;)V

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, LX/0Sh;->b(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;LX/F74;LX/F7X;Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;LX/F9r;LX/0aG;LX/0Sh;LX/1Ck;Lcom/facebook/content/SecureContextHelper;LX/9Tk;LX/0Uh;LX/0Sy;Lcom/facebook/performancelogger/PerformanceLogger;LX/0Or;LX/0ad;LX/2Ku;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;",
            "LX/F74;",
            "LX/F7X;",
            "Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;",
            "LX/F9r;",
            "LX/0aG;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/1Ck;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/9Tk;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/common/userinteraction/UserInteractionController;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0ad;",
            "LX/2Ku;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2201603
    iput-object p1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->a:LX/F74;

    iput-object p2, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->b:LX/F7X;

    iput-object p3, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->c:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    iput-object p4, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->d:LX/F9r;

    iput-object p5, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->e:LX/0aG;

    iput-object p6, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->f:LX/0Sh;

    iput-object p7, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->g:LX/1Ck;

    iput-object p8, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->h:Lcom/facebook/content/SecureContextHelper;

    iput-object p9, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->i:LX/9Tk;

    iput-object p10, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->j:LX/0Uh;

    iput-object p11, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->k:LX/0Sy;

    iput-object p12, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->l:Lcom/facebook/performancelogger/PerformanceLogger;

    iput-object p13, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->m:LX/0Or;

    iput-object p14, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->n:LX/0ad;

    iput-object p15, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->o:LX/2Ku;

    return-void
.end method

.method private static a(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;Ljava/util/List;II)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/model/FacebookPhonebookContact;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 2201604
    new-instance v0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;

    iget-object v2, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->F:Ljava/lang/String;

    iget v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->G:I

    iget-object v4, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->H:LX/89v;

    iget-object v5, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->I:Ljava/lang/String;

    add-int/lit8 v1, p3, -0x1

    div-int/lit8 v6, v1, 0x32

    const/16 v7, 0x32

    const/16 v8, 0xa

    move-object v1, p1

    move v9, p2

    invoke-direct/range {v0 .. v9}, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;-><init>(Ljava/util/List;Ljava/lang/String;ILX/89v;Ljava/lang/String;IIII)V

    .line 2201605
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2201606
    const-string v1, "growthFriendFinderParamsKey"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2201607
    iget-object v6, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->g:LX/1Ck;

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->e:LX/0aG;

    const-string v1, "growth_friend_finder"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->q:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x5eb64dad

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    new-instance v1, LX/F7H;

    invoke-direct {v1, p0, p3}, LX/F7H;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;I)V

    invoke-virtual {v6, v7, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2201608
    return-void
.end method

.method public static a(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;Z)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 2201609
    iget-object v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->w:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2201610
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->x:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2201611
    return-void

    :cond_0
    move v0, v2

    .line 2201612
    goto :goto_0

    :cond_1
    move v2, v1

    .line 2201613
    goto :goto_1
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 16

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v15

    move-object/from16 v0, p0

    check-cast v0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;

    const-class v1, LX/F74;

    invoke-interface {v15, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/F74;

    invoke-static {v15}, LX/F7X;->a(LX/0QB;)LX/F7X;

    move-result-object v2

    check-cast v2, LX/F7X;

    invoke-static {v15}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->b(LX/0QB;)Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    move-result-object v3

    check-cast v3, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    invoke-static {v15}, LX/F9r;->b(LX/0QB;)LX/F9r;

    move-result-object v4

    check-cast v4, LX/F9r;

    invoke-static {v15}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v5

    check-cast v5, LX/0aG;

    invoke-static {v15}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v6

    check-cast v6, LX/0Sh;

    invoke-static {v15}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-static {v15}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v15}, LX/9Tk;->a(LX/0QB;)LX/9Tk;

    move-result-object v9

    check-cast v9, LX/9Tk;

    invoke-static {v15}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-static {v15}, LX/0Sy;->a(LX/0QB;)LX/0Sy;

    move-result-object v11

    check-cast v11, LX/0Sy;

    invoke-static {v15}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v12

    check-cast v12, Lcom/facebook/performancelogger/PerformanceLogger;

    const/16 v13, 0x15ed

    invoke-static {v15, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static {v15}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v14

    check-cast v14, LX/0ad;

    invoke-static {v15}, LX/2Ku;->a(LX/0QB;)LX/2Ku;

    move-result-object v15

    check-cast v15, LX/2Ku;

    invoke-static/range {v0 .. v15}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->a(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;LX/F74;LX/F7X;Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;LX/F9r;LX/0aG;LX/0Sh;LX/1Ck;Lcom/facebook/content/SecureContextHelper;LX/9Tk;LX/0Uh;LX/0Sy;Lcom/facebook/performancelogger/PerformanceLogger;LX/0Or;LX/0ad;LX/2Ku;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;Lcom/facebook/growth/protocol/FriendFinderMethod$Result;I)V
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 2201614
    if-nez p1, :cond_0

    .line 2201615
    invoke-static {p0, p2}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->e(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;I)V

    .line 2201616
    :goto_0
    return-void

    .line 2201617
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/growth/protocol/FriendFinderMethod$Result;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;

    .line 2201618
    iget-wide v4, v0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->userId:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 2201619
    iget-object v4, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->z:Ljava/util/Set;

    invoke-interface {v4, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->b:LX/F7X;

    iget-wide v6, v0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->userId:J

    invoke-virtual {v4, v6, v7}, LX/F7X;->b(J)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2201620
    iget-object v4, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->A:Ljava/util/List;

    .line 2201621
    new-instance v5, LX/F7L;

    const/4 v6, 0x0

    invoke-direct {v5, v0, v6}, LX/F7L;-><init>(Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;Z)V

    move-object v0, v5

    .line 2201622
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2201623
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->z:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2201624
    :cond_2
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->A:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2201625
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->z:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2201626
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->l:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x3f0001

    const-string v2, "FriendFinderAddFriendsTTI"

    const-string v4, "state"

    const-string v5, "success"

    invoke-interface/range {v0 .. v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2201627
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v4, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->J:J

    sub-long v6, v0, v4

    .line 2201628
    iget-object v4, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->i:LX/9Tk;

    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->H:LX/89v;

    iget-object v5, v0, LX/89v;->value:Ljava/lang/String;

    const/16 v8, 0x32

    const/16 v9, 0xa

    iget-object v10, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->I:Ljava/lang/String;

    .line 2201629
    iget-object v0, v4, LX/9Tk;->a:LX/0Zb;

    sget-object v1, LX/9Tj;->FIRST_RESULTS_READY:LX/9Tj;

    invoke-virtual {v1}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/9Tk;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "api"

    sget-object v11, LX/9Ti;->FRIEND_FINDER_API:LX/9Ti;

    invoke-virtual {v11}, LX/9Ti;->getApiName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v2, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ci_flow"

    invoke-virtual {v1, v2, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "time"

    invoke-virtual {v1, v2, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "batch_size"

    invoke-virtual {v1, v2, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "pagination_size"

    invoke-virtual {v1, v2, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "session_id"

    invoke-virtual {v1, v2, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2201630
    :cond_3
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->k:LX/0Sy;

    invoke-virtual {v0}, LX/0Sy;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2201631
    invoke-static {p0}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->k(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;)V

    .line 2201632
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/growth/protocol/FriendFinderMethod$Result;->c()I

    move-result v0

    if-lez v0, :cond_5

    .line 2201633
    invoke-virtual {p1}, Lcom/facebook/growth/protocol/FriendFinderMethod$Result;->c()I

    move-result v0

    invoke-static {p0, v3, v0, p2}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->a(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;Ljava/util/List;II)V

    goto/16 :goto_0

    .line 2201634
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/growth/protocol/FriendFinderMethod$Result;->b()Ljava/util/List;

    move-result-object v0

    .line 2201635
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/protocol/FriendFinderMethod$Result$Invites;

    .line 2201636
    invoke-virtual {v0}, Lcom/facebook/growth/protocol/FriendFinderMethod$Result$Invites;->a()J

    move-result-wide v2

    .line 2201637
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->P:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;

    .line 2201638
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/CharSequence;

    const/4 v5, 0x0

    iget-object v6, v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->name:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0}, Lcom/facebook/ipc/model/FacebookPhonebookContact;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 2201639
    iget-object v4, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->Q:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v4, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 2201640
    :cond_7
    invoke-static {p0, p2}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->e(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;I)V

    goto/16 :goto_0
.end method

.method private d(I)V
    .locals 4

    .prologue
    .line 2201641
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->s:Landroid/widget/ProgressBar;

    const-string v1, "progress"

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput p1, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 2201642
    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 2201643
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2201644
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 2201645
    return-void
.end method

.method public static e(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;I)V
    .locals 14

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 2201454
    add-int/lit8 v0, p1, 0x32

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->E:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2201455
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->E:Ljava/util/List;

    invoke-interface {v0, p1, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    .line 2201456
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->E:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    .line 2201457
    :goto_0
    const v3, 0x459c4000    # 5000.0f

    mul-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->d(I)V

    .line 2201458
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2201459
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->l:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x3f0001

    const-string v2, "FriendFinderAddFriendsTTI"

    const/4 v3, 0x0

    const-string v4, "state"

    const-string v5, "no matches"

    invoke-interface/range {v0 .. v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2201460
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->z:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v5

    .line 2201461
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->t:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0168

    new-array v3, v10, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v9

    invoke-virtual {v1, v2, v5, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2201462
    const/16 v0, 0x7d0

    invoke-direct {p0, v0}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->a(I)V

    .line 2201463
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->J:J

    sub-long v2, v0, v2

    .line 2201464
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->i:LX/9Tk;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->H:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->E:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/16 v6, 0x32

    const/16 v7, 0xa

    iget-object v8, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->I:Ljava/lang/String;

    .line 2201465
    iget-object v11, v0, LX/9Tk;->a:LX/0Zb;

    sget-object v12, LX/9Tj;->COMPLETED:LX/9Tj;

    invoke-virtual {v12}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, LX/9Tk;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v12

    const-string v13, "ci_flow"

    invoke-virtual {v12, v13, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v12

    const-string v13, "time"

    invoke-virtual {v12, v13, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v12

    const-string v13, "phonebook_size"

    invoke-virtual {v12, v13, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v12

    const-string v13, "matches"

    invoke-virtual {v12, v13, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v12

    const-string v13, "batch_size"

    invoke-virtual {v12, v13, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v12

    const-string v13, "pagination_size"

    invoke-virtual {v12, v13, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v12

    const-string v13, "session_id"

    invoke-virtual {v12, v13, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v12

    invoke-interface {v11, v12}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2201466
    iget-object v11, v0, LX/9Tk;->b:LX/0if;

    sget-object v12, LX/0ig;->b:LX/0ih;

    sget-object v13, LX/9Tj;->COMPLETED:LX/9Tj;

    invoke-virtual {v13}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2201467
    iget-object v11, v0, LX/9Tk;->b:LX/0if;

    sget-object v12, LX/0ig;->b:LX/0ih;

    invoke-virtual {v11, v12}, LX/0if;->c(LX/0ih;)V

    .line 2201468
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->j:LX/0Uh;

    sget v1, LX/Ezz;->a:I

    invoke-virtual {v0, v1, v9}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2201469
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->o:LX/2Ku;

    new-instance v1, LX/F01;

    invoke-direct {v1, v5}, LX/F01;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2201470
    :cond_0
    invoke-direct {p0}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->l()V

    .line 2201471
    invoke-static {p0}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->r(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;)V

    .line 2201472
    iput-boolean v10, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->K:Z

    .line 2201473
    :goto_1
    return-void

    .line 2201474
    :cond_1
    int-to-float v0, p1

    iget-object v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->E:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v0, v3

    goto/16 :goto_0

    .line 2201475
    :cond_2
    invoke-static {p0, v2, v9, v1}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->a(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;Ljava/util/List;II)V

    goto :goto_1
.end method

.method public static k(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;)V
    .locals 2

    .prologue
    .line 2201646
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->A:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2201647
    :goto_0
    return-void

    .line 2201648
    :cond_0
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->y:LX/F73;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->A:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/F73;->a(Ljava/util/List;)V

    .line 2201649
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->A:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0
.end method

.method private l()V
    .locals 8

    .prologue
    .line 2201650
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2201651
    new-instance v0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;

    iget v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->G:I

    iget-object v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->I:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->H:LX/89v;

    iget-object v5, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->z:Ljava/util/Set;

    invoke-static {v5}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;-><init>(ILjava/lang/String;LX/89v;Ljava/util/List;)V

    .line 2201652
    const-string v1, "growthFriendFinderPYMKParamsKey"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2201653
    iget-object v6, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->g:LX/1Ck;

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->e:LX/0aG;

    const-string v1, "growth_friend_finder_pymk"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->q:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x4fa67598

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    new-instance v1, LX/F7I;

    invoke-direct {v1, p0}, LX/F7I;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;)V

    invoke-virtual {v6, v7, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2201654
    return-void
.end method

.method public static m(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;)Z
    .locals 2

    .prologue
    .line 2201579
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->j:LX/0Uh;

    const/16 v1, 0x32

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private q()V
    .locals 2

    .prologue
    .line 2201580
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->M:Z

    if-nez v0, :cond_1

    .line 2201581
    :cond_0
    :goto_0
    return-void

    .line 2201582
    :cond_1
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2201583
    if-eqz v0, :cond_0

    .line 2201584
    const v1, 0x7f08339e

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2201585
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    .line 2201586
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    goto :goto_0
.end method

.method public static r(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;)V
    .locals 3

    .prologue
    .line 2201411
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->M:Z

    if-nez v0, :cond_1

    .line 2201412
    :cond_0
    :goto_0
    return-void

    .line 2201413
    :cond_1
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2201414
    if-eqz v0, :cond_0

    .line 2201415
    invoke-static {p0}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->m(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->Q:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const v1, 0x7f080029

    :goto_1
    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2201416
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    .line 2201417
    iput-object v1, v2, LX/108;->g:Ljava/lang/String;

    .line 2201418
    move-object v2, v2

    .line 2201419
    iput-object v1, v2, LX/108;->j:Ljava/lang/String;

    .line 2201420
    move-object v1, v2

    .line 2201421
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 2201422
    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2201423
    new-instance v1, LX/F7B;

    invoke-direct {v1, p0}, LX/F7B;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;)V

    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    goto :goto_0

    .line 2201424
    :cond_2
    const v1, 0x7f08001e

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2201425
    const-string v0, "friend_finder_add_friends_fragment"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2201426
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2201427
    new-instance v0, LX/4nE;

    invoke-direct {v0}, LX/4nE;-><init>()V

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 2201428
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2201429
    invoke-static {}, LX/0RA;->c()Ljava/util/LinkedHashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->z:Ljava/util/Set;

    .line 2201430
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->A:Ljava/util/List;

    .line 2201431
    new-instance v0, LX/F7C;

    invoke-direct {v0, p0}, LX/F7C;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;)V

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->B:LX/0Vq;

    .line 2201432
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->k:LX/0Sy;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->B:LX/0Vq;

    invoke-virtual {v0, v1}, LX/0Sy;->a(LX/0Vq;)V

    .line 2201433
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->E:Ljava/util/List;

    .line 2201434
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->F:Ljava/lang/String;

    .line 2201435
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b2261

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->G:I

    .line 2201436
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2201437
    if-nez v0, :cond_0

    sget-object v0, LX/89v;->UNKNOWN:LX/89v;

    :goto_0
    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->H:LX/89v;

    .line 2201438
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->I:Ljava/lang/String;

    .line 2201439
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->J:J

    .line 2201440
    iput-boolean v4, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->K:Z

    .line 2201441
    iput v4, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->L:I

    .line 2201442
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->P:Ljava/util/Map;

    .line 2201443
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->Q:Ljava/util/Map;

    .line 2201444
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->l:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v1, LX/0Yj;

    const v2, 0x3f0001

    const-string v3, "FriendFinderAddFriendsTTI"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    new-array v2, v5, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v1

    invoke-virtual {v1}, LX/0Yj;->b()LX/0Yj;

    move-result-object v1

    .line 2201445
    iput-boolean v5, v1, LX/0Yj;->n:Z

    .line 2201446
    move-object v1, v1

    .line 2201447
    invoke-interface {v0, v1, v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(LX/0Yj;Z)V

    .line 2201448
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->i:LX/9Tk;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->H:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    const/16 v2, 0x32

    const/16 v3, 0xa

    iget-object v4, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->I:Ljava/lang/String;

    .line 2201449
    iget-object v5, v0, LX/9Tk;->a:LX/0Zb;

    sget-object v6, LX/9Tj;->OPENED:LX/9Tj;

    invoke-virtual {v6}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/9Tk;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string p0, "api"

    sget-object p1, LX/9Ti;->FRIEND_FINDER_API:LX/9Ti;

    invoke-virtual {p1}, LX/9Ti;->getApiName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string p0, "ci_flow"

    invoke-virtual {v6, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string p0, "batch_size"

    invoke-virtual {v6, p0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string p0, "pagination_size"

    invoke-virtual {v6, p0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string p0, "session_id"

    invoke-virtual {v6, p0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-interface {v5, v6}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2201450
    iget-object v5, v0, LX/9Tk;->b:LX/0if;

    sget-object v6, LX/0ig;->b:LX/0ih;

    sget-object p0, LX/9Tj;->OPENED:LX/9Tj;

    invoke-virtual {p0}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v5, v6, p0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2201451
    return-void

    .line 2201452
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2201453
    const-string v1, "ci_flow"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/89v;

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 2201476
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->r:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->u:Landroid/view/View;

    if-nez v0, :cond_1

    .line 2201477
    :cond_0
    :goto_0
    return-void

    .line 2201478
    :cond_1
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 2201479
    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 2201480
    new-instance v1, LX/F7G;

    invoke-direct {v1, p0}, LX/F7G;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2201481
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->r:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2201482
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2201483
    if-nez p1, :cond_0

    .line 2201484
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2201485
    instance-of v1, v0, Lcom/facebook/growth/friendfinder/FriendFinderHostingActivity;

    if-eqz v1, :cond_1

    .line 2201486
    check-cast v0, Lcom/facebook/growth/friendfinder/FriendFinderHostingActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/facebook/growth/friendfinder/FriendFinderHostingActivity;->b(I)V

    .line 2201487
    :cond_0
    :goto_0
    return-void

    .line 2201488
    :cond_1
    instance-of v1, v0, Lcom/facebook/growth/nux/UserAccountNUXActivity;

    if-eqz v1, :cond_0

    .line 2201489
    const-string v1, "contact_importer"

    .line 2201490
    check-cast v0, LX/8DG;

    invoke-interface {v0, v1}, LX/8DG;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x390e85f0

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2201491
    const v1, 0x7f0306d0

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x182d528

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x32c95c5f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2201492
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->l:Lcom/facebook/performancelogger/PerformanceLogger;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(Ljava/lang/String;)V

    .line 2201493
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2201494
    const/16 v1, 0x2b

    const v2, 0x3da3bf67

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, 0x5a6a1824

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2201495
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->g:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2201496
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->C:LX/F7J;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/F7J;->cancel(Z)Z

    .line 2201497
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->y:LX/F73;

    invoke-virtual {v1}, LX/F73;->b()V

    .line 2201498
    iput-object v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->r:Landroid/view/View;

    .line 2201499
    iput-object v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->s:Landroid/widget/ProgressBar;

    .line 2201500
    iput-object v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->t:Landroid/widget/TextView;

    .line 2201501
    iput-object v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->u:Landroid/view/View;

    .line 2201502
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->v:Lcom/facebook/ui/search/SearchEditText;

    iget-object v2, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->D:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/facebook/ui/search/SearchEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2201503
    iput-object v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->v:Lcom/facebook/ui/search/SearchEditText;

    .line 2201504
    iput-object v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->w:Landroid/widget/TextView;

    .line 2201505
    iput-object v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->x:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2201506
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2201507
    const/16 v1, 0x2b

    const v2, -0x7a71074b

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x79dc64c6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2201508
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->v:Lcom/facebook/ui/search/SearchEditText;

    .line 2201509
    invoke-static {v1}, Lcom/facebook/ui/search/SearchEditText;->i(Lcom/facebook/ui/search/SearchEditText;)V

    .line 2201510
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->M:Z

    .line 2201511
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2201512
    const/16 v1, 0x2b

    const v2, -0x16b9437

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x6139c130

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2201513
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2201514
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->M:Z

    .line 2201515
    invoke-direct {p0}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->q()V

    .line 2201516
    const-wide/16 v8, 0x0

    const-wide/16 v6, -0x1

    .line 2201517
    iget-object v10, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->H:LX/89v;

    sget-object v11, LX/89v;->NEW_ACCOUNT_NUX:LX/89v;

    if-eq v10, v11, :cond_4

    .line 2201518
    :cond_0
    :goto_0
    move-wide v2, v6

    .line 2201519
    iget-boolean v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->K:Z

    if-nez v1, :cond_1

    cmp-long v1, v2, v4

    if-nez v1, :cond_3

    .line 2201520
    :cond_1
    invoke-static {p0}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->r(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;)V

    .line 2201521
    :cond_2
    :goto_1
    const v1, 0x5b3e0b3d

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 2201522
    :cond_3
    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 2201523
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->f:LX/0Sh;

    new-instance v4, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment$3;

    invoke-direct {v4, p0}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment$3;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;)V

    invoke-virtual {v1, v4, v2, v3}, LX/0Sh;->b(Ljava/lang/Runnable;J)V

    goto :goto_1

    .line 2201524
    :cond_4
    iget-object v10, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->n:LX/0ad;

    sget v11, LX/F94;->a:I

    const/4 v12, -0x1

    invoke-interface {v10, v11, v12}, LX/0ad;->a(II)I

    move-result v10

    .line 2201525
    if-ltz v10, :cond_0

    .line 2201526
    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v10, v10

    invoke-virtual {v6, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    iget-wide v12, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->J:J

    sub-long/2addr v10, v12

    sub-long/2addr v6, v10

    .line 2201527
    cmp-long v10, v6, v8

    if-gtz v10, :cond_0

    move-wide v6, v8

    goto :goto_0
.end method

.method public final onStop()V
    .locals 14

    .prologue
    const/16 v4, 0x32

    const/16 v5, 0xa

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x4c3d662b

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v11

    .line 2201528
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->z:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->K:Z

    if-nez v0, :cond_0

    .line 2201529
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->J:J

    sub-long v2, v0, v2

    .line 2201530
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->i:LX/9Tk;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->H:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->I:Ljava/lang/String;

    .line 2201531
    iget-object v7, v0, LX/9Tk;->a:LX/0Zb;

    sget-object v8, LX/9Tj;->CANCELED:LX/9Tj;

    invoke-virtual {v8}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/9Tk;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "api"

    sget-object v10, LX/9Ti;->FRIEND_FINDER_API:LX/9Ti;

    invoke-virtual {v10}, LX/9Ti;->getApiName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "ci_flow"

    invoke-virtual {v8, v9, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "time"

    invoke-virtual {v8, v9, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "batch_size"

    invoke-virtual {v8, v9, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "pagination_size"

    invoke-virtual {v8, v9, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "session_id"

    invoke-virtual {v8, v9, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    invoke-interface {v7, v8}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2201532
    iget-object v7, v0, LX/9Tk;->b:LX/0if;

    sget-object v8, LX/0ig;->b:LX/0ih;

    sget-object v9, LX/9Tj;->CANCELED:LX/9Tj;

    invoke-virtual {v9}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2201533
    iget-object v7, v0, LX/9Tk;->b:LX/0if;

    sget-object v8, LX/0ig;->b:LX/0ih;

    invoke-virtual {v7, v8}, LX/0if;->c(LX/0ih;)V

    .line 2201534
    :goto_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2201535
    const v0, -0x5e8e3b26

    invoke-static {v0, v11}, LX/02F;->f(II)V

    return-void

    .line 2201536
    :cond_0
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->i:LX/9Tk;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->H:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->y:LX/F73;

    iget v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->L:I

    invoke-virtual {v2, v3}, LX/F73;->a(I)I

    move-result v2

    iget-object v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->y:LX/F73;

    .line 2201537
    iget-object v6, v3, LX/F73;->e:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    iget-object v7, v3, LX/F73;->f:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    add-int/2addr v6, v7

    move v3, v6

    .line 2201538
    iget-object v6, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->I:Ljava/lang/String;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    iget-wide v12, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->J:J

    sub-long v7, v8, v12

    iget-object v9, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->y:LX/F73;

    invoke-virtual {v9}, LX/F73;->e()I

    move-result v9

    iget-object v10, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->y:LX/F73;

    invoke-virtual {v10}, LX/F73;->f()I

    move-result v10

    invoke-virtual/range {v0 .. v10}, LX/9Tk;->a(Ljava/lang/String;IIIILjava/lang/String;JII)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2201539
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2201540
    const v0, 0x7f0d1264

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->r:Landroid/view/View;

    .line 2201541
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->r:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2201542
    const v0, 0x7f0d1265

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->s:Landroid/widget/ProgressBar;

    .line 2201543
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->s:Landroid/widget/ProgressBar;

    const/16 v1, 0x1388

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 2201544
    const v0, 0x7f0d1266

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->t:Landroid/widget/TextView;

    .line 2201545
    const v0, 0x7f0d1268

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->u:Landroid/view/View;

    .line 2201546
    const v0, 0x7f0d126a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->w:Landroid/widget/TextView;

    .line 2201547
    const v0, 0x7f0d126b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->x:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2201548
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->x:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2201549
    new-instance v0, LX/F7E;

    invoke-direct {v0, p0}, LX/F7E;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;)V

    move-object v0, v0

    .line 2201550
    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->D:Landroid/text/TextWatcher;

    .line 2201551
    const v0, 0x7f0d1269

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/search/SearchEditText;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->v:Lcom/facebook/ui/search/SearchEditText;

    .line 2201552
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->v:Lcom/facebook/ui/search/SearchEditText;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->D:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2201553
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->c:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->c:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    invoke-virtual {v1}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->b(Z)V

    .line 2201554
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->t:Landroid/widget/TextView;

    const v1, 0x7f0833c7

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2201555
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->y:LX/F73;

    if-nez v0, :cond_1

    .line 2201556
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->a:LX/F74;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/F74;->a(Landroid/content/Context;)LX/F73;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->y:LX/F73;

    .line 2201557
    :goto_0
    iget-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->N:Z

    if-nez v0, :cond_0

    .line 2201558
    new-instance v0, LX/F7J;

    invoke-direct {v0, p0}, LX/F7J;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;)V

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->C:LX/F7J;

    .line 2201559
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->C:LX/F7J;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, LX/3nE;->a(Landroid/content/Context;[Ljava/lang/Object;)LX/3nE;

    .line 2201560
    :cond_0
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    .line 2201561
    const v1, 0x1020004

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setEmptyView(Landroid/view/View;)V

    .line 2201562
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->y:LX/F73;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2201563
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setStickyHeaderEnabled(Z)V

    .line 2201564
    new-instance v1, LX/F7F;

    invoke-direct {v1, p0}, LX/F7F;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;)V

    move-object v1, v1

    .line 2201565
    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2201566
    new-instance v1, LX/F7D;

    invoke-direct {v1, p0}, LX/F7D;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;)V

    .line 2201567
    iput-object v1, v0, Lcom/facebook/widget/listview/BetterListView;->y:LX/2i6;

    .line 2201568
    return-void

    .line 2201569
    :cond_1
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->y:LX/F73;

    invoke-virtual {v0}, LX/F73;->a()V

    goto :goto_0
.end method

.method public final setUserVisibleHint(Z)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 2201570
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 2201571
    if-eqz p1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->O:Z

    if-nez v1, :cond_0

    .line 2201572
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->O:Z

    .line 2201573
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->i:LX/9Tk;

    iget-object v2, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->H:LX/89v;

    invoke-virtual {v2}, LX/89v;->name()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/9Ti;->FRIEND_FINDER_API:LX/9Ti;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->J:J

    sub-long/2addr v4, v6

    iget-object v6, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->y:LX/F73;

    if-nez v6, :cond_2

    move v6, v0

    :goto_0
    iget-object v7, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->y:LX/F73;

    if-nez v7, :cond_3

    move v7, v0

    :goto_1
    iget-boolean v8, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->K:Z

    invoke-virtual/range {v1 .. v8}, LX/9Tk;->a(Ljava/lang/String;LX/9Ti;JIIZ)V

    .line 2201574
    :cond_0
    invoke-direct {p0}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->q()V

    .line 2201575
    iget-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->K:Z

    if-eqz v0, :cond_1

    .line 2201576
    invoke-static {p0}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->r(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;)V

    .line 2201577
    :cond_1
    return-void

    .line 2201578
    :cond_2
    iget-object v6, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->y:LX/F73;

    invoke-virtual {v6}, LX/F73;->e()I

    move-result v6

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->y:LX/F73;

    invoke-virtual {v0}, LX/F73;->f()I

    move-result v7

    goto :goto_1
.end method
