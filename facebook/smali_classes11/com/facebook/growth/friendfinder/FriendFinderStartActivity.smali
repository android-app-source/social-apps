.class public Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2202581
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2202582
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;->s:Z

    return-void
.end method

.method private static a(Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2202583
    iput-object p1, p0, Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;->p:Lcom/facebook/content/SecureContextHelper;

    iput-object p2, p0, Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p3, p0, Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;->r:Ljava/lang/String;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;

    invoke-static {v2}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v2}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v2}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;->a(Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/lang/String;)V

    return-void
.end method

.method private a()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2202584
    iget-boolean v2, p0, Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;->s:Z

    if-eqz v2, :cond_1

    .line 2202585
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;->r:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v3, v4}, LX/1nR;->a(Ljava/lang/String;Lcom/facebook/prefs/shared/FbSharedPreferences;)LX/0Tn;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2202586
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2202587
    invoke-static {p0, p0}, Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2202588
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ci_flow"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    invoke-static {v0}, LX/89v;->fromSerializable(Ljava/io/Serializable;)LX/89v;

    move-result-object v1

    .line 2202589
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "ccu_ref"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2202590
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "force_show_legal_screen"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;->s:Z

    .line 2202591
    invoke-direct {p0}, Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2202592
    const v0, 0x7f0306cf

    invoke-virtual {p0, v0}, Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;->setContentView(I)V

    .line 2202593
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2202594
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2202595
    const v3, 0x7f080faa

    invoke-interface {v0, v3}, LX/0h5;->setTitle(I)V

    .line 2202596
    new-instance v3, LX/F7n;

    invoke-direct {v3, p0}, LX/F7n;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;)V

    invoke-interface {v0, v3}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2202597
    invoke-static {v1, v2}, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->a(LX/89v;Ljava/lang/String;)Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    move-result-object v0

    .line 2202598
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d1263

    invoke-virtual {v1, v2, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2202599
    :goto_0
    return-void

    .line 2202600
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/facebook/growth/friendfinder/FriendFinderHostingActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2202601
    const-string v2, "ci_flow"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2202602
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;->p:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2202603
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;->finish()V

    goto :goto_0
.end method
