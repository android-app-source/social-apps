.class public Lcom/facebook/growth/friendfinder/FriendFinderIntroView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/8tu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/F7a;

.field private d:Lcom/facebook/fbui/facepile/FacepileView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/view/View;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2202474
    const-class v0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2202369
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2202370
    invoke-direct {p0}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->b()V

    .line 2202371
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2202471
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2202472
    invoke-direct {p0}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->b()V

    .line 2202473
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2202468
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2202469
    invoke-direct {p0}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->b()V

    .line 2202470
    return-void
.end method

.method private static a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;Ljava/lang/String;II)V
    .locals 2

    .prologue
    .line 2202461
    if-nez p1, :cond_0

    .line 2202462
    :goto_0
    return-void

    .line 2202463
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2202464
    iput p3, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 2202465
    iput p2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2202466
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2202467
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v1, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;

    invoke-static {v0}, LX/8tu;->a(LX/0QB;)LX/8tu;

    move-result-object v0

    check-cast v0, LX/8tu;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->a:LX/8tu;

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 2202459
    const-class v0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;

    invoke-static {v0, p0}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2202460
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2202475
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->d:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 2202476
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2202477
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2202478
    return-void
.end method

.method public final a(Ljava/lang/String;II)V
    .locals 1

    .prologue
    .line 2202456
    const v0, 0x7f0d0bbe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2202457
    invoke-static {v0, p1, p2, p3}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;Ljava/lang/String;II)V

    .line 2202458
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2202453
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2202454
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2202455
    return-void
.end method

.method public final a(Ljava/util/List;I)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2202446
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 2202447
    if-ne v0, v7, :cond_0

    .line 2202448
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080faf

    new-array v3, v7, [Ljava/lang/Object;

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2202449
    :goto_0
    return-void

    .line 2202450
    :cond_0
    if-ne v0, v3, :cond_1

    .line 2202451
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080fb0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2202452
    :cond_1
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080fb1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v6

    add-int/lit8 v4, p2, -0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;II)V
    .locals 1

    .prologue
    .line 2202443
    const v0, 0x7f0d14b1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2202444
    invoke-static {v0, p1, p2, p3}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;Ljava/lang/String;II)V

    .line 2202445
    return-void
.end method

.method public setFacepileFaces(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2202441
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->d:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceUrls(Ljava/util/List;)V

    .line 2202442
    return-void
.end method

.method public setFriendFinderIntroViewListener(LX/F7a;)V
    .locals 0

    .prologue
    .line 2202439
    iput-object p1, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->c:LX/F7a;

    .line 2202440
    return-void
.end method

.method public setIntroViewTheme(LX/F7k;)V
    .locals 9

    .prologue
    const/16 v7, 0x21

    .line 2202372
    sget-object v0, LX/F7j;->a:[I

    invoke-virtual {p1}, LX/F7k;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2202373
    const v4, 0x7f0306d3

    .line 2202374
    const v3, 0x7f0833a5

    .line 2202375
    const v2, 0x7f0833a9

    .line 2202376
    const v1, 0x7f0833ae

    .line 2202377
    const v0, 0x7f0833b4

    move v5, v3

    move v3, v1

    move v8, v2

    move v2, v0

    move v0, v4

    move v4, v8

    .line 2202378
    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2202379
    const v0, 0x7f0d126e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->g:Landroid/widget/TextView;

    .line 2202380
    const v0, 0x7f0d126f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->h:Landroid/widget/TextView;

    .line 2202381
    const v0, 0x7f0d1270

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->d:Lcom/facebook/fbui/facepile/FacepileView;

    .line 2202382
    const v0, 0x7f0d1271

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->e:Landroid/widget/TextView;

    .line 2202383
    const v0, 0x7f0d1272

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->f:Landroid/view/View;

    .line 2202384
    const v0, 0x7f0d0b63

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2202385
    const v1, 0x7f0d0b64

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 2202386
    iget-object v6, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->g:Landroid/widget/TextView;

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(I)V

    .line 2202387
    iget-object v5, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->h:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(I)V

    .line 2202388
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 2202389
    new-instance v2, LX/F7f;

    invoke-direct {v2, p0}, LX/F7f;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderIntroView;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2202390
    new-instance v1, LX/47x;

    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2202391
    sget-object v2, LX/F7k;->NEW_STYLE_DOUBLE_STEP:LX/F7k;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/F7k;->DAILY_DIALOGUE_STYLE:LX/F7k;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/F7k;->REJECT_REG_TERMS_DOUBLE_STEP:LX/F7k;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/F7k;->REJECT_REG_TERMS_DD_STYLE:LX/F7k;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/F7k;->EVENT_INVITE_GUEST_STYLE:LX/F7k;

    if-ne p1, v2, :cond_1

    .line 2202392
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "{SETTINGS_TOKEN}"

    const-string v4, "{MANAGE_OR_DELETE_TOKEN}"

    const-string v5, "{LEARN_MORE_TOKEN}"

    invoke-static {v2, v3, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2202393
    const-string v2, "{SETTINGS_TOKEN}"

    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0833b2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/F7g;

    invoke-direct {v4, p0}, LX/F7g;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderIntroView;)V

    invoke-virtual {v1, v2, v3, v4, v7}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2202394
    :goto_1
    const-string v2, "{MANAGE_OR_DELETE_TOKEN}"

    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0833b1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/F7h;

    invoke-direct {v4, p0}, LX/F7h;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderIntroView;)V

    invoke-virtual {v1, v2, v3, v4, v7}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2202395
    const-string v2, "{LEARN_MORE_TOKEN}"

    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0833b3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/F7i;

    invoke-direct {v4, p0}, LX/F7i;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderIntroView;)V

    invoke-virtual {v1, v2, v3, v4, v7}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2202396
    iget-object v2, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->a:LX/8tu;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2202397
    invoke-virtual {v1}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2202398
    return-void

    .line 2202399
    :pswitch_0
    const v4, 0x7f030389

    .line 2202400
    const v3, 0x7f0833a7

    .line 2202401
    const v2, 0x7f0833ab

    .line 2202402
    const v1, 0x7f0833b0

    .line 2202403
    const v0, 0x7f0833b4

    move v5, v3

    move v3, v1

    move v8, v2

    move v2, v0

    move v0, v4

    move v4, v8

    .line 2202404
    goto/16 :goto_0

    .line 2202405
    :pswitch_1
    const v4, 0x7f030389

    .line 2202406
    const v3, 0x7f0833a6

    .line 2202407
    const v2, 0x7f0833aa

    .line 2202408
    const v1, 0x7f0833af

    .line 2202409
    const v0, 0x7f0833b4

    move v5, v3

    move v3, v1

    move v8, v2

    move v2, v0

    move v0, v4

    move v4, v8

    .line 2202410
    goto/16 :goto_0

    .line 2202411
    :pswitch_2
    const v4, 0x7f0307d3

    .line 2202412
    const v3, 0x7f0833a6

    .line 2202413
    const v2, 0x7f0833aa

    .line 2202414
    const v1, 0x7f0833af

    .line 2202415
    const v0, 0x7f0833b4

    move v5, v3

    move v3, v1

    move v8, v2

    move v2, v0

    move v0, v4

    move v4, v8

    .line 2202416
    goto/16 :goto_0

    .line 2202417
    :pswitch_3
    const v4, 0x7f030389

    .line 2202418
    const v3, 0x7f0833a8

    .line 2202419
    const v2, 0x7f0833ac

    .line 2202420
    const v1, 0x7f0833af

    .line 2202421
    const v0, 0x7f0833b5

    move v5, v3

    move v3, v1

    move v8, v2

    move v2, v0

    move v0, v4

    move v4, v8

    .line 2202422
    goto/16 :goto_0

    .line 2202423
    :pswitch_4
    const v4, 0x7f0307d3

    .line 2202424
    const v3, 0x7f0833a8

    .line 2202425
    const v2, 0x7f0833ac

    .line 2202426
    const v1, 0x7f0833af

    .line 2202427
    const v0, 0x7f0833b5

    move v5, v3

    move v3, v1

    move v8, v2

    move v2, v0

    move v0, v4

    move v4, v8

    .line 2202428
    goto/16 :goto_0

    .line 2202429
    :pswitch_5
    const v4, 0x7f0307d3

    .line 2202430
    const v3, 0x7f0833a6

    .line 2202431
    const v2, 0x7f0833ad

    .line 2202432
    const v1, 0x7f0833af

    .line 2202433
    const v0, 0x7f0833b4

    move v5, v3

    move v3, v1

    move v8, v2

    move v2, v0

    move v0, v4

    move v4, v8

    .line 2202434
    goto/16 :goto_0

    .line 2202435
    :cond_1
    sget-object v2, LX/F7k;->NEW_STYLE_SINGLE_STEP:LX/F7k;

    if-ne p1, v2, :cond_2

    .line 2202436
    iget-object v2, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->g:Landroid/widget/TextView;

    const/16 v4, 0x53

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 2202437
    iget-object v2, p0, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->h:Landroid/widget/TextView;

    const/4 v4, 0x3

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 2202438
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "{MANAGE_OR_DELETE_TOKEN}"

    const-string v4, "{LEARN_MORE_TOKEN}"

    invoke-static {v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
