.class public Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0l6;


# instance fields
.field public p:LX/8tu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/9Tk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Or;
    .annotation runtime Lcom/facebook/contacts/upload/annotation/IsContactsUploadBackgroundTaskEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Or;
    .annotation runtime Lcom/facebook/contactlogs/annotation/IsContactLogsUploadEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:Z

.field public w:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2202497
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2202498
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2202522
    const v1, 0x7f0833c1

    .line 2202523
    iget-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->v:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->w:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->u:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2202524
    :cond_0
    const v0, 0x7f0833c2

    .line 2202525
    iget-boolean v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->w:Z

    if-nez v1, :cond_1

    .line 2202526
    const v0, 0x7f0833c3

    .line 2202527
    :cond_1
    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "{MANAGE_OR_DELETE_TOKEN}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private static a(Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;LX/8tu;LX/17W;LX/9Tk;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;",
            "LX/8tu;",
            "LX/17W;",
            "LX/9Tk;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2202521
    iput-object p1, p0, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->p:LX/8tu;

    iput-object p2, p0, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->q:LX/17W;

    iput-object p3, p0, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->r:LX/9Tk;

    iput-object p4, p0, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->s:LX/0Or;

    iput-object p5, p0, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->t:LX/0Or;

    iput-object p6, p0, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->u:LX/0Or;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;

    invoke-static {v6}, LX/8tu;->a(LX/0QB;)LX/8tu;

    move-result-object v1

    check-cast v1, LX/8tu;

    invoke-static {v6}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v2

    check-cast v2, LX/17W;

    invoke-static {v6}, LX/9Tk;->a(LX/0QB;)LX/9Tk;

    move-result-object v3

    check-cast v3, LX/9Tk;

    const/16 v4, 0x30d

    invoke-static {v6, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x308

    invoke-static {v6, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v7, 0x15e7

    invoke-static {v6, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->a(Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;LX/8tu;LX/17W;LX/9Tk;LX/0Or;LX/0Or;LX/0Or;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2202502
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2202503
    invoke-static {p0, p0}, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2202504
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v5}, LX/03R;->asBoolean(Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->v:Z

    .line 2202505
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->t:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v5}, LX/03R;->asBoolean(Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->w:Z

    .line 2202506
    const v0, 0x7f040056

    const v1, 0x7f0400a6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->overridePendingTransition(II)V

    .line 2202507
    const v0, 0x7f0306d5

    invoke-virtual {p0, v0}, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->setContentView(I)V

    .line 2202508
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2202509
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2202510
    const v1, 0x7f080faa

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 2202511
    new-instance v1, LX/F7l;

    invoke-direct {v1, p0}, LX/F7l;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2202512
    const v0, 0x7f0d1273

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2202513
    new-instance v2, LX/47x;

    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v2, v1}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2202514
    invoke-direct {p0}, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2202515
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->u:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2202516
    const-string v1, "{MANAGE_OR_DELETE_TOKEN}"

    const v3, 0x7f0833b1

    invoke-virtual {p0, v3}, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/47x;->a(Ljava/lang/String;Ljava/lang/CharSequence;)LX/47x;

    .line 2202517
    :goto_0
    invoke-virtual {v2}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2202518
    return-void

    .line 2202519
    :cond_0
    const-string v1, "{MANAGE_OR_DELETE_TOKEN}"

    const v3, 0x7f0833b1

    invoke-virtual {p0, v3}, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/F7m;

    invoke-direct {v4, p0}, LX/F7m;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;)V

    const/16 v5, 0x21

    invoke-virtual {v2, v1, v3, v4, v5}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2202520
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->p:LX/8tu;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    goto :goto_0
.end method

.method public final finish()V
    .locals 2

    .prologue
    .line 2202499
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 2202500
    const v0, 0x7f0400a1

    const v1, 0x7f040093

    invoke-virtual {p0, v0, v1}, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->overridePendingTransition(II)V

    .line 2202501
    return-void
.end method
