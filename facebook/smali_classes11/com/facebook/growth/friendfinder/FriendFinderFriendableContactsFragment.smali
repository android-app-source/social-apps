.class public Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0fv;


# static fields
.field private static final p:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;",
            ">;"
        }
    .end annotation
.end field

.field public static final q:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:Landroid/view/View;

.field public B:Lcom/facebook/ui/search/SearchEditText;

.field public C:Landroid/widget/TextView;

.field public D:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public E:LX/F73;

.field public F:I

.field public G:LX/89v;

.field public H:LX/0g8;

.field private I:J

.field public J:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation
.end field

.field private K:Z

.field public L:Z

.field public M:Z

.field public N:Z

.field private O:Z

.field public P:I

.field private Q:J

.field private R:J

.field public S:I

.field public T:Z

.field public a:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/30e;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/30Z;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/F74;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/F7X;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/9Tk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/F7y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Tf;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final r:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final s:Ljava/lang/String;

.field private final t:Landroid/view/View$OnClickListener;

.field private final u:LX/1DI;

.field private final v:LX/F7U;

.field public w:LX/F7s;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Landroid/view/View;

.field private y:Landroid/widget/ProgressBar;

.field public z:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2201982
    const-class v0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    .line 2201983
    sput-object v0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->p:Ljava/lang/Class;

    const-string v1, "friend_finder_add_friends_fragment"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->q:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 2201948
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2201949
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->r:Ljava/util/Set;

    .line 2201950
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->s:Ljava/lang/String;

    .line 2201951
    new-instance v0, LX/F7P;

    invoke-direct {v0, p0}, LX/F7P;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->t:Landroid/view/View$OnClickListener;

    .line 2201952
    new-instance v0, LX/F7Q;

    invoke-direct {v0, p0}, LX/F7Q;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->u:LX/1DI;

    .line 2201953
    new-instance v0, LX/F7U;

    invoke-direct {v0, p0}, LX/F7U;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V

    move-object v0, v0

    .line 2201954
    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->v:LX/F7U;

    .line 2201955
    iput-boolean v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->K:Z

    .line 2201956
    iput-boolean v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->L:Z

    .line 2201957
    iput-boolean v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->M:Z

    .line 2201958
    iput-boolean v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->N:Z

    .line 2201959
    iput-boolean v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->O:Z

    .line 2201960
    iput v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->P:I

    .line 2201961
    iput-wide v2, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->Q:J

    .line 2201962
    iput-wide v2, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->R:J

    .line 2201963
    iput v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->S:I

    .line 2201964
    iput-boolean v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->T:Z

    return-void
.end method

.method public static a(LX/89v;Z)Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;
    .locals 3

    .prologue
    .line 2201975
    new-instance v0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    invoke-direct {v0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;-><init>()V

    .line 2201976
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2201977
    const-string v2, "ci_flow"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2201978
    const-string v2, "go_to_profile_enabled"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2201979
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2201980
    return-object v0
.end method

.method private static a(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;LX/0Sh;Ljava/util/concurrent/ExecutorService;LX/0aG;LX/30e;LX/0SG;LX/30Z;LX/17W;LX/F74;LX/F7X;LX/9Tk;LX/F7y;Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;LX/0Uh;LX/0Tf;LX/1Ck;)V
    .locals 0

    .prologue
    .line 2201981
    iput-object p1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->a:LX/0Sh;

    iput-object p2, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->b:Ljava/util/concurrent/ExecutorService;

    iput-object p3, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->c:LX/0aG;

    iput-object p4, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->d:LX/30e;

    iput-object p5, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->e:LX/0SG;

    iput-object p6, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->f:LX/30Z;

    iput-object p7, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->g:LX/17W;

    iput-object p8, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->h:LX/F74;

    iput-object p9, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->i:LX/F7X;

    iput-object p10, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->j:LX/9Tk;

    iput-object p11, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->k:LX/F7y;

    iput-object p12, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->l:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    iput-object p13, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->m:LX/0Uh;

    iput-object p14, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->n:LX/0Tf;

    iput-object p15, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->o:LX/1Ck;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 16

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v15

    move-object/from16 v0, p0

    check-cast v0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    invoke-static {v15}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {v15}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {v15}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    invoke-static {v15}, LX/30e;->a(LX/0QB;)LX/30e;

    move-result-object v4

    check-cast v4, LX/30e;

    invoke-static {v15}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {v15}, LX/30Z;->a(LX/0QB;)LX/30Z;

    move-result-object v6

    check-cast v6, LX/30Z;

    invoke-static {v15}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v7

    check-cast v7, LX/17W;

    const-class v8, LX/F74;

    invoke-interface {v15, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/F74;

    invoke-static {v15}, LX/F7X;->a(LX/0QB;)LX/F7X;

    move-result-object v9

    check-cast v9, LX/F7X;

    invoke-static {v15}, LX/9Tk;->a(LX/0QB;)LX/9Tk;

    move-result-object v10

    check-cast v10, LX/9Tk;

    invoke-static {v15}, LX/F7y;->b(LX/0QB;)LX/F7y;

    move-result-object v11

    check-cast v11, LX/F7y;

    invoke-static {v15}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->b(LX/0QB;)Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    move-result-object v12

    check-cast v12, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    invoke-static {v15}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v13

    check-cast v13, LX/0Uh;

    invoke-static {v15}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v14

    check-cast v14, LX/0Tf;

    invoke-static {v15}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v15

    check-cast v15, LX/1Ck;

    invoke-static/range {v0 .. v15}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->a(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;LX/0Sh;Ljava/util/concurrent/ExecutorService;LX/0aG;LX/30e;LX/0SG;LX/30Z;LX/17W;LX/F74;LX/F7X;LX/9Tk;LX/F7y;Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;LX/0Uh;LX/0Tf;LX/1Ck;)V

    return-void
.end method

.method private a(Z)V
    .locals 6

    .prologue
    .line 2201984
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->d:LX/30e;

    invoke-virtual {v0}, LX/30e;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2201985
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->w:LX/F7s;

    if-eqz v0, :cond_0

    .line 2201986
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->w:LX/F7s;

    invoke-virtual {v0}, LX/F7s;->a()V

    .line 2201987
    :cond_0
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->x:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2201988
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->d:LX/30e;

    invoke-virtual {v0}, LX/30e;->f()Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->a$redex0(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;Ljava/util/List;)V

    .line 2201989
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    sget-object v1, LX/F72;->DEFAULT:LX/F72;

    invoke-virtual {v0, v1}, LX/F73;->a(LX/F72;)V

    .line 2201990
    invoke-static {p0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->t(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V

    .line 2201991
    const-string v3, "valid"

    .line 2201992
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->N:Z

    .line 2201993
    :goto_0
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->j:LX/9Tk;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    sget-object v2, LX/9Ti;->FRIENDABLE_CONTACTS_API:LX/9Ti;

    iget-boolean v5, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->O:Z

    move v4, p1

    invoke-virtual/range {v0 .. v5}, LX/9Tk;->a(Ljava/lang/String;LX/9Ti;Ljava/lang/String;ZZ)V

    .line 2201994
    return-void

    .line 2201995
    :cond_1
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->x:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2201996
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    sget-object v1, LX/F72;->LOADING_MORE:LX/F72;

    invoke-virtual {v0, v1}, LX/F73;->a(LX/F72;)V

    .line 2201997
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->d:LX/30e;

    invoke-virtual {v0}, LX/30e;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2201998
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->d:LX/30e;

    .line 2201999
    iget v1, v0, LX/30e;->m:I

    move v0, v1

    .line 2202000
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->d:LX/30e;

    .line 2202001
    iget v2, v1, LX/30e;->l:I

    move v1, v2

    .line 2202002
    invoke-static {p0, v0, v1}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->a$redex0(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;II)V

    .line 2202003
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->d:LX/30e;

    invoke-virtual {v0}, LX/30e;->f()Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->a$redex0(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;Ljava/util/List;)V

    .line 2202004
    const-string v0, "uploading"

    .line 2202005
    :goto_1
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->d:LX/30e;

    iget-object v2, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->v:LX/F7U;

    .line 2202006
    iput-object v2, v1, LX/30e;->h:LX/F7U;

    .line 2202007
    move-object v3, v0

    goto :goto_0

    .line 2202008
    :cond_2
    invoke-static {p0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->p(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V

    .line 2202009
    const-string v0, "need_full_upload"

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;II)V
    .locals 5

    .prologue
    .line 2202010
    if-nez p2, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    .line 2202011
    :goto_0
    const v1, 0x459c4000    # 5000.0f

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 2202012
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->y:Landroid/widget/ProgressBar;

    const-string v2, "progress"

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    aput v0, v3, v4

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 2202013
    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 2202014
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2202015
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 2202016
    return-void

    .line 2202017
    :cond_0
    int-to-float v0, p1

    int-to-float v1, p2

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/EjK;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2202046
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 2202047
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->r:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    .line 2202048
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EjK;

    .line 2202049
    iget-object v4, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->r:Ljava/util/Set;

    iget-wide v6, v0, LX/EjK;->a:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2202050
    iget-boolean v4, v0, LX/EjK;->e:Z

    move v4, v4

    .line 2202051
    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->i:LX/F7X;

    iget-wide v6, v0, LX/EjK;->a:J

    invoke-virtual {v4, v6, v7}, LX/F7X;->b(J)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2202052
    new-instance v4, LX/F7K;

    invoke-direct {v4}, LX/F7K;-><init>()V

    iget-wide v6, v0, LX/EjK;->a:J

    .line 2202053
    iput-wide v6, v4, LX/F7K;->a:J

    .line 2202054
    move-object v4, v4

    .line 2202055
    iget-object v5, v0, LX/EjK;->c:Ljava/lang/String;

    .line 2202056
    iput-object v5, v4, LX/F7K;->b:Ljava/lang/String;

    .line 2202057
    move-object v4, v4

    .line 2202058
    iget-object v5, v0, LX/EjK;->b:Ljava/lang/String;

    .line 2202059
    iput-object v5, v4, LX/F7K;->c:Ljava/lang/String;

    .line 2202060
    move-object v4, v4

    .line 2202061
    iget v5, v0, LX/EjK;->d:I

    .line 2202062
    iput v5, v4, LX/F7K;->d:I

    .line 2202063
    move-object v4, v4

    .line 2202064
    const/4 v5, 0x0

    .line 2202065
    iput-boolean v5, v4, LX/F7K;->e:Z

    .line 2202066
    move-object v4, v4

    .line 2202067
    invoke-virtual {v4}, LX/F7K;->a()LX/F7L;

    move-result-object v4

    .line 2202068
    iget-object v5, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->r:Ljava/util/Set;

    iget-wide v6, v0, LX/EjK;->a:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2202069
    invoke-virtual {v1, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2202070
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    .line 2202071
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2202072
    if-eqz v2, :cond_2

    .line 2202073
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->j:LX/9Tk;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    invoke-static {p0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->u$redex0(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)J

    move-result-wide v2

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {p0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->w(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)J

    move-result-wide v5

    invoke-virtual/range {v0 .. v6}, LX/9Tk;->a(Ljava/lang/String;JIJ)V

    .line 2202074
    :cond_2
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    invoke-virtual {v0, v7}, LX/F73;->a(Ljava/util/List;)V

    .line 2202075
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->j:LX/9Tk;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    invoke-static {p0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->u$redex0(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)J

    move-result-wide v2

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {p0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->w(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)J

    move-result-wide v5

    sget-object v7, LX/9Tj;->FRIENDABLE_CONTACTS_PAGE_FETCHED:LX/9Tj;

    invoke-virtual/range {v0 .. v7}, LX/9Tk;->a(Ljava/lang/String;JIJLX/9Tj;)V

    .line 2202076
    :cond_3
    return-void
.end method

.method public static d(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 2202018
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->C:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2202019
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->D:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const v1, 0x7f08003c

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->u:LX/1DI;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 2202020
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->A:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2202021
    return-void
.end method

.method public static m(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2202033
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    sget-object v1, LX/F72;->DEFAULT:LX/F72;

    invoke-virtual {v0, v1}, LX/F73;->a(LX/F72;)V

    .line 2202034
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->C:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2202035
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->D:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2202036
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->D:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2202037
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    sget-object v1, LX/89v;->FRIENDS_CENTER:LX/89v;

    if-ne v0, v1, :cond_0

    .line 2202038
    invoke-static {p0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->q(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V

    .line 2202039
    :goto_0
    return-void

    .line 2202040
    :cond_0
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->x:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2202041
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    sget-object v1, LX/F72;->LOADING_MORE:LX/F72;

    invoke-virtual {v0, v1}, LX/F73;->a(LX/F72;)V

    .line 2202042
    invoke-static {p0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->p(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V

    .line 2202043
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->d:LX/30e;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->v:LX/F7U;

    .line 2202044
    iput-object v1, v0, LX/30e;->h:LX/F7U;

    .line 2202045
    goto :goto_0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 2202077
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->K:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    sget-object v1, LX/89v;->FRIENDS_CENTER:LX/89v;

    if-eq v0, v1, :cond_1

    .line 2202078
    :cond_0
    :goto_0
    return-void

    .line 2202079
    :cond_1
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2202080
    if-eqz v0, :cond_0

    .line 2202081
    const v1, 0x7f08339e

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2202082
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    .line 2202083
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    goto :goto_0
.end method

.method public static p(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V
    .locals 3

    .prologue
    .line 2202031
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment$10;

    invoke-direct {v1, p0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment$10;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V

    const v2, -0x79647741

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2202032
    return-void
.end method

.method public static q(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V
    .locals 1

    .prologue
    .line 2202026
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->o:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2202027
    :goto_0
    return-void

    .line 2202028
    :cond_0
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->k:LX/F7y;

    invoke-virtual {v0}, LX/F7y;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2202029
    invoke-direct {p0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->r()V

    goto :goto_0

    .line 2202030
    :cond_1
    invoke-static {p0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->t(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V

    goto :goto_0
.end method

.method private r()V
    .locals 4

    .prologue
    .line 2202022
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->R:J

    .line 2202023
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    sget-object v1, LX/F72;->LOADING_MORE:LX/F72;

    invoke-virtual {v0, v1}, LX/F73;->a(LX/F72;)V

    .line 2202024
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->o:LX/1Ck;

    const-string v1, "fetch_friendable_contacts"

    new-instance v2, LX/F7M;

    invoke-direct {v2, p0}, LX/F7M;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V

    new-instance v3, LX/F7N;

    invoke-direct {v3, p0}, LX/F7N;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2202025
    return-void
.end method

.method public static t(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V
    .locals 13

    .prologue
    .line 2201965
    iget-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->L:Z

    if-nez v0, :cond_0

    .line 2201966
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->Q:J

    .line 2201967
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->j:LX/9Tk;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    invoke-static {p0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->u$redex0(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->r:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    .line 2201968
    iget-object v5, v0, LX/9Tk;->a:LX/0Zb;

    sget-object v6, LX/9Tj;->PYMK_START_FETCHING:LX/9Tj;

    invoke-virtual {v6}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/9Tk;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "ci_flow"

    invoke-virtual {v6, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "time_since_creation"

    invoke-virtual {v6, v7, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "fetched_candidates_size"

    invoke-virtual {v6, v7, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-interface {v5, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2201969
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 2201970
    new-instance v5, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;

    iget v6, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->F:I

    iget-object v8, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->s:Ljava/lang/String;

    iget-object v9, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    iget-object v10, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->r:Ljava/util/Set;

    invoke-static {v10}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-direct {v5, v6, v8, v9, v10}, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;-><init>(ILjava/lang/String;LX/89v;Ljava/util/List;)V

    .line 2201971
    const-string v6, "growthFriendFinderPYMKParamsKey"

    invoke-virtual {v7, v6, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2201972
    iget-object v11, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->o:LX/1Ck;

    const/4 v12, 0x0

    iget-object v5, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->c:LX/0aG;

    const-string v6, "growth_friend_finder_pymk"

    sget-object v8, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v9, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->q:Lcom/facebook/common/callercontext/CallerContext;

    const v10, 0x6100ebd5

    invoke-static/range {v5 .. v10}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v5

    invoke-interface {v5}, LX/1MF;->start()LX/1ML;

    move-result-object v5

    new-instance v6, LX/F7O;

    invoke-direct {v6, p0}, LX/F7O;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V

    invoke-virtual {v11, v12, v5, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2201973
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->L:Z

    .line 2201974
    :cond_0
    return-void
.end method

.method public static u$redex0(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)J
    .locals 4

    .prologue
    .line 2201850
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->I:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public static v(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)J
    .locals 4

    .prologue
    .line 2201851
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->Q:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public static w(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)J
    .locals 4

    .prologue
    .line 2201852
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->R:J

    sub-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2201853
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2201854
    new-instance v0, LX/4nE;

    invoke-direct {v0}, LX/4nE;-><init>()V

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 2201855
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2201856
    invoke-static {}, LX/0wB;->c()I

    move-result v0

    iput v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->F:I

    .line 2201857
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2201858
    if-nez v0, :cond_0

    sget-object v0, LX/89v;->UNKNOWN:LX/89v;

    :goto_0
    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    .line 2201859
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2201860
    if-eqz v0, :cond_1

    .line 2201861
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2201862
    const-string v1, "go_to_profile_enabled"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->M:Z

    .line 2201863
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->I:J

    .line 2201864
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->j:LX/9Tk;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    .line 2201865
    iget-object v2, v0, LX/9Tk;->a:LX/0Zb;

    sget-object v3, LX/9Tj;->OPENED:LX/9Tj;

    invoke-virtual {v3}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/9Tk;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p0, "api"

    sget-object p1, LX/9Ti;->FRIENDABLE_CONTACTS_API:LX/9Ti;

    invoke-virtual {p1}, LX/9Ti;->getApiName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p0, "ci_flow"

    invoke-virtual {v3, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2201866
    return-void

    .line 2201867
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2201868
    const-string v1, "ci_flow"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/89v;

    goto :goto_0

    .line 2201869
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 2201870
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->x:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->A:Landroid/view/View;

    if-nez v0, :cond_1

    .line 2201871
    :cond_0
    :goto_0
    return-void

    .line 2201872
    :cond_1
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 2201873
    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 2201874
    new-instance v1, LX/F7T;

    invoke-direct {v1, p0}, LX/F7T;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2201875
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->x:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2201876
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->H:LX/0g8;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0g8;->g(I)V

    .line 2201877
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 2201878
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->H:LX/0g8;

    invoke-interface {v0}, LX/0g8;->n()Z

    move-result v0

    return v0
.end method

.method public final k()LX/0g8;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2201879
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->H:LX/0g8;

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x66916de5

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2201880
    const v1, 0x7f0306d0

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x5a5438de

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x4cd6a59b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2201833
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->o:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2201834
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->d:LX/30e;

    .line 2201835
    iput-object v3, v1, LX/30e;->h:LX/F7U;

    .line 2201836
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->J:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    .line 2201837
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->J:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2201838
    :cond_0
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    invoke-virtual {v1}, LX/F73;->b()V

    .line 2201839
    iput-object v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->x:Landroid/view/View;

    .line 2201840
    iput-object v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->y:Landroid/widget/ProgressBar;

    .line 2201841
    iput-object v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->z:Landroid/widget/TextView;

    .line 2201842
    iput-object v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->A:Landroid/view/View;

    .line 2201843
    iput-object v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->B:Lcom/facebook/ui/search/SearchEditText;

    .line 2201844
    iput-object v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->D:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2201845
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->C:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2201846
    iput-object v3, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->C:Landroid/widget/TextView;

    .line 2201847
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->H:LX/0g8;

    invoke-interface {v1, v3}, LX/0g8;->a(LX/2ii;)V

    .line 2201848
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2201849
    const/16 v1, 0x2b

    const v2, -0x7fbcf1de

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x248acaae

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2201881
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->B:Lcom/facebook/ui/search/SearchEditText;

    .line 2201882
    invoke-static {v1}, Lcom/facebook/ui/search/SearchEditText;->i(Lcom/facebook/ui/search/SearchEditText;)V

    .line 2201883
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->K:Z

    .line 2201884
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2201885
    const/16 v1, 0x2b

    const v2, 0x49f3616a    # 1993773.2f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6f260f10

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2201944
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2201945
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->K:Z

    .line 2201946
    invoke-direct {p0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->n()V

    .line 2201947
    const/16 v1, 0x2b

    const v2, -0x703be448

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 13

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x282defd5

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 2201886
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->r:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->N:Z

    if-nez v0, :cond_0

    .line 2201887
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->j:LX/9Tk;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    invoke-static {p0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->u$redex0(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)J

    move-result-wide v2

    .line 2201888
    iget-object v4, v0, LX/9Tk;->a:LX/0Zb;

    sget-object v5, LX/9Tj;->CANCELED:LX/9Tj;

    invoke-virtual {v5}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/9Tk;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "api"

    sget-object v7, LX/9Ti;->FRIENDABLE_CONTACTS_API:LX/9Ti;

    invoke-virtual {v7}, LX/9Ti;->getApiName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "ci_flow"

    invoke-virtual {v5, v6, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "time_since_creation"

    invoke-virtual {v5, v6, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2201889
    :goto_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2201890
    const v0, 0x1669d567

    invoke-static {v0, v8}, LX/02F;->f(II)V

    return-void

    .line 2201891
    :cond_0
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->j:LX/9Tk;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    invoke-static {p0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->u$redex0(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->r:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    iget v6, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->P:I

    invoke-virtual {v5, v6}, LX/F73;->a(I)I

    move-result v5

    iget-object v6, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    invoke-virtual {v6}, LX/F73;->e()I

    move-result v6

    iget-object v7, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    invoke-virtual {v7}, LX/F73;->f()I

    move-result v7

    .line 2201892
    iget-object v9, v0, LX/9Tk;->a:LX/0Zb;

    sget-object v10, LX/9Tj;->HOW_MANY_SEEN:LX/9Tj;

    invoke-virtual {v10}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, LX/9Tk;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "api"

    sget-object v12, LX/9Ti;->FRIENDABLE_CONTACTS_API:LX/9Ti;

    invoke-virtual {v12}, LX/9Ti;->getApiName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "ci_flow"

    invoke-virtual {v10, v11, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "time_since_creation"

    invoke-virtual {v10, v11, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "total_candidates"

    invoke-virtual {v10, v11, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "how_many_seen"

    invoke-virtual {v10, v11, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "friendable_count"

    invoke-virtual {v10, v11, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "pymk_count"

    invoke-virtual {v10, v11, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    invoke-interface {v9, v10}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2201893
    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 2201894
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2201895
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->I:J

    .line 2201896
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->m:LX/0Uh;

    const/16 v1, 0x411

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->T:Z

    .line 2201897
    iget-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->T:Z

    if-eqz v0, :cond_2

    .line 2201898
    const v0, 0x7f0d1264

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->x:Landroid/view/View;

    .line 2201899
    const v0, 0x7f0d1265

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->y:Landroid/widget/ProgressBar;

    .line 2201900
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->y:Landroid/widget/ProgressBar;

    const/16 v1, 0x1388

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 2201901
    const v0, 0x7f0d1266

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->z:Landroid/widget/TextView;

    .line 2201902
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->z:Landroid/widget/TextView;

    const v1, 0x7f0833c7

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2201903
    :goto_0
    const v0, 0x7f0d1268

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->A:Landroid/view/View;

    .line 2201904
    const v0, 0x7f0d126a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->C:Landroid/widget/TextView;

    .line 2201905
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->C:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->t:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2201906
    const v0, 0x7f0d126b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->D:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2201907
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->D:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2201908
    const v0, 0x7f0d1269

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/search/SearchEditText;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->B:Lcom/facebook/ui/search/SearchEditText;

    .line 2201909
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->B:Lcom/facebook/ui/search/SearchEditText;

    .line 2201910
    new-instance v1, LX/F7S;

    invoke-direct {v1, p0}, LX/F7S;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V

    move-object v1, v1

    .line 2201911
    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2201912
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->l:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    invoke-virtual {v0, v2}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->b(Z)V

    .line 2201913
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    if-nez v0, :cond_4

    .line 2201914
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->h:LX/F74;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/F74;->a(Landroid/content/Context;)LX/F73;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    .line 2201915
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->u:LX/1DI;

    .line 2201916
    iput-object v1, v0, LX/F73;->p:LX/1DI;

    .line 2201917
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->j:LX/9Tk;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    invoke-static {p0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->u$redex0(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)J

    move-result-wide v2

    sget-object v4, LX/9Tj;->FRIENDABLE_CONTACTS_START_FETCHING:LX/9Tj;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/9Tk;->a(Ljava/lang/String;JLX/9Tj;)V

    .line 2201918
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    sget-object v1, LX/89v;->FRIENDS_CENTER:LX/89v;

    if-ne v0, v1, :cond_3

    .line 2201919
    invoke-direct {p0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->r()V

    .line 2201920
    :cond_0
    :goto_1
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    .line 2201921
    const v1, 0x1020004

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setEmptyView(Landroid/view/View;)V

    .line 2201922
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2201923
    invoke-virtual {v0, v5}, Lcom/facebook/widget/listview/BetterListView;->setStickyHeaderEnabled(Z)V

    .line 2201924
    new-instance v1, LX/F7R;

    invoke-direct {v1, p0}, LX/F7R;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V

    .line 2201925
    iput-object v1, v0, Lcom/facebook/widget/listview/BetterListView;->y:LX/2i6;

    .line 2201926
    new-instance v1, LX/2iI;

    invoke-direct {v1, v0}, LX/2iI;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    iput-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->H:LX/0g8;

    .line 2201927
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->H:LX/0g8;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    invoke-interface {v0, v1}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2201928
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->H:LX/0g8;

    new-instance v1, LX/F7V;

    invoke-direct {v1, p0}, LX/F7V;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V

    invoke-interface {v0, v1}, LX/0g8;->a(LX/0fx;)V

    .line 2201929
    iget-boolean v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->M:Z

    if-eqz v0, :cond_1

    .line 2201930
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->H:LX/0g8;

    new-instance v1, LX/F7W;

    invoke-direct {v1, p0}, LX/F7W;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V

    invoke-interface {v0, v1}, LX/0g8;->a(LX/2ii;)V

    .line 2201931
    :cond_1
    return-void

    .line 2201932
    :cond_2
    const v0, 0x7f0d1267

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->x:Landroid/view/View;

    goto/16 :goto_0

    .line 2201933
    :cond_3
    invoke-direct {p0, v5}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->a(Z)V

    goto :goto_1

    .line 2201934
    :cond_4
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    invoke-virtual {v0}, LX/F73;->a()V

    .line 2201935
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    sget-object v1, LX/89v;->FRIENDS_CENTER:LX/89v;

    if-eq v0, v1, :cond_0

    .line 2201936
    invoke-direct {p0, v2}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->a(Z)V

    goto :goto_1
.end method

.method public final setUserVisibleHint(Z)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 2201937
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 2201938
    if-eqz p1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->O:Z

    if-nez v1, :cond_0

    .line 2201939
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->O:Z

    .line 2201940
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->j:LX/9Tk;

    iget-object v2, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    invoke-virtual {v2}, LX/89v;->name()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/9Ti;->FRIENDABLE_CONTACTS_API:LX/9Ti;

    iget-object v4, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->e:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->I:J

    sub-long/2addr v4, v6

    iget-object v6, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    if-nez v6, :cond_1

    move v6, v0

    :goto_0
    iget-object v7, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    if-nez v7, :cond_2

    move v7, v0

    :goto_1
    iget-boolean v8, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->N:Z

    invoke-virtual/range {v1 .. v8}, LX/9Tk;->a(Ljava/lang/String;LX/9Ti;JIIZ)V

    .line 2201941
    :cond_0
    invoke-direct {p0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->n()V

    .line 2201942
    return-void

    .line 2201943
    :cond_1
    iget-object v6, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    invoke-virtual {v6}, LX/F73;->e()I

    move-result v6

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    invoke-virtual {v0}, LX/F73;->f()I

    move-result v7

    goto :goto_1
.end method
