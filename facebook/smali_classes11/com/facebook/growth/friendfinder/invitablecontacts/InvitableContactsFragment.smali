.class public Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0fv;


# static fields
.field public static final m:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:Landroid/view/View;

.field public B:LX/F8M;

.field public C:LX/F8S;

.field public D:LX/89v;

.field public E:LX/0g7;

.field private F:Landroid/text/TextWatcher;

.field private G:J

.field private H:J

.field public I:I

.field public J:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation
.end field

.field private K:Z

.field public L:Z

.field private M:Z

.field public N:Z

.field public a:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/30e;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/30Z;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/9Tk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Tf;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/F8N;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/F8T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/F80;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final n:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Landroid/view/View$OnClickListener;

.field private final p:LX/1DI;

.field private final q:LX/F8b;

.field private r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public s:Lcom/facebook/ui/search/SearchEditText;

.field private t:Landroid/widget/TextView;

.field private u:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field private v:Landroid/view/View;

.field public w:Landroid/view/View;

.field private x:Landroid/widget/ProgressBar;

.field private y:Landroid/widget/TextView;

.field private z:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2204356
    const-class v0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2204438
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2204439
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->n:Ljava/util/Set;

    .line 2204440
    new-instance v0, LX/F8X;

    invoke-direct {v0, p0}, LX/F8X;-><init>(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)V

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->o:Landroid/view/View$OnClickListener;

    .line 2204441
    new-instance v0, LX/F8Y;

    invoke-direct {v0, p0}, LX/F8Y;-><init>(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)V

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->p:LX/1DI;

    .line 2204442
    new-instance v0, LX/F8b;

    invoke-direct {v0, p0}, LX/F8b;-><init>(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)V

    move-object v0, v0

    .line 2204443
    iput-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->q:LX/F8b;

    .line 2204444
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->H:J

    .line 2204445
    iput v2, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->I:I

    .line 2204446
    iput-boolean v2, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->K:Z

    .line 2204447
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->L:Z

    .line 2204448
    iput-boolean v2, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->M:Z

    .line 2204449
    iput-boolean v2, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->N:Z

    return-void
.end method

.method public static a(LX/89v;)Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;
    .locals 3

    .prologue
    .line 2204433
    new-instance v0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    invoke-direct {v0}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;-><init>()V

    .line 2204434
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2204435
    const-string v2, "ci_flow"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2204436
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2204437
    return-object v0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    invoke-static {p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/30e;->a(LX/0QB;)LX/30e;

    move-result-object v3

    check-cast v3, LX/30e;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {p0}, LX/30Z;->a(LX/0QB;)LX/30Z;

    move-result-object v5

    check-cast v5, LX/30Z;

    invoke-static {p0}, LX/9Tk;->a(LX/0QB;)LX/9Tk;

    move-result-object v6

    check-cast v6, LX/9Tk;

    invoke-static {p0}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->b(LX/0QB;)Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    move-result-object v7

    check-cast v7, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    invoke-static {p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, LX/0Tf;

    const-class v9, LX/F8N;

    invoke-interface {p0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/F8N;

    const-class v10, LX/F8T;

    invoke-interface {p0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/F8T;

    new-instance v0, LX/F80;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v11

    check-cast v11, LX/0tX;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v12

    check-cast v12, LX/0TD;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object p1

    check-cast p1, LX/0dC;

    invoke-direct {v0, v11, v12, p1}, LX/F80;-><init>(LX/0tX;LX/0TD;LX/0dC;)V

    move-object v11, v0

    check-cast v11, LX/F80;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v12

    check-cast v12, LX/0Uh;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p0

    check-cast p0, LX/1Ck;

    iput-object v2, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->a:Ljava/util/concurrent/ExecutorService;

    iput-object v3, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->b:LX/30e;

    iput-object v4, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->c:LX/0SG;

    iput-object v5, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->d:LX/30Z;

    iput-object v6, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->e:LX/9Tk;

    iput-object v7, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->f:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    iput-object v8, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->g:LX/0Tf;

    iput-object v9, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->h:LX/F8N;

    iput-object v10, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->i:LX/F8T;

    iput-object v11, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->j:LX/F80;

    iput-object v12, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->k:LX/0Uh;

    iput-object p0, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->l:LX/1Ck;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;II)V
    .locals 5

    .prologue
    .line 2204425
    if-nez p2, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    .line 2204426
    :goto_0
    const v1, 0x459c4000    # 5000.0f

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 2204427
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->x:Landroid/widget/ProgressBar;

    const-string v2, "progress"

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    aput v0, v3, v4

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 2204428
    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 2204429
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2204430
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 2204431
    return-void

    .line 2204432
    :cond_0
    int-to-float v0, p1

    int-to-float v1, p2

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/EjL;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2204409
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2204410
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v8

    .line 2204411
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EjL;

    .line 2204412
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->n:Ljava/util/Set;

    iget-wide v2, v0, LX/EjL;->a:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, LX/EjL;->c:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2204413
    iget-object v1, v0, LX/EjL;->b:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v4, v0, LX/EjL;->c:Ljava/lang/String;

    .line 2204414
    :goto_1
    new-instance v1, LX/F8P;

    iget-wide v2, v0, LX/EjL;->a:J

    iget-object v5, v0, LX/EjL;->c:Ljava/lang/String;

    invoke-static {p0}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->u(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)LX/5P4;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, LX/F8P;-><init>(JLjava/lang/String;Ljava/lang/String;LX/5P4;)V

    .line 2204415
    iget-object v2, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->n:Ljava/util/Set;

    iget-wide v4, v0, LX/EjL;->a:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2204416
    invoke-virtual {v7, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2204417
    :cond_1
    iget-object v4, v0, LX/EjL;->b:Ljava/lang/String;

    goto :goto_1

    .line 2204418
    :cond_2
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2204419
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 2204420
    if-eqz v8, :cond_3

    .line 2204421
    const/4 v1, 0x1

    invoke-static {p0, v1}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->a$redex0(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;Z)V

    .line 2204422
    :cond_3
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->B:LX/F8M;

    invoke-virtual {v1, v0}, LX/F8M;->a(Ljava/util/List;)V

    .line 2204423
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->e:LX/9Tk;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->D:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    invoke-static {p0}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->s(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)J

    move-result-wide v2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {p0}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->t(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)J

    move-result-wide v5

    sget-object v7, LX/9Tj;->INVITES_PAGE_FETCHED:LX/9Tj;

    invoke-virtual/range {v0 .. v7}, LX/9Tk;->a(Ljava/lang/String;JIJLX/9Tj;)V

    .line 2204424
    :cond_4
    return-void
.end method

.method public static a$redex0(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;Z)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 2204392
    sget-object v0, LX/F8W;->a:[I

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->B:LX/F8M;

    .line 2204393
    iget-object v4, v1, LX/F8M;->i:LX/F8L;

    move-object v1, v4

    .line 2204394
    invoke-virtual {v1}, LX/F8L;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2204395
    :cond_0
    :goto_0
    return-void

    .line 2204396
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->v:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2204397
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2204398
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->u:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const v1, 0x7f08003c

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->p:LX/1DI;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    goto :goto_0

    .line 2204399
    :pswitch_1
    if-eqz p1, :cond_1

    .line 2204400
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->v:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2204401
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->z:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2204402
    iget-boolean v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->L:Z

    if-eqz v0, :cond_0

    .line 2204403
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->A:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2204404
    :cond_1
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->v:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2204405
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2204406
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->u:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2204407
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->z:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2204408
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->A:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private b(Z)V
    .locals 6

    .prologue
    .line 2204370
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->b:LX/30e;

    invoke-virtual {v0}, LX/30e;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2204371
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->w:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2204372
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->b:LX/30e;

    invoke-virtual {v0}, LX/30e;->g()Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->a$redex0(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;Ljava/util/List;)V

    .line 2204373
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->B:LX/F8M;

    sget-object v1, LX/F8L;->DEFAULT:LX/F8L;

    invoke-virtual {v0, v1}, LX/F8M;->a(LX/F8L;)V

    .line 2204374
    const-string v3, "valid"

    .line 2204375
    :goto_0
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->e:LX/9Tk;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->D:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    sget-object v2, LX/9Ti;->INVITABLE_CONTACTS_API:LX/9Ti;

    iget-boolean v5, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->M:Z

    move v4, p1

    invoke-virtual/range {v0 .. v5}, LX/9Tk;->a(Ljava/lang/String;LX/9Ti;Ljava/lang/String;ZZ)V

    .line 2204376
    return-void

    .line 2204377
    :cond_0
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->w:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2204378
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->B:LX/F8M;

    sget-object v1, LX/F8L;->LOADING_MORE:LX/F8L;

    invoke-virtual {v0, v1}, LX/F8M;->a(LX/F8L;)V

    .line 2204379
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->b:LX/30e;

    invoke-virtual {v0}, LX/30e;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2204380
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->b:LX/30e;

    .line 2204381
    iget v1, v0, LX/30e;->m:I

    move v0, v1

    .line 2204382
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->b:LX/30e;

    .line 2204383
    iget v2, v1, LX/30e;->l:I

    move v1, v2

    .line 2204384
    invoke-static {p0, v0, v1}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->a$redex0(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;II)V

    .line 2204385
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->b:LX/30e;

    invoke-virtual {v0}, LX/30e;->g()Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->a$redex0(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;Ljava/util/List;)V

    .line 2204386
    const-string v0, "uploading"

    .line 2204387
    :goto_1
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->b:LX/30e;

    iget-object v2, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->q:LX/F8b;

    .line 2204388
    iput-object v2, v1, LX/30e;->i:LX/F8b;

    .line 2204389
    move-object v3, v0

    goto :goto_0

    .line 2204390
    :cond_1
    invoke-static {p0}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->m(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)V

    .line 2204391
    const-string v0, "need_full_upload"

    goto :goto_1
.end method

.method public static d(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2204357
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->B:LX/F8M;

    sget-object v1, LX/F8L;->DEFAULT:LX/F8L;

    invoke-virtual {v0, v1}, LX/F8M;->a(LX/F8L;)V

    .line 2204358
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->t:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2204359
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->u:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2204360
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->u:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2204361
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->D:LX/89v;

    sget-object v1, LX/89v;->FRIENDS_CENTER:LX/89v;

    if-ne v0, v1, :cond_0

    .line 2204362
    invoke-static {p0}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->n(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)V

    .line 2204363
    :goto_0
    return-void

    .line 2204364
    :cond_0
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->w:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2204365
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->B:LX/F8M;

    sget-object v1, LX/F8L;->LOADING_MORE:LX/F8L;

    invoke-virtual {v0, v1}, LX/F8M;->a(LX/F8L;)V

    .line 2204366
    invoke-static {p0}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->m(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)V

    .line 2204367
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->b:LX/30e;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->q:LX/F8b;

    .line 2204368
    iput-object v1, v0, LX/30e;->i:LX/F8b;

    .line 2204369
    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 2204349
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->K:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->D:LX/89v;

    sget-object v1, LX/89v;->FRIENDS_CENTER:LX/89v;

    if-eq v0, v1, :cond_1

    .line 2204350
    :cond_0
    :goto_0
    return-void

    .line 2204351
    :cond_1
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2204352
    if-eqz v0, :cond_0

    .line 2204353
    const v1, 0x7f0833a3

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2204354
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    .line 2204355
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    goto :goto_0
.end method

.method public static m(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)V
    .locals 3

    .prologue
    .line 2204347
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment$7;

    invoke-direct {v1, p0}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment$7;-><init>(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)V

    const v2, -0x25bb82ac

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2204348
    return-void
.end method

.method public static n(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)V
    .locals 1

    .prologue
    .line 2204344
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->l:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->j:LX/F80;

    invoke-virtual {v0}, LX/F80;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2204345
    :cond_0
    :goto_0
    return-void

    .line 2204346
    :cond_1
    invoke-direct {p0}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->o()V

    goto :goto_0
.end method

.method private o()V
    .locals 4

    .prologue
    .line 2204450
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->H:J

    .line 2204451
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->B:LX/F8M;

    sget-object v1, LX/F8L;->LOADING_MORE:LX/F8L;

    invoke-virtual {v0, v1}, LX/F8M;->a(LX/F8L;)V

    .line 2204452
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->l:LX/1Ck;

    const-string v1, "fetch_invitable_contacts"

    new-instance v2, LX/F8d;

    invoke-direct {v2, p0}, LX/F8d;-><init>(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)V

    new-instance v3, LX/F8e;

    invoke-direct {v3, p0}, LX/F8e;-><init>(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2204453
    return-void
.end method

.method public static s(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)J
    .locals 4

    .prologue
    .line 2204233
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->G:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public static t(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)J
    .locals 4

    .prologue
    .line 2204236
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->H:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public static u(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)LX/5P4;
    .locals 2

    .prologue
    .line 2204237
    sget-object v0, LX/F8W;->b:[I

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->D:LX/89v;

    invoke-virtual {v1}, LX/89v;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2204238
    sget-object v0, LX/5P4;->UNKNOWN:LX/5P4;

    :goto_0
    return-object v0

    .line 2204239
    :pswitch_0
    sget-object v0, LX/5P4;->CI_FRIENDS_CENTER:LX/5P4;

    goto :goto_0

    .line 2204240
    :pswitch_1
    sget-object v0, LX/5P4;->CI_NUX:LX/5P4;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2204241
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2204242
    const-class v0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    invoke-static {v0, p0}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2204243
    new-instance v0, LX/4nE;

    invoke-direct {v0}, LX/4nE;-><init>()V

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 2204244
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2204245
    if-nez v0, :cond_0

    sget-object v0, LX/89v;->UNKNOWN:LX/89v;

    :goto_0
    iput-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->D:LX/89v;

    .line 2204246
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->k:LX/0Uh;

    const/16 v2, 0x30

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->L:Z

    .line 2204247
    return-void

    .line 2204248
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2204249
    const-string v2, "ci_flow"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/89v;

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2204250
    goto :goto_1
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2204234
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->E:LX/0g7;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0g7;->g(I)V

    .line 2204235
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 2204251
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->E:LX/0g7;

    invoke-virtual {v0}, LX/0g7;->n()Z

    move-result v0

    return v0
.end method

.method public final k()LX/0g8;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2204252
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->E:LX/0g7;

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x5700bfd3

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2204253
    const v1, 0x7f03098e

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x57e34e8d

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, 0x5ae2e18d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2204254
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->l:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2204255
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->b:LX/30e;

    .line 2204256
    iput-object v3, v1, LX/30e;->i:LX/F8b;

    .line 2204257
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->J:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    .line 2204258
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->J:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2204259
    :cond_0
    iput-object v3, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->w:Landroid/view/View;

    .line 2204260
    iput-object v3, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->x:Landroid/widget/ProgressBar;

    .line 2204261
    iput-object v3, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->y:Landroid/widget/TextView;

    .line 2204262
    iput-object v3, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->z:Landroid/view/View;

    .line 2204263
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->s:Lcom/facebook/ui/search/SearchEditText;

    iget-object v2, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->F:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/facebook/ui/search/SearchEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2204264
    iput-object v3, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->s:Lcom/facebook/ui/search/SearchEditText;

    .line 2204265
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2204266
    iput-object v3, v1, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->s:LX/1OT;

    .line 2204267
    iput-object v3, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2204268
    iput-object v3, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->v:Landroid/view/View;

    .line 2204269
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->t:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2204270
    iput-object v3, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->t:Landroid/widget/TextView;

    .line 2204271
    iput-object v3, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->u:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2204272
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->E:LX/0g7;

    invoke-virtual {v1, v3}, LX/0g7;->a(LX/0fx;)V

    .line 2204273
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->A:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2204274
    iput-object v3, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->A:Landroid/view/View;

    .line 2204275
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2204276
    const/16 v1, 0x2b

    const v2, 0x66bf121c

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6d27dbab

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2204277
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->s:Lcom/facebook/ui/search/SearchEditText;

    .line 2204278
    invoke-static {v1}, Lcom/facebook/ui/search/SearchEditText;->i(Lcom/facebook/ui/search/SearchEditText;)V

    .line 2204279
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->K:Z

    .line 2204280
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2204281
    const/16 v1, 0x2b

    const v2, 0x1069ff1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x706225f5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2204282
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2204283
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->K:Z

    .line 2204284
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->D:LX/89v;

    sget-object v2, LX/89v;->FRIENDS_CENTER:LX/89v;

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->M:Z

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->B:LX/F8M;

    if-eqz v1, :cond_1

    .line 2204285
    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->B:LX/F8M;

    .line 2204286
    iget-boolean v2, v1, LX/F8M;->h:Z

    if-eqz v2, :cond_1

    iget-object v2, v1, LX/F8M;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2204287
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->e()V

    .line 2204288
    const/16 v1, 0x2b

    const v2, 0x5b53c897

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2204289
    :cond_2
    iget-object v2, v1, LX/F8M;->e:LX/F6f;

    invoke-virtual {v2}, LX/F6f;->a()V

    .line 2204290
    const/4 v2, 0x0

    iput-boolean v2, v1, LX/F8M;->h:Z

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 12
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2204291
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2204292
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->k:LX/0Uh;

    const/16 v3, 0x411

    invoke-virtual {v0, v3, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->N:Z

    .line 2204293
    iget-boolean v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->N:Z

    if-eqz v0, :cond_2

    .line 2204294
    const v0, 0x7f0d1860

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->w:Landroid/view/View;

    .line 2204295
    const v0, 0x7f0d1861

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->x:Landroid/widget/ProgressBar;

    .line 2204296
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->x:Landroid/widget/ProgressBar;

    const/16 v3, 0x1388

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 2204297
    const v0, 0x7f0d1862

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->y:Landroid/widget/TextView;

    .line 2204298
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->y:Landroid/widget/TextView;

    const v3, 0x7f0833c7

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 2204299
    :goto_0
    const v0, 0x7f0d1864

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->z:Landroid/view/View;

    .line 2204300
    new-instance v0, LX/F8a;

    invoke-direct {v0, p0}, LX/F8a;-><init>(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)V

    move-object v0, v0

    .line 2204301
    iput-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->F:Landroid/text/TextWatcher;

    .line 2204302
    const v0, 0x7f0d1865

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/search/SearchEditText;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->s:Lcom/facebook/ui/search/SearchEditText;

    .line 2204303
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->s:Lcom/facebook/ui/search/SearchEditText;

    iget-object v3, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->F:Landroid/text/TextWatcher;

    invoke-virtual {v0, v3}, Lcom/facebook/ui/search/SearchEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2204304
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->G:J

    .line 2204305
    const v0, 0x7f0d1867

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->A:Landroid/view/View;

    .line 2204306
    iget-boolean v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->L:Z

    if-eqz v0, :cond_0

    .line 2204307
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->A:Landroid/view/View;

    new-instance v3, LX/F8U;

    invoke-direct {v3, p0}, LX/F8U;-><init>(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2204308
    :cond_0
    const v0, 0x1020004

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->v:Landroid/view/View;

    .line 2204309
    const v0, 0x7f0d126a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->t:Landroid/widget/TextView;

    .line 2204310
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->t:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2204311
    const v0, 0x7f0d126b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->u:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2204312
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->u:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2204313
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->f:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    invoke-virtual {v0, v2}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->b(Z)V

    .line 2204314
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->B:LX/F8M;

    if-nez v0, :cond_4

    .line 2204315
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->h:LX/F8N;

    iget-object v2, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->D:LX/89v;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 2204316
    new-instance v6, LX/F8M;

    const-class v7, LX/F8k;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/F8k;

    invoke-static {v0}, LX/F6f;->a(LX/0QB;)LX/F6f;

    move-result-object v8

    check-cast v8, LX/F6f;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v9

    check-cast v9, LX/0W9;

    move-object v10, v2

    move-object v11, v3

    invoke-direct/range {v6 .. v11}, LX/F8M;-><init>(LX/F8k;LX/F6f;LX/0W9;LX/89v;Landroid/content/Context;)V

    .line 2204317
    move-object v0, v6

    .line 2204318
    iput-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->B:LX/F8M;

    .line 2204319
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->B:LX/F8M;

    iget-object v2, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->p:LX/1DI;

    .line 2204320
    iput-object v2, v0, LX/F8M;->j:LX/1DI;

    .line 2204321
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->e:LX/9Tk;

    iget-object v2, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->D:LX/89v;

    iget-object v2, v2, LX/89v;->value:Ljava/lang/String;

    invoke-static {p0}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->s(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)J

    move-result-wide v4

    sget-object v3, LX/9Tj;->INVITES_START_FETCHING:LX/9Tj;

    invoke-virtual {v0, v2, v4, v5, v3}, LX/9Tk;->a(Ljava/lang/String;JLX/9Tj;)V

    .line 2204322
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->D:LX/89v;

    sget-object v2, LX/89v;->FRIENDS_CENTER:LX/89v;

    if-ne v0, v2, :cond_3

    .line 2204323
    invoke-direct {p0}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->o()V

    .line 2204324
    :goto_1
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->i:LX/F8T;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->D:LX/89v;

    iget-object v2, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->B:LX/F8M;

    invoke-virtual {v0, v1, v2}, LX/F8T;->a(LX/89v;LX/1OM;)LX/F8S;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->C:LX/F8S;

    .line 2204325
    const v0, 0x7f0d1866

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2204326
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/1P0;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1P0;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2204327
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->B:LX/F8M;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2204328
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/F8Z;

    invoke-direct {v1, p0}, LX/F8Z;-><init>(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)V

    .line 2204329
    iput-object v1, v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->s:LX/1OT;

    .line 2204330
    new-instance v0, LX/0g7;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-direct {v0, v1}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->E:LX/0g7;

    .line 2204331
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->D:LX/89v;

    sget-object v1, LX/89v;->FRIENDS_CENTER:LX/89v;

    if-ne v0, v1, :cond_1

    .line 2204332
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->E:LX/0g7;

    new-instance v1, LX/F8c;

    invoke-direct {v1, p0}, LX/F8c;-><init>(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)V

    invoke-virtual {v0, v1}, LX/0g7;->a(LX/0fx;)V

    .line 2204333
    :cond_1
    return-void

    .line 2204334
    :cond_2
    const v0, 0x7f0d1863

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->w:Landroid/view/View;

    goto/16 :goto_0

    .line 2204335
    :cond_3
    invoke-direct {p0, v1}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->b(Z)V

    goto :goto_1

    .line 2204336
    :cond_4
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->D:LX/89v;

    sget-object v3, LX/89v;->FRIENDS_CENTER:LX/89v;

    if-ne v0, v3, :cond_6

    .line 2204337
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    invoke-static {p0, v0}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->a$redex0(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;Z)V

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2

    .line 2204338
    :cond_6
    invoke-direct {p0, v2}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->b(Z)V

    goto :goto_1
.end method

.method public final setUserVisibleHint(Z)V
    .locals 1

    .prologue
    .line 2204339
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 2204340
    if-eqz p1, :cond_0

    .line 2204341
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->M:Z

    .line 2204342
    :cond_0
    invoke-direct {p0}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->e()V

    .line 2204343
    return-void
.end method
