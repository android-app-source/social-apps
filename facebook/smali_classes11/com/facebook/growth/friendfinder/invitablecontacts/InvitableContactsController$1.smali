.class public final Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsController$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/F8P;

.field public final synthetic c:LX/F8S;


# direct methods
.method public constructor <init>(LX/F8S;JLX/F8P;)V
    .locals 0

    .prologue
    .line 2204085
    iput-object p1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsController$1;->c:LX/F8S;

    iput-wide p2, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsController$1;->a:J

    iput-object p4, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsController$1;->b:LX/F8P;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    .prologue
    .line 2204071
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsController$1;->c:LX/F8S;

    iget-object v0, v0, LX/F8S;->g:Ljava/util/Map;

    iget-wide v2, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsController$1;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2204072
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsController$1;->c:LX/F8S;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsController$1;->b:LX/F8P;

    .line 2204073
    iget-object v4, v1, LX/F8P;->e:LX/F8O;

    move-object v4, v4

    .line 2204074
    sget-object v5, LX/F8O;->PENDING_CAN_UNDO:LX/F8O;

    if-eq v4, v5, :cond_0

    .line 2204075
    :goto_0
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsController$1;->c:LX/F8S;

    iget-object v1, p0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsController$1;->b:LX/F8P;

    sget-object v2, LX/F8O;->PENDING_CANNOT_UNDO:LX/F8O;

    invoke-static {v0, v1, v2}, LX/F8S;->a$redex0(LX/F8S;LX/F8P;LX/F8O;)V

    .line 2204076
    return-void

    .line 2204077
    :cond_0
    iget-object v4, v0, LX/F8S;->d:LX/1Ck;

    .line 2204078
    iget-object v5, v1, LX/F8P;->c:Ljava/lang/String;

    move-object v5, v5

    .line 2204079
    iget-object v6, v0, LX/F8S;->b:LX/2dj;

    .line 2204080
    iget-object v7, v1, LX/F8P;->c:Ljava/lang/String;

    move-object v7, v7

    .line 2204081
    iget-object v8, v1, LX/F8P;->d:LX/5P4;

    move-object v8, v8

    .line 2204082
    iget-object v9, v1, LX/F8P;->b:Ljava/lang/String;

    move-object v9, v9

    .line 2204083
    iget-wide v12, v1, LX/F8P;->a:J

    move-wide v10, v12

    .line 2204084
    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v7, v8, v9, v10}, LX/2dj;->a(Ljava/lang/String;LX/5P4;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    new-instance v7, LX/F8Q;

    invoke-direct {v7, v0, v1}, LX/F8Q;-><init>(LX/F8S;LX/F8P;)V

    invoke-virtual {v4, v5, v6, v7}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method
