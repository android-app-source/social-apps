.class public final Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2ba23fc1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2203074
    const-class v0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2203073
    const-class v0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2203071
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2203072
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 2203059
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2203060
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2203061
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x6c41a5f9

    invoke-static {v2, v1, v3}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2203062
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2203063
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->m()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x23125102

    invoke-static {v4, v3, v5}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2203064
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2203065
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2203066
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2203067
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2203068
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2203069
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2203070
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2203043
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2203044
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2203045
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x6c41a5f9

    invoke-static {v2, v0, v3}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2203046
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2203047
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;

    .line 2203048
    iput v3, v0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->f:I

    move-object v1, v0

    .line 2203049
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2203050
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x23125102

    invoke-static {v2, v0, v3}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2203051
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2203052
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;

    .line 2203053
    iput v3, v0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->h:I

    move-object v1, v0

    .line 2203054
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2203055
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 2203056
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2203057
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move-object p0, v1

    .line 2203058
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2203023
    new-instance v0, LX/F83;

    invoke-direct {v0, p1}, LX/F83;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2203042
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2203038
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2203039
    const/4 v0, 0x1

    const v1, -0x6c41a5f9

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->f:I

    .line 2203040
    const/4 v0, 0x3

    const v1, -0x23125102

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->h:I

    .line 2203041
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2203075
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2203076
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2203037
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2203034
    new-instance v0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;-><init>()V

    .line 2203035
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2203036
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2203033
    const v0, 0x239c4e9d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2203032
    const v0, 0x285feb

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2203030
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->e:Ljava/lang/String;

    .line 2203031
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMutualFriends"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2203028
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2203029
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2203026
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->g:Ljava/lang/String;

    .line 2203027
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2203024
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2203025
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
