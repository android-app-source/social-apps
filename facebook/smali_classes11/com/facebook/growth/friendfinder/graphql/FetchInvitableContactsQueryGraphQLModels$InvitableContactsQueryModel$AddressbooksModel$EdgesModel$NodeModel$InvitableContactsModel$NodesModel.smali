.class public final Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3d22268d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2203659
    const-class v0, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2203660
    const-class v0, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2203661
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2203662
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2203663
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2203664
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2203665
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2203666
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2203667
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2203668
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2203669
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2203670
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2203671
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2203672
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2203673
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2203674
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2203675
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2203676
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;->e:Ljava/lang/String;

    .line 2203677
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2203678
    new-instance v0, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;-><init>()V

    .line 2203679
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2203680
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2203681
    const v0, 0x6652fbcf

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2203682
    const v0, -0x1e395f7d

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2203683
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;->f:Ljava/lang/String;

    .line 2203684
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2203685
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;->g:Ljava/lang/String;

    .line 2203686
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;->g:Ljava/lang/String;

    return-object v0
.end method
