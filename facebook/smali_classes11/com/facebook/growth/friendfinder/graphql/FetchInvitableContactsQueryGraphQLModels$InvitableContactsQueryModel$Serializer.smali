.class public final Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2203713
    const-class v0, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel;

    new-instance v1, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2203714
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2203715
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 2203716
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2203717
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2203718
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2203719
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2203720
    if-eqz v2, :cond_9

    .line 2203721
    const-string v3, "addressbooks"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2203722
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2203723
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2203724
    if-eqz v3, :cond_8

    .line 2203725
    const-string v4, "edges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2203726
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2203727
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_7

    .line 2203728
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result v5

    .line 2203729
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2203730
    const/4 p0, 0x0

    invoke-virtual {v1, v5, p0}, LX/15i;->g(II)I

    move-result p0

    .line 2203731
    if-eqz p0, :cond_6

    .line 2203732
    const-string v0, "node"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2203733
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2203734
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2203735
    if-eqz v0, :cond_5

    .line 2203736
    const-string v2, "invitable_contacts"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2203737
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2203738
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2203739
    if-eqz v2, :cond_1

    .line 2203740
    const-string v5, "nodes"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2203741
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2203742
    const/4 v5, 0x0

    :goto_1
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v5, p0, :cond_0

    .line 2203743
    invoke-virtual {v1, v2, v5}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1}, LX/F8E;->a(LX/15i;ILX/0nX;)V

    .line 2203744
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 2203745
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2203746
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2203747
    if-eqz v2, :cond_4

    .line 2203748
    const-string v5, "page_info"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2203749
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2203750
    const/4 v5, 0x0

    invoke-virtual {v1, v2, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 2203751
    if-eqz v5, :cond_2

    .line 2203752
    const-string p0, "end_cursor"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2203753
    invoke-virtual {p1, v5}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2203754
    :cond_2
    const/4 v5, 0x1

    invoke-virtual {v1, v2, v5}, LX/15i;->b(II)Z

    move-result v5

    .line 2203755
    if-eqz v5, :cond_3

    .line 2203756
    const-string p0, "has_next_page"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2203757
    invoke-virtual {p1, v5}, LX/0nX;->a(Z)V

    .line 2203758
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2203759
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2203760
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2203761
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2203762
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 2203763
    :cond_7
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2203764
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2203765
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2203766
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2203767
    check-cast p1, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$Serializer;->a(Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
