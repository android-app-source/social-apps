.class public final Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2203637
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2203638
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2203635
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2203636
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 2203591
    if-nez p1, :cond_0

    .line 2203592
    :goto_0
    return v0

    .line 2203593
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2203594
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2203595
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2203596
    const v2, -0x4321c283

    const/4 v5, 0x0

    .line 2203597
    if-nez v1, :cond_1

    move v3, v5

    .line 2203598
    :goto_1
    move v1, v3

    .line 2203599
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2203600
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2203601
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2203602
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2203603
    const v2, -0x194c3b8f

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2203604
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2203605
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2203606
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2203607
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2203608
    const v2, 0x51b0ff62

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2203609
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2203610
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2203611
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2203612
    :sswitch_3
    const-class v1, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v1

    .line 2203613
    invoke-static {p3, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 2203614
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v2

    .line 2203615
    const v3, -0x742b6b54

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 2203616
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2203617
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2203618
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 2203619
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2203620
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2203621
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2203622
    invoke-virtual {p0, p1, v4}, LX/15i;->b(II)Z

    move-result v2

    .line 2203623
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2203624
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2203625
    invoke-virtual {p3, v4, v2}, LX/186;->a(IZ)V

    .line 2203626
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2203627
    :cond_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result p1

    .line 2203628
    if-nez p1, :cond_2

    const/4 v3, 0x0

    .line 2203629
    :goto_2
    if-ge v5, p1, :cond_3

    .line 2203630
    invoke-virtual {p0, v1, v5}, LX/15i;->q(II)I

    move-result p2

    .line 2203631
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v3, v5

    .line 2203632
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 2203633
    :cond_2
    new-array v3, p1, [I

    goto :goto_2

    .line 2203634
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v3, v5}, LX/186;->a([IZ)I

    move-result v3

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x742b6b54 -> :sswitch_4
        -0x4321c283 -> :sswitch_1
        -0x221e3809 -> :sswitch_0
        -0x194c3b8f -> :sswitch_2
        0x51b0ff62 -> :sswitch_3
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2203590
    new-instance v0, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2203586
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 2203587
    if-eqz v0, :cond_0

    .line 2203588
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2203589
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2203567
    sparse-switch p2, :sswitch_data_0

    .line 2203568
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2203569
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2203570
    const v1, -0x4321c283

    .line 2203571
    if-eqz v0, :cond_0

    .line 2203572
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 2203573
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 2203574
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2203575
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2203576
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2203577
    :cond_0
    :goto_1
    :sswitch_1
    return-void

    .line 2203578
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2203579
    const v1, -0x194c3b8f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_1

    .line 2203580
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2203581
    const v1, 0x51b0ff62

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_1

    .line 2203582
    :sswitch_4
    const-class v0, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 2203583
    invoke-static {v0, p3}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 2203584
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2203585
    const v1, -0x742b6b54

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x742b6b54 -> :sswitch_1
        -0x4321c283 -> :sswitch_2
        -0x221e3809 -> :sswitch_0
        -0x194c3b8f -> :sswitch_3
        0x51b0ff62 -> :sswitch_4
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2203561
    if-eqz p1, :cond_0

    .line 2203562
    invoke-static {p0, p1, p2}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 2203563
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;

    .line 2203564
    if-eq v0, v1, :cond_0

    .line 2203565
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2203566
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2203560
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2203527
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2203528
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2203555
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2203556
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2203557
    :cond_0
    iput-object p1, p0, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2203558
    iput p2, p0, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;->b:I

    .line 2203559
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2203554
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2203553
    new-instance v0, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2203550
    iget v0, p0, LX/1vt;->c:I

    .line 2203551
    move v0, v0

    .line 2203552
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2203547
    iget v0, p0, LX/1vt;->c:I

    .line 2203548
    move v0, v0

    .line 2203549
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2203544
    iget v0, p0, LX/1vt;->b:I

    .line 2203545
    move v0, v0

    .line 2203546
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2203541
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2203542
    move-object v0, v0

    .line 2203543
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2203532
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2203533
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2203534
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2203535
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2203536
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2203537
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2203538
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2203539
    invoke-static {v3, v9, v2}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2203540
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2203529
    iget v0, p0, LX/1vt;->c:I

    .line 2203530
    move v0, v0

    .line 2203531
    return v0
.end method
