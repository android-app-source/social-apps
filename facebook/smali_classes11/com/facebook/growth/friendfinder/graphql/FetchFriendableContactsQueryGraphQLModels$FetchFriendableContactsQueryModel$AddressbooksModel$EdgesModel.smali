.class public final Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4ccedc4d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2203177
    const-class v0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2203178
    const-class v0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2203162
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2203163
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2203171
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2203172
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel;->a()Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2203173
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2203174
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2203175
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2203176
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2203179
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2203180
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel;->a()Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2203181
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel;->a()Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;

    .line 2203182
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel;->a()Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2203183
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel;

    .line 2203184
    iput-object v0, v1, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel;->e:Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;

    .line 2203185
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2203186
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2203169
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel;->e:Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel;->e:Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;

    .line 2203170
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel;->e:Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2203166
    new-instance v0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel;-><init>()V

    .line 2203167
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2203168
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2203165
    const v0, 0x56c4ce7d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2203164
    const v0, 0x48b75e56

    return v0
.end method
