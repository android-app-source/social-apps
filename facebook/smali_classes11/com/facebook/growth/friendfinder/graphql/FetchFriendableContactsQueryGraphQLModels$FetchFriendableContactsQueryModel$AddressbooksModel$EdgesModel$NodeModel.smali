.class public final Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3ca399d2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2203154
    const-class v0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2203153
    const-class v0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2203151
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2203152
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2203145
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2203146
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;->a()Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2203147
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2203148
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2203149
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2203150
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2203130
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2203131
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;->a()Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2203132
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;->a()Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel;

    .line 2203133
    invoke-virtual {p0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;->a()Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2203134
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;

    .line 2203135
    iput-object v0, v1, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;->e:Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel;

    .line 2203136
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2203137
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFriendableContacts"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2203143
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;->e:Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel;

    iput-object v0, p0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;->e:Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel;

    .line 2203144
    iget-object v0, p0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;->e:Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2203140
    new-instance v0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel;-><init>()V

    .line 2203141
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2203142
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2203139
    const v0, -0x75bc1f54

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2203138
    const v0, 0x1149d8dd

    return v0
.end method
