.class public final Lcom/facebook/growth/contactimporter/FindFriendsAnalyticsEvent;
.super Lcom/facebook/analytics/logger/HoneyClientEvent;
.source ""


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2200719
    const-string v0, "flow"

    invoke-direct {p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2200720
    const-string v0, "flow_name"

    const-string v1, "contact_importer"

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2200721
    const-string v0, "step_name"

    invoke-virtual {p0, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2200722
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/facebook/growth/contactimporter/FindFriendsAnalyticsEvent;
    .locals 1

    .prologue
    .line 2200723
    const-string v0, "invite_candidates_count"

    invoke-virtual {p0, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2200724
    return-object p0
.end method

.method public final m(Ljava/lang/String;)Lcom/facebook/growth/contactimporter/FindFriendsAnalyticsEvent;
    .locals 1

    .prologue
    .line 2200725
    const-string v0, "step_phase"

    invoke-virtual {p0, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2200726
    return-object p0
.end method
