.class public final Lcom/facebook/growth/contactimporter/BaseInvitesAdapter$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/F6e;


# direct methods
.method public constructor <init>(LX/F6e;J)V
    .locals 0

    .prologue
    .line 2200580
    iput-object p1, p0, Lcom/facebook/growth/contactimporter/BaseInvitesAdapter$2;->b:LX/F6e;

    iput-wide p2, p0, Lcom/facebook/growth/contactimporter/BaseInvitesAdapter$2;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 2200571
    iget-object v0, p0, Lcom/facebook/growth/contactimporter/BaseInvitesAdapter$2;->b:LX/F6e;

    iget-object v0, v0, LX/F6e;->n:Ljava/util/Hashtable;

    iget-wide v2, p0, Lcom/facebook/growth/contactimporter/BaseInvitesAdapter$2;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2200572
    iget-object v0, p0, Lcom/facebook/growth/contactimporter/BaseInvitesAdapter$2;->b:LX/F6e;

    iget-object v0, v0, LX/F6e;->n:Ljava/util/Hashtable;

    iget-wide v2, p0, Lcom/facebook/growth/contactimporter/BaseInvitesAdapter$2;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2200573
    :cond_0
    iget-object v0, p0, Lcom/facebook/growth/contactimporter/BaseInvitesAdapter$2;->b:LX/F6e;

    const v2, -0x26771cdd

    invoke-static {v0, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2200574
    iget-object v0, p0, Lcom/facebook/growth/contactimporter/BaseInvitesAdapter$2;->b:LX/F6e;

    iget-object v0, v0, LX/F6e;->m:Ljava/util/Set;

    iget-wide v2, p0, Lcom/facebook/growth/contactimporter/BaseInvitesAdapter$2;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2200575
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2200576
    iget-object v0, p0, Lcom/facebook/growth/contactimporter/BaseInvitesAdapter$2;->b:LX/F6e;

    iget-object v0, v0, LX/F6e;->j:Ljava/util/Map;

    iget-wide v4, p0, Lcom/facebook/growth/contactimporter/BaseInvitesAdapter$2;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;

    invoke-virtual {v0}, Lcom/facebook/ipc/model/FacebookPhonebookContact;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2200577
    iget-object v0, p0, Lcom/facebook/growth/contactimporter/BaseInvitesAdapter$2;->b:LX/F6e;

    iget-object v0, v0, LX/F6e;->g:LX/F6i;

    const/4 v3, 0x0

    sget-object v4, Lcom/facebook/growth/contactimporter/StepInviteActivity;->p:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v3, v4}, LX/F6i;->a(Ljava/util/List;ZLcom/facebook/common/callercontext/CallerContext;)V

    .line 2200578
    iget-object v0, p0, Lcom/facebook/growth/contactimporter/BaseInvitesAdapter$2;->b:LX/F6e;

    iget-object v0, v0, LX/F6e;->h:LX/F6k;

    iget-object v2, p0, Lcom/facebook/growth/contactimporter/BaseInvitesAdapter$2;->b:LX/F6e;

    iget-object v2, v2, LX/F6e;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const-string v3, "invite_button"

    iget-object v4, p0, Lcom/facebook/growth/contactimporter/BaseInvitesAdapter$2;->b:LX/F6e;

    iget-wide v4, v4, LX/F6e;->k:J

    invoke-virtual/range {v0 .. v5}, LX/F6k;->a(IILjava/lang/String;J)V

    .line 2200579
    :cond_1
    return-void
.end method
