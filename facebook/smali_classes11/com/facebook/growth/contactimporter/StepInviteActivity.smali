.class public Lcom/facebook/growth/contactimporter/StepInviteActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/10X;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final p:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:J

.field public q:LX/9Tk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/F6j;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/F6k;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/F6h;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private v:Z

.field private w:LX/89v;

.field private x:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/facebook/ipc/model/FacebookPhonebookContact;",
            ">;"
        }
    .end annotation
.end field

.field private y:LX/F6g;

.field private z:LX/F6i;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2200824
    const-class v0, Lcom/facebook/growth/contactimporter/StepInviteActivity;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2200893
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2200894
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->v:Z

    return-void
.end method

.method private a()Landroid/text/Spanned;
    .locals 2

    .prologue
    .line 2200895
    new-instance v0, Landroid/text/SpannableString;

    const v1, 0x7f08339d

    invoke-virtual {p0, v1}, Lcom/facebook/growth/contactimporter/StepInviteActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method private a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2200897
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2200898
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 2200899
    iget-object v4, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->x:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;

    .line 2200900
    invoke-virtual {v0}, Lcom/facebook/ipc/model/FacebookPhonebookContact;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2200901
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2200902
    :cond_0
    return-object v2
.end method

.method private static a(Lcom/facebook/growth/contactimporter/StepInviteActivity;LX/9Tk;LX/0Uh;LX/F6j;LX/F6k;LX/F6h;)V
    .locals 0

    .prologue
    .line 2200896
    iput-object p1, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->q:LX/9Tk;

    iput-object p2, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->r:LX/0Uh;

    iput-object p3, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->s:LX/F6j;

    iput-object p4, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->t:LX/F6k;

    iput-object p5, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->u:LX/F6h;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/growth/contactimporter/StepInviteActivity;

    invoke-static {v5}, LX/9Tk;->a(LX/0QB;)LX/9Tk;

    move-result-object v1

    check-cast v1, LX/9Tk;

    invoke-static {v5}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    const-class v3, LX/F6j;

    invoke-interface {v5, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/F6j;

    new-instance v6, LX/F6k;

    invoke-static {v5}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-direct {v6, v4}, LX/F6k;-><init>(LX/0Zb;)V

    move-object v4, v6

    check-cast v4, LX/F6k;

    const-class v6, LX/F6h;

    invoke-interface {v5, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/F6h;

    invoke-static/range {v0 .. v5}, Lcom/facebook/growth/contactimporter/StepInviteActivity;->a(Lcom/facebook/growth/contactimporter/StepInviteActivity;LX/9Tk;LX/0Uh;LX/F6j;LX/F6k;LX/F6h;)V

    return-void
.end method

.method public static b(Lcom/facebook/growth/contactimporter/StepInviteActivity;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2200882
    iget-boolean v0, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->v:Z

    if-eqz v0, :cond_0

    .line 2200883
    :goto_0
    return-void

    .line 2200884
    :cond_0
    iget-object v0, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->y:LX/F6g;

    .line 2200885
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, LX/F6e;->n:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v1, v1

    .line 2200886
    iget-object v0, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->z:LX/F6i;

    invoke-direct {p0, v1}, Lcom/facebook/growth/contactimporter/StepInviteActivity;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v4, Lcom/facebook/growth/contactimporter/StepInviteActivity;->p:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v3, v4}, LX/F6i;->a(Ljava/util/List;ZLcom/facebook/common/callercontext/CallerContext;)V

    .line 2200887
    iget-object v0, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->y:LX/F6g;

    .line 2200888
    iget-object v2, v0, LX/F6e;->m:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 2200889
    iget-object v0, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->t:LX/F6k;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->x:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    iget-wide v4, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->A:J

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, LX/F6k;->a(IILjava/lang/String;J)V

    .line 2200890
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->v:Z

    goto :goto_0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 2200891
    const v0, 0x7f0d11a4

    invoke-virtual {p0, v0}, Lcom/facebook/growth/contactimporter/StepInviteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/F6m;

    invoke-direct {v1, p0}, LX/F6m;-><init>(Lcom/facebook/growth/contactimporter/StepInviteActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2200892
    return-void
.end method

.method public static m(Lcom/facebook/growth/contactimporter/StepInviteActivity;)V
    .locals 3

    .prologue
    .line 2200878
    new-instance v0, LX/F6n;

    invoke-direct {v0, p0}, LX/F6n;-><init>(Lcom/facebook/growth/contactimporter/StepInviteActivity;)V

    .line 2200879
    new-instance v1, LX/0ju;

    invoke-direct {v1, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v2, 0x1080027

    invoke-virtual {v1, v2}, LX/0ju;->c(I)LX/0ju;

    move-result-object v1

    const v2, 0x7f0833a2

    invoke-virtual {p0, v2}, Lcom/facebook/growth/contactimporter/StepInviteActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080020

    invoke-virtual {v1, v2, v0}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080021

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2200880
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 2200881
    return-void
.end method

.method public static n(Lcom/facebook/growth/contactimporter/StepInviteActivity;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 2200871
    iget-boolean v0, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->v:Z

    if-eqz v0, :cond_0

    .line 2200872
    :goto_0
    return-void

    .line 2200873
    :cond_0
    iget-object v0, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->z:LX/F6i;

    invoke-direct {p0}, Lcom/facebook/growth/contactimporter/StepInviteActivity;->o()Ljava/util/ArrayList;

    move-result-object v1

    sget-object v2, Lcom/facebook/growth/contactimporter/StepInviteActivity;->p:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v6, v2}, LX/F6i;->a(Ljava/util/List;ZLcom/facebook/common/callercontext/CallerContext;)V

    .line 2200874
    iget-object v0, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->x:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    .line 2200875
    iget-object v0, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->t:LX/F6k;

    const-string v3, "invite_all"

    iget-wide v4, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->A:J

    move v2, v1

    invoke-virtual/range {v0 .. v5}, LX/F6k;->a(IILjava/lang/String;J)V

    .line 2200876
    iget-object v0, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->q:LX/9Tk;

    iget-object v2, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->w:LX/89v;

    iget-object v2, v2, LX/89v;->value:Ljava/lang/String;

    sget-object v3, LX/9Ti;->FRIEND_FINDER_API:LX/9Ti;

    invoke-virtual {v0, v2, v1, v3}, LX/9Tk;->a(Ljava/lang/String;ILX/9Ti;)V

    .line 2200877
    iput-boolean v6, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->v:Z

    goto :goto_0
.end method

.method private o()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2200867
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2200868
    iget-object v0, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->x:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;

    .line 2200869
    invoke-virtual {v0}, Lcom/facebook/ipc/model/FacebookPhonebookContact;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2200870
    :cond_0
    return-object v1
.end method

.method public static p(Lcom/facebook/growth/contactimporter/StepInviteActivity;)V
    .locals 4

    .prologue
    .line 2200862
    iget-object v0, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->t:LX/F6k;

    .line 2200863
    iget-object v1, v0, LX/F6k;->a:LX/0Zb;

    new-instance v2, Lcom/facebook/growth/contactimporter/FindFriendsAnalyticsEvent;

    const-string v3, "end"

    invoke-direct {v2, v3}, Lcom/facebook/growth/contactimporter/FindFriendsAnalyticsEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2200864
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/facebook/growth/contactimporter/StepInviteActivity;->setResult(I)V

    .line 2200865
    invoke-virtual {p0}, Lcom/facebook/growth/contactimporter/StepInviteActivity;->finish()V

    .line 2200866
    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2200861
    const v0, 0x7f0833a3

    invoke-virtual {p0, v0}, Lcom/facebook/growth/contactimporter/StepInviteActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v1, 0x0

    .line 2200828
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2200829
    invoke-static {p0, p0}, Lcom/facebook/growth/contactimporter/StepInviteActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2200830
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->A:J

    .line 2200831
    invoke-virtual {p0}, Lcom/facebook/growth/contactimporter/StepInviteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "invitee_credentials"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookPhonebookContactMap;

    .line 2200832
    iget-object v2, v0, Lcom/facebook/ipc/model/FacebookPhonebookContactMap;->a:Ljava/util/Map;

    move-object v0, v2

    .line 2200833
    iput-object v0, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->x:Ljava/util/Map;

    .line 2200834
    invoke-virtual {p0}, Lcom/facebook/growth/contactimporter/StepInviteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "ci_flow"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/89v;

    iput-object v0, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->w:LX/89v;

    .line 2200835
    iget-object v0, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->s:LX/F6j;

    iget-object v2, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->w:LX/89v;

    invoke-virtual {v0, v2}, LX/F6j;->a(LX/89v;)LX/F6i;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->z:LX/F6i;

    .line 2200836
    iget-object v0, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->t:LX/F6k;

    iget-object v2, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->x:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    .line 2200837
    iget-object v3, v0, LX/F6k;->a:LX/0Zb;

    new-instance v4, Lcom/facebook/growth/contactimporter/FindFriendsAnalyticsEvent;

    const-string v5, "invite"

    invoke-direct {v4, v5}, Lcom/facebook/growth/contactimporter/FindFriendsAnalyticsEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "opened"

    invoke-virtual {v4, v5}, Lcom/facebook/growth/contactimporter/FindFriendsAnalyticsEvent;->m(Ljava/lang/String;)Lcom/facebook/growth/contactimporter/FindFriendsAnalyticsEvent;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/facebook/growth/contactimporter/FindFriendsAnalyticsEvent;->a(I)Lcom/facebook/growth/contactimporter/FindFriendsAnalyticsEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2200838
    iget-object v0, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->x:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 2200839
    iget-object v0, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->t:LX/F6k;

    const-string v3, "no_contacts"

    iget-wide v4, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->A:J

    move v2, v1

    invoke-virtual/range {v0 .. v5}, LX/F6k;->a(IILjava/lang/String;J)V

    .line 2200840
    invoke-static {p0}, Lcom/facebook/growth/contactimporter/StepInviteActivity;->p(Lcom/facebook/growth/contactimporter/StepInviteActivity;)V

    .line 2200841
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/facebook/growth/contactimporter/StepInviteActivity;->setResult(I)V

    .line 2200842
    invoke-virtual {p0}, Lcom/facebook/growth/contactimporter/StepInviteActivity;->finish()V

    .line 2200843
    :goto_0
    return-void

    .line 2200844
    :cond_0
    const v0, 0x7f03066b

    invoke-virtual {p0, v0}, Lcom/facebook/growth/contactimporter/StepInviteActivity;->setContentView(I)V

    .line 2200845
    iget-object v0, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->r:LX/0Uh;

    const/16 v2, 0x30

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2200846
    const v0, 0x7f0d11a3

    invoke-virtual {p0, v0}, Lcom/facebook/growth/contactimporter/StepInviteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2200847
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2200848
    :cond_1
    iget-object v3, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->u:LX/F6h;

    iget-object v5, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->w:LX/89v;

    iget-object v6, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->x:Ljava/util/Map;

    invoke-direct {p0}, Lcom/facebook/growth/contactimporter/StepInviteActivity;->a()Landroid/text/Spanned;

    move-result-object v7

    iget-wide v8, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->A:J

    iget-object v10, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->z:LX/F6i;

    iget-object v11, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->t:LX/F6k;

    move-object v4, p0

    invoke-virtual/range {v3 .. v11}, LX/F6h;->a(Landroid/content/Context;LX/89v;Ljava/util/Map;Landroid/text/Spanned;JLX/F6i;LX/F6k;)LX/F6g;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->y:LX/F6g;

    .line 2200849
    const v0, 0x7f0d11a2

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    .line 2200850
    iget-object v2, p0, Lcom/facebook/growth/contactimporter/StepInviteActivity;->y:LX/F6g;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2200851
    invoke-virtual {v0, v12}, Lcom/facebook/widget/listview/BetterListView;->setFastScrollAlwaysVisible(Z)V

    .line 2200852
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2200853
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2200854
    const v2, 0x7f080031

    invoke-virtual {p0, v2}, Lcom/facebook/growth/contactimporter/StepInviteActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2200855
    new-array v3, v12, [Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v4

    .line 2200856
    iput-object v2, v4, LX/108;->g:Ljava/lang/String;

    .line 2200857
    move-object v2, v4

    .line 2200858
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2200859
    new-instance v1, LX/F6l;

    invoke-direct {v1, p0}, LX/F6l;-><init>(Lcom/facebook/growth/contactimporter/StepInviteActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2200860
    invoke-direct {p0}, Lcom/facebook/growth/contactimporter/StepInviteActivity;->l()V

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 2200825
    const-string v0, "back_button"

    invoke-static {p0, v0}, Lcom/facebook/growth/contactimporter/StepInviteActivity;->b(Lcom/facebook/growth/contactimporter/StepInviteActivity;Ljava/lang/String;)V

    .line 2200826
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2200827
    return-void
.end method
