.class public final Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2391750
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2391751
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2391718
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2391719
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 2391720
    if-nez p1, :cond_0

    .line 2391721
    :goto_0
    return v0

    .line 2391722
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2391723
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2391724
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2391725
    const v2, 0x23f217e7

    const/4 v6, 0x0

    .line 2391726
    if-nez v1, :cond_1

    move v5, v6

    .line 2391727
    :goto_1
    move v1, v5

    .line 2391728
    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2391729
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2391730
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2391731
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2391732
    invoke-virtual {p3, v3, v2}, LX/186;->b(II)V

    .line 2391733
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2391734
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2391735
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2391736
    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2391737
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2391738
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2391739
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2391740
    invoke-virtual {p3, v3, v2}, LX/186;->b(II)V

    .line 2391741
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2391742
    :cond_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v7

    .line 2391743
    if-nez v7, :cond_2

    const/4 v5, 0x0

    .line 2391744
    :goto_2
    if-ge v6, v7, :cond_3

    .line 2391745
    invoke-virtual {p0, v1, v6}, LX/15i;->q(II)I

    move-result p2

    .line 2391746
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v5, v6

    .line 2391747
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 2391748
    :cond_2
    new-array v5, v7, [I

    goto :goto_2

    .line 2391749
    :cond_3
    const/4 v6, 0x1

    invoke-virtual {p3, v5, v6}, LX/186;->a([IZ)I

    move-result v5

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x7367b57b -> :sswitch_0
        0x23f217e7 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2391684
    new-instance v0, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 2391759
    sparse-switch p2, :sswitch_data_0

    .line 2391760
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2391761
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2391762
    const v1, 0x23f217e7

    .line 2391763
    if-eqz v0, :cond_0

    .line 2391764
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 2391765
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 2391766
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2391767
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2391768
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2391769
    :cond_0
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7367b57b -> :sswitch_0
        0x23f217e7 -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2391752
    if-eqz p1, :cond_0

    .line 2391753
    invoke-static {p0, p1, p2}, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$DraculaImplementation;

    move-result-object v1

    .line 2391754
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$DraculaImplementation;

    .line 2391755
    if-eq v0, v1, :cond_0

    .line 2391756
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2391757
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2391758
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2391711
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2391712
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2391713
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2391714
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2391715
    :cond_0
    iput-object p1, p0, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$DraculaImplementation;->a:LX/15i;

    .line 2391716
    iput p2, p0, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$DraculaImplementation;->b:I

    .line 2391717
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2391710
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2391709
    new-instance v0, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2391706
    iget v0, p0, LX/1vt;->c:I

    .line 2391707
    move v0, v0

    .line 2391708
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2391703
    iget v0, p0, LX/1vt;->c:I

    .line 2391704
    move v0, v0

    .line 2391705
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2391700
    iget v0, p0, LX/1vt;->b:I

    .line 2391701
    move v0, v0

    .line 2391702
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2391697
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2391698
    move-object v0, v0

    .line 2391699
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2391688
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2391689
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2391690
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2391691
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2391692
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2391693
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2391694
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2391695
    invoke-static {v3, v9, v2}, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2391696
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2391685
    iget v0, p0, LX/1vt;->c:I

    .line 2391686
    move v0, v0

    .line 2391687
    return v0
.end method
