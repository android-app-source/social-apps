.class public final Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$UploadContactsQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2391770
    const-class v0, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$UploadContactsQueryModel;

    new-instance v1, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$UploadContactsQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$UploadContactsQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2391771
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2391772
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2391773
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2391774
    const/4 v2, 0x0

    .line 2391775
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 2391776
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2391777
    :goto_0
    move v1, v2

    .line 2391778
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2391779
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2391780
    new-instance v1, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$UploadContactsQueryModel;

    invoke-direct {v1}, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$UploadContactsQueryModel;-><init>()V

    .line 2391781
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2391782
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2391783
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2391784
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2391785
    :cond_0
    return-object v1

    .line 2391786
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2391787
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2391788
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2391789
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2391790
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 2391791
    const-string v4, "free_fb_invitable_contact_list"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2391792
    const/4 v3, 0x0

    .line 2391793
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_a

    .line 2391794
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2391795
    :goto_2
    move v1, v3

    .line 2391796
    goto :goto_1

    .line 2391797
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2391798
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2391799
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 2391800
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2391801
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_9

    .line 2391802
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2391803
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2391804
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_6

    if-eqz v5, :cond_6

    .line 2391805
    const-string v6, "invitable_contacts"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2391806
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2391807
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_7

    .line 2391808
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_7

    .line 2391809
    const/4 v6, 0x0

    .line 2391810
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_f

    .line 2391811
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2391812
    :goto_5
    move v5, v6

    .line 2391813
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2391814
    :cond_7
    invoke-static {v4, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 2391815
    goto :goto_3

    .line 2391816
    :cond_8
    const-string v6, "meta_data"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2391817
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_3

    .line 2391818
    :cond_9
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 2391819
    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 2391820
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 2391821
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto/16 :goto_2

    :cond_a
    move v1, v3

    move v4, v3

    goto :goto_3

    .line 2391822
    :cond_b
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2391823
    :cond_c
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, p0, :cond_e

    .line 2391824
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2391825
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2391826
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_c

    if-eqz v8, :cond_c

    .line 2391827
    const-string p0, "contact_point"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_d

    .line 2391828
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_6

    .line 2391829
    :cond_d
    const-string p0, "name"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 2391830
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_6

    .line 2391831
    :cond_e
    const/4 v8, 0x2

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 2391832
    invoke-virtual {v0, v6, v7}, LX/186;->b(II)V

    .line 2391833
    const/4 v6, 0x1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 2391834
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto/16 :goto_5

    :cond_f
    move v5, v6

    move v7, v6

    goto :goto_6
.end method
