.class public final Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$UploadContactsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$UploadContactsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2391835
    const-class v0, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$UploadContactsQueryModel;

    new-instance v1, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$UploadContactsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$UploadContactsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2391836
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2391837
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$UploadContactsQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 2391838
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2391839
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2391840
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2391841
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2391842
    if-eqz v2, :cond_5

    .line 2391843
    const-string v3, "free_fb_invitable_contact_list"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2391844
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2391845
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2391846
    if-eqz v3, :cond_3

    .line 2391847
    const-string v4, "invitable_contacts"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2391848
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2391849
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result p0

    if-ge v4, p0, :cond_2

    .line 2391850
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result p0

    .line 2391851
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2391852
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2391853
    if-eqz v0, :cond_0

    .line 2391854
    const-string p2, "contact_point"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2391855
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2391856
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2391857
    if-eqz v0, :cond_1

    .line 2391858
    const-string p2, "name"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2391859
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2391860
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2391861
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2391862
    :cond_2
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2391863
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2391864
    if-eqz v3, :cond_4

    .line 2391865
    const-string v4, "meta_data"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2391866
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2391867
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2391868
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2391869
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2391870
    check-cast p1, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$UploadContactsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$UploadContactsQueryModel$Serializer;->a(Lcom/facebook/growth/contactinviter/graphql/ContactInviterModels$UploadContactsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
