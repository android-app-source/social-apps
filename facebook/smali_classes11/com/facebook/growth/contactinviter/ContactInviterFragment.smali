.class public Lcom/facebook/growth/contactinviter/ContactInviterFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/Glv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/GmB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/GmH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Gm8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Z

.field public i:I

.field private j:LX/Gm2;

.field private k:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public l:Landroid/widget/LinearLayout;

.field private m:Lcom/facebook/resources/ui/FbButton;

.field public n:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/GmD;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2391544
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2391545
    return-void
.end method

.method public static a(Lcom/facebook/growth/contactinviter/ContactInviterFragment;LX/GmG;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2391535
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "contact_inviter_action_click"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2391536
    const-string v1, "action_type"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2391537
    const-string v1, "phone_number"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2391538
    iget-object v1, p0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->e:LX/GmH;

    .line 2391539
    const-string p0, "tag"

    sget-object p1, LX/GmH;->a:Ljava/lang/String;

    invoke-virtual {v0, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2391540
    const-string p0, "CONTACT_INVITER"

    .line 2391541
    iput-object p0, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2391542
    iget-object p0, v1, LX/GmH;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/0Zb;

    invoke-interface {p0, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2391543
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/growth/contactinviter/ContactInviterFragment;

    new-instance v4, LX/Glv;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v2

    check-cast v2, LX/0W9;

    invoke-static {p0}, LX/Gm9;->a(LX/0QB;)LX/Gm9;

    move-result-object v3

    check-cast v3, LX/Gm9;

    invoke-direct {v4, v2, v3}, LX/Glv;-><init>(LX/0W9;LX/Gm9;)V

    move-object v2, v4

    check-cast v2, LX/Glv;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    new-instance v6, LX/GmB;

    invoke-static {p0}, LX/F9r;->b(LX/0QB;)LX/F9r;

    move-result-object v5

    check-cast v5, LX/F9r;

    invoke-direct {v6, v5}, LX/GmB;-><init>(LX/F9r;)V

    move-object v5, v6

    check-cast v5, LX/GmB;

    new-instance v6, LX/GmH;

    const/16 v7, 0xbc

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-direct {v6, v7}, LX/GmH;-><init>(LX/0Ot;)V

    move-object v6, v6

    check-cast v6, LX/GmH;

    new-instance v0, LX/Gm8;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    invoke-static {p0}, LX/Gm9;->a(LX/0QB;)LX/Gm9;

    move-result-object p1

    check-cast p1, LX/Gm9;

    invoke-direct {v0, v7, p1}, LX/Gm8;-><init>(LX/0tX;LX/Gm9;)V

    move-object v7, v0

    check-cast v7, LX/Gm8;

    invoke-static {p0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object p0

    check-cast p0, LX/0if;

    iput-object v2, v1, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->a:LX/Glv;

    iput-object v3, v1, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object v4, v1, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->c:LX/1Ck;

    iput-object v5, v1, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->d:LX/GmB;

    iput-object v6, v1, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->e:LX/GmH;

    iput-object v7, v1, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->f:LX/Gm8;

    iput-object p0, v1, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->g:LX/0if;

    return-void
.end method

.method public static k(Lcom/facebook/growth/contactinviter/ContactInviterFragment;)V
    .locals 2

    .prologue
    .line 2391532
    iget-object v0, p0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->j:LX/Gm2;

    if-eqz v0, :cond_0

    .line 2391533
    iget-object v0, p0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->j:LX/Gm2;

    iget-object v1, p0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->o:Ljava/util/List;

    invoke-interface {v0, v1}, LX/Gm2;->a(Ljava/util/List;)V

    .line 2391534
    :cond_0
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 3

    .prologue
    .line 2391526
    iget-object v0, p0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->l:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2391527
    iget-object v0, p0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->n:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2391528
    iget-object v0, p0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->d:LX/GmB;

    new-instance v1, LX/Glx;

    invoke-direct {v1, p0}, LX/Glx;-><init>(Lcom/facebook/growth/contactinviter/ContactInviterFragment;)V

    const/4 p0, 0x0

    .line 2391529
    iput-object v1, v0, LX/GmB;->b:LX/Glx;

    .line 2391530
    new-instance v2, LX/GmA;

    invoke-direct {v2, v0}, LX/GmA;-><init>(LX/GmB;)V

    new-array p0, p0, [Ljava/lang/Void;

    invoke-virtual {v2, p0}, LX/GmA;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 2391531
    return-void
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2391523
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/content/Context;)V

    .line 2391524
    check-cast p1, LX/Gm2;

    iput-object p1, p0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->j:LX/Gm2;

    .line 2391525
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/16 v0, 0x2a

    const v1, 0xb6b4e03

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2391483
    const-class v1, Lcom/facebook/growth/contactinviter/ContactInviterFragment;

    invoke-static {v1, p0}, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2391484
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2391485
    if-eqz v1, :cond_0

    .line 2391486
    const-string v2, "has_title_bar"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->h:Z

    .line 2391487
    const-string v2, "title_bar_title_string_id"

    const v3, 0x7f08339f

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->i:I

    .line 2391488
    const-string v2, "analytics_tag"

    const-string v3, "unknown"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2391489
    sput-object v1, LX/GmH;->a:Ljava/lang/String;

    .line 2391490
    :cond_0
    const v1, 0x7f030364

    invoke-virtual {p1, v1, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x5cded2c5

    invoke-static {v5, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5baffee3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2391491
    iget-object v1, p0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->c:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2391492
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2391493
    const/16 v1, 0x2b

    const v2, -0x101740aa

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetach()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x517bb080

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2391494
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDetach()V

    .line 2391495
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->j:LX/Gm2;

    .line 2391496
    const/16 v1, 0x2b

    const v2, 0x67d79056

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2391497
    invoke-super {p0, p2}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2391498
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2391499
    iget-boolean v1, p0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->h:Z

    if-nez v1, :cond_0

    .line 2391500
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setVisibility(I)V

    .line 2391501
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->o:Ljava/util/List;

    .line 2391502
    const v0, 0x7f0d0b21

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->k:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2391503
    const v0, 0x7f0d05b0

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->n:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2391504
    const v0, 0x7f0d0b22

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->l:Landroid/widget/LinearLayout;

    .line 2391505
    const v0, 0x7f0d0b23

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->m:Lcom/facebook/resources/ui/FbButton;

    .line 2391506
    iget-object v0, p0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->k:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/1P0;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1P0;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2391507
    iget-object v0, p0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->a:LX/Glv;

    .line 2391508
    iput-object p0, v0, LX/Glv;->a:Lcom/facebook/growth/contactinviter/ContactInviterFragment;

    .line 2391509
    iget-object v0, p0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->k:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->a:LX/Glv;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2391510
    invoke-virtual {p0}, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->b()V

    .line 2391511
    iget-object v0, p0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->m:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/Glw;

    invoke-direct {v1, p0}, LX/Glw;-><init>(Lcom/facebook/growth/contactinviter/ContactInviterFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2391512
    return-void

    .line 2391513
    :cond_0
    iget v1, p0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->i:I

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 2391514
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setHasBackButton(Z)V

    .line 2391515
    new-instance v1, LX/Gm0;

    invoke-direct {v1, p0}, LX/Gm0;-><init>(Lcom/facebook/growth/contactinviter/ContactInviterFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 2391516
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2391517
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p2, 0x7f08001e

    invoke-virtual {v3, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2391518
    iput-object v3, v2, LX/108;->g:Ljava/lang/String;

    .line 2391519
    move-object v2, v2

    .line 2391520
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2391521
    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setButtonSpecs(Ljava/util/List;)V

    .line 2391522
    new-instance v1, LX/Gm1;

    invoke-direct {v1, p0}, LX/Gm1;-><init>(Lcom/facebook/growth/contactinviter/ContactInviterFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setActionButtonOnClickListener(LX/107;)V

    goto/16 :goto_0
.end method
