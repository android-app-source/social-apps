.class public Lcom/facebook/growth/nux/CILegalNuxActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/1ZF;


# instance fields
.field public p:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:LX/0h5;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2204762
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2204763
    invoke-virtual {p0}, Lcom/facebook/growth/nux/CILegalNuxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083393

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2204764
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    .line 2204765
    iput-object v0, v1, LX/108;->g:Ljava/lang/String;

    .line 2204766
    move-object v1, v1

    .line 2204767
    iput-object v0, v1, LX/108;->j:Ljava/lang/String;

    .line 2204768
    move-object v0, v1

    .line 2204769
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 2204770
    invoke-virtual {p0, v0}, Lcom/facebook/growth/nux/CILegalNuxActivity;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2204771
    invoke-direct {p0}, Lcom/facebook/growth/nux/CILegalNuxActivity;->b()LX/63W;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/growth/nux/CILegalNuxActivity;->a(LX/63W;)V

    .line 2204772
    return-void
.end method

.method private static a(Lcom/facebook/growth/nux/CILegalNuxActivity;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/growth/nux/CILegalNuxActivity;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2204773
    iput-object p1, p0, Lcom/facebook/growth/nux/CILegalNuxActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p2, p0, Lcom/facebook/growth/nux/CILegalNuxActivity;->q:LX/0Or;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/growth/nux/CILegalNuxActivity;

    invoke-static {v1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v2, 0x15e7

    invoke-static {v1, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/growth/nux/CILegalNuxActivity;->a(Lcom/facebook/growth/nux/CILegalNuxActivity;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V

    return-void
.end method

.method private b()LX/63W;
    .locals 1

    .prologue
    .line 2204774
    new-instance v0, LX/F8r;

    invoke-direct {v0, p0}, LX/F8r;-><init>(Lcom/facebook/growth/nux/CILegalNuxActivity;)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/63W;)V
    .locals 1

    .prologue
    .line 2204775
    iget-object v0, p0, Lcom/facebook/growth/nux/CILegalNuxActivity;->r:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2204776
    return-void
.end method

.method public final a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 2

    .prologue
    .line 2204757
    iget-object v1, p0, Lcom/facebook/growth/nux/CILegalNuxActivity;->r:LX/0h5;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1, v0}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2204758
    return-void

    .line 2204759
    :cond_0
    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final a_(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2204760
    iget-object v0, p0, Lcom/facebook/growth/nux/CILegalNuxActivity;->r:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2204761
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2204747
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2204748
    invoke-static {p0, p0}, Lcom/facebook/growth/nux/CILegalNuxActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2204749
    const v0, 0x7f0306cf

    invoke-virtual {p0, v0}, Lcom/facebook/growth/nux/CILegalNuxActivity;->setContentView(I)V

    .line 2204750
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2204751
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/growth/nux/CILegalNuxActivity;->r:LX/0h5;

    .line 2204752
    const v0, 0x7f08338f

    invoke-virtual {p0, v0}, Lcom/facebook/growth/nux/CILegalNuxActivity;->x_(I)V

    .line 2204753
    invoke-direct {p0}, Lcom/facebook/growth/nux/CILegalNuxActivity;->a()V

    .line 2204754
    sget-object v0, LX/89v;->CCU_INTERSTITIAL_NUX:LX/89v;

    sget-object v1, LX/89v;->CCU_INTERSTITIAL_NUX:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->a(LX/89v;Ljava/lang/String;)Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    move-result-object v0

    .line 2204755
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d1263

    invoke-virtual {v1, v2, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2204756
    return-void
.end method

.method public final b(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 0

    .prologue
    .line 2204746
    return-void
.end method

.method public final f()Landroid/view/View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2204745
    const/4 v0, 0x0

    return-object v0
.end method

.method public final k_(Z)V
    .locals 0

    .prologue
    .line 2204744
    return-void
.end method

.method public final lH_()V
    .locals 0

    .prologue
    .line 2204743
    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x22

    const v1, 0x5019c2d7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2204738
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStart()V

    .line 2204739
    iget-object v0, p0, Lcom/facebook/growth/nux/CILegalNuxActivity;->q:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2204740
    if-eqz v0, :cond_0

    .line 2204741
    iget-object v2, p0, Lcom/facebook/growth/nux/CILegalNuxActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    invoke-static {v0}, LX/1nR;->d(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    const/4 v3, 0x1

    invoke-interface {v2, v0, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2204742
    :cond_0
    const/16 v0, 0x23

    const v2, -0xa7a2690

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setCustomTitle(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2204737
    return-void
.end method

.method public final x_(I)V
    .locals 1

    .prologue
    .line 2204735
    iget-object v0, p0, Lcom/facebook/growth/nux/CILegalNuxActivity;->r:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setTitle(I)V

    .line 2204736
    return-void
.end method
