.class public Lcom/facebook/growth/nux/NuxProfilePhotoWrapperActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private p:LX/0h5;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2204849
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2204850
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2204851
    const v0, 0x7f031070

    invoke-virtual {p0, v0}, Lcom/facebook/growth/nux/NuxProfilePhotoWrapperActivity;->setContentView(I)V

    .line 2204852
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/growth/nux/NuxProfilePhotoWrapperActivity;->p:LX/0h5;

    .line 2204853
    iget-object v0, p0, Lcom/facebook/growth/nux/NuxProfilePhotoWrapperActivity;->p:LX/0h5;

    const v1, 0x7f083385

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 2204854
    iget-object v0, p0, Lcom/facebook/growth/nux/NuxProfilePhotoWrapperActivity;->p:LX/0h5;

    new-instance v1, LX/F8x;

    invoke-direct {v1, p0}, LX/F8x;-><init>(Lcom/facebook/growth/nux/NuxProfilePhotoWrapperActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->setTitlebarAsModal(Landroid/view/View$OnClickListener;)V

    .line 2204855
    new-instance v1, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;

    invoke-direct {v1}, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;-><init>()V

    .line 2204856
    invoke-virtual {p0}, Lcom/facebook/growth/nux/NuxProfilePhotoWrapperActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/growth/nux/NuxProfilePhotoWrapperActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "external_photo_source"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2204857
    :goto_0
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2204858
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2204859
    const-string v3, "external_photo_source"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2204860
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2204861
    :cond_0
    new-instance v0, LX/F8y;

    invoke-direct {v0, p0}, LX/F8y;-><init>(Lcom/facebook/growth/nux/NuxProfilePhotoWrapperActivity;)V

    .line 2204862
    iput-object v0, v1, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->n:LX/F8y;

    .line 2204863
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    .line 2204864
    const v2, 0x7f0d275d

    invoke-virtual {v0, v2, v1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 2204865
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2204866
    return-void

    .line 2204867
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
