.class public final Lcom/facebook/growth/nux/preferences/ResetNUXStatusPreference$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/F9W;


# direct methods
.method public constructor <init>(LX/F9W;)V
    .locals 0

    .prologue
    .line 2205506
    iput-object p1, p0, Lcom/facebook/growth/nux/preferences/ResetNUXStatusPreference$2;->a:LX/F9W;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2205507
    iget-object v0, p0, Lcom/facebook/growth/nux/preferences/ResetNUXStatusPreference$2;->a:LX/F9W;

    iget-object v0, v0, LX/F9W;->c:LX/0kL;

    new-instance v1, LX/27k;

    const-string v2, "NUX status reset failed."

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2205508
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 9
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2205509
    iget-object v0, p0, Lcom/facebook/growth/nux/preferences/ResetNUXStatusPreference$2;->a:LX/F9W;

    iget-object v0, v0, LX/F9W;->c:LX/0kL;

    new-instance v1, LX/27k;

    const-string v2, "NUX status reset on server complete. Starting status fetch from server."

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2205510
    iget-object v0, p0, Lcom/facebook/growth/nux/preferences/ResetNUXStatusPreference$2;->a:LX/F9W;

    .line 2205511
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2205512
    new-instance v3, Lcom/facebook/interstitial/api/FetchInterstitialsParams;

    const-string v4, "1630"

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/interstitial/api/FetchInterstitialsParams;-><init>(LX/0Px;)V

    .line 2205513
    const-string v4, "fetchAndUpdateInterstitialsParams"

    invoke-virtual {v5, v4, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2205514
    iget-object v3, v0, LX/F9W;->a:LX/0aG;

    const-string v4, "interstitials_fetch_and_update"

    sget-object v6, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v7, LX/F9W;

    invoke-static {v7}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v7

    const v8, -0x249d6b82

    invoke-static/range {v3 .. v8}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v3

    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    move-result-object v3

    .line 2205515
    new-instance v4, LX/F9V;

    invoke-direct {v4, v0}, LX/F9V;-><init>(LX/F9W;)V

    iget-object v5, v0, LX/F9W;->d:Ljava/util/concurrent/Executor;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2205516
    return-void
.end method
