.class public final Lcom/facebook/growth/nux/preferences/ResetNUXStatusPreference$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public final synthetic a:LX/F9W;


# direct methods
.method public constructor <init>(LX/F9W;)V
    .locals 0

    .prologue
    .line 2205499
    iput-object p1, p0, Lcom/facebook/growth/nux/preferences/ResetNUXStatusPreference$1;->a:LX/F9W;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 9

    .prologue
    .line 2205500
    iget-object v0, p0, Lcom/facebook/growth/nux/preferences/ResetNUXStatusPreference$1;->a:LX/F9W;

    iget-object v0, v0, LX/F9W;->c:LX/0kL;

    new-instance v1, LX/27k;

    const-string v2, "Starting NUX status reset."

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2205501
    iget-object v0, p0, Lcom/facebook/growth/nux/preferences/ResetNUXStatusPreference$1;->a:LX/F9W;

    .line 2205502
    iget-object v3, v0, LX/F9W;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, LX/1nR;->a:LX/0Tn;

    invoke-interface {v3, v4}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 2205503
    iget-object v3, v0, LX/F9W;->a:LX/0aG;

    const-string v4, "reset_nux_status"

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    sget-object v6, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v7, LX/F9W;

    invoke-static {v7}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v7

    const v8, -0x8f5aae1

    invoke-static/range {v3 .. v8}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v3

    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    move-result-object v3

    .line 2205504
    new-instance v4, Lcom/facebook/growth/nux/preferences/ResetNUXStatusPreference$2;

    invoke-direct {v4, v0}, Lcom/facebook/growth/nux/preferences/ResetNUXStatusPreference$2;-><init>(LX/F9W;)V

    iget-object v5, v0, LX/F9W;->d:Ljava/util/concurrent/Executor;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2205505
    const/4 v0, 0x1

    return v0
.end method
