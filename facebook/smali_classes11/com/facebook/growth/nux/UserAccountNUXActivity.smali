.class public Lcom/facebook/growth/nux/UserAccountNUXActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/F7u;
.implements LX/8DG;
.implements LX/1ZF;


# static fields
.field public static final p:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private A:LX/F8w;

.field private B:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private C:LX/0ad;

.field private D:LX/63W;

.field private E:LX/63W;

.field private F:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private G:Z

.field public q:Landroid/support/v4/view/ViewPager;

.field private r:LX/0h5;

.field private s:LX/0aG;

.field private t:LX/0Xl;

.field private u:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private v:LX/F7o;

.field private w:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

.field private x:LX/GvB;

.field private y:LX/2CS;

.field public z:LX/F8v;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2204987
    const-class v0, Lcom/facebook/growth/nux/UserAccountNUXActivity;

    sput-object v0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->p:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2204972
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2204973
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->G:Z

    return-void
.end method

.method private a(LX/8DK;)LX/63W;
    .locals 1

    .prologue
    .line 2204988
    new-instance v0, LX/F8z;

    invoke-direct {v0, p0, p1}, LX/F8z;-><init>(Lcom/facebook/growth/nux/UserAccountNUXActivity;LX/8DK;)V

    return-object v0
.end method

.method public static synthetic a(Lcom/facebook/growth/nux/UserAccountNUXActivity;Ljava/lang/String;LX/8DK;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1

    .prologue
    .line 2204989
    invoke-direct {p0, p1, p2}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->a(Ljava/lang/String;LX/8DK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;LX/8DK;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/8DK;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2204975
    invoke-static {p1}, LX/F8w;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2204976
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2204977
    :goto_0
    return-object v0

    .line 2204978
    :cond_0
    new-instance v0, Lcom/facebook/nux/status/UpdateNuxStatusParams;

    const-string v1, "android_new_account_wizard"

    const-string v2, "1630"

    .line 2204979
    sget-object v3, LX/0Rg;->a:LX/0Rg;

    move-object v5, v3

    .line 2204980
    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/nux/status/UpdateNuxStatusParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8DK;LX/0P1;)V

    .line 2204981
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2204982
    const-string v2, "updateNuxStatusParams"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2204983
    iget-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->s:LX/0aG;

    const-string v2, "update_nux_status"

    const v3, -0x13124112

    invoke-static {v0, v2, v1, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2204984
    new-instance v1, LX/F91;

    invoke-direct {v1, p0, p1}, LX/F91;-><init>(Lcom/facebook/growth/nux/UserAccountNUXActivity;Ljava/lang/String;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method private a(LX/0aG;LX/0Xl;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/F7o;Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;LX/GvB;LX/2CS;LX/F8v;LX/F8w;LX/0Or;LX/0ad;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p12    # LX/0Or;
        .annotation runtime Lcom/facebook/growth/annotations/IsNUXImportGoogleAccountProfilePhotoEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0aG;",
            "LX/0Xl;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/F7o;",
            "Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;",
            "Lcom/facebook/auth/login/ipc/LaunchAuthActivityUtil;",
            "LX/2CS;",
            "LX/F8v;",
            "LX/F8w;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2204990
    iput-object p1, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->s:LX/0aG;

    .line 2204991
    iput-object p2, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->t:LX/0Xl;

    .line 2204992
    iput-object p3, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2204993
    iput-object p4, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->v:LX/F7o;

    .line 2204994
    iput-object p5, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->w:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    .line 2204995
    iput-object p6, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->x:LX/GvB;

    .line 2204996
    iput-object p7, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->y:LX/2CS;

    .line 2204997
    iput-object p8, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->z:LX/F8v;

    .line 2204998
    iput-object p9, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->A:LX/F8w;

    .line 2204999
    iput-object p10, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->B:LX/0Or;

    .line 2205000
    iput-object p11, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->C:LX/0ad;

    .line 2205001
    iput-object p12, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->F:LX/0Or;

    .line 2205002
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 14

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v12

    move-object v0, p0

    check-cast v0, Lcom/facebook/growth/nux/UserAccountNUXActivity;

    invoke-static {v12}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v1

    check-cast v1, LX/0aG;

    invoke-static {v12}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v2

    check-cast v2, LX/0Xl;

    invoke-static {v12}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v12}, LX/F7o;->a(LX/0QB;)LX/F7o;

    move-result-object v4

    check-cast v4, LX/F7o;

    invoke-static {v12}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->b(LX/0QB;)Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    move-result-object v5

    check-cast v5, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    invoke-static {v12}, LX/GvB;->b(LX/0QB;)LX/GvB;

    move-result-object v6

    check-cast v6, LX/GvB;

    invoke-static {v12}, LX/2CS;->a(LX/0QB;)LX/2CS;

    move-result-object v7

    check-cast v7, LX/2CS;

    invoke-static {v12}, LX/F8v;->b(LX/0QB;)LX/F8v;

    move-result-object v8

    check-cast v8, LX/F8v;

    invoke-static {v12}, LX/F8w;->a(LX/0QB;)LX/F8w;

    move-result-object v9

    check-cast v9, LX/F8w;

    const/16 v10, 0x15e7

    invoke-static {v12, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static {v12}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    const/16 v13, 0x320

    invoke-static {v12, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-direct/range {v0 .. v12}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->a(LX/0aG;LX/0Xl;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/F7o;Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;LX/GvB;LX/2CS;LX/F8v;LX/F8w;LX/0Or;LX/0ad;LX/0Or;)V

    return-void
.end method

.method public static b(Lcom/facebook/growth/nux/UserAccountNUXActivity;I)V
    .locals 4

    .prologue
    .line 2205003
    iget-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->z:LX/F8v;

    invoke-virtual {v0, p1}, LX/F8v;->e(I)Ljava/lang/String;

    move-result-object v0

    .line 2205004
    invoke-direct {p0, v0}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->d(Ljava/lang/String;)V

    .line 2205005
    const-string v1, "contact_importer"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->w:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    invoke-virtual {v1}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2205006
    :cond_0
    invoke-direct {p0}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->m()V

    .line 2205007
    :cond_1
    iget-object v1, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->y:LX/2CS;

    .line 2205008
    iget-object v2, v1, LX/2CS;->b:LX/0if;

    sget-object v3, LX/0ig;->e:LX/0ih;

    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "start_"

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, v3, p0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2205009
    iget-object v2, v1, LX/2CS;->b:LX/0if;

    sget-object v3, LX/0ig;->f:LX/0ih;

    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "start_"

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, v3, p0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2205010
    iget-object v2, v1, LX/2CS;->b:LX/0if;

    sget-object v3, LX/0ig;->g:LX/0ih;

    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "start_"

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, v3, p0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2205011
    iget-object v2, v1, LX/2CS;->a:LX/0Zb;

    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p0, LX/2Wl;->NUX_STEP_VIEW:LX/2Wl;

    invoke-virtual {p0}, LX/2Wl;->getEventName()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "account_nux"

    .line 2205012
    iput-object p0, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2205013
    move-object v3, v3

    .line 2205014
    const-string p0, "step"

    invoke-virtual {v3, p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p0, "event"

    const-string p1, "saw_step"

    invoke-virtual {v3, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p0, "platform"

    const-string p1, "android"

    invoke-virtual {v3, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p0, "event_type"

    const-string p1, "step"

    invoke-virtual {v3, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2205015
    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2205016
    const-string v0, "upload_profile_pic"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2205017
    iget-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->r:LX/0h5;

    const v1, 0x7f083385

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 2205018
    :cond_0
    :goto_0
    return-void

    .line 2205019
    :cond_1
    const-string v0, "contact_importer"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2205020
    iget-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->w:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    invoke-virtual {v0}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2205021
    iget-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->r:LX/0h5;

    const v1, 0x7f08338f

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    goto :goto_0

    .line 2205022
    :cond_2
    const-string v0, "people_you_may_know"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2205023
    iget-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->r:LX/0h5;

    const v1, 0x7f083390

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    goto :goto_0

    .line 2205024
    :cond_3
    const-string v0, "native_name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2205025
    iget-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->r:LX/0h5;

    const v1, 0x7f083394

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    goto :goto_0
.end method

.method public static l(Lcom/facebook/growth/nux/UserAccountNUXActivity;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    .line 2205026
    iget-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->q:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 2205027
    iget-object v1, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->y:LX/2CS;

    iget-object v2, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->z:LX/F8v;

    invoke-virtual {v2, v0}, LX/F8v;->e(I)Ljava/lang/String;

    move-result-object v2

    .line 2205028
    iget-object v4, v1, LX/2CS;->b:LX/0if;

    sget-object v5, LX/0ig;->e:LX/0ih;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "finish_"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2205029
    iget-object v4, v1, LX/2CS;->b:LX/0if;

    sget-object v5, LX/0ig;->f:LX/0ih;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "finish_"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2205030
    iget-object v4, v1, LX/2CS;->b:LX/0if;

    sget-object v5, LX/0ig;->g:LX/0ih;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "finish_"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2205031
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->z:LX/F8v;

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 2205032
    iget-object v1, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->q:Landroid/support/v4/view/ViewPager;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0, v3}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 2205033
    :goto_0
    return-void

    .line 2205034
    :cond_0
    iget-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->y:LX/2CS;

    .line 2205035
    iget-object v1, v0, LX/2CS;->b:LX/0if;

    sget-object v2, LX/0ig;->e:LX/0ih;

    sget-object v4, LX/2Wl;->NUX_FINISH:LX/2Wl;

    invoke-virtual {v4}, LX/2Wl;->getEventName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2205036
    iget-object v1, v0, LX/2CS;->b:LX/0if;

    sget-object v2, LX/0ig;->e:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->c(LX/0ih;)V

    .line 2205037
    iget-object v1, v0, LX/2CS;->b:LX/0if;

    sget-object v2, LX/0ig;->f:LX/0ih;

    sget-object v4, LX/2Wl;->NUX_FINISH:LX/2Wl;

    invoke-virtual {v4}, LX/2Wl;->getEventName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2205038
    iget-object v1, v0, LX/2CS;->b:LX/0if;

    sget-object v2, LX/0ig;->f:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->c(LX/0ih;)V

    .line 2205039
    iget-object v1, v0, LX/2CS;->b:LX/0if;

    sget-object v2, LX/0ig;->g:LX/0ih;

    sget-object v4, LX/2Wl;->NUX_FINISH:LX/2Wl;

    invoke-virtual {v4}, LX/2Wl;->getEventName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2205040
    iget-object v1, v0, LX/2CS;->b:LX/0if;

    sget-object v2, LX/0ig;->g:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->c(LX/0ih;)V

    .line 2205041
    iget-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1nR;->a:LX/0Tn;

    invoke-interface {v0, v1, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2205042
    iput-boolean v3, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->G:Z

    .line 2205043
    iget-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->x:LX/GvB;

    invoke-virtual {v0, p0}, LX/GvB;->a(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 2205044
    invoke-virtual {p0}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083393

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2205045
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    .line 2205046
    iput-object v0, v1, LX/108;->g:Ljava/lang/String;

    .line 2205047
    move-object v1, v1

    .line 2205048
    iput-object v0, v1, LX/108;->j:Ljava/lang/String;

    .line 2205049
    move-object v0, v1

    .line 2205050
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 2205051
    iget-object v1, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->r:LX/0h5;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2205052
    iget-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->r:LX/0h5;

    iget-object v1, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->D:LX/63W;

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2205053
    return-void
.end method

.method private n()V
    .locals 2

    .prologue
    .line 2205054
    iget-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->r:LX/0h5;

    if-nez v0, :cond_0

    .line 2205055
    :goto_0
    return-void

    .line 2205056
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083392

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2205057
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    .line 2205058
    iput-object v0, v1, LX/108;->g:Ljava/lang/String;

    .line 2205059
    move-object v1, v1

    .line 2205060
    iput-object v0, v1, LX/108;->j:Ljava/lang/String;

    .line 2205061
    move-object v0, v1

    .line 2205062
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 2205063
    iget-object v1, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->r:LX/0h5;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2205064
    iget-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->r:LX/0h5;

    iget-object v1, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->E:LX/63W;

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    goto :goto_0
.end method

.method private o()LX/0hc;
    .locals 1

    .prologue
    .line 2205065
    new-instance v0, LX/F90;

    invoke-direct {v0, p0}, LX/F90;-><init>(Lcom/facebook/growth/nux/UserAccountNUXActivity;)V

    return-object v0
.end method

.method public static p(Lcom/facebook/growth/nux/UserAccountNUXActivity;)V
    .locals 4

    .prologue
    .line 2205066
    new-instance v0, LX/F92;

    invoke-direct {v0, p0}, LX/F92;-><init>(Lcom/facebook/growth/nux/UserAccountNUXActivity;)V

    .line 2205067
    new-instance v1, LX/F93;

    invoke-direct {v1, p0}, LX/F93;-><init>(Lcom/facebook/growth/nux/UserAccountNUXActivity;)V

    .line 2205068
    new-instance v2, LX/0ju;

    invoke-direct {v2, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v3, 0x7f08338f

    invoke-virtual {v2, v3}, LX/0ju;->a(I)LX/0ju;

    move-result-object v2

    const v3, 0x7f0833a4

    invoke-virtual {p0, v3}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v2

    const v3, 0x7f08338f

    invoke-virtual {p0, v3}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v2, 0x7f083393

    invoke-virtual {p0, v2}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2205069
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 2205070
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2204985
    const-string v0, "contact_importer"

    invoke-virtual {p0, v0}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->b(Ljava/lang/String;)V

    .line 2204986
    return-void
.end method

.method public final a(LX/63W;)V
    .locals 1

    .prologue
    .line 2204897
    iget-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->r:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2204898
    return-void
.end method

.method public final a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 2

    .prologue
    .line 2204899
    iget-object v1, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->r:LX/0h5;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1, v0}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2204900
    return-void

    .line 2204901
    :cond_0
    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final a_(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2204902
    iget-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->r:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2204903
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 2204904
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2204905
    invoke-static {p0, p0}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2204906
    const v0, 0x7f03155a

    invoke-virtual {p0, v0}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->setContentView(I)V

    .line 2204907
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2204908
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->r:LX/0h5;

    .line 2204909
    const v0, 0x7f0d300e

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->q:Landroid/support/v4/view/ViewPager;

    .line 2204910
    iget-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->y:LX/2CS;

    .line 2204911
    iget-object v1, v0, LX/2CS;->b:LX/0if;

    sget-object v2, LX/0ig;->e:LX/0ih;

    sget-object p1, LX/2Wl;->NUX_START:LX/2Wl;

    invoke-virtual {p1}, LX/2Wl;->getEventName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2204912
    iget-object v1, v0, LX/2CS;->b:LX/0if;

    sget-object v2, LX/0ig;->f:LX/0ih;

    sget-object p1, LX/2Wl;->NUX_START:LX/2Wl;

    invoke-virtual {p1}, LX/2Wl;->getEventName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2204913
    sget-object v1, LX/0ig;->g:LX/0ih;

    const/16 v2, 0x258

    .line 2204914
    iput v2, v1, LX/0ih;->c:I

    .line 2204915
    iget-object v1, v0, LX/2CS;->b:LX/0if;

    sget-object v2, LX/0ig;->g:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->a(LX/0ih;)V

    .line 2204916
    invoke-virtual {p0}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "show_single_step"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2204917
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2204918
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    .line 2204919
    iget-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->z:LX/F8v;

    const/4 v3, 0x0

    .line 2204920
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2204921
    const/4 v2, 0x1

    .line 2204922
    array-length v6, v1

    move v4, v2

    move v2, v3

    :goto_0
    if-ge v2, v6, :cond_0

    aget-object v7, v1, v2

    .line 2204923
    new-instance p1, Lcom/facebook/ipc/model/NuxStep;

    invoke-direct {p1, v7, v4}, Lcom/facebook/ipc/model/NuxStep;-><init>(Ljava/lang/String;Z)V

    .line 2204924
    invoke-interface {v5, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2204925
    add-int/lit8 v2, v2, 0x1

    move v4, v3

    goto :goto_0

    .line 2204926
    :cond_0
    invoke-static {v5}, LX/F8w;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, v0, LX/F8v;->b:Ljava/util/List;

    .line 2204927
    :cond_1
    iget-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->z:LX/F8v;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    if-nez v0, :cond_2

    .line 2204928
    invoke-static {p0}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->l(Lcom/facebook/growth/nux/UserAccountNUXActivity;)V

    .line 2204929
    :cond_2
    iget-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->z:LX/F8v;

    const/4 v3, 0x0

    .line 2204930
    move v2, v3

    :goto_1
    iget-object v1, v0, LX/F8v;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_4

    .line 2204931
    iget-object v1, v0, LX/F8v;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/model/NuxStep;

    invoke-virtual {v1}, Lcom/facebook/ipc/model/NuxStep;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2204932
    :goto_2
    move v0, v2

    .line 2204933
    iget-object v1, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->q:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->z:LX/F8v;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2204934
    iget-object v1, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->q:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2204935
    iget-object v1, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->q:Landroid/support/v4/view/ViewPager;

    invoke-direct {p0}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->o()LX/0hc;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 2204936
    sget-object v1, LX/8DK;->SKIPPED:LX/8DK;

    invoke-direct {p0, v1}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->a(LX/8DK;)LX/63W;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->D:LX/63W;

    .line 2204937
    sget-object v1, LX/8DK;->COMPLETE:LX/8DK;

    invoke-direct {p0, v1}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->a(LX/8DK;)LX/63W;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->E:LX/63W;

    .line 2204938
    invoke-static {p0, v0}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->b(Lcom/facebook/growth/nux/UserAccountNUXActivity;I)V

    .line 2204939
    return-void

    .line 2204940
    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_4
    move v2, v3

    .line 2204941
    goto :goto_2
.end method

.method public final b(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 0

    .prologue
    .line 2204942
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2204943
    sget-object v0, LX/8DK;->COMPLETE:LX/8DK;

    invoke-direct {p0, p1, v0}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->a(Ljava/lang/String;LX/8DK;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2204944
    invoke-static {p0}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->l(Lcom/facebook/growth/nux/UserAccountNUXActivity;)V

    .line 2204945
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2204946
    invoke-static {p1}, LX/F8w;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2204947
    :goto_0
    return-void

    .line 2204948
    :cond_0
    invoke-direct {p0}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->n()V

    goto :goto_0
.end method

.method public final f()Landroid/view/View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2204949
    const/4 v0, 0x0

    return-object v0
.end method

.method public final k_(Z)V
    .locals 0

    .prologue
    .line 2204950
    return-void
.end method

.method public final lH_()V
    .locals 0

    .prologue
    .line 2204951
    return-void
.end method

.method public final onBackPressed()V
    .locals 4

    .prologue
    .line 2204952
    iget-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->q:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 2204953
    iget-object v1, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->z:LX/F8v;

    invoke-virtual {v1, v0}, LX/F8v;->e(I)Ljava/lang/String;

    move-result-object v1

    .line 2204954
    const-string v2, "contact_importer"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->C:LX/0ad;

    sget-short v2, LX/F94;->c:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2204955
    iget-object v1, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->v:LX/F7o;

    .line 2204956
    iget v2, v1, LX/F7o;->c:I

    move v2, v2

    .line 2204957
    if-lez v2, :cond_2

    iget-object v2, v1, LX/F7o;->a:LX/0gc;

    if-eqz v2, :cond_2

    iget-object v2, v1, LX/F7o;->a:LX/0gc;

    invoke-virtual {v2}, LX/0gc;->f()I

    move-result v2

    if-lez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 2204958
    if-eqz v1, :cond_1

    .line 2204959
    iget-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->v:LX/F7o;

    .line 2204960
    iget-object v1, v0, LX/F7o;->a:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->d()V

    .line 2204961
    iget v1, v0, LX/F7o;->c:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, LX/F7o;->c:I

    .line 2204962
    iget-object v0, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->v:LX/F7o;

    invoke-virtual {v0}, LX/F7o;->c()LX/F7v;

    move-result-object v0

    sget-object v1, LX/F7v;->LEGAL_SCREEN:LX/F7v;

    if-ne v0, v1, :cond_0

    .line 2204963
    invoke-direct {p0}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->m()V

    .line 2204964
    :cond_0
    :goto_1
    return-void

    .line 2204965
    :cond_1
    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->z:LX/F8v;

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2204966
    iget-object v1, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->q:Landroid/support/v4/view/ViewPager;

    add-int/lit8 v0, v0, -0x1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x96ccd9d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2204967
    iget-boolean v1, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->G:Z

    if-eqz v1, :cond_0

    .line 2204968
    iget-object v1, p0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->t:LX/0Xl;

    const-string v2, "com.facebook.growth.constants.nux_completed"

    invoke-interface {v1, v2}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 2204969
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2204970
    const/16 v1, 0x23

    const v2, -0x7a286bfa

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setCustomTitle(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2204971
    return-void
.end method

.method public final x_(I)V
    .locals 0

    .prologue
    .line 2204974
    return-void
.end method
