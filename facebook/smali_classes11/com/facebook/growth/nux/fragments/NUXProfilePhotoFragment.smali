.class public Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;
.super Lcom/facebook/growth/nux/NUXFragment;
.source ""


# static fields
.field private static final j:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/6Hl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/8Hz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/BYf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:LX/0i5;

.field public l:J

.field public m:Z

.field public n:LX/F8y;

.field private o:Z

.field public p:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2205285
    const-class v0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;

    sput-object v0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->j:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2205183
    invoke-direct {p0}, Lcom/facebook/growth/nux/NUXFragment;-><init>()V

    .line 2205184
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->o:Z

    .line 2205185
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->p:Ljava/lang/String;

    .line 2205186
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 10

    .prologue
    .line 2205257
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2205258
    if-eqz v0, :cond_0

    sget-object v1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2205259
    :cond_0
    invoke-static {p0}, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->m(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;)V

    .line 2205260
    :cond_1
    :goto_0
    return-void

    .line 2205261
    :cond_2
    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 2205262
    if-eqz v0, :cond_3

    .line 2205263
    iget-object v1, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 2205264
    if-nez v1, :cond_4

    .line 2205265
    :cond_3
    invoke-static {p0}, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->m(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;)V

    goto :goto_0

    .line 2205266
    :cond_4
    iget-object v1, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->n:LX/F8y;

    if-eqz v1, :cond_5

    .line 2205267
    iget-object v1, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 2205268
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    .line 2205269
    :cond_5
    iget-boolean v1, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->o:Z

    if-eqz v1, :cond_1

    .line 2205270
    iget-object v1, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v0, v1

    .line 2205271
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 2205272
    new-instance v2, LX/4At;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f08338d

    invoke-direct {v2, v3, v4}, LX/4At;-><init>(Landroid/content/Context;I)V

    .line 2205273
    invoke-virtual {v2}, LX/4At;->beginShowingProgress()V

    .line 2205274
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2205275
    iget-boolean v3, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->m:Z

    if-eqz v3, :cond_6

    const-string v8, "upload"

    .line 2205276
    :goto_1
    iget-object v3, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->p:Ljava/lang/String;

    if-nez v3, :cond_7

    const-string v7, "nux"

    .line 2205277
    :goto_2
    new-instance v3, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;

    iget-wide v4, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->l:J

    move-object v6, v0

    invoke-direct/range {v3 .. v8}, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2205278
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2205279
    const-string v5, "growthSetProfilePhotoParams"

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2205280
    iget-object v3, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->a:LX/0aG;

    const-string v5, "growth_set_profile_photo"

    const v6, -0x1cda44f1

    invoke-static {v3, v5, v4, v6}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v3

    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    move-result-object v3

    .line 2205281
    new-instance v4, LX/F9E;

    invoke-direct {v4, p0, v9, v2}, LX/F9E;-><init>(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;Ljava/io/File;LX/4At;)V

    invoke-static {v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2205282
    goto :goto_0

    .line 2205283
    :cond_6
    const-string v8, "camera"

    goto :goto_1

    .line 2205284
    :cond_7
    iget-object v7, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->p:Ljava/lang/String;

    goto :goto_2
.end method

.method private a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2205231
    invoke-virtual {p2}, Landroid/view/ViewGroup;->removeAllViewsInLayout()V

    .line 2205232
    const v0, 0x7f03069b

    const/4 v1, 0x1

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2205233
    iget-object v0, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->h:LX/BYf;

    invoke-virtual {v0}, LX/BYf;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2205234
    const v0, 0x7f0d275f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 2205235
    const v1, 0x7f0d2761

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 2205236
    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2205237
    const v0, 0x7f0d2762

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2205238
    iget-object v1, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->h:LX/BYf;

    .line 2205239
    iget-object v3, v1, LX/BYf;->a:LX/0ad;

    sget-char p1, LX/BYe;->a:C

    const-string p2, ""

    invoke-interface {v3, p1, p2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v1, v3

    .line 2205240
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2205241
    :cond_0
    const v0, 0x7f0d1213

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2205242
    iget-object v1, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f083387

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2205243
    const v0, 0x7f0d1214

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2205244
    const v1, 0x7f0d1216

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 2205245
    new-instance v3, LX/F97;

    invoke-direct {v3, p0}, LX/F97;-><init>(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2205246
    const v1, 0x7f0d1215

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 2205247
    new-instance v2, LX/F98;

    invoke-direct {v2, p0}, LX/F98;-><init>(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2205248
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 2205249
    const-string v3, "android.hardware.camera"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "android.hardware.camera.front"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_1
    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 2205250
    if-eqz v2, :cond_3

    .line 2205251
    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 2205252
    new-instance v1, LX/F99;

    invoke-direct {v1, p0}, LX/F99;-><init>(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2205253
    :goto_2
    return-void

    .line 2205254
    :cond_2
    const v1, 0x7f083386

    goto :goto_0

    .line 2205255
    :cond_3
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 2205256
    new-instance v1, LX/F9A;

    invoke-direct {v1, p0}, LX/F9A;-><init>(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static m(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;)V
    .locals 2

    .prologue
    .line 2205229
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08338e

    invoke-static {v0, v1}, LX/0kL;->a(Landroid/content/Context;I)V

    .line 2205230
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    .line 2205219
    invoke-super {p0, p1}, Lcom/facebook/growth/nux/NUXFragment;->a(Landroid/os/Bundle;)V

    .line 2205220
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;

    invoke-static {p1}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    invoke-static {p1}, LX/6Hl;->b(LX/0QB;)LX/6Hl;

    move-result-object v4

    check-cast v4, LX/6Hl;

    invoke-static {p1}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    const/16 v6, 0x15e7

    invoke-static {p1, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p1}, LX/FAm;->a(LX/0QB;)LX/FAm;

    move-result-object v7

    check-cast v7, LX/8Hz;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    const-class v9, LX/0i4;

    invoke-interface {p1, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/0i4;

    new-instance v0, LX/BYf;

    invoke-static {p1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-direct {v0, v10}, LX/BYf;-><init>(LX/0ad;)V

    move-object v10, v0

    check-cast v10, LX/BYf;

    const/16 v0, 0x1c6

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v3, v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->a:LX/0aG;

    iput-object v4, v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->b:LX/6Hl;

    iput-object v5, v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->c:Ljava/lang/Boolean;

    iput-object v6, v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->d:LX/0Or;

    iput-object v7, v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->e:LX/8Hz;

    iput-object v8, v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->f:Lcom/facebook/content/SecureContextHelper;

    iput-object v9, v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->g:LX/0i4;

    iput-object v10, v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->h:LX/BYf;

    iput-object p1, v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->i:LX/0Ot;

    .line 2205221
    iget-object v0, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->g:LX/0i4;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->k:LX/0i5;

    .line 2205222
    iget-object v0, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2205223
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->l:J

    .line 2205224
    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2205225
    if-eqz v0, :cond_0

    .line 2205226
    const-string v1, "external_photo_source"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->p:Ljava/lang/String;

    .line 2205227
    :cond_0
    return-void

    .line 2205228
    :cond_1
    iget-object v0, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->l:J

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2205215
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, LX/8AA;

    sget-object v2, LX/8AB;->PROFILEPIC_NUX:LX/8AB;

    invoke-direct {v1, v2}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v1}, LX/8AA;->l()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->i()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->j()LX/8AA;

    move-result-object v1

    sget-object v2, LX/8A9;->LAUNCH_PROFILE_PIC_CROPPER:LX/8A9;

    invoke-virtual {v1, v2}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v0

    .line 2205216
    iget-object v1, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->f:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x1a7

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2205217
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->m:Z

    .line 2205218
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2205213
    iget-object v0, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->k:LX/0i5;

    sget-object v1, LX/F8t;->a:[Ljava/lang/String;

    new-instance v2, LX/F9D;

    invoke-direct {v2, p0}, LX/F9D;-><init>(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 2205214
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 2205200
    const/16 v0, 0x1a6

    if-eq p1, v0, :cond_0

    const/16 v0, 0x1a7

    if-ne p1, v0, :cond_3

    .line 2205201
    :cond_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_2

    .line 2205202
    :cond_1
    :goto_0
    return-void

    .line 2205203
    :cond_2
    invoke-direct {p0, p3}, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 2205204
    :cond_3
    const/16 v0, 0x1a5

    if-ne p1, v0, :cond_6

    .line 2205205
    const/4 v0, 0x5

    if-ne p2, v0, :cond_1

    .line 2205206
    invoke-static {}, Lcom/facebook/camera/activity/CameraActivity;->l()Landroid/net/Uri;

    move-result-object v0

    .line 2205207
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    .line 2205208
    :cond_4
    invoke-static {p0}, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->m(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;)V

    goto :goto_0

    .line 2205209
    :cond_5
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 2205210
    iget-object v1, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->e:LX/8Hz;

    invoke-interface {v2, v0}, LX/8Hz;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const/16 v3, 0x1a6

    invoke-interface {v1, v2, v3, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2205211
    goto :goto_0

    .line 2205212
    :cond_6
    sget-object v0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->j:Ljava/lang/Class;

    const-string v1, "Unexpected request code received %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 2205195
    invoke-super {p0, p1}, Lcom/facebook/growth/nux/NUXFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2205196
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2205197
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2205198
    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v1, v0}, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 2205199
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x76b5e0c9

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2205192
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2205193
    invoke-direct {p0, p1, v1}, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 2205194
    const/16 v2, 0x2b

    const v3, 0x2b16ddb0

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, 0x140e0378

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2205187
    iput-object v2, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->e:LX/8Hz;

    .line 2205188
    iput-object v2, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->f:Lcom/facebook/content/SecureContextHelper;

    .line 2205189
    iput-object v2, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->a:LX/0aG;

    .line 2205190
    invoke-super {p0}, Lcom/facebook/growth/nux/NUXFragment;->onDestroy()V

    .line 2205191
    const/16 v1, 0x2b

    const v2, 0x516dfef8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
