.class public Lcom/facebook/growth/nux/fragments/NUXNativeNameFragment;
.super Lcom/facebook/growth/nux/NUXFragment;
.source ""


# instance fields
.field public a:LX/1CX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/F9S;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2205117
    invoke-direct {p0}, Lcom/facebook/growth/nux/NUXFragment;-><init>()V

    .line 2205118
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2205119
    invoke-super {p0, p1}, Lcom/facebook/growth/nux/NUXFragment;->a(Landroid/os/Bundle;)V

    .line 2205120
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/growth/nux/fragments/NUXNativeNameFragment;

    invoke-static {v0}, LX/1CX;->a(LX/0QB;)LX/1CX;

    move-result-object p1

    check-cast p1, LX/1CX;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    iput-object p1, p0, Lcom/facebook/growth/nux/fragments/NUXNativeNameFragment;->a:LX/1CX;

    iput-object v0, p0, Lcom/facebook/growth/nux/fragments/NUXNativeNameFragment;->b:LX/0aG;

    .line 2205121
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/16 v0, 0x2a

    const v1, -0x31200676

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2205122
    const v0, 0x7f03069a

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2205123
    new-instance v0, LX/F9S;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v0, v3, v4}, LX/F9S;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/facebook/growth/nux/fragments/NUXNativeNameFragment;->c:LX/F9S;

    .line 2205124
    const v0, 0x7f0d1211

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2205125
    iget-object v3, p0, Lcom/facebook/growth/nux/fragments/NUXNativeNameFragment;->c:LX/F9S;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2205126
    const v0, 0x7f0d1212

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    check-cast v0, Landroid/widget/Button;

    .line 2205127
    new-instance v3, LX/F96;

    invoke-direct {v3, p0}, LX/F96;-><init>(Lcom/facebook/growth/nux/fragments/NUXNativeNameFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2205128
    const/16 v0, 0x2b

    const v3, -0x3fa2e058

    invoke-static {v6, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method
