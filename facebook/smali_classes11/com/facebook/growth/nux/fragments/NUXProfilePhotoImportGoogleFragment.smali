.class public Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;
.super Lcom/facebook/growth/nux/NUXFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final l:Lcom/facebook/common/callercontext/CallerContext;

.field private static final m:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/F9q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/8Hz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/6Hl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8KY;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private n:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2205461
    const-class v0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->l:Lcom/facebook/common/callercontext/CallerContext;

    .line 2205462
    const-class v0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;

    sput-object v0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->m:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2205460
    invoke-direct {p0}, Lcom/facebook/growth/nux/NUXFragment;-><init>()V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2205437
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2205438
    :try_start_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "r"

    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    .line 2205439
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 2205440
    :try_start_1
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2205441
    :try_start_2
    invoke-static {v3, v1}, LX/0hW;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2205442
    if-eqz v3, :cond_0

    .line 2205443
    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 2205444
    :cond_0
    :goto_0
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 2205445
    :goto_1
    return-void

    .line 2205446
    :catch_0
    move-exception v0

    .line 2205447
    sget-object v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->m:Ljava/lang/Class;

    const-string v3, "failed to close Google picture temp file input stream"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2205448
    :catch_1
    move-exception v0

    .line 2205449
    sget-object v1, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->m:Ljava/lang/Class;

    const-string v2, "failed to close Google picture destination file output stream"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2205450
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v2, :cond_1

    .line 2205451
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 2205452
    :cond_1
    :goto_3
    if-eqz v1, :cond_2

    .line 2205453
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 2205454
    :cond_2
    :goto_4
    throw v0

    .line 2205455
    :catch_2
    move-exception v2

    .line 2205456
    sget-object v3, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->m:Ljava/lang/Class;

    const-string v4, "failed to close Google picture temp file input stream"

    invoke-static {v3, v4, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 2205457
    :catch_3
    move-exception v1

    .line 2205458
    sget-object v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->m:Ljava/lang/Class;

    const-string v3, "failed to close Google picture destination file output stream"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 2205459
    :catchall_1
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_2

    :catchall_2
    move-exception v0

    move-object v2, v3

    goto :goto_2
.end method

.method public static b(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2205428
    new-instance v1, LX/4At;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f08338d

    invoke-direct {v1, v0, v2}, LX/4At;-><init>(Landroid/content/Context;I)V

    .line 2205429
    invoke-virtual {v1}, LX/4At;->beginShowingProgress()V

    .line 2205430
    new-instance v2, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;

    iget-object v0, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2205431
    iget-object v3, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v3

    .line 2205432
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-direct {v2, v4, v5, p1}, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;-><init>(JLjava/lang/String;)V

    .line 2205433
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2205434
    const-string v3, "growthSetProfilePhotoParams"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2205435
    iget-object v2, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->h:LX/1Ck;

    const/4 v3, 0x0

    new-instance v4, LX/F9P;

    invoke-direct {v4, p0, v0}, LX/F9P;-><init>(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;Landroid/os/Bundle;)V

    new-instance v0, LX/F9F;

    invoke-direct {v0, p0, p1, v1}, LX/F9F;-><init>(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;Ljava/lang/String;LX/4At;)V

    invoke-virtual {v2, v3, v4, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2205436
    return-void
.end method

.method public static c(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;)Z
    .locals 2

    .prologue
    .line 2205426
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 2205427
    const-string v1, "android.hardware.camera"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.hardware.camera.front"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;)Ljava/lang/String;
    .locals 14
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2205410
    iget-object v0, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8KY;

    .line 2205411
    sget-object v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->m:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->n:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/8KY;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 2205412
    if-nez v2, :cond_1

    .line 2205413
    :try_start_0
    sget-object v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->m:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const/high16 v3, 0x500000

    const/high16 v4, 0x100000

    .line 2205414
    const-string v9, "tmp"

    const-string v10, "scratch"

    move-object v5, v0

    move-object v6, v2

    move v7, v3

    move v8, v4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2205415
    invoke-static {v5, v7, v8}, LX/8KY;->a(LX/8KY;II)Ljava/io/File;

    move-result-object v11

    .line 2205416
    new-instance v12, Ljava/io/File;

    const-string v13, "%s.%s"

    new-array p0, v4, [Ljava/lang/Object;

    aput-object v6, p0, v2

    aput-object v9, p0, v3

    invoke-static {v13, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v11, v13}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2205417
    new-instance v13, Ljava/io/File;

    const-string p0, "%s.%s.%s"

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    aput-object v6, v0, v2

    aput-object v9, v0, v3

    aput-object v10, v0, v4

    invoke-static {p0, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v13, v11, p0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2205418
    new-instance v11, LX/8KX;

    invoke-direct {v11, v12, v13, v2}, LX/8KX;-><init>(Ljava/io/File;Ljava/io/File;Z)V

    move-object v5, v11

    .line 2205419
    move-object v0, v5

    .line 2205420
    if-nez v0, :cond_0

    move-object v0, v1

    .line 2205421
    :goto_0
    return-object v0

    .line 2205422
    :cond_0
    iget-object v2, v0, LX/8KX;->a:Ljava/io/File;

    move-object v0, v2

    .line 2205423
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;
    :try_end_0
    .catch LX/8KV; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 2205424
    :catch_0
    move-object v0, v1

    goto :goto_0

    .line 2205425
    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;)V
    .locals 3

    .prologue
    .line 2205407
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, LX/8AA;

    sget-object v2, LX/8AB;->PROFILEPIC_NUX:LX/8AB;

    invoke-direct {v1, v2}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v1}, LX/8AA;->l()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->i()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->j()LX/8AA;

    move-result-object v1

    sget-object v2, LX/8A9;->LAUNCH_PROFILE_PIC_CROPPER:LX/8A9;

    invoke-virtual {v1, v2}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v0

    .line 2205408
    iget-object v1, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->g:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x1a7

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2205409
    return-void
.end method

.method public static k(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2205396
    iget-object v0, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->f:LX/6Hl;

    invoke-virtual {v0, v4}, LX/6Hl;->b(Z)LX/6Hl;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/6Hl;->c(Z)LX/6Hl;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    .line 2205397
    iput-object v1, v0, LX/6Hl;->b:Landroid/app/Activity;

    .line 2205398
    move-object v0, v0

    .line 2205399
    invoke-virtual {v0, v4}, LX/6Hl;->d(Z)LX/6Hl;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2205400
    iget-object v2, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v2

    .line 2205401
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2205402
    iput-wide v2, v1, LX/6Hl;->c:J

    .line 2205403
    move-object v0, v1

    .line 2205404
    invoke-virtual {v0, v4}, LX/6Hl;->a(I)LX/6Hl;

    move-result-object v0

    invoke-virtual {v0}, LX/6Hl;->a()Landroid/content/Intent;

    move-result-object v0

    .line 2205405
    iget-object v1, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->g:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x1a5

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2205406
    return-void
.end method

.method public static l(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;)V
    .locals 3

    .prologue
    .line 2205394
    iget-object v0, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->j:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08338e

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2205395
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    .line 2205390
    invoke-super {p0, p1}, Lcom/facebook/growth/nux/NUXFragment;->a(Landroid/os/Bundle;)V

    .line 2205391
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->n:Ljava/lang/String;

    .line 2205392
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;

    const/16 v3, 0x12cb

    invoke-static {p1, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p1}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    new-instance v7, LX/F9q;

    const-class v5, Landroid/content/Context;

    invoke-interface {p1, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-direct {v7, v5, v6}, LX/F9q;-><init>(Landroid/content/Context;LX/03V;)V

    move-object v5, v7

    check-cast v5, LX/F9q;

    const/16 v6, 0x509

    invoke-static {p1, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p1}, LX/FAm;->a(LX/0QB;)LX/FAm;

    move-result-object v7

    check-cast v7, LX/8Hz;

    invoke-static {p1}, LX/6Hl;->b(LX/0QB;)LX/6Hl;

    move-result-object v8

    check-cast v8, LX/6Hl;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v10

    check-cast v10, LX/1Ck;

    invoke-static {p1}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v11

    check-cast v11, LX/0aG;

    invoke-static {p1}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v12

    check-cast v12, LX/0kL;

    const/16 v0, 0x2ed9

    invoke-static {p1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v3, v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->a:LX/0Or;

    iput-object v4, v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->b:LX/0TD;

    iput-object v5, v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->c:LX/F9q;

    iput-object v6, v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->d:LX/0Or;

    iput-object v7, v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->e:LX/8Hz;

    iput-object v8, v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->f:LX/6Hl;

    iput-object v9, v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->g:Lcom/facebook/content/SecureContextHelper;

    iput-object v10, v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->h:LX/1Ck;

    iput-object v11, v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->i:LX/0aG;

    iput-object v12, v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->j:LX/0kL;

    iput-object p1, v2, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->k:LX/0Ot;

    .line 2205393
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 2205339
    const/16 v0, 0x1a6

    if-eq p1, v0, :cond_0

    const/16 v0, 0x1a7

    if-ne p1, v0, :cond_4

    .line 2205340
    :cond_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_2

    .line 2205341
    :cond_1
    :goto_0
    return-void

    .line 2205342
    :cond_2
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2205343
    if-eqz v0, :cond_3

    sget-object v1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 2205344
    :cond_3
    invoke-static {p0}, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->l(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;)V

    .line 2205345
    :goto_1
    goto :goto_0

    .line 2205346
    :cond_4
    const/16 v0, 0x1a5

    if-ne p1, v0, :cond_7

    .line 2205347
    const/4 v0, 0x5

    if-ne p2, v0, :cond_1

    .line 2205348
    invoke-static {}, Lcom/facebook/camera/activity/CameraActivity;->l()Landroid/net/Uri;

    move-result-object v0

    .line 2205349
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_6

    .line 2205350
    :cond_5
    invoke-static {p0}, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->l(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;)V

    goto :goto_0

    .line 2205351
    :cond_6
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 2205352
    iget-object v1, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->e:LX/8Hz;

    invoke-interface {v1, v0}, LX/8Hz;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2205353
    iget-object v2, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->g:Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x1a6

    invoke-interface {v2, v1, v3, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2205354
    goto :goto_0

    .line 2205355
    :cond_7
    sget-object v0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->m:Ljava/lang/Class;

    const-string v1, "Unexpected request code received %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 2205356
    :cond_8
    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 2205357
    if-eqz v0, :cond_9

    .line 2205358
    iget-object v1, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 2205359
    if-nez v1, :cond_a

    .line 2205360
    :cond_9
    invoke-static {p0}, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->l(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;)V

    goto :goto_1

    .line 2205361
    :cond_a
    iget-object v1, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v0, v1

    .line 2205362
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->b(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0xa84579b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2205389
    const v1, 0x7f03069d

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x73802485

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x73d62d15

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2205386
    iget-object v1, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->h:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2205387
    invoke-super {p0}, Lcom/facebook/growth/nux/NUXFragment;->onDestroy()V

    .line 2205388
    const/16 v1, 0x2b

    const v2, -0x3c6163e6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2205363
    invoke-super {p0, p1, p2}, Lcom/facebook/growth/nux/NUXFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2205364
    const v0, 0x7f0d1217

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2205365
    const v1, 0x7f0d1218

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2205366
    const v2, 0x7f0d1219

    invoke-virtual {p0, v2}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 2205367
    const v3, 0x7f0d121a

    invoke-virtual {p0, v3}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 2205368
    iget-object v4, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->a:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/user/model/User;

    invoke-virtual {v4}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2205369
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2205370
    iget-object v4, p0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->c:LX/F9q;

    invoke-virtual {v4}, LX/F9q;->a()Landroid/net/Uri;

    move-result-object v4

    .line 2205371
    if-eqz v4, :cond_0

    .line 2205372
    const v5, 0x7f08001a

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v5, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 2205373
    invoke-virtual {v2, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2205374
    new-instance v1, LX/F9G;

    invoke-direct {v1, p0, v4}, LX/F9G;-><init>(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;Landroid/net/Uri;)V

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2205375
    const v1, 0x7f08338c

    invoke-virtual {v3, v1}, Landroid/widget/Button;->setText(I)V

    .line 2205376
    new-instance v1, LX/F9H;

    invoke-direct {v1, p0}, LX/F9H;-><init>(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;)V

    invoke-virtual {v3, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2205377
    :goto_0
    sget-object v1, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v4, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2205378
    return-void

    .line 2205379
    :cond_0
    const v1, 0x7f083388

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setText(I)V

    .line 2205380
    new-instance v1, LX/F9I;

    invoke-direct {v1, p0}, LX/F9I;-><init>(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;)V

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2205381
    const v1, 0x7f083389

    invoke-virtual {v3, v1}, Landroid/widget/Button;->setText(I)V

    .line 2205382
    new-instance v1, LX/F9J;

    invoke-direct {v1, p0}, LX/F9J;-><init>(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;)V

    invoke-virtual {v3, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2205383
    invoke-static {p0}, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->c(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2205384
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 2205385
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method
