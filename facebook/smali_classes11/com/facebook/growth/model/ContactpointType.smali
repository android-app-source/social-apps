.class public final enum Lcom/facebook/growth/model/ContactpointType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/growth/model/ContactpointTypeDeserializer;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/growth/model/ContactpointType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/growth/model/ContactpointType;

.field public static final enum EMAIL:Lcom/facebook/growth/model/ContactpointType;

.field public static final enum PHONE:Lcom/facebook/growth/model/ContactpointType;

.field public static final enum UNKNOWN:Lcom/facebook/growth/model/ContactpointType;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2204600
    const-class v0, Lcom/facebook/growth/model/ContactpointTypeDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2204601
    new-instance v0, Lcom/facebook/growth/model/ContactpointType;

    const-string v1, "PHONE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/growth/model/ContactpointType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    .line 2204602
    new-instance v0, Lcom/facebook/growth/model/ContactpointType;

    const-string v1, "EMAIL"

    invoke-direct {v0, v1, v3}, Lcom/facebook/growth/model/ContactpointType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/growth/model/ContactpointType;->EMAIL:Lcom/facebook/growth/model/ContactpointType;

    .line 2204603
    new-instance v0, Lcom/facebook/growth/model/ContactpointType;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, Lcom/facebook/growth/model/ContactpointType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/growth/model/ContactpointType;->UNKNOWN:Lcom/facebook/growth/model/ContactpointType;

    .line 2204604
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/growth/model/ContactpointType;

    sget-object v1, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/growth/model/ContactpointType;->EMAIL:Lcom/facebook/growth/model/ContactpointType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/growth/model/ContactpointType;->UNKNOWN:Lcom/facebook/growth/model/ContactpointType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/growth/model/ContactpointType;->$VALUES:[Lcom/facebook/growth/model/ContactpointType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2204605
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/growth/model/ContactpointType;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonCreator;
    .end annotation

    .prologue
    .line 2204606
    sget-object v0, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    invoke-virtual {v0}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    .line 2204607
    :goto_0
    return-object v0

    .line 2204608
    :cond_0
    sget-object v0, Lcom/facebook/growth/model/ContactpointType;->EMAIL:Lcom/facebook/growth/model/ContactpointType;

    invoke-virtual {v0}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/facebook/growth/model/ContactpointType;->EMAIL:Lcom/facebook/growth/model/ContactpointType;

    goto :goto_0

    .line 2204609
    :cond_1
    sget-object v0, Lcom/facebook/growth/model/ContactpointType;->UNKNOWN:Lcom/facebook/growth/model/ContactpointType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/growth/model/ContactpointType;
    .locals 1

    .prologue
    .line 2204610
    const-class v0, Lcom/facebook/growth/model/ContactpointType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/model/ContactpointType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/growth/model/ContactpointType;
    .locals 1

    .prologue
    .line 2204611
    sget-object v0, Lcom/facebook/growth/model/ContactpointType;->$VALUES:[Lcom/facebook/growth/model/ContactpointType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/growth/model/ContactpointType;

    return-object v0
.end method
