.class public Lcom/facebook/growth/model/Contactpoint;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/growth/model/ContactpointDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/growth/model/Contactpoint;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final isoCountryCode:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "iso_country_code"
    .end annotation
.end field

.field public final normalized:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "normalized_contactpoint"
    .end annotation
.end field

.field public final type:Lcom/facebook/growth/model/ContactpointType;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "contactpoint_type"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2204576
    const-class v0, Lcom/facebook/growth/model/ContactpointDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2204545
    new-instance v0, LX/F8n;

    invoke-direct {v0}, LX/F8n;-><init>()V

    sput-object v0, Lcom/facebook/growth/model/Contactpoint;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2204571
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2204572
    iput-object v1, p0, Lcom/facebook/growth/model/Contactpoint;->normalized:Ljava/lang/String;

    .line 2204573
    sget-object v0, Lcom/facebook/growth/model/ContactpointType;->UNKNOWN:Lcom/facebook/growth/model/ContactpointType;

    iput-object v0, p0, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    .line 2204574
    iput-object v1, p0, Lcom/facebook/growth/model/Contactpoint;->isoCountryCode:Ljava/lang/String;

    .line 2204575
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2204566
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2204567
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/model/Contactpoint;->normalized:Ljava/lang/String;

    .line 2204568
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/growth/model/ContactpointType;->valueOf(Ljava/lang/String;)Lcom/facebook/growth/model/ContactpointType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    .line 2204569
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/model/Contactpoint;->isoCountryCode:Ljava/lang/String;

    .line 2204570
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/facebook/growth/model/ContactpointType;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2204561
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2204562
    iput-object p1, p0, Lcom/facebook/growth/model/Contactpoint;->normalized:Ljava/lang/String;

    .line 2204563
    iput-object p2, p0, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    .line 2204564
    iput-object p3, p0, Lcom/facebook/growth/model/Contactpoint;->isoCountryCode:Ljava/lang/String;

    .line 2204565
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/growth/model/Contactpoint;
    .locals 3

    .prologue
    .line 2204560
    new-instance v0, Lcom/facebook/growth/model/Contactpoint;

    sget-object v1, Lcom/facebook/growth/model/ContactpointType;->EMAIL:Lcom/facebook/growth/model/ContactpointType;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/facebook/growth/model/Contactpoint;-><init>(Ljava/lang/String;Lcom/facebook/growth/model/ContactpointType;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/growth/model/Contactpoint;
    .locals 2

    .prologue
    .line 2204559
    new-instance v0, Lcom/facebook/growth/model/Contactpoint;

    sget-object v1, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    invoke-direct {v0, p0, v1, p1}, Lcom/facebook/growth/model/Contactpoint;-><init>(Ljava/lang/String;Lcom/facebook/growth/model/ContactpointType;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2204557
    iget-object v1, p0, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    sget-object v2, Lcom/facebook/growth/model/ContactpointType;->UNKNOWN:Lcom/facebook/growth/model/ContactpointType;

    if-ne v1, v2, :cond_1

    .line 2204558
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/facebook/growth/model/Contactpoint;->normalized:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2204556
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2204551
    if-ne p0, p1, :cond_1

    .line 2204552
    :cond_0
    :goto_0
    return v0

    .line 2204553
    :cond_1
    if-eqz p1, :cond_2

    instance-of v2, p1, Lcom/facebook/growth/model/Contactpoint;

    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 2204554
    :cond_3
    check-cast p1, Lcom/facebook/growth/model/Contactpoint;

    .line 2204555
    iget-object v2, p0, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    iget-object v3, p1, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/growth/model/Contactpoint;->isoCountryCode:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/growth/model/Contactpoint;->isoCountryCode:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/growth/model/Contactpoint;->normalized:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/growth/model/Contactpoint;->normalized:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2204550
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/growth/model/Contactpoint;->normalized:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/growth/model/Contactpoint;->isoCountryCode:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2204546
    iget-object v0, p0, Lcom/facebook/growth/model/Contactpoint;->normalized:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2204547
    iget-object v0, p0, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    invoke-virtual {v0}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2204548
    iget-object v0, p0, Lcom/facebook/growth/model/Contactpoint;->isoCountryCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2204549
    return-void
.end method
