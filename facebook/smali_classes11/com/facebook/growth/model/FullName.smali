.class public Lcom/facebook/growth/model/FullName;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/growth/model/FullName;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2204685
    new-instance v0, LX/F8p;

    invoke-direct {v0}, LX/F8p;-><init>()V

    sput-object v0, Lcom/facebook/growth/model/FullName;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2204713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2204714
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/model/FullName;->b:Ljava/lang/String;

    .line 2204715
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/model/FullName;->d:Ljava/lang/String;

    .line 2204716
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/model/FullName;->c:Ljava/lang/String;

    .line 2204717
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/model/FullName;->a:Ljava/lang/String;

    .line 2204718
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2204707
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2204708
    iput-object p1, p0, Lcom/facebook/growth/model/FullName;->b:Ljava/lang/String;

    .line 2204709
    iput-object p2, p0, Lcom/facebook/growth/model/FullName;->d:Ljava/lang/String;

    .line 2204710
    iput-object p3, p0, Lcom/facebook/growth/model/FullName;->c:Ljava/lang/String;

    .line 2204711
    iput-object p4, p0, Lcom/facebook/growth/model/FullName;->a:Ljava/lang/String;

    .line 2204712
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2204706
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2204719
    if-ne p0, p1, :cond_1

    .line 2204720
    :cond_0
    :goto_0
    return v0

    .line 2204721
    :cond_1
    if-eqz p1, :cond_2

    instance-of v2, p1, Lcom/facebook/growth/model/FullName;

    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    .line 2204722
    goto :goto_0

    .line 2204723
    :cond_3
    check-cast p1, Lcom/facebook/growth/model/FullName;

    .line 2204724
    iget-object v2, p0, Lcom/facebook/growth/model/FullName;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/growth/model/FullName;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/growth/model/FullName;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/growth/model/FullName;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/growth/model/FullName;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/growth/model/FullName;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/growth/model/FullName;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/growth/model/FullName;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2204705
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/growth/model/FullName;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/growth/model/FullName;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/growth/model/FullName;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/growth/model/FullName;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2204691
    iget-object v0, p0, Lcom/facebook/growth/model/FullName;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2204692
    iget-object v0, p0, Lcom/facebook/growth/model/FullName;->a:Ljava/lang/String;

    .line 2204693
    :cond_0
    :goto_0
    return-object v0

    .line 2204694
    :cond_1
    const-string v0, ""

    .line 2204695
    iget-object v1, p0, Lcom/facebook/growth/model/FullName;->b:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2204696
    iget-object v0, p0, Lcom/facebook/growth/model/FullName;->b:Ljava/lang/String;

    .line 2204697
    :cond_2
    iget-object v1, p0, Lcom/facebook/growth/model/FullName;->d:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2204698
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2204699
    iget-object v0, p0, Lcom/facebook/growth/model/FullName;->d:Ljava/lang/String;

    .line 2204700
    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/facebook/growth/model/FullName;->c:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2204701
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2204702
    iget-object v0, p0, Lcom/facebook/growth/model/FullName;->c:Ljava/lang/String;

    goto :goto_0

    .line 2204703
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/growth/model/FullName;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2204704
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/growth/model/FullName;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2204686
    iget-object v0, p0, Lcom/facebook/growth/model/FullName;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2204687
    iget-object v0, p0, Lcom/facebook/growth/model/FullName;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2204688
    iget-object v0, p0, Lcom/facebook/growth/model/FullName;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2204689
    iget-object v0, p0, Lcom/facebook/growth/model/FullName;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2204690
    return-void
.end method
