.class public Lcom/facebook/growth/model/DeviceOwnerData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/growth/model/DeviceOwnerData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lcom/facebook/growth/model/Birthday;

.field private b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/growth/model/FullName;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2204619
    new-instance v0, LX/F8o;

    invoke-direct {v0}, LX/F8o;-><init>()V

    sput-object v0, Lcom/facebook/growth/model/DeviceOwnerData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2204675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2204676
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->a:Lcom/facebook/growth/model/Birthday;

    .line 2204677
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->b:Ljava/util/Set;

    .line 2204678
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->c:Ljava/util/Set;

    .line 2204679
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->d:Ljava/util/Set;

    .line 2204680
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->e:Ljava/lang/String;

    .line 2204681
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2204662
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2204663
    const-class v0, Lcom/facebook/growth/model/Birthday;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/model/Birthday;

    iput-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->a:Lcom/facebook/growth/model/Birthday;

    .line 2204664
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2204665
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 2204666
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1, v0}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/facebook/growth/model/DeviceOwnerData;->b:Ljava/util/Set;

    .line 2204667
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2204668
    sget-object v1, Lcom/facebook/growth/model/FullName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 2204669
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1, v0}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/facebook/growth/model/DeviceOwnerData;->c:Ljava/util/Set;

    .line 2204670
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2204671
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 2204672
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1, v0}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/facebook/growth/model/DeviceOwnerData;->d:Ljava/util/Set;

    .line 2204673
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->e:Ljava/lang/String;

    .line 2204674
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/facebook/growth/model/Birthday;
    .locals 1

    .prologue
    .line 2204661
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->a:Lcom/facebook/growth/model/Birthday;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/growth/model/Birthday;)V
    .locals 1

    .prologue
    .line 2204658
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/facebook/growth/model/DeviceOwnerData;->a:Lcom/facebook/growth/model/Birthday;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2204659
    monitor-exit p0

    return-void

    .line 2204660
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/growth/model/DeviceOwnerData;)V
    .locals 3

    .prologue
    .line 2204645
    monitor-enter p0

    if-nez p1, :cond_1

    .line 2204646
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2204647
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->a:Lcom/facebook/growth/model/Birthday;

    if-nez v0, :cond_2

    .line 2204648
    iget-object v0, p1, Lcom/facebook/growth/model/DeviceOwnerData;->a:Lcom/facebook/growth/model/Birthday;

    iput-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->a:Lcom/facebook/growth/model/Birthday;

    .line 2204649
    :cond_2
    iget-object v0, p1, Lcom/facebook/growth/model/DeviceOwnerData;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2204650
    iget-object v2, p0, Lcom/facebook/growth/model/DeviceOwnerData;->b:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 2204651
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2204652
    :cond_3
    :try_start_1
    iget-object v0, p1, Lcom/facebook/growth/model/DeviceOwnerData;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/model/FullName;

    .line 2204653
    iget-object v2, p0, Lcom/facebook/growth/model/DeviceOwnerData;->c:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2204654
    :cond_4
    iget-object v0, p1, Lcom/facebook/growth/model/DeviceOwnerData;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2204655
    iget-object v2, p0, Lcom/facebook/growth/model/DeviceOwnerData;->d:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2204656
    :cond_5
    iget-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2204657
    iget-object v0, p1, Lcom/facebook/growth/model/DeviceOwnerData;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->e:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/facebook/growth/model/FullName;)V
    .locals 1

    .prologue
    .line 2204642
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2204643
    monitor-exit p0

    return-void

    .line 2204644
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2204639
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2204640
    monitor-exit p0

    return-void

    .line 2204641
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/growth/model/FullName;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2204638
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->c:Ljava/util/Set;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2204635
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2204636
    monitor-exit p0

    return-void

    .line 2204637
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2204634
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->b:Ljava/util/Set;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2204631
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/facebook/growth/model/DeviceOwnerData;->e:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2204632
    monitor-exit p0

    return-void

    .line 2204633
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2204630
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->d:Ljava/util/Set;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2204629
    const/4 v0, 0x0

    return v0
.end method

.method public final declared-synchronized e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2204628
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->e:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()Z
    .locals 1

    .prologue
    .line 2204627
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()Z
    .locals 1

    .prologue
    .line 2204626
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2204620
    iget-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->a:Lcom/facebook/growth/model/Birthday;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2204621
    iget-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->b:Ljava/util/Set;

    new-array v1, v2, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 2204622
    iget-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->c:Ljava/util/Set;

    new-array v1, v2, [Lcom/facebook/growth/model/FullName;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 2204623
    iget-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->d:Ljava/util/Set;

    new-array v1, v2, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 2204624
    iget-object v0, p0, Lcom/facebook/growth/model/DeviceOwnerData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2204625
    return-void
.end method
