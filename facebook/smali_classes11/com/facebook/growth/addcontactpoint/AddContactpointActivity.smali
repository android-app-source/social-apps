.class public Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private A:Ljava/lang/String;

.field public p:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/F9m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/1CW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/9Tl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2U8;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/3fx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:Landroid/widget/EditText;

.field public x:Lcom/facebook/widget/countryspinner/CountrySpinner;

.field public y:Landroid/widget/Button;

.field public z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2200559
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2200560
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->A:Ljava/lang/String;

    return-void
.end method

.method private static a(Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;LX/0aG;LX/F9m;LX/1CW;LX/9Tl;LX/0Ot;LX/0kL;LX/3fx;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;",
            "LX/0aG;",
            "LX/F9m;",
            "LX/1CW;",
            "LX/9Tl;",
            "LX/0Ot",
            "<",
            "LX/2U8;",
            ">;",
            "LX/0kL;",
            "LX/3fx;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2200558
    iput-object p1, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->p:LX/0aG;

    iput-object p2, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->q:LX/F9m;

    iput-object p3, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->r:LX/1CW;

    iput-object p4, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->s:LX/9Tl;

    iput-object p5, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->t:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->u:LX/0kL;

    iput-object p7, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->v:LX/3fx;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    invoke-static {v7}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v1

    check-cast v1, LX/0aG;

    invoke-static {v7}, LX/F9m;->b(LX/0QB;)LX/F9m;

    move-result-object v2

    check-cast v2, LX/F9m;

    invoke-static {v7}, LX/1CW;->b(LX/0QB;)LX/1CW;

    move-result-object v3

    check-cast v3, LX/1CW;

    invoke-static {v7}, LX/9Tl;->b(LX/0QB;)LX/9Tl;

    move-result-object v4

    check-cast v4, LX/9Tl;

    const/16 v5, 0x3ea

    invoke-static {v7, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v7}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v6

    check-cast v6, LX/0kL;

    invoke-static {v7}, LX/3fx;->a(LX/0QB;)LX/3fx;

    move-result-object v7

    check-cast v7, LX/3fx;

    invoke-static/range {v0 .. v7}, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->a(Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;LX/0aG;LX/F9m;LX/1CW;LX/9Tl;LX/0Ot;LX/0kL;LX/3fx;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 2200541
    iget-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->s:LX/9Tl;

    .line 2200542
    iget-object v1, v0, LX/9Tl;->a:LX/0Zb;

    sget-object v2, LX/9Tm;->ADD_CONTACTPOINT_FAILURE:LX/9Tm;

    invoke-virtual {v2}, LX/9Tm;->getAnalyticsName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2200543
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2200544
    const-string v2, "growth"

    invoke-virtual {v1, v2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2200545
    const-string v2, "error_code"

    const/4 p0, -0x1

    .line 2200546
    if-nez p1, :cond_1

    move v3, p0

    .line 2200547
    :goto_0
    move v3, v3

    .line 2200548
    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2200549
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2200550
    :cond_0
    return-void

    .line 2200551
    :cond_1
    iget-object v3, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v3, v3

    .line 2200552
    if-nez v3, :cond_2

    move v3, p0

    .line 2200553
    goto :goto_0

    .line 2200554
    :cond_2
    const-string v0, "result"

    invoke-virtual {v3, v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/http/protocol/ApiErrorResult;

    .line 2200555
    if-nez v3, :cond_3

    move v3, p0

    .line 2200556
    goto :goto_0

    .line 2200557
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v3

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 2200533
    iget-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->s:LX/9Tl;

    .line 2200534
    iget-object v1, v0, LX/9Tl;->a:LX/0Zb;

    sget-object v2, LX/9Tm;->ADD_CONTACTPOINT_ATTEMPT:LX/9Tm;

    invoke-virtual {v2}, LX/9Tm;->getAnalyticsName()Ljava/lang/String;

    move-result-object v2

    const/4 p0, 0x1

    invoke-interface {v1, v2, p0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2200535
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2200536
    const-string v2, "growth"

    invoke-virtual {v1, v2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2200537
    const-string v2, "phone_number_added"

    invoke-virtual {v1, v2, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2200538
    const-string v2, "phone_number_changed"

    invoke-virtual {v1, v2, p2}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 2200539
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2200540
    :cond_0
    return-void
.end method

.method private b()Landroid/widget/AdapterView$OnItemSelectedListener;
    .locals 1

    .prologue
    .line 2200478
    new-instance v0, LX/F6V;

    invoke-direct {v0, p0}, LX/F6V;-><init>(Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;)V

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2200526
    iget-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->s:LX/9Tl;

    .line 2200527
    iget-object v1, v0, LX/9Tl;->a:LX/0Zb;

    sget-object v2, LX/9Tm;->ADD_CONTACTPOINT_FLOW_ENTER:LX/9Tm;

    invoke-virtual {v2}, LX/9Tm;->getAnalyticsName()Ljava/lang/String;

    move-result-object v2

    const/4 p0, 0x1

    invoke-interface {v1, v2, p0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2200528
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2200529
    const-string v2, "growth"

    invoke-virtual {v1, v2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2200530
    const-string v2, "launch_point"

    invoke-virtual {v1, v2, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2200531
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2200532
    :cond_0
    return-void
.end method

.method public static c(Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2200561
    iget-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->s:LX/9Tl;

    .line 2200562
    iget-object v1, v0, LX/9Tl;->a:LX/0Zb;

    sget-object v2, LX/9Tm;->ADD_CONTACTPOINT_COUNTRY_SELECTED:LX/9Tm;

    invoke-virtual {v2}, LX/9Tm;->getAnalyticsName()Ljava/lang/String;

    move-result-object v2

    const/4 p0, 0x1

    invoke-interface {v1, v2, p0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2200563
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2200564
    const-string v2, "growth"

    invoke-virtual {v1, v2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2200565
    const-string v2, "country_selected"

    invoke-virtual {v1, v2, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2200566
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2200567
    :cond_0
    return-void
.end method

.method private m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2200525
    invoke-virtual {p0}, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0833cd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2200524
    invoke-virtual {p0}, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0833ce

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private o()V
    .locals 3

    .prologue
    .line 2200520
    iget-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->q:LX/F9m;

    invoke-virtual {v0}, LX/F9m;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->z:Ljava/lang/String;

    .line 2200521
    iget-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->z:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2200522
    iget-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->w:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->v:LX/3fx;

    iget-object v2, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->z:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/3fx;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2200523
    :cond_0
    return-void
.end method

.method private p()LX/6oO;
    .locals 1

    .prologue
    .line 2200519
    new-instance v0, LX/F6W;

    invoke-direct {v0, p0}, LX/F6W;-><init>(Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;)V

    return-object v0
.end method

.method private q()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2200518
    new-instance v0, LX/F6Y;

    invoke-direct {v0, p0}, LX/F6Y;-><init>(Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;)V

    return-object v0
.end method

.method public static r(Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;)V
    .locals 3

    .prologue
    .line 2200512
    iget-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->s:LX/9Tl;

    .line 2200513
    iget-object v1, v0, LX/9Tl;->a:LX/0Zb;

    sget-object v2, LX/9Tm;->ADD_CONTACTPOINT_SUCCESS:LX/9Tm;

    invoke-virtual {v2}, LX/9Tm;->getAnalyticsName()Ljava/lang/String;

    move-result-object v2

    const/4 p0, 0x1

    invoke-interface {v1, v2, p0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2200514
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2200515
    const-string v2, "growth"

    invoke-virtual {v1, v2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2200516
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2200517
    :cond_0
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2200479
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2200480
    invoke-static {p0, p0}, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2200481
    const v0, 0x7f03009f

    invoke-virtual {p0, v0}, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->setContentView(I)V

    .line 2200482
    invoke-virtual {p0}, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2200483
    invoke-virtual {p0}, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "launch_point"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->A:Ljava/lang/String;

    .line 2200484
    :cond_0
    iget-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->A:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2200485
    const-string v0, "quick_promotion_phone_acquisition"

    iput-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->A:Ljava/lang/String;

    .line 2200486
    :cond_1
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2200487
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2200488
    const v1, 0x7f0833cc

    move v1, v1

    .line 2200489
    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 2200490
    invoke-direct {p0}, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->m()Ljava/lang/String;

    move-result-object v1

    .line 2200491
    const v0, 0x7f0d04a5

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2200492
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2200493
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2200494
    invoke-direct {p0}, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->n()Ljava/lang/String;

    move-result-object v1

    .line 2200495
    const v0, 0x7f0d04a6

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->w:Landroid/widget/EditText;

    .line 2200496
    iget-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->w:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2200497
    iget-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->w:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2200498
    iget-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->w:Landroid/widget/EditText;

    .line 2200499
    const/4 v1, 0x3

    move v1, v1

    .line 2200500
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 2200501
    iget-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->w:Landroid/widget/EditText;

    invoke-direct {p0}, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->p()LX/6oO;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2200502
    const v0, 0x7f0d04a7

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/countryspinner/CountrySpinner;

    iput-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->x:Lcom/facebook/widget/countryspinner/CountrySpinner;

    .line 2200503
    iget-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->x:Lcom/facebook/widget/countryspinner/CountrySpinner;

    invoke-direct {p0}, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->b()Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/countryspinner/CountrySpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 2200504
    const v0, 0x7f0d04a8

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->y:Landroid/widget/Button;

    .line 2200505
    iget-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->y:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->q()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2200506
    iget-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->y:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2200507
    iget-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->y:Landroid/widget/Button;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setAlpha(F)V

    .line 2200508
    invoke-direct {p0}, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->o()V

    .line 2200509
    iget-object v0, p0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->A:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->b(Ljava/lang/String;)V

    .line 2200510
    invoke-virtual {p0}, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 2200511
    return-void
.end method
