.class public Lcom/facebook/loyalty/view/fragment/LoyaltyHomeFragment;
.super Lcom/facebook/fbreact/fragment/FbReactFragment;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2411901
    invoke-direct {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 2411906
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2411907
    const-class v0, Lcom/facebook/loyalty/view/fragment/LoyaltyHomeFragment;

    .line 2411908
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, p0, v1}, LX/0QA;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 2411909
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2411910
    const-string v1, "route_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 2411911
    invoke-super {p0, p1}, Lcom/facebook/fbreact/fragment/FbReactFragment;->a(Landroid/os/Bundle;)V

    .line 2411912
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x696dbebb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2411904
    invoke-super {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->onPause()V

    .line 2411905
    const/16 v1, 0x2b

    const v2, 0xe85ee1a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x36324ab9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2411902
    invoke-super {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->onResume()V

    .line 2411903
    const/16 v1, 0x2b

    const v2, 0x17392cdd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
