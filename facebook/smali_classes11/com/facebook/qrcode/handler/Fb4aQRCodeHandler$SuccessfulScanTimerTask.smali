.class public final Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$SuccessfulScanTimerTask;
.super Ljava/util/TimerTask;
.source ""


# instance fields
.field public final synthetic a:LX/FUe;

.field private b:J

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/FUe;J)V
    .locals 0

    .prologue
    .line 2249434
    iput-object p1, p0, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$SuccessfulScanTimerTask;->a:LX/FUe;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 2249435
    iput-wide p2, p0, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$SuccessfulScanTimerTask;->b:J

    .line 2249436
    return-void
.end method

.method public constructor <init>(LX/FUe;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2249437
    iput-object p1, p0, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$SuccessfulScanTimerTask;->a:LX/FUe;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 2249438
    iput-object p2, p0, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$SuccessfulScanTimerTask;->c:Ljava/lang/String;

    .line 2249439
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 2249440
    iget-object v0, p0, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$SuccessfulScanTimerTask;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2249441
    iget-object v0, p0, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$SuccessfulScanTimerTask;->a:LX/FUe;

    iget-object v1, p0, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$SuccessfulScanTimerTask;->c:Ljava/lang/String;

    .line 2249442
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2249443
    const-string v3, "timeline_friend_request_ref"

    sget-object p0, LX/5P2;->QR_CODE:LX/5P2;

    invoke-virtual {v2, v3, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2249444
    iget-object v3, v0, LX/FUe;->e:LX/17W;

    iget-object p0, v0, LX/FUe;->a:Landroid/content/Context;

    invoke-virtual {v3, p0, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2249445
    :goto_0
    return-void

    .line 2249446
    :cond_0
    iget-object v0, p0, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$SuccessfulScanTimerTask;->a:LX/FUe;

    iget-wide v2, p0, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$SuccessfulScanTimerTask;->b:J

    .line 2249447
    sget-object v1, LX/0ax;->bE:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2249448
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2249449
    const-string v5, "timeline_friend_request_ref"

    sget-object v6, LX/5P2;->QR_CODE:LX/5P2;

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2249450
    iget-object v5, v0, LX/FUe;->e:LX/17W;

    iget-object v6, v0, LX/FUe;->a:Landroid/content/Context;

    invoke-virtual {v5, v6, v1, v4}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2249451
    goto :goto_0
.end method
