.class public final Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$ViewLinkActionTimerTask;
.super Ljava/util/TimerTask;
.source ""


# instance fields
.field public final synthetic a:LX/FUe;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/FUe;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2249452
    iput-object p1, p0, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$ViewLinkActionTimerTask;->a:LX/FUe;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 2249453
    iput-object p2, p0, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$ViewLinkActionTimerTask;->b:Ljava/lang/String;

    .line 2249454
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2249455
    iget-object v0, p0, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$ViewLinkActionTimerTask;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2249456
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2249457
    :try_start_0
    const-string v2, "fb"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v0}, LX/1H1;->c(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2249458
    :cond_0
    iget-object v0, p0, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$ViewLinkActionTimerTask;->a:LX/FUe;

    iget-object v0, v0, LX/FUe;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$ViewLinkActionTimerTask;->a:LX/FUe;

    iget-object v2, v2, LX/FUe;->a:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2249459
    :goto_0
    return-void

    .line 2249460
    :cond_1
    iget-object v0, p0, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$ViewLinkActionTimerTask;->a:LX/FUe;

    iget-object v0, v0, LX/FUe;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$ViewLinkActionTimerTask;->a:LX/FUe;

    iget-object v2, v2, LX/FUe;->a:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2249461
    :catch_0
    move-exception v0

    .line 2249462
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "Error in opening the link!"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
