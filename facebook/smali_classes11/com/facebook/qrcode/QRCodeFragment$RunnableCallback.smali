.class public final Lcom/facebook/qrcode/QRCodeFragment$RunnableCallback;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public a:LX/FUH;

.field public b:LX/FUT;

.field public c:Ljava/lang/Exception;


# direct methods
.method public constructor <init>(LX/FUH;LX/FUT;)V
    .locals 1

    .prologue
    .line 2248322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2248323
    iput-object p1, p0, Lcom/facebook/qrcode/QRCodeFragment$RunnableCallback;->a:LX/FUH;

    .line 2248324
    iput-object p2, p0, Lcom/facebook/qrcode/QRCodeFragment$RunnableCallback;->b:LX/FUT;

    .line 2248325
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment$RunnableCallback;->c:Ljava/lang/Exception;

    .line 2248326
    return-void
.end method

.method public constructor <init>(LX/FUH;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 2248310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2248311
    iput-object p1, p0, Lcom/facebook/qrcode/QRCodeFragment$RunnableCallback;->a:LX/FUH;

    .line 2248312
    sget-object v0, LX/FUT;->EXCEPTION:LX/FUT;

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment$RunnableCallback;->b:LX/FUT;

    .line 2248313
    iput-object p2, p0, Lcom/facebook/qrcode/QRCodeFragment$RunnableCallback;->c:Ljava/lang/Exception;

    .line 2248314
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 2248315
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment$RunnableCallback;->b:LX/FUT;

    sget-object v1, LX/FUT;->SUCCESS:LX/FUT;

    if-ne v0, v1, :cond_1

    .line 2248316
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment$RunnableCallback;->a:LX/FUH;

    invoke-interface {v0}, LX/FUH;->a()V

    .line 2248317
    :cond_0
    :goto_0
    return-void

    .line 2248318
    :cond_1
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment$RunnableCallback;->b:LX/FUT;

    sget-object v1, LX/FUT;->ERROR:LX/FUT;

    if-ne v0, v1, :cond_2

    .line 2248319
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment$RunnableCallback;->a:LX/FUH;

    invoke-interface {v0}, LX/FUH;->b()V

    goto :goto_0

    .line 2248320
    :cond_2
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment$RunnableCallback;->b:LX/FUT;

    sget-object v1, LX/FUT;->EXCEPTION:LX/FUT;

    if-ne v0, v1, :cond_0

    .line 2248321
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment$RunnableCallback;->a:LX/FUH;

    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment$RunnableCallback;->c:Ljava/lang/Exception;

    invoke-interface {v0, v1}, LX/FUH;->a(Ljava/lang/Exception;)V

    goto :goto_0
.end method
