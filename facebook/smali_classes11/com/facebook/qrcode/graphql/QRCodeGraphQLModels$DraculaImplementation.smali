.class public final Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2248983
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2248984
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2248985
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2248986
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2248987
    if-nez p1, :cond_0

    move v0, v1

    .line 2248988
    :goto_0
    return v0

    .line 2248989
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2248990
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2248991
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2248992
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2248993
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2248994
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2248995
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2248996
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2248997
    const v2, 0x7259f6de

    const/4 v5, 0x0

    .line 2248998
    if-nez v0, :cond_1

    move v4, v5

    .line 2248999
    :goto_1
    move v0, v4

    .line 2249000
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2249001
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2249002
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2249003
    :sswitch_2
    const-class v0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;

    .line 2249004
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2249005
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2249006
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2249007
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2249008
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2249009
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2249010
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2249011
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2249012
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2249013
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    .line 2249014
    if-nez p1, :cond_2

    const/4 v4, 0x0

    .line 2249015
    :goto_2
    if-ge v5, p1, :cond_3

    .line 2249016
    invoke-virtual {p0, v0, v5}, LX/15i;->q(II)I

    move-result p2

    .line 2249017
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v4, v5

    .line 2249018
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 2249019
    :cond_2
    new-array v4, p1, [I

    goto :goto_2

    .line 2249020
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v4, v5}, LX/186;->a([IZ)I

    move-result v4

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x6613c1a4 -> :sswitch_1
        -0x4e4ce2c1 -> :sswitch_3
        0x1f3d1eef -> :sswitch_0
        0x7259f6de -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2249045
    new-instance v0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2249021
    if-eqz p0, :cond_0

    .line 2249022
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2249023
    if-eq v0, p0, :cond_0

    .line 2249024
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2249025
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2249026
    sparse-switch p2, :sswitch_data_0

    .line 2249027
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2249028
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2249029
    const v1, 0x7259f6de

    .line 2249030
    if-eqz v0, :cond_0

    .line 2249031
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 2249032
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 2249033
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2249034
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2249035
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2249036
    :cond_0
    :goto_1
    :sswitch_1
    return-void

    .line 2249037
    :sswitch_2
    const-class v0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;

    .line 2249038
    invoke-static {v0, p3}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6613c1a4 -> :sswitch_0
        -0x4e4ce2c1 -> :sswitch_1
        0x1f3d1eef -> :sswitch_1
        0x7259f6de -> :sswitch_2
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2249039
    if-eqz p1, :cond_0

    .line 2249040
    invoke-static {p0, p1, p2}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 2249041
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$DraculaImplementation;

    .line 2249042
    if-eq v0, v1, :cond_0

    .line 2249043
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2249044
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2248980
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2248981
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2248982
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2248975
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2248976
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2248977
    :cond_0
    iput-object p1, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2248978
    iput p2, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$DraculaImplementation;->b:I

    .line 2248979
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2248974
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2248973
    new-instance v0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2248970
    iget v0, p0, LX/1vt;->c:I

    .line 2248971
    move v0, v0

    .line 2248972
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2248967
    iget v0, p0, LX/1vt;->c:I

    .line 2248968
    move v0, v0

    .line 2248969
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2248964
    iget v0, p0, LX/1vt;->b:I

    .line 2248965
    move v0, v0

    .line 2248966
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2248961
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2248962
    move-object v0, v0

    .line 2248963
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2248952
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2248953
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2248954
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2248955
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2248956
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2248957
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2248958
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2248959
    invoke-static {v3, v9, v2}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2248960
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2248949
    iget v0, p0, LX/1vt;->c:I

    .line 2248950
    move v0, v0

    .line 2248951
    return v0
.end method
