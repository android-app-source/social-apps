.class public final Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x31698e41
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2248948
    const-class v0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2248947
    const-class v0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2248945
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2248946
    return-void
.end method

.method private a()Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFbqrcode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2248929
    iget-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel;->e:Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel;

    iput-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel;->e:Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel;

    .line 2248930
    iget-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel;->e:Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2248939
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2248940
    invoke-direct {p0}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel;->a()Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2248941
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2248942
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2248943
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2248944
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2248931
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2248932
    invoke-direct {p0}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel;->a()Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2248933
    invoke-direct {p0}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel;->a()Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel;

    .line 2248934
    invoke-direct {p0}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel;->a()Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2248935
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel;

    .line 2248936
    iput-object v0, v1, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel;->e:Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel;

    .line 2248937
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2248938
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2248926
    new-instance v0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel;

    invoke-direct {v0}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel;-><init>()V

    .line 2248927
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2248928
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2248925
    const v0, 0x214eb59a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2248924
    const v0, -0x42f95d9d

    return v0
.end method
