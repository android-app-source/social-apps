.class public final Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7f72a4d8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Lcom/facebook/graphql/enums/GraphQLQRCodeStyleType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/enums/GraphQLQRCodeType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2249112
    const-class v0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2249111
    const-class v0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2249109
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2249110
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2249107
    iget-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    .line 2249108
    iget-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2249105
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2249106
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLQRCodeStyleType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2249103
    iget-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->h:Lcom/facebook/graphql/enums/GraphQLQRCodeStyleType;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLQRCodeStyleType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLQRCodeStyleType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLQRCodeStyleType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLQRCodeStyleType;

    iput-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->h:Lcom/facebook/graphql/enums/GraphQLQRCodeStyleType;

    .line 2249104
    iget-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->h:Lcom/facebook/graphql/enums/GraphQLQRCodeStyleType;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLQRCodeType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2249101
    iget-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->i:Lcom/facebook/graphql/enums/GraphQLQRCodeType;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLQRCodeType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLQRCodeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLQRCodeType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLQRCodeType;

    iput-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->i:Lcom/facebook/graphql/enums/GraphQLQRCodeType;

    .line 2249102
    iget-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->i:Lcom/facebook/graphql/enums/GraphQLQRCodeType;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2249113
    iget-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    .line 2249114
    iget-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 2249086
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2249087
    invoke-direct {p0}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2249088
    invoke-direct {p0}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x4e4ce2c1

    invoke-static {v2, v1, v3}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2249089
    invoke-direct {p0}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->l()Lcom/facebook/graphql/enums/GraphQLQRCodeStyleType;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 2249090
    invoke-direct {p0}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->m()Lcom/facebook/graphql/enums/GraphQLQRCodeType;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 2249091
    invoke-direct {p0}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2249092
    const/4 v5, 0x6

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2249093
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 2249094
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2249095
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2249096
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2249097
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2249098
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2249099
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2249100
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2249076
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2249077
    invoke-direct {p0}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2249078
    invoke-direct {p0}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x4e4ce2c1

    invoke-static {v2, v0, v3}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2249079
    invoke-direct {p0}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2249080
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;

    .line 2249081
    iput v3, v0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->f:I

    .line 2249082
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2249083
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 2249084
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 2249085
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2249075
    invoke-direct {p0}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2249066
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2249067
    const/4 v0, 0x1

    const v1, -0x4e4ce2c1

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->f:I

    .line 2249068
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;->g:Z

    .line 2249069
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2249072
    new-instance v0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$FetchQRCodesQueryModel$AllQrcodesModel$EdgesModel$NodeModel;-><init>()V

    .line 2249073
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2249074
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2249071
    const v0, -0x96966dd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2249070
    const v0, -0x66364096

    return v0
.end method
