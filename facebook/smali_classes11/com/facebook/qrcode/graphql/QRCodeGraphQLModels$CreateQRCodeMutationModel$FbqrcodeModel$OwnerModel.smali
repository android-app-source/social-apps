.class public final Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel$OwnerModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6c970a70
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel$OwnerModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel$OwnerModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2248830
    const-class v0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel$OwnerModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2248833
    const-class v0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel$OwnerModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2248831
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2248832
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2248827
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel$OwnerModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2248828
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel$OwnerModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2248829
    :cond_0
    iget-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel$OwnerModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2248825
    iget-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel$OwnerModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel$OwnerModel;->f:Ljava/lang/String;

    .line 2248826
    iget-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel$OwnerModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2248823
    iget-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel$OwnerModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel$OwnerModel;->g:Ljava/lang/String;

    .line 2248824
    iget-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel$OwnerModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2248821
    iget-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel$OwnerModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel$OwnerModel;->h:Ljava/lang/String;

    .line 2248822
    iget-object v0, p0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel$OwnerModel;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2248834
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2248835
    invoke-direct {p0}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel$OwnerModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2248836
    invoke-direct {p0}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel$OwnerModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2248837
    invoke-direct {p0}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel$OwnerModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2248838
    invoke-direct {p0}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel$OwnerModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2248839
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2248840
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2248841
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2248842
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2248843
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2248844
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2248845
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2248808
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2248809
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2248810
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2248811
    new-instance v0, LX/FUY;

    invoke-direct {v0, p1}, LX/FUY;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2248812
    invoke-direct {p0}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel$OwnerModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2248813
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2248814
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2248815
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2248818
    new-instance v0, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel$OwnerModel;

    invoke-direct {v0}, Lcom/facebook/qrcode/graphql/QRCodeGraphQLModels$CreateQRCodeMutationModel$FbqrcodeModel$OwnerModel;-><init>()V

    .line 2248819
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2248820
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2248816
    const v0, 0x4910c52a    # 592978.6f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2248817
    const v0, 0x3c2b9d5

    return v0
.end method
