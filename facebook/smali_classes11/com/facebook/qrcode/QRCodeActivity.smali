.class public Lcom/facebook/qrcode/QRCodeActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/FUg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2248044
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/qrcode/QRCodeActivity;LX/FUg;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/qrcode/QRCodeActivity;",
            "LX/FUg;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2248045
    iput-object p1, p0, Lcom/facebook/qrcode/QRCodeActivity;->p:LX/FUg;

    iput-object p2, p0, Lcom/facebook/qrcode/QRCodeActivity;->q:LX/0Or;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/qrcode/QRCodeActivity;

    invoke-static {v1}, LX/FUg;->a(LX/0QB;)LX/FUg;

    move-result-object v0

    check-cast v0, LX/FUg;

    const/16 v2, 0x122d

    invoke-static {v1, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/qrcode/QRCodeActivity;->a(Lcom/facebook/qrcode/QRCodeActivity;LX/FUg;LX/0Or;)V

    return-void
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 2248046
    const-string v0, "1785947001643369"

    invoke-static {v0}, LX/7Fv;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2248047
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeActivity;->q:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    const-string v1, "1785947001643369"

    .line 2248048
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 2248049
    move-object v0, v0

    .line 2248050
    invoke-virtual {v0, p0}, LX/0gt;->b(Landroid/content/Context;)V

    .line 2248051
    const/4 v0, 0x1

    .line 2248052
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/facebook/qrcode/QRCodeActivity;)Z
    .locals 1

    .prologue
    .line 2248053
    invoke-direct {p0}, Lcom/facebook/qrcode/QRCodeActivity;->a()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2248054
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2248055
    invoke-static {p0, p0}, Lcom/facebook/qrcode/QRCodeActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2248056
    const v0, 0x7f0310b7

    invoke-virtual {p0, v0}, Lcom/facebook/qrcode/QRCodeActivity;->setContentView(I)V

    .line 2248057
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2248058
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/qrcode/QRCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2248059
    const v1, 0x7f0832ee

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 2248060
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0h5;->setHasBackButton(Z)V

    .line 2248061
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0h5;->setCustomTitleView(Landroid/view/View;)V

    .line 2248062
    invoke-interface {p0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    .line 2248063
    const v2, 0x7f0d002f

    invoke-virtual {v1, v2}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 2248064
    new-instance v3, LX/FUG;

    invoke-direct {v3, p0, v1}, LX/FUG;-><init>(Lcom/facebook/qrcode/QRCodeActivity;LX/0gc;)V

    invoke-interface {v0, v3}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2248065
    if-nez v2, :cond_0

    .line 2248066
    invoke-virtual {p0}, Lcom/facebook/qrcode/QRCodeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "source"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2248067
    invoke-static {v0}, LX/FUk;->fromString(Ljava/lang/String;)LX/FUk;

    move-result-object v0

    .line 2248068
    new-instance v2, Lcom/facebook/qrcode/QRCodeFragment;

    invoke-direct {v2}, Lcom/facebook/qrcode/QRCodeFragment;-><init>()V

    .line 2248069
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2248070
    const-string v4, "source"

    invoke-virtual {v0}, LX/FUk;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, v4, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2248071
    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2248072
    move-object v0, v2

    .line 2248073
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2248074
    :cond_0
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeActivity;->q:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    const-string v1, "1785947001643369"

    .line 2248075
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 2248076
    move-object v0, v0

    .line 2248077
    invoke-virtual {v0}, LX/0gt;->b()V

    .line 2248078
    return-void
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 2248079
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeActivity;->p:LX/FUg;

    .line 2248080
    const-string v1, "END_FROM_BACK"

    iput-object v1, v0, LX/FUg;->d:Ljava/lang/String;

    .line 2248081
    invoke-direct {p0}, Lcom/facebook/qrcode/QRCodeActivity;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2248082
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2248083
    :cond_0
    return-void
.end method
