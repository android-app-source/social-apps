.class public Lcom/facebook/qrcode/QRCodeFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/6HO;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final B:Lcom/facebook/common/callercontext/CallerContext;

.field private static final C:Landroid/graphics/Point;

.field private static final D:[Ljava/lang/String;

.field public static final E:[Ljava/lang/String;

.field public static final F:[Ljava/lang/String;


# instance fields
.field public A:LX/2SX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private G:LX/FUU;

.field private H:LX/6IG;

.field private I:Landroid/view/View;

.field public J:Lcom/facebook/drawee/view/DraweeView;

.field public K:Landroid/graphics/Bitmap;

.field private L:Landroid/graphics/drawable/Drawable;

.field public M:LX/FUV;

.field private N:LX/1aZ;

.field private O:LX/1aZ;

.field public P:LX/6HU;

.field private Q:LX/6HV;

.field private R:Landroid/widget/RelativeLayout;

.field private S:Landroid/view/View;

.field private T:Lcom/facebook/camera/views/RotateLayout;

.field private final U:LX/FUL;

.field public V:Lcom/facebook/fbui/glyph/GlyphButton;

.field public W:Landroid/view/View;

.field public X:Landroid/view/View;

.field public Y:Lcom/facebook/resources/ui/FbButton;

.field public Z:Lcom/facebook/resources/ui/FbButton;

.field public a:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public aa:Landroid/view/View;

.field public ab:Ljava/util/TimerTask;

.field private ac:LX/0i5;

.field private ad:J

.field public b:LX/2Qx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/6Hj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Zr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/2Ib;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation runtime Lcom/facebook/analytics/impression/NewImpressionId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/FUX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/FUe;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/FUW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/FUf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/FUg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/1Er;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:Landroid/os/Vibrator;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/FUm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2248414
    const-class v0, Lcom/facebook/qrcode/QRCodeFragment;

    const-string v1, "qr_code"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/qrcode/QRCodeFragment;->B:Lcom/facebook/common/callercontext/CallerContext;

    .line 2248415
    new-instance v0, Landroid/graphics/Point;

    const/4 v1, 0x4

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/facebook/qrcode/QRCodeFragment;->C:Landroid/graphics/Point;

    .line 2248416
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "android.permission.CAMERA"

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/qrcode/QRCodeFragment;->D:[Ljava/lang/String;

    .line 2248417
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/qrcode/QRCodeFragment;->E:[Ljava/lang/String;

    .line 2248418
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/qrcode/QRCodeFragment;->F:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2248419
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2248420
    sget-object v0, LX/6IG;->PORTRAIT:LX/6IG;

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->H:LX/6IG;

    .line 2248421
    sget-object v0, LX/FUV;->STANDARD:LX/FUV;

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->M:LX/FUV;

    .line 2248422
    new-instance v0, LX/FUL;

    invoke-direct {v0, p0}, LX/FUL;-><init>(Lcom/facebook/qrcode/QRCodeFragment;)V

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->U:LX/FUL;

    .line 2248423
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->ad:J

    .line 2248424
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2248425
    const v0, 0x7f0d27b8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->I:Landroid/view/View;

    .line 2248426
    const v0, 0x7f0d27ba

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/DraweeView;

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->J:Lcom/facebook/drawee/view/DraweeView;

    .line 2248427
    const v0, 0x7f0d0681

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->R:Landroid/widget/RelativeLayout;

    .line 2248428
    const v0, 0x7f0d27be

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->S:Landroid/view/View;

    .line 2248429
    const v0, 0x7f0d11d9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/camera/views/RotateLayout;

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->T:Lcom/facebook/camera/views/RotateLayout;

    .line 2248430
    const v0, 0x7f0d27b7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->W:Landroid/view/View;

    .line 2248431
    const v0, 0x7f0d27b6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->X:Landroid/view/View;

    .line 2248432
    const v0, 0x7f0d27bb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->Y:Lcom/facebook/resources/ui/FbButton;

    .line 2248433
    const v0, 0x7f0d27bc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->Z:Lcom/facebook/resources/ui/FbButton;

    .line 2248434
    const v0, 0x7f0d16fc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->aa:Landroid/view/View;

    .line 2248435
    const v0, 0x7f0d27c0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->V:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 2248436
    const v0, 0x7f0d27b9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2248437
    const v1, 0x7f0d27bf

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2248438
    const v2, 0x7f0d27b5

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 2248439
    const p1, 0x7f0832ec

    move v2, p1

    .line 2248440
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 2248441
    const v2, 0x7f0832e9

    move v0, v2

    .line 2248442
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 2248443
    invoke-static {p0}, Lcom/facebook/qrcode/QRCodeFragment;->p(Lcom/facebook/qrcode/QRCodeFragment;)V

    .line 2248444
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->L:Landroid/graphics/drawable/Drawable;

    .line 2248445
    iput-object v1, v0, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2248446
    move-object v0, v0

    .line 2248447
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2248448
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->J:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2248449
    return-void
.end method

.method private static a(Lcom/facebook/qrcode/QRCodeFragment;LX/0Sh;LX/2Qx;LX/6Hj;LX/0SG;Landroid/content/Context;Ljava/util/concurrent/ExecutorService;LX/03V;LX/0Zr;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/lang/String;LX/2Ib;Ljava/lang/String;LX/1Ad;LX/FUX;LX/FUe;LX/FUW;LX/FUf;LX/FUg;Lcom/facebook/content/SecureContextHelper;LX/1Ck;LX/1Er;LX/0kL;Landroid/os/Vibrator;LX/FUm;LX/0i4;LX/0Or;LX/2SX;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/qrcode/QRCodeFragment;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/2Qx;",
            "LX/6Hj;",
            "LX/0SG;",
            "Landroid/content/Context;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Zr;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Ljava/lang/String;",
            "LX/2Ib;",
            "Ljava/lang/String;",
            "LX/1Ad;",
            "Lcom/facebook/qrcode/config/QRCodeConfig;",
            "Lcom/facebook/qrcode/handler/QRCodeHandler;",
            "LX/FUW;",
            "LX/FUf;",
            "LX/FUg;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/1Ck;",
            "LX/1Er;",
            "LX/0kL;",
            "Landroid/os/Vibrator;",
            "LX/FUm;",
            "LX/0i4;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/2SX;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2248450
    iput-object p1, p0, Lcom/facebook/qrcode/QRCodeFragment;->a:LX/0Sh;

    iput-object p2, p0, Lcom/facebook/qrcode/QRCodeFragment;->b:LX/2Qx;

    iput-object p3, p0, Lcom/facebook/qrcode/QRCodeFragment;->c:LX/6Hj;

    iput-object p4, p0, Lcom/facebook/qrcode/QRCodeFragment;->d:LX/0SG;

    iput-object p5, p0, Lcom/facebook/qrcode/QRCodeFragment;->e:Landroid/content/Context;

    iput-object p6, p0, Lcom/facebook/qrcode/QRCodeFragment;->f:Ljava/util/concurrent/ExecutorService;

    iput-object p7, p0, Lcom/facebook/qrcode/QRCodeFragment;->g:LX/03V;

    iput-object p8, p0, Lcom/facebook/qrcode/QRCodeFragment;->h:LX/0Zr;

    iput-object p9, p0, Lcom/facebook/qrcode/QRCodeFragment;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p10, p0, Lcom/facebook/qrcode/QRCodeFragment;->j:Ljava/lang/String;

    iput-object p11, p0, Lcom/facebook/qrcode/QRCodeFragment;->k:LX/2Ib;

    iput-object p12, p0, Lcom/facebook/qrcode/QRCodeFragment;->l:Ljava/lang/String;

    iput-object p13, p0, Lcom/facebook/qrcode/QRCodeFragment;->m:LX/1Ad;

    iput-object p14, p0, Lcom/facebook/qrcode/QRCodeFragment;->n:LX/FUX;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->o:LX/FUe;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->p:LX/FUW;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->r:LX/FUg;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->s:Lcom/facebook/content/SecureContextHelper;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->t:LX/1Ck;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->u:LX/1Er;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->v:LX/0kL;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->w:Landroid/os/Vibrator;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->x:LX/FUm;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->y:LX/0i4;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->z:LX/0Or;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->A:LX/2SX;

    return-void
.end method

.method public static a(Lcom/facebook/qrcode/QRCodeFragment;Ljava/io/File;LX/FUH;)V
    .locals 3

    .prologue
    .line 2248451
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->M:LX/FUV;

    sget-object v1, LX/FUV;->STANDARD:LX/FUV;

    if-ne v0, v1, :cond_0

    .line 2248452
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->f:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/qrcode/QRCodeFragment$14;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/qrcode/QRCodeFragment$14;-><init>(Lcom/facebook/qrcode/QRCodeFragment;Ljava/io/File;LX/FUH;)V

    const v2, 0x773de89f

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2248453
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 30

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v29

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/qrcode/QRCodeFragment;

    invoke-static/range {v29 .. v29}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-static/range {v29 .. v29}, LX/2Qx;->a(LX/0QB;)LX/2Qx;

    move-result-object v4

    check-cast v4, LX/2Qx;

    invoke-static/range {v29 .. v29}, LX/6Hj;->a(LX/0QB;)LX/6Hj;

    move-result-object v5

    check-cast v5, LX/6Hj;

    invoke-static/range {v29 .. v29}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    const-class v7, Landroid/content/Context;

    move-object/from16 v0, v29

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static/range {v29 .. v29}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {v29 .. v29}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static/range {v29 .. v29}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v10

    check-cast v10, LX/0Zr;

    invoke-static/range {v29 .. v29}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v11

    check-cast v11, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v29 .. v29}, LX/0dG;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-static/range {v29 .. v29}, LX/2Ib;->a(LX/0QB;)LX/2Ib;

    move-result-object v13

    check-cast v13, LX/2Ib;

    invoke-static/range {v29 .. v29}, LX/13q;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    invoke-static/range {v29 .. v29}, LX/1Ad;->a(LX/0QB;)LX/1Ad;

    move-result-object v15

    check-cast v15, LX/1Ad;

    invoke-static/range {v29 .. v29}, LX/FUX;->a(LX/0QB;)LX/FUX;

    move-result-object v16

    check-cast v16, LX/FUX;

    invoke-static/range {v29 .. v29}, LX/FUe;->a(LX/0QB;)LX/FUe;

    move-result-object v17

    check-cast v17, LX/FUe;

    invoke-static/range {v29 .. v29}, LX/FUW;->a(LX/0QB;)LX/FUW;

    move-result-object v18

    check-cast v18, LX/FUW;

    invoke-static/range {v29 .. v29}, LX/FUf;->a(LX/0QB;)LX/FUf;

    move-result-object v19

    check-cast v19, LX/FUf;

    invoke-static/range {v29 .. v29}, LX/FUg;->a(LX/0QB;)LX/FUg;

    move-result-object v20

    check-cast v20, LX/FUg;

    invoke-static/range {v29 .. v29}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v21

    check-cast v21, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v29 .. v29}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v22

    check-cast v22, LX/1Ck;

    invoke-static/range {v29 .. v29}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v23

    check-cast v23, LX/1Er;

    invoke-static/range {v29 .. v29}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v24

    check-cast v24, LX/0kL;

    invoke-static/range {v29 .. v29}, LX/3RR;->a(LX/0QB;)Landroid/os/Vibrator;

    move-result-object v25

    check-cast v25, Landroid/os/Vibrator;

    invoke-static/range {v29 .. v29}, LX/FUm;->a(LX/0QB;)LX/FUm;

    move-result-object v26

    check-cast v26, LX/FUm;

    const-class v27, LX/0i4;

    move-object/from16 v0, v29

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v27

    check-cast v27, LX/0i4;

    const/16 v28, 0x122d

    move-object/from16 v0, v29

    move/from16 v1, v28

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v28

    invoke-static/range {v29 .. v29}, LX/2SX;->a(LX/0QB;)LX/2SX;

    move-result-object v29

    check-cast v29, LX/2SX;

    invoke-static/range {v2 .. v29}, Lcom/facebook/qrcode/QRCodeFragment;->a(Lcom/facebook/qrcode/QRCodeFragment;LX/0Sh;LX/2Qx;LX/6Hj;LX/0SG;Landroid/content/Context;Ljava/util/concurrent/ExecutorService;LX/03V;LX/0Zr;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/lang/String;LX/2Ib;Ljava/lang/String;LX/1Ad;LX/FUX;LX/FUe;LX/FUW;LX/FUf;LX/FUg;Lcom/facebook/content/SecureContextHelper;LX/1Ck;LX/1Er;LX/0kL;Landroid/os/Vibrator;LX/FUm;LX/0i4;LX/0Or;LX/2SX;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/qrcode/QRCodeFragment;LX/FUU;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2248454
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->G:LX/FUU;

    .line 2248455
    if-ne v0, p1, :cond_0

    .line 2248456
    :goto_0
    return-void

    .line 2248457
    :cond_0
    iput-object p1, p0, Lcom/facebook/qrcode/QRCodeFragment;->G:LX/FUU;

    .line 2248458
    sget-object v1, LX/FUU;->SCAN:LX/FUU;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->P:LX/6HU;

    if-eqz v0, :cond_1

    .line 2248459
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->P:LX/6HU;

    invoke-virtual {v0}, LX/6HU;->l()V

    .line 2248460
    :cond_1
    sget-object v0, LX/FUU;->SHOW:LX/FUU;

    if-ne p1, v0, :cond_3

    .line 2248461
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->W:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 2248462
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->X:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 2248463
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->I:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2248464
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->R:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2248465
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    .line 2248466
    iget-object v1, v0, LX/FUf;->a:LX/0if;

    sget-object v2, LX/0ig;->H:LX/0ih;

    const-string v3, "ENTER_SHOW_CODE_MODE"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2248467
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->p:LX/FUW;

    .line 2248468
    const-string v1, "qrcode_code_rendered"

    invoke-static {v0, v1}, LX/FUW;->e(LX/FUW;Ljava/lang/String;)V

    .line 2248469
    :cond_2
    :goto_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/qrcode/QRCodeFragment;->d(Z)V

    goto :goto_0

    .line 2248470
    :cond_3
    sget-object v0, LX/FUU;->SCAN:LX/FUU;

    if-ne p1, v0, :cond_2

    .line 2248471
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->W:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 2248472
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->X:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 2248473
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->I:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2248474
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->R:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2248475
    sget-object v0, Lcom/facebook/qrcode/QRCodeFragment;->D:[Ljava/lang/String;

    new-instance v1, Lcom/facebook/qrcode/QRCodeFragment$9;

    invoke-direct {v1, p0}, Lcom/facebook/qrcode/QRCodeFragment$9;-><init>(Lcom/facebook/qrcode/QRCodeFragment;)V

    invoke-static {p0, p2, v0, v1}, Lcom/facebook/qrcode/QRCodeFragment;->a$redex0(Lcom/facebook/qrcode/QRCodeFragment;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 2248476
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    .line 2248477
    iget-object v1, v0, LX/FUf;->a:LX/0if;

    sget-object v2, LX/0ig;->H:LX/0ih;

    const-string v3, "ENTER_SCANNER_MODE"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2248478
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->p:LX/FUW;

    .line 2248479
    const-string v1, "qrcode_scanner_rendered"

    invoke-static {v0, v1}, LX/FUW;->e(LX/FUW;Ljava/lang/String;)V

    .line 2248480
    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/qrcode/QRCodeFragment;LX/FUV;)V
    .locals 2

    .prologue
    .line 2248481
    const/4 v0, 0x0

    .line 2248482
    sget-object v1, LX/FUV;->STANDARD:LX/FUV;

    if-ne p1, v1, :cond_2

    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->N:LX/1aZ;

    if-eqz v1, :cond_2

    .line 2248483
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->N:LX/1aZ;

    .line 2248484
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 2248485
    iput-object p1, p0, Lcom/facebook/qrcode/QRCodeFragment;->M:LX/FUV;

    .line 2248486
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->J:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2248487
    :cond_1
    return-void

    .line 2248488
    :cond_2
    sget-object v1, LX/FUV;->VANITY:LX/FUV;

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->O:LX/1aZ;

    if-eqz v1, :cond_0

    .line 2248489
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->O:LX/1aZ;

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/qrcode/QRCodeFragment;LX/G90;Z)V
    .locals 9

    .prologue
    .line 2248490
    iget-object v0, p1, LX/G90;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2248491
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->o:LX/FUe;

    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 2248492
    if-nez v0, :cond_4

    move-object v2, v3

    .line 2248493
    :goto_0
    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2248494
    iget-object v2, v1, LX/FUe;->b:LX/FUf;

    .line 2248495
    iget-object v4, v2, LX/FUf;->a:LX/0if;

    sget-object v5, LX/0ig;->H:LX/0ih;

    const-string v6, "BLANK_TEXT_DETECTED"

    invoke-virtual {v4, v5, v6}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2248496
    :goto_1
    move-object v1, v3

    .line 2248497
    iput-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->ab:Ljava/util/TimerTask;

    .line 2248498
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->ab:Ljava/util/TimerTask;

    if-nez v1, :cond_2

    .line 2248499
    if-eqz p2, :cond_1

    .line 2248500
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/facebook/qrcode/QRCodeFragment;->ad:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x1388

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 2248501
    :goto_2
    return-void

    .line 2248502
    :cond_0
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/qrcode/QRCodeFragment;->ad:J

    .line 2248503
    :cond_1
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v1, v1

    .line 2248504
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 2248505
    const v2, 0x7f0d002f

    .line 2248506
    new-instance v3, Lcom/facebook/qrcode/QRCodeTextFragment;

    invoke-direct {v3}, Lcom/facebook/qrcode/QRCodeTextFragment;-><init>()V

    .line 2248507
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2248508
    const-string v5, "text_content"

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2248509
    invoke-virtual {v3, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2248510
    move-object v0, v3

    .line 2248511
    invoke-virtual {v1, v2, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 2248512
    invoke-virtual {v1}, LX/0hH;->b()I

    goto :goto_2

    .line 2248513
    :cond_2
    if-eqz p2, :cond_3

    .line 2248514
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->w:Landroid/os/Vibrator;

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    .line 2248515
    :cond_3
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->S:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 2248516
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 2248517
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->ab:Ljava/util/TimerTask;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 2248518
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->r:LX/FUg;

    .line 2248519
    const-string v1, "END_FROM_SUCCESSFUL_SCAN"

    iput-object v1, v0, LX/FUg;->d:Ljava/lang/String;

    .line 2248520
    goto :goto_2

    .line 2248521
    :cond_4
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 2248522
    :cond_5
    :try_start_0
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 2248523
    sget-object v5, Landroid/util/Patterns;->WEB_URL:Ljava/util/regex/Pattern;

    invoke-virtual {v5, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    .line 2248524
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->find()Z

    move-result v7

    if-eqz v7, :cond_b

    .line 2248525
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v5

    move-object v7, v5

    .line 2248526
    :goto_3
    if-eqz v8, :cond_8

    .line 2248527
    invoke-virtual {v8}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    .line 2248528
    invoke-static {v8}, LX/FUe;->a(Landroid/net/Uri;)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 2248529
    const/4 v4, 0x0

    invoke-static {v1, v8, v4}, LX/FUe;->a(LX/FUe;Landroid/net/Uri;Z)Ljava/util/TimerTask;

    move-result-object v3

    goto/16 :goto_1

    .line 2248530
    :cond_6
    invoke-static {v5}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_8

    .line 2248531
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v5, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    const/4 v5, -0x1

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result p1

    sparse-switch p1, :sswitch_data_0

    :cond_7
    move v4, v5

    :goto_4
    packed-switch v4, :pswitch_data_0

    .line 2248532
    if-nez v7, :cond_8

    .line 2248533
    iget-object v4, v1, LX/FUe;->b:LX/FUf;

    .line 2248534
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v5

    const-string v6, "uri"

    invoke-virtual {v5, v6, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v5

    .line 2248535
    iget-object v6, v4, LX/FUf;->a:LX/0if;

    sget-object v7, LX/0ig;->H:LX/0ih;

    const-string v8, "MAYBE_NATIVE_DEEP_LINK"

    const/4 p1, 0x0

    invoke-virtual {v6, v7, v8, p1, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2248536
    new-instance v4, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$ViewLinkActionTimerTask;

    invoke-direct {v4, v1, v2}, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$ViewLinkActionTimerTask;-><init>(LX/FUe;Ljava/lang/String;)V

    move-object v3, v4

    goto/16 :goto_1

    .line 2248537
    :sswitch_0
    const-string v6, "fb"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    goto :goto_4

    :sswitch_1
    const-string v4, "facebook"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    move v4, v6

    goto :goto_4

    :sswitch_2
    const-string v4, "fblogin"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    const/4 v4, 0x2

    goto :goto_4

    :sswitch_3
    const-string v4, "instagram"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    const/4 v4, 0x3

    goto :goto_4

    :sswitch_4
    const-string v4, "market"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    const/4 v4, 0x4

    goto :goto_4

    .line 2248538
    :pswitch_0
    iget-object v4, v1, LX/FUe;->b:LX/FUf;

    .line 2248539
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v5

    const-string v6, "uri"

    invoke-virtual {v5, v6, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v5

    .line 2248540
    iget-object v6, v4, LX/FUf;->a:LX/0if;

    sget-object v7, LX/0ig;->H:LX/0ih;

    const-string v8, "SUPPORTED_NATIVE_DEEP_LINK"

    const/4 p1, 0x0

    invoke-virtual {v6, v7, v8, p1, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2248541
    new-instance v4, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$ViewLinkActionTimerTask;

    invoke-direct {v4, v1, v2}, Lcom/facebook/qrcode/handler/Fb4aQRCodeHandler$ViewLinkActionTimerTask;-><init>(LX/FUe;Ljava/lang/String;)V

    move-object v3, v4

    goto/16 :goto_1

    .line 2248542
    :cond_8
    if-eqz v7, :cond_a

    .line 2248543
    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 2248544
    invoke-virtual {v4}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 2248545
    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "http"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    .line 2248546
    :cond_9
    invoke-static {v4}, LX/FUe;->a(Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 2248547
    const/4 v5, 0x1

    invoke-static {v1, v4, v5}, LX/FUe;->a(LX/FUe;Landroid/net/Uri;Z)Ljava/util/TimerTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto/16 :goto_1

    .line 2248548
    :catch_0
    move-exception v4

    .line 2248549
    iget-object v5, v1, LX/FUe;->b:LX/FUf;

    .line 2248550
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v6

    const-string v7, "msg"

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v6

    const-string v7, "text"

    invoke-virtual {v6, v7, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v6

    .line 2248551
    iget-object v7, v5, LX/FUf;->a:LX/0if;

    sget-object v8, LX/0ig;->H:LX/0ih;

    const-string p1, "EXCEPTION_ON_HANDLE_DECODED_STR"

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v8, p1, v1, v6}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2248552
    goto/16 :goto_1

    .line 2248553
    :cond_a
    iget-object v4, v1, LX/FUe;->b:LX/FUf;

    .line 2248554
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v5

    const-string v6, "uri"

    invoke-virtual {v5, v6, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v5

    .line 2248555
    iget-object v6, v4, LX/FUf;->a:LX/0if;

    sget-object v7, LX/0ig;->H:LX/0ih;

    const-string v8, "NON_FB_URL_DECODED_BLOCK"

    const/4 p1, 0x0

    invoke-virtual {v6, v7, v8, p1, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2248556
    goto/16 :goto_1

    :cond_b
    move-object v7, v3

    goto/16 :goto_3

    nop

    :sswitch_data_0
    .sparse-switch
        -0x40736bc4 -> :sswitch_4
        -0x3ee387d3 -> :sswitch_2
        0xcbc -> :sswitch_0
        0x1b907b2 -> :sswitch_3
        0x1da19ac6 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static a$redex0(Lcom/facebook/qrcode/QRCodeFragment;Landroid/graphics/Bitmap;Ljava/io/File;LX/FUH;)V
    .locals 4

    .prologue
    .line 2248557
    const/4 v1, 0x0

    .line 2248558
    if-nez p1, :cond_1

    .line 2248559
    :try_start_0
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2248560
    :catch_0
    move-exception v0

    .line 2248561
    :goto_0
    :try_start_1
    iget-object v2, p0, Lcom/facebook/qrcode/QRCodeFragment;->a:LX/0Sh;

    new-instance v3, Lcom/facebook/qrcode/QRCodeFragment$RunnableCallback;

    invoke-direct {v3, p3, v0}, Lcom/facebook/qrcode/QRCodeFragment$RunnableCallback;-><init>(LX/FUH;Ljava/lang/Exception;)V

    invoke-virtual {v2, v3}, LX/0Sh;->b(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2248562
    if-eqz v1, :cond_0

    .line 2248563
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 2248564
    :cond_0
    :goto_1
    return-void

    .line 2248565
    :cond_1
    :try_start_3
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2248566
    :try_start_4
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v1, 0x5a

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v0

    .line 2248567
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->a:LX/0Sh;

    new-instance v3, Lcom/facebook/qrcode/QRCodeFragment$RunnableCallback;

    if-eqz v0, :cond_2

    sget-object v0, LX/FUT;->SUCCESS:LX/FUT;

    :goto_2
    invoke-direct {v3, p3, v0}, Lcom/facebook/qrcode/QRCodeFragment$RunnableCallback;-><init>(LX/FUH;LX/FUT;)V

    invoke-virtual {v1, v3}, LX/0Sh;->b(Ljava/lang/Runnable;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2248568
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 2248569
    :catch_1
    goto :goto_1

    .line 2248570
    :cond_2
    :try_start_6
    sget-object v0, LX/FUT;->ERROR:LX/FUT;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_2

    .line 2248571
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_3

    .line 2248572
    :try_start_7
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    .line 2248573
    :cond_3
    :goto_4
    throw v0

    :catch_2
    goto :goto_1

    :catch_3
    goto :goto_4

    .line 2248574
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_3

    .line 2248575
    :catch_4
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/qrcode/QRCodeFragment;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 4
    .param p2    # [Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2248578
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    .line 2248579
    iget-object v1, v0, LX/FUf;->a:LX/0if;

    sget-object v2, LX/0ig;->H:LX/0ih;

    const-string v3, "CHECK_PERMS_START"

    invoke-virtual {v1, v2, v3, p1}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2248580
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->ac:LX/0i5;

    new-instance v1, LX/FUK;

    invoke-direct {v1, p0, p1, p3}, LX/FUK;-><init>(Lcom/facebook/qrcode/QRCodeFragment;Ljava/lang/String;Ljava/lang/Runnable;)V

    invoke-virtual {v0, p2, v1}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 2248581
    return-void
.end method

.method public static d(Lcom/facebook/qrcode/QRCodeFragment;I)V
    .locals 2

    .prologue
    .line 2248576
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->a:LX/0Sh;

    new-instance v1, Lcom/facebook/qrcode/QRCodeFragment$16;

    invoke-direct {v1, p0, p1}, Lcom/facebook/qrcode/QRCodeFragment$16;-><init>(Lcom/facebook/qrcode/QRCodeFragment;I)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 2248577
    return-void
.end method

.method private d(Z)V
    .locals 4

    .prologue
    .line 2248672
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 2248673
    if-eqz v0, :cond_0

    .line 2248674
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 2248675
    iget-object v2, p0, Lcom/facebook/qrcode/QRCodeFragment;->G:LX/FUU;

    sget-object v3, LX/FUU;->SHOW:LX/FUU;

    if-ne v2, v3, :cond_1

    if-eqz p1, :cond_1

    .line 2248676
    const v2, 0x3f333333    # 0.7f

    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    .line 2248677
    :goto_0
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 2248678
    :cond_0
    return-void

    .line 2248679
    :cond_1
    const/high16 v2, -0x40800000    # -1.0f

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    goto :goto_0
.end method

.method public static o(Lcom/facebook/qrcode/QRCodeFragment;)V
    .locals 10

    .prologue
    .line 2248658
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->P:LX/6HU;

    if-nez v0, :cond_0

    .line 2248659
    new-instance v0, LX/6HU;

    iget-object v2, p0, Lcom/facebook/qrcode/QRCodeFragment;->e:Landroid/content/Context;

    iget-object v3, p0, Lcom/facebook/qrcode/QRCodeFragment;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v4, p0, Lcom/facebook/qrcode/QRCodeFragment;->p:LX/FUW;

    iget-object v5, p0, Lcom/facebook/qrcode/QRCodeFragment;->k:LX/2Ib;

    iget-object v6, p0, Lcom/facebook/qrcode/QRCodeFragment;->c:LX/6Hj;

    iget-object v7, p0, Lcom/facebook/qrcode/QRCodeFragment;->a:LX/0Sh;

    iget-object v8, p0, Lcom/facebook/qrcode/QRCodeFragment;->h:LX/0Zr;

    iget-object v9, p0, Lcom/facebook/qrcode/QRCodeFragment;->g:LX/03V;

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, LX/6HU;-><init>(LX/6HO;Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/6HF;LX/2Ib;LX/6Hj;LX/0Sh;LX/0Zr;LX/03V;)V

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->P:LX/6HU;

    .line 2248660
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->P:LX/6HU;

    invoke-virtual {v0}, LX/6HU;->e()V

    .line 2248661
    :cond_0
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->P:LX/6HU;

    invoke-virtual {v0}, LX/6HU;->j()V

    .line 2248662
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->x:LX/FUm;

    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->U:LX/FUL;

    invoke-virtual {v0, v1}, LX/FUm;->a(LX/FUL;)V

    .line 2248663
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->S:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 2248664
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->ab:Ljava/util/TimerTask;

    .line 2248665
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->P:LX/6HU;

    .line 2248666
    iget-boolean v1, v0, LX/6HU;->T:Z

    move v0, v1

    .line 2248667
    if-eqz v0, :cond_1

    .line 2248668
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->V:Lcom/facebook/fbui/glyph/GlyphButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2248669
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->V:Lcom/facebook/fbui/glyph/GlyphButton;

    const v1, 0x7f02077e

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setImageResource(I)V

    .line 2248670
    :goto_0
    return-void

    .line 2248671
    :cond_1
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->V:Lcom/facebook/fbui/glyph/GlyphButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private static p(Lcom/facebook/qrcode/QRCodeFragment;)V
    .locals 14

    .prologue
    const/4 v3, 0x0

    .line 2248627
    :try_start_0
    const/4 v6, 0x0

    .line 2248628
    iget-object v4, p0, Lcom/facebook/qrcode/QRCodeFragment;->e:Landroid/content/Context;

    const-string v5, "www.%s"

    invoke-static {v4, v5}, LX/14Z;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2248629
    new-instance v5, Landroid/net/Uri$Builder;

    invoke-direct {v5}, Landroid/net/Uri$Builder;-><init>()V

    const-string v7, "https"

    invoke-virtual {v5, v7}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "qr"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "id"

    iget-object v7, p0, Lcom/facebook/qrcode/QRCodeFragment;->j:Ljava/lang/String;

    invoke-virtual {v4, v5, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2248630
    iget-object v5, p0, Lcom/facebook/qrcode/QRCodeFragment;->J:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v5}, Lcom/facebook/drawee/view/DraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 2248631
    iget v7, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v5, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v7, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 2248632
    new-instance v7, Ljava/util/EnumMap;

    const-class v8, LX/G8u;

    invoke-direct {v7, v8}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 2248633
    sget-object v8, LX/G8u;->ERROR_CORRECTION:LX/G8u;

    sget-object v9, LX/G9c;->L:LX/G9c;

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2248634
    new-instance v8, LX/G9O;

    invoke-direct {v8}, LX/G9O;-><init>()V

    .line 2248635
    sget-object v8, LX/G8o;->QR_CODE:LX/G8o;

    invoke-static {v4, v8, v5, v5, v7}, LX/G9O;->a(Ljava/lang/String;LX/G8o;IILjava/util/Map;)LX/G96;

    move-result-object v10

    .line 2248636
    iget v4, v10, LX/G96;->a:I

    move v7, v4

    .line 2248637
    iget v4, v10, LX/G96;->b:I

    move v11, v4

    .line 2248638
    mul-int v4, v7, v11

    new-array v5, v4, [I

    move v9, v6

    .line 2248639
    :goto_0
    if-ge v9, v11, :cond_2

    .line 2248640
    mul-int v12, v9, v7

    move v8, v6

    .line 2248641
    :goto_1
    if-ge v8, v7, :cond_1

    .line 2248642
    add-int v13, v12, v8

    invoke-virtual {v10, v8, v9}, LX/G96;->a(II)Z

    move-result v4

    if-eqz v4, :cond_0

    const/high16 v4, -0x1000000

    :goto_2
    aput v4, v5, v13

    .line 2248643
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_1

    .line 2248644
    :cond_0
    const/4 v4, -0x1

    goto :goto_2

    .line 2248645
    :cond_1
    add-int/lit8 v4, v9, 0x1

    move v9, v4

    goto :goto_0

    .line 2248646
    :cond_2
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v11, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    move v8, v6

    move v9, v6

    move v10, v7

    .line 2248647
    invoke-virtual/range {v4 .. v11}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 2248648
    move-object v0, v4

    .line 2248649
    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->K:Landroid/graphics/Bitmap;

    .line 2248650
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->K:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 2248651
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/qrcode/QRCodeFragment;->K:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->L:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch LX/G94; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 2248652
    :cond_3
    :goto_3
    iput-object v3, p0, Lcom/facebook/qrcode/QRCodeFragment;->N:LX/1aZ;

    .line 2248653
    iput-object v3, p0, Lcom/facebook/qrcode/QRCodeFragment;->O:LX/1aZ;

    .line 2248654
    sget-object v0, LX/FUV;->STANDARD:LX/FUV;

    invoke-static {p0, v0}, Lcom/facebook/qrcode/QRCodeFragment;->a$redex0(Lcom/facebook/qrcode/QRCodeFragment;LX/FUV;)V

    .line 2248655
    return-void

    .line 2248656
    :catch_0
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->v:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0832e0

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_3

    .line 2248657
    :catch_1
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->v:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0832e0

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_3
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2248622
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->T:Lcom/facebook/camera/views/RotateLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/camera/views/RotateLayout;->setVisibility(I)V

    .line 2248623
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->Q:LX/6HV;

    if-eqz v0, :cond_0

    .line 2248624
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->R:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->Q:LX/6HV;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 2248625
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->Q:LX/6HV;

    .line 2248626
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 2248621
    return-void
.end method

.method public final a(LX/6HV;)V
    .locals 2

    .prologue
    .line 2248618
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->R:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;I)V

    .line 2248619
    iput-object p1, p0, Lcom/facebook/qrcode/QRCodeFragment;->Q:LX/6HV;

    .line 2248620
    return-void
.end method

.method public final a(LX/6HX;)V
    .locals 0

    .prologue
    .line 2248343
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 2248617
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2248594
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2248595
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/qrcode/QRCodeFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2248596
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2248597
    const-string v1, "source"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/FUk;->fromString(Ljava/lang/String;)LX/FUk;

    move-result-object v0

    .line 2248598
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    .line 2248599
    iget-object v2, v1, LX/FUf;->a:LX/0if;

    sget-object v3, LX/0ig;->H:LX/0ih;

    const-string v4, "QR_FRAGMENT_CREATE"

    invoke-virtual {v2, v3, v4}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2248600
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->p:LX/FUW;

    iget-object v2, p0, Lcom/facebook/qrcode/QRCodeFragment;->l:Ljava/lang/String;

    .line 2248601
    iput-object v2, v1, LX/FUW;->b:Ljava/lang/String;

    .line 2248602
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->p:LX/FUW;

    iget-object v2, v0, LX/FUk;->value:Ljava/lang/String;

    .line 2248603
    const-string v3, "qrcode_started"

    invoke-static {v1, v3}, LX/FUW;->f(LX/FUW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2248604
    const-string v4, "flow_name"

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2248605
    invoke-static {v1, v3}, LX/FUW;->a(LX/FUW;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2248606
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->m:LX/1Ad;

    sget-object v2, Lcom/facebook/qrcode/QRCodeFragment;->B:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    .line 2248607
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->y:LX/0i4;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->ac:LX/0i5;

    .line 2248608
    if-nez p1, :cond_0

    .line 2248609
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->r:LX/FUg;

    invoke-virtual {v0}, LX/FUk;->name()Ljava/lang/String;

    move-result-object v0

    .line 2248610
    iput-object v0, v1, LX/FUg;->b:Ljava/lang/String;

    .line 2248611
    :goto_0
    return-void

    .line 2248612
    :cond_0
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->r:LX/FUg;

    .line 2248613
    const-string v1, "qrCodeSource"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/FUg;->b:Ljava/lang/String;

    .line 2248614
    const-string v1, "lifecycleEntryPoint"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/FUg;->c:Ljava/lang/String;

    .line 2248615
    const-string v1, "galleryImportRecentlyPressed"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, LX/FUg;->e:Z

    .line 2248616
    goto :goto_0
.end method

.method public final a(Ljava/util/List;Ljava/util/List;LX/6HR;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;",
            "LX/6HR;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2248411
    sget-object v0, Lcom/facebook/qrcode/QRCodeFragment;->C:Landroid/graphics/Point;

    invoke-static {p1, v0}, LX/6IF;->a(Ljava/util/List;Landroid/graphics/Point;)Landroid/hardware/Camera$Size;

    move-result-object v0

    iput-object v0, p3, LX/6HR;->a:Landroid/hardware/Camera$Size;

    .line 2248412
    sget-object v0, Lcom/facebook/qrcode/QRCodeFragment;->C:Landroid/graphics/Point;

    invoke-static {p2, v0}, LX/6IF;->a(Ljava/util/List;Landroid/graphics/Point;)Landroid/hardware/Camera$Size;

    move-result-object v0

    iput-object v0, p3, LX/6HR;->b:Landroid/hardware/Camera$Size;

    .line 2248413
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 2248582
    if-nez p1, :cond_0

    .line 2248583
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->v:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0832e1

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2248584
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->p:LX/FUW;

    .line 2248585
    const-string v1, "qrcode_camera_not_loaded"

    invoke-static {v0, v1}, LX/FUW;->e(LX/FUW;Ljava/lang/String;)V

    .line 2248586
    :goto_0
    return-void

    .line 2248587
    :cond_0
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->T:Lcom/facebook/camera/views/RotateLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/camera/views/RotateLayout;->setVisibility(I)V

    .line 2248588
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->P:LX/6HU;

    if-eqz v0, :cond_1

    .line 2248589
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->P:LX/6HU;

    sget-object v1, LX/6HX;->LAST_SECOND_AUTOFOCUS:LX/6HX;

    invoke-virtual {v0, v1}, LX/6HU;->a(LX/6HX;)V

    .line 2248590
    :cond_1
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->Q:LX/6HV;

    .line 2248591
    iput-object p0, v0, LX/6HV;->e:Lcom/facebook/qrcode/QRCodeFragment;

    .line 2248592
    iget-object v1, v0, LX/6HV;->c:Landroid/hardware/Camera;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 2248593
    goto :goto_0
.end method

.method public final a([BI)V
    .locals 1

    .prologue
    .line 2248328
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->P:LX/6HU;

    if-eqz v0, :cond_0

    .line 2248329
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->P:LX/6HU;

    invoke-virtual {v0}, LX/6HU;->j()V

    .line 2248330
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2248331
    return-void
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 2248332
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 2248333
    return-void
.end method

.method public final c()Lcom/facebook/camera/views/RotateLayout;
    .locals 1

    .prologue
    .line 2248334
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->T:Lcom/facebook/camera/views/RotateLayout;

    return-object v0
.end method

.method public final c(Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 2248335
    return-void
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    .line 2248336
    if-eqz p1, :cond_0

    .line 2248337
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->V:Lcom/facebook/fbui/glyph/GlyphButton;

    const v1, 0x7f02077d

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setImageResource(I)V

    .line 2248338
    :goto_0
    return-void

    .line 2248339
    :cond_0
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->V:Lcom/facebook/fbui/glyph/GlyphButton;

    const v1, 0x7f02077e

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setImageResource(I)V

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2248340
    const/4 v0, 0x0

    return v0
.end method

.method public final e()LX/6IG;
    .locals 1

    .prologue
    .line 2248341
    invoke-virtual {p0}, Lcom/facebook/qrcode/QRCodeFragment;->f()LX/6IG;

    move-result-object v0

    return-object v0
.end method

.method public final f()LX/6IG;
    .locals 1

    .prologue
    .line 2248342
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->H:LX/6IG;

    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 2248327
    const/4 v0, 0x0

    return v0
.end method

.method public final h()LX/6Hd;
    .locals 1

    .prologue
    .line 2248344
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2248345
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2248346
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 2248347
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->f:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/qrcode/QRCodeFragment$15;

    invoke-direct {v1, p0, p3}, Lcom/facebook/qrcode/QRCodeFragment$15;-><init>(Lcom/facebook/qrcode/QRCodeFragment;Landroid/content/Intent;)V

    const v2, 0x1ba25370

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2248348
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x277ed899

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2248349
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    .line 2248350
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 2248351
    const v1, 0x7f0310b8

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2248352
    invoke-direct {p0, v1}, Lcom/facebook/qrcode/QRCodeFragment;->a(Landroid/view/View;)V

    .line 2248353
    iget-object v2, p0, Lcom/facebook/qrcode/QRCodeFragment;->W:Landroid/view/View;

    new-instance v3, LX/FUM;

    invoke-direct {v3, p0}, LX/FUM;-><init>(Lcom/facebook/qrcode/QRCodeFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2248354
    iget-object v2, p0, Lcom/facebook/qrcode/QRCodeFragment;->X:Landroid/view/View;

    new-instance v3, LX/FUN;

    invoke-direct {v3, p0}, LX/FUN;-><init>(Lcom/facebook/qrcode/QRCodeFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2248355
    iget-object v2, p0, Lcom/facebook/qrcode/QRCodeFragment;->aa:Landroid/view/View;

    new-instance v3, LX/FUO;

    invoke-direct {v3, p0}, LX/FUO;-><init>(Lcom/facebook/qrcode/QRCodeFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2248356
    iget-object v2, p0, Lcom/facebook/qrcode/QRCodeFragment;->V:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v3, LX/FUP;

    invoke-direct {v3, p0}, LX/FUP;-><init>(Lcom/facebook/qrcode/QRCodeFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2248357
    iget-object v2, p0, Lcom/facebook/qrcode/QRCodeFragment;->Y:Lcom/facebook/resources/ui/FbButton;

    new-instance v3, LX/FUQ;

    invoke-direct {v3, p0}, LX/FUQ;-><init>(Lcom/facebook/qrcode/QRCodeFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2248358
    iget-object v2, p0, Lcom/facebook/qrcode/QRCodeFragment;->Z:Lcom/facebook/resources/ui/FbButton;

    new-instance v3, LX/FUR;

    invoke-direct {v3, p0}, LX/FUR;-><init>(Lcom/facebook/qrcode/QRCodeFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2248359
    iget-object v2, p0, Lcom/facebook/qrcode/QRCodeFragment;->J:Lcom/facebook/drawee/view/DraweeView;

    new-instance v3, LX/FUS;

    invoke-direct {v3, p0}, LX/FUS;-><init>(Lcom/facebook/qrcode/QRCodeFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/view/DraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2248360
    sget-object v2, LX/FUU;->SCAN:LX/FUU;

    const-string v3, "FRAG_CREATE_VIEW"

    invoke-static {p0, v2, v3}, Lcom/facebook/qrcode/QRCodeFragment;->a$redex0(Lcom/facebook/qrcode/QRCodeFragment;LX/FUU;Ljava/lang/String;)V

    .line 2248361
    const/16 v2, 0x2b

    const v3, 0x2bf47dd2

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x39155b66

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2248362
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->K:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 2248363
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->K:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 2248364
    :cond_0
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->t:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2248365
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2248366
    const/16 v1, 0x2b

    const v2, -0x6aab18ae

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x259a4525

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2248367
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 2248368
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/facebook/qrcode/QRCodeFragment;->d(Z)V

    .line 2248369
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2248370
    const/16 v1, 0x2b

    const v2, 0x6df18d94

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x9869d30

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2248371
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->G:LX/FUU;

    sget-object v2, LX/FUU;->SCAN:LX/FUU;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->P:LX/6HU;

    if-eqz v1, :cond_0

    .line 2248372
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->P:LX/6HU;

    invoke-virtual {v1}, LX/6HU;->l()V

    .line 2248373
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->x:LX/FUm;

    invoke-virtual {v1}, LX/FUm;->a()V

    .line 2248374
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2248375
    const/16 v1, 0x2b

    const v2, -0xaf25c6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x51027884

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2248376
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2248377
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->G:LX/FUU;

    sget-object v2, LX/FUU;->SCAN:LX/FUU;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->P:LX/6HU;

    if-eqz v1, :cond_0

    .line 2248378
    const-string v1, "RESUME_SCAN"

    sget-object v2, Lcom/facebook/qrcode/QRCodeFragment;->D:[Ljava/lang/String;

    new-instance v3, Lcom/facebook/qrcode/QRCodeFragment$10;

    invoke-direct {v3, p0}, Lcom/facebook/qrcode/QRCodeFragment$10;-><init>(Lcom/facebook/qrcode/QRCodeFragment;)V

    invoke-static {p0, v1, v2, v3}, Lcom/facebook/qrcode/QRCodeFragment;->a$redex0(Lcom/facebook/qrcode/QRCodeFragment;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 2248379
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x65fbe0bd

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2248380
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment;->r:LX/FUg;

    .line 2248381
    const-string v1, "qrCodeSource"

    iget-object v2, v0, LX/FUg;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2248382
    const-string v1, "lifecycleEntryPoint"

    iget-object v2, v0, LX/FUg;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2248383
    const-string v1, "galleryImportRecentlyPressed"

    iget-boolean v2, v0, LX/FUg;->e:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2248384
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2248385
    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7fd26072

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2248386
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2248387
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment;->r:LX/FUg;

    .line 2248388
    const-string v2, "START_FROM_BACKGROUND"

    iget-object v4, v1, LX/FUg;->c:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "START_TYPE_FROM_PREV_SUCCESS"

    iget-object v4, v1, LX/FUg;->c:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2248389
    :cond_0
    iget-object v2, v1, LX/FUg;->a:LX/FUf;

    iget-object v4, v1, LX/FUg;->b:Ljava/lang/String;

    iget-object p0, v1, LX/FUg;->c:Ljava/lang/String;

    invoke-virtual {v2, v4, p0}, LX/FUf;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2248390
    :cond_1
    const-string v2, "END_FROM_HOME"

    iput-object v2, v1, LX/FUg;->d:Ljava/lang/String;

    .line 2248391
    const/4 v2, 0x0

    iput-boolean v2, v1, LX/FUg;->e:Z

    .line 2248392
    const/16 v1, 0x2b

    const v2, -0x73272934

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x25f5cf5e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2248393
    const/4 v0, 0x0

    .line 2248394
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2248395
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isChangingConfigurations()Z

    move-result v0

    .line 2248396
    :cond_0
    iget-object v2, p0, Lcom/facebook/qrcode/QRCodeFragment;->r:LX/FUg;

    .line 2248397
    if-nez v0, :cond_1

    iget-boolean v4, v2, LX/FUg;->e:Z

    if-eqz v4, :cond_3

    .line 2248398
    :cond_1
    const-string v4, "funnelNotStoppedDoNotLog"

    iput-object v4, v2, LX/FUg;->c:Ljava/lang/String;

    .line 2248399
    :cond_2
    :goto_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2248400
    const/16 v0, 0x2b

    const v2, 0x7c392f4c

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2248401
    :cond_3
    iget-object v4, v2, LX/FUg;->a:LX/FUf;

    iget-object v5, v2, LX/FUg;->d:Ljava/lang/String;

    .line 2248402
    iget-object v6, v4, LX/FUf;->a:LX/0if;

    sget-object v7, LX/0ig;->H:LX/0ih;

    const-string v0, "QR_ENDED_FROM"

    invoke-virtual {v6, v7, v0, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2248403
    iget-object v6, v4, LX/FUf;->a:LX/0if;

    sget-object v7, LX/0ig;->H:LX/0ih;

    invoke-virtual {v6, v7}, LX/0if;->c(LX/0ih;)V

    .line 2248404
    const-string v4, "END_FROM_SUCCESSFUL_SCAN"

    iget-object v5, v2, LX/FUg;->d:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2248405
    const-string v4, "START_TYPE_FROM_PREV_SUCCESS"

    iput-object v4, v2, LX/FUg;->c:Ljava/lang/String;

    goto :goto_0

    .line 2248406
    :cond_4
    const-string v4, "END_FROM_HOME"

    iget-object v5, v2, LX/FUg;->d:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2248407
    const-string v4, "START_FROM_BACKGROUND"

    iput-object v4, v2, LX/FUg;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public final setUserVisibleHint(Z)V
    .locals 0

    .prologue
    .line 2248408
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 2248409
    invoke-direct {p0, p1}, Lcom/facebook/qrcode/QRCodeFragment;->d(Z)V

    .line 2248410
    return-void
.end method
