.class public final Lcom/facebook/qrcode/QRCodeFragment$15;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/content/Intent;

.field public final synthetic b:Lcom/facebook/qrcode/QRCodeFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/qrcode/QRCodeFragment;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 2248150
    iput-object p1, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iput-object p2, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->a:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    const/4 v8, 0x0

    const/4 v10, 0x0

    .line 2248151
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v0, v0, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    .line 2248152
    iget-object v1, v0, LX/FUf;->a:LX/0if;

    sget-object v2, LX/0ig;->H:LX/0ih;

    const-string v3, "IMPORT_FILE_SELECTED_FILE"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2248153
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v0, v0, Lcom/facebook/qrcode/QRCodeFragment;->p:LX/FUW;

    .line 2248154
    const-string v1, "qrcode_import_loaded"

    invoke-static {v0, v1}, LX/FUW;->e(LX/FUW;Ljava/lang/String;)V

    .line 2248155
    new-instance v9, LX/G9N;

    invoke-direct {v9}, LX/G9N;-><init>()V

    .line 2248156
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 2248157
    :try_start_0
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v1, v1, Lcom/facebook/qrcode/QRCodeFragment;->b:LX/2Qx;

    iget-object v2, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v2, v2, Lcom/facebook/qrcode/QRCodeFragment;->e:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, LX/2Qx;->a(Landroid/content/Context;Landroid/net/Uri;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2248158
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 2248159
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 2248160
    mul-int v1, v3, v7

    new-array v1, v1, [I

    .line 2248161
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move v6, v3

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 2248162
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 2248163
    new-instance v0, LX/G8z;

    invoke-direct {v0, v3, v7, v1}, LX/G8z;-><init>(II[I)V

    .line 2248164
    new-instance v1, LX/G8q;

    new-instance v2, LX/G9E;

    invoke-direct {v2, v0}, LX/G9E;-><init>(LX/G8w;)V

    invoke-direct {v1, v2}, LX/G8q;-><init>(LX/G8p;)V
    :try_end_0
    .catch LX/42y; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/42x; {:try_start_0 .. :try_end_0} :catch_1
    .catch LX/430; {:try_start_0 .. :try_end_0} :catch_2
    .catch LX/42z; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_4

    .line 2248165
    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    .line 2248166
    sget-object v0, LX/G8t;->TRY_HARDER:LX/G8t;

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2248167
    :try_start_1
    invoke-virtual {v9, v1, v2}, LX/G9N;->a(LX/G8q;Ljava/util/Map;)LX/G90;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5

    move-result-object v0

    .line 2248168
    :goto_0
    if-nez v0, :cond_0

    .line 2248169
    sget-object v0, LX/G8t;->PURE_BARCODE:LX/G8t;

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2248170
    :try_start_2
    invoke-virtual {v9, v1, v2}, LX/G9N;->a(LX/G8q;Ljava/util/Map;)LX/G90;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6

    move-result-object v0

    .line 2248171
    :cond_0
    :goto_1
    if-nez v0, :cond_1

    .line 2248172
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    const v1, 0x7f0832df

    invoke-static {v0, v1}, Lcom/facebook/qrcode/QRCodeFragment;->d(Lcom/facebook/qrcode/QRCodeFragment;I)V

    .line 2248173
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v0, v0, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    .line 2248174
    iget-object v1, v0, LX/FUf;->a:LX/0if;

    sget-object v2, LX/0ig;->H:LX/0ih;

    const-string v3, "IMPORT_FILE_NO_CODE_DETECTED"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2248175
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v0, v0, Lcom/facebook/qrcode/QRCodeFragment;->p:LX/FUW;

    invoke-virtual {v0}, LX/FUW;->f()V

    .line 2248176
    :goto_2
    return-void

    .line 2248177
    :catch_0
    move-exception v0

    .line 2248178
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    const v2, 0x7f0832df

    invoke-static {v1, v2}, Lcom/facebook/qrcode/QRCodeFragment;->d(Lcom/facebook/qrcode/QRCodeFragment;I)V

    .line 2248179
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v1, v1, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    invoke-virtual {v1, v0}, LX/FUf;->c(Ljava/lang/Exception;)V

    .line 2248180
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v0, v0, Lcom/facebook/qrcode/QRCodeFragment;->p:LX/FUW;

    invoke-virtual {v0}, LX/FUW;->f()V

    goto :goto_2

    .line 2248181
    :catch_1
    move-exception v0

    .line 2248182
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    const v2, 0x7f0832df

    invoke-static {v1, v2}, Lcom/facebook/qrcode/QRCodeFragment;->d(Lcom/facebook/qrcode/QRCodeFragment;I)V

    .line 2248183
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v1, v1, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    invoke-virtual {v1, v0}, LX/FUf;->c(Ljava/lang/Exception;)V

    .line 2248184
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v0, v0, Lcom/facebook/qrcode/QRCodeFragment;->p:LX/FUW;

    invoke-virtual {v0}, LX/FUW;->f()V

    goto :goto_2

    .line 2248185
    :catch_2
    move-exception v0

    .line 2248186
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    const v2, 0x7f0832df

    invoke-static {v1, v2}, Lcom/facebook/qrcode/QRCodeFragment;->d(Lcom/facebook/qrcode/QRCodeFragment;I)V

    .line 2248187
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v1, v1, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    invoke-virtual {v1, v0}, LX/FUf;->c(Ljava/lang/Exception;)V

    .line 2248188
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v0, v0, Lcom/facebook/qrcode/QRCodeFragment;->p:LX/FUW;

    invoke-virtual {v0}, LX/FUW;->h()V

    goto :goto_2

    .line 2248189
    :catch_3
    move-exception v0

    .line 2248190
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    const v2, 0x7f0832df

    invoke-static {v1, v2}, Lcom/facebook/qrcode/QRCodeFragment;->d(Lcom/facebook/qrcode/QRCodeFragment;I)V

    .line 2248191
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v1, v1, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    invoke-virtual {v1, v0}, LX/FUf;->c(Ljava/lang/Exception;)V

    .line 2248192
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v0, v0, Lcom/facebook/qrcode/QRCodeFragment;->p:LX/FUW;

    invoke-virtual {v0}, LX/FUW;->h()V

    goto :goto_2

    .line 2248193
    :catch_4
    move-exception v0

    .line 2248194
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    const v2, 0x7f0832df

    invoke-static {v1, v2}, Lcom/facebook/qrcode/QRCodeFragment;->d(Lcom/facebook/qrcode/QRCodeFragment;I)V

    .line 2248195
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v1, v1, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    .line 2248196
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v2

    const-string v3, "msg"

    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v2

    .line 2248197
    iget-object v3, v1, LX/FUf;->a:LX/0if;

    sget-object v4, LX/0ig;->H:LX/0ih;

    const-string v5, "OOM_ON_IMPORT_FILE"

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2248198
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v0, v0, Lcom/facebook/qrcode/QRCodeFragment;->p:LX/FUW;

    invoke-virtual {v0}, LX/FUW;->f()V

    goto/16 :goto_2

    .line 2248199
    :catch_5
    move-object v0, v8

    goto/16 :goto_0

    .line 2248200
    :catch_6
    move-object v0, v8

    goto/16 :goto_1

    .line 2248201
    :cond_1
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v1, v1, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    .line 2248202
    iget-object v2, v1, LX/FUf;->a:LX/0if;

    sget-object v3, LX/0ig;->H:LX/0ih;

    const-string v4, "IMPORT_FILE_CODE_DETECTED_SUCC"

    invoke-virtual {v2, v3, v4}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2248203
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v1, v1, Lcom/facebook/qrcode/QRCodeFragment;->p:LX/FUW;

    .line 2248204
    iget-object v2, v0, LX/G90;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2248205
    invoke-virtual {v1, v2, v10}, LX/FUW;->b(Ljava/lang/String;Z)V

    .line 2248206
    iget-object v1, p0, Lcom/facebook/qrcode/QRCodeFragment$15;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v1, v1, Lcom/facebook/qrcode/QRCodeFragment;->a:LX/0Sh;

    new-instance v2, Lcom/facebook/qrcode/QRCodeFragment$15$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/qrcode/QRCodeFragment$15$1;-><init>(Lcom/facebook/qrcode/QRCodeFragment$15;LX/G90;)V

    const-wide/16 v4, 0x64

    invoke-virtual {v1, v2, v4, v5}, LX/0Sh;->a(Ljava/lang/Runnable;J)V

    goto/16 :goto_2
.end method
