.class public final Lcom/facebook/qrcode/QRCodeFragment$6$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/FUQ;


# direct methods
.method public constructor <init>(LX/FUQ;)V
    .locals 0

    .prologue
    .line 2248249
    iput-object p1, p0, Lcom/facebook/qrcode/QRCodeFragment$6$1;->a:LX/FUQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 2248250
    iget-object v0, p0, Lcom/facebook/qrcode/QRCodeFragment$6$1;->a:LX/FUQ;

    iget-object v0, v0, LX/FUQ;->a:Lcom/facebook/qrcode/QRCodeFragment;

    .line 2248251
    :try_start_0
    new-instance v1, Ljava/io/File;

    sget-object v2, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-static {v2}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    const-string v3, "Facebook"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2248252
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2248253
    iget-object v1, v0, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    .line 2248254
    iget-object v2, v1, LX/FUf;->a:LX/0if;

    sget-object v3, LX/0ig;->H:LX/0ih;

    const-string v4, "STORAGE_ERROR_ON_SAVE"

    invoke-virtual {v2, v3, v4}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2248255
    :goto_0
    return-void

    .line 2248256
    :cond_0
    :try_start_1
    new-instance v2, Ljava/io/File;

    const-string v3, "%s_%s%s"

    const-string v4, "QRCODE"

    iget-object v5, v0, Lcom/facebook/qrcode/QRCodeFragment;->d:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    const-string v6, ".jpg"

    invoke-static {v3, v4, v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2248257
    new-instance v1, LX/FUI;

    invoke-direct {v1, v0, v2}, LX/FUI;-><init>(Lcom/facebook/qrcode/QRCodeFragment;Ljava/io/File;)V

    invoke-static {v0, v2, v1}, Lcom/facebook/qrcode/QRCodeFragment;->a(Lcom/facebook/qrcode/QRCodeFragment;Ljava/io/File;LX/FUH;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 2248258
    :catch_0
    iget-object v1, v0, Lcom/facebook/qrcode/QRCodeFragment;->v:LX/0kL;

    new-instance v2, LX/27k;

    const v3, 0x7f0832e6

    invoke-direct {v2, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0
.end method
