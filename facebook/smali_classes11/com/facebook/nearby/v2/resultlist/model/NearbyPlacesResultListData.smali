.class public Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Landroid/location/Location;

.field public b:Landroid/location/Location;

.field public c:Ljava/lang/String;

.field public d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

.field public e:LX/FPn;

.field public f:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

.field public g:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

.field private h:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

.field public i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/lang/String;

.field public k:Z

.field public l:Z

.field public m:Ljava/lang/String;

.field public n:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2237652
    new-instance v0, LX/FPc;

    invoke-direct {v0}, LX/FPc;-><init>()V

    sput-object v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2237726
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2237727
    new-instance v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

    invoke-direct {v0}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

    .line 2237728
    sget-object v0, LX/FPn;->NONE:LX/FPn;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->e:LX/FPn;

    .line 2237729
    return-void
.end method

.method public constructor <init>(Landroid/location/Location;Landroid/location/Location;Ljava/lang/String;Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;LX/FPn;Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;Ljava/util/ArrayList;Ljava/lang/String;ZZLjava/lang/String;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/location/Location;",
            "Landroid/location/Location;",
            "Ljava/lang/String;",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;",
            "LX/FPn;",
            "Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;",
            "Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;",
            ">;",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2237710
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2237711
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->a:Landroid/location/Location;

    .line 2237712
    iput-object p2, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->b:Landroid/location/Location;

    .line 2237713
    iput-object p3, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->c:Ljava/lang/String;

    .line 2237714
    iput-object p4, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->g:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    .line 2237715
    iput-object p5, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->e:LX/FPn;

    .line 2237716
    iput-object p6, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

    .line 2237717
    iput-object p7, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->f:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    .line 2237718
    iput-object p8, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->h:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    .line 2237719
    iput-object p9, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->i:Ljava/util/ArrayList;

    .line 2237720
    iput-object p10, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->j:Ljava/lang/String;

    .line 2237721
    iput-boolean p11, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->k:Z

    .line 2237722
    iput-boolean p12, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->l:Z

    .line 2237723
    iput-object p13, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->m:Ljava/lang/String;

    .line 2237724
    iput-boolean p14, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->n:Z

    .line 2237725
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2237688
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2237689
    const-class v0, Landroid/location/Location;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->a:Landroid/location/Location;

    .line 2237690
    const-class v0, Landroid/location/Location;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->b:Landroid/location/Location;

    .line 2237691
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->c:Ljava/lang/String;

    .line 2237692
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->g:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    .line 2237693
    const-class v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

    .line 2237694
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 2237695
    invoke-static {}, LX/FPn;->values()[LX/FPn;

    move-result-object v3

    aget-object v0, v3, v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->e:LX/FPn;

    .line 2237696
    const-class v0, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->f:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    .line 2237697
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->h:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    .line 2237698
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2237699
    const-class v3, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 2237700
    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->i:Ljava/util/ArrayList;

    .line 2237701
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->j:Ljava/lang/String;

    .line 2237702
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->k:Z

    .line 2237703
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->l:Z

    .line 2237704
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->m:Ljava/lang/String;

    .line 2237705
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->n:Z

    .line 2237706
    return-void

    :cond_0
    move v0, v2

    .line 2237707
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2237708
    goto :goto_1

    :cond_2
    move v1, v2

    .line 2237709
    goto :goto_2
.end method


# virtual methods
.method public final a()Landroid/location/Location;
    .locals 1

    .prologue
    .line 2237687
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->a:Landroid/location/Location;

    return-object v0
.end method

.method public final b()Landroid/location/Location;
    .locals 1

    .prologue
    .line 2237686
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->b:Landroid/location/Location;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2237685
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;
    .locals 1

    .prologue
    .line 2237684
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->g:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2237683
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;
    .locals 1

    .prologue
    .line 2237682
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

    return-object v0
.end method

.method public final f()LX/FPn;
    .locals 1

    .prologue
    .line 2237681
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->e:LX/FPn;

    return-object v0
.end method

.method public final g()Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;
    .locals 1

    .prologue
    .line 2237680
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->f:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    return-object v0
.end method

.method public final h()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2237677
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->i:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 2237678
    const/4 v0, 0x0

    .line 2237679
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->i:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2237676
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 2237675
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->k:Z

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 2237674
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->l:Z

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2237673
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 2237672
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->n:Z

    return v0
.end method

.method public final n()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;
    .locals 1

    .prologue
    .line 2237671
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->h:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2237653
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->a:Landroid/location/Location;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2237654
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->b:Landroid/location/Location;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2237655
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2237656
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->g:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2237657
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2237658
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->e:LX/FPn;

    invoke-virtual {v0}, LX/FPn;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2237659
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->f:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2237660
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->h:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2237661
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->i:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2237662
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2237663
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->k:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2237664
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->l:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2237665
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2237666
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->n:Z

    if-eqz v0, :cond_2

    :goto_2
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2237667
    return-void

    :cond_0
    move v0, v2

    .line 2237668
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2237669
    goto :goto_1

    :cond_2
    move v1, v2

    .line 2237670
    goto :goto_2
.end method
