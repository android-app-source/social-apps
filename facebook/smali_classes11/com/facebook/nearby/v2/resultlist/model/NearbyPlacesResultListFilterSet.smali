.class public Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/FPo;

.field public b:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;

.field public c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListPriceCategoriesFilter;

.field public d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2237828
    new-instance v0, LX/FPg;

    invoke-direct {v0}, LX/FPg;-><init>()V

    sput-object v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2237824
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2237825
    sget-object v0, LX/FPo;->RELEVANCE:LX/FPo;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->a:LX/FPo;

    .line 2237826
    new-instance v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->b:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;

    .line 2237827
    return-void
.end method

.method public constructor <init>(LX/FPh;)V
    .locals 2

    .prologue
    .line 2237816
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2237817
    iget-object v0, p1, LX/FPh;->a:LX/FPo;

    if-nez v0, :cond_0

    sget-object v0, LX/FPo;->RELEVANCE:LX/FPo;

    :goto_0
    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->a:LX/FPo;

    .line 2237818
    iget-object v0, p1, LX/FPh;->b:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;

    if-nez v0, :cond_1

    new-instance v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;-><init>(I)V

    :goto_1
    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->b:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;

    .line 2237819
    iget-object v0, p1, LX/FPh;->c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListPriceCategoriesFilter;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListPriceCategoriesFilter;

    .line 2237820
    iget-object v0, p1, LX/FPh;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;

    .line 2237821
    return-void

    .line 2237822
    :cond_0
    iget-object v0, p1, LX/FPh;->a:LX/FPo;

    goto :goto_0

    .line 2237823
    :cond_1
    iget-object v0, p1, LX/FPh;->b:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;

    goto :goto_1
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2237810
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2237811
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/FPo;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->a:LX/FPo;

    .line 2237812
    const-class v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->b:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;

    .line 2237813
    const-class v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListPriceCategoriesFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListPriceCategoriesFilter;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListPriceCategoriesFilter;

    .line 2237814
    const-class v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;

    .line 2237815
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2237829
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2237798
    if-ne p0, p1, :cond_1

    move v2, v1

    .line 2237799
    :cond_0
    :goto_0
    return v2

    .line 2237800
    :cond_1
    instance-of v0, p1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

    if-eqz v0, :cond_0

    .line 2237801
    check-cast p1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

    .line 2237802
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->a:LX/FPo;

    iget-object v3, p1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->a:LX/FPo;

    if-ne v0, v3, :cond_0

    .line 2237803
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->b:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->b:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;

    iget-object v3, p1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->b:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;

    invoke-virtual {v0, v3}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2237804
    :cond_2
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListPriceCategoriesFilter;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListPriceCategoriesFilter;

    iget-object v3, p1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListPriceCategoriesFilter;

    invoke-virtual {v0, v3}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListPriceCategoriesFilter;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_2
    if-nez v0, :cond_0

    .line 2237805
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;

    iget-object v3, p1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;

    invoke-virtual {v0, v3}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_4
    if-nez v0, :cond_0

    move v2, v1

    .line 2237806
    goto :goto_0

    .line 2237807
    :cond_3
    iget-object v0, p1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->b:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_4
    move v0, v2

    .line 2237808
    goto :goto_1

    :cond_5
    iget-object v0, p1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListPriceCategoriesFilter;

    goto :goto_2

    :cond_6
    move v0, v2

    .line 2237809
    goto :goto_3

    :cond_7
    iget-object v0, p1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;

    goto :goto_4
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2237790
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->a:LX/FPo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->a:LX/FPo;

    invoke-virtual {v0}, LX/FPo;->hashCode()I

    move-result v0

    .line 2237791
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->b:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->b:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 2237792
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListPriceCategoriesFilter;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListPriceCategoriesFilter;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListPriceCategoriesFilter;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 2237793
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;

    invoke-virtual {v1}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 2237794
    return v0

    :cond_1
    move v0, v1

    .line 2237795
    goto :goto_0

    :cond_2
    move v0, v1

    .line 2237796
    goto :goto_1

    :cond_3
    move v0, v1

    .line 2237797
    goto :goto_2
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2237785
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->a:LX/FPo;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2237786
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->b:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2237787
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListPriceCategoriesFilter;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2237788
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2237789
    return-void
.end method
