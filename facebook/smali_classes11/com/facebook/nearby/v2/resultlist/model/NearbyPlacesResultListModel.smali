.class public Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

.field public b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2237873
    new-instance v0, LX/FPi;

    invoke-direct {v0}, LX/FPi;-><init>()V

    sput-object v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2237868
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2237869
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->b:Z

    .line 2237870
    const-class v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    .line 2237871
    return-void

    .line 2237872
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Z)V
    .locals 1

    .prologue
    .line 2237864
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2237865
    iput-boolean p1, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->b:Z

    .line 2237866
    new-instance v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    invoke-direct {v0}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;-><init>()V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    .line 2237867
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;)V
    .locals 17

    .prologue
    .line 2237849
    if-nez p1, :cond_0

    .line 2237850
    new-instance v2, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    invoke-direct {v2}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    .line 2237851
    :goto_0
    return-void

    .line 2237852
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->j()Z

    move-result v2

    if-nez v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    if-nez v2, :cond_2

    .line 2237853
    :cond_1
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    goto :goto_0

    .line 2237854
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->h()Ljava/util/ArrayList;

    move-result-object v2

    .line 2237855
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    invoke-virtual {v3}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->h()Ljava/util/ArrayList;

    move-result-object v3

    .line 2237856
    if-eqz v3, :cond_3

    if-eqz v2, :cond_3

    .line 2237857
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 2237858
    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2237859
    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2237860
    :goto_1
    new-instance v2, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    invoke-virtual {v3}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->a()Landroid/location/Location;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    invoke-virtual {v4}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->b()Landroid/location/Location;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    invoke-virtual {v5}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->c()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    invoke-virtual {v6}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->d()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    invoke-virtual {v7}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->f()LX/FPn;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    invoke-virtual {v8}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->e()Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    invoke-virtual {v9}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->g()Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    invoke-virtual {v10}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->n()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->i()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->j()Z

    move-result v13

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->k()Z

    move-result v14

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->l()Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->m()Z

    move-result v16

    invoke-direct/range {v2 .. v16}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;-><init>(Landroid/location/Location;Landroid/location/Location;Ljava/lang/String;Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;LX/FPn;Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;Ljava/util/ArrayList;Ljava/lang/String;ZZLjava/lang/String;Z)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    goto/16 :goto_0

    .line 2237861
    :cond_3
    if-nez v3, :cond_4

    move-object v11, v2

    .line 2237862
    goto :goto_1

    :cond_4
    move-object v11, v3

    .line 2237863
    goto :goto_1
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2237833
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    .line 2237834
    iget-object p0, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->i:Ljava/util/ArrayList;

    if-eqz p0, :cond_1

    iget-object p0, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->i:Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_1

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 2237835
    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 2237841
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    .line 2237842
    iget-object v1, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

    move-object v0, v1

    .line 2237843
    if-eqz v0, :cond_0

    .line 2237844
    sget-object v0, LX/FPo;->RELEVANCE:LX/FPo;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    .line 2237845
    iget-object p0, v1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

    move-object v1, p0

    .line 2237846
    iget-object p0, v1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;->a:LX/FPo;

    move-object v1, p0

    .line 2237847
    invoke-virtual {v0, v1}, LX/FPo;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2237848
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2237840
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2237836
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2237837
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2237838
    return-void

    .line 2237839
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
