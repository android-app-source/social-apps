.class public Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2237780
    new-instance v0, LX/FPd;

    invoke-direct {v0}, LX/FPd;-><init>()V

    sput-object v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2237777
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2237778
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;->a:LX/0Px;

    .line 2237779
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2237773
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2237774
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;->a:LX/0Px;

    .line 2237775
    return-void

    .line 2237776
    :cond_0
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2237772
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2237767
    instance-of v1, p1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;

    if-eqz v1, :cond_0

    .line 2237768
    check-cast p1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;

    iget-object v1, p1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;->a:LX/0Px;

    .line 2237769
    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;->a:LX/0Px;

    if-nez v2, :cond_1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 2237770
    :cond_0
    :goto_0
    return v0

    .line 2237771
    :cond_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2237754
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 2237757
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;->a:LX/0Px;

    if-nez v0, :cond_0

    .line 2237758
    const-string v0, "null"

    .line 2237759
    :goto_0
    return-object v0

    .line 2237760
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "{"

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2237761
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2237762
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_1

    .line 2237763
    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2237764
    :cond_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2237765
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2237766
    :cond_2
    const-string v0, "}"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2237755
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFeaturesFilter;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 2237756
    return-void
.end method
