.class public Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2237893
    new-instance v0, LX/FPj;

    invoke-direct {v0}, LX/FPj;-><init>()V

    sput-object v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 2237894
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2237895
    iput p1, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;->a:I

    .line 2237896
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2237889
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2237890
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;->a:I

    .line 2237891
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2237886
    const-string v0, "on"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;-><init>(I)V

    .line 2237887
    return-void

    .line 2237888
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2237892
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2237880
    instance-of v2, p1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;

    if-nez v2, :cond_1

    .line 2237881
    :cond_0
    :goto_0
    return v0

    .line 2237882
    :cond_1
    if-ne p1, p0, :cond_2

    move v0, v1

    .line 2237883
    goto :goto_0

    .line 2237884
    :cond_2
    check-cast p1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;

    .line 2237885
    iget v2, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;->a:I

    iget v3, p1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;->a:I

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2237879
    iget v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;->a:I

    add-int/lit8 v0, v0, 0x1f

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2237877
    iget v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListOpenNowFilter;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2237878
    return-void
.end method
