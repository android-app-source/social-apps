.class public abstract Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2237045
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2237046
    return-void
.end method

.method public static b(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;)Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;
    .locals 3

    .prologue
    .line 2237047
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2237048
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->b:Z

    if-eqz v0, :cond_0

    .line 2237049
    new-instance v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;

    invoke-direct {v0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;-><init>()V

    .line 2237050
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2237051
    const-string v2, "options"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2237052
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2237053
    move-object v0, v0

    .line 2237054
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->a(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;)Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(LX/FOz;)V
.end method

.method public abstract a(LX/FPO;)V
.end method

.method public abstract a(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)V
.end method

.method public abstract a(Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;)V
.end method

.method public abstract a(Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;)V
.end method

.method public abstract b()V
.end method

.method public final d()Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;
    .locals 2

    .prologue
    .line 2237055
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2237056
    const-string v1, "options"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

    return-object v0
.end method
