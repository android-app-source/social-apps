.class public Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;
.super Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;
.source ""

# interfaces
.implements LX/FPO;
.implements LX/FPP;
.implements LX/FPQ;
.implements LX/FPR;


# static fields
.field private static final g:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/FPr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/FPb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/121;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0yc;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/FPa;

.field private h:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

.field private i:LX/FOz;

.field private j:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

.field private k:LX/FPO;

.field private l:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

.field private m:Landroid/view/ViewStub;

.field private n:Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

.field public o:Landroid/widget/ViewFlipper;

.field public p:Lcom/facebook/uicontrib/fab/FabView;

.field private q:Z

.field private r:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

.field private s:LX/FPN;

.field private t:Landroid/animation/ObjectAnimator;

.field public final u:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/FPN;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2237133
    const-class v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;

    sput-object v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->g:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2237134
    invoke-direct {p0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;-><init>()V

    .line 2237135
    iput-boolean v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->q:Z

    .line 2237136
    sget-object v0, LX/FPN;->SHOW_FAB:LX/FPN;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->s:LX/FPN;

    .line 2237137
    new-instance v0, Ljava/util/HashMap;

    invoke-static {}, LX/FPN;->values()[LX/FPN;

    move-result-object v2

    array-length v2, v2

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->u:Ljava/util/Map;

    .line 2237138
    invoke-static {}, LX/FPN;->values()[LX/FPN;

    move-result-object v2

    .line 2237139
    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 2237140
    iget-object v5, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->u:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2237141
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2237142
    :cond_0
    return-void
.end method

.method private a(LX/FPN;)V
    .locals 6

    .prologue
    .line 2237143
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->s:LX/FPN;

    .line 2237144
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->t:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->t:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2237145
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->t:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 2237146
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->p:Lcom/facebook/uicontrib/fab/FabView;

    invoke-static {v0}, LX/0vv;->s(Landroid/view/View;)F

    move-result v1

    .line 2237147
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->u:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    .line 2237148
    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->p:Lcom/facebook/uicontrib/fab/FabView;

    const-string v3, "translationY"

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    aput v1, v4, v5

    const/4 v1, 0x1

    aput v0, v4, v1

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c004e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->t:Landroid/animation/ObjectAnimator;

    .line 2237149
    sget-object v0, LX/FPN;->HIDE_FAB:LX/FPN;

    invoke-virtual {v0, p1}, LX/FPN;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2237150
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->t:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/AnticipateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AnticipateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2237151
    :cond_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->t:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 2237152
    return-void
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2237153
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->n:Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->m:Landroid/view/ViewStub;

    if-eqz v0, :cond_1

    .line 2237154
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->m:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->n:Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    .line 2237155
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->n:Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    invoke-virtual {v0, p1}, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->a(Landroid/os/Bundle;)V

    .line 2237156
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->n:Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    invoke-virtual {v0, p0}, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->setOnPlacePinSelectedListener(LX/FPR;)V

    .line 2237157
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->n:Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    invoke-virtual {v0, p0}, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->setOnPlaceClickedListener(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;)V

    .line 2237158
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->n:Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    .line 2237159
    iput-object p0, v0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->j:LX/FPQ;

    .line 2237160
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->r:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    if-eqz v0, :cond_0

    .line 2237161
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->n:Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->r:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->a(Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;)V

    .line 2237162
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->m:Landroid/view/ViewStub;

    .line 2237163
    :cond_1
    return-void
.end method

.method public static d(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 2237164
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->o:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v0

    .line 2237165
    if-ne v0, p1, :cond_0

    .line 2237166
    :goto_0
    return-void

    .line 2237167
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2237168
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->r:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->r:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    .line 2237169
    iget-object v3, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v0, v3

    .line 2237170
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->r:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    .line 2237171
    iget-object v3, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v0, v3

    .line 2237172
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->h()Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_3

    :cond_1
    move v0, v1

    .line 2237173
    :goto_1
    iget-object v3, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->r:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->r:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    invoke-virtual {v3}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->d()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v1, 0x1

    .line 2237174
    :cond_2
    packed-switch p1, :pswitch_data_0

    .line 2237175
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->a:LX/03V;

    sget-object v1, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->g:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Unknown child passed to setViewFlipperDisplayedChild"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2237176
    :cond_3
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->r:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    .line 2237177
    iget-object v3, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v0, v3

    .line 2237178
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->h()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_1

    .line 2237179
    :pswitch_0
    iget-object v3, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->f:LX/FPa;

    invoke-virtual {v3, v0, v4, v1}, LX/FPa;->b(ILjava/lang/String;Z)V

    .line 2237180
    invoke-direct {p0, v4}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->b(Landroid/os/Bundle;)V

    .line 2237181
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->p:Lcom/facebook/uicontrib/fab/FabView;

    const v1, 0x7f020902

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/fab/FabView;->setGlyphDrawableID(I)V

    .line 2237182
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->o:Landroid/widget/ViewFlipper;

    const v1, 0x7f0400e3

    invoke-virtual {v0, v2, v1}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/content/Context;I)V

    .line 2237183
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->o:Landroid/widget/ViewFlipper;

    const v1, 0x7f0400d4

    invoke-virtual {v0, v2, v1}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/content/Context;I)V

    .line 2237184
    sget-object v0, LX/FPN;->SHOW_MAP:LX/FPN;

    .line 2237185
    :goto_2
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->o:Landroid/widget/ViewFlipper;

    invoke-virtual {v1, p1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 2237186
    invoke-direct {p0, v0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->a(LX/FPN;)V

    goto :goto_0

    .line 2237187
    :pswitch_1
    iget-object v3, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->f:LX/FPa;

    invoke-virtual {v3, v0, v4, v1}, LX/FPa;->a(ILjava/lang/String;Z)V

    .line 2237188
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->p:Lcom/facebook/uicontrib/fab/FabView;

    const v1, 0x7f020964

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/fab/FabView;->setGlyphDrawableID(I)V

    .line 2237189
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->o:Landroid/widget/ViewFlipper;

    const v1, 0x7f0400e6

    invoke-virtual {v0, v2, v1}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/content/Context;I)V

    .line 2237190
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->o:Landroid/widget/ViewFlipper;

    const v1, 0x7f0400d1

    invoke-virtual {v0, v2, v1}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/content/Context;I)V

    .line 2237191
    sget-object v0, LX/FPN;->SHOW_FAB:LX/FPN;

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2237192
    const-string v0, "nearby_places_result_list"

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 2237220
    if-lez p1, :cond_1

    .line 2237221
    sget-object v0, LX/FPN;->HIDE_FAB:LX/FPN;

    invoke-direct {p0, v0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->a(LX/FPN;)V

    .line 2237222
    :cond_0
    :goto_0
    return-void

    .line 2237223
    :cond_1
    if-gez p1, :cond_0

    .line 2237224
    sget-object v0, LX/FPN;->SHOW_FAB:LX/FPN;

    invoke-direct {p0, v0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->a(LX/FPN;)V

    goto :goto_0
.end method

.method public final a(LX/FOz;)V
    .locals 1

    .prologue
    .line 2237193
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->i:LX/FOz;

    .line 2237194
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->l:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    if-eqz v0, :cond_0

    .line 2237195
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->l:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->a(LX/FOz;)V

    .line 2237196
    :cond_0
    return-void
.end method

.method public final a(LX/FPO;)V
    .locals 0
    .param p1    # LX/FPO;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2237197
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->k:LX/FPO;

    .line 2237198
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2237199
    invoke-super {p0, p1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->a(Landroid/os/Bundle;)V

    .line 2237200
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p1}, LX/FPr;->a(LX/0QB;)LX/FPr;

    move-result-object v4

    check-cast v4, LX/FPr;

    const-class v5, LX/FPb;

    invoke-interface {p1, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/FPb;

    invoke-static {p1}, LX/128;->b(LX/0QB;)LX/128;

    move-result-object v6

    check-cast v6, LX/121;

    const/16 v0, 0x4e0

    invoke-static {p1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v3, v2, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->a:LX/03V;

    iput-object v4, v2, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->b:LX/FPr;

    iput-object v5, v2, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->c:LX/FPb;

    iput-object v6, v2, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->d:LX/121;

    iput-object p1, v2, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->e:LX/0Ot;

    .line 2237201
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    .line 2237202
    const-string v0, "resultList"

    invoke-virtual {v1, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->l:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    .line 2237203
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->l:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    if-nez v0, :cond_0

    .line 2237204
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->d()Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->a(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;)Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->l:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    .line 2237205
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d1d3c

    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->l:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2237206
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->l:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->h:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;)V

    .line 2237207
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->l:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->i:LX/FOz;

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->a(LX/FOz;)V

    .line 2237208
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->l:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-virtual {v0, p0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->a(LX/FPO;)V

    .line 2237209
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->l:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    .line 2237210
    iput-object p0, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->n:LX/FPP;

    .line 2237211
    return-void
.end method

.method public final a(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)V
    .locals 1
    .param p1    # Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2237212
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->l:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    if-nez v0, :cond_0

    .line 2237213
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->j:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    .line 2237214
    :goto_0
    return-void

    .line 2237215
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->l:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->a(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;)V
    .locals 1

    .prologue
    .line 2237216
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->h:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2237217
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->l:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    if-eqz v0, :cond_0

    .line 2237218
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->l:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;)V

    .line 2237219
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;IIZ)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 2237126
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->f:LX/FPa;

    if-eqz v0, :cond_0

    .line 2237127
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->f:LX/FPa;

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    sget-object v5, LX/FPZ;->CELL:LX/FPZ;

    move v2, p2

    move v3, p3

    move-object v7, v6

    move-object v8, v6

    move v9, p4

    invoke-virtual/range {v0 .. v9}, LX/FPa;->c(Ljava/lang/String;IIZLX/FPZ;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Z)V

    .line 2237128
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->b:LX/FPr;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->g:Ljava/lang/Class;

    invoke-virtual {v0, p1, v6, v1, v2}, LX/FPr;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;Landroid/location/Location;Landroid/content/Context;Ljava/lang/Class;)V

    .line 2237129
    return-void
.end method

.method public final a(Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;)V
    .locals 1

    .prologue
    .line 2237130
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->l:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    if-eqz v0, :cond_0

    .line 2237131
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->l:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->a(Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;)V

    .line 2237132
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2237114
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->f:LX/FPa;

    if-nez v0, :cond_3

    if-eqz p1, :cond_3

    const/4 v0, 0x1

    .line 2237115
    :goto_0
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->r:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    .line 2237116
    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->p:Lcom/facebook/uicontrib/fab/FabView;

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->c()Z

    move-result v3

    if-eqz v3, :cond_4

    :goto_1
    invoke-virtual {v2, v1}, Lcom/facebook/uicontrib/fab/FabView;->setVisibility(I)V

    .line 2237117
    if-eqz v0, :cond_0

    .line 2237118
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->c:LX/FPb;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->h:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->i:LX/FOz;

    iget-object v3, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->r:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    invoke-virtual {v0, v1, v2, v3}, LX/FPb;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;LX/FOz;Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;)LX/FPa;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->f:LX/FPa;

    .line 2237119
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->n:Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    if-eqz v0, :cond_1

    .line 2237120
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->n:Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    invoke-virtual {v0, p1}, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->a(Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;)V

    .line 2237121
    :cond_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->k:LX/FPO;

    if-eqz v0, :cond_2

    .line 2237122
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->k:LX/FPO;

    invoke-interface {v0, p1}, LX/FPO;->a(Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;)V

    .line 2237123
    :cond_2
    return-void

    :cond_3
    move v0, v1

    .line 2237124
    goto :goto_0

    .line 2237125
    :cond_4
    const/16 v1, 0x8

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2237110
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->l:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    if-nez v0, :cond_0

    .line 2237111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->q:Z

    .line 2237112
    :goto_0
    return-void

    .line 2237113
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->l:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->b()V

    goto :goto_0
.end method

.method public final b(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;IIZ)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 2237107
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->f:LX/FPa;

    if-eqz v0, :cond_0

    .line 2237108
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->f:LX/FPa;

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    sget-object v5, LX/FPZ;->CELL:LX/FPZ;

    move v2, p2

    move v3, p3

    move-object v7, v6

    move-object v8, v6

    move v9, p4

    invoke-virtual/range {v0 .. v9}, LX/FPa;->b(Ljava/lang/String;IIZLX/FPZ;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Z)V

    .line 2237109
    :cond_0
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x2

    const/16 v2, 0x2a

    const v3, -0x17466894

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2237097
    invoke-super {p0, p1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2237098
    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->j:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    if-eqz v2, :cond_0

    .line 2237099
    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->l:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    iget-object v3, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->j:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    invoke-virtual {v2, v3}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->a(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)V

    .line 2237100
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->j:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    .line 2237101
    :cond_0
    iget-boolean v2, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->q:Z

    if-eqz v2, :cond_1

    .line 2237102
    iput-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->q:Z

    .line 2237103
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->b()V

    .line 2237104
    :cond_1
    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->p:Lcom/facebook/uicontrib/fab/FabView;

    iget-object v3, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->r:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->r:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    invoke-virtual {v3}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->c()Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/uicontrib/fab/FabView;->setVisibility(I)V

    .line 2237105
    const v0, -0x392a3499

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2237106
    :cond_2
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x8fc9cde

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2237096
    const v1, 0x7f030bbf

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x4bb75585    # 2.4029962E7f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3bfd629e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2237092
    invoke-super {p0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->onDestroy()V

    .line 2237093
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->n:Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    if-eqz v1, :cond_0

    .line 2237094
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->n:Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    invoke-virtual {v1}, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->e()V

    .line 2237095
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x7a948e08

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x487035bd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2237087
    invoke-super {p0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->onDestroyView()V

    .line 2237088
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->n:Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    if-eqz v1, :cond_0

    .line 2237089
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->n:Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    invoke-virtual {v1}, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->e()V

    .line 2237090
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->n:Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    .line 2237091
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x1cd9bb19

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLowMemory()V
    .locals 1

    .prologue
    .line 2237083
    invoke-super {p0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->onLowMemory()V

    .line 2237084
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->n:Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    if-eqz v0, :cond_0

    .line 2237085
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->n:Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->f()V

    .line 2237086
    :cond_0
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4e440c8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2237079
    invoke-super {p0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->onPause()V

    .line 2237080
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->n:Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    if-eqz v1, :cond_0

    .line 2237081
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->n:Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    invoke-virtual {v1}, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->b()V

    .line 2237082
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x70537b65

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x9c6b712

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2237075
    invoke-super {p0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->onResume()V

    .line 2237076
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->n:Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    if-eqz v1, :cond_0

    .line 2237077
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->n:Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    invoke-virtual {v1}, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->a()V

    .line 2237078
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x588ba9a8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2237069
    invoke-super {p0, p1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2237070
    const-string v1, "map_exists"

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->n:Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2237071
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->n:Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    if-eqz v0, :cond_0

    .line 2237072
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->n:Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    invoke-virtual {v0, p1}, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->b(Landroid/os/Bundle;)V

    .line 2237073
    :cond_0
    return-void

    .line 2237074
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2237060
    invoke-super {p0, p1, p2}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2237061
    const v0, 0x7f0d1d3b

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->o:Landroid/widget/ViewFlipper;

    .line 2237062
    const v0, 0x7f0d1d3f

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/fab/FabView;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->p:Lcom/facebook/uicontrib/fab/FabView;

    .line 2237063
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->p:Lcom/facebook/uicontrib/fab/FabView;

    new-instance v1, LX/FPL;

    invoke-direct {v1, p0}, LX/FPL;-><init>(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/fab/FabView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2237064
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->p:Lcom/facebook/uicontrib/fab/FabView;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/fab/FabView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/FPM;

    invoke-direct {v1, p0}, LX/FPM;-><init>(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2237065
    const v0, 0x7f0d1d3d

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->m:Landroid/view/ViewStub;

    .line 2237066
    if-eqz p2, :cond_0

    const-string v0, "map_exists"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2237067
    invoke-direct {p0, p2}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->b(Landroid/os/Bundle;)V

    .line 2237068
    :cond_0
    return-void
.end method

.method public final z_(I)V
    .locals 3

    .prologue
    .line 2237057
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->u:Ljava/util/Map;

    sget-object v1, LX/FPN;->SHOW_MAP:LX/FPN;

    neg-int v2, p1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2237058
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->s:LX/FPN;

    invoke-direct {p0, v0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;->a(LX/FPN;)V

    .line 2237059
    return-void
.end method
