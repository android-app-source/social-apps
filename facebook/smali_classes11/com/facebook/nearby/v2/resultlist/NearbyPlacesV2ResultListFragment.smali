.class public Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;
.super Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;
.source ""

# interfaces
.implements LX/FPP;


# static fields
.field public static final e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/FPA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/FPr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/FPb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/FPJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final f:LX/FPS;

.field private final g:Landroid/widget/AbsListView$OnScrollListener;

.field private final h:LX/FPU;

.field public i:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

.field private j:LX/FOz;

.field public k:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

.field public l:LX/FPO;

.field private m:LX/FPp;

.field public n:LX/FPP;

.field private o:Landroid/view/ViewGroup;

.field private p:Landroid/widget/ProgressBar;

.field public q:Lcom/facebook/widget/listview/BetterListView;

.field private r:Landroid/view/View;

.field public s:LX/FPI;

.field public t:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

.field public u:LX/FPW;

.field public v:LX/FPa;

.field private w:Z

.field public final x:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public y:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2237503
    const-class v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    sput-object v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->e:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2237496
    invoke-direct {p0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;-><init>()V

    .line 2237497
    new-instance v0, LX/FPS;

    invoke-direct {v0, p0}, LX/FPS;-><init>(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->f:LX/FPS;

    .line 2237498
    new-instance v0, LX/FPT;

    invoke-direct {v0, p0}, LX/FPT;-><init>(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->g:Landroid/widget/AbsListView$OnScrollListener;

    .line 2237499
    new-instance v0, LX/FPU;

    invoke-direct {v0, p0}, LX/FPU;-><init>(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->h:LX/FPU;

    .line 2237500
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->w:Z

    .line 2237501
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->x:Ljava/util/Set;

    .line 2237502
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->y:Ljava/lang/String;

    return-void
.end method

.method public static a(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;)Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;
    .locals 3

    .prologue
    .line 2237491
    new-instance v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-direct {v0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;-><init>()V

    .line 2237492
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2237493
    const-string v2, "options"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2237494
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2237495
    return-object v0
.end method

.method public static synthetic a(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;ILcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;LX/FPZ;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 10

    .prologue
    .line 2237477
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->v:LX/FPa;

    invoke-virtual {p2}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->t:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    const/4 v3, 0x0

    .line 2237478
    iget-object v4, v2, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v4, v4

    .line 2237479
    if-nez v4, :cond_2

    .line 2237480
    :cond_0
    :goto_0
    move v3, v3

    .line 2237481
    const/4 v4, 0x0

    .line 2237482
    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->y:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 2237483
    const-string v2, ", "

    iget-object v5, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->x:Ljava/util/Set;

    invoke-static {v2, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->y:Ljava/lang/String;

    .line 2237484
    :cond_1
    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->y:Ljava/lang/String;

    move-object v8, v2

    .line 2237485
    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->t:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    invoke-virtual {v2}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->d()Z

    move-result v9

    move v2, p1

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-virtual/range {v0 .. v9}, LX/FPa;->a(Ljava/lang/String;IIZLX/FPZ;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Z)V

    .line 2237486
    return-void

    .line 2237487
    :cond_2
    iget-object v4, v2, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v4, v4

    .line 2237488
    invoke-virtual {v4}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->h()Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 2237489
    iget-object v3, v2, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v3, v3

    .line 2237490
    invoke-virtual {v3}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->h()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v4

    check-cast p1, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    new-instance v3, LX/FPA;

    invoke-static {v4}, LX/9kE;->b(LX/0QB;)LX/9kE;

    move-result-object v1

    check-cast v1, LX/9kE;

    invoke-static {v4}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-direct {v3, v1, v2}, LX/FPA;-><init>(LX/9kE;LX/0tX;)V

    move-object v1, v3

    check-cast v1, LX/FPA;

    invoke-static {v4}, LX/FPr;->a(LX/0QB;)LX/FPr;

    move-result-object v2

    check-cast v2, LX/FPr;

    const-class v3, LX/FPb;

    invoke-interface {v4, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/FPb;

    const-class p0, LX/FPJ;

    invoke-interface {v4, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/FPJ;

    iput-object v1, p1, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->a:LX/FPA;

    iput-object v2, p1, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->b:LX/FPr;

    iput-object v3, p1, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->c:LX/FPb;

    iput-object v4, p1, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->d:LX/FPJ;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;LX/FP6;)V
    .locals 9

    .prologue
    .line 2237440
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->a:LX/FPA;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2237441
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->t:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2237442
    sget-object v0, LX/FPV;->b:[I

    invoke-virtual {p1}, LX/FP6;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2237443
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2237444
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->t:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    .line 2237445
    iget-object v1, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v0, v1

    .line 2237446
    if-eqz v0, :cond_1

    .line 2237447
    iget-boolean v1, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->l:Z

    move v1, v1

    .line 2237448
    if-eqz v1, :cond_1

    .line 2237449
    iget-object v1, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->j:Ljava/lang/String;

    move-object v0, v1

    .line 2237450
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2237451
    sget-object v0, LX/FPW;->LOADING_PAGINATION:LX/FPW;

    .line 2237452
    :goto_1
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->a:LX/FPA;

    .line 2237453
    iget-object v2, v1, LX/FPA;->a:LX/9kE;

    invoke-virtual {v2}, LX/9kE;->c()V

    .line 2237454
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->a:LX/FPA;

    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->j:LX/FOz;

    iget-object v3, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->t:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    iget-object v4, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->f:LX/FPS;

    .line 2237455
    const/4 v5, 0x0

    .line 2237456
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2237457
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2237458
    invoke-interface {v2}, LX/FOz;->c()Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-result-object v6

    .line 2237459
    if-nez v6, :cond_3

    .line 2237460
    :goto_2
    move-object v5, v5

    .line 2237461
    if-nez v5, :cond_2

    .line 2237462
    :cond_0
    :goto_3
    invoke-static {p0, v0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->a$redex0(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;LX/FPW;)V

    .line 2237463
    return-void

    .line 2237464
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2237465
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->x:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 2237466
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->y:Ljava/lang/String;

    .line 2237467
    sget-object v0, LX/FPW;->LOADING_INITIAL_RESULTS:LX/FPW;

    goto :goto_1

    .line 2237468
    :cond_2
    invoke-static {v5}, LX/FPA;->a(LX/FP4;)LX/CQD;

    move-result-object v6

    .line 2237469
    if-eqz v6, :cond_0

    .line 2237470
    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    .line 2237471
    iget-object v7, v1, LX/FPA;->a:LX/9kE;

    iget-object v8, v1, LX/FPA;->b:LX/0tX;

    invoke-virtual {v8, v6}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    invoke-static {v6}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    new-instance v8, LX/FP8;

    invoke-direct {v8, v1, v5, v4}, LX/FP8;-><init>(LX/FPA;LX/FP4;LX/0TF;)V

    invoke-virtual {v7, v6, v8}, LX/9kE;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_3

    .line 2237472
    :cond_3
    sget-object v7, LX/FP5;->a:[I

    invoke-virtual {p1}, LX/FP6;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_1

    goto :goto_2

    .line 2237473
    :pswitch_2
    invoke-static {v3}, LX/FP7;->a(Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;)LX/FP4;

    move-result-object v5

    goto :goto_2

    .line 2237474
    :pswitch_3
    invoke-static {v6, v3}, LX/FP7;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;)LX/FP4;

    move-result-object v5

    goto :goto_2

    .line 2237475
    :pswitch_4
    invoke-static {v3}, LX/FP7;->b(Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;)LX/FP4;

    move-result-object v5

    goto :goto_2

    .line 2237476
    :pswitch_5
    invoke-static {v6, v3}, LX/FP7;->b(Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;)LX/FP4;

    move-result-object v5

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static a$redex0(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;LX/FPW;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/16 v3, 0x8

    .line 2237423
    sget-object v0, LX/FPV;->c:[I

    invoke-virtual {p1}, LX/FPW;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 2237424
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid view state for NearbyPlaces Fragment!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2237425
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2237426
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 2237427
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->r:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2237428
    :goto_0
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->u:LX/FPW;

    .line 2237429
    return-void

    .line 2237430
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2237431
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 2237432
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->r:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2237433
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2237434
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->t:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->t:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2237435
    :goto_1
    iget-object v4, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    if-eqz v0, :cond_1

    move v2, v1

    :goto_2
    invoke-virtual {v4, v2}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 2237436
    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->r:Landroid/view/View;

    if-eqz v0, :cond_2

    :goto_3
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    move v0, v1

    .line 2237437
    goto :goto_1

    :cond_1
    move v2, v3

    .line 2237438
    goto :goto_2

    :cond_2
    move v3, v1

    .line 2237439
    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a$redex0(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;)V
    .locals 5

    .prologue
    .line 2237420
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2237421
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->b:LX/FPr;

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4, v0}, LX/FPr;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    .line 2237422
    return-void
.end method

.method public static d(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;I)Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2237407
    if-ltz p1, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2237408
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->s:LX/FPI;

    invoke-virtual {v0}, LX/FPI;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 2237409
    if-eqz v3, :cond_3

    if-ltz p1, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2237410
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->s:LX/FPI;

    invoke-virtual {v0}, LX/FPI;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2237411
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_4

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->t:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    .line 2237412
    iget-object v1, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v0, v1

    .line 2237413
    iget-boolean v1, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->l:Z

    move v0, v1

    .line 2237414
    if-eqz v0, :cond_4

    .line 2237415
    const/4 v0, 0x0

    .line 2237416
    :goto_2
    return-object v0

    :cond_2
    move v0, v2

    .line 2237417
    goto :goto_0

    :cond_3
    move v0, v2

    .line 2237418
    goto :goto_1

    .line 2237419
    :cond_4
    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    goto :goto_2
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2237406
    const-string v0, "nearby_places_result_list"

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 2237403
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->n:LX/FPP;

    if-eqz v0, :cond_0

    .line 2237404
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->n:LX/FPP;

    invoke-interface {v0, p1}, LX/FPP;->a(I)V

    .line 2237405
    :cond_0
    return-void
.end method

.method public final a(LX/FOz;)V
    .locals 0

    .prologue
    .line 2237401
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->j:LX/FOz;

    .line 2237402
    return-void
.end method

.method public final a(LX/FPO;)V
    .locals 0
    .param p1    # LX/FPO;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2237312
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->l:LX/FPO;

    .line 2237313
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2237379
    invoke-super {p0, p1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->a(Landroid/os/Bundle;)V

    .line 2237380
    const-class v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-static {v0, p0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2237381
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->i:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2237382
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->j:LX/FOz;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2237383
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->j:LX/FOz;

    invoke-interface {v0}, LX/FOz;->c()Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->h()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2237384
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->d()Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

    move-result-object v1

    .line 2237385
    if-nez p1, :cond_0

    .line 2237386
    new-instance v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    .line 2237387
    iget-boolean v2, v1, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->e:Z

    move v2, v2

    .line 2237388
    invoke-direct {v0, v2}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;-><init>(Z)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->t:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    .line 2237389
    sget-object v0, LX/FPW;->LOADING_INITIAL_RESULTS:LX/FPW;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->u:LX/FPW;

    .line 2237390
    :goto_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->d:LX/FPJ;

    .line 2237391
    new-instance v2, LX/FPI;

    const/16 v3, 0x2aaa

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-direct {v2, v1, v3}, LX/FPI;-><init>(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;LX/0Ot;)V

    .line 2237392
    move-object v0, v2

    .line 2237393
    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->s:LX/FPI;

    .line 2237394
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->s:LX/FPI;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->t:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    invoke-virtual {v0, v1}, LX/FPI;->a(Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;)V

    .line 2237395
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->s:LX/FPI;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->h:LX/FPU;

    .line 2237396
    iput-object v1, v0, LX/FPI;->d:LX/FPU;

    .line 2237397
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->c:LX/FPb;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->i:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->j:LX/FOz;

    iget-object v3, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->t:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    invoke-virtual {v0, v1, v2, v3}, LX/FPb;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;LX/FOz;Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;)LX/FPa;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->v:LX/FPa;

    .line 2237398
    return-void

    .line 2237399
    :cond_0
    const-string v0, "nearby_places_result_list_model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->t:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    .line 2237400
    const-string v0, "nearby_places_result_list_view_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/FPW;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->u:LX/FPW;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)V
    .locals 0
    .param p1    # Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2237377
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->k:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    .line 2237378
    return-void
.end method

.method public final a(Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;)V
    .locals 0

    .prologue
    .line 2237375
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->i:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2237376
    return-void
.end method

.method public final a(Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;)V
    .locals 2

    .prologue
    .line 2237367
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->t:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    .line 2237368
    iget-object v1, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v0, v1

    .line 2237369
    if-eqz v0, :cond_0

    .line 2237370
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->t:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    .line 2237371
    iget-object v1, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v0, v1

    .line 2237372
    iput-object p1, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

    .line 2237373
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->b()V

    .line 2237374
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2237356
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->t:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    if-nez v0, :cond_1

    .line 2237357
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->w:Z

    .line 2237358
    :cond_0
    :goto_0
    return-void

    .line 2237359
    :cond_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->j:LX/FOz;

    invoke-interface {v0}, LX/FOz;->c()Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-result-object v0

    .line 2237360
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2237361
    iget-boolean v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    move v1, v1

    .line 2237362
    if-eqz v1, :cond_2

    .line 2237363
    iget-object v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->c:Landroid/location/Location;

    move-object v0, v1

    .line 2237364
    if-eqz v0, :cond_0

    .line 2237365
    :cond_2
    sget-object v0, LX/FP6;->RESULT_LIST_REQUEST:LX/FP6;

    invoke-static {p0, v0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->a$redex0(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;LX/FP6;)V

    .line 2237366
    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x56f89c4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2237344
    invoke-super {p0, p1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2237345
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->u:LX/FPW;

    invoke-static {p0, v1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->a$redex0(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;LX/FPW;)V

    .line 2237346
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->j:LX/FOz;

    invoke-interface {v1}, LX/FOz;->c()Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-result-object v1

    .line 2237347
    iget-object v2, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2237348
    if-nez v2, :cond_0

    .line 2237349
    iget-object v2, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->c:Landroid/location/Location;

    move-object v1, v2

    .line 2237350
    if-eqz v1, :cond_1

    .line 2237351
    :cond_0
    sget-object v1, LX/FP6;->RESULT_LIST_REQUEST:LX/FP6;

    invoke-static {p0, v1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->a$redex0(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;LX/FP6;)V

    .line 2237352
    :cond_1
    iget-boolean v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->w:Z

    if-eqz v1, :cond_2

    .line 2237353
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->w:Z

    .line 2237354
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->b()V

    .line 2237355
    :cond_2
    const/16 v1, 0x2b

    const v2, -0x29e23669

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x6e538cb8

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2237329
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->d()Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

    move-result-object v0

    .line 2237330
    iget-boolean v1, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->a:Z

    move v0, v1

    .line 2237331
    if-eqz v0, :cond_0

    const v0, 0x7f030bcc

    .line 2237332
    :goto_0
    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->o:Landroid/view/ViewGroup;

    .line 2237333
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->o:Landroid/view/ViewGroup;

    const v1, 0x7f0d1d49

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    .line 2237334
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->o:Landroid/view/ViewGroup;

    const v1, 0x7f0d1d4b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->p:Landroid/widget/ProgressBar;

    .line 2237335
    const v0, 0x7f03127c

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->o:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->r:Landroid/view/View;

    .line 2237336
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->r:Landroid/view/View;

    const v1, 0x7f0d2b68

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2237337
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->r:Landroid/view/View;

    const v3, 0x7f0d2b69

    invoke-static {v1, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 2237338
    const v3, 0x7f020630

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2237339
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0822d1

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2237340
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->r:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2237341
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->o:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->r:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2237342
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->o:Landroid/view/ViewGroup;

    const v1, 0x643d54ce

    invoke-static {v1, v2}, LX/02F;->f(II)V

    return-object v0

    .line 2237343
    :cond_0
    const v0, 0x7f030bc5

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2237325
    invoke-super {p0, p1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2237326
    const-string v0, "nearby_places_result_list_model"

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->t:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2237327
    const-string v0, "nearby_places_result_list_view_state"

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->u:LX/FPW;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2237328
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6808e2fb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2237321
    invoke-super {p0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->onStart()V

    .line 2237322
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->t:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    invoke-virtual {v1}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->v:LX/FPa;

    if-eqz v1, :cond_0

    .line 2237323
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->v:LX/FPa;

    invoke-virtual {v1}, LX/FPa;->a()V

    .line 2237324
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x7f2d9df4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2237314
    invoke-super {p0, p1, p2}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2237315
    new-instance v0, LX/FPp;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    invoke-direct {v0, v1}, LX/FPp;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->m:LX/FPp;

    .line 2237316
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->m:LX/FPp;

    .line 2237317
    iput-object p0, v0, LX/FPp;->b:LX/FPP;

    .line 2237318
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->s:LX/FPI;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2237319
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->g:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2237320
    return-void
.end method
