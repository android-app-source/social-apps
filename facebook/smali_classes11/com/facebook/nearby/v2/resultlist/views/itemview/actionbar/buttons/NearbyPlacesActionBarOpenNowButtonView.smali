.class public Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;
.super Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final f:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private g:I

.field private h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2239177
    const-class v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;

    sput-object v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;->f:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2239157
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2239158
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2239175
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2239176
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2239168
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2239169
    sget-object v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;->f:Ljava/lang/Class;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2239170
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2239171
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2239172
    const v1, 0x7f0a00d4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;->g:I

    .line 2239173
    const v1, 0x7f0a00d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;->h:I

    .line 2239174
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 3

    .prologue
    .line 2239159
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2239160
    if-eqz p1, :cond_0

    .line 2239161
    iget v1, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;->g:I

    .line 2239162
    const v2, 0x7f08211e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2239163
    :goto_0
    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2239164
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2239165
    return-void

    .line 2239166
    :cond_0
    iget v1, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;->h:I

    .line 2239167
    const v2, 0x7f08211f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
