.class public Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final f:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Landroid/content/Context;

.field public final b:Lcom/facebook/fbui/glyph/GlyphView;

.field public final c:Landroid/widget/TextView;

.field public final d:Landroid/view/View;

.field public e:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2239109
    const-class v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;

    sput-object v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->f:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2239110
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2239111
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2239112
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2239113
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 2239114
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2239115
    const v0, 0x7f030bc7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2239116
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->a:Landroid/content/Context;

    .line 2239117
    sget-object v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->f:Ljava/lang/Class;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2239118
    const v0, 0x7f0d1d53

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->c:Landroid/widget/TextView;

    .line 2239119
    const v0, 0x7f0d1d54

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->d:Landroid/view/View;

    .line 2239120
    const v0, 0x7f0d1d52

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2239121
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->getImageResourceId()I

    move-result v0

    .line 2239122
    if-lez v0, :cond_0

    .line 2239123
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2239124
    :cond_0
    return-void
.end method


# virtual methods
.method public getImageResourceId()I
    .locals 1

    .prologue
    .line 2239125
    const/4 v0, 0x0

    return v0
.end method

.method public setShowRightBorder(Z)V
    .locals 2

    .prologue
    .line 2239126
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->d:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2239127
    return-void

    .line 2239128
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
