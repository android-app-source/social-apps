.class public Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;
.super Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final f:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2239219
    const-class v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;

    sput-object v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;->f:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2239217
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2239218
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2239220
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2239221
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2239212
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2239213
    sget-object v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;->f:Ljava/lang/Class;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2239214
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;->g:I

    .line 2239215
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    iget v1, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;->g:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2239216
    return-void
.end method


# virtual methods
.method public final a(D)V
    .locals 5

    .prologue
    .line 2239208
    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    .line 2239209
    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->c:Landroid/widget/TextView;

    iget v3, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;->g:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2239210
    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->c:Landroid/widget/TextView;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v3}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2239211
    return-void
.end method

.method public getImageResourceId()I
    .locals 1

    .prologue
    .line 2239207
    const v0, 0x7f0209e1

    return v0
.end method
