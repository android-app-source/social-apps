.class public Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2239295
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2239296
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2239297
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2239298
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2239299
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2239300
    return-void
.end method

.method private static varargs a(Ljava/lang/String;[Ljava/lang/String;)Landroid/text/SpannableString;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2239301
    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2239302
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2239303
    :goto_1
    array-length v3, p1

    if-ge v2, v3, :cond_2

    .line 2239304
    aget-object v3, p1, v2

    .line 2239305
    invoke-virtual {p0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 2239306
    if-ltz v4, :cond_0

    .line 2239307
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v4

    .line 2239308
    new-instance v5, Landroid/text/style/StyleSpan;

    invoke-direct {v5, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v6, 0x21

    invoke-virtual {v0, v5, v4, v3, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2239309
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move v0, v2

    .line 2239310
    goto :goto_0

    .line 2239311
    :cond_2
    return-object v0
.end method

.method private static a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel$EdgesModel;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2239312
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel$EdgesModel;->a()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel$EdgesModel$NodeModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2239313
    :cond_0
    const-string v0, ""

    .line 2239314
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel$EdgesModel;->a()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;Ljava/lang/String;)Landroid/text/SpannableString;
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2239315
    if-nez p1, :cond_1

    .line 2239316
    :cond_0
    :goto_0
    return-object v0

    .line 2239317
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;->b()LX/0Px;

    move-result-object v1

    .line 2239318
    if-eqz v1, :cond_0

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2239319
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2239320
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;->a()I

    move-result v3

    .line 2239321
    if-ne v3, v8, :cond_2

    .line 2239322
    invoke-virtual {v1, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel$EdgesModel;

    invoke-static {v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel$EdgesModel;)Ljava/lang/String;

    move-result-object v0

    .line 2239323
    const v1, 0x7f082118    # 1.8094684E38f

    new-array v3, v9, [Ljava/lang/Object;

    aput-object v0, v3, v7

    aput-object p2, v3, v8

    invoke-virtual {v2, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2239324
    new-array v2, v8, [Ljava/lang/String;

    aput-object v0, v2, v7

    invoke-static {v1, v2}, Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v0

    goto :goto_0

    .line 2239325
    :cond_2
    if-ne v3, v9, :cond_3

    .line 2239326
    invoke-virtual {v1, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel$EdgesModel;

    invoke-static {v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel$EdgesModel;)Ljava/lang/String;

    move-result-object v3

    .line 2239327
    invoke-virtual {v1, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel$EdgesModel;

    invoke-static {v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel$EdgesModel;)Ljava/lang/String;

    move-result-object v0

    .line 2239328
    const v1, 0x7f082119

    new-array v4, v10, [Ljava/lang/Object;

    aput-object v3, v4, v7

    aput-object v0, v4, v8

    aput-object p2, v4, v9

    invoke-virtual {v2, v1, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2239329
    new-array v2, v9, [Ljava/lang/String;

    aput-object v3, v2, v7

    aput-object v0, v2, v8

    invoke-static {v1, v2}, Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v0

    goto :goto_0

    .line 2239330
    :cond_3
    if-lt v3, v10, :cond_0

    .line 2239331
    invoke-virtual {v1, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel$EdgesModel;

    invoke-static {v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel$EdgesModel;)Ljava/lang/String;

    move-result-object v4

    .line 2239332
    invoke-virtual {v1, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel$EdgesModel;

    invoke-static {v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel$EdgesModel;)Ljava/lang/String;

    move-result-object v0

    .line 2239333
    add-int/lit8 v1, v3, -0x2

    .line 2239334
    const v3, 0x7f0f0100

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v2, v3, v1, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2239335
    const v3, 0x7f082124

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v4, v5, v7

    aput-object v0, v5, v8

    aput-object v1, v5, v9

    aput-object p2, v5, v10

    invoke-virtual {v2, v3, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2239336
    new-array v3, v10, [Ljava/lang/String;

    aput-object v4, v3, v7

    aput-object v0, v3, v8

    aput-object v1, v3, v9

    invoke-static {v2, v3}, Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2239337
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;->a()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2239338
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2239339
    invoke-direct {p0, p1, p2}, Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;->b(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;->setText(Ljava/lang/CharSequence;)V

    .line 2239340
    return-void

    :cond_0
    move v0, v2

    .line 2239341
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2239342
    goto :goto_1
.end method
