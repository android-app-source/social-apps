.class public Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;

.field private b:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;

.field private c:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarPriceRatingButtonView;

.field private d:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;

.field private e:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarLikesButtonView;

.field private f:Z

.field public g:LX/FQG;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2239107
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2239108
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2239105
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2239106
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2239097
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2239098
    const v0, 0x7f030bc6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2239099
    const v0, 0x7f0d1d4d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->a:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;

    .line 2239100
    const v0, 0x7f0d1d4e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->b:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;

    .line 2239101
    const v0, 0x7f0d1d4f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarPriceRatingButtonView;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->c:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarPriceRatingButtonView;

    .line 2239102
    const v0, 0x7f0d1d50

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->d:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;

    .line 2239103
    const v0, 0x7f0d1d51

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarLikesButtonView;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->e:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarLikesButtonView;

    .line 2239104
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2239086
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->a:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2239087
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->a:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;

    sget-object v1, LX/FQN;->REVIEW:LX/FQN;

    invoke-direct {p0, v1}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->b(LX/FQN;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2239088
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->b:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2239089
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->b:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;

    sget-object v1, LX/FQN;->DISTANCE:LX/FQN;

    invoke-direct {p0, v1}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->b(LX/FQN;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2239090
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->c:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarPriceRatingButtonView;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2239091
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->c:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarPriceRatingButtonView;

    sget-object v1, LX/FQN;->PRICE:LX/FQN;

    invoke-direct {p0, v1}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->b(LX/FQN;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarPriceRatingButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2239092
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->d:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2239093
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->d:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;

    sget-object v1, LX/FQN;->OPEN_NOW:LX/FQN;

    invoke-direct {p0, v1}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->b(LX/FQN;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2239094
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->e:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarLikesButtonView;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2239095
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->e:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarLikesButtonView;

    sget-object v1, LX/FQN;->LIKES:LX/FQN;

    invoke-direct {p0, v1}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->b(LX/FQN;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarLikesButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2239096
    return-void
.end method

.method public static a$redex0(Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;LX/FQN;)V
    .locals 1

    .prologue
    .line 2239083
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->g:LX/FQG;

    if-nez v0, :cond_0

    .line 2239084
    :goto_0
    return-void

    .line 2239085
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->g:LX/FQG;

    invoke-interface {v0, p1}, LX/FQG;->a(LX/FQN;)V

    goto :goto_0
.end method

.method private final b(LX/FQN;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2239082
    new-instance v0, LX/FQM;

    invoke-direct {v0, p0, p1}, LX/FQM;-><init>(Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;LX/FQN;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;Landroid/location/Location;Z)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/16 v7, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2239037
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2239038
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->f:Z

    if-nez v0, :cond_0

    .line 2239039
    invoke-direct {p0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->a()V

    .line 2239040
    iput-boolean v2, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->f:Z

    .line 2239041
    :cond_0
    invoke-static {p1, p2, p3}, LX/FQL;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;Landroid/location/Location;Z)LX/FQL;

    move-result-object v0

    .line 2239042
    iget v4, v0, LX/FQL;->a:I

    .line 2239043
    const/4 v0, 0x0

    .line 2239044
    and-int/lit8 v1, v4, 0x1

    if-lez v1, :cond_3

    .line 2239045
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->n()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->n()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;->c()D

    move-result-wide v0

    .line 2239046
    :goto_0
    iget-object v5, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->a:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;

    invoke-virtual {v5, v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;->a(D)V

    .line 2239047
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->a:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;

    invoke-virtual {v0, v3}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;->setVisibility(I)V

    .line 2239048
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->a:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;

    invoke-virtual {v0, v2}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->setShowRightBorder(Z)V

    .line 2239049
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->a:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;

    move v1, v2

    .line 2239050
    :goto_1
    and-int/lit8 v5, v4, 0x2

    if-lez v5, :cond_4

    .line 2239051
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->b:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->f()Landroid/location/Location;

    move-result-object v5

    invoke-virtual {v0, p2, v5}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;->a(Landroid/location/Location;Landroid/location/Location;)V

    .line 2239052
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->b:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;

    invoke-virtual {v0, v3}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;->setVisibility(I)V

    .line 2239053
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->b:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;

    invoke-virtual {v0, v2}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->setShowRightBorder(Z)V

    .line 2239054
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->b:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;

    .line 2239055
    add-int/lit8 v1, v1, 0x1

    .line 2239056
    :goto_2
    and-int/lit8 v5, v4, 0x4

    if-lez v5, :cond_5

    if-ge v1, v8, :cond_5

    .line 2239057
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->c:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarPriceRatingButtonView;

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarPriceRatingButtonView;->a(Ljava/lang/String;)V

    .line 2239058
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->c:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarPriceRatingButtonView;

    invoke-virtual {v0, v3}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarPriceRatingButtonView;->setVisibility(I)V

    .line 2239059
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->c:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarPriceRatingButtonView;

    invoke-virtual {v0, v2}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->setShowRightBorder(Z)V

    .line 2239060
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->c:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarPriceRatingButtonView;

    .line 2239061
    add-int/lit8 v1, v1, 0x1

    .line 2239062
    :goto_3
    and-int/lit8 v5, v4, 0x8

    if-lez v5, :cond_6

    if-ge v1, v8, :cond_6

    .line 2239063
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->d:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->m()LX/0Px;

    move-result-object v5

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->g()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, LX/FPq;->a(LX/0Px;Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v0, v5}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;->a(Z)V

    .line 2239064
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->d:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;

    invoke-virtual {v0, v3}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;->setVisibility(I)V

    .line 2239065
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->d:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;

    invoke-virtual {v0, v2}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->setShowRightBorder(Z)V

    .line 2239066
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->d:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;

    .line 2239067
    add-int/lit8 v1, v1, 0x1

    .line 2239068
    :goto_4
    and-int/lit8 v4, v4, 0x10

    if-lez v4, :cond_7

    if-ge v1, v8, :cond_7

    .line 2239069
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->e:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarLikesButtonView;

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->j()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarLikesButtonView;->c(I)V

    .line 2239070
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->e:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarLikesButtonView;

    invoke-virtual {v0, v3}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarLikesButtonView;->setVisibility(I)V

    .line 2239071
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->e:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarLikesButtonView;

    invoke-virtual {v0, v2}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->setShowRightBorder(Z)V

    .line 2239072
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->e:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarLikesButtonView;

    .line 2239073
    :goto_5
    if-eqz v0, :cond_1

    .line 2239074
    invoke-virtual {v0, v3}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->setShowRightBorder(Z)V

    .line 2239075
    :cond_1
    return-void

    .line 2239076
    :cond_2
    const-wide/16 v0, 0x0

    goto/16 :goto_0

    .line 2239077
    :cond_3
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->a:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;

    invoke-virtual {v1, v7}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarReviewRatingButtonView;->setVisibility(I)V

    move v1, v3

    goto/16 :goto_1

    .line 2239078
    :cond_4
    iget-object v5, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->b:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;

    invoke-virtual {v5, v7}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;->setVisibility(I)V

    goto :goto_2

    .line 2239079
    :cond_5
    iget-object v5, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->c:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarPriceRatingButtonView;

    invoke-virtual {v5, v7}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarPriceRatingButtonView;->setVisibility(I)V

    goto :goto_3

    .line 2239080
    :cond_6
    iget-object v5, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->d:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;

    invoke-virtual {v5, v7}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarOpenNowButtonView;->setVisibility(I)V

    goto :goto_4

    .line 2239081
    :cond_7
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->e:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarLikesButtonView;

    invoke-virtual {v1, v7}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarLikesButtonView;->setVisibility(I)V

    goto :goto_5
.end method

.method public setOnActionBarItemClickedListener(LX/FQG;)V
    .locals 0

    .prologue
    .line 2239035
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->g:LX/FQG;

    .line 2239036
    return-void
.end method
