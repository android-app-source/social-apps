.class public abstract Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollPhotoHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollPhotoHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2239222
    const-class v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollPhotoHelper;

    sput-object v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollPhotoHelper;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2239223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;ILandroid/content/Context;Landroid/content/res/Resources;)Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 2239224
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2239225
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2239226
    if-nez p0, :cond_1

    .line 2239227
    :cond_0
    :goto_0
    return-object v0

    .line 2239228
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;->jM_()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v1

    .line 2239229
    if-eqz v1, :cond_0

    .line 2239230
    invoke-virtual {v1}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 2239231
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2239232
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2239233
    if-eqz v1, :cond_0

    .line 2239234
    new-instance v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-direct {v0, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 2239235
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, p1, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2239236
    const/16 v3, 0x10

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2239237
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2239238
    const v3, 0x7f0b1518

    invoke-virtual {p3, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    move v3, v3

    .line 2239239
    invoke-virtual {v2, v4, v4, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2239240
    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2239241
    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setMinimumWidth(I)V

    .line 2239242
    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setMaxWidth(I)V

    .line 2239243
    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setMinimumHeight(I)V

    .line 2239244
    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setMaxHeight(I)V

    .line 2239245
    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 2239246
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2239247
    const v5, 0x7f0b1517

    invoke-virtual {p3, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v5

    double-to-int v5, v5

    move v2, v5

    .line 2239248
    invoke-virtual {v0, v2, v2, v2, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setPadding(IIII)V

    .line 2239249
    const v2, 0x7f0210c4

    move v2, v2

    .line 2239250
    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setBackgroundResource(I)V

    .line 2239251
    sget-object v2, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollPhotoHelper;->a:Ljava/lang/Class;

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2239252
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setClickable(Z)V

    .line 2239253
    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->requestLayout()V

    goto :goto_0
.end method
