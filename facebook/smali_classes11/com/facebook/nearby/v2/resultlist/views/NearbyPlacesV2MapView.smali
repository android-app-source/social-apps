.class public Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/FQ1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0y3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Lcom/facebook/maps/FbMapViewDelegate;

.field private d:Lcom/facebook/widget/CustomViewPager;

.field private e:LX/68w;

.field private f:LX/68w;

.field public g:I

.field public h:LX/0Rh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rh",
            "<",
            "LX/6ax;",
            "Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/6ax;

.field public j:LX/FPQ;

.field public k:I

.field public l:I

.field private m:I

.field private final n:LX/6Zz;

.field private final o:LX/6Zz;

.field private final p:LX/6Zz;

.field private final q:LX/6Zz;

.field private final r:LX/0hc;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2238207
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2238208
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->k:I

    .line 2238209
    new-instance v0, LX/FPu;

    invoke-direct {v0, p0}, LX/FPu;-><init>(Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->n:LX/6Zz;

    .line 2238210
    new-instance v0, LX/FPv;

    invoke-direct {v0, p0}, LX/FPv;-><init>(Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->o:LX/6Zz;

    .line 2238211
    new-instance v0, LX/FPw;

    invoke-direct {v0, p0}, LX/FPw;-><init>(Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->p:LX/6Zz;

    .line 2238212
    new-instance v0, LX/FPx;

    invoke-direct {v0, p0}, LX/FPx;-><init>(Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->q:LX/6Zz;

    .line 2238213
    new-instance v0, LX/FPy;

    invoke-direct {v0, p0}, LX/FPy;-><init>(Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->r:LX/0hc;

    .line 2238214
    invoke-direct {p0}, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->g()V

    .line 2238215
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2238347
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2238348
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->k:I

    .line 2238349
    new-instance v0, LX/FPu;

    invoke-direct {v0, p0}, LX/FPu;-><init>(Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->n:LX/6Zz;

    .line 2238350
    new-instance v0, LX/FPv;

    invoke-direct {v0, p0}, LX/FPv;-><init>(Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->o:LX/6Zz;

    .line 2238351
    new-instance v0, LX/FPw;

    invoke-direct {v0, p0}, LX/FPw;-><init>(Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->p:LX/6Zz;

    .line 2238352
    new-instance v0, LX/FPx;

    invoke-direct {v0, p0}, LX/FPx;-><init>(Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->q:LX/6Zz;

    .line 2238353
    new-instance v0, LX/FPy;

    invoke-direct {v0, p0}, LX/FPy;-><init>(Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->r:LX/0hc;

    .line 2238354
    invoke-direct {p0}, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->g()V

    .line 2238355
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2238338
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2238339
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->k:I

    .line 2238340
    new-instance v0, LX/FPu;

    invoke-direct {v0, p0}, LX/FPu;-><init>(Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->n:LX/6Zz;

    .line 2238341
    new-instance v0, LX/FPv;

    invoke-direct {v0, p0}, LX/FPv;-><init>(Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->o:LX/6Zz;

    .line 2238342
    new-instance v0, LX/FPw;

    invoke-direct {v0, p0}, LX/FPw;-><init>(Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->p:LX/6Zz;

    .line 2238343
    new-instance v0, LX/FPx;

    invoke-direct {v0, p0}, LX/FPx;-><init>(Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->q:LX/6Zz;

    .line 2238344
    new-instance v0, LX/FPy;

    invoke-direct {v0, p0}, LX/FPy;-><init>(Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->r:LX/0hc;

    .line 2238345
    invoke-direct {p0}, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->g()V

    .line 2238346
    return-void
.end method

.method private static a(Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;LX/FQ1;LX/0y3;)V
    .locals 0

    .prologue
    .line 2238337
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->a:LX/FQ1;

    iput-object p2, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->b:LX/0y3;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    new-instance p1, LX/FQ1;

    invoke-static {v1}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-static {v1}, LX/FQA;->a(LX/0QB;)LX/FQA;

    move-result-object v2

    check-cast v2, LX/FQA;

    invoke-direct {p1, v0, v2}, LX/FQ1;-><init>(LX/0Sh;LX/FQA;)V

    move-object v0, p1

    check-cast v0, LX/FQ1;

    invoke-static {v1}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v1

    check-cast v1, LX/0y3;

    invoke-static {p0, v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->a(Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;LX/FQ1;LX/0y3;)V

    return-void
.end method

.method private a(LX/6ax;I)Z
    .locals 5
    .param p1    # LX/6ax;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/high16 v4, 0x3f000000    # 0.5f

    .line 2238322
    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->i:LX/6ax;

    if-ne p1, v2, :cond_0

    .line 2238323
    :goto_0
    return v0

    .line 2238324
    :cond_0
    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->i:LX/6ax;

    if-eqz v2, :cond_1

    .line 2238325
    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->i:LX/6ax;

    iget-object v3, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->e:LX/68w;

    invoke-virtual {v2, v3}, LX/6ax;->a(LX/68w;)V

    .line 2238326
    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->i:LX/6ax;

    invoke-virtual {v2, v4, v4}, LX/6ax;->a(FF)V

    .line 2238327
    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->i:LX/6ax;

    invoke-virtual {v2}, LX/6ax;->e()V

    .line 2238328
    :cond_1
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->i:LX/6ax;

    .line 2238329
    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->i:LX/6ax;

    if-nez v2, :cond_2

    move v0, v1

    .line 2238330
    goto :goto_0

    .line 2238331
    :cond_2
    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->i:LX/6ax;

    iget-object v3, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->f:LX/68w;

    invoke-virtual {v2, v3}, LX/6ax;->a(LX/68w;)V

    .line 2238332
    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->i:LX/6ax;

    const v3, 0x3f4ccccd    # 0.8f

    invoke-virtual {v2, v4, v3}, LX/6ax;->a(FF)V

    .line 2238333
    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->i:LX/6ax;

    invoke-virtual {v2}, LX/6ax;->d()V

    .line 2238334
    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->d:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v2, p2, v0}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 2238335
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->c:Lcom/facebook/maps/FbMapViewDelegate;

    new-instance v2, LX/FPz;

    invoke-direct {v2, p0}, LX/FPz;-><init>(Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;)V

    invoke-virtual {v0, v2}, LX/6Zn;->a(LX/6Zz;)V

    move v0, v1

    .line 2238336
    goto :goto_0
.end method

.method public static synthetic a(Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;LX/6ax;I)Z
    .locals 1

    .prologue
    .line 2238321
    invoke-direct {p0, p1, p2}, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->a(LX/6ax;I)Z

    move-result v0

    return v0
.end method

.method public static a$redex0(Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;LX/6al;)V
    .locals 14

    .prologue
    .line 2238288
    invoke-virtual {p1}, LX/6al;->a()V

    .line 2238289
    invoke-static {}, LX/0Rh;->d()LX/4y1;

    move-result-object v3

    .line 2238290
    iget v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->g:I

    iget v1, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->k:I

    invoke-static {p1, v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->b(LX/6al;II)V

    .line 2238291
    new-instance v4, LX/696;

    invoke-direct {v4}, LX/696;-><init>()V

    .line 2238292
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->a:LX/FQ1;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v5

    .line 2238293
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_3

    .line 2238294
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->a:LX/FQ1;

    invoke-virtual {v0, v2}, LX/FQ1;->a(I)Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    move-result-object v6

    .line 2238295
    if-eqz v6, :cond_0

    .line 2238296
    invoke-virtual {v6}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->f()Landroid/location/Location;

    move-result-object v7

    .line 2238297
    if-eqz v7, :cond_0

    .line 2238298
    if-nez v2, :cond_1

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->f:LX/68w;

    move-object v1, v0

    .line 2238299
    :goto_1
    if-nez v2, :cond_2

    const v0, 0x3f4ccccd    # 0.8f

    .line 2238300
    :goto_2
    new-instance v8, LX/699;

    invoke-direct {v8}, LX/699;-><init>()V

    new-instance v9, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v7}, Landroid/location/Location;->getLatitude()D

    move-result-wide v10

    invoke-virtual {v7}, Landroid/location/Location;->getLongitude()D

    move-result-wide v12

    invoke-direct {v9, v10, v11, v12, v13}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    .line 2238301
    iput-object v9, v8, LX/699;->b:Lcom/facebook/android/maps/model/LatLng;

    .line 2238302
    move-object v7, v8

    .line 2238303
    invoke-virtual {v6}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a()Ljava/lang/String;

    move-result-object v8

    .line 2238304
    iput-object v8, v7, LX/699;->h:Ljava/lang/String;

    .line 2238305
    move-object v7, v7

    .line 2238306
    iput-object v1, v7, LX/699;->c:LX/68w;

    .line 2238307
    move-object v1, v7

    .line 2238308
    const/high16 v7, 0x3f000000    # 0.5f

    invoke-virtual {v1, v7, v0}, LX/699;->a(FF)LX/699;

    move-result-object v0

    .line 2238309
    invoke-virtual {p1, v0}, LX/6al;->a(LX/699;)LX/6ax;

    move-result-object v1

    .line 2238310
    invoke-virtual {v3, v1, v6}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    .line 2238311
    iget-object v6, v0, LX/699;->b:Lcom/facebook/android/maps/model/LatLng;

    move-object v0, v6

    .line 2238312
    invoke-virtual {v4, v0}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    .line 2238313
    if-nez v2, :cond_0

    .line 2238314
    iput-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->i:LX/6ax;

    .line 2238315
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2238316
    :cond_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->e:LX/68w;

    move-object v1, v0

    goto :goto_1

    .line 2238317
    :cond_2
    const/high16 v0, 0x3f000000    # 0.5f

    goto :goto_2

    .line 2238318
    :cond_3
    invoke-virtual {v3}, LX/4y1;->a()LX/0Rh;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->h:LX/0Rh;

    .line 2238319
    invoke-virtual {v4}, LX/696;->a()LX/697;

    move-result-object v0

    iget v1, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->m:I

    invoke-static {v0, v1}, LX/6aN;->a(LX/697;I)LX/6aM;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/6al;->a(LX/6aM;)V

    .line 2238320
    return-void
.end method

.method public static b(LX/6al;II)V
    .locals 1

    .prologue
    .line 2238286
    add-int v0, p1, p2

    invoke-virtual {p0, p1, p1, p1, v0}, LX/6al;->a(IIII)V

    .line 2238287
    return-void
.end method

.method private g()V
    .locals 4

    .prologue
    .line 2238267
    const-class v0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;

    invoke-static {v0, p0}, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2238268
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2238269
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2238270
    const v2, 0x10e0001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->l:I

    .line 2238271
    const v2, 0x7f0b152b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    const v3, 0x7f0b152c

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->m:I

    .line 2238272
    invoke-static {v0}, LX/690;->a(Landroid/content/Context;)V

    .line 2238273
    const v0, 0x7f0210b7

    invoke-static {v0}, LX/690;->a(I)LX/68w;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->e:LX/68w;

    .line 2238274
    const v0, 0x7f0210b8

    invoke-static {v0}, LX/690;->a(I)LX/68w;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->f:LX/68w;

    .line 2238275
    const v0, 0x7f0b152c

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->g:I

    .line 2238276
    const v0, 0x7f030bc4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2238277
    const v0, 0x7f0d1d47

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/maps/FbMapViewDelegate;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->c:Lcom/facebook/maps/FbMapViewDelegate;

    .line 2238278
    const v0, 0x7f0d1d48

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomViewPager;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->d:Lcom/facebook/widget/CustomViewPager;

    .line 2238279
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->d:Lcom/facebook/widget/CustomViewPager;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 2238280
    const v0, 0x7f0b0060

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 2238281
    mul-int/lit8 v0, v0, -0x3

    .line 2238282
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->d:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 2238283
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->d:Lcom/facebook/widget/CustomViewPager;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->r:LX/0hc;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 2238284
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->d:Lcom/facebook/widget/CustomViewPager;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->a:LX/FQ1;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2238285
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2238264
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->c:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0}, LX/6Zn;->d()V

    .line 2238265
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->c:Lcom/facebook/maps/FbMapViewDelegate;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->o:LX/6Zz;

    invoke-virtual {v0, v1}, LX/6Zn;->a(LX/6Zz;)V

    .line 2238266
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2238261
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->c:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0, p1}, LX/6Zn;->a(Landroid/os/Bundle;)V

    .line 2238262
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->c:Lcom/facebook/maps/FbMapViewDelegate;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->n:LX/6Zz;

    invoke-virtual {v0, v1}, LX/6Zn;->a(LX/6Zz;)V

    .line 2238263
    return-void
.end method

.method public final a(Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;)V
    .locals 6

    .prologue
    .line 2238241
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->a:LX/FQ1;

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 2238242
    if-eqz p1, :cond_0

    .line 2238243
    iget-object v2, p1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v2, v2

    .line 2238244
    if-eqz v2, :cond_0

    .line 2238245
    iget-object v2, p1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v2, v2

    .line 2238246
    invoke-virtual {v2}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->h()Ljava/util/ArrayList;

    move-result-object v2

    if-nez v2, :cond_2

    .line 2238247
    :cond_0
    iput-object v3, v0, LX/FQ1;->b:Ljava/util/List;

    .line 2238248
    iput-object v3, v0, LX/FQ1;->c:Ljava/util/Map;

    .line 2238249
    iput-boolean v1, v0, LX/FQ1;->d:Z

    .line 2238250
    :cond_1
    invoke-virtual {v0}, LX/0gG;->kV_()V

    .line 2238251
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->c:Lcom/facebook/maps/FbMapViewDelegate;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->q:LX/6Zz;

    invoke-virtual {v0, v1}, LX/6Zn;->a(LX/6Zz;)V

    .line 2238252
    return-void

    .line 2238253
    :cond_2
    iget-object v2, p1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v2, v2

    .line 2238254
    invoke-virtual {v2}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->h()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, v0, LX/FQ1;->b:Ljava/util/List;

    .line 2238255
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->d()Z

    move-result v2

    iput-boolean v2, v0, LX/FQ1;->d:Z

    .line 2238256
    iget-object v2, v0, LX/FQ1;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 2238257
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v3, v0, LX/FQ1;->c:Ljava/util/Map;

    .line 2238258
    :goto_0
    if-ge v1, v2, :cond_1

    .line 2238259
    iget-object v3, v0, LX/FQ1;->c:Ljava/util/Map;

    iget-object v4, v0, LX/FQ1;->b:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2238260
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2238239
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->c:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0}, LX/6Zn;->c()V

    .line 2238240
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2238237
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->c:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0, p1}, LX/6Zn;->b(Landroid/os/Bundle;)V

    .line 2238238
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2238235
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->c:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0}, LX/6Zn;->a()V

    .line 2238236
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2238233
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->c:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0}, LX/6Zn;->b()V

    .line 2238234
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 2238224
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomFrameLayout;->onLayout(ZIIII)V

    .line 2238225
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->d:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0}, Lcom/facebook/widget/CustomViewPager;->getHeight()I

    move-result v0

    .line 2238226
    iget v1, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->k:I

    if-eq v1, v0, :cond_1

    .line 2238227
    iput v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->k:I

    .line 2238228
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->c:Lcom/facebook/maps/FbMapViewDelegate;

    if-eqz v0, :cond_0

    .line 2238229
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->c:Lcom/facebook/maps/FbMapViewDelegate;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->p:LX/6Zz;

    invoke-virtual {v0, v1}, LX/6Zn;->a(LX/6Zz;)V

    .line 2238230
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->j:LX/FPQ;

    if-eqz v0, :cond_1

    .line 2238231
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->j:LX/FPQ;

    iget v1, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->k:I

    invoke-interface {v0, v1}, LX/FPQ;->z_(I)V

    .line 2238232
    :cond_1
    return-void
.end method

.method public setOnPlaceClickedListener(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;)V
    .locals 1

    .prologue
    .line 2238221
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->a:LX/FQ1;

    .line 2238222
    iput-object p1, v0, LX/FQ1;->f:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;

    .line 2238223
    return-void
.end method

.method public setOnPlacePinSelectedListener(LX/FPR;)V
    .locals 1

    .prologue
    .line 2238218
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->a:LX/FQ1;

    .line 2238219
    iput-object p1, v0, LX/FQ1;->e:LX/FPR;

    .line 2238220
    return-void
.end method

.method public setOnViewPagerHeightChangeListener(LX/FPQ;)V
    .locals 0
    .param p1    # LX/FPQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2238216
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/views/NearbyPlacesV2MapView;->j:LX/FPQ;

    .line 2238217
    return-void
.end method
