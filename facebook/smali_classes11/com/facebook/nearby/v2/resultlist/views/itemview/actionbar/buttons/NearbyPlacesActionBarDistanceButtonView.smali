.class public Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;
.super Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final g:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public f:LX/6aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2239135
    const-class v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;

    sput-object v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;->g:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2239136
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2239137
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2239133
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2239134
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2239138
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2239139
    const-class v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;

    invoke-static {v0, p0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2239140
    sget-object v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;->g:Ljava/lang/Class;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2239141
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2239142
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;

    invoke-static {v0}, LX/6aG;->b(LX/0QB;)LX/6aG;

    move-result-object v0

    check-cast v0, LX/6aG;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;->f:LX/6aG;

    return-void
.end method


# virtual methods
.method public final a(Landroid/location/Location;Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 2239129
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 2239130
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->c:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2239131
    :goto_0
    return-void

    .line 2239132
    :cond_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarDistanceButtonView;->f:LX/6aG;

    invoke-virtual {v1, p1, p2}, LX/6aG;->a(Landroid/location/Location;Landroid/location/Location;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
