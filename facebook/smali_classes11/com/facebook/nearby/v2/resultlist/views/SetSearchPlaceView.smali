.class public Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/FQ2;


# static fields
.field public static final e:Ljava/lang/String;

.field public static final f:LX/FQ9;

.field private static final g:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:Lcom/facebook/resources/ui/EllipsizingTextView;

.field private B:Lcom/facebook/resources/ui/EllipsizingTextView;

.field private C:Lcom/facebook/resources/ui/EllipsizingTextView;

.field public D:Lcom/facebook/resources/ui/EllipsizingTextView;

.field private E:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private F:Lcom/facebook/resources/ui/EllipsizingTextView;

.field public G:Landroid/widget/LinearLayout;

.field public H:LX/FQ9;

.field private I:Landroid/view/View$OnClickListener;

.field private J:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;

.field public K:LX/FPC;

.field public L:Ljava/lang/String;

.field private M:Ljava/lang/String;

.field public N:Lcom/facebook/graphql/enums/GraphQLSavedState;

.field private O:D

.field private P:I

.field private Q:Ljava/lang/String;

.field private R:Ljava/lang/String;

.field private S:Ljava/lang/Boolean;

.field private T:Ljava/lang/String;

.field private U:Ljava/lang/String;

.field private V:Ljava/lang/String;

.field public W:Ljava/lang/CharSequence;

.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private aa:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ab:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ac:Ljava/lang/String;

.field private ad:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3rL",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field public ae:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field public final af:Landroid/view/View$OnClickListener;

.field public b:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5up;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Landroid/text/style/ForegroundColorSpan;

.field public i:Landroid/text/style/ForegroundColorSpan;

.field public j:Landroid/text/style/ForegroundColorSpan;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Landroid/text/style/ImageSpan;

.field public n:I

.field private o:Landroid/graphics/drawable/Drawable;

.field private p:Landroid/graphics/drawable/Drawable;

.field public q:Landroid/view/ViewStub;

.field public r:Landroid/view/ViewStub;

.field public s:Landroid/view/ViewStub;

.field public t:Landroid/view/ViewStub;

.field public u:Landroid/view/ViewStub;

.field public v:Landroid/view/ViewStub;

.field public w:Landroid/view/ViewStub;

.field public x:Landroid/view/ViewStub;

.field private y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public z:Lcom/facebook/resources/ui/FbImageButton;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2238735
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->e:Ljava/lang/String;

    .line 2238736
    sget-object v0, LX/FQ9;->NONE:LX/FQ9;

    sput-object v0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->f:LX/FQ9;

    .line 2238737
    const-class v0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->g:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2238733
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2238734
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2238731
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2238732
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    .line 2238669
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2238670
    new-instance v0, LX/FQ5;

    invoke-direct {v0, p0}, LX/FQ5;-><init>(Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->af:Landroid/view/View$OnClickListener;

    .line 2238671
    const-class v0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    invoke-static {v0, p0}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2238672
    const/4 v4, 0x0

    .line 2238673
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2238674
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const v2, 0x7f0a008a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iput-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->h:Landroid/text/style/ForegroundColorSpan;

    .line 2238675
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const v2, 0x7f0a00d4

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iput-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->i:Landroid/text/style/ForegroundColorSpan;

    .line 2238676
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const v2, 0x7f0a00d3

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iput-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->j:Landroid/text/style/ForegroundColorSpan;

    .line 2238677
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->b:LX/0wM;

    const v2, 0x7f0209e3

    const v3, -0xbd984e

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2238678
    if-eqz v1, :cond_0

    .line 2238679
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2238680
    new-instance v2, Landroid/text/style/ImageSpan;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    iput-object v2, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->m:Landroid/text/style/ImageSpan;

    .line 2238681
    :cond_0
    const v1, 0x7f08211e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->k:Ljava/lang/String;

    .line 2238682
    const v1, 0x7f08211f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->l:Ljava/lang/String;

    .line 2238683
    const v1, 0x7f0c004f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->n:I

    .line 2238684
    const/4 v2, 0x0

    .line 2238685
    const v0, 0x7f0312fd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2238686
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->setOrientation(I)V

    .line 2238687
    const v0, 0x7f0d2c2d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/EllipsizingTextView;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->A:Lcom/facebook/resources/ui/EllipsizingTextView;

    .line 2238688
    const v0, 0x7f0d2c2a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->q:Landroid/view/ViewStub;

    .line 2238689
    const v0, 0x7f0d2c34

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->r:Landroid/view/ViewStub;

    .line 2238690
    const v0, 0x7f0d2c2e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->s:Landroid/view/ViewStub;

    .line 2238691
    const v0, 0x7f0d2c30

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->t:Landroid/view/ViewStub;

    .line 2238692
    const v0, 0x7f0d2c32

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->u:Landroid/view/ViewStub;

    .line 2238693
    const v0, 0x7f0d2c36

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->v:Landroid/view/ViewStub;

    .line 2238694
    const v0, 0x7f0d2c38

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->w:Landroid/view/ViewStub;

    .line 2238695
    const v0, 0x7f0d2c3a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->x:Landroid/view/ViewStub;

    .line 2238696
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0060

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 2238697
    invoke-static {p0, v2, v0, v2, v0}, LX/0vv;->b(Landroid/view/View;IIII)V

    .line 2238698
    const/4 v1, 0x0

    .line 2238699
    sget-object v0, LX/03r;->SetSearchPlaceView:[I

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 2238700
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v3

    move v0, v1

    .line 2238701
    :goto_0
    if-ge v0, v3, :cond_9

    .line 2238702
    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v4

    .line 2238703
    const/16 v5, 0x1

    if-ne v4, v5, :cond_2

    .line 2238704
    const/4 v5, 0x1

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    .line 2238705
    invoke-virtual {p0, v4}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    .line 2238706
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2238707
    :cond_2
    const/16 v5, 0x2

    if-ne v4, v5, :cond_3

    .line 2238708
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    .line 2238709
    invoke-virtual {p0, v4}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->b(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    goto :goto_1

    .line 2238710
    :cond_3
    const/16 v5, 0x3

    if-ne v4, v5, :cond_4

    .line 2238711
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    .line 2238712
    invoke-virtual {p0, v4}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->c(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    goto :goto_1

    .line 2238713
    :cond_4
    const/16 v5, 0x4

    if-ne v4, v5, :cond_5

    .line 2238714
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    .line 2238715
    invoke-virtual {p0, v4}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->d(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    goto :goto_1

    .line 2238716
    :cond_5
    const/16 v5, 0x5

    if-ne v4, v5, :cond_6

    .line 2238717
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    .line 2238718
    invoke-virtual {p0, v4}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->e(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    goto :goto_1

    .line 2238719
    :cond_6
    const/16 v5, 0x6

    if-ne v4, v5, :cond_7

    .line 2238720
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    .line 2238721
    invoke-virtual {p0, v4}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->f(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    goto :goto_1

    .line 2238722
    :cond_7
    const/16 v5, 0x7

    if-ne v4, v5, :cond_8

    .line 2238723
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    .line 2238724
    invoke-virtual {p0, v4}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->g(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    goto :goto_1

    .line 2238725
    :cond_8
    const/16 v5, 0x0

    if-ne v4, v5, :cond_1

    .line 2238726
    const/16 v4, 0x0

    const/4 v5, -0x1

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    sget-object v5, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->f:LX/FQ9;

    invoke-static {v4, v5}, LX/FQ9;->fromValue(ILX/FQ9;)LX/FQ9;

    move-result-object v4

    .line 2238727
    invoke-virtual {p0, v4}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(LX/FQ9;)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    goto :goto_1

    .line 2238728
    :cond_9
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 2238729
    new-instance v0, LX/FQ6;

    invoke-direct {v0, p0, p0}, LX/FQ6;-><init>(Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->I:Landroid/view/View$OnClickListener;

    .line 2238730
    return-void
.end method

.method public static a(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 2238665
    if-eqz p0, :cond_0

    .line 2238666
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2238667
    :cond_0
    return-void

    .line 2238668
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    const/16 v1, 0x259

    invoke-static {v2, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v2}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    const/16 v4, 0x12c4

    invoke-static {v2, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x329d

    invoke-static {v2, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    iput-object v3, p1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a:LX/0Ot;

    iput-object v1, p1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->b:LX/0wM;

    iput-object v4, p1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->c:LX/0Ot;

    iput-object v2, p1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->d:LX/0Ot;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    .locals 6

    .prologue
    .line 2238662
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5up;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne p2, v1, :cond_0

    sget-object v1, LX/5uo;->SAVE:LX/5uo;

    :goto_0
    const-string v3, "native_search"

    const-string v4, "toggle_button"

    new-instance v5, LX/FQ7;

    invoke-direct {v5, p0}, LX/FQ7;-><init>(Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;)V

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, LX/5up;->a(LX/5uo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    .line 2238663
    return-void

    .line 2238664
    :cond_0
    sget-object v1, LX/5uo;->UNSAVE:LX/5uo;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/FQ9;)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2238651
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->H:LX/FQ9;

    .line 2238652
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->H:LX/FQ9;

    if-eqz v0, :cond_1

    sget-object v0, LX/FQ9;->NONE:LX/FQ9;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->H:LX/FQ9;

    invoke-virtual {v0, v1}, LX/FQ9;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v1, v2

    .line 2238653
    :goto_0
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->z:Lcom/facebook/resources/ui/FbImageButton;

    if-nez v0, :cond_0

    .line 2238654
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->r:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbImageButton;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->z:Lcom/facebook/resources/ui/FbImageButton;

    .line 2238655
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->r:Landroid/view/ViewStub;

    .line 2238656
    :cond_0
    if-eqz v1, :cond_2

    .line 2238657
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->z:Lcom/facebook/resources/ui/FbImageButton;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->I:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2238658
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->z:Lcom/facebook/resources/ui/FbImageButton;

    invoke-static {v0, v2}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    .line 2238659
    :goto_1
    return-object p0

    :cond_1
    move v1, v3

    .line 2238660
    goto :goto_0

    .line 2238661
    :cond_2
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->z:Lcom/facebook/resources/ui/FbImageButton;

    invoke-static {v0, v3}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    goto :goto_1
.end method

.method public final a(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;
    .locals 2

    .prologue
    .line 2238644
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez v0, :cond_0

    .line 2238645
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->q:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2238646
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->q:Landroid/view/ViewStub;

    .line 2238647
    :cond_0
    if-eqz p1, :cond_1

    .line 2238648
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->M:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->setThumbnailUrl(Ljava/lang/String;)V

    .line 2238649
    :goto_0
    return-object p0

    .line 2238650
    :cond_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method public final a(DILjava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 9

    .prologue
    .line 2238607
    iput-wide p1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->O:D

    .line 2238608
    iput p3, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->P:I

    .line 2238609
    iput-object p4, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->Q:Ljava/lang/String;

    .line 2238610
    iput-object p5, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->R:Ljava/lang/String;

    .line 2238611
    iput-object p6, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->S:Ljava/lang/Boolean;

    .line 2238612
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->B:Lcom/facebook/resources/ui/EllipsizingTextView;

    if-eqz v0, :cond_7

    .line 2238613
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2238614
    iget-wide v2, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->O:D

    const-wide/16 v4, 0x0

    cmpl-double v0, v2, v4

    if-lez v0, :cond_0

    .line 2238615
    const-string v0, "%.1f"

    iget-wide v2, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->O:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2238616
    iget v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->P:I

    if-lez v0, :cond_0

    .line 2238617
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 2238618
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    .line 2238619
    const-string v3, "*"

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/text/NumberFormat;->getNumberInstance()Ljava/text/NumberFormat;

    move-result-object v4

    iget v5, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->P:I

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2238620
    iget-object v3, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->h:Landroid/text/style/ForegroundColorSpan;

    const/4 v4, 0x0

    const/16 v5, 0x12

    invoke-virtual {v1, v3, v4, v0, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2238621
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->m:Landroid/text/style/ImageSpan;

    add-int/lit8 v3, v2, 0x1

    const/16 v4, 0x12

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2238622
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->Q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2238623
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 2238624
    const-string v0, " \u2022 "

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2238625
    :cond_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->Q:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2238626
    :cond_2
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->R:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2238627
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 2238628
    const-string v0, " \u2022 "

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2238629
    :cond_3
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->R:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2238630
    :cond_4
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->S:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 2238631
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_5

    .line 2238632
    const-string v0, " \u2022 "

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2238633
    :cond_5
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    .line 2238634
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->S:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->k:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2238635
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    .line 2238636
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->S:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->i:Landroid/text/style/ForegroundColorSpan;

    :goto_1
    const/16 v4, 0x12

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2238637
    :cond_6
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_a

    .line 2238638
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->B:Lcom/facebook/resources/ui/EllipsizingTextView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    .line 2238639
    :cond_7
    :goto_2
    return-void

    .line 2238640
    :cond_8
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->l:Ljava/lang/String;

    goto :goto_0

    .line 2238641
    :cond_9
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->j:Landroid/text/style/ForegroundColorSpan;

    goto :goto_1

    .line 2238642
    :cond_a
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->B:Lcom/facebook/resources/ui/EllipsizingTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/EllipsizingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2238643
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->B:Lcom/facebook/resources/ui/EllipsizingTextView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2238586
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->T:Ljava/lang/String;

    .line 2238587
    iput-object p2, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->U:Ljava/lang/String;

    .line 2238588
    iput-object p3, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->V:Ljava/lang/String;

    .line 2238589
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->C:Lcom/facebook/resources/ui/EllipsizingTextView;

    if-eqz v0, :cond_5

    .line 2238590
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2238591
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->V:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2238592
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->V:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2238593
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->T:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2238594
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 2238595
    const-string v0, " \u2022 "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2238596
    :cond_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->T:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2238597
    :cond_2
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->U:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2238598
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 2238599
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->T:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, " \u2022 "

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2238600
    :cond_3
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->U:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2238601
    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_7

    .line 2238602
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->C:Lcom/facebook/resources/ui/EllipsizingTextView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    .line 2238603
    :cond_5
    :goto_1
    return-void

    .line 2238604
    :cond_6
    const-string v0, ", "

    goto :goto_0

    .line 2238605
    :cond_7
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->C:Lcom/facebook/resources/ui/EllipsizingTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/EllipsizingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2238606
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->C:Lcom/facebook/resources/ui/EllipsizingTextView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    goto :goto_1
.end method

.method public final a(Ljava/util/List;Ljava/lang/String;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/3rL",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 2238738
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->ab:Ljava/util/List;

    .line 2238739
    iput-object p2, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->ac:Ljava/lang/String;

    .line 2238740
    iput-object p3, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->ad:Ljava/util/List;

    .line 2238741
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->F:Lcom/facebook/resources/ui/EllipsizingTextView;

    if-eqz v0, :cond_3

    .line 2238742
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2238743
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->ac:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2238744
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->ac:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2238745
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->ad:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 2238746
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->ad:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    .line 2238747
    new-instance v4, Landroid/text/style/StyleSpan;

    invoke-direct {v4, v6}, Landroid/text/style/StyleSpan;-><init>(I)V

    iget-object v1, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v1, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v0, v0, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v1

    const/16 v1, 0x11

    invoke-virtual {v2, v4, v5, v0, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 2238748
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->ab:Ljava/util/List;

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2238749
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 2238750
    const-string v0, " "

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2238751
    :cond_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->ab:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 2238752
    packed-switch v1, :pswitch_data_0

    .line 2238753
    const v0, 0x7f08212d

    .line 2238754
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->ab:Ljava/util/List;

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v4, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2238755
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2238756
    :cond_2
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_4

    .line 2238757
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->F:Lcom/facebook/resources/ui/EllipsizingTextView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    .line 2238758
    :cond_3
    :goto_2
    return-void

    .line 2238759
    :pswitch_0
    const v0, 0x7f08212b

    goto :goto_1

    .line 2238760
    :pswitch_1
    const v0, 0x7f08212c

    goto :goto_1

    .line 2238761
    :cond_4
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->F:Lcom/facebook/resources/ui/EllipsizingTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/EllipsizingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2238762
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->F:Lcom/facebook/resources/ui/EllipsizingTextView;

    invoke-static {v0, v6}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;
    .locals 8

    .prologue
    .line 2238487
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->B:Lcom/facebook/resources/ui/EllipsizingTextView;

    if-nez v0, :cond_0

    .line 2238488
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->s:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/EllipsizingTextView;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->B:Lcom/facebook/resources/ui/EllipsizingTextView;

    .line 2238489
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->s:Landroid/view/ViewStub;

    .line 2238490
    :cond_0
    if-eqz p1, :cond_1

    .line 2238491
    iget-wide v2, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->O:D

    iget v4, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->P:I

    iget-object v5, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->Q:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->R:Ljava/lang/String;

    iget-object v7, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->S:Ljava/lang/Boolean;

    move-object v1, p0

    invoke-virtual/range {v1 .. v7}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(DILjava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2238492
    :goto_0
    return-object p0

    .line 2238493
    :cond_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->B:Lcom/facebook/resources/ui/EllipsizingTextView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method public final c(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;
    .locals 3

    .prologue
    .line 2238507
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->C:Lcom/facebook/resources/ui/EllipsizingTextView;

    if-nez v0, :cond_0

    .line 2238508
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->t:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/EllipsizingTextView;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->C:Lcom/facebook/resources/ui/EllipsizingTextView;

    .line 2238509
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->t:Landroid/view/ViewStub;

    .line 2238510
    :cond_0
    if-eqz p1, :cond_1

    .line 2238511
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->T:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->U:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->V:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2238512
    :goto_0
    return-object p0

    .line 2238513
    :cond_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->C:Lcom/facebook/resources/ui/EllipsizingTextView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method public final d(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;
    .locals 2

    .prologue
    .line 2238494
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->D:Lcom/facebook/resources/ui/EllipsizingTextView;

    if-nez v0, :cond_0

    .line 2238495
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->u:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/EllipsizingTextView;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->D:Lcom/facebook/resources/ui/EllipsizingTextView;

    .line 2238496
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->u:Landroid/view/ViewStub;

    .line 2238497
    :cond_0
    if-eqz p1, :cond_2

    .line 2238498
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->W:Ljava/lang/CharSequence;

    .line 2238499
    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->W:Ljava/lang/CharSequence;

    .line 2238500
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->D:Lcom/facebook/resources/ui/EllipsizingTextView;

    if-eqz v1, :cond_1

    .line 2238501
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->W:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2238502
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->D:Lcom/facebook/resources/ui/EllipsizingTextView;

    const/4 p1, 0x0

    invoke-static {v1, p1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    .line 2238503
    :cond_1
    :goto_0
    return-object p0

    .line 2238504
    :cond_2
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->D:Lcom/facebook/resources/ui/EllipsizingTextView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    goto :goto_0

    .line 2238505
    :cond_3
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->D:Lcom/facebook/resources/ui/EllipsizingTextView;

    iget-object p1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->W:Ljava/lang/CharSequence;

    invoke-virtual {v1, p1}, Lcom/facebook/resources/ui/EllipsizingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2238506
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->D:Lcom/facebook/resources/ui/EllipsizingTextView;

    const/4 p1, 0x1

    invoke-static {v1, p1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method public final e(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2238514
    if-eqz p1, :cond_2

    .line 2238515
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->E:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    if-nez v0, :cond_0

    .line 2238516
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->v:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->E:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2238517
    new-instance v0, LX/1P0;

    invoke-virtual {p0}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1P0;-><init>(Landroid/content/Context;)V

    .line 2238518
    invoke-virtual {v0, v3}, LX/1P1;->b(I)V

    .line 2238519
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->E:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2238520
    new-instance v0, LX/62a;

    invoke-virtual {p0}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b152e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-direct {v0, v1}, LX/62a;-><init>(I)V

    .line 2238521
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->E:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2238522
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->v:Landroid/view/ViewStub;

    .line 2238523
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->J:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;

    if-nez v0, :cond_1

    .line 2238524
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b152d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2238525
    new-instance v1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;

    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->E:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0, p0}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;-><init>(Landroid/content/Context;ILX/FQ2;)V

    iput-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->J:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;

    .line 2238526
    :cond_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->E:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->J:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2238527
    :cond_2
    if-eqz p1, :cond_3

    .line 2238528
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->aa:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->setPhotoUrls(Ljava/util/List;)V

    .line 2238529
    :goto_0
    return-object p0

    .line 2238530
    :cond_3
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->E:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-static {v0, v3}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method public final f(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;
    .locals 3

    .prologue
    .line 2238531
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->F:Lcom/facebook/resources/ui/EllipsizingTextView;

    if-nez v0, :cond_0

    .line 2238532
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->w:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/EllipsizingTextView;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->F:Lcom/facebook/resources/ui/EllipsizingTextView;

    .line 2238533
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->w:Landroid/view/ViewStub;

    .line 2238534
    :cond_0
    if-eqz p1, :cond_1

    .line 2238535
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->ab:Ljava/util/List;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->ac:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->ad:Ljava/util/List;

    invoke-virtual {p0, v0, v1, v2}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Ljava/util/List;Ljava/lang/String;Ljava/util/List;)V

    .line 2238536
    :goto_0
    return-object p0

    .line 2238537
    :cond_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->F:Lcom/facebook/resources/ui/EllipsizingTextView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method public final g(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;
    .locals 7

    .prologue
    .line 2238538
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->G:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    .line 2238539
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->x:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->G:Landroid/widget/LinearLayout;

    .line 2238540
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->x:Landroid/view/ViewStub;

    .line 2238541
    :cond_0
    if-eqz p1, :cond_2

    .line 2238542
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->ae:Ljava/util/List;

    const/4 v4, 0x0

    .line 2238543
    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->ae:Ljava/util/List;

    .line 2238544
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->G:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_1

    .line 2238545
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->ae:Ljava/util/List;

    invoke-static {v1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2238546
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->G:Landroid/widget/LinearLayout;

    invoke-static {v1, v4}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    .line 2238547
    :cond_1
    :goto_0
    return-object p0

    .line 2238548
    :cond_2
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->G:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    goto :goto_0

    .line 2238549
    :cond_3
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->G:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 2238550
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 2238551
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->ae:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget v2, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->n:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v6

    move v3, v4

    .line 2238552
    :goto_1
    if-ge v3, v6, :cond_4

    .line 2238553
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->ae:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 2238554
    const v2, 0x7f031300

    iget-object p1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->G:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v2, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbButton;

    .line 2238555
    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2238556
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->af:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2238557
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->G:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2238558
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 2238559
    :cond_4
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->G:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method public getSavedDrawable()Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    .line 2238560
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->o:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 2238561
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->b:LX/0wM;

    sget-object v1, LX/FQ9;->BOOKMARK:LX/FQ9;

    iget v1, v1, LX/FQ9;->mIconResId:I

    invoke-virtual {p0}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->H:LX/FQ9;

    iget v3, v3, LX/FQ9;->mSelectedColorResId:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->o:Landroid/graphics/drawable/Drawable;

    .line 2238562
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->o:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getUnsavedDrawable()Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    .line 2238563
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->p:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 2238564
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->b:LX/0wM;

    sget-object v1, LX/FQ9;->BOOKMARK:LX/FQ9;

    iget v1, v1, LX/FQ9;->mIconResId:I

    invoke-virtual {p0}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->H:LX/FQ9;

    iget v3, v3, LX/FQ9;->mUnselectedColorResId:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->p:Landroid/graphics/drawable/Drawable;

    .line 2238565
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->p:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public setPhotoUrls(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2238566
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->aa:Ljava/util/List;

    .line 2238567
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->E:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    if-eqz v0, :cond_0

    .line 2238568
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->aa:Ljava/util/List;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2238569
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->E:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    .line 2238570
    :cond_0
    :goto_0
    return-void

    .line 2238571
    :cond_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->E:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    .line 2238572
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->J:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->aa:Ljava/util/List;

    .line 2238573
    iput-object v1, v0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;->d:Ljava/util/List;

    .line 2238574
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2238575
    goto :goto_0
.end method

.method public setThumbnailUrl(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2238576
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->M:Ljava/lang/String;

    .line 2238577
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_0

    .line 2238578
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->M:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2238579
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    .line 2238580
    :cond_0
    :goto_0
    return-void

    .line 2238581
    :cond_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->M:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->g:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2238582
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method public final y_(I)V
    .locals 1

    .prologue
    .line 2238583
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->K:LX/FPC;

    if-eqz v0, :cond_0

    .line 2238584
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->K:LX/FPC;

    invoke-virtual {v0, p0, p1}, LX/FPC;->a(Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;I)V

    .line 2238585
    :cond_0
    return-void
.end method
