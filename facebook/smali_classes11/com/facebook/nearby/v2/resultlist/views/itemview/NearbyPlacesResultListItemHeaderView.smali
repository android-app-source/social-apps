.class public Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Landroid/content/Context;

.field private final c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/TextView;

.field private g:LX/FQC;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2238929
    const-class v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2238927
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2238928
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2238925
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2238926
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2238869
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2238870
    const v0, 0x7f030bc8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2238871
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->b:Landroid/content/Context;

    .line 2238872
    const v0, 0x7f0d1d58

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->d:Landroid/widget/TextView;

    .line 2238873
    const v0, 0x7f0d1d59

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->e:Landroid/widget/TextView;

    .line 2238874
    const v0, 0x7f0d1d5a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->f:Landroid/widget/TextView;

    .line 2238875
    const v0, 0x7f0d1d56

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2238876
    return-void
.end method

.method private a(LX/FQC;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2238921
    sget-object v1, LX/FQB;->a:[I

    invoke-virtual {p1}, LX/FQC;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    move v1, v0

    .line 2238922
    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    :cond_0
    return v0

    .line 2238923
    :pswitch_0
    const v1, 0x7f0b14fc

    goto :goto_0

    .line 2238924
    :pswitch_1
    const v1, 0x7f0b14fd

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(LX/0Px;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 2238930
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08211c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {p1, v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->a(LX/0Px;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 2238931
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2238932
    const-string v0, ""

    .line 2238933
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->b:Landroid/content/Context;

    const v2, 0x7f08211d

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/0Px;Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "I)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2238912
    if-nez p1, :cond_0

    .line 2238913
    const-string p1, ""

    .line 2238914
    :cond_0
    if-eqz p0, :cond_1

    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    if-gtz p2, :cond_2

    .line 2238915
    :cond_1
    const-string v0, ""

    .line 2238916
    :goto_0
    return-object v0

    .line 2238917
    :cond_2
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v1, :cond_3

    .line 2238918
    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 2238919
    :cond_3
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p0, v2, v0}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v0

    .line 2238920
    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v2

    invoke-static {p1, v1}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(LX/0Px;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 2238911
    const-string v0, " \u00b7 "

    const/4 v1, 0x3

    invoke-static {p0, v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->a(LX/0Px;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c(I)V
    .locals 1

    .prologue
    .line 2238907
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2238908
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2238909
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->requestLayout()V

    .line 2238910
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;LX/FQC;)V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 2238877
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2238878
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2238879
    iput-object p2, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->g:LX/FQC;

    .line 2238880
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->d:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2238881
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2238882
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->k()LX/0Px;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->b(LX/0Px;)Ljava/lang/String;

    move-result-object v0

    .line 2238883
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2238884
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->i()LX/0Px;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->a(LX/0Px;)Ljava/lang/String;

    move-result-object v1

    .line 2238885
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2238886
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->d()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2238887
    iget-object v0, p1, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->B()Z

    move-result v0

    move v0, v0

    .line 2238888
    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->d()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;->d()Ljava/lang/String;

    move-result-object v0

    .line 2238889
    :goto_0
    iget-object v4, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->g:LX/FQC;

    invoke-direct {p0, v4}, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->a(LX/FQC;)I

    move-result v4

    .line 2238890
    invoke-direct {p0, v4}, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->c(I)V

    .line 2238891
    sget-object v4, LX/FQB;->a:[I

    invoke-virtual {p2}, LX/FQC;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2238892
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Undefined Header View State"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2238893
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2238894
    :pswitch_0
    iget-object v4, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_2

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2238895
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2238896
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2238897
    :goto_2
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2238898
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v2, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2238899
    :cond_1
    return-void

    :cond_2
    move v1, v3

    .line 2238900
    goto :goto_1

    .line 2238901
    :pswitch_1
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_3

    move v3, v2

    :cond_3
    invoke-virtual {v1, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2238902
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2238903
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 2238904
    :pswitch_2
    iget-object v4, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2238905
    iget-object v4, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->e:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2238906
    iget-object v4, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->f:Landroid/widget/TextView;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    :goto_3
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_4
    move v3, v2

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
