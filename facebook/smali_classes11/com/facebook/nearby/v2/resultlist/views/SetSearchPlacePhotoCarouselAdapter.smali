.class public Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/FQ4;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Landroid/content/Context;

.field private c:I

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/FQ2;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2238416
    const-class v0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILX/FQ2;)V
    .locals 0

    .prologue
    .line 2238417
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2238418
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;->b:Landroid/content/Context;

    .line 2238419
    iput p2, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;->c:I

    .line 2238420
    iput-object p3, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;->e:LX/FQ2;

    .line 2238421
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2238422
    new-instance v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 2238423
    new-instance v1, LX/1a3;

    iget v2, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;->c:I

    iget v3, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;->c:I

    invoke-direct {v1, v2, v3}, LX/1a3;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2238424
    new-instance v1, LX/FQ4;

    invoke-direct {v1, p0, v0}, LX/FQ4;-><init>(Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 2238425
    check-cast p1, LX/FQ4;

    .line 2238426
    iput p2, p1, LX/FQ4;->n:I

    .line 2238427
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;->d:Ljava/util/List;

    if-eqz v0, :cond_0

    if-ltz p2, :cond_0

    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p2, v0, :cond_1

    .line 2238428
    :cond_0
    iget-object v0, p1, LX/FQ4;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2238429
    :goto_0
    return-void

    .line 2238430
    :cond_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;->d:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2238431
    iget-object v1, p1, LX/FQ4;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v2, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2238432
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;->d:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlacePhotoCarouselAdapter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method
