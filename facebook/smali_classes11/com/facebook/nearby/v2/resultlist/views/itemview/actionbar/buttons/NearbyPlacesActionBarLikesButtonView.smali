.class public Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarLikesButtonView;
.super Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final f:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarLikesButtonView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2239143
    const-class v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarLikesButtonView;

    sput-object v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarLikesButtonView;->f:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2239144
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarLikesButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2239145
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2239146
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarLikesButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2239147
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2239148
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2239149
    sget-object v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarLikesButtonView;->f:Ljava/lang/Class;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarLikesButtonView;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2239150
    return-void
.end method


# virtual methods
.method public final c(I)V
    .locals 4

    .prologue
    .line 2239151
    if-gez p1, :cond_0

    .line 2239152
    const/4 p1, 0x0

    .line 2239153
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    .line 2239154
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/buttons/NearbyPlacesActionBarButtonView;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2239155
    return-void
.end method

.method public getImageResourceId()I
    .locals 1

    .prologue
    .line 2239156
    const v0, 0x7f020e0a

    return v0
.end method
