.class public Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/HorizontalImageGallery;

.field public b:LX/FQE;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2239293
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2239294
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2239291
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2239292
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 2239282
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2239283
    const v0, 0x7f030bc9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2239284
    const v0, 0x7f0d1d5d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/HorizontalImageGallery;

    iput-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;->a:Lcom/facebook/widget/HorizontalImageGallery;

    .line 2239285
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;->a:Lcom/facebook/widget/HorizontalImageGallery;

    .line 2239286
    iget-object v1, v0, Lcom/facebook/widget/HorizontalImageGallery;->b:Landroid/widget/LinearLayout;

    move-object v0, v1

    .line 2239287
    if-eqz v0, :cond_0

    .line 2239288
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-direct {p0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;->getImageSize()I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2239289
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestLayout()V

    .line 2239290
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;I)V
    .locals 1

    .prologue
    .line 2239279
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;->b:LX/FQE;

    if-nez v0, :cond_0

    .line 2239280
    :goto_0
    return-void

    .line 2239281
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;->b:LX/FQE;

    invoke-interface {v0, p1}, LX/FQE;->a(I)V

    goto :goto_0
.end method

.method private getImageSize()I
    .locals 2

    .prologue
    .line 2239278
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1515

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2239258
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;->a:Lcom/facebook/widget/HorizontalImageGallery;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2239259
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;->a:Lcom/facebook/widget/HorizontalImageGallery;

    invoke-virtual {v0}, Lcom/facebook/widget/HorizontalImageGallery;->a()V

    .line 2239260
    return-void
.end method

.method public final a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2239263
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2239264
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->l()LX/0Px;

    move-result-object v3

    .line 2239265
    if-eqz v3, :cond_1

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2239266
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;->a:Lcom/facebook/widget/HorizontalImageGallery;

    invoke-virtual {v0}, Lcom/facebook/widget/HorizontalImageGallery;->a()V

    .line 2239267
    invoke-direct {p0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;->getImageSize()I

    move-result v4

    move v2, v1

    .line 2239268
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 2239269
    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;

    .line 2239270
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {p0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {v0, v4, v5, v6}, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollPhotoHelper;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;ILandroid/content/Context;Landroid/content/res/Resources;)Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v0

    .line 2239271
    if-eqz v0, :cond_0

    .line 2239272
    new-instance v5, LX/FQO;

    invoke-direct {v5, p0, v2}, LX/FQO;-><init>(Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;I)V

    invoke-virtual {v0, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2239273
    iget-object v5, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;->a:Lcom/facebook/widget/HorizontalImageGallery;

    const/4 v6, -0x1

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    invoke-virtual {v5, v0, v6, v7}, Lcom/facebook/widget/HorizontalImageGallery;->a(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 2239274
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move v0, v1

    .line 2239275
    goto :goto_0

    .line 2239276
    :cond_2
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;->a:Lcom/facebook/widget/HorizontalImageGallery;

    invoke-virtual {v0, v1, v1}, Lcom/facebook/widget/HorizontalImageGallery;->scrollTo(II)V

    .line 2239277
    return-void
.end method

.method public setOnPhotoClickedListener(LX/FQE;)V
    .locals 0

    .prologue
    .line 2239261
    iput-object p1, p0, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;->b:LX/FQE;

    .line 2239262
    return-void
.end method
