.class public final Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:Z

.field public final d:Z

.field public final e:Z

.field public final f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2237509
    new-instance v0, LX/FPX;

    invoke-direct {v0}, LX/FPX;-><init>()V

    sput-object v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/FPY;)V
    .locals 1

    .prologue
    .line 2237510
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2237511
    iget-boolean v0, p1, LX/FPY;->a:Z

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->a:Z

    .line 2237512
    iget-boolean v0, p1, LX/FPY;->b:Z

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->b:Z

    .line 2237513
    iget-boolean v0, p1, LX/FPY;->c:Z

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->c:Z

    .line 2237514
    iget-boolean v0, p1, LX/FPY;->d:Z

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->d:Z

    .line 2237515
    iget-boolean v0, p1, LX/FPY;->e:Z

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->e:Z

    .line 2237516
    iget-boolean v0, p1, LX/FPY;->f:Z

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->f:Z

    .line 2237517
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2237518
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2237519
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->a:Z

    .line 2237520
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->b:Z

    .line 2237521
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->c:Z

    .line 2237522
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->d:Z

    .line 2237523
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->e:Z

    .line 2237524
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_5

    :goto_5
    iput-boolean v1, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->f:Z

    .line 2237525
    return-void

    :cond_0
    move v0, v2

    .line 2237526
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2237527
    goto :goto_1

    :cond_2
    move v0, v2

    .line 2237528
    goto :goto_2

    :cond_3
    move v0, v2

    .line 2237529
    goto :goto_3

    :cond_4
    move v0, v2

    .line 2237530
    goto :goto_4

    :cond_5
    move v1, v2

    .line 2237531
    goto :goto_5
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2237532
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2237533
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2237534
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->b:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2237535
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->c:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2237536
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->d:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2237537
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->e:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2237538
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->f:Z

    if-eqz v0, :cond_5

    :goto_5
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2237539
    return-void

    :cond_0
    move v0, v2

    .line 2237540
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2237541
    goto :goto_1

    :cond_2
    move v0, v2

    .line 2237542
    goto :goto_2

    :cond_3
    move v0, v2

    .line 2237543
    goto :goto_3

    :cond_4
    move v0, v2

    .line 2237544
    goto :goto_4

    :cond_5
    move v1, v2

    .line 2237545
    goto :goto_5
.end method
