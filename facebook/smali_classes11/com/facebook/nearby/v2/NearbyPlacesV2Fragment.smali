.class public Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/0fj;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/6ZZ;


# static fields
.field public static final i:Lcom/facebook/common/callercontext/CallerContext;

.field public static final j:[Ljava/lang/String;


# instance fields
.field public a:LX/H1i;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1sS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/6Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/FOx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final k:Lcom/facebook/location/FbLocationOperationParams;

.field private final l:LX/0fN;

.field private m:Z

.field public n:Z

.field public o:Z

.field private p:Landroid/view/ViewGroup;

.field public q:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

.field public r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

.field public s:LX/H3k;

.field public t:LX/0i5;

.field public u:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

.field private v:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2421729
    const-class v0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->i:Lcom/facebook/common/callercontext/CallerContext;

    .line 2421730
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.ACCESS_COARSE_LOCATION"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->j:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 2421731
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2421732
    sget-object v0, LX/0yF;->HIGH_ACCURACY:LX/0yF;

    invoke-static {v0}, Lcom/facebook/location/FbLocationOperationParams;->a(LX/0yF;)LX/1S7;

    move-result-object v0

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v2, v3}, LX/1S7;->c(J)LX/1S7;

    move-result-object v0

    const-wide/16 v2, 0x4e20

    .line 2421733
    iput-wide v2, v0, LX/1S7;->b:J

    .line 2421734
    move-object v0, v0

    .line 2421735
    invoke-virtual {v0}, LX/1S7;->a()Lcom/facebook/location/FbLocationOperationParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->k:Lcom/facebook/location/FbLocationOperationParams;

    .line 2421736
    new-instance v0, LX/H3d;

    invoke-direct {v0, p0}, LX/H3d;-><init>(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->l:LX/0fN;

    .line 2421737
    iput-boolean v1, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->m:Z

    .line 2421738
    iput-boolean v1, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->n:Z

    .line 2421739
    sget-object v0, LX/H3k;->INTENT_VIEW_FRAGMENT:LX/H3k;

    iput-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->s:LX/H3k;

    .line 2421740
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->u:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

    .line 2421741
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->v:Z

    .line 2421742
    return-void
.end method

.method public static a(LX/H3k;LX/H3k;)LX/H3j;
    .locals 5

    .prologue
    const v0, 0x10a0001

    const/high16 v1, 0x10a0000

    .line 2421743
    sget-object v2, LX/H3k;->INTENT_VIEW_FRAGMENT:LX/H3k;

    if-ne p0, v2, :cond_0

    sget-object v2, LX/H3k;->RESULT_LIST_FRAGMENT:LX/H3k;

    if-ne p1, v2, :cond_0

    .line 2421744
    const v3, 0x7f0400d6

    .line 2421745
    const v2, 0x7f0400e1

    .line 2421746
    const v1, 0x7f0400d5

    .line 2421747
    const v0, 0x7f0400e2

    .line 2421748
    :goto_0
    new-instance v4, LX/H3j;

    invoke-direct {v4, v3, v2, v1, v0}, LX/H3j;-><init>(IIII)V

    move-object v0, v4

    :goto_1
    return-object v0

    .line 2421749
    :cond_0
    sget-object v2, LX/H3k;->RESULT_LIST_FRAGMENT:LX/H3k;

    if-eq p0, v2, :cond_1

    sget-object v2, LX/H3k;->MAP_VIEW_FRAGMENT:LX/H3k;

    if-ne p0, v2, :cond_2

    :cond_1
    sget-object v2, LX/H3k;->INTENT_VIEW_FRAGMENT:LX/H3k;

    if-ne p1, v2, :cond_2

    .line 2421750
    const v1, 0x7f0400d5

    .line 2421751
    const v0, 0x7f0400e2

    move v2, v0

    move v3, v1

    .line 2421752
    goto :goto_0

    .line 2421753
    :cond_2
    sget-object v2, LX/H3k;->INTENT_VIEW_FRAGMENT:LX/H3k;

    if-ne p0, v2, :cond_3

    sget-object v2, LX/H3k;->INTENT_VIEW_FRAGMENT:LX/H3k;

    if-ne p1, v2, :cond_3

    .line 2421754
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move v2, v0

    move v3, v1

    .line 2421755
    goto :goto_0
.end method

.method public static a(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;Landroid/support/v4/app/Fragment;Ljava/lang/String;Ljava/lang/String;ZLX/H3j;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2421756
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2421757
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2421758
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v0

    .line 2421759
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    .line 2421760
    if-eqz p5, :cond_4

    .line 2421761
    iget v1, p5, LX/H3j;->b:I

    move v1, v1

    .line 2421762
    iget v2, p5, LX/H3j;->a:I

    move v2, v2

    .line 2421763
    iget v3, p5, LX/H3j;->d:I

    move v3, v3

    .line 2421764
    iget v4, p5, LX/H3j;->c:I

    move v4, v4

    .line 2421765
    invoke-virtual {v0, v1, v2, v3, v4}, LX/0hH;->a(IIII)LX/0hH;

    move-result-object v0

    .line 2421766
    :goto_0
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2421767
    invoke-static {p0}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->k(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    move-result-object v1

    .line 2421768
    if-eqz v1, :cond_0

    if-eq v1, p1, :cond_0

    .line 2421769
    invoke-virtual {v0, v1}, LX/0hH;->b(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 2421770
    :cond_0
    invoke-static {p0}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->l(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;

    move-result-object v1

    .line 2421771
    if-eqz v1, :cond_1

    if-eq v1, p1, :cond_1

    .line 2421772
    invoke-virtual {v0, v1}, LX/0hH;->b(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 2421773
    :cond_1
    invoke-static {p0}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->m(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 2421774
    if-eqz v1, :cond_2

    if-eq v1, p1, :cond_2

    .line 2421775
    invoke-virtual {v0, v1}, LX/0hH;->b(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 2421776
    :cond_2
    iget-object v1, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->p:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getId()I

    move-result v1

    invoke-virtual {v0, v1, p1, p2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    .line 2421777
    if-eqz p4, :cond_3

    .line 2421778
    invoke-virtual {v0, p3}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    .line 2421779
    :cond_3
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2421780
    iput-boolean v5, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->m:Z

    .line 2421781
    return-void

    .line 2421782
    :cond_4
    invoke-virtual {v0, v5}, LX/0hH;->a(I)LX/0hH;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;Z)V
    .locals 2

    .prologue
    .line 2421783
    iget-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->q:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    if-eqz v0, :cond_0

    .line 2421784
    iget-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->q:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;)V

    .line 2421785
    :cond_0
    if-nez p2, :cond_2

    .line 2421786
    :cond_1
    :goto_0
    return-void

    .line 2421787
    :cond_2
    sget-object v0, LX/H3i;->a:[I

    iget-object v1, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->s:LX/H3k;

    invoke-virtual {v1}, LX/H3k;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2421788
    :pswitch_0
    invoke-static {p0}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->k(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    move-result-object v0

    .line 2421789
    if-eqz v0, :cond_1

    .line 2421790
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->b()V

    goto :goto_0

    .line 2421791
    :pswitch_1
    invoke-static {p0}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->l(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;

    move-result-object v0

    .line 2421792
    if-eqz v0, :cond_1

    .line 2421793
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->b()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 2421794
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->o:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421795
    iget-object v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v0, v1

    .line 2421796
    iget-object v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->c:Landroid/location/Location;

    move-object v0, v1

    .line 2421797
    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 2421798
    :cond_0
    :goto_0
    return-void

    .line 2421799
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->o:Z

    .line 2421800
    iget-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->t:LX/0i5;

    sget-object v1, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->j:[Ljava/lang/String;

    new-instance v2, LX/H3f;

    invoke-direct {v2, p0}, LX/H3f;-><init>(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)V

    invoke-virtual {v0, v1, v2}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2421817
    const-string v1, "Fragment should not be null!"

    invoke-static {p1, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2421818
    instance-of v1, p1, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->m:Z

    if-eqz v1, :cond_1

    .line 2421819
    :cond_0
    :goto_0
    return v0

    .line 2421820
    :cond_1
    instance-of v1, p1, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->s:LX/H3k;

    sget-object v2, LX/H3k;->RESULT_LIST_FRAGMENT:LX/H3k;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->s:LX/H3k;

    sget-object v2, LX/H3k;->MAP_VIEW_FRAGMENT:LX/H3k;

    if-eq v1, v2, :cond_0

    .line 2421821
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;LX/FOw;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2421801
    iput-boolean v1, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->o:Z

    .line 2421802
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->n:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421803
    iget-object v2, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v0, v2

    .line 2421804
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 2421805
    :goto_0
    iget-object v2, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421806
    iget-object v3, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v2, v3

    .line 2421807
    iput-object p1, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->a:LX/FOw;

    .line 2421808
    iget-object v2, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421809
    iget-object v3, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v2, v3

    .line 2421810
    iput-boolean v1, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    .line 2421811
    iget-object v2, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421812
    iget-object v3, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v2, v3

    .line 2421813
    invoke-static {p0, v2, v0}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->a(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;Z)V

    .line 2421814
    iput-boolean v1, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->n:Z

    .line 2421815
    return-void

    :cond_1
    move v0, v1

    .line 2421816
    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;LX/H3k;)V
    .locals 8

    .prologue
    .line 2421824
    invoke-static {p0, p1}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->b(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;LX/H3k;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2421825
    iget-object v1, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->s:LX/H3k;

    invoke-static {p0, v1}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->b(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;LX/H3k;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    move-object v1, v1

    .line 2421826
    if-eqz v1, :cond_4

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2421827
    if-eqz v0, :cond_0

    .line 2421828
    :goto_1
    return-void

    .line 2421829
    :cond_0
    sget-object v0, LX/H3i;->a:[I

    invoke-virtual {p1}, LX/H3k;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2421830
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid view state for NearbyPlaces Fragment!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2421831
    :pswitch_0
    invoke-static {p0}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->l(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;

    move-result-object v3

    .line 2421832
    if-nez v3, :cond_2

    .line 2421833
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2421834
    iget-object v2, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->u:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

    if-nez v2, :cond_1

    .line 2421835
    new-instance v2, LX/FPY;

    invoke-direct {v2}, LX/FPY;-><init>()V

    .line 2421836
    iput-boolean v3, v2, LX/FPY;->a:Z

    .line 2421837
    move-object v2, v2

    .line 2421838
    iput-boolean v3, v2, LX/FPY;->b:Z

    .line 2421839
    move-object v2, v2

    .line 2421840
    iput-boolean v3, v2, LX/FPY;->c:Z

    .line 2421841
    move-object v2, v2

    .line 2421842
    iput-boolean v3, v2, LX/FPY;->d:Z

    .line 2421843
    move-object v2, v2

    .line 2421844
    iput-boolean v4, v2, LX/FPY;->e:Z

    .line 2421845
    move-object v2, v2

    .line 2421846
    iput-boolean v4, v2, LX/FPY;->f:Z

    .line 2421847
    move-object v2, v2

    .line 2421848
    invoke-virtual {v2}, LX/FPY;->a()Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->u:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

    .line 2421849
    :cond_1
    iget-object v2, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->u:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

    move-object v2, v2

    .line 2421850
    invoke-static {v2}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->b(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;)Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;

    move-result-object v3

    .line 2421851
    iget-object v2, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    invoke-virtual {v3, v2}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;)V

    .line 2421852
    iget-object v2, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    invoke-virtual {v3, v2}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->a(LX/FOz;)V

    .line 2421853
    :cond_2
    invoke-virtual {v3, p0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->a(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)V

    .line 2421854
    const-string v4, "ResultList"

    const-class v2, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v3}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->a(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;Ljava/lang/Object;)Z

    move-result v6

    iget-object v2, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->s:LX/H3k;

    sget-object v7, LX/H3k;->RESULT_LIST_FRAGMENT:LX/H3k;

    invoke-static {v2, v7}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->a(LX/H3k;LX/H3k;)LX/H3j;

    move-result-object v7

    move-object v2, p0

    invoke-static/range {v2 .. v7}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->a(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;Landroid/support/v4/app/Fragment;Ljava/lang/String;Ljava/lang/String;ZLX/H3j;)V

    .line 2421855
    :goto_2
    :pswitch_1
    invoke-static {p0}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->o(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)V

    goto :goto_1

    .line 2421856
    :pswitch_2
    invoke-static {p0}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->k(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    move-result-object v3

    .line 2421857
    if-nez v3, :cond_3

    .line 2421858
    iget-object v2, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    iget-object v3, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421859
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2421860
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2421861
    new-instance v4, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    invoke-direct {v4}, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;-><init>()V

    .line 2421862
    iput-object v2, v4, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->m:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421863
    iput-object v3, v4, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->l:LX/FOz;

    .line 2421864
    move-object v3, v4

    .line 2421865
    :cond_3
    iput-object p0, v3, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->k:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    .line 2421866
    const-string v4, "IntentView"

    const-class v2, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v3}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->a(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;Ljava/lang/Object;)Z

    move-result v6

    iget-object v2, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->s:LX/H3k;

    sget-object v7, LX/H3k;->INTENT_VIEW_FRAGMENT:LX/H3k;

    invoke-static {v2, v7}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->a(LX/H3k;LX/H3k;)LX/H3j;

    move-result-object v7

    move-object v2, p0

    invoke-static/range {v2 .. v7}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->a(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;Landroid/support/v4/app/Fragment;Ljava/lang/String;Ljava/lang/String;ZLX/H3j;)V

    .line 2421867
    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;LX/H3k;)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 2421868
    sget-object v0, LX/H3i;->a:[I

    invoke-virtual {p1}, LX/H3k;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2421869
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid view state for NearbyPlaces Fragment!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2421870
    :pswitch_0
    invoke-static {p0}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->l(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;

    move-result-object v0

    .line 2421871
    :goto_0
    return-object v0

    .line 2421872
    :pswitch_1
    invoke-static {p0}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->k(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    move-result-object v0

    goto :goto_0

    .line 2421873
    :pswitch_2
    invoke-static {p0}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->m(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static k(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;
    .locals 2

    .prologue
    .line 2421823
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "IntentView"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    return-object v0
.end method

.method public static l(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;
    .locals 2

    .prologue
    .line 2421822
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "ResultList"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;

    return-object v0
.end method

.method public static m(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 2421723
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "MapView"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public static n(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)V
    .locals 2

    .prologue
    .line 2421724
    iget-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->q:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2421725
    iget-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->q:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v1, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421726
    iget-object p0, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v1, p0

    .line 2421727
    invoke-virtual {v0, v1}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;)V

    .line 2421728
    :cond_0
    return-void
.end method

.method public static o(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)V
    .locals 4

    .prologue
    .line 2421545
    invoke-static {p0}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->k(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    move-result-object v0

    .line 2421546
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2421547
    sget-object v0, LX/H3k;->INTENT_VIEW_FRAGMENT:LX/H3k;

    iput-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->s:LX/H3k;

    .line 2421548
    :cond_0
    :goto_0
    const v0, 0x7f0820c1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2421549
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2421550
    if-nez v1, :cond_3

    .line 2421551
    :goto_1
    const/4 v1, 0x0

    .line 2421552
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2421553
    if-nez v0, :cond_4

    .line 2421554
    :goto_2
    return-void

    .line 2421555
    :cond_1
    invoke-static {p0}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->l(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;

    move-result-object v0

    .line 2421556
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2421557
    sget-object v0, LX/H3k;->RESULT_LIST_FRAGMENT:LX/H3k;

    iput-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->s:LX/H3k;

    goto :goto_0

    .line 2421558
    :cond_2
    invoke-static {p0}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->m(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2421559
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2421560
    sget-object v0, LX/H3k;->MAP_VIEW_FRAGMENT:LX/H3k;

    iput-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->s:LX/H3k;

    goto :goto_0

    .line 2421561
    :cond_3
    invoke-interface {v1, v0}, LX/1ZF;->a_(Ljava/lang/String;)V

    goto :goto_1

    .line 2421562
    :cond_4
    sget-object v2, LX/H3i;->a:[I

    iget-object v3, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->s:LX/H3k;

    invoke-virtual {v3}, LX/H3k;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2421563
    :goto_3
    :pswitch_0
    if-eqz v1, :cond_5

    .line 2421564
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2421565
    iput-object v1, v2, LX/108;->g:Ljava/lang/String;

    .line 2421566
    move-object v1, v2

    .line 2421567
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    .line 2421568
    new-instance v1, LX/H3g;

    invoke-direct {v1, p0}, LX/H3g;-><init>(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)V

    .line 2421569
    :goto_4
    invoke-interface {v0, v2}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2421570
    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    .line 2421571
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    goto :goto_2

    .line 2421572
    :pswitch_1
    const v1, 0x7f08211b

    goto :goto_3

    .line 2421573
    :cond_5
    sget-object v2, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2421574
    const/4 v1, 0x0

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 2421575
    iget-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->q:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2421576
    iget-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->q:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->d()V

    .line 2421577
    invoke-static {p0}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->n(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)V

    .line 2421578
    const/4 v0, 0x1

    .line 2421579
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2421580
    const-string v0, "nearby_places_fragment"

    return-object v0
.end method

.method public final a(LX/6ZY;)V
    .locals 1

    .prologue
    .line 2421581
    sget-object v0, LX/6ZY;->DIALOG_SUCCESS:LX/6ZY;

    if-ne p1, v0, :cond_0

    .line 2421582
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->a(Z)V

    .line 2421583
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 2421584
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2421585
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    invoke-static {v0}, LX/H1i;->b(LX/0QB;)LX/H1i;

    move-result-object v3

    check-cast v3, LX/H1i;

    invoke-static {v0}, LX/1sS;->b(LX/0QB;)LX/1sS;

    move-result-object v4

    check-cast v4, LX/1sS;

    const/16 v5, 0xc81

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    const-class v7, LX/0i4;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/0i4;

    invoke-static {v0}, LX/6Zb;->b(LX/0QB;)LX/6Zb;

    move-result-object v8

    check-cast v8, LX/6Zb;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p1

    check-cast p1, LX/0ad;

    invoke-static {v0}, LX/FOx;->a(LX/0QB;)LX/FOx;

    move-result-object v0

    check-cast v0, LX/FOx;

    iput-object v3, v2, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->a:LX/H1i;

    iput-object v4, v2, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->b:LX/1sS;

    iput-object v5, v2, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->c:LX/0Or;

    iput-object v6, v2, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->d:LX/1Ck;

    iput-object v7, v2, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->e:LX/0i4;

    iput-object v8, v2, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->f:LX/6Zb;

    iput-object p1, v2, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->g:LX/0ad;

    iput-object v0, v2, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->h:LX/FOx;

    .line 2421586
    iget-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->f:LX/6Zb;

    invoke-virtual {v0, p0, p0}, LX/6Zb;->a(Lcom/facebook/base/fragment/FbFragment;LX/6ZZ;)V

    .line 2421587
    return-void
.end method

.method public final a(Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;)V
    .locals 2

    .prologue
    .line 2421588
    iget-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->q:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->d()V

    .line 2421589
    iget-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    new-instance v1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    invoke-direct {v1, p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;-><init>(Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;)V

    .line 2421590
    iput-object v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2421591
    iget-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->s:LX/H3k;

    sget-object v1, LX/H3k;->RESULT_LIST_FRAGMENT:LX/H3k;

    if-ne v0, v1, :cond_1

    .line 2421592
    invoke-static {p0}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->l(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;

    move-result-object v0

    .line 2421593
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->b()V

    .line 2421594
    :cond_0
    :goto_0
    return-void

    .line 2421595
    :cond_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421596
    iget-object v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v0, v1

    .line 2421597
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2421598
    sget-object v0, LX/H3k;->RESULT_LIST_FRAGMENT:LX/H3k;

    invoke-static {p0, v0}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->a$redex0(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;LX/H3k;)V

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x1211f090

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2421599
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2421600
    iget-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->e:LX/0i4;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->t:LX/0i5;

    .line 2421601
    if-eqz p1, :cond_4

    .line 2421602
    iput-boolean v7, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->m:Z

    .line 2421603
    const-string v0, "nearby_places_fragment_model_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421604
    const-string v0, "nearby_places_fragment_view_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/H3k;

    iput-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->s:LX/H3k;

    .line 2421605
    const-string v0, "nearby_places_is_initial_load"

    invoke-virtual {p1, v0, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->m:Z

    .line 2421606
    :goto_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2421607
    iget-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421608
    iget-object v2, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v0, v2

    .line 2421609
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2421610
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v2, v0

    .line 2421611
    const-string v0, "TypeaheadFragment"

    invoke-virtual {v2, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iput-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->q:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    .line 2421612
    iget-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->q:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    if-nez v0, :cond_5

    .line 2421613
    iget-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    iget-object v3, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421614
    iget-object v4, v3, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v3, v4

    .line 2421615
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2421616
    new-instance v4, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    invoke-direct {v4}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;-><init>()V

    .line 2421617
    iput-object v0, v4, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421618
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2421619
    const-string v8, "nearby_places_search_data"

    invoke-virtual {v5, v8, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2421620
    invoke-virtual {v4, v5}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2421621
    move-object v0, v4

    .line 2421622
    iput-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->q:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    .line 2421623
    invoke-virtual {v2}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v2, 0x7f0d1d46

    iget-object v3, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->q:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    const-string v4, "TypeaheadFragment"

    invoke-virtual {v0, v2, v3, v4}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2421624
    :goto_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->q:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    .line 2421625
    iput-object p0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->p:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    .line 2421626
    if-nez p1, :cond_6

    .line 2421627
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2421628
    const-string v2, "nearby_places_query_topic"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2421629
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2421630
    const-string v3, "nearby_places_location_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2421631
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2421632
    const-string v4, "nearby_places_location_name"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2421633
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2421634
    iget-object v4, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421635
    iget-object v5, v4, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v4, v5

    .line 2421636
    iput-boolean v7, v4, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    .line 2421637
    iget-object v4, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421638
    iget-object v5, v4, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v4, v5

    .line 2421639
    iput-object v2, v4, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->d:Ljava/lang/String;

    .line 2421640
    iget-object v2, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421641
    iget-object v4, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v2, v4

    .line 2421642
    iput-object v3, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->e:Ljava/lang/String;

    .line 2421643
    iget-object v2, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421644
    iget-object v3, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v2, v3

    .line 2421645
    new-instance v3, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    invoke-direct {v3, v0, v0}, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2421646
    iput-object v3, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->h:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    .line 2421647
    sget-object v0, LX/H3k;->RESULT_LIST_FRAGMENT:LX/H3k;

    iput-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->s:LX/H3k;

    .line 2421648
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->s:LX/H3k;

    invoke-static {p0, v0}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->a$redex0(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;LX/H3k;)V

    .line 2421649
    :cond_1
    :goto_2
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->v:Z

    if-nez v0, :cond_3

    .line 2421650
    iget-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->a:LX/H1i;

    iget-object v2, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421651
    iget-object v3, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->a:Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    move-object v2, v3

    .line 2421652
    iget-object v3, v2, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->c:Ljava/lang/String;

    move-object v2, v3

    .line 2421653
    iget-object v3, v0, LX/H1i;->a:LX/0Zb;

    const-string v4, "nearby_places_view_was_opened"

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2421654
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2421655
    const-string v4, "places_recommendations"

    invoke-virtual {v3, v4}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v4

    const-string v5, "session_id"

    invoke-virtual {v4, v5, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2421656
    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2421657
    :cond_2
    iput-boolean v6, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->v:Z

    .line 2421658
    :cond_3
    const v0, -0xcdefacc

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2421659
    :cond_4
    iput-boolean v6, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->m:Z

    .line 2421660
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2421661
    const-string v2, "nearby_places_entry_point"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/CQB;

    .line 2421662
    new-instance v2, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    sget-object v3, LX/CQC;->nearby_places:LX/CQC;

    iget-object v4, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->h:LX/FOx;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/FOx;->a(Landroid/app/Activity;)LX/FOw;

    move-result-object v4

    invoke-direct {v2, v0, v3, v4}, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;-><init>(LX/CQB;LX/CQC;LX/FOw;)V

    iput-object v2, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    goto/16 :goto_0

    .line 2421663
    :cond_5
    iget-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->q:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v2, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421664
    iput-object v2, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421665
    goto/16 :goto_1

    .line 2421666
    :cond_6
    invoke-static {p0}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->l(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;

    move-result-object v0

    .line 2421667
    if-eqz v0, :cond_7

    .line 2421668
    iget-object v2, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    invoke-virtual {v0, v2}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->a(LX/FOz;)V

    .line 2421669
    iget-object v2, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    invoke-virtual {v0, v2}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;)V

    .line 2421670
    invoke-virtual {v0, p0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment;->a(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)V

    .line 2421671
    :cond_7
    invoke-static {p0}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->k(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    move-result-object v0

    .line 2421672
    if-eqz v0, :cond_1

    .line 2421673
    iget-object v2, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421674
    iput-object v2, v0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->l:LX/FOz;

    .line 2421675
    iget-object v2, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421676
    iput-object v2, v0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->m:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421677
    iput-object p0, v0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->k:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    .line 2421678
    goto/16 :goto_2
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x2596df4b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2421679
    const v0, 0x7f030bc3

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2421680
    const v0, 0x7f0d1d45

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->p:Landroid/view/ViewGroup;

    .line 2421681
    const/16 v0, 0x2b

    const v3, -0x74e28c59

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroy()V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x359c01e6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2421682
    iget-object v1, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->a:LX/H1i;

    iget-object v2, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421683
    iget-object v4, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->a:Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    move-object v2, v4

    .line 2421684
    iget-object v4, v2, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->c:Ljava/lang/String;

    move-object v2, v4

    .line 2421685
    iget-object v4, v1, LX/H1i;->a:LX/0Zb;

    const-string v5, "nearby_places_session_did_end"

    const/4 v6, 0x1

    invoke-interface {v4, v5, v6}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 2421686
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2421687
    const-string v5, "places_recommendations"

    invoke-virtual {v4, v5}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v5

    const-string v6, "session_id"

    invoke-virtual {v5, v6, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2421688
    invoke-virtual {v4}, LX/0oG;->d()V

    .line 2421689
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2421690
    const/16 v1, 0x2b

    const v2, -0x6e9bf8ff

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6b9298d7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2421691
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2421692
    invoke-static {p0}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->o(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)V

    .line 2421693
    const/16 v1, 0x2b

    const v2, 0x5efc324b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2421694
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2421695
    const-string v0, "nearby_places_fragment_model_state"

    iget-object v1, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2421696
    const-string v0, "nearby_places_fragment_view_state"

    iget-object v1, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->s:LX/H3k;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2421697
    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x9999f2c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2421698
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2421699
    iget-object v1, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    if-eqz v1, :cond_0

    .line 2421700
    iget-object v1, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421701
    iget-object v2, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->a:Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    move-object v1, v2

    .line 2421702
    invoke-virtual {v1}, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->e()V

    .line 2421703
    :cond_0
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v1, v1

    .line 2421704
    iget-object v2, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->l:LX/0fN;

    invoke-virtual {v1, v2}, LX/0gc;->a(LX/0fN;)V

    .line 2421705
    iget-object v1, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->h:LX/FOx;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/FOx;->a(Landroid/app/Activity;)LX/FOw;

    move-result-object v1

    .line 2421706
    iget-object v2, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421707
    iget-object v4, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v2, v4

    .line 2421708
    iput-object v1, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->a:LX/FOw;

    .line 2421709
    iget-object v1, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421710
    iget-object v2, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v1, v2

    .line 2421711
    iget-object v2, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->c:Landroid/location/Location;

    move-object v1, v2

    .line 2421712
    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421713
    iget-object v2, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v1, v2

    .line 2421714
    iget-object v2, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->a:LX/FOw;

    move-object v1, v2

    .line 2421715
    sget-object v2, LX/FOw;->OKAY:LX/FOw;

    if-ne v1, v2, :cond_1

    .line 2421716
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->a(Z)V

    .line 2421717
    :cond_1
    const/16 v1, 0x2b

    const v2, 0x3b8c8614

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x77f3cdec

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2421718
    iget-object v1, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->d:LX/1Ck;

    const-string v2, "nearby_places_get_location_task_key"

    invoke-virtual {v1, v2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2421719
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v1, v1

    .line 2421720
    iget-object v2, p0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->l:LX/0fN;

    invoke-virtual {v1, v2}, LX/0gc;->b(LX/0fN;)V

    .line 2421721
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2421722
    const/16 v1, 0x2b

    const v2, 0x5020868

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
