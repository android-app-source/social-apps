.class public Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2422363
    new-instance v0, LX/H48;

    invoke-direct {v0}, LX/H48;-><init>()V

    sput-object v0, Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2422364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2422365
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;->a:LX/0Px;

    .line 2422366
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2422367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2422368
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2422369
    const-class v1, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 2422370
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;->a:LX/0Px;

    .line 2422371
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2422372
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2422373
    iget-object v0, p0, Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2422374
    return-void
.end method
