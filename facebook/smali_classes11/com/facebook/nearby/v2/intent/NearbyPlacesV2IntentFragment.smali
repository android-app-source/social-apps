.class public Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# static fields
.field public static final d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/H1i;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/H3n;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/FPr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final e:LX/H3o;

.field private final f:LX/H3w;

.field public g:Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;

.field private h:Landroid/view/ViewGroup;

.field private i:Lcom/facebook/widget/listview/BetterListView;

.field public j:LX/H3u;

.field public k:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

.field public l:LX/FOz;

.field public m:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2422217
    const-class v0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    sput-object v0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->d:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2422214
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2422215
    new-instance v0, LX/H3v;

    invoke-direct {v0, p0}, LX/H3v;-><init>(Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->e:LX/H3o;

    .line 2422216
    new-instance v0, LX/H3w;

    invoke-direct {v0, p0}, LX/H3w;-><init>(Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->f:LX/H3w;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;I)V
    .locals 7

    .prologue
    .line 2422208
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2422209
    iget-object v0, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->j:LX/H3u;

    invoke-virtual {v0, p1}, LX/H3u;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    .line 2422210
    if-nez v2, :cond_1

    .line 2422211
    :goto_1
    return-void

    .line 2422212
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2422213
    :cond_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->c:LX/FPr;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->m:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    sget-object v6, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->d:Ljava/lang/Class;

    move-object v5, p0

    invoke-virtual/range {v0 .. v6}, LX/FPr;->a(Landroid/content/Context;Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;Landroid/location/Location;Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;Landroid/support/v4/app/Fragment;Ljava/lang/Class;)V

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2422141
    const-string v0, "nearby_places_intent"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2422205
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2422206
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    invoke-static {p1}, LX/H1i;->b(LX/0QB;)LX/H1i;

    move-result-object v2

    check-cast v2, LX/H1i;

    new-instance v1, LX/H3n;

    invoke-static {p1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p1}, LX/9kE;->b(LX/0QB;)LX/9kE;

    move-result-object v0

    check-cast v0, LX/9kE;

    invoke-direct {v1, v3, v0}, LX/H3n;-><init>(LX/0tX;LX/9kE;)V

    move-object v3, v1

    check-cast v3, LX/H3n;

    invoke-static {p1}, LX/FPr;->a(LX/0QB;)LX/FPr;

    move-result-object p1

    check-cast p1, LX/FPr;

    iput-object v2, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->a:LX/H1i;

    iput-object v3, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->b:LX/H3n;

    iput-object p1, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->c:LX/FPr;

    .line 2422207
    return-void
.end method

.method public final b()V
    .locals 12

    .prologue
    .line 2422172
    iget-object v0, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->j:LX/H3u;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2422173
    iget-object v0, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->l:LX/FOz;

    invoke-interface {v0}, LX/FOz;->c()Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-result-object v0

    .line 2422174
    iget-object v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->c:Landroid/location/Location;

    move-object v0, v1

    .line 2422175
    if-eqz v0, :cond_2

    .line 2422176
    iget-object v0, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->g:Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->g:Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2422177
    if-nez v0, :cond_2

    .line 2422178
    iget-object v2, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->b:LX/H3n;

    invoke-virtual {v2}, LX/H3n;->a()V

    .line 2422179
    iget-object v2, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->l:LX/FOz;

    invoke-interface {v2}, LX/FOz;->c()Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-result-object v2

    .line 2422180
    iget-object v3, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->c:Landroid/location/Location;

    move-object v2, v3

    .line 2422181
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2422182
    iget-object v3, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->b:LX/H3n;

    new-instance v4, LX/H3y;

    invoke-direct {v4, p0}, LX/H3y;-><init>(Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;)V

    .line 2422183
    invoke-virtual {v3}, LX/H3n;->a()V

    .line 2422184
    new-instance v8, LX/4DI;

    invoke-direct {v8}, LX/4DI;-><init>()V

    .line 2422185
    const-string v9, ""

    invoke-virtual {v8, v9}, LX/4DI;->a(Ljava/lang/String;)LX/4DI;

    .line 2422186
    if-eqz v2, :cond_1

    .line 2422187
    new-instance v9, LX/3Aj;

    invoke-direct {v9}, LX/3Aj;-><init>()V

    .line 2422188
    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/3Aj;->a(Ljava/lang/Double;)LX/3Aj;

    .line 2422189
    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/3Aj;->b(Ljava/lang/Double;)LX/3Aj;

    .line 2422190
    invoke-virtual {v2}, Landroid/location/Location;->getAccuracy()F

    move-result v10

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/3Aj;->c(Ljava/lang/Double;)LX/3Aj;

    .line 2422191
    invoke-virtual {v2}, Landroid/location/Location;->hasSpeed()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 2422192
    invoke-virtual {v2}, Landroid/location/Location;->getSpeed()F

    move-result v10

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/3Aj;->d(Ljava/lang/Double;)LX/3Aj;

    .line 2422193
    :cond_0
    invoke-virtual {v8, v9}, LX/4DI;->a(LX/3Aj;)LX/4DI;

    .line 2422194
    :cond_1
    new-instance v9, LX/CRj;

    invoke-direct {v9}, LX/CRj;-><init>()V

    move-object v9, v9

    .line 2422195
    const-string v10, "query_data"

    invoke-virtual {v9, v10, v8}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2422196
    const-string v8, "suggestion_context"

    const-string v10, "nearby_places"

    invoke-virtual {v9, v8, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2422197
    const-string v8, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v10

    invoke-virtual {v9, v8, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2422198
    const-string v8, "friends_who_visited_count"

    const/4 v10, 0x3

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v8, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2422199
    const-string v8, "friendRecommendationsCount"

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v8, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2422200
    move-object v5, v9

    .line 2422201
    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    .line 2422202
    iget-object v6, v3, LX/H3n;->b:LX/9kE;

    iget-object v7, v3, LX/H3n;->a:LX/0tX;

    invoke-virtual {v7, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v5

    invoke-static {v5}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    new-instance v7, LX/H3m;

    invoke-direct {v7, v3, v4}, LX/H3m;-><init>(LX/H3n;LX/0TF;)V

    invoke-virtual {v6, v5, v7}, LX/9kE;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2422203
    :cond_2
    iget-object v0, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->j:LX/H3u;

    const v1, 0x6c016e0c

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2422204
    return-void

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x3a3bf387

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2422157
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2422158
    if-eqz p1, :cond_2

    .line 2422159
    const-string v0, "intent_model_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->g:Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;

    .line 2422160
    :goto_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->l:LX/FOz;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2422161
    iget-object v0, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->j:LX/H3u;

    if-nez v0, :cond_0

    .line 2422162
    new-instance v0, LX/H3u;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->g:Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;

    iget-object v3, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->l:LX/FOz;

    iget-object v4, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->a:LX/H1i;

    iget-object v5, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->m:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2422163
    iget-object p1, v5, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->a:Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    move-object v5, p1

    .line 2422164
    iget-object p1, v5, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->c:Ljava/lang/String;

    move-object v5, p1

    .line 2422165
    invoke-direct/range {v0 .. v5}, LX/H3u;-><init>(Landroid/content/Context;Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;LX/FOz;LX/H1i;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->j:LX/H3u;

    .line 2422166
    iget-object v0, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->j:LX/H3u;

    new-instance v1, LX/H3x;

    invoke-direct {v1, p0}, LX/H3x;-><init>(Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;)V

    .line 2422167
    iput-object v1, v0, LX/H3u;->g:Landroid/view/View$OnClickListener;

    .line 2422168
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->i:Lcom/facebook/widget/listview/BetterListView;

    if-eqz v0, :cond_1

    .line 2422169
    iget-object v0, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->i:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->j:LX/H3u;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2422170
    :cond_1
    const v0, -0x4e4f7dc9

    invoke-static {v0, v6}, LX/02F;->f(II)V

    return-void

    .line 2422171
    :cond_2
    new-instance v0, Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;

    invoke-direct {v0}, Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;-><init>()V

    iput-object v0, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->g:Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6b2d29c2

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2422154
    const v0, 0x7f030bc1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->h:Landroid/view/ViewGroup;

    .line 2422155
    iget-object v0, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->h:Landroid/view/ViewGroup;

    const v2, 0x7f0d1d41

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->i:Lcom/facebook/widget/listview/BetterListView;

    .line 2422156
    iget-object v0, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->h:Landroid/view/ViewGroup;

    const/16 v2, 0x2b

    const v3, 0x2cbe3acf

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4030691

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2422148
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2422149
    iget-object v1, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->j:LX/H3u;

    iget-object v2, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->e:LX/H3o;

    .line 2422150
    iput-object v2, v1, LX/H3u;->f:LX/H3o;

    .line 2422151
    iget-object v1, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->j:LX/H3u;

    iget-object v2, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->f:LX/H3w;

    .line 2422152
    iput-object v2, v1, LX/H3u;->h:LX/H3w;

    .line 2422153
    const/16 v1, 0x2b

    const v2, 0x5df3d33

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2422145
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2422146
    const-string v0, "intent_model_state"

    iget-object v1, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->g:Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2422147
    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x66a84412

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2422142
    iget-object v1, p0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->b:LX/H3n;

    invoke-virtual {v1}, LX/H3n;->a()V

    .line 2422143
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2422144
    const/16 v1, 0x2b

    const v2, 0x4626f3f1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
