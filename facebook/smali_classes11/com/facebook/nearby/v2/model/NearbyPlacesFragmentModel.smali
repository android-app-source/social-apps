.class public Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/FOz;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

.field public b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2236452
    new-instance v0, LX/FOy;

    invoke-direct {v0}, LX/FOy;-><init>()V

    sput-object v0, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/CQB;LX/CQC;LX/FOw;)V
    .locals 1

    .prologue
    .line 2236453
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2236454
    new-instance v0, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    invoke-direct {v0, p1, p2}, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;-><init>(LX/CQB;LX/CQC;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->a:Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    .line 2236455
    new-instance v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    invoke-direct {v0, p3}, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;-><init>(LX/FOw;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2236456
    return-void
.end method

.method public constructor <init>(LX/CQB;LX/CQC;LX/FOw;Lcom/facebook/search/results/model/SearchResultsMutableContext;)V
    .locals 1

    .prologue
    .line 2236457
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2236458
    new-instance v0, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    invoke-direct {v0, p1, p2, p4}, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;-><init>(LX/CQB;LX/CQC;Lcom/facebook/search/results/model/SearchResultsMutableContext;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->a:Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    .line 2236459
    new-instance v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    invoke-direct {v0, p3}, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;-><init>(LX/FOw;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2236460
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2236461
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2236462
    const-class v0, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    iput-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->a:Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    .line 2236463
    const-class v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2236464
    return-void
.end method


# virtual methods
.method public final synthetic c()Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;
    .locals 1

    .prologue
    .line 2236465
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v0, v0

    .line 2236466
    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2236467
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2236468
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->a:Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2236469
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2236470
    return-void
.end method
