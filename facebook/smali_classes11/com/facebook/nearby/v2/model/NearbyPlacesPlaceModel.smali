.class public Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/CQM;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2236513
    new-instance v0, LX/FP0;

    invoke-direct {v0}, LX/FP0;-><init>()V

    sput-object v0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/CQM;)V
    .locals 0

    .prologue
    .line 2236509
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2236510
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2236511
    iput-object p1, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    .line 2236512
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2236506
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2236507
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/CQM;

    iput-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    .line 2236508
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2236505
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2236474
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;
    .locals 1

    .prologue
    .line 2236504
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->b()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;
    .locals 1

    .prologue
    .line 2236503
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->z()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2236502
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;
    .locals 1

    .prologue
    .line 2236501
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->k()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()Landroid/location/Location;
    .locals 4

    .prologue
    .line 2236495
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2236496
    const/4 v0, 0x0

    .line 2236497
    :goto_0
    return-object v0

    .line 2236498
    :cond_0
    new-instance v0, Landroid/location/Location;

    const-string v1, ""

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 2236499
    iget-object v1, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v1}, LX/CQM;->o()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;->a()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 2236500
    iget-object v1, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v1}, LX/CQM;->o()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;->b()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2236494
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->o()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;->c()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2236493
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->x()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2236492
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->I()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 2236489
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->r()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2236490
    const/4 v0, 0x0

    .line 2236491
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->r()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;->a()I

    move-result v0

    goto :goto_0
.end method

.method public final k()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2236488
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->G()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLInterfaces$Photo320Fragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2236485
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->s()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2236486
    const/4 v0, 0x0

    .line 2236487
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->F()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final m()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLInterfaces$NearbyPagePlaceInfoFragment$Hours;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2236482
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->w()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2236483
    const/4 v0, 0x0

    .line 2236484
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->l()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final n()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;
    .locals 1

    .prologue
    .line 2236481
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->q()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    move-result-object v0

    return-object v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 2236480
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->o()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 2236479
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->x()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 2236478
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->F()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->F()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final w()Z
    .locals 1

    .prologue
    .line 2236477
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->l()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2236475
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2236476
    return-void
.end method
