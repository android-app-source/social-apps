.class public Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2236517
    new-instance v0, LX/FP1;

    invoke-direct {v0}, LX/FP1;-><init>()V

    sput-object v0, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2236518
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2236519
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;->a:Ljava/lang/String;

    .line 2236520
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;->b:Ljava/lang/String;

    .line 2236521
    return-void
.end method

.method public constructor <init>(Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;)V
    .locals 1

    .prologue
    .line 2236522
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2236523
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2236524
    iget-object v0, p1, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2236525
    iput-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;->a:Ljava/lang/String;

    .line 2236526
    iget-object v0, p1, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2236527
    iput-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;->b:Ljava/lang/String;

    .line 2236528
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2236529
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2236530
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2236531
    iput-object p1, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;->a:Ljava/lang/String;

    .line 2236532
    iput-object p2, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;->b:Ljava/lang/String;

    .line 2236533
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2236534
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2236535
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2236536
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2236537
    return-void
.end method
