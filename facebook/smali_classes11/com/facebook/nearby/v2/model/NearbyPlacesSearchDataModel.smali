.class public Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/FOw;

.field public b:Z

.field public c:Landroid/location/Location;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:D

.field public g:D

.field public h:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2236590
    new-instance v0, LX/FP3;

    invoke-direct {v0}, LX/FP3;-><init>()V

    sput-object v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/FOw;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2236591
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2236592
    sget-object v0, LX/FOw;->OKAY:LX/FOw;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    .line 2236593
    iput-object p1, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->a:LX/FOw;

    .line 2236594
    iput-object v1, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->d:Ljava/lang/String;

    .line 2236595
    iput-object v1, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->e:Ljava/lang/String;

    .line 2236596
    iput-object v1, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->c:Landroid/location/Location;

    .line 2236597
    iput-object v1, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->h:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    .line 2236598
    return-void

    .line 2236599
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2236611
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2236612
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/FOw;

    iput-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->a:LX/FOw;

    .line 2236613
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    .line 2236614
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->d:Ljava/lang/String;

    .line 2236615
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->e:Ljava/lang/String;

    .line 2236616
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->f:D

    .line 2236617
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->g:D

    .line 2236618
    const-class v0, Landroid/location/Location;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->c:Landroid/location/Location;

    .line 2236619
    const-class v0, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    iput-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->h:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    .line 2236620
    return-void

    .line 2236621
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2236600
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2236601
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2236602
    iget-object v0, p1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->a:LX/FOw;

    iput-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->a:LX/FOw;

    .line 2236603
    iget-boolean v0, p1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    .line 2236604
    iget-object v0, p1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->d:Ljava/lang/String;

    .line 2236605
    iget-object v0, p1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->e:Ljava/lang/String;

    .line 2236606
    iget-object v0, p1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->c:Landroid/location/Location;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->c:Landroid/location/Location;

    .line 2236607
    iget-object v0, p1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->h:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    if-nez v0, :cond_1

    :goto_1
    iput-object v1, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->h:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    .line 2236608
    return-void

    .line 2236609
    :cond_0
    new-instance v0, Landroid/location/Location;

    iget-object v2, p1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->c:Landroid/location/Location;

    invoke-direct {v0, v2}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    goto :goto_0

    .line 2236610
    :cond_1
    new-instance v1, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    iget-object v0, p1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->h:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    invoke-direct {v1, v0}, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;-><init>(Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/location/Location;)V
    .locals 0

    .prologue
    .line 2236587
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2236588
    iput-object p1, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->c:Landroid/location/Location;

    .line 2236589
    return-void
.end method

.method public final d()LX/FP2;
    .locals 2

    .prologue
    .line 2236564
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->h:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    if-nez v0, :cond_0

    .line 2236565
    sget-object v0, LX/FP2;->INVALID:LX/FP2;

    .line 2236566
    :goto_0
    return-object v0

    .line 2236567
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->h:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    .line 2236568
    iget-object v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2236569
    if-eqz v0, :cond_6

    const-string v1, "keywords_places"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 2236570
    if-eqz v0, :cond_1

    .line 2236571
    sget-object v0, LX/FP2;->KEYWORDS_PLACES:LX/FP2;

    goto :goto_0

    .line 2236572
    :cond_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->h:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    .line 2236573
    iget-object v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2236574
    if-eqz v0, :cond_7

    const-string v1, "places-in"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    :goto_2
    move v0, v1

    .line 2236575
    if-eqz v0, :cond_2

    .line 2236576
    sget-object v0, LX/FP2;->PLACES_IN:LX/FP2;

    goto :goto_0

    .line 2236577
    :cond_2
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->h:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    .line 2236578
    iget-object v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2236579
    if-eqz v0, :cond_8

    const-string v1, "intersect"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v1, 0x1

    :goto_3
    move v0, v1

    .line 2236580
    if-eqz v0, :cond_3

    .line 2236581
    sget-object v0, LX/FP2;->INTERSECT:LX/FP2;

    goto :goto_0

    .line 2236582
    :cond_3
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    if-eqz v0, :cond_4

    .line 2236583
    sget-object v0, LX/FP2;->CURRENT_LOCATION:LX/FP2;

    goto :goto_0

    .line 2236584
    :cond_4
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2236585
    sget-object v0, LX/FP2;->SPECIFIED_LOCATION:LX/FP2;

    goto :goto_0

    .line 2236586
    :cond_5
    sget-object v0, LX/FP2;->INVALID:LX/FP2;

    goto :goto_0

    :cond_6
    const/4 v1, 0x0

    goto :goto_1

    :cond_7
    const/4 v1, 0x0

    goto :goto_2

    :cond_8
    const/4 v1, 0x0

    goto :goto_3
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2236551
    const/4 v0, 0x0

    return v0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 2236563
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->a:LX/FOw;

    sget-object v1, LX/FOw;->OKAY:LX/FOw;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 2236562
    sget-object v0, LX/FP2;->INVALID:LX/FP2;

    invoke-virtual {p0}, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->d()LX/FP2;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/FP2;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2236552
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->a:LX/FOw;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2236553
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2236554
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2236555
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2236556
    iget-wide v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->f:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 2236557
    iget-wide v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->g:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 2236558
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->c:Landroid/location/Location;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2236559
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->h:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2236560
    return-void

    .line 2236561
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
