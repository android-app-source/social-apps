.class public Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2423170
    new-instance v0, LX/H4b;

    invoke-direct {v0}, LX/H4b;-><init>()V

    sput-object v0, Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2423171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2423172
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;->a()V

    .line 2423173
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;->b()V

    .line 2423174
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2423175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2423176
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;->a:Ljava/lang/String;

    .line 2423177
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;->b:Ljava/lang/String;

    .line 2423178
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2423179
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;->a:Ljava/lang/String;

    .line 2423180
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2423181
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;->b:Ljava/lang/String;

    .line 2423182
    return-void
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2423183
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2423184
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2423185
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2423186
    return-void
.end method
