.class public Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public a:LX/H4c;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/H4I;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/H4N;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

.field public g:Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;

.field private h:Landroid/view/ViewGroup;

.field public i:Landroid/view/ViewGroup;

.field public j:Lcom/facebook/widget/listview/BetterListView;

.field public k:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

.field public l:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

.field public m:Landroid/widget/ProgressBar;

.field public n:LX/H4G;

.field public o:LX/H4L;

.field public p:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

.field public q:LX/H4a;

.field public r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

.field private final s:Landroid/view/View$OnFocusChangeListener;

.field private final t:Landroid/widget/AbsListView$OnScrollListener;

.field private final u:Landroid/text/TextWatcher;

.field private final v:Landroid/widget/TextView$OnEditorActionListener;

.field private final w:Landroid/text/TextWatcher;

.field public final x:Landroid/widget/AdapterView$OnItemClickListener;

.field public final y:Landroid/widget/AdapterView$OnItemClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2422925
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2422926
    new-instance v0, LX/H4O;

    invoke-direct {v0, p0}, LX/H4O;-><init>(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->s:Landroid/view/View$OnFocusChangeListener;

    .line 2422927
    new-instance v0, LX/H4P;

    invoke-direct {v0, p0}, LX/H4P;-><init>(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->t:Landroid/widget/AbsListView$OnScrollListener;

    .line 2422928
    new-instance v0, LX/H4Q;

    invoke-direct {v0, p0}, LX/H4Q;-><init>(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->u:Landroid/text/TextWatcher;

    .line 2422929
    new-instance v0, LX/H4R;

    invoke-direct {v0, p0}, LX/H4R;-><init>(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->v:Landroid/widget/TextView$OnEditorActionListener;

    .line 2422930
    new-instance v0, LX/H4S;

    invoke-direct {v0, p0}, LX/H4S;-><init>(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->w:Landroid/text/TextWatcher;

    .line 2422931
    new-instance v0, LX/H4T;

    invoke-direct {v0, p0}, LX/H4T;-><init>(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->x:Landroid/widget/AdapterView$OnItemClickListener;

    .line 2422932
    new-instance v0, LX/H4U;

    invoke-direct {v0, p0}, LX/H4U;-><init>(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->y:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2422853
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->l:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2422854
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->l:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->setTextColor(I)V

    .line 2422855
    return-void
.end method

.method private m()V
    .locals 3

    .prologue
    .line 2422933
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->k:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    if-eqz v0, :cond_0

    .line 2422934
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2422935
    iget-object v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->h:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    move-object v0, v1

    .line 2422936
    if-eqz v0, :cond_2

    .line 2422937
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->k:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    iget-object v1, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v1, v1, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2422938
    iget-object v2, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->h:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    move-object v1, v2

    .line 2422939
    iget-object v2, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2422940
    invoke-virtual {v0, v1}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2422941
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->l:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    if-eqz v0, :cond_1

    .line 2422942
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2422943
    iget-object v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->e:Ljava/lang/String;

    move-object v0, v1

    .line 2422944
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2422945
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2422946
    iget-object v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->e:Ljava/lang/String;

    move-object v0, v1

    .line 2422947
    invoke-static {p0, v0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->a$redex0(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;Ljava/lang/String;)V

    .line 2422948
    :cond_1
    :goto_1
    return-void

    .line 2422949
    :cond_2
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->k:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2422950
    :cond_3
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2422951
    iget-boolean v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    move v0, v1

    .line 2422952
    if-eqz v0, :cond_4

    .line 2422953
    invoke-static {p0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->n(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;)V

    goto :goto_1

    .line 2422954
    :cond_4
    const-string v0, ""

    invoke-static {p0, v0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->a$redex0(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static n(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;)V
    .locals 3

    .prologue
    .line 2422955
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->l:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082123

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2422956
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->l:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->setTextColor(I)V

    .line 2422957
    return-void
.end method

.method public static p(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;)V
    .locals 12

    .prologue
    .line 2422968
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->m:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2422969
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    .line 2422970
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;

    .line 2422971
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->o:LX/H4L;

    if-ne v0, v1, :cond_0

    .line 2422972
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->o:LX/H4L;

    const v1, -0x155279eb

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2422973
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->k:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 2422974
    const-wide/16 v8, 0x0

    .line 2422975
    iget-object v6, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v6, v6, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2422976
    iget-object v7, v6, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->d:Ljava/lang/String;

    move-object v6, v7

    .line 2422977
    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v6, v6, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2422978
    iget-wide v10, v6, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->f:D

    move-wide v6, v10

    .line 2422979
    cmpl-double v6, v6, v8

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v6, v6, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2422980
    iget-wide v10, v6, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->g:D

    move-wide v6, v10

    .line 2422981
    cmpl-double v6, v6, v8

    if-eqz v6, :cond_2

    .line 2422982
    new-instance v6, Landroid/location/Location;

    const-string v7, ""

    invoke-direct {v6, v7}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 2422983
    iget-object v7, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v7, v7, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2422984
    iget-wide v10, v7, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->f:D

    move-wide v8, v10

    .line 2422985
    invoke-virtual {v6, v8, v9}, Landroid/location/Location;->setLatitude(D)V

    .line 2422986
    iget-object v7, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v7, v7, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2422987
    iget-wide v10, v7, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->g:D

    move-wide v8, v10

    .line 2422988
    invoke-virtual {v6, v8, v9}, Landroid/location/Location;->setLongitude(D)V

    .line 2422989
    :goto_0
    move-object v4, v6

    .line 2422990
    if-nez v4, :cond_1

    new-instance v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;

    invoke-direct {v0, v1}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;-><init>(Ljava/lang/String;)V

    .line 2422991
    :goto_1
    iget-object v1, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->c:LX/H4N;

    new-instance v2, LX/H4V;

    invoke-direct {v2, p0}, LX/H4V;-><init>(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;)V

    invoke-virtual {v1, v0, v2}, LX/H4N;->a(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;LX/0TF;)V

    .line 2422992
    return-void

    .line 2422993
    :cond_1
    new-instance v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;

    invoke-virtual {v4}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;-><init>(Ljava/lang/String;DD)V

    goto :goto_1

    .line 2422994
    :cond_2
    iget-object v6, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v6, v6, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2422995
    iget-boolean v7, v6, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    move v6, v7

    .line 2422996
    if-eqz v6, :cond_3

    .line 2422997
    iget-object v6, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v6, v6, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2422998
    iget-object v7, v6, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->c:Landroid/location/Location;

    move-object v6, v7

    .line 2422999
    goto :goto_0

    .line 2423000
    :cond_3
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static q(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;)V
    .locals 3

    .prologue
    .line 2422958
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->m:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2422959
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->b()V

    .line 2422960
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->n:LX/H4G;

    if-ne v0, v1, :cond_0

    .line 2422961
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->n:LX/H4G;

    const v1, -0x286e1d1d

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2422962
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2422963
    iget-boolean v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    move v0, v1

    .line 2422964
    if-eqz v0, :cond_1

    const-string v0, ""

    :goto_0
    move-object v0, v0

    .line 2422965
    iget-object v1, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->b:LX/H4I;

    new-instance v2, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;

    invoke-direct {v2, v0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;-><init>(Ljava/lang/String;)V

    new-instance v0, LX/H4W;

    invoke-direct {v0, p0}, LX/H4W;-><init>(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;)V

    invoke-virtual {v1, v2, v0}, LX/H4I;->a(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;LX/0TF;)V

    .line 2422966
    return-void

    :cond_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->l:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2422967
    const-string v0, "nearby_places_typeahead"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2422918
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2422919
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    const-class v3, LX/H4c;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/H4c;

    invoke-static {v0}, LX/H4I;->b(LX/0QB;)LX/H4I;

    move-result-object v4

    check-cast v4, LX/H4I;

    new-instance p0, LX/H4N;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/9kE;->b(LX/0QB;)LX/9kE;

    move-result-object p1

    check-cast p1, LX/9kE;

    invoke-direct {p0, v5, p1}, LX/H4N;-><init>(LX/0tX;LX/9kE;)V

    move-object v5, p0

    check-cast v5, LX/H4N;

    const/16 p1, 0xbc6

    invoke-static {v0, p1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v3, v2, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->a:LX/H4c;

    iput-object v4, v2, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->b:LX/H4I;

    iput-object v5, v2, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->c:LX/H4N;

    iput-object p1, v2, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->d:LX/0Or;

    iput-object v0, v2, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->e:Lcom/facebook/content/SecureContextHelper;

    .line 2422920
    return-void
.end method

.method public final a(Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;)V
    .locals 2

    .prologue
    .line 2422921
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2422922
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    new-instance v1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    invoke-direct {v1, p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;-><init>(Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;)V

    iput-object v1, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2422923
    invoke-direct {p0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->m()V

    .line 2422924
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2422916
    iget-object v1, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->i:Landroid/view/ViewGroup;

    if-nez v1, :cond_1

    .line 2422917
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->i:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2422911
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->l:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->clearFocus()V

    .line 2422912
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->k:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->clearFocus()V

    .line 2422913
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->i:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2422914
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->e()V

    .line 2422915
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 2422906
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2422907
    if-eqz v0, :cond_0

    .line 2422908
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2422909
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2422910
    :cond_0
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x2

    const/16 v2, 0x2a

    const v3, 0x1a9ae6a8

    invoke-static {v0, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2422884
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2422885
    if-eqz p1, :cond_3

    .line 2422886
    const-string v0, "typeahead_model_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    .line 2422887
    const-string v0, "typeahead_is_visible_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 2422888
    iget-object v3, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->i:Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2422889
    const-string v0, "typeahead_logger_data_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->g:Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;

    .line 2422890
    :goto_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2422891
    invoke-direct {p0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->m()V

    .line 2422892
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->a:LX/H4c;

    iget-object v3, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2422893
    new-instance p1, LX/H4a;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-direct {p1, v3, p0, p0, v4}, LX/H4a;-><init>(Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;LX/0Zb;)V

    .line 2422894
    move-object v0, p1

    .line 2422895
    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->q:LX/H4a;

    .line 2422896
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->n:LX/H4G;

    if-nez v0, :cond_0

    .line 2422897
    new-instance v0, LX/H4G;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    invoke-direct {v0, v3, v4, v1}, LX/H4G;-><init>(Landroid/content/Context;Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;Z)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->n:LX/H4G;

    .line 2422898
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->o:LX/H4L;

    if-nez v0, :cond_1

    .line 2422899
    new-instance v0, LX/H4L;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    invoke-direct {v0, v1, v3}, LX/H4L;-><init>(Landroid/content/Context;Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->o:LX/H4L;

    .line 2422900
    :cond_1
    const v0, -0x6dae6cb

    invoke-static {v0, v2}, LX/02F;->f(II)V

    return-void

    .line 2422901
    :cond_2
    const/16 v0, 0x8

    goto :goto_0

    .line 2422902
    :cond_3
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2422903
    const-string v3, "nearby_places_search_data"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2422904
    new-instance v3, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    new-instance v4, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    invoke-direct {v4, v0}, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;-><init>(Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;)V

    invoke-direct {v3, v4}, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;-><init>(Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;)V

    iput-object v3, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    .line 2422905
    new-instance v0, Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;

    invoke-direct {v0}, Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;-><init>()V

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->g:Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;

    goto :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x4f178c37

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2422869
    const v0, 0x7f030bbc

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->h:Landroid/view/ViewGroup;

    .line 2422870
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->h:Landroid/view/ViewGroup;

    const v2, 0x7f0d1d31

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->i:Landroid/view/ViewGroup;

    .line 2422871
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->h:Landroid/view/ViewGroup;

    const v2, 0x7f0d1d32

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    .line 2422872
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->t:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2422873
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->h:Landroid/view/ViewGroup;

    const v2, 0x7f0d1d2f

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->k:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    .line 2422874
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->k:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    iget-object v2, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->s:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v2}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2422875
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->k:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    iget-object v2, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->w:Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->setInputTextListener(Landroid/text/TextWatcher;)V

    .line 2422876
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->k:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    iget-object v2, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->v:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v0, v2}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 2422877
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->h:Landroid/view/ViewGroup;

    const v2, 0x7f0d1d30

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->l:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    .line 2422878
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->h:Landroid/view/ViewGroup;

    const v2, 0x7f0d1d33

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->m:Landroid/widget/ProgressBar;

    .line 2422879
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->m:Landroid/widget/ProgressBar;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2422880
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->l:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    iget-object v2, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->s:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v2}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2422881
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->l:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    iget-object v2, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->u:Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->setInputTextListener(Landroid/text/TextWatcher;)V

    .line 2422882
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->l:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    iget-object v2, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->v:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v0, v2}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 2422883
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->h:Landroid/view/ViewGroup;

    const/16 v2, 0x2b

    const v3, 0xd0d5417

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2422864
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2422865
    const-string v0, "typeahead_model_state"

    iget-object v1, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2422866
    const-string v0, "typeahead_is_visible_state"

    invoke-virtual {p0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->c()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2422867
    const-string v0, "typeahead_logger_data_state"

    iget-object v1, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->g:Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2422868
    return-void
.end method

.method public final onStart()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x62e07ee0

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2422856
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2422857
    iget-object v1, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->o:LX/H4L;

    if-ne v1, v2, :cond_1

    .line 2422858
    iget-object v1, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v1, v1, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;

    if-eqz v1, :cond_0

    .line 2422859
    iget-object v1, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->q:LX/H4a;

    sget-object v2, LX/H4Z;->PLACES_TOPICS:LX/H4Z;

    invoke-virtual {v1, v2}, LX/H4a;->b(LX/H4Z;)V

    .line 2422860
    :cond_0
    :goto_0
    const v1, -0x6dc51984

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 2422861
    :cond_1
    iget-object v1, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->n:LX/H4G;

    if-ne v1, v2, :cond_0

    .line 2422862
    iget-object v1, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v1, v1, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->c:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    if-eqz v1, :cond_0

    .line 2422863
    iget-object v1, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->q:LX/H4a;

    sget-object v2, LX/H4Z;->LOCATIONS:LX/H4Z;

    invoke-virtual {v1, v2}, LX/H4a;->b(LX/H4Z;)V

    goto :goto_0
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x20495f6b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2422849
    iget-object v1, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->c:LX/H4N;

    invoke-virtual {v1}, LX/H4N;->a()V

    .line 2422850
    iget-object v1, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->b:LX/H4I;

    invoke-virtual {v1}, LX/H4I;->a()V

    .line 2422851
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2422852
    const/16 v1, 0x2b

    const v2, -0x6e17deb4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
