.class public Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

.field public b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2423257
    new-instance v0, LX/H4f;

    invoke-direct {v0}, LX/H4f;-><init>()V

    sput-object v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2423248
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2423249
    const-class v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2423250
    const-class v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;

    .line 2423251
    const-class v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->c:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    .line 2423252
    return-void
.end method

.method public constructor <init>(Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;)V
    .locals 1

    .prologue
    .line 2423254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2423255
    new-instance v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    invoke-direct {v0, p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;-><init>(Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;)V

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2423256
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 1

    .prologue
    .line 2423258
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->c:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    .line 2423259
    return-void
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2423253
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2423244
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2423245
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2423246
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->c:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2423247
    return-void
.end method
