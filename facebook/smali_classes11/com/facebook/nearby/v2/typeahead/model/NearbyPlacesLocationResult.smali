.class public Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel$NodeModel;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2423192
    new-instance v0, LX/H4d;

    invoke-direct {v0}, LX/H4d;-><init>()V

    sput-object v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2423193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2423194
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;->a:Ljava/lang/String;

    .line 2423195
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    .line 2423196
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;->b:LX/0Px;

    .line 2423197
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;->c:Ljava/lang/String;

    .line 2423198
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0Px;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel$NodeModel;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2423199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2423200
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2423201
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2423202
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2423203
    iput-object p1, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;->a:Ljava/lang/String;

    .line 2423204
    iput-object p2, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;->b:LX/0Px;

    .line 2423205
    iput-object p3, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;->c:Ljava/lang/String;

    .line 2423206
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2423207
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2423208
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2423209
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;->b:LX/0Px;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 2423210
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2423211
    return-void
.end method
