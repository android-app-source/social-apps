.class public Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadPlaceFragmentModel;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2423215
    new-instance v0, LX/H4e;

    invoke-direct {v0}, LX/H4e;-><init>()V

    sput-object v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2423232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2423233
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->a:Ljava/lang/String;

    .line 2423234
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    .line 2423235
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->b:LX/0Px;

    .line 2423236
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2423237
    const-class v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 2423238
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->c:LX/0Px;

    .line 2423239
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->d:Ljava/lang/String;

    .line 2423240
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadPlaceFragmentModel;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2423222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2423223
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2423224
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2423225
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2423226
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2423227
    iput-object p1, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->a:Ljava/lang/String;

    .line 2423228
    invoke-static {p2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->b:LX/0Px;

    .line 2423229
    invoke-static {p3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->c:LX/0Px;

    .line 2423230
    iput-object p4, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->d:Ljava/lang/String;

    .line 2423231
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2423221
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2423216
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2423217
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->b:LX/0Px;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 2423218
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->c:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2423219
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2423220
    return-void
.end method
