.class public Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;
.super Lcom/facebook/resources/ui/FbEditText;
.source ""


# instance fields
.field private b:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2422718
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2422719
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2422700
    const v0, 0x101006e

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2422701
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2422716
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2422717
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2422713
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->b:Landroid/text/TextWatcher;

    if-eqz v0, :cond_0

    .line 2422714
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->b:Landroid/text/TextWatcher;

    invoke-virtual {p0, v0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2422715
    :cond_0
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 2422710
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->b:Landroid/text/TextWatcher;

    if-eqz v0, :cond_0

    .line 2422711
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->b:Landroid/text/TextWatcher;

    invoke-virtual {p0, v0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2422712
    :cond_0
    return-void
.end method


# virtual methods
.method public setInputTextListener(Landroid/text/TextWatcher;)V
    .locals 0

    .prologue
    .line 2422706
    invoke-direct {p0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->a()V

    .line 2422707
    iput-object p1, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->b:Landroid/text/TextWatcher;

    .line 2422708
    invoke-direct {p0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->b()V

    .line 2422709
    return-void
.end method

.method public final setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .locals 0

    .prologue
    .line 2422702
    invoke-direct {p0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->a()V

    .line 2422703
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 2422704
    invoke-direct {p0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->b()V

    .line 2422705
    return-void
.end method
