.class public Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:D

.field public final b:D

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2423021
    new-instance v0, LX/H4X;

    invoke-direct {v0}, LX/H4X;-><init>()V

    sput-object v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2423016
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2423017
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;->c:Ljava/lang/String;

    .line 2423018
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;->a:D

    .line 2423019
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;->b:D

    .line 2423020
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 2423014
    move-object v0, p0

    move-object v1, p1

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;-><init>(Ljava/lang/String;DD)V

    .line 2423015
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;DD)V
    .locals 0

    .prologue
    .line 2423009
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2423010
    iput-object p1, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;->c:Ljava/lang/String;

    .line 2423011
    iput-wide p2, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;->a:D

    .line 2423012
    iput-wide p4, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;->b:D

    .line 2423013
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2423008
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2423004
    iget-object v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2423005
    iget-wide v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;->a:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 2423006
    iget-wide v0, p0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;->b:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 2423007
    return-void
.end method
