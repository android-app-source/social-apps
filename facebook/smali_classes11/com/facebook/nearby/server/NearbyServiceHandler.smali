.class public Lcom/facebook/nearby/server/NearbyServiceHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile m:Lcom/facebook/nearby/server/NearbyServiceHandler;


# instance fields
.field private final a:LX/18V;

.field private final b:LX/H3J;

.field private final c:LX/H2u;

.field private final d:LX/H2N;

.field private final e:LX/H2Q;

.field private final f:LX/H1u;

.field private final g:LX/H22;

.field private final h:LX/H1s;

.field private final i:LX/0SG;

.field private final j:LX/03V;

.field private final k:LX/H1i;

.field private volatile l:Lcom/facebook/graphql/model/GraphQLGeoRectangle;


# direct methods
.method public constructor <init>(LX/18V;LX/H3J;LX/H2u;LX/H2N;LX/H2Q;LX/H1u;LX/H22;LX/0SG;LX/03V;LX/H1i;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2421309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2421310
    iput-object p1, p0, Lcom/facebook/nearby/server/NearbyServiceHandler;->a:LX/18V;

    .line 2421311
    iput-object p2, p0, Lcom/facebook/nearby/server/NearbyServiceHandler;->b:LX/H3J;

    .line 2421312
    iput-object p3, p0, Lcom/facebook/nearby/server/NearbyServiceHandler;->c:LX/H2u;

    .line 2421313
    iput-object p4, p0, Lcom/facebook/nearby/server/NearbyServiceHandler;->d:LX/H2N;

    .line 2421314
    iput-object p5, p0, Lcom/facebook/nearby/server/NearbyServiceHandler;->e:LX/H2Q;

    .line 2421315
    iput-object p6, p0, Lcom/facebook/nearby/server/NearbyServiceHandler;->f:LX/H1u;

    .line 2421316
    iput-object p7, p0, Lcom/facebook/nearby/server/NearbyServiceHandler;->g:LX/H22;

    .line 2421317
    iput-object p8, p0, Lcom/facebook/nearby/server/NearbyServiceHandler;->i:LX/0SG;

    .line 2421318
    iput-object p9, p0, Lcom/facebook/nearby/server/NearbyServiceHandler;->j:LX/03V;

    .line 2421319
    iput-object p10, p0, Lcom/facebook/nearby/server/NearbyServiceHandler;->k:LX/H1i;

    .line 2421320
    new-instance v0, LX/H1s;

    iget-object v1, p0, Lcom/facebook/nearby/server/NearbyServiceHandler;->i:LX/0SG;

    invoke-direct {v0, v1}, LX/H1s;-><init>(LX/0SG;)V

    iput-object v0, p0, Lcom/facebook/nearby/server/NearbyServiceHandler;->h:LX/H1s;

    .line 2421321
    return-void
.end method

.method private a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 18

    .prologue
    .line 2421322
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/nearby/server/NearbyServiceHandler;->a:LX/18V;

    invoke-virtual {v2}, LX/18V;->a()LX/2VK;

    move-result-object v9

    .line 2421323
    invoke-virtual/range {p1 .. p1}, LX/1qK;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    .line 2421324
    const-string v3, "fetchLayoutAndTilesParams"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsParams;

    .line 2421325
    iget-object v0, v2, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsParams;->a:Lcom/facebook/nearby/protocol/NearbyTilesParams;

    move-object/from16 v17, v0

    .line 2421326
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/nearby/protocol/NearbyTilesParams;->a()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v16

    .line 2421327
    if-eqz v16, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/nearby/server/NearbyServiceHandler;->l:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-object/from16 v0, v16

    invoke-static {v0, v3}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLGeoRectangle;Lcom/facebook/graphql/model/GraphQLGeoRectangle;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2421328
    sget-object v2, LX/1nY;->NO_ERROR:LX/1nY;

    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2421329
    :goto_0
    return-object v2

    .line 2421330
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/nearby/server/NearbyServiceHandler;->f:LX/H1u;

    invoke-virtual {v3}, LX/H1u;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2421331
    invoke-static/range {p0 .. p0}, Lcom/facebook/nearby/server/NearbyServiceHandler;->a(Lcom/facebook/nearby/server/NearbyServiceHandler;)V

    .line 2421332
    if-eqz v16, :cond_1

    .line 2421333
    new-instance v8, Landroid/graphics/RectF;

    invoke-virtual/range {v16 .. v16}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->l()D

    move-result-wide v4

    double-to-float v3, v4

    invoke-virtual/range {v16 .. v16}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->j()D

    move-result-wide v4

    double-to-float v4, v4

    invoke-virtual/range {v16 .. v16}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->a()D

    move-result-wide v6

    double-to-float v5, v6

    invoke-virtual/range {v16 .. v16}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->k()D

    move-result-wide v6

    double-to-float v6, v6

    invoke-direct {v8, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2421334
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/nearby/protocol/NearbyTilesParams;->j()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v7

    .line 2421335
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/nearby/protocol/NearbyTilesParams;->k()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v6

    .line 2421336
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/nearby/server/NearbyServiceHandler;->f:LX/H1u;

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/nearby/protocol/NearbyTilesParams;->b()F

    move-result v4

    float-to-double v4, v4

    invoke-virtual/range {v3 .. v8}, LX/H1u;->a(DLjava/util/Set;Ljava/util/Set;Landroid/graphics/RectF;)Ljava/util/List;

    move-result-object v12

    .line 2421337
    invoke-static {v8, v12}, LX/H1s;->a(Landroid/graphics/RectF;Ljava/util/List;)D

    move-result-wide v4

    const-wide v10, 0x3fef0a3d70a3d70aL    # 0.97

    cmpl-double v3, v4, v10

    if-ltz v3, :cond_1

    .line 2421338
    new-instance v8, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;

    sget-object v9, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/nearby/server/NearbyServiceHandler;->i:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/nearby/server/NearbyServiceHandler;->f:LX/H1u;

    invoke-virtual {v2}, LX/H1u;->b()Ljava/lang/String;

    move-result-object v13

    move-object v14, v6

    move-object v15, v7

    invoke-direct/range {v8 .. v17}, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;-><init>(LX/0ta;JLjava/util/List;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;Lcom/facebook/graphql/model/GraphQLGeoRectangle;Lcom/facebook/nearby/protocol/NearbyTilesParams;)V

    .line 2421339
    invoke-static {v8}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    goto :goto_0

    .line 2421340
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/nearby/server/NearbyServiceHandler;->k:LX/H1i;

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/nearby/protocol/NearbyTilesParams;->j()Ljava/util/List;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/nearby/protocol/NearbyTilesParams;->a()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v5

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/nearby/protocol/NearbyTilesParams;->b()F

    move-result v6

    invoke-virtual {v3, v4, v5, v6}, LX/H1i;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLGeoRectangle;F)V

    .line 2421341
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/nearby/server/NearbyServiceHandler;->c:LX/H2u;

    move-object/from16 v0, v17

    invoke-static {v3, v0}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v3

    const-string v4, "nearby_tiles"

    invoke-virtual {v3, v4}, LX/2Vk;->a(Ljava/lang/String;)LX/2Vk;

    move-result-object v3

    invoke-virtual {v3}, LX/2Vk;->a()LX/2Vj;

    move-result-object v3

    invoke-interface {v9, v3}, LX/2VK;->a(LX/2Vj;)V

    .line 2421342
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/nearby/server/NearbyServiceHandler;->d:LX/H2N;

    iget-object v2, v2, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsParams;->b:Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutParams;

    invoke-static {v3, v2}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v2

    const-string v3, "layout_places"

    invoke-virtual {v2, v3}, LX/2Vk;->a(Ljava/lang/String;)LX/2Vk;

    move-result-object v2

    invoke-virtual {v2}, LX/2Vk;->a()LX/2Vj;

    move-result-object v2

    invoke-interface {v9, v2}, LX/2VK;->a(LX/2Vj;)V

    .line 2421343
    :try_start_0
    const-string v2, "fetchLayoutAndTilesRequest"

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-interface {v9, v2, v3}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2421344
    :goto_1
    const-string v2, "nearby_tiles"

    invoke-interface {v9, v2}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/nearby/protocol/NearbyTilesResult;

    .line 2421345
    const-string v3, "nearby_tiles"

    invoke-interface {v9, v3}, LX/2VK;->b(Ljava/lang/String;)Ljava/lang/Exception;

    move-result-object v3

    .line 2421346
    if-eqz v3, :cond_2

    .line 2421347
    throw v3

    .line 2421348
    :catch_0
    move-exception v2

    .line 2421349
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/nearby/server/NearbyServiceHandler;->j:LX/03V;

    const-string v4, "FETCH_LAYOUT_AND_TILES_REQUEST"

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, LX/0VK;->a(Z)LX/0VK;

    move-result-object v2

    invoke-virtual {v2}, LX/0VK;->g()LX/0VG;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/03V;->a(LX/0VG;)V

    goto :goto_1

    .line 2421350
    :cond_2
    if-nez v2, :cond_3

    .line 2421351
    sget-object v2, LX/1nY;->OTHER:LX/1nY;

    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    goto/16 :goto_0

    .line 2421352
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/nearby/protocol/NearbyTilesResult;->f()Lcom/facebook/nearby/protocol/NearbyTilesParams;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/nearby/protocol/NearbyTilesParams;->a()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v3

    if-nez v3, :cond_4

    invoke-virtual {v2}, Lcom/facebook/nearby/protocol/NearbyTilesResult;->e()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v3

    if-nez v3, :cond_4

    .line 2421353
    sget-object v2, LX/1nY;->OTHER:LX/1nY;

    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    goto/16 :goto_0

    .line 2421354
    :cond_4
    const-string v3, "layout_places"

    invoke-interface {v9, v3}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutResult;

    .line 2421355
    new-instance v10, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;

    invoke-direct {v10, v2, v3}, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;-><init>(Lcom/facebook/nearby/protocol/NearbyTilesResult;Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutResult;)V

    .line 2421356
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/nearby/server/NearbyServiceHandler;->f:LX/H1u;

    invoke-virtual {v2}, Lcom/facebook/nearby/protocol/NearbyTilesResult;->c()Ljava/util/Set;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/nearby/protocol/NearbyTilesResult;->d()Ljava/util/Set;

    move-result-object v5

    invoke-virtual {v10}, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->a()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v2}, Lcom/facebook/nearby/protocol/NearbyTilesResult;->b()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/nearby/server/NearbyServiceHandler;->i:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v8

    invoke-virtual/range {v3 .. v9}, LX/H1u;->a(Ljava/util/Set;Ljava/util/Set;Ljava/util/List;Ljava/lang/String;J)V

    .line 2421357
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/nearby/server/NearbyServiceHandler;->g:LX/H22;

    invoke-virtual {v2, v10}, LX/H22;->a(Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;)V

    .line 2421358
    invoke-static {v10}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/nearby/server/NearbyServiceHandler;
    .locals 14

    .prologue
    .line 2421359
    sget-object v0, Lcom/facebook/nearby/server/NearbyServiceHandler;->m:Lcom/facebook/nearby/server/NearbyServiceHandler;

    if-nez v0, :cond_1

    .line 2421360
    const-class v1, Lcom/facebook/nearby/server/NearbyServiceHandler;

    monitor-enter v1

    .line 2421361
    :try_start_0
    sget-object v0, Lcom/facebook/nearby/server/NearbyServiceHandler;->m:Lcom/facebook/nearby/server/NearbyServiceHandler;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2421362
    if-eqz v2, :cond_0

    .line 2421363
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2421364
    new-instance v3, Lcom/facebook/nearby/server/NearbyServiceHandler;

    invoke-static {v0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v4

    check-cast v4, LX/18V;

    .line 2421365
    new-instance v9, LX/H3J;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v7

    check-cast v7, LX/0lC;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v8

    check-cast v8, LX/0sO;

    invoke-direct {v9, v5, v6, v7, v8}, LX/H3J;-><init>(LX/0SG;Landroid/content/res/Resources;LX/0lC;LX/0sO;)V

    .line 2421366
    move-object v5, v9

    .line 2421367
    check-cast v5, LX/H3J;

    .line 2421368
    new-instance v9, LX/H2u;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v8

    check-cast v8, LX/0sO;

    invoke-direct {v9, v6, v7, v8}, LX/H2u;-><init>(LX/0SG;Landroid/content/res/Resources;LX/0sO;)V

    .line 2421369
    move-object v6, v9

    .line 2421370
    check-cast v6, LX/H2u;

    .line 2421371
    new-instance v8, LX/H2N;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-direct {v8, v7}, LX/H2N;-><init>(LX/0SG;)V

    .line 2421372
    move-object v7, v8

    .line 2421373
    check-cast v7, LX/H2N;

    .line 2421374
    new-instance v9, LX/H2Q;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-direct {v9, v8}, LX/H2Q;-><init>(LX/0SG;)V

    .line 2421375
    move-object v8, v9

    .line 2421376
    check-cast v8, LX/H2Q;

    invoke-static {v0}, LX/H1u;->a(LX/0QB;)LX/H1u;

    move-result-object v9

    check-cast v9, LX/H1u;

    invoke-static {v0}, LX/H22;->a(LX/0QB;)LX/H22;

    move-result-object v10

    check-cast v10, LX/H22;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v11

    check-cast v11, LX/0SG;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v12

    check-cast v12, LX/03V;

    invoke-static {v0}, LX/H1i;->b(LX/0QB;)LX/H1i;

    move-result-object v13

    check-cast v13, LX/H1i;

    invoke-direct/range {v3 .. v13}, Lcom/facebook/nearby/server/NearbyServiceHandler;-><init>(LX/18V;LX/H3J;LX/H2u;LX/H2N;LX/H2Q;LX/H1u;LX/H22;LX/0SG;LX/03V;LX/H1i;)V

    .line 2421377
    move-object v0, v3

    .line 2421378
    sput-object v0, Lcom/facebook/nearby/server/NearbyServiceHandler;->m:Lcom/facebook/nearby/server/NearbyServiceHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2421379
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2421380
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2421381
    :cond_1
    sget-object v0, Lcom/facebook/nearby/server/NearbyServiceHandler;->m:Lcom/facebook/nearby/server/NearbyServiceHandler;

    return-object v0

    .line 2421382
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2421383
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/nearby/server/NearbyServiceHandler;)V
    .locals 12

    .prologue
    .line 2421384
    iget-object v0, p0, Lcom/facebook/nearby/server/NearbyServiceHandler;->g:LX/H22;

    invoke-virtual {v0}, LX/H22;->c()LX/H1v;

    move-result-object v8

    .line 2421385
    iget-object v0, v8, LX/H1v;->b:Ljava/util/List;

    move-object v0, v0

    .line 2421386
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H29;

    .line 2421387
    iget-object v1, p0, Lcom/facebook/nearby/server/NearbyServiceHandler;->f:LX/H1u;

    .line 2421388
    iget-object v2, v0, LX/H29;->c:LX/0Rf;

    move-object v2, v2

    .line 2421389
    iget-object v3, v0, LX/H29;->d:LX/0Rf;

    move-object v3, v3

    .line 2421390
    const/4 v4, 0x1

    new-array v4, v4, [Lcom/facebook/nearby/model/MapTile;

    const/4 v5, 0x0

    .line 2421391
    iget-object v6, v0, LX/H29;->a:Lcom/facebook/nearby/model/MapTile;

    move-object v6, v6

    .line 2421392
    aput-object v6, v4, v5

    invoke-static {v4}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    .line 2421393
    iget-object v5, v8, LX/H1v;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2421394
    iget-wide v10, v0, LX/H29;->b:J

    move-wide v6, v10

    .line 2421395
    invoke-virtual/range {v1 .. v7}, LX/H1u;->a(Ljava/util/Set;Ljava/util/Set;Ljava/util/List;Ljava/lang/String;J)V

    goto :goto_0

    .line 2421396
    :cond_0
    return-void
.end method

.method private b(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2421397
    iget-object v0, p0, Lcom/facebook/nearby/server/NearbyServiceHandler;->a:LX/18V;

    invoke-virtual {v0}, LX/18V;->a()LX/2VK;

    move-result-object v1

    .line 2421398
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2421399
    const-string v2, "searchNearbyPlacesAndLayoutsParams"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsParams;

    .line 2421400
    iget-object v2, p0, Lcom/facebook/nearby/server/NearbyServiceHandler;->b:LX/H3J;

    iget-object v3, v0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsParams;->a:Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;

    invoke-static {v2, v3}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v2

    const-string v3, "search_places"

    .line 2421401
    iput-object v3, v2, LX/2Vk;->c:Ljava/lang/String;

    .line 2421402
    move-object v2, v2

    .line 2421403
    invoke-virtual {v2}, LX/2Vk;->a()LX/2Vj;

    move-result-object v2

    invoke-interface {v1, v2}, LX/2VK;->a(LX/2Vj;)V

    .line 2421404
    iget-object v2, p0, Lcom/facebook/nearby/server/NearbyServiceHandler;->e:LX/H2Q;

    iget-object v0, v0, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsParams;->b:Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutParams;

    invoke-static {v2, v0}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v2, "layout_places"

    .line 2421405
    iput-object v2, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2421406
    move-object v0, v0

    .line 2421407
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v1, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 2421408
    :try_start_0
    const-string v0, "searchPlacesAndLayoutsRequest"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2421409
    :goto_0
    const-string v0, "search_places"

    invoke-interface {v1, v0}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;

    .line 2421410
    const-string v2, "search_places"

    invoke-interface {v1, v2}, LX/2VK;->b(Ljava/lang/String;)Ljava/lang/Exception;

    move-result-object v2

    .line 2421411
    if-eqz v2, :cond_0

    .line 2421412
    throw v2

    .line 2421413
    :catch_0
    move-exception v0

    .line 2421414
    iget-object v2, p0, Lcom/facebook/nearby/server/NearbyServiceHandler;->j:LX/03V;

    const-string v3, "SEARCH_NEARBY_PLACES_AND_LAYOUT"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    const/4 v3, 0x0

    .line 2421415
    iput-boolean v3, v0, LX/0VK;->d:Z

    .line 2421416
    move-object v0, v0

    .line 2421417
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/03V;->a(LX/0VG;)V

    goto :goto_0

    .line 2421418
    :cond_0
    if-nez v0, :cond_1

    .line 2421419
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2421420
    :goto_1
    return-object v0

    .line 2421421
    :cond_1
    const-string v2, "layout_places"

    invoke-interface {v1, v2}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutResult;

    .line 2421422
    new-instance v2, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;

    invoke-direct {v2, v0, v1}, Lcom/facebook/nearby/protocol/NearbyTypeaheadWithLayoutsResult;-><init>(Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutResult;)V

    .line 2421423
    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2421424
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2421425
    const-string v1, "search_nearby_places_and_layout"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2421426
    invoke-direct {p0, p1}, Lcom/facebook/nearby/server/NearbyServiceHandler;->b(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2421427
    :goto_0
    return-object v0

    .line 2421428
    :cond_0
    const-string v1, "fetch_layout_and_tiles"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2421429
    invoke-direct {p0, p1}, Lcom/facebook/nearby/server/NearbyServiceHandler;->a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2421430
    :cond_1
    new-instance v1, Ljava/lang/Exception;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
.end method
