.class public Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nearby/model/TypeaheadPlaceWithLayoutDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final layout:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "layout"
    .end annotation
.end field

.field public final typeaheadPlace:Lcom/facebook/nearby/model/TypeaheadPlace;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "typeahead_place"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2416668
    const-class v0, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayoutDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2416690
    new-instance v0, LX/H2B;

    invoke-direct {v0}, LX/H2B;-><init>()V

    sput-object v0, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2416686
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2416687
    iput-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;->typeaheadPlace:Lcom/facebook/nearby/model/TypeaheadPlace;

    .line 2416688
    iput-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;->layout:Ljava/lang/String;

    .line 2416689
    return-void
.end method

.method public constructor <init>(Lcom/facebook/nearby/model/TypeaheadPlace;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2416682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2416683
    iput-object p1, p0, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;->typeaheadPlace:Lcom/facebook/nearby/model/TypeaheadPlace;

    .line 2416684
    iput-object p2, p0, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;->layout:Ljava/lang/String;

    .line 2416685
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2416681
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2416673
    if-ne p0, p1, :cond_1

    .line 2416674
    :cond_0
    :goto_0
    return v0

    .line 2416675
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 2416676
    goto :goto_0

    .line 2416677
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 2416678
    goto :goto_0

    .line 2416679
    :cond_3
    check-cast p1, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;

    .line 2416680
    iget-object v2, p1, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;->typeaheadPlace:Lcom/facebook/nearby/model/TypeaheadPlace;

    iget-object v3, p0, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;->typeaheadPlace:Lcom/facebook/nearby/model/TypeaheadPlace;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p1, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;->layout:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;->layout:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2416672
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;->typeaheadPlace:Lcom/facebook/nearby/model/TypeaheadPlace;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;->layout:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2416669
    iget-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;->typeaheadPlace:Lcom/facebook/nearby/model/TypeaheadPlace;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2416670
    iget-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;->layout:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2416671
    return-void
.end method
