.class public Lcom/facebook/nearby/model/MapTile;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nearby/model/MapTileDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/model/MapTile;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final backgroundPlaces:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "backgroundPlaces"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;",
            ">;"
        }
    .end annotation
.end field

.field public final bounds:Lcom/facebook/graphql/model/GraphQLGeoRectangle;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "bounds"
    .end annotation
.end field

.field public final creationTime:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "creationTime"
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field public final maxZoom:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "maxZoom"
    .end annotation
.end field

.field public final minZoom:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "minZoom"
    .end annotation
.end field

.field public final places:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "places"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;",
            ">;"
        }
    .end annotation
.end field

.field public final timeToLiveInSeconds:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "timeToLiveInSeconds"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2416289
    const-class v0, Lcom/facebook/nearby/model/MapTileDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2416343
    const-class v0, Lcom/facebook/nearby/model/MapTileSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2416342
    new-instance v0, LX/H24;

    invoke-direct {v0}, LX/H24;-><init>()V

    sput-object v0, Lcom/facebook/nearby/model/MapTile;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2416332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2416333
    iput-object v0, p0, Lcom/facebook/nearby/model/MapTile;->id:Ljava/lang/String;

    .line 2416334
    iput-object v0, p0, Lcom/facebook/nearby/model/MapTile;->bounds:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 2416335
    iput-wide v2, p0, Lcom/facebook/nearby/model/MapTile;->creationTime:J

    .line 2416336
    iput-wide v2, p0, Lcom/facebook/nearby/model/MapTile;->timeToLiveInSeconds:J

    .line 2416337
    iput v1, p0, Lcom/facebook/nearby/model/MapTile;->minZoom:F

    .line 2416338
    iput v1, p0, Lcom/facebook/nearby/model/MapTile;->maxZoom:F

    .line 2416339
    iput-object v0, p0, Lcom/facebook/nearby/model/MapTile;->places:Ljava/util/List;

    .line 2416340
    iput-object v0, p0, Lcom/facebook/nearby/model/MapTile;->backgroundPlaces:Ljava/util/List;

    .line 2416341
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2416322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2416323
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/model/MapTile;->id:Ljava/lang/String;

    .line 2416324
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/nearby/model/MapTile;->creationTime:J

    .line 2416325
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/nearby/model/MapTile;->timeToLiveInSeconds:J

    .line 2416326
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    iput-object v0, p0, Lcom/facebook/nearby/model/MapTile;->bounds:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 2416327
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/model/MapTile;->minZoom:F

    .line 2416328
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/model/MapTile;->maxZoom:F

    .line 2416329
    const-class v0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/model/MapTile;->places:Ljava/util/List;

    .line 2416330
    const-class v0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/model/MapTile;->backgroundPlaces:Ljava/util/List;

    .line 2416331
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JJFFLcom/facebook/graphql/model/GraphQLGeoRectangle;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "JJFF",
            "Lcom/facebook/graphql/model/GraphQLGeoRectangle;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2416312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2416313
    iput-object p1, p0, Lcom/facebook/nearby/model/MapTile;->id:Ljava/lang/String;

    .line 2416314
    iput-object p8, p0, Lcom/facebook/nearby/model/MapTile;->bounds:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 2416315
    iput-wide p2, p0, Lcom/facebook/nearby/model/MapTile;->creationTime:J

    .line 2416316
    iput-wide p4, p0, Lcom/facebook/nearby/model/MapTile;->timeToLiveInSeconds:J

    .line 2416317
    iput p6, p0, Lcom/facebook/nearby/model/MapTile;->minZoom:F

    .line 2416318
    iput p7, p0, Lcom/facebook/nearby/model/MapTile;->maxZoom:F

    .line 2416319
    iput-object p9, p0, Lcom/facebook/nearby/model/MapTile;->places:Ljava/util/List;

    .line 2416320
    iput-object p10, p0, Lcom/facebook/nearby/model/MapTile;->backgroundPlaces:Ljava/util/List;

    .line 2416321
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 2416311
    iget-wide v0, p0, Lcom/facebook/nearby/model/MapTile;->timeToLiveInSeconds:J

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2416310
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2416302
    if-ne p0, p1, :cond_1

    .line 2416303
    :cond_0
    :goto_0
    return v0

    .line 2416304
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 2416305
    goto :goto_0

    .line 2416306
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 2416307
    goto :goto_0

    .line 2416308
    :cond_3
    check-cast p1, Lcom/facebook/nearby/model/MapTile;

    .line 2416309
    iget-object v2, p1, Lcom/facebook/nearby/model/MapTile;->id:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/nearby/model/MapTile;->id:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-wide v2, p1, Lcom/facebook/nearby/model/MapTile;->creationTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v4, p0, Lcom/facebook/nearby/model/MapTile;->creationTime:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-wide v2, p1, Lcom/facebook/nearby/model/MapTile;->timeToLiveInSeconds:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v4, p0, Lcom/facebook/nearby/model/MapTile;->timeToLiveInSeconds:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p1, Lcom/facebook/nearby/model/MapTile;->bounds:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    iget-object v3, p0, Lcom/facebook/nearby/model/MapTile;->bounds:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-static {v2, v3}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLGeoRectangle;Lcom/facebook/graphql/model/GraphQLGeoRectangle;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p1, Lcom/facebook/nearby/model/MapTile;->minZoom:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iget v3, p0, Lcom/facebook/nearby/model/MapTile;->minZoom:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p1, Lcom/facebook/nearby/model/MapTile;->maxZoom:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iget v3, p0, Lcom/facebook/nearby/model/MapTile;->maxZoom:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p1, Lcom/facebook/nearby/model/MapTile;->places:Ljava/util/List;

    iget-object v3, p0, Lcom/facebook/nearby/model/MapTile;->places:Ljava/util/List;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p1, Lcom/facebook/nearby/model/MapTile;->backgroundPlaces:Ljava/util/List;

    iget-object v3, p0, Lcom/facebook/nearby/model/MapTile;->backgroundPlaces:Ljava/util/List;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 8

    .prologue
    .line 2416299
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/nearby/model/MapTile;->id:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/nearby/model/MapTile;->creationTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/facebook/nearby/model/MapTile;->timeToLiveInSeconds:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/nearby/model/MapTile;->bounds:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 2416300
    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->j()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->k()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->l()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->a()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v4

    move v2, v4

    .line 2416301
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/facebook/nearby/model/MapTile;->minZoom:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/facebook/nearby/model/MapTile;->maxZoom:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/nearby/model/MapTile;->places:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/nearby/model/MapTile;->backgroundPlaces:Ljava/util/List;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2416290
    iget-object v0, p0, Lcom/facebook/nearby/model/MapTile;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2416291
    iget-wide v0, p0, Lcom/facebook/nearby/model/MapTile;->creationTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2416292
    iget-wide v0, p0, Lcom/facebook/nearby/model/MapTile;->timeToLiveInSeconds:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2416293
    iget-object v0, p0, Lcom/facebook/nearby/model/MapTile;->bounds:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2416294
    iget v0, p0, Lcom/facebook/nearby/model/MapTile;->minZoom:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2416295
    iget v0, p0, Lcom/facebook/nearby/model/MapTile;->maxZoom:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2416296
    iget-object v0, p0, Lcom/facebook/nearby/model/MapTile;->places:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2416297
    iget-object v0, p0, Lcom/facebook/nearby/model/MapTile;->backgroundPlaces:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2416298
    return-void
.end method
