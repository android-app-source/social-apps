.class public Lcom/facebook/nearby/model/TypeaheadPlace;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/model/TypeaheadPlace;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:D

.field public final d:Lcom/facebook/graphql/model/GraphQLImage;

.field public final e:Lcom/facebook/graphql/model/GraphQLLocation;

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Lcom/facebook/graphql/model/GraphQLStreetAddress;

.field public final h:I

.field public final i:Lcom/facebook/graphql/enums/GraphQLPlaceType;

.field public final j:Z

.field public final k:Lcom/facebook/graphql/enums/GraphQLSavedState;

.field public final l:Z

.field public final m:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2416612
    new-instance v0, LX/H2A;

    invoke-direct {v0}, LX/H2A;-><init>()V

    sput-object v0, Lcom/facebook/nearby/model/TypeaheadPlace;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2416613
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2416614
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->a:Ljava/lang/String;

    .line 2416615
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->b:Ljava/lang/String;

    .line 2416616
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->f:Ljava/util/List;

    .line 2416617
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreetAddress;

    iput-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->g:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 2416618
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->c:D

    .line 2416619
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->d:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2416620
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    iput-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->e:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 2416621
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->h:I

    .line 2416622
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPlaceType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->i:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 2416623
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->j:Z

    .line 2416624
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLSavedState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->k:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2416625
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->l:Z

    .line 2416626
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->m:Z

    .line 2416627
    return-void

    :cond_0
    move v0, v2

    .line 2416628
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2416629
    goto :goto_1

    :cond_2
    move v1, v2

    .line 2416630
    goto :goto_2
.end method

.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLNode;)V
    .locals 2

    .prologue
    .line 2416631
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2416632
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->a:Ljava/lang/String;

    .line 2416633
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->b:Ljava/lang/String;

    .line 2416634
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->bj()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->f:Ljava/util/List;

    .line 2416635
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->x()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->g:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 2416636
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->fW()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->c:D

    .line 2416637
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->hn()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->d:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2416638
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->fj()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->e:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 2416639
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->fn()I

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->h:I

    .line 2416640
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->gA()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->i:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 2416641
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->eD()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->j:Z

    .line 2416642
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->kT()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->k:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2416643
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->cc()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->l:Z

    .line 2416644
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->eC()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->m:Z

    .line 2416645
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2416646
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2416647
    iget-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2416648
    iget-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2416649
    iget-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->f:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2416650
    iget-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->g:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2416651
    iget-wide v4, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->c:D

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeDouble(D)V

    .line 2416652
    iget-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->d:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2416653
    iget-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->e:Lcom/facebook/graphql/model/GraphQLLocation;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2416654
    iget v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2416655
    iget-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->i:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->i:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPlaceType;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2416656
    iget-boolean v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->j:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2416657
    iget-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->k:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->k:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLSavedState;->name()Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2416658
    iget-boolean v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->l:Z

    if-eqz v0, :cond_3

    move v0, v2

    :goto_2
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2416659
    iget-boolean v0, p0, Lcom/facebook/nearby/model/TypeaheadPlace;->m:Z

    if-eqz v0, :cond_4

    :goto_3
    int-to-byte v0, v2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2416660
    return-void

    :cond_1
    move-object v0, v1

    .line 2416661
    goto :goto_0

    :cond_2
    move v0, v3

    .line 2416662
    goto :goto_1

    :cond_3
    move v0, v3

    .line 2416663
    goto :goto_2

    :cond_4
    move v2, v3

    .line 2416664
    goto :goto_3
.end method
