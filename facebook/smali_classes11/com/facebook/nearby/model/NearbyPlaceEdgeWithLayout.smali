.class public Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayoutDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final layout:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "layout"
    .end annotation
.end field

.field public final placeEdge:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "placeEdge"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2416430
    const-class v0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayoutDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2416429
    const-class v0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayoutSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2416428
    new-instance v0, LX/H26;

    invoke-direct {v0}, LX/H26;-><init>()V

    sput-object v0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2416424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2416425
    iput-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;->placeEdge:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;

    .line 2416426
    iput-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;->layout:Ljava/lang/String;

    .line 2416427
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2416420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2416421
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;

    iput-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;->placeEdge:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;

    .line 2416422
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;->layout:Ljava/lang/String;

    .line 2416423
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2416416
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2416417
    iput-object p1, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;->placeEdge:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;

    .line 2416418
    iput-object p2, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;->layout:Ljava/lang/String;

    .line 2416419
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2416415
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2416401
    if-ne p0, p1, :cond_1

    .line 2416402
    :cond_0
    :goto_0
    return v0

    .line 2416403
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 2416404
    goto :goto_0

    .line 2416405
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 2416406
    goto :goto_0

    .line 2416407
    :cond_3
    check-cast p1, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;

    .line 2416408
    iget-object v2, p1, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;->placeEdge:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;

    iget-object v3, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;->placeEdge:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2416409
    if-nez v2, :cond_6

    if-nez v3, :cond_6

    .line 2416410
    :cond_4
    :goto_1
    move v2, v4

    .line 2416411
    if-eqz v2, :cond_5

    iget-object v2, p1, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;->layout:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;->layout:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_5
    move v0, v1

    goto :goto_0

    .line 2416412
    :cond_6
    if-eqz v2, :cond_7

    if-nez v3, :cond_8

    :cond_7
    move v4, v5

    .line 2416413
    goto :goto_1

    .line 2416414
    :cond_8
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->k()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->k()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-static {v6, v7}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    :cond_9
    move v4, v5

    goto :goto_1
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2416397
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;->placeEdge:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;->layout:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2416398
    iget-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;->placeEdge:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2416399
    iget-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;->layout:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2416400
    return-void
.end method
