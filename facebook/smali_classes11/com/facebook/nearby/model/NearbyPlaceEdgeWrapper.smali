.class public Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;

.field public b:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;

.field public c:Lcom/facebook/graphql/model/GraphQLPage;

.field public d:Lcom/facebook/graphql/model/GraphQLGraphSearchConnectedFriendsConnection;

.field public e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel;

.field public f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel$NodeModel;

.field public g:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

.field public h:Landroid/location/Location;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2416469
    new-instance v0, LX/H27;

    invoke-direct {v0}, LX/H27;-><init>()V

    sput-object v0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2416504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2416505
    iput-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->a:Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;

    .line 2416506
    iput-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->b:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;

    .line 2416507
    iput-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->c:Lcom/facebook/graphql/model/GraphQLPage;

    .line 2416508
    iput-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->d:Lcom/facebook/graphql/model/GraphQLGraphSearchConnectedFriendsConnection;

    .line 2416509
    iput-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel;

    .line 2416510
    iput-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel$NodeModel;

    .line 2416511
    iput-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->g:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    .line 2416512
    iput-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->h:Landroid/location/Location;

    .line 2416513
    const-class v0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;

    .line 2416514
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel;

    .line 2416515
    if-eqz v0, :cond_0

    .line 2416516
    iput-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->a:Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;

    .line 2416517
    iget-object p1, v0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;->placeEdge:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;

    iput-object p1, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->b:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;

    .line 2416518
    iget-object p1, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->b:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->c:Lcom/facebook/graphql/model/GraphQLPage;

    .line 2416519
    iget-object p1, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->b:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object p1

    .line 2416520
    if-eqz p1, :cond_0

    .line 2416521
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->a()Lcom/facebook/graphql/model/GraphQLGraphSearchConnectedFriendsConnection;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->d:Lcom/facebook/graphql/model/GraphQLGraphSearchConnectedFriendsConnection;

    .line 2416522
    :cond_0
    if-eqz v1, :cond_1

    .line 2416523
    iput-object v1, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel;

    .line 2416524
    invoke-virtual {v1}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel;->a()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel$NodeModel;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel$NodeModel;

    .line 2416525
    iget-object p1, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel$NodeModel;

    if-eqz p1, :cond_1

    .line 2416526
    iget-object p1, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel$NodeModel;

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel$NodeModel;->j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->g:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    .line 2416527
    :cond_1
    return-void
.end method

.method private e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2416534
    iget-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->c:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    .line 2416535
    iget-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->c:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    .line 2416536
    :goto_0
    return-object v0

    .line 2416537
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel$NodeModel;

    if-eqz v0, :cond_1

    .line 2416538
    iget-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2416539
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method private f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2416528
    iget-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->c:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    .line 2416529
    iget-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->c:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v0

    .line 2416530
    :goto_0
    return-object v0

    .line 2416531
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel$NodeModel;

    if-eqz v0, :cond_1

    .line 2416532
    iget-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2416533
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method public final a()D
    .locals 2

    .prologue
    .line 2416484
    iget-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->c:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    .line 2416485
    iget-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->c:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->N()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    .line 2416486
    if-eqz v0, :cond_1

    .line 2416487
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v0

    .line 2416488
    :goto_0
    return-wide v0

    .line 2416489
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel$NodeModel;

    if-eqz v0, :cond_1

    .line 2416490
    iget-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel$NodeModel;->l()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    move-result-object v0

    .line 2416491
    if-eqz v0, :cond_1

    .line 2416492
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;->a()D

    move-result-wide v0

    goto :goto_0

    .line 2416493
    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final c()D
    .locals 2

    .prologue
    .line 2416494
    iget-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->c:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    .line 2416495
    iget-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->c:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->N()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    .line 2416496
    if-eqz v0, :cond_1

    .line 2416497
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v0

    .line 2416498
    :goto_0
    return-wide v0

    .line 2416499
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel$NodeModel;

    if-eqz v0, :cond_1

    .line 2416500
    iget-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel$NodeModel;->l()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    move-result-object v0

    .line 2416501
    if-eqz v0, :cond_1

    .line 2416502
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;->b()D

    move-result-wide v0

    goto :goto_0

    .line 2416503
    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final d()D
    .locals 2

    .prologue
    .line 2416478
    iget-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->b:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;

    if-eqz v0, :cond_0

    .line 2416479
    iget-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->b:Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlacesTileResultsEdge;->k()D

    move-result-wide v0

    .line 2416480
    :goto_0
    return-wide v0

    .line 2416481
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel;

    if-eqz v0, :cond_1

    .line 2416482
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    goto :goto_0

    .line 2416483
    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2416477
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 2416473
    invoke-virtual {p0}, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->d()D

    move-result-wide v0

    .line 2416474
    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpg-double v2, v0, v2

    if-gez v2, :cond_0

    .line 2416475
    const-string v2, "%s [name=%s, id=%s, searchScore=%f]"

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0}, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->f()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0}, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->e()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-static {v2, v3, v4, v5, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2416476
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "%s [name=%s, id=%s]"

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->f()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2416470
    iget-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->a:Lcom/facebook/nearby/model/NearbyPlaceEdgeWithLayout;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2416471
    iget-object v0, p0, Lcom/facebook/nearby/model/NearbyPlaceEdgeWrapper;->e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldBrowseOldResultConnectionFragmentModel$EdgesModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2416472
    return-void
.end method
