.class public Lcom/facebook/nearby/model/NearbyTilesMethodResult;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nearby/model/NearbyTilesMethodResultDeserializer;
.end annotation


# instance fields
.field public final nearbyTiles:Lcom/facebook/nearby/model/NearbyTiles;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "tiles"
    .end annotation
.end field

.field public final searchSessionKey:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "search_session_key"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2416575
    const-class v0, Lcom/facebook/nearby/model/NearbyTilesMethodResultDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2416576
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2416577
    iput-object v0, p0, Lcom/facebook/nearby/model/NearbyTilesMethodResult;->nearbyTiles:Lcom/facebook/nearby/model/NearbyTiles;

    .line 2416578
    iput-object v0, p0, Lcom/facebook/nearby/model/NearbyTilesMethodResult;->searchSessionKey:Ljava/lang/String;

    .line 2416579
    return-void
.end method
