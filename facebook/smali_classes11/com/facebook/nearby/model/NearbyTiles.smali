.class public Lcom/facebook/nearby/model/NearbyTiles;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation


# instance fields
.field public final canonicalization:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "canonicalization"
    .end annotation
.end field

.field public final displayRegionHint:Lcom/facebook/graphql/model/GraphQLGeoRectangle;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "display_region_hint"
    .end annotation
.end field

.field public final tiles:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "edges"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMapTileNode;",
            ">;"
        }
    .end annotation
.end field

.field public final version:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "version"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2416544
    const-class v0, Lcom/facebook/nearby/model/NearbyTilesDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2416545
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2416546
    iput-object v0, p0, Lcom/facebook/nearby/model/NearbyTiles;->tiles:Ljava/util/List;

    .line 2416547
    iput-object v0, p0, Lcom/facebook/nearby/model/NearbyTiles;->version:Ljava/lang/String;

    .line 2416548
    iput-object v0, p0, Lcom/facebook/nearby/model/NearbyTiles;->canonicalization:Ljava/lang/String;

    .line 2416549
    iput-object v0, p0, Lcom/facebook/nearby/model/NearbyTiles;->displayRegionHint:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 2416550
    return-void
.end method
